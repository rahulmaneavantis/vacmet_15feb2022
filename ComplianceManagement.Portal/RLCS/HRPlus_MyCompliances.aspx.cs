﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OfficeOpenXml;
using System.IO;
using System.Globalization;
using OfficeOpenXml.Style;
using System.Data;
using System.Web.Security;
using com.VirtuosoITech.ComplianceManagement.Portal.ProductMapping;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
	public partial class HRPlus_MyCompliances : System.Web.UI.Page
	{
		public static List<long> SelectedBranchList = new List<long>();
		protected bool initialLoad;
		public string filter = String.Empty;
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				try
				{
					initialLoad = true;

					string filterType = string.Empty;
					string comType = string.Empty;
					string ESIcomType = string.Empty;
					string strCustID = string.Empty;
					string strUserID = string.Empty;

					string strStartPeriod = string.Empty;
					string strEndPeriod = string.Empty;
					// var userAssignedBranchList =;

					BindCustomers();
					ddlCustomer_SelectedIndexChanged(sender, e);
					//BindSPOCs();

					BindActList(SelectedBranchList);

					if (!string.IsNullOrEmpty(Request.QueryString["filterType"]))
					{
						filterType = Request.QueryString["filterType"];
						filter = filterType;
						if (ddlFType.Items.FindByValue(filterType) != null)
						{
							ddlFType.ClearSelection();
							ddlFType.Items.FindByValue(filterType).Selected = true;
						}
					}

					if (!string.IsNullOrEmpty(Request.QueryString["comType"]))
					{
						comType = Request.QueryString["comType"];

						if (ddlComType.Items.FindByValue(comType) != null)
						{
							ddlComType.ClearSelection();
							ddlComType.Items.FindByValue(comType).Selected = true;
						}
					}

					if (!string.IsNullOrEmpty(Request.QueryString["ESIcomType"]))
					{
						ESIcomType = Request.QueryString["ESIcomType"];

						if (ddlESIComType.Items.FindByValue(ESIcomType) != null)
						{
							ddlESIComType.ClearSelection();
							ddlESIComType.Items.FindByValue(ESIcomType).Selected = true;
						}
					}

					if (!string.IsNullOrEmpty(Request.QueryString["customerid"]))
					{
						strCustID = Request.QueryString["customerid"];

						if (ddlCustomers.Items.FindByValue(strCustID) != null)
						{
							ddlCustomers.ClearSelection();
							ddlCustomers.Items.FindByValue(strCustID).Selected = true;
						}
					}

					if (!string.IsNullOrEmpty(Request.QueryString["userID"]))
					{
						strUserID = Request.QueryString["userID"];

						if (ddlSPOCs.Items.FindByValue(strUserID) != null)
						{
							ddlSPOCs.ClearSelection();
							ddlSPOCs.Items.FindByValue(strUserID).Selected = true;
						}
					}

					if (!string.IsNullOrEmpty(Request.QueryString["startPeriod"]))
					{
						strStartPeriod = Request.QueryString["startPeriod"];

						if (!string.IsNullOrEmpty(strStartPeriod))
						{
							txtStartPeriod.Text = strStartPeriod;
						}
					}

					if (!string.IsNullOrEmpty(Request.QueryString["endPeriod"]))
					{
						strEndPeriod = Request.QueryString["endPeriod"];

						if (!string.IsNullOrEmpty(strEndPeriod))
						{
							txtEndPeriod.Text = strEndPeriod;
						}
					}

					if (!string.IsNullOrEmpty(Request.QueryString["branchID"]))
					{
						ClearTreeViewSelection(tvFilterLocation);
						TreeViewSelectedNode(tvFilterLocation, Request.QueryString["branchID"]);
						tvFilterLocation_SelectedNodeChanged(sender, e);
					}

					if (!string.IsNullOrEmpty(Request.QueryString["risk"]))
					{
						if (ddlRisk.Items.FindByText(Request.QueryString["risk"]) != null)
						{
							ddlRisk.ClearSelection();
							ddlRisk.Items.FindByText(Request.QueryString["risk"]).Selected = true;
						}
					}

					if (!string.IsNullOrEmpty(Request.QueryString["Freq"]))
					{
						string strFreq = Request.QueryString["Freq"];

						if (ddlFrequency.Items.FindByValue(strFreq) != null)
						{
							ddlFrequency.ClearSelection();
							ddlFrequency.Items.FindByValue(strFreq).Selected = true;

							ddlFrequency_SelectedIndexChanged(null, null);
						}
					}

					if (!string.IsNullOrEmpty(Request.QueryString["period"]))
					{
						string strPeriods = Request.QueryString["period"];

						SetSelectedPeriod(strPeriods);
					}
					else
					{
						txtPeriodList.Enabled = false;
					}

					if (!string.IsNullOrEmpty(Request.QueryString["year"]))
					{
						string strYear = Request.QueryString["year"];

						if (ddlYear.Items.FindByValue(strYear) != null)
						{
							ddlYear.ClearSelection();
							ddlYear.Items.FindByValue(strYear).Selected = true;
						}
					}

					BindGrid();
				}
				catch (Exception ex)
				{
					LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
				}
			}
		}

		public void BindCustomers()
		{
			try
			{
				int customerID = -1;
				int serviceProviderID = -1;
				int distributorID = -1;

				if (AuthenticationHelper.Role == "CADMN")
				{
					customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
				}
				else if (AuthenticationHelper.Role == "SPADM")
				{
					serviceProviderID = Convert.ToInt32(AuthenticationHelper.CustomerID);
				}
				else if (AuthenticationHelper.Role == "DADMN")
				{
					distributorID = Convert.ToInt32(AuthenticationHelper.CustomerID);
				}
				else
				{
					customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
				}

				ddlCustomers.Items.Clear();
				ddlCustomers.DataTextField = "CustomerName";
				ddlCustomers.DataValueField = "CustomerID";
				//var lstMyAssignedCustomers = UserCustomerMappingManagement.Get_UserCustomerMapping(AuthenticationHelper.UserID);
				var lstMyAssignedCustomers = UserCustomerMappingManagement.Get_AssignedCustomers(AuthenticationHelper.UserID, distributorID, AuthenticationHelper.Role);
				ddlCustomers.DataSource = lstMyAssignedCustomers;
				ddlCustomers.DataBind();

				ddlCustomers.Items.Insert(0, new ListItem("Customer", "-1"));
			}
			catch (Exception ex)
			{
				LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
			}
		}

		private void BindSPOCs()
		{
			try
			{
				int customerID = -1;
				int serviceProviderID = -1;
				int distributorID = -1;

				if (AuthenticationHelper.Role == "CADMN")
				{
					customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
				}
				else if (AuthenticationHelper.Role == "SPADM")
				{
					serviceProviderID = Convert.ToInt32(AuthenticationHelper.CustomerID);
				}
				else if (AuthenticationHelper.Role == "DADMN")
				{
					distributorID = Convert.ToInt32(AuthenticationHelper.CustomerID);
				}
				else
				{
					customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
				}

				if (!string.IsNullOrEmpty(ddlCustomers.SelectedValue) && ddlCustomers.SelectedValue != "-1")
				{
					customerID = Convert.ToInt32(ddlCustomers.SelectedValue);
				}

				if (customerID != -1)
				{
					ddlSPOCs.Items.Clear();
					ddlSPOCs.DataTextField = "Name";
					ddlSPOCs.DataValueField = "ID";
					using (ComplianceDBEntities entities = new ComplianceDBEntities())
					{
						var lstRecords = (entities.SP_RLCS_ComplianceInstanceTransactionCount_Dashboard(AuthenticationHelper.UserID, AuthenticationHelper.ProfileID, customerID, distributorID, serviceProviderID, AuthenticationHelper.Role).Where(x => x.RoleID == 3).Distinct());
						var userList = (from row in entities.Users
										where row.IsActive == true && (customerID != -1 && row.CustomerID == customerID)
										select new { ID = row.ID, Name = row.FirstName + " " + row.LastName }).Distinct().ToList();

						var PerformerList = (from row in userList
											 join item in lstRecords on row.ID equals item.UserID
											 select new { ID = row.ID, row.Name }).Distinct();

						ddlSPOCs.DataSource = PerformerList;
						ddlSPOCs.DataBind();
					}
					//var lstMyAssignedSPOCs = UserCustomerMappingManagement.GetAllUser_ServiceProviderDistributorCustomer(customerID, serviceProviderID, distributorID);
				}
				else
				{
					ddlSPOCs.Items.Clear();
					ddlSPOCs.DataTextField = "UserName";
					ddlSPOCs.DataValueField = "UserID";
					using (ComplianceDBEntities entities = new ComplianceDBEntities())
					{
						var lstRecords = (entities.SP_RLCS_ComplianceInstanceTransactionCount_Dashboard(AuthenticationHelper.UserID, AuthenticationHelper.ProfileID, customerID, distributorID, serviceProviderID, AuthenticationHelper.Role).Where(x => x.RoleID == 3).Distinct());
						var userList = (from row in entities.Users
										where row.IsActive == true
										select new { ID = row.ID, Name = row.FirstName + " " + row.LastName }).Distinct().ToList();

						var PerformerList = (from row in userList
											 join item in lstRecords on row.ID equals item.UserID
											 select new { ID = row.ID, row.Name }).Distinct();

						ddlSPOCs.DataSource = PerformerList;
						ddlSPOCs.DataBind();
					}
					//var lstMyAssignedSPOCs = UserCustomerMappingManagement.Get_AssignedSPOC(AuthenticationHelper.UserID, distributorID, AuthenticationHelper.Role);
				}

				ddlSPOCs.Items.Insert(0, new ListItem("Assigned User", "-1"));
			}
			catch (Exception ex)
			{
				LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
			}
		}

		private void BindActList(List<long> SelectedBranchList)
		{
			try
			{
				int categoryID = -1;
				int customerID = -1;

				if (!string.IsNullOrEmpty(ddlCustomers.SelectedValue) && ddlCustomers.SelectedValue != "-1")
				{
					customerID = Convert.ToInt32(ddlCustomers.SelectedValue);
				}

				var ActList = ActManagement.GetAllAssignedActs_RLCS(customerID, AuthenticationHelper.UserID, categoryID, AuthenticationHelper.ProfileID, SelectedBranchList, true);//1

				ddlAct.Items.Clear();
				ddlAct.DataTextField = "Name";
				ddlAct.DataValueField = "ID";
				ddlAct.DataSource = ActList;
				ddlAct.DataBind();
				ddlAct.Items.Insert(0, new ListItem("Act", "-1"));
			}
			catch (Exception ex)
			{
				LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
			}
		}

		private void LoadDashboardCustomerWise(List<Customer> lstMyAssignedCustomers)
		{
			int customerID = -1;
			int serviceProviderID = -1;
			int distributorID = -1;

			if (AuthenticationHelper.Role == "CADMN")
			{
				customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
			}
			else if (AuthenticationHelper.Role == "SPADM")
			{
				serviceProviderID = Convert.ToInt32(AuthenticationHelper.CustomerID);
			}
			else if (AuthenticationHelper.Role == "DADMN")
			{
				distributorID = Convert.ToInt32(AuthenticationHelper.CustomerID);
			}

			List<int> lstSelectedCustomers = new List<int>();

			if (lstMyAssignedCustomers == null)
				lstMyAssignedCustomers = UserCustomerMappingManagement.Get_UserCustomerMapping(AuthenticationHelper.UserID);

			using (ComplianceDBEntities entities = new ComplianceDBEntities())
			{
				entities.Database.CommandTimeout = 180;

				var MastertransactionsQuery = (entities.SP_RLCS_ComplianceInstanceTransactionCount_Dashboard(AuthenticationHelper.UserID, AuthenticationHelper.ProfileID, customerID, distributorID, serviceProviderID, AuthenticationHelper.Role)
										.Where(entry => entry.IsActive == true
											&& entry.IsUpcomingNotDeleted == true
											&& lstMyAssignedCustomers.Select(row => row.ID).Contains(entry.CustomerID))).ToList();

				if (!string.IsNullOrEmpty(ddlCustomers.SelectedValue) && ddlCustomers.SelectedValue != "-1")
				{
					lstSelectedCustomers.Add(Convert.ToInt32(ddlCustomers.SelectedValue));
				}

				if (lstSelectedCustomers.Count > 0)
				{
					lstMyAssignedCustomers = lstMyAssignedCustomers.Where(row => lstSelectedCustomers.Contains(row.ID)).ToList();
				}

				MastertransactionsQuery = GetFilteredData(MastertransactionsQuery);


			}
		}

		private void BindGrid()
		{
			try
			{
				lblAdvanceSearchScrum.Text = String.Empty;

				int customerID = -1;
				int serviceProviderID = -1;
				int distributorID = -1;

				if (AuthenticationHelper.Role == "CADMN")
				{
					customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
				}
				else if (AuthenticationHelper.Role == "SPADM")
				{
					serviceProviderID = Convert.ToInt32(AuthenticationHelper.CustomerID);
				}
				else if (AuthenticationHelper.Role == "DADMN")
				{
					distributorID = Convert.ToInt32(AuthenticationHelper.CustomerID);
				}
				else
				{
					customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
				}

				string roleCode = string.Empty;
				roleCode = AuthenticationHelper.Role;
				//if (AuthenticationHelper.Role == "HMGR" || AuthenticationHelper.Role == "DADMN")
				//    roleCode = "MGR";
				//else if (AuthenticationHelper.Role == "HEXCT")
				//    roleCode = "SPOC";

				using (ComplianceDBEntities entities = new ComplianceDBEntities())
				{
					entities.Database.CommandTimeout = 180;
					//var lstMyAssignedCustomers = UserCustomerMappingManagement.Get_UserCustomerMapping(AuthenticationHelper.UserID);
					var lstMyAssignedCustomers = UserCustomerMappingManagement.Get_AssignedCustomers(AuthenticationHelper.UserID, distributorID, AuthenticationHelper.Role);

					var masterRecords = (entities.SP_RLCS_ComplianceInstanceTransactionCount_Dashboard(AuthenticationHelper.UserID, AuthenticationHelper.ProfileID, customerID, distributorID, serviceProviderID, roleCode)
									   .Where(entry => entry.IsActive == true
										   && entry.IsUpcomingNotDeleted == true
										   && lstMyAssignedCustomers.Select(row => row.CustomerID).Contains(entry.CustomerID))).ToList();

					if (masterRecords.Count > 0)
					{
						if (initialLoad)
							BindYears(masterRecords);



						masterRecords = GetFilteredData(masterRecords);
					}

					Session["TotalRows"] = masterRecords.Count;

					gridCompliances.DataSource = masterRecords;
					gridCompliances.DataBind();
					gridCompliances.Visible = true;
				}

				UpDetailView.Update();
				GetPageDisplaySummary();
			}
			catch (Exception ex)
			{
				LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
			}
		}

		public List<SP_RLCS_ComplianceInstanceTransactionCount_Dashboard_Result> GetFilteredData(List<SP_RLCS_ComplianceInstanceTransactionCount_Dashboard_Result> MastertransactionsQuery)
		{
			#region Customer
			List<int> lstSelectedCustomers = new List<int>();
			if (!string.IsNullOrEmpty(ddlCustomers.SelectedValue) && ddlCustomers.SelectedValue != "-1")
			{
				lstSelectedCustomers.Add(Convert.ToInt32(ddlCustomers.SelectedValue));
			}

			if (lstSelectedCustomers.Count > 0)
			{
				MastertransactionsQuery = MastertransactionsQuery.Where(row => lstSelectedCustomers.Contains(row.CustomerID)).ToList();
			}

			#endregion

			#region Entity/Branch
			int custBranchID = 0;

			if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1")
				custBranchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
			else
				custBranchID = 0;

			if (custBranchID > 0)
				MastertransactionsQuery = MastertransactionsQuery.Where(entry => (entry.CustomerBranchID == custBranchID)).ToList();
			#endregion

			#region User
			List<long> lstSelectedSPOCs = new List<long>();
			if (!string.IsNullOrEmpty(ddlSPOCs.SelectedValue) && ddlSPOCs.SelectedValue != "-1")
			{
				lstSelectedSPOCs.Add(Convert.ToInt32(ddlSPOCs.SelectedValue));
			}

			if (lstSelectedSPOCs.Count > 0)
			{
				MastertransactionsQuery = MastertransactionsQuery.Where(row => lstSelectedSPOCs.Contains(row.UserID)).ToList();
			}
			#endregion

			#region Date Period Filter
			try
			{
				DateTime? startDate = null;
				DateTime? endDate = null;

				if (!string.IsNullOrEmpty(txtStartPeriod.Text) && !string.IsNullOrEmpty(txtEndPeriod.Text))
				{
					string[] startPeriod = txtStartPeriod.Text.Split('-');
					if (startPeriod.Length > 1)
					{
						string monthValue = com.VirtuosoITech.ComplianceManagement.Business.ComplianceManagement.GetMonthIndexFromAbbreviatedMonth(startPeriod[0]);

						if (!string.IsNullOrEmpty(monthValue))
						{
							int month = Convert.ToInt32(monthValue);
							int year = Convert.ToInt32(startPeriod[1]);

							startDate = new DateTime(year, month, 1).Date;
						}
					}

					string[] endPeriod = txtEndPeriod.Text.Split('-');
					if (endPeriod.Length > 1)
					{
						string monthValue = com.VirtuosoITech.ComplianceManagement.Business.ComplianceManagement.GetMonthIndexFromAbbreviatedMonth(endPeriod[0]);

						if (!string.IsNullOrEmpty(monthValue))
						{
							int month = Convert.ToInt32(monthValue);
							int year = Convert.ToInt32(endPeriod[1]);

							endDate = new DateTime(year, month, DateTime.DaysInMonth(year, month)).Date;
						}
					}
				}
				else if (!string.IsNullOrEmpty(txtStartPeriod.Text) && string.IsNullOrEmpty(txtEndPeriod.Text))
				{
					string[] startPeriod = txtStartPeriod.Text.Split('-');
					if (startPeriod.Length > 1)
					{
						string monthValue = com.VirtuosoITech.ComplianceManagement.Business.ComplianceManagement.GetMonthIndexFromAbbreviatedMonth(startPeriod[0]);

						if (!string.IsNullOrEmpty(monthValue))
						{
							int month = Convert.ToInt32(monthValue);
							int year = Convert.ToInt32(startPeriod[1]);

							startDate = new DateTime(year, month, 1).Date;
							endDate = new DateTime(year, month, DateTime.DaysInMonth(year, month)).Date;
						}
					}
				}
				else if (string.IsNullOrEmpty(txtStartPeriod.Text) && !string.IsNullOrEmpty(txtEndPeriod.Text))
				{
					string[] startPeriod = txtStartPeriod.Text.Split('-');
					if (startPeriod.Length > 1)
					{
						string monthValue = com.VirtuosoITech.ComplianceManagement.Business.ComplianceManagement.GetMonthIndexFromAbbreviatedMonth(startPeriod[0]);

						if (!string.IsNullOrEmpty(monthValue))
						{
							int month = Convert.ToInt32(monthValue);
							int year = Convert.ToInt32(startPeriod[1]);

							startDate = new DateTime(year, month, 1).Date;
							endDate = new DateTime(year, month, DateTime.DaysInMonth(year, month)).Date;
						}
					}
				}

				if (startDate != null && endDate != null)
					MastertransactionsQuery = MastertransactionsQuery.Where(row => row.PerformerScheduledOn.Date >= startDate && row.PerformerScheduledOn.Date <= endDate).ToList();
			}
			catch (Exception ex)
			{

			}

			#endregion

			#region Period Filter

			if (!string.IsNullOrEmpty(ddlFrequency.SelectedValue))
			{
				List<string> lstPeriod = new List<string>();
				lstPeriod = GetSelectedPeriod();

				string frequencySelected = string.Empty;
				frequencySelected = ddlFrequency.SelectedValue;

				if (frequencySelected.Equals("BiAnnual"))
					frequencySelected = "TwoYearly";

				if (ddlFrequency.SelectedValue != "-1" && lstPeriod.Count > 0)
				{
					MastertransactionsQuery = MastertransactionsQuery.Where(row => row.Frequency.Equals(frequencySelected) && lstPeriod.Contains(row.RLCS_PayrollMonth)).ToList();
				}
				else if (ddlFrequency.SelectedValue != "-1")
				{
					MastertransactionsQuery = MastertransactionsQuery.Where(row => row.Frequency.Equals(frequencySelected)).ToList();
				}
			}

			if (!string.IsNullOrEmpty(ddlYear.SelectedValue))
			{
				if (ddlYear.SelectedValue != "-1")
				{
					MastertransactionsQuery = MastertransactionsQuery.Where(row => row.RLCS_PayrollYear.Equals(ddlYear.SelectedValue)).ToList();
				}
			}

			#endregion

			string filterType, compType, ESICompType = string.Empty;

			#region FilterType- Upcoming, Overdue
			filterType = ddlFType.SelectedItem.Text;
			MastertransactionsQuery = DashboardData_ComplianceDisplayCount(MastertransactionsQuery, filterType);
			#endregion

			#region Compliance Type- Register, Returns, Challan
			compType = ddlComType.SelectedValue;
			ESICompType = ddlESIComType.SelectedValue;
			MastertransactionsQuery = FilterbyComType(compType, ESICompType, MastertransactionsQuery);
			#endregion

			if (!string.IsNullOrEmpty(ddlRisk.SelectedValue))
			{
				if (ddlRisk.SelectedValue != "-1")
				{
					MastertransactionsQuery = MastertransactionsQuery.Where(row => row.Risk.Equals(Convert.ToByte(ddlRisk.SelectedValue))).ToList();
				}
			}

			return MastertransactionsQuery;
		}

		public List<SP_RLCS_ComplianceInstanceTransactionCount_Dashboard_Result> FilterbyComType(string compType, string ESICompType, List<SP_RLCS_ComplianceInstanceTransactionCount_Dashboard_Result> MastertransactionsQuery)
		{
			using (ComplianceDBEntities entities = new ComplianceDBEntities())
			{
				List<long> lstComplianceIDs = new List<long>();

				if (compType == "REG")
				{
					lstComplianceIDs = (from row in entities.RLCS_Register_Compliance_Mapping
										where row.AVACOM_ComplianceID != null
										select (long)row.AVACOM_ComplianceID).ToList();
				}
				else if (compType == "RET")
				{
					lstComplianceIDs = (from row in entities.RLCS_Returns_Compliance_Mapping
										where row.AVACOM_ComplianceID != null
										select (long)row.AVACOM_ComplianceID).ToList();
				}
				else if (compType == "CHA")
				{

					ESICompType = string.IsNullOrEmpty(ESICompType) ? "" : ESICompType;
					if (ESICompType != "")
					{
						lstComplianceIDs = (from row in entities.RLCS_Challan_Compliance_Mapping
											where row.AVACOM_ComplianceID != null && row.ChallanType == ESICompType
											select (long)row.AVACOM_ComplianceID).ToList();
					}
					else
					{
						lstComplianceIDs = (from row in entities.RLCS_Challan_Compliance_Mapping
											where row.AVACOM_ComplianceID != null
											select (long)row.AVACOM_ComplianceID).ToList();
					}
				}

				if (lstComplianceIDs.Count > 0)
					MastertransactionsQuery = (from row in MastertransactionsQuery
											   where lstComplianceIDs.Contains(row.ComplianceID)
											   select row).ToList();

				return MastertransactionsQuery;
			}
		}

		protected void gridCompliances_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			try
			{
				if (e.Row.RowType == DataControlRowType.DataRow)
				{
					HyperLink hlShowMyWorkspace = (HyperLink)e.Row.FindControl("hlShowMyWorkspace");
					LinkButton lnkbtn_GotoIOM = (LinkButton)e.Row.FindControl("lnkbtn_GotoIOM");

					Label lblCanChangeStatus = (Label)e.Row.FindControl("lblCanChangeStatus");

					if (lblCanChangeStatus != null)
					{
						if (!string.IsNullOrEmpty(lblCanChangeStatus.Text) && (lblCanChangeStatus.Text == "0" || lblCanChangeStatus.Text == "1"))
						{
							if (Convert.ToBoolean(Convert.ToInt32(lblCanChangeStatus.Text)))
							{
								if (hlShowMyWorkspace != null)
								{
									if (!string.IsNullOrEmpty(ddlFType.SelectedValue) && ddlFType.SelectedValue != "-1")
									{
										if (ddlFType.SelectedValue == "PR")
											hlShowMyWorkspace.Visible = true;
										if (ddlFType.SelectedValue == "PA")
											hlShowMyWorkspace.Visible = true;
									}
								}

								if (lnkbtn_GotoIOM != null)
								{

									if (!string.IsNullOrEmpty(ddlFType.SelectedValue) && ddlFType.SelectedValue != "-1")
									{
										if (ddlFType.SelectedValue != "PR")
											lnkbtn_GotoIOM.Visible = true;
										if (Convert.ToBoolean(Convert.ToInt32(lblCanChangeStatus.Text)) == true  && ddlFType.SelectedValue == "PA")
										{
											lnkbtn_GotoIOM.Visible = false;
										}

									}

								}
							}
						}
					}
				}
			}
			catch (Exception ex)
			{
				LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
			}
		}

		protected void gridCompliances_RowCommand(object sender, GridViewCommandEventArgs e)
		{
			try
			{
				bool fwdSuccess = false;
				if (e.CommandArgument != null)
				{
					if (e.CommandName.Equals("GOTO_IO"))
					{
						string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { '|' });

						if (commandArgs.Length > 0)
						{
							string complianceType = string.Empty;
							int returnRegisterChallanID = 0;

							string[] ScheduleOnIdArray = commandArgs[0].ToString().Split(',');
							List<long> LstScheduleOnIds = new List<long>();
							foreach (var item in ScheduleOnIdArray)
							{
								LstScheduleOnIds.Add(Convert.ToInt64(item));
							}

							long scheduleOnID = LstScheduleOnIds[0];
							int customerID = Convert.ToInt32(commandArgs[1]);
							int branchID = Convert.ToInt32(commandArgs[2]);
							int actID = Convert.ToInt32(commandArgs[3]);
							int complianceID = Convert.ToInt32(commandArgs[4]);
							string month = commandArgs[5];
							string year = commandArgs[6];

							if (customerID != 0)
							{
								//string[] userDetails = HttpContext.Current.User.Identity.Name.Split(';');

								//if (userDetails.Length == 9)
								//{
								//    //profileID = userDetails[7];
								//    FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8}",
								//      userDetails[0], userDetails[1], userDetails[2], userDetails[3], userDetails[4], customerID, userDetails[6], userDetails[7], userDetails[8]), false);                                    
								//}

								var masterRecord = RLCS_ComplianceManagement.GetRegisterReturnIDComType(customerID, branchID, actID, complianceID);

								if (masterRecord != null)
								{
									string masterID = masterRecord.MasterID;
									if (!string.IsNullOrEmpty(masterID))
									{
										string[] comTypeRegRetID = masterID.Split(new char[] { '-' });

										if (comTypeRegRetID.Length > 0)
										{
											complianceType = comTypeRegRetID[0];
											returnRegisterChallanID = Convert.ToInt32(comTypeRegRetID[1]);

											if (!string.IsNullOrEmpty(complianceType) && returnRegisterChallanID != 0)
											{
												if (complianceType.Equals("REG"))
													complianceType = "Register";
												else if (complianceType.Equals("RET"))
													complianceType = "Return";
												else
													complianceType = "Challan";

												fwdSuccess = true;

												if (masterRecord.RLCS_Frequency != null)
													if (masterRecord.RLCS_Frequency.Equals("BiAnnual") || masterRecord.RLCS_Frequency.Equals("Biennial"))
														month = "01,02,03,04,05,06,07,08,09,10,11,12";

												ProductMappingStructure _obj = new ProductMappingStructure();

												//customerID --- User's CustomerID not Selected CustomerID
												_obj.ReAuthenticate_User(customerID);

												ScriptManager.RegisterStartupScript(this, Page.GetType(), "CallMyFunction", "CallParentMethod('" + complianceType + "','" + returnRegisterChallanID + "','" + actID + "','" + complianceID + "','" + branchID + "','" + month + "','" + year + "');", true);

												//Response.Redirect("~/RLCS/ComplianceInputOutput.aspx?ComplianceType=" + complianceType
												//                                                + "&ReturnRegisterChallanID=" + returnRegisterChallanID
												//                                                + "&ActID=" + actID
												//                                                + "&ComplianceID=" + complianceID
												//                                                + "&BranchID=" + branchID
												//                                                + "&Month=" + month
												//                                                + "&Year=" + year, false);
											}
										}
									}
								}
							}
						}
					}
				}

				if (!fwdSuccess)
				{
					cvCompliances.IsValid = false;
					cvCompliances.ErrorMessage = "Something went wrong, Please try again";
				}
			}
			catch (Exception ex)
			{
				LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
			}
		}

		protected void lnkClearAdvanceSearch_Click(object sender, EventArgs e)
		{
			try
			{
				ddlAct.ClearSelection();

				lblAdvanceSearchScrum.Text = String.Empty;
				divAdvSearch.Visible = false;

				var nvc = HttpUtility.ParseQueryString(Request.Url.Query);
				nvc.Remove("customerid");
				nvc.Remove("Category");
				nvc.Remove("ActID");
				nvc.Remove("Branchid");
				nvc.Remove("UserID");
				nvc.Remove("Internalsatutory");
				string url = Request.Url.AbsolutePath + "?" + nvc.ToString();
				Response.Redirect("~/RLCS/RLCS_MyDueUpcomingCompliance.aspx" + "?" + nvc.ToString(), false);
				Context.ApplicationInstance.CompleteRequest();

				//Rebind Grid
				//BindGrid();
			}
			catch (Exception ex)
			{
				LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
			}
		}

		protected void btnSearch_Click(object sender, EventArgs e)
		{
			try
			{
				SelectedPageNo.Text = "1";
				gridCompliances.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
				lblAdvanceSearchScrum.Text = String.Empty;

				BindGrid();
				ScriptManager.RegisterStartupScript(this, this.GetType(), "SetDate", "  bindFromToMonthPicker();", true);
			}
			catch (Exception ex)
			{
				LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
			}
		}

		protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				SelectedPageNo.Text = "1";
				int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);
				if (currentPageNo < GetTotalPagesCount())
				{
					SelectedPageNo.Text = (currentPageNo).ToString();
				}
				gridCompliances.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
				gridCompliances.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
				//Reload the Grid
				BindGrid();
				GetPageDisplaySummary();
			}
			catch (Exception ex)
			{
				LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
			}
		}

		private void GetPageDisplaySummary()
		{
			try
			{
				DivRecordsScrum.Visible = true;
				if (Convert.ToString(Session["TotalRows"]) != null && Convert.ToString(Session["TotalRows"]) != "")
				{
					lblTotalRecord.Text = " " + Session["TotalRows"].ToString();
					lTotalCount.Text = GetTotalPagesCount().ToString();

					if (lTotalCount.Text != "0")
					{
						if (SelectedPageNo.Text == "" || SelectedPageNo.Text == "0" || SelectedPageNo.Text == "1")
						{
							SelectedPageNo.Text = "1";
							lblStartRecord.Text = "1";

							if (!(Convert.ToInt32(ddlPageSize.SelectedValue) > Convert.ToInt32(Session["TotalRows"].ToString())))
								lblEndRecord.Text = ddlPageSize.SelectedValue + " ";
							else
								lblEndRecord.Text = Session["TotalRows"].ToString() + " ";
						}
					}
					else if (lTotalCount.Text == "0")
					{
						SelectedPageNo.Text = "0";
						DivRecordsScrum.Visible = false;
					}
				}
			}
			catch (Exception ex)
			{
				LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
				throw ex;
			}
		}

		private int GetTotalPagesCount()
		{
			try
			{
				TotalRows.Value = Session["TotalRows"].ToString();

				int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

				// total page item to be displyed
				int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

				// remaing no of pages
				if (pageItemRemain > 0)// set total No of pages
				{
					totalPages = totalPages + 1;
				}
				else
				{
					totalPages = totalPages + 0;
				}
				return totalPages;
			}
			catch (Exception ex)
			{
				LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
				return 0;
			}
		}

		protected void Next_Click(object sender, EventArgs e)
		{
			try
			{
				int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);
				int StartRecord = Convert.ToInt32(lblStartRecord.Text);
				int EndRecord = 0;
				if (currentPageNo < GetTotalPagesCount())
				{
					SelectedPageNo.Text = (currentPageNo + 1).ToString();
				}
				if (!(StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) > Convert.ToInt32(Session["TotalRows"])))
					StartRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue);

				EndRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) - 1;

				if (EndRecord > Convert.ToInt32(Session["TotalRows"]))
					EndRecord = Convert.ToInt32(Session["TotalRows"]);

				lblStartRecord.Text = StartRecord.ToString();
				lblEndRecord.Text = EndRecord.ToString() + " ";
				gridCompliances.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
				gridCompliances.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
				//Reload the Grid
				BindGrid();
			}
			catch (Exception ex)
			{
				LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
			}
		}

		protected void Previous_Click(object sender, EventArgs e)
		{
			try
			{
				int StartRecord = Convert.ToInt32(lblStartRecord.Text);
				int EndRecord = 0;

				if (Convert.ToInt32(SelectedPageNo.Text) > 1)
				{
					SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
				}

				StartRecord = StartRecord - Convert.ToInt32(ddlPageSize.SelectedValue);

				if (StartRecord < 1)
					StartRecord = 1;

				EndRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) - 1;

				if (EndRecord > Convert.ToInt32(Session["TotalRows"]))
					EndRecord = Convert.ToInt32(Session["TotalRows"]);

				lblStartRecord.Text = StartRecord.ToString();
				lblEndRecord.Text = EndRecord.ToString() + " ";
				gridCompliances.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
				gridCompliances.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
				//Reload the Grid
				BindGrid();
			}
			catch (Exception ex)
			{
				LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
			}
		}

		protected void lbtnExportExcel_Click(object sender, EventArgs e)
		{
			try
			{
				using (ExcelPackage exportPackge = new ExcelPackage())
				{
					int customerID = -1;
					int serviceProviderID = -1;
					int distributorID = -1;

					if (AuthenticationHelper.Role == "CADMN")
					{
						customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
					}
					else if (AuthenticationHelper.Role == "SPADM")
					{
						serviceProviderID = Convert.ToInt32(AuthenticationHelper.CustomerID);
					}
					else if (AuthenticationHelper.Role == "DADMN")
					{
						distributorID = Convert.ToInt32(AuthenticationHelper.CustomerID);
					}
					else
					{
						customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
					}

					string roleCode = string.Empty;
					roleCode = AuthenticationHelper.Role;


					string selectedMonth = string.Empty;
					string selectedYear = string.Empty;

					string filterType, compType = string.Empty;

					filterType = ddlFType.SelectedValue;
					compType = ddlComType.SelectedValue;

					using (ComplianceDBEntities entities = new ComplianceDBEntities())
					{
						entities.Database.CommandTimeout = 180;
						//var lstMyAssignedCustomers = UserCustomerMappingManagement.Get_UserCustomerMapping(AuthenticationHelper.UserID);
						var lstMyAssignedCustomers = UserCustomerMappingManagement.Get_AssignedCustomers(AuthenticationHelper.UserID, distributorID, AuthenticationHelper.Role);

						var masterRecords = (entities.SP_RLCS_ComplianceInstanceTransactionCount_Dashboard(AuthenticationHelper.UserID, AuthenticationHelper.ProfileID, customerID, distributorID, serviceProviderID, roleCode)
										   .Where(entry => entry.IsActive == true
											   && entry.IsUpcomingNotDeleted == true
											   && lstMyAssignedCustomers.Select(row => row.CustomerID).Contains(entry.CustomerID))).ToList();

						if (masterRecords.Count > 0)
						{
							masterRecords = GetFilteredData(masterRecords);
						}

						if (masterRecords.Count > 0)
						{
							ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add(filterType);
							DataTable ExcelData = null;
							DataView view = new System.Data.DataView(masterRecords.ToDataTable());
							ExcelData = view.ToTable("Selected", false, "CustomerBranchName", "ShortForm", "RequiredForms", "Frequency", "ScheduledOn");//

							exWorkSheet.Cells["A2"].Style.Font.Bold = true;
							exWorkSheet.Cells["A2"].Value = "Report Name:";
							exWorkSheet.Cells["B2:C2"].Merge = true;
							exWorkSheet.Cells["B2"].Value = "Report of " + filterType + " Compliances";

							exWorkSheet.Cells["A3"].Style.Font.Bold = true;
							exWorkSheet.Cells["A3"].Value = "Report Generated On:";
							exWorkSheet.Cells["B3"].Value = DateTime.Today.Date.ToString("dd/MMM/yyyy");

							exWorkSheet.Cells["A6"].LoadFromDataTable(ExcelData, false);

							exWorkSheet.Cells["A5"].Value = "Entity / Branch";
							exWorkSheet.Cells["B5"].Value = "Compliance";
							exWorkSheet.Cells["C5"].Value = "Form";
							exWorkSheet.Cells["D5"].Value = "Frequency";
							exWorkSheet.Cells["E5"].Value = "Due Date";


							//exWorkSheet.Cells["A1"].Style.Font.Bold = true;
							//exWorkSheet.Cells["A1"].Value = "Entity/ Location:";

							//exWorkSheet.Cells["B1:C1"].Merge = true;
							//exWorkSheet.Cells["B1"].Value = LocationName;

							exWorkSheet.Cells["A2"].Style.Font.Bold = true;
							exWorkSheet.Cells["A2"].Value = "Report Name:";
							exWorkSheet.Cells["B2:C2"].Merge = true;


							exWorkSheet.Cells["B2"].Value = "Report of Compliances";
							exWorkSheet.Cells["A3"].Style.Font.Bold = true;
							exWorkSheet.Cells["A3"].Value = "Report Generated On:";
							exWorkSheet.Cells["B3"].Value = DateTime.Today.Date.ToString("dd/MMM/yyyy");

							exWorkSheet.Cells["A5"].LoadFromDataTable(ExcelData, true);

							exWorkSheet.Cells["A5"].Style.Font.Bold = true;
							exWorkSheet.Cells["A5"].Style.Font.Size = 12;
							exWorkSheet.Cells["A5"].Value = "Entity / Branch";
							exWorkSheet.Cells["A5"].AutoFitColumns(20);

							exWorkSheet.Cells["B5"].Style.Font.Bold = true;
							exWorkSheet.Cells["B5"].Style.Font.Size = 12;
							exWorkSheet.Cells["B5"].Value = "Compliance";
							exWorkSheet.Cells["B5"].AutoFitColumns(60);

							exWorkSheet.Cells["C5"].Style.Font.Bold = true;
							exWorkSheet.Cells["C5"].Style.Font.Size = 12;
							exWorkSheet.Cells["C5"].Value = "Form";
							exWorkSheet.Cells["C5"].AutoFitColumns(15);

							exWorkSheet.Cells["D5"].Style.Font.Bold = true;
							exWorkSheet.Cells["D5"].Style.Font.Size = 12;
							exWorkSheet.Cells["D5"].Value = "Frequency";
							exWorkSheet.Cells["D5"].AutoFitColumns(10);

							exWorkSheet.Cells["E5"].Style.Font.Bold = true;
							exWorkSheet.Cells["E5"].Style.Font.Size = 12;
							exWorkSheet.Cells["E5"].Value = "Due Date";
							exWorkSheet.Cells["E5"].AutoFitColumns(12);

							using (ExcelRange col = exWorkSheet.Cells[5, 1, 5 + ExcelData.Rows.Count, 5])
							{
								col.Style.WrapText = true;
								col.Style.Numberformat.Format = "dd/MMM/yyyy";
								col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
								col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
								//col.AutoFitColumns();

								// Assign borders
								col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
								col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
								col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
								col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
							}

							string filename = "";
							if (filterType == "U")
								filename = "Upcomming_Compliance_Report.xlsx";
							else if (filterType == "O")
								filename = "Overdue_Compliance_Report.xlsx";
							else if (filterType == "PR")
								filename = "Pending_For_Review_Report.xlsx";


							Byte[] fileBytes = exportPackge.GetAsByteArray();
							Response.ClearContent();
							Response.Buffer = true;
							Response.AddHeader("content-disposition", "attachment;filename=" + filename);
							Response.Charset = "";
							Response.ContentType = "application/vnd.ms-excel";
							StringWriter sw = new StringWriter();
							Response.BinaryWrite(fileBytes);
							HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
							HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
							HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.                    
						}

					}
				}
			}
			catch (Exception ex)
			{
				LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
			}
		}

		public static List<SP_RLCS_ComplianceInstanceTransactionCount_Dashboard_Result> DashboardData_ComplianceDisplayCount(List<SP_RLCS_ComplianceInstanceTransactionCount_Dashboard_Result> MastertransactionsQuery, string filter)
		{
			//List<SP_RLCS_ComplianceInstanceTransactionCount_Dashboard_Result> transactionsQuery = new List<SP_RLCS_ComplianceInstanceTransactionCount_Dashboard_Result>();

			if (!string.IsNullOrEmpty(filter))
			{
				DateTime now = DateTime.Now.Date;
				DateTime nextOneMonth = DateTime.Now.AddMonths(1);
				DateTime nextTenDays = DateTime.Now.AddDays(10);

				switch (filter)
				{
					case "Upcoming":
						MastertransactionsQuery = (from row in MastertransactionsQuery
												   where (row.PerformerScheduledOn > now && row.PerformerScheduledOn <= nextOneMonth)
												   && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 13)
												   select row).ToList().GroupBy(entity => new { entity.ComplianceSchedules, entity.ScheduledOnID }).Select(entity => entity.FirstOrDefault()).ToList();
						break;

					case "Overdue":
						MastertransactionsQuery = (from row in MastertransactionsQuery
												   where (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 6 || row.ComplianceStatusID == 8 || row.ComplianceStatusID == 10)
													&& row.PerformerScheduledOn < now
												   select row).ToList().GroupBy(entity => new { entity.ComplianceSchedules, entity.ScheduledOnID }).Select(entity => entity.FirstOrDefault()).ToList();
						break;
					case "PendingForReview":
						MastertransactionsQuery = (from row in MastertransactionsQuery
												   where (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 12)
												   select row).ToList().GroupBy(entity => new { entity.ComplianceSchedules, entity.ScheduledOnID }).Select(entity => entity.FirstOrDefault()).ToList();

						break;
					case "PendingForAction":
						MastertransactionsQuery = (from row in MastertransactionsQuery
												   where (row.ComplianceStatusID == 20)
												   select row).ToList().GroupBy(entity => new { entity.ComplianceSchedules, entity.ScheduledOnID }).Select(entity => entity.FirstOrDefault()).ToList();

						break;



					case "DueToday":
						MastertransactionsQuery = (from row in MastertransactionsQuery
												   where (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 13 ||
												   row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3)
												   && row.PerformerScheduledOn == now
												   select row).ToList().GroupBy(entity => new { entity.ComplianceSchedules, entity.ScheduledOnID }).Select(entity => entity.FirstOrDefault()).ToList();
						break;

					case "NotCompleted":
						MastertransactionsQuery = (from row in MastertransactionsQuery
												   where (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3
													   || row.ComplianceStatusID == 6 || row.ComplianceStatusID == 8 || row.ComplianceStatusID == 10)
												   select row).ToList().GroupBy(entity => new { entity.ComplianceSchedules, entity.ScheduledOnID }).Select(entity => entity.FirstOrDefault()).ToList();
						break;

					case "ClosedTimely":
						MastertransactionsQuery = (from row in MastertransactionsQuery
												   where (row.ComplianceStatusID == 4 || row.ComplianceStatusID == 7)
												   select row).ToList().GroupBy(entity => new { entity.ComplianceSchedules, entity.ScheduledOnID }).Select(entity => entity.FirstOrDefault()).ToList();
						break;

					case "ClosedDelayed":
						MastertransactionsQuery = (from row in MastertransactionsQuery
												   where (row.ComplianceStatusID == 5 || row.ComplianceStatusID == 9)
												   select row).ToList().GroupBy(entity => new { entity.ComplianceSchedules, entity.ScheduledOnID }).Select(entity => entity.FirstOrDefault()).ToList();
						break;

					case "All":
						MastertransactionsQuery = (from row in MastertransactionsQuery
												   select row).ToList().GroupBy(entity => new { entity.ComplianceSchedules, entity.ScheduledOnID }).Select(entity => entity.FirstOrDefault()).ToList();
						break;
				}
			}

			return MastertransactionsQuery.ToList();
		}

		protected void BindYears(List<SP_RLCS_ComplianceInstanceTransactionCount_Dashboard_Result> MastertransactionsQueryList)
		{
			try
			{
				List<YearCalander> _objlst = new List<YearCalander>();
				int currentYear = Convert.ToInt32(DateTime.Now.Year);
				for (int i = currentYear; i >= currentYear - 4; i--)
				{
					YearCalander obj = new YearCalander();
					obj.Text = i.ToString();
					obj.Value = i.ToString();
					_objlst.Add(obj);
				}
				if (_objlst != null && _objlst.Count > 0)
				{
					ddlYear.DataTextField = "Text";
					ddlYear.DataValueField = "Value";
					ddlYear.DataSource = _objlst;
					ddlYear.DataBind();
					ddlYear.Items.Insert(0, new ListItem("Year", "-1"));
					ddlYear.SelectedIndex = -1;
				}

				//var lstYears = MastertransactionsQueryList.Select(row => row.RLCS_PayrollYear).Distinct().OrderByDescending(row => row).ToList();
				//List<YearCalander> _objlst = new List<YearCalander>();
				//foreach (var year in lstYears)
				//{
				//    if(year != null)
				//    {
				//        YearCalander obj = new YearCalander();
				//        obj.Text = year.ToString();
				//        obj.Value = year.ToString();
				//        _objlst.Add(obj);
				//    }
				//}
				//if (_objlst != null && _objlst.Count > 0)
				//{
				//    ddlYear.DataTextField = "Text";
				//    ddlYear.DataValueField = "Value";
				//    ddlYear.DataSource = _objlst;
				//    ddlYear.DataBind();
				//    ddlYear.Items.Insert(0, new ListItem("Year", "-1"));
				//    ddlYear.SelectedIndex = -1;
				//}

				//ddlYear.Items.Clear();

				//ddlYear.DataSource = lstYears;

				//ddlYear.Items.Insert(0, new ListItem("Year", "-1"));
				//ddlYear.DataBind();

			}
			catch (Exception ex)
			{
				LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
			}
		}
		public class YearCalander
		{
			public string Text { get; set; }
			public string Value { get; set; }
		}

		public List<string> GetSelectedPeriod()
		{
			List<string> lstPeriod = new List<string>();
			try
			{
				foreach (RepeaterItem aItem in rptPeriodList.Items)
				{
					try
					{
						CheckBox chkPeriod = (CheckBox)aItem.FindControl("chkPeriod");
						if (chkPeriod != null && chkPeriod.Checked)
						{
							Label lblPeriodName = (Label)aItem.FindControl("lblPeriodName");
							if (lblPeriodName != null && !string.IsNullOrEmpty(lblPeriodName.Text))
							{
								if (!string.IsNullOrEmpty(lblPeriodName.Text.Trim()))
								{
									Label lblPeriodID = (Label)aItem.FindControl("lblPeriodID");
									if (lblPeriodID != null && !string.IsNullOrEmpty(lblPeriodID.Text))
									{
										if (ddlFrequency.SelectedValue == "Monthly")
											lstPeriod.Add(lblPeriodID.Text);
										else if (ddlFrequency.SelectedValue == "Quarterly")
										{
											if (lblPeriodID.Text == "Q1")
											{
												lstPeriod.Add("03");//CY
												lstPeriod.Add("06");//FY
											}
											else if (lblPeriodID.Text == "Q2")
											{
												lstPeriod.Add("06");//CY
												lstPeriod.Add("09");//FY
											}
											else if (lblPeriodID.Text == "Q3")
											{
												lstPeriod.Add("09");//CY
												lstPeriod.Add("12");//FY
											}
											else if (lblPeriodID.Text == "Q4")
											{
												lstPeriod.Add("12");//CY
												lstPeriod.Add("03");//FY
											}
										}
										else if (ddlFrequency.SelectedValue == "HalfYearly")
										{
											if (lblPeriodID.Text == "HY1")
											{
												lstPeriod.Add("06");//CY
												lstPeriod.Add("09");//FY
											}
											else if (lblPeriodID.Text == "HY2")
											{
												lstPeriod.Add("12");//CY
												lstPeriod.Add("03");//FY
											}
										}
										else if (ddlFrequency.SelectedValue == "Annual")
										{
											lstPeriod.Add("12");//CY
											lstPeriod.Add("03");//FY
										}
									}
								}
							}
						}
					}
					catch (Exception)
					{

					}
				}

				return lstPeriod;
			}
			catch (Exception ex)
			{
				return lstPeriod;
			}
		}

		public bool SetSelectedPeriod(string strPeriods)
		{
			List<string> lstPeriod = strPeriods.Split(',').ToList();
			try
			{
				foreach (RepeaterItem aItem in rptPeriodList.Items)
				{
					try
					{
						CheckBox chkPeriod = (CheckBox)aItem.FindControl("chkPeriod");
						if (chkPeriod != null)
						{
							Label lblPeriodID = (Label)aItem.FindControl("lblPeriodID");
							if (lblPeriodID != null && !string.IsNullOrEmpty(lblPeriodID.Text))
							{
								if (ddlFrequency.SelectedValue == "Monthly")
								{
									if (lstPeriod.Contains(lblPeriodID.Text))
										chkPeriod.Checked = true;
								}
								else if (ddlFrequency.SelectedValue == "Quarterly")
								{
									if (lblPeriodID.Text == "Q1")
									{
										if (lstPeriod.Contains("03") || lstPeriod.Contains("06"))
											chkPeriod.Checked = true;
									}
									else if (lblPeriodID.Text == "Q2")
									{
										if (lstPeriod.Contains("06") || lstPeriod.Contains("09"))
											chkPeriod.Checked = true;
									}
									else if (lblPeriodID.Text == "Q3")
									{
										if (lstPeriod.Contains("09") || lstPeriod.Contains("12"))
											chkPeriod.Checked = true;
									}
									else if (lblPeriodID.Text == "Q4")
									{
										if (lstPeriod.Contains("12") || lstPeriod.Contains("03"))
											chkPeriod.Checked = true;
									}
								}
								else if (ddlFrequency.SelectedValue == "HalfYearly")
								{
									if (lblPeriodID.Text == "HY1")
									{
										if (lstPeriod.Contains("06") || lstPeriod.Contains("09"))
											chkPeriod.Checked = true;
									}
									else if (lblPeriodID.Text == "HY2")
									{
										if (lstPeriod.Contains("12") || lstPeriod.Contains("03"))
											chkPeriod.Checked = true;
									}
								}
								else if (ddlFrequency.SelectedValue == "Annual")
								{
									if (lstPeriod.Contains("12") || lstPeriod.Contains("03"))
										chkPeriod.Checked = true;
								}
							}
						}
					}
					catch (Exception)
					{

					}
				}

				return true;
			}
			catch (Exception ex)
			{
				return false;
			}
		}

		public void ClearSelectedPeriod()
		{
			try
			{
				foreach (RepeaterItem aItem in rptPeriodList.Items)
				{
					try
					{
						CheckBox chkPeriod = (CheckBox)aItem.FindControl("chkPeriod");
						if (chkPeriod != null && chkPeriod.Checked)
						{
							chkPeriod.Checked = false;
						}
					}
					catch (Exception)
					{

					}
				}
			}
			catch (Exception ex)
			{
				LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
			}
		}

		private void BindPeriod(string selectedFreq)
		{
			try
			{
				if (!string.IsNullOrEmpty(selectedFreq))
				{
					List<Tuple<string, string, string>> tupleList = new List<Tuple<string, string, string>>();

					tupleList.Add(new Tuple<string, string, string>("Annual", "Annual", "Annual"));
					//tupleList.Add(new Tuple<string, string, string>("Biennial", "Biennial", "Biennial"));
					tupleList.Add(new Tuple<string, string, string>("BiAnnual", "BiAnnual", "BiAnnual"));

					tupleList.Add(new Tuple<string, string, string>("Half Yearly", "First Half", "HY1"));
					tupleList.Add(new Tuple<string, string, string>("Half Yearly", "Second Half", "HY2"));

					tupleList.Add(new Tuple<string, string, string>("Quarterly", "Q1", "Q1"));
					tupleList.Add(new Tuple<string, string, string>("Quarterly", "Q2", "Q2"));
					tupleList.Add(new Tuple<string, string, string>("Quarterly", "Q3", "Q3"));
					tupleList.Add(new Tuple<string, string, string>("Quarterly", "Q4", "Q4"));

					tupleList.Add(new Tuple<string, string, string>("Monthly", "January", "01"));
					tupleList.Add(new Tuple<string, string, string>("Monthly", "February", "02"));
					tupleList.Add(new Tuple<string, string, string>("Monthly", "March", "03"));
					tupleList.Add(new Tuple<string, string, string>("Monthly", "April", "04"));

					tupleList.Add(new Tuple<string, string, string>("Monthly", "May", "05"));
					tupleList.Add(new Tuple<string, string, string>("Monthly", "June", "06"));
					tupleList.Add(new Tuple<string, string, string>("Monthly", "July", "07"));
					tupleList.Add(new Tuple<string, string, string>("Monthly", "August", "08"));

					tupleList.Add(new Tuple<string, string, string>("Monthly", "September", "09"));
					tupleList.Add(new Tuple<string, string, string>("Monthly", "Octomber", "10"));
					tupleList.Add(new Tuple<string, string, string>("Monthly", "November", "11"));
					tupleList.Add(new Tuple<string, string, string>("Monthly", "December", "12"));

					DataTable dtReturnPeriods = new DataTable();
					dtReturnPeriods.Columns.Add("ID");
					dtReturnPeriods.Columns.Add("Name");

					var lst = tupleList.FindAll(m => m.Item1 == selectedFreq);
					if (lst.Count > 0)
					{
						foreach (var item in lst)
						{
							DataRow drReturnPeriods = dtReturnPeriods.NewRow();
							drReturnPeriods["ID"] = item.Item3;
							drReturnPeriods["Name"] = item.Item2;
							dtReturnPeriods.Rows.Add(drReturnPeriods);
						}

						rptPeriodList.DataSource = dtReturnPeriods;
						rptPeriodList.DataBind();
						txtPeriodList.Enabled = true;
					}
				}
				else
				{
					txtPeriodList.Enabled = false;
				}
			}
			catch (Exception ex)
			{
				LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
			}
		}

		protected void ddlFrequency_SelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				if (!string.IsNullOrEmpty(ddlFrequency.SelectedValue))
				{
					BindPeriod(ddlFrequency.SelectedValue);
					ddlYear.ClearSelection();
				}
			}
			catch (Exception ex)
			{
				LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
			}
		}

		protected void upFreqFilter_Load(object sender, EventArgs e)
		{
			try
			{
				ScriptManager.RegisterStartupScript(this, this.GetType(), "initializeJQueryUIDeptDDL", "initializeJQueryUIDeptDDL();", true);
			}
			catch (Exception ex)
			{
				LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
			}
		}

		protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				BindLocationFilter();
				BindSPOCs();
			}
			catch (Exception ex)
			{
				LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
			}
		}

		protected void btnClear_Click(object sender, EventArgs e)
		{
			ddlCustomers.ClearSelection();
			ClearTreeViewSelection(tvFilterLocation);
			TreeViewSelectedNode(tvFilterLocation, "-1");
			tvFilterLocation_SelectedNodeChanged(sender, e);
			ddlSPOCs.ClearSelection();
			ddlComType.ClearSelection();
			ddlRisk.ClearSelection();
			txtStartPeriod.Text = string.Empty;
			txtEndPeriod.Text = string.Empty;
			ddlFrequency.ClearSelection();
			ddlFrequency_SelectedIndexChanged(sender, e);
			ddlYear.ClearSelection();

			ddlFType.ClearSelection();

			//ddlCustomers.SelectedValue = "-1";
			//ddlSPOCs.SelectedValue = "-1";
			//ddlComType.SelectedValue = "-1";
			//// txtStartPeriod.Text = "";
			////  txtEndPeriod.Text = "";
			//ddlFrequency.SelectedValue = "-1";
			//txtPeriodList.Text = "";
			//ddlYear.SelectedValue = "";
			ScriptManager.RegisterStartupScript(this, this.GetType(), "SetDate", "bindFromToMonthPicker();", true);

			BindGrid();
		}

		public void BindLocationFilter()
		{
			try
			{
				using (ComplianceDBEntities entities = new ComplianceDBEntities())
				{
					int customerID = -1;

					if (!string.IsNullOrEmpty(ddlCustomers.SelectedValue))
						customerID = Convert.ToInt32(ddlCustomers.SelectedValue);
					else
						customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

					List<int> assignedbranchIDs = new List<int>();

					entities.Database.CommandTimeout = 180;
					var userAssignedBranchList = (entities.SP_GetManagerAssignedBranch(AuthenticationHelper.UserID)).ToList();

					if (userAssignedBranchList != null)
					{
						if (userAssignedBranchList.Count > 0)
						{
							assignedbranchIDs = userAssignedBranchList.Select(row => row.ID).ToList();
						}

						var bracnhes = CustomerBranchManagement.GetAllHierarchyManagementSatutory(customerID);

						tvFilterLocation.Nodes.Clear();

						TreeNode node = new TreeNode("Entity/Branch", "-1");
						node.Selected = true;
						tvFilterLocation.Nodes.Add(node);

						foreach (var item in bracnhes)
						{
							node = new TreeNode(item.Name, item.ID.ToString());
							node.SelectAction = TreeNodeSelectAction.Expand;
							CustomerBranchManagement.BindBranchesHierarchy(node, item, assignedbranchIDs);
							tvFilterLocation.Nodes.Add(node);
						}

						tvFilterLocation.CollapseAll();
						tvFilterLocation_SelectedNodeChanged(null, null);
					}
				}
			}
			catch (Exception ex)
			{
				LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
			}
		}

		private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp)
		{
			try
			{
				foreach (var item in nvp.Children)
				{
					TreeNode node = new TreeNode(item.Name, item.ID.ToString());
					BindBranchesHierarchy(node, item);
					parent.ChildNodes.Add(node);
				}
			}
			catch (Exception ex)
			{
				LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
			}
		}

		protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
		{
			try
			{
				tbxFilterLocation.Text = tvFilterLocation.SelectedNode != null ? tvFilterLocation.SelectedNode.Text : "Entity/Branch";
			}
			catch (Exception ex)
			{
				LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
			}
		}

		public static void ClearTreeViewSelection(TreeView tree)
		{
			if (tree.SelectedNode != null)
			{
				tree.SelectedNode.Selected = false;
			}
		}

		protected void TreeViewSelectedNode(TreeView tree, string value)
		{
			foreach (TreeNode node in tree.Nodes)
			{
				if (node.Value == value)
				{
					node.Selected = true;
				}
				else if (node.ChildNodes.Count > 0)
				{
					foreach (TreeNode child in node.ChildNodes)
					{
						if (child.Value == value)
						{
							child.Selected = true;
						}
					}
				}
			}
		}

		protected void upCustomers_Load(object sender, EventArgs e)
		{
			try
			{
				ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
				ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
			}
			catch (Exception ex)
			{
				LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
			}
		}
		protected void ddlComType_SelectedIndexChanged(object sender, EventArgs e)
		{

			if (ddlComType.SelectedValue == "CHA")
			{
				divchallantype.Visible = true;
			}
			else
			{
				divchallantype.Visible = false;
			}

		}
	}
}