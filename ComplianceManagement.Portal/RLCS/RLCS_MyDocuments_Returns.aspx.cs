﻿using System;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using System.Configuration;
using static com.VirtuosoITech.ComplianceManagement.Business.RLCS.RLCSAPIClasses;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class RLCS_MyDocuments_Returns : System.Web.UI.Page
    {
        protected int CustId;
        protected int UserId;
        protected static string Path;

        protected static int newReturnsMonth;
        protected static int newReturnsYear;
        protected static string ProfileID;
        protected void Page_Load(object sender, EventArgs e)
        {
            CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            UserId = Convert.ToInt32(AuthenticationHelper.UserID);
            Path = ConfigurationManager.AppSettings["AVACOM_RLCS_API_URL"];

            newReturnsMonth = Convert.ToInt32(ConfigurationManager.AppSettings["RLCS_Return_Month"]);
            newReturnsYear = Convert.ToInt32(ConfigurationManager.AppSettings["RLCS_Return_Year"]);
            ProfileID = Convert.ToString(AuthenticationHelper.ProfileID);

            if (!string.IsNullOrEmpty(Request.QueryString["State"]) && !string.IsNullOrEmpty(Request.QueryString["Location"]) && !string.IsNullOrEmpty(Request.QueryString["BranchID"]) && !string.IsNullOrEmpty(Request.QueryString["Month"]) && !string.IsNullOrEmpty(Request.QueryString["Year"]))
            {
                string State = Request.QueryString["State"];
                string taskID = Request.QueryString["TaskID"];
                string Month = Request.QueryString["Month"];
                string Year = Request.QueryString["Year"];

                if (State != null && taskID != null && Month != null && Year != null)
                {
                    bool _objDocreturns = GetMyDocuments_ReturnDocuments(State, taskID, Month, Year);
                    if (!_objDocreturns)
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('No Document available to download')", true);
                    }
                }
            }
        }

        private bool GetMyDocuments_ReturnDocuments(string state, string taskID, string month, string year)
        {
            List<long> lstScheduleOnIDs = new List<long>();
            List<long> lstScheduleOnIDsToDownload = new List<long>();
            List<long> lstComplianceIDs = new List<long>();

            var lstReturn_Compliances = RLCSManagement.GetAll_ReturnsRelatedCompliance_SectionWise(state, taskID);

            if (lstReturn_Compliances != null)
            {
                if (lstReturn_Compliances.Count > 0)
                {
                    lstComplianceIDs = lstReturn_Compliances.Select(row => (long)row.AVACOM_ComplianceID).ToList();

                    List<MappedCompliance> lstComplianceToProcess = new List<MappedCompliance>();

                    lstReturn_Compliances.ForEach(eachReturnMappedCompliance =>
                    {
                        //lstComplianceToProcess.Clear();
                        //lstComplianceToProcess.Add(eachReturnMappedCompliance); //Needs to find ActCode of Each Compliance that

                        if (eachReturnMappedCompliance.ActCode.Trim().ToUpper().Equals("EPF") || eachReturnMappedCompliance.ActCode.Trim().ToUpper().Equals("ESI"))
                        {
                            //Map Compliance to Code 
                            //Get All CustomerBranch With Code and Map compliance to it

                            string type = string.Empty;
                            if (eachReturnMappedCompliance.ActCode.Trim().ToUpper().Equals("EPF"))
                                type = "EPF";
                            else if (eachReturnMappedCompliance.ActCode.Trim().ToUpper().Equals("ESI"))
                                type = "ESI";

                            var lstCustomerBranches = RLCSManagement.GetCustomerBranchesByPFOrESICCode(type, state, "B"); //LM_State--CodeValue                                            

                            if (lstCustomerBranches != null)
                            {
                                bool getSchdules = false;

                                if (lstCustomerBranches.Count > 0)
                                {
                                    lstCustomerBranches.ForEach(eachCustomerBranch =>
                                    {
                                        if (eachCustomerBranch != null)
                                        {
                                            if (eachCustomerBranch.AVACOM_BranchID != null)
                                            {
                                                if (!getSchdules)
                                                {
                                                    lstScheduleOnIDs = RLCSManagement.GetAll_Registers_ScheduleOnIDs(lstComplianceIDs, Convert.ToInt32(eachCustomerBranch.AVACOM_BranchID), year, month);

                                                    if (lstScheduleOnIDs.Count > 0)
                                                    {
                                                        lstScheduleOnIDsToDownload.Union(lstScheduleOnIDs);
                                                        getSchdules = true;
                                                    }
                                                }
                                            }
                                        }
                                    });
                                }
                            }
                        }
                        else if (eachReturnMappedCompliance.ActCode.Trim().ToUpper().Equals("PT") || eachReturnMappedCompliance.ActCode.Trim().ToUpper().Equals("LWF"))
                        {
                            //Map Compliance to State                                               
                            var customerBranchDetails = RLCSManagement.GetCustomerBranchByStateCode(CustId, state, "S");

                            if (customerBranchDetails != null)
                            {
                                if (customerBranchDetails.AVACOM_BranchID != null)
                                {
                                    lstScheduleOnIDs = RLCSManagement.GetAll_Registers_ScheduleOnIDs(lstComplianceIDs, Convert.ToInt32(customerBranchDetails.AVACOM_BranchID), year, month);

                                    if (lstScheduleOnIDs.Count > 0)
                                    {
                                        lstScheduleOnIDsToDownload.Union(lstScheduleOnIDs);
                                    }
                                }
                            }
                        }
                    });

                    if (lstScheduleOnIDsToDownload.Count > 0)
                    {
                        return RLCS_DocumentManagement.GetRLCSDocuments("Return", lstScheduleOnIDsToDownload);
                        //return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            else { return true; }
        }
    }
}