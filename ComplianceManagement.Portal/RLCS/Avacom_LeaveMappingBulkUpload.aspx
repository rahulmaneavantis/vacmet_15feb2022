﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Avacom_LeaveMappingBulkUpload.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.Avacom_LeaveMappingBulkUpload" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap theme -->
    <link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />

    <link href="~/NewCSS/bootstrap-datepicker.min.css" rel="stylesheet" />
    <!-- font icon -->
    <link href="~/NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <!--external css-->
    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/contract_custom_style.css" rel="stylesheet" />
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="/Newjs/jquery.js"></script>
    <script type="text/javascript" src="/Newjs/bootstrap.min.js"></script>


    <link href="~/NewCSS/3.3.7/bootstrap.min.css" rel="stylesheet" />
    <style>
        .form-control {
            margin-bottom: 8px;
            height: 31px;
            font-family: sans-serif;
        }

        .m-10 {
            /*margin-left: 10px;*/
            padding-left: 24px;
        }
        .alert-danger {
    color: #a94442;
    background-color: #f2dede;
    border-color: #ebccd1;
}


        ul li {
            list-style: none;
           // color: Red;
            
        }
    </style>

    <script type="text/javascript">
        function addremoveCSS()
        {
            $("#vsUploadUtility").removeClass("alert alert-danger");
            $("#vsUploadUtility").addClass("alert alert-success");
        }
    </script>

</head>
<body style="background: white; width: 700px;">
    <form runat="server" id="formvalidation">
        <%-- must uncomment if no error occurs--%>
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true"></asp:ScriptManager>
        <asp:UpdatePanel ID="updatepnl" runat="server">
            <ContentTemplate>
                <div class="form-group clearfix"></div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12" style="margin-left: 30px;">
                        <asp:ValidationSummary ID="vsUploadUtility" runat="server"
                            class="alert alert-block alert-danger fade in" DisplayMode="BulletList" ValidationGroup="uploadUtilityValidationGroup"  />
                        <asp:CustomValidator ID="cvUploadUtilityPage" runat="server" EnableClientScript="False" CssClass="alert alert-danger"
                            ValidationGroup="uploadUtilityValidationGroup" Display="None" Enabled="true" ShowSummary="true"/>
                    </div>

                </div>


                <div class="form-group clearfix"></div>
                <div class="row" style="margin-top: 10px; margin-bottom: 20px; margin-left: 0px;">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <div class="col-xs-4 col-md-4">
                                <asp:LinkButton ID="btnDownload" runat="server" OnClick="btnDownload_Click"><i class="fa fa-file-excel-o"></i>Sample Document</asp:LinkButton>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row" style="margin-top: 10px; margin-left: 0px;">
                    <div style="display:inline-grid">
                        <asp:FileUpload ID="LeaveMappingFileUpload" runat="server" Style="display: block; font-size: 13px; color: #333; margin-left: 29px;" />
                        <asp:RequiredFieldValidator ID="RequiredFieldForFileUpload" ErrorMessage="Select File to Upload" ControlToValidate="LeaveMappingFileUpload"
                            runat="server" Display="None" ValidationGroup="uploadUtilityValidationGroup" />
                    </div>
                    <div style="display:inline-grid;margin-left: 300px;"> 
                        <asp:Button runat="server" ID="btnUploadExcel" CssClass="btn btn-primary" Text="Upload" OnClick="btnUploadExcel_Click" ValidationGroup="uploadUtilityValidationGroup" />
                        <asp:HiddenField ID="hdFileName" runat="server" />
                        <asp:HiddenField ID="hdClientId" runat="server" />
                    </div>


                </div>
                <div class="form-group clearfix"></div>

                <div class="col-xs-12 col-sm-12 col-md-12 colpadding0" style="margin-top: 10px">
                    <div class="col-md-12">
                        <span id="spanErrors" runat="server" style="color: red">Uploaded File Contains Some Errors. Click to Download Error File 
                                                <asp:Button runat="server" ID="btnDownloadError" CssClass="btn btn-danger" Text="Download" OnClick="btnDownloadError_Click" />
                        </span>
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnUploadExcel" />
                <asp:PostBackTrigger ControlID="btnDownload" />
                <asp:PostBackTrigger ControlID="btnDownloadError" />
            </Triggers>
        </asp:UpdatePanel>
    </form>
</body>
</html>
