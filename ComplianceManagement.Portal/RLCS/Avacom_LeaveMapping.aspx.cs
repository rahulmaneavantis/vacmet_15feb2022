﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OfficeOpenXml;
using System.IO;
using System.Text;
using com.VirtuosoITech.ComplianceManagement.Business.Data;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Avacom_LeaveMapping
{
    public partial class Avacom_LeaveMapping : System.Web.UI.Page
    {

        List<ErrorMessage> lstErrorMessage = new List<ErrorMessage>();
        ErrorMessage objError = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            string custID=Session["CustomerID_new"].ToString();
            if (!IsPostBack)
            {
                GridView1.Visible = false;
                btnProcess.Visible = false;
                UploadBtn.Visible = false;
                ClientScript.RegisterStartupScript(GetType(), "hwa", "hidesampleDownLink();", true);
                LeaveMappingFileUpload.Visible = false;
                var d = ddlClientList.SelectedIndex;
                btnDownloadError.Visible = false;

                //USE Customer_ID to bind Clint DDL
                BindClients(Convert.ToInt32(custID));


            }
        }
        private void BindClients(int CustomerID)
        {
            try
            {
                ddlClientList.DataTextField = "Name";
                ddlClientList.DataValueField = "ID";
                var list = GetAll_Entities(CustomerID);
                ddlClientList.DataSource = list;
                ddlClientList.DataBind();
                ddlClientList.Items.Insert(0, "Select Client");

            }
            catch (Exception ex)
            {

            }
        }

        public static List<object> GetAll_Entities(int CustomerID)
        {
            List<object> EntityList = new List<object>();

            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    EntityList = (
                              from RCRM in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                              join CB in entities.CustomerBranches on RCRM.AVACOM_BranchID equals CB.ID
                              where RCRM.BranchType == "E"
                              && RCRM.CM_Status == "A"
                              && RCRM.IsProcessed == true
                              && RCRM.AVACOM_CustomerID == CustomerID
                              && CB.IsDeleted == false
                              orderby RCRM.CM_ClientName ascending
                              select new
                              {
                                  ID = RCRM.CM_ClientID,
                                  Name = RCRM.CM_ClientName
                              }).Distinct().ToList<object>();

                    return EntityList;
                }
            }
            catch (Exception ex)
            {

            }
            return EntityList;
        }

        public void bindGridview()
        {

            using (ComplianceDBEntities db = new ComplianceDBEntities())

            {
                var result = (from d1 in db.RLCS_LeaveTypeMaster
                              join d2 in db.RLCS_LeaveTypeMaster_Mapping on new { key1 = d1.LeaveType, key2 = ddlClientList.SelectedValue } equals new { key1 = d2.AVACOM_LeaveType, key2 = d2.ClientID } into mappedData
                              from md in mappedData.DefaultIfEmpty()
                              select new { LeaveType = d1.LeaveType, Leave_Description = d1.Leave_Description, ClientCode = md.LeaveType }).Distinct().ToList();


                GridView1.DataSource = result;
                GridView1.DataBind();
            }
        }
        protected void btnProcess_Click(object sender, EventArgs e)
        {
            string str = string.Empty;
            List<RLCS_LeaveTypeMaster_Mapping> _LeaveTypeMaster = new List<RLCS_LeaveTypeMaster_Mapping>();
            foreach (GridViewRow gvrow in GridView1.Rows)
            {
                CheckBox chk = (CheckBox)gvrow.FindControl("chkChild");
                if (chk != null & chk.Checked)
                {
                    var ind = gvrow.RowIndex;
                    GridViewRow row = GridView1.Rows[ind];
                    Label _label = (Label)row.Cells[1].FindControl("_label");
                    TextBox _txtbox = (TextBox)row.Cells[3].FindControl("txtbox");


                    RLCS_LeaveTypeMaster_Mapping c = new RLCS_LeaveTypeMaster_Mapping();

                    c.AVACOM_LeaveType = _label.Text;
                    c.ClientID = ddlClientList.SelectedValue;
                    c.LeaveType = _txtbox.Text;
                    c.IsActive = true;

                     _LeaveTypeMaster.Add(c);
                   

                }
            }
            try
            {
                using (ComplianceDBEntities db = new ComplianceDBEntities())
                {
                    db.RLCS_LeaveTypeMaster_Mapping.AddRange(_LeaveTypeMaster);
                    //db.Client_LeaveMapping.AddRange(_clientLeaveMapping);
                   var count= db.SaveChanges();
                    if (count > 0)
                    {
                        file_Validator.IsValid = false;
                        file_Validator.ErrorMessage = "Save Successfully!";
                    }
                    else
                    {
                        file_Validator.IsValid = false;
                        file_Validator.ErrorMessage = "Please check CheckBoc to Insert Data!";

                    }
                }
            }
            catch (Exception ex)
            {
                file_Validator.IsValid = false;
                file_Validator.ErrorMessage = "Something Went Wrong !";

            }
        }
        protected void ddlClientList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlClientList.SelectedIndex == 0)
            {
                GridView1.Visible = false;
                btnProcess.Visible = false;
                UploadBtn.Visible = false;
                ClientScript.RegisterStartupScript(GetType(), "hwa", "hidesampleDownLink();", true);
                //_sampleFile.Visible = false;
                LeaveMappingFileUpload.Visible = false;


            }
            else if (ddlClientList.SelectedIndex != 0)
            {

                btnProcess.Visible = true;
                UploadBtn.Visible = true;
                ClientScript.RegisterStartupScript(GetType(), "hwa", "showsampleDownLink();", true);
                //   _sampleFile.Visible = true;
                LeaveMappingFileUpload.Visible = true;
                GridView1.Visible = true;
                bindGridview();
                var data = ddlClientList.SelectedItem;
                var id = ddlClientList.SelectedValue;
            }
        }
        protected void UploadBtn_Click(object sender, EventArgs e)
        {
            hdFileName.Value = "";
            if (LeaveMappingFileUpload.HasFile && Path.GetExtension(LeaveMappingFileUpload.FileName).ToLower() == ".xlsx")
            {

                try
                {
                    if (!Directory.Exists(Server.MapPath("~/Attendance_Mapping/Uploaded/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Attendance_Mapping/Uploaded/"));
                    }

                    string filename = Path.GetFileName(LeaveMappingFileUpload.FileName);
                    LeaveMappingFileUpload.SaveAs(Server.MapPath("~/Attendance_Mapping/Uploaded/") + filename.Trim());
                    FileInfo excelfile = new FileInfo(Server.MapPath("~/Attendance_Mapping/Uploaded/") + filename.Trim());

                    if (excelfile != null)
                    {
                        using (ExcelPackage xlWorkbook = new ExcelPackage(excelfile))
                        {
                            bool matchSuccess = checkSheetExist(xlWorkbook, "SampleFIleLeaveMapping");
                            if (matchSuccess)
                            {
                                ProcessChecklistData(xlWorkbook);
                               
                            }
                            else
                            {
                                file_Validator.IsValid = false;
                                file_Validator.ErrorMessage = "No Data Found in Excel Document or Sheet Name must be 'SampleFIleLeaveMapping'.";
                            }
                        }
                    }
                    else
                    {
                        file_Validator.IsValid = false;
                        file_Validator.ErrorMessage = "Error Uploading Excel Document. Please try again.";
                    }
                }
                catch (Exception ex)
                {
                    file_Validator.IsValid = false;
                    file_Validator.ErrorMessage = "Something went wrong, Please try again";
                }
            }
            else
            {
                file_Validator.IsValid = false;
                file_Validator.ErrorMessage = "Please upload excel file (.xlsx)";
            }
        }
        private void ProcessChecklistData(ExcelPackage xlWorkbook)
        {
            try
            {
                

                ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["SampleFIleLeaveMapping"];
                //   List<Client_LeaveMapping> data = new List<Client_LeaveMapping>();
                List<RLCS_LeaveTypeMaster_Mapping> data = new List<RLCS_LeaveTypeMaster_Mapping>();

                if (xlWorksheet != null)
                {
                    int xlrow2 = xlWorksheet.Dimension.End.Row;

                    string ErrorFileName = "ErrorFile_Avacom_LeaveTypeMapping" + DateTime.Now.ToString("ddMMMyyyyhhmmss") + ".csv";
                    hdFileName.Value = ErrorFileName;
                    List<String> clientIDList = new List<string>();

                    for (int i = 2; i <= xlrow2; i++)
                    {

                        string AvacomLeaveType = xlWorksheet.Cells[i, 1].Text.Trim();
                        string ClientCode = xlWorksheet.Cells[i, 2].Text.Trim();


                        bool isValidLeaveType = checkValidLeaveType(AvacomLeaveType);
                        if (!isValidLeaveType)
                        {
                            objError = new ErrorMessage();
                            objError.ErrorDescription = "Invaid Avacom_LeaveType => " + AvacomLeaveType;
                            objError.RowNum = i.ToString();
                            lstErrorMessage.Add(objError);
                        }

                       

                        
                        if (lstErrorMessage.Count == 0)
                        {
                            
                            RLCS_LeaveTypeMaster_Mapping _mappingData = new RLCS_LeaveTypeMaster_Mapping()
                            {
                                AVACOM_LeaveType = AvacomLeaveType,
                                LeaveType = ClientCode,
                                ClientID = ddlClientList.SelectedValue

                            };

                            data.Add(_mappingData);
                        }
                    }
                    if (lstErrorMessage.Count == 0)
                    {
                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        {
                            entities.RLCS_LeaveTypeMaster_Mapping.AddRange(data);
                            entities.SaveChanges();
                            file_Validator.IsValid = false;
                            file_Validator.ErrorMessage = "Upload Successfully!";
                        }

                    }
                    else
                    {

                        GenerateAvacom_LeaveMappingErrorCSV(lstErrorMessage, ErrorFileName);
                        file_Validator.IsValid = false;
                        file_Validator.ErrorMessage = "Uploaded data Contains some error plz Download Error file to check errors.";

                    }

                }
            }
            catch (Exception ex)
            {
                file_Validator.IsValid = false;
                file_Validator.ErrorMessage = "Server Error Occurred. Please try again.";

            }
        }
        public static bool checkSheetExist(ExcelPackage xlWorkbook, string sheetNameUploaded)
        {
            try
            {
                bool matchFlag = false;
                foreach (ExcelWorksheet sheet in xlWorkbook.Workbook.Worksheets)
                {
                    if (sheet.Name.Trim().Equals(sheetNameUploaded))
                    {
                        matchFlag = true;
                    }
                } //End ForEach
                return matchFlag;
            }
            catch (Exception ex)
            {

                return false;
            }
        }
        public bool checkValidLeaveType(string leaveType)
        {
            bool flag = false;

            using (ComplianceDBEntities db = new ComplianceDBEntities())
            {
                flag = db.RLCS_LeaveTypeMaster.Where(x => x.LeaveType == leaveType).Any();

            }
            return flag;
        }

        public void GenerateAvacom_LeaveMappingErrorCSV(List<ErrorMessage> detailView, string FileName)
        {

            try
            {

                if (!Directory.Exists(Server.MapPath("~/Attendance_Mapping/TempEmpError/")))
                {
                    Directory.CreateDirectory(Server.MapPath("~/Attendance_Mapping/TempEmpError/"));
                }

                string FilePath = Server.MapPath("~/Attendance_Mapping/TempEmpError/") + FileName;

                using (StreamWriter swOutputFile = new StreamWriter(new FileStream(FilePath, FileMode.Create, FileAccess.Write, FileShare.Read)))
                {
                    string delimiter = "";
                    StringBuilder sb = new StringBuilder();
                    List<string> CsvRow = new List<string>();

                    if (detailView.Count > 0)
                    {

                        string content = "Error Description,Row No";
                        CsvRow.Add(content + Environment.NewLine);

                        foreach (var data in detailView)
                        {
                            if (data != null)
                            {

                                content = data.ErrorDescription + "," + data.RowNum;
                                CsvRow.Add(content + Environment.NewLine);
                            }
                        }
                    }

                    sb.AppendLine(string.Join(delimiter, CsvRow));
                    swOutputFile.WriteLine(sb.ToString());
                    btnDownloadError.Visible = true;
                }
            }
            catch (Exception e)
            {

            }
        }
        protected void btnDownloadError_Click(object sender, EventArgs e)
        {
            try
            {
                string filePath = Server.MapPath("~/Attendance_Mapping/TempEmpError/" + hdFileName.Value);
                DownloadFile(filePath);
            }
            catch (Exception ex)
            {
                file_Validator.IsValid = false;
                file_Validator.ErrorMessage = "Internal server Error";
            }
        }
        private void DownloadFile(string Path)
        {
            try
            {
                FileInfo file = new FileInfo(Path);
                if (file.Exists)
                {
                    Response.Clear();
                    Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
                    Response.AddHeader("Content-Length", file.Length.ToString());
                    Response.ContentType = "text/plain";
                    Response.Flush();
                    Response.TransmitFile(file.FullName);
                    Response.End();
                }
            }
            catch (Exception ex)
            {

            }
        }
        

        protected void _sampleFile_Click(object sender, EventArgs e)
        {
            if ((Directory.Exists(Server.MapPath("~/Attendance_Mapping/SampleFile"))))
            {
                string filepath = Server.MapPath("~/Attendance_Mapping/SampleFile/SampleFIleLeaveMapping.xlsx");
                DownloadFile(filepath);
            }
            else
            {

            }
        }
    }

    public class ErrorMessage
    {
        public string ErrorDescription { get; set; }
        public string RowNum { get; set; }
    }
}