﻿<%@ Page Title="My Workspace" Language="C#" MasterPageFile="~/HRPlusCompliance.Master" AutoEventWireup="true" CodeBehind="RLCS_MyWorkspace.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.RLCS_MyWorkspace" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        $(document).ready(function () {
            fhead('My Workspace/My Compliances');
            $("#divFilterLocation").hide();
        });

        $(document).on("click", "#ContentPlaceHolder1_UpdatePanel1", function (event) {
            if (event.target.id == "") {
                var idvid = $(event.target).closest('div');
                if ($(idvid).attr('id') != undefined && $(idvid).attr('id') != '' && $(idvid).attr('id').indexOf('tvFilterLocation') > -1) {
                    $("#divFilterLocation").show();
                } else {
                    $("#divFilterLocation").hide();
                }
            }
            else if (event.target.id != "ContentPlaceHolder1_tbxFilterLocation") {
                $("#divFilterLocation").hide();
            } else if (event.target.id != "" && event.target.id.indexOf('tvFilterLocation') > -1) {
                $("#divFilterLocation").show();
            } else if (event.target.id == "ContentPlaceHolder1_tbxFilterLocation") {
                $("#ContentPlaceHolder1_tbxFilterLocation").unbind('click');

                $("#ContentPlaceHolder1_tbxFilterLocation").click(function () {
                    $("#divFilterLocation").toggle("blind", null, 500, function () { });
                });
            }
        });      
        $('#divShowDialog').on('show.bs.modal', function () {

            //alert("called");
            $('#divShowDialog').find('.modal-body').css({
                width: 'auto', //probably not needed
                height: 'auto', //probably not needed 
                'max-height': '100%'
            });
        });
        $('#divShowDialog').on("show", function () {
            $(this).find(".modal-body").css("max-height", height);
        });

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }

        function hidediv() {
            var div = document.getElementById('AdvanceSearch');
            div.style.display == "none" ? "block" : "none";
            $('.modal-backdrop').hide();
            return true;
        }

        function ShowDialog(InstanceID, ScheduleOnID) {
            var modalHeight = screen.height - 150;

            if (modalHeight < 0)
                modalHeight = 200;

            $('#divShowDialog').modal('show');
            $('.modal-dialog').css('width', '95%');
            $('#showdetails').attr('width', '100%');
            $('#showdetails').attr('height', modalHeight + "px");
            $('#showdetails').attr('src', "/RLCS/RLCS_ComplianceStatusTransaction.aspx?instanceID=" + InstanceID + "&scheduleOnID=" + ScheduleOnID);
        };

        function CloseModal() {
            $('#divShowDialog').modal('hide');
            document.getElementById('<%= btnSearch.ClientID %>').click();
            return true;
        }

        function GotoComplianceInputOutputPage(complianceType, returnRegisterChallanID, actID, complianceID, branchID, month, year) {
            window.location.href = "/RLCS/ComplianceInputOutput.aspx?ComplianceType=" + complianceType
                                                            + "&ReturnRegisterChallanID=" + returnRegisterChallanID
                                                            + "&ActID=" + actID
                                                            + "&ComplianceID=" + complianceID
                                                            + "&BranchID=" + branchID
                                                            + "&Month=" + month
                                                            + "&Year=" + year;
        }
    </script>

    <style type="text/css">
        .clspenaltysave {
            font-weight: bold;
            margin-left: 15px;
        }

        .btnss {
            background-image: url(../Images/edit_icon_new.png);
            border: 0px;
            width: 24px;
            height: 24px;
            background-color: transparent;
        }

        .table > thead > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

        .table > tbody > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

        .table > thead > tr > th > a {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

        .table tr th {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .clsheadergrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: bottom !important;
            border-bottom: 2px solid #dddddd !important;
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
            text-align: left;
        }

        .clsROWgrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: top !important;
            border-top: 1px solid #dddddd !important;
            color: #666666 !important;
            font-size: 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }

        .Inforamative {
            color: blue !important;
            border-top: 1px solid #dddddd !important;
        }

        tr.Inforamative > td {
            color: blue !important;
            border-top: 1px solid #dddddd !important;
        }


        .circle {
            /*background-color: green;*/
            width: 15px;
            height: 15px;
            border-radius: 50%;
            display: inline-block;
            margin-right: 20px;
        }
        .col-md-1half{
            width:12.499999995%;
            float:left;
            position: relative;
            min-height: 1px;
            padding-left: 15px;
            padding-right: 15px;
        }
        .pl0 {
            padding-left: 0px;
        }

        .pr0 {
            padding-right: 0px;
        }

        .chosen-results {
            max-height: 100px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional" OnLoad="upComplianceDetails_Load">
        <ContentTemplate>

            <div class="row Dashboard-white-widget">
                <div class="col-md-12 colpadding0">
                    <asp:ValidationSummary ID="vsDocGen" runat="server" class="alert alert-block alert-danger fade in" ValidationGroup="DocGenValidationGroup" />
                    <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                        ValidationGroup="DocGenValidationGroup" Display="None" />
                </div>

                <div class="col-md-12 colpadding0">
                    <div class="col-md-1 pl0" style="width: 8%;">
                        <%--<label for="ddlPageSize" class="control-label">Show</label>--%>
                        <asp:DropDownList runat="server" ID="ddlPageSize" AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged"
                            class="form-control" style="height: 32px;">
                            <asp:ListItem Text="5" />
                            <asp:ListItem Text="10" Selected="True" />
                            <asp:ListItem Text="20" />
                            <asp:ListItem Text="50" />
                        </asp:DropDownList>
                    </div>

                    <div class="col-md-1 pl0" style="width: 13%;">
                        <%--<label for="ddlScopeType" class="control-label">Compliance Type</label>--%>
                        <asp:DropDownListChosen runat="server" ID="ddlScopeType" Width="100%" class="form-control" AutoPostBack="true">
                            <%--OnSelectedIndexChanged="ddlScopeType_SelectedIndexChanged"--%>
                            <asp:ListItem Text="Type" Value="All"></asp:ListItem>
                            <asp:ListItem Text="Register" Value="SOW03"></asp:ListItem>
                            <asp:ListItem Text="PF Challan" Value="EPF"></asp:ListItem>
                            <asp:ListItem Text="ESI Challan" Value="ESI"></asp:ListItem>
                            <asp:ListItem Text="PT Challan" Value="PT"></asp:ListItem>
                            <asp:ListItem Text="Return" Value="SOW05"></asp:ListItem>
                        </asp:DropDownListChosen>
                    </div>
                    
                    <div class="col-md-1 pl0" style="width: 24%;">
                        <%--<label for="tbxFilterLocation" class="control-label">Entity/Branch</label>--%>
                        <asp:TextBox runat="server" AutoCompleteType="None" ID="tbxFilterLocation" Style="padding: 0px; padding-left: 10px; margin: 0px; height: 32px; width: 100%; border: 1px solid #c7c7cc; border-radius: 4px; color: #444"
                            CssClass="txtbox" />
                        <div id="divFilterLocation" style="margin-left: 1px; position: absolute; z-index: 10; width: 95%; margin-top: -20px;">
                            <asp:TreeView runat="server" ID="tvFilterLocation" SelectedNodeStyle-Font-Bold="true" NodeStyle-ForeColor="#8e8e93"
                                Style="overflow: auto; border-left: 1px solid #c7c7cc; border-right: 1px solid #c7c7cc; border-bottom: 1px solid #c7c7cc; background-color: #ffffff; color: #8e8e93 !important;" ShowLines="true"
                                OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged">
                            </asp:TreeView>
                        </div>
                    </div>

                    <div class="col-md-1 pl0" style="width: 10%;">
                        <%--<label for="ddlRiskType" class="control-label">Risk</label>--%>
                        <asp:DropDownListChosen runat="server" ID="ddlRiskType" class="form-control" Width="100%">
                            <asp:ListItem Text="Risk-All" Value="-1" />
                            <asp:ListItem Text="High" Value="0" />
                            <asp:ListItem Text="Medium" Value="1" />
                            <asp:ListItem Text="Low" Value="2" />
                        </asp:DropDownListChosen>
                    </div>

                      <div class="col-md-1 pl0" style="width: 12.5%;">
                        <%--<label for="ddlMonth" class="control-label">Month</label>--%>
                        <asp:DropDownListChosen runat="server" ID="ddlMonth" class="form-control" Width="100%">
                            <asp:ListItem Text="Month" Value="-1"></asp:ListItem>
                            <asp:ListItem Text="January" Value="01"></asp:ListItem>
                            <asp:ListItem Text="February" Value="02"></asp:ListItem>
                            <asp:ListItem Text="March" Value="03"></asp:ListItem>
                            <asp:ListItem Text="April" Value="04"></asp:ListItem>
                            <asp:ListItem Text="May" Value="05"></asp:ListItem>
                            <asp:ListItem Text="June" Value="06"></asp:ListItem>
                            <asp:ListItem Text="July" Value="07"></asp:ListItem>
                            <asp:ListItem Text="August" Value="08"></asp:ListItem>
                            <asp:ListItem Text="September" Value="09"></asp:ListItem>
                            <asp:ListItem Text="October" Value="10"></asp:ListItem>
                            <asp:ListItem Text="November" Value="11"></asp:ListItem>
                            <asp:ListItem Text="December" Value="12"></asp:ListItem>
                        </asp:DropDownListChosen>
                    </div>

                    <div class="col-md-1 pl0" style="width: 12.5%;">
                        <%--<label for="ddlYear" class="control-label">Year</label>--%>
                        <asp:DropDownListChosen runat="server" ID="ddlYear" class="form-control" Width="100%">
                            <asp:ListItem Text="Year" Value="-1"></asp:ListItem>
                            <asp:ListItem Text="2020" Value="2020"></asp:ListItem>
                            <asp:ListItem Text="2019" Value="2019"></asp:ListItem>
                            <asp:ListItem Text="2018" Value="2018"></asp:ListItem>
                            <asp:ListItem Text="2017" Value="2017"></asp:ListItem>
                            <asp:ListItem Text="2016" Value="2016"></asp:ListItem>
                            <asp:ListItem Text="2015" Value="2015"></asp:ListItem>
                            <asp:ListItem Text="2014" Value="2014"></asp:ListItem>
                            <asp:ListItem Text="2013" Value="2013"></asp:ListItem>
                            <asp:ListItem Text="2012" Value="2012"></asp:ListItem>
                            <asp:ListItem Text="2011" Value="2011"></asp:ListItem>                            
                            <asp:ListItem Text="2010" Value="2010"></asp:ListItem>
                            <asp:ListItem Text="2009" Value="2009"></asp:ListItem>
                        </asp:DropDownListChosen>
                    </div>

                     <div class="col-md-1 pl0 pr0" style="width: 13%;">
                        <%--<label for="ddlStatus" class="control-label">Status</label>--%>
                        <asp:DropDownListChosen runat="server" ID="ddlStatus" class="form-control" Width="100%">
                        </asp:DropDownListChosen>
                    </div>

                    <div class="col-md-1 pr0 text-right" style="width: 7%;">
                        <%--<label for="btnFilter" class="hidden-label">Apply</label>--%>
                        <asp:Button ID="btnSearch" CausesValidation="false" class="btn btn-primary" OnClick="btnSearch_Click" runat="server" Text="Apply" />
                    </div>
                </div>

                <div class="tab-content">
                    <div runat="server" id="performerdocuments" class="tab-pane active">
                        <asp:GridView runat="server" ID="grdComplianceTransactions" AutoGenerateColumns="false" AllowSorting="true"
                            GridLines="None" CssClass="table" AllowPaging="True" PageSize="10" DataKeyNames="ScheduledOnID" OnPageIndexChanging="grdComplianceTransactions_PageIndexChanging"
                            OnRowDataBound="grdComplianceTransactions_RowDataBound" OnRowCommand="grdComplianceTransactions_RowCommand" ShowHeaderWhenEmpty="true">
                            <Columns>
                               <%-- <asp:TemplateField HeaderText="Sr" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                                    <ItemTemplate>
                                        <%#Container.DataItemIndex+1 %>
                                    </ItemTemplate>
                                </asp:TemplateField>--%>

                                <asp:TemplateField HeaderText="Compliance">
                                    <ItemTemplate>
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;"> <%--width: 250px;--%>
                                            <asp:Label ID="LabelD" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ShortForm") %>' ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                                            <asp:Label ID="lblInterimdays" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Interimdays") %>' Visible="false"></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Entity/Branch">
                                    <ItemTemplate>
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;"> <%--width: 100px;--%>
                                            <asp:Label ID="lbllocation" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("CustomerBranchName") %>' ToolTip='<%# Eval("CustomerBranchName") %>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Period">
                                    <ItemTemplate>
                                        <%# Eval("ForMonth") %>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Due Date">
                                    <ItemTemplate>
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;"> <%--width: 100px;--%>
                                            <asp:Label ID="lblScheduledOn" runat="server" data-toggle="tooltip" data-placement="bottom"
                                                Text='<%# Convert.ToDateTime(Eval("PerformerScheduledOn")).ToString("dd-MMM-yyyy") %>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Status">
                                    <ItemTemplate>
                                        <asp:Label ID="lblStatus" runat="server" data-toggle="tooltip" data-placement="bottom"
                                            Text='<%# Eval("ComplianceStatusName") %>' ToolTip='<%# Eval("ComplianceStatusName") %>'></asp:Label>
                                        <asp:Label ID="lblStatusID" runat="server" data-toggle="tooltip" data-placement="bottom"
                                            Text='<%# Eval("ComplianceStatusID") %>' ToolTip='<%# Eval("ComplianceStatusID") %>' Visible="false"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="CanChangeStatus" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCanChangeStatus" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("CanChangeStatus") %>' ToolTip='<%# Eval("CanChangeStatus") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Action" HeaderStyle-CssClass="text-right" ItemStyle-CssClass="text-right">
                                    <ItemTemplate>
                                        <asp:Button runat="server" ID="btnChangeStatus" OnClick="btnChangeStatus_Click" CssClass="btnss" Visible="false" 
                                            CommandName="CHANGE_STATUS" CommandArgument='<%# Eval("ScheduledOnID") + "," + Eval("ComplianceInstanceID") + "," + Eval("Interimdays") + "," + Eval("ComplianceStatusName") + "," + Eval("PerformerScheduledOn")  %>' />                                        
                                        <asp:LinkButton ID="lnkbtn_GotoIOM" runat="server" Visible="false" CommandName="GOTO_IO" CommandArgument='<%# Eval("ScheduledOnID")+","+Eval("CustomerID")+","+Eval("CustomerBranchID")+","+ Eval("ActID")+","+ Eval("ComplianceID")+","+Eval("RLCS_PayrollMonth")+","+Eval("RLCS_PayrollYear")+"" %>' data-toggle="tooltip" data-placement="left" ToolTip="Upload Input"><img src="../Images/proceed.png" style="vertical-align:top;" alt="Upload" title="Upload" /></asp:LinkButton>
                                        <%--Visible='<%# CanChangeStatus((long)Eval("UserID"), (int)Eval("RoleID"), (int)Eval("ComplianceStatusID")) %>'--%>
                                    </ItemTemplate> 
                                </asp:TemplateField>
                            </Columns>
                            <PagerStyle HorizontalAlign="Right" />
                            <RowStyle CssClass="clsROWgrid" />
                            <HeaderStyle CssClass="clsheadergrid" />
                            <PagerTemplate>
                                <table style="display: none">
                                    <tr>
                                        <td>
                                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                        </td>
                                    </tr>
                                </table>
                            </PagerTemplate>
                            <EmptyDataTemplate>
                                No Records Found.
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </div>

                    <div class="col-md-12 colpadding0">
                        <div class="col-md-6 colpadding0">
                            <div runat="server" id="DivRecordsScrum">
                                <p style="padding-right: 0px !Important;color: #666;">
                                    <asp:Label ID="Label4" runat="server" Text="Showing "></asp:Label>
                                    <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>- 
                                            <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                                            <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                                </p>
                            </div>
                        </div>
                        <div class="col-md-6 colpadding0">
                            <div class="table-paging" style="margin-bottom: 20px">
                                <asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick="Previous_Click" />

                                <div class="table-paging-text">
                                    <p>
                                        <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                        <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>
                                    </p>
                                </div>
                                <asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick="Next_Click" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                         
            <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
            <asp:HiddenField ID="hdnTitle" runat="server" />
            <asp:HiddenField ID="IsActiveControl" runat="server" Value="false" />
            <div class="modal fade" id="divShowDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header bgColor-gray" style="height: 35px; text-align: left;">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="closeModal();">&times;</button>
                            </div>

                            <div class="modal-body" style="background-color: #f7f7f7;">
                                <iframe id="showdetails" src="about:blank" width="95%" height="80%" frameborder="0"></iframe>
                            </div>
                        </div>
                    </div>
                </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    
</asp:Content>

