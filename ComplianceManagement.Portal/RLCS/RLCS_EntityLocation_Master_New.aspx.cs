﻿using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class RLCS_EntityLocation_Master_New : System.Web.UI.Page
    {
        protected static int CustId, UserId;
        protected static string avacomRLCSAPI_URL;
        protected static int? ServiceProviderID;
        protected static int IsSPDist;
        protected static int spID = -1;
        protected static int distID = -1;
        protected static int custID = -1;
        protected int userID;
        protected static string RoleCode;

        protected void Page_Load(object sender, EventArgs e)
        {
            custID = -1;
            spID = -1;
            distID = -1;

            if (HttpContext.Current.Request.IsAuthenticated)
            {

                CustId = 0;
                userID = Convert.ToInt32(AuthenticationHelper.UserID);
                RoleCode = AuthenticationHelper.Role;
                if (!string.IsNullOrEmpty(Request.QueryString["CustID"]))
                {
                    CustId = Convert.ToInt32(Request.QueryString["CustID"]);
                    UserId = Convert.ToInt32(AuthenticationHelper.UserID);
                    ViewState["CustomerID"] = CustId;
                    ServiceProviderID = Business.CustomerManagement.GetServiceProviderID(Convert.ToInt32(CustId));
                }
                else
                {
                    CustId = (int)AuthenticationHelper.CustomerID;
                }

                //CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                avacomRLCSAPI_URL = ConfigurationManager.AppSettings["AVACOM_RLCS_API_URL"];

                if (AuthenticationHelper.Role == "HEXCT" && ServiceProviderID == 94)
                {
                    lnkBtnComAssign.Visible = false;
                    lnkBtnComAct.Visible = false;
                }
                if (AuthenticationHelper.Role == "SPADM")
                {
                    spID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    IsSPDist = 0;
                }
                else if (AuthenticationHelper.Role == "DADMN")
                {
                    distID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    IsSPDist = 1;
                }
                else if (AuthenticationHelper.Role == "IMPT")
                {
                    IsSPDist = 2;
                }
                else if (AuthenticationHelper.Role == "CADMN")
                {
                    custID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    IsSPDist = 3;
                }
            }
            else
            {
                FormsAuthentication.SignOut();
                Session.Abandon();
                FormsAuthentication.RedirectToLoginPage();
            }
        }

        protected void Page_PreInit(object sender, EventArgs e)
        {
            string masterpage = Convert.ToString(Session["masterpage"]);
            if (masterpage == "HRPlusSPOCMGR")
            {
                this.Page.MasterPageFile = "~/HRPlusSPOCMGR.Master";
            }
            else if (masterpage == "HRPlusCompliance")
            {
                this.Page.MasterPageFile = "~/HRPlusCompliance.Master";
            }
        }

        protected void btnPaycode_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/RLCS/RLCS_PaycodeMappingDetails_Updated.aspx?CustomerID=" + Convert.ToInt32(ViewState["CustomerID"]));
        }
        protected void btnAttendanceMapping_Click(object sender, EventArgs e)
        {
            var CustomerId = -1;
            if(hdnSelectedCustomerId.Value != "" && hdnSelectedCustomerId.Value != null)
            {
                CustomerId = Convert.ToInt32(hdnSelectedCustomerId.Value); 
            }
            else if(Convert.ToInt32(ViewState["CustomerID"]) != 0)
            {
                CustomerId = Convert.ToInt32(ViewState["CustomerID"]);
            }
            else
            {
                CustomerId = CustId;
            }
            Response.Redirect("~/RLCS/Avacom_LeaveMappingNew.aspx?CustomerID=" + CustomerId);
        }
    }
}