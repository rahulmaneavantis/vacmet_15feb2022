﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Avacom_LeaveMapping.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Avacom_LeaveMapping.Avacom_LeaveMapping" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../NewCSS/3.3.7/bootstrap.min.css" rel="stylesheet" />
    <link href="../NewCSS/bootstrap-datepicker.min.css" rel="stylesheet" />
    <link href="../NewCSS/bootstrap-datepicker.css" rel="stylesheet" />
    <script src="../Newjs/3.3.1/jquery.min.js"></script>
    <script src="../Newjs/3.3.7/bootstrap.min.js"></script>
    <script src="../Newjs/bootstrap-datepicker.min.js"></script>
    <script src="../Newjs/jquery.validate.min.js"></script>
    <script src="../Newjs/jquery.validate.unobtrusive.js"></script>

    <link href="style.css" rel="stylesheet" />
    <link href="../assets/select2.min.css" rel="stylesheet" />
    <script src="../assets/select2.min.js"></script>

    <link href="../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <link href="../NewCSS/font-awesome.min.css" rel="stylesheet" />


    <style>
        element.style {
        }

        input[type=checkbox], input[type=radio] {
            margin: 4px 15px 0;
            margin-top: 1px\9;
            line-height: normal;
        }
    </style>
    <script type="text/javascript">
        function sampledownload() {
            document.getElementById("_sampleFile").click();
        }
        function hidesampleDownLink()
        {
            document.getElementById('lnkDownload').style.display = 'none';
           // $("#lnkDownload").style.display = 'none';
        }
        function showsampleDownLink()
        {
            $("#lnkDownload").style.display = 'block';
            document.getElementById('lnkDownload').style.display = 'block';
        }
        
        function HeaderCheckBoxClick(checkbox)
        {
            var gridview = document.getElementById("GridView1");
            for(var i=1;i<gridview.rows.length;i++)
            {
                gridview.rows[i].cells[0].getElementsByTagName("INPUT")[0].checked = checkbox.checked;
            }
        }
        function ChildCheckBoxClick(checkbox) {
            var gridview = document.getElementById("GridView1");
            var singlecheckboxUnchecked = false;
            for (var i = 1; i < gridview.rows.length; i++) {
                if(gridview.rows[i].cells[0].getElementsByTagName("INPUT")[0].checked ==false)
                {
                    singlecheckboxUnchecked = true;
                    break;
                }
            }
            gridview.rows[0].cells[0].getElementsByTagName("INPUT")[0].checked = !singlecheckboxUnchecked;

        }
    </script>
</head>
    <body>
    <form id="form1" runat="server">

        <asp:CustomValidator ID="file_Validator" runat="server" Enabled="true"></asp:CustomValidator>
        <table>
            <tr>
                <td>
                    <div style="width: 80%; float: left;">
                        <asp:DropDownList runat="server" ID="ddlClientList" OnSelectedIndexChanged="ddlClientList_SelectedIndexChanged" Style="padding: 0px; margin: 0px; height: 35px; width: 390px;"
                            CssClass="form-control" AutoPostBack="true" NoResultsText="No results match." Width="350px"
                            AllowSingleDeselect="true" />
                    </div>
                </td>
                <td>
                    <asp:Button ID="UploadBtn" class="btn btn-primary" BackColor="SkyBlue" Style="height: 36px; font-weight: 400; float: right;" runat="server" OnClick="UploadBtn_Click" Text="Upload" />
                </td>
                <td>
                    <a id="lnkDownload" title="Download Sample File" onclick="sampledownload()" data-toggle="tooltip" data-placement="left" style="background: url('../images/Excel _icon.png'); float: right; width: 75px; height: 33px;"></a>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:HiddenField ID="hdFileName" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblmsg" runat="server" />
                    <asp:FileUpload  style="margin-top:20px; " ID="LeaveMappingFileUpload" runat="server" />
                </td>
            </tr>
        </table>



        <div>
            <asp:Button ID="_sampleFile" class="btn btn-primary" Style="height: 36px; font-weight: 400; display: none" runat="server" Text="SampleFile" OnClick="_sampleFile_Click" />

            <asp:Button ID="btnDownloadError" runat="server" OnClick="btnDownloadError_Click" Text="Download_ErrorFile" />
            <br />
        </div>
        <div style="padding-top: 25px;">
            <asp:GridView ID="GridView1" AutoGenerateColumns="false" runat="server" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="5">
                <FooterStyle BackColor="White" ForeColor="#000066" />
                <HeaderStyle BackColor="#E9EAEA" ForeColor="#666666" Font-Names="sans-serif" Font-Size="15px" HorizontalAlign="Center" height="35px" />
                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                <RowStyle ForeColor="#000066" />
                <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                <SortedAscendingHeaderStyle BackColor="#007DBB" />
                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                <SortedDescendingHeaderStyle BackColor="#00547E" />

                <Columns>
                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="40px">
                        <HeaderTemplate>
                            <asp:CheckBox ID="chkHeader" runat="server" onclick="HeaderCheckBoxClick(this)" />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkChild" runat="server" onclick="ChildCheckBoxClick(this)" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-Width="300px" HeaderText="Avacom_LeaveType">
                        <ItemTemplate>
                            <asp:Label ID="_label" Text='<%# Eval("LeaveType") %>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-Width="300px" HeaderText="Description">
                        <ItemTemplate>
                            <asp:Label Text='<%# Eval("Leave_Description") %>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-Width="300px" HeaderText="ClientCode">
                        <ItemTemplate>
                            <asp:TextBox runat="server" ID="txtbox" style=" width:300px;  border:none;"  Text='<%# Eval("ClientCode")%>'></asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>

                </Columns>
            </asp:GridView>
        </div>
        <div>
            <asp:Button ID="btnProcess" class="btn btn-primary" BackColor="SkyBlue" Style="height: 36px; font-weight: 400; margin-left: 210px; margin-top: 15px;" Text="Save" runat="server"
                Font-Bold="true" OnClick="btnProcess_Click" />
        </div>


    </form>
</body>
</html>
