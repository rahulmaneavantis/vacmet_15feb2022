﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HRPlus_PricingPlan.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.HRPlus_PricingPlan" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" style="background: white;">
<head runat="server">
    <title></title>
    <!-- Bootstrap CSS -->
    <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap theme -->
    <link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <!-- font icon -->
    <link href="~/NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <!--external css-->
    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/contract_custom_style.css" rel="stylesheet" />
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="/Newjs/jquery.js"></script>
    <script type="text/javascript" src="/Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="/Newjs/bootstrap.min.js"></script>

    <!-- nice scroll -->
    <script type="text/javascript" src="/Newjs/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="/Newjs/jquery.nicescroll.js"></script>

    <link href="../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../Newjs/jszip.min.js"></script>

    <script src="/Newjs/bootstrap-datepicker.min.js"></script>
    <link href="/NewCSS/bootstrap-datepicker.min.css" rel="stylesheet" />

    <script>
        $(document).ready(function () {
            //Bind_PricingPlan();
            $('.fromMonthPicker').datepicker({
                autoclose: true,
                minViewMode: 1,
                format: 'dd-MM-yyyy'
            });
        });
    </script>

    <script>
        function Bind_PricingPlan() {

            debugger;

            var isDateField = [];

            var apiURL = '<%=AVACOM_RLCS_API_URL%>GetPricingPlan';

            var dataSource = new kendo.data.DataSource({
                transport: {
                    read: {
                        url: apiURL,
                        dataType: "json",
                        beforeSend: function (request) {
                            request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                            request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                            request.setRequestHeader('Content-Type', 'application/json');
                        },
                    }
                },
                schema: {
                    data: function (response) {
                        debugger;
                        if (response != null && response != undefined) {
                            generateGrid(response.Result);
                            return response.Result;
                        }
                    },
                    total: function (response) {
                        if (response.Result != null && response.Result != undefined) {
                            if (response.Result.length > 0) {

                            }
                            return response.Result.length;
                        }
                    }
                },
                pageSize: 50,
            });

            dataSource.read();
        }

        function generateGrid(response) {
            var model = generateModel(response);
            var columns = generateColumns(response);

            var grid = $("#grid").kendoGrid({
                dataSource: {
                    transport: {
                        read: function (options) {
                            options.success(response.data);
                        }
                    },
                    pageSize: 5,
                    schema: {
                        model: model
                    }
                },
                columns: columns,
                pageable: true,
                editable: true
            });
        }

        function generateColumns(response) {
            debugger;
            var columnNames = response["columns"];
            return columnNames.map(function (name) {
                return { field: name, format: (isDateField[name] ? "{0:D}" : "") };
            })
        }

        function generateModel(response) {
            debugger;
            var sampleDataItem = response[0];

            var model = {};
            var fields = {};
            for (var property in sampleDataItem) {
                if (property.indexOf("ID") !== -1) {
                    model["id"] = property;
                }
                var propType = typeof sampleDataItem[property];

                if (propType === "number") {
                    fields[property] = {
                        type: "number",
                        validation: {
                            required: true
                        }
                    };
                    if (model.id === property) {
                        fields[property].editable = false;
                        fields[property].validation.required = false;
                    }
                } else if (propType === "boolean") {
                    fields[property] = {
                        type: "boolean"
                    };
                } else if (propType === "string") {
                    var parsedDate = kendo.parseDate(sampleDataItem[property]);
                    if (parsedDate) {
                        fields[property] = {
                            type: "date",
                            validation: {
                                required: true
                            }
                        };
                        isDateField[property] = true;
                    } else {
                        fields[property] = {
                            validation: {
                                required: true
                            }
                        };
                    }
                } else {
                    fields[property] = {
                        validation: {
                            required: true
                        }
                    };
                }
            }

            model.fields = fields;

            return model;
        }
    </script>
    <style>
        .table tbody > tr > td, .table tbody > tr > th, .table tfoot > tr > td, .table tfoot > tr > th, .table thead > tr > td, .table thead > tr > th
{
            border:0.2px solid lightgray;
        }   
             span.input-group-addon {
            padding: 0px;
        }
    </style>
</head>
<body style="background: white;">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="smPricingPlan" EnablePartialRendering="true" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <div>
                    <div class="row col-md-12 col-sm-12 col-xs-12 colpadding0 mb10">
                        <div id="grid" style="border: none;"></div>
                    </div>

                    <div class="row col-md-12 col-sm-12 col-xs-12 colpadding0 mb10">
                        <div class="col-md-10 col-sm-8 col-xs-8">
                        </div>

                        <div class="col-md-2 col-sm-4 col-xs-4 input-group date" style="padding-left: 0px;">
                            <span class="input-group-addon">
                                <span class="fa fa-calendar" style="padding: 6px !important; color: black;"></span>
                            </span>
                            <asp:TextBox runat="server" Height="32px" placeholder="Effective Date" Style="padding-left: 5px;"
                                class="form-control text-center fromMonthPicker" ID="txtEffectiveDate" autocomplete="off" OnTextChanged="txtEffectiveDate_TextChanged" />
                        </div>
                    </div>

                    <div class="row col-md-12">
                        <asp:GridView runat="server" ID="gridCompliances" AutoGenerateColumns="true" AllowSorting="true" GridLines="None"
                            CssClass="table" AllowPaging="true" PageSize="50" Width="100%" ShowHeaderWhenEmpty="true" EnableSortingAndPagingCallbacks="true">

                            <RowStyle CssClass="clsROWgrid" />
                            <HeaderStyle CssClass="clsheadergrid" />

                            <PagerTemplate>
                                <table style="display: none">
                                    <tr>
                                        <td>
                                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                        </td>
                                    </tr>
                                </table>
                            </PagerTemplate>

                            <EmptyDataTemplate>
                                No Records Found.                       
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
            </Triggers>
        </asp:UpdatePanel>
    </form>
</body>
</html>
