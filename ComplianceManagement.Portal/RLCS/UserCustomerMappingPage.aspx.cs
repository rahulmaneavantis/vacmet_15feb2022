﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business;
using System.Globalization;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class UserCustomerMappingPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            vsUserCustomerMapping.CssClass = "alert alert-danger";
            if (!IsPostBack)
            {
                BindCustomers();

                long recordID = 0;
                if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
                {
                    recordID = Convert.ToInt64(Request.QueryString["ID"]);
                }

                if (recordID != 0)
                {
                    EditRecord(recordID);
                }
                else
                {
                    ddlCustomerPopup_SelectedIndexChanged(null, null);
                }
            }
        }

        private void BindCustomers()
        {
            try
            {
                int customerID = -1;
                int serviceProviderID = -1;
                int distributorID = -1;

                if (AuthenticationHelper.Role == "CADMN")
                {
                    //customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "SPADM")
                {
                    serviceProviderID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "DADMN")
                {
                    distributorID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }

                var customerList = CustomerManagement.GetAll_HRComplianceCustomersByServiceProviderOrDistributor(customerID, serviceProviderID, distributorID, 2, false);
                // var customerList = CustomerManagement.GetAll_HRComplianceCustomers_IncludesServiceProvider(customerID, serviceProviderID, 2);
                //var customerList = CustomerManagement.GetAll_HRComplianceCustomers(customerID, serviceProviderID, 2);

                #region Pop-Up DropDown

                ddlCustomerPopup.DataTextField = "Name";
                ddlCustomerPopup.DataValueField = "ID";

                ddlCustomerPopup.DataSource = customerList;
                ddlCustomerPopup.DataBind();

                //ddlCustomerPages.Items.Insert(0, new ListItem("Select", "-1"));

                #endregion
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUCMPopup.IsValid = false;
                cvUCMPopup.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void ddlCustomerPopup_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ddlUserPopup.ClearSelection();
                ddlMgrPopup.ClearSelection();

                BindUsers_Popup();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUCMPopup.IsValid = false;
                cvUCMPopup.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        private void BindUsers_Popup()
        {
            try
            {
                int customerID = -1;
                int distID = -1;
                int serviceProviderID = -1;

                if (!string.IsNullOrEmpty(ddlCustomerPopup.SelectedValue))
                {
                    customerID = Convert.ToInt32(ddlCustomerPopup.SelectedValue);
                }

                var lstUsers = UserCustomerMappingManagement.GetAll_Users(customerID, serviceProviderID, distID);

                BindUserToDropDown(lstUsers, ddlUserPopup);
                BindUserToDropDown(lstUsers, ddlMgrPopup, "HMGR");
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUCMPopup.IsValid = false;
                cvUCMPopup.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        private void BindUserToDropDown(List<SP_RLCS_GetAllUser_ServiceProviderDistributor_Result> lstUsers, DropDownList ddltoBind, string roleCode = "")
        {
            try
            {
                if (lstUsers.Count > 0 && !string.IsNullOrEmpty(roleCode))
                    lstUsers = lstUsers.Where(row => row.RoleCode == roleCode).ToList();

                List<object> users = new List<object>();
                users = (from row in lstUsers
                         select new { ID = row.ID, Name = row.FirstName + " " + row.LastName, RoleCode = row.RoleCode }).Distinct().OrderBy(row => row.Name).ToList<object>();

                ddltoBind.Items.Clear();

                ddltoBind.DataTextField = "Name";
                ddltoBind.DataValueField = "ID";

                users.Insert(0, new { ID = -1, Name = "Select" });

                ddltoBind.DataSource = users;
                ddltoBind.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUCMPopup.IsValid = false;
                cvUCMPopup.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                bool saveSuccess = false;

                if (!string.IsNullOrEmpty(ddlCustomerPopup.SelectedValue) && ddlCustomerPopup.SelectedValue != "-1")
                {
                    if (!string.IsNullOrEmpty(ddlUserPopup.SelectedValue) && ddlUserPopup.SelectedValue != "-1")
                    {
                        //if (!string.IsNullOrEmpty(ddlMgrPopup.SelectedValue) && ddlMgrPopup.SelectedValue != "-1")
                        //{
                        int userID = Convert.ToInt32(ddlUserPopup.SelectedValue);
                        int customerID = Convert.ToInt32(ddlCustomerPopup.SelectedValue);
                        //int mgrID = Convert.ToInt32(ddlMgrPopup.SelectedValue);

                        List<UserCustomerMapping> lstUserCustmerMapping = new List<UserCustomerMapping>();
                        UserCustomerMapping objUserCustMapping = new UserCustomerMapping()
                        {
                            UserID = userID,
                            CustomerID = customerID,
                            //MgrID = mgrID,
                            IsActive = true
                        };

                        if (!string.IsNullOrEmpty(ddlMgrPopup.SelectedValue) && ddlMgrPopup.SelectedValue != "-1")
                            objUserCustMapping.MgrID = Convert.ToInt32(ddlMgrPopup.SelectedValue);

                        saveSuccess = UserCustomerMappingManagement.CreateUpdate_UserCustomerMapping(objUserCustMapping);
                        if (saveSuccess)
                        {
                            ddlUserPopup.ClearSelection();
                            ddlCustomerPopup.ClearSelection();
                            ddlMgrPopup.ClearSelection();

                            cvUCMPopup.IsValid = false;
                            cvUCMPopup.ErrorMessage = "Details Save Successfully";
                            vsUserCustomerMapping.CssClass = "alert alert-success";
                        }
                        else
                        {
                            cvUCMPopup.IsValid = false;
                            cvUCMPopup.ErrorMessage = "Something went wrong, please try again";
                        }
                        //}
                        //else
                        //{
                        //    cvUCMPopup.IsValid = false;
                        //    cvUCMPopup.ErrorMessage = "Select Customer/User to Assign";
                        //}
                    }
                    else
                    {
                        cvUCMPopup.IsValid = false;
                        cvUCMPopup.ErrorMessage = "Select User to Assign";
                    }
                }
                else
                {
                    cvUCMPopup.IsValid = false;
                    cvUCMPopup.ErrorMessage = "Select Customer to Assign";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUCMPopup.IsValid = false;
                cvUCMPopup.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        private void EditRecord(long recordID)
        {
            var objRecord = UserCustomerMappingManagement.GetRecord_UserCustomerMapping(recordID);

            if (objRecord != null)
            {
                if (ddlCustomerPopup.Items.FindByValue(objRecord.CustomerID.ToString()) != null)
                {
                    ddlCustomerPopup.ClearSelection();
                    ddlCustomerPopup.Items.FindByValue(objRecord.CustomerID.ToString()).Selected = true;

                    ddlCustomerPopup_SelectedIndexChanged(null, null);
                }

                if (ddlUserPopup.Items.FindByValue(objRecord.UserID.ToString()) != null)
                {
                    ddlUserPopup.ClearSelection();
                    ddlUserPopup.Items.FindByValue(objRecord.UserID.ToString()).Selected = true;
                }

                if (objRecord.MgrID != null)
                {
                    if (ddlMgrPopup.Items.FindByValue(objRecord.MgrID.ToString()) != null)
                    {
                        ddlMgrPopup.ClearSelection();
                        ddlMgrPopup.Items.FindByValue(objRecord.MgrID.ToString()).Selected = true;
                    }
                }

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenModal", "$(\"#divAssignEntitiesDialogpopup\").modal('show');", true);
            }
        }
    }
}