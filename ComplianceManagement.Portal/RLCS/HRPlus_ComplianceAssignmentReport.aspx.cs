﻿using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class HRPlus_ComplianceAssignmentReport : System.Web.UI.Page
    {
        protected static string AVACOM_RLCS_API_URL;

        protected static int customerID;
        protected static int loggedInUserID;
        protected static string loggedUserRole;
        protected static string loggedUserProfileID;

        protected static int distID = -1;
        protected static int spID = -1;

        protected void Page_PreInit(object sender, EventArgs e)
        {
            string custWise = Request.QueryString["CustWise"];
            if (!string.IsNullOrEmpty(custWise))
                this.MasterPageFile = "~/HRPlusCompliance.Master";
            else
                this.MasterPageFile = "~/HRPlusSPOCMGR.Master";
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            AVACOM_RLCS_API_URL = ConfigurationManager.AppSettings["AVACOM_RLCS_API_URL"];

            loggedInUserID = Convert.ToInt32(AuthenticationHelper.UserID);
            customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            loggedUserRole = AuthenticationHelper.Role;
            loggedUserProfileID = AuthenticationHelper.ProfileID;

            if (AuthenticationHelper.Role == "SPADM")
            {
                spID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            }
            else if (AuthenticationHelper.Role == "DADMN")
            {
                distID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            }

            if (!IsPostBack)
            {
                try
                {

                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
            }


            //}

        }
    }
}