﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ComplianceDetails.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.ComplianceDetails" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    
    <link href="../NewCSS/stylenew.css" rel="stylesheet" />
    <link href="../NewCSS/bootstrap.min.css" rel="stylesheet" />

     <!-- bootstrap theme -->
    <link href="../NewCSS/bootstrap-theme.css" rel="stylesheet" />
    <!-- Bootstrap CSS -->
    
    <script type="text/javascript" src="../Newjs/jquery-1.8.3.min.js"></script>

    <style type="text/css">
        .ui-widget-header {
            border: 0px !important;
            background: inherit;
            font-size: 20px;
            color: #666666;
            font-weight: normal;
            padding-top: 0px;
            margin-top: 5px;
        }

        .table > tbody > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

        .table > tbody > tr > th > a {
            vertical-align: bottom;
            color: #666666 !important;
            text-decoration: none !important;
            border-bottom: 2px solid #dddddd;
        }

        .table tr th {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .clsheadergrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: bottom !important;
            border-bottom: 2px solid #dddddd !important;
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
            text-align: left;
        }

        .clsROWgrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: top !important;
            border-top: 1px solid #dddddd !important;
            color: #666666 !important;
            font-size: 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }

        .m-10 {
            margin-top: 10px;
            margin-left: 10px;
        }

        .btn-search {
            background: #1fd9e1;
            float: right;
        }

        .chosen-results {
            max-height: 150px !important;
        }

        .table tbody tr td {
            padding: 1px;
            vertical-align: middle;
        }

        .modal-body {
            padding: 3px 15px;
        }

        .table {
            margin-bottom: 0px;
        }

            .table tbody > tr > td, .table tbody > tr > th, .table tfoot > tr > td, .table tfoot > tr > th, .table thead > tr > td, .table thead > tr > th {
                padding: 0.5px;
                line-height: 1.428571429;
                vertical-align: middle;
            }
           
             td {
        max-width: 350px;
        overflow: hidden;
        text-overflow: ellipsis;
        white-space: nowrap;
        text-align:left;
    }

            .chosen-container.chosen-with-drop .chosen-drop {
                  width: 900px;
                  margin-left: -345px;
                  margin-top: 1px;
            }
    </style>

    <script type="text/javascript">
        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }
        $(document).ready(function () {
            $(document).tooltip({ selector: '[data-toggle="tooltip"]' });
            // window.parent.forchild($('#grdSummaryDetails').find('tr').length);
            var cal = ($('.table > tbody > tr').length * 45) + 200;
            //window.parent.fchangeheight('', cal + "px")
        });

        $(document).ready(function () {
            $("#GridStatutory").css('margin-bottom', '0px');

            if ($(window.parent.document).find("#openlink").children().first().hasClass("k-loading-mask"))
                $(window.parent.document).find("#openlink").children().first().hide();

            if ($(window.parent.document).find("#divreports").children().first().hasClass("k-loading-mask"))
                $(window.parent.document).find("#divreports").children().first().hide();

            if ($(window.parent.document).find("#divCompliancesShow").children().first().hasClass("k-loading-mask"))
                $(window.parent.document).find("#divCompliancesShow").children().first().hide();            
        });

        window.parent.loaderhide();
    </script>

    <script type="text/javascript">
            function fComplianceOverview(obj) {
                OpenOverViewpup( $(obj).attr('instanceid'), $(obj).attr('Ctype'));
            }

            function OpenOverViewpup(instanceid) {
                $('#divOverView').modal('show');
                $('#OverViewvs').attr('width', '950px');
                $('#OverViews').attr('height', '400px');
                $('.modal-dialog').css('width', '1000px');
                $('.modal-dialog').css('height', '414px');
                $('#OverViews').attr('src', "../RLCS/RLCS_ComplianceOverview.aspx?ComplianceScheduleID=&ComplainceInstatnceID=" + instanceid);
            }
    </script>

    <style>
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

        <asp:UpdatePanel ID="UpDetailView" runat="server" UpdateMode="Conditional" OnLoad="upDivLocation_Load">
            <ContentTemplate>
                <asp:UpdateProgress ID="updateProgress" runat="server">
                    <ProgressTemplate>
                        <div id="updateProgress1" style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                            <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                                AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
                        </div>
                    </ProgressTemplate>
                </asp:UpdateProgress>
                               
                <div class="row col-md-12 col-sm-12" style="margin-top: 10px;">
                    <div class="col-md-1 col-sm-1" style="padding-left: 10px;"> 
                        <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control" 
                            AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                            <asp:ListItem Text="10" Selected="True" />
                            <asp:ListItem Text="20" />
                            <asp:ListItem Text="50" />
                        </asp:DropDownList>
                    </div>

                    <div class="col-md-4 col-sm-4" style="padding-left: 0px;">
                        <asp:TextBox runat="server" ID="tbxFilterLocation" Style="padding: 0px; padding-left: 10px; margin: 0px; height: 33px; width: 100%; border: 1px solid #c7c7cc; border-radius: 4px; color: #8e8e93;"
                            CssClass="txtbox" />
                        <div style="margin-left: 1px; position: absolute; z-index: 10; margin-top: -20px;" id="divFilterLocation">
                            <asp:TreeView runat="server" ID="tvFilterLocation" SelectedNodeStyle-Font-Bold="true" Width="340px" NodeStyle-ForeColor="#8e8e93"
                                Style="overflow: auto; border-left: 1px solid #c7c7cc; border-right: 1px solid #c7c7cc; border-bottom: 1px solid #c7c7cc; background-color: #ffffff; color: #8e8e93 !important;" ShowLines="true"
                                OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged">
                            </asp:TreeView>
                        </div>
                    </div>

                    <div class="col-md-6 col-sm-6" style="padding-left: 0px;">
                        <asp:DropDownListChosen runat="server" ID="ddlAct" class="form-control" Width="100%" DataPlaceHolder="Select Act">
                        </asp:DropDownListChosen>
                    </div>

                    <%--    <div class="col-md-2 col-sm-2" style="padding-left: 0px; display: none;"></div>--%>
                    <div class="col-md-1 col-sm-1" style="padding-left: 0px; float: right;">
                        <label for="lbtnExportExcel" class="control-label" hidden="hidden">Export</label>&nbsp;
                        <asp:LinkButton runat="server" CssClass="btn" ID="lbtnExportExcel" Style="background: url('../images/Excel _icon.png'); float: right; width: 47px; height: 33px;"
                            data-toggle="tooltip" data-placement="bottom" Title="Export to Excel" ToolTip="Export to Excel" OnClick="lbtnExportExcel_Click">                                                  
                        </asp:LinkButton>
                    </div>
                </div>
                <div class="row col-md-12 col-sm-12">
                    <div class="col-xs-1 col-sm-1 col-md-1" style="width: 15%; float: right; text-align: right;">
                        <label for="txtStartPeriod" class="control-label" hidden="hidden">Filter</label>&nbsp;
                        <div>
                            <asp:Button ID="btnSearch" CausesValidation="false" class="btn btn-primary" runat="server" Text="Apply" OnClick="btnSearch_Click" />
                            <asp:Button ID="btnClear" CausesValidation="false" class="btn btn-primary" runat="server" Text="Clear" OnClick="btnClear_Click" />
                        </div>
                    </div>
                </div>

                <div class="row col-md-12 col-sm-12 AdvanceSearchScrum" style="padding-bottom: 0px;">
                    <div id="divAdvSearch" runat="server" visible="false">
                        <p>
                            <asp:Label ID="lblAdvanceSearchScrum" runat="server" Text=""></asp:Label>
                        </p>
                        <p>
                            <asp:LinkButton ID="lnkClearAdvanceList" OnClick="lnkClearAdvanceSearch_Click" runat="server">Clear Search</asp:LinkButton>
                        </p>
                    </div>
                    <div runat="server" id="DivRecordsScrum" style="float: right;">
                        <p style="padding-right: 0px !Important;">
                            <asp:Label ID="Label4" runat="server" Text="Showing "></asp:Label>
                            <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>- 
                                <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                                <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                        </p>
                    </div>
                </div>

                <div style="margin-bottom: -23px; font-weight: bold; font-size: 12px;">
                    <asp:Label runat="server" ID="lblDetailViewTitle"></asp:Label>
                </div>

                <div class="row col-md-12" style="margin-bottom: 0px;">
                    <asp:GridView runat="server" ID="GridStatutory" AutoGenerateColumns="false" AllowSorting="true" GridLines="None" 
                        CssClass="table" AllowPaging="true" PageSize="10">
                        <Columns>

                          <%--  <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Sr">
                                <ItemTemplate>
                                    <%#Container.DataItemIndex+1 %>
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                            <asp:TemplateField HeaderText="ComplianceInstance" Visible="false">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                        <asp:Label ID="lblPerformer" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# (long)Eval("ComplianceInstanceID") %>' ToolTip='<%#(long)Eval("ComplianceInstanceID") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>

                              <asp:TemplateField HeaderText="Branch">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                        <asp:Label ID="lblBranchName" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Branch") %>' ToolTip='<%# Eval("Branch") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Act">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px;">
                                        <asp:Label ID="lblActName" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Sections">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                        <asp:Label ID="lblSections" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Section") %>' ToolTip='<%# Eval("Section") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Compliance">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px;">
                                        <asp:Label ID="lblShortDesc" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ShortForm") %>' ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Form">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 90px;">
                                        <asp:Label ID="lblform" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("RequiredForms") %>' ToolTip='<%# Eval("RequiredForms") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                                <asp:TemplateField HeaderText="Frequency">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 90px;">
                                        <asp:Label ID="lblFrequency" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Frequency") %>' ToolTip='<%# Eval("Frequency") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Name" Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblUser" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("User") %>' ToolTip='<%# Eval("User") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" HeaderText="Action">
                                <ItemTemplate>
                                    <asp:UpdatePanel ID="upDownloadFile" runat="server">
                                        <ContentTemplate>
                                            <asp:ImageButton ID="lblOverView" runat="server" ImageAlign="Middle"
                                                CssClass="k-button k-button-icontext ob-overview k-grid-edit2" Visible="false"
                                                instanceId='<%#Eval("ComplianceInstanceID") %>' ctype='Statutory'
                                                OnClientClick='fComplianceOverview(this)' ToolTip="Click to OverView"></asp:ImageButton>
                                            <input type="image" name="grdCompliancePerformer$ctl02$lblOverView" id="grdCompliancePerformer_lblOverView_0" title="Click to OverView" instanceid='<%#Eval("ComplianceInstanceID") %>' ctype='Statutory' src="../Images/Eye.png"
                                                onclick="fComplianceOverview(this);" style="cursor: pointer; padding-top: 1px; vertical-align: middle;" />

                                            <a role="button" class="btn btn primary" instanceid='<%#Eval("ComplianceInstanceID") %>' ctype='Statutory'
                                                onclick='fComplianceOverview(this)' tooltip="Click to OverView" <%--style="border-radius: 44px; width: 36px; height: 34px;"--%>><span class="fa icon-facebook" style="margin-left: 3px;"></span></a>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>

                        <RowStyle CssClass="clsROWgrid" />
                        <HeaderStyle CssClass="clsheadergrid" />

                        <PagerTemplate>
                            <table style="display: none">
                                <tr>
                                    <td>
                                        <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                    </td>
                                </tr>
                            </table>
                        </PagerTemplate>

                        <EmptyDataTemplate>
                            No Records Found.
                        </EmptyDataTemplate>
                    </asp:GridView>

                    <asp:GridView runat="server" ID="GridInternalCompliance" AutoGenerateColumns="false" GridLines="none" AllowSorting="true"
                        CssClass="table" AllowPaging="true" PageSize="10" Width="100%">
                        <%--OnPageIndexChanging="grdSummaryDetails_PageIndexChanging" DataKeyNames="ID"--%>
                        <Columns>

                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Sr">
                                <ItemTemplate>
                                    <%#Container.DataItemIndex+1 %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Description">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                        <asp:Label ID="lblSections" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ShortForm") %>' ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Type">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                        <asp:Label ID="lblActName" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("InternalComplianceTypeName") %>' ToolTip='<%# Eval("InternalComplianceTypeName") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Category">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                        <asp:Label ID="lblShortDesc" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("InternalComplianceCategoryName") %>' ToolTip='<%# Eval("InternalComplianceCategoryName") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblUser" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("User") %>' ToolTip='<%# Eval("User") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <RowStyle CssClass="clsROWgrid" />
                        <HeaderStyle CssClass="clsheadergrid" />
                        <PagerTemplate>
                            <table style="display: none">
                                <tr>
                                    <td>
                                        <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                    </td>
                                </tr>
                            </table>
                        </PagerTemplate>

                        <EmptyDataTemplate>
                            No Records Found.
                        </EmptyDataTemplate>
                    </asp:GridView>

                </div>

                <div class="clearfix"></div>

                <div class="col-md-12 col-sm-12 colpadding0" style="margin-top: 10px;">
                    <div class="col-md-6 col-sm-6 colpadding0" style="float: right;">
                        <div class="table-paging" style="margin-bottom: 0px; margin-right: 10px;">
                            <asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick="Previous_Click" />

                            <div class="table-paging-text">
                                <p>
                                    <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                            <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>
                                </p>
                            </div>

                            <asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick="Next_Click" />
                            <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                        </div>
                    </div>
                </div>

                <div>
                    <div class="modal fade" id="divOverView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                        <div class="modal-dialog" style="width: 1150px;">
                            <div class="modal-content" style="width: 100%;">
                                <div class="modal-header" style="border-bottom: none; height: 20px;">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                </div>

                                <div class="modal-body">

                                    <iframe id="OverViews" src="about:blank" width="950px" height="100%" frameborder="0"></iframe>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="lbtnExportExcel" />
            </Triggers>
        </asp:UpdatePanel>

        <script type="text/javascript" src="../Newjs/bootstrap.min.js"></script>

        <%-- <script>
             $('#updateProgress1').show();

             $(document).ready(function () {
                 $(document).tooltip({ selector: '[data-toggle="tooltip"]' });
                 $('#updateProgress1').hide();
             });
         </script>--%>
    </form>
</body>
</html>
