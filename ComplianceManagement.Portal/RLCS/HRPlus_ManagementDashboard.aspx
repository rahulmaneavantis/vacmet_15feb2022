﻿<%@ Page Title="MY DASHBOARD" Language="C#" MasterPageFile="~/HRPlusCompliance.Master" AutoEventWireup="true" CodeBehind="HRPlus_ManagementDashboard.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.HRPlus_ManagementDashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="AVANTIS - Products that simplify">
        <meta name="author" content="AVANTIS - Development Team">

        <title>DASHBOARD :: AVANTIS - Products that simplify</title>
       
       
        
        <!-- Offline-->
         <%-- <script type="text/javascript" src="../Newjs/jquery.min.js"></script> --%>
     <%-- <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>--%>
     <%--   <base href="https://demos.telerik.com/kendo-ui/slider/events">--%>
    <%--<style>html { font-size: 14px; font-family: Arial, Helvetica, sans-serif; }</style>--%>

   <%-- <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2020.2.617/styles/kendo.default-v2.min.css" />
    <script src="https://kendo.cdn.telerik.com/2020.2.617/js/jquery.min.js"></script>
    <script src="https://kendo.cdn.telerik.com/2020.2.617/js/kendo.all.min.js"></script>--%>
   <%-- <link rel="stylesheet" href="../content/shared/styles/examples-offline.css">--%>
    <%--<script src="../content/shared/js/console.js"></script>--%>



        <!--  <link href="https://avacdn.azureedge.net/avantischarts/jquery-ui.min.css" rel="stylesheet" />-->

         <link href="../newcss/spectrum.css" rel="stylesheet" />

        <link href="../tree/jquerysctipttop.css" rel="stylesheet" />

        <link rel="stylesheet" href="../tree/jquery-simple-tree-table.css" />

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>

         <link href="../../NewCSS/kendo.common.min.css" rel="stylesheet" />
        <link href="../../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
        <link href="../../NewCSS/kendo.silver.min.css" rel="stylesheet" />
        <link href="../../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

        <script type="text/javascript" src="../../Newjs/jquery-1.12.4.min.js"></script>
        <script type="text/javascript" src="../../Newjs/kendo.all.min.js"></script>
        <script type="text/javascript" src="../../Newjs/jszip.min.js"></script>
    
      
        <style type="text/css">

            .k-reset.k-slider-items{
                display:none;
            }
            .k-slider-track {
                min-width: 304px;
            }


            .k-reset.k-slider-items{
                display:none;
            }

               #fieldlist {
                   margin: 0 0 -2em;
                   padding: 0;
                   text-align: center;
               }

               #fieldlist > li {
                   list-style: none;
                   padding-bottom: 2em;
               }

               #fieldlist label {
                   display: block;
                   padding-bottom: 1em;
                   font-weight: bold;
                   text-transform: uppercase;
                   font-size: 12px;
                   color: #444;
               }

            </style>
        <style type="text/css">
            .FirstDiv {
                height: 400Px;
                width: 150px;               
                top: 50px;
                left: 0px;
                border: 1px solid #ceced2;             
            }
            .responsive-calendar .day a {
                color: #000000;
                display: block;
                cursor: default;
                padding: 30% 0 20% 0;
            }

            .responsive-calendar .day.active a {
                background-color: #1d86c8;
                color: #ffffff;
                height: 100%;
                cursor: pointer;
            }
            span.k-icon.k-i-window-minimize {
                display: none;
            }

            .modal-body, .widget .padd {
                padding: 10px 10px 2px 10px;
            } 
            
             /*dailyupdates*/
          
        .tags > * {
                display: inline-block;
                list-style: none;
            }
            .card-text-sm {
                margin-bottom: -1rem !important;
                -webkit-margin-before: -30px;
                display: block;
                margin-block-start: 0;
                margin-block-end: 0;
                margin-inline-start: 0;
                margin-inline-end: 0;
                font-weight: bold;
            }
            .tags {
                margin: 0;
                padding: 0;
                margin-bottom: 0;
            }
            .h-100 {
                height: 100% !important;
            }
            .text-black-50 {
                font-size: 10px;
                color: rgba(0,0,0,.5) !important;
                margin-top: 15px;
                font-weight: 420;
            }
            .box {
                border: 1px solid transparent;
                padding: 10px;
                border-radius: 5px;
                background-color: #FFFFFF;
                box-shadow: 0 7px 20px 0 rgba(210,210,210,0.5);
            }
            .a-tag {
                color: #37A8F1;
                background-color: rgba(55, 168, 241, 0.20);
                font-size: 12px;
                font-weight: bold;
                padding: 6px 10px 7px;
                height: 28px;
                border-radius: 14px;
                display: inline-block;
                margin: 0 4px 10px;
                max-width: 130px;
                min-width: 104px;

                width: 100px;
                overflow: hidden;
                white-space: nowrap;
                text-overflow: ellipsis;
                text-align: center;
            }
            /*end dailyupdates*/ 
            #dailyupdates .bx-viewport {
                height: 326px !important;
            }

            .info-box:hover {
                color: #FF7473;
                font-weight: 500;
                -webkit-transform: scale(1.1);
                -ms-transform: scale(1.1);
                transform: scale(1.1);
                z-index: 5;
            }

            .function-selector-radio-label {
                font-size: 12px;
            }

            .colorPickerWidget {
                padding: 10px;
                margin: 10px;
                text-align: center;
                width: 360px;
                border-radius: 5px;
                background: #fafafa;
                border: 2px solid #ddd;
            }

            #ContentPlaceHolder1_grdGradingRepportSummary.table tr td {
                border: 1px solid white;
            }

            .TMB {
                padding-left: 0px !important;
                padding-right: 0px !important;
                width: 16.5% !important;
            }

            .TMB1 {
                padding-left: 0px !important;
                padding-right: 0px !important;
                /*width: 19.5% !important;*/
                width: 23.5% !important;
                margin-left: 15.5px;
            }

            .TMBImg {
                padding-left: 0px !important;
                padding-right: 0px !important;
                margin-left: -7%;
                margin-right: -3%;
            }

            .clscircle {
                margin-right: 7px !important;
            }

            .fixwidth {
                width: 20% !important;
            }

            .badge {
                font-size: 10px !important;
                font-weight: 200 !important;
            }

            .responsive-calendar .day {
                width: 13.7% !important;
                height: 45px;
            }

                .responsive-calendar .day.cal-header {
                    border-bottom: none !important;
                    width: 13.9% !important;
                    font-size: 17px;
                    height: 25px;
                }

            #collapsePerformerLoc > div > div > div > div > div > div > a.bx-prev {
                left: 0%;
            }

            #collapsePreviewerLoc > div > div > div > div > div > div > a.bx-prev {
                left: 0%;
            }

            .bx-viewport {
                height: 285px !important;
                margin-left: -41px;
                width: 110% !important;
            }

            /*#dailyupdates .bx-viewport {
                height: 190px !important;
            }*/

            table#ContentPlaceHolder1_rbFinancialYearFunctionSummery > tbody > tr > td > span > label {
                margin-left: 3px;
            }

            .graphcmp {
                margin-left: 36%;
                font-size: 16px;
                margin-top: -3%;
                color: #666666;
                font-family: 'Roboto';
            }

            .days > div.day {
                margin: 1px;
                background: #eee;
            }

            .overdue ~ div > a {
                background: red;
            }

            .info-box {
                min-height: 95px !important;
            }

            .Dashboard-white-widget {
                padding: 5px 10px 0px !important;
            }

            .dashboardProgressbar {
                display: none;
            }

            .TMBImg > img {
                width: 47px;
                margin-left: 10px;
            }

            #reviewersummary {
                height: 150px;
            }

            #performersummary {
                height: 150px;
            }

            #eventownersummary {
                height: 150px;
            }

            #performersummarytask {
                height: 150px;
            }

            #reviewersummarytask {
                height: 150px;
            }

            div.panel {
                margin-bottom: 12px;
            }

            .panel .panel-heading .panel-actions {
                height: 25px !important;
            }

            hr {
                margin-bottom: 8px;
            }

            .panel .panel-heading h2 {
                font-size: 18px;
            }

            td > label {
                padding: 6px;
            }

            .radioboxlist radioboxlistStyle {
                font-size: x-large;
                padding-right: 20px;
            }

            span.input-group-addon {
                padding: 0px;
            }

            td > label {
                padding: 3px 4px 0 4px;
                margin-top: -1%;
            }

            .nav-tabs > li > a {
                color: #333 !important;
            }

            .nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover {
                color: #1fd9e1 !important;
                background-color: #eee !important;
            }

            .panel-heading .nav .active, .panel-heading .nav > li.active > a, .panel-heading .nav > li > a:hover {
                background-color: #eee !important;
            }
        </style>

        <script type="text/javascript">
 
            function fComplianceOverview(obj) {
                OpenOverViewpup($(obj).attr('scheduledonid'), $(obj).attr('instanceid'), $(obj).attr('Ctype'));
            }

            function OpenOverViewpup(scheduledonid, instanceid) {
                $('#divOverView').modal('show');
                $('#OverViews').attr('width', '1250px');
                $('#OverViews').attr('height', '500px');
                $('.modal-dialog').css('width', '1306px');
                $('#OverViews').attr('src', "../Common/ComplianceOverview.aspx?ComplianceScheduleID=" + scheduledonid + "&ComplainceInstatnceID=" + instanceid);

            }
            function initializeDatePickerTopStartdate(TopStartdate) 
            {
                $('#<%= txtAdvStartDate.ClientID %>').datepicker({
                    pickTime: false,
                    format: "dd/mm/yyyy",
                    autoclose: true

                }).on('changeDate', function (selected) {
                    startDate = new Date(selected.date.valueOf());
                    startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
                    $('#<%= txtAdvEndDate.ClientID %>').datepicker('setStartDate', startDate);
                });

                if (TopStartdate != null) 
                    {
                        $("#<%= txtAdvStartDate.ClientID %>").datepicker("option", "defaultDate", TopStartdate);
                    }
            }
            function initializeDatePickerTopEnddate(TopEnddate) 
            {
                $('#<%= txtAdvEndDate.ClientID %>').datepicker({
                    pickTime: false,
                    format: "dd/mm/yyyy",
                    autoclose: true,
                }).on('changeDate', function (selected) {
                    FromEndDate = new Date(selected.date.valueOf());
                    FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
                    $('#<%= txtAdvStartDate.ClientID %>').datepicker('setEndDate', FromEndDate);

                });
             
                if (TopEnddate != null) 
                 {
                        $("#<%= txtAdvEndDate.ClientID %>").datepicker("option", "defaultDate", TopEnddate);
                 }
            }

            function initializeDatePickerTopStartdate(TopStartdate) 
            {
                var startDate = new Date();
                $('#<%= txtAdvStartDate.ClientID %>').datepicker({
                    dateFormat: 'dd-mm-yy',
                    maxDate: $('#<%=  txtAdvEndDate.ClientID  %>').val(),
                    minDate: $('#<%=  txtAdvStartDate.ClientID  %>').val(),
                    numberOfMonths: 1,
                    changeMonth: true,
                    changeYear: true,
                    autoclose: true
                });

                if (TopStartdate != null) 
                {
                    $("#<%= txtAdvStartDate.ClientID %>").datepicker("option", "defaultDate", TopStartdate);
                }
            }

            function initializeDatePickerTopEnddate(TopEnddate) 
            {
                var startDate = new Date();
                $('#<%= txtAdvEndDate.ClientID %>').datepicker({
                    dateFormat: 'dd-mm-yy',
                    maxDate: $('#<%=  txtAdvEndDate.ClientID %>').val(),
                    minDate: $('#<%=  txtAdvStartDate.ClientID  %>').val(),
                    numberOfMonths: 1,
                    changeMonth: true,
                    changeYear: true,
                    autoclose: true
                });

                if (TopEnddate != null) 
                {
                    $("#<%= txtAdvEndDate.ClientID %>").datepicker("option", "defaultDate", TopEnddate);
                }
            }   
           
        </script>


        <style type="text/css">

            .modal-body, .widget .padd {
                padding: 0px 15px;
            } 

            table#basic > tr, td {
                border-radius: 5px;
            }

            table#basic {
                border-collapse: unset;
                border-spacing: 3px;
            }

            .locationheadbg {
                background-color: #999;
                color: #fff;
                border: #666;
            }

            .locationheadLocationbg {
                background-color: #fff;
                border: 1px solid #e0e0e0;
            }

            td.locationheadLocationbg > span.tree-icon {
                background-color: #1976d2 !important;
                padding-right: 12px;
                color: white;
            }

            .GradingRating1 {
                /*background-color: #8fc156;*/
                background-color: #006500;
                border: 1px solid #4a8407;
                Cursor: pointer;
            }

            .GradingRating2 {
                /*background-color: #ffc107;*/
                background-color: #ffcd70;
                border: 1px solid #ba8b00;
                Cursor: pointer;
            }

            .GradingRating3 {
                /*background-color: #ef9a9a;*/
                background-color: #FF0000;
                border: 1px solid #b65454;
                Cursor: pointer;
            }

            .GradingRating4 {
                background-color: #e6e6e6;
                border: 1px solid #c4c4c4;
            }

            li#ContentPlaceHolder1_liTLDashboard:hover {                
                color: #1fd9e1 !important;
                background-color: #fff !important;
                border-top-left-radius: 5px;
                border-top-right-radius: 5px;
            }

            a#ContentPlaceHolder1_lnkShowTLDashboard :hover {
                background-color: #fff !important;
            }

            a#ContentPlaceHolder1_lnkShowTLDashboard :hover {
                background: white !important;
            }
        </style>

    </head>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section id="container" class=""> 
         <div class="clearfix"></div>
         <header class="panel-heading tab-bg-primary " style="background: transparent; padding-top: 0px; padding-left: 0px; height: 18px;">
             <div class="col-md-12 colpadding0">
                   
                 <div class="col-md-12 colpadding0">
                        <div style="float: right; margin-right: 6px;">            
                            <asp:Label ID="Label1" runat="server" Text="" Style="color:#666;margin-right: 8px;"  ></asp:Label>
                            <asp:LinkButton ID="btnRefresh1" runat="server" OnClick="btnRefresh_Click" ToolTip="Refresh Now" data-toggle="tooltip">
                            <label style="font-weight: bold;color: blue; cursor: pointer;">Refresh Now</label>              
                            </asp:LinkButton>           
                        </div>
                </div>
             </div>                                      
         </header>
     <div class="clearfix" style="height:20px;"></div>
     
    
    <!-- Top Count start -->
  
    <div id="divTabs" runat="server" visible="false" class="row">

        <div id="entitycount" runat="server" class="col-lg-3 col-md-3 col-sm-12 col-xs-12 TMB" onclick="settracknew('Management Dashboard','Summary','Entities','')">
            <div class="info-box indigo-bg">
                <div class="div-location" >
                    <div class="col-md-5 TMBImg">
                        <img class="imgDynamic" data-src="../img/manager-ico-location.png" src="../img/manager-ico-location.png"/>
                    </div>
                    <div class="col-md-7 colpadding0">
                        <div class="titleMD" style="text-align: left">Entities</div>
                        <div id="divEntitesCount" runat="server" class="countMD" style="text-align: left">0</div>
                    </div>
                </div>
            </div>
            <!--/.info-box-->
        </div>
        <!--/.col-->

        <div id="locationcount" runat="server" class="col-lg-3 col-md-3 col-sm-12 col-xs-12 TMB" onclick="settracknew('Management Dashboard','Summary','Location','')">
            <div class="info-box seablue-bg">
                <div class="div-location" style="cursor: pointer" onclick="PopulateGraphdata('<%=IsApprover%>')">
                    <div class="col-md-5 TMBImg">
                        <img class="imgDynamic" data-src="../img/manager-ico-location.png" src="../img/manager-ico-location.png"/>
                    </div>
                    <div class="col-md-7 colpadding0">
                        <div class="titleMD" style="text-align: left">Locations</div>
                        <div id="divLocationCount" runat="server" class="countMD" style="text-align: left">0</div>
                    </div>
                </div>
            </div>
            <!--/.info-box-->
        </div>
        <!--/.col-->
        <!--/.col-->

        <div id="compliancecount" runat="server" class="col-lg-3 col-md-3 col-sm-12 col-xs-12 TMB" onclick="settracknew('Management Dashboard','Summary','Compliance','')">
            <div class="info-box green-bg">
                <div class="div-location" style="cursor: pointer" onclick="fCompliances(<%=customerID%>,<%=BID %>,'<%=IsSatutoryInternal%>','<%=IsApprover%>')">
                    <div class="col-md-5 TMBImg">
                        <img class="imgDynamic" data-src="../img/manager-ico-compliances.png" src="../img/manager-ico-compliances.png"/>
                    </div>
                    <div class="col-md-7 colpadding0" style="margin-left: -4%;">
                        <div class="titleMD" style="text-align: left; font-size: 17px;">Compliances</div>
                        <div id="divCompliancesCount" runat="server" class="countMD" style="text-align: left">0</div>
                    </div>
                </div>
            </div>
            <!--/.info-box-->
        </div>
        <!--/.col-->

        <div id="usercount" runat="server" class="col-lg-3 col-md-3 col-sm-12 col-xs-12 TMB" onclick="settracknew('Management Dashboard','Summary','Users','')">
            <div class="info-box rednew-bg">
                <div class="div-location" onclick="fusers(<%=customerID%>,<%=BID %>,'<%=IsSatutoryInternal%>','<%=IsApprover%>')" style="cursor: pointer;">
                    <div class="col-md-5 TMBImg">
                        <img class="imgDynamic" data-src="../img/manager-ico-users.png" src="../img/manager-ico-users.png"/>
                    </div>
                    <div class="col-md-7 colpadding0">
                        <div class="titleMD" style="text-align: left">Users</div>
                        <div id="divUsersCount" runat="server" class="countMD" style="text-align: left">0</div>
                    </div>
                </div>
            </div>
            <!--/.info-box-->
        </div>
        <!--/.col-->
    </div>
    <!-- Top Count End -->

    <div class="clearfix"></div>
    <!-- Overdue start -->
    <div id="DivOverDueCompliance" runat="server" class="row Dashboard-white-widget">
        <div class="dashboard">
            <div class="col-lg-12 col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a data-toggle="collapse" data-parent="#accordion" href="#ContentPlaceHolder1_collapseDivOverDueCompliance">
                            <h2>Summary of Overdue Compliances</h2>
                        </a>
                        <div class="panel-actions">
                            
                        </div>
                    </div>
                    <div id="collapseDivOverDueCompliance" class="panel-collapse">
                        <div id="DivStatutoryOverdueCompliance" >
                            <iframe id="IFOverdueCompliance" src="about:blank" scrolling="no" frameborder="0" width="100%" height="130px"></iframe>
                        </div>

                              <div style="float: right;">
                                    <button id="lnkOverdueCompliance"  Style="margin-left: -5px; color: blue; font-style: italic; border: none; background: white;" onclick="OpenOverdueComplainceList(event)" 
                                       >Show All</button>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- Overdue End -->

    <div class="clearfix"></div>
    <!-- Filter start -->
    <div id="DivFilters" class="row Dashboard-white-widget">
        <div class="dashboard">
            <div class="col-lg-12 col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading" style="margin-bottom: 5px;">
                        
                            <h2><a data-toggle="collapse" data-parent="#accordion" href="#ContentPlaceHolder1_collapseDivFilters">Filters </a></h2>
                    </div>
                    <div id="collapseDivFilters" class="panel-collapse collapse in" runat="server">
                        <div class="col-md-12" style="padding-left: 0px;">
                            <div class="col-md-2" style="padding-left: 0px; padding-right: 0px; width: 0%;display:none;">
                                <asp:DropDownList runat="server" ID="ddlStatus" Width="100%" Height="33px" class="form-control m-bot15 select_Date">
                                    <asp:ListItem Text="Statutory" Value="0" Selected="True" />
                                </asp:DropDownList>
                            </div>

                            <div class="col-md-3" style="padding-left: 0px; padding-right: 0px; width: 30%;">
                                <div class="col-md-2 input-group date" style="padding-left: 0px; padding-right: 0px;">
                                    <span class="input-group-addon">
                                        <span class="fa fa-calendar" style="padding: 3px !important; color: black;"></span>
                                    </span>
                                    <asp:TextBox runat="server" Height="33px" placeholder="Start Date" Width="120px" autocomplete="off"
                                        Style="padding-left: 5px;" class="form-control m-bot15 select_Date" ID="txtAdvStartDate" />
                                </div>
                                <div class="col-md-2 input-group date" style="padding-left: 40px; padding-right: 0px;">
                                    <span class="input-group-addon">
                                        <span class="fa fa-calendar" style="padding: 3px !important; color: black;"></span>
                                    </span>
                                    <asp:TextBox runat="server" Height="33px" placeholder="End Date" Width="120px" autocomplete="off"
                                        Style="padding-left: 5px; margin-left: 0px;"
                                        class="form-control m-bot15 select_Date" ID="txtAdvEndDate" />
                                </div>
                            </div>                          
                            <div class="col-md-3" style="width: 30%; padding-left: 30px;" >
                                 
                                    <asp:DropDownList runat="server" ID="ddlperiod" AutoPostBack="true"
                                        OnSelectedIndexChanged="ddlperiod_SelectedIndexChanged" Width="100%" Height="33px" class="form-control">
                                        <asp:ListItem Text="Current Month" Value="6" />
                                        <asp:ListItem Text="Previous Month" Value="3" />
                                        <asp:ListItem Text="Last 3 Months" Value="4" />
                                        <asp:ListItem Text="Last 6 Months" Value="5" />
                                        <asp:ListItem Text="Current YTD" Value="0" Selected="True" />
                                        <asp:ListItem Text="Current + Previous YTD" Value="1" />
                                        <asp:ListItem Text="All" Value="2" />                                                    
                                                                
                                    </asp:DropDownList>
                                 <div style="clear: both; height: 5px" ></div>
                                <div style="float: left; color: #666666;" >YTD: Year to Date</div>
                              </div>

                            <div class="col-md-3" style="margin-left: 5px; width: 28%; display: none;">
                                <fieldset runat="server" id="fldrbFinancialYearFunctionSummery" style="border-style: solid; border-radius: 5px; border-width: 1px; border-color: #c7c7cc; width: 100%; height: 33px;">
                                    <asp:UpdatePanel ID="uprdo" runat="server">
                                        <ContentTemplate>
                                            <div class="radiobuttoncontainer" style="margin-top: -1%;">
                                                <asp:RadioButtonList ID="rbFinancialYearFunctionSummery" runat="server" CssClass="radioboxlist"
                                                    RepeatDirection="Horizontal" AutoPostBack="true" Width="303px"
                                                    OnSelectedIndexChanged="rbFinancialYearFunctionSummery_SelectedIndexChanged">
                                                    <asp:ListItem Text="Current YTD" Value="0" Selected="True" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="Current Financial Year to date" />
                                                    <asp:ListItem Text="Current + Previous YTD" Value="1" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="Current + Previous Financial Year to date" />
                                                    <asp:ListItem Text="All" Value="2" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="All Financial Year to date" />
                                                </asp:RadioButtonList>
                                            </div>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="rbFinancialYearFunctionSummery" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </fieldset>
                                <div style="clear: both; height: 5px"></div>
                                <div style="float: left; color: #666666;">YTD: Year to Date</div>
                            </div>
                              <asp:UpdatePanel ID="upDivLocation" runat="server" UpdateMode="Conditional" OnLoad="upDivLocation_Load">
                                <ContentTemplate>
                                    <div class="col-md-3" style="padding-left: 25px; padding-right: 0px; width: 30%;">
                                        <asp:TextBox runat="server" ID="tbxFilterLocation" Style="padding: 0px; padding-left: 10px; margin: 0px; height: 33px; width: 100%; border: 1px solid #c7c7cc; border-radius: 4px; color: #8e8e93" CssClass="txtbox" autocomplete="off" />
                                        <div style="margin-left: 1px; display: none; position: absolute; z-index: 10; margin-top: -20px; height: 270px;" id="divFilterLocation">
                                            <asp:TreeView runat="server" ID="tvFilterLocation" SelectedNodeStyle-Font-Bold="true" Width="280px" NodeStyle-ForeColor="#8e8e93"
                                                Style="overflow-y: scroll; border-left: 1px solid #c7c7cc; border-right: 1px solid #c7c7cc; border-bottom: 1px solid #c7c7cc; background-color: #ffffff; color: #8e8e93 !important;" ShowLines="true"
                                                OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged">
                                            </asp:TreeView>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <div class="col-md-1" style="padding-left: 30px; width: 2.8%; margin-top:-1px;">

                                <asp:Button ID="btnTopSearch" class="btn btn-search" runat="server" Text="&nbsp;Apply&nbsp;&nbsp;" OnClick="btnTopSearch_Click"></asp:Button>

                            </div>

                        </div>
                        <div class="col-md-12" style="float: left; margin-bottom: 10px; padding-left: 0px; margin-top: -14px;">
                            <h5>Select Your Risk Color Preference</h5>

                            <div class="col-md-1 colpadding0" style="width: 4.333333%;">
                                <input id="critical" style="display: none;">
                            </div>
                            <div class="col-md-1 colpadding0">
                                <h5 class="colpadding0">Critical</h5>
                            </div>

                            <div class="col-md-1 colpadding0" style="width: 4.333333%;">
                                <input id="High" style="display: none;">
                            </div>
                            <div class="col-md-1 colpadding0">
                                <h5 class="colpadding0">High</h5>
                            </div>

                            <div class="col-md-1 colpadding0" style="width: 4.333333%;">
                                <input id="medium" style="display: none;">
                            </div>
                            <div class="col-md-1 colpadding0">
                                <h5 class="colpadding0">Medium</h5>
                            </div>

                            <div class="col-md-1 colpadding0" style="width: 4.333333%;">
                                <input id="low" style="display: none;">
                            </div>
                            <div class="col-md-1 colpadding0">
                                <h5 class="colpadding0">Low</h5>
                            </div>

                            
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- Filter End -->

    <!-- Functions Summary start -->
    <div id="FunctionSummary" class="row Dashboard-white-widget">
        <div class="dashboard">
            <div class="col-lg-12 col-md-12" style="padding-left: 5px; padding-right: 5px;">
                <div class="panel panel-default">
                    <div class="panel-heading" style="margin-left: 10px;">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseFunctionSummary">
                            <h2>Performance Summary </h2>
                        </a>
                        <div class="panel-actions">
                            <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapseFunctionSummary"><i class="fa fa-chevron-up"></i></a>
                            <a href="javascript:closeDiv('FunctionSummary')" class="btn-close"><i class="fa fa-times"></i></a>
                        </div>
                    </div>

                    <div id="collapseFunctionSummary" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <div class="col-md-12">
                                <div id="perStatusPieChartDiv" class="col-md-5" style="margin-left: -30px; width: 36.6%">
                                </div>
                                <div id="perFunctionChartDiv" class="col-md-7" style="margin-left: -8px; width: 65.3%">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Functions Summary End -->

    <!-- Functions Risk Criteria start -->
    <div id="RiskCriteria" class="row Dashboard-white-widget">
        <div class="dashboard">
            <div class="col-lg-12 col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading" style="margin-left: 10px;">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseRiskCriteria">
                            <h2>Risk Summary </h2>
                        </a>
                        <div class="panel-actions">
                            <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapseRiskCriteria"><i class="fa fa-chevron-up"></i></a>
                            <a href="javascript:closeDiv('RiskCriteria')" class="btn-close"><i class="fa fa-times"></i></a>
                        </div>
                    </div>
                    <div id="collapseRiskCriteria" class="panel-collapse collapse in">
                        <div style="clear: both; height: 2px;"></div>
                        <div class="panel-body">
                            <div class="col-md-12">
                                <div class="col-md-2"></div>
                                <div id="perRiskStackedColumnChartDiv" class="col-md-12">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Functions Risk Criteria end -->

     <!-- Functions Imprisonment Penalty Criteria start -->
    
    <!-- Functions Imprisonment Penalty Criteria end -->


    <!-- Functions Department Criteria start -->
    
    <!-- Functions Department Criteria end -->

  <asp:HiddenField ID="highcolor" runat="server" Value="#FF0000" />
    <asp:HiddenField ID="mediumcolor" runat="server" Value="#FFC952" />
    <asp:HiddenField ID="lowcolor" runat="server" Value="#6AA84F" />
    <asp:HiddenField ID="criticalcolor" runat="server" Value="#a61c00" />

    <asp:HiddenField ID="NotApplicableColor" runat="server" Value="#afafaf" />
    <asp:HiddenField ID="NotCompliedColor" runat="server" Value="#2195f2" />
    <asp:HiddenField ID="RejectedColor" runat="server" Value="#ed7d31" />
    <asp:HiddenField ID="OverdueColor" runat="server" Value="#F69990" />  <%--#800000--%>
    <asp:HiddenField ID="InProgressColor" runat="server" Value="#4bc0c0" />
    <asp:HiddenField ID="PendingforreviewColor" runat="server" Value="#0085de" />
    <asp:HiddenField ID="CompiledbutdocumentpendingColor" runat="server" Value="#332288" />
    <asp:HiddenField ID="closetimelyColor" runat="server" Value="#afafaf" />
    <asp:HiddenField ID="closedelayColor" runat="server" Value="#2195f2" />
    
    <!-- Functions Grading start -->
    
    <!-- Functions Grading end -->

    <!-- Calender start -->
    <div id="ComplianceCalender" runat="server" class="row Dashboard-white-widget" style="margin-top: 10px;">
        <div class="dashboard">

            <div class="col-lg-12 col-md-12 ">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapsePerformerCalender">
                            <h2>My Compliance Calendar</h2>
                        </a>

                        <div class="panel-actions">
                            <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapsePerformerCalender"><i class="fa fa-chevron-up"></i></a>
                            <a href="javascript:closeDiv('ComplianceCalender')" class="btn-close"><i class="fa fa-times"></i></a>
                        </div>
                    </div>
                </div>
                <div id="collapsePerformerCalender" class="panel-collapse collapse in">
                    <div class="row">
                        <div class="col-md-5 colpadding0">
                            <div style="float: left; width: 96%;">
                                
                                    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                        <ContentTemplate>
                                            <div class="radiobuttoncalendercontainer">
                                                <asp:RadioButtonList ID="rdbcalender" runat="server" CssClass="radioboxcalenderlist"
                                                    RepeatDirection="Horizontal" AutoPostBack="false" Width="100%" Visible="false">
                                                    <asp:ListItem Text="Statutory" Value="0" />
                                                    <asp:ListItem Text="All(Including Checklist)" Value="1" Selected="True" />
                                                </asp:RadioButtonList>
                                            </div>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="rdbcalender" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                               
                            </div>
                            <div style="clear: both; height: 5px;"></div>
                            <!-- Responsive calendar - START -->
                             <img class="imgDynamic_cal"  src="../images/loader.gif" style="display: none;position: fixed;top: 375px;z-index: 103;left: 447px;">

                            <div class="responsive-calendar" style="width: 95%">

                                <img class="imgDynamic" data-src="../images/loader.gif">
                            </div>
                          
                        </div>
                        <div class="col-md-7">
                            <div>
                                <span style="float: left; height: 20px; font-family: 'Roboto',sans-serif; color: #666; font-size: 16px;" id="clsdatel"></span>
                                <br />
                                <i style="font-family: 'Roboto',sans-serif; color: #666;">Select a date from calendar to view details</i>
                            </div>
                            <div class="clearfix" style="height: 0px;"></div>
                            <div id="datacal">
                                <div class="chart-loading" id="idloadercal"></div>
                                <img class="imgDynamic" data-src="../images/loader.gif" id="imgcaldate" style="position: absolute; margin-left: 40%; margin-top: 15%;">
                                <iframe id="calframe" src="about:blank" scrolling="no" frameborder="0" width="100%" height="350px"></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix" style="height: 10px"></div>
        </div>
    </div>
    <!-- Calender End -->

    <!-- Daily Updates - START -->
    <div class="row Dashboard-white-widget" id="dailyupdates">
        <div class="dashboard">
            <div class="col-lg-12 col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseDailyUpdates">
                            <h2>Daily Updates</h2>
                        </a>
                        <div class="panel-actions">
                            <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapseDailyUpdates"><i class="fa fa-chevron-up"></i></a>
                            <a href="javascript:closeDiv('dailyupdates')" class="btn-close"><i class="fa fa-times"></i></a>
                        </div>
                    </div>
                </div>
                <div id="collapseDailyUpdates" class="panel-collapse collapse in">
                    <div class="row dailyupdates">
                        <ul id="DailyUpdatesrow" class="bxslider bxdaily" style="width: 100%; max-width: none;">
                        </ul>
                    </div>
                    <!--/.row-->
                    <div class="clearfix" style="height: 10px"></div>
                </div>
                <div class="modal fade" id="NewsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog" style="width: 900px">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                            </div>
                            <div class="modal-body" id="dailyupdatecontent">
                                <h2 id="dailyupdatedate" style="margin-top: 1px;">Daily Updates: November 3, 2016</h2>
                                <h3 id="dailyupdatedateInner">Daily Updates: November 3,</h3>
                                <p id="dailytitle"></p>
                                <div id="contents" style="color: #666666;"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="display:none;">
                    <a class="btn btn-search" style="float: right;" href="../Users/DailyUpdateList.aspx" title="View" onclick="settracknew('Management Dashboard','LegalUpdate','Detailedview','')">View All</a>
                    <div class="clearfix" style="height: 50px"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Daily Updates - End -->

    <!-- News Letter - START -->
    <div class="row Dashboard-white-widget" id="newsletter" style="display:none;">
        <div class="dashboard">
            <div class="col-lg-12 col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseNewsletter">
                            <h2>News Letter</h2>
                        </a>
                        <div class="panel-actions">
                            <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapseNewsletter"><i class="fa fa-chevron-up"></i></a>
                            <a href="javascript:closeDiv('newsletter')" class="btn-close"><i class="fa fa-times"></i></a>
                        </div>
                    </div>
                </div>
                <div id="collapseNewsletter" class="panel-collapse collapse in">
                    <div class="row dailyupdates">
                        <ul id="Newslettersrow" class="bxslider bxnews" style="width: 100%; max-width: none;">
                        </ul>
                    </div>
                    <!--/.row-->
                    <div class="modal fade" id="Newslettermodal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog" style="width: 550px;">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                </div>
                                <div class="modal-body">
                                    <img id="newsImg" class="imgDynamic" data-src="../Images/xyz.png" style="border-bottom-left-radius: 10px; border-bottom-right-radius: 10px; width: 100%" />
                                    <h2 id="newsTitle"></h2>
                                    <div class="clearfix" style="height: 10px;"></div>
                                    <div id="newsDesc"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <a class="btn btn-search" style="float: right;" href="../Users/NewsLetterList.aspx" onclick="settracknew('Management Dashboard','LegalUpdate','ViewMore',''" title="View">View All</a>
                        <div class="clearfix" style="height: 50px"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- News Letter - End -->
    <!-- custom widget - START -->
    <div id="CustomWidgetCriteria" class="row Dashboard-white-widget">
        <div class="dashboard">
            <div class="col-lg-12 col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading" style="margin-left: 10px;">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseCustomWidgetCriteria">
                            <%--<h2>Widget Summary </h2>--%>
                        </a>
                        <div class="panel-actions">
                            <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapseCustomWidgetCriteria"><i class="fa fa-chevron-up"></i></a>
                            <a href="javascript:closeDiv('CustomWidgetCriteria')" class="btn-close"><i class="fa fa-times"></i></a>
                        </div>
                    </div>
                    <div id="collapseCustomWidgetCriteria" class="panel-collapse collapse in">
                        <div style="clear: both; height: 2px;"></div>
                        <div class="panel-body">
                            <div class="col-md-12">
                                <div class="col-md-2"></div>
                                <div id="DivGraphProcessObsStatus" runat="server" class="col-md-12">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- custom widget - End -->
    <div class="modal fade" id="divreports" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog" style="width: 98%;">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" style="margin-top:-5px;" aria-hidden="true">&times;</button>
                </div>

                <div class="modal-body" style="width: 100%">

                    <iframe id="showdetails" src="blank.html"  width="100%" height="100%" frameborder="0" scrolling="yes" style="height: 692px;"></iframe>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="divreportsLocation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog" style="width: 98%;">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" style="margin-top:-5px;" aria-hidden="true">&times;</button>
                </div>

                <div class="modal-body">

                    <iframe id="showdetailsLocation" src="blank.html"  width="100%" height="100%" frameborder="0" style="width: 100%;height: 470px"></iframe>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="divNotification" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>

                <div class="modal-body">
                    <h3 style="text-align: center">Notifications</h3>
                    <br />
                    <div id="divNotificationData"></div>
                </div>
            </div>
        </div>
    </div>


    <div>
        <div class="modal fade" id="divOverView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
            <div class="modal-dialog" style="width: 1150px;">
                <div class="modal-content" style="width: 100%;">
                    <div class="modal-header" style="border-bottom: none;">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>

                    <div class="modal-body">

                        <iframe id="OverViews" src="about:blank" width="1200px" height="100%" frameborder="0"></iframe>

                    </div>
                </div>
            </div>
        </div>
    </div>

    </section>       
   
    <asp:HiddenField ID="hidposition" runat="server" Value="" />
    <%--<script type="text/javascript" src="../avantischarts/jQuery-v1.12.1/jQuery-v1.12.1.js"></script>--%>
    <script type="text/javascript" src="https://avacdn.azureedge.net/avantischarts/highcharts.js"></script>
    <script type="text/javascript" src="https://avacdn.azureedge.net/avantischarts/drilldown.js"></script>
    <script type="text/javascript" src="https://avacdn.azureedge.net/avantischarts/exporting.js"></script>
   
    <script type="text/javascript" src="../Newjs/responsive-calendar1.js"></script>
   
    <script type="text/javascript" src="../tree/jquery-simple-tree-table.js"></script>
    <script type="text/javascript" src="../newjs/spectrum.js"></script>
  
  
    <script type="text/javascript">
        function fposition() {
            $('#<%=hidposition.ClientID%>').val(document.body.scrollTop);
        }
        function fsetscroll() {
            document.body.scrollTop = $('#<%=hidposition.ClientID%>').val();
        }
        //carousel
        $(document).ready(function () {
            complianceSummaryDetail();

            $('.bxslider-map').bxSlider({
                minSlides: 1,
                maxSlides: 1,
                slideWidth: 250,
                slideMargin: 50,
                moveSlides: 1,
                auto: false,

            });
        });
        //custom select box
        $(function () {
            $('select.styled').customSelect();
        });
    </script>
    <script type="text/javascript">
        $(function () {

            $(".knob").knob({

                draw: function () {

                    // "tron" case
                    if (this.$.data('skin') == 'tron') {

                        var a = this.angle(this.cv)  // Angle
                            , sa = this.startAngle          // Previous start angle
                            , sat = this.startAngle         // Start angle
                            , ea                            // Previous end angle
                            , eat = sat + a                 // End angle
                            , r = 1;

                        this.g.lineWidth = this.lineWidth;

                        this.o.cursor
                            && (sat = eat - 0.3)
                            && (eat = eat + 0.3);

                        if (this.o.displayPrevious) {
                            ea = this.startAngle + this.angle(this.v);
                            this.o.cursor
                                && (sa = ea - 0.3)
                                && (ea = ea + 0.3);
                            this.g.beginPath();
                            this.g.strokeStyle = this.pColor;
                            this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sa, ea, false);
                            this.g.stroke();
                        }

                        this.g.beginPath();
                        this.g.strokeStyle = r ? this.o.fgColor : this.fgColor;
                        this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sat, eat, false);
                        this.g.stroke();

                        this.g.lineWidth = 2;
                        this.g.beginPath();
                        this.g.strokeStyle = this.o.fgColor;
                        this.g.arc(this.xy, this.xy, this.radius - this.lineWidth + 1 + this.lineWidth * 2 / 3, 0, 2 * Math.PI, false);
                        this.g.stroke();
                        return false;
                    }
                }
            });
        });
    </script>
    <script type="text/javascript">
        /*script to showHide Div*/
        function closeDiv(id) {
            document.getElementById(id + 'Checkbox').className = 'menucheckbox-notchecked';
            document.getElementById(id).style.display = 'none';
            // do something
        }

        function showDiv(id) {
            if (document.getElementById(id).style.display != 'none') {
                document.getElementById(id + 'Checkbox').className = 'menucheckbox-notchecked';
                document.getElementById(id).style.display = 'none';
            }
            else {
                //if (document.getElementById(id + 'Checkbox').className == 'menucheckbox-checked') {
                document.getElementById(id).style.display = 'block';
                document.getElementById(id + 'Checkbox').className = 'menucheckbox-checked';

                // }
            }
        }
    </script>
    <script type="text/javascript">
       <%-- $(document).ready(function () {
            $("#rangeslider").kendoRangeSlider({
                min: 0,
                max: 10000,
                smallStep: 500,
                largeStep: 1000,
                change: sliderOnChange
            });
            $("#rangesliderImp").kendoRangeSlider({
                min: 0,
                max: 10,
                smallStep: 1,
                largeStep: 5,
                change: sliderOnChangeImp
            });

            var slider = $("#rangeslider").data("kendoRangeSlider");
            var sliderImp = $("#rangesliderImp").data("kendoRangeSlider");
            slider.setOptions({
                min: 0,
                max: 10000,
                smallStep: 500,
                largeStep: 1000,
                change: sliderOnChange,
            });
            sliderImp.setOptions({
                min: 0,
                max: 10,
                smallStep: 1,
                largeStep: 5,
                change: sliderOnChangeImp,
            });
            slider.value([0, 10000])
            slider.resize();

            sliderImp.value([0, 10])
            sliderImp.resize();
        });
        function sliderOnChange(e) {
            $("#<%=ddlMinimum.ClientID%>").val(e.value['0']);
            $("#<%=ddlMaximum.ClientID%>").val(e.value['1']);
            e.preventDefault();
        }
        function sliderOnChangeImp(e) {
            $("#<%=ddlMinimumImprisonment.ClientID%>").val(e.value['0']);
            $("#<%=ddlMaximumImprisonment.ClientID%>").val(e.value['1']);
            e.preventDefault();
        }--%>
    </script>
   
    <script type="text/javascript">

        function complianceSummaryDetail()
        {
            if ($('#ContentPlaceHolder1_ddlStatus').val() == 0)  //Statutary
            {
                $('#IFOverdueCompliance').attr('src', '../RLCS/Labour_ComplianceOverdueSummary.aspx');                
                $("#lnkOverdueCompliance").show();
                $("#lnkInternalOverdueCompliance").hide();
            }           
            return;
        }
        function fclosepopcompliance(dt) {
            $('#divreports').modal('hide');          
        }

        // replace/populate colors from user saved profile        
        var perFunctionChartColorScheme = {
            high: "<%=highcolor.Value %>",
            medium:"<%=mediumcolor.Value %>",
            low: "<%=lowcolor.Value %>",
            critical: "<%=criticalcolor.Value %>",

            NotApplicable: "<%=NotApplicableColor.Value %>",
            NotComplied: "<%=NotCompliedColor.Value %>",
            Rejected:"<%=RejectedColor.Value %>",
            Overdue: "<%=OverdueColor.Value %>",
            InProgress: "<%=InProgressColor.Value %>",
            Pendingforreview: "<%=PendingforreviewColor.Value %>",
            Compiledbutdocumentpending: "<%=CompiledbutdocumentpendingColor.Value %>",
            closetimely: "<%=closetimelyColor.Value %>",
            closedelay: "<%=closedelayColor.Value %>"
        };
        var perRiskStackedColumnChartColorScheme = {
            high: "<%=highcolor.Value %>",
            medium:"<%=mediumcolor.Value %>",
            low: "<%=lowcolor.Value %>",
            critical: "<%=criticalcolor.Value %>",
            
            NotApplicable: "<%=NotApplicableColor.Value %>",
            NotComplied: "<%=NotCompliedColor.Value %>",
            Rejected:"<%=RejectedColor.Value %>",
            Overdue: "<%=OverdueColor.Value %>",
            InProgress: "<%=InProgressColor.Value %>",
            Pendingforreview: "<%=PendingforreviewColor.Value %>",
            Compiledbutdocumentpending: "<%=CompiledbutdocumentpendingColor.Value %>",
            closetimely: "<%=closetimelyColor.Value %>",
            closedelay: "<%=closedelayColor.Value %>"
        };

        var perImprisonmentPenaltyStackedColumnChartColorScheme = {
            high: "<%=highcolor.Value %>",
            medium:"<%=mediumcolor.Value %>",
            low: "<%=lowcolor.Value %>",
            critical: "<%=criticalcolor.Value %>",
            
            NotApplicable: "<%=NotApplicableColor.Value %>",
            NotComplied: "<%=NotCompliedColor.Value %>",
            Rejected:"<%=RejectedColor.Value %>",
            Overdue: "<%=OverdueColor.Value %>",
            InProgress: "<%=InProgressColor.Value %>",
            Pendingforreview: "<%=PendingforreviewColor.Value %>",
            Compiledbutdocumentpending: "<%=CompiledbutdocumentpendingColor.Value %>",
            closetimely: "<%=closetimelyColor.Value %>",
            closedelay: "<%=closedelayColor.Value %>"
        };

        var perStatusChartColorScheme = {
            high: "<%=highcolor.Value %>",
            medium:"<%=mediumcolor.Value %>",
            low: "<%=lowcolor.Value %>",
            critical: "<%=criticalcolor.Value %>",

            NotApplicable: "<%=NotApplicableColor.Value %>",
            NotComplied: "<%=NotCompliedColor.Value %>",
            Rejected:"<%=RejectedColor.Value %>",
            Overdue: "<%=OverdueColor.Value %>",
            InProgress: "<%=InProgressColor.Value %>",
            Pendingforreview: "<%=PendingforreviewColor.Value %>",
            Compiledbutdocumentpending: "<%=CompiledbutdocumentpendingColor.Value %>",
            closetimely: "<%=closetimelyColor.Value %>",
            closedelay: "<%=closedelayColor.Value %>"
        };
         $(document).ready(function () {
            $(function () {
                // Chart Global options
                Highcharts.setOptions({
                    credits: {
                        text: '',
                        href: 'https://teamleaseregtech.com',
                    },
                    lang: {
                        drillUpText: "< Back",
                    },
                });

                //Process Wise Observation Status               


                <%=ProcessWiseObservationStatusChart%>

            });
        });
        // function executes when page is ready
        // main documentReady function starts
        $(function () {
            // Chart Global options
            Highcharts.setOptions({
                credits: {
                    text: '',
                    href: 'https://teamleaseregtech.com',
                },
                lang: {
                    drillUpText: "< Back",
                },
            });

            //perFunctionChart
            var perFunctionChart = Highcharts.chart('perFunctionChartDiv', {
                chart: {
                    type: 'column',
                    events: {
                        drilldown: function (e) {
                            this.setTitle({ text: e.point.name });
                            this.subtitle.update({ text: 'Click on graph to view documents' });
                        },
                        drillup: function () {
                            this.setTitle({ text: 'Per Function' });
                            this.subtitle.update({ text: 'Completion Status - Overall Functions' });
                        }
                    },
                },
                title: {
                    text: 'Per Function',
                    style: {
                        display: 'none'
                    },
                },
                subtitle: {
                    text: 'Completion Status - Overall Functions',
                    style: {
                        "font-family": 'Roboto',
                        fontWeight: '300',
                        fontSize:'15px'
                    }
                },
                xAxis: {
                    type: 'category',
                },
                yAxis: {
                    title: {
                        text: 'Number of Compliances'
                    },
                    //labels: {
                    //    enabled:false,
                    //},
                },
                plotOptions: {
                    series: {
                        dataLabels: {
                            enabled: true,
                            style: {
                                color: 'gray',
                            }
                        },
                    }
                },
                tooltip: {
                    hideDelay: 0,
                    backgroundColor: 'rgba(247,247,247,1)',
                    headerFormat: '<b>{point.key}</b><br/>',
                    pointFormat: '{series.name}: {point.y}'
                },              
             <%=perFunctionChart%>
            });            
            
            var perStatusPieChart = Highcharts.chart('perStatusPieChartDiv', {
                chart: {
                    type: 'pie',
                    events: {
                        drilldown: function (e) {
                            this.setTitle({ text: e.point.name });
                            // this.subtitle.update({ text: 'Click on graph to view documents' });
                            this.subtitle.update({ text: 'Completion Status - By Risk' });
                        },
                        drillup: function () {
                            this.setTitle({ text: 'Per Status' });
                            //this.subtitle.update({ text: 'Click on graph to drilldown' });
                            this.subtitle.update({ text: 'Completion Status' });
                        }
                    },
                },
                title: {
                    text: 'Per Status',
                    style: {
                        display: 'none'
                    },
                },
                subtitle: {
                    text: 'Completion Status - Overall',                   
                    style: {
                        fontWeight: '300',
                        fontSize:'15px'
                    }
                },
                xAxis: {
                    type: 'category',
                },
                plotOptions: {
                    series: {
                        dataLabels: {
                            enabled: true,
                            //format: '{y} <br>{point.name}',
                            format: '{y}',
                            distance: 5,
                        },
                        showInLegend: true,
                    },
                  
                },
                legend: {
                    itemDistance: 2,
                },
                tooltip: {
                    hideDelay: 0,
                    backgroundColor: 'rgba(247,247,247,1)',
                    formatter: function () {
                        if (this.series.name == 'Status')
                            return 'Click to Drilldown';	// text before drilldown
                        else
                            return 'Click to View Documents';		// text after drilldown
                    }
                },
                <%=perFunctionPieChart%>                
            });

            
          
            var perRiskStackedColumnChart = Highcharts.chart('perRiskStackedColumnChartDiv', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Per Risk',
                    style: {
                        // "font-family": 'Helvetica',
                        display: 'none'
                    },
                },
                subtitle: {
                    //text: 'Click on graph to view documents',
                    text: '',
                    style: {
                        // "font-family": 'Helvetica',
                    }
                },
                xAxis: {
                    categories: ['Critical','High', 'Medium', 'Low']
                },
                yAxis: {
                    title: {
                        text: 'Number of Compliances',                     
                    },
                    stackLabels: {
                        enabled: true,
                        style: {
                            fontWeight: 'bold',
                            color: 'gray'
                        }
                    }
                },
                tooltip: {
                    hideDelay: 0,
                    backgroundColor: 'rgba(247,247,247,1)',
                    headerFormat: '<b>{point.x}</b><br/>',
                    pointFormat: '{series.name}: {point.y}'
                },
                plotOptions: {
                    column: {
                        stacking: 'normal',
                        dataLabels: {
                            enabled: true,
                            color: 'white',
                            style:{
                              
                                textShadow:false,
                            }
                        },
                        cursor: 'pointer'
                    },
                },
              <%=perRiskChart%>
            });


           

            if($('div#perDepartmentStackedColumnChartDiv > div > svg > g > g > rect').length==0){$('div#DepartmentCriteria').hide();}
            
            // change color in color picker according to chart selected
            $('input[name=radioButton]').change(function () {
                // destroy all color pickers
                $('#highColorPicker').simplecolorpicker('destroy');
                $('#mediumColorPicker').simplecolorpicker('destroy');
                $('#lowColorPicker').simplecolorpicker('destroy');
                $('#criticalColorPicker').simplecolorpicker('destroy');
                // setting the colors for risks according to chart selected
                var chart = $('input[name=radioButton]:checked').val();
                switch (chart) {
                    case "perFunction":
                        $('#highColorPicker').val(perFunctionChartColorScheme.high);
                        $('#mediumColorPicker').val(perFunctionChartColorScheme.medium);
                        $('#lowColorPicker').val(perFunctionChartColorScheme.low);
                        $('#criticalColorPicker').val(perFunctionChartColorScheme.critical);
                        break;
                    case "perRisk":
                        $('#highColorPicker').val(perRiskStackedColumnChartColorScheme.high);
                        $('#mediumColorPicker').val(perRiskStackedColumnChartColorScheme.medium);
                        $('#lowColorPicker').val(perRiskStackedColumnChartColorScheme.low);
                        $('#criticalColorPicker').val(perRiskStackedColumnChartColorScheme.critical);
                        break;
                    case "perImprisonmentPenalty":
                        $('#highColorPicker').val(perImprisonmentPenaltyStackedColumnChartColorScheme.high);
                        $('#mediumColorPicker').val(perImprisonmentPenaltyStackedColumnChartColorScheme.medium);
                        $('#lowColorPicker').val(perImprisonmentPenaltyStackedColumnChartColorScheme.low);
                        $('#criticalColorPicker').val(perImprisonmentPenaltyStackedColumnChartColorScheme.critical);
                        break;
                    case "perStatus":
                        $('#highColorPicker').val(perStatusChartColorScheme.high);
                        $('#mediumColorPicker').val(perStatusChartColorScheme.medium);
                        $('#lowColorPicker').val(perStatusChartColorScheme.low);
                        $('#criticalColorPicker').val(perStatusChartColorScheme.critical);
                        break;
                    default:
                        $('#highColorPicker').val('#7CB5EC');
                        $('#mediumColorPicker').val('#434348');
                        $('#lowColorPicker').val('#90ED7D');
                        $('#criticalColorPicker').val('#a61c00');
                }

                // initialise the color piskers again
                $('#highColorPicker').simplecolorpicker({ picker: true });
                $('#mediumColorPicker').simplecolorpicker({ picker: true });
                $('#lowColorPicker').simplecolorpicker({ picker: true });
                $('#criticalColorPicker').simplecolorpicker({ picker: true });
            });

            // chart type selector radio buttons
            try	{
                $("#radioButtonGroup").buttonset();
                $(".chart-selector-radio-buttons").checkboxradio({
                    icon: false
                });
            }catch(e){}
            //apply color scheme to all charts [Apply to All] click event handler
            $('#applyToAllButton').click(function () {
                // value of colors selected at the time in color picker
                // $('#highColorPicker').val();
                // $('#mediumColorPicker').val();
                // $('#lowColorPicker').val();
            });

            $('#showModal').click(function () {
                $('#modalDiv').modal();
            });
            
        });   //	main documentReady function END
        var btnclosemgt=$('.modal-header').find('button');
        $(btnclosemgt).click(function () { 
            $('#showdetails').attr('src','blank.html');
        });
        function fpopulatedwidgetdata(type, attribute, customerid,fromdate,enddate, internalsatutory, widgetid,riskid) {
            $('#divreports').modal('show');                
            $('#showdetails').attr('width', '100%');
            $('#showdetails').attr('height', '800px');
            $('.modal-dialog').css('width', '98%');
            $('#showdetails').attr('src', "../Management/WidgetComplianceAPI.aspx?pointname="+type+"&attrubute=" + attribute + "&customerid=" + customerid + "&FromDate=" + fromdate + "&Enddate=" + enddate + "&Internalsatutory=" + internalsatutory + "&Widgetid=" + widgetid + "&RISKID="+riskid+"");                
        }
        function fpopulateddata(type,attribute,customerid,branchid,fromdate,enddate,filter,functionid,internalsatutory,chartname,listcategoryid,userid,isapprover,DisplayName)
        {  
            //alert("HI...");
            $('#divreports').modal('show');         
            if (internalsatutory == "Statutory") {
                $('#showdetails').attr('width','100%'); 
                $('#showdetails').attr('height','690px'); 
                $('.modal-dialog').css('width','98%');  
                $('#showdetails').attr('src', "../RLCS/Labour_SatutoryManagementAPI.aspx?pointname=" + type + "&attrubute=" + attribute + "&customerid=" + customerid + "&branchid=" + branchid + "&FromDate=" + fromdate + "&Enddate=" + enddate + "&Filter=" + filter + "&functionid=" + functionid + "&ChartName=" + chartname + "&Internalsatutory=" + internalsatutory+"&listcategoryid="+listcategoryid+"&Userid="+userid+"&IsApprover="+isapprover+"&DisplayName="+DisplayName);                
            }
                      
        }
        function PopulateGraphdata(isapprover)
        {            
            if(Displays() ==true)
            {
                $('#divreportsLocation').modal('show');       
                $('#showdetailsLocation').attr('width','100%'); 
                $('#showdetailsLocation').attr('height','466px'); 
                $('.modal-dialog').css('width','98%');  
                $('#showdetailsLocation').attr('src', "../Management/ManagementLocations.aspx?ctype="+ $('#ContentPlaceHolder1_ddlStatus').val()+"&IsApprover="+isapprover);
            }  
        }
        function fusers(customerid,branchid,internalsatutory,isapprover)
        {
            if(Displays() ==true)
            {                
                $('#divreports').modal('show');  
                $('#showdetails').attr('width','100%'); 
                $('#showdetails').attr('height','450px'); 
                $('.modal-dialog').css('width','99%');  
                $('#showdetails').attr('src', "../RLCS/Labour_ManagementusersAPI.aspx?customerid="+customerid+"&branchid="+branchid+"&Internalsatutory=" + internalsatutory+"&IsApprover="+isapprover);
            }
        }
        //function fEntity(internalsatutory,isapprover)
        //{
        //    if(Displays() ==true)
        //    {            
        //        $('#divreports').modal('show');  
        //        $('#showdetails').attr('width','98%'); 
        //        $('#showdetails').attr('height','650px'); 
        //       // $('.modal-dialog').css('width','98%');     
        //        $('#showdetails').attr('src', "../Management/ManageEntity.aspx?Internalsatutory=" + internalsatutory+"&IsApprover="+isapprover);
        //        //$('#showdetails').removeAttr("scrolling");
    
        //        //$('#divreports').modal('show');  
        //        //$('#showdetails').attr('width','100%'); 
        //        //$('#showdetails').attr('height','640px'); 
        //        //$('.modal-dialog').css('width','100%');     
        //        //$('#showdetails').attr('src', "../Management/ManageEntity.aspx?Internalsatutory=" + internalsatutory+"&IsApprover="+isapprover);
        //    }
                                  
        //}
        function fusersRahul()
        {            
            $('#divreports').modal('show');  
            $('#showdetails').attr('width','100%'); 
            $('#showdetails').attr('height','650px'); 
            $('.modal-dialog').css('width','99%');                                                  
        }
        function fFunctions(customerid,branchid,IsSatutoryInternal,isapprover)
        {
            if(Displays() ==true)
            {                 
                //$('#divreports').modal('show');      
                //$('#showdetails').attr('width','98%'); 
                //$('#showdetails').attr('height','550px'); 
                //$('.modal-dialog').css('width','75%');    
                //$('#showdetails').attr('src', "../Management/FunctionDetails.aspx?customerid="+customerid+"&branchid="+branchid+"&Internalsatutory="+IsSatutoryInternal+"&IsApprover="+isapprover);
          
                $('#divreports').modal('show');      
                $('#showdetails').attr('width','100%'); 
                $('#showdetails').attr('height','410px'); 
                $('.modal-dialog').css('width','100%');    
                $('#showdetails').attr('src', "../Management/categoryDetails.aspx?customerid="+customerid+"&branchid="+branchid+"&Internalsatutory="+IsSatutoryInternal+"&IsApprover="+isapprover);
            }
                                         
        }
        function fCompliancesRahul() {            
            $('#divreports').modal('show');
            $('#showdetails').attr('width','100%'); 
            $('#showdetails').attr('height','650px'); 
            $('.modal-dialog').css('width','100%');  
            return true;
           
        }
        function fCompliances(customerid,branchid,IsSatutoryInternal,isapprover)   ///Compliances Box
        {
            var IsAvantisFlag=true;
            if(Displays() ==true)
            {                
                $('#divreports').modal('show');    
                $('#showdetails').attr('width','100%'); 
                $('#showdetails').attr('height','650px'); 
                $('.modal-dialog').css('width','100%'); 
                $('#showdetails').attr('src', "../RLCS/Labour_ComplianceDetailsAPI.aspx?customerid="+customerid+"&branchid="+branchid+"&Internalsatutory="+IsSatutoryInternal+"&IsApprover="+isapprover);
                //$('#showdetails').attr('src', "../RLCS/ComplianceDetails.aspx?customerid="+customerid+"&branchid="+branchid+"&Internalsatutory="+IsSatutoryInternal+"&IsApprover="+isapprover+"&IsAvantisFlag="+IsAvantisFlag); //Old RLCS
               
            }                                         
        }

    </script>
    <script type="text/javascript">
        $(document).ready(function () {

          if($('#ContentPlaceHolder1_DivGraphProcessObsStatus').html().trim()==''){$('div#CustomWidgetCriteria').hide();}

            setactivemenu('leftdashboardmenu');
            fhead('Dashboard'); 
        });
    </script>
    <script type="text/javascript">       
        var dailyupadatepageno = 1;
        var Newslettersrowpageno = 1;
        function fdailyclick() { dailyupadatepageno += 1; binddailyupdatedata(); }
        function fnewsclick() { Newslettersrowpageno += 1; bindnewsdata(); }
        var dailyupdateJson;
        var NewsLetterJson;
        function bindnewsdata() {
            var k = 0;//Comment Not Access Control ///CORS
            //$.ajax({
            //    async: true,
            //    type: 'Get',                
            //    url: 'https://login.teamleaseregtech.com/apadr/Data/GetNewsletterData?page=' + Newslettersrowpageno,
            //    content: 'application/json;charset=utf-8',
            //    processData: true,
            //    headers: {
            //        "Authorization": "Bearer"
            //    },
            //    success: function (result) {
            //        NewsLetterJson = result;
            //        var step = 0;
            //        var str = '';
            //        for (step = 0; step < result.length; step++) {
            //            var objDate = new Date(result[step].NewsDate);
            //            str += '<li><img style="width:250px;height:166px" src="../NewsLetterImages/' + result[step].FileName + '" /><h3>' + result[step].Title + '</h3><p><a class="view-pdf" data-toggle="modal" style="cursor:pointer" onclick="viewpdf(this)" data-title="' + result[step].Title + '"  data-href="../NewsLetterImages/' + result[step].DocFileName + '"> Issue ' + getmonths(objDate) + ' ' + objDate.getFullYear() + '</a></p></li>'
            //        }
            //        $("#Newslettersrow").html(str);
            //        var ker = $('.bxnews').bxSlider({
            //            minSlides: 3,
            //            maxSlides: 3,
            //            slideWidth: 250,
            //            slideMargin: 50,
            //            moveSlides: 1,
            //            auto: false,
            //            nextCss: 'newsletternext',
            //            nextclickFunction:'fnewsclick()',
            //        });
            //    },
            //    error: function (e, t) { }
            //})
        }

        function binddailyupdatedata() 
        {

            var data = { Email: '<%=UserInformation%>' };
            var k = 0;
            $.ajax({
                async: true,
                type: 'post',
              
                url: "https://api.avantis.co.in/api/v2/legalupdates/params/",
                content: 'application/json;charset=utf-8',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Token 04f7a4e33a692196c39ef9ba54c53459c9a9b25b'
                },
                processData: true,
                body: JSON.stringify(data),
                success: function (result) {
                    //alert(result.data);
                    dailyupdateJson = result.data;
                    var step = 0;
                    var str = '';
                    for (step = 0; step < result.data.length; step++) {
                        var titledaily = '';
                        var Descriptiondaily = '';
                        if (result.data[step].title.length > 35) {
                            titledaily = result.data[step].title.substring(0, 35) + '...'
                        } else {
                            titledaily = result.data[step].title;
                        }
                        if (result.data[step].description.length > 150) {
                            if (result.data[step].description.indexOf('<div') == -1 || result.data[step].description.indexOf('<span') == -1) {
                                Descriptiondaily = result.data[step].description.substring(0, 150) + '...'
                            }
                        } else {
                            if (result.data[step].description.indexOf('<div') == -1 || result.data[step].description.indexOf('<span') == -1) {
                                Descriptiondaily = result.data[step].description;
                            }
                        }
                        var category = '';
                        if (result.data[step].Categories.length > 12) {
                            category = result.data[step].Categories.substring(0, 12) + '...'
                        } else {
                            category = result.data[step].Categories;
                        }

                        var objDate = new Date(result.data[step].date);
                        var GetDateDetail = getmonths(objDate) + ' ' + objDate.getDate() + ', ' + objDate.getFullYear();
                        
                        
                        str += '<div class="col-sm-6 col-md-6 col-lg-6 col-xl-6 c-col mb-2" style="padding-top: 12px;"><div class="box h-100 focusable" style="min-height: 270px!important;max-height:270px!important;"><ul class="tags"><li><a class="a-tag" title=' + result.data[step].Categories + ' hreflang="en" href="https://teamleaseregtech.com/legalupdates/?cat=' + result.data[step].Categories + '" target="_blank">' + category + '</a></li><li><a class="a-tag" title=' + result.data[step].Types + ' hreflang="en" href="https://teamleaseregtech.com/legalupdates/?state=' + result.data[step].Types + '" target="_blank">' + result.data[step].Types + '</a></li></ul><li><a href="https://teamleaseregtech.com/updates/article/' + result.data[step].id + '/update" target="_blank"><h4 class="card-text-sm"> ' + titledaily + ' </h4></a><a href="https://teamleaseregtech.com/updates/article/' + result.data[step].id + '/update" target="_blank"><p class="text-black-50">' + GetDateDetail + '</p><p>' + Descriptiondaily + '</p><p></a><br /><a data-toggle="modal" onclick="SetTrakforreadmore();" href="https://teamleaseregtech.com/updates/article/' + result.data[step].id + '/update" target="_blank">Read More</a></p></li></div></div>'
                        
                  
                    }
                    $("#DailyUpdatesrow").append(str);
                    var ite = $('.bxdaily').bxSlider({
                        minSlides: 3,
                        maxSlides: 3,
                        slideWidth: 300,
                        slideMargin: 0,
                        moveSlides: 1,
                        auto: false,
                        nextCss: 'dailyupdatenext',
                        nextclickFunction: 'fdailyclick()',
                    });
                },
                error: function (e, t) {
                    //alert('false');
                }
            });

            function SetTrakforreadmore() {
                settracknew('Management Dashboard', 'LegalUpdate', 'ViewMore', '');
            }
        }
            var newspop=true;
            var calpop=true;

            window.onscroll = function(){
                if(document.documentElement.scrollTop >400 && newspop==true){newspop=false;binddailyupdatedata();bindnewsdata();}
                if(document.documentElement.scrollTop >200 && calpop==true){
                    calpop=false;
                    $.get("/controls/calendarstandalone.aspx?m=8&type=1", function(data){
                        $(".responsive-calendar").html(data);
                        frendercalendar();
                    });   
                }
            }; 
            function fcal(dt) {
                try {                
                    var ddata = new Date(dt);
                    $('#clsdatel').html('Compliance items for date ' + formatDate(ddata));
                    
                } catch (e) { }

                //$('#imgcaldate').show();
                $('#idloadercal').show();
                var valtype=  $('#ContentPlaceHolder1_rdbcalender input:checked').val();
                $('#calframe').attr('src', '../RLCS/RLCS_MyCalendarData.aspx?m=8&date=' + dt+'&type='+valtype+'&isavantis=1');
               
                return;               
            }
            var Isprocessing = true;
  
            function fcallcalwithMonth(month, year) {
                var dat = month.split('-');
                var mn = parseInt(dat[1]) - 1;
                var yr = parseInt(dat[0]);
                if (mn - 1 < 0) {
                    mn = 12;
                    yr = yr - 1;
                }
                Isprocessing = false;
                $('.imgDynamic_cal').show();
                $.get("/controls/calendarstandalone.aspx?m=8&type=1&month=" + mn + "&year=" + yr, function (data) {

                    $(".responsive-calendar").html('');
                    $(".responsive-calendar").html(data);
                    Isprocessing = true;
                    frendercalendar();
                    $('.imgDynamic_cal').hide();
                });
            }
            function fcallcalwithMonth1(month, year) {
                debugger;
                var dat = month.split('-');
                var mn = parseInt(dat[1]) + 1;
                var yr = parseInt(dat[0]);
                if (mn >12) {
                    mn = 1;
                    yr = yr + 1;
                }
                Isprocessing = false;
                $('.imgDynamic_cal').show();
                $.get("/controls/calendarstandalone.aspx?m=8&type=1&month=" + mn + "&year=" + yr, function (data) {

                    $(".responsive-calendar").html('');
                    $(".responsive-calendar").html(data);
                    Isprocessing = true;
                    frendercalendar();
                    $('.imgDynamic_cal').hide();
                });
           
            }
            function timeconvert(ds) {
                var D, dtime, T, tz, off,
                dobj = ds.match(/(\d+)|([+-])|(\d{4})/g);
                T = parseInt(dobj[0]);
                tz = dobj[1];
                off = dobj[2];
                if (off) {
                    off = (parseInt(off.substring(0, 2), 10) * 3600000) +
                     (parseInt(off.substring(2), 10) * 60000);
                    if (tz == '-') off *= -1;
                }
                else off = 0;
                return new Date(T += off).toUTCString();
            }
            function getmonths(mon) {
                try {
                    return mon.toString().split(' ')[1];
                } catch (e) { }
            }
        
        function Binddailyupdatepopup(DID) {
            for (var i = 0; i < dailyupdateJson.length; i++) {
                if (DID == dailyupdateJson[i].ID) {
                    var objDate = new Date(timeconvert(dailyupdateJson[i].CreatedDate));
                    if (timeconvert(dailyupdateJson[i].CreatedDate) =="Invalid Date") {
                        objDate = new Date(dailyupdateJson[i].CreatedDate);
                    }
                    var mon_I = getmonths(objDate);
                    $("#dailyupdatedate").html('Daily Updates:' + mon_I + ' ' + objDate.getDate() + ', ' + objDate.getFullYear());
                    $("#dailyupdatedateInner").html('Daily Updates:' + mon_I + ' ' + objDate.getDate());
                    $("#contents").html(dailyupdateJson[i].Description);
                    $("#dailytitle").html(dailyupdateJson[i].Title);
                    break;
                }
            }

        }
        function BindNewsLetterpopup(DID) {
            for (var i = 0; i < NewsLetterJson.length; i++) {
                if (DID == NewsLetterJson[i].ID) {

                    $("#newsImg").attr('src', '../Images/' + NewsLetterJson[i].FileName);
                    $("#newsTitle").html(NewsLetterJson[i].Title);
                    $("#newsDesc").html(NewsLetterJson[i].Description);

                    break;
                }
            }

        }

        function forchild(hgt)
        {
            $('#showdetails').attr('height',10 + (hgt));
            $('#showdetails').css('height',10 + (hgt));
        }

       
        $("#High").spectrum({
            color:"<%=highcolor.Value %>",
            showInput: true,
            className: "full-spectrum",
            showInitial: true,
            showPalette: true,
            showSelectionPalette: true,
            maxSelectionSize: 10,
            preferredFormat: "hex",
            containerClassName: "highcls",
            move: function (color) {

            },
            show: function () {

            },
            beforeShow: function () {

            },
            hide: function () {

            },
            change: function () {
                 
            },
            palette: [
                ["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)",
                "rgb(204, 204, 204)", "rgb(217, 217, 217)", "rgb(255, 255, 255)"],
                ["rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)",
                "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"],
                ["rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)",
                "rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)",
                "rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)",
                "rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)",
                "rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)",
                "rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)",
                "rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)",
                "rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)",
                "rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)",
                "rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)"]
            ]
        });
            $("#medium").spectrum({
                color: "<%=mediumcolor.Value %>",
                showInput: true,
                className: "full-spectrum",
                showInitial: true,
                showPalette: true,
                showSelectionPalette: true,
                maxSelectionSize: 10,
                preferredFormat: "hex",
                containerClassName: "mediumcls",
                move: function (color) {

                },
                show: function () {

                },
                beforeShow: function () {

                },
                hide: function () {

                },
                change: function () {
            
                },
                palette: [
                    ["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)",
                    "rgb(204, 204, 204)", "rgb(217, 217, 217)", "rgb(255, 255, 255)"],
                    ["rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)",
                    "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"],
                    ["rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)",
                    "rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)",
                    "rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)",
                    "rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)",
                    "rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)",
                    "rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)",
                    "rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)",
                    "rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)",
                    "rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)",
                    "rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)"]
                ]
            });
            var lowchange = false;
            $("#low").spectrum({
                color:"<%=lowcolor.Value %>",
                showInput: true,
                className: "full-spectrum",
                showInitial: true,
                showPalette: true,
                showSelectionPalette: true,
                maxSelectionSize: 10,
                preferredFormat: "hex",
                containerClassName:"lowcls",
                move: function (color) {
              
                },
                show: function () {
            
                },
                beforeShow: function () {
              
                },
                hide: function () {
            
                },
                change: function () {
               
                },
                palette: [
                    ["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)",
                    "rgb(204, 204, 204)", "rgb(217, 217, 217)", "rgb(255, 255, 255)"],
                    ["rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)",
                    "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"],
                    ["rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)",
                    "rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)",
                    "rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)",
                    "rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)",
                    "rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)",
                    "rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)",
                    "rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)",
                    "rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)",
                    "rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)",
                    "rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)"]
                ]
            });

        var criticalchange = false;
            $("#critical").spectrum({
                color:"<%=criticalcolor.Value %>",
                showInput: true,
                className: "full-spectrum",
                showInitial: true,
                showPalette: true,
                showSelectionPalette: true,
                maxSelectionSize: 10,
                preferredFormat: "hex",
                containerClassName:"criticalcls",
                move: function (color) {
              
                },
                show: function () {
            
                },
                beforeShow: function () {
              
                },
                hide: function () {
            
                },
                change: function () {
               
                },
                palette: [
                    ["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)",
                    "rgb(204, 204, 204)", "rgb(217, 217, 217)", "rgb(255, 255, 255)"],
                    ["rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)",
                    "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"],
                    ["rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)",
                    "rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)",
                    "rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)",
                    "rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)",
                    "rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)",
                    "rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)",
                    "rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)",
                    "rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)",
                    "rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)",
                    "rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)"]
                ]
            });

            var mediumbtn = $(".mediumcls").find('button');
            $(mediumbtn).click(function () {
            
                var inputclr = $(".mediumcls").find('input.sp-input');
                if( $('#ContentPlaceHolder1_mediumcolor').val()!=$(inputclr).val()){
                    upcolor();
                    $('#ContentPlaceHolder1_mediumcolor').val($(inputclr).val());
                    document.getElementById("<%=btnTopSearch.ClientID %>").click();
                }
            });
            var highbtn = $(".highcls").find('button');
            $(highbtn).click(function () {
                var inputclr1 = $(".highcls").find('input.sp-input');
          
         
                if( $('#ContentPlaceHolder1_highcolor').val()!=$(inputclr1).val()){
                    $('#ContentPlaceHolder1_highcolor').val($(inputclr1).val());
                    upcolor();
                    document.getElementById("<%=btnTopSearch.ClientID %>").click();
                }
      
          
            });
            var lowbtn=$(".lowcls").find('button');
            $(lowbtn).click(function () {
         
                var inputclr2 = $(".lowcls").find('input.sp-input');
        
                if($('#ContentPlaceHolder1_lowcolor').val()!=$(inputclr2).val())
                {   
                    $('#ContentPlaceHolder1_lowcolor').val($(inputclr2).val());
                    upcolor();
                    document.getElementById("<%=btnTopSearch.ClientID %>").click();
                }
           
            });
        var criticalbtn=$(".criticalcls").find('button');
        $(criticalbtn).click(function () {
         
                var inputclr3 = $(".criticalcls").find('input.sp-input');
        
                if($('#ContentPlaceHolder1_criticalcolor').val()!=$(inputclr3).val())
                {   
                    $('#ContentPlaceHolder1_criticalcolor').val($(inputclr3).val());
                    upcolor();
                    document.getElementById("<%=btnTopSearch.ClientID %>").click();
                }
           
            });
            function upcolor()
            {
            
                var k = 0;
                $.ajax({
                    async: true,
                    type: 'Post',
                    url: '/dailyupdateservice.svc/upcolor',                       
                    data: JSON.stringify({"high": $('#ContentPlaceHolder1_highcolor').val(),"sender": <%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID %>, "medium":  $('#ContentPlaceHolder1_mediumcolor').val(), "low":  $('#ContentPlaceHolder1_lowcolor').val(), "critical":  $('#ContentPlaceHolder1_criticalcolor').val()}),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    processData: true,
                    success: function (result) {
                    
                    },
                    error: function (e, t) { }
                });
            }
    
            function initializeJQueryUI(textBoxID, divID) {
                $("#" + textBoxID).unbind('click');

                $("#" + textBoxID).click(function () {
                    $("#" + divID).toggle("blind", null, 500, function () { });
                });
            }
         
 
            (function(a){a.createModal=function(b){defaults={title:"",message:"Your Message Goes Here!",closeButton:true,scrollable:false};var b=a.extend({},defaults,b);var c=(b.scrollable===true)?'style="max-height: 420px;overflow-y: auto;"':"";html='<div class="modal fade" id="myModal">';html+='<div class="modal-dialog"  style="width:1000px;">';html+='<div class="modal-content" style="width:1100px;">';html+='<div class="modal-header">';html+='<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>';if(b.title.length>0){html+='<h4 class="modal-title">'+b.title+"</h4>"}html+="</div>";html+='<div class="modal-body" '+c+">";html+=b.message;html+="</div>";html+='<div class="modal-footer">';if(b.closeButton===true){html+='<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>'}html+="</div>";html+="</div>";html+="</div>";html+="</div>";a("body").prepend(html);a("#myModal").modal().on("hidden.bs.modal",function(){a(this).remove()})}})(jQuery);
    
            /*
            * Here is how you use it
            */
 
            function viewpdf(obj)  {
                settracknew('Management Dashboard', 'Newsletter', 'Detailedview', '');	
                var pdf_link = $(obj).attr('data-href');
                var pdf_title = $(obj).attr('data-title');          
                var iframe = '<object type="application/pdf" data="' + pdf_link + '"#toolbar=1&navpanes=0&statusbar=0&messages=0 width="100%" height="500">No Support</object>'
                $.createModal({
                    title: pdf_title,
                    message: iframe,
                    closeButton:true,
                    scrollable: false
                });
                return false;        
            } 

            function openNotificationModal() {            
		try{
              	 	 if (sessionStorage.getItem("Notify") == null && $('#Notecount').text()!="0") {
                  	  $('#divNotification').modal('show');
                  	  SetNotifyKey();
                  	  return true;
                	}
		}catch(e){}
            
            }       

            function BindNewNotifications() {
                var k = 0;
               <%-- $.ajax({
                    async: true,
                    type: 'Get',
                    url: '/dailyupdateservice.svc/BindNotifications?userid=<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID %>',
                    content: 'application/json;charset=utf-8',
                    processData: true,
                    success: function (result) { 
              
                        var step = 0;
                        var str = '';
                        for (step = 0; step < result.length; step++) {
                        
                            str += '<li><p><a href="../DailyUpdates/Notification.aspx">' + result[step] + '</a></p></li>'
                        }
                        $("#divNotificationData").html(str);
                   	if(result.length==0){   $('#divNotification').modal('hide');}
                    },
                    error: function (e, t) { }
               
                })       --%>             
            
            }
            $(document).ready(function () {            
                //    $.get("/controls/calendarstandalone.aspx?m=8&type=0", function(data){
                //         $(".responsive-calendar").html(data);
                //     });        //Commented 31 Dec 2019
                setInterval(setcolor, 1000);
            });
            var strop=0;
            $('#ContentPlaceHolder1_rdbcalender').change(function () {
                //$(this).click(function () {
                var val=  $('#ContentPlaceHolder1_rdbcalender input:checked').val()
                if(strop==0)                        
                    fcallcal(val);
                //});
            });
            function fcallcal(val){                
                strop=1
                $(".responsive-calendar").html('<img src="../images/loader.gif">');
                $.get("/controls/calendarstandalone.aspx?m=8&type="+val, function(data){
                    $(".responsive-calendar").html(data);
                   frendercalendar();
                    strop=0;
                });     
            }

            function setcolor() {
                $('.overdue').closest('div').find('a').css('background-color', '#FF0000');
                $('.complete').closest('div').find('a').css('background-color', '#006500');
                $('.pending').closest('div').find('a').css('background-color', '#00008d');
                $('.delayed').closest('div').find('a').css('background-color', '#ffcd70');
                
            }
            $(document).ready(function (ClassName) {
                $("a").addClass(ClassName);
                
            });
            function formatDate(date) {
                var monthNames = [
                  "January", "February", "March",
                  "April", "May", "June", "July",
                  "August", "September", "October",
                  "November", "December"
                ];
                var day = date.getDate();
                var monthIndex = date.getMonth();
                var year = date.getFullYear();
                return day + ' ' + monthNames[monthIndex] + ' ' + year;
            }
            function fclosepopcal(dt) {
                $('#divreports').modal('hide');
                fcal(dt)
            }
            function hideloader()
            {
                $('#imgcaldate').hide();
            }
        //function fcal(dt)    //Remove
        //{
        //        settracknew('Management Dashboard', 'Calendar', 'Date Selection', '');		

        //        try {                
        //            var ddata = new Date(dt);
        //            $('#clsdatel').html('Compliance items for date ' + formatDate(ddata));
        //        } catch (e) { }

        //        $('#imgcaldate').show();
        //        var valtype=  $('#ContentPlaceHolder1_rdbcalender input:checked').val()
        //        $('#calframe').attr('src', '/controls/calendardataAPI.aspx?m=8&date=' + dt+'&type='+valtype)
        //        return;               
        //    }
            function OpenOverViewpup(scheduledonid, instanceid, CType) {
                if (CType == 'Statutory' || CType == 'Statutory CheckList' || CType == 'Event Based') {
                    $('#divOverView').modal('show');
                    $('#OverViews').attr('width', '100%');
                    $('#OverViews').attr('height', '600px');
                    $('.modal-dialog').css('width', '98%');
                    $('#OverViews').attr('src', "../Common/ComplianceOverview.aspx?ComplianceScheduleID=" + scheduledonid + "&ComplainceInstatnceID=" + instanceid);
                }
            }
            function OpenPerrevpopup(scheduledonid, instanceid, Interimdays, CType, RoleID, dtdate) {
                $('#divreports').modal('show');
                $('#showdetails').attr('width', '100%');
                $('#showdetails').attr('height', '650px');
                $('.modal-dialog').css('width', '85%');
                $('#showdetails').attr('src', "../Compliances/CalenderPopup.aspx?scheduleonId=" + scheduledonid + "&InstanceId=" + instanceid + "&Interimdays=" + Interimdays + "&CType=" + CType + "&RoleID=" + RoleID + "&dt=" + dtdate);
            }
            function OpenOverdueComplainceList(e){ 
                //alert("Show all")
                //settracknew('Management Dashboard', 'OvedueSummary', 'ViewMore', '');	
                $('#divreports').modal('show');    
                $('#showdetails').attr('width','100%'); 
                $('#showdetails').attr('height','650px'); 
                $('.modal-dialog').css('width','98%');  
                //$('#showdetails').attr('src', "../RLCS/HRPlus_MyCompliances.aspx"); //Old Page Working
                $('#showdetails').attr('src', "../RLCS/Labour_OverdueComplianceAPI.aspx");
                e.preventDefault();
            }
            
            function SetNotifyKey() {
                sessionStorage.setItem("Notify", "1");
            }
            BindNewNotifications();
            $("#DivFilters").click(function (event) {
 
                if (event.target.id == "") {
                    var idvid = $(event.target).closest('div');
                    if ($(idvid).attr('id') != undefined && $(idvid).attr('id') != '' && $(idvid).attr('id').indexOf('tvFilterLocation') > -1) {
                        $("#divFilterLocation").show();
                    } else {
                        $("#divFilterLocation").hide();
                    }
                }
                else if (event.target.id != "ContentPlaceHolder1_tbxFilterLocation") {
                    $("#divFilterLocation").hide();
                } else if (event.target.id != "" && event.target.id.indexOf('tvFilterLocation') > -1) {
                    $("#divFilterLocation").show();
                } else if (event.target.id == "ContentPlaceHolder1_tbxFilterLocation") {
                    $("#ContentPlaceHolder1_tbxFilterLocation").unbind('click');

                    $("#ContentPlaceHolder1_tbxFilterLocation").click(function () {
                        $("#divFilterLocation").toggle("blind", null, 500, function () { });
                    });

                }
            });

            
    </script>
    <script type="text/javascript">
               
        function fchangeheight(obj,cal)
        {
            $('iframe#showdetails').removeAttr('height');
            $('iframe#showdetails').attr('height',cal)
        }
        function loaderhide(){
           
            $('#idloadercal').hide();
           
        }
    </script>
     
</asp:Content>
