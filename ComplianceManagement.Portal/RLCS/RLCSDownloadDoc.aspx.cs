﻿using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using static com.VirtuosoITech.ComplianceManagement.Business.RLCS.RLCSAPIClasses;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class RLCSDownloadDoc : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString["RLCSIds"]))
                {
                    string[] commandArg = Request.QueryString["RLCSIds"].ToString().Split(',');

                    string IsFlag = Convert.ToString(Request.QueryString["IsFlag"]);

                    if (IsFlag.Equals("Register"))
                    {
                        if (commandArg.Length > 0)
                        {
                            bool _objRegData = GetRegisterDocuments(commandArg);
                            if (!_objRegData)
                            {
                                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('No Document to Download')", true);
                            }
                        }
                    }
                    else if (IsFlag.Equals("Return"))
                    {
                        if (commandArg.Length > 0)
                        {
                            bool _objDocreturns = GetMyDocuments_ReturnDocuments(commandArg);
                            if (!_objDocreturns)
                            {
                                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('No Document available to download')", true);
                            }
                        }
                    }
                    else if (IsFlag.Equals("Challan"))
                    {
                        if (commandArg.Length > 0)
                        {
                            bool _objDocreturns = GetChallanDocuments(commandArg);
                            if (!_objDocreturns)
                            {
                                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('No Document available to download')", true);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        
        private bool GetChallanDocuments(string[] commandArg)
        {
            try
            {
                List<long> lstScheduleOnMasterIDs = new List<long>();
                List<Tuple<string, List<long>>> lstScheduleOnIDsWithBranchName = new List<Tuple<string, List<long>>>();

                int folderCount = 0;

                foreach (var item in commandArg)
                {
                    folderCount++;

                    string[] splitDetails = item.ToString().Split('-');

                    long recordID = Convert.ToInt64(splitDetails[0]);
                    string ScopeID = Convert.ToString(splitDetails[1]);
                    string State = Convert.ToString(splitDetails[2]);
                    string Month = Convert.ToString(splitDetails[3]);
                    string Year = Convert.ToString(splitDetails[4]);
                    string challanType = Convert.ToString(splitDetails[5]);

                    if (recordID > 0)
                    {
                        string branchName = RLCSManagement.GetRLCSBranchNameByRecordID(recordID);

                        if (String.IsNullOrEmpty(branchName))
                            branchName = "Document" + folderCount;
                                                
                        List<long> lstScheduleOnIDs = RLCSManagement.GetScheduleOnIDsByRecordID(recordID);

                        if (lstScheduleOnIDs.Count > 0)
                            lstScheduleOnIDsWithBranchName.Add(new Tuple<string, List<long>>(branchName, lstScheduleOnIDs));
                    }

                    #region Prev Code
                    //if (!string.IsNullOrEmpty(challanType) && !string.IsNullOrEmpty(State) && !string.IsNullOrEmpty(Month) && !string.IsNullOrEmpty(Year))
                    //{

                    //    if (challanType.Trim().ToUpper().Equals("EPF") || challanType.Trim().ToUpper().Equals("PF"))
                    //        challanType = "EPF";
                    //    else if (challanType.Trim().ToUpper().Equals("ESI") || challanType.Trim().ToUpper().Equals("LWF"))
                    //        challanType = challanType.Trim().ToUpper();

                    //    List<long> lstComplianceIDs = RLCSManagement.GetAll_ChallanRelatedCompliance(challanType);
                    //    if (lstComplianceIDs != null)
                    //    {
                    //        if (lstComplianceIDs.Count > 0)
                    //        {
                    //            var lstCustomerBranches = RLCSManagement.GetCustomerBranchesByPFOrESICCode(challanType, State, "B");
                    //            foreach (var eachCustBranch in lstCustomerBranches)
                    //            {
                    //                List<long> lstScheduleOnIDs = RLCSManagement.GetAll_Registers_ScheduleOnIDs(lstComplianceIDs, Convert.ToInt32(eachCustBranch.AVACOM_BranchID), Year, Month);
                    //                lstComplianceIDMasters = lstComplianceIDMasters.Union(lstScheduleOnIDs).ToList();
                    //            }
                    //        }
                    //    }
                    //}
                    #endregion
                }

                if (lstScheduleOnIDsWithBranchName.Count > 0)
                {
                    bool _objDocument_ChlDownloadval = RLCS_DocumentManagement.GetChallanDocuments_RLCS_WithBranchName("Challan", lstScheduleOnIDsWithBranchName);
                    return _objDocument_ChlDownloadval;
                }
                else
                {
                    return false;
                }

                #region Prev Code
                //if (lstComplianceIDMasters.Count > 0)
                //{
                //    bool _objDocument_ChlDownloadval = RLCS_DocumentManagment.GetRLCSDocuments("Challan", lstComplianceIDMasters);
                //    return _objDocument_ChlDownloadval;
                //}
                //else
                //{
                //    return false;
                //}
                #endregion
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        private bool GetMyDocuments_ReturnDocuments(string[] commandArg)//string state, string taskID, string month, string year)
        {
            List<long> lstScheduleOnIDs = new List<long>();
            List<long> lstScheduleOnIDsToDownload = new List<long>();
            List<long> lstComplianceIDs = new List<long>();

            foreach (var item in commandArg)
            {
                string[] splitDetails = item.ToString().Split('-');

                string taskID = Convert.ToString(splitDetails[0]);
                string state = Convert.ToString(splitDetails[1]);                
                string Location = Convert.ToString(splitDetails[2]);
                int CustId = Convert.ToInt32(splitDetails[3]);
                string Month = Convert.ToString(splitDetails[4]);
                string Year = Convert.ToString(splitDetails[5]);

                if (!string.IsNullOrEmpty(taskID) && !string.IsNullOrEmpty(state) && !string.IsNullOrEmpty(Month) && !string.IsNullOrEmpty(Year))
                {
                    var lstReturn_Compliances = RLCSManagement.GetAll_ReturnsRelatedCompliance_SectionWise(state, taskID);

                    if (lstReturn_Compliances != null)
                    {
                        if (lstReturn_Compliances.Count > 0)
                        {
                            lstComplianceIDs = lstReturn_Compliances.Select(row => (long)row.AVACOM_ComplianceID).ToList();

                            List<MappedCompliance> lstComplianceToProcess = new List<MappedCompliance>();

                            lstReturn_Compliances.ForEach(eachReturnMappedCompliance =>
                            {
                                //lstComplianceToProcess.Clear();
                                //lstComplianceToProcess.Add(eachReturnMappedCompliance); //Needs to find ActCode of Each Compliance that

                                if (eachReturnMappedCompliance.ActCode.Trim().ToUpper().Equals("EPF") || eachReturnMappedCompliance.ActCode.Trim().ToUpper().Equals("ESI"))
                                {
                                    //Map Compliance to Code 
                                    //Get All CustomerBranch With Code and Map compliance to it

                                    string type = string.Empty;
                                    if (eachReturnMappedCompliance.ActCode.Trim().ToUpper().Equals("EPF"))
                                        type = "EPF";
                                    else if (eachReturnMappedCompliance.ActCode.Trim().ToUpper().Equals("ESI"))
                                        type = "ESI";

                                    var lstCustomerBranches = RLCSManagement.GetCustomerBranchesByPFOrESICCode(type, state, "B"); //LM_State--CodeValue                                            

                                    if (lstCustomerBranches != null)
                                    {
                                        bool getSchdules = false;

                                        if (lstCustomerBranches.Count > 0)
                                        {
                                            lstCustomerBranches.ForEach(eachCustomerBranch =>
                                            {
                                                if (eachCustomerBranch != null)
                                                {
                                                    if (eachCustomerBranch.AVACOM_BranchID != null)
                                                    {
                                                        if (!getSchdules)
                                                        {
                                                            lstScheduleOnIDs = RLCSManagement.GetAll_Registers_ScheduleOnIDs(lstComplianceIDs, Convert.ToInt32(eachCustomerBranch.AVACOM_BranchID), Year, Month);

                                                            if (lstScheduleOnIDs.Count > 0)
                                                            {
                                                                lstScheduleOnIDsToDownload=lstScheduleOnIDsToDownload.Union(lstScheduleOnIDs).ToList();
                                                                getSchdules = true;
                                                            }
                                                        }
                                                    }
                                                }
                                            });
                                        }
                                    }
                                }
                                else if (eachReturnMappedCompliance.ActCode.Trim().ToUpper().Equals("PT") || eachReturnMappedCompliance.ActCode.Trim().ToUpper().Equals("LWF"))
                                {
                                    //Map Compliance to State                                               
                                    var customerBranchDetails = RLCSManagement.GetCustomerBranchByStateCode(CustId, state, "S");

                                    if (customerBranchDetails != null)
                                    {
                                        if (customerBranchDetails.AVACOM_BranchID != null)
                                        {
                                            lstScheduleOnIDs = RLCSManagement.GetAll_Registers_ScheduleOnIDs(lstComplianceIDs, Convert.ToInt32(customerBranchDetails.AVACOM_BranchID), Year, Month);

                                            if (lstScheduleOnIDs.Count > 0)
                                            {
                                                lstScheduleOnIDsToDownload=lstScheduleOnIDsToDownload.Union(lstScheduleOnIDs).ToList();
                                            }
                                        }
                                    }
                                }
                            });
                        }
                    }
                }
            }
            if (lstScheduleOnIDsToDownload.Count > 0)
            {
                return RLCS_DocumentManagement.GetRLCSDocuments("Return", lstScheduleOnIDsToDownload);
                //return true;
            }
            else
            {
                return false;
            }
        }
        
        private bool GetRegisterDocuments(string[] commandArg)
        {
            try
            {
                //List<long> lstScheduleOnIDs = new List<long>();

                List<long> lstScheduleOnMasterIDs = new List<long>();
                List<Tuple<int, List<long>>> lstScheduleOnIDsWithBranchID = new List<Tuple<int, List<long>>>();
                
                foreach (var item in commandArg)
                {
                    string[] splitDetails = item.ToString().Split('-');

                    int customerBranchID = Convert.ToInt32(splitDetails[0]);
                    string establishmentType = Convert.ToString(splitDetails[1]);
                    string state = Convert.ToString(splitDetails[2]);
                    string month = Convert.ToString(splitDetails[3]);
                    string year = Convert.ToString(splitDetails[4]);
                    long recordID = Convert.ToInt64(splitDetails[5]);

                    if (recordID > 0)
                    {
                        //lstScheduleOnIDs.Clear();
                        List<long> lstScheduleOnIDs = RLCSManagement.GetScheduleOnIDsByRecordID(recordID);

                        if (customerBranchID > 0 && lstScheduleOnIDs.Count > 0)
                            lstScheduleOnIDsWithBranchID.Add(new Tuple<int, List<long>>(customerBranchID, lstScheduleOnIDs));
                    }
                    else
                    {
                        List<long> lstComplianceIDs = RLCSManagement.GetAll_Registers_Compliance_StateWise(state, establishmentType);
                        if (lstComplianceIDs.Count > 0 && lstComplianceIDs != null)
                        {
                            lstScheduleOnMasterIDs = RLCSManagement.GetAll_Registers_ScheduleOnIDs(lstComplianceIDs, customerBranchID, year, month);
                        }
                        if (lstScheduleOnMasterIDs.Count > 0)
                        {
                            lstScheduleOnIDsWithBranchID.Add(new Tuple<int, List<long>>(customerBranchID, lstScheduleOnMasterIDs));
                        }
                    }

                    #region Prev Code
                    //if (customerBranchID > 0 && !string.IsNullOrEmpty(establishmentType) && !string.IsNullOrEmpty(state) && !string.IsNullOrEmpty(month) && !string.IsNullOrEmpty(year))
                    //{
                    //    List<long> lstComplianceIDs = RLCSManagement.GetAll_Registers_Compliance_StateWise(state, establishmentType);

                    //    if (lstComplianceIDs.Count > 0 && lstComplianceIDs != null)
                    //    {
                    //        lstScheduleOnIDs = RLCSManagement.GetAll_Registers_ScheduleOnIDs(lstComplianceIDs, customerBranchID, year, month);
                    //    }
                    //    if (lstScheduleOnIDs.Count > 0)
                    //    {
                    //        lstScheduleOnMasterIDs = lstScheduleOnMasterIDs.Union(lstScheduleOnIDs).ToList();
                    //    }
                    //}
                    #endregion
                }
                
                if (lstScheduleOnIDsWithBranchID.Count > 0)
                {    
                    bool _objDocument_RegDownloadval = RLCS_DocumentManagement.GetRegisterDocuments_RLCS_WithBranchName("Register", lstScheduleOnIDsWithBranchID);
                    if (!_objDocument_RegDownloadval)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                
                else
                {
                    return false;
                }               

                #region Prev Code
                //if (lstScheduleOnMasterIDs.Count > 0)
                //{                    
                //    bool _objDocument_RegDownloadval = RLCS_DocumentManagment.GetRLCSDocuments("Register", lstScheduleOnMasterIDs);                   
                //    if (!_objDocument_RegDownloadval)
                //    {
                //        return false;
                //    }
                //    else
                //    {
                //        return true;
                //    }
                //}
                //else
                //{
                //    return false;
                //}
                #endregion
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

    }
}