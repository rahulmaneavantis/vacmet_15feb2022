﻿using System;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using System.Configuration;
using com.VirtuosoITech.ComplianceManagement.Business.Contract;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class RLCS_MyDocuments_Challans : System.Web.UI.Page
    {
        protected int CustId;
        protected int UserId;
        protected static string Path;
        protected static int newRegistersMonth;
        protected static int newRegistersYear;
        protected static string ProfileID;

        protected void Page_Load(object sender, EventArgs e)
        {
            Path = ConfigurationManager.AppSettings["AVACOM_RLCS_API_URL"];
            CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            UserId = Convert.ToInt32(AuthenticationHelper.UserID);
            ProfileID = Convert.ToString(AuthenticationHelper.ProfileID);

            newRegistersMonth = Convert.ToInt32(ConfigurationSettings.AppSettings["RLCS_RegisterChallan_Month"]);
            newRegistersYear = Convert.ToInt32(ConfigurationSettings.AppSettings["RLCS_RegisterChallan_Year"]);

            if (!string.IsNullOrEmpty(Request.QueryString["ScopeID"]) && !string.IsNullOrEmpty(Request.QueryString["Code"])  && !string.IsNullOrEmpty(Request.QueryString["Month"]) && !string.IsNullOrEmpty(Request.QueryString["Year"]) && !string.IsNullOrEmpty(Request.QueryString["ScheduleList"]))
            {
                string ScopeID = Request.QueryString["ScopeID"].ToString();
                string Code = Request.QueryString["Code"].ToString();
                string Month = Request.QueryString["Month"];
                string Year = Request.QueryString["Year"];
                string ScheduleList = Request.QueryString["ScheduleList"];
              
                string FileType = "W"; // Working File Flag
                if (!string.IsNullOrEmpty(Request.QueryString["FileType"]))
                {
                    FileType = Request.QueryString["FileType"];
                }
                if (!String.IsNullOrEmpty(ScheduleList))
                {
                    List<long> lstScheduleOnIDsToDownload = new List<long>();
                    lstScheduleOnIDsToDownload = ScheduleList.Split(',').Select(long.Parse).ToList();
                    //GetRLCSDocumentsByFileTypeId
                    // Working FileTypeId --> 2
                    int FileTypeId = 2;
                    if (FileType != null)
                    {
                        if (FileType == "C")
                        {
                            FileTypeId = 1;
                        }
                        if (FileType == "W")
                        {
                            FileTypeId = 2;
                        }
                    }
                    RLCS_DocumentManagement.GetRLCSDocumentsByFileTypeIdAndCode("Challan", lstScheduleOnIDsToDownload, FileTypeId, Code);
                    //RLCS_DocumentManagement.GetRLCSDocuments("Return", lstScheduleOnIDsToDownload);
                }
                //if (challanType != null)
                //{
                //    if (!string.IsNullOrEmpty(challanType))
                //    {
                //        bool _objChlDownload = GetChallanDocuments(State, Month, Year, challanType, FileTypeId, clientID, recordID, State);
                //        if (!_objChlDownload)
                //        {
                //            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('No Document Available to Download')", true);
                //        }
                //    }
                //}
            }
        }

        private bool GetChallanDocuments(string state, string month, string year, string challanType, int FileTypeId, string clientID, long recordID, string StateCode)
        {
            try
            {
                if (challanType.Trim().ToUpper().Equals("EPF") || challanType.Trim().ToUpper().Equals("PF"))
                    challanType = "EPF";
                else if (challanType.Trim().ToUpper().Equals("ESI") || challanType.Trim().ToUpper().Equals("LWF") || challanType.Trim().ToUpper().Equals("PT"))
                    challanType = challanType.Trim().ToUpper();

                List<long> lstComplianceIDs = new List<long>();
                List <RLCS_CustomerBranch_ClientsLocation_Mapping> lstCustomerBranches = new List<RLCS_CustomerBranch_ClientsLocation_Mapping>();

                //var lstCustomerBranches = RLCSManagement.GetCustomerBranchesByPFOrESICCode(challanType, state, "B");

                if (challanType.Trim().ToUpper().Equals("EPF") || challanType.Trim().ToUpper().Equals("ESI"))
                {
                    lstComplianceIDs = RLCSManagement.GetAll_ChallanRelatedCompliance(challanType);
                    
                    lstCustomerBranches = RLCSManagement.GetCustomerBranchesByRecordID(recordID, "B");
                    
                    //if (challanType.Equals("EPF"))
                    //    challanType = "PF";
                    //else if (challanType.Equals("ESI"))
                    //    challanType = "ESIC";
                    //lstCustomerBranches = RLCSManagement.GetCustomerBranchesByPFOrESICOrPTOrLWF_API(Convert.ToInt32(AuthenticationHelper.CustomerID), clientID, challanType, state, "B");
                    //lstCustomerBranches = RLCSManagement.GetCustomerBranchesByPFOrESICCode(clientID, challanType, state, "B");                   
                }
                else if (challanType.Trim().Trim().ToUpper().Equals("PT"))
                {
                    lstCustomerBranches = RLCSManagement.GetCustomerBranchesByRecordID(recordID, "B");

                    //lstCustomerBranches = RLCSManagement.GetCustomerBranchesByPFOrESICOrPTOrLWF_API(Convert.ToInt32(AuthenticationHelper.CustomerID), clientID, challanType, state, "B");
                    //lstCustomerBranches = RLCSManagement.GetCustomerBranchesByPTState(Convert.ToInt32(AuthenticationHelper.CustomerID), clientID, state, "B");

                    lstComplianceIDs = RLCSManagement.GetAll_ChallanRelatedCompliance(state, challanType);
                }

                if (lstComplianceIDs != null && lstCustomerBranches != null)
                {
                    if (lstComplianceIDs.Count > 0 && lstCustomerBranches.Count > 0)
                    {
                        bool getSchdules = false;
                        foreach (var eachCustBranch in lstCustomerBranches)
                        {
                            if (!getSchdules)
                            {
                                List<long> lstScheduleOnIDs = RLCSManagement.GetAll_Registers_ScheduleOnIDs(lstComplianceIDs, Convert.ToInt32(eachCustBranch.AVACOM_BranchID), year, month);

                                if (lstScheduleOnIDs.Count > 0)
                                {
                                    bool _objDocument_ChlDownloadval = RLCS_DocumentManagement.GetRLCSDocumentsByFileTypeId("Challan", lstScheduleOnIDs, FileTypeId, StateCode);

                                    if (_objDocument_ChlDownloadval)
                                    {
                                        return true;
                                    }
                                    else
                                    {
                                        return false;
                                    }
                                }
                                else
                                {
                                    return false;
                                }
                            }
                        }

                        return true;
                    }

                    return false;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }
    }
}