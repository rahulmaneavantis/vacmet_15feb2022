﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Globalization;
using System.Configuration;
using System.Web.Security;
using Newtonsoft.Json;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;


namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class HRPlusDashboard : System.Web.UI.Page
    {
        protected static DateTime FromFinancialYearSummery;
        protected static DateTime ToFinancialYearSummery;
        protected string Performername;
        protected string Reviewername;
        protected string InternalPerformername;
        public static string CalendarDate;
        public static DateTime CalendarTodayOrNextDate;
        public static string date = "";
        protected static string CalenderDateString;
        protected static string perFunctionChart;
        protected static string perFunctionPieChart;
        protected static string perPenaltyStatusPieChart;
        protected static string perRiskChart;
        protected static string perFunctionHIGHPenalty;
        protected static string perFunctionMEDIUMPenalty;
        protected static string perFunctionLOWPenalty;
        protected static int customerID;
        protected static string RegulatoryAuthKey;
        protected static int BID;
        protected static string IsSatutoryInternal;
        protected static string PieMonth;
        protected static bool IsApprover = false;
        protected static int IsStatues = 0;
        public static List<long> Branchlist = new List<long>();
        public static List<long> BranchlistGrading = new List<long>();
        public List<KeyValuePair<int, string>> LstApplicableStatues = new List<KeyValuePair<int, string>>();
        bool IsPenaltyVisible = true;
       
        protected static string data_areas;

        protected List<Datascroll> lst = new List<Datascroll>();

        protected static string tlConnect_url = String.Empty;
        protected static string rlcs_API_url;
                
        protected static string seriesData_GraphMonthlyCompliancePie;
        protected static string complianceSummaryMonth;
        protected static string MonthlyYear;
        protected static string UserAuthKey;
        protected static string userProfileID;
        protected static string userProfileID_Encrypted;
        protected static string authKey;
        protected static string profileID;
        protected static string AvacomRLCSApiURL;

        //ChartPieMonthly_Category
        List<int> assignedbranchIDs = new List<int>();

        //protected static bool showPerfRiskMapCalendarChart = true;

        protected class Datascroll
        {
            public Datascroll()
            {
                lstscroll = new List<Datascroll>();
            }
            public string id { get; set; }
            public string Value { get; set; }
            public List<Datascroll> lstscroll { get; set; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            rlcs_API_url = ConfigurationManager.AppSettings["RLCSAPIURL"].ToString();
            RegulatoryAuthKey = ConfigurationManager.AppSettings["RegulatoryAuth"].ToString();
            UserAuthKey= ConfigurationManager.AppSettings["X-User-Id-1"].ToString();
            AvacomRLCSApiURL= ConfigurationManager.AppSettings["AVACOM_RLCS_API_URL"].ToString();

            if (!IsPostBack)
            {
                try
                {
                    #region IsPostBack

                    if (HttpContext.Current.Request.IsAuthenticated)
                    {
                        //if ((AuthenticationHelper.Role == "HMGMT") || (AuthenticationHelper.Role == "LSPOC") ||
                        //    (AuthenticationHelper.Role == "HAPPR") || (AuthenticationHelper.Role == "HEXCT") ||
                        //    (AuthenticationHelper.Role == "SPADM") || (AuthenticationHelper.Role == "DADMN"))
                        //{
                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                            {
                                IsApprover = false;

                                HiddenField home = (HiddenField)Master.FindControl("Ishome");
                                home.Value = "true";

                                entities.Database.CommandTimeout = 180;
                                var userAssignedBranchList = (entities.SP_GetManagerAssignedBranch(AuthenticationHelper.UserID)).ToList();

                                if (userAssignedBranchList != null)
                                {
                                    tlConnect_url = ConfigurationManager.AppSettings["TLConnect_API_URL"].ToString();

                                    if (userAssignedBranchList.Count > 0)
                                    {
                                        assignedbranchIDs = userAssignedBranchList.Select(row => row.ID).ToList();
                                    }

                                    var Ecnt = (entities.sp_Entitycount(Convert.ToInt32(AuthenticationHelper.UserID), "SAT")).ToList();
                                    if (Ecnt.Count > 0)
                                    {
                                        divEntitesCount.InnerText = Convert.ToString(Ecnt.Count);
                                    }
                                    else
                                    {
                                        divEntitesCount.InnerText = "0";
                                    }

                                    if (customerID != 817 && AuthenticationHelper.UserID != 7656)
                                    {
                                        setCompliancesSummaryCounts(assignedbranchIDs);
                                    }

                                    fldsCalender.Visible = true;
                                    Datascroll d = new Datascroll();
                                    Branchlist.Clear();
                                    BranchlistGrading.Clear();
                                    
                                    GetPanIndiaCompliance_MAP(customerID, AuthenticationHelper.UserID, assignedbranchIDs, Branchlist);
                                    GetApplicableStatues(customerID, AuthenticationHelper.UserID);
                                                                       
                                    string TLConnectKey = ConfigurationManager.AppSettings["TLConnect_Encrypt_Decrypt_Key"];

                                    if (!string.IsNullOrEmpty(TLConnectKey))
                                    {
                                        userProfileID = string.Empty;
                                        userProfileID = AuthenticationHelper.ProfileID;
                                        if (!string.IsNullOrEmpty(userProfileID))
                                        {
                                            userProfileID_Encrypted = CryptographyHandler.encrypt(userProfileID.Trim(), TLConnectKey);
                                        }
                                        authKey = AuthenticationHelper.AuthKey;
                                    }
                                    BindStates();
                                    BindLocationFilter(assignedbranchIDs);
                                    
                                    ddlYearNew_SelectedIndexChanged(sender, e);                                    

                                    BindLocationCount(customerID, IsPenaltyVisible);

                                    BindOverDueComplianceList();

                                    //BindGradingReportSummary(Convert.ToInt32(ddlPeriodGrading.SelectedValue), customerID, userProfileID);
                                    MonthlyYear = ddlYearNew.SelectedItem.Text;

                                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);

                                    int UnreadNotifications = Business.ComplianceManagement.GetUnreadUserNotification(AuthenticationHelper.UserID);
                                    if (UnreadNotifications > 0)
                                    {
                                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "divNotification", "openNotificationModal();", true);
                                    }
                                    

                                    divTabs.Visible = true;
                                    ComplianceCalender.Visible = true;
                                    DivOverDueCompliance.Visible = true;
                                }
                            }
                        //}
                        //else
                        //{
                        //    FormsAuthentication.SignOut();
                        //    Session.Abandon();
                        //    FormsAuthentication.RedirectToLoginPage();
                        //}
                    }
                    else
                    {
                        FormsAuthentication.SignOut();
                        Session.Abandon();
                        FormsAuthentication.RedirectToLoginPage();
                    }

                    #endregion
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
            }
        }
        
        protected void setCompliancesSummaryCounts(List<int> assignedBranchList)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    entities.Database.CommandTimeout = 180;

                    var MastertransactionsQuery = (entities.SP_RLCS_ComplianceInstanceTransactionCount(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, AuthenticationHelper.ProfileID)
                      .Where(entry => entry.IsActive == true && entry.IsUpcomingNotDeleted == true && entry.RoleID == 3)).ToList();

                    if (MastertransactionsQuery.Count > 0)
                    {
                        if (assignedBranchList.Count > 0)
                            MastertransactionsQuery = MastertransactionsQuery.Where(row => assignedBranchList.Contains(row.CustomerBranchID)).ToList();

                        //Upcoming                                     
                        divUpcomingCount.InnerText = Convert.ToString(RLCSManagement.DashboardData_ComplianceDisplayCount(MastertransactionsQuery, "Upcoming").Count());

                        //DueToday                                     
                        divDueToday.InnerText = Convert.ToString(RLCSManagement.DashboardData_ComplianceDisplayCount(MastertransactionsQuery, "DueToday").Count());

                        //Overdue                                     
                        divOverdueCount.InnerText = Convert.ToString(RLCSManagement.DashboardData_ComplianceDisplayCount(MastertransactionsQuery, "Overdue").Count());

                        //HighRisk                                     
                        //divhighRiskCount.InnerText = Convert.ToString(RLCSManagement.DashboardData_ComplianceDisplayCount(MastertransactionsQuery, "HighRisk").Count());
                    }
                    else
                    {
                        divUpcomingCount.InnerText = "0";
                        divDueToday.InnerText = "0";
                        divOverdueCount.InnerText = "0";

                        //showPerfRiskMapCalendarChart = false;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                SelectedPageNo.Text = "1";
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo).ToString();
                }

                //GridStatutory.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                //GridStatutory.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                //Reload the Grid

                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        protected void Next_Click(object sender, EventArgs e)
        {
            try
            {
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;

                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo + 1).ToString();
                }
                else
                {

                }

                if (!(StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) > Convert.ToInt32(Session["TotalRows"])))
                    StartRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue);

                EndRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalRows"]))
                    EndRecord = Convert.ToInt32(Session["TotalRows"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";


                GridMonthlyPie.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                GridMonthlyPie.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;


                //Reload the Grid
                //BindCompliancePieChart(assignedbranchIDs);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //  ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        protected void Previous_Click(object sender, EventArgs e)
        {
            try
            {
                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;

                if (Convert.ToInt32(SelectedPageNo.Text) > 1)
                {
                    SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
                }

                StartRecord = StartRecord - Convert.ToInt32(ddlPageSize.SelectedValue);

                if (StartRecord < 1)
                    StartRecord = 1;

                EndRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalRows"]))
                    EndRecord = Convert.ToInt32(Session["TotalRows"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";

                GridMonthlyPie.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                GridMonthlyPie.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                //Reload the Grid
                //BindCompliancePieChart(assignedbranchIDs);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        private void GetPageDisplaySummary()
        {
            try
            {
                // DivRecordsScrum.Visible = true;
                if (Convert.ToString(Session["TotalRows"]) != null && Convert.ToString(Session["TotalRows"]) != "")
                {
                    lblTotalRecord.Text = " " + Session["TotalRows"].ToString();
                    lTotalCount.Text = GetTotalPagesCount().ToString();

                    if (lTotalCount.Text != "0")
                    {
                        if (SelectedPageNo.Text == "" || SelectedPageNo.Text == "0" || SelectedPageNo.Text == "1")
                        {
                            SelectedPageNo.Text = "1";
                            lblStartRecord.Text = "1";

                            if (!(Convert.ToInt32(ddlPageSize.SelectedValue) > Convert.ToInt32(Session["TotalRows"].ToString())))
                                lblEndRecord.Text = ddlPageSize.SelectedValue + " ";
                            else
                                lblEndRecord.Text = Session["TotalRows"].ToString() + " ";
                        }
                    }
                    else if (lTotalCount.Text == "0")
                    {
                        SelectedPageNo.Text = "0";
                        // DivRecordsScrum.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                throw ex;
            }
        }
        
        #region Common Function   

        private void BindOverDueComplianceList()
        {
            try
            {
                if (ddlStatus.SelectedItem.Text == "Statutory")
                {
                    IsSatutoryInternal = "Statutory";

                    int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    int UserID = Convert.ToInt32(AuthenticationHelper.UserID);

                    var detailView = RLCSManagement.GetOverdueComplianceListByRisk(customerID, AuthenticationHelper.ProfileID, UserID, 0).ToList()
                                  .GroupBy(entity => entity.ScheduledOnID)
                                  .Select(entity => entity.FirstOrDefault())
                                  .OrderByDescending(entity => entity.OverdueBy).Take(2).ToList();

                    grdSummaryDetails.Visible = true;
                    grdInternalSummaryDetails.Visible = false;

                    lnkInternalOverdueCompliance.Visible = false;

                    grdSummaryDetails.DataSource = detailView;
                    grdSummaryDetails.DataBind();

                    if (detailView.Count > 0)
                        lnkOverdueCompliance.Visible = true;
                    else
                        lnkOverdueCompliance.Visible = false;
                }
                else if (ddlStatus.SelectedItem.Text == "Internal")
                {
                    IsSatutoryInternal = "Internal";

                    int customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    int UserID = Convert.ToInt32(AuthenticationHelper.UserID);

                    var detailView = InternalDashboardManagement.GetComplianceDashboardOverdueInternal(customerid, "MGMT", UserID, false, 0, 1).ToList()
                            .GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).OrderByDescending(entity => entity.OverdueBy).Take(2).ToList();

                    grdSummaryDetails.Visible = false;
                    grdInternalSummaryDetails.Visible = true;

                    lnkOverdueCompliance.Visible = false;

                    grdInternalSummaryDetails.DataSource = detailView;
                    grdInternalSummaryDetails.DataBind();

                    if (detailView.Count > 0)
                        lnkInternalOverdueCompliance.Visible = true;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected string GetPerformer(long complianceinstanceid)
        {
            try
            {
                string result = "";
                result = DashboardManagement.GetUserName(complianceinstanceid, 3);
                Performername = result;
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }
           
        private void BindLocationCount(int customerID, bool IsPenaltyVisible)
        {
            try
            {
                GetManagementCompliancesSummary(customerID, Branchlist, Convert.ToInt32(tvFilterLocation.SelectedValue),
                    "T", FromFinancialYearSummery, ToFinancialYearSummery, IsApprover, GetDate(txtAdvStartDate.Text), GetDate(txtAdvEndDate.Text), AuthenticationHelper.UserID);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        #endregion

        public void GetPanIndiaCompliance_MAP(int custID, int userID, List<int> assignedBranchList, List<long> blist)
        {
            data_areas = string.Empty;
            Datascroll datap = new Datascroll(); ;

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                string str = "";
                var lstPanIndiaAssignedLocationDetails = (from CB in entities.CustomerBranches
                                                          join RELA in entities.RLCS_EntitiesLocationAssignment
                                                          on CB.ID equals RELA.BranchID
                                                          join RCBCLM in entities.RLCS_CustomerBranch_ClientsLocation_Mapping on RELA.BranchID equals (long)RCBCLM.AVACOM_BranchID
                                                          join RLCM in entities.RLCS_Location_City_Mapping on CB.CityID equals RLCM.AVACOM_CityID
                                                          join RSM in entities.RLCS_State_Mapping on RLCM.SM_Code equals RSM.SM_Code
                                                          where CB.CustomerID == custID && CB.IsDeleted == false && CB.Status == 1
                                                          && RCBCLM.BranchType == "B"
                                                          && RELA.UserID == userID
                                                          && RELA.UserProfileID == AuthenticationHelper.ProfileID
                                                          select new
                                                          {
                                                              AVACOM_BranchID = CB.ID,
                                                              locationCityName = RLCM.LM_Name,
                                                              stateName = RSM.SM_Name
                                                          }).Distinct().ToList();

                if (lstPanIndiaAssignedLocationDetails.Count > 0)
                {
                    if (blist.Count > 0)
                        lstPanIndiaAssignedLocationDetails = lstPanIndiaAssignedLocationDetails.Where(entry => entry.AVACOM_BranchID != null && blist.Contains((long)entry.AVACOM_BranchID)).ToList();

                    foreach (var eachAssignedLocation in lstPanIndiaAssignedLocationDetails)
                    {
                        //{ "id": $(this).attr('id'), "color": "#FC4C4C" }
                        data_areas += "{id:'" + eachAssignedLocation.AVACOM_BranchID + "', color:'#FC4C4C'},";

                        datap.lstscroll.Add(new Datascroll { id = eachAssignedLocation.AVACOM_BranchID.ToString(), Value = eachAssignedLocation.locationCityName });

                        str += " ['" + eachAssignedLocation.locationCityName + "', '" + eachAssignedLocation.locationCityName + "', '" + eachAssignedLocation.AVACOM_BranchID + "'],";

                        //str += " ['" + eachAssignedLocation.AVACOM_BranchID + "', '" + eachAssignedLocation.locationCityName + "'],";
                    }

                    lst = datap.lstscroll;
                }

                if (!string.IsNullOrEmpty(data_areas))
                {
                    data_areas = "[" + data_areas.Trim(',') + "]";
                }

                ClientScript.RegisterClientScriptBlock(this.GetType(), "JSScript2", "<script language='javascript'> locations = [" + str.TrimEnd(',') + "];</script>");
            }
        }

        public void GetPanIndiaCompliance_MAP(int custID, int userID, List<int> assignedBranchList)
        {
            data_areas = string.Empty;
            Datascroll datap = new Datascroll(); ;

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                string str = "";
                //var lstPanIndiaAssignedLocationDetails = (from CB in entities.CustomerBranches
                //                               join RLCM in entities.RLCS_Location_City_Mapping on CB.CityID equals RLCM.AVACOM_CityID
                //                               where CB.CustomerID == custID
                //                               select new
                //                               {
                //                                   AVACOM_BranchID = CB.ID,
                //                                   locationCityName = RLCM.LM_Name
                //                               }).Distinct().ToList();

                var lstPanIndiaAssignedLocationDetails = (from CB in entities.CustomerBranches
                                                          join RELA in entities.RLCS_EntitiesLocationAssignment
                                                          on CB.ID equals RELA.BranchID
                                                          join RLCM in entities.RLCS_Location_City_Mapping on CB.CityID equals RLCM.AVACOM_CityID
                                                          join RSM in entities.RLCS_State_Mapping on RLCM.SM_Code equals RSM.SM_Code
                                                          where CB.CustomerID == custID && CB.IsDeleted == false && CB.Status == 1
                                                          && RELA.UserID == userID
                                                          && RELA.UserProfileID == AuthenticationHelper.ProfileID
                                                          select new
                                                          {
                                                              AVACOM_BranchID = CB.ID,
                                                              locationCityName = RLCM.LM_Name,
                                                              stateName = RSM.SM_Name
                                                          }).Distinct().ToList();

                if (lstPanIndiaAssignedLocationDetails.Count > 0)
                {
                    foreach (var eachAssignedLocation in lstPanIndiaAssignedLocationDetails)
                    {
                        //{ "id": $(this).attr('id'), "color": "#FC4C4C" }
                        data_areas += "{id:'" + eachAssignedLocation.AVACOM_BranchID + "', color:'#FC4C4C'},";

                        datap.lstscroll.Add(new Datascroll { id = eachAssignedLocation.AVACOM_BranchID.ToString(), Value = eachAssignedLocation.locationCityName });

                        str += " ['" + eachAssignedLocation.locationCityName + "', '" + eachAssignedLocation.locationCityName + "', '" + eachAssignedLocation.AVACOM_BranchID + "'],";

                        //str += " ['" + eachAssignedLocation.AVACOM_BranchID + "', '" + eachAssignedLocation.locationCityName + "'],";
                    }

                    lst = datap.lstscroll;
                }

                if (!string.IsNullOrEmpty(data_areas))
                {
                    data_areas = "[" + data_areas.Trim(',') + "]";
                }

                ClientScript.RegisterClientScriptBlock(this.GetType(), "JSScript2", "<script language='javascript'> locations = [" + str.TrimEnd(',') + "];</script>");
            }
        }

        public void GetPanIndiaCompliance_MAP()
        {
            data_areas = string.Empty;
            Datascroll datap = new Datascroll(); ;

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                string str = "";
                var lstPanIndiaStateDetails = (from e in entities.CustomerBranches
                                               join r in entities.RLCS_State_Mapping
                                               on e.StateID equals r.AVACOM_StateID
                                               where e.CustomerID == customerID
                                               select new
                                               {
                                                   RLCSState = r.SM_PanIndia_ID,
                                                   State = r.SM_Name
                                               }).Distinct();

                if (lstPanIndiaStateDetails.Count() > 0)
                {
                    foreach (var eachState in lstPanIndiaStateDetails)
                    {
                        //{ "id": $(this).attr('id'), "color": "#FC4C4C" }
                        data_areas += "{id:'" + eachState.RLCSState + "', color:'#FC4C4C'},";

                        datap.lstscroll.Add(new Datascroll { id = eachState.RLCSState, Value = eachState.State });
                        str += " ['" + eachState.RLCSState + "', '" + eachState.State + "'],";
                    }

                    lst = datap.lstscroll;
                }

                if (!string.IsNullOrEmpty(data_areas))
                {
                    data_areas = "[" + data_areas.Trim(',') + "]";
                }

                ClientScript.RegisterClientScriptBlock(this.GetType(), "JSScript2", "<script language='javascript'> var locations = [" + str.TrimEnd(',') + "];</script>");
            }
        }

        public void GetApplicableStatues(int custID, int userID)
        {
            string str1 = "";
            IsStatues = 0;
            bool IsAvantisFlag = true;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var statues = (entities.SP_RLCS_GetApplicableActs(custID, userID, AuthenticationHelper.ProfileID, IsAvantisFlag)).ToList();

                statues = statues.GroupBy(entity => entity.ACTID).Select(entity => entity.FirstOrDefault()).ToList();

                foreach (var i in statues)
                {
                    LstApplicableStatues.Add(new KeyValuePair<int, string>(i.ACTID, i.ACTNAME));
                    str1 += " ['" + i.ACTID + "', '" + i.ACTNAME + "'],";
                }

                if (LstApplicableStatues.Count > 0)
                {
                    StatuesCount.InnerText = Convert.ToString(LstApplicableStatues.Count);
                    IsStatues = 1;
                }
            }
        }

        //public void GetApplicableStatues()
        //{
        //    string str1 = "";
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        var statues = (entities.SP_GetApplicableActs(customerID)).ToList();
        //        foreach (var i in statues)
        //        {
        //            LstApplicableStatues.Add(new KeyValuePair<int, string>(i.ACTID, i.ACTNAME));
        //            str1 += " ['" + i.ACTID + "', '" + i.ACTNAME + "'],";
        //        }
        //        //ClientScript.RegisterClientScriptBlock(this.GetType(), "script", "<script language='javascript'> var Acts = [" + str1.TrimEnd(',') + "];</script>");
        //    }
        //}
        
        #region Top DropDown Selected Index Change

        private void BindUserColors()
        {
            try
            {
                var Cmd = Business.ComplianceManagement.Getcolor(AuthenticationHelper.UserID);
                if (Cmd != null)
                {
                    highcolor.Value = Cmd.High;
                    mediumcolor.Value = Cmd.Medium;
                    lowcolor.Value = Cmd.Low;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        
        public static List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchy(int customerID, int customerbranchid)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }
            return hierarchy;
        }

        public static void LoadSubEntities(int customerid, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp, bool isClient, ComplianceDBEntities entities)
        {


            IQueryable<CustomerBranch> query = (from row in entities.CustomerBranches
                                                where row.IsDeleted == false && row.CustomerID == customerid
                                                     && row.ParentID == nvp.ID
                                                select row);
            var subEntities = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }
        
        protected void btnTopSearch_Click(object sender, EventArgs e)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    if (customerID == -1)
                    {
                        customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    }
                    Branchlist.Clear();
                    GetAllHierarchy(customerID, Convert.ToInt32(tvFilterLocation.SelectedValue));
                    BID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                    var userAssignedBranchList = (entities.SP_RLCS_GetAssignedLocationBranches(customerID, AuthenticationHelper.UserID, AuthenticationHelper.ProfileID)).ToList();
                    if (userAssignedBranchList != null)
                    {
                        if (userAssignedBranchList.Count > 0)
                        {
                            assignedbranchIDs = userAssignedBranchList.Select(row => row.ID).ToList();
                        }
                    }
                    Branchlist.ToList();
                    BindLocationCount(customerID, IsPenaltyVisible);
                    //BindGradingReportSummary(Convert.ToInt32(ddlPeriodGrading.SelectedValue), customerID, AuthenticationHelper.ProfileID);
                    BindOverDueComplianceList();
                    
                    GetPanIndiaCompliance_MAP(customerID, AuthenticationHelper.UserID, assignedbranchIDs, Branchlist);
                    GetApplicableStatues(customerID, AuthenticationHelper.UserID);
                }
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilterFunction", "$(\"#divFilterLocationFunction\").hide(\"blind\", null, 500, function () { });", true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilterRisk", "$(\"#divFilterLocationRisk\").hide(\"blind\", null, 500, function () { });", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


        #endregion

        #region  Function Report
        
        private void BindLocationFilter(List<int> assignedBranchList)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int customerID = -1;
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                    var bracnhes = CustomerBranchManagement.GetAllHierarchyManagementSatutory(customerID);

                    tvFilterLocation.Nodes.Clear();

                    string isstatutoryinternal = "";
                    //if (ddlStatus.SelectedValue == "Statutory")
                    //{
                    if (ddlStatus.SelectedValue == "0")
                    {
                        isstatutoryinternal = "S";
                    }
                    else if (ddlStatus.SelectedValue == "1")
                    {
                        isstatutoryinternal = "I";
                    }

                    //var LocationList = CustomerBranchManagement.GetAssignedLocationList(AuthenticationHelper.UserID, customerID, AuthenticationHelper.Role, isstatutoryinternal);

                    TreeNode node = new TreeNode("Entity/State/Location/Branch", "-1");
                    node.Selected = true;
                    tvFilterLocation.Nodes.Add(node);

                    foreach (var item in bracnhes)
                    {
                        node = new TreeNode(item.Name, item.ID.ToString());
                        node.SelectAction = TreeNodeSelectAction.Expand;
                        CustomerBranchManagement.BindBranchesHierarchy(node, item, assignedBranchList);
                        tvFilterLocation.Nodes.Add(node);
                    }

                    tvFilterLocation.CollapseAll();
                    tvFilterLocation_SelectedNodeChanged(null, null);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public DateTime? GetDate(string date)
        {
            if (date != null && date != "")
            {
                string date1 = "";
                if (date.Contains("/"))
                {
                    date1 = date.Trim().Substring(date.IndexOf("/") + 4, 4) + "-" + date.Substring(date.IndexOf("/") + 1, 2) + "-" + date.Substring(0, 2);
                }
                else if (date.Trim().Contains("-"))
                {
                    date1 = date.Substring(date.IndexOf("-") + 4, 4) + "-" + date.Substring(date.IndexOf("-") + 1, 2) + "-" + date.Substring(0, 2);
                }
                else if (date.Trim().Contains(" "))
                {
                    date1 = date.Substring(date.IndexOf(" ") + 4, 4) + "-" + date.Substring(date.IndexOf(" ") + 1, 2) + "-" + date.Substring(0, 2);
                }
                return Convert.ToDateTime(date1);
            }
            else
            {
                return null;
            }
        }                
               
        public void BindEntityCount(string tlConnectAPIUrl, string profileID, string encryptedProfileID, string authenticationKey)
        {
            try
            {
                string requestUrl = string.Empty;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    requestUrl = tlConnectAPIUrl + "Dashboard/GetProfile_BasicInformationByProfileID?profileID=" + profileID;

                    string responseData = RLCSAPIClasses.InvokeWithAuthKey("GET", requestUrl, authenticationKey, encryptedProfileID, "");

                    if (!string.IsNullOrEmpty(responseData))
                    {
                        var _objProfileInfo = JsonConvert.DeserializeObject<Profile_BasicInfo>(responseData);

                        if (_objProfileInfo != null)
                        {
                            if (_objProfileInfo.lstBasicInfrmation != null)
                            {
                                divEntitesCount.InnerText = _objProfileInfo.lstBasicInfrmation.Count.ToString();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void GetManagementCompliancesSummary(int customerid, List<long> Branchlist, int CustomerbranchID, string clickflag, DateTime FIFromDate, DateTime FIEndDate, bool approver = false, DateTime? FromDate = null, DateTime? EndDateF = null, int userId = -1)
        {
            try
            {
                string tempperFunctionChart = perFunctionChart;
                string tempperFunctionPieChart = perFunctionPieChart;
                string tempperRiskChart = perRiskChart;

                if (1 == 2)
                {
                    //if graph wise filter is required make 1==1
                    if (clickflag == "T")
                    {
                        perFunctionChart = string.Empty;
                        perFunctionPieChart = string.Empty;
                        perRiskChart = string.Empty;
                    }
                    else if (clickflag == "F")
                    {
                        perFunctionChart = string.Empty;
                    }
                    else if (clickflag == "R")
                    {
                        perRiskChart = string.Empty;
                    }
                }
                else
                {
                    perFunctionChart = string.Empty;
                    perFunctionPieChart = string.Empty;
                    perRiskChart = string.Empty;
                }

                bool auditor = false;

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    entities.Database.CommandTimeout = 180;

                    ////Entities
                    //var entityCount = (from CB in entities.CustomerBranches
                    //                   join RELA in entities.RLCS_EntitiesLocationAssignment
                    //                   on CB.ID equals RELA.BranchID
                    //                   where 
                    //                   //CB.CustomerID == customerid &&
                    //                   RELA.UserID == userId
                    //                   && CB.IsDeleted == false
                    //                   && CB.Status == 1
                    //                   && RELA.UserProfileID == AuthenticationHelper.ProfileID
                    //                   && CB.ParentID == null
                    //                   select CB.ID).Distinct().Count();

                    //divEntitesCount.InnerText = Convert.ToString(entityCount);

                    List<SP_RLCS_GetManagementCompliancesSummary_Result> MasterManagementCompliancesSummaryQuery = new List<SP_RLCS_GetManagementCompliancesSummary_Result>();

                    MasterManagementCompliancesSummaryQuery = (entities.SP_RLCS_GetManagementCompliancesSummary(AuthenticationHelper.UserID, customerid, AuthenticationHelper.ProfileID)).ToList();
                    //MasterManagementCompliancesSummaryQuery = (entities.SP_RLCS_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID)).ToList();

                    //Compliance Count
                    if (MasterManagementCompliancesSummaryQuery != null)
                    {
                        if (MasterManagementCompliancesSummaryQuery.Count > 0)
                        {
                            var complianceCount = MasterManagementCompliancesSummaryQuery.Select(entry => entry.ComplianceID).Distinct().Count();
                            divCompliancesCount.InnerText = complianceCount.ToString();
                        }
                        else
                        {
                            divCompliancesCount.InnerText = "0";
                        }
                    }

                    if (MasterManagementCompliancesSummaryQuery != null)
                    {
                        if (MasterManagementCompliancesSummaryQuery.Count > 0)
                        {
                            if (AuthenticationHelper.Role != "AUDT")
                            {
                                if (FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
                                {
                                    MasterManagementCompliancesSummaryQuery = MasterManagementCompliancesSummaryQuery.Where(entry => entry.ScheduledOn >= FIFromDate && entry.ScheduledOn <= FIEndDate).ToList();
                                }
                            }

                            if (Branchlist.Count > 0)
                            {
                                MasterManagementCompliancesSummaryQuery = MasterManagementCompliancesSummaryQuery.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                            }

                            List<SP_RLCS_GetManagementCompliancesSummary_Result> transactionsQuery = new List<SP_RLCS_GetManagementCompliancesSummary_Result>();
                            DateTime EndDate = DateTime.Today.Date;
                            DateTime? PassEndValue;

                            if (FromDate != null && EndDateF != null)
                            {
                                transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                     where row.CustomerID == customerid
                                                     && row.ScheduledOn >= FromDate
                                                     && row.ScheduledOn <= EndDateF
                                                     select row).ToList();

                                PassEndValue = EndDateF;
                            }
                            else if (FromDate != null)
                            {
                                transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                     where row.CustomerID == customerid
                                                     && row.ScheduledOn >= FromDate && row.ScheduledOn <= EndDate
                                                     select row).ToList();

                                PassEndValue = EndDate;
                            }
                            else if (EndDateF != null)
                            {
                                transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                     where row.CustomerID == customerid && row.ScheduledOn <= EndDateF
                                                     select row).ToList();


                                PassEndValue = EndDateF;
                            }
                            else
                            {
                                if (approver == true)
                                {
                                    transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                         where row.CustomerID == customerid
                                                         && (row.ComplianceStatusID == 5 || row.ComplianceStatusID == 4
                                                         || row.ScheduledOn <= EndDate)
                                                         select row).ToList();
                                }
                                else
                                {
                                    if (FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
                                    {
                                        if (AuthenticationHelper.Role == "AUDT")
                                        {
                                            transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                                 where row.CustomerID == customerid
                                                                 && (row.ComplianceStatusID == 5 || row.ComplianceStatusID == 4
                                                                 || row.ScheduledOn <= FIEndDate)
                                                                 select row).ToList();
                                        }
                                        else
                                        {
                                            //For Financial Year
                                            transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                                 where row.CustomerID == customerid
                                                                 && row.ScheduledOn >= FIFromDate
                                                                 && row.ScheduledOn <= FIEndDate
                                                                 select row).ToList();
                                        }
                                    }
                                    else
                                    {
                                        transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                             where row.CustomerID == customerid
                                                             && row.ScheduledOn <= FIEndDate
                                                             select row).ToList();
                                    }
                                }

                                PassEndValue = FIEndDate;
                            }

                            if (AuthenticationHelper.Role != "AUDT")
                            {
                                if (FromDate == null && EndDateF == null && FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
                                {
                                    PassEndValue = FIEndDate;
                                    FromDate = FIFromDate;
                                }
                            }

                            if (approver == true)
                            {
                                transactionsQuery = transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == AuthenticationHelper.UserID).ToList();
                            }

                            transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                            //PieChart
                            long totalPieCompletedcount = 0;
                            long totalPieNotCompletedCount = 0;
                            long totalPieAfterDueDatecount = 0;

                            //added by rahul on 19 april 2018
                            //long totalPieOverduecount = 0;
                            //long totalPieUpcomingcount = 0;

                            long totalPieCompletedHIGH = 0;
                            long totalPieCompletedMEDIUM = 0;
                            long totalPieCompletedLOW = 0;

                            long totalPieAfterDueDateHIGH = 0;
                            long totalPieAfterDueDateMEDIUM = 0;
                            long totalPieAfterDueDateLOW = 0;

                            long totalPieNotCompletedHIGH = 0;
                            long totalPieNotCompletedMEDIUM = 0;
                            long totalPieNotCompletedLOW = 0;

                            //added by rahul on 19 april 2018
                            //long totalPieOverdueHIGH = 0;
                            //long totalPieOverdueMEDIUM = 0;
                            //long totalPieOverdueLOW = 0;

                            //added by rahul on 19 april 2018
                            //long totalPieUpcomingHIGH = 0;
                            //long totalPieUpcomingMEDIUM = 0;
                            //long totalPieUpcomingLOW = 0;

                            long highcount;
                            long mediumCount;
                            long lowcount;
                            long totalcount;
                            DataTable table = new DataTable();
                            table.Columns.Add("ID", typeof(int));
                            table.Columns.Add("Catagory", typeof(string));
                            table.Columns.Add("High", typeof(long));
                            table.Columns.Add("Medium", typeof(long));
                            table.Columns.Add("Low", typeof(long));

                            string listCategoryId = "";
                            List<sp_ComplianceAssignedCategory_Result> CatagoryList = new List<sp_ComplianceAssignedCategory_Result>();
                            if (approver == true)
                            {
                                CatagoryList = RLCSManagement.GetNewAll(AuthenticationHelper.UserID, CustomerbranchID, Branchlist, true);
                            }
                            else
                            {
                                CatagoryList = RLCSManagement.GetNewAll(AuthenticationHelper.UserID, CustomerbranchID, Branchlist);
                            }

                            string perFunctionHIGH = "";
                            string perFunctionMEDIUM = "";
                            string perFunctionLOW = "";

                            string perFunctiondrilldown = "";

                            perFunctionHIGH = "series: [ {name: 'High',id: 'High',color: '#FF0000' , data: ["; /*perFunctionChartColorScheme.high*/
                            perFunctionMEDIUM = "{name: 'Medium',id: 'Medium',color: '#FFD320', data: ["; /*perFunctionChartColorScheme.medium*/
                            perFunctionLOW = "{name: 'Low',id: 'Low',color: '#64B973', data: ["; /*perFunctionChartColorScheme.low*/


                            perFunctiondrilldown = "drilldown:{drillUpButton:{relativeTo: 'spacingBox',position:{y: 0,x:0},theme:{fill: 'white','stroke-width': 1,stroke: 'silver',r: 0,states:{hover:{fill:'#f7f7f7'},select:{stroke: '#039',fill:'#f7f7f7'}}}},activeDataLabelStyle:{textDecoration: 'none',color: 'gray',}, series: [";
                            string[] ColoumnNames = table.Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToArray();

                            foreach (sp_ComplianceAssignedCategory_Result cc in CatagoryList)
                            {
                                listCategoryId += ',' + cc.Id.ToString();
                                highcount = transactionsQuery.Where(entry => entry.Risk == 0 && entry.ComplianceCategoryId == cc.Id && entry.ComplianceStatusID != 11).Count();
                                mediumCount = transactionsQuery.Where(entry => entry.Risk == 1 && entry.ComplianceCategoryId == cc.Id && entry.ComplianceStatusID != 11).Count();
                                lowcount = transactionsQuery.Where(entry => entry.Risk == 2 && entry.ComplianceCategoryId == cc.Id && entry.ComplianceStatusID != 11).Count();
                                totalcount = highcount + mediumCount + lowcount;

                                if (totalcount != 0)
                                {
                                    //High
                                    long AfterDueDatecountHigh;
                                    long CompletedCountHigh;
                                    long NotCompletedcountHigh;

                                    //added by rahul on 19 april 2018                            
                                    //long OverduecountHigh;
                                    //long UpcomingcountHigh;

                                    if (ColoumnNames[2] == "High")
                                    {
                                        perFunctionHIGH += "{ name:'" + cc.Name + "', y: " + highcount + ",drilldown: 'high' + '" + cc.Name + "',},";
                                    }
                                    AfterDueDatecountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.ComplianceCategoryId == cc.Id).Count();
                                    CompletedCountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7) && entry.ComplianceCategoryId == cc.Id).Count();
                                    NotCompletedcountHigh = transactionsQuery.Where(entry => entry.Risk == 0
                                    && (entry.ComplianceStatusID == 1 
                                    || entry.ComplianceStatusID == 2 
                                    || entry.ComplianceStatusID == 3 
                                    || entry.ComplianceStatusID == 6 
                                    || entry.ComplianceStatusID == 8 
                                    || entry.ComplianceStatusID == 10) && entry.ComplianceCategoryId == cc.Id).Count();

                                    //added by rahul on 19 april 2018
                                    //OverduecountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10) && entry.ComplianceCategoryId == cc.ID && entry.ScheduledOn<DateTime.Today.Date).Count();
                                    //UpcomingcountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10) && entry.ComplianceCategoryId == cc.ID && entry.ScheduledOn> DateTime.Today.Date).Count();

                                    perFunctiondrilldown += "{" +
                                        " id: 'high' + '" + cc.Name + "'," +
                                        " name: 'High'," +
                                        " color: '#FF0000'," + /*perFunctionChartColorScheme.high*/
                                        " data: [ ['In Time', " + CompletedCountHigh + "], " +
                                        " ['After due date', " + AfterDueDatecountHigh + "]," +
                                        " ['Not completed', " + NotCompletedcountHigh + "],]," +
                                        " events: {" +
                                        " click: function(e){" +
                                        " fpopulateddata(e.point.name,'High'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function'," + cc.Id + ",'Statutory','Functionbarchart','0'," + AuthenticationHelper.UserID + ",'" + approver + "')}" +
                                        " }},";

                                    //per Status pie Chart
                                    totalPieCompletedcount += CompletedCountHigh;
                                    totalPieNotCompletedCount += NotCompletedcountHigh;
                                    totalPieAfterDueDatecount += AfterDueDatecountHigh;

                                    //added by rahul on 19 april 2018
                                    //totalPieOverduecount += OverduecountHigh;
                                    //totalPieUpcomingcount += UpcomingcountHigh;

                                    //pie drill down
                                    totalPieAfterDueDateHIGH += AfterDueDatecountHigh;
                                    totalPieCompletedHIGH += CompletedCountHigh;
                                    totalPieNotCompletedHIGH += NotCompletedcountHigh;

                                    //added by rahul on 19 april 2018                            
                                    //totalPieOverdueHIGH += OverduecountHigh;
                                    //totalPieUpcomingHIGH += UpcomingcountHigh;

                                    //Medium
                                    long AfterDueDatecountMedium;
                                    long CompletedCountMedium;
                                    long NotCompletedcountMedium;

                                    //added by rahul on 19 april 2018                            
                                    //long OverduecountMedium;
                                    //long UpcomingcountMedium;

                                    if (ColoumnNames[3] == "Medium")
                                    {
                                        perFunctionMEDIUM += "{ name:'" + cc.Name + "', y: " + mediumCount + ",drilldown: 'mid' + '" + cc.Name + "',},";
                                    }
                                    AfterDueDatecountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.ComplianceCategoryId == cc.Id).Count();
                                    CompletedCountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7) && entry.ComplianceCategoryId == cc.Id).Count();
                                    NotCompletedcountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10) && entry.ComplianceCategoryId == cc.Id).Count();

                                    //added by rahul on 19 april 2018
                                    //OverduecountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10) && entry.ComplianceCategoryId == cc.ID && entry.ScheduledOn < DateTime.Today.Date).Count();
                                    //UpcomingcountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10) && entry.ComplianceCategoryId == cc.ID && entry.ScheduledOn > DateTime.Today.Date).Count();

                                    perFunctiondrilldown += "{ " +
                                        " id: 'mid' + '" + cc.Name + "'," +
                                        " name: 'Medium'," +
                                        " color: '#FFD320'," + /*perFunctionChartColorScheme.medium*/
                                        " data: [['In Time', " + CompletedCountMedium + "]," +
                                        " ['After due date', " + AfterDueDatecountMedium + "]," +
                                        " ['Not completed', " + NotCompletedcountMedium + "],], " +
                                        " events: {" +
                                        " click: function(e){ " +
                                        " fpopulateddata(e.point.name,'Medium'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "','Function'," + cc.Id + ",'Statutory','Functionbarchart','0'," + AuthenticationHelper.UserID + ",'" + approver + "')}" +
                                        " }},";

                                    //per Status pie Chart
                                    totalPieCompletedcount += CompletedCountMedium;
                                    totalPieNotCompletedCount += NotCompletedcountMedium;
                                    totalPieAfterDueDatecount += AfterDueDatecountMedium;

                                    //added by rahul on 19 april 2018
                                    //totalPieOverduecount += OverduecountMedium;
                                    //totalPieUpcomingcount += UpcomingcountMedium;

                                    //pie drill down 
                                    totalPieAfterDueDateMEDIUM += AfterDueDatecountMedium;
                                    totalPieCompletedMEDIUM += CompletedCountMedium;
                                    totalPieNotCompletedMEDIUM += NotCompletedcountMedium;

                                    //added by rahul on 19 april 2018                            
                                    //totalPieOverdueMEDIUM += OverduecountMedium;
                                    //totalPieUpcomingMEDIUM += UpcomingcountMedium;

                                    //Low
                                    long AfterDueDatecountLow;
                                    long CompletedCountLow;
                                    long NotCompletedcountLow;

                                    //added by rahul on 19 april 2018                            
                                    //long OverduecountLow;
                                    //long UpcomingcountLow;

                                    if (ColoumnNames[4] == "Low")
                                    {
                                        perFunctionLOW += "{ name:'" + cc.Name + "', y: " + lowcount + ",drilldown: 'low' + '" + cc.Name + "',},";
                                    }

                                    AfterDueDatecountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.ComplianceCategoryId == cc.Id).Count();
                                    CompletedCountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7) && entry.ComplianceCategoryId == cc.Id).Count();
                                    NotCompletedcountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10) && entry.ComplianceCategoryId == cc.Id).Count();

                                    //added by rahul on 19 april 2018
                                    //OverduecountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10) && entry.ComplianceCategoryId == cc.ID && entry.ScheduledOn < DateTime.Today.Date).Count();
                                    //UpcomingcountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10) && entry.ComplianceCategoryId == cc.ID && entry.ScheduledOn > DateTime.Today.Date).Count();


                                    perFunctiondrilldown += "{" +
                                        " id: 'low' + '" + cc.Name + "', " +
                                        " name: 'Low', " +
                                        " color: '#64B973', " + /*perFunctionChartColorScheme.low*/
                                        " data: [['In Time', " + CompletedCountLow + "], " +
                                        " ['After due date'," + AfterDueDatecountLow + "], " +
                                        " ['Not completed'," + NotCompletedcountLow + "],], " +
                                        " events: {" +
                                        " click: function(e){ " +
                                        " fpopulateddata(e.point.name,'Low'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function'," + cc.Id + ",'Statutory','Functionbarchart','0'," + AuthenticationHelper.UserID + ",'" + approver + "')}" +
                                        " }},";

                                    //per Status pie Chart
                                    totalPieCompletedcount += CompletedCountLow;
                                    totalPieNotCompletedCount += NotCompletedcountLow;
                                    totalPieAfterDueDatecount += AfterDueDatecountLow;

                                    //added by rahul on 19 april 2018
                                    //totalPieOverduecount += OverduecountLow;
                                    //totalPieUpcomingcount += UpcomingcountLow;

                                    //pie drill down
                                    totalPieAfterDueDateLOW += AfterDueDatecountLow;
                                    totalPieCompletedLOW += CompletedCountLow;
                                    totalPieNotCompletedLOW += NotCompletedcountLow;

                                    //added by rahul on 19 april 2018                            
                                    //totalPieOverdueLOW += OverduecountLow;
                                    //totalPieUpcomingLOW += UpcomingcountLow;
                                }
                            }
                            perFunctionHIGH += "],},";
                            perFunctionMEDIUM += "],},";
                            perFunctionLOW += "],},],";
                            perFunctiondrilldown += "],},";
                            perFunctionChart = perFunctionHIGH + "" + perFunctionMEDIUM + "" + perFunctionLOW + perFunctiondrilldown;


                            #region Upcoming Overdue In Time After Due Date
                            //perFunctionPieChart = "series: [{name: 'Status',tooltip:{pointFormat: 'hello',},data: [{name: 'Overdue',y: " + totalPieOverduecount + ",color: perStatusChartColorScheme.high,drilldown: 'overdue',},{ name: 'Upcoming',y: " + totalPieUpcomingcount + ",color:'#1d86c8',drilldown: 'upcoming',},{ name: 'After Due Date',y: " + totalPieAfterDueDatecount + ",color: perStatusChartColorScheme.medium,drilldown: 'afterDueDate',},{name: 'In Time',y: " + totalPieCompletedcount + ",color: perStatusChartColorScheme.low,drilldown: 'inTime',}],}],";

                            //perFunctionPieChart += "drilldown:{activeDataLabelStyle: {textDecoration: 'none',color: 'gray',}, " +
                            //                   " series: [" +

                            //                    " {id: 'inTime',name: 'In Time',cursor: 'pointer',data: [{name: 'High',y: " + totalPieCompletedHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                            //                    // " // In time - High                   
                            //                    " fpopulateddata(e.point.name,'In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "')" +
                            //                    " },},},{name: 'Medium',y: " + totalPieCompletedMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                            //                    // " // In time - Medium                                
                            //                    " fpopulateddata(e.point.name,'In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "')" +
                            //                    "},},},{name: 'Low',y: " + totalPieCompletedLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                            //                    // " // In time - Low                               
                            //                    " fpopulateddata(e.point.name,'In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "')" +

                            //                    " },},}],}," +
                            //                    " {id: 'afterDueDate',name: 'After Due Date',cursor: 'pointer',data: [{name: 'High',y: " + totalPieAfterDueDateHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                            //                    // " // After Due Date - High
                            //                    " fpopulateddata(e.point.name,'After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "')" +
                            //                    " },},},{name: 'Medium',y: " + totalPieAfterDueDateMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                            //                    //  " // After Due Date - Medium
                            //                    " fpopulateddata(e.point.name,'After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "')" +
                            //                    " },},},{name: 'Low',y: " + totalPieAfterDueDateLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                            //                    // " // After Due Date - Low
                            //                    " fpopulateddata(e.point.name,'After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "')" +

                            //                    " },},}],}," +
                            //                    " {id: 'overdue',name: 'Overdue',cursor: 'pointer',data: [{name: 'High',y: " + totalPieOverdueHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                            //                    // " // Not Completed - High
                            //                    " fpopulateddata(e.point.name,'Overdue'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "')" +
                            //                    " },},},{name: 'Medium',y: " + totalPieOverdueMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                            //                    // " // Not Completed - Medium
                            //                    " fpopulateddata(e.point.name,'Overdue'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "')" +
                            //                    " },},},{name: 'Low',y: " + totalPieOverdueLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                            //                    // " // Not Completed - Low
                            //                    " fpopulateddata(e.point.name,'Overdue'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "')" +

                            //                    " },},}],}," +

                            //                    " {id: 'upcoming',name: 'Upcoming',cursor: 'pointer',data: [{name: 'High',y: " + totalPieUpcomingHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                            //                    // " // Not Completed - High
                            //                    " fpopulateddata(e.point.name,'Upcoming'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "')" +
                            //                    " },},},{name: 'Medium',y: " + totalPieUpcomingMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                            //                    // " // Not Completed - Medium
                            //                    " fpopulateddata(e.point.name,'Upcoming'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "')" +
                            //                    " },},},{name: 'Low',y: " + totalPieUpcomingLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                            //                    // " // Not Completed - Low
                            //                    " fpopulateddata(e.point.name,'Upcoming'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "')" +

                            //                     " },},}],}],},";

                            #endregion

                            #region Previous Working
                            perFunctionPieChart = "series: [{name: 'Status',tooltip:{pointFormat: 'hello',},data: [{name: 'Not Completed',y: " + totalPieNotCompletedCount + ",color: '#FF0000',drilldown: 'notCompleted',},{name: 'After Due Date',y: " + totalPieAfterDueDatecount + ",color: '#FFD320',drilldown: 'afterDueDate',},{name: 'In Time',y: " + totalPieCompletedcount + ",color: '#64B973',drilldown: 'inTime',}],}],";

                            perFunctionPieChart += "drilldown:{activeDataLabelStyle: {textDecoration: 'none',color: 'gray',}, " +
                                               " series: [" +

                                                " {id: 'inTime',name: 'In Time',cursor: 'pointer',data: [{name: 'High',y: " + totalPieCompletedHIGH + ",color: '#FF0000',events:{click: function(e) { " +
                                                // " // In time - High                   
                                                " fpopulateddata(e.point.name,'In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "')" +
                                                " },},},{name: 'Medium',y: " + totalPieCompletedMEDIUM + ",color: '#FFD320',events:{click: function(e) { " +
                                                // " // In time - Medium                                
                                                " fpopulateddata(e.point.name,'In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "')" +
                                                "},},},{name: 'Low',y: " + totalPieCompletedLOW + ",color: '#64B973',events:{click: function(e) { " +
                                                // " // In time - Low                               
                                                " fpopulateddata(e.point.name,'In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "')" +

                                                " },},}],}," +
                                                " {id: 'afterDueDate',name: 'After Due Date',cursor: 'pointer',data: [{name: 'High',y: " + totalPieAfterDueDateHIGH + ",color: '#FF0000',events:{click: function(e) { " +
                                                // " // After Due Date - High
                                                " fpopulateddata(e.point.name,'After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "')" +
                                                " },},},{name: 'Medium',y: " + totalPieAfterDueDateMEDIUM + ",color: '#FFD320',events:{click: function(e) { " +
                                                //  " // After Due Date - Medium
                                                " fpopulateddata(e.point.name,'After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "')" +
                                                " },},},{name: 'Low',y: " + totalPieAfterDueDateLOW + ",color: '#64B973',events:{click: function(e) { " +
                                                // " // After Due Date - Low
                                                " fpopulateddata(e.point.name,'After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "')" +

                                                " },},}],}," +
                                                " {id: 'notCompleted',name: 'Not Completed',cursor: 'pointer',data: [{name: 'High',y: " + totalPieNotCompletedHIGH + ",color: '#FF0000',events:{click: function(e) { " +
                                                // " // Not Completed - High
                                                " fpopulateddata(e.point.name,'Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "')" +
                                                " },},},{name: 'Medium',y: " + totalPieNotCompletedMEDIUM + ",color: '#FFD320',events:{click: function(e) { " +
                                                // " // Not Completed - Medium
                                                " fpopulateddata(e.point.name,'Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "')" +
                                                " },},},{name: 'Low',y: " + totalPieNotCompletedLOW + ",color: '#64B973',events:{click: function(e) { " +
                                                // " // Not Completed - Low
                                                " fpopulateddata(e.point.name,'Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "')" +

                                                " },},}],}],},";
                            #endregion

                            perRiskChart = "series: [{name: 'Not Completed', color: '#FF0000',data: [{" + /*perRiskStackedColumnChartColorScheme.high*/
                                                                                                          // Not Completed - High
                            "y: " + totalPieNotCompletedHIGH + ",events:{click: function(e) {" +
                             " fpopulateddata('High','Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "')" +

                            "}}},{" +

                            // Not Completed - Medium
                            " y: " + totalPieNotCompletedMEDIUM + ",events:{click: function(e) {" +
                            " fpopulateddata('Medium','Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "')" +

                            "}}},{  " +

                            // Not Completed - Low
                            "y: " + totalPieNotCompletedLOW + ",events:{click: function(e) {" +
                              " fpopulateddata('Low','Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "')" +

                            "}}}]},{name: 'After Due Date',color:'#FFD320',data: [{" + /*color: perRiskStackedColumnChartColorScheme.medium*/

                            // After Due Date - High
                            "y: " + totalPieAfterDueDateHIGH + ",events:{click: function(e) { " +
                            " fpopulateddata('High','After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "')" +

                            "}}},{   " +

                            // After Due Date - Medium
                            "y: " + totalPieAfterDueDateMEDIUM + ",events:{click: function(e) {" +
                            " fpopulateddata('Medium','After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "')" +

                            "}}},{   " +
                            // After Due Date - Low
                            "y: " + totalPieAfterDueDateLOW + ",events:{click: function(e) {" +
                            " fpopulateddata('Low','After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "')" +

                            "}}}]},{name: 'In Time',color:'#64B973',data: [{" + /*perRiskStackedColumnChartColorScheme.low*/
                                                                                // In Time - High
                            "y: " + totalPieCompletedHIGH + ",events:{click: function(e) {" +
                            " fpopulateddata('High','In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "')" +

                            "}}},{  " +
                            // In Time - Medium
                            "y: " + totalPieCompletedMEDIUM + ",events:{click: function(e) {" +
                            " fpopulateddata('Medium','In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "')" +

                            "}}},{  " +
                            // In Time - Low
                            "y: " + totalPieCompletedLOW + ",events:{click: function(e) {" +
                            " fpopulateddata('Low','In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "')" +

                            "}}}]}]";

                            if (1 == 2)
                            {
                                //if graph wise filter is required make 1==1
                                if (clickflag == "F")
                                {
                                    perRiskChart = tempperRiskChart;
                                }
                                else if (clickflag == "R")
                                {
                                    perFunctionChart = tempperFunctionChart;
                                    perFunctionPieChart = tempperFunctionPieChart;
                                }
                            }
                        }
                        else
                        {

                        }
                    }

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        
        #endregion
        
        protected void upDivLocation_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);

                //Top
                DateTime TopStartdate = DateTime.MinValue;
                if (DateTime.TryParseExact(txtAdvStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out TopStartdate))
                {
                    //ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerTopStartdate", string.Format("initializeDatePickerTopStartdate(new Date({0}, {1}, {2}));", TopStartdate.Year, TopStartdate.Month - 1, TopStartdate.Day), true);
                }
                else
                {
                    //ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerTopStartdate", "initializeDatePickerTopStartdate(null);", true);
                }

                DateTime TopEnddate = DateTime.MinValue;
                if (DateTime.TryParseExact(txtAdvEndDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out TopEnddate))
                {
                    //ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerTopEnddate", string.Format("initializeDatePickerTopEnddate(new Date({0}, {1}, {2}));", TopEnddate.Year, TopEnddate.Month - 1, TopEnddate.Day), true);
                }
                else
                {
                    // ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerTopEnddate", "initializeDatePickerTopEnddate(null);", true);
                }

                //Function

                DateTime FunctionStartDate = DateTime.MinValue;
                if (DateTime.TryParseExact(txtAdvStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out FunctionStartDate))
                {
                    //ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerFunctionStartDate", string.Format("initializeDatePickerFunctionStartDate(new Date({0}, {1}, {2}));", FunctionStartDate.Year, FunctionStartDate.Month - 1, FunctionStartDate.Day), true);
                }
                else
                {
                    // ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerFunctionStartDate", "initializeDatePickerFunctionStartDate(null);", true);
                }

                DateTime FunctionEndDate = DateTime.MinValue;
                if (DateTime.TryParseExact(txtAdvEndDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out FunctionEndDate))
                {
                    //ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerFunctionEndDate", string.Format("initializeDatePickerFunctionEndDate(new Date({0}, {1}, {2}));", FunctionEndDate.Year, FunctionEndDate.Month - 1, FunctionEndDate.Day), true);
                }
                else
                {
                    // ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerFunctionEndDate", "initializeDatePickerFunctionEndDate(null);", true);
                }


                //Risk
                DateTime RiskStartDate = DateTime.MinValue;
                if (DateTime.TryParseExact(txtAdvStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out RiskStartDate))
                {
                    // ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerRiskStartDate", string.Format("initializeDatePickerRiskStartDate(new Date({0}, {1}, {2}));", RiskStartDate.Year, RiskStartDate.Month - 1, RiskStartDate.Day), true);
                }
                else
                {
                    // ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerRiskStartDate", "initializeDatePickerRiskStartDate(null);", true);
                }

                DateTime RiskEndDate = DateTime.MinValue;
                if (DateTime.TryParseExact(txtAdvEndDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out RiskEndDate))
                {
                    // ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerRiskEndDate", string.Format("initializeDatePickerRiskEndDate(new Date({0}, {1}, {2}));", RiskEndDate.Year, RiskEndDate.Month - 1, RiskEndDate.Day), true);
                }
                else
                {
                    // ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerRiskEndDate", "initializeDatePickerRiskEndDate(null);", true);
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected string GetPerformerInternal(long complianceinstanceid)
        {
            try
            {
                string result = "";
                result = DashboardManagement.GetUserNameInternal(complianceinstanceid, 3);
                InternalPerformername = result;
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }

        }

        protected void ddlYearNew_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(ddlYearNew.SelectedItem.Text))
                {
                    MonthlyYear = ddlYearNew.SelectedItem.Text;

                    string dtfrom = Convert.ToString("01" + "-01" + "-" + ddlYearNew.SelectedItem.Text);
                    string dtto = Convert.ToString("31" + "-12" + "-" + ddlYearNew.SelectedItem.Text);

                    FromFinancialYearSummery = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    ToFinancialYearSummery = DateTime.ParseExact(dtto.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }

                //MonthlyYear = ddlYearNew.SelectedItem.Text;

                ////IsFinancialYear = true;
                //if (ddlYearNew.SelectedValue.Equals("0"))
                //{
                //    //if (DateTime.Today.Month > 3)
                //    //{
                //        string dtfrom = Convert.ToString("01" + "-01" + "-" + ddlYearNew.SelectedItem.Text);
                //        //DateTime dtnext = DateTime.Now.AddYears(1);
                //        string dtto = Convert.ToString("31" + "-12" + "-" + ddlYearNew.SelectedItem.Text);
                //        FromFinancialYearSummery = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                //        ToFinancialYearSummery = DateTime.ParseExact(dtto.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                //    //}
                //    //else
                //    //{
                //    //    DateTime dtprev = DateTime.Now.AddYears(-1);
                //    //    string dtfrom = Convert.ToString("01" + "-01" + "-" + dtprev.Year);
                //    //    string dtto = Convert.ToString("31" + "-12" + "-" + DateTime.Now.Year);
                //    //    FromFinancialYearSummery = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                //    //    ToFinancialYearSummery = DateTime.ParseExact(dtto.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                //    //}
                //}
                //else if (ddlYearNew.SelectedValue.Equals("1"))
                //{
                //    //if (DateTime.Today.Month > 3)
                //    //{
                //        DateTime dtprev = DateTime.Now.AddYears(-1);
                //        string dtfrom = Convert.ToString("01" + "-01" + "-" + ddlYearNew.SelectedItem.Text);
                //        //DateTime dtnext = DateTime.Now.AddYears(1);
                //        string dtto = Convert.ToString("31" + "-12" + "-" + ddlYearNew.SelectedItem.Text);
                //        FromFinancialYearSummery = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                //        ToFinancialYearSummery = DateTime.ParseExact(dtto.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                //    //}
                //    //else
                //    //{
                //    //    DateTime dtprev = DateTime.Now.AddYears(-2);
                //    //    string dtfrom = Convert.ToString("01" + "-04" + "-" + dtprev.Year);
                //    //    string dtto = Convert.ToString("31" + "-03" + "-" + DateTime.Now.Year);
                //    //    FromFinancialYearSummery = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                //    //    ToFinancialYearSummery = DateTime.ParseExact(dtto.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                //    //}
                //}
                //else if (ddlYearNew.SelectedValue.Equals("2"))
                //{
                //    string dtfrom = Convert.ToString("01-01-1900");
                //    string dtto = Convert.ToString("01-01-1900");
                //    FromFinancialYearSummery = DateTime.ParseExact(dtfrom.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                //    ToFinancialYearSummery = DateTime.ParseExact(dtto.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                //}
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
              
        public void GetCalender()
        {
            DateTime dt1 = DateTime.Now.Date;
            string month = dt1.Month.ToString();

            if (Convert.ToInt32(month) < 10)
            {
                month = "0" + month;
            }

            string year = dt1.Year.ToString();

            CalendarDate = year + "-" + month;

            //var roles = CustomerBranchManagement.GetAssignedroleid(AuthenticationHelper.UserID);

            int customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int userid = AuthenticationHelper.UserID;

            DataTable dt = fetchComplianceInstanceTransactions(customerid, userid);

            var list = (from r in dt.AsEnumerable()
                        select r["ScheduledOn"]).Distinct().ToList();

            var datetodayornext = (from r in dt.AsEnumerable()
                                   orderby r.Field<DateTime>("ScheduledOn")
                                   where r.Field<DateTime>("ScheduledOn") >= DateTime.Now.AddDays(-1)
                                   select r["ScheduledOn"]).Distinct().FirstOrDefault();

            CalendarTodayOrNextDate = Convert.ToDateTime(datetodayornext);

            CalenderDateString = "";

            for (int i = 0; i < list.Count; i++)
            {
                Compliancecalendar _Event = new Compliancecalendar();
                string clor = "";
                int assignedCount = 0;
                DateTime date = Convert.ToDateTime(list[i]);

                //if (date < DateTime.Now)
                //{

                var Assigned = (from dts in dt.AsEnumerable()
                                where dts.Field<DateTime>("ScheduledOn").Year == date.Year
                                && dts.Field<DateTime>("ScheduledOn").Month == date.Month
                                && dts.Field<DateTime>("ScheduledOn").Day == date.Day
                                // && dts.Field<int>("RoleID") == 3
                                && (dts.Field<int>("ComplianceStatusID") == 1
                                || dts.Field<int>("ComplianceStatusID") == 10
                                || dts.Field<int>("ComplianceStatusID") == 2
                                || dts.Field<int>("ComplianceStatusID") == 3
                                || dts.Field<int>("ComplianceStatusID") == 4
                                || dts.Field<int>("ComplianceStatusID") == 6
                                || dts.Field<int>("ComplianceStatusID") == 5)
                                select dts.Field<long>("ScheduledOnID")).Distinct().ToList();

                var Completed = (from dts in dt.AsEnumerable()
                                 where dts.Field<DateTime>("ScheduledOn").Year == date.Year
                                 && dts.Field<DateTime>("ScheduledOn").Month == date.Month
                                 && dts.Field<DateTime>("ScheduledOn").Day == date.Day
                                 //&& dts.Field<int>("RoleID") == 4
                                 && (dts.Field<int>("ComplianceStatusID") == 4
                                 || dts.Field<int>("ComplianceStatusID") == 5)
                                 select dts.Field<long>("ScheduledOnID")).Distinct().ToList();

                assignedCount = Assigned.Count;

                if (date > DateTime.Now)
                {
                    clor = "";

                    if (Assigned.Count > Completed.Count)
                    {
                        clor = "upcoming";
                    }
                    else if ((Assigned.Count) == (Completed.Count))
                    {
                        clor = "complete";
                    }
                }
                else
                {
                    if (Assigned.Count > Completed.Count)
                    {
                        clor = "overdue";
                    }
                    else if ((Assigned.Count) == (Completed.Count))
                    {
                        clor = "complete";
                    }
                }
                //}
                //else
                //{
                //    clor = "upcoming";
                //}


                DateTime dtdate = Convert.ToDateTime(list[i]);

                var Date = dtdate.ToString("yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);

                CalenderDateString += "\"" + Date.ToString() + "\": { \"number\" :" + assignedCount + ", \"badgeClass\": \"badge-warning " + clor + "\",\"url\": \"javascript: fcal('" + Date.ToString() + "');\"},";

            }

            CalenderDateString = CalenderDateString.Trim(',');
        }

        public DataTable fetchComplianceInstanceTransactions(int customerid, int userid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                //statutory and statutory Checklist
                var Query = (from row in entities.SP_RLCS_ComplianceInstanceTransactionCount(customerid, userid, AuthenticationHelper.ProfileID)
                             select new
                             {
                                 ScheduledOn = row.PerformerScheduledOn,
                                 ComplianceStatusID = row.ComplianceStatusID,
                                 RoleID = row.RoleID,
                                 ScheduledOnID = row.ScheduledOnID,
                                 CustomerBranchID = row.CustomerBranchID,
                             }).Distinct().ToList();


                DataTable table = new DataTable();
                table.Columns.Add("ScheduledOn", typeof(DateTime));
                table.Columns.Add("ComplianceStatusID", typeof(int));
                table.Columns.Add("RoleID", typeof(int));
                table.Columns.Add("ScheduledOnID", typeof(long));
                table.Columns.Add("CustomerBranchID", typeof(int));

                if (Query.Count > 0)
                {
                    foreach (var item in Query)
                    {
                        DataRow dr = table.NewRow();
                        dr["ScheduledOn"] = item.ScheduledOn;
                        dr["ComplianceStatusID"] = item.ComplianceStatusID;
                        dr["RoleID"] = item.RoleID;
                        dr["ScheduledOnID"] = item.ScheduledOnID;
                        dr["CustomerBranchID"] = item.CustomerBranchID;
                        table.Rows.Add(dr);
                    }
                }

                return table;
            }
        }
        
        [WebMethod]
        public static List<ActDetail> GetApplicableActs()
        {
            List<ActDetail> lstActDetail = new List<ActDetail>();

            string requestUrl = ConfigurationManager.AppSettings["TLConnect_API_URL"].ToString();
            requestUrl += "Dashboard/GetRLCSActNameByProfileID?profileID=HVZCE4TNUGIZFYX";

            string responseData = RLCSAPIClasses.Invoke("GET", requestUrl, "");

            if (!string.IsNullOrEmpty(responseData))
            {
                var lstActDetail_Response = JsonConvert.DeserializeObject<List<ActDetail>>(responseData);

                if (lstActDetail_Response != null)
                {
                    if (lstActDetail_Response.Count > 0)
                    {
                        lstActDetail = lstActDetail_Response;
                    }
                }
            }

            return lstActDetail;
        }

        public static string GetMonthName(int monthNumber)
        {
            string monthName = string.Empty;

            if (monthNumber != 0)
            {
                if (monthNumber == 1)
                    monthName = "Jan";
                else if (monthNumber == 2)
                    monthName = "Feb";
                else if (monthNumber == 3)
                    monthName = "Mar";
                else if (monthNumber == 4)
                    monthName = "Apr";
                else if (monthNumber == 5)
                    monthName = "May";
                else if (monthNumber == 6)
                    monthName = "Jun";
                else if (monthNumber == 7)
                    monthName = "Jul";
                else if (monthNumber == 8)
                    monthName = "Aug";
                else if (monthNumber == 9)
                    monthName = "Sep";
                else if (monthNumber == 10)
                    monthName = "Oct";
                else if (monthNumber == 11)
                    monthName = "Nov";
                else if (monthNumber == 12)
                    monthName = "Dec";
            }

            return monthName;
        }



        public void BindStates()
        {
            try
            {
                ddlState.DataTextField = "Name";
                ddlState.DataValueField = "ID";
                ddlState.Items.Clear();

                ddlState.DataSource = RLCS_ClientsManagement.GetStates();
                ddlState.DataBind();
                ddlState.Items.Insert(0, new ListItem("--Select State--", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
        }

        public void BindActs()
        {
            try
            {
                ddlAct.DataTextField = "Name";
                ddlAct.DataValueField = "ID";
                ddlAct.Items.Clear();

                ddlAct.DataSource = RLCS_ClientsManagement.GetAllActs(Convert.ToInt32(ddlState.SelectedItem.Value));
                ddlAct.DataBind();
                ddlAct.Items.Insert(0, new ListItem("--Select Act--", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
        }

        public void BindMonthYear()
        {
            try
            {
                if (!string.IsNullOrEmpty(Convert.ToString(DateTime.Now.Month)))
                    ddlMonthReg.SelectedItem.Value = Convert.ToString(DateTime.Now.Month);
                if (!string.IsNullOrEmpty(Convert.ToString(DateTime.Now.Year)))
                    ddlYearReg.SelectedItem.Value = Convert.ToString(DateTime.Now.Year);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }

        }
                protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
        {
            // ddlAct.ClearSelection();
            BindActs();
        }
    
    }
}