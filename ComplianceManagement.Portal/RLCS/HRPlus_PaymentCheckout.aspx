﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HRPlus_PaymentCheckout.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.PaymentCheckout" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
     <!-- Bootstrap CSS -->
    <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap theme -->
    <link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <!-- font icon -->
    <link href="~/NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <!--external css-->
    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/contract_custom_style.css" rel="stylesheet" />
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="/Newjs/jquery.js"></script>
    <script type="text/javascript" src="/Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="/Newjs/bootstrap.min.js"></script>
    <link href="/NewCSS/contract_custom_style.css" rel="stylesheet" />


    <link href="../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.default.min.css" rel="stylesheet" />
    <script type="text/javascript" src="/Newjs/kendo.all.min.js"></script>
    <script type="text/javascript"> 
        <%-- window.onload = function () {
            var rbl = document.getElementById("<%=rbMode.ClientID %>");
            var radio = rbl.getElementsByTagName("INPUT");
            for (var i = 0; i < radio.length; i++) {
                radio[i].onchange = function () {
                    setRadioButtonClass();
                };
            }
        };--%>

        $(document).ready(function () {
            $(document).tooltip({ selector: '[data-toggle="tooltip"]' });
            // $('#updateProgress1').hide();
            debugger;
            if ($(window.parent.document).find("#divProceedPayment").children().first().hasClass("k-loading-mask"))
                $(window.parent.document).find("#divProceedPayment").children().first().hide();

            var start = $("#txtTransactionOfflineDate").kendoDatePicker({
                change: startChange,
                format: "dd-MMM-yyyy",
                dateInput: false
            }).data("kendoDatePicker");
            start.max(end.value());

        });

        function setRadioButtonClass() {
            debugger;
            var rbl = document.getElementById("<%=rbMode.ClientID %>");
            if (rbl != null && rbl != undefined) {
                var radio = rbl.getElementsByTagName("INPUT");
                if (radio != null && radio != undefined) {
                    for (var i = 0; i < radio.length; i++) {
                        var radiobutton = radio[i];
                        var label = radiobutton.parentNode.getElementsByTagName("LABEL")[0];
                        if (label != null && label != undefined) {
                            label.setAttribute('class', 'radio-inline');
                        }
                    }
                }
            }

            rbl = document.getElementById("<%=rbOfflineType.ClientID %>");
            if (rbl != null && rbl != undefined) {
                var radio = rbl.getElementsByTagName("INPUT");
                if (radio != null && radio != undefined) {
                    for (var i = 0; i < radio.length; i++) {
                        var radiobutton = radio[i];
                        var label = radiobutton.parentNode.getElementsByTagName("LABEL")[0];
                        if (label != null && label != undefined) {
                            label.setAttribute('class', 'radio-inline');
                        }
                    }
                }
            }

            var start = $("#txtTransactionOfflineDate").kendoDatePicker({
                change: startChange,
                format: "dd-MMM-yyyy",
                dateInput: false
            }).data("kendoDatePicker");
            start.max(end.value());

        }

        function startChange() {
            debugger;
            var startDate = start.value();
            if (startDate) {
                startDate = new Date(startDate);
                startDate.setDate(startDate.getDate());
            }
        }

    </script>
    <style>

        html{
            font-size: 62.5%;
    -webkit-tap-highlight-color: transparent;
    background: white;
        }

        .radio-inline {
            padding-left: 0px;
            margin-left: 0em;
            margin-right: 0.5em;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:ScriptManager ID="smComplianceApplicability" EnablePartialRendering="true" runat="server"></asp:ScriptManager>
            <asp:HiddenField ID="hdnCustID" runat="server" />

            <asp:UpdatePanel ID="upPaymentChecout" runat="server">
                <ContentTemplate>
                    <div id="divPayment" style=" height: 650px" class="row Dashboard-white-widget">
                        <div class="row">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                ValidationGroup="OnlinePayment" />
                            <asp:CustomValidator ID="cvPayment" runat="server" EnableClientScript="False"
                                ValidationGroup="OnlinePayment" Display="none" class="alert alert-block alert-danger fade in" />
                        </div>

                        <div class="row">
                            <asp:ValidationSummary ID="vsOfflinePayment" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                ValidationGroup="OfflinePayment" />
                            <asp:CustomValidator ID="cvOfflinepayment" runat="server" EnableClientScript="False"
                                ValidationGroup="OfflinePayment" Display="none" class="alert alert-block alert-danger fade in" />
                        </div>

                        <div class="row col-md-12 colpadding0 mb10">
                            <div class="col-md-12 text-center mt-1">
                                <div class="col-md-3">
                                    <div style="display: none">
                                        <div class="col-md-12">
                                            <h5 class="control-label">Your Payment Details</h5>
                                        </div>

                                        <div class="checkbox text-left">
                                            <label>
                                                <input type="radio" class="m10" checked="checked" />
                                                <img src="/images/paytm.png" width="100px" alt="PayTM" title="PayTM" class="m5all" /><br>
                                            </label>
                                        </div>

                                        <div class="checkbox text-left">
                                            <label>
                                                <input type="radio" class="m10" />
                                                <i class="fa fa-forwardslash fa-2x"></i>
                                                <img src="/images/netbanking.png" width="50px" alt="NetBanking" title="PayUMoney" class="m5all" /><br>
                                            </label>
                                        </div>

                                        <div class="checkbox text-left">
                                            <label>
                                                <input type="radio" class="m10" />
                                                <img src="/images/mobikwik.png" width="100px" alt="MobiKwik" title="MobiKwik" class="m5all" /><br>
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6" style="border: 1px solid">
                                    <div class="form-group row">
                                        <h2>Order Summary</h2>
                                    </div>
                                    <div class="form-group row" id="divrbMode">
                                        <div class="col-md-4 col-sm-4 col-xs-4 text-right vertical-align-center">
                                            <label class="control-label">Mode</label>
                                        </div>
                                        <div class="col-md-8 col-sm-8 col-xs-8">
                                            <asp:RadioButtonList ID="rbMode" runat="server" AutoPostBack="true" RepeatDirection="Horizontal" OnSelectedIndexChanged="rbMode_CheckedChanged">
                                                <asp:ListItem Text="Online" Value="1" Selected="True" CssClass="radio-inline"></asp:ListItem>
                                                <asp:ListItem Text="Offline" Value="2" CssClass="radio-inline"></asp:ListItem>
                                            </asp:RadioButtonList>
                                            <%-- --%>
                                        </div>
                                    </div>

                                    <div class="form-group row" id="divOfflineType" runat="server">
                                        <div class="col-md-4 col-sm-4 col-xs-4 text-right vertical-align-center">
                                            <label class="control-label">Select</label>
                                        </div>
                                        <div class="col-md-8 col-sm-8 col-xs-8">
                                            <asp:RadioButtonList ID="rbOfflineType" runat="server" OnSelectedIndexChanged="rbMode_CheckedChanged" AutoPostBack="true" RepeatDirection="Horizontal">
                                                <asp:ListItem Text="NEFT/IMPS" Value="NEFT/IMPS" Selected="True"></asp:ListItem>
                                                <asp:ListItem Text="Cheque" Value="Cheque"></asp:ListItem>
                                                <asp:ListItem Text="Other" Value="Other"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </div>
                                    </div>

                                    <div class="form-group row required" id="divUTR" runat="server">
                                        <div class="col-md-4 col-sm-4 col-xs-4 text-right vertical-align-center">
                                            <label class="control-label">UTR/Cheque No</label>
                                        </div>
                                        <div class="col-md-8 col-sm-8 col-xs-8">
                                            <asp:TextBox ID="txtTxnNo" runat="server" CssClass="form-control" data-toggle="tooltip" data-placement="bottom" ToolTip="Unique Transaction Reference or Cheque Number"></asp:TextBox>
                                            <asp:RequiredFieldValidator ErrorMessage="Required UTR/Cheque No" ControlToValidate="txtTxnNo" ID="rfvTxnNo"
                                                runat="server" ValidationGroup="OfflinePayment" Display="None" />
                                        </div>
                                    </div>

                                    <div class="form-group row" id="divBank" runat="server">
                                        <div class="col-md-4 col-sm-4 col-xs-4 text-right vertical-align-center">
                                            <label class="control-label">Bank</label>
                                        </div>
                                        <div class="col-md-8 col-sm-8 col-xs-8">
                                            <asp:TextBox ID="txtBank" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group row required">
                                      <asp:Panel ID="pnlPaymentOnlineDate" runat="server">
                                          <div class="col-md-4 col-sm-4 col-xs-4 text-right vertical-align-center">
                                            <label class="control-label">Transaction Date</label>
                                        </div>
                                            <div class="col-md-8 col-sm-8 col-xs-8">
                                                <asp:TextBox ID="txtPaymentDate" runat="server" CssClass="form-control"></asp:TextBox>
                                                <asp:RequiredFieldValidator ErrorMessage="Required Transaction Date" ControlToValidate="txtPaymentDate" ID="rfvPaymentDate"
                                                    runat="server" ValidationGroup="OnlinePayment" Display="None" />
                                            </div>
                                        </asp:Panel>
                                        <asp:Panel ID="pnlPaymentOfflineDate" runat="server" Visible="false" >
                                          <div class="col-md-4 col-sm-4 col-xs-4 text-right vertical-align-center">
                                            <label class="control-label">Transaction Date</label>
                                        </div>
                                        <div class="col-md-8 col-sm-8 col-xs-8" runat="server">
                                            <asp:TextBox ID="txtTransactionOfflineDate" runat="server" Style="width: 100%" />
                                        </div>
                                        </asp:Panel>
                                        </div>
                                  <div class="form-group row required">
                                            <div class="col-md-4 col-sm-4 col-xs-4 text-right vertical-align-center">
                                                <label class="control-label">OrderID</label>
                                            </div>
                                            <div class="col-md-8 col-sm-8 col-xs-8">
                                                <asp:TextBox ID="txtOrderID" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                <asp:RequiredFieldValidator ErrorMessage="Required OrderID" ControlToValidate="txtOrderID" ID="rfvOrderID"
                                                    runat="server" ValidationGroup="OnlinePayment" Display="None" />
                                            </div>
                                        </div>

                                        <div class="form-group row required">
                                            <div class="col-md-4 col-sm-4 col-xs-4 text-right vertical-align-center">
                                                <label class="control-label">Amount</label>
                                            </div>
                                            <div class="col-md-8 col-sm-8 col-xs-8">
                                                <asp:TextBox ID="txtPaymentAmount" runat="server" CssClass="form-control"></asp:TextBox>
                                                <asp:RequiredFieldValidator ErrorMessage="Required Amount" ControlToValidate="txtPaymentAmount" ID="rfvPaymentAmount"
                                                    runat="server" ValidationGroup="OnlinePayment" Display="None" />
                                                <asp:CompareValidator runat="server" ID="cvAmount1"
                                                    ControlToValidate="txtPaymentAmount"
                                                    Operator="DataTypeCheck"
                                                    Type="Integer"
                                                    ErrorMessage="Enter Numbers only in Amount"
                                                    Display="None"
                                                    ValidationGroup="PaymentValidationGroup"
                                                    Text="*" />

                                                <asp:CompareValidator runat="server" ID="cvAmount2"
                                                    ControlToValidate="txtPaymentAmount"
                                                    Operator="GreaterThanEqual"
                                                    ValueToCompare="0"
                                                    Type="Integer"
                                                    ErrorMessage="Amount can not be less than 0"
                                                    Display="None" ValidationGroup="PaymentValidationGroup"
                                                    Text="*" />
                                            </div>
                                        </div>

                                        <div class="form-group row required">
                                            <div class="col-md-4 col-sm-4 col-xs-4 text-right vertical-align-center">
                                                <label class="control-label">Mobile</label>
                                            </div>
                                            <div class="col-md-8 col-sm-8 col-xs-8">
                                                <asp:TextBox ID="txtPaymentMobile" runat="server" CssClass="form-control"></asp:TextBox>
                                                <asp:RequiredFieldValidator ErrorMessage="Required Mobile Number" ControlToValidate="txtPaymentMobile" ID="rfvPaymentMobile"
                                                    runat="server" ValidationGroup="OnlinePayment" Display="None" />
                                            </div>
                                        </div>

                                        <div class="form-group row required">
                                            <div class="col-md-4 col-sm-4 col-xs-4 text-right vertical-align-center">
                                                <label class="control-label">Email</label>
                                            </div>
                                            <div class="col-md-8 col-sm-8 col-xs-8">
                                                <asp:TextBox ID="txtPaymentEmail" runat="server" CssClass="form-control"></asp:TextBox>
                                                <asp:RequiredFieldValidator ErrorMessage="Required Email" ControlToValidate="txtPaymentEmail" ID="rfvPaymentEmail"
                                                    runat="server" ValidationGroup="OnlinePayment" Display="None" />
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <asp:Button ID="btnProceedPayment" runat="server" CssClass="btn btn-primary" Text="Proceed to Payment" ValidationGroup="OnlinePayment"
                                                OnClick="btnProceedPayment_Click" CausesValidation="true"></asp:Button>
                                            <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary" Text="Submit" ValidationGroup="OfflinePayment"
                                                CausesValidation="true" OnClick="btnSubmit_Click"></asp:Button>
                                        </div>
                                    </div>

                                    <div class="col-md-3"></div>
                                </div>
                            </div>
                        </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </form>
</body>
</html>
