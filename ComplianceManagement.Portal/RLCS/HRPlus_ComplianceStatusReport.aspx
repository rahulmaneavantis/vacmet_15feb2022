﻿<%@ Page Title="Status Report" Language="C#" MasterPageFile="~/HRPlusSPOCMGR.Master" AutoEventWireup="true" CodeBehind="HRPlus_ComplianceStatusReport.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.HRPlus_ComplianceStatusReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title></title>

    <link href="../NewCSS/kendo_V1.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo_V1.silver.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../Newjs/jszip.min.js"></script>

    <link href="../NewCSS/Kendouicss_V1.css" rel="stylesheet" />
    <link href="../NewCSS/contract_custom_style.css" rel="stylesheet" />

    <script src="../Newjs/moment.min.js"></script>
      <style type="text/css">
          /*.k-popup.k-calendar-container, .k-popup.k-list-container {
    background-color: white;
    width: 159px;
}*/ 
          .k-textbox>input{
         text-align:left;
     }


          .k-check-all{
           zoom:1.15;
           font-size:12.6px;
             
          }
            .k-label input[type="checkbox"] {
    zoom: 1.2;
    margin: 0px 5px 0 !important;
}
          .k-list-container.k-popup-dropdowntree .k-check-all {
    margin: 10px 8.5px 0;
}
        .k-autocomplete.k-state-focused>.k-i-close, .k-autocomplete.k-state-hover>.k-i-close, .k-dropdown-wrap.k-state-focused>.k-i-close, .k-dropdown-wrap.k-state-hover>.k-i-close, .k-multiselect-wrap.k-state-focused>.k-i-close, .k-multiselect-wrap.k-state-hover>.k-i-close {
    display: inline-block;
    outline: 0;
}
        .k-multiselect-wrap>.k-i-close {
    top: 4px;
}
         
          .k-treeview .k-item {
    display: block;
    border-width: 0;
    margin: 0;
    padding: 0 0 0 0px;
}
          .k-treeview .k-content, .k-treeview .k-item>.k-group, .k-treeview>.k-group {
    margin: 0px;
    padding: 0;
    background: 0 0;
    list-style-type: none;
    position: relative;
    margin-top: -10px;
}
           .k-treeview .k-state-hover, .k-treeview .k-state-hover:hover {
    cursor: pointer;
    background-color: transparent;
    border-color: transparent;
    color:#1fd9e1;
     }
    .k-checkbox-label:hover{
            color:#1fd9e1;
     }
          label.k-label:hover{
              color:#1fd9e1;
          }
             .k-list > .k-state-focused.k-state-selected, .k-listview > .k-state-focused.k-state-selected, .k-state-focused.k-state-selected, td.k-state-focused.k-state-selected {
            -webkit-box-shadow: inset 0 0 3px 1px rgba(0, 0, 0, 0.5);
            
            box-shadow: inset 0 0 3px 1px rgba(0, 0, 0, 0.5);
        }

        input[type=checkbox], input[type=radio] {
            margin: 4px 6px 0px 0px;
            margin-top: 1px;
            line-height: normal;
        }
        .k-calendar-container {
    background-color: white;
    width: 217px;
  }
      
        .k-picker-wrap{
            height:34px;
        }
        .k-autocomplete .k-input, .k-dropdown-wrap .k-input, .k-multiselect-wrap .k-input, .k-numeric-wrap .k-input, .k-picker-wrap .k-input, .k-selectbox .k-input, .k-textbox > input{
            padding-bottom:6px;
            padding-top:5px;
        }
        .div.k-grid-footer, div.k-grid-header {
            border-top-width: 1px;
            margin-right: 0px;
        }
        .k-calendar td.k-state-selected .k-link, .k-calendar td.k-today.k-state-selected.k-state-hover .k-link {
            color: #515967;
        }
       .k-grid-footer-wrap, .k-grid-header-wrap {
            position: relative;
            width: 100%;
            overflow: hidden;
            border-style: solid;
            border-width: 0 1px 0 0;
            zoom: 1;
        }

        .k-grid-content {
            min-height: auto !important;
        }
        .k-popup.k-calendar-container, .k-popup.k-list-container {
            margin-left:0px;
            
        }
          .k-autocomplete > .k-i-close, .k-dropdown-wrap > .k-i-close {
              top: 30%;
          }
        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }
         .k-tooltip-content{
         width: max-content;
       }
        .k-checkbox-label, .k-radio-label {
            display: inline;
            padding-left:22px;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 2.0em;
            border-bottom-width: 1px;
            background-color: white;
            border-width: 0 1px 1px 0px;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
            background-color: white;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: white;
        }

        #grid .k-grid-toolbar {
            background: white;
        }

        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 0px;
            margin-left:2.7px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: white;
        }

        html .k-grid tr.k-alt:hover {
            background: white;
        }

        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            background: white;
            border: none;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }
      .k-list-container {
    background-color: white;
    width: auto !important;
}
          .k-list-filter > .k-icon {
              top: 32%;
          }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
            margin-right: 2px;
            color:rgba(0,0,0,0.5);
        }

        .k-filter-menu .k-button {
            width: 27%;
        }
      
        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: white;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 0px 1px 0px;
            text-align: center;
        }

        .k-grid-pager {
            margin-top: 0px;
            border-width:0px 0px 0px 0px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            border-width: 0px 0px 1px 0px;
            background: #E9EAEA;
            font-weight: bold;
            margin-right: 18px;
            font-family: 'Roboto',sans-serif;
            font-size: 15px;
            color: rgba(0, 0, 0, 0.5);
            height: 20px;
            vertical-align: middle;
        }

        .k-dropdown-wrap.k-state-default {
            background-color: white;
            height:34px;
        }
        .k-multiselect-wrap, .k-floatwrap{
            height:34px;
            
        }
        .k-popup.k-calendar-container, .k-popup.k-list-container {
            background-color: white;
        }

        .k-grid-header th.k-state-focused, .k-list > .k-state-focused, .k-listview > .k-state-focused, .k-state-focused, td.k-state-focused {
            -webkit-box-shadow: inset 0 0 3px 1px white;
            box-shadow: inset 0 0 3px 1px white;
        }

        label.k-label {
            font-family: roboto,sans-serif !important;
            color: #515967;
            /* font-stretch: 100%; */
            font-style: normal;
            font-weight: 400;
            min-width: max-content !important;
            white-space: pre-wrap;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: flex;
            margin-bottom: 0px;
        }
       
        .k-state-default > .k-select {
            border-color: #ceced2;
            margin-top: 0px;
        }

        .k-grid-norecords {
            width: 100%;
            height: 100%;
            text-align: center;
            margin-top: 2%;
        }

        .k-grouping-header {
            font-style: italic;
            background-color: white;
        }

        .k-grid-toolbar {
            background: white;
            border: none;
        }

        .k-grid table {
            width: 100.5%;
        }
          
          .k-icon, .k-tool-icon{
              margin-top:5px;
          }
          .k-i-excel {
             margin-top: -3px;
          }
          .k-i-more-vertical{
              margin-top:0px;
          }
           .k-dropdown .k-state-focused, .k-filebrowser-dropzone, .k-list > .k-state-hover, .k-mobile-list .k-edit-label.k-check:active, .k-mobile-list .k-item > .k-label:active, .k-mobile-list .k-item > .k-link:active, .k-mobile-list .k-recur-view .k-check:active, .k-pager-wrap .k-link:hover, .k-scheduler .k-scheduler-toolbar ul li.k-state-hover, .k-splitbar-horizontal-hover:hover, .k-splitbar-vertical-hover:hover, .k-state-hover, .k-state-hover:hover{
            color: #2e2e2e;
            background-color: #d8d6d6;
        }
           .btn{
              font-weight:400;
          }
           
      
    </style>      
    <script id="templateTooltip" type="text/x-kendo-template">
        <div>
           <div> #:value ? value : "N/A" #</div>
        </div>
    </script>

    <script>
        $(document).ready(function () {
            fhead('My Reports/Status Report');
            // Bind_Customer();
            //var CustWise = getUrlVars();
            //if (CustWise == "CustWise") { } else { Bind_Customer(); }
            Bind_Customer();
            Bind_SPOC();
            Bind_ComplianceType();
            Bind_FilterType();

            Bind_Frequency();

            Bind_Period();

            BindGrid();

            $("#btnExcel").click(function (e) {
                ExportToExcel(e);
            });

            $('#btnClearFilter').css('display', 'none');
        });

        function getUrlVars() {
            var vars = [], hash;
            var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
            for (var i = 0; i < hashes.length; i++) {
                hash = hashes[i].split('=');
                vars.push(hash[0]);
                vars[hash[0]] = hash[1];
            }
            return vars;
        }

        function ExportToExcel(e) {
            e.preventDefault();
            kendo.ui.progress($("#gridCompliances").data("kendoGrid").element, true);

            // trigger export of the products grid
            $("#gridCompliances").data("kendoGrid").saveAsExcel();

            kendo.ui.progress($("#gridCompliances").data("kendoGrid").element, false);
            return false;
        }

        $(document).ready(function () {

            function startChange() {
                var startDate = start.value(),
                endDate = end.value();

                if (startDate) {
                    startDate = new Date(startDate);
                    startDate.setDate(startDate.getDate());
                    end.min(startDate);
                } else if (endDate) {
                    start.max(new Date(endDate));
                } else {
                    endDate = new Date();
                    start.max(endDate);
                    end.min(endDate);
                }

                ApplyFilter();
            }

            function endChange() {
                var endDate = end.value(),
                startDate = start.value();

                if (endDate) {
                    endDate = new Date(endDate);
                    endDate.setDate(endDate.getDate());
                    start.max(endDate);
                } else if (startDate) {
                    end.min(new Date(startDate));
                } else {
                    endDate = new Date();
                    start.max(endDate);
                    end.min(endDate);
                }

                ApplyFilter();
            }

            var start = $("#txtStartPeriod").kendoDatePicker({
                change: startChange,
                format: "dd-MMM-yyyy",
                dateInput: false
            }).data("kendoDatePicker");

            var end = $("#txtEndPeriod").kendoDatePicker({
                change: endChange,
                format: "dd-MMM-yyyy",
                dateInput: false
            }).data("kendoDatePicker");

            start.max(end.value());
            end.min(start.value());
        });

        function Bind_Customer() {
            $("#ddlCustomers").kendoDropDownList({
                optionLabel: "Customers",
                filter: "contains",
                autoClose: false,
                tagMode: "single",
                checkboxes: true,
                checkAll: true,
                checkAllTemplate: "Select All",
                dataTextField: "CustomerName",
                dataValueField: "CustomerID",
                change: ChangeCustomer,
                dataSource: {
                    transport: {
                        read: {
                            url: '<% =AVACOM_RLCS_API_URL%>GetMyAssignedCustomers?userID=<% =loggedInUserID%>&distID=<% =distID%>&roleCode=<% =loggedUserRole%>',
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },
                            dataType: 'json',
                        }
                    },
                    schema: {
                        data: function (response) {
                            return response.Result;
                        }
                    },
                },
            });
        }

        function Bind_SPOC() {

            var customerID = -1;
            if ($('#ddlSPOCs').data('kendoDropDownTree')) {
                $('#ddlSPOCs').data('kendoDropDownTree').destroy();
                $('#divSPOCs').empty();
                $('#divSPOCs').append('<input id="ddlSPOCs" data-placeholder="User" style="width: 100%;" />');
            }
            var customerID = -1;
            if ($("#ddlCustomers").val() != '' && $("#ddlCustomers").val() != null && $("#ddlCustomers").val() != undefined) {
                customerID = $("#ddlCustomers").val();
            }

            var apiURL = '';

            if (customerID !== -1) {
                apiURL = '<%=AVACOM_RLCS_API_URL%>GetUsers?custID=' + customerID + '&distID=<% =distID%>&spID=<% =spID%>';
            } else {                
                apiURL = '<% =AVACOM_RLCS_API_URL%>GetAssignedSPOCs?userID=<% =loggedInUserID%>&distID=<% =distID%>&roleCode=<% =loggedUserRole%>';
            }

            $("#ddlSPOCs").kendoDropDownTree({
                placeholder: "Users",
                filter: "contains",
                autoClose: false,
                tagMode: "single",
                checkboxes: true,
                checkAll: true,
                checkAllTemplate: "Select All",
                dataTextField: "UserName",
                dataValueField: "UserID",
                change: ApplyFilter,
                dataSource: {
                    transport: {
                        read: {
                            url:apiURL, 
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },
                            dataType: 'json',
                        }
                    },
                    schema: {
                        data: function (response) {
                            return response.Result;
                        }
                    },
                },
            });
        }

        function Bind_ComplianceType() {
            $("#ddlComType").kendoDropDownTree({
                placeholder: "Types",
                autoClose: false,
                tagMode: "single",
                checkboxes: true,
                checkAll: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                //change: ApplyFilter,
                dataSource: [
                    //{ text: "All", value: "-1" },
                    { text: "Register", value: "REG" },
                    { text: "Return", value: "RET" },
                    { text: "Challan", value: "CHA" },
                ]
            });
        }
      
        function Bind_FilterType() {
            $("#ddlFType").kendoDropDownTree({
                placeholder: "Status",
                autoClose: false,
                tagMode: "single",
                checkboxes: true,
                checkAll: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: ApplyFilter,
                dataSource: [
                    //{ text: "All", value: "-1" },
                    { text: "Upcoming", value: "Upcoming" },
                    { text: "Overdue", value: "Overdue" },
                    { text: "Pending Review", value: "Pending Review" },
                    { text: "Closed-Timely", value: "Closed-Timely" },
                    { text: "Closed-Delayed", value: "Closed-Delayed" },
                ]
            });
        }

        function Bind_Frequency() {
            $("#ddlFreq").kendoDropDownTree({
                placeholder: "Frequency",
                autoClose: true,
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: Bind_Period,
                dataSource: [
                    //{ text: "Frequency-All", value: "-1" },
                    { text: "Frequency", value: "-1" },
                    { text: "Monthly", value: "Monthly" },
                    { text: "Quarterly", value: "Quarterly" },
                    { text: "Half Yearly", value: "HalfYearly" },
                    { text: "Annual", value: "Annual" },
                    { text: "BiAnnual", value: "TwoYearly" },
                ]
            });
        }

        function Bind_Period() {
            if ($('#ddlPeriod').data('kendoDropDownTree')) {
                $('#ddlPeriod').data('kendoDropDownTree').destroy();
                $('#divPeriod').empty();
                $('#divPeriod').append('<input id="ddlPeriod" data-placeholder="Period" style="width: 100%;" />')
            }

            $("#ddlPeriod").kendoDropDownTree({
                placeholder: "Period",
                autoClose: false,
                tagMode: "single",
                checkboxes: true,
                checkAll: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: ApplyFilter,
                //dataSource: [
                //        { freq: "Annual", text: "Annual", value: "Annual" },
                //        { freq: "Biennial", text: "Biennial", value: "BiAnnual" },

                //        { freq: "Half Yearly", text: "First Half", value: "HY1" },
                //        { freq: "Half Yearly", text: "Second Half", value: "HY2" },

                //        { freq: "Quarterly", text: "Q1", value: "Q1" },
                //        { freq: "Quarterly", text: "Q2", value: "Q2" },
                //        { freq: "Quarterly", text: "Q3", value: "Q3" },
                //        { freq: "Quarterly", text: "Q4", value: "Q4" },

                //        { freq: "Monthly", text: "January", value: "01" },
                //        { freq: "Monthly", text: "February", value: "02" },
                //        { freq: "Monthly", text: "March", value: "03" },
                //        { freq: "Monthly", text: "April", value: "04" },
                //        { freq: "Monthly", text: "May", value: "05" },
                //        { freq: "Monthly", text: "June", value: "06" },
                //        { freq: "Monthly", text: "July", value: "07" },
                //        { freq: "Monthly", text: "August", value: "08" },
                //        { freq: "Monthly", text: "September", value: "09" },
                //        { freq: "Monthly", text: "Octomber", value: "10" },
                //        { freq: "Monthly", text: "November", value: "11" },
                //        { freq: "Monthly", text: "December", value: "12" },
                //]
                dataSource: onPeriodDataBound()
            });

            function onPeriodDataBound() {
                if ($("#ddlFreq").val() == "Annual") {
                    return [{ freq: "Annual", text: "Annual", value: "Annual" }];
                }
                else if ($("#ddlFreq").val() == "BiAnnual" || $("#ddlFreq").val() == "TwoYearly") {
                    return [{ freq: "Biennial", text: "BiAnnual", value: "BiAnnual" }];
                }
                else if ($("#ddlFreq").val() == "HalfYearly") {
                    return [{ freq: "Half Yearly", text: "First Half", value: "HY1" }, { freq: "Half Yearly", text: "Second Half", value: "HY2" }];
                }
                else if ($("#ddlFreq").val() == "Quarterly") {
                    return [{ "freq": "Quarterly", "text": "Q1", "value": "Q1" }, { "freq": "Quarterly", "text": "Q2", "value": "Q2" }, { "freq": "Quarterly", "text": "Q3", "value": "Q3" }, { "freq": "Quarterly", "text": "Q4", "value": "Q4" }];
                }
                else if ($("#ddlFreq").val() == "Monthly") {
                    return [{ freq: "Monthly", text: "January", value: "01" }, { freq: "Monthly", text: "February", value: "02" }, { freq: "Monthly", text: "March", value: "03" }, { freq: "Monthly", text: "April", value: "04" }, { freq: "Monthly", text: "May", value: "05" }, { freq: "Monthly", text: "June", value: "06" }, { freq: "Monthly", text: "July", value: "07" }, { freq: "Monthly", text: "August", value: "08" }, { freq: "Monthly", text: "September", value: "09" }, { freq: "Monthly", text: "Octomber", value: "10" }, { freq: "Monthly", text: "November", value: "11" }, { freq: "Monthly", text: "December", value: "12" }];
                }

            }
            var dropDownPeriod = $("#ddlPeriod").data("kendoDropDownTree");
            var dropDownFreqSelectedValue = $("#ddlFreq").data("kendoDropDownTree").value();
            if (dropDownPeriod != null && dropDownPeriod != undefined) {
                if (dropDownFreqSelectedValue != "") {
                    dropDownPeriod.enable(true);
                    dropDownPeriod.dataSource.filter({ field: "freq", operator: "eq", value: $("#ddlFreq").val() });
                } else {
                    dropDownPeriod.enable(false);
                }
            }

            ApplyFilter();
        }

        function Bind_Year(DataSource) {
            $("#ddlYear").kendoDropDownTree({
                placeholder: "Year",
                autoClose: true,
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: ApplyFilter,
                dataSource: DataSource
            });
            if ($("#ddlYear").data("kendoDropDownTree")) {
                $("#ddlYear").data("kendoDropDownTree").dataSource.sort({ field: "text", dir: "desc" });
            }
        }

        function BindGrid() {
            var fileName = '';
            var exportExcelName = fileName.concat('ComplianceStatusReport-', '<%=DateTime.Now.ToString("ddMMyyyy")%>', ".xlsx");

            var dataSource = new kendo.data.DataSource({
                transport: {
                    read: {
                        url: '<%=AVACOM_RLCS_API_URL%>GetComplianceStatusReport?userID=<% =loggedInUserID%>&profileID=<% =loggedUserProfileID%>&custID=<% =customerID%>&distID=<% =distID%>&spID=<% =spID%>&roleCode=<% =loggedUserRole%>',
                        dataType: "json",
                        beforeSend: function (request) {
                            request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                            request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                            request.setRequestHeader('Content-Type', 'application/json');
                        },
                    }
                },
                schema: {
                    data: function (response) {
                        if (response != null && response != undefined) {
                            return response.Result;
                        }
                    },
                    total: function (response) {
                        if (response.Result != null && response.Result != undefined) {
                            if (response.Result.length > 0) {

                            }
                            return response.Result.length;
                        }
                    }
                },
               pageSize:10,
            });

            var initialLoad = true;

            $("#gridCompliances").kendoGrid({
                dataSource: dataSource,
                //pageable: {
                //    pageSizes: [50, 100, 500],
                //},
                pageable: true,
                pageSize:10,
                persistSelection: true,
                //scrollable: true,
                //scrollable: {
                //    virtual: true,
                //    endless: true
                //},
                pageable: {
                    //numeric: true,
                    pageSize:10,
                    pageSizes:["All",5,10,20],
                    buttonCount: 3,
                },
                dataBinding: function () {
                    record = 0;
                    var total = this.dataSource._pristineTotal;
                    if (this.dataSource.pageSize() == undefined) {
                        this.dataSource.pageSize(total);
                        record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                    }
                    else {
                        record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                    }
                },
                sortable: true,
                filterable:true,
                resizable: true,
                reorderable:true,
                columnMenu:true,
                columns: [
                    {
                        field: "CustomerID",
                        sortable: {
                            initialDirection: "asc"
                        },
                        hidden: true,menu:false,
                    },
                    {
                        field: "CustomerName", title: 'Customer', width: "15.6%", filterable: { multi: true, search: true },
                       attributes: {
                            style: 'white-space: nowrap;'
                        }, 
                        
                        //sortable: {
                        //    initialDirection: "asc"
                        //}
                    },
                    {
                        field: "CustomerBranchName", title: 'Branch', width: "14.9%", filterable: { multi: true, search: true },
                        attributes: { style: 'white-space: nowrap;' }, 
                    },
                    { field: "ActID", hidden: true, menu: false, },
                    { field: "ShortForm", title: 'Compliance', width: "21.9%", filterable: { multi: true, search: true }, attributes: { style: 'white-space: nowrap;' }, },
                    { field: "RequiredForms", title: 'Form', width: "11%", filterable: { multi: true, search: true }, attributes: { style: 'white-space: nowrap;' }, },
                    { field: "Frequency", title: 'Frequency', width: "14.8%", filterable: { multi: true, search: true }, attributes: { style: 'white-space: nowrap;' }, },
                    {
                        field: "ForMonth", title: 'Period', width: "11.8%", filterable: { multi: true, search: true }, attributes: { style: 'white-space: nowrap; text-align:left;' },
                        headerAttributes: { style: 'white-space: nowrap; text-align:left;'},
                    },
                    //{
                    //    field: "ScheduledOn", title: 'Due Date',

                    //    template: "#= dateConverter(data)#",
                    //    width: "15%", type: "date", filterable: { multi: true, search: true },
                    //    attributes: { style: 'white-space: nowrap;text-align:left;' },
                    //    headerAttributes: { style: 'white-space: nowrap; text-align:left;' },
                    //},
                 {
                     field: "ScheduledOn",
                     title: 'Due Date',
                     type: "date",
                    // format: "{0:dd-MMM-yyyy}",
                     //format: "{0:dd-MMM-yyyy}",
                    template: "#= kendo.toString(kendo.parseDate(ScheduledOn, 'yyyy-MM-dd'), 'dd-MMM-yyyy') #",
                     filterable: {
                         multi: true,
                         extra: false,
                         search: true,
                         operators: {
                             string: {
                                 type: 'date',
                                // template: "#= kendo.toString(kendo.parseDate(ScheduledOn, 'yyyy-MM-dd'), 'dd-MMM-yyyy') #",
                                 format: "{0:dd-MMM-yyyy}",
                                 eq: "Is equal to",
                                 neq: "Is not equal to",
                                 contains: "Contains"
                             }
                         }
                     },width:"15%"
                 },
                    { field: "ComplianceStatusID", hidden: true, menu: false, },
                    {
                        field: "FilterStatus", title: 'Status', width: "13%", filterable: { multi: true, search: true },
                        attributes: { style: 'white-space: nowrap;text-align:left;' },
                        headerAttributes: { style: 'white-space: nowrap; text-align:left;' },
                    },
                    { field: "ComplianceStatusName", title: 'Status', hidden: true, menu: false, },
                    { field: "RLCS_PayrollMonth", title: 'PayrollMonth', type: "string", hidden: true, menu: false, },
                    { field: "RLCS_PayrollYear", title: 'PayrollYear', hidden: true, menu: false, },
                ],

                dataBound: onDataBound,

                excelExport: function (e) {
                    var sheet = e.workbook.sheets[0];

                    for (var rowIndex = 1; rowIndex < sheet.rows.length; rowIndex++) {                        
                        var row = sheet.rows[rowIndex];
                        var cellIndex = 6;

                        var value = row.cells[cellIndex].value;
                        var newValue = DateFormatConverter(value);

                        row.cells[cellIndex].value = newValue;
                        row.cells[cellIndex].format = "dd-MMM-yyyy";
                        row.cells[cellIndex].autoWidth = true;
                    }

                    var columns = e.workbook.sheets[0].columns;
                    columns.forEach(function (column) {
                        delete column.width;
                        column.autoWidth = true;
                    });
                },
                //toolbar: ["excel"],
                excel: {
                    fileName: exportExcelName,
                    allPages: true,
                },
            });

            function onDataBound() {
                var cnt = $('#gridCompliances').data('kendoGrid').dataSource.total();

                var dataSource = $('#gridCompliances').data('kendoGrid').dataSource;
                var data = dataSource.data();

                var ddlSource = [];
                var dataSourceToBind = [];
                for (var i = 0; i < data.length; i++) {
                    var currentPayrollYear = data[i].RLCS_PayrollYear;
                    if (!ddlSource.includes(currentPayrollYear)) {
                        ddlSource.push(currentPayrollYear);
                        dataSourceToBind.push({ text: currentPayrollYear, value: currentPayrollYear });
                    }
                }

                if (dataSourceToBind.length > 0) {
                    if ($('#ddlYear').data('kendoDropDownTree')) {
                        $('#ddlYear').data('kendoDropDownTree').destroy();
                        $('#divYear').empty();
                        $('#divYear').append('<input id="ddlYear" data-placeholder="Year" style="width: 100%;" />');

                        Bind_Year(dataSourceToBind);
                    } else {
                        Bind_Year(dataSourceToBind);
                    }
                }
                //var CustWise = getUrlVars();
                //if (CustWise == "CustWise") {
                //    for (var i = 0; i < data.length; i++) {
                //        var CustName = data[0].CustomerName;
                //        var CustID = data[0].CustomerID;

                //        $("#ddlCustomers").val(CustName).attr("disabled", true);
                //        $("#hdnCutomerId").val(CustID);

                //        break;
                //    }
                //}



            }

            //var grid = $("#gridCompliances").data("kendoGrid");
            //grid.dataSource.page(1);
            $("#btnExcel").kendoTooltip({
             //   filter: ".k-grid-edit3",
                content: function (e) {
                    return "Export to Excel";
                }
            });
            $("#gridCompliances").kendoTooltip({
                filter: "td:nth-child(2)",
                position: "bottom",
                content: function (e) {
                    var dataItem = $("#gridCompliances").data("kendoGrid").dataItem(e.target.closest("tr"));

                    var content = dataItem.CustomerName;
                    return content;
                }
            }).data("kendoTooltip");
            $("#gridCompliances").kendoTooltip({
                filter: "td:nth-child(3)",
                position: "bottom",
                content: function (e) {
                    var dataItem = $("#gridCompliances").data("kendoGrid").dataItem(e.target.closest("tr"));

                    var content = dataItem.CustomerBranchName;
                    return content;
                }
            }).data("kendoTooltip");
          
            $("#gridCompliances").kendoTooltip({
                filter: "td:nth-child(5)",
                position: "bottom",
                content: function (e) {
                    var dataItem = $("#gridCompliances").data("kendoGrid").dataItem(e.target.closest("tr"));

                    var content = dataItem.ShortForm;
                    return content;
                }
            }).data("kendoTooltip");

            $("#gridCompliances").kendoTooltip({
                filter: "td:nth-child(6)", 
                position: "bottom",
                content: function (e) {
                    var dataItem = $("#gridCompliances").data("kendoGrid").dataItem(e.target.closest("tr"));

                    var content = dataItem.RequiredForms;
                    return content;
                }
            }).data("kendoTooltip");
            $("#gridCompliances").kendoTooltip({
                filter: "td:nth-child(7)",
                position: "bottom",
                content: function (e) {
                    var dataItem = $("#gridCompliances").data("kendoGrid").dataItem(e.target.closest("tr"));

                    var content = dataItem.Frequency;
                    return content;
                }
            }).data("kendoTooltip");
                $("#gridCompliances").kendoTooltip({
                filter: "td:nth-child(8)",
                position: "bottom",
                content: function (e) {
                    var dataItem = $("#gridCompliances").data("kendoGrid").dataItem(e.target.closest("tr"));

                    var content = dataItem.ForMonth;
                    return content;
                }
            }).data("kendoTooltip");


            $("#gridCompliances").kendoTooltip({
                filter: "td:nth-child(9)", 
                position: "bottom",
                content: function (e) {
                    var dataItem = $("#gridCompliances").data("kendoGrid").dataItem(e.target.closest("tr"));

                    var content = DateFormatConverter(dataItem.ScheduledOn);
                    return content;
                }
            }).data("kendoTooltip");
            $("#gridCompliances").kendoTooltip({
                filter: "td:nth-child(11)",
                position: "bottom",
                content: function (e) {
                    var dataItem = $("#gridCompliances").data("kendoGrid").dataItem(e.target.closest("tr"));

                    var content = dataItem.FilterStatus;
                    return content;
                }
            }).data("kendoTooltip");

        }

        function getStatusName(data) {

            var strStatus = '';
            var todayDate = new Date(); // convert to actual date

            var year = todayDate.getFullYear();
            var month = todayDate.getMonth();

            if (month == 12) {
                year = year + 1;
                month = 1;
            } else {
                month = month + 1;
            }

            var nextMonth = new Date(year, month, todayDate.getDate());

            kendo.culture("en-IN");
            if (data.ScheduledOn != null && data.ScheduledOn != undefined && data.ComplianceStatusID != null && data.ComplianceStatusID != undefined) {
                if ((data.ComplianceStatusID == 1 || data.ComplianceStatusID == 12 || data.ComplianceStatusID == 13)
                    && (kendo.parseDate(data.ScheduledOn) > todayDate && kendo.parseDate(data.ScheduledOn) <= nextMonth)) {
                    strStatus = 'Upcoming';
                } else if ((data.ComplianceStatusID == 1 || data.ComplianceStatusID == 12 || data.ComplianceStatusID == 13)
                    && (kendo.parseDate(data.ScheduledOn) < todayDate)) {
                    strStatus = 'Overdue';
                } else if (data.ComplianceStatusID == 2 || data.ComplianceStatusID == 3 || data.ComplianceStatusID == 12) {
                    strStatus = 'Pending Review';
                } else if (data.ComplianceStatusID == 4) {
                    strStatus = 'Closed-Timely';
                } else if (data.ComplianceStatusID == 5) {
                    strStatus = 'Closed-Delayed';
                }
            }
            return strStatus;
        }

        function DateFormatConverter(providedDate) {

            var strDate = '';
            kendo.culture("en-IN");
            if (providedDate != null && providedDate != undefined) {
                strDate = kendo.toString(kendo.parseDate(providedDate), "dd-MMM-yyyy")
            }
            return strDate;
        }

        function dateConverter(data) {

            var strDate = '';
            kendo.culture("en-IN");
            if (data.ScheduledOn != null && data.ScheduledOn != undefined) {
                strDate = kendo.toString(kendo.parseDate(data.ScheduledOn), "dd-MMM-yyyy")
            }
            return strDate;
        }

        function ChangeCustomer() {
            
            Bind_SPOC();
            ApplyFilter();
        }

        function ApplyFilter() {
            
            //Customer
            //var CustWise = getUrlVars();
            //if (CustWise == "CustWise") {
            //    var selectedCustIDs = $("#hdnCutomerId").val();
            //}
            //else {
            //    var selectedCustIDs = $("#ddlCustomers").val();
            //}
            var selectedCustIDs = $("#ddlCustomers").val();

            //User/SPOC
            var selectedSPOCs = $("#ddlSPOCs").data("kendoDropDownTree")._values;

            //ComType           
            var selectedComTypes = $("#ddlComType").data("kendoDropDownTree")._values;

            //Filter Type - Upcoming, Overdue 
            var selectedFTypes = $("#ddlFType").data("kendoDropDownTree")._values;
            //var checkedFTypes = $("#ddlFType").data("kendoDropDownTree")._allCheckedItems;

            //var selectedFTypes = [];

            //if (checkedFTypes.length > 0) {
            //    for (var i = 0; i < checkedFTypes.length; i += 1) {
            //        var currentProduct = checkedFTypes[i];

            //        selectedFTypes.push({
            //            Name: currentProduct.text
            //        });
            //    }
            //}  

            var finalSelectedfilter = { logic: "and", filters: [] };

            if (selectedCustIDs != "" || selectedSPOCs.length > 0 || selectedComTypes.length > 0 || selectedFTypes.length > 0) {

                if (selectedCustIDs != '' && selectedCustIDs != null && selectedCustIDs != undefined && selectedCustIDs != "-1") {
                    var custFilter = { logic: "or", filters: [] };

                    custFilter.filters.push({
                        field: "CustomerID", operator: "eq", value: selectedCustIDs
                    });

                    finalSelectedfilter.filters.push(custFilter);
                }

                if (selectedSPOCs.length > 0) {
                    var spocFilter = { logic: "or", filters: [] };

                    $.each(selectedSPOCs, function (i, v) {
                        spocFilter.filters.push({
                            field: "UserID", operator: "eq", value: v
                        });
                    });

                    finalSelectedfilter.filters.push(spocFilter);
                }

                if (selectedComTypes.length > 0) {
                    var stateFilter = { logic: "or", filters: [] };

                    $.each(selectedStates, function (i, v) {
                        stateFilter.filters.push({
                            field: "StateID", operator: "eq", value: v
                        });
                    });

                    finalSelectedfilter.filters.push(stateFilter);
                }

                if (selectedFTypes.length > 0) {
                    var fTypeStatusFilter = { logic: "or", filters: [] };

                    $.each(selectedFTypes, function (i, v) {
                        fTypeStatusFilter.filters.push({
                            field: "FilterStatus", operator: "eq", value: v
                        });
                    });

                    finalSelectedfilter.filters.push(fTypeStatusFilter);
                }
            }

            //datefilter 
            var datefilter = { logic: "or", filters: [] };

            //if ($("#txtStartPeriod").data("kendoDatePicker") != null && $("#txtStartPeriod").data("kendoDatePicker") != undefined) {
            //    if ($("#txtStartPeriod").data("kendoDatePicker").value() != null && $("#txtStartPeriod").data("kendoDatePicker").value() != "") {
            //        //var startFilterDate = new Date($("#FromMonth_Deduction").data("kendoDatePicker").value());
            //        var startFilterDate ="01-Jan-2019" //new Date($("#txtStartPeriod").data("kendoDatePicker").value().getFullYear(),
            //        //                               $("#txtStartPeriod").data("kendoDatePicker").value().getMonth(),
            //        //                               1);
            //        datefilter.filters.push({ logic: "or", field: "ScheduledOn", operator: "gte", value: startFilterDate });
            //    }
            //}

            //if ($("#txtEndPeriod").data("kendoDatePicker") != null && $("#txtEndPeriod").data("kendoDatePicker") != undefined) {
            //    if ($("#txtEndPeriod").data("kendoDatePicker").value() != null && $("#txtEndPeriod").data("kendoDatePicker").value() != "") {
            //        //var endFilterDate = moment($("#txnDateTo").data("kendoDatePicker").value());

            //        var year = $("#txtEndPeriod").data("kendoDatePicker").value().getFullYear();
            //        var month = $("#txtEndPeriod").data("kendoDatePicker").value().getMonth();

            //        if (month == 12) {
            //            year = year + 1;
            //            month = 1;
            //        } else {
            //            month = month + 1;
            //        }

            //        var endFilterDate = "01-Jan-2020"//new Date(year, month, 0, 23, 59, 59);
            //        datefilter.filters.push({ logic: "or", field: "ScheduledOn", operator: "lte", value: endFilterDate });
            //    }
            //}

            if ($("#txtStartPeriod").data("kendoDatePicker") != null && $("#txtStartPeriod").data("kendoDatePicker") != undefined && $("#txtStartPeriod").data("kendoDatePicker").value() != "") {

                if ($("#txtStartPeriod").data("kendoDatePicker").value() != null) {
                    var year = $("#txtStartPeriod").data("kendoDatePicker").value().getFullYear();
                    var month = $("#txtStartPeriod").data("kendoDatePicker").value().getMonth();
                    var date = $("#txtStartPeriod").data("kendoDatePicker").value().getDate();

                    if (month == 12) { year = year + 1; month = 1; } else { month = month + 1; }
                    if (month >= 10) { month = month; } else { month = '0' + month; }
                    if (date >= 10) { date = date; } else { date = '0' + date; }
                    var startFilterDate = year + '-' + month + '-' + date + "T00:00:00";
                    finalSelectedfilter.filters.push({ logic: "or", field: "ScheduledOn", operator: "gte", value: startFilterDate });
                }
            }

            if ($("#txtEndPeriod").data("kendoDatePicker") != null && $("#txtEndPeriod").data("kendoDatePicker") != undefined && $("#txtEndPeriod").data("kendoDatePicker").value() != "") {

                if ($("#txtEndPeriod").data("kendoDatePicker").value() != null) {
                    var year = $("#txtEndPeriod").data("kendoDatePicker").value().getFullYear();
                    var month = $("#txtEndPeriod").data("kendoDatePicker").value().getMonth();
                    var date = $("#txtEndPeriod").data("kendoDatePicker").value().getDate();

                    if (month == 12) { year = year + 1; month = 1; } else { month = month + 1; }
                    if (month >= 10) { month = month; } else { month = '0' + month; }
                    if (date >= 10) { date = date; } else { date = '0' + date; }
                    var endFilterDate = year + '-' + month + '-' + date + "T00:00:00";
                    finalSelectedfilter.filters.push({ logic: "or", field: "ScheduledOn", operator: "lte", value: endFilterDate });
                }
            }

            if (datefilter.filters.length > 0)
                finalSelectedfilter.filters.push(datefilter);
           
            //Frequency       
            if ($("#ddlFreq").val() != null && $("#ddlFreq").val() != undefined) {
                if ($("#ddlFreq").val() != "-1" && $("#ddlFreq").val() != "") {
                    finalSelectedfilter.filters.push({ logic: "or", field: "Frequency", operator: "eq", value: $("#ddlFreq").val() });

                    //Period                      
                    if ($("#ddlPeriod").val() != null && $("#ddlPeriod").val() != undefined) {
                        if ($("#ddlPeriod").val() != "-1") {
                            var selectedPeriods = $("#ddlPeriod").data("kendoDropDownTree")._values;
                            if (selectedPeriods.length > 0) {
                                var periodFilter = { logic: "or", filters: [] };

                                if ($("#ddlFreq").val() == "Monthly") {
                                    $.each(selectedPeriods, function (i, v) {
                                        periodFilter.filters.push({ logic: "or", field: "RLCS_PayrollMonth", operator: "eq", value: v });
                                    });
                                } else if ($("#ddlFreq").val() == "Quarterly") {
                                    $.each(selectedPeriods, function (i, v) {                                        
                                        if (v == "Q1") {
                                            periodFilter.filters.push({ logic: "or", field: "RLCS_PayrollMonth", operator: "eq", value: "03" });
                                            periodFilter.filters.push({ logic: "or", field: "RLCS_PayrollMonth", operator: "eq", value: "06" });
                                        } else if (v == "Q2") {
                                            periodFilter.filters.push({ logic: "or", field: "RLCS_PayrollMonth", operator: "eq", value: "06" });
                                            periodFilter.filters.push({ logic: "or", field: "RLCS_PayrollMonth", operator: "eq", value: "09" });
                                        } else if (v == "Q3") {
                                            periodFilter.filters.push({ logic: "or", field: "RLCS_PayrollMonth", operator: "eq", value: "09" });
                                            periodFilter.filters.push({ logic: "or", field: "RLCS_PayrollMonth", operator: "eq", value: "12" });
                                        } else if (v == "Q4") {
                                            periodFilter.filters.push({ logic: "or", field: "RLCS_PayrollMonth", operator: "eq", value: "12" });
                                            periodFilter.filters.push({ logic: "or", field: "RLCS_PayrollMonth", operator: "eq", value: "03" });
                                        }
                                    });
                                } else if ($("#ddlFreq").val() == "HalfYearly") {
                                    $.each(selectedPeriods, function (i, v) {                                        
                                        if (v == "HY1") {
                                            periodFilter.filters.push({ logic: "or", field: "RLCS_PayrollMonth", operator: "eq", value: "06" });
                                            periodFilter.filters.push({ logic: "or", field: "RLCS_PayrollMonth", operator: "eq", value: "09" });
                                        } else if (v == "HY2") {
                                            periodFilter.filters.push({ logic: "or", field: "RLCS_PayrollMonth", operator: "eq", value: "12" });
                                            periodFilter.filters.push({ logic: "or", field: "RLCS_PayrollMonth", operator: "eq", value: "03" });
                                        }
                                    });
                                } else if ($("#ddlFreq").val() == "Annual") {
                                    $.each(selectedPeriods, function (i, v) {
                                        if (v == "Annual") {                                            
                                            periodFilter.filters.push({ logic: "or", field: "RLCS_PayrollMonth", operator: "eq", value: "12" });
                                            periodFilter.filters.push({ logic: "or", field: "RLCS_PayrollMonth", operator: "eq", value: "03" });
                                        }
                                    });
                                } else if ($("#ddlFreq").val() == "BiAnnual" || $("#ddlFreq").val() == "TwoYearly") {
                                    $.each(selectedPeriods, function (i, v) {
                                        periodFilter.filters.push({ logic: "or", field: "RLCS_PayrollMonth", operator: "eq", value: "12" });
                                        periodFilter.filters.push({ logic: "or", field: "RLCS_PayrollMonth", operator: "eq", value: "03" });
                                    });
                                }

                                if (periodFilter.filters.length > 0)
                                    finalSelectedfilter.filters.push(periodFilter);
                            }
                        }
                    }
                }
            }

            //Year      
            var yearFilter = { logic: "or", filters: [] };
            if ($("#ddlYear").val() != null && $("#ddlYear").val() != undefined) {
                if ($("#ddlYear").val() != "") {
                    yearFilter.filters.push({ logic: "or", field: "RLCS_PayrollYear", operator: "eq", value: $("#ddlYear").val() });
                }
            }

            if (yearFilter.filters.length > 0)
                finalSelectedfilter.filters.push(yearFilter);

            if (finalSelectedfilter.filters.length > 0) {
                if ($("#gridCompliances").data("kendoGrid") != null && $("#gridCompliances").data("kendoGrid") != undefined) {
                    var dataSource = $("#gridCompliances").data("kendoGrid").dataSource;
                    dataSource.filter(finalSelectedfilter);
                    $('#btnClearFilter').css('display', 'block');
                }
            } else {
                if ($("#gridCompliances").data("kendoGrid") != null && $("#gridCompliances").data("kendoGrid") != undefined) {
                    var dataSource = $("#gridCompliances").data("kendoGrid").dataSource;
                    dataSource.filter({});
                    $('#btnClearFilter').css('display', 'none');
                }
            }
        }

        function ClearFilter(e) {
            e.preventDefault();
            //e.stopPropogation();
            //$("#ddlCustomers").data("kendoDropDownTree").value([]);
            //var CustWise = getUrlVars();
            //if (CustWise == "CustWise") {
            //}
            //else {
            //    $("#ddlCustomers").data("kendoDropDownList").value("-1");
            //}
            $("#ddlCustomers").data("kendoDropDownList").value("-1");
            $("#ddlSPOCs").data("kendoDropDownTree").value([]);
            $("#ddlComType").data("kendoDropDownTree").value([]);
            $("#ddlFType").data("kendoDropDownTree").value([]);
            $("#ddlFreq").data("kendoDropDownTree").value([]);
            $("#ddlPeriod").data("kendoDropDownTree").value([]);
            $("#ddlYear").data("kendoDropDownTree").value("-1");

            var txtStartPeriod = $("#txtStartPeriod").data("kendoDatePicker");

            if (txtStartPeriod != null && txtStartPeriod != undefined) {
                txtStartPeriod.value(null);
                txtStartPeriod.trigger("change");
            }

            var txtEndPeriod = $("#txtEndPeriod").data("kendoDatePicker");

            if (txtEndPeriod != null && txtEndPeriod != undefined) {
                txtEndPeriod.value(null);
                txtEndPeriod.trigger("change");
            }

            $('#btnClearFilter').css('display', 'none');

            return false;
        }

        var start = $("#txtStartPeriod").kendoDatePicker({
            change: startChange,
            format: "dd-MMM-yyyy",
            dateInput: false
        }).data("kendoDatePicker");

        var end = $("#txtEndPeriod").kendoDatePicker({
            change: endChange,
            format: "dd-MMM-yyyy",
            dateInput: false
        }).data("kendoDatePicker");

        start.max(end.value());
        end.min(start.value());
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container colpadding0 plr0">
        <div class="row col-md-12 colpadding0 mt10 mb10">
            <div class="col-md-1 pl0" style="width: 20%">
                <%--<label for="ddlCustomers" class="control-label">Customer</label>--%>
                <input id="ddlCustomers" data-placeholder="type" style="width: 100%;" />
                <asp:HiddenField runat="server" ID="hdnCutomerId" Value="0" />
            </div>

            <div id="divSPOCs" class="col-md-1 pl0" style="width: 20%">
                <%--<label for="ddlSPOCs" class="control-label">SPOC</label>--%>
                <input id="ddlSPOCs" data-placeholder="type" style="width: 100%;" />
            </div>

            <div class="col-md-1 pl0 d-none" style="width: 15%">
                <%--<label for="ddlComType" class="control-label">Compliance Type</label>--%>
                <input id="ddlComType" data-placeholder="State" style="width: 100%;" />
            </div>

            <div class="col-md-1 pl0" style="width: 15%">
                <%--<label for="ddlFType" class="control-label">Filter Type</label>--%>
                <input id="ddlFType" data-placeholder="Type" style="width: 100%;" />
            </div>

            <div class="col-md-1 pl0" style="width: 15%">
                <input id="ddlFreq" data-placeholder="Frequency" style="width: 100%;" />
            </div>

            <div id="divPeriod" class="col-md-1 pl0" style="width: 15%">
                <input id="ddlPeriod" data-placeholder="Period" style="width: 100%;"/>
            </div>

            <div id="divYear" class="col-md-1 pl0" style="width: 15%">
                <input id="ddlYear" data-placeholder="Year" style="width: 96%;" />
            </div>
        </div>

        <div class="row col-md-12 colpadding0 mt10 mb10">
            <div class="col-md-9 colpadding0">
                <div class="col-md-3 pl0" style="width: 26%;">
                    <input id="txtStartPeriod" placeholder="From Date" style="width:99%"/>
                </div>

                <div class="col-md-3 pl0" style="width: 26%;">
                    <input id="txtEndPeriod" placeholder="To Date" style="width:99%"/>
                </div>

                <div class="col-md-6" style="width: 15%;">
                    <input id="hdnInput3" class="d-none" data-placeholder="Year" style="width: 100%;" />
                </div>
            </div>
            <div class="col-md-3" style="width: 22%;">
                <div class="col-md-6">
                    <button id="btnClearFilter" class="btn btn-primary" onclick="ClearFilter(event)" style="height: 36px;"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;" style="margin-top: -3px;"></span>Clear Filter</button>
                </div>
                <div class="col-md-6">
                    <button id="btnExcel" class="btn btn-primary" data-placement="bottom" style="height: 36px; margin-left: 15px"><span class="k-icon k-i-excel k-grid-edit3"></span>Export</button>
                </div>
            </div>
        </div>

        <div class="row col-md-12 colpadding0 mb10">
            <div id="gridCompliances" <%--style="border: none;"--%>></div>
        </div>
    </div>
</asp:Content>
