﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class Labour_ComplianceDetailsAPI : System.Web.UI.Page
    {
        protected static string Authorization;
        protected  string CId;
        protected  string UserId;
        protected  int CustId;
        protected  int UId;      
        protected  int StatusFlagID;       
        protected  string Flag;     
        protected  string Path;
        protected  int isapprover;
        protected  string Internalsatutory;
        protected  int IsDeptHead;
        protected  int ComplianceTypeID;
        protected  int BID;
        protected  int catid;
        protected static string CustomerName;

        protected void Page_Load(object sender, EventArgs e)
        {
            int CacheClearTime = Convert.ToInt32(ConfigurationManager.AppSettings["CacheClearTimeToken"]);
            string CacheName = "CacheGetTokenData_" + Convert.ToString(AuthenticationHelper.UserID) + "_" + Convert.ToString(AuthenticationHelper.CustomerID);
            Authorization = (string)HttpContext.Current.Cache[CacheName];
            if (Authorization == null)
            {
                Authorization = Business.ComplianceManagement.getToken(Convert.ToString(AuthenticationHelper.UserID));
                HttpContext.Current.Cache.Insert(CacheName, Authorization, null, DateTime.Now.AddMinutes(CacheClearTime), System.Web.Caching.Cache.NoSlidingExpiration); // add it to cache
            }
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                IsDeptHead = 0;
                if (!string.IsNullOrEmpty(Request.QueryString["IsDeptHead"]))
                {
                    IsDeptHead = Convert.ToInt32(Request.QueryString["IsDeptHead"]);
                }
                if (AuthenticationHelper.Role.Equals("HMGR"))
                {
                    isapprover = 0;
                    Flag = "HMGR";
                }
                if (AuthenticationHelper.Role == "HMGR")
                {
                    Path = ConfigurationManager.AppSettings["KendoPathApp"];
                    CId = Convert.ToString(AuthenticationHelper.CustomerID);
                    UserId = Convert.ToString(AuthenticationHelper.UserID);
                    UId = Convert.ToInt32(AuthenticationHelper.UserID);
                    CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    CustomerName = GetCustomerName(CustId);
                    StatusFlagID = -1;

                    if (!string.IsNullOrEmpty(Request.QueryString["Internalsatutory"]))
                    {
                        Internalsatutory = Request.QueryString["Internalsatutory"];
                        if (Internalsatutory == "Statutory")
                        {
                            StatusFlagID = -1;
                        }
                    }

                    BID = 0;
                    catid = 0;

                    if (!string.IsNullOrEmpty(Request.QueryString["branchid"]))
                    {
                        BID = Convert.ToInt32(Request.QueryString["branchid"]);
                    }
                    if (!string.IsNullOrEmpty(Request.QueryString["Category"]))
                    {
                        catid = Convert.ToInt32(Request.QueryString["Category"]);
                    }

                    if (StatusFlagID == -1)
                    {
                        if (IsDeptHead == 0)
                            ComplianceTypeID = 0;//statutory
                        else
                            ComplianceTypeID = 1;//statutoryDept
                    }
                }
            }
        }
        public static string GetCustomerName(int CID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                string CName = (from row in entities.CustomerViews
                                where row.ID == CID
                                select row.Name).Single();

                return CName;
            }
        }
    }
}