﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Configuration;
using System.Web;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class DownloadErrorFile : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string name = Request.QueryString["fn"].ToString();

            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(Convert.ToString(ConfigurationManager.AppSettings["DiyStorageconnectionstring"]));
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
            CloudBlobContainer container = blobClient.GetContainerReference("diy");
            CloudBlockBlob blockBlob = container.GetBlockBlobReference(name);
            System.IO.MemoryStream memStream = new System.IO.MemoryStream();
            blockBlob.DownloadToStream(memStream);
            HttpResponse response = HttpContext.Current.Response;
            response.ContentType = blockBlob.Properties.ContentType;
            response.AddHeader("Content-Disposition", "Attachment; filename=" + name);
            response.AddHeader("Content-Length", blockBlob.Properties.Length.ToString());
            response.BinaryWrite(memStream.ToArray());
        }
    }
}