﻿<%@ Page Title="Vendor Audit:: My Report" Language="C#" MasterPageFile="~/RLCSCompliance.Master" AutoEventWireup="true" CodeBehind="RLCS_VendorAuditReport_Client.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.RLCS_VendorAuditReport_Client" %>
<%@ Register assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" namespace="System.Web.UI.DataVisualization.Charting" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <style type="text/css">
        .customDropDownCheckBoxCSS {
            height: 32px !important;
            width: 100%;
        }

        .AddNewspan1 {
            padding: 5px;
            border: 2px solid #f4f0f0;
            border-radius: 51px;
            display: inline-block;
            height: 26px;
            width: 26px;
        }

        .modal-header-custom {
            display: block;
            float: left;
            font-size: 20px;
            color: #1fd9e1; /*width: 225px;*/
            font-weight: 400;
        }
    </style>
    <script type="text/javascript">
        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);
        window.onunload = function () { };
        $(document).ready(function () {
            BindControls();
            setactivemenu('leftnoticesmenu1');
            fhead('My Report');
        });

        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(InitializeRequest);
            prm.add_endRequest(EndRequest);
            if ($("#<%=tbxFilterLocation.ClientID %>") != null) {
                $("#<%=tbxFilterLocation.ClientID %>").unbind('click');

                $("#<%=tbxFilterLocation.ClientID %>").click(function () {
                    $("#divBranches").toggle("blind", null, 500, function () { });
                });
            }
            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            });

            $(document).tooltip({ selector: '[data-toggle="tooltip"]' });
        });
        function InitializeRequest(sender, args) { }
        function EndRequest(sender, args) { }
        function hideDivBranch() {
            $('#divBranches').hide("blind", null, 0, function () { });
        }
        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }
       
        function closeModal() {
            document.getElementById('<%= lnkBtnBindGrid.ClientID %>').click();
        }
        function BindControls() {

            $(function () {
                $('input[id*=txtAuditDate]').datepicker(
                    {
                        dateFormat: 'd-M-yy'
                    });
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   <div class="row Dashboard-white-widget">
        <div class="dashboard">
               <div class="col-md-12 colpadding0" style="margin-bottom: 41px;">
                 <div class="col-md-4 colpadding0">
                <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                    class="alert alert-block alert-danger fade in" />
                        </div>
                                      <div class="col-md-6">
                                          </div>
            </div>

            <div class="clearfix"></div>
            <div class="clearfix"></div>
            <div class="row">

                <%--  <div class="col-md-4" id="CustomerList1" runat="server">
                  <asp:DropDownListChosen runat="server" ID="ddlFilterCustomer"
                        AllowSingleDeselect="false" DisableSearchThreshold="5"
                        DataPlaceHolder="Select Customer"
                        class="form-control" Width="100%" Enabled="true"
                        OnSelectedIndexChanged="ddlFilterCustomer_SelectedIndexChanged"
                        AutoPostBack="true" />
                    <asp:RequiredFieldValidator ID="rfvCustomer" ErrorMessage="Please Select Customer"
                        ControlToValidate="ddlFilterCustomer" runat="server" ValidationGroup="LicenseListPageValidationGroup" Display="None" />
                </div>--%>
                <div class="col-md-4">
                    <asp:TextBox runat="server" ID="tbxFilterLocation" PlaceHolder="Select Entity/State/Location/Branch" autocomplete="off"
                        CssClass="clsDropDownTextBox" style=" width: 100%;" />
                    <div style="margin-left: 1px; position: absolute; z-index: 10; overflow-y: auto; height: 200px; width: 95%" id="divBranches">
                        <asp:TreeView runat="server" ID="tvFilterLocation" SelectedNodeStyle-Font-Bold="true" NodeStyle-ForeColor="#8e8e93"
                            Style="margin-top: -5%; overflow: auto; border-left: 1px solid #c7c7cc; border-right: 1px solid #c7c7cc; border-bottom: 1px solid #c7c7cc; background-color: #ffffff; color: #8e8e93 !important;" ShowLines="true"
                            OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged">
                        </asp:TreeView>
                    </div>
                </div>
                <div class="col-md-2">
                    <asp:DropDownListChosen runat="server" ID="ddlVendorListBase"
                        AllowSingleDeselect="false" DisableSearchThreshold="5"
                        DataPlaceHolder="Select Vendor"
                        class="form-control" Width="100%" Enabled="true"
                        OnSelectedIndexChanged="ddlVendorListBase_SelectedIndexChanged"
                        AutoPostBack="true" />
                </div>
                   <div class="col-md-2">                      
                          <asp:DropDownList runat="server" ID="ddlFinYear" class="form-control m-bot15" OnSelectedIndexChanged="ddlFinYear_SelectedIndexChanged"                            AutoPostBack="true">
                            <asp:ListItem Text="Year" Value="-1" Selected="True" />
                            <asp:ListItem Text="2016" Value="2016"  />
                           <asp:ListItem Text="2017" Value="2017"  />
                            <asp:ListItem Text="2018" Value="2018" />
                            <asp:ListItem Text="2019" Value="2019" />
                            <asp:ListItem Text="2020" Value="2020" />
                            <asp:ListItem Text="2021" Value="2021" />
                        </asp:DropDownList>
                        </div>
                    <div class="col-md-2">
                      <asp:DropDownList runat="server" ID="ddlFrequency" class="form-control m-bot15" OnSelectedIndexChanged="ddlFrequency_SelectedIndexChanged"
                            AutoPostBack="true">
                            <asp:ListItem Text="Frequency" Value="-1" Selected="True" />
                            <asp:ListItem Text="Annually" Value="A"  />
                           <asp:ListItem Text="Half Yearly" Value="H"  />
                            <asp:ListItem Text="Quarterly" Value="Q" />
                            <asp:ListItem Text="Monthly" Value="M" />
                        </asp:DropDownList>
                 </div>
                   

                <div class="col-md-2">
                     <asp:LinkButton Text="OverAll Report" CssClass="btn btn-primary" runat="server"
                         data-toggle="tooltip" ToolTip="Download Vendor Audit Status Report"
                        ID="lnkBtnOverAllExcel" OnClick="lnkBtnOverAllExcel_Click"
                        Style="float: right;width: 89%;" />
                    </div>

                <div class="col-md-2" style="display: none;">
                    <asp:LinkButton Text="Apply" CssClass="btn btn-primary" runat="server"
                        ID="lnkBtnApplyFilter" OnClick="lnkBtnApplyFilter_Click"
                        Style="float: right;" />

                    <asp:LinkButton ID="lnkBtnBindGrid" OnClick="lnkBtnBindGrid_Click" Style="float: right; display: none;" Width="100%" runat="server">
                    </asp:LinkButton>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <asp:DropDownListChosen runat="server" ID="ddlPeriod" OnSelectedIndexChanged="ddlPeriod_SelectedIndexChanged"
                    AllowSingleDeselect="false" DisableSearchThreshold="5"
                    DataPlaceHolder="Select Period"
                    class="form-control" Width="150px" Enabled="true"
                    AutoPostBack="true">
                    </asp:DropDownListChosen>
                </div>
                <div class="col-md-1" >                     
                      <asp:TextBox runat="server" ID="txtInvoiceNo" PlaceHolder="Invoice No" AutoPostBack="true" OnTextChanged="txtInvoiceNo_TextChanged" autocomplete="off" CssClass="clsDropDownTextBox" style="width: 141%;margin-left: -13px;height: 34px;" />
                </div>
                <div class="col-md-2">
                     <asp:TextBox ID="txtAuditDate" OnTextChanged="txtAuditDate_TextChanged" class="form-control endate" PlaceHolder="Start Date" runat="server" AutoPostBack="true"   >
                    </asp:TextBox>
                </div>
                <div class="col-md-7">
                     <asp:LinkButton Text="Summary Report" CssClass="btn btn-primary" runat="server"
                         data-toggle="tooltip" ToolTip="Download Vendor Audit Status Report"
                        ID="lnkBtnExcel" OnClick="lnkBtnExcel_Click"
                        Style="float: right;" />
                    </div>
            </div>

            <div class="row">
                <div class="col-md-12 colpadding0">
                    <asp:GridView runat="server" ID="grdChecklistDetails" AutoGenerateColumns="false" AllowSorting="true"
                        ShowHeaderWhenEmpty="true" OnRowCommand="grdChecklistDetails_RowCommand" OnRowDataBound="grdChecklistDetails_RowDataBound"
                        PageSize="10" AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%">
                        <Columns>
                            <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="2%">
                                <ItemTemplate>
                                    <%#Container.DataItemIndex+1 %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Entity/Client" ItemStyle-Width="10%">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px">
                                        <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ClientName") %>' ToolTip='<%# Eval("ClientName") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Branch" ItemStyle-Width="8%">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px">
                                        <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("CustomerBranchName") %>' ToolTip='<%# Eval("CustomerBranchName") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Vendor" ItemStyle-Width="8%">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                        <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("VendorName") %>'
                                            ToolTip='<%# Eval("VendorName") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="InvoiceNo" ItemStyle-Width="10%">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; ">
                                        <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("InVoiceNo") %>'
                                            ToolTip='<%# Eval("InVoiceNo") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Auditor" ItemStyle-Width="8%">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                        <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("AuditorName") %>' ToolTip='<%# Eval("AuditorName") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Start Date" ItemStyle-Width="5%">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; ">
                                        <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("StartDate")!= null ?  Convert.ToDateTime(Eval("StartDate")).ToString("dd-MMM-yyyy") : ""%>'
                                            ToolTip='<%# Eval("StartDate")!= null ?  Convert.ToDateTime(Eval("StartDate")).ToString("dd-MMM-yyyy") : ""%>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Period" ItemStyle-Width="8%">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                        <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ForMonth") %>'  ToolTip='<%# Eval("ForMonth") %>' ></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                               <asp:TemplateField HeaderText="Status" ItemStyle-Width="8%">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                        <asp:Label ID="lblStatus" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("StatusName") %>'  ToolTip='<%# Eval("StatusName") %>' ></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Action" ItemStyle-Width="10%" HeaderStyle-CssClass="text-right" ItemStyle-CssClass="text-right">
                                <ItemTemplate>

                                    <asp:Button runat="server" ID="btnChangeStatus" Text="Excel" CssClass="btn btn-primary" CommandName="CHANGE_STATUS" data-toggle="tooltip" ToolTip="Download Vendor Audit Status Report"
                                        CommandArgument='<%# Eval("AuditID") + "," + Eval("AVACOM_BranchID") +","+ Eval("Avacom_VendorID")+","+ Eval("ScheduleonID")+","+Eval("AuditorName")+","+Eval("ClientName")+","+ Eval("ForMonth")+","+ Eval("AuditedDate")+","+ Eval("StartDate")+","+ Eval("EndDate") %>' />                                    

                                     <asp:Button runat="server" ID="btnExportToPdf" Text="PDF" CssClass="btn btn-primary" EnableEventValidation="false"  CommandName="CHANGE_STATUS_PDF" data-toggle="tooltip" ToolTip="Download Vendor Audit Status Report"
                                                CommandArgument='<%# Eval("AuditID") + "," + Eval("AVACOM_BranchID") +","+ Eval("Avacom_VendorID")+","+ Eval("ScheduleonID")+","+Eval("AuditorName")+","+Eval("ClientName")+","+ Eval("ForMonth")+","+ Eval("AuditedDate")+","+ Eval("StartDate")+","+ Eval("EndDate") %>' />                              
                                   
                                    <asp:Button runat="server" ID="btnDraftReport" Text="Draft report" CssClass="btn btn-primary" EnableEventValidation="false"  CommandName="CHANGE_STATUS_DraftPDF" data-toggle="tooltip" ToolTip="Download Draft  Report"
                                                CommandArgument='<%# Eval("AuditID") + "," + Eval("AVACOM_BranchID") +","+ Eval("Avacom_VendorID")+","+ Eval("ScheduleonID")+","+Eval("AuditorName")+","+Eval("ClientName")+","+ Eval("ForMonth")+","+ Eval("AuditedDate")+","+ Eval("StartDate")+","+ Eval("EndDate") %>' />      
                                    
                                </ItemTemplate>
                            </asp:TemplateField>


                        </Columns>
                        <RowStyle CssClass="clsROWgrid" />
                        <HeaderStyle CssClass="clsheadergrid" />
                        <PagerSettings Visible="false" />
                        <PagerTemplate>
                        </PagerTemplate>
                        <EmptyDataTemplate>
                            No Record Found
                        </EmptyDataTemplate>
                    </asp:GridView>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 colpadding0">
                    <div class="col-md-8 colpadding0">
                        <div runat="server" id="DivRecordsScrum" style="float: left; margin-top: 5px; color: #999">
                            <p style="padding-right: 0px !Important;">
                                <asp:Label ID="Label4" runat="server" Text="Showing "></asp:Label>
                                <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>- 
                                            <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                                            <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                            </p>
                        </div>
                    </div>

                    <div class="col-md-2 colpadding0">
                        <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 37%; float: right; height: 32px !important"
                            AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                            <asp:ListItem Text="5" />
                            <asp:ListItem Text="10" Selected="True" />
                            <asp:ListItem Text="20" />
                            <asp:ListItem Text="50" />
                        </asp:DropDownList>
                    </div>

                    <div class="col-md-2 colpadding0" style="float: right;">
                        <div style="float: left; width: 60%">
                            <p class="clsPageNo">Page</p>
                        </div>
                        <div style="float: left; width: 40%">
                            <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true" DataPlaceHolder="No" AllowSingleDeselect="false"
                                OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged" class="form-control m-bot15" Width="100%" Height="30px">
                            </asp:DropDownListChosen>
                        </div>
                    </div>
                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                </div>
            </div>

        </div>
    </div>

    <div class="col-md-12 colpadding0 text-right">
 
    </div>
    
        <asp:chart id="Chart1" runat="server"  Height="250px" Width="350px"  CssClass="hidden">
                  <titles>
                    <asp:Title ShadowOffset="3" Name="Title1" />
                  </titles>
                      <legends>
                        <asp:Legend Alignment="Center" Docking="Bottom"
                                    IsTextAutoFit="False" Name="Default"
                                    LegendStyle="Row" />
                      </legends>
                  <series>
                    <asp:Series Name="Default" IsValueShownAsLabel="true"/>
                  </series>
                      <chartareas>
                        <asp:ChartArea Name="ChartArea1"
                                         BorderWidth="0" />
                      </chartareas>
        </asp:chart>

     <asp:chart id="Chart2" runat="server"  Height="250px" Width="350px"  CssClass="hidden">
                  <titles>
                    <asp:Title ShadowOffset="3" Name="Title2" />
                  </titles>
                      <legends>
                        <asp:Legend Alignment="Center" Docking="Bottom"
                                    IsTextAutoFit="False" Name="Default"
                                    LegendStyle="Row" />
                      </legends>
                  <series>
                    <asp:Series Name="Default" IsValueShownAsLabel="true"/>
                  </series>
                      <chartareas>
                        <asp:ChartArea Name="ChartArea2"
                                         BorderWidth="0" />
                      </chartareas>
        </asp:chart>

    <asp:Chart ID="Chart3" runat="server" Height="300px" Width="500px" CssClass="hidden">
        <Titles>
            <asp:Title ShadowOffset="2" Name="Items" />
        </Titles>
        <Legends>
            <asp:Legend Alignment="Center" Docking="Bottom" IsTextAutoFit="False" Name="Default"
                LegendStyle="Row" />
        </Legends>
        <ChartAreas>
            <asp:ChartArea Name="ChartArea3" BorderWidth="0" />
        </ChartAreas>
    </asp:Chart>

    <asp:Chart ID="Chart4" runat="server" Height="300px" Width="500px" CssClass="hidden">
        <Titles>
            <asp:Title ShadowOffset="2" Name="Items" />
        </Titles>
        <Legends>
            <asp:Legend Alignment="Center" Docking="Bottom" IsTextAutoFit="False" Name="Default"
                LegendStyle="Row" />
        </Legends>
        <ChartAreas>
            <asp:ChartArea Name="ChartArea4" BorderWidth="0" />
        </ChartAreas>
    </asp:Chart>
</asp:Content>

