﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RLCS_MyDueUpcomingCompliance.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.RLCS_MyDueUpcomingCompliance" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <link href="../NewCSS/stylenew.css" rel="stylesheet" />
    <link href="../NewCSS/bootstrap.min.css" rel="stylesheet" />
    <link href="../NewCSS/bootstrap-theme.css" rel="stylesheet" />    

    <script type="text/javascript" src="../Newjs/jquery-1.8.3.min.js"></script>

    <style type="text/css">
        .ui-widget-header {
            border: 0px !important;
            background: inherit;
            font-size: 20px;
            color: #666666;
            font-weight: normal;
            padding-top: 0px;
            margin-top: 5px;
        }

        .table > tbody > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

            .table > tbody > tr > th > a {
                vertical-align: bottom;
                color: #666666 !important;
                text-decoration: none !important;
                border-bottom: 2px solid #dddddd;
            }

        .table tbody > tr > td, .table tbody > tr > th, .table tfoot > tr > td, .table tfoot > tr > th, .table thead > tr > td, .table thead > tr > th {
            padding: 5px;
            line-height: 1.428571429;
            vertical-align: middle;
        }

        .table tr th {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .clsheadergrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: bottom !important;
            border-bottom: 2px solid #dddddd !important;
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
            text-align: left;
        }

        .clsROWgrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: top !important;
            border-top: 1px solid #dddddd !important;
            color: #666666 !important;
            font-size: 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }

        .m-10 {
            margin-top: 10px;
            margin-left: 10px;
        }

        .btn-search {
            background: #1fd9e1;
            float: right;
        }

        .chosen-results {
            max-height: 150px !important;
        }

        .table, pre.prettyprint {
     margin-bottom: 0px; 
}
        .k-loading-image {
        display: none;
    } 
        
    </style>
    <style>
       .k-loading-mask {
            width: 0px;
            height: 0px;
        }
    </style>
    <script type="text/javascript">
        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }
        //$(document).ready(function () {
        //    debugger;
        //  $('.k-loading-mask').hide();
        //    $('#windowduetoday').children().first().remove();
        //    //kendo.ui.progress($("#customerSettings"), false);
        //})
        //$(window).load(function () {
        //    $('.k-loading-mask').hide();
        //    window.parent.loaderhide();
        //    $('#windowduetoday').children().first().remove();
        //    alert("reached");
        //    $('#windowduetoday').children('.k-loading-mask').hide();
        //});
        //window.parent.loaderhide();
    </script>
</head> 
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

        <asp:UpdatePanel ID="UpDetailView" runat="server" UpdateMode="Conditional" OnLoad="upDivLocation_Load">
            <ContentTemplate>
                <asp:UpdateProgress ID="updateProgress" runat="server">
                    <ProgressTemplate>
                        <div id="updateProgress1" style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                            <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                                AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
                        </div>
                    </ProgressTemplate>
                </asp:UpdateProgress>

                <div class="row col-md-12 col-sm-12" style="margin-top:20px">
                    <div class="col-md-1 col-sm-1" style="padding-left: 0px;">
                        <asp:DropDownListChosen runat="server" ID="ddlPageSize" class="" Style="width: 100%; float: left"
                            AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                            <asp:ListItem Text="10" Selected="True" />
                            <asp:ListItem Text="20" />
                            <asp:ListItem Text="50" />
                        </asp:DropDownListChosen>
                    </div>

                    <div class="col-md-3 col-sm-3" style="padding-left: 0px;">
                        <asp:TextBox runat="server" ID="tbxFilterLocation" Style="padding: 0px; padding-left: 10px; margin: 0px; height: 33px; width: 100%; border: 1px solid #c7c7cc; border-radius: 4px; color: #8e8e93;"
                            CssClass="txtbox" />
                        <div style="margin-left: 1px; position: absolute; z-index: 10; margin-top: -20px;" id="divFilterLocation">
                            <asp:TreeView runat="server" ID="tvFilterLocation" SelectedNodeStyle-Font-Bold="true" Width="279px" NodeStyle-ForeColor="#8e8e93"
                                Style="overflow: auto; border-left: 1px solid #c7c7cc; border-right: 1px solid #c7c7cc; border-bottom: 1px solid #c7c7cc; background-color: #ffffff; color: #8e8e93 !important;" ShowLines="true"
                                OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged">
                            </asp:TreeView>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-3" style="padding-left: 0px;">
                        <asp:DropDownListChosen runat="server" ID="ddlAct" class="form-control" Width="100%" DataPlaceHolder="Act">
                        </asp:DropDownListChosen>
                    </div>

                    <div class="col-md-2 col-sm-2" style="padding-left: 0px;">
                        <asp:DropDownListChosen runat="server" ID="ddlmonth" class="form-control" Width="100%" DataPlaceHolder="Month">
                            <asp:ListItem Text="Month" Selected="True" Value="-1" />
                               <asp:ListItem Text="January" Value="01" />
                               <asp:ListItem Text="February" Value="02" />
                               <asp:ListItem Text="March" Value="03" />
                               <asp:ListItem Text="April" Value="04" />
                               <asp:ListItem Text="May" Value="05" />
                               <asp:ListItem Text="June" Value="06" />
                               <asp:ListItem Text="July" Value="07" />
                               <asp:ListItem Text="August" Value="08" />
                               <asp:ListItem Text="September" Value="09" />
                               <asp:ListItem Text="October" Value="10" />
                               <asp:ListItem Text="November" Value="11" />
                               <asp:ListItem Text="December" Value="12" />
                        </asp:DropDownListChosen>
                    </div>

                    <div class="col-md-1 col-sm-1" style="padding-left: 0px;">
                        <asp:DropDownListChosen runat="server" ID="ddlyear" class="form-control" Width="100%" DataPlaceHolder="Year">
                                        <asp:ListItem Text="Year" Selected="True" Value="-1" />
                                        <asp:ListItem Text="2021" Value="2021" />
                                        <asp:ListItem Text="2020" Value="2020" />
                                        <asp:ListItem Text="2019" Value="2019" />
                                        <asp:ListItem Text="2018" Value="2018" />
                                        <asp:ListItem Text="2017" Value="2017" />
                                        <asp:ListItem Text="2016" Value="2016" />
                                        <asp:ListItem Text="2015" Value="2015" />
                                        <asp:ListItem Text="2014" Value="2014" />
                                        <asp:ListItem Text="2013" Value="2013" />
                                        <asp:ListItem Text="2012" Value="2012" />
                                        <asp:ListItem Text="2011" Value="2011" />
                                        <asp:ListItem Text="2010" Value="2010" />
                                        <asp:ListItem Text="2009" Value="2009" />
                                        <asp:ListItem Text="2008" Value="2008" />
                                        <asp:ListItem Text="2007" Value="2007" />
                                        <asp:ListItem Text="2006" Value="2006" />
                                        <asp:ListItem Text="2005" Value="2005" />
                                        <asp:ListItem Text="2004" Value="2004" />
                                        <asp:ListItem Text="2003" Value="2003" />
                                        <asp:ListItem Text="2002" Value="2002" />
                                        <asp:ListItem Text="2001" Value="2001" />
                                        <asp:ListItem Text="2000" Value="2000" />
                        </asp:DropDownListChosen>
                    </div>

                    <div class="col-md-1 col-sm-1 text-right" style="padding-right: 0px;">
                        <asp:Button ID="btnSearch" CausesValidation="false" class="btn btn-primary" runat="server" Text="Apply" OnClick="btnSearch_Click" />
                    </div>

                    <div class="col-md-1 col-sm-1 text-right">
                        <asp:LinkButton runat="server" CssClass="btn" ID="lbtnExportExcel"
                            Style="background: url('../images/Excel _icon.png'); float: right; width: 47px; height: 33px;"
                            toggle="tooltip" data-placement="bottom" Title="Export All to Excel" ToolTip="Export All to Excel" OnClick="lbtnExportExcel_Click">                                                  
                        </asp:LinkButton>
                        <%----%>
                    </div>
                </div>

                <div class="row col-md-12 col-sm-12 AdvanceSearchScrum" style="margin-bottom:0px;">
                    <div id="divAdvSearch" runat="server" visible="false">
                        <p>
                            <asp:Label ID="lblAdvanceSearchScrum" runat="server" Text=""></asp:Label>
                        </p>
                        <p>
                            <asp:LinkButton ID="lnkClearAdvanceList" OnClick="lnkClearAdvanceSearch_Click" runat="server">Clear Search</asp:LinkButton>
                        </p>
                    </div>
                    <div runat="server" id="DivRecordsScrum" style="float: right;">
                        <p style="padding-right: 0px !Important;">
                            <asp:Label ID="Label4" runat="server" Text="Showing "></asp:Label>
                            <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>- 
                                <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                                <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                        </p>
                    </div>
                </div>

                <div style="margin-bottom: 0px; font-weight: bold; font-size: 12px;">
                    <asp:Label runat="server" ID="lblDetailViewTitle"></asp:Label>
                </div>

                <div class="row col-md-12">
                    <asp:GridView runat="server" ID="GridStatutory" AutoGenerateColumns="false" AllowSorting="true" GridLines="None"
                        CssClass="table" AllowPaging="true" PageSize="10" Width="100%" ShowHeaderWhenEmpty="true">
                        <Columns>
                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Sr">
                                <ItemTemplate>
                                    <%#Container.DataItemIndex+1 %>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Branch/Code">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                        <asp:Label ID="lblBranchName" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("CustomerBranchName") %>' ToolTip='<%# Eval("CustomerBranchName") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>

                           <asp:TemplateField HeaderText="Compliance">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 350px;">
                                        <asp:Label ID="lblCompliance" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ShortForm") %>' ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Form">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 90px;">
                                        <asp:Label ID="lblform" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("RequiredForms") %>' ToolTip='<%# Eval("RequiredForms") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Frequency">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 90px;">
                                        <asp:Label ID="lblFrequency" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Frequency") %>' ToolTip='<%# Eval("Frequency") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
			                <asp:TemplateField HeaderText="Period">
                                <ItemTemplate>
                                    <asp:Label ID="lblPeriod" runat="server" data-toggle="tooltip" data-placement="bottom"
                                        Text='<%# Eval("ForMonth") %>' ToolTip='<%# Eval("ForMonth") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                             <asp:TemplateField HeaderText="Due Date">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                    <asp:Label ID="lblUser" runat="server" data-toggle="tooltip" data-placement="bottom" 
                                        Text='<%# Eval("ScheduledOn")!= null?((DateTime)Eval("ScheduledOn")).ToString("dd-MMM-yyyy"):""%>' 
                                        ToolTip='<%# Eval("ScheduledOn")!= null?((DateTime)Eval("ScheduledOn")).ToString("dd-MMM-yyyy"):""%>'></asp:Label>
                                        </div>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Status" Visible="false">
                                <ItemTemplate>
                                    <%--<div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px;">--%>
                                        <asp:Label ID="lblStatus" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ComplianceStatusName") %>' ToolTip='<%# Eval("ComplianceStatusName") %>'></asp:Label>
                                    <%--</div>--%>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>

                        <RowStyle CssClass="clsROWgrid" />
                        <HeaderStyle CssClass="clsheadergrid" />

                        <PagerTemplate>
                            <table style="display: none">
                                <tr>
                                    <td>
                                        <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                    </td>
                                </tr>
                            </table>
                        </PagerTemplate>

                        <EmptyDataTemplate>
                            No Records Found.
                        </EmptyDataTemplate>
                    </asp:GridView>
                </div>

                <div class="clearfix"></div>

                <div class="col-md-12 col-sm-12 colpadding0">
                    <div class="col-md-6 col-sm-6 colpadding0" style="float: right; margin-right:12px;">
                        <div class="table-paging" style="margin-bottom: 2px;">
                            <asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick="Previous_Click" />

                            <div class="table-paging-text">
                                <p>
                                    <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                            <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>
                                </p>
                            </div>

                            <asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick="Next_Click" />
                            <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                        </div>
                    </div>                    
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="lbtnExportExcel" />
            </Triggers>
        </asp:UpdatePanel>

        <script type="text/javascript" src="../Newjs/bootstrap.min.js"></script>

        <script>
            //$('#updateProgress1').show();

            $(document).ready(function () {
                $(document).tooltip({ selector: '[data-toggle="tooltip"]' });
                
                if ($(window.parent.document).find("#windowduetoday") != null && $(window.parent.document).find("#windowduetoday") != undefined)
                    if ($(window.parent.document).find("#windowduetoday").children().first().hasClass("k-loading-mask"))
                        $(window.parent.document).find("#windowduetoday").children().first().hide();
            });
            
            function HideLoaderDiv() {
               
                window.parent.loaderhide();
                
            }
        </script>
    </form>
</body>
</html>
