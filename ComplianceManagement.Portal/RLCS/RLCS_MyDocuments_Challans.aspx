﻿<%@ Page Title="Challans :: My Documents" Language="C#" AutoEventWireup="true" MasterPageFile="~/RLCSCompliance.Master" CodeBehind="RLCS_MyDocuments_Challans.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.RLCS_MyDocuments_Challans" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../Newjs/jszip.min.js"></script>

<%--<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.2.620/styles/kendo.common.min.css" />
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.2.620/styles/kendo.rtl.min.css" />
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.2.620/styles/kendo.silver.min.css" />
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.2.620/styles/kendo.mobile.all.min.css" />

    <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="https://kendo.cdn.telerik.com/2018.2.620/js/kendo.all.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.4.0/jszip.min.js"></script>--%>

    <link href="../NewCSS/Kendouicss.css" rel="stylesheet" />

    <title>Challans::My Documents</title>

    <style>
        .k-grid-content {
            min-height: 250px;
        }
    </style>

    <script id="templateTooltip" type="text/x-kendo-template">
        <div>
            <div> #:value ? value : "N/A" #</div>
        </div>
    </script>

    <script id="fileTemplate" type="text/x-kendo-template">                 
        <span class='k-progress'></span>
            <div class='file-wrapper'>  
        <span class='k-icon k-i-file-txt k-i-txt'></span>#=FileName# 
    </script>

    <script type="text/javascript">
        $(document).ready(function () {



            
              function startChange() {
                debugger;
                $("#Enddatepicker").data("kendoDatePicker").value(null);

                var dt = new Date('<% =newRegistersMonth%>/30/<% =newRegistersYear%>');
                //var dt = new Date("05/31/2018");
                var dtnew = new Date("05/30/2050");
                var startDate = start.value(),
                    endDate = end.value();
                if (startDate) {
                    startDate = new Date(startDate);
                    startDate.setDate(startDate.getDate());

                    end.min(startDate);                    
                    //if ((startDate.getMonth() >= 05 && startDate.getFullYear() == 2018) || startDate.getFullYear() > 2018) {
                    if ((startDate.getMonth() >= <% =newRegistersMonth%> && startDate.getFullYear() == <% =newRegistersYear%>) || startDate.getFullYear() > <% =newRegistersYear%>) {
                        end.max(new Date(dtnew));
                    }
                    else {
                        end.max(new Date(dt));
                    }
                    //endDate.setDate(startDate.getDate());
                } else if (endDate) {
                    start.max(new Date(endDate));
                } else {
                    endDate = new Date();
                    start.max(endDate);
                    end.min(endDate);
                }

                var y = startDate.getFullYear();
                if (y == 2021) {
                    y = 0;
                }
                else if (y == 2020) {
                    y = 1;
                }
		else if (y == 2019) {
                    y = 2;
                }
                else if (y == 2018) {
                    y = 3;
                }
                else if (y == 2017) {
                    y = 4;
                }
                else if (y == 2016) {
                    y = 5;
                }
                $("#ddlMonth").data("kendoDropDownList").select(parseInt(startDate.getMonth()));
                $("#ddlYear").data("kendoDropDownList").select(y);
                dropDownFilter();
            }

            function endChange() {

                var endDate = end.value(),
                    startDate = start.value();

                if (endDate) {
                    endDate = new Date(endDate);
                    endDate.setDate(endDate.getDate());
                    //start.max(endDate);
                } else if (startDate) {
                    end.min(new Date(startDate));
                } else {
                    endDate = new Date();
                    //start.max(endDate);
                    end.min(endDate);
                }

                dropDownFilter();
            }

            var start = $("#Startdatepicker").kendoDatePicker({
                start: "year",
                // defines when the calendar should return date
                depth: "year",
                // display month and year in the input
                format: "MMMM yyyy",
                // specifies that DateInput is used for masking the input element
                value: new Date(),
                dateInput: true,
                change: startChange
            }).data("kendoDatePicker");

            var end = $("#Enddatepicker").kendoDatePicker({
                start: "year",
                // defines when the calendar should return date
                depth: "year",
                // display month and year in the input
                format: "MMMM yyyy",
                // specifies that DateInput is used for masking the input element
              //  value: new Date(),
                dateInput: true,
                change: endChange
            }).data("kendoDatePicker");

            start.max(end.value());
            end.min(start.value());



            setactivemenu('leftnoticesmenu');
            fhead('My Documents/ Challans');
		try{
                ApiTrack_Activity("Challans", "pageView", null);
		}catch(e){}
        });

        $(document).ready(function () {

            var d = new Date();
            var n = d.getMonth();
            var y = d.getFullYear();

            BindLocationFilter();
            BindMonth();
            BindYear();
            BindChallanType();

            if (n == 0) {
                y = y - 1;
                n = 12;
            }
            
                if (y == 2021) {
                    y = 0;
                }
            else if (y == 2020) {
                y = 1;
            }            
            else if (y == 2019) {
                y = 2;
            }            
            else if (y == 2018) {
                y = 3;
            }
            else if (y == 2017) {
                y = 4;
            }
            else if (y == 2016) {
                y = 5;
            }
          
            $("#ddlMonth").data("kendoDropDownList").select(parseInt(n-1));
            $("#ddlYear").data("kendoDropDownList").select(y);
           
            dropDownFilter();
            // BindFirstGrid();
            //BindSecondGrid();
              
            // monthYearFilter_Grid_Historical();

            //$("#grid tbody").on("click", "tr", function (e) {
            //    var rowElement = this;
            //    var row = $(rowElement);
            //    var grid = $("#grid").getKendoGrid();
            //    if (row.hasClass("k-state-selected")) {
            //        var selected = grid.select();
            //        selected = $.grep(selected, function (x) {
            //            var itemToRemove = grid.dataItem(row);
            //            var currentItem = grid.dataItem(x);
            //            return itemToRemove.ID != currentItem.ID
            //        })
            //        grid.clearSelection();
            //        grid.select(selected);
            //        //e.stopPropagation();
            //    } else {
            //        grid.select(row)
            //        //e.stopPropagation();
            //    }
            //});
        });

        function dropDownFilter() {
         debugger;
            $('#ClearfilterMain').css('display', 'none');
            $('#dvbtndownloadDocumentMain').css('display', 'none');
            $('input[name="sel_chkbx"]').attr("checked", false);
            $('input[name="sel_chkbx_Main"]').attr("checked", false);

            var month = $("#ddlMonth").val();
            var year = $("#ddlYear").val();
            if (year > <% =newRegistersYear%>) {
                var grid = $('#grid1').data("kendoGrid"); 
                if (grid != undefined || grid != null)
                    $('#grid1').empty();
                BindSecondGrid();
                $('#divddlChallanType').css('display', 'block');
            //commented by shraddha
               //  $('#divddlEntitySubEntityLocation').css('display', 'none');
                //monthYearFilter_Grid_latest();
               
            }
            else if ((month > <% =newRegistersMonth%>) && (year >= <% =newRegistersYear%>)) {
                var grid = $('#grid1').data("kendoGrid"); 
                if (grid != undefined || grid != null)
                    $('#grid1').empty();
                BindSecondGrid();
                $('#divddlChallanType').css('display', 'block');
                //commented by shraddha
                //   $('#divddlEntitySubEntityLocation').css('display', 'none');
                //monthYearFilter_Grid_latest();
               
            } else if (year <  <% =newRegistersYear%> || (month <=  <% =newRegistersMonth%> && year <=  <% =newRegistersYear%>)) {
                //commented by shraddha
                //   $('#divddlEntitySubEntityLocation').css('display', 'block');
                $('#divddlChallanType').css('display', 'none');
                var grid = $('#grid').data("kendoGrid"); 
                if (grid != undefined || grid != null)
                    $('#grid').empty();
                BindFirstGrid();

                $("#grid tbody").on("click", "tr", function (e) {
                    var rowElement = this;
                    var row = $(rowElement);
                    var grid = $("#grid").getKendoGrid();
                    if (row.hasClass("k-state-selected")) {
                        var selected = grid.select();
                        selected = $.grep(selected, function (x) {
                            var itemToRemove = grid.dataItem(row);
                            var currentItem = grid.dataItem(x);
                            return itemToRemove.ID != currentItem.ID
                        })
                        grid.clearSelection();
                        grid.select(selected);
                        
                    } else {
                        grid.select(row);
                    }
                });

                // monthYearFilter_Grid_Historical();
               
            } else {
                ClearAllFilter();
            }
        }

        function monthYearFilter_Grid_Historical() {
            if ($('#ddlMonth') != null && $('#ddlYear') != null) {
                var filter = { logic: "and", filters: [] };
                filter.filters.push({
                    field: "PayrollMonth", operator: "eq", value: parseInt($('#ddlMonth').val())
                });
                filter.filters.push({
                    field: "PayrollYear", operator: "eq", value: parseInt($('#ddlYear').val())
                });
                var dataSource = $("#grid").data("kendoGrid").dataSource;
                dataSource.filter(filter);
            }
        }

        function monthYearFilter_Grid_latest() {
            if ($('#ddlMonth') != null && $('#ddlYear') != null) {
                var filter = { logic: "and", filters: [] };
                filter.filters.push({
                    field: "PayrollMonth", operator: "eq", value: parseInt($('#ddlMonth').val())
                });
                filter.filters.push({
                    field: "PayrollYear", operator: "eq", value: parseInt($('#ddlYear').val())
                });
                var dataSource = $("#grid1").data("kendoGrid").dataSource;
                dataSource.filter(filter);
            }
        }

        $(document).on("click", "#grid tbody tr .ob-download", function (e) {          
            var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
            OpenDownloadOverviewpupfirst(item.RecordID, "SOW04", "Download");
            analytics("Challans","Download",$('#CName').val(),1);
                try{
                ApiTrack_Activity("Challans", "Download", null,item.RecordID);
                }catch(e){}
            return true;
        });



        $(document).on("click", "#grid tbody tr .ob-overview", function (e) {
            var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
            OpenDocumentOverviewpupfirst(item.RecordID, "SOW04");
            analytics("Challans","OverView",$('#CName').val(),1);
                try{
                ApiTrack_Activity("Challans", "OverView", null,item.RecordID);
                }catch(e){}
            return true;
        });

        function ClearAllFilterMain(e) {

            e.preventDefault();

            $('#chkAll_Main').removeAttr('checked');
            $('#dvbtndownloadDocumentMain').css('display', 'none');
            $('#chkAll').removeAttr('checked');
            $('#dvbtndownloadDocumentMain').css('display', 'none');

            $("#ddlEntitySubEntityLocation").data("kendoDropDownTree").value([]);          
            $('#ClearfilterMain').css('display', 'none');   
            $('#dvbtndownloadDocumentMain').css('display', 'none');
        }

        function ClearAllFilters() {
            $("#ddlEntitySubEntityLocation").data("kendoDropDownTree").value([]);
            $("#ddlMonth").data("kendoDropDownTree").value([]);
            $("#ddlYear").data("kendoDropDownTree").value([]);
            $('#ClearfilterMain').css('display', 'none');

            $('#chkAll_Main').removeAttr('checked');
            $('#dvbtndownloadDocumentMain').css('display', 'none');
            $('#chkAll').removeAttr('checked');
            $('#dvbtndownloadDocumentMain').css('display', 'none');
        }

        function OpenDownloadOverviewpupfirst(RecordID, DocType, Event) {
            $('#DownloadViews').attr('src', "RLCS_MyRegistrations.aspx?RLCSRecordID=" + RecordID + "&RLCSDockType=" + DocType + "&Event=" + Event);
        }

        function OpenDocumentOverviewpupfirst(RecordID, DocType, Event) {        
            $('#divViewDocument').modal('show');
            //$('#OverViews').attr('width', '1150px');
            //$('#OverViews').attr('height', '600px');
            $('.modal-dialog').css('width', '1200px');
            $('#OverViews').attr('src', "RLCS_DocumentOverview.aspx?RecordID=" + RecordID + "&DocType=" + DocType + "&Event=" + Event);
        }

        $(document).on("click", "#grid1 tbody tr .ob-download", function (e) {         
            var item = $("#grid1").data("kendoGrid").dataItem($(this).closest("tr"));
            OpenDownloadOverviewChallans(item.ScopeID, item.Code, item.PayrollMonth, item.PayrollYear, item.ScheduleIds, "W");
            analytics("Challans","Download",$('#CName').val(),1);
                try{
                    ApiTrack_Activity("Challans", "Download", null, item.Code);
                }catch(e){}
            return true;
        });

        $(document).on("click", "#grid1 tbody tr .ob-downloadCompliance", function (e) {
            var item = $("#grid1").data("kendoGrid").dataItem($(this).closest("tr"));
            OpenDownloadOverviewChallans(item.ScopeID, item.Code, item.PayrollMonth, item.PayrollYear, item.ScheduleIds, "C");
            analytics("Challans", "Download", $('#CName').val(), 1);
            try {
                ApiTrack_Activity("Challans", "Download", null, item.Code);
            } catch (e) { }
            return true;
        });

        
        $(document).on("click", "#grid1 tbody tr .ob-downloadAcknowledgement", function (e) {         
            var item = $("#grid1").data("kendoGrid").dataItem($(this).closest("tr"));
            OpenDownloadOverviewpupsecond(item.RecordID, item.LM_ScopeID, item.LM_State, item.LM_PayrollMonth, item.LM_PayrollYear, $("#ddlChallanType").val(),"3",item.LM_ClientID)
            analytics("Challans","Download",$('#CName').val(),1);
                try{
                ApiTrack_Activity("Challans", "Download", null,item.RecordID);
                }catch(e){}
            return true;
        });

        $(document).on("click", "#grid1 tbody tr .ob-downloadChallan", function (e) {         
            var item = $("#grid1").data("kendoGrid").dataItem($(this).closest("tr"));
            OpenDownloadOverviewpupsecond(item.RecordID, item.LM_ScopeID, item.LM_State, item.LM_PayrollMonth, item.LM_PayrollYear, $("#ddlChallanType").val(),"4",item.LM_ClientID)
            analytics("Challans","Download",$('#CName').val(),1);
                try{
                ApiTrack_Activity("Challans", "Download", null,item.RecordID);
                }catch(e){}
            return true;
        });

        
        function OpenDownloadOverviewChallans(ScopeID, Code, Month, Year, ScheduleList, FileType) {
            $('#DownloadViews').attr('src', "RLCS_MyDocuments_Challans.aspx?ScopeID=" + ScopeID + "&Code=" + Code + "&Month=" + Month + "&Year=" + Year + "&ScheduleList=" + ScheduleList + "&FileType=" + FileType);
            analytics("Challans", "download", $('#CName').val(), 1);
        }

        function OpenDownloadOverviewpupsecond(RecordID, ScopeID, State, Month, Year, Type,FileTypeId,ClientID) {            
            $('#DownloadViews').attr('src', "RLCS_MyDocuments_Challans.aspx?RecordID=" + RecordID + "&ScopeID=" + ScopeID + "&State=" + State + "&Month=" + Month + "&Year=" + Year + "&Type=" + Type + "&FileTypeId=" + FileTypeId + "&ClientID=" + ClientID);
            analytics("Challans","download",$('#CName').val(),1);
        }

        $(document).on("click", "#grid1 tbody tr .ob-overview", function (e) {         
            var item = $("#grid1").data("kendoGrid").dataItem($(this).closest("tr"));
            OpenOverviewChallans(item.ScopeID, item.Code, item.PayrollMonth, item.PayrollYear, item.ScheduleIds, "W");
            //OpenDocumentOverviewpupsecond(item.RecordID, item.LM_ScopeID, item.LM_State, item.LM_PayrollMonth, item.LM_PayrollYear, $("#ddlChallanType").val(),"2",item.LM_ClientID)          
            analytics("Challans","OverView",$('#CName').val(),1);
                try{
                ApiTrack_Activity("Challans", "OverView", null,item.RecordID);
                }catch(e){}
            return true;
        });

        $(document).on("click", "#grid1 tbody tr .ob-overviewCompliance", function (e) {
            var item = $("#grid1").data("kendoGrid").dataItem($(this).closest("tr"));
            OpenOverviewChallans(item.ScopeID, item.Code, item.PayrollMonth, item.PayrollYear, item.ScheduleIds, "C");
            //OpenDocumentOverviewpupsecond(item.RecordID, item.LM_ScopeID, item.LM_State, item.LM_PayrollMonth, item.LM_PayrollYear, $("#ddlChallanType").val(), "2", item.LM_ClientID)
            analytics("Challans", "OverView", $('#CName').val(), 1);
            try {
                ApiTrack_Activity("Challans", "OverView", null, item.RecordID);
            } catch (e) { }
            return true;
        });

        $(document).on("click", "#grid1 tbody tr .ob-overviewAcknowledgement", function (e) {
         
            var item = $("#grid1").data("kendoGrid").dataItem($(this).closest("tr"));
            OpenDocumentOverviewpupsecond(item.RecordID, item.LM_ScopeID, item.LM_State, item.LM_PayrollMonth, item.LM_PayrollYear, $("#ddlChallanType").val(),"3",item.LM_ClientID)          
            analytics("Challans","OverView",$('#CName').val(),1);
                try{
                ApiTrack_Activity("Challans", "OverView", null,item.RecordID);
                }catch(e){}
            return true;
        });
        $(document).on("click", "#grid1 tbody tr .ob-overviewChallan", function (e) {
         
            var item = $("#grid1").data("kendoGrid").dataItem($(this).closest("tr"));
            OpenDocumentOverviewpupsecond(item.RecordID, item.LM_ScopeID, item.LM_State, item.LM_PayrollMonth, item.LM_PayrollYear, $("#ddlChallanType").val(),"4",item.LM_ClientID)          
            analytics("Challans","OverView",$('#CName').val(),1);
                try{
                ApiTrack_Activity("Challans", "OverView", null,item.RecordID);
                }catch(e){}
            return true;
        });        

        function OpenDocumentOverviewpupsecond(RecordID, ScopeID, State, Month, Year, Type,FiletypeID,ClientID) {             
            $('#divViewDocument').modal('show');
            $('.modal-dialog').css('width', '90%');
            $('#OverViews').attr('src', "RLCS_DocumentOverview.aspx?RecordID=" + RecordID + "&ScopeID=" + ScopeID + "&State=" + State + "&Month=" + Month + "&Year=" + Year + "&Type=" + Type + "&FiletypeID=" + FiletypeID  + "&ClientID=" + ClientID);
          
        }

        function OpenOverviewChallans(ScopeID, Code, Month, Year, ScheduleList, FileType) {
            $('#divViewDocument').modal('show');
            $('.modal-dialog').css('width', '90%');
            $('#OverViews').attr('src', "RLCS_DocumentOverview.aspx?ScopeID=" + ScopeID + "&Code=" + Code + "&Month=" + Month + "&Year=" + Year + "&ScheduleList=" + ScheduleList + "&FileType=" + FileType);

        }

        function fclosebtn(tbn) {
            $('#' + tbn).css('display', 'none');
            $('#' + tbn).html('');
        }

        function BindFirstGrid() {
            $("#grid1").hide();
            $("#grid").show();

            var grid = $("#grid").kendoGrid({
                dataSource: {
                    serverPaging: false,
                    pageSize: 10,
                    transport: {
                        read: {
                            url: '<% =Path%>GetDocuments_BranchList_ReturnChallan_Historical?customerID=<%= CustId%>&userID=<% =UserId%>&scopeid=sow04&MonthId=&YearID=&profileID=<% =ProfileID%>&FromDate='+ $("#Startdatepicker").val()+ '&EndDate='+ $("#Enddatepicker").val(),
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },
                        }
                    },
                    batch: true,
                    pageSize: 10,
                    schema: {
                        data: function (response) {
                            return response.Result;
                        },
                        total: function (response) {
                            return response.Result.length;
                        }
                    }
                },

                // toolbar: kendo.template($("#template").html()),
                //height: 513,
                sortable: true,
                filterable: true,
                columnMenu: true,
                //pageable: {
                //    refresh: true
                //},
                pageable: {
                    refresh: true,
                    change: function (e) {
                        $('#chkAll_Main').removeAttr('checked');
                        $('#dvbtndownloadDocumentMain').css('display', 'none');
                    },
                    //pageSizes: true,
                    buttonCount: 3
                },
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                columns: [
                 {
                     template: "<input name='sel_chkbx_Main' id='sel_chkbx_Main' type='checkbox' value='#=RecordID#' >",
                     filterable: false, sortable: false,
                     headerTemplate: "<input type='checkbox' id='chkAll_Main' />",
                     width: "3%;"//, lock: true
                 },
                    
                    {
                        title: "Sr.No",
                        field: "rowNumber",
                        template: "<span class='row-number'></span>",
                        width: "10.7%;",
                        attributes: {
                            style: 'white-space: nowrap;'

                        },
                        filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "EntityClientName", title: 'Entity/Client',
                        width: "29.7%;",
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "SM_Name", title: 'State',
                        // width: "32.7%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "LM_Name", title: 'Location', filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "AVACOM_BranchName", title: 'Branch',
                        attributes: {
                            style: 'white-space: nowrap '
                        },
                        filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    { hidden: false, field: "MonthName", title: "Month" },
                    { hidden: false, field: "PayrollYear", title: "Year" },
                    {
                        command: [
                             { name:"editLable1", text: "No Document", className:"ob-lable" },
                             { name: "edit1", text: "", iconClass: "k-icon k-i-download", className: "ob-download" },
                             { name: "edit2", text: "", iconClass: "k-icon k-i-eye", className: "ob-overview" }
                        ], title: "Action", lock: true,  width: "12.7%;"
                    }
                ],
                dataBound: function () {
                    var rows = this.items();
                    $(rows).each(function () {
                        var index = $(this).index() + 1
                            + ($("#grid").data("kendoGrid").dataSource.pageSize() * ($("#grid").data("kendoGrid").dataSource.page() - 1));;
                        var rowLabel = $(this).find(".row-number");
                        $(rowLabel).html(index);
                    });
                    
                    // Selects all delete buttons
                    $("#grid tbody tr .k-grid-edit1").each(function () {
                        var currentDataItem = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
                        //Check in the current dataItem if the row is deletable
                        if (currentDataItem.DocStatus == 'Document Not Available') {
                            $(this).remove();
                        }                    
                    });

                    $("#grid tbody tr .k-grid-edit2").each(function () {
                        var currentDataItem = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
                        //Check in the current dataItem if the row is deletable
                        //if (currentDataItem.DocStatus == 'Document Not Available') {
                        //    $(this).remove();
                        //}
                        $(this).remove();
                    });


                    $("#grid tbody tr .k-grid-editLable1").each(function () {
                     
                        var currentDataItem = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
                        //Check in the current dataItem if the row is deletable
                        if (currentDataItem.DocStatus == 'Document Available') {
                            $(this).remove();
                        }                    
                    });

                }
            });


            $("#grid").kendoTooltip({
                filter: "td:nth-child(5)", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    return content;
                }
            }).data("kendoTooltip");

            $("#grid").kendoTooltip({
                filter: ".k-grid-edit1",                  
                content: function (e) {         
                    var dataItem = $("#grid").data("kendoGrid").dataItem(e.target.closest('tr'));                                   
                    return dataItem.DocStatus;                
                }
            });
            $("#grid").kendoTooltip({
                filter: ".k-grid-edit2",                  
                content: function (e) {                                                             
                    return "View";                
                }
            });

        }

        $(document).on("click", "#sel_chkbx", function (e) {

            if (($('input[name="sel_chkbx"]:checked').length) == 0) {
                $('#dvbtndownloadDocumentMain').css('display', 'none');
            }
            else {
                $('#dvbtndownloadDocumentMain').css('display', 'block');
            }
            return true;
        });

        $(document).on("click", "#chkAll", function (e) {  

          
            if ($('input[id=chkAll]').prop('checked')) {

                $('input[name="sel_chkbx"]').each(function (i, e) {
                    e.click();
                });
            }
            else {                   
                $('input[name="sel_chkbx"]').attr("checked", false);
            }
            if (($('input[name="sel_chkbx"]:checked').length) == 0) {
                $('#dvbtndownloadDocumentMain').css('display', 'none');
            }
            else {
                $('#dvbtndownloadDocumentMain').css('display', 'block');
            }
            return true;
        });

        function selectedDocument(e) {
            e.preventDefault();
          
            if (($('input[name="sel_chkbx"]:checked').length) == 0 && ($('input[name="sel_chkbx_Main"]:checked').length) == 0) {
                return;
            }
            var checkboxlist = [];

            if (($('input[name="sel_chkbx"]:checked').length) > 0)
            {
                $('input[name="sel_chkbx"]').each(function (i, e) {
                    if ($(e).is(':checked')) {
                        checkboxlist.push(e.value+"-"+ $("#ddlChallanType").val());
                    }
                });

                $('#DownloadViews').attr('src', "../RLCS/RLCSDownloadDoc.aspx?RLCSIds=" + checkboxlist.join(",") + "&IsFlag=Challan"); //&IsFlag=NoHistorical
            }

            if (($('input[name="sel_chkbx_Main"]:checked').length) > 0)
            {
                $('input[name="sel_chkbx_Main"]').each(function (i, e) {
                    if ($(e).is(':checked')) {
                        checkboxlist.push(e.value);
                    }
                });
                
                $('#DownloadViews').attr('src', "RLCS_MyRegistrations.aspx?RLCSRecordID=" + checkboxlist.join(",") + "&RLCSDockType=SOW04&Event=Download");                     
            }
                
            return false;
        }

        $(document).on("click", "#sel_chkbx_Main", function (e) {


            if (($('input[name="sel_chkbx_Main"]:checked').length) == 0) {
                $('#dvbtndownloadDocumentMain').css('display', 'none');
            }
            else {
                $('#dvbtndownloadDocumentMain').css('display', 'block');
            }
            return true;
        });

        $(document).on("click", "#chkAll_Main", function (e) {  

          
            if ($('input[id=chkAll_Main]').prop('checked')) {

                $('input[name="sel_chkbx_Main"]').each(function (i, e) {
                    e.click();
                });
            }
            else {                   
                $('input[name="sel_chkbx_Main"]').attr("checked", false);
            }
            if (($('input[name="sel_chkbx_Main"]:checked').length) == 0) {
                $('#dvbtndownloadDocumentMain').css('display', 'none');
            }
            else {
                $('#dvbtndownloadDocumentMain').css('display', 'block');
            }
            return true;
        });

       

        function BindSecondGrid() {
            $("#grid").hide();

            $("#grid1").show();

            var grid = $("#grid1").kendoGrid({
                dataSource: {
                    serverPaging: false,
                    pageSize: 10,
                    transport: {
                        read: {
                            url: '<% =Path%>GetDocuments_BranchList_Challans?customerID=<% =CustId%>&userID=<%=UserId%>&challanType=' + $("#ddlChallanType").val() + '&MonthId=&YearID=&profileID=<% =ProfileID%>&FromDate='+ $("#Startdatepicker").val()+ '&EndDate='+ $("#Enddatepicker").val(),
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },
                        }
                    },
                    batch: true,
                    pageSize: 10,
                    schema: {
                        data: function (response) {
		            if(response!=undefined && response!=null)
                            return response.Result;
else 
return null;
                        },
                        total: function (response) {
		            if(response!=undefined && response!=null)
                            return response.Result.length;
else 
return null;
                        }
                    }
                },

                //toolbar: kendo.template($("#template").html()),
                //height: 413,
                sortable: true,
                filterable: true,
                columnMenu: true,
                pageable: {
                    refresh: true,
                    change: function (e) {
                        $('#chkAll').removeAttr('checked');
                        $('#dvbtndownloadDocumentMain').css('display', 'none');
                    },
                    //pageSizes: true,
                    buttonCount: 3
                },
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                columns: [
                    {
                        //value='#=RecordID#-#=LM_ScopeID#-#=LM_State#-#=LM_PayrollMonth#-#=LM_PayrollYear#'
                        template: "<input name='sel_chkbx' id='sel_chkbx' type='checkbox'  >",
                        filterable: false, sortable: false,
                        headerTemplate: "<input type='checkbox' id='chkAll' />",
                        width: "3%;"//, lock: true
                    },
                   
                    {
                        field: "Entity", title: 'Entity/Client',
                        // width: "16.7%;",
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "Code", title: 'Code',
                        // width: "32.7%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        hidden: true,
                        field: "ScheduleIds",
                        title: "ScheduleList",
                        attributes: {
                            style: 'white-space: nowrap;'

                        },
                        filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                     { hidden: false, field: "MonthName", title: "Month" },
                    { hidden: false, field: "PayrollYear", title: "Year" },
                    
                    {
                        command: [
                             //{ name:"editLable1", text: "No Document", className:"ob-lable" },
                            { name: "edit1", text: "", iconClass: "k-icon k-i-download", className: "ob-download" }
                            , { name: "edit2", text: "", iconClass: "k-icon k-i-eye", className: "ob-overview" }
                        ], title: "Working", lock: true,
                    },
                     {
                        command: [
                              //{ name:"editLable3", text: "No Document", className:"ob-lable" },
                            { name: "edit5", text: "", iconClass: "k-icon k-i-download", className: "ob-downloadCompliance" }
                            , { name: "edit6", text: "", iconClass: "k-icon k-i-eye", className: "ob-overviewCompliance" }
                        ], title: "Compliance", lock: true,
                    }
                ]                              
                //dataBound: function () {

                //    //working
                //    // Selects all delete buttons
                //    $("#grid1 tbody tr .k-grid-edit1").each(function () {
                //        var currentDataItem = $("#grid1").data("kendoGrid").dataItem($(this).closest("tr"));
                //        //Check in the current dataItem if the row is deletable
                //        if (currentDataItem.WorkingDocStatus == 'Document Not Available') {
                //            $(this).remove();
                //        }
                //    });

                //    $("#grid1 tbody tr .k-grid-edit2").each(function () {
                //        var currentDataItem = $("#grid1").data("kendoGrid").dataItem($(this).closest("tr"));
                //        //Check in the current dataItem if the row is deletable
                //        if (currentDataItem.WorkingDocStatus == 'Document Not Available') {
                //            $(this).remove();
                //        }
                //    });


                //    $("#grid1 tbody tr .k-grid-editLable1").each(function () {
                //        var currentDataItem = $("#grid1").data("kendoGrid").dataItem($(this).closest("tr"));
                //        //Check in the current dataItem if the row is deletable
                //        if (currentDataItem.WorkingDocStatus == 'Document Available') {
                //            $(this).remove();
                //        }
                //    });


                //    //Challan
                //    // Selects all delete buttons
                //    $("#grid1 tbody tr .k-grid-edit5").each(function () {
                //        var currentDataItem = $("#grid1").data("kendoGrid").dataItem($(this).closest("tr"));
                //        //Check in the current dataItem if the row is deletable
                //        if (currentDataItem.ChallanDocStatus == 'Document Not Available') {
                //            $(this).remove();
                //        }
                //    });

                //    $("#grid1 tbody tr .k-grid-edit6").each(function () {
                //        var currentDataItem = $("#grid1").data("kendoGrid").dataItem($(this).closest("tr"));
                //        //Check in the current dataItem if the row is deletable
                //        if (currentDataItem.ChallanDocStatus == 'Document Not Available') {
                //            $(this).remove();
                //        }
                //    });

                //    $("#grid1 tbody tr .k-grid-editLable3").each(function () {
                //        var currentDataItem = $("#grid1").data("kendoGrid").dataItem($(this).closest("tr"));
                //        //Check in the current dataItem if the row is deletable
                //        if (currentDataItem.ChallanDocStatus == 'Document Available') {
                //            $(this).remove();
                //        }
                //    });

                //    //Acknowledgement
                //    // Selects all delete buttons
                //    $("#grid1 tbody tr .k-grid-edit3").each(function () {
                //        var currentDataItem = $("#grid1").data("kendoGrid").dataItem($(this).closest("tr"));
                //        //Check in the current dataItem if the row is deletable
                //        if (currentDataItem.AcknowDocStatus == 'Document Not Available') {
                //            $(this).remove();
                //        }
                //    });

                //    $("#grid1 tbody tr .k-grid-edit4").each(function () {
                //        var currentDataItem = $("#grid1").data("kendoGrid").dataItem($(this).closest("tr"));
                //        //Check in the current dataItem if the row is deletable
                //        if (currentDataItem.AcknowDocStatus == 'Document Not Available') {
                //            $(this).remove();
                //        }
                //    });

                //    $("#grid1 tbody tr .k-grid-editLable2").each(function () {
                //        var currentDataItem = $("#grid1").data("kendoGrid").dataItem($(this).closest("tr"));
                //        //Check in the current dataItem if the row is deletable
                //        if (currentDataItem.AcknowDocStatus == 'Document Available') {
                //            $(this).remove();
                //        }
                //    });




                //}



            });
            $("#grid1").kendoTooltip({
                filter: "td:nth-child(4)", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    return content;
                }
            }).data("kendoTooltip");

            $("#grid1").kendoTooltip({
                filter: ".k-grid-edit1",                  
                content: function (e) {         
                    return "Download";              
                }
            });
            $("#grid1").kendoTooltip({
                filter: ".k-grid-edit2",                  
                content: function (e) {                                                             
                    return "View";                
                }
            });
        }

        function BindLocationFilter() {
            $("#ddlEntitySubEntityLocation").kendoDropDownTree({
                placeholder: "Entity/State/Location/Branch",
                checkboxes: true,
                checkboxes: { checkChildren: true },
                checkAll: true,
                autoWidth: true,
                checkAllTemplate: "Select All",
                dataTextField: "Name",
                dataValueField: "ID",
                change: function (e) {



                    var month =<% =newRegistersMonth%>;
                        var year =<% =newRegistersYear%>;
                        if (month < $("#ddlMonth").val() && year >= $("#ddlYear").val()) {

                            var filter = { logic: "or", filters: [] };
                            var values = this.value();

                            $.each(values, function (i, v) {
                                filter.filters.push({
                                    field: "AVACOM_BranchID", operator: "eq", value: parseInt(v)
                                });
                            });

                            fCreateStoryBoard('ddlEntitySubEntityLocation', 'filtersstoryboard', 'loc');
                            var dataSource = $("#grid1").data("kendoGrid").dataSource;
                            dataSource.filter(filter);
                        }
                        else {

                            var filter = { logic: "or", filters: [] };
                            var values = this.value();

                            $.each(values, function (i, v) {
                                filter.filters.push({
                                    field: "AVACOM_BranchID", operator: "eq", value: parseInt(v)
                                });
                            });

                            fCreateStoryBoard('ddlEntitySubEntityLocation', 'filtersstoryboard', 'loc');
                            var dataSource = $("#grid").data("kendoGrid").dataSource;
                            dataSource.filter(filter);
                        }

                        $('#chkAll_Main').removeAttr('checked');                       
                        $('#chkAll').removeAttr('checked');
                        $('#dvbtndownloadDocumentMain').css('display', 'none');
                    },
                    dataSource: {
                        severFiltering: true,
                        transport: {
                            read: {
                                url: "<% =Path%>GetAssignedEntitiesLocationsList?customerId=<% =CustId%>&userId=<%=UserId%>&profileID=<% =ProfileID%>",
                                type: 'Get',
                                beforeSend: function (request) {
                                    request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                    request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                    request.setRequestHeader('Content-Type', 'application/json');
                                },
                                dataType: 'json'
                            }
                        },
                        schema: {
                            data: function (response) {
                                return response.Result;
                            },
                            model: {
                                children: "Children"
                            }
                        }
                    }
                });
            }

            function BindMonth() {
                $("#ddlMonth").kendoDropDownList({
                    placeholder: "Month",
                    autoClose: true,
                    autoWidth: true,
                    dataTextField: "text",
                    dataValueField: "value",
                    index: 0,
                    change: function (e) {
                       
                        dropDownFilter();
                        $("#ddlEntitySubEntityLocation").data("kendoDropDownTree").value([]); 
                        
                    <%--     $('#ClearfilterMain').css('display', 'none');
                         $('#dvbtndownloadDocumentMain').css('display', 'none');
                         $('input[name="sel_chkbx"]').attr("checked", false);
                         $('input[name="sel_chkbx_Main"]').attr("checked", false);

                        var month = $("#ddlMonth").val();
                        var year = $("#ddlYear").val();

                        //if (month > 5 && year >= 2018) {  change by amol
                        if (month > <% =newRegistersMonth%> && year >= <% =newRegistersYear%>) {
                            var grid = $('#grid').data("kendoGrid");
                            grid.setDataSource(null);

                            BindSecondGrid();

                            $('#divddlChallanType').css('display', 'block');

                            var values = this.value().trim();
                            if (values != "" && values != null) {
                                var filter = { logic: "or", filters: [] };
                                filter.filters.push({
                                    field: "PayrollMonth", operator: "eq", value: parseInt(values)
                                });
                                var dataSource = $("#grid1").data("kendoGrid").dataSource;
                                dataSource.filter(filter);
                            }

                            //BindSecondGrid();
                            //BindSecondGridMonth();
                            //BindChallansType();
                        } else if (year <  <% =newRegistersYear%> || (month <=  <% =newRegistersMonth%> && year <=  <% =newRegistersYear%>)) {

                            $('#divddlChallanType').css('display', 'none');

                            BindFirstGrid();

                            var values = this.value().trim();
                            if (values != "" && values != null) {
                                var filter = { logic: "or", filters: [] };
                                filter.filters.push({
                                    field: "PayrollMonth", operator: "eq", value: parseInt(values)
                                });
                                var dataSource = $("#grid").data("kendoGrid").dataSource;
                                dataSource.filter(filter);
                            }
                        } else {
                            ClearAllFilter();
                        }--%>
                    },
                    dataSource: [
                            { text: "January", value: "01" },
                            { text: "February", value: "02" },
                            { text: "March", value: "03" },
                            { text: "April", value: "04" },
                            { text: "May", value: "05" },
                            { text: "June", value: "06" },
                            { text: "July", value: "07" },
                            { text: "August", value: "08" },
                            { text: "September", value: "09" },
                            { text: "October", value: "10" },
                            { text: "November", value: "11" },
                            { text: "December", value: "12" }
                    ]
                });
            }


            function BindYear() {
                $("#ddlYear").kendoDropDownList({
                    placeholder: "Year",
                    autoWidth: true,
                    dataTextField: "text",
                    dataValueField: "value",
                    index: 1,
                    change: function (e) {


                        dropDownFilter();
                        $("#ddlEntitySubEntityLocation").data("kendoDropDownTree").value([]); 
                       <%--   $('#ClearfilterMain').css('display', 'none');
                         $('#dvbtndownloadDocumentMain').css('display', 'none');
                         $('input[name="sel_chkbx"]').attr("checked", false);
                         $('input[name="sel_chkbx_Main"]').attr("checked", false);

                        var month = $("#ddlMonth").val();
                        var year = $("#ddlYear").val();

                        //if (month > 5 && year >= 2018) {  change by amol
                            if (month > <% =newRegistersMonth%> && year >= <% =newRegistersYear%>) {
                            var grid = $('#grid').data("kendoGrid");
                            grid.setDataSource(null);

                            BindSecondGrid();

                            $('#divddlChallanType').css('display', 'block');

                            var values = this.value().trim();
                            if (values != "" && values != null) {
                                var filter = { logic: "or", filters: [] };
                                filter.filters.push({
                                    field: "PayrollMonth", operator: "eq", value: parseInt(values)
                                });
                                var dataSource = $("#grid1").data("kendoGrid").dataSource;
                                dataSource.filter(filter);
                            }

                            //BindSecondGrid();
                            //BindSecondGridMonth();
                            //BindChallansType();
                        } else if (year < <% =newRegistersYear%> || (month <= <% =newRegistersMonth%> && year <= <% =newRegistersYear%>)) {

                            $('#divddlChallanType').css('display', 'none');

                            BindFirstGrid();

                            var values = this.value().trim();
                            if (values != "" && values != null) {
                                var filter = { logic: "or", filters: [] };
                                filter.filters.push({
                                    field: "PayrollMonth", operator: "eq", value: parseInt(values)
                                });
                                var dataSource = $("#grid").data("kendoGrid").dataSource;
                                dataSource.filter(filter);
                            }
                        } else {
                            ClearAllFilter();
                        }--%>
                    },
                    dataSource: [
                        { text: "2021", value: "2021" },
                        { text: "2020", value: "2020" },
                        { text: "2019", value: "2019" },
                        { text: "2018", value: "2018" },
                        { text: "2017", value: "2017" },                       
                        { text: "2016", value: "2016" }
                    ]
                });
            }

            function BindChallanType() {
                $("#ddlChallanType").kendoDropDownList({
                    placeholder: "Challan Type",
                    autoClose: true,
                    autoWidth: true,
                    dataTextField: "text",
                    dataValueField: "value",
                    index: 0,
                    change: function (e) {

                        dropDownFilter();  
                        $("#ddlEntitySubEntityLocation").data("kendoDropDownTree").value([]); 
                        //$('#ClearfilterMain').css('display', 'none');
                        //$('#dvbtndownloadDocumentMain').css('display', 'none');
                        //$('input[name="sel_chkbx"]').attr("checked", false);
                        //$('input[name="sel_chkbx_Main"]').attr("checked", false);

                       <%-- var month = $("#ddlMonth").val();
                        var year = $("#ddlYear").val();

                        //if (month > 5 && year >= 2018) {  change by amol
                            if (month > <% =newRegistersMonth%> && year >= <% =newRegistersYear%>) {
                            var grid = $('#grid').data("kendoGrid");
                            grid.setDataSource(null);

                            BindSecondGrid();

                            $('#divddlChallanType').css('display', 'block');

                            var values = this.value().trim();
                            if (values != "" && values != null) {
                                var filter = { logic: "or", filters: [] };
                                filter.filters.push({
                                    field: "PayrollMonth", operator: "eq", value: parseInt(values)
                                });
                                var dataSource = $("#grid1").data("kendoGrid").dataSource;
                                dataSource.filter(filter);
                            }

                            //BindSecondGrid();
                            //BindSecondGridMonth();
                            //BindChallansType();
                        } else if (year < <% =newRegistersYear%> || (month <= <% =newRegistersMonth%> && year <= <% =newRegistersYear%>)) {

                            $('#divddlChallanType').css('display', 'none');

                            BindFirstGrid();

                            var values = this.value().trim();
                            if (values != "" && values != null) {
                                var filter = { logic: "or", filters: [] };
                                filter.filters.push({
                                    field: "PayrollMonth", operator: "eq", value: parseInt(values)
                                });
                                var dataSource = $("#grid").data("kendoGrid").dataSource;
                                dataSource.filter(filter);
                            }
                        } else {
                            ClearAllFilter();
                        }--%>
                      },
                    dataSource: [
                            { text: "ESI", value: "ESI" },
                            { text: "EPF", value: "EPF" },
                            { text: "PT", value: "PT" },
                    ]
                });
            }


          

            function BindSecondGridLocation() {
                $("#ddlEntitySubEntityLocation").kendoDropDownTree({
                    placeholder: "Entity/Sub-Entity/Location",
                    //checkboxes: true,
                    checkboxes: { checkChildren: true },
                    checkAll: true,
                    autoWidth: true,
                    checkAllTemplate: "Select All",
                    dataTextField: "Name",
                    dataValueField: "ID",
                    change: function (e) {
                        var filter = { logic: "or", filters: [] };
                        var values = this.value();
                        $.each(values, function (i, v) {
                            filter.filters.push({
                                field: "AVACOM_BranchID", operator: "eq", value: parseInt(v)
                            });
                        });
                        fCreateStoryBoard('ddlEntitySubEntityLocation', 'filtersstoryboard', 'loc')
                        var dataSource = $("#grid1").data("kendoGrid").dataSource;
                        dataSource.filter(filter);
                    },
                    dataSource: {
                        severFiltering: true,
                        transport: {
                            read: {
                                url: "<% =Path%>GetAssignedEntitiesLocationsList?customerId=<%= CustId%>&userId=<% =UserId%>&profileID=<% =ProfileID%>",
                                    type: 'Get',
                                    beforeSend: function (request) {
                                        request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                        request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                        request.setRequestHeader('Content-Type', 'application/json');
                                    },
                                    dataType: 'json'
                                }
                            },
                            schema: {
                                data: function (response) {
                                    return response.Result;
                                },
                                model: {
                                    children: "Children"
                                }
                            }
                        }
                    });
                }

                function BindSecondGridMonth() {
             
                    var rahul = [
                           { text: "January", value: "1" },
                           { text: "February", value: "2" },
                           { text: "March", value: "3" },
                           { text: "April", value: "4" },
                           { text: "May", value: "5" },
                           { text: "June", value: "6" },
                           { text: "July", value: "7" },
                           { text: "August", value: "8" },
                           { text: "September", value: "9" },
                           { text: "October", value: "10" },
                           { text: "November", value: "11" },
                           { text: "December", value: "12" }
                    ];
                    var dataSource12 = new kendo.data.DataSource({
                        schema: {
                            data: rahul
                        }
                    });

                    // dataSource12.read();
                    $("#ddlMonth").data("kendoDropDownList").setDataSource(dataSource12);
                }

                function ClearAllFilter(e) {
                    $('#Clearfilter').css('display', 'none');
                }

                function fcloseStory(obj) {
                    var DataId = $(obj).attr('data-Id');
                    var dataKId = $(obj).attr('data-K-Id');
                    var seq = $(obj).attr('data-seq');
                    var deepspan = $('#' + DataId + ' > li > span > span.k-i-close')[seq];
                    $(deepspan).trigger('click');
                    var upperli = $('#' + dataKId);
                    $(upperli).remove();
                    //for rebind if any pending filter is present (Main Grid)
                    fCreateStoryBoard('ddlEntitySubEntityLocation', 'filtersstoryboard', 'loc');
                    fCreateStoryBoard('ddlMonth', 'filtermonth', 'mon');
                    fCreateStoryBoard('ddlYear', 'filteryear', 'yer');
                    //CheckFilterClearorNot();
                    CheckFilterClearorNotMain();
                };

                function CheckFilterClearorNotMain() {
                    if (($($($('#ddlEntitySubEntityLocation').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
                        ($($($('#ddlMonth').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
                        ($($($('#ddlYear').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0)) {
                        $('#ClearfilterMain').css('display', 'none');
                    }
                }


                function fCreateStoryBoard(Id, div, filtername) {

                    var IdKReset = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('id');
                    $('#' + div).html('');
                    $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '28px');
                    $('#' + div).css('display', 'block');

                    if (div == 'filtersstoryboard') {
                        $('#' + div).append('Location&nbsp;&nbsp;&nbsp;:');//Dashboard
                        $('#ClearfilterMain').css('display', 'block');
                    }
                    else if (div == 'filtermonth') {
                        $('#' + div).append('Type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');//Dashboard               
                    }
                    else if (div == 'filteryear') {
                        $('#' + div).append('Risk&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');//Dashboard
                        $('#ClearfilterMain').css('display', 'block');
                    }
                    for (var i = 0; i < $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length; i++) {
                        var button = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button')[i];
                        $(button).css('display', 'none');
                        $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('placeholder', 'Shree');
                        var buttontest = $($(button).find('span')[0]).text();
                        if (buttontest.length > 10) {
                            buttontest = buttontest.substring(0, 10).concat("...");
                        }
                        $('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="background-color:#1fd9e1; height: 20px;Color:white;"><span unselectable="on">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory(this)" title="delete" aria-label="delete" class="k-select"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory(this)"  class="k-icon k-i-close"></span></span></li>&nbsp;');
                        //$('#' + div).append('&nbsp;&nbsp;&nbsp;');
                    }

                    if ($($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) {
                        $('#' + div).css('display', 'none');
                        $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');

                    }
            
                    //CheckFilterClearorNot();

                    CheckFilterClearorNotMain();
                }
                function exportReport() {
                    $("#grid").getKendoGrid().saveAsExcel();
                    return false;
                };

                $("#newModelClose").on("click", function () {
                    myWindow3.close();
                });
                function CloseClearOV() {
                    $('#OverViews').attr('src', "../Common/blank.html");
                }
                function CloseClearDV() {
                    $('#DownloadViews').attr('src', "../Common/blank.html");
                }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="example">
        <div class="row" style="padding-bottom: 4px;">
            <div class="col-md-12" style="padding: 0px;">
                <div class="col-md-2" style="width: 25%; padding-left: 0px;" id="divddlEntitySubEntityLocation">
                    <input id="ddlEntitySubEntityLocation" data-placeholder="Entity/State/Location/Branch" style="width: 100%;" />
                </div>
                <div class="col-md-2" style="width: 15%; padding-left: 0px;display:none;">
                    <input id="ddlMonth" data-placeholder="Month" style="width: 100%;" />
                </div>
                <div class="col-md-2" style="width: 15%; padding-left: 0px;display:none;">
                    <input id="ddlYear" data-placeholder="Year" style="width: 100%;" />
                </div>
                 <div class="col-md-2" style="width: 20%; padding-left: 0px;">
                      <input id="Startdatepicker" placeholder="Start Date" CssClass="clsROWgrid" title="startdatepicker" style="width: 100%;"/>                   
                </div>
                <div class="col-md-2" style="width: 20%; padding-left: 0px;">
                      <input id="Enddatepicker" placeholder="End Date" CssClass="clsROWgrid" title="Enddatepicker" style="width: 100%;"/>                   
                </div>
                <div class="col-md-2" style="width: 15%; padding-left: 0px; display: none;" id="divddlChallanType">
                    <input id="ddlChallanType" data-placeholder="Challan Type" style="width: 100%;" />
                </div>
                <div class="col-md-4" style="width: 25%; padding-left: 0px;">
                    <button id="ClearfilterMain" style="float: right; margin-right: 1%; display: none;" onclick="ClearAllFilterMain(event)">
                        <span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear Filter
                    </button>
                    <button id="dvbtndownloadDocumentMain" style="display: none; width: 107px; float: right;" onclick="selectedDocument(event)">Download</button>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
            </div>
        </div>

        <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: black;" id="filtersstoryboard">&nbsp;</div>
        <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none;" id="filtermonth">&nbsp;</div>
        <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none;" id="filteryear">&nbsp;</div>

        <div id="grid" style="border: none;"></div>
        <div id="grid1" style="border: none;"></div>

    </div>

    <div class="modal fade" id="divDownloadView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog" style="width: 360px;">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header" style="border-bottom: none;">
                    <button type="button" class="close" data-dismiss="modal" onclick="CloseClearDV();" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <iframe id="DownloadViews" src="about:blank" width="300px" height="150px" frameborder="0"></iframe>
                </div>
            </div>
        </div>
    </div>

    <div>
        <div class="modal fade" id="divViewDocument" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
            <div class="modal-dialog" style="width: 70%;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body" style="height: 570px;">
                        <div style="width: 100%;">
                            <div class="col-md-12 colpadding0">
                                <asp:Label runat="server" ID="lblMessage" Style="color: red;"></asp:Label>
                                <fieldset style="height: 550px; width: 100%;">
                                    <iframe src="about:blank" id="OverViews" width="100%" height="100%" frameborder="0"></iframe>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
