﻿<%@ Page Title="" Language="C#" MasterPageFile="~/RLCSCompliance.Master" AutoEventWireup="true" CodeBehind="RLCSMIS.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.RLCSMIS" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<link href="../NewCSS/kendo.common.min.css" rel="stylesheet" />
	<link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
	<link href="../NewCSS/kendo.silver.min.css" rel="stylesheet" />
	<link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

	<script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>
	<script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
	<script type="text/javascript" src="../Newjs/jszip.min.js"></script>
	<link href="../NewCSS/Kendouicss.css" rel="stylesheet" />

	<style>
		.astrick:after {
			content: " *";
			color: red;
			display: inline;
		}

		label {
			font-size: 1.3em;
			/*cursor: pointer;*/
		}

		.col-md-3 {
			width: 20%;
		}

		.col-md-2 {
			width: 15.666667%;
		}
		
		#branchwisegrid  th.k-header:nth-child(6) { text-align:center; }
		#clientwisegrid  th.k-header:nth-child(4) { text-align:center; }

	</style>


	<script type="text/javascript">
		$(document).ready(function () {
			BindEntities();
			fhead('My Documents/ Monthly MIS');

			var start = $("#Startdatepicker").kendoDatePicker({
				start: "year",
				// defines when the calendar should return date
				depth: "year",
				// display month and year in the input
				format: "MMMM yyyy",
				// specifies that DateInput is used for masking the input element
				value: new Date(),
				dateInput: true,
				change: function (e) {
					$('#chkAll_Main_C').removeAttr('checked');
					$('#chkAll_Main_B').removeAttr('checked');
					$("#dvbtndownloadDocumentMain").css('display', 'none');
					masterGridFunction();
				},
			}).data("kendoDatePicker");

			$(document).on("click", "#chkAll_Main_B", function (e) {


				if ($('input[id=chkAll_Main_B]').prop('checked')) {
					$("#dvbtndownloadDocumentMain").css('display', 'block');
					$('input[name="sel_chkbx_Main_B"]').each(function (i, e) {
						e.click();
					});
				}
				else {
					$("#dvbtndownloadDocumentMain").css('display', 'none');
					$('input[name="sel_chkbx_Main_B"]').attr("checked", false);
				}

				return true;
			});
			$(document).on("click", "#sel_chkbx_Main_B", function (e) {
				if (($('input[name="sel_chkbx_Main_B"]:checked').length) > 1) {
					$("#dvbtndownloadDocumentMain").css('display', 'block');
				}
				else {
					$("#dvbtndownloadDocumentMain").css('display', 'none');
				}

			});
			$(document).on("click", "#chkAll_Main_C", function (e) {
				debugger;
				if ($('input[id=chkAll_Main_C]').prop('checked')) {
					$("#dvbtndownloadDocumentMain").css('display', 'block');
					$('input[name="sel_chkbx_Main_C"]').each(function (i, e) {
						e.click();
					});
				}
				else {
					$("#dvbtndownloadDocumentMain").css('display', 'none');
					$('input[name="sel_chkbx_Main_C"]').attr("checked", false);
				}

				return true;
			});

			$(document).on("click", "#sel_chkbx_Main_C", function (e) {
				if (($('input[name="sel_chkbx_Main_C"]:checked').length) > 1) {
					$("#dvbtndownloadDocumentMain").css('display', 'block');
				}
				else {
					$("#dvbtndownloadDocumentMain").css('display', 'none');
				}

			});
			//Action button edit code in kendo grid
			$(document).on("click", "#branchwisegrid tbody tr .k-grid-edit1", function (e) {
				debugger;
				var item = $("#branchwisegrid").data("kendoGrid").dataItem($(this).closest("tr"));
				$('#DownloadViews').attr('src', "RLCSMIS.aspx?MISPaths=" + item.DocumentPath + "&Flag=D");
				//DownloadDocuments(item.DocumentPath);
				return false;
			});
			$(document).on("click", "#clientwisegrid tbody tr .k-grid-edit2", function (e) {
				debugger;
				var item = $("#clientwisegrid").data("kendoGrid").dataItem($(this).closest("tr"));
				$('#DownloadViews').attr('src', "RLCSMIS.aspx?MISPaths=" + item.DocumentPath + "&Flag=D");
				//DownloadDocuments(item.DocumentPath);
				return false;
			});
			function DownloadDocuments(DocumentPath) {
				debugger;
				$('#DownloadViews').attr('src', "RLCS_MISDownload.aspx?MISPaths=" + DocumentPath + "&Flag=D");
			}

			//Code for tooltip in grid Data
			$("#branchwisegrid").kendoTooltip({
				filter: "td", //this filter selects the second column's cells
				position: "down",
				content: function (e) {
					var content = e.target.context.textContent;
					if (content != "") {
						return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
					}
					else
						e.preventDefault();
				}
			}).data("kendoTooltip");
			//Code for tooltip in grid Data
			$("#clientwisegrid").kendoTooltip({
				filter: "td", //this filter selects the second column's cells
				position: "down",
				content: function (e) {
					var content = e.target.context.textContent;
					if (content != "") {
						return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
					}
					else
						e.preventDefault();
				}
			}).data("kendoTooltip");

			$("#branchwisegrid").kendoTooltip({
				filter: ".k-grid-edit1",
				content: function (e) {
					return "Download";
				}
			});
			$("#clientwisegrid").kendoTooltip({
				filter: ".k-grid-edit2",
				content: function (e) {
					return "Download";
				}
			});
			$("#branchwisegrid").kendoTooltip({
				filter: ".k-grid-edit2",
				content: function (e) {
					return "View";
				}
			});
			$("#clientwisegrid").kendoTooltip({
				filter: ".k-grid-edit3",
				content: function (e) {
					return "View";
				}
			});

			$(document).on("click", "#clientwisegrid tbody tr .ob-overview", function (e) {
				debugger;
				var item = $("#clientwisegrid").data("kendoGrid").dataItem($(this).closest("tr"));
				ViewDocuments(item.DocumentPath);
				return true;
			});
			$(document).on("click", "#branchwisegrid tbody tr .ob-overview", function (e) {
				debugger;
				var item = $("#branchwisegrid").data("kendoGrid").dataItem($(this).closest("tr"));
				ViewDocuments(item.DocumentPath);
				return true;
			});

			function ViewDocuments(DocumentPath) {
				$('#divViewDocument').modal('show');
				$('.modal-dialog').css('width', '90%');
				$('#OverViews').attr('src', "RLCS_MISDocumentOverview.aspx?DocumentPath=" + DocumentPath);
			}

			$('input[type=radio][name=MISCheckBox]').change(function () {
				debugger;
				if (this.value == 'B') {
					$("#dvbtndownloadDocumentMain").css('display', 'none');
					$('#chkAll_Main_C').removeAttr('checked');
				}
				else if (this.value == 'C') {
					$('#chkAll_Main_B').removeAttr('checked');
					$("#dvbtndownloadDocumentMain").css('display', 'none');
				}
				masterGridFunction();

			});
		});
		function masterGridFunction() {
			debugger;
			if ($('input[name="MISCheckBox"]:checked').val() != '' && $("#ddlIlstforClient").val() != '' && $("#Startdatepicker").val() != '') {
				if ($('input[name="MISCheckBox"]:checked').val() == 'B') {
					BindBranchGrid();
				}
				else if ($('input[name="MISCheckBox"]:checked').val() == 'C') { BindClientGrid(); }
			}

		}
		function BindClientGrid() {
			var datepicker = $("#Startdatepicker").data("kendoDatePicker").value();
			var month = (datepicker.getMonth() + 1).toString();
			if (month.length == 1) {
				month = '0' + month;
			}
			var year = datepicker.getFullYear();
			$("#clientwisegrid").show();
			$("#branchwisegrid").hide();
			var dataSource = new kendo.data.DataSource({
				transport: {
					read: {
						url: '<% =RLCS_API_URL%>Masters/GetMISPortalDetails?ClientId=' + $("#ddlIlstforClient").val() + '&Type=C' + '&Month=' + month + '&Year=' + year,
						dataType: "json",
					}
				},
				pageSize: 10,
				schema: {
					data: function (response) {
						return response;
					},
					total: function (response) {
						return response.length;

					}
				},
			});

			var grid = $("#clientwisegrid").kendoGrid({
				dataSource: dataSource,
				sortable: true,
				filterable: true,
				columnMenu: true,
				//pageable: {
				//    refresh: true
				//},
				pageable: {
					pageSizes: ['All', 5, 10, 20],
					refresh: true,
					change: function (e) {
						$('#chkAll_Main_C').removeAttr('checked');
						$('#dvbtndownloadDocumentMain').css('display', 'none');
					},
					pageSizes: true,
					buttonCount: 3
				},
				batch: true,
				pageSize: 10,
				reorderable: true,
				resizable: true,
				multi: true,
				selectable: true,
				columns: [
					{
						template: "<input name='sel_chkbx_Main_C' id='sel_chkbx_Main_C' type='checkbox' value='#=DocumentPath#' >",
						filterable: false, sortable: false,
						headerTemplate: "<input type='checkbox' id='chkAll_Main_C'/>",
						width: "3%;"//, lock: true
					},

					{
						field: "ClientName", title: 'Client Name',
						width: "62.7%;",
						attributes: {
							style: 'white-space: nowrap;'

						}, filterable: {
							extra: false,
							operators: {
								string: {
									eq: "Is equal to",
									neq: "Is not equal to",
									contains: "Contains"
								}
							}
						}
					},
					{
						field: "DocumentPath", title: 'MIS Document',
						hidden: true,
					},
					{
						command: [
							{ name: "edit2", text: "", iconClass: "k-icon k-i-download", className: "ob-download"},
							{ name: "edit3", text: "", iconClass: "k-icon k-i-eye", className: "ob-overview" }
						], title: "Action", lock: true, width: "6.8%;", attributes: { style: "text-align:center" },
					},
				],
			});
		}

		function BindBranchGrid() {

			var datepicker = $("#Startdatepicker").data("kendoDatePicker").value();
			var month = (datepicker.getMonth() + 1).toString();
			if (month.length == 1) {
				month = '0' + month;
			}
			var year = datepicker.getFullYear().toString();

			$("#clientwisegrid").hide();
			$("#branchwisegrid").show();
			var dataSource = new kendo.data.DataSource({
				transport: {
					read: {
						
						url: '<% =RLCS_API_URL%>Masters/GetMISPortalDetails?ClientId=' + $("#ddlIlstforClient").val() + '&Type=B' + '&Month=' + month + '&Year=' + year,
						dataType: "json",
					}
				},
				pageSize: 10,
				schema: {
					data: function (response) {
						return response;
					},
					total: function (response) {
						return response.length;

					}
				},
			});


			//by kunal
			var grid = $("#branchwisegrid").kendoGrid({
				dataSource: dataSource,
				sortable: true,
				filterable: true,
				columnMenu: true,
				//pageable: {
				//    refresh: true
				//},
				pageable: {
					pageSizes: ['All', 5, 10, 20],
					refresh: true,
					change: function (e) {
						$('#chkAll_Main_B').removeAttr('checked');
						$('#dvbtndownloadDocumentMain').css('display', 'none');
					},
					pageSizes: true,
					buttonCount: 3
				},
				batch: true,
				pageSize: 10,
				reorderable: true,
				resizable: true,
				multi: true,
				selectable: true,
				columns: [
					{
						template: "<input name='sel_chkbx_Main_B' id='sel_chkbx_Main_B' type='checkbox' value='#=DocumentPath#' >",
						filterable: false, sortable: false,
						headerTemplate: "<input type='checkbox' id='chkAll_Main_B' />",
						width: "3%;"//, lock: true
					},

					{
						field: "StateName", title: 'State Name',
						width: "24.7%;",
						attributes: {
							style: 'white-space: nowrap;'

						}, filterable: {
							extra: false,
							operators: {
								string: {
									eq: "Is equal to",
									neq: "Is not equal to",
									contains: "Contains"
								}
							}
						}
					},
					{
						field: "LocationName", title: 'Location Name',
						width: "24.7%",
						attributes: {
							style: 'white-space: nowrap;'
						},
						filterable: {
							multi: true,
							extra: false,
							search: true,
							operators: {
								string: {
									eq: "Is equal to",
									neq: "Is not equal to",
									contains: "Contains"
								}
							}
						}
					},
					{
						field: "Branch", title: 'Branch Name', width: "20%", filterable: {
							extra: false,
							operators: {
								string: {
									eq: "Is equal to",
									neq: "Is not equal to",
									contains: "Contains"
								}
							}
						}
					},
					{
						field: "DocumentPath", title: 'MIS Document',
						hidden: true,
					},
					{
						command: [
							{ name: "edit1", text: "", iconClass: "k-icon k-i-download", className: "ob-download" },
							{ name: "edit2", text: "", iconClass: "k-icon k-i-eye", className: "ob-overview" }
						], title: "Action", lock: true, width: "6.8%;"
					},
				],
			});

		}

		function BindEntities() {
			$("#ddlIlstforClient").width(265).kendoDropDownList({
				dataTextField: "AVACOM_BranchName",
				dataValueField: "CM_ClientID",
				optionLabel: "Select Entity",
				change: function (e) {
					$('#chkAll_Main_B').removeAttr('checked');
					$('#chkAll_Main_C').removeAttr('checked');
					$("#dvbtndownloadDocumentMain").css('display', 'none');
					masterGridFunction();

				},
				dataSource: {
					transport: {
						read: {
							url: "<% =avacomAPIUrl%>GetAssignedEntities?customerID=<% =CId%>&userID=<% =UId%>&profileID=<% =ProfileID%>",
							beforeSend: function (request) {
								request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
								request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
								request.setRequestHeader('Content-Type', 'application/json');
							},
							dataType: 'json',
						}
					},
					schema: {
						data: function (response) {
							debugger;
							if (response.Result != null && response.Result != undefined) {
								console.log($("#ddlIlstforClient").val());
								//BindsubCode();
							}
							return response.Result;
						}
					}
				}
			});
		}

		function selectedDocument(e) {
			e.preventDefault();
			debugger;

			if (($('input[name="sel_chkbx_Main_B"]:checked').length) == 0 && ($('input[name="sel_chkbx_Main_C"]:checked').length) == 0) {
				return;
			}
			var checkboxlist = [];

			if ($(branchwisegrid).is(':visible')) {
				if (($('input[name="sel_chkbx_Main_B"]:checked').length) > 0) {
					$('input[name="sel_chkbx_Main_B"]').each(function (i, e) {
						if ($(e).is(':checked')) {
							
							checkboxlist.push(e.value);
						}
					});
					console.log(checkboxlist);
					$('#DownloadViews').attr('src', "RLCS_MISDownload.aspx?MISPaths=" + checkboxlist.join(",") + "&Flag=B");
				}
			}
			else {
				if (($('input[name="sel_chkbx_Main_C"]:checked').length) > 0) {
					$('input[name="sel_chkbx_Main_C"]').each(function (i, e) {
						if ($(e).is(':checked')) {
							checkboxlist.push(e.value);
						}
					});
					console.log(checkboxlist);
					$('#DownloadViews').attr('src', "RLCS_MISDownload.aspx?MISPaths=" + checkboxlist.join(",") + "&Flag=C");
				}
			}

			return false;
		}

	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
	<div class="row">
		<div class="col-md-12">

			<div class="col-md-4 astrick" style="margin-left: -29px;">
				<input id="ddlIlstforClient" />
			</div>
			<div class="col-md-3 astrick ">
				<input id="Startdatepicker" placeholder="Start Date" cssclass="clsROWgrid" title="startdatepicker" />
			</div>
			<div class="col-md-4 " style="text-align: left">
				<input class="form-check-input" type="radio" name="MISCheckBox" id="ClientWisebtn" value="C" checked>
				<label class="form-check-label" for="ClientWisebtn" style="color: black">
					Entity
				</label>
				<input class="form-check-input" style="margin-left: 15px;" type="radio" name="MISCheckBox" id="BranchWisebtn" value="B">
				<label class="form-check-label" for="BranchWisebtn" style="color: black">
					Branch  
				</label>
			</div>

			<div class="col-md-1">
				<button id="dvbtndownloadDocumentMain" class=" btn-primary btn pull-right" style="display: none;" data-toggle='tooltip' data-placement='bottom' title='Download All MIS Document' onclick="selectedDocument(event)">Download</button>
			</div>
		</div>
		<%--<div class="col-md-1">
			<button id="ClearfilterMain" class=" btn-primary btn pull-right" style="display: none; height: 36px;" onclick="ClearAllFilterMain(event)"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear Filter</button>
		</div>--%>
		<div id="branchwisegrid" style="border: none; margin-top: 40px;"></div>
		<div id="clientwisegrid" style="border: none; margin-top: 40px;"></div>
		<div class="modal-body" style="display: none;">
			<iframe id="DownloadViews" src="about:blank" width="300px" height="150px" frameborder="0"></iframe>
		</div>

		<div>
			<div class="modal fade" id="divViewDocument" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
				<div class="modal-dialog" style="width: 70%;">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						</div>
						<div class="modal-body" style="height: 570px;">
							<div style="width: 100%;">
								<div class="col-md-12 colpadding0">
									<asp:Label runat="server" ID="lblMessage" Style="color: red;"></asp:Label>
									<fieldset style="height: 550px; width: 100%;">
										<iframe src="about:blank" id="OverViews" width="100%" height="100%" frameborder="0"></iframe>
									</fieldset>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


</asp:Content>
