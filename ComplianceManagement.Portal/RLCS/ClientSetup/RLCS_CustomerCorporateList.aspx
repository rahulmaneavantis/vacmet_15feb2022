﻿<%@ Page Title="Customer/Corporate :: Setup HR+" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="RLCS_CustomerCorporateList.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.ClientSetup.RLCS_CustomerCorporateList" %>

<%@ Register Src="~/RLCS/ClientSetup/RLCS_CustomerCorporateDetails.ascx" TagName="CustomerDetailsControl" TagPrefix="vit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../../Newjs/kendo.all.min.js"></script>

    <link href="/NewCSS/arrow-Progressbar.css" rel="stylesheet" />

    <style type="text/css">
        .aspNetDisabled {
            cursor: not-allowed;
        }
    </style>
    <script type="text/javascript">
        function initializeCombobox() {           
            $("#<%= ddlSPList.ClientID %>").combobox();
        }

        $(document).ready(function () {
            $('#divAddEditCustomerDialog').hide();
            initializeCombobox();
        });       
    </script>

    <script type="text/javascript">
        function OpenAddCustomerPopup() {
            debugger;

            if ($("#<%= ddlSPList.ClientID %>").val() != null && $("#<%= ddlSPList.ClientID %>") != undefined) {
                var distID = $("#<%= ddlSPList.ClientID %>").val();

                $('#divAddEditCustomerDialog').show();
                var myWindowAdv = $("#divAddEditCustomerDialog");

                myWindowAdv.kendoWindow({
                    width: "60%",
                    height: '90%',
                    iframe: true,
                    title: "Customer Details",
                    visible: false,
                    actions: [
                        //"Pin",
                        "Close"
                    ],
                    open: onOpen,
                    close: onClose
                });

                //$('#iframeAdd').attr('src', 'about:blank');
                myWindowAdv.data("kendoWindow").center().open();
                $('#iframeAdd').attr('src', "/RLCS/RLCS_CustomerDetails.aspx?CustomerID=0&DistID=" + distID);
            }

            return false;
        }
        
        function onClose() {
            debugger;
            //document.getElementById('<%= btnRefresh.ClientID %>').click();
            document.getElementById('<%= btnRefresh1.ClientID %>').click();
            //$(this.element).empty(); //must comment
            $("#BodyContent_updateProgress").hide();
        }

        function onOpen(e) {
            //kendo.ui.progress(e.sender.element, true);
            $("#BodyContent_updateProgress").show();
        }

        function OpenEditCustomerPopup(obj) {
            debugger;
            var custID = $(obj).attr('data-clId');
            var distID = $(obj).attr('data-parentId');

            $('#divAddEditCustomerDialog').show();
            var myWindowAdv = $("#divAddEditCustomerDialog");

            myWindowAdv.kendoWindow({
                width: "60%",
                height: '90%',
                iframe: true,
                title: "Customer Details",
                visible: false,
                actions: [
                    //"Pin",
                    "Close"
                ],
                open: onOpen,
                close: onClose
            });

            //$('#iframeAdd').attr('src', 'about:blank');
            myWindowAdv.data("kendoWindow").center().open();
            $('#iframeAdd').attr('src', "/RLCS/RLCS_CustomerDetails.aspx?CustomerID=" + custID + "&DistID=" + distID);
            return false;
        }
    </script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 25%; left: 40%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

    <div class="container">
        <div style="margin-left:1%;">
            <div class="arrow-steps clearfix">
                <div class="step current">
                    <asp:LinkButton ID="lnkBtnCustomer" PostBackUrl="~/RLCS/ClientSetup/RLCS_CustomerCorporateList.aspx" runat="server" Text="Customer"></asp:LinkButton>
                </div>
                <div class="step">
                    <asp:LinkButton ID="lnkBtnEntityCustomerBranch" runat="server" Text="Entity-Branch"></asp:LinkButton> <%--PostBackUrl="~/RLCS/Setup/RLCS_EntityBranchList.aspx"--%>
                </div>
                <div class="step">
                    <asp:LinkButton ID="lnkBtnUser" PostBackUrl="~/RLCS/ClientSetup/RLCS_UserMaster.aspx" runat="server" Text="User"></asp:LinkButton>
                </div>
                <div class="step">
                    <asp:LinkButton ID="lnkBtnEmployee" PostBackUrl="~/RLCS/ClientSetup/RLCS_EmployeeMaster.aspx" runat="server" Text="Employee"></asp:LinkButton>
                </div>
                <div class="step">
                    <asp:LinkButton ID="lnkBtnEntityAssign" PostBackUrl="~/RLCS/ClientSetup/RLCS_EntityAssignment.aspx" runat="server" Text="Entity-Branch Assignment"></asp:LinkButton>
                </div>
                <div class="step">
                    <asp:LinkButton ID="lnkBtnComAssign" PostBackUrl="~/RLCS/ClientSetup/RLCS_HRCompliance_Assign.aspx" runat="server" Text="Compliance Assignment"></asp:LinkButton>
                </div>
                <div class="step">
                    <asp:LinkButton ID="lnkBtnComAct" PostBackUrl="~/RLCS/ClientSetup/RLCS_HRCompliance_Activate.aspx" runat="server" Text="Compliance Activation"></asp:LinkButton>
                </div>
            </div>           
        </div>
    </div>

    <asp:UpdatePanel ID="upCustomerList" runat="server" UpdateMode="Conditional" OnLoad="upCustomer_Load">
        <ContentTemplate>
            <table width="100%">
                <tr>
                    <td style="width: 50%;">
                        <div id="divCustomerfilter" runat="server">
                            <div style="width: 35%; float: left;">
                                <label style="display: block; float: right; font-size: 13px; color: #333; font-weight: bold; margin-top: 5px;">
                                    Service Provider/Distributor:</label>
                            </div>
                            <div style="width: 65%; float: left;">
                                <asp:DropDownList runat="server" ID="ddlSPList" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                    CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlSPList_SelectedIndexChanged" />
                            </div>
                        </div>
                    </td>
                    <td style="width: 40%;">
                        <div style="width: 30%; float: left;">
                            <label style="display: block; float: right; font-size: 13px; color: #333; font-weight: bold; margin-top: 5px;">Filter :</label>
                        </div>
                        <div style="width: 70%; float: left;">
                            <asp:TextBox runat="server" ID="tbxFilter" Width="100%" MaxLength="50" AutoPostBack="true"
                                OnTextChanged="tbxFilter_TextChanged" />
                        </div>
                    </td>
                    <td align="right" style="width: 10%;">
                        <asp:LinkButton Text="Add New" runat="server" ID="btnAddCustomer" CssClass="button" OnClientClick = "return OpenAddCustomerPopup()" /> <%--OnClick="btnAddCustomer_Click"--%>                        
                    </td>
                    <td align="right" style="width: 10%; display: none">
                        <asp:LinkButton Text="Refresh" runat="server" ID="btnRefresh" CssClass="button" OnClick="btnRefresh_Click" />
                           <asp:LinkButton Text="Refresh" runat="server" ID="btnRefresh1" CssClass="button" OnClick="btnRefresh1_Click" />
                        <%--OnClick="btnAddCustomer_Click"--%>                        
                    </td>
                </tr>
            </table>
            <div style="margin-bottom: 4px">
                <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False" 
                    ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
            </div>

            <asp:GridView runat="server" ID="grdCustomer" AutoGenerateColumns="false" GridLines="Vertical" ShowHeaderWhenEmpty="true"
                BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true" OnRowCreated="grdCustomer_RowCreated"
                CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="12" Width="100%" OnSorting="grdCustomer_Sorting"
                Font-Size="12px" DataKeyNames="ID" OnRowCommand="grdCustomer_RowCommand" OnPageIndexChanging="grdCustomer_PageIndexChanging"
                OnPreRender="grdCustomer_OnPreRender">
                <Columns>
                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="60px">
                        <HeaderTemplate>
                            <asp:CheckBox ID="chkCompliancesHeader" Text="All" runat="server" onclick="javascript:SelectheaderCheckboxes(this)" />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkCustomer" runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:BoundField DataField="DistributorOrCustomer" HeaderText="Type" SortExpression="DistributorOrCustomer" />

                    <asp:TemplateField HeaderText="CorporateID" ItemStyle-Width="5%" SortExpression="CorporateID">
                            <ItemTemplate>                                
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("CorporateID") %>' ToolTip='<%# Eval("CorporateID") %>'></asp:Label>                                
                            </ItemTemplate>
                        </asp:TemplateField>
                    <asp:BoundField DataField="Name" HeaderText="Name" HeaderStyle-Height="20px" ItemStyle-Height="25px" SortExpression="Name" />
                    <asp:BoundField DataField="BuyerName" HeaderText="Buyer Name" SortExpression="BuyerName" />
                    <asp:BoundField DataField="BuyerContactNumber" HeaderText="Buyer Contact" ItemStyle-HorizontalAlign="Center" SortExpression="BuyerContactNumber" />
                    <asp:BoundField DataField="BuyerEmail" HeaderText="Buyer Email" SortExpression="BuyerEmail" />
                    <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="Status" ItemStyle-HorizontalAlign="Center" />
                    <asp:TemplateField HeaderText="Start Date" ItemStyle-HorizontalAlign="Center" SortExpression="StartDate" Visible="false">
                        <ItemTemplate>
                            <%# Eval("StartDate") != null ? ((DateTime)Eval("StartDate")).ToString("dd-MMM-yyyy")  : " "%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="End Date" ItemStyle-HorizontalAlign="Center" SortExpression="EndDate" Visible="false">
                        <ItemTemplate>
                            <%# Eval("EndDate") != null ? ((DateTime)Eval("EndDate")).ToString("dd-MMM-yyyy") : " "%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <%--<asp:BoundField DataField="DiskSpace" HeaderText="Disk Space" SortExpression="DiskSpace" />--%>
                    <asp:BoundField DataField="ServiceProvider" HeaderText="Service Provider" SortExpression="ServiceProvider" ItemStyle-HorizontalAlign="Center" />
                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Entity/Branch">
                        <ItemTemplate>
                            <asp:LinkButton runat="server" CommandName="VIEW_COMPANIES" CommandArgument='<%# Eval("ID") %>' ToolTip="View Entity/Branch">Entity/Branch</asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-Width="60px" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:LinkButton ID="LinkButton1" runat="server" data-clId='<%# Eval("ID")%>' data-parentId='<%# Eval("ParentID")%>' OnClientClick="OpenEditCustomerPopup(this)">
                                <img src="../../Images/edit_icon.png" alt="Edit Customer" title="Edit Customer" /></asp:LinkButton> <%--CommandName="EDIT_CUSTOMER" CommandArgument='<%# Eval("ID") %>'--%>
                            <asp:LinkButton ID="LinkButton2" runat="server" CommandName="DELETE_CUSTOMER" CommandArgument='<%# Eval("ID") %>'
                                OnClientClick="return confirm('This will also delete all the sub-entities associated with customer. Are you certain you want to delete this customer entry?');"><img src="../../Images/delete_icon.png" alt="Disable Customer" title="Disable Customer" />
                            </asp:LinkButton>
                        </ItemTemplate>
                        <HeaderTemplate>
                        </HeaderTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <FooterStyle BackColor="#CCCC99" />
                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                <PagerSettings Position="Top" />
                <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                <AlternatingRowStyle BackColor="#E6EFF7" />
                <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                <EmptyDataTemplate>
                    No Records Found.
                </EmptyDataTemplate>
            </asp:GridView>            
        </ContentTemplate>
    </asp:UpdatePanel>

    <div id="divNotificationTime">
        <asp:UpdatePanel ID="upServerDownTimeDetails" runat="server" UpdateMode="Conditional" OnLoad="upServerDownTimeDetails_Load">
            <ContentTemplate>
                <div style="margin: 5px">
                    <div style="margin-bottom: 4px">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="vdsummary" ValidationGroup="ComplianceCategoryValidationGroup1" />
                        <asp:CustomValidator ID="CustomValidator1" runat="server" EnableClientScript="False"
                            ValidationGroup="ComplianceCategoryValidationGroup1" Display="None" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Date</label>
                        <asp:TextBox runat="server" ID="txtDate" Style="height: 16px; width: 200px;" ReadOnly="true" MaxLength="200" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Date can not be empty." ControlToValidate="txtDate"
                            runat="server" ValidationGroup="ComplianceCategoryValidationGroup1" Display="None" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Time
                        </label>
                        <asp:DropDownList runat="server" ID="ddlStartTime"></asp:DropDownList>
                        <asp:CompareValidator ID="CompareValidator1" ErrorMessage="Please select start time." ControlToValidate="ddlStartTime"
                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceCategoryValidationGroup1"
                            Display="None" />
                        To
                        <asp:DropDownList runat="server" ID="ddlEndTime"></asp:DropDownList>
                        <asp:CompareValidator ID="CompareValidator2" ErrorMessage="Please select end time." ControlToValidate="ddlEndTime"
                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceCategoryValidationGroup1"
                            Display="None" />
                    </div>
                    <div style="margin-bottom: 7px; float: right; margin-right: 66px; margin-top: 10px;">
                        <asp:Button Text="Send" runat="server" ID="btnSend" CssClass="button" OnClick="btnSend_click"
                            ValidationGroup="ComplianceCategoryValidationGroup1" />
                        <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="button" OnClientClick="$('#divNotificationTime').dialog('close');" />
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <script type="text/javascript">

        $(function () {
            $('#divNotificationTime').dialog({
                height: 300,
                width: 500,
                autoOpen: false,
                showOn: true,
                draggable: true,
                title: "Software Update Details",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });
        });
        
        function initializeDowntimeDatePicker(date) {
            var startDate = new Date();
            $('#<%= txtDate.ClientID %>').datepicker({
                dateFormat: 'dd-mm-yy',
                numberOfMonths: 1,
                minDate: startDate
            });

            if (date != null) {
                $("#<%= txtDate.ClientID %>").datepicker("option", "defaultDate", date);
            }
        }

        function SelectheaderCheckboxes(headerchk) {
            var rolecolumn;

            var chkheaderid = headerchk.id.split("_");

            var gvcheck = document.getElementById("<%=grdCustomer.ClientID %>");
            var i;

            if (headerchk.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[0].getElementsByTagName("INPUT")[0].checked = headerchk.checked;
                }
            }

            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[0].getElementsByTagName("INPUT")[0].checked = headerchk.checked;
                }
            }
        }

        function Selectchildcheckboxes(header) {
            var i;
            var count = 0;
            var rolecolumn;
            var gvcheck = document.getElementById("<%=grdCustomer.ClientID %>");
            var headerchk = document.getElementById(header);
            var chkheaderid = header.split("_");

            var rowcount = gvcheck.rows.length;

            for (i = 1; i < gvcheck.rows.length - 1; i++) {
                if (gvcheck.rows[i + 1].cells[0].getElementsByTagName("INPUT")[0].checked) {
                    count++;
                }
            }

            if (count == gvcheck.rows.length - 2) {
                headerchk.checked = true;
            }
            else {
                headerchk.checked = false;
            }
        }

    </script>

    <vit:CustomerDetailsControl runat="server" ID="udcInputForm" />

    <div id="divAddEditCustomerDialog">
        <iframe id="iframeAdd" style="width: 100%; height: 100%; border: none"></iframe>
    </div>
</asp:Content>
