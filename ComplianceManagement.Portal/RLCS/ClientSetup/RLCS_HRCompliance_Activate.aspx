﻿<%@ Page Title="HR+ Compliance-Activate::Setup HR+" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="RLCS_HRCompliance_Activate.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.ClientSetup.RLCS_HRCompliance_Activate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="/NewCSS/arrow-Progressbar.css" rel="stylesheet" />
 <link href="../../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../../Newjs/kendo.all.min.js"></script>



    <script type="text/javascript">
        $(document).ready(function () {
            $('#divReAssignCompliance').hide();
            initializeCombobox();
        });
        function OpenReAssignPopup(ID) {
            debugger;
            $('#divReAssignCompliance').show();

            var myWindowAdv = $("#divReAssignCompliance");
            
            myWindowAdv.kendoWindow({
                width: "85%",
                height: '90%',
                //maxHeight: '90%',
                //minHeight:'50%',
                title: "Re-Assign Compliance",
                visible: false,
                actions: ["Close"],
                
            });

            $('#iframeReAssign').attr('src', 'about:blank');
            myWindowAdv.data("kendoWindow").center().open();
            $('#iframeReAssign').attr('src', "/RLCS/ClientSetup/RLCS_ComplianceReAssign.aspx?CustID=" + ID);

            return false;
        }

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }

        function initializeCombobox() {

            $("#<%= ddlCustomer.ClientID %>").combobox();
            $("#<%= ddlScopeType.ClientID %>").combobox();
        }

        function initializeDatePicker(date) {
            var startDate = new Date();
            $(".StartDate").datepicker({
                dateFormat: 'dd-mm-yy',
                setDate: startDate,
                numberOfMonths: 1,
                changeMonth: true,
                changeYear: true,
            });
        }

        function setDate() {
            $(".StartDate").datepicker();
        }

        function initializeConfirmDatePicker(date) {
            var startDate = new Date();
            $('#<%= tbxStartDate.ClientID %>').datepicker({
                dateFormat: 'dd-mm-yy',
                numberOfMonths: 1,
                changeMonth: true,
                changeYear: true,
            });

            initializeDatePicker();

            <%--$('#<%= txtStartDate.ClientID %>').datepicker({
                dateFormat: 'dd-mm-yy',
                numberOfMonths: 1
            });--%>
        }

        function Confirm() {
            var a = document.getElementById("BodyContent_tbxStartDate").value;
            if (a != "") {
                var confirm_value = document.createElement("INPUT");
                confirm_value.type = "hidden";
                confirm_value.name = "confirm_value";
                var Date = document.getElementById("<%=tbxStartDate.ClientID %>").value;
                confirm_value.value = "";
                confirm_value
                if (confirm("Start date is " + Date + ", to continue saving click OK!!")) {
                    return true;
                } else {
                    return false;
                }
            }
            else {
                alert("Start date should not be blank..!");
            }
        };
        <%--   function Confirm() {
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            var Date = document.getElementById("<%=tbxStartDate.ClientID %>").value;
            confirm_value.value = "";
            confirm_value
            if (confirm("Start date is " + Date+", to continue saving click OK!!")) {
                return true;
            } else {
                return false;
            }
        }--%>
    </script>

    <style type="text/css">
        .td1 {
            width: 10%;
        }

        .td2 {
            width: 23%;
        }

        .td3 {
            width: 10%;
        }

        .td4 {
            width: 23%;
        }

        .td3 {
            width: 10%;
        }

        .td4 {
            width: 23%;
        }

        /*.custom-combobox-input {
            width: 225px;
        }*/
         .bk{background-color: beige;}  

          .alert-success {	
        color: #3c763d;	
        background-color: #dff8e3;	
        border-color: #5c9566;	
        }	
    .alert {	
        /*padding: 15px;	
        margin-bottom: 20px;*/	
        border: 1px solid transparent;	
        border-radius: 4px;	
    }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 25%; left: 40%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

    <div class="container">
        <div style="margin-left: 1%;">
            <div class="arrow-steps clearfix">
                <div class="step done">
                    <asp:LinkButton ID="lnkBtnCustomer" PostBackUrl="~/RLCS/ClientSetup/RLCS_CustomerCorporateList.aspx" runat="server" Text="Customer"></asp:LinkButton>
                </div>
                <div class="step done">
                    <asp:LinkButton ID="lnkBtnEntityCustomerBranch" runat="server" Text="Entity-Branch"></asp:LinkButton>
                    <%--PostBackUrl="~/RLCS/Setup/RLCS_EntityBranchList.aspx"--%>
                </div>
                <div class="step done">
                    <asp:LinkButton ID="lnkBtnUser" PostBackUrl="~/RLCS/ClientSetup/RLCS_UserMaster.aspx" runat="server" Text="User"></asp:LinkButton>
                </div>
                <div class="step">
                    <asp:LinkButton ID="lnkBtnEmployee" PostBackUrl="~/RLCS/ClientSetup/RLCS_EmployeeMaster.aspx" runat="server" Text="Employee"></asp:LinkButton>
                </div>
                <div class="step done">
                    <asp:LinkButton ID="lnkBtnEntityAssign" PostBackUrl="~/RLCS/ClientSetup/RLCS_EntityAssignment.aspx" runat="server" Text="Entity-Branch Assignment"></asp:LinkButton>
                </div>
                <div class="step done">
                    <asp:LinkButton ID="lnkBtnComAssign" PostBackUrl="~/RLCS/ClientSetup/RLCS_HRCompliance_Assign.aspx" runat="server" Text="Compliance Assignment"></asp:LinkButton>
                </div>
                <div class="step current">
                    <asp:LinkButton ID="lnkBtnComAct" PostBackUrl="~/RLCS/ClientSetup/RLCS_HRCompliance_Activate.aspx" runat="server" Text="Compliance Activation"></asp:LinkButton>
                </div>
            </div>
        </div>
    </div>

    <asp:UpdatePanel ID="upComplianceTypeList" runat="server" UpdateMode="Conditional" OnLoad="upComplianceTypeList_Load">
        <ContentTemplate>
            <center>
                <table runat="server" width="100%">
                    <tr>
                        <td colspan="6" align="left">
                            <asp:CompareValidator ControlToValidate="tbxFilterLocation" ErrorMessage="Please select Location."
                                runat="server" ValueToCompare="< Select Location >" Operator="NotEqual" ValidationGroup="ComplianceInstanceValidationGroup"
                                Display="None" />
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="vdsummary" ValidationGroup="ComplianceInstanceValidationGroup" />
                            <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                            <asp:Label ID="lblErrorMessage" runat="server" Style="color: Red"></asp:Label>
                        </td>
                    </tr>

                    <tr>
                        <td class="td1">
                            <label style="display: block; font-size: 13px; color: #333; float: right; font-weight: bold;">
                                Customer</label>
                        </td>
                        <td class="td2">
                            <asp:DropDownList runat="server" ID="ddlCustomer" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlCustomer_SelectedIndexChanged" />
                        </td>

                        <td class="td3" align="right">
                            <label style="display: block; font-size: 13px; color: #333; float: right; font-weight: bold;">
                                Entity/Branch
                            </label>
                        </td>
                        <td class="td4" align="left">
                            <asp:TextBox runat="server" ID="tbxFilterLocation" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;" CssClass="txtbox" />
                            <div style="margin-left: 0px; position: absolute; z-index: 10" id="divFilterLocation">
                                <asp:TreeView runat="server" ID="tvFilterLocation" BackColor="White" BorderColor="Black"
                                    BorderWidth="1" SelectedNodeStyle-Font-Bold="true" Style="min-height: 50px; max-height: 200px; overflow: auto; width: 390px;"
                                    ShowLines="true" OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged">
                                </asp:TreeView>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="td1">
                            <label style="display: block; font-size: 13px; color: #333; float: right; font-weight: bold;">
                                Compliance Type
                            </label>
                        </td>
                        <td class="td2">
                            <asp:DropDownList runat="server" ID="ddlScopeType" Style="padding: 0px; margin: 0px; height: 22px; width: 100%;"
                                CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlScopeType_SelectedIndexChanged">
                                <asp:ListItem Text="All" Value="-1"></asp:ListItem>
                                <asp:ListItem Text="Register" Value="SOW03"></asp:ListItem>
                                <asp:ListItem Text="PF Challan" Value="SOW13"></asp:ListItem>
                                <asp:ListItem Text="ESI Challan" Value="SOW14"></asp:ListItem>
                                <asp:ListItem Text="PT Challan" Value="SOW17"></asp:ListItem>
                                <asp:ListItem Text="Return" Value="SOW05"></asp:ListItem>
                            </asp:DropDownList>
                        </td>

                        <td class="td3" align="right">
                            <label style="display: block; font-size: 13px; color: #333; float: right; font-weight: bold;">
                                Start Date
                            </label>
                        </td>
                        <td class="td4" align="left">
                            <asp:TextBox runat="server" ID="tbxStartDate" Style="height: 22px; width: 40%;" AutoPostBack="true" OnTextChanged="tbxStartDate_TextChanged" />
                            <asp:RequiredFieldValidator ID="reqfldfordate" runat="server" ControlToValidate="tbxStartDate" ValidationGroup="ComplianceInstanceValidationGroup"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:Button ID="btnReAssign" runat="server" CssClass="btn btn-primary" OnClick="btnReAssign_Click" Text="Re-Assign"  Visible="false"/>
                        </td>
                    </tr>

                    <tr style="display: none;">
                        <td class="td1" align="right">
                            <asp:CheckBox ID="chkToActiveInstance" runat="server" AutoPostBack="true" OnCheckedChanged="chkToActiveInstance_CheckedChanged" Checked="true" />
                        </td>
                        <td class="td2" align="left">
                            <label style="width: 100px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                                To Activate
                            </label>
                        </td>
                        <td class="td3" align="right">
                            <asp:CheckBox ID="chkToDeActiveInstance" runat="server" AutoPostBack="true" OnCheckedChanged="chkToDeActiveInstance_CheckedChanged" />
                        </td>
                        <td class="td4" align="left">
                            <label style="width: 100px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                                To De-Activate
                            </label>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="6" align="center" runat="server">
                            <asp:Panel ID="Panel1" Width="100%" Style="min-height: 10px; overflow-y: auto; margin-bottom: 10px;" runat="server">
                                <asp:GridView runat="server" ID="grdComplianceRoleMatrix" AutoGenerateColumns="false" GridLines="Vertical" OnRowCreated="grdComplianceRoleMatrix_RowCreated"
                                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true" OnSorting="grdComplianceRoleMatrix_Sorting"
                                    CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="50" OnRowDataBound="grdComplianceRoleMatrix__RowDataBound" Width="100%"
                                    Font-Size="12px" DataKeyNames="ID" OnPageIndexChanging="grdComplianceRoleMatrix_PageIndexChanging" ShowHeaderWhenEmpty="true">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Compliance ID" SortExpression="ComplianceID" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <%--width: 150px;--%>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                    <asp:Label ID="lblComplianceID" runat="server" Text='<%# Eval("ComplianceID")%>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Short Description" SortExpression="Description">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 530px;">
                                                    <asp:Label ID="lblShortDescription" runat="server" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("Description") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Location/Branch" SortExpression="Location">
                                            <ItemTemplate>
                                                <%--width: 150px;--%>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                    <asp:Label ID="lblLocation" runat="server" Text='<%# Eval("Location")%>'></asp:Label>
                                                    <asp:Label ID="lblBranchID" runat="server" Text='<%# Eval("CustomerBranchID") %>' Visible="false"></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Assigned User" SortExpression="Users">
                                            <ItemTemplate>
                                                <%--width: 550px;--%>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                    <asp:Label ID="lblUsers" runat="server" Text='<%# Eval("Users") %>'></asp:Label>
                                                    <asp:Label ID="lblUserID" runat="server" Text='<%# Eval("userID") %>' Visible="false"></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Role" SortExpression="Role">
                                            <ItemTemplate>
                                                <%-- width: 550px;--%>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                    <asp:Label ID="lblRole" runat="server" Text='<%# Eval("Role") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Start Date" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:UpdatePanel ID="UpStartDate" runat="server">
                                                    <ContentTemplate>
                                                        <asp:TextBox ID="txtStartDate" CssClass="StartDate" runat="server" ReadOnly="true"
                                                            Style="text-align: center;"></asp:TextBox>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:PostBackTrigger ControlID="txtStartDate" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="chkActivateSelectAll" Text="Activate" runat="server" AutoPostBack="true"
                                                    OnCheckedChanged="chkActivateSelectAll_CheckedChanged" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkActivate" runat="server" AutoPostBack="true" OnCheckedChanged="chkActivate_CheckedChanged" />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                    <FooterStyle BackColor="#CCCC99" />
                                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Small" />
                                    <PagerSettings Position="Top" />
                                    <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                    <AlternatingRowStyle BackColor="#E6EFF7" />
                                    <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                                    <EmptyDataTemplate>
                                        No Records Found.
                                    </EmptyDataTemplate>
                                </asp:GridView>

                                <asp:GridView runat="server" ID="grdToDeactivate" AutoGenerateColumns="false" GridLines="Vertical" OnRowCreated="grdToDeactivate_RowCreated"
                                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true" OnSorting="grdToDeactivate_Sorting"
                                    CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="50" OnRowDataBound="grdToDeactivate__RowDataBound"
                                    Font-Size="12px" DataKeyNames="ID" OnPageIndexChanging="grdToDeactivate_PageIndexChanging">
                                    <Columns>
                                        <asp:TemplateField HeaderText="ComplianceID" SortExpression="Sections">
                                            <ItemTemplate>
                                                <%--width: 150px;--%>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                    <asp:Label ID="lblComplianceID" runat="server" Text='<%# Eval("ComplianceID")%>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Description" SortExpression="Description">
                                            <ItemTemplate>

                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 500px;">
                                                    <asp:Label ID="lblShortDescription" runat="server" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("Description") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Users" SortExpression="Users">
                                            <ItemTemplate>
                                                <%--width: 550px;--%>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                    <asp:Label ID="lblUsers" runat="server" Text='<%# Eval("Users") %>'></asp:Label>
                                                    <asp:Label ID="lblUserID" runat="server" Text='<%# Eval("userID") %>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lblComplianceInstanceID" runat="server" Text='<%# Eval("ComplianceInstanceID") %>' Visible="false"></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Role" SortExpression="Role">
                                            <ItemTemplate>
                                                <%-- width: 550px;--%>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                    <asp:Label ID="lblRole" runat="server" Text='<%# Eval("Role") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="CreatedOn" SortExpression="CreatedOn">
                                            <ItemTemplate>
                                                <%-- width: 550px;--%>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                    <%-- <asp:Label ID="lblCreatedOn" runat="server" Text='<%# Eval("CreatedOn") %>'  ></asp:Label>--%>
                                                    <%# Convert.ToDateTime(Eval("CreatedOn")).ToString("dd-MMM-yyyy") %>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Start Date" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <%--<asp:Label ID="lblStartDate" runat="server" Text='<%# Eval("StartDate") %>'  ></asp:Label>--%>
                                                <%# Convert.ToDateTime(Eval("StartDate")).ToString("dd-MMM-yyyy") %>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                            <HeaderTemplate>

                                                <asp:CheckBox ID="chkDeActivateSelectAll" Text="Deactivate" runat="server" AutoPostBack="true" OnCheckedChanged="chkDeActivateSelectAll_CheckedChanged" />

                                            </HeaderTemplate>
                                            <ItemTemplate>

                                                <asp:CheckBox ID="chkDeActivate" runat="server" AutoPostBack="true" OnCheckedChanged="chkDeActivate_CheckedChanged" />


                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                    <FooterStyle BackColor="#CCCC99" />
                                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Small" />
                                    <PagerSettings Position="Top" />
                                    <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                    <AlternatingRowStyle BackColor="#E6EFF7" />
                                    <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                                    <EmptyDataTemplate>
                                        No Records Found.
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </asp:Panel>
                            <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" OnClientClick="return Confirm();" CssClass="button"
                                ValidationGroup="ComplianceInstanceValidationGroup" Visible="false" />
                        </td>
                    </tr>
                </table>
            </center>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div id="divReAssignCompliance">
        <iframe id="iframeReAssign" style="width: 100%; height: 100%; border: none; " ></iframe>
    </div>
</asp:Content>
