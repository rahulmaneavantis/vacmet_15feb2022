﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS.ClientSetup
{
    public partial class RLCS_EntityBranchDetails : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["CustID"]) && !string.IsNullOrEmpty(Request.QueryString["ClientID"]))
                    {
                        ViewState["ParentID"] = null;

                        BindCustomerStatus();
                        
                        BindStates();

                        btnSave.Enabled = true;
                        lblCheckClient.Visible = false;

                        BindLegalEntityType();
                        BindCompanyTypeType();

                        int customerID = Convert.ToInt32(Request.QueryString["CustID"]);

                        if (customerID != 0)
                        {
                            int complianceProductType = 0;
                            complianceProductType = RLCS_Master_Management.GetComplianceProductType(customerID);
                            ViewState["ComplianceProductType"] = complianceProductType;

                            if (complianceProductType == 1 || complianceProductType == 2)
                                ViewState["CorpID"] = RLCS_Master_Management.GetCorporateIDByCustID(customerID);

                            ViewState["CustomerID"] = customerID;
                        }

                        int entityClientID = Convert.ToInt32(Request.QueryString["ClientID"]);

                        if (entityClientID != 0)
                        {
                            EDIT_CUSTOMER_BRANCH(entityClientID);
                        }
                        else
                        {
                            Add_CUSTOMER_BRANCH();
                        }
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
                }
            }
        }



        /// <summary>
        /// This method for binding status.
        /// </summary>
        public void BindCustomerStatus()
        {
            try
            {
                ddlCustomerStatus.DataTextField = "Name";
                ddlCustomerStatus.DataValueField = "ID";

                ddlCustomerStatus.DataSource = Enumerations.GetAll<CustomerStatus>();
                ddlCustomerStatus.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        private void BindLegalRelationShips(bool parent)
        {
            try
            {
                ddlLegalRelationShip.DataTextField = "Name";
                ddlLegalRelationShip.DataValueField = "ID";

                var legalRelationShips = Enumerations.GetAll<LegalRelationship>();
                if (!parent)
                {
                    legalRelationShips.RemoveAt(0);
                }
                ddlLegalRelationShip.DataSource = legalRelationShips;
                ddlLegalRelationShip.DataBind();

                ddlLegalRelationShip.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        private void BindBranchTypes()
        {
            try
            {
                ddlType.DataSource = null;
                ddlType.DataBind();
                ddlType.ClearSelection();

                ddlType.DataTextField = "Name";
                ddlType.DataValueField = "ID";

                var dataSource = CustomerBranchManagement.GetAllNodeTypes();
                if (ViewState["ParentID"] == null)
                {
                    dataSource = dataSource.Where(entry => entry.ID == 1).ToList();
                }
                else
                {
                    dataSource = dataSource.Where(entry => entry.ID != 1).ToList();
                }

                ddlType.DataSource = dataSource;
                ddlType.DataBind();
                //ddlType.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        private void BindLegalEntityType()
        {
            try
            {

                ddlLegalEntityType.DataTextField = "EntityTypeName";
                ddlLegalEntityType.DataValueField = "ID";
                ddlLegalEntityType.DataSource = CustomerBranchManagement.GetAllLegalEntityType();
                ddlLegalEntityType.DataBind();
                ddlLegalEntityType.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        private void BindCompanyTypeType()
        {
            try
            {

                ddlCompanyType.DataTextField = "Name";
                ddlCompanyType.DataValueField = "ID";
                ddlCompanyType.DataSource = CustomerBranchManagement.GetAllComanyType();
                ddlCompanyType.DataBind();
                ddlCompanyType.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }
        
        private void BindCities()
        {
            try
            {
                if (ddlState.SelectedValue != "" || !string.IsNullOrEmpty(ddlState.SelectedValue) || ddlState.SelectedValue != "-1")
                {
                    ddlCity.Items.Clear();

                    //ddlCity.DataTextField = "Name";
                    //ddlCity.DataValueField = "ID";

                    //ddlCity.DataSource = AddressManagement.GetAllCitiesByState(Convert.ToInt32(ddlState.SelectedValue));
                    //ddlCity.DataBind();

                    ddlCity.DataTextField = "Name";
                    ddlCity.DataValueField = "ID";

                    ddlCity.DataSource = RLCS_Master_Management.FillLocationCityByStateID(Convert.ToInt32(ddlState.SelectedValue));
                    ddlCity.DataBind();

                    //ddlCity.Items.Insert(0, new ListItem("< Other >", "0"));
                    ddlCity.Items.Insert(0, new ListItem("< Select >", "-1"));
                }
                else
                {
                    ddlCity.Items.Clear();
                    ddlCity.Items.Insert(0, new ListItem("<Select City>", "-1"));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        private void BindStates()
        {
            try
            {
                ddlState.DataTextField = "Name";
                ddlState.DataValueField = "ID";

                //ddlState.DataSource = AddressManagement.GetAllStates();
                //ddlState.DataBind();

                ddlState.DataSource = RLCS_Master_Management.FillStates();
                ddlState.DataBind();

                ddlState.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindCities();
            ddlCity.SelectedValue = "-1";
        }

        protected void ddlCity_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlCity.SelectedValue == "0" && divOther.Visible == false)
                {
                    divOther.Visible = true;
                    tbxOther.Text = string.Empty;
                }
                else if (ddlCity.SelectedValue != "0" && string.IsNullOrEmpty(tbxPinCode.Text.Trim()))
                {
                    if (ddlCity.SelectedItem.Text.Contains("-"))
                    {
                        string[] city = ddlCity.SelectedItem.Text.Split('-');

                        if (city != null)
                        {
                            if (city.Length == 2)
                                tbxPinCode.Text = city[1];
                        }
                    }

                    divOther.Visible = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void ddlType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                divLegalRelationship.Visible = ddlType.SelectedValue == "1";
                string industryId = string.Empty;

                //if (ViewState["IndustryId"] != null)
                //{
                //    BindLegalRelationShipOrStatus(Convert.ToInt32(ViewState["IndustryId"].ToString()));
                //    //ddlIndustry.SelectedValue = ViewState["IndustryId"].ToString();
                //}
                //if (!divIndustry.Visible)
                //{
                //    //    ddlIndustry.SelectedIndex = -1;
                //    //    if (ViewState["IndustryId"] == null)
                //    //BindLegalRelationShipOrStatus(0);
                //}
                if (!divLegalRelationship.Visible)
                {
                    ddlLegalRelationShip.SelectedIndex = -1;
                }

                if (ddlType.SelectedValue == "1")
                {
                    divLegalEntityType.Visible = true;
                    divCompanyType.Visible = true;

                    int complianceProductType = 0;
                    if (ViewState["ComplianceProductType"] != null)
                    {
                        complianceProductType = Convert.ToInt32(ViewState["ComplianceProductType"]);
                    }

                    if (complianceProductType > 0)
                    {
                        divClientID.Visible = true;
                        if (ViewState["Mode"].ToString() == "1")
                        {
                            lblCheckClient.Visible = false;
                        }
                        else
                            lblCheckClient.Visible = true;
                    }
                    else
                    {
                        divClientID.Visible = false;
                        lblCheckClient.Visible = false;
                        btnSave.Enabled = true;
                    }

                }
                else
                {
                    divLegalEntityType.Visible = false;
                    divCompanyType.Visible = false;

                    divClientID.Visible = false;
                    lblCheckClient.Visible = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        private void Add_CUSTOMER_BRANCH()
        {
            try
            {
                ViewState["Mode"] = 0;

                //ddlIndustry.Enabled = true;
                ddlType.Enabled = true;
                //ddlLegalRelationShipOrStatus.Enabled = true;
                ddlLegalRelationShip.Enabled = true;

                tbxName.Text = tbxAddressLine1.Text = tbxAddressLine2.Text = tbxOther.Text = tbxPinCode.Text = tbxContactPerson.Text = tbxLandline.Text = tbxMobile.Text = tbxEmail.Text = string.Empty;
                PopulateInputForm();

                tbxName.Enabled = true;

                //ddlIndustry.SelectedValue = "-1";
                ddlType.SelectedValue = "-1";
                ddlType_SelectedIndexChanged(null, null);

                ddlCompanyType.SelectedValue = "-1";

                ddlState.SelectedValue = "-1";
                ddlState_SelectedIndexChanged(null, null);
                ddlCity_SelectedIndexChanged(null, null);

                ddlCustomerStatus.SelectedIndex = -1;
                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        private void PopulateInputForm()
        {
            try
            {
                BindBranchTypes();
                BindLegalRelationShips(ViewState["ParentID"] == null);
                divParent.Visible = ViewState["ParentID"] != null;

                litCustomer.Text = CustomerManagement.GetByID(Convert.ToInt32(ViewState["CustomerID"])).Name;

                if (ViewState["ParentID"] != null)
                {
                    litParent.Text = CustomerBranchManagement.GetByID(Convert.ToInt32(ViewState["ParentID"])).Name;
                }

                if (!String.IsNullOrEmpty(litCustomer.Text))
                {
                    string customer = litCustomer.Text;
                    txtClientID.Text = GetClientID(litCustomer.Text);
                    txtClientID.Enabled = true;
                    lblCheckClient.Visible = false;
                    btnCheck.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        private void EDIT_CUSTOMER_BRANCH(int customerBranchID)
        {
            try
            {
                ViewState["Mode"] = 1;
                ViewState["CustomerBranchID"] = customerBranchID;
                
                CustomerBranch customerBranch = CustomerBranchManagement.GetByID(customerBranchID);

                PopulateInputForm();

                if (!string.IsNullOrEmpty(customerBranch.Name))
                {
                    tbxName.Text = customerBranch.Name;
                    tbxName.Enabled = false;
                }
                if (!string.IsNullOrEmpty(Convert.ToString(customerBranch.Type)))
                {
                    if (customerBranch.Type != -1)
                    {
                        ddlType.SelectedValue = customerBranch.Type.ToString();
                        ddlType_SelectedIndexChanged(null, null);
                    }
                }

                if (!string.IsNullOrEmpty(Convert.ToString(customerBranch.LegalRelationShipID)))
                {
                    if (customerBranch.LegalRelationShipID != -1 && customerBranch.LegalRelationShipID != 0)
                    {
                        ddlLegalRelationShip.SelectedValue = (customerBranch.LegalRelationShipID ?? -1).ToString();
                    }
                }
                if (!string.IsNullOrEmpty(Convert.ToString(customerBranch.LegalEntityTypeID)))
                {
                    if (customerBranch.LegalEntityTypeID != -1 && customerBranch.LegalEntityTypeID != 0)
                    {
                        ddlLegalEntityType.SelectedValue = (customerBranch.LegalEntityTypeID ?? -1).ToString();
                    }
                }
                if (!string.IsNullOrEmpty(Convert.ToString(customerBranch.ComType)))
                {
                    if (customerBranch.ComType != 0 && customerBranch.ComType != -1)
                    {
                        ddlCompanyType.SelectedValue = (customerBranch.ComType).ToString();
                    }
                }
                                                                  
                tbxAddressLine1.Text = customerBranch.AddressLine1;
                tbxAddressLine2.Text = customerBranch.AddressLine2;

                if (!string.IsNullOrEmpty(Convert.ToString(customerBranch.StateID)))
                {
                    if (customerBranch.StateID != -1)
                    {
                        ddlState.SelectedValue = customerBranch.StateID.ToString();
                        ddlState_SelectedIndexChanged(null, null);
                    }
                }

                if (!string.IsNullOrEmpty(Convert.ToString(customerBranch.CityID)))
                {
                    if (customerBranch.CityID != -1)
                    {
                        ddlCity.SelectedValue = customerBranch.CityID.ToString();
                        ddlCity_SelectedIndexChanged(null, null);
                    }
                }

                tbxOther.Text = customerBranch.Others;
                tbxPinCode.Text = customerBranch.PinCode;

                //tbxIndustry.Text = customerBranch.Industry;
                tbxContactPerson.Text = customerBranch.ContactPerson;
                tbxLandline.Text = customerBranch.Landline;
                tbxMobile.Text = customerBranch.Mobile;
                tbxEmail.Text = customerBranch.EmailID;
                if (!string.IsNullOrEmpty(Convert.ToString(customerBranch.Status)))
                {
                    if (customerBranch.Status != -1)
                    {
                        ddlCustomerStatus.SelectedValue = Convert.ToString(customerBranch.Status);
                    }
                }
                if (!string.IsNullOrEmpty(Convert.ToString(customerBranch.AuditPR)))
                {
                    if (customerBranch.AuditPR == false)
                    {
                        ddlPersonResponsibleApplicable.SelectedValue = Convert.ToString(0);
                    }
                    else
                    {
                        ddlPersonResponsibleApplicable.SelectedValue = Convert.ToString(1);
                    }
                }
                //ddlIndustry.Enabled = false;
                ddlType.Enabled = false;
                //ddlLegalRelationShipOrStatus.Enabled = false;
                ddlLegalRelationShip.Enabled = false;

                txtClientID.Text = RLCS_Master_Management.GetClientIDByBranchID(customerBranchID);

                if (ViewState["Mode"] != null)
                {
                    if ((int)ViewState["Mode"] == 1)
                    {
                        txtClientID.Enabled = false;
                        btnCheck.Enabled = false;
                        btnSave.Enabled = true;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                bool saveSuccess = false;
                int avacom_CustomerID = 0;

                if (!string.IsNullOrEmpty(txtClientID.Text))
                {
                    if (!string.IsNullOrEmpty(ddlType.SelectedValue))
                    {
                        if (ddlType.SelectedValue == "1")
                        {
                            if (ddlCompanyType.SelectedValue == "-1")
                            {
                                cvDuplicateEntry.ErrorMessage = "Please select Type.";
                                cvDuplicateEntry.IsValid = false;
                                return;
                            }
                        }
                    }
                    else
                    {
                        cvDuplicateEntry.ErrorMessage = "Please select Type.";
                        cvDuplicateEntry.IsValid = false;
                        return;
                    }

                    int comType = 0;
                    if (!string.IsNullOrEmpty(ddlCompanyType.SelectedValue))
                    {
                        if (ddlCompanyType.SelectedValue.ToString() == "-1")
                        {
                            comType = 0;
                        }
                        else
                        {
                            comType = Convert.ToInt32(ddlCompanyType.SelectedValue);
                        }
                    }

                    CustomerBranch customerBranch = new CustomerBranch()
                    {
                        Name = tbxName.Text.Trim(),
                        Type = Convert.ToByte(ddlType.SelectedValue),
                        ComType = Convert.ToByte(comType),
                        AddressLine1 = tbxAddressLine1.Text.Trim(),
                        AddressLine2 = tbxAddressLine2.Text.Trim(),
                        StateID = Convert.ToInt32(ddlState.SelectedValue),
                        CityID = Convert.ToInt32(ddlCity.SelectedValue),
                        Others = tbxOther.Text.Trim(),
                        PinCode = tbxPinCode.Text.Trim(),
                        //Industry = Convert.ToInt32(ddlIndustry.SelectedValue),
                        ContactPerson = tbxContactPerson.Text.Trim(),
                        Landline = tbxLandline.Text.Trim(),
                        Mobile = tbxMobile.Text.Trim(),
                        EmailID = tbxEmail.Text.Trim(),
                        CustomerID = Convert.ToInt32(ViewState["CustomerID"]),
                        ParentID = ViewState["ParentID"] == null ? (int?)null : Convert.ToInt32(ViewState["ParentID"]),
                        Status = Convert.ToInt32(ddlCustomerStatus.SelectedValue)
                    };

                    avacom_CustomerID = customerBranch.CustomerID;

                    if (ddlPersonResponsibleApplicable.SelectedItem.Text == "Yes")
                    {
                        customerBranch.AuditPR = true;
                    }
                    if (ddlType.SelectedValue == "1")
                    {
                        if (!string.IsNullOrEmpty(ddlLegalRelationShip.SelectedValue))
                        {
                            customerBranch.LegalRelationShipID = Convert.ToInt32(ddlLegalRelationShip.SelectedValue);
                        }
                        if (!string.IsNullOrEmpty(ddlLegalEntityType.SelectedValue))
                        {
                            customerBranch.LegalEntityTypeID = Convert.ToInt32(ddlLegalEntityType.SelectedValue);
                        }
                    }
                    else
                    {
                        customerBranch.LegalRelationShipID = null;
                        customerBranch.LegalEntityTypeID = null;
                    }

                    if (!string.IsNullOrEmpty(ddlLegalRelationShip.SelectedValue))
                    {
                        if (ddlLegalRelationShip.SelectedValue != "-1")
                            customerBranch.LegalRelationShipOrStatus = Convert.ToByte(ddlLegalRelationShip.SelectedValue);
                        else
                            customerBranch.LegalRelationShipOrStatus = 0;
                    }
                    else
                    {
                        customerBranch.LegalRelationShipOrStatus = 0;
                    }

                    if ((int)ViewState["Mode"] == 1)
                    {
                        customerBranch.ID = Convert.ToInt32(ViewState["CustomerBranchID"]);
                    }

                    if (CustomerBranchManagement.Exists(customerBranch, Convert.ToInt32(ViewState["CustomerID"])))
                    {
                        cvDuplicateEntry.ErrorMessage = "Customer branch name already exists.";
                        cvDuplicateEntry.IsValid = false;
                        return;
                    }

                    if ((int)ViewState["Mode"] == 0)
                    {
                        int resultData = 0;
                        resultData = CustomerBranchManagement.Create(customerBranch);

                        if (resultData > 0)
                        {
                            saveSuccess = true;
                            // Added by SACHIN 28 April 2016
                            string ReplyEmailAddressName = "Avantis";
                            string portalURL = string.Empty;
                            URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(AuthenticationHelper.CustomerID));
                            if (Urloutput != null)
                            {
                                portalURL = Urloutput.URL;
                            }
                            else
                            {
                                portalURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                            }
                            string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EMailTemplate_NewCustomerBranchCreaded
                                               .Replace("@NewCustomer", litCustomer.Text)
                                               .Replace("@BranchName", tbxName.Text)
                                               .Replace("@LoginUser", AuthenticationHelper.User)
                                               .Replace("@PortalURL", Convert.ToString(portalURL))
                                               .Replace("@From", ReplyEmailAddressName)
                                               .Replace("@URL", Convert.ToString(portalURL));
                            //string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EMailTemplate_NewCustomerBranchCreaded
                            //                    .Replace("@NewCustomer", litCustomer.Text)
                            //                    .Replace("@BranchName", tbxName.Text)
                            //                    .Replace("@LoginUser", AuthenticationHelper.User)
                            //                    .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                            //                    .Replace("@From", ReplyEmailAddressName)
                            //                    .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]));

                            string SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"].ToString();
                            string CustomerCreatedEmail = ConfigurationManager.AppSettings["CustomerCreatedEmail"].ToString();

                            //EmailManager.SendMail(SenderEmailAddress, new List<String>(new String[] { CustomerCreatedEmail }), null, null, "AVACOM-New Branch Added", message);
                        }
                    }
                    else if ((int)ViewState["Mode"] == 1)
                    {
                        saveSuccess = CustomerBranchManagement.Update(customerBranch);
                    }

                    com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_CustomerBranch customerBranch1 = new com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_CustomerBranch()
                    {
                        Name = tbxName.Text.Trim(),
                        Type = Convert.ToByte(ddlType.SelectedValue),
                        ComType = Convert.ToByte(comType),
                        AddressLine1 = tbxAddressLine1.Text.Trim(),
                        AddressLine2 = tbxAddressLine2.Text.Trim(),
                        StateID = Convert.ToInt32(ddlState.SelectedValue),
                        CityID = Convert.ToInt32(ddlCity.SelectedValue),
                        Others = tbxOther.Text.Trim(),
                        PinCode = tbxPinCode.Text.Trim(),
                        //Industry = Convert.ToInt32(ddlIndustry.SelectedValue),
                        ContactPerson = tbxContactPerson.Text.Trim(),
                        Landline = tbxLandline.Text.Trim(),
                        Mobile = tbxMobile.Text.Trim(),
                        EmailID = tbxEmail.Text.Trim(),
                        CustomerID = Convert.ToInt32(ViewState["CustomerID"]),
                        ParentID = ViewState["ParentID"] == null ? (int?)null : Convert.ToInt32(ViewState["ParentID"]),
                        Status = Convert.ToInt32(ddlCustomerStatus.SelectedValue)
                    };

                    if (ddlPersonResponsibleApplicable.SelectedItem.Text == "Yes")
                    {
                        customerBranch1.AuditPR = true;
                    }

                    if (ddlType.SelectedValue == "1")
                    {
                        if (!string.IsNullOrEmpty(ddlLegalRelationShip.SelectedValue))
                        {
                            customerBranch1.LegalRelationShipID = Convert.ToInt32(ddlLegalRelationShip.SelectedValue);
                        }
                        if (!string.IsNullOrEmpty(ddlLegalEntityType.SelectedValue))
                        {
                            customerBranch1.LegalEntityTypeID = Convert.ToInt32(ddlLegalEntityType.SelectedValue);
                        }
                    }
                    else
                    {
                        customerBranch1.LegalRelationShipID = null;
                        customerBranch1.LegalEntityTypeID = null;
                    }

                    //if (!string.IsNullOrEmpty(ddlLegalRelationShip.SelectedValue))
                    //{
                    //    if (ddlLegalRelationShipOrStatus.SelectedValue != "-1")
                    //        customerBranch1.LegalRelationShipOrStatus = Convert.ToByte(ddlLegalRelationShipOrStatus.SelectedValue);
                    //    else
                    //        customerBranch1.LegalRelationShipOrStatus = 0;
                    //}
                    //else
                    //{
                    //    customerBranch1.LegalRelationShipOrStatus = 0;
                    //}

                    if ((int)ViewState["Mode"] == 1)
                    {
                        customerBranch1.ID = Convert.ToInt32(ViewState["CustomerBranchID"]);
                    }

                    if (CustomerBranchManagement.Exists1(customerBranch1, Convert.ToInt32(ViewState["CustomerID"])))
                    {
                        cvDuplicateEntry.ErrorMessage = "Branch with same name already exists";
                        cvDuplicateEntry.IsValid = false;
                        return;
                    }

                    if ((int)ViewState["Mode"] == 0)
                    {
                        saveSuccess = CustomerBranchManagement.Create1(customerBranch1);                       
                    }
                    else if ((int)ViewState["Mode"] == 1)
                    {
                        saveSuccess = CustomerBranchManagement.Update1(customerBranch1);                        
                    }

                    //Create or Update CustomerBranch in RLCS DB
                    int complianceProductType = 0;
                    if (ViewState["ComplianceProductType"] != null)
                    {
                        complianceProductType = Convert.ToInt32(ViewState["ComplianceProductType"]);
                    }

                    if (complianceProductType > 0)
                    {
                        string stateCode = string.Empty, cityCode = string.Empty;
                        string corpID = string.Empty;
                        string clientID = string.Empty;

                        if (ViewState["CorpID"] != null)
                            corpID = Convert.ToString(ViewState["CorpID"]);

                        if (ViewState["ParentID"] == null)
                            clientID = txtClientID.Text.Trim();
                        else
                        {
                            if (ViewState["EntityClientID"] != null)
                                clientID = Convert.ToString(ViewState["EntityClientID"]);
                        }

                        if (saveSuccess && !string.IsNullOrEmpty(corpID) && !string.IsNullOrEmpty(clientID) && avacom_CustomerID != 0)
                        {
                            RLCS_CustomerBranch_ClientsLocation_Mapping RLCS_CustomerBranch = new RLCS_CustomerBranch_ClientsLocation_Mapping()
                            {
                                AVACOM_CustomerID = avacom_CustomerID,
                                AVACOM_BranchID = customerBranch.ID,
                                AVACOM_BranchName = customerBranch.Name,
                                BranchType = "E",
                                CM_Address = customerBranch.AddressLine1,
                                CO_CorporateID = corpID,
                                CM_ClientID = clientID,
                                CM_ClientName = customerBranch.Name,
                            };

                            if (ViewState["ParentID"] == null)
                                RLCS_CustomerBranch.BranchType = "E";
                            else
                                RLCS_CustomerBranch.BranchType = "B";

                            stateCode = RLCS_Master_Management.GetStateCodeByStateID(customerBranch.StateID);

                            if (!string.IsNullOrEmpty(stateCode))
                                RLCS_CustomerBranch.CM_State = stateCode;

                            cityCode = RLCS_Master_Management.GetCityCodeByCityID(customerBranch.CityID);

                            if (!string.IsNullOrEmpty(cityCode))
                            {
                                RLCS_CustomerBranch.CM_City = cityCode;
                                RLCS_CustomerBranch.CM_Pincode = cityCode;
                                RLCS_CustomerBranch.CL_Pincode = cityCode;
                            }

                            if (ddlCustomerStatus.SelectedItem.Text.Equals("Active"))
                                RLCS_CustomerBranch.CM_Status = "A";
                            else
                                RLCS_CustomerBranch.CM_Status = "I";

                            RLCSManagement.CreateUpdate_Partial_CustomerBranch_ClientsOrLocation_Mapping(RLCS_CustomerBranch);

                            //RLCSManagement.AddRLCSCustBranch(RLCS_CustomerBranch);
                        }
                    }
                }
                else
                {
                    cvDuplicateEntry.ErrorMessage = "Required ClientID";
                    cvDuplicateEntry.IsValid = false;
                    return;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void btnCheck_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(txtClientID.Text))
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    bool clientExists = false;

                    string rlcsAPIURL = ConfigurationManager.AppSettings["RLCSAPIURL"];
                    rlcsAPIURL += "AventisIntegration/CheckClientIdExists?ClientId=" + txtClientID.Text.Trim();

                    string responseData = RLCSAPIClasses.Invoke("GET", rlcsAPIURL, "");

                    if (responseData != null)
                    {
                        string data = responseData;
                        if (!string.IsNullOrWhiteSpace(data))
                            clientExists = Convert.ToBoolean(data);
                        else
                            clientExists = false;
                        if (clientExists)
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Entity/Client with Same ClientID aleady exists, Please choose other";
                            btnSave.Enabled = false;
                        }
                        else
                        {
                            clientExists = RLCS_Master_Management.Exists_ClientID(txtClientID.Text.Trim());

                            if (clientExists)
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Entity/Client with Same ClientID aleady exists, Please choose other";
                                btnSave.Enabled = false;
                            }
                            else
                                btnSave.Enabled = true;
                            lblCheckClient.Visible = false;
                        }
                    }
                    else
                        btnSave.Enabled = false;
                }
            }
            else
                btnSave.Enabled = false;
        }

        protected void tbxName_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["Mode"] != null)
                {
                    if ((int)ViewState["Mode"] == 0 && ViewState["ParentID"] == null)
                    {
                        txtClientID.Text = GetClientID(tbxName.Text.Trim());
                        btnSave.Enabled = false;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        public string GetClientID(string clientName)
        {
            StringBuilder result = new StringBuilder();
            try
            {
                foreach (char c in clientName)
                {
                    if (result.Length < 5)
                    {
                        if (Char.IsLetterOrDigit(c))
                            result.Append(c);
                    }
                }

                return "AVA" + result.ToString().ToUpper();
            }
            catch (Exception ex)
            {
                return result.ToString();
            }
        }
        
    }
}