﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS.ClientSetup
{
    public partial class RLCS_ComplianceReAssign : System.Web.UI.Page
    {
        public static List<int> branchList = new List<int>();

        protected void Page_Load(object sender, EventArgs e)
        {
            vsHRCompAssign.CssClass = "alert alert-danger";
            if (HttpContext.Current.Request.IsAuthenticated)
            {
                if (!IsPostBack)
                {
                    btnSave.Visible = false;

                    if (!string.IsNullOrEmpty(Request.QueryString["CustID"]))
                    {
                        var custID = Request.QueryString["CustID"];

                        if (!string.IsNullOrEmpty(custID))
                        {
                            int customerID = Convert.ToInt32(custID);

                            if (customerID != 0)
                            {
                                BindCustomers();
                                //BindUsers(customerID, ddlUser, false);                            

                                if (ddlCustomer.Items.FindByValue(customerID.ToString()) != null)
                                {
                                    ddlCustomer.ClearSelection();
                                    ddlCustomer.Items.FindByValue(customerID.ToString()).Selected = true;

                                    ddlCustomer_SelectedIndexChanged(sender, e);
                                }
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
                            }
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
                        }
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
                    }
                }
            }
            else
            {
                FormsAuthentication.SignOut();
                Session.Abandon();
                FormsAuthentication.RedirectToLoginPage();
            }
        }

        protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                {
                    BindLocationFilter(Convert.ToInt32(ddlCustomer.SelectedValue));
                    BindUsers(ddlUser, false);

                    tvFilterLocation_SelectedNodeChanged(sender, e);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void ddlUser_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue) && !string.IsNullOrEmpty(ddlUser.SelectedValue))
                {
                    BindUsers(ddlNewUser, true);

                    if (ddlNewUser.Items.FindByValue(ddlUser.SelectedValue) != null)
                    {                        
                        ddlNewUser.Items.Remove(ddlNewUser.Items.FindByValue(ddlUser.SelectedValue));
                    }

                    BindGrid(Convert.ToInt32(ddlCustomer.SelectedValue));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        private void BindCustomers()
        {
            try
            {
                ddlCustomer.DataTextField = "Name";
                ddlCustomer.DataValueField = "ID";

                int customerID = -1;
                int serviceProviderID = -1;
                int distributorID = -1;

                if (AuthenticationHelper.Role == "CADMN")
                {
                    //customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "SPADM")
                {
                    serviceProviderID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "DADMN")
                {
                    distributorID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }

                ddlCustomer.DataSource = CustomerManagement.GetAll_HRComplianceCustomersByServiceProviderOrDistributor(customerID, serviceProviderID, distributorID, 1, false);
                ddlCustomer.DataBind();

                if (AuthenticationHelper.Role == "CADMN")
                {
                    if (ddlCustomer.Items.FindByValue(customerID.ToString()) != null)
                    {
                        ddlCustomer.ClearSelection();
                        ddlCustomer.Items.FindByValue(customerID.ToString()).Selected = true;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        private void BindLocationFilter(int customerID)
        {
            try
            {
                if (customerID != -1)
                {
                    var bracnhes = RLCS_ClientsManagement.GetAllHierarchy(customerID);

                    tvFilterLocation.Nodes.Clear();
                    TreeNode node = new TreeNode("Entity/Branch", "-1");
                    node.Selected = true;
                    tvFilterLocation.Nodes.Add(node);

                    foreach (var item in bracnhes)
                    {
                        node = new TreeNode(item.Name, item.ID.ToString());
                        node.SelectAction = TreeNodeSelectAction.Expand;
                        BindBranchesHierarchy(node, item);
                        tvFilterLocation.Nodes.Add(node);
                    }

                    tvFilterLocation.CollapseAll();

                    tvFilterLocation.CollapseAll();
                    //tvFilterLocation_SelectedNodeChanged(null, null);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp)
        {
            try
            {
                foreach (var item in nvp.Children)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    BindBranchesHierarchy(node, item);
                    parent.ChildNodes.Add(node);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        private void BindUsers(DropDownList ddlUserList, bool addSelect)
        {
            try
            {
                int customerID = -1;
                int serviceProviderID = -1;
                int distributorID = -1;

                if (AuthenticationHelper.Role == "CADMN")
                {
                    //customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "SPADM")
                {
                    if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                    {
                        customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                    }
                    distributorID = RLCS_Master_Management.Get_DistributorID(customerID);
                    //serviceProviderID = Convert.ToInt32(AuthenticationHelper.CustomerID);                    
                }
                else if (AuthenticationHelper.Role == "DADMN")
                {
                    if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                    {
                        customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                    }
                    distributorID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else
                {
                    if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                    {
                        customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                    }
                }

                if (customerID != -1)
                {
                    ddlUserList.DataTextField = "Name";
                    ddlUserList.DataValueField = "ID";
                    ddlUserList.Items.Clear();

                    var users = UserCustomerMappingManagement.GetAllUser_ServiceProviderDistributorCustomer(customerID, serviceProviderID, distributorID);
                    //var users = RLCS_Master_Management.GetAll_RLCSUsers_IncludingServiceProviderOrDistributor(customerID, serviceProviderID, distributorID, 2);
                    //var users = UserManagement.GetAllNVP(customerID, ids: ids, Flags: true);

                    ddlUserList.DataSource = users;
                    ddlUserList.DataBind();

                    if (addSelect)
                        ddlUserList.Items.Insert(0, new ListItem("Select", "-1"));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        //private void BindUsers(int customerID, DropDownList ddlUserList, bool addSelect)
        //{
        //    try
        //    {
        //        int serviceProviderID = -1;
        //        int distributorID = -1;

        //        if (AuthenticationHelper.Role == "SPADM")
        //        {
        //            serviceProviderID = Convert.ToInt32(AuthenticationHelper.CustomerID);
        //        }
        //        else if (AuthenticationHelper.Role == "DADMN")
        //        {
        //            distributorID = Convert.ToInt32(AuthenticationHelper.CustomerID);
        //        }

        //        if (customerID != -1)
        //        {
        //            ddlUserList.DataTextField = "Name";
        //            ddlUserList.DataValueField = "ID";
        //            ddlUserList.Items.Clear();

        //            var users = RLCS_Master_Management.GetAll_RLCSUsers_IncludingServiceProviderOrDistributor(customerID, serviceProviderID, distributorID, 2);
        //            //var users = UserManagement.GetAllNVP(customerID, ids: ids, Flags: true);

        //            ddlUserList.DataSource = users;
        //            ddlUserList.DataBind();

        //            if (addSelect)
        //                ddlUserList.Items.Insert(0, new ListItem("Select", "-1"));
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvDuplicateEntry.IsValid = false;
        //        cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
        //    }
        //}

        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;

            if (!String.IsNullOrEmpty(ddlCustomer.SelectedValue))
            {
                BindGrid(Convert.ToInt32(ddlCustomer.SelectedValue));
            }
        }
        
        #region To Find Branches not Entities and Sub-Entities

        public static List<NameValueHierarchy> GetAll_Branches(int customerID, int selectedEntityID)
        {
            List<NameValueHierarchy> hierarchy = null;

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             where row.IsDeleted == false
                             && row.CustomerID == customerID
                             && row.ID == selectedEntityID
                             select row);

                hierarchy = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();

                foreach (var item in hierarchy)
                {
                    branchList.Add(item.ID);
                    LoadChildBranches(customerID, item, true, entities);
                }
            }

            return hierarchy;
        }

        public static void LoadChildBranches(int customerid, NameValueHierarchy nvp, bool isClient, ComplianceDBEntities entities)
        {
            IQueryable<CustomerBranch> query = (from row in entities.CustomerBranches
                                                where row.IsDeleted == false
                                                && row.CustomerID == customerid
                                                && row.ParentID == nvp.ID
                                                //&& row.Type != 1
                                                select row);

            var subEntities = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();

            foreach (var item in subEntities)
            {
                branchList.Add(item.ID);
                LoadChildBranches(customerid, item, false, entities);
            }
        }

        #endregion

        private void BindGrid(int customerID)
        {
            try
            {
                List<SP_RLCS_GetAssignedCompliance_Result> lstAssignedCompliances = new List<SP_RLCS_GetAssignedCompliance_Result>();

                if (customerID != -1)
                {
                    //customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;

                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        lstAssignedCompliances = (from row in entities.SP_RLCS_GetAssignedCompliance(customerID)
                                                  select row).ToList();

                        if (lstAssignedCompliances.Count > 0)
                        {
                            branchList.Clear();

                            if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue))
                            {
                                GetAll_Branches(customerID, Convert.ToInt32(tvFilterLocation.SelectedValue));
                                branchList.ToList();
                            }

                            if (branchList.Count > 0)
                                lstAssignedCompliances = lstAssignedCompliances.Where(row => branchList.Contains((int)row.CustomerBranchID)).ToList();

                            if (!string.IsNullOrEmpty(ddlUser.SelectedValue))
                            {
                                int userID = Convert.ToInt32(ddlUser.SelectedValue);
                                lstAssignedCompliances = lstAssignedCompliances.Where(row => row.UserID == userID).ToList();
                            }

                            if (!string.IsNullOrEmpty(ddlRole.SelectedValue) && ddlRole.SelectedValue != "-1")
                            {
                                int roleID = Convert.ToInt32(ddlRole.SelectedValue);
                                lstAssignedCompliances = lstAssignedCompliances.Where(row => row.RoleID == roleID).ToList();
                            }
                        }
                    }
                }

                grdAssignedCompliance.DataSource = lstAssignedCompliances;
                grdAssignedCompliance.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void chkSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                CheckBox chkSelectAll = (CheckBox)grdAssignedCompliance.HeaderRow.FindControl("chkSelectAll");
                int countCheckedCheckbox = 0;
                foreach (GridViewRow row in grdAssignedCompliance.Rows)
                {
                    CheckBox chkBox = (CheckBox)row.FindControl("chkBox");
                    if (chkSelectAll.Checked == true)
                    {
                        countCheckedCheckbox++;
                        chkBox.Checked = true;
                    }
                    else
                    {
                        chkBox.Checked = false;
                    }
                }

                if (countCheckedCheckbox > 0)
                    btnSave.Visible = true;
                else
                    btnSave.Visible = false;

                ForceCloseFilterBranchesTreeView();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void chkBox_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                CheckBox chkSelectAll = (CheckBox)grdAssignedCompliance.HeaderRow.FindControl("chkSelectAll");
                int countCheckedCheckbox = 0;
                for (int i = 0; i < grdAssignedCompliance.Rows.Count; i++)
                {
                    GridViewRow row = grdAssignedCompliance.Rows[i];
                    if (((CheckBox)row.FindControl("chkBox")).Checked)
                    {
                        countCheckedCheckbox = countCheckedCheckbox + 1;
                    }
                }

                if (countCheckedCheckbox == grdAssignedCompliance.Rows.Count)
                {
                    chkSelectAll.Checked = true;
                }
                else
                {
                    chkSelectAll.Checked = false;
                }

                if (countCheckedCheckbox > 0)
                    btnSave.Visible = true;
                else
                    btnSave.Visible = false;

                ForceCloseFilterBranchesTreeView();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        private void ForceCloseFilterBranchesTreeView()
        {
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideFilterTreeView", "$(\"#divFilterLocation\").hide(\"blind\", null, 5, function () { });", true);
        }

        protected void grdAssignedCompliance_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                {
                    if (e.Row.RowType == DataControlRowType.DataRow)
                    {
                        DropDownList ddlNewUserGrid = (e.Row.FindControl("ddlNewUser") as DropDownList);

                        if (ddlNewUserGrid != null)
                        {
                            BindUsers(ddlNewUserGrid, true);

                            if (!string.IsNullOrEmpty(ddlNewUser.SelectedValue))
                            {
                                if (ddlNewUserGrid.Items.FindByValue(ddlNewUser.SelectedValue) != null)
                                {
                                    ddlNewUserGrid.ClearSelection();
                                    ddlNewUserGrid.Items.FindByValue(ddlNewUser.SelectedValue).Selected = true;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
            {
                List<Tuple<long, int, int>> lstNewAssignment = new List<Tuple<long, int, int>>();

                foreach (GridViewRow row in grdAssignedCompliance.Rows)
                {
                    CheckBox chkBox = (CheckBox)row.FindControl("chkBox");
                    if (chkBox.Checked)
                    {
                        Label lblInstanceID = (row.FindControl("lblInstanceID") as Label);
                        Label lblRoleID = (row.FindControl("lblRoleID") as Label);
                        DropDownList ddlNewUser = (row.FindControl("ddlNewUser") as DropDownList);

                        if (lblInstanceID != null && lblRoleID != null && ddlNewUser != null)
                        {
                            if (!string.IsNullOrEmpty(lblInstanceID.Text) && !string.IsNullOrEmpty(lblInstanceID.Text)
                                && !string.IsNullOrEmpty(ddlNewUser.SelectedValue))
                            {
                                lstNewAssignment.Add(new Tuple<long, int, int>(Convert.ToInt64(lblInstanceID.Text), Convert.ToInt32(lblRoleID.Text), Convert.ToInt32(ddlNewUser.SelectedValue)));
                            }
                        }
                    }
                }

                if (lstNewAssignment.Count > 0)
                {
                    bool saveSuccess = RLCS_ComplianceManagement.UpdateComplianceAssignment(lstNewAssignment);

                    if (saveSuccess)
                    {
                        BindGrid(Convert.ToInt32(ddlCustomer.SelectedValue));

                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Selected Compliance Re-Assign Successfully";

                        vsHRCompAssign.CssClass = "alert alert-success";
                    }
                }
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Select one or more compliance to Re-Assign";
                }
            }
            else
            {
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Select Customer";
            }
        }

        protected void ddlNewUser_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(ddlNewUser.SelectedValue) && ddlNewUser.SelectedValue != "-1")
                {
                    foreach (GridViewRow row in grdAssignedCompliance.Rows)
                    {
                        DropDownList ddlNewUserGrid = (row.FindControl("ddlNewUser") as DropDownList);

                        if (ddlNewUserGrid != null)
                        {
                            if (ddlNewUserGrid.Items.FindByValue(ddlNewUser.SelectedValue) != null)
                            {
                                ddlNewUserGrid.ClearSelection();
                                ddlNewUserGrid.Items.FindByValue(ddlNewUser.SelectedValue).Selected = true;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void ddlRole_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(ddlCustomer.SelectedValue))
            {
                BindGrid(Convert.ToInt32(ddlCustomer.SelectedValue));
            }
        }
        
        protected void upComplianceReAssign_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }
    }
}