<%@ Page Title="Entity/ Location/ Branch::Setup HR+" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="RLCS_EntityBranchList.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.ClientSetup.RLCS_EntityBranchList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%--<link href="/avantischarts/jquery-ui-v1.12.1/jquery-ui.min.css" rel="stylesheet" />--%>
    <%--<script type="text/javascript" src="../../avantischarts/jquery-ui-v1.12.1/jquery-ui.min.js"></script>--%>
    <%--<link href="../../avantischarts/jquery-ui-v1.12.1/jquery-ui.min.css" rel="stylesheet" />--%>

    <link href="../../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../../Newjs/kendo.all.min.js"></script>

    <link href="/NewCSS/arrow-Progressbar.css" rel="stylesheet" />

    <style type="text/css">
        *.disabled {
            cursor: not-allowed;
        }

        .aspNetDisabled {
            cursor: not-allowed;
        }

        .m-10 {
            margin-left: 10px;
        }
    </style>

     <script type="text/javascript">
         $(document).ready(function () {
             DisableLink();
             HideUploadSelected();
         });

         function onOpen(e) {
             //kendo.ui.progress(e.sender.element, true);
             $("#BodyContent_updateProgress").show();
         }

         function DisableLink() {
             if ($("#BodyContent_dlBreadcrumb span a").length > 1)
                 $("#BodyContent_dlBreadcrumb span a").first().removeClass("aspNetDisabled").removeAttr("disabled");
             else
                 $("#BodyContent_dlBreadcrumb span a").first().addClass("aspNetDisabled").attr("disabled", "disabled");
         }
        function OpenAddBranchPopup(){
            debugger;
            $('#divRLCSCustomerBranchesDialog').show();
            var Cust = '<%=CustID%>';
            //$('#ddlCustomer').val();
            var myWindowAdv = $("#divRLCSCustomerBranchesDialog");

            //function onClose() {

            //}
            myWindowAdv.kendoWindow({
                width: "75%",
                height: '90%',
                content: "/RLCS/RLCS_CustomerBranchDetails_New.aspx?CustomerID=" + Cust+"&Mode=0",
                iframe: true,
                title: "Entity/Client Details",
                visible: false,
                actions: [
                    //"Pin",
                    "Close"
                ],
                open: onOpen,
                close: onClose
                
            });

            myWindowAdv.data("kendoWindow").center().open();
            return false;
        }

         function fEditBranchPopup(obj) {
             debugger;
             var BranchID = $(obj).attr('data-clId');
             $('#divRLCSCustomerBranchesDialog').show();
             var Cust = '';
             var myWindowAdv = $("#divRLCSCustomerBranchesDialog");

             //function onClose() {

             //}

             myWindowAdv.kendoWindow({
                 width: "75%",
                 height: '90%',
                 content: "/RLCS/RLCS_CustomerBranchDetails_New.aspx?CustomerID=<%=CustID%>" + "&Mode=1&BranchID=" + BranchID,
                 iframe: true,
                 title: "Entity/Client Details",
                 visible: false,
                 actions: ["Close"],
                 open: onOpen,
                 close: onClose
             });

             myWindowAdv.data("kendoWindow").center().open();
             return false;
         }
        //ADD NEW FUNCTION
         function OpenWindowSelectedBulkUploadEntity() {
             debugger;
              var ServiceProviderID='<%=ServiceProviderID%>';
             var   cust=<%=CustID%>;
            $('#divRLCSEmployeeUploadDialogBulk').show();
            var myWindowAdv = $("#divRLCSEmployeeUploadDialogBulk");
            myWindowAdv.kendoWindow({
                width: "85%",
                height: "80%",
                title: "Update Selected Column Client/Entity",
                visible: false,
                actions: [
                    //"Pin",
                    "Close"
                ],
                close: onClose
            });
            myWindowAdv.data("kendoWindow").center().open();
           $("#BodyContent_updateProgress").show();
           $('#iframeUploadBulk').attr('src', "/Setup/UploadEntityUpdateSelected?CustID="+ <%=CustID%>+"&UserID="+<%=UserID%>+"&SPID="+ServiceProviderID);
            return false;
         }
         //ADD NEW FUNCTION
         function OpenWindowSelectedBulkUploadLocation() {
             debugger;
              var ServiceProviderID='<%=ServiceProviderID%>';
             var   cust=<%=CustID%>;
            $('#divRLCSEmployeeUploadDialogBulkLoc').show();
            var myWindowAdv = $("#divRLCSEmployeeUploadDialogBulkLoc");
            myWindowAdv.kendoWindow({
                width: "85%",
                height: "80%",
                title: "Update Selected Column Location/Branch",
                visible: false,
                actions: [
                    //"Pin",
                    "Close"
                ],
                close: onClose
            });
            myWindowAdv.data("kendoWindow").center().open();
           $("#BodyContent_updateProgress").show();
           $('#iframeUploadBulkLoc').attr('src', "/Setup/UploadLocationUpdateSelected?CustID="+ <%=CustID%>+"&UserID="+<%=UserID%>+"&SPID="+ServiceProviderID);
            return false;
         }
         //RLCS only
        function HideUploadSelected()
        {
            var ServiceProviderID='<%=ServiceProviderID%>';
            if (ServiceProviderID==94) {
                $("#BodyContent_btnbulkLocationUpload").show();
            }
            else {
                $("#BodyContent_btnbulkLocationUpload").hide();
            }
        }
    </script>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 25%; left: 40%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

    <div class="container">
        <div style="margin-left: 1%;">
            <div class="arrow-steps clearfix">
                <div class="step done">
                    <asp:LinkButton ID="lnkBtnCustomer" PostBackUrl="~/RLCS/ClientSetup/RLCS_CustomerCorporateList.aspx" runat="server" Text="Customer"></asp:LinkButton>
                </div>
                <div class="step current">
                    <asp:LinkButton ID="lnkBtnEntityCustomerBranch" runat="server" Text="Entity/ Location/ Branch" PostBackUrl="~/RLCS/ClientSetup/RLCS_EntityBranchList.aspx"></asp:LinkButton>
                    <%--PostBackUrl="~/RLCS/Setup/RLCS_EntityBranchList.aspx"--%>
                </div>
                <div class="step">
                    <asp:LinkButton ID="lnkBtnUser" PostBackUrl="~/RLCS/ClientSetup/RLCS_UserMaster.aspx" runat="server" Text="User"></asp:LinkButton>
                </div>
                <div class="step">
                    <asp:LinkButton ID="lnkBtnEmployee" PostBackUrl="~/RLCS/ClientSetup/RLCS_EmployeeMaster.aspx" runat="server" Text="Employee"></asp:LinkButton>
                </div>
                <div class="step">
                    <asp:LinkButton ID="lnkBtnEntityAssign" PostBackUrl="~/RLCS/ClientSetup/RLCS_EntityAssignment.aspx" runat="server" Text="Entity/Branch Assignment"></asp:LinkButton>
                </div>
                <div class="step">
                    <asp:LinkButton ID="lnkBtnComAssign" PostBackUrl="~/RLCS/ClientSetup/RLCS_HRCompliance_Assign.aspx" runat="server" Text="Compliance Assignment"></asp:LinkButton>
                </div>
                <div class="step">
                    <asp:LinkButton ID="lnkBtnComAct" PostBackUrl="~/RLCS/ClientSetup/RLCS_HRCompliance_Activate.aspx" runat="server" Text="Compliance Activation"></asp:LinkButton>
                </div>
            </div>
        </div>
    </div>

    <asp:UpdatePanel ID="upCustomerBranchList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div style="margin-bottom: 4px">
                <asp:ValidationSummary runat="server" CssClass="vdsummary"
                    ValidationGroup="CustomerBranchValidationGroup" />
                <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                    ValidationGroup="CustomerBranchValidationGroup" Display="None" />
            </div>
            <table width="100%">
                <tr>
                    <td align="left" style="width: 20%"></td>
                    <td align="left" style="width: 30%">Filter :
                        <asp:TextBox runat="server" ID="tbxFilter" Width="250px" MaxLength="50" AutoPostBack="true"
                            OnTextChanged="tbxFilter_TextChanged" />
                    </td>

                    <td align="center" style="width: 50%">
                        <div style="width: 100%">
                            <div style="width: 33%; float: right;">
                                <asp:LinkButton Text="Update Selected Column Location" runat="server" ID="btnbulkLocationUpload" OnClientClick="return OpenWindowSelectedBulkUploadLocation()" CssClass="k-button" />
                            </div>
                            <div style="width: 32%; float: right;">
                                <asp:LinkButton Text="Update Selected Column Client" runat="server" ID="btnbulkClientUpload" OnClientClick="return OpenWindowSelectedBulkUploadEntity()" CssClass="k-button" />
                            </div>

                            <div style="width: 11%; float: right;">
                                <asp:LinkButton Text="Paycode" runat="server" ID="btnPaycodeMapping" OnClick="btnAddPaycodeMapping_Click" CssClass="k-button" />
                            </div>
                            <div style="width: 11%; float: right;">
                                <asp:LinkButton Text="Upload" runat="server" ID="btnUpload" OnClientClick="return OpenUploadWindow()" CssClass="k-button" />
                            </div>
                            <div style="width: 11%; float: right;">
                                <asp:LinkButton Text="Add New" runat="server" ID="btnAddCustomerBranch" OnClientClick="return OpenAddBranchPopup()" CssClass="k-button" />
                            </div>


                        </div>
                    </td>
                    <td align="right" style="width: 10%; display: none">
                        <asp:LinkButton Text="Refresh" runat="server" ID="btnRefresh" CssClass="button" OnClick="btnRefresh_Click" />                     
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <asp:DataList runat="server" ID="dlBreadcrumb" OnItemCommand="dlBreadcrumb_ItemCommand"
                            RepeatLayout="Flow" RepeatDirection="Horizontal">
                            <ItemTemplate>
                                <asp:LinkButton Text='<%# Eval("Name") %>' CommandArgument='<%# Eval("ID") %>' CommandName="ITEM_CLICKED"
                                    runat="server" Style="text-decoration: none; color: Black" />
                            </ItemTemplate>
                            <ItemStyle Font-Size="12" ForeColor="Black" Font-Underline="true" />
                            <SeparatorStyle Font-Size="12" />
                            <SeparatorTemplate>
                                &gt;
                            </SeparatorTemplate>
                        </asp:DataList>
                    </td>
                </tr>
            </table>
            <div style="width: 100%">
                <asp:GridView runat="server" ID="grdCustomerBranch" AutoGenerateColumns="false" GridLines="Vertical"
                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true" OnRowCreated="grdCustomerBranch_RowCreated"
                    CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="12" Width="100%" OnSorting="grdCustomerBranch_Sorting"
                    Font-Size="12px" OnRowCommand="grdCustomerBranch_RowCommand" OnPageIndexChanging="grdCustomerBranch_PageIndexChanging">
                    <%--DataKeyNames="ID"--%>
                    <Columns>
                        <asp:BoundField DataField="ID" HeaderText="ID" SortExpression="ID" />
                        <asp:TemplateField HeaderText="ClientID" ItemStyle-Width="5%">
                            <ItemTemplate>
                                <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# ShowClientID((int)Eval("ID")) %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
                        <asp:BoundField DataField="TypeName" HeaderText="Type Name" SortExpression="TypeName" />
                        <asp:BoundField DataField="ContactPerson" HeaderText="Contact Person" HeaderStyle-Height="20px" ItemStyle-Height="25px" SortExpression="ContactPerson" />
                        <asp:BoundField DataField="EmailID" HeaderText="Email" SortExpression="EmailID" />
                        <asp:BoundField DataField="Landline" HeaderText="Landline" ItemStyle-HorizontalAlign="Center" SortExpression="Landline" Visible="false" />
                        <asp:BoundField DataField="BranchStatus" HeaderText="Status" ItemStyle-HorizontalAlign="Center" SortExpression="BranchStatus" />
                        <asp:TemplateField HeaderText="Created On" ItemStyle-HorizontalAlign="Center" SortExpression="CreatedOn">
                            <ItemTemplate>
                                <%# ((DateTime)Eval("CreatedOn")).ToString("dd-MMM-yy HH:mm")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="View">
                            <ItemTemplate>
                                <asp:LinkButton runat="server" CommandName="VIEW_CHILDREN" CommandArgument='<%# Eval("ID") %>' ToolTip="View Entity/Branch">Entity/Branch</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Width="80px" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton1" runat="server" data-clId='<%# Eval("ID")%>' OnClientClick="fEditBranchPopup(this)"
                                    Visible='<%# Eval("Type")!=DBNull.Value && Eval("ComplianceProductType")!= DBNull.Value ? ShowHideButtons("C", (int)Eval("ID")):false %>'>
                                     <img src="../../Images/edit_icon.png" alt="Edit" title="Edit Entity Details" /></asp:LinkButton>
                                <asp:LinkButton ID="LnkClientLoc" runat="server" CommandName="RLCS_CUSTOMER_BRANCH" data-clId='<%# Eval("ID")%>' CommandArgument='<%# Eval("ID") %>' OnClientClick="return EditClientLocationSetup(this)"
                                    Visible='<%# Eval("Type")!=DBNull.Value && Eval("ComplianceProductType")!= DBNull.Value ? ShowHideButtons("B", (int)Eval("ID")):false %>'>
                                    <img src="../../Images/edit_icon.png" alt="Edit" title="Location/Branch Details" />
                                </asp:LinkButton>
                                <asp:LinkButton ID="LinkButton2" runat="server" CommandName="DELETE_CUSTOMER_BRANCH" CommandArgument='<%# Eval("ID") %>'
                                    OnClientClick="return confirm('This will also delete all the sub-entities associated with current entity. Are you certain you want to delete this entity?');">
                                    <img src="../../Images/delete_icon.png" alt="Disable" title="Disable Entity/Branch" />
                                </asp:LinkButton>
                                <asp:LinkButton ID="LnkAddBranch" runat="server" CommandName="RLCS_CUSTOMER_BRANCH" data-clId='<%# Eval("ID")%>' CommandArgument='<%# Eval("ID") %>' OnClientClick="return ClientLocationSetup(this)" Visible='<%# Eval("Type")!= null ? Convert.ToInt32(Eval("Type"))==1?true:false:true %>'>                               
                                <img src="../../Images/add_icon.png" alt="Add" title="Add New Location/Branch" /></asp:LinkButton><%-- Visible='<%# Eval("Type")!= null ? Convert.ToInt32(Eval("Type"))==1?true:false : true %>'--%>

                                <%-- <asp:LinkButton ID="LinkButton3" runat="server" CommandName="RLCS_CUSTOMER_BRANCH" data-clId='<%# Eval("ID")%>' CommandArgument='<%# Eval("ID") %>' OnClientClick="return ClientSetup(this)"
                                    Visible='<%# Eval("Type")!=DBNull.Value && Eval("ComplianceProductType")!= DBNull.Value ? ShowHideButtons("C", (byte)Eval("Type"), (int)Eval("ComplianceProductType")):false %>'>                               
                                <img src="../../Images/edit_icon.png" alt="Edit" title="Entity/Client Details" /></asp:LinkButton><%-- Visible='<%# Eval("Type")!= null ? Convert.ToInt32(Eval("Type"))==1?true:false : true %>'--%>
                            </ItemTemplate>
                            <HeaderTemplate>
                            </HeaderTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle BackColor="#CCCC99" />
                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                    <PagerSettings Position="Top" />
                    <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                    <AlternatingRowStyle BackColor="#E6EFF7" />
                    <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                    <EmptyDataTemplate>
                        No Records Found.
                    </EmptyDataTemplate>
                </asp:GridView>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

       <div id="divRLCSEmployeeUploadDialogBulkLoc">
        <iframe id="iframeUploadBulkLoc" style="width: 100%; height: 100%; border: none">
            <div class="chart-loading"></div>
        </iframe>
    </div>
    <div id="divRLCSEmployeeUploadDialogBulk">
        <iframe id="iframeUploadBulk" style="width: 100%; height: 100%; border: none">
            <div class="chart-loading"></div>
        </iframe>
    </div>
    <div id="divRLCSCustomerBranchesDialog">
        <iframe id="iframeRLCSEntityClient" style="width: 70%; height: 80%; border: none">
            <div class="chart-loading"></div>
        </iframe>
    </div>

    <div id="divRLCSCustomerLocationDialog">
        <iframe id="iframeRLCSLocationBranch" style="width: 100%; height: 100%; border: none">
            <div class="chart-loading"></div>
        </iframe>
    </div>

    <div id="divUploadLocationDetails">
        <div class="chart-loading" id="loader"></div>
        <iframe id="iframeLocationUpload" style="width: 580px; height: 300px; border: none"></iframe>
    </div>

    <script type="text/javascript">
        $(function () {
            $('#divRLCSCustomerBranchesDialog').hide();
            $('#divRLCSCustomerLocationDialog').hide();
            $('#divRLCSEmployeeUploadDialogBulk').hide();
            $('#divRLCSEmployeeUploadDialogBulkLoc').hide();
            //$('#divCustomerBranchesDialog').dialog({
            //    height: 650,
            //    width: 720,
            //    autoOpen: false,
            //    draggable: true,
            //    title: "Entity(Client)/Branch Details",
            //    open: function (type, data) {
            //        $(this).parent().appendTo("form");
            //    }
            //});
        });

        function onClose() {
                   debugger;
            document.getElementById('<%= btnRefresh.ClientID %>').click();
            //$(this.element).empty();
            $("#BodyContent_updateProgress").hide();//rohan added
        }

        function OpenUploadWindow() {
            var ServiceProviderID = '<%=ServiceProviderID%>';
            $('#divUploadLocationDetails').show();
            kendo.ui.progress($(".chart-loading"), true);
            $('#iframeLocationUpload').attr('src', "/Setup/UploadLocationFiles?SPID="+ServiceProviderID);
            var myWindowAdv = $("#divUploadLocationDetails");

            //function onClose() {

            //}

            myWindowAdv.kendoWindow({
                width: "50%",
                height: "60%",
                title: "Upload Entity/Branch",
                visible: false,
                actions: [
                    //"Pin",
                    "Close"
                ],
                close: onClose
            });

            myWindowAdv.data("kendoWindow").center().open();
            $('#loader').hide();

            return false;
        }

        function OpenRLCSEntityClientPopup(ID) {
            $('#divRLCSCustomerBranchesDialog').show();

            var myWindowAdv = $("#divRLCSCustomerBranchesDialog");

            //function onClose() {

            //}

            myWindowAdv.kendoWindow({
                width: "80%",
                height: '70%',
                title: "Entity/Client Details",
                visible: false,
                actions: [
                    //"Pin",
                    "Close"
                ],
                close: onClose
            });

            myWindowAdv.data("kendoWindow").center().open();
            $('#iframeRLCSEntityClient').attr('src', "/Setup/CreateClientBasicSetup?Id=" + ID);

            return false;
        }

        function OpenRLCSLocationBranchPopup(ID) {
            $('#divRLCSCustomerLocationDialog').show();

            var myWindowAdv = $("#divRLCSCustomerLocationDialog");

            //function onClose() {

            //}

            myWindowAdv.kendoWindow({
                width: "87%",
                height: "90%",
                title: "Branch Details-HR+ Compliance",
                visible: false,
                actions: [
                    //"Pin",
                    "Close"
                ],
                open: onOpen,
                close: onClose
            });
            //alert(ID);
            myWindowAdv.data("kendoWindow").center().open();
            $('#iframeRLCSLocationBranch').attr('src', "/Setup/CreateClientLocationSetup?Id=" + ID + "&Edit=NewBranch");
           
            return false;
        }

        function EditRLCSLocationBranchPopup(ID) {
            debugger;
            $('#divRLCSCustomerLocationDialog').show();

            var myWindowAdv = $("#divRLCSCustomerLocationDialog");

           <%-- function onClose() {
                   debugger;
            document.getElementById('<%= btnRefresh.ClientID %>').click();
            //$(this.element).empty();
            }--%>

            myWindowAdv.kendoWindow({
                width: "87%",
                height: "90%",
                title: "Branch Details-HR+ Compliance",
                visible: false,
                actions: [
                    //"Pin",
                    "Close"
                ],
                open: onOpen,
                close: onClose
            });
            //alert(ID);
            myWindowAdv.data("kendoWindow").center().open();
            $('#iframeRLCSLocationBranch').attr('src', "/Setup/CreateClientLocationSetup?Id=" + ID +"&Edit=YES");
           
            return false;
        }

        function ClientSetup(obj) {
            var ID = $(obj).attr('data-clId');
            OpenRLCSEntityClientPopup(ID);
            return false;
        }

        function ClientLocationSetup(obj) {
            var ID = $(obj).attr('data-clId');
            OpenRLCSLocationBranchPopup(ID);
            return false;
        }
        function EditClientLocationSetup(obj) {
            debugger;
            debugger;
            var ID = $(obj).attr('data-clId');
            EditRLCSLocationBranchPopup(ID);
            return false;
        }

       
        function initializeRadioButtonsList(controlID) {
            $(controlID).buttonset();
        }

      

        function checkAll(cb) {
            var ctrls = document.getElementsByTagName('input');
            for (var i = 0; i < ctrls.length; i++) {
                var cbox = ctrls[i];
                if (cbox.type == "checkbox" && cbox.id.indexOf("chkIndustry") > -1) {
                    cbox.checked = cb.checked;
                }
            }
        }

        function UncheckHeader() {
            var rowCheckBox = $("#RepeaterTable input[id*='chkIndustry']");
            var rowCheckBoxSelected = $("#RepeaterTable input[id*='chkIndustry']:checked");
            var rowCheckBoxHeader = $("#RepeaterTable input[id*='IndustrySelectAll']");
            if (rowCheckBox.length == rowCheckBoxSelected.length) {
                rowCheckBoxHeader[0].checked = true;
            } else {

                rowCheckBoxHeader[0].checked = false;
            }
        }
    </script>
</asp:Content>
