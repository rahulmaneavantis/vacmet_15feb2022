﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using Logger;
using System.Reflection;
using System.Configuration;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Text;
using System.Web.Security;
using System.Web;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS.ClientSetup
{
    public partial class RLCS_EntityBranchList : System.Web.UI.Page
    {
        protected static int CustID, UserID,ServiceProviderID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (HttpContext.Current.Request.IsAuthenticated)
            {
                if (!IsPostBack)
                {
                    try
                    {
                        ViewState["CustomerID"] = Session["CustomerID"];
                        ViewState["ParentID"] = null;

                        BindCustomerBranches();


                        if (!(AuthenticationHelper.Role.Equals("SADMN") || AuthenticationHelper.Role.Equals("IMPT") || AuthenticationHelper.Role.Equals("SPADM")))
                        {
                            btnAddCustomerBranch.Visible = false;
                        }


                        if (ViewState["CustomerID"] != null)
                        {
                            int customerID = Convert.ToInt32(ViewState["CustomerID"]);
                            CustID = customerID;
                            ServiceProviderID = Convert.ToInt32(CustomerManagement.GetServiceProviderID(Convert.ToInt32(customerID)));
                            if (customerID != 0)
                            {
                                int complianceProductType = 0;
                                complianceProductType = RLCS_Master_Management.GetComplianceProductType(customerID);
                                ViewState["ComplianceProductType"] = complianceProductType;

                                if (complianceProductType == 1 || complianceProductType == 2)
                                    ViewState["CorpID"] = RLCS_Master_Management.GetCorporateIDByCustID(customerID);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
                    }
                }
            }
            else
            {
                FormsAuthentication.SignOut();
                Session.Abandon();
                FormsAuthentication.RedirectToLoginPage();
            }
            //else
            //{
            //    if (ViewState["CustomerID"] != null)
            //    {
            //        int customerID = Convert.ToInt32(ViewState["CustomerID"]);

            //        if (customerID != 0)
            //        {
            //            int complianceProductType = 0;
            //            complianceProductType = RLCS_Master_Management.GetComplianceProductType(customerID);
            //            ViewState["ComplianceProductType"] = complianceProductType;

            //            if (complianceProductType == 1 || complianceProductType == 2)
            //                ViewState["CorpID"] = RLCS_Master_Management.GetCorporateIDByCustID(customerID);
            //        }
            //    }
            //}

        }

     
        
        //private void BindIndustry()
        //{
        //    try
        //    {
        //        ddlIndustry.DataTextField = "Name";
        //        ddlIndustry.DataValueField = "ID";

        //        ddlIndustry.DataSource = CustomerBranchManagement.GetAllIndustry();
        //        ddlIndustry.DataBind();
        //        ddlIndustry.Items.Insert(0, new ListItem("< Select >", "-1"));
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvDuplicateEntry.IsValid = false;
        //        cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
        //    }
        //}

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            BindCustomerBranches();
            //dlBreadcrumb_ItemCommand(null,null);
        }


        protected void dlBreadcrumb_ItemCommand(object source, DataListCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "ITEM_CLICKED")
                {
                    if (e.Item.ItemIndex == 0)
                    {
                        ViewState["ParentID"] = null;
                        ViewState["EntityClientID"] = null;
                    }
                    else
                    {
                        ViewState["ParentID"] = e.CommandArgument.ToString();
                    }
                    BindCustomerBranches();
                    upCustomerBranchList.Update();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        private void BindCustomerBranches()
        {
            try
            {
                long parentID = -1;
                //ddlLegalRelationShipOrStatus.Items.Clear();
                if (ViewState["ParentID"] != null)
                {
                    long.TryParse(ViewState["ParentID"].ToString(), out parentID);
                    //BindLegalRelationShipOrStatus(Convert.ToInt32(ViewState["IndustryId"].ToString()));
                }
                else
                {
                    ViewState["IndustryId"] = null;
                }

                dlBreadcrumb.DataSource = CustomerBranchManagement.GetHierarchy(Convert.ToInt32(ViewState["CustomerID"]), parentID);
                dlBreadcrumb.DataBind();

               var DataBranch= CustomerBranchManagement.GetAll(Convert.ToInt32(ViewState["CustomerID"]), parentID, tbxFilter.Text);
               foreach (var item in DataBranch)
                {
                    if (item.TypeName == "Branch")
                    {
                        btnPaycodeMapping.Visible = false;
                        btnAddCustomerBranch.Visible = false;
                        btnbulkClientUpload.Visible = false;
                        btnbulkLocationUpload.Visible = true;
                        break;
                    }
                    else
                    {
                        btnPaycodeMapping.Visible = true;
                        btnAddCustomerBranch.Visible = true;
                        btnbulkClientUpload.Visible = true;
                        btnbulkLocationUpload.Visible = false;
                        break;
                    }
                }
                grdCustomerBranch.DataSource = DataBranch;
                grdCustomerBranch.DataBind();

                upCustomerBranchList.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        public static string ShowClientID(int custBranchID)
        {
            try
            {
                string clientID = string.Empty;
                clientID = RLCS_Master_Management.GetClientIDByBranchID(custBranchID);
                return clientID;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return string.Empty;
            }
        }

        protected bool ShowHideButtons(string btnType, byte type, int complianceProdType)
        {
            bool showButton = false;
            if (!string.IsNullOrEmpty(btnType))
            {
                if (Convert.ToInt32(complianceProdType) > 0)
                {
                    if (btnType.Trim().ToUpper().Equals("C"))
                    {
                        if (Convert.ToInt32(type) == 1)
                        {
                            showButton = true;
                        }
                        else
                        {
                            showButton = false;
                        }
                    }
                    else
                    {
                        if (Convert.ToInt32(type) != 1)
                        {
                            showButton = true;
                        }
                    }
                    //else if (btnType.Trim().ToUpper().Equals("B"))
                    //{
                    //    if (Convert.ToInt32(type) != 1)
                    //    {
                    //        showButton = true;
                    //    }
                    //}
                }
            }

            return showButton;
        }

        protected bool ShowHideButtons(string btnType, int custBranchID)
        {
            bool showButton = false;
            if (!string.IsNullOrEmpty(btnType))
            {
                if (custBranchID > 0)
                {
                    string branchType = RLCS_Master_Management.GetBranchType(custBranchID);
                    if (btnType.Trim().ToUpper().Equals("C"))
                    {
                        if (branchType == "E")
                            showButton = true;
                        else
                            showButton = false;
                    }
                    else if (btnType.Trim().ToUpper().Equals("B"))
                    {
                        if (branchType == "B")
                            showButton = true;
                        else
                            showButton = false;
                    }
                }
            }

            return showButton;
        }

        //protected void ddlIndustry_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        BindLegalRelationShipOrStatus(Convert.ToInt32(ddlIndustry.SelectedValue));
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvDuplicateEntry.IsValid = false;
        //        cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
        //    }
        //}

        //private void BindLegalRelationShipOrStatus(List<int> industryList)
        //{
        //    try
        //    {
        //        ddlLegalRelationShipOrStatus.DataSource = null;
        //        ddlLegalRelationShipOrStatus.DataBind();
        //        ddlLegalRelationShipOrStatus.ClearSelection();

        //        ddlLegalRelationShipOrStatus.DataTextField = "Name";
        //        ddlLegalRelationShipOrStatus.DataValueField = "ID";


        //        ddlLegalRelationShipOrStatus.DataSource = CustomerBranchManagement.GetAllLegalStatus(industryId, ddlType.SelectedValue.Length > 0 ? Convert.ToInt32(ddlType.SelectedValue) : -1);
        //        ddlLegalRelationShipOrStatus.DataBind();

        //        ddlLegalRelationShipOrStatus.Items.Insert(0, new ListItem("< Select >", "-1"));
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvDuplicateEntry.IsValid = false;
        //        cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
        //    }
        //}




        protected void grdCustomerBranch_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int customerBranchID = 0;
            try
            {
                if (!String.IsNullOrEmpty(Convert.ToString(e.CommandArgument)))
                {
                     customerBranchID = Convert.ToInt32(e.CommandArgument);
                }
                

                if (e.CommandName.Equals("EDIT_CUSTOMER_BRANCH"))
                {
                    //ViewState["Mode"] = 1;
                    //ViewState["CustomerBranchID"] = customerBranchID;
                    //txtIndustry.Text = "< Select >";
                    //CustomerBranch customerBranch = CustomerBranchManagement.GetByID(customerBranchID);

                    //PopulateInputForm();

                    //if (!string.IsNullOrEmpty(customerBranch.Name))
                    //{
                    //    tbxName.Text = customerBranch.Name;
                    //    tbxName.Enabled = false;
                    //}
                    //if (!string.IsNullOrEmpty(Convert.ToString(customerBranch.Type)))
                    //{
                    //    if (customerBranch.Type != -1)
                    //    {
                    //        ddlType.SelectedValue = customerBranch.Type.ToString();
                    //        ddlType_SelectedIndexChanged(null, null);
                    //    }
                    //}

                    //if (!string.IsNullOrEmpty(Convert.ToString(customerBranch.LegalRelationShipID)))
                    //{
                    //    if (customerBranch.LegalRelationShipID != -1 && customerBranch.LegalRelationShipID != 0)
                    //    {
                    //        ddlLegalRelationShip.SelectedValue = (customerBranch.LegalRelationShipID ?? -1).ToString();
                    //    }
                    //}
                    //if (!string.IsNullOrEmpty(Convert.ToString(customerBranch.LegalEntityTypeID)))
                    //{
                    //    if (customerBranch.LegalEntityTypeID != -1 && customerBranch.LegalEntityTypeID != 0)
                    //    {
                    //        ddlLegalEntityType.SelectedValue = (customerBranch.LegalEntityTypeID ?? -1).ToString();
                    //    }
                    //}
                    //if (!string.IsNullOrEmpty(Convert.ToString(customerBranch.ComType)))
                    //{
                    //    if (customerBranch.ComType != 0 && customerBranch.ComType != -1)
                    //    {
                    //        ddlCompanyType.SelectedValue = (customerBranch.ComType).ToString();
                    //    }
                    //}

                    //var vGetIndustryMappedIDs = Business.ComplianceManagement.GetCustomerBranchIndustryMappedID(customerBranchID);

                    //foreach (RepeaterItem aItem in rptIndustry.Items)
                    //{
                    //    CheckBox chkIndustry = (CheckBox)aItem.FindControl("chkIndustry");
                    //    chkIndustry.Checked = false;
                    //    CheckBox IndustrySelectAll = (CheckBox)rptIndustry.Controls[0].Controls[0].FindControl("IndustrySelectAll");

                    //    for (int i = 0; i <= vGetIndustryMappedIDs.Count - 1; i++)
                    //    {
                    //        if (((Label)aItem.FindControl("lblIndustryID")).Text.Trim() == vGetIndustryMappedIDs[i].ToString())
                    //        {
                    //            chkIndustry.Checked = true;
                    //        }
                    //    }
                    //    if ((rptIndustry.Items.Count) == (vGetIndustryMappedIDs.Count))
                    //    {
                    //        IndustrySelectAll.Checked = true;
                    //    }
                    //    else
                    //    {
                    //        IndustrySelectAll.Checked = false;
                    //    }
                    //}

                    ////if (!string.IsNullOrEmpty(Convert.ToString(customerBranch.Industry)))
                    ////{
                    ////    if (customerBranch.Industry != -1)
                    ////    {
                    ////        //ddlIndustry.SelectedValue = (customerBranch.Industry).ToString();
                    ////        //ddlIndustry_SelectedIndexChanged(null, null);

                    ////        if (customerBranch.Industry == -1 || string.IsNullOrEmpty(customerBranch.Industry.ToString()))
                    ////        {
                    ////            int industryID = -1;
                    ////            if (ViewState["IndustryId"] == null)
                    ////            {
                    ////                if (customerBranch.ParentID != null)//Added by Rahul on 9 FEB 2016
                    ////                {
                    ////                    industryID = Convert.ToInt32(CustomerBranchManagement.GetByID(Convert.ToInt32(customerBranch.ParentID)).Industry);
                    ////                }
                    ////            }
                    ////            else
                    ////            {
                    ////                industryID = Convert.ToInt32(ViewState["IndustryId"]);
                    ////            }
                    ////            BindLegalRelationShipOrStatus(industryID);
                    ////        }
                    ////    }
                    ////}

                    ////if (!string.IsNullOrEmpty(Convert.ToString(customerBranch.LegalRelationShipOrStatus)))
                    ////{
                    ////    if (customerBranch.LegalRelationShipOrStatus != -1)
                    ////    {
                    ////        ddlLegalRelationShipOrStatus.SelectedValue = customerBranch.LegalRelationShipOrStatus != 0 ? customerBranch.LegalRelationShipOrStatus.ToString() : "-1";
                    ////    }
                    ////}                                        
                    //tbxAddressLine1.Text = customerBranch.AddressLine1;
                    //tbxAddressLine2.Text = customerBranch.AddressLine2;

                    //if (!string.IsNullOrEmpty(Convert.ToString(customerBranch.StateID)))
                    //{
                    //    if (customerBranch.StateID != -1)
                    //    {
                    //        ddlState.SelectedValue = customerBranch.StateID.ToString();
                    //        ddlState_SelectedIndexChanged(null, null);
                    //    }
                    //}

                    //if (!string.IsNullOrEmpty(Convert.ToString(customerBranch.CityID)))
                    //{
                    //    if (customerBranch.CityID != -1)
                    //    {
                    //        ddlCity.SelectedValue = customerBranch.CityID.ToString();
                    //        ddlCity_SelectedIndexChanged(null, null);
                    //    }
                    //}

                    //tbxOther.Text = customerBranch.Others;
                    //tbxPinCode.Text = customerBranch.PinCode;
              //  checked from herer
                    ////tbxIndustry.Text = customerBranch.Industry;
                    //tbxContactPerson.Text = customerBranch.ContactPerson;
                    //tbxLandline.Text = customerBranch.Landline;
                    //tbxMobile.Text = customerBranch.Mobile;
                    //tbxEmail.Text = customerBranch.EmailID;
                    //if (!string.IsNullOrEmpty(Convert.ToString(customerBranch.Status)))
                    //{
                    //    if (customerBranch.Status != -1)
                    //    {
                    //        ddlCustomerStatus.SelectedValue = Convert.ToString(customerBranch.Status);
                    //    }
                    //}
                    //if (!string.IsNullOrEmpty(Convert.ToString(customerBranch.AuditPR)))
                    //{
                    //    if (customerBranch.AuditPR == false)
                    //    {
                    //        ddlPersonResponsibleApplicable.SelectedValue = Convert.ToString(0);
                    //    }
                    //    else
                    //    {
                    //        ddlPersonResponsibleApplicable.SelectedValue = Convert.ToString(1);
                    //    }
                    //}
                    ////ddlIndustry.Enabled = false;
                    //ddlType.Enabled = false;
                    ////ddlLegalRelationShipOrStatus.Enabled = false;
                    //ddlLegalRelationShip.Enabled = false;

                    //txtClientID.Text = RLCS_Master_Management.GetClientIDByCustBranchID(customerBranchID);

                    //if (ViewState["Mode"] != null)
                    //{
                    //    if ((int)ViewState["Mode"] == 1)
                    //    {
                    //        txtClientID.Enabled = false;
                    //        btnCheck.Enabled = false;
                    //        btnSave.Enabled = true;
                    //    }
                    //}

                    //upCustomerBranches.Update();
                    //ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divCustomerBranchesDialog\").dialog('open')", true);
                }
                else if (e.CommandName.Equals("DELETE_CUSTOMER_BRANCH"))
                {
                    if (CustomerManagement.CustomerBranchDelete(customerBranchID))
                    {
                        CustomerBranchManagement.Delete(customerBranchID);
                        CustomerBranchManagementRisk.Delete(customerBranchID);
                        CustomerBranchManagement.Delete_RLCSMappingBranch(customerBranchID);
                        BindCustomerBranches();
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "alert('CustomerBranch is associated with assigned compliance, can not be deleted')", true);
                    }
                }

                //else if (e.CommandName.Equals("RLCS_CUSTOMER_BRANCH"))
                //{
                //    ScriptManager.RegisterStartupScript(this, Page.GetType(),"", "populateLabel();" true);


                //}
                else if (e.CommandName.Equals("VIEW_CHILDREN"))
                {
                    try
                    {
                        //if (e.Row.RowType == DataControlRowType.DataRow)
                        //{
                        //    if (Session["dept"].ToString() == "RiskAdmin")
                        //    {
                        //        LinkButton lnkBtn = (LinkButton)e.Row.FindControl("btnedit");
                        //        lnkBtn.Visible = false;
                        //    }
                        //}
                        ViewState["ParentID"] = customerBranchID;

                        if (ViewState["IndustryId"] == null)
                        {
                            ViewState["IndustryId"] = CustomerBranchManagement.GetIndustryIDByCustomerId(customerBranchID);
                        }

                        BindCustomerBranches();

                        if (ViewState["EntityClientID"] == null)
                        {
                            string clientID = RLCS_Master_Management.GetClientIDByBranchID(customerBranchID);
                            if (clientID == "")
                            {
                                ///txtClientID.Text = GetClientID(tbxName.Text);
                                ViewState["EntityClientID"] = clientID;
                            }
                            else
                            {
                                ViewState["EntityClientID"] = clientID;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void grdCustomerBranch_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdCustomerBranch.PageIndex = e.NewPageIndex;
                BindCustomerBranches();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void btnAddCustomerBranch_Click(object sender, EventArgs e)
        {
           
        }

        //private void PopulateInputForm()
        //{
        //    try
        //    {
        //        BindBranchTypes();
        //        BindLegalRelationShips(ViewState["ParentID"] == null);
        //        divParent.Visible = ViewState["ParentID"] != null;

        //        litCustomer.Text = CustomerManagement.GetByID(Convert.ToInt32(ViewState["CustomerID"])).Name;

        //        if (ViewState["ParentID"] != null)
        //        {
        //            litParent.Text = CustomerBranchManagement.GetByID(Convert.ToInt32(ViewState["ParentID"])).Name;
        //        }

        //        if (!String.IsNullOrEmpty(litCustomer.Text))
        //        {
        //            string customer = litCustomer.Text;
        //            txtClientID.Text = GetClientID(litCustomer.Text);
        //            txtClientID.Enabled = true;
        //            lblCheckClient.Visible = false;
        //            btnCheck.Enabled = true;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvDuplicateEntry.IsValid = false;
        //        cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
        //    }
        //}

        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                grdCustomerBranch.PageIndex = 0;
                BindCustomerBranches();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }


   

        //protected void grdCustomerBranch_OnPreRender(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (AuthenticationHelper.Role.Equals("CADMN"))
        //        {
        //            foreach (DataControlField column in grdCustomerBranch.Columns)
        //            {
        //                if (column.HeaderText == "")
        //                    column.Visible = false;
        //            }

        //            grdCustomerBranch.Columns[4].HeaderText = string.Empty;
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvDuplicateEntry.IsValid = false;
        //        cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
        //    }
        //}

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void grdCustomerBranch_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {

                long parentID = -1;
                if (ViewState["ParentID"] != null)
                {
                    long.TryParse(ViewState["ParentID"].ToString(), out parentID);
                }

                var customerBranchList = CustomerBranchManagement.GetAll(Convert.ToInt32(ViewState["CustomerID"]), parentID, tbxFilter.Text);
                if (direction == SortDirection.Ascending)
                {
                    customerBranchList = customerBranchList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                }
                else
                {
                    customerBranchList = customerBranchList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                }


                foreach (DataControlField field in grdCustomerBranch.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdCustomerBranch.Columns.IndexOf(field);
                    }
                }

                grdCustomerBranch.DataSource = customerBranchList;
                grdCustomerBranch.DataBind();


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void grdCustomerBranch_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        /// <summary>
        /// This method for binding customer status.
        /// </summary>
       
     
        protected void grdCustomerBranch_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //if (Convert.ToString(e.Row.Cells[3]) =="Branch" )
            //{
            //        LinkButton lnkbtn = (LinkButton)e.Row.FindControl("LnkClients");
            //        lnkbtn.Visible = false;
               
            //}
        }

        protected void btnAddPaycodeMapping_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/RLCS/ClientSetup/RLCS_PaycodeMappingDetails.aspx?CustomerID="+ Convert.ToInt32(ViewState["CustomerID"]));
        }

       
        
    }
}