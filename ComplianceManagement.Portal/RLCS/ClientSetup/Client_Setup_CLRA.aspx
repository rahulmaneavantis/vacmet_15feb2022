﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Client_Setup_CLRA.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.ClientSetup.Client_Setup_CLRA" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   
    <!-- Bootstrap CSS -->
    <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap theme -->
    <link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <!-- font icon -->
    <link href="~/NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <!--external css-->
    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/contract_custom_style.css" rel="stylesheet" />
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="/Newjs/jquery.js"></script>
    <script type="text/javascript" src="/Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="/Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="/Newjs/bootstrap.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $("body").tooltip({ selector: '[data-toggle=tooltip]' });

            if ($(window.parent.document).find("#divReAssignCompliance").children().first().hasClass("k-loading-mask"))
                $(window.parent.document).find("#divReAssignCompliance").children().first().hide();
        });

        $(document).on("click", "#<%=tbxFilterLocation.ClientID%>", function (event) {
            if (event.target.id == "") {
                var idvid = $(event.target).closest('div');
                if ($(idvid).attr('id') != undefined && $(idvid).attr('id') != '' && $(idvid).attr('id').indexOf('<%=tvFilterLocation.ClientID%>') > -1) {
                    $("#divFilterLocation").show();
                } else {
                    $("#divFilterLocation").hide();
                }
            }
            else if (event.target.id != "<%=tbxFilterLocation.ClientID%>") {
                $("#divFilterLocation").hide();
            } else if (event.target.id != "" && event.target.id.indexOf('<%=tvFilterLocation.ClientID%>') > -1) {
                $("#divFilterLocation").show();
            } else if (event.target.id == "<%=tbxFilterLocation.ClientID%>") {
                $("#<%=tbxFilterLocation.ClientID%>").unbind('click');

                $("#<%=tbxFilterLocation.ClientID%>").click(function () {
                    $("#divFilterLocation").toggle("blind", null, 500, function () { });
                });
            }
        });

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }
    </script>
</head>
<body style="background:white;">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="smComplianceReassign" EnablePartialRendering="true" runat="server"></asp:ScriptManager>

        <div class="mainDiv">
            <asp:UpdatePanel ID="upComplianceReAssign" runat="server" UpdateMode="Conditional" OnLoad="upComplianceReAssign_Load">
                <ContentTemplate>

                    <div class="row col-xs-12 col-sm-12 col-md-12 colpadding0">
                        <asp:ValidationSummary ID="vsHRCompAssign" runat="server" class="alert alert-block alert-danger"
                            ValidationGroup="ComplianceInstanceValidationGroup" />
                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                            ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                    </div>

                    <div class="clearfix"></div>

                    <div class=" col-xs-12 col-sm-12 col-md-12 colpadding0">
                        <div class="col-xs-1 col-sm-1 col-md-1 colpadding0" style="width: 20%">
                            <label for="ddlCustomer" class="control-label">Customer</label>
                            <asp:DropDownListChosen runat="server" ID="ddlCustomer" AllowSingleDeselect="false" DataPlaceHolder="Select"
                                 OnSelectedIndexChanged="ddlCustomer_SelectedIndexChanged" AutoPostBack="true" class="form-control" Width="95%">
                            </asp:DropDownListChosen>
                        </div>

                        <div class="col-xs-1 col-sm-1 col-md-1 colpadding0" style="width: 20%">
                            <label for="tbxFilterLocation" class="control-label">Entity/Branch</label>
                            <asp:TextBox runat="server" ID="tbxFilterLocation" autocomplete="off" CssClass="form-control" Width="95%" />
                            <div style="margin-left: 0px; position: absolute; z-index: 10; width: 95%" id="divFilterLocation">
                                <asp:TreeView runat="server" ID="tvFilterLocation" SelectedNodeStyle-Font-Bold="true" NodeStyle-ForeColor="#8e8e93"
                                    Style="overflow: auto; border-left: 1px solid #c7c7cc; border-right: 1px solid #c7c7cc; border-bottom: 1px solid #c7c7cc; margin-top: -20px; background-color: #ffffff; color: #8e8e93 !important;" ShowLines="true" OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged">
                                </asp:TreeView>
                            </div>
                        </div>

                        <div class="col-xs-1 col-sm-1 col-md-1 colpadding0" style="width: 20%">
                            <label for="ddlUser" class="control-label">User</label>
                            <asp:DropDownListChosen runat="server" ID="ddlUser" AllowSingleDeselect="false" DataPlaceHolder="Select"
                                OnSelectedIndexChanged="ddlUser_SelectedIndexChanged" AutoPostBack="true" class="form-control" Width="95%">
                            </asp:DropDownListChosen>
                        </div>
                        
                        <div class="col-xs-1 col-sm-1 col-md-1 colpadding0" style="width: 20%">
                            <label for="ddlRole" class="control-label">Role</label>
                            <asp:DropDownListChosen runat="server" ID="ddlRole" AllowSingleDeselect="false" DataPlaceHolder="Select"
                                class="form-control" Width="95%" OnSelectedIndexChanged="ddlRole_SelectedIndexChanged" AutoPostBack="true">
                                <asp:ListItem Text="All" Value="-1" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="Performer" Value="3"></asp:ListItem>
                                <asp:ListItem Text="Reviewer" Value="4"></asp:ListItem>
                            </asp:DropDownListChosen>
                        </div>

                        <div class="col-xs-1 col-sm-1 col-md-1 colpadding0" style="width: 20%">
                            <label for="ddlRole" class="control-label">New User</label>
                            <asp:DropDownListChosen runat="server" ID="ddlNewUser" AllowSingleDeselect="false" DataPlaceHolder="Select"
                                class="form-control" Width="95%" OnSelectedIndexChanged="ddlNewUser_SelectedIndexChanged" AutoPostBack="true">
                            </asp:DropDownListChosen>
                        </div>
                    </div>

                    <div class="clearfix"></div>

                    <div class="col-xs-12 col-sm-12 col-md-12" style="min-height: 200px; max-height: 450px; overflow-y: auto;">                        
                        <asp:GridView runat="server" ID="grdAssignedCompliance" AutoGenerateColumns="false" GridLines="None"
                            AllowSorting="true" CssClass="table" CellPadding="4" AllowPaging="True" PageSize="50" Width="100%"
                            Font-Size="12px" ShowHeaderWhenEmpty="true" OnRowDataBound="grdAssignedCompliance_RowDataBound">
                            <Columns>
                                <asp:TemplateField HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center">
                                    <HeaderTemplate>
                                        <asp:CheckBox ID="chkSelectAll" Text="" runat="server" AutoPostBack="true"
                                            OnCheckedChanged="chkSelectAll_CheckedChanged" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkBox" runat="server" AutoPostBack="true" OnCheckedChanged="chkBox_CheckedChanged" />
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Sr." HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <%#Container.DataItemIndex+1 %>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCompID" runat="server" Text='<%# Eval("ComplianceID") %>'></asp:Label>
                                        <asp:Label ID="lblInstanceID" runat="server" Text='<%# Eval("ComplianceInstanceID") %>'></asp:Label>
                                        <asp:Label ID="lblUserID" runat="server" Text='<%# Eval("UserID") %>'></asp:Label>
                                        <asp:Label ID="lblRoleID" runat="server" Text='<%# Eval("RoleID") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:BoundField DataField="BranchName" HeaderText="Entity/Branch" />

                                <asp:TemplateField HeaderText="Compliance">
                                    <ItemTemplate>
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                            <asp:Label ID="lblShortDesc" runat="server" Text='<%# Eval("ShortForm") %>' ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:BoundField DataField="RoleName" HeaderText="Role" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center" />

                                <asp:TemplateField HeaderText="New User">
                                    <ItemTemplate>
                                        <asp:DropDownListChosen ID="ddlNewUser" runat="server" Width="100%"></asp:DropDownListChosen>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <PagerTemplate>
                                <table style="display: none">
                                    <tr>
                                        <td>
                                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                        </td>
                                    </tr>
                                </table>
                            </PagerTemplate>
                            <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                            <EmptyDataTemplate>
                                No record(s) found or all records are filtered out
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12" style="margin-top: 10px;">     
                        <asp:Button ID="btnSave" runat="server" CssClass="btn btn-primary" OnClick="btnSave_Click" Text="Save" />
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>

    </form>
</body>
</html>
