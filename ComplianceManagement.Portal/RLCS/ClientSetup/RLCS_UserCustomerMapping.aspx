﻿<%@ Page Title="User-Customer Mapping :: Setup HR+" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="RLCS_UserCustomerMapping.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.ClientSetup.RLCS_UserCustomerMapping" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../../Newjs/kendo.all.min.js"></script>

    <link href="/NewCSS/arrow-Progressbar.css" rel="stylesheet" />

    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }

        .custom-combobox-input {
            width: 225px;
        }
    </style>
    <script type="text/javascript">
        function OpenPopupUCM(recordID) {
            var myWindowAdv = $("#divUserCustomerMapping");

            function onClose() {
                $(this.element).empty();
            }

            function onOpen(e) {
                kendo.ui.progress(e.sender.element, true);
            }

            myWindowAdv.kendoWindow({
                width: "35%",
                height: "60%",
                title: "Assign Customer to User",
                content: "/RLCS/UserCustomerMappingPage.aspx?ID=" + recordID,
                iframe: "true",
                visible: false,
                actions: [
                    "Pin",
                    "Close"
                ],
                close: onClose,
                open: onOpen,
            });

            myWindowAdv.data("kendoWindow").center().open();
            //$('#idloader9').show();
            // $('#showOverdueDetails').attr('src', "../RLCS/RLCS_OverdueCompliances.aspx");
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
     <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 25%; left: 40%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

    <div class="container">
        <div style="margin-left: 1%;">
            <div class="arrow-steps clearfix">
                <div class="step done">
                    <asp:LinkButton ID="lnkBtnCustomer" PostBackUrl="~/RLCS/ClientSetup/RLCS_CustomerCorporateList.aspx" runat="server" Text="Customer"></asp:LinkButton>
                </div>
                <div class="step done">
                    <asp:LinkButton ID="lnkBtnEntityCustomerBranch" runat="server" Text="Entity/ Location/ Branch"></asp:LinkButton>
                    <%--PostBackUrl="~/RLCS/Setup/RLCS_EntityBranchList.aspx"--%>
                </div>
                <div class="step current">
                    <asp:LinkButton ID="lnkBtnUser" PostBackUrl="~/RLCS/ClientSetup/RLCS_UserMaster.aspx" runat="server" Text="User"></asp:LinkButton>
                </div>
                <div class="step">
                    <asp:LinkButton ID="lnkBtnEmployee" PostBackUrl="~/RLCS/ClientSetup/RLCS_EmpMasterCADMN.aspx" runat="server" Text="Employee"></asp:LinkButton>
                </div>
                <div class="step">
                    <asp:LinkButton ID="lnkBtnEntityAssign" PostBackUrl="~/RLCS/ClientSetup/RLCS_EntityAssignment.aspx" runat="server" Text="Entity/Branch Assignment"></asp:LinkButton>
                </div>
                <div class="step">
                    <asp:LinkButton ID="lnkBtnComAssign" PostBackUrl="~/RLCS/ClientSetup/RLCS_HRCompliance_Assign.aspx" runat="server" Text="Compliance Assignment"></asp:LinkButton>
                </div>
                <div class="step">
                    <asp:LinkButton ID="lnkBtnComAct" PostBackUrl="~/RLCS/ClientSetup/RLCS_HRCompliance_Activate.aspx" runat="server" Text="Compliance Activation"></asp:LinkButton>
                </div>
            </div>
        </div>
    </div>

    <asp:UpdatePanel ID="upUserCustomerList" runat="server" UpdateMode="Conditional"
        OnLoad="upComplianceTypeList_Load">
        <ContentTemplate>
            <div style="width: 100%">
                <div runat="server" id="divCustomer" style="width: 30%; float: left; margin-top: 5px;">
                    <div style="width: 30%; float: left;">
                        <label style="width: 100%; display: block; text-align: right; font-size: 13px; color: #333; margin-top: 4px; font-weight: bold;">
                            Customer:</label>
                    </div>
                    <div style="width: 70%; float: right;">
                        <asp:DropDownList runat="server" ID="ddlCustomer" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                            CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlCustomer_SelectedIndexChanged" />
                        <asp:CompareValidator ID="CompareValidator4" ErrorMessage="Please select customer."
                            ControlToValidate="ddlCustomer" runat="server" ValueToCompare="-1" Operator="NotEqual"
                            ValidationGroup="UserValidationGroup" Display="None" />
                    </div>
                </div>

                <div runat="server" id="divFilterUsers" style="width: 30%; float: left; margin-top: 5px;">
                    <div style="width: 30%; float: left;">
                        <label style="width: 100%; display: block; text-align: right; font-size: 13px; color: #333; margin-top: 4px; font-weight: bold;">
                            User:
                        </label>
                    </div>

                    <div style="width: 70%; float: right;">
                        <asp:DropDownList runat="server" ID="ddlFilterUsers" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;"
                            CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlFilterUsers_SelectedIndexChanged">
                        </asp:DropDownList>
                    </div>
                </div>

                <div runat="server" id="div2" style="width: 30%; float: left; margin-top: 5px;">
                    <div style="width: 30%; float: left;">
                        <label style="width: 100%; display: block; text-align: right; font-size: 13px; color: #333; margin-top: 4px; font-weight: bold;">
                            Manager:
                        </label>
                    </div>

                    <div style="width: 70%; float: right;">
                        <asp:DropDownList runat="server" ID="ddlMgrPage" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;"
                            CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlMgrPage_SelectedIndexChanged">
                        </asp:DropDownList>
                    </div>
                </div>               

                <div style="width: 10%; float: right; margin-top: 10px;">
                    <asp:LinkButton Text="Add New" runat="server" ID="btnAddNew"  CssClass="button" OnClientClick="return OpenPopupUCM(0);" />
                     <%--OnClick="btnAddNew_Click"--%>                                
                </div>
            </div>

            <div style="width: 100%">
                <asp:GridView runat="server" ID="grdUserCustomerMapping" AutoGenerateColumns="false" GridLines="Vertical"
                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true" OnRowCreated="grdUserCustomerMapping_RowCreated"
                    CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="12" Width="100%" OnSorting="grdUserCustomerMapping_Sorting"
                    Font-Size="12px" OnPageIndexChanging="grdUserCustomerMapping_PageIndexChanging" ShowHeaderWhenEmpty="true" OnRowCommand="grdUserCustomerMapping_RowCommand">
                    <Columns>
                        <asp:TemplateField HeaderText="SR.No" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <%#Container.DataItemIndex+1 %>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:BoundField DataField="CustomerName" HeaderText="Customer" HeaderStyle-HorizontalAlign="Center" ItemStyle-Height="20px" HeaderStyle-Height="20px" SortExpression="CustomerName" />
                        <asp:BoundField DataField="UserName" HeaderText="User" HeaderStyle-HorizontalAlign="Center" ItemStyle-Height="20px" HeaderStyle-Height="20px" SortExpression="UserName" />
                        <asp:BoundField DataField="ManagerName" HeaderText="Manager" HeaderStyle-HorizontalAlign="Center" ItemStyle-Height="20px" HeaderStyle-Height="20px" SortExpression="ManagerName" />

                        <asp:TemplateField ItemStyle-Width="110px" ItemStyle-HorizontalAlign="Center" HeaderText="Action">
                            <ItemTemplate>
                                <asp:LinkButton runat="server" CommandName="EDIT_USER" CommandArgument='<%# Eval("ID") %>' ID="lbtnEdit">
                                                <img src="../../Images/edit_icon.png" alt="Edit" title="Edit Mapping" /></asp:LinkButton>
                                <asp:LinkButton runat="server" CommandName="DELETE_USER" CommandArgument='<%# Eval("ID") %>' ID="lbtnDelete"
                                    OnClientClick="return confirm('Are you certain you want to delete this Mapping?');"><img src="../../Images/delete_icon.png" alt="Delete" title="Delete Mapping" /></asp:LinkButton>
                            </ItemTemplate>                           
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle BackColor="#CCCC99" />
                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                    <PagerSettings Position="Top" />
                    <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                    <AlternatingRowStyle BackColor="#E6EFF7" />
                    <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                    <EmptyDataTemplate>
                        No User Customer Mapping record(s) found or all records are filtered out
                    </EmptyDataTemplate>
                </asp:GridView>
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>

    <div id="divAssignUserCustomerDialog">
        <asp:UpdatePanel ID="upMapUser" runat="server" UpdateMode="Conditional" OnLoad="upMapUser_Load">
            <ContentTemplate>
                <div style="margin: 5px">
                    <div style="margin-bottom: 4px">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="vdsummary" ValidationGroup="ComplianceInstanceValidationGroup" />
                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                            ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                        <asp:Label runat="server" ID="lblErrorMassage" Style="color: Red"></asp:Label>
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            User</label>
                        <asp:DropDownList ID="ddlUsers" runat="server" Style="padding: 0px; margin: 0px; height: 22px; width: 300px;"
                            CssClass="txtbox">
                        </asp:DropDownList>
                        <asp:CompareValidator ID="CompareValidator1" ErrorMessage="Please select User." ControlToValidate="ddlUsers"
                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceInstanceValidationGroup"
                            Display="None" />
                    </div>
                   <%--<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>--%>
                            <div style="margin-bottom: 7px">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                     Customer</label>
                                
                                <asp:ListBox runat="server" SelectionMode="Multiple" ID="lblCustomerPopUP" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;" CssClass="txtbox" AutoPostBack="false">
                                </asp:ListBox>
                                <%--<asp:ListBox runat="server" ID="ddlCustomersPopUp" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;"
                                    CssClass="txtbox" AutoPostBack="false">
                                </asp:ListBox>--%>
                                <asp:CompareValidator ID="CompareValidator2" ErrorMessage="Please select Customer." ControlToValidate="lblCustomerPopUP"
                                    runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceInstanceValidationGroup"
                                    Display="None" />
                            </div>
                       <%-- </ContentTemplate>
                    </asp:UpdatePanel>--%>
                    
                    <div style="margin-bottom: 7px; text-align: center; margin-top: 10px; clear: both">
                        <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="button"
                            ValidationGroup="ComplianceInstanceValidationGroup" />
                        <asp:Button Text="Close" runat="server" ID="btnClose" CssClass="button" OnClientClick="CloseCustomerAssignPopUp();" />
                    </div>
                    <div style="margin-bottom: 7px; margin-left: 10px; margin-top: 64px; width: 260px;">
                        <p style="color: red;"><strong>Note:</strong> (*) fields are compulsory</p>
                    </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

     <div id="divUserCustomerMapping" style="overflow: hidden">
    </div>

    <script type="text/javascript">
        $(function () {
            $('#divAssignUserCustomerDialog').dialog({
                //height: auto,
                width: 500,
                autoOpen: false,
                draggable: true,
                title: "Assign Customer To User",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });
            initializeCombobox();
        });

        function initializeCombobox() {
            $("#<%= ddlCustomer.ClientID %>").combobox();
            $("#<%= ddlFilterUsers.ClientID %>").combobox();
            $("#<%= ddlMgrPage.ClientID %>").combobox();

            $("#<%= ddlUsers.ClientID %>").combobox();
            $("#<%= lblCustomerPopUP.ClientID %>").combobox();            
        }

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }

        function CloseCustomerAssignPopUp()
        {
            $('#divAssignUserCustomerDialog').dialog('close');

            $("#BodyContent_lblCustomerPopUP").selectedIndex = -1;
            $("#BodyContent_lblCustomerPopUP").find("option").attr("selected", false);
            //$("select[id*=BodyContent_lblCustomerPopUP]").attr("disabled", false);

        }
    </script>


</asp:Content>

