﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS.ClientSetup
{
    public partial class RLCS_PaycodeMappingDetails : System.Web.UI.Page
    {
        protected static int CustID;
        public string ClientID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (HttpContext.Current.Request.IsAuthenticated)
            {
                if (!IsPostBack)
                {
                    if (HttpContext.Current.Request.IsAuthenticated)
                    {
                        ViewState["SortOrder"] = "Asc";
                        ViewState["SortExpression"] = "Name";
                        if (!string.IsNullOrEmpty(Request.QueryString["CustomerID"]))
                        {
                            var CustomerID = Convert.ToInt32(Request.QueryString["CustomerID"]);
                            BindClients(CustomerID);
                        }

                        //BindPaycodeDetails();


                        Session["CurrentRole"] = AuthenticationHelper.Role;
                        Session["CurrentUserId"] = AuthenticationHelper.UserID;
                    }
                    else
                    {
                        FormsAuthentication.SignOut();
                        Session.Abandon();
                        FormsAuthentication.RedirectToLoginPage();
                    }
                }
            }
            else
            {
                FormsAuthentication.SignOut();
                Session.Abandon();
                FormsAuthentication.RedirectToLoginPage();
            }
        }

        protected void dlBreadcrumb_ItemCommand(object source, DataListCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "ITEM_CLICKED")
                {
                    if (e.Item.ItemIndex == 0)
                    {
                        ViewState["ParentID"] = null;
                        ViewState["EntityClientID"] = null;
                    }
                    else
                    {
                        ViewState["ParentID"] = e.CommandArgument.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }
        private void BindClients(int CustomerID)
        {
            try
            {
                ddlClientList.DataTextField = "Name";
                ddlClientList.DataValueField = "ID";

                var list = RLCS_ClientsManagement.GetAll_Entities(CustomerID);
                ddlClientList.DataSource = list;
                ddlClientList.DataBind();
                if (list.Count > 0)
                {
                    ddlClientList.SelectedIndex = 1;
                    BindPaycodeDetails();
                }
                /// ddlClientList.Items.Insert(0, new ListItem("Select", "-1"));
                //GG ADD 27JAN2020
                long parentID = -1;
                dlBreadcrumb.DataSource = CustomerBranchManagement.GetHierarchy(CustomerID, parentID);
                dlBreadcrumb.DataBind();
                //END

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        private void BindPaycodeDetails()
        {
            try
            {
                int customerID = -1;

                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }

                if (ddlClientList.SelectedValue != "-1")
                {
                    btnAddEmployee.Visible = true;
                    btnUploadEmployee.Visible = true;
                    var client = ddlClientList.SelectedItem.Text;
                    // var entity = client.Split('/');
                    // ClientID = entity[0].Trim();
                    ClientID = Convert.ToString(ddlClientList.SelectedValue);
                    hdnClientID.Value = Convert.ToString(ddlClientList.SelectedValue);
                    var ListData = RLCS_ClientsManagement.GetAllPaycodeDetailsList(ClientID, tbxFilter.Text);
                    if (ListData.Count > 0)
                    {
                        grdPaycodeMappingDetails.DataSource = ListData;
                        grdPaycodeMappingDetails.DataBind();
                    }
                    else
                    {
                        grdPaycodeMappingDetails.DataSource = null;
                        grdPaycodeMappingDetails.DataBind();
                    }
                }
                else
                {
                    grdPaycodeMappingDetails.DataSource = null;
                    grdPaycodeMappingDetails.DataBind();
                }



                upEmpMasterCADMN.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void ddlClientList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (AuthenticationHelper.Role == "SADMN" || AuthenticationHelper.Role == "IMPT" || AuthenticationHelper.Role == "SPADM")
                {
                    if (ddlClientList.SelectedItem.Text == "< Select Customer >")
                    {
                        btnAddEmployee.Visible = false;
                        btnUploadEmployee.Visible = false;
                    }
                    else
                    {
                        btnAddEmployee.Visible = true;
                        btnUploadEmployee.Visible = true;
                    }
                }

                if (!String.IsNullOrEmpty(ddlClientList.SelectedValue) && ddlClientList.SelectedItem.Text != "< Select Customer >")
                {
                    //  CustID = Convert.ToInt32(ddlClientList.SelectedValue);
                    //  Session["CustID"] = CustID;

                    btnAddEmployee.Visible = true;
                    btnUploadEmployee.Visible = true;
                }
                else
                {
                    btnAddEmployee.Visible = false;
                    btnUploadEmployee.Visible = false;
                }

                BindPaycodeDetails();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdPaycodeMappingDetails_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                int customerID = -1;

                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    //customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }

                if (ddlClientList.SelectedValue != "-1")
                {
                    customerID = Convert.ToInt32(ddlClientList.SelectedValue);
                }

                var employees = RLCS_Master_Management.GetAll_EmployeeMaster(customerID);

                if (direction == SortDirection.Ascending)
                {
                    employees = employees.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                }
                else
                {
                    employees = employees.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                }

                foreach (DataControlField field in grdPaycodeMappingDetails.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdPaycodeMappingDetails.Columns.IndexOf(field);
                    }
                }

                grdPaycodeMappingDetails.DataSource = employees;
                grdPaycodeMappingDetails.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                grdPaycodeMappingDetails.PageIndex = 0;
                BindPaycodeDetails();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }


        protected void grdPaycodeMappingDetails_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdPaycodeMappingDetails.PageIndex = e.NewPageIndex;

                BindPaycodeDetails();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void grdPaycodeMappingDetails_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                bool deleteSuccess = false;
                string headerCode = "";
                if (e.CommandArgument != null)
                {
                    if (e.CommandName.Equals("Delete_Employee"))
                    {
                        GridViewRow gvrow = (GridViewRow)((Control)e.CommandSource).NamingContainer;
                        //Get Grid Data
                        HiddenField CPMD_ClientID = (HiddenField)gvrow.FindControl("CPMD_ClientID");
                        CPMD_ClientID.Value = Convert.ToString(CPMD_ClientID.Value);
                        HiddenField CPMD_Status = (HiddenField)gvrow.FindControl("hdnStatus");
                        HiddenField CPMD_Header = (HiddenField)gvrow.FindControl("Header");
                        CPMD_Header.Value = Convert.ToString(CPMD_Header.Value);
                        long recordID = Convert.ToInt64(e.CommandArgument);
                        HiddenField CPMD_PayCode = (HiddenField)gvrow.FindControl("hdnPayCode");
                        HiddenField CPMD_Standard_Column = (HiddenField)gvrow.FindControl("hdnStandard_Column");
                        HiddenField CPMD_PayGroup = (HiddenField)gvrow.FindControl("hdnPayGroup");
                        
                        if ((CPMD_PayGroup.Value).ToLower()== "standard")
                        {
                            headerCode = CPMD_Standard_Column.Value;
                        }
                        else
                        {
                            headerCode = CPMD_PayCode.Value;
                        }
                        //if (!string.IsNullOrEmpty(recordID) && (!string.IsNullOrEmpty(CPMD_ClientID.Value)))
                        //    deleteSuccess = RLCS_ClientsManagement.DeletePaycodeDetails(recordID, CPMD_ClientID.Value);
                        //else
                        //{
                        //    HiddenField CPMD_Header = (HiddenField)gvrow.FindControl("Header");
                        //    CPMD_Header.Value = Convert.ToString(CPMD_Header.Value);

                        //    deleteSuccess = RLCS_ClientsManagement.DeletePaycodeDetails(CPMD_Header.Value, CPMD_ClientID.Value);
                        //}
                        bool Res = false;
                        if (Convert.ToString(CPMD_Status.Value) == "I")
                        {
                            Res = RLCS_ClientsManagement.ActivePaycodeCheck(CPMD_ClientID.Value, headerCode);
                            if (Res == true)
                            {
                                cvDuplicate.IsValid = false;
                                cvDuplicate.ErrorMessage = "Same Paycode Already Active..Please Check....!";
                            }
                        }
                        
                        if (Res==false)
                            {
                                if (!string.IsNullOrEmpty(Convert.ToString(recordID)) && (!string.IsNullOrEmpty(CPMD_ClientID.Value)))
                                    deleteSuccess = RLCS_ClientsManagement.DeletePaycodeDetails(recordID, CPMD_ClientID.Value);

                                if (deleteSuccess)
                                {
                                    deleteSuccess = RLCS_ClientsManagement.UpdateIsProcessedPaycodeMaster(CPMD_ClientID.Value);
                                    if (deleteSuccess)
                                    {
                                        cvDuplicateEntry.IsValid = false;
                                        cvDuplicateEntry.ErrorMessage = "Paycode Deleted Successfully";

                                        BindPaycodeDetails();
                                    }
                                }
                           
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }
        protected void upEmpMasterCADMN_Load(object sender, EventArgs e)
        {
            try
            {
                // ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeCombobox", "initializeCombobox();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void grdPaycodeMappingDetails_RowEditing(object sender, GridViewEditEventArgs e)
        {

        }
        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                ClientID = Convert.ToString(ddlClientList.SelectedValue);
                hdnClientID.Value = Convert.ToString(ddlClientList.SelectedValue);
                var list = RLCS_ClientsManagement.GetAllPaycodeDetailsList(ClientID, tbxFilter.Text);
                if (list.Count() > 0)
                {
                    grdPaycodeMappingDetails.DataSource = list;
                    grdPaycodeMappingDetails.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

    }
}