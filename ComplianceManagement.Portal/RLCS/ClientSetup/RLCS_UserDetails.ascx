﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RLCS_UserDetails.ascx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.ClientSetup.RLCS_UserDetails" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<script type="text/javascript">
    $(function () {
        $('#divUsersDialog').dialog({
            height: 580,
            width: 700,
            autoOpen: false,
            draggable: true,
            title: "Add/Edit User Details",
            open: function (type, data) {
                $(this).parent().appendTo("form");
            }
        });
    });
    
    function initializeCombobox() {
        $("#<%= ddlRiskRole.ClientID %>").combobox();
        $("#<%= ddlRole.ClientID %>").combobox();
        $("#<%= ddlDepartment.ClientID %>").combobox();
        $("#<%= ddlCustomer.ClientID %>").combobox();
        $("#<%= ddlReportingTo.ClientID %>").combobox();
    }

    function disableCombobox() {
        $(".custom-combobox").attr('disabled', 'disabled');
    }

    function initializeJQueryUI() {

        $("#<%= tbxBranch.ClientID %>").unbind('click');

        $("#<%= tbxBranch.ClientID %>").click(function () {
            $("#divBranches").toggle("blind", null, 500, function () { });
        });
    }    
</script>

<div id="divUsersDialog">
    <asp:UpdatePanel ID="upUsers" runat="server" UpdateMode="Conditional" OnLoad="upUsers_Load">
        <ContentTemplate>
            <div style="margin: 5px">
                <div style="margin-bottom: 4px">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="vdsummary" ValidationGroup="UserValidationGroup" />                    
                    <asp:CustomValidator ID="cvEmailError" runat="server" EnableClientScript="False"
                        ErrorMessage="Email already exists." ValidationGroup="UserValidationGroup" Display="None" />
                </div>

                <div runat="server" id="divCustomer" style="margin-bottom: 7px">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                        Customer</label>
                    <asp:DropDownList runat="server" ID="ddlCustomer" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                        CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlCustomer_SelectedIndexChanged" />
                    <asp:CompareValidator ID="CompareValidator2" ErrorMessage="Please select customer."
                        ControlToValidate="ddlCustomer" runat="server" ValueToCompare="-1" Operator="NotEqual"
                        ValidationGroup="UserValidationGroup" Display="None" />
                </div>

                <div style="margin-bottom: 7px">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                        First Name</label>
                    <asp:TextBox runat="server" ID="tbxFirstName" Style="height: 16px; width: 390px;"
                        MaxLength="100" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="First Name can not be empty."
                        ControlToValidate="tbxFirstName" runat="server" ValidationGroup="UserValidationGroup"
                        Display="None" />
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="None" runat="server"
                        ValidationGroup="UserValidationGroup" ErrorMessage="Please enter a valid first name."
                        ControlToValidate="tbxFirstName" ValidationExpression="^[a-zA-Z_]+[a-zA-Z0-9_ .]*$"></asp:RegularExpressionValidator>
                </div>
                <div style="margin-bottom: 7px">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                        Last Name</label>
                    <asp:TextBox runat="server" ID="tbxLastName" Style="height: 16px; width: 390px;"
                        MaxLength="100" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Last Name can not be empty."
                        ControlToValidate="tbxLastName" runat="server" ValidationGroup="UserValidationGroup"
                        Display="None" />
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" Display="None" runat="server"
                        ValidationGroup="UserValidationGroup" ErrorMessage="Please enter a valid last name."
                        ControlToValidate="tbxLastName" ValidationExpression="^[a-zA-Z_]+[a-zA-Z0-9_ .]*$"></asp:RegularExpressionValidator>
                </div>
                <div style="margin-bottom: 7px">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                        Designation</label>
                    <asp:TextBox runat="server" ID="tbxDesignation" Style="height: 16px; width: 390px;"
                        MaxLength="50" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="Designation can not be empty."
                        ControlToValidate="tbxDesignation" runat="server" ValidationGroup="UserValidationGroup"
                        Display="None" />
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" Display="None" runat="server"
                        ValidationGroup="UserValidationGroup" ErrorMessage="Please enter a valid designation."
                        ControlToValidate="tbxDesignation" ValidationExpression="^[a-zA-Z_]+[a-zA-Z0-9_ .]*$"></asp:RegularExpressionValidator>
                </div>
                <div style="margin-bottom: 7px">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                        Email</label>
                    <asp:TextBox runat="server" ID="tbxEmail" Style="height: 16px; width: 390px;" MaxLength="200" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ErrorMessage="Email can not be empty."
                        ControlToValidate="tbxEmail" runat="server" ValidationGroup="UserValidationGroup"
                        Display="None" />
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" Display="None" runat="server"
                        ValidationGroup="UserValidationGroup" ErrorMessage="Please enter a valid email."
                        ControlToValidate="tbxEmail" ValidationExpression="^([\w-_]+\.)*[\w-_]+@([\w-_]+\.)*[\w-_]+\.[\w-_]+$"></asp:RegularExpressionValidator>
                </div>
                <div style="margin-bottom: 7px">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                        Contact No</label>
                    <asp:TextBox runat="server" ID="tbxContactNo" Style="height: 16px; width: 390px;"
                        MaxLength="32" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ErrorMessage="Contact Number can not be empty."
                        ControlToValidate="tbxContactNo" runat="server" ValidationGroup="UserValidationGroup"
                        Display="None" />
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator5" Display="None" runat="server"
                        ValidationGroup="UserValidationGroup" ErrorMessage="Please enter a valid contact number."
                        ControlToValidate="tbxContactNo" ValidationExpression="^[\+\d]+(?:[\d-.\s()]*)$"></asp:RegularExpressionValidator>
                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers" TargetControlID="tbxContactNo" />
                   <asp:RegularExpressionValidator ID="RegularExpressionValidator6" Display="None" runat="server"
                        ValidationGroup="UserValidationGroup" ErrorMessage="Please enter only 10 digit."
                        ControlToValidate="tbxContactNo" ValidationExpression="[0-9]{10}"></asp:RegularExpressionValidator>
                </div>

                <div style="margin-bottom: 7px; display:none">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                        Department</label>
                    <asp:DropDownList runat="server" ID="ddlDepartment" Style="padding: 0px; margin: 0px; height: 22px; width: 180px;"
                        CssClass="txtbox" />
                    <asp:CheckBox runat="server" ID="chkHead" Visible="false" Style="padding: 0px; margin: 0px; height: 22px; width: 10px;" Text="Is Department Head" />
                </div>

                <%--<div style="margin-bottom: 7px">
                    <asp:CompareValidator ID="CVDept" ErrorMessage="Please select department." ControlToValidate="ddlDepartment"
                        runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="UserValidationGroup"
                        Display="None" />
                </div>--%>

                <div style="margin-bottom: 7px; display:none;">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                        &nbsp;</label>
                    <asp:RadioButtonList runat="server" ID="rblAuditRole" RepeatDirection="Horizontal"
                        RepeatLayout="Flow" OnSelectedIndexChanged="rblAuditRole_SelectedIndexChanged">
                        <asp:ListItem Text="Is Audit Head" Value="IAH" />
                        <asp:ListItem Text="Is Audit Manager" Value="IAM" />
                    </asp:RadioButtonList>
                </div>
                <div runat="server" id="divComplianceRole" style="margin-bottom: 7px">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                         Role</label>
                    <asp:DropDownList runat="server" ID="ddlRole" Style="padding: 0px; margin: 0px; height: 22px; width: 180px;"
                        CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlRole_SelectedIndexChanged" />
                    <asp:CompareValidator ID="CompareValidator1" ErrorMessage="Select Role" ControlToValidate="ddlRole"
                        runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="UserValidationGroup"
                        Display="None" />
                </div>

                <div runat="server" id="divRiskRole" style="margin-bottom: 7px">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                        Risk Role</label>
                    <asp:DropDownList runat="server" ID="ddlRiskRole" Style="padding: 0px; margin: 0px; height: 22px; width: 180px;"
                        CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlRiskRole_SelectedIndexChanged" />
                    <asp:CompareValidator ID="CompareValidator3" ErrorMessage="Please select Role." ControlToValidate="ddlRiskRole"
                        runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="UserValidationGroup"
                        Display="None" />
                </div>

                <div runat="server" id="Auditor1" style="margin-bottom: 7px" visible="false">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                    <label style=" display: block; float: left; font-size: 13px; color: #333;">
                        Auditor Login Start Date: <asp:TextBox runat="server" ID="txtStartDate" Style="height: 16px; width: 180px;"/> </label>
                   
                    <label style="display: block; font-size: 13px; color: #333;">
                       End Date: <asp:TextBox runat="server" ID="txtEndDate" Style="height: 16px; width: 180px;"/></label>  
                      <asp:RequiredFieldValidator ID="reqAudit1" Visible="false" ErrorMessage="Auditor login start date can not be empty."
                        ControlToValidate="txtStartDate" runat="server" ValidationGroup="UserValidationGroup"
                        Display="None" />
                      <asp:RequiredFieldValidator ID="reqAudit2" Visible="false" ErrorMessage="Auditor login end date can not be empty."
                        ControlToValidate="txtEndDate" runat="server" ValidationGroup="UserValidationGroup"
                        Display="None" /> 
                </div>

                 <div runat="server" id="Auditor2" style="margin-bottom: 7px" visible="false">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                    <label style=" display: block; float: left; font-size: 13px; color: #333;">
                        Auditor Period Start Date: <asp:TextBox runat="server" ID="txtperiodStartDate" CssClass="select_Date" Style="height: 16px; width: 180px;"/> </label>
                   
                      <label style="display: block;  font-size: 13px; color: #333;">
                       End Date: <asp:TextBox runat="server"  ID="txtperiodEndDate" Style="height: 16px; width: 180px;"/></label> 
                   <asp:RequiredFieldValidator ID="reqAudit3" Visible="false" ErrorMessage="Auditor Period start date can not be empty."
                        ControlToValidate="txtperiodStartDate" runat="server" ValidationGroup="UserValidationGroup"
                        Display="None" />
                      <asp:RequiredFieldValidator ID="reqAudit4" Visible="false" ErrorMessage="Auditor Period end date can not be empty."
                        ControlToValidate="txtperiodEndDate" runat="server" ValidationGroup="UserValidationGroup"
                        Display="None" /> 
                 </div>
                
                <div runat="server" id="divCustomerBranch" style="margin-bottom: 7px">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                        Location</label>
                    <asp:TextBox runat="server" ID="tbxBranch" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                        CssClass="txtbox" />
                    <div style="margin-left: 150px; position: absolute; z-index: 10" id="divBranches">
                        <asp:TreeView runat="server" ID="tvBranches" BackColor="White" BorderColor="Black"
                            BorderWidth="1" SelectedNodeStyle-Font-Bold="true" Height="200px" Width="390px"
                            Style="overflow: auto" ShowLines="true" OnSelectedNodeChanged="tvBranches_SelectedNodeChanged">
                        </asp:TreeView>
                    </div>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ErrorMessage="Please select Location."
                        ControlToValidate="tbxBranch" runat="server" ValidationGroup="UserValidationGroup" InitialValue="< Select Location >"
                        Display="None" />
                </div>
                <div runat="server" id="divReportingTo" style="margin-bottom: 7px">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                        Reporting to</label>
                    <asp:DropDownList runat="server" ID="ddlReportingTo" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;"
                        CssClass="txtbox" />
                </div>
                <div style="margin-bottom: 7px">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                        Address</label>
                    <asp:TextBox runat="server" ID="tbxAddress" Style="height: 50px; width: 390px;" MaxLength="500"
                        TextMode="MultiLine" />
                </div>

                <%--  <asp:UpdatePanel ID="upImage" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>--%>
                <div style="margin-bottom: 7px;display:none;">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                        Profile Picture</label>
                    <asp:FileUpload ID="UserImageUpload" runat="server"  />
                  
                    <asp:Button ID="btnUpload" runat="server" Text="Upload" OnClick="Upload" Visible="false"/>
                </div>
                <div style="margin-bottom: 7px">
                      <asp:Label ID="lblRErrormsg" class="alert alert-block alert-danger fade in" runat="server"></asp:Label>
                    </div>
                <div style="margin-bottom: 7px">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                        &nbsp;
                    </label>
                    <asp:Image ID="ImageShow" runat="server" Height="100" Width="100" ImageUrl="~/UserPhotos/DefaultImage.png" Visible="false" />
                </div>


                <asp:Repeater runat="server" ID="repParameters">
                    <ItemTemplate>
                        <div style="margin-bottom: 7px">
                            <asp:Label runat="server" ID="lblName" Style="width: 150px; display: block; float: left; font-size: 13px; color: #333;"
                                Text='<%# Eval("Name")  + ":"%>' />
                            <asp:TextBox runat="server" ID="tbxValue" Style="height: 20px; width: 390px;" Text='<%# Eval("Value") %>'
                                MaxLength='<%# Eval ("Length") %>' />
                            <asp:HiddenField runat="server" ID="hdnID" Value='<%# Eval("ValueID") %>' />
                            <asp:HiddenField runat="server" ID="hdnEntityParameterID" Value='<%# Eval("ParameterID") %>' />
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
                <div style="margin-bottom: 7px; float: right; margin-right: 257px; margin-top: 10px;">
                    <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="button"
                       CausesValidation="true" ValidationGroup="UserValidationGroup"/>
                    <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="button" OnClientClick="$('#divUsersDialog').dialog('close');" />
                </div>
            </div>

            <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 10px;">
                <p style="color: red;"><strong>Note:</strong> (*) fields are compulsory</p>
            </div>
        </ContentTemplate>
        <Triggers>
            <%--<asp:PostBackTrigger ControlID="btnSave" />--%>            
            <%--<asp:AsyncPostBackTrigger ControlID="btnUpload" EventName="Click" />--%>
        </Triggers>
    </asp:UpdatePanel>
</div>

<script type="text/javascript">         
    function ValidateFilestatus() {        
        var InvalidvalidFilesTypes = ["exe", "bat", "dll", "docx", "xlsx", "html", "css", "js", "txt", "doc", "gif", "jsp",
            "php5","pht","phtml","shtml","asa","cer","asax","swf","xap","aspx","asp","zip","rar","php","reg","rdp"];
        var isValidFile = true;
        var lblerror = document.getElementById("<%=lblRErrormsg.ClientID%>");
        if (lblerror != null || lblerror != undefined) {
            var fuSampleFile = $("#<%=UserImageUpload.ClientID%>").get(0).files;           
            for (var i = 0; i < fuSampleFile.length; i++) {
                var fileExtension = fuSampleFile[i].name.split('.').pop();
                if (InvalidvalidFilesTypes.indexOf(fileExtension) != -1) {
                    isValidFile = false;
                    break;
                }
            }
            if (!isValidFile) {
                lblerror.style.color = "red";                
                lblerror.innerHTML = "Invalid file extension. format not supported.";
            }
        }
        return isValidFile;
    }
</script>
