﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
	public partial class RLCS_ComplianceOverview : System.Web.UI.Page
	{
		static string sampleFormPath1 = "";
		public static string CompDocReviewPath = "";
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				if (!string.IsNullOrEmpty(Request.QueryString["ComplainceInstatnceID"]))
				{
					int ScheduledOnID = 0;
					if (!string.IsNullOrEmpty(Request.QueryString["ComplianceScheduleID"]))
					{
						string ComplianceSchedules = Request.QueryString["ComplianceScheduleID"];
						string[] ScheduleArray = ComplianceSchedules.ToString().Split(',');
						List<int> LstScheduleIds = new List<int>();
						foreach (var item in ScheduleArray)
							ScheduledOnID = Convert.ToInt32(item);
						//ScheduledOnID = Convert.ToInt32(Request.QueryString["ComplianceScheduleID"]);

					}
					int complianceInstanceID = Convert.ToInt32(Request.QueryString["ComplainceInstatnceID"]);
					if (complianceInstanceID == 0)
					{
						using (ComplianceDBEntities entity = new ComplianceDBEntities())
						{

							var CI = entity.ComplianceScheduleOns.Where(x => x.ID == ScheduledOnID).FirstOrDefault();
								
							complianceInstanceID = Convert.ToInt32(CI.ComplianceInstanceID);
						}

					}
					BindTransactionDetails(ScheduledOnID, complianceInstanceID);


					ComplianceDBEntities entities = new ComplianceDBEntities();
					var ParentEvent = entities.SP_GetAuditLogDate(ScheduledOnID).ToList();
					gvParentGrid.DataSource = ParentEvent;
					gvParentGrid.DataBind();

					licomplianceoverview1.Attributes.Add("class", "active");
					lidocuments1.Attributes.Add("class", "");
					liAudit1.Attributes.Add("class", "");

					complianceoverview.Attributes.Remove("class");
					complianceoverview.Attributes.Add("class", "tab-pane active");
					documents.Attributes.Remove("class");
					documents.Attributes.Add("class", "tab-pane");
					audits.Attributes.Remove("class");
					audits.Attributes.Add("class", "tab-pane");


					divMgmtRemrks.Visible = false;

					if (AuthenticationHelper.Role == "MGMT")
					{
						divMgmtRemrks.Visible = true;
					}
				}
			}
		}

		private void BindTransactionDetails(int ScheduledOnID, int complianceInstanceID)
		{
			try
			{
				lblPenalty.Text = string.Empty;
				lblRisk.Text = string.Empty;

				var complianceStatusTxnRecord = RLCS_ComplianceManagement.GetPeriodBranchLocation(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, AuthenticationHelper.ProfileID, ScheduledOnID, complianceInstanceID);

				if (complianceStatusTxnRecord != null)
				{
					if (Convert.ToDateTime(complianceStatusTxnRecord.ScheduledOn) < DateTime.Now)
					{
						if (complianceStatusTxnRecord.ComplianceStatusName.ToLower() == "open"
							|| complianceStatusTxnRecord.ComplianceStatusName.ToLower() == "complied but pending review"
							|| complianceStatusTxnRecord.ComplianceStatusName.ToLower() == "complied delayed but pending review"
							|| complianceStatusTxnRecord.ComplianceStatusName.ToLower() == "not complied")
						{
							lblStatus.Text = "Overdue";
							divRiskType.Attributes["style"] = "background-color:red;";

						}
						else if (complianceStatusTxnRecord.ComplianceStatusName.ToLower() == "closed-timely"
							|| complianceStatusTxnRecord.ComplianceStatusName.ToLower() == "closed-delayed"
							|| complianceStatusTxnRecord.ComplianceStatusName.ToLower() == "approved"
							|| complianceStatusTxnRecord.ComplianceStatusName.ToLower() == "revise compliance")
						{
							lblStatus.Text = "Completed";
							divRiskType.Attributes["style"] = "background-color:green;";
						}
					}

					if (Convert.ToDateTime(complianceStatusTxnRecord.ScheduledOn) >= DateTime.Now)
					{
						if (complianceStatusTxnRecord.ComplianceStatusName.ToLower() == "open"
							|| complianceStatusTxnRecord.ComplianceStatusName.ToLower() == "complied but pending review"
							|| complianceStatusTxnRecord.ComplianceStatusName.ToLower() == "complied delayed but pending review"
							|| complianceStatusTxnRecord.ComplianceStatusName.ToLower() == "not complied")
						{
							lblStatus.Text = "Upcoming";
							divRiskType.Attributes["style"] = "background-color:#ffbf00;";
						}
						else if (complianceStatusTxnRecord.ComplianceStatusName.ToLower() == "closed-timely"
							|| complianceStatusTxnRecord.ComplianceStatusName.ToLower() == "closed-delayed"
							|| complianceStatusTxnRecord.ComplianceStatusName.ToLower() == "revise compliance")
						{
							lblStatus.Text = "Completed";
							divRiskType.Attributes["style"] = "background-color:green;";
						}
					}

					lblLocation.Text = complianceStatusTxnRecord.CustomerBranchName;
					lblDueDate.Text = Convert.ToDateTime(complianceStatusTxnRecord.ScheduledOn).ToString("dd-MMM-yyyy");
					lblPeriod.Text = complianceStatusTxnRecord.ForMonth;
				}

				#region Prev Code
				//var complianceType = Business.ComplianceManagement.GetComplianceType(complianceInstanceID).ComplianceType;
				//if (complianceType == 1)  //CheckList
				//{
				//    var AllComData = Business.DocumentManagement.GetPeriodBranchLocationChecklist(ScheduledOnID, complianceInstanceID);
				//    var complinaceinstance = Business.ComplianceManagement.GetBranchLocation(complianceInstanceID);

				//    var customerbranchID = complinaceinstance.CustomerBranchID;
				//    if (AllComData.Count > 0)
				//    {
				//        foreach (var item in AllComData)
				//        {
				//            if (Convert.ToDateTime(item.ScheduledOn) < DateTime.Now)
				//            {
				//                if (item.Status.ToLower() == "open" || item.Status.ToLower() == "complied but pending review" || item.Status.ToLower() == "complied delayed but pending review" || item.Status.ToLower() == "not complied")
				//                {
				//                    lblStatus.Text = "Overdue";
				//                    divRiskType.Attributes["style"] = "background-color:red;";

				//                }
				//                else if (item.Status.ToLower() == "closed-timely" || item.Status.ToLower() == "closed-delayed" || item.Status.ToLower() == "approved" || item.Status.ToLower() == "revise compliance")
				//                {
				//                    lblStatus.Text = "Completed";
				//                    divRiskType.Attributes["style"] = "background-color:green;";
				//                }
				//            }

				//            if (Convert.ToDateTime(item.ScheduledOn) >= DateTime.Now)
				//            {
				//                if (item.Status.ToLower() == "open" || item.Status.ToLower() == "complied but pending review" || item.Status.ToLower() == "complied delayed but pending review" || item.Status.ToLower() == "not complied")
				//                {
				//                    lblStatus.Text = "Upcoming";
				//                    divRiskType.Attributes["style"] = "background-color:#ffbf00;";
				//                }
				//                else if (item.Status.ToLower() == "closed-timely" || item.Status.ToLower() == "closed-delayed" || item.Status.ToLower() == "revise compliance")
				//                {
				//                    lblStatus.Text = "Completed";
				//                    divRiskType.Attributes["style"] = "background-color:green;";
				//                }
				//            }
				//            lblLocation.Text = item.Branch;
				//            lblDueDate.Text = Convert.ToDateTime(item.ScheduledOn).ToString("dd-MMM-yyyy");
				//            lblPeriod.Text = item.ForMonth;
				//        }
				//    }
				//}
				//else  //Statutory
				//{
				//    var AllComData = Business.ComplianceManagement.GetPeriodBranchLocation(ScheduledOnID, complianceInstanceID);
				//    var complinaceinstance = Business.ComplianceManagement.GetBranchLocation(complianceInstanceID);
				//    var customerbranchID = complinaceinstance.CustomerBranchID;

				//    if (AllComData.Count > 0)
				//    {
				//        foreach (var item in AllComData)
				//        {
				//            if (Convert.ToDateTime(item.PerformerScheduledOn) < DateTime.Now)
				//            {
				//                if (item.Status.ToLower() == "open" || item.Status.ToLower() == "complied but pending review" || item.Status.ToLower() == "complied delayed but pending review" || item.Status.ToLower() == "not complied")
				//                {
				//                    lblStatus.Text = "Overdue";
				//                    divRiskType.Attributes["style"] = "background-color:red;";

				//                }
				//                else if (item.Status.ToLower() == "closed-timely" || item.Status.ToLower() == "closed-delayed" || item.Status.ToLower() == "approved" || item.Status.ToLower() == "revise compliance")
				//                {
				//                    lblStatus.Text = "Completed";
				//                    divRiskType.Attributes["style"] = "background-color:green;";
				//                }
				//            }

				//            if (Convert.ToDateTime(item.PerformerScheduledOn) >= DateTime.Now)
				//            {
				//                if (item.Status.ToLower() == "open" || item.Status.ToLower() == "complied but pending review" || item.Status.ToLower() == "complied delayed but pending review" || item.Status.ToLower() == "not complied")
				//                {
				//                    lblStatus.Text = "Upcoming";
				//                    divRiskType.Attributes["style"] = "background-color:#ffbf00;";
				//                }
				//                else if (item.Status.ToLower() == "closed-timely" || item.Status.ToLower() == "closed-delayed" || item.Status.ToLower() == "revise compliance")
				//                {
				//                    lblStatus.Text = "Completed";
				//                    divRiskType.Attributes["style"] = "background-color:green;";
				//                }
				//            }

				//            lblLocation.Text = item.Branch;
				//            lblDueDate.Text = Convert.ToDateTime(item.PerformerScheduledOn).ToString("dd-MMM-yyyy");
				//            lblPeriod.Text = item.ForMonth;
				//        }
				//    }
				//}

				#endregion

				//var RecentComplianceTransaction = Business.ComplianceManagement.GetCurrentStatusByComplianceID(ScheduledOnID);                
				if (ScheduledOnID <= 0)
				{
					ScheduledOnID = Convert.ToInt32(complianceStatusTxnRecord.ScheduledOnID);
				}



				ViewState["complianceInstanceID"] = complianceStatusTxnRecord.ScheduledOnID;
				var complianceInfo = Business.ComplianceManagement.GetComplianceByInstanceID(ScheduledOnID);



				//var complianceForm = Business.ComplianceManagement.GetComplianceFormByID(complianceInfo.ID);

				if (complianceInfo != null)
				{
					//lblComplianceID.Text = Convert.ToString(complianceInfo.ID);
					lnkSampleForm.Text = Convert.ToString(complianceInfo.SampleFormLink);
					lblComplianceDiscription.Text = complianceInfo.ShortDescription;
					lblDetailedDiscription.Text = complianceInfo.Description;
					lblFrequency.Text = Enumerations.GetEnumByID<Frequency>(Convert.ToInt32(complianceInfo.Frequency != null ? (int)complianceInfo.Frequency : -1));
					lblRefrenceText.Text = WebUtility.HtmlEncode(complianceInfo.ReferenceMaterialText);
					lblPenalty.Text = complianceInfo.PenaltyDescription;
					string risk = Business.ComplianceManagement.GetRiskType(complianceInfo, complianceStatusTxnRecord.ComplianceInstanceID);
					lblRisk.Text = risk;
					hdnActId.Value = Convert.ToString(complianceInfo.ActID);
					hdnComplianceId.Value = Convert.ToString(complianceInfo.ID);
					var ActInfo = Business.ActManagement.GetByID(complianceInfo.ActID);

					if (risk == "HIGH")
					{
						lblRisk.Attributes["style"] = "color:red;font-weight: bold;";
					}
					else if (risk == "MEDIUM")
					{
						lblRisk.Attributes["style"] = "color:#ffbf00;font-weight: bold;";
					}
					else if (risk == "LOW")
					{
						lblRisk.Attributes["style"] = "color:green;font-weight: bold;";
					}

					//if (ActInfo != null)
					//{
					//    lblActName.Text = ActInfo.Name;
					//}

					lblActName.Text = complianceStatusTxnRecord.ActName;
					lblRule.Text = complianceInfo.Sections;
					lblFormNumber.Text = complianceInfo.RequiredForms;
				}
				if (ScheduledOnID > 0)
				{
					var documentVersionData = DocumentManagement.GetFileData1(ScheduledOnID).Select(x => new
					{
						ID = x.ID,
						FileName = x.FileName,
						ScheduledOnID = x.ScheduledOnID,
						FileID = x.FileID,
						Version = string.IsNullOrEmpty(x.Version) ? "1.0" : x.Version,
						VersionDate = x.VersionDate,
						VersionComment = string.IsNullOrEmpty(x.VersionComment) ? "" : x.VersionComment
					}).GroupBy(entry => entry.Version).Select(entry => entry.FirstOrDefault()).ToList();

					//var documentVersionData = DocumentManagement.GetFileData1(Convert.ToInt32(RecentComplianceTransaction.ComplianceScheduleOnID)).Select(x => new
					//{
					//    ID = x.ID,
					//    ScheduledOnID = x.ScheduledOnID,
					//    FileID = x.FileID,
					//    Version = string.IsNullOrEmpty(x.Version) ? "1.0" : x.Version,
					//    VersionDate = x.VersionDate,
					//    VersionComment = string.IsNullOrEmpty(x.VersionComment) ? "" : x.VersionComment
					//}).GroupBy(entry => entry.Version).Select(entry => entry.FirstOrDefault()).ToList();

					rptComplianceVersion.DataSource = documentVersionData;
					rptComplianceVersion.DataBind();
				}
				rptComplianceDocumnets.DataSource = null;
				rptComplianceDocumnets.DataBind();

				rptWorkingFiles.DataSource = null;
				rptWorkingFiles.DataBind();
			}
			catch (Exception ex)
			{
				LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
			}
		}

		protected void rptComplianceVersion_ItemCommand(object source, RepeaterCommandEventArgs e)
		{
			try
			{
				string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
				List<GetComplianceDocumentsView> ComplianceFileData = new List<GetComplianceDocumentsView>();
				List<GetComplianceDocumentsView> ComplianceDocument = new List<GetComplianceDocumentsView>();
				ComplianceDocument = DocumentManagement.GetFileData1(Convert.ToInt32(commandArgs[0])).ToList();

				if (commandArgs[1].Equals("1.0"))
				{
					ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == commandArgs[1]).ToList();
					if (ComplianceFileData.Count <= 0)
					{
						ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == null).ToList();
					}
				}
				else
				{
					ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == commandArgs[1]).ToList();
				}

				if (e.CommandName.Equals("version"))
				{
					if (e.CommandName.Equals("version"))
					{
						rptComplianceDocumnets.DataSource = ComplianceFileData.Where(entry => entry.FileType == 1).ToList();
						rptComplianceDocumnets.DataBind();

						rptWorkingFiles.DataSource = ComplianceFileData.Where(entry => entry.FileType == 2).ToList();
						rptWorkingFiles.DataBind();

						var documentVersionData = ComplianceDocument.Select(x => new
						{
							ID = x.ID,
							FileName = x.FileName,
							ScheduledOnID = x.ScheduledOnID,
							Version = string.IsNullOrEmpty(x.Version) ? "1.0" : x.Version,
							VersionDate = x.VersionDate,
							VersionComment = string.IsNullOrEmpty(x.VersionComment) ? "" : x.VersionComment
						}).GroupBy(entry => entry.Version).Select(entry => entry.FirstOrDefault()).ToList();

						rptComplianceVersion.DataSource = documentVersionData;
						rptComplianceVersion.DataBind();
						UpDetailView.Update();
					}
				}
				else if (e.CommandName.Equals("Download"))
				{
					using (ZipFile ComplianceZip = new ZipFile())
					{
						var compliancetype = DocumentManagement.GetComplianceTypeID(Convert.ToInt32(commandArgs[0]));
						if (compliancetype == 1)
						{
							var ComplianceData = DocumentManagement.GetForMonthChecklist(Convert.ToInt32(commandArgs[0]));
							ComplianceZip.AddDirectoryByName(ComplianceData.ForMonth + "/" + commandArgs[1]);
							if (ComplianceFileData.Count > 0)
							{
								int i = 0;
								foreach (var file in ComplianceFileData)
								{
									string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
									if (file.FilePath != null && File.Exists(filePath))
									{
										string[] filename = file.FileName.Split('.');
										string str = filename[0] + i + "." + filename[1];
										if (file.EnType == "M")
										{
											ComplianceZip.AddEntry(ComplianceData.ForMonth + "/" + commandArgs[1] + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
										}
										else
										{
											ComplianceZip.AddEntry(ComplianceData.ForMonth + "/" + commandArgs[1] + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
										}
										i++;
									}
								}

								var zipMs = new MemoryStream();
								ComplianceZip.Save(zipMs);
								zipMs.Position = 0;
								byte[] data = zipMs.ToArray();
								Response.Buffer = true;
								Response.ClearContent();
								Response.ClearHeaders();
								Response.Clear();
								Response.ContentType = "application/zip";
								Response.AddHeader("content-disposition", "attachment; filename=ComplianceDocuments.zip");
								Response.BinaryWrite(data);
								Response.Flush();
								HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
								HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
								HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
							}
						}
						else
						{
							var ComplianceData = DocumentManagement.GetForMonth(Convert.ToInt32(commandArgs[0]));
							ComplianceZip.AddDirectoryByName(ComplianceData.ForMonth + "/" + commandArgs[1]);
							if (ComplianceFileData.Count > 0)
							{
								int i = 0;
								foreach (var file in ComplianceFileData)
								{
									string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
									if (file.FilePath != null && File.Exists(filePath))
									{
										string[] filename = file.FileName.Split('.');
										string str = filename[0] + i + "." + filename[1];
										if (file.EnType == "M")
										{
											ComplianceZip.AddEntry(ComplianceData.ForMonth + "/" + commandArgs[1] + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
										}
										else
										{
											ComplianceZip.AddEntry(ComplianceData.ForMonth + "/" + commandArgs[1] + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
										}
										i++;
									}
								}
								var zipMs = new MemoryStream();
								ComplianceZip.Save(zipMs);
								zipMs.Position = 0;
								byte[] data = zipMs.ToArray();
								Response.Buffer = true;
								Response.ClearContent();
								Response.ClearHeaders();
								Response.Clear();
								Response.ContentType = "application/zip";
								Response.AddHeader("content-disposition", "attachment; filename=ComplianceDocuments.zip");
								Response.BinaryWrite(data);
								Response.Flush();
								HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
								HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
								HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
							}
						}
					}
				}
				else if (e.CommandName.Equals("View"))
				{
					string[] commandArg = e.CommandArgument.ToString().Split(',');
					List<GetComplianceDocumentsView> CMPDocuments = DocumentManagement.GetFileData1(Convert.ToInt32(commandArgs[0])).ToList();
					Session["ScheduleOnID"] = commandArg[0];
					if (CMPDocuments != null)
					{
						List<GetComplianceDocumentsView> entitiesData = CMPDocuments.Where(entry => entry.Version != null).ToList();
						if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
						{
							GetComplianceDocumentsView entityData = new GetComplianceDocumentsView();
							entityData.Version = "1.0";
							entityData.ScheduledOnID = Convert.ToInt64(commandArg[0]);
							entitiesData.Add(entityData);
						}

						if (entitiesData.Count > 0)
						{
							foreach (var file in CMPDocuments)
							{
								rptComplianceVersionView.DataSource = entitiesData.OrderBy(entry => entry.Version);
								rptComplianceVersionView.DataBind();
								string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

								if (file.FilePath != null && File.Exists(filePath))
								{
									string Folder = "~/TempFiles";
									string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

									string DateFolder = Folder + "/" + File;

									string extension = System.IO.Path.GetExtension(filePath);
									if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
									{
										lblMessage.Text = "";
										lblMessage.Text = "Zip file can't view please download it";
										ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReviewPopUp();", true);
									}
									else
									{
										Directory.CreateDirectory(Server.MapPath(DateFolder));

										if (!Directory.Exists(DateFolder))
										{
											Directory.CreateDirectory(Server.MapPath(DateFolder));
										}

										int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

										string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

										string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

										string FileName = DateFolder + "/" + User + "" + extension;

										FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
										BinaryWriter bw = new BinaryWriter(fs);
										if (file.EnType == "M")
										{
											bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
										}
										else
										{
											bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
										}
										bw.Close();
										CompDocReviewPath = FileName;

										CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);

										lblMessage.Text = "";
										ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReview('" + CompDocReviewPath + "');", true);
									}
								}
								else
								{
									lblMessage.Text = "There is no file to preview";
									ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReviewPopUp();", true);
								}
								break;
							}
						}
					}
				}
			}
			catch (Exception ex)
			{
				LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
			}
		}

		protected void rptComplianceVersion_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnComplinceVersionDoc");
				var scriptManager = ScriptManager.GetCurrent(this.Page);
				scriptManager.RegisterPostBackControl(lblDownLoadfile);

				LinkButton lblDocumentVersion = (LinkButton)e.Item.FindControl("lblDocumentVersion");
				scriptManager.RegisterAsyncPostBackControl(lblDocumentVersion);
			}
		}
		protected void rptComplianceDocumnets_ItemCommand(object source, RepeaterCommandEventArgs e)
		{
			try
			{

				DownloadFile(Convert.ToInt32(e.CommandArgument));
			}
			catch (Exception ex)
			{
				LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
			}
		}

		protected void rptWorkingFiles_ItemCommand(object source, RepeaterCommandEventArgs e)
		{
			try
			{
				DownloadFile(Convert.ToInt32(e.CommandArgument));
			}
			catch (Exception ex)
			{
				LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
			}
		}

		protected void rptWorkingFiles_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnWorkingFiles");
				var scriptManager = ScriptManager.GetCurrent(this.Page);
				scriptManager.RegisterPostBackControl(lblDownLoadfile);
			}
		}
		protected void rptComplianceDocumnets_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnComplianceDocumnets");
				var scriptManager = ScriptManager.GetCurrent(this.Page);
				scriptManager.RegisterPostBackControl(lblDownLoadfile);
			}
		}



		protected void rptComplianceVersionView_ItemCommand(object source, RepeaterCommandEventArgs e)
		{
			try
			{
				string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

				if (e.CommandName.Equals("View"))
				{

					string[] commandArg = e.CommandArgument.ToString().Split(',');
					List<GetComplianceDocumentsView> CMPDocuments = DocumentManagement.GetFileDataView(Convert.ToInt32(commandArgs[0]), Convert.ToInt32(commandArgs[2])).ToList();
					Session["ScheduleOnID"] = commandArg[0];

					if (CMPDocuments != null)
					{
						List<GetComplianceDocumentsView> entitiesData = CMPDocuments.Where(entry => entry.Version != null).ToList();
						if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
						{
							GetComplianceDocumentsView entityData = new GetComplianceDocumentsView();
							entityData.Version = "1.0";
							entityData.ScheduledOnID = Convert.ToInt64(commandArg[0]);
							entitiesData.Add(entityData);
						}

						if (entitiesData.Count > 0)
						{
							foreach (var file in CMPDocuments)
							{
								string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
								if (file.FilePath != null && File.Exists(filePath))
								{
									string Folder = "~/TempFiles";
									string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

									string DateFolder = Folder + "/" + File;

									string extension = System.IO.Path.GetExtension(filePath);
									if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
									{
										lblMessage.Text = "";
										lblMessage.Text = "Zip file can't view please download it";
										ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReviewPopUp();", true);
									}
									else
									{
										Directory.CreateDirectory(Server.MapPath(DateFolder));

										if (!Directory.Exists(DateFolder))
										{
											Directory.CreateDirectory(Server.MapPath(DateFolder));
										}

										int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
										string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

										string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

										string FileName = DateFolder + "/" + User + "" + extension;

										FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
										BinaryWriter bw = new BinaryWriter(fs);
										if (file.EnType == "M")
										{
											bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
										}
										else
										{
											bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
										}
										bw.Close();
										CompDocReviewPath = FileName;
										CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
										ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReview('" + CompDocReviewPath + "');", true);
										lblMessage.Text = "";
									}
								}
								else
								{
									lblMessage.Text = "There is no file to preview";
									ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReviewPopUp();", true);
								}
								break;
							}
						}
					}
				}
			}
			catch (Exception ex)
			{
				LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
			}
		}

		protected void rptComplianceVersionView_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				var scriptManager = ScriptManager.GetCurrent(this.Page);

				LinkButton lblDocumentVersionView = (LinkButton)e.Item.FindControl("lblDocumentVersionView");
				scriptManager.RegisterAsyncPostBackControl(lblDocumentVersionView);
			}
		}

		public void DownloadFile(int fileId)
		{
			try
			{
				var file = Business.ComplianceManagement.GetFile(fileId);
				if (file.FilePath != null)
				{
					string filePath = string.Empty;
					if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
					{
						string pathvalue = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.Name));
						filePath = pathvalue.Replace("~", ConfigurationManager.AppSettings["DriveUrl"]);
					}
					else
					{
						filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
					}
					if (filePath != null && File.Exists(filePath))
					{
						Response.Buffer = true;
						Response.Clear();
						Response.ClearContent();
						Response.ContentType = "application/octet-stream";
						Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
						if (file.EnType == "M")
						{
							Response.BinaryWrite(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
						}
						else
						{
							Response.BinaryWrite(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
						}
						Response.Flush(); // send it to the client to download
						HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
						HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
						HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

					}
				}
			}
			catch (Exception)
			{
				throw;
			}
		}
		protected void gvParentToComplianceGrid_OnRowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				Label lblUserName = (Label)e.Row.FindControl("lblUserName");
				string UserName = Convert.ToString(lblUserName.Text);

				Label lblStatus = (Label)e.Row.FindControl("lblStatus");
				string Status = Convert.ToString(lblStatus.Text);

				Label lblRemarks = (Label)e.Row.FindControl("lblRemarks");
				string Remarks = Convert.ToString(lblRemarks.Text);

				Label lblDocumentVersionView = (Label)e.Row.FindControl("lblcustomlabel");
				if (Status.ToLower() == "open" && Remarks.ToLower() == "new compliance assigned.")
				{
					lblDocumentVersionView.Text = "Compliance assinged to " + UserName;
				}
				if (Status.ToLower() == "not complied" || Status.ToLower() == "rejected")
				{
					lblDocumentVersionView.Text = "Compliance has been rejected by " + UserName;
				}
				if (Status.ToLower() == "complied but pending review")
				{
					if (Remarks != "")
						lblDocumentVersionView.Text = "Compliance has been submitted by " + UserName + " in time with remark " + Remarks;
					else
						lblDocumentVersionView.Text = "Compliance has been submitted by " + UserName + " in time ";
				}
				if (Status.ToLower() == "complied delayed but pending review")
				{
					if (Remarks != "")
						lblDocumentVersionView.Text = "Compliance has been submitted by " + UserName + " after due date time with remark " + Remarks;
					else
						lblDocumentVersionView.Text = "Compliance has been submitted by " + UserName + " after due date ";
				}
				if (Status.ToLower() == "complied delayed but pending review")
				{
					if (Remarks != "")
						lblDocumentVersionView.Text = "Compliance has been submitted by " + UserName + " after due date time with remark " + Remarks;
					else
						lblDocumentVersionView.Text = "Compliance has been submitted by " + UserName + " after due date ";
				}

				if (Status.ToLower() == "closed-timely")
				{
					if (Remarks != "")
						lblDocumentVersionView.Text = "Compliance has been reviewed and closed by " + UserName + " in time with remark " + Remarks;
					else
						lblDocumentVersionView.Text = "Compliance has been reviewed and closed by " + UserName + " in time ";
				}

				if (Status.ToLower() == "closed-delayed")
				{
					if (Remarks != "")
						lblDocumentVersionView.Text = "Compliance has been reviewed and closed by " + UserName + " after due date with remark" + Remarks;
					else
						lblDocumentVersionView.Text = "Compliance has been  reviewed and closed by " + UserName + " after due date ";
				}

				if (Status.ToLower() == "approved")
				{
					lblDocumentVersionView.Text = "Compliance has been approved and closed by " + UserName;
				}

				if (Status.ToLower() == "revise compliance")
				{
					lblDocumentVersionView.Text = "Compliance has been revised by " + UserName;
				}
				if (Status.ToLower() == "submitted for interim review")
				{
					lblDocumentVersionView.Text = "Interim compliance has been reviewed by " + UserName;
				}

				if (Status.ToLower() == "interim review approved")
				{
					lblDocumentVersionView.Text = "Interim compliance has been submitted for review approved by " + UserName;
				}
				if (Status.ToLower() == "interim rejected")
				{
					lblDocumentVersionView.Text = "Interim compliance has been rejected by " + UserName;
				}

				if (Status.ToLower() == "reminder")
				{
					if (Remarks.ToLower() == "upcoming")
						lblDocumentVersionView.Text = "Upcoming compliance reminder has been sent " + UserName;
					if (Remarks.ToLower() == "overdue")
						lblDocumentVersionView.Text = "Overdue compliance reminder has been sent " + UserName;
					if (Remarks.ToLower() == "escalation notification")
						lblDocumentVersionView.Text = "Escalation notification compliance reminder has been sent " + UserName;
					if (Remarks.ToLower() == "penalty performer")
						lblDocumentVersionView.Text = "Penalty performer reminder has been sent " + UserName;
					if (Remarks.ToLower() == "penalty reviewer")
						lblDocumentVersionView.Text = "Penalty Reviewer reminder has been sent " + UserName;
					if (Remarks.ToLower() == "pending for review")
						lblDocumentVersionView.Text = "Pending for review compliance reminder has been sent " + UserName;
					if (Remarks.ToLower() == "checklist reminder")
						lblDocumentVersionView.Text = "Checklist compliance reminder has been sent " + UserName;
					if (Remarks.ToLower() == "checklist escalation notification")
						lblDocumentVersionView.Text = "Checklist escalation notification reminder has been sent " + UserName;
					if (Remarks.ToLower() == "upcoming perofrmer monthly")
						lblDocumentVersionView.Text = "Upcoming perofrmer monthly reminder has been sent " + UserName;
					if (Remarks.ToLower() == "upcoming reviewer monthly")
						lblDocumentVersionView.Text = "Upcoming reviewer monthly reminder has been sent " + UserName;
					if (Remarks.ToLower() == "upcoming management")
						lblDocumentVersionView.Text = "Upcoming management reminder has been sent " + UserName;
					if (Remarks.ToLower() == "weekly management")
						lblDocumentVersionView.Text = "Weekly management reminder has been sent " + UserName;
					if (Remarks.ToLower() == "upcoming approver")
						lblDocumentVersionView.Text = "Upcoming approver reminder has been sent " + UserName;
					if (Remarks.ToLower() == "weekly  approver")
						lblDocumentVersionView.Text = "Weekly approver reminder has been sent " + UserName;
					if (Remarks.ToLower() == "approver notification")
						lblDocumentVersionView.Text = "Approver notification reminder has been sent " + UserName;
				}
				if (Status.ToLower() == "managementremark")
				{
					lblDocumentVersionView.Text = UserName + " has added remark " + Remarks;
				}
			}
		}
		protected void gvParentGrid_OnRowDataBound(object sender, GridViewRowEventArgs e)
		{
			try
			{
				if (e.Row.RowType == DataControlRowType.DataRow)
				{
					if (!String.IsNullOrEmpty(Request.QueryString["ComplianceScheduleID"]))
					{
						if (gvParentGrid.DataKeys[e.Row.RowIndex]["SendDate"] != null)
						{
							DateTime? Date = Convert.ToDateTime(gvParentGrid.DataKeys[e.Row.RowIndex]["SendDate"]);

							//DateTime? Date = Convert.ToDateTime(e.Row.Cells[1].Text);
							int? ScheduledOnID = Convert.ToInt32(Request.QueryString["ComplianceScheduleID"]);

							//GridView gv = (GridView)e.Row.FindControl("gvChildGrid");
							//string type = Convert.ToString(gvParentGrid.DataKeys[e.Row.RowIndex]["Type"]);

							ComplianceDBEntities entities = new ComplianceDBEntities();
							GridView gv1 = (GridView)e.Row.FindControl("gvParentToComplianceGrid");
							var compliance = entities.SP_GetAuditLogDetail(ScheduledOnID, Date).ToList();
							gv1.DataSource = compliance;
							gv1.DataBind();
						}
					}
				}
			}
			catch (Exception ex)
			{
				LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
			}
		}

		protected void lnkSampleForm_Click(object sender, EventArgs e)
		{
			try
			{
				if (lnkSampleForm.Text != "")
				{
					string url = lnkSampleForm.Text;

					string fullURL = "window.open('" + url + "', '_blank', 'height=500,width=800,status=yes,toolbar=no,menubar=no,location=no,scrollbars=yes,resizable=no,titlebar=no' );";
					lnkSampleForm.Attributes.Add("OnClick", fullURL);
				}
			}
			catch (Exception ex)
			{
				LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
			}
		}

		protected void btnSaveRemarks_Click(object sender, EventArgs e)
		{
			try
			{

				if (txtManagementRemarks.Text != "")
				{
					using (ComplianceDBEntities entities = new ComplianceDBEntities())
					{
						ManagementComment cm = new ManagementComment();
						cm.remarks = txtManagementRemarks.Text;
						cm.createdBy = AuthenticationHelper.UserID;
						cm.createdDate = DateTime.UtcNow;
						cm.ComplainceInstatnceID = Convert.ToInt32(Request.QueryString["ComplainceInstatnceID"]);
						cm.ComplianceScheduleID = Convert.ToInt32(Request.QueryString["ComplianceScheduleID"]);
						entities.ManagementComments.Add(cm);
						entities.SaveChanges();
						int instanceId = Convert.ToInt32(Request.QueryString["ComplainceInstatnceID"]);
						var compliance = (from row in entities.ComplianceAssignments
										  where row.ComplianceInstanceID == instanceId
										  select row).ToList();

						//hdnActId.Value = Convert.ToString(complianceInfo.ActID);
						//hdnComplianceId.Value = Convert.ToString(complianceInfo.ID);
						string buildstringfornotification = "Management sent you remarks on compliance assigned to you for period " + lblPeriod.Text;
						buildstringfornotification += "\n Remarks is:" + txtManagementRemarks.Text;
						Notification ntc = new Notification();
						ntc.Remark = buildstringfornotification;
						ntc.UpdatedBy = AuthenticationHelper.UserID;
						ntc.UpdatedOn = DateTime.UtcNow;
						ntc.CreatedBy = AuthenticationHelper.UserID;
						ntc.CreatedOn = DateTime.UtcNow;
						ntc.Type = "Compliance";
						ntc.ActID = Convert.ToInt32(hdnActId.Value);
						ntc.ComplianceID = Convert.ToInt32(hdnComplianceId.Value);

						entities.Notifications.Add(ntc);
						entities.SaveChanges();


						var perf = (from row in compliance
									where row.RoleID == 3
									select row.UserID).FirstOrDefault();

						var rev = (from row in compliance
								   where row.RoleID == 4
								   select row.UserID).FirstOrDefault();

						try
						{
							if (perf != 0)
							{
								var User = UserManagement.GetByID(Convert.ToInt32(perf));
								var MgmtUser = UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID));
								string ReplyEmailAddressName = CustomerManagement.GetByID(Convert.ToInt32(AuthenticationHelper.CustomerID)).Name;
								var ShortDescription = Business.ComplianceManagement.GetCompliance(Convert.ToInt32(hdnComplianceId.Value)).ShortDescription;
								string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EMailTemplate_ManagmentRemarkNotification
													.Replace("@User", User.FirstName + " " + User.LastName)
													.Replace("@MgmtUser", MgmtUser.FirstName + " " + MgmtUser.LastName)
													.Replace("@ShortDescription", ShortDescription)
													.Replace("@Period", lblPeriod.Text)
													.Replace("@Remarks", txtManagementRemarks.Text)
													.Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
													.Replace("@From", ReplyEmailAddressName);

								new Thread(() => { EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], (new string[] { User.Email }).ToList(), null, null, "AVACOM Notification for Management Remarks/Comment.", message); }).Start();
							}

							if (rev != 0)
							{
								var User = UserManagement.GetByID(Convert.ToInt32(rev));
								var MgmtUser = UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID));
								string ReplyEmailAddressName = CustomerManagement.GetByID(Convert.ToInt32(AuthenticationHelper.CustomerID)).Name;
								var ShortDescription = Business.ComplianceManagement.GetCompliance(Convert.ToInt32(hdnComplianceId.Value)).ShortDescription;
								string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EMailTemplate_ManagmentRemarkNotification
													.Replace("@User", User.FirstName + " " + User.LastName)
													.Replace("@MgmtUser", MgmtUser.FirstName + " " + MgmtUser.LastName)
													.Replace("@ShortDescription", ShortDescription)
													.Replace("@Period", lblPeriod.Text)
													.Replace("@Remarks", txtManagementRemarks.Text)
													.Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
													.Replace("@From", ReplyEmailAddressName);

								new Thread(() => { EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], (new string[] { User.Email }).ToList(), null, null, "AVACOM Notification for Management Remarks/Comment.", message); }).Start();
							}
						}
						catch (Exception)
						{
						}

						UserNotification unt = new UserNotification();
						unt.NotificationID = ntc.ID;
						unt.UserID = Convert.ToInt32(perf);
						unt.IsRead = false;
						entities.UserNotifications.Add(unt);
						entities.SaveChanges();

						unt = new UserNotification();
						unt.NotificationID = ntc.ID;
						unt.UserID = Convert.ToInt32(rev);
						unt.IsRead = false;
						entities.UserNotifications.Add(unt);
						entities.SaveChanges();

						CustomValidator1.IsValid = false;
						CustomValidator1.ErrorMessage = "Management Remarks Saved Successfully.";
					}
				}
			}
			catch (Exception ex)
			{
				LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
			}

		}
	}
}