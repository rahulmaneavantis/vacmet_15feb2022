﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Globalization;
using System.IO;
using System.Dynamic;
using Ionic.Zip;
using com.VirtuosoITech.ComplianceManagement.Portal.Properties;
using System.Configuration;
using System.Net;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
	public partial class RLCS_ComplianceStatusTransaction : System.Web.UI.Page
	{
		static string sampleFormPath1 = "";
		public static string CompDocReviewPath = "";
		public static string TaskDocViewPath = "";
		bool IsPenaltyVisible = true;
		public string changeStatus = "";
		public string flag = "";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (Request.QueryString["flag"] != null)
			{
				if (!string.IsNullOrEmpty(Request.QueryString["flag"].ToString()))
				{
					flag = Request.QueryString["flag"];
				}
			}
			if (!IsPostBack)
			{

				try
				{
					int instanceID = Convert.ToInt32(Request.QueryString["instanceID"]);
					string scheduleOnIDs = Request.QueryString["scheduleOnID"];
					hdnComplianceType.Value = Request.QueryString["Type"];
					hdnComplianceCode.Value = Request.QueryString["Code"];
					tbxRemarks1.Text = tbxDate1.Text = string.Empty;
					changeStatus = Request.QueryString["changestatus"];
					flag = Request.QueryString["flag"];
					BindTransactionDetails(instanceID, scheduleOnIDs);
					//upUsers.Update();
				}
				catch (Exception ex)
				{
					LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
					cvDuplicateEntry.IsValid = false;
					cvDuplicateEntry.ErrorMessage = "Something went wrong. Please try again.";
				}
			}
		}

		protected bool CanChangeStatus(long userID, int roleID, int statusID)
		{
			try
			{
				bool result = false;

				if (userID == AuthenticationHelper.UserID)
				{
					if (roleID == 3)
					{
						result = statusID == 1;
					}
					else if (roleID == 4)
					{
						result = statusID == 2 || statusID == 3;
					}
					else if (roleID == 5)
					{
						result = statusID == 2 || statusID == 3;
					}
					else if (roleID == 6)
					{
						result = statusID == 4 || statusID == 5 || statusID == 6;
					}
				}

				return result;
			}
			catch (Exception ex)
			{
				LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
				cvDuplicateEntry.IsValid = false;
				cvDuplicateEntry.ErrorMessage = "Something went wrong. Please try again.";
			}
			return false;
		}

		private void BindTransactionDetails(int complianceInstanceID, string ScheduledOnIDs)
		{
			try
			{
				string[] ScheduleOnIdArray = ScheduledOnIDs.ToString().Split(',');
				List<int> LstScheduleOnIds = new List<int>();
				foreach (var item in ScheduleOnIdArray)
				{
					LstScheduleOnIds.Add(Convert.ToInt32(item));
				}
				lblPenalty1.Text = string.Empty;
				lblRisk1.Text = string.Empty;
				lbDownloadSample1.Text = string.Empty;

				hdnfieldPanaltyDisplay.Value = "true";
				hdnComplianceInstanceID.Value = complianceInstanceID.ToString();
				hdnComplianceScheduledOnId.Value = ScheduledOnIDs.ToString();
				ViewState["ScheduledOnID"] = ScheduledOnIDs;

				int roleID = RLCS_ComplianceManagement.GetAssignedComplianceRole(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, complianceInstanceID);

				if (roleID != 0)
				{
					rdbtnStatus1.Items.Clear();

					if (roleID == 3)
					{
						//rdbtnStatus1.Items.Add(new ListItem("In Progress", "10"));
						rdbtnStatus1.Items.Add(new ListItem("Complied but pending review", "2"));
						rdbtnStatus1.Items.Add(new ListItem("Complied Delayed but pending review", "3"));

						btnReject1.Visible = false;
					}
					else if (roleID == 4)
					{
						rdbtnStatus1.Items.Add(new ListItem("Closed-Timely", "4"));
						rdbtnStatus1.Items.Add(new ListItem("Closed-Delayed", "5"));

						btnReject1.Visible = true;
					}
				}

				//var AllComData = Business.ComplianceManagement.GetPeriodBranchLocation(ScheduledOnID, complianceInstanceID);
				int ScheduledOnID = LstScheduleOnIds[0];
				var complianceStatusTxnRecord = RLCS_ComplianceManagement.GetPeriodBranchLocation(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, AuthenticationHelper.ProfileID, ScheduledOnID, complianceInstanceID);
				var complianceInfo = Business.ComplianceManagement.GetComplianceByInstanceID(ScheduledOnID);
				var complianceForm = Business.ComplianceManagement.GetComplianceFormByID(complianceInfo.ID);
				var RecentComplianceTransaction = Business.ComplianceManagement.GetCurrentStatusByComplianceID(ScheduledOnID);
				var complinaceinstance = Business.ComplianceManagement.GetBranchLocation(complianceInstanceID);
				var customerbranchID = complinaceinstance.CustomerBranchID;

				if (complianceStatusTxnRecord != null)
				{
					lblLocation.Text = complianceStatusTxnRecord.CustomerBranchName;
					lblDueDate.Text = Convert.ToDateTime(complianceStatusTxnRecord.PerformerScheduledOn).ToString("dd-MMM-yyyy");
					lblPeriod.Text = complianceStatusTxnRecord.ForMonth;

					//foreach (var item in AllComData)
					//{
					//    lblLocation.Text = item.Branch;
					//    lblDueDate.Text = Convert.ToDateTime(item.PerformerScheduledOn).ToString("dd-MMM-yyyy");
					//    lblPeriod.Text = item.ForMonth;
					//}
				}

				if (complianceInfo != null)
				{
					lblComplianceID1.Text = Convert.ToString(complianceInfo.ID);
					lnkSampleForm1.Text = Convert.ToString(complianceInfo.SampleFormLink);
					lblFrequency1.Text = Enumerations.GetEnumByID<Frequency>(Convert.ToInt32(complianceInfo.Frequency != null ? (int)complianceInfo.Frequency : -1));
					// lblRefrenceText1.Text = complianceInfo.ReferenceMaterialText;
					lblRefrenceText1.Text = WebUtility.HtmlEncode(complianceInfo.ReferenceMaterialText);
					lblComplianceDiscription1.Text = complianceInfo.ShortDescription;
					lblDetailedDiscription1.Text = complianceInfo.Description;
					// string Penalty = Business.ComplianceManagement.GetPanalty(complianceInfo);
					lblPenalty1.Text = complianceInfo.PenaltyDescription;

					string risk = Business.ComplianceManagement.GetRiskType(complianceInfo, complianceInstanceID);
					lblRisk1.Text = risk;
					lblRiskType1.Text = Business.ComplianceManagement.GetRisk(complianceInfo, complianceInstanceID);

					if (risk == "HIGH")
					{
						divRiskType1.Attributes["style"] = "background-color:red;";
					}
					else if (risk == "MEDIUM")
					{
						divRiskType1.Attributes["style"] = "background-color:yellow;";
					}
					else if (risk == "LOW")
					{
						divRiskType1.Attributes["style"] = "background-color:green;";
					}

					if (complianceStatusTxnRecord != null)
					{
						lblActName1.Text = complianceStatusTxnRecord.ActName;
					}

					//var ActInfo = Business.ActManagement.GetByID(complianceInfo.ActID);
					//if (ActInfo != null)
					//{
					//    lblActName1.Text = ActInfo.Name;
					//}

					lblRule1.Text = complianceInfo.Sections;
					lblFormNumber1.Text = complianceInfo.RequiredForms;
				}

				if (complianceInfo.UploadDocument == true && complianceForm != null)
				{
					//lbDownloadSample1.Text = "<u style=color:blue>Click here</u> to download sample form.";
					lbDownloadSample1.Text = "Download";
					lbDownloadSample1.CommandArgument = complianceForm.ComplianceID.ToString();

					sampleFormPath1 = complianceForm.FilePath;
					sampleFormPath1 = sampleFormPath1.Substring(2, sampleFormPath1.Length - 2);
					lblpathsample1.Text = sampleFormPath1;
					lnkViewSampleForm1.Visible = true;
					lblSlash1.Visible = true;
					lblNote.Visible = true;

					var complianceform = Business.ComplianceManagement.GetComplianceFileNameMultiple(Convert.ToInt32(lbDownloadSample1.CommandArgument));

					if (complianceform != null)
					{
						rptComplianceSampleView1.DataSource = complianceform;
						rptComplianceSampleView1.DataBind();
					}
				}
				else
				{
					//lblNote.Visible = false;
					lblSlash1.Visible = false;
					lnkViewSampleForm1.Visible = false;
					sampleFormPath1 = "";
				}

				BindComplianceDocuments(LstScheduleOnIds);

				//rptComplianceVersion.DataSource = documentVersionData;
				//rptComplianceVersion.DataBind();

				//rptComplianceDocumnets.DataSource = null;
				//rptComplianceDocumnets.DataBind();

				//rptWorkingFiles.DataSource = null;
				//rptWorkingFiles.DataBind();

				BindTransactions(ScheduledOnID);

				tbxRemarks1.Text = string.Empty;
				tbxDate1.Text = RecentComplianceTransaction.StatusChangedOn != null ? RecentComplianceTransaction.StatusChangedOn.Value.ToString("dd-MM-yyyy") : " ";

				if (RecentComplianceTransaction.ComplianceStatusID != null)
				{
					if (rdbtnStatus1.Items.FindByValue(RecentComplianceTransaction.ComplianceStatusID.ToString()) != null)
					{
						rdbtnStatus1.ClearSelection();
						rdbtnStatus1.Items.FindByValue(RecentComplianceTransaction.ComplianceStatusID.ToString()).Selected = true;
					}
				}

				if (complianceInfo.EventFlag == true)
				{
					if (complianceInfo.UpDocs == true)
					{
						rdbtnStatus1.Attributes.Add("disabled", "disabled");
						tbxRemarks1.Attributes.Add("disabled", "disabled");
						tbxDate1.Attributes.Add("disabled", "disabled");
						btnSave1.Attributes.Add("disabled", "disabled");
						btnReject1.Attributes.Add("disabled", "disabled");
					}
					else
					{
						rdbtnStatus1.Attributes.Remove("disabled");
						tbxRemarks1.Attributes.Remove("disabled");
						tbxDate1.Attributes.Remove("disabled");
						btnSave1.Attributes.Remove("disabled");
						btnReject1.Attributes.Remove("disabled");


						tbxRemarks1.Attributes.Add("enabled", "enabled");
						tbxRemarks1.Attributes.Add("enabled", "enabled");
						tbxDate1.Attributes.Add("enabled", "enabled");
						btnSave1.Attributes.Add("enabled", "enabled");
						btnReject1.Attributes.Add("enabled", "enabled");
					}
				}
				else
				{
					//if (documentVersionData.Count > 0)
					//{
					//    rdbtnStatus1.Attributes.Add("disabled", "disabled");
					//    tbxRemarks1.Attributes.Add("disabled", "disabled");
					//    tbxDate1.Attributes.Add("disabled", "disabled");
					//    btnSave1.Attributes.Add("disabled", "disabled");
					//    btnReject1.Attributes.Add("disabled", "disabled");
					//}
					//else
					//{
					//    rdbtnStatus1.Attributes.Add("enabled", "enabled");
					//    tbxRemarks1.Attributes.Add("enabled", "enabled");                        
					//    tbxDate1.Attributes.Add("enabled", "enabled");
					//    btnSave1.Attributes.Add("enabled", "enabled");
					//}
				}

				upComplianceDetails1.Update();
				// ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "OpenDialog", "$(\"#divReviewerComplianceDetailsDialog\").dialog('open');", true);
			}
			catch (Exception ex)
			{
				LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
				cvDuplicateEntry.IsValid = false;
				cvDuplicateEntry.ErrorMessage = "Something went wrong. Please try again.";
			}
		}

		protected void rptComplianceDocumnets_ItemCommand(object source, RepeaterCommandEventArgs e)
		{
			try
			{

				DownloadFile(Convert.ToInt32(e.CommandArgument));
			}
			catch (Exception ex)
			{
				LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
				cvDuplicateEntry.IsValid = false;
				cvDuplicateEntry.ErrorMessage = "Something went wrong. Please try again.";
			}
		}

		protected bool SaveData(string FileName, byte[] Data)
		{
			BinaryWriter Writer = null;
			//string Name = @"C:\temp\yourfile.name";

			try
			{
				// Create a new stream to write to the file
				Writer = new BinaryWriter(File.OpenWrite(FileName));

				// Writer raw data                
				Writer.Write(Data);
				Writer.Flush();
				Writer.Close();
			}
			catch
			{
				//...
				return false;
			}

			return true;
		}

		protected void rptWorkingFiles_ItemCommand(object source, RepeaterCommandEventArgs e)
		{
			try
			{
				DownloadFile(Convert.ToInt32(e.CommandArgument));
			}
			catch (Exception ex)
			{
				LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
			}
		}

		void AddSortImage(int columnIndex, GridViewRow headerRow)
		{
			//System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
			//sortImage.ImageAlign = ImageAlign.AbsMiddle;

			//if (direction == SortDirection.Ascending)
			//{
			//    sortImage.ImageUrl = "../Images/SortAsc.gif";
			//    sortImage.AlternateText = "Ascending Order";
			//}
			//else
			//{
			//    sortImage.ImageUrl = "../Images/SortDesc.gif";
			//    sortImage.AlternateText = "Descending Order";
			//}
			//headerRow.Cells[columnIndex].Controls.Add(sortImage);
		}

		protected void grdTransactionAuditLog_Sorting(object sender, GridViewSortEventArgs e)
		{
			try
			{
				var complianceTransactionHistory = Business.ComplianceManagement.GetAllTransactionLog(Convert.ToInt32(ViewState["ScheduledOnID"]));

				if (direction == SortDirection.Ascending)
				{
					complianceTransactionHistory = complianceTransactionHistory.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
					direction = SortDirection.Descending;
				}
				else
				{
					complianceTransactionHistory = complianceTransactionHistory.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
					direction = SortDirection.Ascending;
				}

				foreach (DataControlField field in grdTransactionAuditLog.Columns)
				{
					if (field.SortExpression == e.SortExpression)
					{
						ViewState["SortIndexHistory"] = grdTransactionAuditLog.Columns.IndexOf(field);
					}
				}

				grdTransactionAuditLog.DataSource = complianceTransactionHistory;
				grdTransactionAuditLog.DataBind();


			}
			catch (Exception ex)
			{
				LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
				cvDuplicateEntry.IsValid = false;
				cvDuplicateEntry.ErrorMessage = "Something went wrong. Please try again.";
			}
		}

		protected void grdTransactionAuditLog_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			try
			{
				grdTransactionAuditLog.PageIndex = e.NewPageIndex;
				BindTransactions(Convert.ToInt32(ViewState["ScheduledOnID"]));
			}
			catch (Exception ex)
			{
				LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
				cvDuplicateEntry.IsValid = false;
				cvDuplicateEntry.ErrorMessage = "Something went wrong. Please try again.";
			}
		}

		private void BindComplianceDocuments(List<int> scheduledOnID)
		{
			try
			{
				List<GetComplianceDocumentsView> LstDocumentVersionData = new List<GetComplianceDocumentsView>();
				List<GetComplianceDocumentsView> LstDocumentsData = new List<GetComplianceDocumentsView>();
				foreach (var item in scheduledOnID)
				{
					LstDocumentVersionData.AddRange(DocumentManagement.GetFileData1(item));
				}
				string DocType = Convert.ToString(hdnComplianceType.Value);
				string Code = "";
				if (DocType == "Challan")
				{
					Code = Convert.ToString(hdnComplianceCode.Value);

					LstDocumentsData = LstDocumentVersionData;

					LstDocumentsData = LstDocumentsData.Where(x => x.Code == Code).ToList();
					if (LstDocumentsData.Count > 0)
					{
						LstDocumentVersionData = LstDocumentsData;
					}

				}


				var documentVersionData = LstDocumentVersionData.Select(x => new
				{
					ID = x.ID,
					FileID = x.FileID,
					FileName = x.FileName,
					FilePath = x.FilePath,
					FileType = x.FileType,
					IsLink = x.ISLink,
					ScheduledOnID = x.ScheduledOnID,
					Version = string.IsNullOrEmpty(x.Version) ? "1.0" : x.Version,
					VersionDate = x.VersionDate,
					VersionComment = string.IsNullOrEmpty(x.VersionComment) ? "" : x.VersionComment
				}).GroupBy(entry => entry.FileID).Select(entry => entry.FirstOrDefault()).OrderByDescending(entry => entry.VersionDate).ToList().GroupBy(x => x.FileName).Select(entry => entry.FirstOrDefault()).OrderByDescending(entry => entry.VersionDate).ToList();



				if (documentVersionData.Any(row => row.FileType == 1))
				{
					btnSave1.Enabled = true;
					btnSave1.ToolTip = "Upload one or more compliance document before compliance closure";
				}
				else
				{
					btnSave1.Enabled = false;
					btnSave1.ToolTip = "";
				}

				grdComplianceDocument.DataSource = documentVersionData;
				grdComplianceDocument.DataBind();


			}
			catch (Exception ex)
			{
				LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
				cvDuplicateEntry.IsValid = false;
				cvDuplicateEntry.ErrorMessage = "Something went wrong. Please try again.";
			}
		}

		private void BindTransactions(int ScheduledOnID)
		{
			try
			{
				// throw new NotImplementedException();
				grdTransactionAuditLog.DataSource = Business.ComplianceManagement.GetAllTransactionLog(ScheduledOnID);
				grdTransactionAuditLog.DataBind();
			}
			catch (Exception ex)
			{
				LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
				cvDuplicateEntry.IsValid = false;
				cvDuplicateEntry.ErrorMessage = "Something went wrong. Please try again.";
			}
		}

		private void BindEscalationRevStatusList()
		{
			try
			{
				rdbtnStatus1.Items.Clear();
				var statusList = ComplianceStatusManagement.GetEscalationRevStatusList();

				foreach (ComplianceStatu st in statusList)
				{
					rdbtnStatus1.Items.Add(new ListItem(st.Name, st.ID.ToString()));
				}
			}
			catch (Exception ex)
			{
				LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
				cvDuplicateEntry.IsValid = false;
				cvDuplicateEntry.ErrorMessage = "Something went wrong. Please try again.";
			}
		}

		private void BindStatusList(int statusID)
		{
			try
			{
				rdbtnStatus1.Items.Clear();
				var statusList = ComplianceStatusManagement.GetStatusList();

				List<ComplianceStatu> allowedStatusList = null;

				List<ComplianceStatusTransition> ComplianceStatusTransitionList = ComplianceStatusManagement.GetStatusTransitionListByInitialId(statusID);
				List<int> finalStatusIDs = ComplianceStatusTransitionList.Select(entry => entry.FinalStateID).ToList();

				allowedStatusList = statusList.Where(entry => finalStatusIDs.Contains(entry.ID)).OrderBy(entry => entry.Name).ToList();

				foreach (ComplianceStatu st in allowedStatusList)
				{
					if (!(st.ID == 6))
					{
						rdbtnStatus1.Items.Add(new ListItem(st.Name, st.ID.ToString()));
					}
				}

				lblStatus1.Visible = allowedStatusList.Count > 0 ? true : false;
				divDated1.Visible = allowedStatusList.Count > 0 ? true : false;

			}
			catch (Exception ex)
			{
				LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
				cvDuplicateEntry.IsValid = false;
				cvDuplicateEntry.ErrorMessage = "Something went wrong. Please try again.";
			}
		}

		protected void UploadDocument_Click(object sender, EventArgs e)
		{
			try
			{
				grdComplianceDocument.DataSource = null;
				grdComplianceDocument.DataBind();

				string ScheduledOnIds = Convert.ToString(ViewState["ScheduledOnID"]);
				string[] ScheduleArray = ScheduledOnIds.ToString().Split(',');
				List<long> LstSchedules = new List<long>();
				long ScheduledOnID = 0;
				foreach (var item in ScheduleArray)
				{
					LstSchedules.Add(Convert.ToInt64(item));
				}
				ScheduledOnID = LstSchedules.FirstOrDefault();
				//long ScheduledOnID = Convert.ToInt64(ViewState["ScheduledOnID"]);
				long complianceInstanceID = Convert.ToInt64(ViewState["complianceInstanceID"]);

				HttpFileCollection fileCollection = Request.Files;

				if (fileCollection.Count > 0)
				{
					#region file upload
					for (int i = 0; i < fileCollection.Count; i++)
					{
						HttpPostedFile uploadfile = null;
						uploadfile = fileCollection[i];
						string fileName = Path.GetFileName(uploadfile.FileName);
						string directoryPath = null;

						if (!string.IsNullOrEmpty(fileName))
						{
							string[] keys = fileCollection.Keys[i].Split('$');
							if (keys[keys.Count() - 1].Equals("fuSampleFile"))
							{
								directoryPath = Server.MapPath("~/TempDocuments/Statutory/");
							}
							else
							{
								directoryPath = Server.MapPath("~/TempDocuments/Working/");
							}

							DocumentManagement.CreateDirectory(directoryPath);
							string finalPath = Path.Combine(directoryPath, fileName);
							finalPath = finalPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");

							fileCollection[i].SaveAs(Server.MapPath(finalPath));

							Stream fs = uploadfile.InputStream;
							BinaryReader br = new BinaryReader(fs);
							Byte[] bytes = br.ReadBytes((Int32)fs.Length);

							TempComplianceDocument tempComplianceDocument = new TempComplianceDocument()
							{
								ScheduleOnID = ScheduledOnID,
								ComplianceInstanceID = complianceInstanceID,
								DocPath = finalPath,
								DocData = bytes,
								DocName = fileCollection[i].FileName,
								FileSize = uploadfile.ContentLength,
							};

							if (keys[keys.Count() - 1].Equals("fuSampleFile"))
							{
								tempComplianceDocument.DocType = "S";
							}
							else
							{
								tempComplianceDocument.DocType = "W";
							}

							long _objTempDocumentID = DocumentManagement.CreateTempComplinaceDocument(tempComplianceDocument);

							if (_objTempDocumentID > 0)
							{
								BindTempDocumentData(ScheduledOnID);
							}

						}
					}
					#endregion
				}

				BindTempDocumentData(ScheduledOnID);

				//if (lblDueDate.Text != "")
				//{
				//    if (ddlStatus.SelectedValue == "3")
				//    {
				//        DateTime Date = DateTime.ParseExact(hiddenDueDate.Value, "dd-MM-yyyy", CultureInfo.InvariantCulture);
				//        Date = Date.AddDays(1);
				//        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerOverDue", string.Format("initializeDatePickerOverDue(new Date({0}, {1}, {2}));", Date.Year, Date.Month - 1, Date.Day), true);
				//    }
				//    if (ddlStatus.SelectedValue == "2")
				//    {
				//        DateTime Date = DateTime.ParseExact(hiddenDueDate.Value, "dd-MM-yyyy", CultureInfo.InvariantCulture);
				//        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerInTime", string.Format("initializeDatePickerInTime(new Date({0}, {1}, {2}));", Date.Year, Date.Month - 1, Date.Day), true);
				//    }
				//}
				//ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "DisplayProgress();", true);

				//ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "openModal();", true);
			}
			catch (Exception ex)
			{
				LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
				cvDuplicateEntry.IsValid = false;
				cvDuplicateEntry.ErrorMessage = "Something went wrong. Please try again.";
			}
		}

		public void BindTempDocumentData(long ScheduledOnID)
		{
			try
			{
				grdComplianceDocument.DataSource = null;
				grdComplianceDocument.DataBind();

				var DocData = DocumentManagement.GetTempComplianceDocumentData(ScheduledOnID);

				grdComplianceDocument.DataSource = DocData;
				grdComplianceDocument.DataBind();

				if (DocData.Count > 0)
				{
					grdComplianceDocument.Visible = true;
				}
				else
				{
					grdComplianceDocument.Visible = false;
				}
			}
			catch (Exception ex)
			{
				LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
				cvDuplicateEntry.IsValid = false;
				cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
			}
		}

		protected void btnSave1_Click(object sender, EventArgs e)
		{
			try
			{
				bool saveSuccess = false;

				if (!string.IsNullOrEmpty(hdnComplianceScheduledOnId.Value))
				{
					string ScheduledOnID = Convert.ToString(hdnComplianceScheduledOnId.Value);
					string[] ScheduleOnIdArray = ScheduledOnID.ToString().Split(',');
					List<int> LstScheduleOnIds = new List<int>();
					foreach (var item in ScheduleOnIdArray)
					{
						LstScheduleOnIds.Add(Convert.ToInt32(item));
					}
					long ComplianceInstanceId = 0;
					foreach (var item in LstScheduleOnIds)
					{
						using (ComplianceDBEntities entities = new ComplianceDBEntities())
						{
							ComplianceInstanceId = entities.ComplianceScheduleOns.Where(x => x.ID == item).Select(I => I.ComplianceInstanceID).FirstOrDefault().Value;
						}

						int StatusID = 0;
						if (lblStatus1.Visible)
							StatusID = Convert.ToInt32(rdbtnStatus1.SelectedValue);
						else
							StatusID = Convert.ToInt32(ComplianceManagement.Business.ComplianceManagement.GetClosedTransaction(item).ComplianceStatusID);

						ComplianceTransaction transaction = new ComplianceTransaction()
						{
							ComplianceScheduleOnID = Convert.ToInt64(item),
							ComplianceInstanceId = ComplianceInstanceId,
							CreatedBy = AuthenticationHelper.UserID,
							CreatedByText = AuthenticationHelper.User,
							StatusId = StatusID,
							StatusChangedOn = DateTime.ParseExact(tbxDate1.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture),
							Remarks = tbxRemarks1.Text,
							PenaltySubmit = string.Empty,
						};

						saveSuccess = Business.ComplianceManagement.CreateTransaction(transaction, null, null, null);

						//Call Challan Code Auto Status Updates For Schedules 
						//using (ComplianceDBEntities entities = new ComplianceDBEntities())
						//{
						//    entities.sp_AutoCloseChallanSchedules(Convert.ToInt32(hdnComplianceInstanceID.Value), Convert.ToInt32(hdnComplianceScheduledOnId.Value));
						//}
					}
					if (saveSuccess)
					{
						ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "CloseAndBindData();", true);
						cvDuplicateEntry.IsValid = false;
						cvDuplicateEntry.ErrorMessage = "Submitted Successfully";
						vsSummary.CssClass = "alert alert-success";
					}
				}
			}
			catch (Exception ex)
			{
				LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
				cvDuplicateEntry.IsValid = false;
				cvDuplicateEntry.ErrorMessage = "Something went wrong. Please try again.";
			}
		}

		protected void btnReject1_Click(object sender, EventArgs e)
		{
			try
			{
				if (!string.IsNullOrEmpty(tbxRemarks1.Text))
				{
					string ScheduledOnID = Convert.ToString(hdnComplianceScheduledOnId.Value);
					string[] ScheduleOnIdArray = ScheduledOnID.ToString().Split(',');
					List<long> LstScheduleOnIds = new List<long>();
					foreach (var item in ScheduleOnIdArray)
					{
						LstScheduleOnIds.Add(Convert.ToInt64(item));
					}
					long ComplianceInstanceId = 0;

					foreach (var item in LstScheduleOnIds)
					{
						using (ComplianceDBEntities entities = new ComplianceDBEntities())
						{
							ComplianceInstanceId = entities.ComplianceScheduleOns.Where(x => x.ID == item).Select(I => I.ComplianceInstanceID).FirstOrDefault().Value;
						}
						ComplianceTransaction transaction = new ComplianceTransaction()
						{
							ComplianceScheduleOnID = item,
							ComplianceInstanceId = ComplianceInstanceId,
							CreatedBy = AuthenticationHelper.UserID,
							CreatedByText = AuthenticationHelper.User,
							StatusId = 20,
							StatusChangedOn = DateTime.ParseExact(tbxDate1.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture),
							Remarks = tbxRemarks1.Text
						};

						Business.ComplianceManagement.CreateTransaction(transaction, null, null, null);
						//Call Challan Code Auto Status Updates For Schedules 

						if (OnSaved != null)
						{
							OnSaved(this, null);
						}

						var Compliance = ComplianceManagement.Business.ComplianceManagement.GetInstanceTransactionCompliance(item);
						if (Compliance != null)
						{
							int customerID = -1;
							customerID = UserManagement.GetByID(Convert.ToInt32(Compliance.UserID)).CustomerID ?? 0;
							string ReplyEmailAddressName = CustomerManagement.GetByID(customerID).Name;
							var user = UserManagement.GetByID(Convert.ToInt32(Compliance.UserID));
							string message = Settings.Default.EMailTemplate_RejectedCompliance
												.Replace("@User", Compliance.User)
												.Replace("@ComplianceDescription", Compliance.ShortDescription)
												.Replace("@Role", Compliance.Role)
												.Replace("@ScheduledOn", Compliance.ScheduledOn.ToString("dd-MMM-yyyy"))
												.Replace("@From", ReplyEmailAddressName)
												.Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]));

							EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], (new string[] { user.Email }).ToList(), null, null, "AVACOM Notification :: Compliance has been rejected.", message);

						}
					}
					//ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "OpenDialog", "$(\"#divReviewerComplianceDetailsDialog\").dialog('close');", true);
					ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "CloseAndBindData();", true);
					cvDuplicateEntry.IsValid = false;
					cvDuplicateEntry.ErrorMessage = "Rejected Successfully";
					vsSummary.CssClass = "alert alert-success";
				}
				else
				{
					cvDuplicateEntry.IsValid = false;
					cvDuplicateEntry.ErrorMessage = "Please Provide Remark";
				}
			}
			catch (Exception ex)
			{
				LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
				cvDuplicateEntry.IsValid = false;
				cvDuplicateEntry.ErrorMessage = "Something went wrong. Please try again.";
			}
		}

		protected void grdTransactionAuditLog_RowCommand(object sender, GridViewCommandEventArgs e)
		{
			try
			{

				if (e.CommandName.Equals("DOWNLOAD_FILE"))
				{
					int fileID = Convert.ToInt32(e.CommandArgument);
					var file = Business.ComplianceManagement.GetFile(fileID);

					if (file.FilePath == null)
					{
						//Change by rahul on 21 JAN 2017 for Azure Drive or Local Drive
						if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
						{
							string pathvalue = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.Name));
							//using (FileStream fs = File.OpenRead(Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.Name))))
							using (FileStream fs = File.OpenRead(pathvalue.Replace("~", ConfigurationManager.AppSettings["DriveUrl"])))
							{
								int length = (int)fs.Length;
								byte[] buffer;

								using (BinaryReader br = new BinaryReader(fs))
								{
									buffer = br.ReadBytes(length);
								}

								Response.Buffer = true;
								Response.Clear();
								Response.ContentType = "application/octet-stream";
								Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
								if (file.EnType == "M")
								{
									Response.BinaryWrite(CryptographyHandler.Decrypt(buffer)); // create the file
								}
								else
								{
									Response.BinaryWrite(CryptographyHandler.AESDecrypt(buffer)); // create the file
								}
								Response.Flush(); // send it to the client to download
							}
						}
						else
						{
							using (FileStream fs = File.OpenRead(Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name))))
							{
								int length = (int)fs.Length;
								byte[] buffer;

								using (BinaryReader br = new BinaryReader(fs))
								{
									buffer = br.ReadBytes(length);
								}

								Response.Buffer = true;
								Response.Clear();
								Response.ContentType = "application/octet-stream";
								Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
								if (file.EnType == "M")
								{
									Response.BinaryWrite(CryptographyHandler.Decrypt(buffer)); // create the file
								}
								else
								{
									Response.BinaryWrite(CryptographyHandler.AESDecrypt(buffer)); // create the file
								}
								Response.Flush(); // send it to the client to download
							}
						}
					}
				}
			}
			catch (Exception ex)
			{
				LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
				cvDuplicateEntry.IsValid = false;
				cvDuplicateEntry.ErrorMessage = "Something went wrong. Please try again.";
			}
		}

		protected void btnDownload_Click(object sender, EventArgs e)
		{
			try
			{
				var file = Business.ComplianceManagement.GetFile(Convert.ToInt32(hdlSelectedDocumentID.Value));

				Response.Buffer = true;
				//Response.Clear();
				Response.ClearContent();
				Response.ContentType = "application/octet-stream";
				Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
				//Response.BinaryWrite(file.Data); // create the file
				Response.End();
				Response.Flush(); // send it to the client to download
			}
			catch (Exception ex)
			{
				LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
				cvDuplicateEntry.IsValid = false;
				cvDuplicateEntry.ErrorMessage = "Something went wrong. Please try again.";
			}

		}

		protected void lbDownloadSample1_Click(object sender, EventArgs e)
		{
			try
			{

				var complianceform = Business.ComplianceManagement.GetComplianceFileNameMultiple(Convert.ToInt32(lbDownloadSample1.CommandArgument));
				int complianceID1 = Convert.ToInt32(lbDownloadSample1.CommandArgument);
				using (ZipFile ComplianceZip = new ZipFile())
				{
					int i = 0;
					foreach (var file in complianceform)
					{
						if (file.FilePath != null)
						{
							string[] filename = file.Name.Split('.');
							string str = filename[0] + i + "." + filename[1];
							ComplianceZip.AddEntry(str, Business.DocumentManagement.ReadDocFiles(Server.MapPath(file.FilePath)));
							i++;
						}
					}

					string fileName = "ComplianceID_" + complianceID1 + "_SampleForms.zip";
					var zipMs = new MemoryStream();
					ComplianceZip.Save(zipMs);
					zipMs.Position = 0;
					byte[] data = zipMs.ToArray();

					Response.Buffer = true;

					Response.ClearContent();
					Response.ClearHeaders();
					Response.Clear();
					Response.ContentType = "application/zip";
					Response.AddHeader("content-disposition", "attachment; filename= " + fileName);
					Response.BinaryWrite(data);
					Response.Flush();
					HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
					HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
					HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
				}

				//var file = Business.ComplianceManagement.GetComplianceFormByID(Convert.ToInt32(lbDownloadSample1.CommandArgument));

				//Response.Buffer = true;
				//Response.Clear();
				//Response.ContentType = "application/octet-stream";

				//Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
				//Response.BinaryWrite(file.FileData); // create the file
				//Response.Flush(); // send it to the client to download

			}
			catch (Exception ex)
			{
				LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
				cvDuplicateEntry.IsValid = false;
				cvDuplicateEntry.ErrorMessage = "Something went wrong. Please try again.";
			}
		}

		protected void upComplianceDetails1_Load(object sender, EventArgs e)
		{
			try
			{
				DateTime date = DateTime.MinValue;
				if (DateTime.TryParseExact(tbxDate1.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
				{
					ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "InitializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
				}
				else
				{
					ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "InitializeDatePicker", "initializeDatePicker(null);", true);
				}

				// ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "Script", "initializeComboboxUpcoming();", true);
			}
			catch (Exception ex)
			{
				LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
				cvDuplicateEntry.IsValid = false;
				cvDuplicateEntry.ErrorMessage = "Something went wrong. Please try again.";
			}
		}

		protected void grdTransactionAuditLog_RowCreated(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.Header)
			{
				int sortColumnIndex = Convert.ToInt32(ViewState["SortIndexHistory"]);
				if (sortColumnIndex != -1)
				{
					AddSortImage(sortColumnIndex, e.Row);
				}
			}
		}

		public SortDirection direction
		{
			get
			{
				if (ViewState["dirState"] == null)
				{
					ViewState["dirState"] = SortDirection.Ascending;
				}
				return (SortDirection)ViewState["dirState"];
			}
			set
			{
				ViewState["dirState"] = value;
			}
		}

		public void OpenTransactionPage(int complianceInstanceID, string ScheduledOnID)
		{
			try
			{

				tbxRemarks1.Text = tbxDate1.Text = string.Empty;
				BindTransactionDetails(complianceInstanceID, ScheduledOnID);

				//upUsers.Update();
			}
			catch (Exception ex)
			{
				LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
				cvDuplicateEntry.IsValid = false;
				cvDuplicateEntry.ErrorMessage = "Something went wrong. Please try again.";
			}
		}

		public event EventHandler OnSaved;

		public void DownloadFile(int fileId)
		{
			try
			{
				var file = Business.ComplianceManagement.GetFile(fileId);

				if (file.FilePath != null)
				{
					string filePath = string.Empty;
					//Change by rahul on 21 JAN 2017 for Azure Drive or Local Drive
					if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
					{
						//filePath = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.Name));

						string pathvalue = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.Name));
						filePath = pathvalue.Replace("~", ConfigurationManager.AppSettings["DriveUrl"]);
					}
					else
					{
						filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
					}
					if (filePath != null && File.Exists(filePath))
					{
						Response.Buffer = true;
						Response.Clear();
						Response.ClearContent();
						Response.ContentType = "application/octet-stream";
						Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
						if (file.EnType == "M")
						{
							Response.BinaryWrite(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
						}
						else
						{
							Response.BinaryWrite(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
						}
						Response.Flush(); // send it to the client to download
						HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
						HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
						HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

					}
				}


			}
			catch (Exception)
			{
				throw;
				//LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
				//cvDuplicateEntry.IsValid = false;
				//cvDuplicateEntry.ErrorMessage = "Something went wrong. Please try again.";
			}
		}

		public void ViewDocument(int fileId)
		{
			try
			{
				var file = Business.ComplianceManagement.GetFile(fileId);

				if (file.FilePath != null)
				{
					string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
					//if (filePath != null && File.Exists(filePath))
					//{
					//    Response.Buffer = true;
					//    Response.Clear();
					//    Response.ClearContent();
					//    Response.ContentType = "application/octet-stream";
					//    Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
					//    Response.BinaryWrite(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
					//    Response.Flush(); // send it to the client to download
					//    Response.End();

					//}
					CompDocReviewPath = filePath;
				}
			}
			catch (Exception)
			{
				throw;
				//LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
				//cvDuplicateEntry.IsValid = false;
				//cvDuplicateEntry.ErrorMessage = "Something went wrong. Please try again.";
			}
		}
		protected void rptComplianceDocumnets_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnComplianceDocumnets");
				var scriptManager = ScriptManager.GetCurrent(this.Page);
				scriptManager.RegisterPostBackControl(lblDownLoadfile);
			}
		}

		protected void rptComplianceVersion_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnComplinceVersionDoc");
				var scriptManager = ScriptManager.GetCurrent(this.Page);
				scriptManager.RegisterPostBackControl(lblDownLoadfile);

				LinkButton lblDocumentVersion = (LinkButton)e.Item.FindControl("lblDocumentVersion");
				scriptManager.RegisterAsyncPostBackControl(lblDocumentVersion);
			}
		}

		protected void rptWorkingFiles_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnWorkingFiles");
				var scriptManager = ScriptManager.GetCurrent(this.Page);
				scriptManager.RegisterPostBackControl(lblDownLoadfile);
			}
		}

		protected void lnkSampleForm_Click(object sender, EventArgs e)
		{
			try
			{
				if (lnkSampleForm1.Text != "")
				{
					string url = lnkSampleForm1.Text;

					string fullURL = "window.open('" + url + "', '_blank', 'height=500,width=800,status=yes,toolbar=no,menubar=no,location=no,scrollbars=yes,resizable=no,titlebar=no' );";
					lnkSampleForm1.Attributes.Add("OnClick", fullURL);
				}
			}
			catch (Exception ex)
			{
				LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
				cvDuplicateEntry.IsValid = false;
				cvDuplicateEntry.ErrorMessage = "Something went wrong. Please try again.";
			}
		}

		protected void rptComplianceVersionView_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				var scriptManager = ScriptManager.GetCurrent(this.Page);

				LinkButton lblDocumentVersionView = (LinkButton)e.Item.FindControl("lblDocumentVersionView");
				scriptManager.RegisterAsyncPostBackControl(lblDocumentVersionView);
			}
		}

		protected void rptComplianceVersionView_ItemCommand(object source, RepeaterCommandEventArgs e)
		{
			try
			{
				string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

				if (e.CommandName.Equals("View"))
				{

					string[] commandArg = e.CommandArgument.ToString().Split(',');
					List<GetComplianceDocumentsView> CMPDocuments = DocumentManagement.GetFileDataView(Convert.ToInt32(commandArgs[0]), Convert.ToInt32(commandArgs[2])).ToList();
					Session["ScheduleOnID"] = commandArg[0];

					if (CMPDocuments != null)
					{
						List<GetComplianceDocumentsView> entitiesData = CMPDocuments.Where(entry => entry.Version != null).ToList();
						if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
						{
							GetComplianceDocumentsView entityData = new GetComplianceDocumentsView();
							entityData.Version = "1.0";
							entityData.ScheduledOnID = Convert.ToInt64(commandArg[0]);
							entitiesData.Add(entityData);
						}

						if (entitiesData.Count > 0)
						{
							foreach (var file in CMPDocuments)
							{
								string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
								if (file.FilePath != null && File.Exists(filePath))
								{
									string Folder = "~/TempFiles";
									string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

									string DateFolder = Folder + "/" + File;
									string extension = System.IO.Path.GetExtension(filePath);

									if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
									{
										lblMessageReviewer1.Text = "";
										lblMessageReviewer1.Text = "Zip file can't view please download it";
										ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReviewReviewerPopUp();", true);
									}
									else
									{
										Directory.CreateDirectory(Server.MapPath(DateFolder));

										if (!Directory.Exists(DateFolder))
										{
											Directory.CreateDirectory(Server.MapPath(DateFolder));
										}

										string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

										string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

										string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

										string FileName = DateFolder + "/" + User + "" + extension;

										FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
										BinaryWriter bw = new BinaryWriter(fs);
										if (file.EnType == "M")
										{
											bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
										}
										else
										{
											bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
										}
										bw.Close();
										CompDocReviewPath = FileName;
										CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
										ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReviewReviewer('" + CompDocReviewPath + "');", true);
										lblMessageReviewer1.Text = "";

									}
								}
								else
								{
									lblMessageReviewer1.Text = "There is no file to preview";
									ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReviewReviewerPopUp();", true);
								}
								break;
							}
						}
					}
				}
			}
			catch (Exception ex)
			{
				LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
				cvDuplicateEntry.IsValid = false;
				cvDuplicateEntry.ErrorMessage = "Something went wrong. Please try again.";
			}
		}

		protected string GetUserName(long taskInstanceID, long taskScheduleOnID, int roleID, byte taskType)
		{
			try
			{
				string result = "";

				int customerID = -1;
				customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

				result = TaskManagment.GetTaskAssignedUser(customerID, taskType, taskInstanceID, taskScheduleOnID, roleID);

				return result;
			}
			catch (Exception ex)
			{
				LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
				//cv.IsValid = false;
				//cvDuplicateEntry.ErrorMessage = "Something went wrong. Please try again.";
				return "";
			}

		}

		protected void gridSubTaskReviewer_RowCommand(object sender, GridViewCommandEventArgs e)
		{
			try
			{
				string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

				int taskScheduleOnID = Convert.ToInt32(commandArgs[0]);

				if (taskScheduleOnID != 0)
				{
					List<GetTaskDocumentView> taskDocument = new List<GetTaskDocumentView>();

					taskDocument = TaskManagment.GetTaskRelatedFileData(taskScheduleOnID).ToList();

					if (e.CommandName.Equals("Download"))
					{
						using (ZipFile ComplianceZip = new ZipFile())
						{
							if (taskDocument.Count > 0)
							{
								string fileName = string.Empty;

								GridViewRow row = (GridViewRow)(((Control)e.CommandSource).NamingContainer);

								Label lblTaskTitle = null;

								if (row != null)
								{
									lblTaskTitle = (Label)row.FindControl("lblTaskTitle");
									if (lblTaskTitle != null)
										fileName = lblTaskTitle.Text + "-Documents";

									if (fileName.Length > 250)
										fileName = "TaskDocuments";
								}

								ComplianceZip.AddDirectoryByName(commandArgs[0]);

								int i = 0;
								foreach (var eachFile in taskDocument)
								{
									//comment by rahul on 20 JAN 2017
									string filePath = Path.Combine(Server.MapPath(eachFile.FilePath), eachFile.FileKey + Path.GetExtension(eachFile.FileName));
									//string filePath = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.FileName));

									if (eachFile.FilePath != null && File.Exists(filePath))
									{
										string[] filename = eachFile.FileName.Split('.');
										string str = filename[0] + i + "." + filename[1];
										if (eachFile.EnType == "M")
										{
											ComplianceZip.AddEntry(commandArgs[0] + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
										}
										else
										{
											ComplianceZip.AddEntry(commandArgs[0] + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
										}
										i++;
									}
								}
							}

							var zipMs = new MemoryStream();
							ComplianceZip.Save(zipMs);
							zipMs.Position = 0;
							byte[] data = zipMs.ToArray();

							Response.Buffer = true;

							Response.ClearContent();
							Response.ClearHeaders();
							Response.Clear();
							Response.ContentType = "application/zip";
							Response.AddHeader("content-disposition", "attachment; filename=TaskDocuments.zip");
							Response.BinaryWrite(data);
							Response.Flush();
							HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
							HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
							HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
						}
					}
					else if (e.CommandName.Equals("View"))
					{
						List<GetTaskDocumentView> taskDocumenttoView = new List<GetTaskDocumentView>();

						taskDocumenttoView = TaskManagment.GetTaskRelatedFileData(taskScheduleOnID).ToList();

						Session["ScheduleOnID"] = taskScheduleOnID;

						if (taskDocumenttoView != null && taskDocumenttoView.Count > 0)
						{
							List<GetTaskDocumentView> entitiesData = taskDocumenttoView.Where(entry => entry.Version != null).ToList();

							if (taskDocumenttoView.Where(entry => entry.Version == null).ToList().Count > 0)
							{
								GetTaskDocumentView entityData = new GetTaskDocumentView();
								entityData.Version = "1.0";
								entityData.TaskScheduleOnID = Convert.ToInt64(taskScheduleOnID);
								entitiesData.Add(entityData);
							}

							if (entitiesData.Count > 0)
							{
								foreach (var file in taskDocumenttoView)
								{
									rptTaskVersionView.DataSource = entitiesData.OrderBy(entry => entry.Version);
									rptTaskVersionView.DataBind();

									string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

									if (file.FilePath != null && File.Exists(filePath))
									{
										string Folder = "~/TempFiles";
										string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

										string DateFolder = Folder + "/" + File;

										string extension = System.IO.Path.GetExtension(filePath);
										if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
										{
											lblMessagetask.Text = "";
											lblMessagetask.Text = "Zip file can't view please download it";
											ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendoctaskfileReview1PopUp();", true);
										}
										else
										{
											Directory.CreateDirectory(Server.MapPath(DateFolder));

											if (!Directory.Exists(DateFolder))
											{
												Directory.CreateDirectory(Server.MapPath(DateFolder));
											}

											string customerID = Convert.ToString(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

											string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

											string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

											string FileName = DateFolder + "/" + User + "" + extension;

											FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
											BinaryWriter bw = new BinaryWriter(fs);
											if (file.EnType == "M")
											{
												bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
											}
											else
											{
												bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
											}
											bw.Close();

											TaskDocViewPath = FileName;
											TaskDocViewPath = TaskDocViewPath.Substring(2, TaskDocViewPath.Length - 2);

											ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendoctaskfileReview1('" + TaskDocViewPath + "');", true);
											lblMessagetask.Text = "";
											UpdatePanel2.Update();
										}
									}
									else
									{
										lblMessagetask.Text = "There is no file to preview";
										ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendoctaskfileReview1PopUp();", true);
									}
									break;
								}
							}
						}
						#region

						#endregion
					}
				}
			}
			catch (Exception ex)
			{
				LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
				cvDuplicateEntry.IsValid = false;
				cvDuplicateEntry.ErrorMessage = "Something went wrong. Please try again.";
			}
		}

		protected void rptTaskVersionView_ItemCommand(object source, RepeaterCommandEventArgs e)
		{
			try
			{
				string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

				int taskScheduleOnID = Convert.ToInt32(commandArgs[0]);
				int taskFileidID = Convert.ToInt32(commandArgs[2]);
				if (taskScheduleOnID != 0)
				{
					if (e.CommandName.Equals("View"))
					{
						#region
						List<GetTaskDocumentView> taskDocumenttoView = new List<GetTaskDocumentView>();
						List<GetTaskDocumentView> taskFileData = new List<GetTaskDocumentView>();
						taskDocumenttoView = TaskManagment.GetTaskRelatedFileData(taskScheduleOnID).ToList();
						taskFileData = taskDocumenttoView;
						taskDocumenttoView = taskDocumenttoView.Where(entry => entry.FileID == taskFileidID).ToList();

						Session["ScheduleOnID"] = taskScheduleOnID;

						if (taskDocumenttoView != null && taskDocumenttoView.Count > 0)
						{
							List<GetTaskDocumentView> entitiesData = taskDocumenttoView.Where(entry => entry.Version != null).ToList();

							if (taskDocumenttoView.Where(entry => entry.Version == null).ToList().Count > 0)
							{
								GetTaskDocumentView entityData = new GetTaskDocumentView();
								entityData.Version = "1.0";
								entityData.TaskScheduleOnID = Convert.ToInt64(taskScheduleOnID);
								entitiesData.Add(entityData);
							}

							if (entitiesData.Count > 0)
							{
								rptTaskVersionView.DataSource = taskFileData.OrderBy(entry => entry.Version);
								rptTaskVersionView.DataBind();

								foreach (var file in taskDocumenttoView)
								{
									string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

									if (file.FilePath != null && File.Exists(filePath))
									{
										string Folder = "~/TempFiles";
										string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

										string DateFolder = Folder + "/" + File;

										string extension = System.IO.Path.GetExtension(filePath);

										Directory.CreateDirectory(Server.MapPath(DateFolder));
										if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
										{
											lblMessagetask.Text = "";
											lblMessagetask.Text = "Zip file can't view please download it";
											ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendoctaskfileReview1PopUp();", true);
										}
										else
										{
											if (!Directory.Exists(DateFolder))
											{
												Directory.CreateDirectory(Server.MapPath(DateFolder));
											}

											string customerID = Convert.ToString(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

											string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

											string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

											string FileName = DateFolder + "/" + User + "" + extension;

											FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
											BinaryWriter bw = new BinaryWriter(fs);
											if (file.EnType == "M")
											{
												bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
											}
											else
											{
												bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
											}
											bw.Close();

											TaskDocViewPath = FileName;
											TaskDocViewPath = TaskDocViewPath.Substring(2, TaskDocViewPath.Length - 2);

											ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendoctaskfileReview1('" + TaskDocViewPath + "');", true);
											lblMessagetask.Text = "";
											UpdatePanel2.Update();
										}
									}
									else
									{
										lblMessagetask.Text = "There is no file to preview";
										ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendoctaskfileReview1PopUp();", true);
									}
									break;
								}
							}
						}
						#endregion
					}
				}
			}
			catch (Exception ex)
			{
				LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
			}
		}

		protected void gridSubTaskReviewer_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			try
			{
				if (e.Row.RowType == DataControlRowType.DataRow)
				{
					Label lblStatus = (Label)e.Row.FindControl("lblStatus");
					Label lblTaskSlashReview = (Label)e.Row.FindControl("lblTaskSlashReview");
					LinkButton btnSubTaskDocView = (LinkButton)e.Row.FindControl("btnSubTaskDocView");
					LinkButton btnSubTaskDocDownload = (LinkButton)e.Row.FindControl("btnSubTaskDocDownload");
					Label lblIsTaskClose = (Label)e.Row.FindControl("lblIsTaskClose");

					if (lblStatus != null && btnSubTaskDocDownload != null && lblTaskSlashReview != null && btnSubTaskDocView != null)
					{
						if (lblStatus.Text != "")
						{
							if (lblStatus.Text == "Open")
							{
								btnSubTaskDocDownload.Visible = false;
								lblTaskSlashReview.Visible = false;
								btnSubTaskDocView.Visible = false;
							}
							else
							{
								if (lblIsTaskClose.Text == "True")
								{
									btnSubTaskDocDownload.Visible = false;
									lblTaskSlashReview.Visible = false;
									btnSubTaskDocView.Visible = false;
								}
								else
								{
									btnSubTaskDocDownload.Visible = true;
									lblTaskSlashReview.Visible = true;
									btnSubTaskDocView.Visible = true;
								}
							}
						}
					}
				}
			}
			catch (Exception ex)
			{
				LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
			}
		}

		protected void rptComplianceSampleView1_ItemCommand(object source, RepeaterCommandEventArgs e)
		{
			try
			{
				string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

				if (e.CommandName.Equals("View"))
				{
					string[] commandArg = e.CommandArgument.ToString().Split(',');
					ComplianceForm CMPDocuments = Business.ComplianceManagement.GetSelectedComplianceFileName(Convert.ToInt32(commandArgs[0]), Convert.ToInt32(commandArgs[1]));

					if (CMPDocuments != null)
					{
						string fullfilePath = Path.Combine(Server.MapPath(CMPDocuments.FilePath));
						string filePath = CMPDocuments.FilePath;
						string CompDocPath = filePath.Substring(2, filePath.Length - 2);
						if (CMPDocuments.FilePath != null && File.Exists(fullfilePath))
						{
							string extension = System.IO.Path.GetExtension(CompDocPath);

							if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
							{
								ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopenSampleFileReviewer();", true);
							}
							else
							{
								ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopenSampleFileReviewer('" + CompDocPath + "');", true);
							}
						}
						else
						{
							ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopenSampleFileReviewer();", true);
						}
					}
				}
			}
			catch (Exception ex)
			{
				LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
				cvDuplicateEntry.IsValid = false;
				cvDuplicateEntry.ErrorMessage = "Something went wrong. Please try again.";
			}
		}

		protected void rptComplianceSampleView1_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				var scriptManager = ScriptManager.GetCurrent(this.Page);

				LinkButton lblSampleView = (LinkButton)e.Item.FindControl("lblSampleView");
				scriptManager.RegisterAsyncPostBackControl(lblSampleView);
			}
		}

		protected void lbDownloadSample_Click(object sender, EventArgs e)
		{
			try
			{
				var complianceform = Business.ComplianceManagement.GetComplianceFileNameMultiple(Convert.ToInt32(lbDownloadSample1.CommandArgument));
				int complianceID1 = Convert.ToInt32(lbDownloadSample1.CommandArgument);
				using (ZipFile ComplianceZip = new ZipFile())
				{
					int i = 0;
					foreach (var file in complianceform)
					{
						if (file.FilePath != null)
						{
							string[] filename = file.Name.Split('.');
							string str = filename[0] + i + "." + filename[1];
							ComplianceZip.AddEntry(str, Business.DocumentManagement.ReadDocFiles(Server.MapPath(file.FilePath)));
							i++;
						}
					}

					string fileName = "ComplianceID_" + complianceID1 + "_SampleForms.zip";
					var zipMs = new MemoryStream();
					ComplianceZip.Save(zipMs);
					zipMs.Position = 0;
					byte[] data = zipMs.ToArray();

					Response.Buffer = true;

					Response.ClearContent();
					Response.ClearHeaders();
					Response.Clear();
					Response.ContentType = "application/zip";
					Response.AddHeader("content-disposition", "attachment; filename= " + fileName);
					Response.BinaryWrite(data);
					Response.Flush();
					HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
					HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
					HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
				}
			}
			catch (Exception ex)
			{
				LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
				cvDuplicateEntry.IsValid = false;
				cvDuplicateEntry.ErrorMessage = "Something went wrong. Please try again.";
			}
		}

		protected void grdComplianceDocument_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			try
			{
				if (!string.IsNullOrEmpty(hdnComplianceScheduledOnId.Value))
				{
					// int complianceScheduleOnID = Convert.ToInt32(hdnComplianceScheduledOnId.Value);
					string complianceScheduleOnIDs = hdnComplianceScheduledOnId.Value;
					string[] ScheduleOnIdArray = complianceScheduleOnIDs.ToString().Split(',');
					List<int> LstScheduleOnIds = new List<int>();
					foreach (var item in ScheduleOnIdArray)
					{
						LstScheduleOnIds.Add(Convert.ToInt32(item));

					}
					BindComplianceDocuments(LstScheduleOnIds);
					grdComplianceDocument.PageIndex = e.NewPageIndex;

				}
			}
			catch (Exception ex)
			{
				LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
				cvDuplicateEntry.IsValid = false;
				cvDuplicateEntry.ErrorMessage = "Something went wrong. Please try again.";
			}
		}

		protected void grdComplianceDocument_RowCommand(object sender, GridViewCommandEventArgs e)
		{
			try
			{
				if (e.CommandArgument != null)
				{
					if (e.CommandName.Equals("DeleteDoc"))
					{
						if (!string.IsNullOrEmpty(hdnComplianceScheduledOnId.Value))
						{
							int fileID = -1;
							string ScheduledOnID = Convert.ToString(hdnComplianceScheduledOnId.Value);
							string[] ScheduleOnIdArray = ScheduledOnID.ToString().Split(',');
							List<int> LstScheduleOnIds = new List<int>();
							foreach (var item in ScheduleOnIdArray)
							{
								LstScheduleOnIds.Add(Convert.ToInt32(item));
								int complianceScheduleOnID = Convert.ToInt32(item);
								string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
								fileID = Convert.ToInt32(commandArgs[0]);

								if (fileID > 0)
								{
									if (DeleteFile(fileID))
									{
										ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "viewAlert", "showAlert('Document Deleted Successfully','Info');", true);


									}
								}
							}

							BindComplianceDocuments(LstScheduleOnIds);

						}
					}
					else if (e.CommandName.Equals("Download"))
					{
						long FileID = -1;
						string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
						FileID = Convert.ToInt64(commandArgs[0]);
						if (FileID > 0)
						{
							using (ZipFile ComplianceZip = new ZipFile())
							{
								var file = Business.ComplianceManagement.GetFile(Convert.ToInt32(FileID));
								//TempComplianceDocument file = Business.ComplianceManagement.GetTempComplianceDocument(FileID);
								if (file != null)
								{
									string filePath = string.Empty;
									if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
									{
										string pathvalue = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.Name));
										filePath = pathvalue.Replace("~", ConfigurationManager.AppSettings["DriveUrl"]);
									}
									else
									{
										filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
									}

									if (file.FilePath != null && File.Exists(filePath))
									{
										Response.Buffer = true;
										Response.ClearContent();
										Response.ClearHeaders();
										Response.Clear();
										Response.ContentType = "application/octet-stream";
										Response.AddHeader("content-disposition", "attachment; filename= " + file.Name);
										if (file.EnType == "M")
										{
											Response.BinaryWrite(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
										}
										else
										{
											Response.BinaryWrite(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
										}
										//Response.TransmitFile(Server.MapPath(file.FilePath));
										//Response.Flush();
										HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
										HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
										HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
									}
								}
							}
						}
					}
					else if (e.CommandName.Equals("View"))
					{
						if (!string.IsNullOrEmpty(hdnComplianceScheduledOnId.Value))
						{
							string ScheduledOnID = Convert.ToString(hdnComplianceScheduledOnId.Value);
							string[] ScheduleOnIdArray = ScheduledOnID.ToString().Split(',');
							List<int> LstScheduleOnIds = new List<int>();
							foreach (var item in ScheduleOnIdArray)
							{
								int complianceScheduleOnID = Convert.ToInt32(item);
								int fileID = Convert.ToInt32(e.CommandArgument);

								List<GetComplianceDocumentsView> CMPDocuments = DocumentManagement.GetFileDataView(complianceScheduleOnID, fileID).ToList();

								if (CMPDocuments != null)
								{
									List<GetComplianceDocumentsView> entitiesData = CMPDocuments.Where(entry => entry.Version != null).ToList();

									if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
									{
										GetComplianceDocumentsView entityData = new GetComplianceDocumentsView();
										entityData.Version = "1.0";
										entityData.ScheduledOnID = Convert.ToInt64(complianceScheduleOnID);
										entitiesData.Add(entityData);
									}

									if (entitiesData.Count > 0)
									{
										foreach (var file in CMPDocuments)
										{
											string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
											if (file.FilePath != null && File.Exists(filePath))
											{
												string Folder = "~/TempFiles";
												string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

												string DateFolder = Folder + "/" + File;

												string extension = System.IO.Path.GetExtension(filePath);
												if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
												{
													//lblMessage.Text = "";
													//lblMessage.Text = "Zip file can't view please download it";
													//ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showAlert('Archive');", true);
												}
												else
												{
													Directory.CreateDirectory(Server.MapPath(DateFolder));

													if (!Directory.Exists(DateFolder))
													{
														Directory.CreateDirectory(Server.MapPath(DateFolder));
													}

													int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
													string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

													string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

													string FileName = DateFolder + "/" + User + "" + extension;

													FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
													BinaryWriter bw = new BinaryWriter(fs);
													if (file.EnType == "M")
													{
														bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
													}
													else
													{
														bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
													}

													bw.Close();
													CompDocReviewPath = FileName;
													CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);

													ScriptManager.RegisterStartupScript(Page, Page.GetType(), "OpenDocViewer", "funDocumentViewer('" + CompDocReviewPath + "');", true);
												}
											}
											else
											{
												//lblMessage.Text = "There is no file to preview";
												ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "viewAlert", "showAlert('No Document available for Preview','Info');", true);
											}

											break;
										}
									}
								}

								#region Temp Document Show
								//long FileID = -1;
								//string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
								//FileID = Convert.ToInt64(commandArgs[0]);
								//if (FileID > 0)
								//{
								//    TempComplianceDocument file = Business.ComplianceManagement.GetTempComplianceDocument(FileID);
								//    if (file != null)
								//    {
								//        string filePath = Path.Combine(Server.MapPath(file.DocPath));
								//        if (file.DocName != null && File.Exists(filePath))
								//        {
								//            string Folder = "~/TempFiles";
								//            string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
								//            string DateFolder = Folder + "/" + File;
								//            string extension = System.IO.Path.GetExtension(filePath);
								//            if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
								//            {
								//                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Zip file can't view please download it.')", true);
								//                cvDuplicateEntry.IsValid = false;
								//                cvDuplicateEntry.ErrorMessage = "Zip file can't view please download it.";
								//            }
								//            else
								//            {
								//                string CompDocReviewPath = file.DocPath;

								//                CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
								//                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopenDocumentPriview('" + CompDocReviewPath + "');", true);
								//            }
								//        }
								//        else
								//        {
								//            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('There is no file to preview.')", true);
								//            cvDuplicateEntry.IsValid = false;
								//            cvDuplicateEntry.ErrorMessage = "There is no file to preview.";
								//        }
								//    }
								//} 
								#endregion
							}



						}
					}
				}
			}
			catch (Exception ex)
			{
				LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
				cvDuplicateEntry.IsValid = false;
				cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again"; ;
			}
		}

		protected void grdComplianceDocument_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			try
			{
				if (e.Row.RowType == DataControlRowType.DataRow)
				{
					Label lblDocumentName = (Label)e.Row.FindControl("lblDocumentName");
					Label lblIsLinkTrue = (Label)e.Row.FindControl("lblIsLinkTrue");
					Label lblDocType = (Label)e.Row.FindControl("lblDocType");

					if (lblDocType.Text.Trim() == "1")
					{
						lblDocType.Text = "Compliance";
					}
					else
					{
						lblDocType.Text = "Working Files";
					}

					LinkButton lnkRedirectDocument = (LinkButton)e.Row.FindControl("lnkRedirectDocument");
					LinkButton lnkDownloadDocument = (LinkButton)e.Row.FindControl("lnkDownloadDocument");
					LinkButton lnkViewDocument = (LinkButton)e.Row.FindControl("lnkViewDocument");
					//LinkButton lnkDeleteDocument = (LinkButton)e.Row.FindControl("lnkDeleteDocument");

					if (lblIsLinkTrue.Text == "True")
					{
						lnkDownloadDocument.Visible = false;
						lnkViewDocument.Visible = false;
						lnkRedirectDocument.Visible = true;
						lblDocumentName.Visible = false;
						//lnkDeleteDocument.Visible = false;
					}
					else
					{
						lnkDownloadDocument.Visible = true;
						lnkViewDocument.Visible = true;
						lnkRedirectDocument.Visible = false;
						lblDocumentName.Visible = true;
					}
				}
				//if (e.Row.RowType == DataControlRowType.DataRow)
				//{
				//    Label lblDocType = (Label)e.Row.FindControl("lblDocType");

				//    if (lblDocType.Text.Trim() == "S")
				//    {
				//        lblDocType.Text = "Compliance Document";
				//    }
				//    else
				//    {
				//        lblDocType.Text = "Working Files";
				//    }
				//}
			}
			catch (Exception ex)
			{
				LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
				cvDuplicateEntry.IsValid = false;
				cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
			}
		}

		public bool DeleteFile(int fileID)
		{
			try
			{
				bool deleteSuccess = false;
				var file = Business.ComplianceManagement.GetFile(fileID);
				if (file != null)
				{
					string path = string.Empty;
					if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
					{
						string pathvalue = file.FilePath + "/" + file.FileKey + Path.GetExtension(file.Name);
						path = pathvalue.Replace("~", ConfigurationManager.AppSettings["DriveUrl"]);
					}
					else
					{
						path = Server.MapPath(file.FilePath + "/" + file.FileKey + Path.GetExtension(file.Name));
					}

					deleteSuccess = RLCS_DocumentManagement.DeleteFile(path, fileID);
				}

				return deleteSuccess;
			}
			catch (Exception ex)
			{
				LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
				cvDuplicateEntry.IsValid = false;
				cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
				return false;
			}
		}
		protected void btnSubmit_Click(object sender, EventArgs e)
		{
			try
			{
				if (!string.IsNullOrEmpty(hdnComplianceScheduledOnId.Value))
				{
					int count =0;
					string complianceScheduleOnIDs = hdnComplianceScheduledOnId.Value;
					string[] ScheduleOnIdArray = complianceScheduleOnIDs.ToString().Split(',');
					using (ComplianceDBEntities entities = new ComplianceDBEntities())
					{
						foreach (var item in ScheduleOnIdArray)
						{
							long scheduleOnID = Convert.ToInt32(item);
							var record = (from row in entities.FileDataMappings where row.ScheduledOnID == scheduleOnID && row.FileType == 1 select row).ToList();
							if (record.Count > 0)
							{
								count++;
								int StatusID;
								StatusID = getComplianceStatusID(scheduleOnID);
								createTransactionStatusID(scheduleOnID, StatusID, AuthenticationHelper.UserID, AuthenticationHelper.User);
							}
							
						}
						if (count != 0)
						{
							cvDuplicateEntry.IsValid = false;
							cvDuplicateEntry.ErrorMessage = "Submitted Successfully";
							vsSummary.CssClass = "alert alert-success";
						}
						else
						{
							cvDuplicateEntry.IsValid = false;
							cvDuplicateEntry.ErrorMessage = "No Documents to Submit.";
						}
					}
				}
			}
			catch (Exception ex)
			{
				LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
				cvDuplicateEntry.IsValid = false;
				cvDuplicateEntry.ErrorMessage = "Something went wrong. Please try again.";
			}
		}


		protected void btnUpload_Click(object sender, EventArgs e)
		{
			try
			{
				bool uploadSuccess = false;
				if (!string.IsNullOrEmpty(hdnComplianceScheduledOnId.Value))
				{
					long transactionID = 0;
					string complianceScheduleOnIDs = hdnComplianceScheduledOnId.Value;
					string[] ScheduleOnIdArray = complianceScheduleOnIDs.ToString().Split(',');
					List<int> LstScheduleOnIds = new List<int>();
					foreach (var item in ScheduleOnIdArray)
					{
						LstScheduleOnIds.Add(Convert.ToInt32(item));
					}
					long scheduleOnID = Convert.ToInt32(LstScheduleOnIds[0]);

					var recentTxnRecord = Business.ComplianceManagement.GetCurrentStatusByComplianceID(Convert.ToInt32(scheduleOnID));

					if (recentTxnRecord != null)
					{
						transactionID = recentTxnRecord.ComplianceTransactionID;
					}

					if (transactionID != 0)
					{
						#region Save Uploaded Document

						HttpFileCollection fileCollection = Request.Files;

						if (fileCollection.Count > 0)
						{
							int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

							var InstanceData = DocumentManagement.GetComplianceInstanceData(Convert.ToInt32(hdnComplianceInstanceID.Value));

							if (customerID != 0 && InstanceData != null)
							{
								List<FileData> lstFileData = new List<FileData>();
								List<KeyValuePair<string, int>> lstFileNameWithType = new List<KeyValuePair<string, int>>();
								List<KeyValuePair<string, Byte[]>> fileListWithData = new List<KeyValuePair<string, Byte[]>>();

								for (int i = 0; i < fileCollection.Count; i++)
								{
									HttpPostedFile uploadfile = null;
									uploadfile = fileCollection[i];

									string fileName = Path.GetFileName(uploadfile.FileName);

									string directoryPath = string.Empty;
									string version = string.Empty;

									if (!string.IsNullOrEmpty(fileName))
									{
										if (!string.IsNullOrEmpty(rbDocType.SelectedValue))
										{
											lstFileNameWithType.Add(new KeyValuePair<string, int>(fileName, Convert.ToInt32(rbDocType.SelectedValue)));
										}

										version = RLCS_DocumentManagement.GetDocumentVersion(Convert.ToInt32(scheduleOnID), fileName, Convert.ToInt32(rbDocType.SelectedValue));

										if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
										{
											directoryPath = ConfigurationManager.AppSettings["DriveUrl"] + "\\AvacomDocuments\\" + customerID + "\\" + InstanceData.CustomerBranchID + "\\" + InstanceData.ID.ToString() + "\\" + scheduleOnID.ToString() + "\\" + version;
										}
										else
										{
											directoryPath = ConfigurationManager.AppSettings["AVACOM_FileStorage_Path"] + "/AvacomDocuments/" + customerID + "/" + InstanceData.CustomerBranchID + "/" + InstanceData.ID.ToString() + "/" + scheduleOnID.ToString() + "/" + version;
										}

										if (!Directory.Exists(directoryPath))
											Directory.CreateDirectory(directoryPath);

										Guid fileKey = Guid.NewGuid();

										//string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(fileName));
										//finalPath = finalPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");

										string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(fileName));

										string filepathvalue = string.Empty;
										if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
										{
											string vale = directoryPath.Replace(ConfigurationManager.AppSettings["DriveUrl"], "~");
											filepathvalue = vale.Replace(@"\", "/");
										}
										else
										{
											filepathvalue = directoryPath.Substring(ConfigurationManager.AppSettings["AVACOM_FileStorage_Path"].Length).Replace('\\', '/').Insert(0, "~/");
										}

										Stream fs = uploadfile.InputStream;
										BinaryReader br = new BinaryReader(fs);
										Byte[] bytes = br.ReadBytes((Int32)fs.Length);

										fileListWithData.Add(new KeyValuePair<string, Byte[]>(finalPath, bytes));

										long fileSize = fs.Length;
										string DocType = Convert.ToString(hdnComplianceType.Value);

										string Code = "";
										if (DocType == "Challan")
										{
											Code = Convert.ToString(hdnComplianceCode.Value);
										}
										FileData newFile = new FileData()
										{
											Name = fileName,
											FilePath = filepathvalue,
											FileKey = fileKey.ToString(),
											//Version = "1.0",
											Version = version,
											VersionDate = DateTime.Now,
											FileSize = fileSize,
											EnType = "A",
											Code_Esi_Epf_Lwf_Pt = Code
										};

										lstFileData.Add(newFile);
									}
								}

								if (lstFileData.Count > 0)
								{
									lstFileData.ForEach(eachFileData =>
									{
										uploadSuccess = RLCS_DocumentManagement.CreateFileDataMappingV2(transactionID, scheduleOnID, eachFileData, lstFileNameWithType, fileListWithData);
									});
								}

								if (uploadSuccess)
								{
									cvDuplicateEntry.IsValid = false;
									cvDuplicateEntry.ErrorMessage = "Document Uploaded Successfully";
									vsSummary.CssClass = "alert alert-success";

									//BindComplianceDocuments(Convert.ToInt32(scheduleOnID));

									BindComplianceDocuments(LstScheduleOnIds);


								}
							}
						}
						else
						{
							cvDuplicateEntry.IsValid = false;
							cvDuplicateEntry.ErrorMessage = "Select one or more document(s) to upload";
						}

						#endregion
					}
				}
			}
			catch (Exception ex)
			{
				LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
				cvDuplicateEntry.IsValid = false;
				cvDuplicateEntry.ErrorMessage = "Something went wrong. Please try again.";
			}
		}

		private void createTransactionStatusID(long scheduleOnID, long statusID, int UserID, string createdBy)
		{
			try
			{
				using (ComplianceDBEntities entities = new ComplianceDBEntities())
				{
					var _objlist = (from row in entities.ComplianceScheduleOns where row.ID == scheduleOnID select row).FirstOrDefault();
					var ComplianceInstanceID = _objlist.ComplianceInstanceID;

					ComplianceTransaction complianceTransaction = new ComplianceTransaction()
					{
						ComplianceInstanceId = (long)ComplianceInstanceID,
						StatusId = (int)statusID,
						Dated = DateTime.Now,
						CreatedBy = UserID,
						CreatedByText = createdBy,
						StatusChangedOn = DateTime.Now,
						ComplianceScheduleOnID = scheduleOnID,
						Remarks = ""
					};

					entities.ComplianceTransactions.Add(complianceTransaction);
					entities.SaveChanges();
				}
			}
			catch (Exception ex)
			{ }

		}
		private int getComplianceStatusID(long scheduleOnID)
		{


			int StatusID = 0;
			try
			{
				using (ComplianceDBEntities entities = new ComplianceDBEntities())
				{
					var ScheduleOnDate = (from rows in entities.ComplianceScheduleOns where rows.ID == scheduleOnID select rows.ScheduleOn).FirstOrDefault();
					if (DateTime.Now > ScheduleOnDate)
					{
						StatusID = 3;
					}
					else
					{
						StatusID = 2;
					}
				}
			}
			catch (Exception ex)
			{
				StatusID = 0;
			}
			return StatusID;

		}
	}
}
