﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HRPlus_MyCompliancesDashboard.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.HRPlus_MyCompliancesDashboard" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../NewCSS/kendo_V1.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo_V1.silver.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />
    <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../Newjs/jszip.min.js"></script>
    <link href="../NewCSS/Kendouicss.css" rel="stylesheet" />

    <style type="text/css">
        .ui-widget-header {
            border: 0px !important;
            background: inherit;
            font-size: 20px;
            color: #666666;
            font-weight: normal;
            padding-top: 0px;
            margin-top: 5px;
        }

        .table > tbody > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

            .table > tbody > tr > th > a {
                vertical-align: bottom;
                color: #666666 !important;
                text-decoration: none !important;
                border-bottom: 2px solid #dddddd;
            }

        .table tbody > tr > td, .table tbody > tr > th, .table tfoot > tr > td, .table tfoot > tr > th, .table thead > tr > td, .table thead > tr > th {
            padding: 5px;
            line-height: 1.428571429;
            vertical-align: middle;
        }

        .table tr th {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .clsheadergrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: bottom !important;
            border-bottom: 2px solid #dddddd !important;
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
            text-align: left;
        }

        .clsROWgrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: top !important;
            border-top: 1px solid #dddddd !important;
            color: #666666 !important;
            font-size: 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }

        .m-10 {
            margin-top: 10px;
            margin-left: 10px;
        }

        .btn-search {
            background: #1fd9e1;
            float: right;
        }

        .chosen-results {
            max-height: 150px !important;
        }

        .table, pre.prettyprint {
            margin-bottom: 0px;
        }

        .k-loading-image {
            display: none;
        }

        .chosen-container-single .chosen-single {
            box-shadow: none !important;
        }

        #gridCompliances {
            table-layout: fixed;
            text-overflow: ellipsis;
            width: 100%;
            overflow: hidden;
            white-space: nowrap;
        }

        td {
            max-width: 100px;
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
        }

        .action {
            width: 50px;
        }

        .compliance {
            width: 404px;
        }

        .Top-Mar {
            margin-top: 6px;
        }

        .tvFilterLocation {
            position: absolute;
            z-index: 50;
            background: white;
            border: 1px solid rgb(195, 185, 185);
            height: 200px;
            width: 100%;
            display: block;
            overflow: scroll;
        }
    </style>
    <style>
        .k-loading-mask {
            width: 0px;
            height: 0px;
        }

    </style>
    <script type="text/javascript">
        $(document).ready(function () {
           
        });

        function onOpen(e) {
            kendo.ui.progress(e.sender.element, true);
        }

        function ShowComplianceDialog(InstanceID, ScheduleOnID) {

            $('#divMyCompliances').show();

            var myWindowAdv = $("#divMyCompliances");

            function onClose() {
                $(this.element).empty();
            }

            myWindowAdv.kendoWindow({
                width: "95%",
                height: "90%",
                content: "../RLCS/RLCS_ComplianceStatusTransaction.aspx?instanceID=" + InstanceID + "&scheduleOnID=" + ScheduleOnID,
                iframe: true,
                visible: false,
                actions: ["Close"],
                open: onOpen,
                close: onClose
            });

            myWindowAdv.data("kendoWindow").center().open();

            return false;
        }

        function CallParentMethod(complianceType, returnRegisterChallanID, actID, complianceID, branchID, month, year) {

            //alert("called");
            bindFromToMonthPicker();
            window.parent.GotoComplianceInputOutputPage(complianceType, returnRegisterChallanID, actID, complianceID, branchID, month, year);
        }
    </script>



    <script>

        var status;

        $(document).ready(function () {
            GetAndBindCompliances();
           
        });

        function GetAndBindCompliances() {
            jQuery.ajax({

                type: "POST",
                url: "HRPlus_MyCompliancesDashboard.aspx/GetHRMyCompliances",
                contentType: "application/json; charset=utf-8",
                dataType: "Json",
                data: {},
                success: function (data) {
                    var dataArray = data.d;
                    BindGrid(dataArray);
                    //kendo.ui.progress(e.sender.element, false);
                },
                error: function (d) {
                    alert("error" + d);
                },
                pageSize:2,
            });

            function BindGrid(dataArray) {
                $("#grid").kendoGrid({
                    dataSource: dataArray,
                    
                    height: 85,
                    sortable: false,                   
                    filterable: false,
                    columnMenu: false,
                    reorderable: false,
                    resizable: false,
                    multi: true,
                    selectable: false,

                    dataBound: function() {
                        var grid = this;
                        var trs = this.tbody.find('tr').each(function () {
                            var item = grid.dataItem($(this));
                            if (status != "PR") {
                                $(this).find('.k-grid-edit').hide();
                            }
                        });
                    },
                    columns: [
                       {
                           title: "Customer",
                           field: "CustomerName",
                           width: "10%;",

                           attributes: {
                               //border-width: 0px 1px 1px 0px
                               style: 'white-space: nowrap;text-align:left;'
                           }, filterable: { multi: true, search: true },

                       },
                        {
                            field: "CustomerBranchName",
                            title: "Branch",
                            width: "8%",
                            attributes: {
                                //border-width: 0px 1px 1px 0px;
                                style: 'white-space: nowrap;'
                            },
                            filterable: {
                                multi: false,
                                extra: false,
                                search: false,
                                operators: {
                                    string: {
                                        eq: "Is equal to",
                                        neq: "Is not equal to",
                                        contains: "Contains"
                                    }
                                }
                            }
                        },
                        {
                            field: "ShortForm",
                            title: "Compliance",
                            width: "10%",
                            filterable: {
                                extra: false,
                                multi: false,
                                search: false,
                                operators: {
                                    string: {
                                        eq: "Is equal to",
                                        neq: "Is not equal to",
                                        contains: "Contains"
                                    }
                                }
                            },
                        },
                        {
                            field: "RequiredForms",
                            title: "Form",
                            width: "7%",
                            attributes: {
                                //border-width: 0px 1px 1px 0px;'
                                style: 'white-space: nowrap;'
                            },
                            filterable: {
                                extra: false,
                                multi: true,
                                search: true,
                                operators: {
                                    string: {
                                        eq: "Is equal to",
                                        neq: "Is not equal to",
                                        contains: "Contains"
                                    }
                                }
                            }
                        },
                        {
                            field: "Frequency",
                            title: "Frequency",
                            width: "6%",
                            attributes: {
                                style: 'white-space: nowrap;'
                            },
                            filterable: {
                                extra: false,
                                multi: true,
                                search: true,
                                operators: {
                                    string: {
                                        eq: "Is equal to",
                                        neq: "Is not equal to",
                                        contains: "Contains"
                                    }
                                }
                            }
                        },
                        {
                            field: "ForMonth",
                            title: "Period",
                            width: "6%",
                            attributes: {
                                style: 'white-space: nowrap;'
                            },
                            filterable: {
                                extra: false,
                                multi: true,
                                search: true,
                                operators: {
                                    string: {
                                        eq: "Is equal to",
                                        neq: "Is not equal to",
                                        contains: "Contains"
                                    }
                                }
                            }
                        },
                        {
                            field: "ScheduledOn",
                            title: "Due Date",
                            template: "#= kendo.toString(kendo.parseDate(ScheduledOn, 'yyyy-MM-dd'), 'MM/dd/yyyy') #",
                            width: "8%",
                            attributes: {
                                style: 'white-space: nowrap;'
                            },
                            filterable: {
                                extra: false,
                                multi: true,
                                search: true,
                                operators: {
                                    string: {
                                        eq: "Is equal to",
                                        neq: "Is not equal to",
                                        contains: "Contains"
                                    }
                                }
                            }
                        },
                        {
                            field: "ComplianceStatusName",
                            title: "Status",
                            width: "8%",
                            hidden: true,
                            attributes: {
                                //border-width: 0px 1px 1px 0px;
                                style: 'white-space: nowrap;'
                            },
                            //template: '#:checkCustomerActive(Status)#',
                            filterable: {
                                extra: false,
                                multi: true,
                                search: true,
                                operators: {
                                    string: {
                                        eq: "Is equal to",
                                        neq: "Is not equal to",
                                        contains: "Contains"
                                    }
                                }
                            }
                        },
                        {
                            field: "CanChangeStatus",
                            title: "CanChangeStatus",
                            width: "8%",
                            hidden: true,
                            attributes: {
                                //border-width: 0px 1px 1px 0px;
                                style: 'white-space: nowrap;'
                            },
                            //template: '#:checkCustomerActive(Status)#',
                            filterable: {
                                extra: false,
                                multi: true,
                                search: true,
                                operators: {
                                    string: {
                                        eq: "Is equal to",
                                        neq: "Is not equal to",
                                        contains: "Contains"
                                    }
                                }
                            }
                        },
                        {
                            command:
                                [
                                { name: "edit", text: "", iconClass: ".k-icon k-i-eye k-icon k-edit", className: "k-grid-edit1" }
                                ],
                                
                            title: "Action",
                            lock: true, width: "8%;",
                            attributes: {
                               
                                style: 'white-space: nowrap;'
                            },
                        },
                       

                    ]

                });
            }

        }
        $.ajax({
        type: "POST",
        url: "HRPlus_MyCompliancesDashboard.aspx/GetHRMyCompliances",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        processData: true,
       
        success: function (data) {
          
            kendo.ui.progress($("#gridGenerateDocument"), false);
          
            $("#gridGenerateDocument").kendoGrid({

                dataSource: {
                    data: data.Result,
                    schema: {
                        model: {

                        }
                    },
                    pageSize: 10,

                    total: function (data) {
                        if (data.Result != null && data.Result != undefined) {
                            if (data.Result.length > 0) {

                            }
                            return data.Result.length;
                        }
                    }
                },
                //height: 400,
                sortable: true,
                filterable: true,
                columnMenu: true,
                pageable: {
                    refresh: false,
                    pageSizes:true,
                    pageSize:10,
                    buttonCount: 3,
                   
                },
                reorderable: true,
                resizable: true,
                selectable: true,
                columns: [
      {
          title: "Customer",
          field: "CustomerName",
          width: "10%;",

          attributes: {
              //border-width: 0px 1px 1px 0px
              style: 'white-space: nowrap;text-align:left;'
          }, filterable: { multi: true, search: true },

      },
       {
           field: "CustomerBranchName",
           title: "Branch",
           width: "8%",
           attributes: {
               //border-width: 0px 1px 1px 0px;
               style: 'white-space: nowrap;'
           },
           filterable: {
               multi: false,
               extra: false,
               search: false,
               operators: {
                   string: {
                       eq: "Is equal to",
                       neq: "Is not equal to",
                       contains: "Contains"
                   }
               }
           }
       },
       {
           field: "ShortForm",
           title: "Compliance",
           width: "10%",
           filterable: {
               extra: false,
               multi: false,
               search: false,
               operators: {
                   string: {
                       eq: "Is equal to",
                       neq: "Is not equal to",
                       contains: "Contains"
                   }
               }
           },
       },
       {
           field: "RequiredForms",
           title: "Form",
           width: "7%",
           attributes: {
               //border-width: 0px 1px 1px 0px;'
               style: 'white-space: nowrap;'
           },
           filterable: {
               extra: false,
               multi: true,
               search: true,
               operators: {
                   string: {
                       eq: "Is equal to",
                       neq: "Is not equal to",
                       contains: "Contains"
                   }
               }
           }
       },
       {
           field: "Frequency",
           title: "Frequency",
           width: "6%",
           attributes: {
               style: 'white-space: nowrap;'
           },
           filterable: {
               extra: false,
               multi: true,
               search: true,
               operators: {
                   string: {
                       eq: "Is equal to",
                       neq: "Is not equal to",
                       contains: "Contains"
                   }
               }
           }
       },
       {
           field: "ForMonth",
           title: "Period",
           width: "6%",
           attributes: {
               style: 'white-space: nowrap;'
           },
           filterable: {
               extra: false,
               multi: true,
               search: true,
               operators: {
                   string: {
                       eq: "Is equal to",
                       neq: "Is not equal to",
                       contains: "Contains"
                   }
               }
           }
       },
       {
           field: "ScheduledOn",
           title: "Due Date",
           template: "#= kendo.toString(kendo.parseDate(ScheduledOn, 'yyyy-MM-dd'), 'MM/dd/yyyy') #",
           width: "8%",
           attributes: {
               style: 'white-space: nowrap;'
           },
           filterable: {
               extra: false,
               multi: true,
               search: true,
               operators: {
                   string: {
                       eq: "Is equal to",
                       neq: "Is not equal to",
                       contains: "Contains"
                   }
               }
           }
       },
       {
           field: "ComplianceStatusName",
           title: "Status",
           width: "8%",
           hidden: true,
           attributes: {
               //border-width: 0px 1px 1px 0px;
               style: 'white-space: nowrap;'
           },
           //template: '#:checkCustomerActive(Status)#',
           filterable: {
               extra: false,
               multi: true,
               search: true,
               operators: {
                   string: {
                       eq: "Is equal to",
                       neq: "Is not equal to",
                       contains: "Contains"
                   }
               }
           }
       },
       {
           field: "CanChangeStatus",
           title: "CanChangeStatus",
           width: "8%",
           hidden: true,
           attributes: {
               //border-width: 0px 1px 1px 0px;
               style: 'white-space: nowrap;'
           },
           //template: '#:checkCustomerActive(Status)#',
           filterable: {
               extra: false,
               multi: true,
               search: true,
               operators: {
                   string: {
                       eq: "Is equal to",
                       neq: "Is not equal to",
                       contains: "Contains"
                   }
               }
           }
       },
       {
           command:
               [
               { name: "edit", text: "", iconClass: ".k-icon k-i-eye k-icon k-edit", className: "k-grid-edit1" }
               ],
                                
           title: "Action",
           lock: true, width: "8%;",
           attributes: {
                               
               style: 'white-space: nowrap;'
           },
       },
                       

                ]
               
            });
            $("#gridGenerateDocument").kendoTooltip({
                filter: ".k-grid-edit",
                content: function (e) {
                    return "Generate Document";
                }
            });
            $("#gridGenerateDocument").kendoTooltip({
                filter: ".k-grid-edit2",
                content: function (e) {
                    return "View Document";
                }
            });
            $("#gridGenerateDocument").kendoTooltip({
                   filter: "td", //this filter selects the second column's cells
                   position: "bottom",
                   content: function (e) {
                          var content = e.target.context.textContent;
                       if ($.trim(content) != "") {
                          return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em">' + content + '</div>';
                     }
                      else
                           e.preventDefault();
                   }
                }).data("kendoTooltip");
        },
        failure: function (data) {
          
            kendo.ui.progress($("#gridGenerateDocument"), false);
        }
    });
        $(document).ready(function () {

            $("#grid").kendoTooltip({
                filter: ".k-grid-edit",
                position: "top",
                content: function (e) {
                    return "Edit";
                }
            });


            $("#grid").kendoTooltip({
                filter: ".k-grid-edit1",
                position: "top",
                content: function (e) {
                    return "Overview";
                }
            });
        })

        $(document).on("click", "#grid tbody tr .k-grid-edit", function (e) {
            var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
            var InstanceID = item.ComplianceInstanceID;
            var ScheduleOnID = item.ScheduledOnID;
            ShowComplianceDialog(InstanceID, ScheduleOnID);
        });
    </script>

</head>
<body>
    <form id="form2" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div id="grid">
        </div>
        <script type="text/javascript" src="../Newjs/bootstrap.min.js"></script>

        <script>
            $(document).ready(function () {
                $(document).tooltip({ selector: '[data-toggle="tooltip"]' });
                if ($(window.parent.document).find("#divMyCompliances").children().first().hasClass("k-loading-mask"))
                    $(window.parent.document).find("#divMyCompliances").children().first().hide();
            });
            function initializeJQueryUI(textBoxID, divID) {
                $("#" + textBoxID).unbind('click');

                $("#" + textBoxID).click(function () {
                    $("#" + divID).toggle("blind", null, 500, function () { });
                });
            }
        </script>
    </form>
</body>
</html>
