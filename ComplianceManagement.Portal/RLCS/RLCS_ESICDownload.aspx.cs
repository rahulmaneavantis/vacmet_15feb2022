﻿using com.VirtuosoITech.ComplianceManagement.Portal.RLCS.RLCSAPISettings;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
	public partial class RLCS_ESICDownload : System.Web.UI.Page
	{
		List<ESICDetails> _esicLst = new List<ESICDetails>();
		private HttpClient _httpClient { set; get; }
		HttpResponseMessage response;

		public RLCS_ESICDownload()
		{
			response = null;
			_httpClient = new HttpClient();
			_httpClient.BaseAddress = new Uri(ConfigurationManager.AppSettings["RLCSAPIURL"]);
			_httpClient.Timeout = new TimeSpan(0, 15, 0);
			_httpClient.DefaultRequestHeaders.Accept.Clear();
			_httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
			_httpClient.DefaultRequestHeaders.Add("Authorization", "5E8E3D45-172D-4D58-8DF8-EB0B0E");
			_httpClient.DefaultRequestHeaders.Add("X-User-Id-1", "f2UKmAm0KlI98bEYGGxEHQ==");
			_httpClient.DefaultRequestHeaders.Add("RLCSWEBREQUEST", "true");
			ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
		}
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!string.IsNullOrEmpty(Request.QueryString["RLCSIds"]))
			{
				string[] commandArg = Request.QueryString["RLCSIds"].ToString().Split(',');
				if (commandArg.Length > 0)
				{
					foreach (var item in commandArg)
					{
						try
						{
							string[] obj = item.ToString().Split('-');

							ESICDetails _esicobj = new ESICDetails()
							{
								Client_ID = obj[0].ToString(),
								SubCode = obj[1].ToString(),
								RequestDate_Month = obj[6].ToString(),
								RequestDate_Year = obj[5].ToString(),
								EmployeeID = obj[3].ToString(),
								ESICNumber = Convert.ToInt64(obj[4])
							};
							_esicLst.Add(_esicobj);
						}
						catch (Exception ex)
						{

						}
					}


					bool r = GetESIC_Documents(_esicLst);
					if (!r)
						ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Something went Wrong');", true);

				}
			}
		}

		public bool GetESIC_Documents(List<ESICDetails> lstObj)
		{
			string contentType = string.Empty;
			string filename = string.Empty;
			bool documentSuccess = true;
			try
			{
				using (ZipFile RLCSDocument = new ZipFile())
				{
					try
					{
						_httpClient.BaseAddress = new Uri(ConfigurationManager.AppSettings["RLCSAPIURL"].ToString());
						foreach (var obj in lstObj)
						{
							string month_year = obj.RequestDate_Month + "_" + obj.RequestDate_Year;
							string baseAddress = ConfigurationManager.AppSettings["ESICFilePath"].ToString();
							string empId_ESICNumber = obj.EmployeeID + "_" + obj.ESICNumber.ToString();
							filename = obj.Client_ID + "/" + month_year + "/" + obj.SubCode + "/" + empId_ESICNumber+ ".pdf";
							string filePath = baseAddress + obj.Client_ID + "/" + month_year + "/" + obj.SubCode + "/" + empId_ESICNumber + ".pdf";
							
							
							response = _httpClient.GetAsync("Masters/Generate?FilePath=" + filePath).Result;

							var statuscode = Convert.ToInt32(response.StatusCode);
							if (statuscode == 200)
							{
								var byteFile = response.Content.ReadAsByteArrayAsync().Result;
								if (byteFile.Length > 0)
								{

									string extension = Path.GetExtension(filePath);
									contentType = "application/" + extension.ToString().Replace(".", "").ToLower() + "";
									//filename = "MIS" + extension;
									RLCSDocument.AddEntry(filename, byteFile);
								}
							}
						}
					}
					catch (Exception ex)
					{
						documentSuccess = false;
					}
					if (documentSuccess)
					{
						HttpResponse res = HttpContext.Current.Response;
						var zipMs = new MemoryStream();
						WebClient req = new WebClient();
						RLCSDocument.Save(zipMs);
						zipMs.Position = 0;

						byte[] Filedata = zipMs.ToArray();
						res.Buffer = true;
						res.ClearContent();
						res.ClearHeaders();
						res.Clear();
						res.ContentType = "application/zip";
						res.AddHeader("content-disposition", "attachment; filename=" + "ESIC" + "-Documents-" + DateTime.Now.ToString("ddMMyy") + ".zip");
						res.BinaryWrite(Filedata);
						HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
						HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
						HttpContext.Current.ApplicationInstance.CompleteRequest();
					}
				}

			}
			catch (Exception ex)
			{ documentSuccess = false; }
			return documentSuccess;
		}
	}
	public class ESICDetails
	{
		public string Client_ID { get; set; }
		public string SubCode { get; set; }
		public string RequestDate_Month { get; set; }
		public string RequestDate_Year { get; set; }
		public string EmployeeID { get; set; }
		public DateTime DOJ { get; set; }
		public long ESICNumber { get; set; }
		public DateTime RequestDate { get; set; }
	}
}