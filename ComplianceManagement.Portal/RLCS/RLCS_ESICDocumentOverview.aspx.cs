﻿using GleamTech.DocumentUltimate.Web;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.Spreadsheet;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
	public partial class RLCS_ESICDocumentOverview : System.Web.UI.Page
	{
		protected static string Client_ID;
		protected static string SubCode;
		protected static string EmployeeID;
		protected static DateTime DOJ;
		protected static long ESICNumber;
		protected static DateTime RequestDate;
		private HttpClient _httpClient { set; get; }
		HttpResponseMessage response;
		public RLCS_ESICDocumentOverview()
		{
			response = null;
			_httpClient = new HttpClient();
			_httpClient.BaseAddress = new Uri(ConfigurationManager.AppSettings["RLCSAPIURL"]);
			_httpClient.Timeout = new TimeSpan(0, 15, 0);
			_httpClient.DefaultRequestHeaders.Accept.Clear();
			_httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
			_httpClient.DefaultRequestHeaders.Add("Authorization", "5E8E3D45-172D-4D58-8DF8-EB0B0E");
			_httpClient.DefaultRequestHeaders.Add("X-User-Id-1", "f2UKmAm0KlI98bEYGGxEHQ==");
			_httpClient.DefaultRequestHeaders.Add("RLCSWEBREQUEST", "true");
			ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
		}
		protected void Page_Load(object sender, EventArgs e)
		{

			if (!string.IsNullOrEmpty(Request.QueryString["ClientID"]) && !string.IsNullOrEmpty(Request.QueryString["SubCode"]) && !string.IsNullOrEmpty(Request.QueryString["EmployeeID"]) && !string.IsNullOrEmpty(Request.QueryString["DOJ"]) && !string.IsNullOrEmpty(Request.QueryString["ESICNumber"]) && !string.IsNullOrEmpty(Request.QueryString["RequestDate"]))
			{
				try
				{
					Client_ID = Request.QueryString["ClientID"];
					SubCode = Request.QueryString["SubCode"];
					EmployeeID = Request.QueryString["EmployeeID"];
					DOJ = Convert.ToDateTime(Request.QueryString["DOJ"]);
					RequestDate = Convert.ToDateTime(Request.QueryString["RequestDate"]);
					ESICNumber = Convert.ToInt64(Request.QueryString["ESICNumber"].ToString());
					int month = RequestDate.Month;
					int year = RequestDate.Year;
					string month_year = month + "_" + year;
					string baseAddress = ConfigurationManager.AppSettings["ESICFilePath"].ToString();
					string empId_ESICNumber = EmployeeID + "_" + ESICNumber.ToString();
					string filePath = baseAddress + Client_ID + "/" + month_year + "/" + SubCode + "/" + empId_ESICNumber + ".pdf";

					string Folder = "~/TempFiles/RLCS";
					string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
					string DateFolder = Folder + "/" + File;
					filePath = filePath.Replace("|", "'");
					string extension = System.IO.Path.GetExtension(filePath);
					if (extension != ".zip")
					{
						if (filePath != "")
						{
							response = _httpClient.GetAsync("Masters/Generate?FilePath=" + filePath).Result;
							var statuscode = Convert.ToInt32(response.StatusCode);
							if (statuscode == 200)
							{
								var byteFile = response.Content.ReadAsByteArrayAsync().Result;
								if (byteFile.Length > 0)
								{
									if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
									{
										//DocumentInfo _info = new DocumentInfo("TempFileName");
										//DocumentViewer1.DocumentSource = new DocumentSource(_info,byteFile);
										//DocumentViewer1.DocumentSource = new DocumentSource();
										DocumentViewer1.Document = ConfigurationManager.AppSettings["DriveUrl"] + "/" + Convert.ToString(filePath);
									}
									else
									{
										 
										   DocumentInfo _info = new DocumentInfo(empId_ESICNumber+ ".pdf", empId_ESICNumber+ ".pdf");
										DocumentViewer1.DocumentSource = new DocumentSource(_info, byteFile);
										//DocumentViewer1.Document = Convert.ToString(filePath);
									}
								}
							}
							else
							{
								lblDocuments.Text = "Sorry, No document available to preview.";
								DocumentViewer1.Visible = false;
							}
						}
						else
						{
							lblDocuments.Text = "Sorry, No document available to preview.";
							DocumentViewer1.Visible = false;
						}

					}
					else
					{
						lblDocuments.Text = "Zip file cannot be viewed here. Please use the download option.";
						DocumentViewer1.Visible = false;
					}

				}
				catch (Exception ex)
				{ }
			}
			else
			{
				lblDocuments.Text = "Sorry, No document available to preview.";
				DocumentViewer1.Visible = false;
			}

		}
		//public void ByteArraytoFile(byte[] byteFile)
		//{
		//	//Read the uploaded File as Byte Array from FileUpload control.
		//	BinaryReader br = new BinaryReader(byteFile);
		//	byte[] bytes = br.ReadBytes((Int32)fs.Length);

		//	//Save the Byte Array as File.
		//	string filePath = "~/Files/" + Path.GetFileName(FileUpload1.FileName);
		//	File.WriteAllBytes(Server.MapPath(filePath), bytes);

		//}
	}
}