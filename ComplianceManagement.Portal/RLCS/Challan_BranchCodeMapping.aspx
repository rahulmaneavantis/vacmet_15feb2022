﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Challan_BranchCodeMapping.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.Challan_BranchCodeMapping" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title></title>
	<link href="../NewCSS/stylenew.css" rel="stylesheet" />
	<link href="~/NewCSS/contract_custom_style.css" rel="stylesheet" />

	<link href="../NewCSS/bootstrap.min.css" rel="stylesheet" />
	<link href="../NewCSS/bootstrap-theme.css" rel="stylesheet" />

	<script type="text/javascript" src="../Newjs/jquery-1.8.3.min.js"></script>

	<link href="/NewCSS/kendo.common.min.css" rel="stylesheet" />
	<link href="/NewCSS/kendo.rtl.min.css" rel="stylesheet" />
	<link href="/NewCSS/kendo.silver.min.css" rel="stylesheet" />
	<link href="/NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />
	<script type="text/javascript" src="/Newjs/kendo.all.min.js"></script>

	<script src="/Newjs/bootstrap-datepicker.min.js"></script>
	<link href="/NewCSS/bootstrap-datepicker.min.css" rel="stylesheet" />

	<style type="text/css">
		#btnSearch1 {
			margin-top: 20px;
		}

		#gridBranchMappingDetails {
			table-layout: fixed;
			text-overflow: ellipsis;
			width: 100%;
			overflow: hidden;
			white-space: nowrap;
		}
	</style>

<%--	<script>
		$(document).ready(function () {
			InitializeBranchesFilter();
			HideTreeViewFilter();

		});
		function InitializeBranchesFilter() {
			initializeJQueryUI("<%=CtbxFilterLocation.ClientID %>", 'CdivFilterLocation');
		}

		function HideTreeViewFilter() {
			$("<%=CtbxFilterLocation.ClientID %>").hide("blind", null, 500, function () { });
		}

		function initializeJQueryUI(textBoxID, divID) {
			$("#" + textBoxID).unbind('click');

			$("#" + textBoxID).click(function () {
				$("#" + divID).toggle("blind", null, 500, function () { });
			});
		}
	</script>--%>
</head>
<body>
	<form id="form1" runat="server">
		<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
		<div style="margin-left:50px">
			<%--<asp:UpdatePanel ID="upCCustomers" runat="server" OnLoad="upCCustomers_Load">
				<ContentTemplate>--%>
					<div class="col-xs-1 col-sm-1 col-md-1" style="padding-left: 0px; width: 20%">
						<label for="divddlCustomers" class="control-label" hidden="hidden">Customer</label>&nbsp;                       
                                <asp:DropDownListChosen runat="server" ID="divddlCustomers" class="form-control" Width="100%" OnSelectedIndexChanged="divddlCustomer_SelectedIndexChanged" AutoPostBack="true">
								</asp:DropDownListChosen>
					</div>
					<%--<div class="col-xs-1 col-sm-1 col-md-1" style="padding-left: 0px; width: 20%">
						<label for="CtbxFilterLocation" class="control-label" hidden="hidden">Entity/ Branch</label>&nbsp;                       
                <asp:TextBox runat="server" ID="CtbxFilterLocation" Style="padding: 0px; padding-left: 10px; margin: 0px; height: 32px; width: 100%; border: 1px solid #c7c7cc; border-radius: 4px; color: #8e8e93" CssClass="txtbox" autocomplete="off" />
						<div id="CdivFilterLocation" style="position: absolute; z-index: 10; overflow-y: auto; background: white; border: 1px solid rgb(195, 185, 185); height: 200px; width: 100%; display: block;">
							<asp:TreeView runat="server" ID="CtvFilterLocation" SelectedNodeStyle-Font-Bold="true" Width="100%" CssClass="tvFilterLocation" NodeStyle-ForeColor="#8e8e93"
								Style="overflow: auto; margin-top: -20px; background-color: #ffffff; color: #8e8e93 !important; max-height: 200px;" ShowLines="true"
								OnSelectedNodeChanged="CtvFilterLocation_SelectedNodeChanged">
							</asp:TreeView>
						</div>

					</div>--%>
					<div class="col-xs-1 col-sm-1 col-md-1" style="padding-left: 0px; width: 20%">
						<label for="ddlType" class="control-label" hidden="hidden">Type</label>&nbsp;                       
                                <asp:DropDownListChosen runat="server" ID="clientDropDown" class="form-control" Width="100%" OnSelectedIndexChanged="clientDropDown_SelectedNodeChanged"  AutoPostBack="true">
								</asp:DropDownListChosen>
					</div>
					<div class="col-xs-1 col-sm-1 col-md-1" style="padding-left: 0px; width: 20%">
						<label for="ddlType" class="control-label" hidden="hidden">Type</label>&nbsp;                       
                                <asp:DropDownListChosen runat="server" ID="dropDownChallanType" class="form-control" Width="100%" OnSelectedIndexChanged="dropDownChallanType_SelectedIndexChanged"  AutoPostBack="true">
									<asp:ListItem Text="Select Type" Value="-1" />
									<asp:ListItem Text="ESIC" Value="ESI" />
									<asp:ListItem Text="PF" Value="EPF" />
									<asp:ListItem Text="PT" Value="PT" />
								</asp:DropDownListChosen>
					</div>

					<div class="col-xs-1 col-sm-1 col-md-1" style="padding-left: 0px; width: 20%">
						<label for="ddlType" class="control-label" hidden="hidden">Code</label>&nbsp;                       
                                <asp:DropDownListChosen runat="server" ID="DropDownListCode" class="form-control" Width="100%" OnSelectedIndexChanged="DropDownListCode_SelectedIndexChanged" AutoPostBack="true">
								</asp:DropDownListChosen>
					</div>
					<div>
						<asp:Button ID="btnSearch1" CausesValidation="false" class="btn btn-primary" Font-Size="15px" runat="server" Text="Apply" OnClick="bindGridonApplyclick" Enabled="false" />
					</div>

					<div class="row col-md-12">
						<asp:GridView runat="server" ID="gridBranchMappingDetails" AutoGenerateColumns="false" GridLines="None"
							CssClass="table" ShowHeaderWhenEmpty="true">
							<Columns>
								<asp:TemplateField HeaderText="Sr.No">
									<HeaderStyle Width="100" />
									<ItemTemplate>
										<asp:Label ID="lblSrNo" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("SerialNo") %>' ToolTip='<%# Eval("SerialNo") %>'></asp:Label>
									</ItemTemplate>
								</asp:TemplateField>

								<asp:TemplateField HeaderText="BranchName">
									<ItemTemplate>
										<asp:Label ID="lblBranchName" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("BranchName") %>' ToolTip='<%# Eval("BranchName") %>'></asp:Label>
									</ItemTemplate>
								</asp:TemplateField>
							</Columns>

							<RowStyle CssClass="clsROWgrid" />
							<HeaderStyle CssClass="clsheadergrid" />
							<PagerTemplate>
								<table style="display: none">
									<tr>
										<td>
											<asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
										</td>
									</tr>
								</table>
							</PagerTemplate>

							<EmptyDataTemplate>
								No Records Found.                       
                       
							</EmptyDataTemplate>
						</asp:GridView>
					</div>
			<%--	</ContentTemplate>
			</asp:UpdatePanel>--%>


		</div>
	</form>
</body>
</html>
