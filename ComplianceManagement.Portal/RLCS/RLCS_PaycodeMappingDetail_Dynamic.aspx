﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RLCS_PaycodeMappingDetail_Dynamic.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.RLCS_PaycodeMappingDetail_Dynamic" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <!-- Bootstrap CSS -->
    <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap theme -->
    <link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <!-- font icon -->
    <link href="~/NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <!--external css-->
    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/contract_custom_style.css" rel="stylesheet" />
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="/Newjs/jquery.js"></script>
    <script type="text/javascript" src="/Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="/Newjs/bootstrap.min.js"></script>

    <link href="/NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="/NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="/NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="/NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="/Newjs/kendo.all.min.js"></script>
    <script>
        function LoaderShow() {
            kendo.ui.progress($("#gvPayCode"), true);
            $('#btnUpdate').addClass("hidden");
            $('#btnSave').addClass("hidden");
        }
        function hideuploadbtn() {
            $('#btnUploadHeader').addClass("hidden");
            $('html, body').animate({
                scrollTop: $("body").offset().top
            }, 500);
        }
        function showuploadbtn() {
            $('#btnUploadHeader').removeClass("hidden");
        }
        //btnSave        
        function LoaderShow1() {
            kendo.ui.progress($("#gvPayCode"), true);
            $('#btnSave').addClass("hidden");

        }
        
        function LoaderAdd() {
           
            kendo.ui.progress($("#gvPayCode"), true);
            $('html, body').animate({
                scrollTop: $("body").offset().top
            }, 500);
        }
        function LoaderAdd1() {
            $("#gvPayCode_btnAdd").click();           
            ///
        }
        function LoaderAddRemove() {            
            kendo.ui.progress($("#gvPayCode"), false);
            $("html, body").animate({ scrollTop: $(document).height() }, 100);
        }
        function HideLabel() {
            setTimeout(function () {
                if (window.parent.$("#divRLCSPaycodeAddDialog") != null && window.parent.$("#divRLCSPaycodeAddDialog") != undefined)
                    document.getElementById("<%=lblMessage.ClientID %>").style.display = "none";
                kendo.ui.progress($("#gvPayCode"), false);
                $('#btnUpdate').removeClass("hidden"); $('#btnSave').removeClass("hidden");
                window.parent.$("#divRLCSPaycodeAddDialog").data("kendoWindow").close();
                e.preventDefault();

            }, 3000);
        }
        function numeric(e) {
            var unicode = e.charCode ? e.charCode : e.keyCode;
            if (unicode == 8 || unicode == 9 || (unicode >= 48 && unicode <= 57)) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
    <script>


        $(document).ready(function () {
            $('#divRLCSHeaderUploadDialogBulk').hide();           

        });
        function OpenHeaderPopup() {
            $("#hiddenData").click();
            $('#divRLCSHeaderUploadDialogBulk').show();
            var myWindowAdv = $("#divRLCSHeaderUploadDialogBulk");
            myWindowAdv.kendoWindow({
                width: "75%",
                height: '55%',

                content: "../RLCS/RLCS_PayCodeHeaderBulkUpload.aspx",
                iframe: true,
                title: "Upload",
                visible: false,
                actions: [
                    //"Pin",
                    "Close"
                ],
                close: onClose,
                open: onOpen,
                scrollable: false
            });

            myWindowAdv.data("kendoWindow").center().open();
            return false;
        }


        function onOpen(e) {
            kendo.ui.progress(e.sender.element, false);

        }

        function onClose() {
            debugger;
            var btnExcel = $('#btnExcel');
            btnExcel.click();
            //$('#gvPayCode').data('kendoGrid').dataSource.read();

        }

        $(document).on("click", "#btnRemove", function (e) {
            //debugger
            //var item = $("#gridPaycode").data("kendoGrid").dataItem($(this).closest("tr"));
            //var recordID = item.ID;
            //var clientID = item.CPMD_ClientID;
            //if (item != null && item != undefined) {

            //    window.kendoCustomConfirm("Are you certain you want to delete this Paycode?", "Confirm").then(function () {
            //        debugger
            //        DeletePaycodeDetails(item)
            //    }, function () {
            //        // For Cancel
            //    });
            //}
        }
       );

    </script>
    <script type="text/javascript">
        $(function () {
            $("#<%= btnSave.ClientID %>").click(function (event) {
            //event.preventDefault();
            kendo.ui.progress($("#gvPayCode"), true);
            $('#btnSave').addClass("hidden");
            //$('html, body').animate({
            //    scrollTop: $("body").offset().top
            //}, 500);
        });
    });

    </script>
    <style>
        .form-control {
            margin-bottom: 8px;
            height: 31px;
            font-family: sans-serif;
        }

        .button, input, select, textarea {
            border: none;
        }

        .m-10 {
            padding-left: 24px;
        }

        html, body {
            overflow-x: hidden;
        }

        .k-loading-image {
            /*margin-top: 0px;*/
            left: 0;
            z-index: 2;
        }

        table {
            max-width: 100%;
            margin-top: 30px;
        }

        .AddNewspan {
            padding: 2px;
            border: 2px solid #f4f0f0;
            border-radius: 51px;
            display: inline-block;
            height: 30px;
            width: 30px;
        }

        .navbar-fixed-bottom, .navbar-fixed-top, .navbar-static-top {
            border-radius: 0;
            margin-left: 102px;
            margin-top: 41px;
            width: 83%;
            /*background-color: #1fd9e1;*/
            background-color: #e9eaea;
        }

        table tr th {
            color: #666;
            font-size: 15px;
            /* align-items: center; */
            text-align: -webkit-center;
        }
    </style>
     <script type="text/javascript">
         $(function () {
             var flag = "";
            $(document).on('click', '#gvPayCode_btnAdd', function () {                
                var trs = $('[id*=gvPayCode]').closest('tr:not(":has(th)")');
                debugger;                
                for (var i = 0; i < trs.length-1; i++) {
                    var Header = $("#gvPayCode_txtHeader_" + i).val();
                    var ddlType = $("#gvPayCode_ddlPaycodeType_" + i).val();
                    var ddlPaycode = $("#gvPayCode_ddlPaycode_" + i).val();                    
                    var SeqNo = $("#gvPayCode_txtSquanceOrder_" + i).val(); 
                    if (Header == "" || ddlType == "-1" || ddlPaycode == "-1" || SeqNo == "") {                       
                        flag = "error";
                        LoaderAddRemove();
                        return false;
                    }
                   //array
                }
                if (flag == "") {
                        kendo.ui.progress($("#gvPayCode"), true);
                         $('html, body').animate({
                         scrollTop: $("body").offset().top
                         }, 100);
                }
            });
        });
    </script>
</head>

<body style="background-color: white;">

    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

        <div class="col-xs-12 col-sm-12 col-md-12 colpadding0">
            <div class="col-md-4">
                <asp:DropDownList runat="server" ID="ddlClientList" Style="padding: 0px; margin: 0px;"
                    CssClass="form-control" NoResultsText="No results match." />
            </div>
            <div class="col-md-4"></div>
            <div class="col-md-2">
                <asp:Button ID="btnExcel" runat="server" OnClick="btnExcel_Click" Style="display: none" />
            </div>
            <div class="col-md-2" id="divBtnUploadHeader" runat="server">
                <button id="btnUploadHeader" class="btn btn-primary" runat="server" onclick="return OpenHeaderPopup();"><span class="k-icon k-i-upload"></span>Upload</button>
            </div>


            <%--<table>
                <tr>
                    <th style="width:100px; margin:10px" >
                       Header Name
                    </th>

                    <th>
                       Paycode Type	
                    </th>
                    <th>
                       Paycode
                    </th>
                    <th>
                       Squance<br /> Order
                    </th>
                    <th>
                       ESI<br /> Applicable	
                    </th>
                    <th>
                       PF<br /> Applicable	
                    </th>
                    <th>
                       PT<br /> Applicable	
                    </th>
                    <th>
                       LWF <br />Applicable
                    </th>
                </tr>
            </table>--%>
        </div>
        <div class="row">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <asp:HiddenField ID="hdnbtnSave" runat="server" Value="" />
                    <div class="form-group clearfix"></div>

                    <div style="width: 100%; margin-bottom: 4px">
                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                            ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                        <asp:ValidationSummary ID="vsDocGen" runat="server" class="alert alert-block alert-danger fade in" ValidationGroup="DocGenValidationGroup" />

                        <asp:CustomValidator ID="cvDuplicate" runat="server" EnableClientScript="False"
                            ValidationGroup="DocGenValidationGroup" Display="None" />
                    </div>


                    <div class="row " style="width: 90%; margin-left: 30px; height: 30px; margin-bottom: 30px; position: fixed">
                        <div class="col-lg-12 col-md-12 col-sm-12" style="margin: -35px;">
                            <asp:Label ID="lblMessage" Font-Size="15px" Font-Bold="true" runat="server" Visible="false"></asp:Label>
                            <asp:Label ID="lblPaycodeHeader" Font-Size="15px" CssClass="form-control" Font-Bold="true" runat="server" Visible="false"></asp:Label>
                            <asp:Label ID="lblPaycodeSequence" Font-Size="15px" Font-Bold="true" runat="server" Visible="false"></asp:Label>
                            <asp:Label ID="lblPaycodeType" Font-Size="15px" CssClass="form-control" Font-Bold="true" runat="server" Visible="false"></asp:Label>
                            <asp:Label ID="lblPaycodeAndType" Font-Size="15px" Font-Bold="true" runat="server" Visible="false"></asp:Label>
                            <asp:Label ID="lblHeader" Font-Size="15px" Font-Bold="true" runat="server" Visible="false"></asp:Label>
                        </div>
                    </div>
                    <div class="form-group clearfix"></div>


                    <asp:GridView Width="82.8%" Height="70%" ID="gvPayCode" HorizontalAlign="Center" runat="server" DataKeyNames="ID" ShowFooter="true" AutoGenerateColumns="false" OnRowCreated="gvPayCode_RowCreated">
                        <Columns>
                            <asp:BoundField DataField="ID" Visible="false" HeaderText="ID" />
                            <asp:TemplateField HeaderText="Header Name" ItemStyle-Width="50px" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:RequiredFieldValidator ID="ddlHeaderRequired" runat="server" ControlToValidate="txtHeader" ForeColor="Red" ValidationGroup="valid1" InitialValue="" ErrorMessage="Select Header.">
                                    </asp:RequiredFieldValidator>
                                    <asp:TextBox ID="txtHeader" CssClass="form-control" Width="182px" OnTextChanged="txtHeader_TextChanged" AutoPostBack="true" runat="server"></asp:TextBox>

                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" Width="190px" />
                                <ItemStyle HorizontalAlign="Center" Width="182px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Paycode Type">
                                <ItemTemplate>
                                    <asp:RequiredFieldValidator ID="ddlPaycodeRequired" runat="server" ControlToValidate="ddlPaycodeType" ForeColor="Red" ValidationGroup="valid1" InitialValue="-1" ErrorMessage="Select Paycode type.">
                                    </asp:RequiredFieldValidator>
                                    <asp:DropDownList ID="ddlPaycodeType" CssClass="form-control" runat="server" Width="195px"
                                        AppendDataBoundItems="true" OnSelectedIndexChanged="ddlPaycodeType_SelectedIndexChanged" AutoPostBack="true">
                                        <asp:ListItem Value="-1">Select</asp:ListItem>
                                    </asp:DropDownList>

                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" Width="204px" />
                                <ItemStyle HorizontalAlign="Center" Width="196px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Paycode">
                                <ItemTemplate>
                                    <asp:RequiredFieldValidator ID="ddlPaycodeRequired1" runat="server" ControlToValidate="ddlPaycode" ValidationGroup="valid1" ForeColor="Red" InitialValue="-1" ErrorMessage="Please select Paycode.">
                                    </asp:RequiredFieldValidator>
                                    <asp:DropDownList ID="ddlPaycode" OnSelectedIndexChanged="ddlPaycode_SelectedIndexChanged" AutoPostBack="true" CssClass="form-control" runat="server" Width="190px">
                                        <asp:ListItem Value="-1">Select</asp:ListItem>
                                    </asp:DropDownList>

                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" Width="198px" />
                                <ItemStyle HorizontalAlign="Center" Width="190px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Squance Order">

                                <ItemTemplate>
                                    <asp:RequiredFieldValidator ID="txtSquanceOrderRequired" runat="server" ControlToValidate="txtSquanceOrder" ValidationGroup="valid1" ForeColor="Red" InitialValue="" ErrorMessage="*Required">
                                    </asp:RequiredFieldValidator>
                                    <asp:TextBox ID="txtSquanceOrder" CssClass="form-control" Width="85px" runat="server" MaxLength="5" onkeypress="return numeric(event);" OnTextChanged="txtSquanceOrder_TextChanged" AutoPostBack="true"></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" Width="93px" />
                                <ItemStyle HorizontalAlign="Center" Width="85px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ESI Applicable">
                                <ItemTemplate>
                                    <asp:CheckBox ID="CheckboxESI" runat="server" AppendDataBoundItems="true" />
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" Width="50px" />
                                <ItemStyle HorizontalAlign="Center" Width="50px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="PF Applicable">
                                <ItemTemplate>
                                    <asp:CheckBox ID="CheckboxPF" runat="server"  AppendDataBoundItems="true" />
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" Width="50px" />
                                <ItemStyle HorizontalAlign="Center" Width="50px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="PT Applicable">
                                <ItemTemplate>
                                    <asp:CheckBox ID="CheckboxPT" runat="server" AppendDataBoundItems="true" />
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" Width="50px" />
                                <ItemStyle HorizontalAlign="Center" Width="50px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="LWF Applicable">
                                <ItemTemplate>
                                    <asp:CheckBox ID="CheckboxLWF" runat="server"  AppendDataBoundItems="true" />
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" Width="50px" />
                                <ItemStyle HorizontalAlign="Center" Width="50px" />

                                <FooterStyle HorizontalAlign="Center" Width="50px" />
                                <FooterTemplate>
                                    <asp:ImageButton ID="btnAdd" class="btn btn-primary AddNewspan"  ValidationGroup="valid1" runat="server" ImageUrl="~/images/add.png" BackColor="White" ToolTip="Add New" OnClick="btnAdd_Click" style="display:none"/>
                                    <asp:ImageButton ID="hdnadd" class="btn btn-primary AddNewspan" OnClientClick="LoaderAdd1(); return false;"  runat="server" ImageUrl="~/images/add.png" BackColor="White" ToolTip="Add New" />
                                </FooterTemplate>
                            </asp:TemplateField>
                            <%--<asp:TemplateField>
                            <ItemTemplate>
                                <asp:Button ID="btnRemove" class="btn btn-primary" runat="server" Text="-" OnClick="btnRemove_Click" Style="width:40px;" />
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="50px" />
                            <ItemStyle HorizontalAlign="Center" Width="50px" />
                        </asp:TemplateField>--%>
                        </Columns>
                    </asp:GridView>

                    <div class="col-xs-12 col-sm-12 col-md-12 colpadding0" style="margin-left: 1040px; width: 50px; margin-top: 10px; align-items: flex-end">
                        <div class="chart-loading" id="KendoLoader"></div>
                        <asp:Button ID="btnSave" ValidationGroup="valid1" runat="server" OnClientClick="LoaderAdd(); return true;" class="btn btn-primary" Text="Save" OnClick="btnSave_Click" />
                        <asp:Button ID="btnUpdate" runat="server" class="btn btn-primary" Text="Save" OnClick="btnUpdate_Click" Visible="false" />
                        
                    </div>
                    </div>
                <asp:Button type="button" class="btn btn-primary" ID="hiddenData" Style="display: none" OnClick="btnHidden_Click" runat="server"></asp:Button>
                    <asp:Button ID="btnhdnCall" runat="server" class="btn btn-primary" Text="call" Style="display: none" OnClick="btnhdnCall_Click" Visible="false" />
                </ContentTemplate>
                <Triggers>
                    <%--<asp:PostBackTrigger ControlID="btnSave" />--%>
                    <%--<asp:PostBackTrigger ControlID="btnExcel" />--%>
                    <asp:AsyncPostBackTrigger ControlID="hiddenData" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="btnExcel" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
        <div id="divRLCSHeaderUploadDialogBulk">
            <iframe id="iframeUploadBulk" style="width: 878px; height: 500px; border: none"></iframe>
        </div>
    </form>
</body>

</html>
