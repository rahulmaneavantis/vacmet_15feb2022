﻿<%@ Page Title="Masters :: Leave Type Master" Language="C#" MasterPageFile="~/HRPlusCompliance.Master" AutoEventWireup="true" CodeBehind="RLCS_LeaveType_Master.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.Masters.RLCS_LeaveType_Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

 <link href="../NewCSS/kendo_V1.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo_V1.silver.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../Newjs/jszip.min.js"></script>

    <link href="../NewCSS/Kendouicss_V1.css" rel="stylesheet" />

    <title></title>

 
   
     <style type="text/css">

         .k-textbox>input{
             text-align:left;
         }

         .btn {
         font-size:14px;
         }

         .k-i-filter-clear{
            margin-top: -2px;
            margin-right: 3px;
        }
         
          label.k-label:hover{
              color:#1fd9e1;
          }
         .k-filter-row > th:first-child, .k-grid tbody td:first-child, .k-grid tfoot td:first-child, .k-grid-header th.k-header:first-child {
    border-left-width: 0;
}
             .k-grid-content {
            min-height: auto !important;
        }
        .k-grid tbody .k-button {
            min-width: 25px;
            min-height: 25px;
            background-color: transparent;
            border: none;
            margin-left: 0px;
            margin-right: -7px;
            padding-left: 0px;
            padding-right: 15px;
        }
            .panel-heading .nav > li > a {
            font-size: 16px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }
            .panel-heading .nav > li > a {
            font-size: 16px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }
             .panel-heading .nav > li:hover {
            color: white;
            background-color: #1fd9e1;
            border-top-left-radius: 10px;
            border-top-right-radius: 10px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }
               .panel-heading .nav > li {
            margin-left: 5px !important;
            margin-right: 5px !important;
        }
         .panel-heading .nav {
            background-color: #f8f8f8;
            border: none;
            font-size: 11px;
            margin: 0px 0px 0px 0;
            border-radius: 10px;
        }
           .panel-heading .nav .active, .panel-heading .nav > li.active > a, .panel-heading .nav > li > a:hover {
            color: white;
            background-color: #1fd9e1;
            border-top-left-radius: 10px;
            border-top-right-radius: 10px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }
           .k-grid, .k-listview {
               margin-top: 10px;
           }
        .k-list > .k-state-focused.k-state-selected, .k-listview > .k-state-focused.k-state-selected, .k-state-focused.k-state-selected, td.k-state-focused.k-state-selected {
            -webkit-box-shadow: inset 0 0 3px 1px rgba(0, 0, 0, 0.5);
            
            box-shadow: inset 0 0 3px 1px rgba(0, 0, 0, 0.5);
        }
        
              .k-tooltip-content{
         width: max-content;
        
         }
        input[type=checkbox], input[type=radio] {
            margin: 4px 6px 0px -3px;
            line-height: normal;
        }
        .k-calendar-container {
    background-color: white;
    width: 217px;
}
        .div.k-grid-footer, div.k-grid-header {
            border-top-width: 1px;
            margin-right: 0px;
            margin-top:-7px;
        }


        .k-grid-footer-wrap, .k-grid-header-wrap {
            position: relative;
            width: 100%;
            overflow: hidden;
            border-style: solid;
            border-width: 0 1px 0 0;
            zoom: 1;
        }

       
        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 2.0em;
            border-bottom-width: 1px;
            background-color: white;
            border-width: 0 1px 1px 0px;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
            background-color: white;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: white;
        }

        #grid .k-grid-toolbar {
            background: white;
        }
        .k-dropdown .k-input, .k-dropdown .k-state-focused .k-input, .k-menu .k-popup, .k-multiselect .k-button, .k-multiselect .k-button:hover {
    color: #515967;
    padding-top: 5px;
}
        .k-pager-wrap > .k-link > .k-icon {
            margin-top: -1px;
            margin-left:2.7px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: white;
        }

        html .k-grid tr.k-alt:hover {
            background: white;
        }


        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
            margin-right: 2px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: white;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 0px 1px 0px;
            text-align: center;
        }

        .k-grid-pager {
            margin-top: 0px;
            border-width:0px 0px 0px 0px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            border-width: 0px 0px 1px 0px;
            background: #E9EAEA;
            font-weight: bold;
            margin-right: 18px;
            font-family: 'Roboto',sans-serif;
            font-size: 15px;
            color: rgba(0, 0, 0, 0.5);
            height: 20px;
            vertical-align: middle;
        }

        .k-dropdown-wrap.k-state-default {
            background-color: white;
            height:34px;
        }
        .k-multiselect-wrap, .k-floatwrap{
            height:34px;
        }
        .k-popup.k-calendar-container, .k-popup.k-list-container {
            background-color: white;
        }

        .k-grid-header th.k-state-focused, .k-list > .k-state-focused, .k-listview > .k-state-focused, .k-state-focused, td.k-state-focused {
            -webkit-box-shadow: inset 0 0 3px 1px white;
            box-shadow: inset 0 0 3px 1px white;
        }

        label.k-label {
            font-family: roboto,sans-serif !important;
            color: #515967;
            /* font-stretch: 100%; */
            font-style: normal;
            font-weight: 400;
            min-width: max-content !important;
            white-space: pre-wrap;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: flex;
            margin-bottom: 0px;
        }

        .k-state-default > .k-select {
            border-color: #ceced2;
            margin-top: 3px;
        }

        .k-grid-norecords {
            width: 100%;
            height: 100%;
            text-align: center;
            margin-top: 2%;
        }

        .k-grouping-header {
            font-style: italic;
            background-color: white;
        }

        .k-grid-toolbar {
            background: white;
            border: none;
        }

        .k-grid table {
            width: 100.5%;
        }
    </style>
    <script id="templateTooltip" type="text/x-kendo-template">
        <div>
        <div> #:value ? value : "N/A" #</div>
        </div>
    </script>
    var record=0;
    <script type="text/javascript">

        function BindGrid() {
            var gridview = $("#grid").kendoGrid({
                dataSource: {
                    serverPaging: false,
                    pageSize: 10,
                    transport: {
                        read: {
                            url: '<% =Path%>GetAll_LeaveTypeMaster?CustID=<% =CustId%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },
                        }
                    },
                    batch: true,
                    
                    schema: {
                        data: function (response) {
                            debugger;
                            return response.Result;
                        },
                        total: function (response) {
                            debugger;
                            return response.Result.length;
                        }
                    }
                },
                //toolbar: kendo.template($("#template").html()),
                //height: 471,
                sortable: true,
               
                columnMenu: true,
                pageable: {
                    refresh:false,
                    buttonCount: 3,
                    pageSizes: true,
                    pageSizes:10,
                    change: function (e) {
                        //$('#chkAll').removeAttr('checked');
                        //$('#dvbtndownloadDocumentMain').css('display', 'none');
                    }
                },
                dataBinding: function () {
                    record = 0;
                    var total = this.dataSource._pristineTotal;
                    if (this.dataSource.pageSize() == undefined) {
                        this.dataSource.pageSize(total);
                        record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                    }
                    else {
                        record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                    }
                },
                reorderable: true,
                resizable: true,
                filterable:true,
                multi: true,
                selectable: true,
                columns: [
                    //{
                    //    title: "Sr.No.",
                        
                    //    field: "rowNumber",
                    //    template: "#= ++record #",
                    //    //template: "<span class='row-number'></span>",
                    //    width: "10%;",
                    //    attributes: {
                    //        style: 'white-space: nowrap;text-align:left'
                    //    },
                    //    filterable:false,
                        
                    //},
                    {
                        field: "LeaveType", title: 'Leave Type(Code)',
                        width: "30%;",
                        attributes: {
                            style: 'white-space: nowrap;text-align:left'

                        }, filterable: {
                            extra: false,
                            multi: true,
                            search: true,
                            operators: {
                                string: {
                                    contains: "Contains",
                                    eq: "Is equal to",
                                    neq: "Is not equal to"
                                }
                            }
                        }
                    },
                    {
                        field: "Days", title: 'Deduction (Days)',
                        width: "30%",
                        attributes: {
                            style: 'white-space: nowrap;text-align:left'

                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    contains: "Contains",
                                    eq: "Is equal to",
                                    neq: "Is not equal to"
                                }
                            }
                        }
                    },
                    {
                        field: "Leave_Description", title: 'Description',
                        width: "40%",
                        filterable: {
                            extra: false,
                            multi: true,
                            search: true,
                            operators: {
                                string: {
                                    contains: "Contains",
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                }
                            }
                        }
                    },
                ],
                dataBound: function () {
                    var rows = this.items();
                    $(rows).each(function () {
                        var index = $(this).index() + 1
                            + ($("#grid").data("kendoGrid").dataSource.pageSize() * ($("#grid").data("kendoGrid").dataSource.page() - 1));;
                        var rowLabel = $(this).find(".row-number");
                        $(rowLabel).html(index);
                    });
                }
            });
            $("#grid").kendoTooltip({
                filter: "td:nth-child(1)",
                position: "bottom",
                content: function (e) {
                    var dataItem = $("#grid").data("kendoGrid").dataItem(e.target.closest("tr"));

                    var content = dataItem.LeaveType;
                    return content;
                }
            }).data("kendoTooltip");
            $("#grid").kendoTooltip({
                filter: "td:nth-child(2)",
                position: "bottom",
                content: function (e) {
                    var dataItem = $("#grid").data("kendoGrid").dataItem(e.target.closest("tr"));

                    var content = dataItem.Days;
                    return content;
                }
            }).data("kendoTooltip");
            $("#grid").kendoTooltip({
                filter: "td:nth-child(3)",
                position: "bottom",
                content: function (e) {
                    var dataItem = $("#grid").data("kendoGrid").dataItem(e.target.closest("tr"));

                    var content = dataItem.Leave_Description;
                    return content;
                }
            }).data("kendoTooltip");
        }

        function ClearFilter(e) {
            e.preventDefault();
            $("#txtSearch").val("");

            $('#btnClearFilter').css('display', 'none');
            ApplyFilter(e);
            return false;
        }

        function ApplyFilter(e) {
            e.preventDefault();

            var finalSelectedfilter = { logic: "and", filters: [] };
            var txtSearch = $("#txtSearch").val();
            if (txtSearch) {
                var textFilter = { logic: "or", filters: [] };

                textFilter.filters.push({
                    field: "LeaveType", operator: "contains", value: txtSearch
                });

                textFilter.filters.push({
                    field: "Days", operator: "contains", value: txtSearch
                });

                textFilter.filters.push({
                    field: "Leave_Description", operator: "contains", value: txtSearch
                });

                finalSelectedfilter.filters.push(textFilter);
            }

            if (finalSelectedfilter.filters.length > 0) {
                if ($("#grid").data("kendoGrid") != null && $("#grid").data("kendoGrid") != undefined) {
                    var dataSource = $("#grid").data("kendoGrid").dataSource;
                    dataSource.filter(finalSelectedfilter);
                    $('#btnClearFilter').css('display', 'block');
                }
            } else {
                if ($("#grid").data("kendoGrid") != null && $("#grid").data("kendoGrid") != undefined) {
                    var dataSource = $("#grid").data("kendoGrid").dataSource;
                    dataSource.filter({});
                    $('#btnClearFilter').css('display', 'none');
                }
            }
        }

        $(document).ready(function () {

            $(window).keydown(function(event){
                if(event.keyCode == 13) {
                    event.preventDefault();
                    return false;
                }
            });


            fhead('Masters/ Leave Type');
            $('#btnClearFilter').css('display', 'none');
            BindGrid();
        });
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row colpadding0">
         <div class="col-md-12 colpadding0">
            <header class="panel-heading tab-bg-primary" style="background: none !important; padding: 0px 0px;">
                <ul class="nav nav-tabs">
                    <li>
                        <asp:LinkButton ID="lnkTabStateCity" runat="server" PostBackUrl="~/RLCS/RLCS_Location_Master.aspx">State-City</asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkTabDesignation" runat="server" PostBackUrl="~/RLCS/RLCS_Designation_Master.aspx">Designation</asp:LinkButton>
                    </li>
                    <li class="active">
                        <asp:LinkButton ID="lnkTabLeaveType" runat="server" PostBackUrl="~/RLCS/RLCS_LeaveType_Master.aspx">Leave Type</asp:LinkButton>
                    </li>
                </ul>
            </header>
        </div>

        <div class="col-md-12 colpadding0 toolbar" style="margin-top: 10px;">
        </div>
    </div>

    <div class="row colpadding0">
        <div class="col-md-12 colpadding0 toolbar">
            <div class="col-md-5 colpadding0" style="margin-top:10px">
                <input class="k-textbox" type="text" onkeyup="ApplyFilter(event);"  id="txtSearch" style="width: 70%;height:36px;" placeholder="Type to Search" />
                <button id="btnSearch" class="btn btn-primary" onclick="ApplyFilter(event)"style="height:36px; margin-left:20px; padding-top: 1px;font-weight:400;">Search</button>
            </div>
            <div class="col-md-7 colpadding0">
                <button id="btnClearFilter" class="btn btn-primary" onclick="ClearFilter(event)"style="margin-top:10px;height:36px;margin-left: -29px; padding-top: 3px;font-weight:400;"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear Filter</button>
            </div>
        </div>
    </div>

    <div class="row colpadding0 " style="margin-top: 1%">
        <div class="col-md-12 colpadding0">
            <div id="grid"<%-- style="border: none;"--%>>
                <div class="k-header k-grid-toolbar">
                </div>
            </div>
        </div>
    </div>
</asp:Content>
