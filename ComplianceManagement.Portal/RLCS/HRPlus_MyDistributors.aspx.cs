﻿using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class HRPlus_MyDistributors : System.Web.UI.Page
    {
        protected static int loggedInUserCustID = -1;
        protected static string avacomRLCSAPIURL;
        protected void Page_Load(object sender, EventArgs e)
        {
            loggedInUserCustID = -1;
            loggedInUserCustID = Convert.ToInt32(AuthenticationHelper.CustomerID);

            avacomRLCSAPIURL = ConfigurationManager.AppSettings["AVACOM_RLCS_API_URL"];
        }
    }
}