﻿<%@ Page Title="Compliance-Activate :: Setup HR+" Language="C#" MasterPageFile="~/HRPlusCompliance.Master" AutoEventWireup="true" CodeBehind="RLCS_HRCompliance_Activate_New.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.RLCS_HRCompliance_Activate_New" %>
<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   <script type="text/javascript">
       $(document).on("click", "#<%=tbxFilterLocation.ClientID%>", function (event) {
           
            if (event.target.id == "") {
                var idvid = $(event.target).closest('div');
                if ($(idvid).attr('id') != undefined && $(idvid).attr('id') != '' && $(idvid).attr('id').indexOf('<%=tvFilterLocation.ClientID%>') > -1) {
                    $("#divFilterLocation").show();
                } else {
                    $("#divFilterLocation").hide();
                }
            }else if (event.target.id != "<%=tbxFilterLocation.ClientID%>") {
                $("#divFilterLocation").hide();
            } else if (event.target.id != "" && event.target.id.indexOf('<%=tvFilterLocation.ClientID%>') > -1) {
                $("#divFilterLocation").show();
            } else if (event.target.id == "<%=tbxFilterLocation.ClientID%>") {
                $("#<%=tbxFilterLocation.ClientID%>").unbind('click');

                $("#<%=tbxFilterLocation.ClientID%>").click(function () {
                    $("#divFilterLocation").toggle("blind", null, 500, function () { });
                });
            }
        });

       function initializeJQueryUI(textBoxID, divID) {
           $("#" + textBoxID).unbind('click');

           $("#" + textBoxID).click(function () {
               $("#" + divID).toggle("blind", null, 500, function () { });
           });
       }


        function redirectToEntityBranch() {
            var customerId = $('#<%= ddlCustomer.ClientID %>').val();
            window.location = '/RLCS/RLCS_EntityLocation_Master_New.aspx?CustID=' + customerId.toString();
            return false;
        }

       $(document).ready(function () {
           fhead('Masters/Compliance Activation');

           $('#divReAssignCompliance').hide();
           $("#divFilterLocation").hide();
                      
           var startDate = new Date();
           $('#<%= tbxStartDate.ClientID %>').datepicker({
                  dateFormat: 'dd-mm-yy',
                  numberOfMonths: 1,
                  changeMonth: true,
                  changeYear: true,
           });
        });

        function initializeConfirmDatePicker(date) {
            
            var startDate = new Date();
            $('#<%= tbxStartDate.ClientID %>').datepicker({
                dateFormat: 'dd-mm-yy',
                numberOfMonths: 1,
                changeMonth: true,
                changeYear: true,
            });
            initializeDatePicker();
        }

        function initializeDatePicker(date) {
            var startDate = new Date();
            $(".StartDate").datepicker({
                dateFormat: 'dd-mm-yy',
                setDate: startDate,
                numberOfMonths: 1,
                changeMonth: true,
                changeYear: true,
            });
        }

        function setDate() {
            $(".StartDate").datepicker();
        }
   </script>

    <script type="text/javascript">
       
        function Confirm() {
            
            var a = document.getElementById("ContentPlaceHolder1_tbxStartDate").value;
            if (a != "") {
                var confirm_value = document.createElement("INPUT");
                confirm_value.type = "hidden";
                confirm_value.name = "confirm_value";
                var Date = document.getElementById("<%=tbxStartDate.ClientID %>").value;
                confirm_value.value = "";
                confirm_value
                if (confirm("Activation Date of Selected Compliance is-" + Date + ", Please Click on OK to continue !!")) {
                    return true;
                } else {
                    return false;
                }
            }
            else {
                alert("Start date should not be blank..!");
            }
        };

        function OpenReAssignPopup(ID) {
            
            $('#divReAssignCompliance').show();

            var myWindowAdv = $("#divReAssignCompliance");
            
            myWindowAdv.kendoWindow({
                width: "85%",
                height: '90%',
                //maxHeight: '90%',
                //minHeight:'50%',
                title: "Re-Assign Compliance",
                visible: false,
                actions: ["Close"],                
            });
            
            $('#iframeReAssign').attr('src', 'about:blank');
            myWindowAdv.data("kendoWindow").center().open();
            $('#iframeReAssign').attr('src', "/RLCS/ClientSetup/RLCS_ComplianceReAssign.aspx?CustID=" + ID);
            
            return false;
        }
    </script>   

     <style>
        .panel-heading .nav > li > a {
            font-size: 17px;
        }

        .panel-heading .nav .active, .panel-heading .nav > li.active > a, .panel-heading .nav > li > a:hover {
            color: white;
            background-color: #1fd9e1;
            border-top-left-radius: 10px;
            border-top-right-radius: 10px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }

        .panel-heading .nav > li > a {
            font-size: 16px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }

        .panel-heading .nav {
            background-color: #f8f8f8;
            border: none;
            font-size: 11px;
            margin: 0px 0px 0px 0;
            border-radius: 10px;
        }

          .btn{
            font-weight:400;
            font-size:14px;
        }


        .panel-heading .nav > li {
            margin-left: 5px !important;
            margin-right: 5px !important;
        }

        .panel-heading .nav > li:hover {
            color: white;
            background-color: #1fd9e1;
            border-top-left-radius: 10px;
            border-top-right-radius: 10px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }

        .form-control{
            height: 32px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   <div class="row colpadding0" style="margin-bottom: 10px">
        <div class="col-md-12 colpadding0">
            <header class="panel-heading tab-bg-primary" style="background: none !important; padding: 0px 0px;">
                <ul class="nav nav-tabs">
                    <li>
                        <asp:LinkButton ID="lnkTabCustomer" runat="server" PostBackUrl="~/RLCS/RLCS_CustomerCorporateList_New.aspx">Customer</asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkTabEntityBranch" OnClientClick="return redirectToEntityBranch()" runat="server">Entity-Branch</asp:LinkButton> <%--PostBackUrl="~/RLCS/RLCS_EntityLocation_Master_New.aspx"--%>
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkTabUser" runat="server" PostBackUrl="~/RLCS/RLCS_User_MasterList_New.aspx">User</asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkTabEmployee" runat="server" PostBackUrl="~/RLCS/RLCS_EmployeeMaster_New.aspx">Employee</asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkBtnEntityAssign" PostBackUrl="~/RLCS/RLCS_EntityAssignment_New.aspx" runat="server">Entity-Branch Assignment</asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkBtnComAssign" runat="server" PostBackUrl="~/RLCS/RLCS_HRCompliance_Assign_New.aspx">Compliance Assignment</asp:LinkButton>
                    </li>
                    <li class="active">
                        <asp:LinkButton ID="lnkBtnComAct" runat="server" PostBackUrl="~/RLCS/RLCS_HRCompliance_Activate_New.aspx">Compliance Activation</asp:LinkButton>
                    </li>
                </ul>
            </header>
        </div>
    </div>

    <div class="clearfix"></div>

    <asp:UpdatePanel ID="upComplianceTypeList" runat="server" UpdateMode="Conditional" OnLoad="upComplianceTypeList_Load">
        <ContentTemplate>
            <div class="col-md-12 colpadding0">
                <asp:CompareValidator ControlToValidate="tbxFilterLocation" ErrorMessage="Please select Location."
                    runat="server" ValueToCompare="< Select Location >" Operator="NotEqual" ValidationGroup="ComplianceInstanceValidationGroup"
                    Display="None" />
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-block alert-danger fade in" ValidationGroup="ComplianceInstanceValidationGroup" />
                <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                    ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                <asp:Label ID="lblErrorMessage" runat="server" Style="color: Red"></asp:Label>
            </div>

            <div class="clearfix"></div>

            <div class="row col-md-12 colpadding0">
                <div class="col-md-3 colpadding0">
                    <asp:DropDownListChosen runat="server" ID="ddlCustomer" CssClass="form-control" Width="95%"
                        AutoPostBack="true" AllowSingleDeselect="false" DataPlaceHolder="Select Customer"
                        OnSelectedIndexChanged="ddlCustomer_SelectedIndexChanged">
                    </asp:DropDownListChosen>
                </div>

                <div class="col-md-4 colpadding0">
                    <asp:TextBox runat="server" ID="tbxFilterLocation" autocomplete="off" CssClass="form-control" Width="95%" />
                    <div style="position: absolute;z-index: 50;overflow-y: auto;background: white;border: 1px solid rgb(195, 185, 185);height: 200px;width: 100%;display: block;" id="divFilterLocation">
                        <asp:TreeView runat="server" ID="tvFilterLocation" SelectedNodeStyle-Font-Bold="true" NodeStyle-ForeColor="#8e8e93"
                            Style="overflow: auto; margin-top: -20px; background-color: #ffffff; color: #8e8e93 !important;" ShowLines="true" OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged">
                        </asp:TreeView>                        
                    </div>
                </div>

                <div class="col-md-2 colpadding0">
                    <asp:DropDownListChosen runat="server" ID="ddlScopeType" CssClass="form-control" Width="95%" AllowSingleDeselect="false"
                        AutoPostBack="true" OnSelectedIndexChanged="ddlScopeType_SelectedIndexChanged">
                        <asp:ListItem Text="All" Value="-1"></asp:ListItem>
                        <asp:ListItem Text="Register" Value="SOW03"></asp:ListItem>
                        <asp:ListItem Text="PF Challan" Value="SOW13"></asp:ListItem>
                        <asp:ListItem Text="ESI Challan" Value="SOW14"></asp:ListItem>
                        <asp:ListItem Text="PT Challan" Value="SOW17"></asp:ListItem>
                        <asp:ListItem Text="Return" Value="SOW05"></asp:ListItem>
                    </asp:DropDownListChosen>
                </div>

                <div class="col-md-2 colpadding0">
                    <asp:TextBox runat="server" ID="tbxStartDate" placeholder="DD/MM/YYYY" AutoPostBack="true" TextMode="DateTime" Width="95%"
                        AutoCompleteType="Disabled" class="form-control" OnTextChanged="tbxStartDate_TextChanged" Style="text-align: center;" />
                    <asp:RequiredFieldValidator ID="reqfldfordate" runat="server" ControlToValidate="tbxStartDate" ValidationGroup="ComplianceInstanceValidationGroup">
                    </asp:RequiredFieldValidator>
                </div>

                 <div class="col-md-1 colpadding0">
                    <asp:Button ID="btnReAssign" runat="server" CssClass="btn btn-primary" OnClick="btnReAssign_Click" Text="Re-Assign" Visible="false" />
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="row col-md-12 colpadding0" style="display: none;">
                <asp:CheckBox ID="chkToActiveInstance" runat="server" AutoPostBack="true" OnCheckedChanged="chkToActiveInstance_CheckedChanged" Checked="true" />
                <asp:CheckBox ID="chkToDeActiveInstance" runat="server" AutoPostBack="true" OnCheckedChanged="chkToDeActiveInstance_CheckedChanged" /> 
            </div>

            <div class="row col-md-12 colpadding0">
                <asp:Panel ID="Panel1" Width="100%" Style="min-height: 10px; overflow-y: auto; margin-bottom: 10px;" runat="server">
                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                    <div class="tab-content">
                        <div runat="server" id="performerdocuments" class="tab-pane active">
                            <asp:GridView runat="server" ID="grdComplianceRoleMatrix" AutoGenerateColumns="false" OnRowCreated="grdComplianceRoleMatrix_RowCreated"
                                AllowSorting="true" OnSorting="grdComplianceRoleMatrix_Sorting" GridLines="None" CssClass="table" Width="100%"
                                AllowPaging="True" PageSize="50" OnRowDataBound="grdComplianceRoleMatrix__RowDataBound" CellPadding="4"
                                Font-Size="12px" DataKeyNames="ID" OnPageIndexChanging="grdComplianceRoleMatrix_PageIndexChanging" ShowHeaderWhenEmpty="true">
                                <Columns>
                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkActivateSelectAll" Text="" runat="server" AutoPostBack="true"
                                                OnCheckedChanged="chkActivateSelectAll_CheckedChanged" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkActivate" runat="server" AutoPostBack="true" OnCheckedChanged="chkActivate_CheckedChanged" />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Sr.No" SortExpression=" " ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex+1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ComplianceID" SortExpression="ComplianceID" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <%--width: 150px;--%>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                <asp:Label ID="lblComplianceID" runat="server" Text='<%# Eval("ComplianceID")%>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Compliance" SortExpression="ShortDescription">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 400px;">
                                                <asp:Label ID="lblShortDescription" runat="server" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("Description") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Entity/Branch" SortExpression="CustomerBranchID">
                                        <ItemTemplate>
                                            <%--width: 150px;--%>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                <asp:Label ID="lblLocation" runat="server" Text='<%# Eval("Location")%>'></asp:Label>
                                                <asp:Label ID="lblBranchID" runat="server" Text='<%# Eval("CustomerBranchID") %>' Visible="false"></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="User" SortExpression="userID">
                                        <ItemTemplate>
                                            <%--width: 550px;--%>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                <asp:Label ID="lblUsers" runat="server" Text='<%# Eval("Users") %>'></asp:Label>
                                                <asp:Label ID="lblUserID" runat="server" Text='<%# Eval("userID") %>' Visible="false"></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Role" SortExpression="Role">
                                        <ItemTemplate>
                                            <%-- width: 550px;--%>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                <asp:Label ID="lblRole" runat="server" Text='<%# Eval("Role") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Start Date" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:UpdatePanel ID="UpStartDate" runat="server">
                                                <ContentTemplate>
                                                    <asp:TextBox ID="txtStartDate" CssClass="form-control StartDate" runat="server" TextMode="DateTime" AutoCompleteType="Disabled"
                                                        Style="text-align: center;"></asp:TextBox>
                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:PostBackTrigger ControlID="txtStartDate" />
                                                </Triggers>
                                            </asp:UpdatePanel>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <PagerStyle HorizontalAlign="Right" />
                                <%-- RowStyle CssClass="clsROWgrid" />--%>
                                <%--<HeaderStyle CssClass="clsheadergrid" />--%>
                                <PagerTemplate>
                                    <table style="display: none">
                                        <tr>
                                            <td>
                                                <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                            </td>
                                        </tr>
                                    </table>
                                </PagerTemplate>
                                <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                                <EmptyDataTemplate>
                                    No Records Found.
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                        <div id="record1" runat="server" class="col-md-12 colpadding0">
                            <div class="col-md-6 colpadding0">
                                <div runat="server" id="DivRecordsScrum">
                                    <p style="padding-right: 0px !Important; color: #666; text-align: -webkit-left;">
                                        <asp:Label ID="Label4" runat="server" Text="Showing "></asp:Label>
                                        <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>- 
                                            <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                                            <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-6 colpadding0">
                                <div class="table-paging" runat="server" id="DivnextScrum" style="margin-bottom: 20px">
                                    <asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick="Previous_Click" />

                                    <div class="table-paging-text">
                                        <p>
                                            <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                        <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>
                                        </p>
                                    </div>
                                    <asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick="Next_Click" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1" style="padding-left: 0px;">
                        <label for="ddlPageSize" class="hidden-label">Show</label>
                        <asp:DropDownList runat="server" Visible="false" ID="ddlPageSize" AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged"
                            class="form-control">
                            <asp:ListItem Text="5" />
                            <asp:ListItem Text="10"  />
                            <asp:ListItem Text="20" />
                            <asp:ListItem Text="50" Selected="True" />
                        </asp:DropDownList>
                    </div>

                    <asp:GridView runat="server" ID="grdToDeactivate" AutoGenerateColumns="false" GridLines="Vertical" OnRowCreated="grdToDeactivate_RowCreated"
                        BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true" OnSorting="grdToDeactivate_Sorting"
                        CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="10" OnRowDataBound="grdToDeactivate__RowDataBound"
                        Font-Size="12px" DataKeyNames="ID" OnPageIndexChanging="grdToDeactivate_PageIndexChanging">
                        <Columns>
                            <asp:TemplateField HeaderText="ComplianceID" SortExpression="Sections">
                                <ItemTemplate>
                                    <%--width: 150px;--%>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                        <asp:Label ID="lblComplianceID" runat="server" Text='<%# Eval("ComplianceID")%>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Description" SortExpression="Description">
                                <ItemTemplate>

                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px;">
                                        <asp:Label ID="lblShortDescription" runat="server" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("Description") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Users" SortExpression="Users">
                                <ItemTemplate>
                                    <%--width: 550px;--%>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                        <asp:Label ID="lblUsers" runat="server" Text='<%# Eval("Users") %>'></asp:Label>
                                        <asp:Label ID="lblUserID" runat="server" Text='<%# Eval("userID") %>' Visible="false"></asp:Label>
                                        <asp:Label ID="lblComplianceInstanceID" runat="server" Text='<%# Eval("ComplianceInstanceID") %>' Visible="false"></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Role" SortExpression="Role">
                                <ItemTemplate>
                                    <%-- width: 550px;--%>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                        <asp:Label ID="lblRole" runat="server" Text='<%# Eval("Role") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="CreatedOn" SortExpression="CreatedOn">
                                <ItemTemplate>
                                    <%-- width: 550px;--%>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                        <%-- <asp:Label ID="lblCreatedOn" runat="server" Text='<%# Eval("CreatedOn") %>'  ></asp:Label>--%>
                                        <%# Convert.ToDateTime(Eval("CreatedOn")).ToString("dd-MMM-yyyy") %>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Start Date" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <%--<asp:Label ID="lblStartDate" runat="server" Text='<%# Eval("StartDate") %>'  ></asp:Label>--%>
                                    <%# Convert.ToDateTime(Eval("StartDate")).ToString("dd-MMM-yyyy") %>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                <HeaderTemplate>

                                    <asp:CheckBox ID="chkDeActivateSelectAll" Text="Deactivate" runat="server" AutoPostBack="true" OnCheckedChanged="chkDeActivateSelectAll_CheckedChanged" />

                                </HeaderTemplate>
                                <ItemTemplate>

                                    <asp:CheckBox ID="chkDeActivate" runat="server" AutoPostBack="true" OnCheckedChanged="chkDeActivate_CheckedChanged" />


                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                        <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                        <EmptyDataTemplate>
                            No Records Found.
                        </EmptyDataTemplate>
                    </asp:GridView>

                </asp:Panel>
            </div>

            <div class="row col-md-12 colpadding0" style="text-align: center">
                <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" OnClientClick="return Confirm();" class="btn btn-primary"
                    ValidationGroup="ComplianceInstanceValidationGroup" Visible="false" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

     <div id="divReAssignCompliance">
        <iframe id="iframeReAssign" style="width: 100%; height: 100%; border: none;"></iframe>
    </div>
</asp:Content>
