﻿<%@ Page Title="Masters :: Employee Master" Language="C#" AutoEventWireup="true" MasterPageFile="~/HRPlusCompliance.Master" CodeBehind="RLCS_EmployeeMaster.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.RLCS_EmployeeMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../Newjs/jszip.min.js"></script>

    <link href="../NewCSS/Kendouicss.css" rel="stylesheet" />

    <title></title>

    <style>
        .k-grid-content {
            min-height: 250px;
        }

        .k-btn {
            /*// float: right;*/
            margin-right: 10px;
        }

        .k-btnUpload {
            float: right;
            margin-right: 10px;
        }
    </style>

    <script id="templateTooltip" type="text/x-kendo-template">
                <div>
                <div> #:value ? value : "N/A" #</div>
                </div>
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            //ApiTrack_Activity("EmployeeMaster", "pageView", null);
        });


        $(document).ready(function () {
            $("#dropdowntree").kendoDropDownTree({
                placeholder: "Entity/State/Location/Branch",
                checkboxes: {
                    checkChildren: true
                },
                //checkboxes: true,
                checkAll: true,
                autoWidth: true,
                checkAllTemplate: "Select All",
                //dataTextField: "AVACOM_BranchName",
                //dataValueField: "AVACOM_BranchID",
                dataTextField: "Name",
                dataValueField: "ID",
                //optionLabel: "All",
                change: function (e) {
                    var filter = { logic: "or", filters: [] };
                    var values = this.value();

                    $.each(values, function (i, v) {
                        filter.filters.push({
                            field: "EM_Branch", operator: "eq", value: parseInt(v)
                        });
                    });

                    fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc');
                    var dataSource = $("#grid").data("kendoGrid").dataSource;
                    dataSource.filter(filter);

                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {                            
                            url: "<% =Path%>GetAllEntitiesLocationsList?customerId=<% =CustId%>",
                            //url: "<% =Path%>GetAssignedEntities?customerId=<% =CustId%>&userId=<% =UserId%>&profileID=<% =ProfileID%>",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },
                            dataType: 'json',
                        }
                    },
                    schema: {
                        data: function (response) {
                            debugger;
                            return response.Result;
                        },
                        model: {
                            children: "Children"
                        }
                    }
                }
            });
        });

        function checkEmployeeActive(value) {
            if (value == "A") {
                return "Active";
            } else if (value == "I") {
                return "InActive";
            }
        }

        function BindFirstGrid() {
            debugger;
            var gridview = $("#grid").kendoGrid({
                dataSource: {
                    serverPaging: false,
                    pageSize: 10,
                    transport: {
                        read: {
                            url: '<% =Path%>GetAll_EmployeeMaster?CustomerID=<% =CustId%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },
                        }
                    },
                    batch: true,
                    pageSize: 10,
                    schema: {
                        data: function (response) {
                            debugger;
                            if (response != null || response != undefined)
                                return response.Result;
                        },
                        total: function (response) {
                            debugger;
                            if (response != null || response != undefined)
                                return response.Result.length;
                        }
                    }
                },

                sortable: true,
                filterable: true,
                columnMenu: true,
                pageable: {
                    refresh: true,
                    buttonCount: 3,

                    change: function (e) {

                    }
                },
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                columns: [

                    { hidden: true, field: "AVACOM_CustomerID", title: "CustID" },
                    { hidden: true, field: "AVACOM_BranchID", title: "BranchID" },
                    { hidden: true, field: "EM_Branch", title: "Branch" },
                    {
                        title: "Sr.No",
                        field: "rowNumber",
                        template: "<span class='row-number'></span>",
                        width: "11%;",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "EM_EmpID", title: 'EmpID',
                        width: "20%;",
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "EM_EmpName", title: 'Name',
                        width: "16%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "EM_ClientID", title: 'ClientID',
                        width: "24%",
                        filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "EM_Branch", title: 'Branch',
                        width: "24%",
                        filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "EM_DOB",
                        title: 'Date of Birth',
                        template: "#= kendo.toString(kendo.parseDate(EM_DOB, 'yyyy-MM-dd'), 'dd-MM-yyyy') #",
                        width: "17%",
                        attributes: {
                            style: 'white-space: nowrap '
                        },
                        filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "EM_DOJ",
                        title: 'Joining Date',
                        template: "#= kendo.toString(kendo.parseDate(EM_DOJ, 'yyyy-MM-dd'), 'dd-MM-yyyy') #",
                        width: "17%",
                        attributes: {
                            style: 'white-space: nowrap '
                        },
                        filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "EM_DOL",
                        title: 'Leaving Date',
                        template: "#= kendo.toString(kendo.parseDate(EM_DOL, 'yyyy-MM-dd'), 'dd-MM-yyyy') #",
                        width: "17%",
                        attributes: {
                            style: 'white-space: nowrap '
                        },
                        filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    }, {
                        field: "EM_Status",
                        title: 'Status',
                        template: '#:checkEmployeeActive(EM_Status)#',
                        width: "15%",
                        attributes: {
                            style: 'white-space: nowrap '
                        },
                        filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        command: [
                         { name: "edit", text: "", iconClass: ".k-icon k-i-pencil", className: "ob-download", click: Edit },
                        ], title: "Action", lock: true, width: "12.7%;"
                    }
                ],
                dataBound: function () {
                    var rows = this.items();
                    $(rows).each(function () {
                        var index = $(this).index() + 1
                            + ($("#grid").data("kendoGrid").dataSource.pageSize() * ($("#grid").data("kendoGrid").dataSource.page() - 1));;
                        var rowLabel = $(this).find(".row-number");
                        $(rowLabel).html(index);
                    });
                }
            });

            //gridview.Custom("edit").Click("Edit");
            function Edit(e) {
                debugger;
                var d = e.data;
                var grid = $("#grid").data("kendoGrid");

                var tr = $(e.target).closest("tr");    // get the current table row (tr)
                var item = this.dataItem(tr);
                var EMPID = item.EM_EmpID;
                var Client = item.EM_ClientID;
                var CustID = item.AVACOM_CustomerID;

                $('#divRLCSEmployeeAddDialog').show();
                //$("#idloader1").show();
                var myWindowAdv = $("#divRLCSEmployeeAddDialog");

                function onClose() {

                }

                myWindowAdv.kendoWindow({
                    width: "85%",
                    height: '90%',
                    title: "Employee Details-HR Compliance",
                    visible: false,
                    actions: [
                        //"Pin",
                        "Close"
                    ],
                    close: onClose
                });

                myWindowAdv.data("kendoWindow").center().open();
                $('#iframeAdd').attr('src', "/Setup/CreateEmplodyeeMaster?EmpID=" + EMPID + "&CustID=" + CustID + "&Client=" + Client);

                // $('#grid').data('kendoGrid').refresh();
                // return false;
            }
        }

        $(document).ready(function () {
            $('#divRLCSEmployeeUploadDialog').hide();
            $('#divRLCSEmployeeAddDialog').hide();
            BindFirstGrid();
            $("#btnSearch").click(function () {
                debugger;
                var filter = [];
                $x = $("#txtSearch").val();
                if ($x) {
                    var gridview = $("#grid").data("kendoGrid");
                    gridview.dataSource.query({
                        page: 1,
                        pageSize: 50,
                        filter: {
                            logic: "or",
                            filters: [
                              { field: "EM_Branch", operator: "contains", value: $x },
                            ]
                        }
                    });

                    return false;
                }
                else {
                    BindFirstGrid();
                }
                // return false;
            });



        });
    </script>
    <script type="text/javascript">
        function fcloseStory(obj) {
            var DataId = $(obj).attr('data-Id');
            var dataKId = $(obj).attr('data-K-Id');
            var seq = $(obj).attr('data-seq');
            var deepspan = $('#' + DataId + ' > li > span > span.k-i-close')[seq];
            $(deepspan).trigger('click');
            var upperli = $('#' + dataKId);
            $(upperli).remove();
            //for rebind if any pending filter is present (Main Grid)
            fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc');


            CheckFilterClearorNotMain();
        };

        function fCreateStoryBoard(Id, div, filtername) {
            debugger;

            var IdKReset = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('id');
            $('#' + div).html('');
            $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '28px');
            $('#' + div).css('display', 'block');

            if (div == 'filtersstoryboard') {
                $('#' + div).append('Location&nbsp;&nbsp;&nbsp;:');//Dashboard
                $('#ClearfilterMain').css('display', 'block');
            }

            for (var i = 0; i < $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length; i++) {
                var button = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button')[i];
                $(button).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('placeholder', 'Shree');
                var buttontest = $($(button).find('span')[0]).text();
                if (buttontest.length > 10) {
                    buttontest = buttontest.substring(0, 10).concat("...");
                }
                $('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="background-color:#1fd9e1; height: 20px;Color:white;"><span unselectable="on">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory(this)" title="delete" aria-label="delete" class="k-select"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory(this)"  class="k-icon k-i-close"></span></span></li>&nbsp;');
            }

            if ($($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) {
                $('#' + div).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');
            }
            //CheckFilterClearorNot();

            CheckFilterClearorNotMain();
        }

        $(document).ready(function () {
            function CheckFilterClearorNotMain() {
                if (($($($('#dropdowntree').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0)) {
                    $('#ClearfilterMain').css('display', 'none');
                }
            }
        });
    </script>

    <script>
        function OpenUploadWindow() {

            $('#divRLCSEmployeeUploadDialog').show();
            kendo.ui.progress($(".chart-loading"), true);
            $("#idloader3").show();
            $('#iframeUpload').attr('src', "../Setup/UploadEmployeeFiles");
            var myWindowAdv = $("#divRLCSEmployeeUploadDialog");

            function onClose() {

            }

            myWindowAdv.kendoWindow({
                width: "70%",
                height: "70%",
                title: "Upload Documents",
                visible: false,
                actions: [
                    "Pin",
                    "Close"
                ],
                close: onClose
            });

            myWindowAdv.data("kendoWindow").center().open();


            return false;
        }

        function OpenAddEmployeePopup(ID) {

            $('#divRLCSEmployeeAddDialog').show();
            //$("#idloader1").show();
            var myWindowAdv = $("#divRLCSEmployeeAddDialog");

            function onClose() {

            }

            myWindowAdv.kendoWindow({
                width: "85%",
                height: '90%',
                title: "Employee Details-HR Compliance",
                visible: false,
                actions: [
                    //"Pin",
                    "Close"
                ],
                close: onClose
            });

            myWindowAdv.data("kendoWindow").center().open();
            $('#iframeAdd').attr('src', "../Setup/CreateEmplodyeeMaster?Id=" + ID);

            // $('#grid').data('kendoGrid').refresh();
            return false;
        }


        $(document).ready(function () {
            // function BindLocationFilter() {



            function ClearAllFilterMain(e) {

                e.preventDefault();

                $("#dropdowntree").data("kendoDropDownTree").value([]);
                //$("#ddlMonth").data("kendoDropDownList").value([]);
                //$("#ddlYear").data("kendoDropDownList").value([]);
                $('#ClearfilterMain').css('display', 'none');

                $("#grid").data("kendoGrid").dataSource.filter({});

                $('#chkAll').removeAttr('checked');


            }
        });


    </script>

    <script>
        $(document).ready(function () {
            $("#ddlStatus").kendoDropDownList({
                dataTextField: "text",
                dataValueField: "EM_Status",
                dataSource: [
                     { text: "Select", EM_Status: "S" },
                  { text: "Active", EM_Status: "A" },
                  { text: "InActive", EM_Status: "I" }
                ],
                change: function () {
                    debugger;
                    var value = this.value();

                    var gridview = $("#grid").data("kendoGrid");

                    if (value) {

                        gridview.dataSource.filter({ field: "EM_Status", operator: "eq", value: value });
                    } else {
                        grid.dataSource.filter({});
                    }
                }
            });
        });
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row colpadding0">
        <div class="col-md-12 colpadding0">
            <header class="panel-heading tab-bg-primary" style="background: none !important; padding: 0px 0px;">
                <ul class="nav nav-tabs">
                    <li>
                        <asp:LinkButton ID="lnkTabEntityBranch" runat="server" PostBackUrl="~/RLCS/RLCS_EntityLocation_Master.aspx">Entity\Location\Branch</asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkTabUser" runat="server" PostBackUrl="~/RLCS/RLCS_User_MasterList.aspx">User</asp:LinkButton>
                    </li>
                    <li class="active">
                        <asp:LinkButton ID="lnkTabEmployee" runat="server" PostBackUrl="~/RLCS/RLCS_EmployeeMaster.aspx">Employee</asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkTabStateCity" runat="server" PostBackUrl="~/RLCS/RLCS_Location_Master.aspx">State-City</asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkTabDesignation" runat="server" PostBackUrl="~/RLCS/RLCS_Designation_Master.aspx">Designation</asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkTabLeaveType" runat="server" PostBackUrl="~/RLCS/RLCS_LeaveType_Master.aspx">Leave Type</asp:LinkButton>
                    </li>
                </ul>
            </header>
        </div>

        <div class="col-md-12 colpadding0 toolbar" style="margin-top: 10px;">
            <div class="col-md-4">
                <input id="dropdowntree" data-placeholder="Entity/Location/Branch" style="width: 100%;" />
            </div>

            <div class="col-md-2">
                <input id="ddlStatus" style="width: 98%" />
            </div>

            <div class="col-md-4">
                <input class="k-textbox" type="text" id="txtSearch" style="width: 70%" placeholder="Type to Search..." />
                <button id="btnSearch" class="k-button">Search</button>
            </div>

            <div class="col-md-1">
                <button id="btnAdd" class="k-button k-btnUpload" onclick="return OpenAddEmployeePopup()"><span class="k-icon k-i-plus-outline"></span>Add</button>
                <%--<button id="btnAdd" class="k-button k-btn" onclick="return OenEmployeePopUp()"><span class="k-icon k-i-plus-outline">Add New</span></button>--%>
            </div>

            <div class="col-md-1">
                <button id="Clear" class="k-button k-btn" onclick="return OpenUploadWindow()"><span class="k-icon k-i-upload"></span>Upload</button>
            </div>
            <button id="ClearfilterMain" class="k-button" style="display: none;" onclick="ClearAllFilterMain(event)">
                <span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear Filter
            </button>
        </div>
    </div>
    <div class="row colpadding0">
        <div class="col-md-12 colpadding0 toolbar">
            <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: black;" id="filtersstoryboard">&nbsp;</div>
        </div>
    </div>

    <div class="row colpadding0 " style="margin-top: 1%">
        <div class="col-md-12 colpadding0">

            <div id="grid" style="border: none;">
                <div class="k-header k-grid-toolbar">
                </div>
            </div>
        </div>
    </div>

    <div id="divRLCSEmployeeUploadDialog">
        <iframe id="iframeUpload" style="width: 1165px; height: 677px; border: none"></iframe>
    </div>
    <div id="divRLCSEmployeeAddDialog">
        <iframe id="iframeAdd" style="width: 1165px; height: 677px; border: none"></iframe>
    </div>

</asp:Content>

