﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business;
using System.Globalization;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class RLCS_EntityAssignment_New : System.Web.UI.Page
    {
        public static List<int> branchList = new List<int>();
        protected static int SelectedCustID;
        protected int? serviceproviderid;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["customerID"]))
            {
                SelectedCustID = Convert.ToInt32(Request.QueryString["customerID"]);
            }
            else
            {
                SelectedCustID = 0;
            }
            if (!IsPostBack)
            {
                divFilterUsers.Visible = true;
                FilterLocationdiv.Visible = true;
                btnAddNew.Visible = false;

                BindCustomers();

                if (SelectedCustID > 0)
                    ddlCustomer.SelectedValue = SelectedCustID.ToString();


                BindLocationFilter();
                BindLocation();

                BindUsers(ddlUsers);
                BindUsers(ddlFilterUsers);

                if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue) && ddlCustomer.SelectedValue != "-1")
                {                    
                    btnAddNew.Visible = true;
                }
                else
                {
                    btnAddNew.Visible = false;
                }

                BindComplianceEntityInstances();

                //BindCategory();               

                if (SelectedPageNo.Text == "")
                {
                    SelectedPageNo.Text = "1";
                }

                GetPageDisplaySummary();

                if (Session["TotalEntityRows"] != null)
                    TotalRows.Value = Session["TotalEntityRows"].ToString();

                tbxBranch.Attributes.Add("readonly", "readonly");
                ForceCloseFilterBranchesTreeView();
            }

            serviceproviderid = CustomerManagement.GetServiceProviderID(Convert.ToInt32(AuthenticationHelper.CustomerID));
            if (AuthenticationHelper.Role == "HEXCT" && serviceproviderid == 94)
            {
                lnkBtnComAssign.Visible = false;
                lnkBtnComAct.Visible = false;
            }
        }

        protected void Page_PreInit(object sender, EventArgs e)
        {
            string masterpage = Convert.ToString(Session["masterpage"]);
            if (masterpage == "HRPlusSPOCMGR")
            {
                this.Page.MasterPageFile = "~/HRPlusSPOCMGR.Master";
            }
            else if (masterpage == "HRPlusCompliance")
            {
                this.Page.MasterPageFile = "~/HRPlusCompliance.Master";
            }
        }

        private void BindCustomers()
        {
            try
            {
                ddlCustomer.DataTextField = "Name";
                ddlCustomer.DataValueField = "ID";

                int customerID = -1;
                int serviceProviderID = 95;
                int distributorID = -1;

                if (AuthenticationHelper.Role == "CADMN")
                {
                    //customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "SPADM")
                {
                    serviceProviderID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "DADMN")
                {
                    distributorID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }

                //ddlCustomer.DataSource = CustomerManagement.GetAll_HRComplianceCustomersByServiceProviderOrDistributor(customerID, serviceProviderID, distributorID, 2, false);
                ddlCustomer.DataSource = CustomerManagement.GetAll_HRComplianceCustomers(AuthenticationHelper.UserID, customerID, serviceProviderID, distributorID, AuthenticationHelper.Role, false);                
                ddlCustomer.DataBind();

                GetPageDisplaySummary();

                if (AuthenticationHelper.Role == "CADMN")
                {
                    if (ddlCustomer.Items.FindByValue(customerID.ToString()) != null)
                        ddlCustomer.Items.FindByValue(customerID.ToString()).Selected = true;
                }

                //if (AuthenticationHelper.Role != "CADMN")
                //ddlCustomer.Items.Insert(0, new ListItem(" Select Customer ", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }
        
        private void BindComplianceEntityInstances()
        {
            try
            {
                int branchID = -1;
                if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1")
                {
                    branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                }

                int userID = -1;
                if (!string.IsNullOrEmpty(ddlFilterUsers.SelectedValue))
                {
                    userID = Convert.ToInt32(ddlFilterUsers.SelectedValue);
                }

                //int customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;

                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    //customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else
                {
                    if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                    {
                        customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                    }
                }
                //else if (AuthenticationHelper.Role == "SPADM" || AuthenticationHelper.Role == "DADMN")
                //{
                //    if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                //    {
                //        customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                //    }
                //}
               
                if (customerID != -1)
                {
                    //var assignedEntities = AssignEntityManagement.SelectAllEntities(-1, userID, customerID);
                    var assignedEntities = RLCS_ClientsManagement.GetAssignedEntityBranches(customerID, userID);
                    
                    if (assignedEntities.Count > 0)
                    {
                        if (branchID != -1)
                        {
                            branchList.Clear();
                            GetAll_Branches(customerID, branchID);
                            branchList.ToList();

                            if (branchList.Count > 0)
                                assignedEntities = assignedEntities.Where(row => row.BranchID != null && branchList.Contains((int)row.BranchID)).ToList();
                        }
                    }
                    
                    grdAssignEntities.DataSource = assignedEntities;
                    Session["TotalEntityRows"] = 0;
                    Session["TotalEntityRows"] = assignedEntities.Count();
                    grdAssignEntities.DataBind();

                    if (assignedEntities.Count > 0)
                        record1.Visible = true;
                    else
                        record1.Visible = false;

                    upComplianceTypeList.Update();

                    ForceCloseFilterBranchesTreeView();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void upCompliance_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranches", string.Format("initializeJQueryUI('{0}', 'divBranches');", tbxBranch.ClientID), true);
                //ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void upComplianceTypeList_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divBranches');", tbxBranch.ClientID), true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        private void BindLocation()
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    //customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else 
                {
                    if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                    {
                        customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                    }
                }
                //else if (AuthenticationHelper.Role == "SPADM" || AuthenticationHelper.Role == "DADMN")
                //{
                //    if(!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                //    {
                //        customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                //    }                   
                //}

                if (customerID != -1)
                {
                    tvFilterLocation.Nodes.Clear();

                    var bracnhes = RLCS_ClientsManagement.GetAllHierarchy(customerID);

                    TreeNode node = new TreeNode("Entity/State/Location/Branch", "-1");
                    node.Selected = true;
                    tvFilterLocation.Nodes.Add(node);

                    foreach (var item in bracnhes)
                    {
                        node = new TreeNode(item.Name, item.ID.ToString());
                        node.SelectAction = TreeNodeSelectAction.Expand;
                        BindBranchesHierarchy(node, item);
                        tvFilterLocation.Nodes.Add(node);
                    }

                    tvFilterLocation.CollapseAll();
                    tvFilterLocation_SelectedNodeChanged(null, null);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }
               
        protected void tvBranches_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {                
                tbxBranch.Text = tvBranches.SelectedNode.Text;
                BindComplianceEntityInstances();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindLocationFilter()
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    //customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else
                {
                    if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                    {
                        customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                    }
                }
                //else if (AuthenticationHelper.Role == "SPADM" || AuthenticationHelper.Role == "DADMN")
                //{
                //    if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                //    {
                //        customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                //    }
                //}
               
                if (customerID != -1)
                {
                    var bracnhes = RLCS_ClientsManagement.GetAllHierarchy(customerID);

                    tvBranches.Nodes.Clear();

                    TreeNode node = new TreeNode("Entity/State/Location/Branch", "-1");
                    node.Selected = true;
                    tvBranches.Nodes.Add(node);

                    foreach (var item in bracnhes)
                    {
                        node = new TreeNode(item.Name, item.ID.ToString());
                        node.SelectAction = TreeNodeSelectAction.Expand;
                        BindBranchesHierarchy(node, item);
                        tvBranches.Nodes.Add(node);
                    }

                    tvBranches.CollapseAll();
                    tvBranches_SelectedNodeChanged(null, null);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp)
        {
            try
            {
                foreach (var item in nvp.Children)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    BindBranchesHierarchy(node, item);
                    parent.ChildNodes.Add(node);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }
        
        protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindUsers(ddlFilterUsers);
                BindUsers(ddlUsers);

                if (ddlCustomer.SelectedIndex != -1)
                {
                    btnAddNew.Visible = true;                    

                    BindLocation();
                    BindLocationFilter();
                }
                else
                    btnAddNew.Visible = false;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
            BindComplianceEntityInstances();
            GetPageDisplaySummary();
            //BindCustomers();
        }

        private void ForceCloseFilterBranchesTreeView()
        {
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideFilterTreeView", "$(\"#divFilterLocation\").hide();", true);
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideFilterTreeView1", "$(\"#divBranches\").hide();", true);
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideFilterdivBranches", "$(\"#divBranches\").hide(\"blind\", null, 5, function () { });", true);
        }

        private void BindUsers(DropDownList ddlUserList, List<long> ids = null)
        {
            try
            {   
                int customerID = -1;
                int serviceProviderID = -1;
                int distributorID = -1;

                if (AuthenticationHelper.Role == "CADMN")
                {                    
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "SPADM")
                {
                    serviceProviderID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "DADMN")
                {
                    distributorID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                
                if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                {
                    customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                }

                if (customerID != -1)
                {
                    ddlUserList.DataTextField = "Name";
                    ddlUserList.DataValueField = "ID";

                    ddlUserList.Items.Clear();

                    var users = UserCustomerMappingManagement.GetAllUser_ServiceProviderDistributorCustomer(customerID, serviceProviderID, distributorID);
                    //var users = RLCS_Master_Management.GetAll_RLCSUsers_IncludingServiceProviderOrDistributor(customerID, serviceProviderID, distributorID, 2);
                    //var users = UserManagement.GetAll_RLCSUsers(customerID);
                    users.Insert(0, new { ID = -1, Name = ddlUserList == ddlUsers ? "< Select >" : " All " });

                    ddlUserList.DataSource = users;
                    ddlUserList.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void ddlFilterUsers_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindComplianceEntityInstances();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                //List<int> branchList = new List<int>();                
                int customerID =  0;

                if (AuthenticationHelper.Role == "CADMN")
                {
                    //customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else
                {
                    if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                    {
                        customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                    }
                }
                //else if (AuthenticationHelper.Role == "SPADM" || AuthenticationHelper.Role == "DADMN")
                //{
                //    if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                //    {
                //        customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                //    }
                //}                

                if (customerID != 0)
                {
                    int userID = Convert.ToInt32(ddlUsers.SelectedValue);
                    int branchID = Convert.ToInt32(tvBranches.SelectedNode.Value);
                    
                    if (userID != -1 && branchID != -1)
                    {
                        branchList.Clear();
                        GetAll_Branches(customerID, Convert.ToInt32(tvBranches.SelectedValue));
                        branchList.ToList();

                        if (branchList.Count > 0)
                        {
                            //User-Customer Mapping
                            UserCustomerMapping objUserCustMapping = new UserCustomerMapping()
                            {
                                UserID = userID,
                                CustomerID = customerID,
                                IsActive = true
                            };

                            UserCustomerMappingManagement.CreateUpdate_UserCustomerMapping(objUserCustMapping);

                            //RLCS-Entity Assignment
                            List<int> lstComplianceCategory = new List<int>();
                            lstComplianceCategory.Add(2);
                            lstComplianceCategory.Add(5);

                            lstComplianceCategory.ForEach(EachCategoryID =>
                            {
                                bool recordActive = true;

                                RLCS_Master_Management.CreateUpdate_RLCS_EntityAssignment(userID, branchList, recordActive, lstComplianceCategory);

                                //Entity Assignment
                                List<EntitiesAssignment> assignmentEntities = new List<EntitiesAssignment>();

                                branchList.ForEach(eachBranch =>
                                {
                                    var data = AssignEntityManagement.SelectEntity(eachBranch, userID, EachCategoryID);

                                    if (data == null)
                                    {
                                        EntitiesAssignment objEntitiesAssignment = new EntitiesAssignment();
                                        objEntitiesAssignment.UserID = userID;
                                        objEntitiesAssignment.BranchID = eachBranch;
                                        objEntitiesAssignment.ComplianceCatagoryID = EachCategoryID;
                                        objEntitiesAssignment.CreatedOn = DateTime.Now;

                                        assignmentEntities.Add(objEntitiesAssignment);
                                    }
                                });

                                if (assignmentEntities.Count > 0)
                                    AssignEntityManagement.Create(assignmentEntities);
                            });
                        }

                        BindComplianceEntityInstances();

                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Selected Entity/Branch Assigned to User Successfully";
                        ValidationSummary1.CssClass = "alert alert-success";
                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "script", "ClosePopupWindowAfterSave();", true);
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Select Entity/Branch and User to Assign";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void btnAddNew_Click(object sender, EventArgs e)
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    //customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else
                {
                    if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                    {
                        customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                    }
                }
                //else if (AuthenticationHelper.Role == "SPADM" || AuthenticationHelper.Role == "DADMN")
                //{
                //    if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                //    {
                //        customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                //    }
                //}                

                if (customerID != -1)
                {
                    if (tvBranches.SelectedNode != null)
                    {
                        tvBranches.SelectedNode.Selected = false;
                    }

                    lblErrorMassage.Text = "";
                    ddlUsers.SelectedValue = "-1";
                   
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divAssignEntitiesDialog\").dialog('open');", true);
                    tbxBranch.Text = "Entity/State/Location/Branch";
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView", "$(\"#divBranches\").hide(\"blind\", null, 5, function () { });", true);
                    ForceCloseFilterBranchesTreeView();
                    upCompliance.Update();
                }
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Select Customer";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void grdAssignEntities_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdAssignEntities.PageIndex = e.NewPageIndex;
            BindComplianceEntityInstances();
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                SelectedPageNo.Text = "1";

                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);
                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo).ToString();
                }

                grdAssignEntities.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                grdAssignEntities.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                BindComplianceEntityInstances();
                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void Next_Click(object sender, EventArgs e)
        {
            try
            {
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;

                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo + 1).ToString();
                }
                else
                {

                }

                if (!(StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) > Convert.ToInt32(Session["TotalEntityRows"])))
                    StartRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue);

                EndRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalEntityRows"]))
                    EndRecord = Convert.ToInt32(Session["TotalEntityRows"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";

                grdAssignEntities.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                grdAssignEntities.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                BindComplianceEntityInstances();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void GetPageDisplaySummary()
        {
            try
            {
                //DivRecordsScrum.Attributes.Remove("disabled");
                if (Session["TotalEntityRows"] != null)
                {
                    lblTotalRecord.Text = " " + Session["TotalEntityRows"].ToString();

                    lTotalCount.Text = GetTotalPagesCount().ToString();

                    if (lTotalCount.Text != "0")
                    {
                        //DivRecordsScrum.Visible = true;
                        //DivnextScrum.Visible = true;
                        if (SelectedPageNo.Text == "" || SelectedPageNo.Text == "0" || SelectedPageNo.Text == "1")
                        {
                            SelectedPageNo.Text = "1";
                            lblStartRecord.Text = "1";

                            if (!(Convert.ToInt32(ddlPageSize.SelectedValue) > Convert.ToInt32(Session["TotalEntityRows"].ToString())))
                                lblEndRecord.Text = ddlPageSize.SelectedValue + " ";
                            else
                                lblEndRecord.Text = Session["TotalEntityRows"].ToString() + " ";
                        }
                    }
                    else if (lTotalCount.Text == "0")
                    {
                        SelectedPageNo.Text = "0";
                        DivRecordsScrum.Visible = false;
                        DivnextScrum.Visible = false;
                    }
                }

                
            }

            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                if (Session["TotalEntityRows"] != null)
                TotalRows.Value = Session["TotalEntityRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);
                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);
                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        protected void grdAssignEntities_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {

                int branchID = -1;
                if (tvFilterLocation.SelectedValue != "-1")
                {
                    branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                }

                int userID = -1;
                if ((!string.IsNullOrEmpty(ddlFilterUsers.SelectedValue)))
                {
                    userID = Convert.ToInt32(ddlFilterUsers.SelectedValue);
                }

                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    //customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else
                {
                    if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                    {
                        customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                    }
                }
                //else if (AuthenticationHelper.Role == "SPADM" || AuthenticationHelper.Role == "DADMN")
                //{
                //    if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                //    {
                //        customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                //    }
                //}
                
                if (customerID != -1)
                {
                    var assignmentList = AssignEntityManagement.SelectAllEntities(branchID, userID, customerID);

                    if (direction == SortDirection.Ascending)
                    {
                        assignmentList = assignmentList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                        direction = SortDirection.Descending;
                    }
                    else
                    {
                        assignmentList = assignmentList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                        direction = SortDirection.Ascending;
                    }

                    foreach (DataControlField field in grdAssignEntities.Columns)
                    {
                        if (field.SortExpression == e.SortExpression)
                        {
                            ViewState["SortIndex"] = grdAssignEntities.Columns.IndexOf(field);
                        }
                    }

                    grdAssignEntities.DataSource = assignmentList;
                    grdAssignEntities.DataBind();

                    //tbxFilterLocation.Text = "< Select Location >";
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView", "$(\"#divFilterLocation\").hide(\"blind\", null, 5, function () { });", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void grdAssignEntities_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void Previous_Click(object sender, EventArgs e)
        {
            try
            {
                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;

                if (Convert.ToInt32(SelectedPageNo.Text) > 1)
                {
                    SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
                }

                StartRecord = StartRecord - Convert.ToInt32(ddlPageSize.SelectedValue);

                if (StartRecord < 1)
                    StartRecord = 1;

                EndRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalEntityRows"]))
                    EndRecord = Convert.ToInt32(Session["TotalEntityRows"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";

                grdAssignEntities.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                grdAssignEntities.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                BindComplianceEntityInstances();

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        #region To Find Branches not Entities and Sub-Entities

        public static List<NameValueHierarchy> GetAll_Branches(int customerID, int selectedEntityID)
        {
            List<NameValueHierarchy> hierarchy = null;

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             where row.IsDeleted == false
                             && row.CustomerID == customerID
                             && row.ID == selectedEntityID
                             select row);

                hierarchy = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();

                foreach (var item in hierarchy)
                {
                    branchList.Add(item.ID);
                    LoadChildBranches(customerID, item, true, entities);
                }
            }

            return hierarchy;
        }

        public static void LoadChildBranches(int customerid, NameValueHierarchy nvp, bool isClient, ComplianceDBEntities entities)
        {
            IQueryable<CustomerBranch> query = (from row in entities.CustomerBranches
                                                where row.IsDeleted == false
                                                && row.CustomerID == customerid
                                                && row.ParentID == nvp.ID
                                                //&& row.Type != 1
                                                select row);

            var subEntities = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();

            foreach (var item in subEntities)
            {
                branchList.Add(item.ID);
                LoadChildBranches(customerid, item, false, entities);
            }
        }

        #endregion
    }
}