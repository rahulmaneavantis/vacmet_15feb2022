﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Portal.RLCS.RLCSAPISettings;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Net.Http;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS.TLPages
{

    public class RLCSRedirectMenu
    {
        public string Id { get; set; }

        public string Area { get; set; }
        public string Controller { get; set; }

        public string Action { get; set; }
    }

    public static class RLCSEncryption
    {

        public static string encrypt(string encryptString)
        {
            string EncryptionKey = ConfigurationManager.AppSettings["TLConnect_Encrypt_Decrypt_Key"].ToString();
            byte[] clearBytes = Encoding.Unicode.GetBytes(encryptString);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] {
            0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76
        });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    encryptString = Convert.ToBase64String(ms.ToArray());
                }
            }
            return encryptString;
        }
    }



    public partial class RLCSRedirect : System.Web.UI.Page
    {
        public RLCSRedirect()
        {
            _httpClient = HttpClientConfig._httpClient;
        }
        private HttpClient _httpClient { set; get; }
        protected void Page_Load(object sender, EventArgs e)
        {
            List<RLCSRedirectMenu> Lst = new List<RLCSRedirectMenu>();

            var MenuId = Request.QueryString["id"];
            HttpResponseMessage response = null;
            response = _httpClient.GetAsync("api/Masters/GetRLCSAvacomScreenMenuDetails?Role=" + AuthenticationHelper.Role + "&User=" + AuthenticationHelper.ProfileID).Result;
            Lst = response.Content.ReadAsAsync<List<RLCSRedirectMenu>>().Result;
            RLCSRedirectMenu menudetails = Lst.FirstOrDefault(a => a.Id.ToString() == MenuId);

            //RLCSRedirectMenu menudetails = Lst.FirstOrDefault(a => a.Id.ToString() == MenuId);  //from x in Lst where x.Id == nwId select x;

            
            var RLCSURL = ConfigurationManager.AppSettings["RLCS_WEB_URL"].ToString() + "/" + menudetails.Area + "/" + menudetails.Controller + "/" + menudetails.Action + "?AvacomUserId=" + RLCSEncryption.encrypt(User.Identity.Name.Split(';')[0]);


            RLCSIFrame.Attributes["src"] = RLCSURL;

            //Response.Redirect(RLCSURL, false);
        }
    }


}