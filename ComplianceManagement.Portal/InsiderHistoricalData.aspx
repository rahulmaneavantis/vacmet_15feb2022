﻿<%@ Page Language="C#" MasterPageFile="~/InsiderTrade.Master" AutoEventWireup="true" CodeBehind="InsiderHistoricalData.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.InsiderHistoricalData" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     

    <link href="NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />
        
   
    <script type="text/javascript" src="Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="Newjs/jszip.min.js"></script>

    <style type="text/css">
    .k-calendar td.k-state-focused.k-state-selected{
        background-color:blue;
    }
    .k-i-arrow-60-down{
        margin-top:5px;
    }
    .k-i-calendar{
        margin-top:5px;
    }
    .k-grid-search {
        float:left !important;
    }
    .k-grid{
        padding:0;
    }
    .customer-photo {
        display: inline-block;
        width: 32px;
        height: 32px;
        border-radius: 50%;
        background-size: 32px 35px;
        background-position: center center;
        vertical-align: middle;
        line-height: 32px;
        box-shadow: inset 0 0 1px #999, inset 0 0 10px rgba(0,0,0,.2);
        margin-left: 5px;
        cursor:pointer;
    }

    .customer-name {
        display: inline-block;
        vertical-align: middle;
        line-height: 32px;
        padding-left: 3px;
    }
</style>
    <style>
        body{
        color:#666;
        font-weight:300;
    }
        .form-group.required .control-label:after { 
   content:"*";
   color:red;
}
        html .k-grid tr:hover {
    background: #E4F7FB;
}
         .k-state-focused.k-state-selected{
              background: transparent;
              color:#666;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="col-sm-12">
        <div class="col-sm-12" style="padding:0">
            <div class="toolbar">  
                    <label class="control-label" style="font-weight:450; padding-top: 5px;" for="sel1">Select Company: &nbsp; </label>
                    <input id="dropdownlistComplianceType" data-placeholder="Type" style="width:172px;">

                <button  style="float: right;"  type="button" class="btn btn-primary" download onclick="DownloadExcl()" >Download Excel</button>
                    <button  style="float: right;margin-right: 1%;"  id="button1" type="button" class="btn btn-primary"  onclick="UploadBtn(event)" >Upload</button> 
            </div>
        </div>
        <div class="col-sm-12" style="margin-bottom:2%;" >
            
        </div>

        <div class="col-sm-12" id="grid"></div>
    </div>

       <div id="divAdvanceSearchModel" style="padding-top: 5px;z-index: 999;display:none;">
        <input  id="dropdownName" style="width:40%" data-placeholder="Name" />
     </div>

     <div id="uploadbtn_popup" style="padding-top: 5px;z-index: 999;display:none;">
        <div  class="col-sm-12" style="padding:0">
            <form  class="col-sm-12 form-inline" enctype="multipart/form-data" id="myform"  >
                <div class="col-sm-12" style="padding:0;padding-top:10px;">
                    <div class="col-sm-6  required " style="padding:0">
                        <label class="control-label" style="font-weight:450;padding-top: 5px;" for="sel1">Select Company: &nbsp; </label>
                        <input id="dropdownlistComplianceType1" data-placeholder="Type" style="width:172px;">
                        <p id="warn_msg" style="color:red;display:none">* Please Select Company</p>
                        
                        <br />
                        <br />
                        <label class="control-label" style="font-weight:450;padding-top: 5px;" for="sel1">Upload File: &nbsp; </label>
                        <input type="file" id="input_file" name="files" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" onchange="return fileValidation()" required ="true" data-required-msg="Please enter a text." />
                        <p style="color:red">* Please Upload .xls file</p>
                    </div>
                    <div class="col-sm-6  required " style="padding:0">
                        <label class="control-label" style="font-weight:450;padding-top: 5px;margin-left: 25%;" for="sel1">Sample Excel: &nbsp; </label>
                        <a href="https://avacdnvid.azureedge.net/pdf/Sample_Report_Data.xls" style="float:unset;" class="btn btn-default" download ="true"><img src="../../Images/Excel _icon.png"/></a> 
                    </div>
                 </div>
                <div class="pt-5 text-center" id ="loading" style="display:none">
                    <img src = "https://cdn-static.teamleaseregtech.com/static/images/loader.gif" id="loader" alt="Loading" style="float:left; margin-top:2em">
                </div>
                 <button id="saveBtn1" style="margin-top: 20%; margin-left:-6%" type="submit" onclick="saveDetails1(event)" class="btn btn-primary" disabled>Submit</button>
              </form>
              <label class="control-label" id="checkA" style="margin-left:13%;margin-top:-9%;font-weight:450;color:red;"><input type="checkbox" id="showcheckbox1" value="1" onclick="showBtn()"> Please check the data before uploading, once the data is uploaded it can not be changed.</label>
            </div>
        </div>
        <script type="text/javascript">
            $(document).ready(function () {


                var xx = $('#dropdownlistComplianceType').val();
                //alert(xx)

                $("#grid").kendoGrid({

                    dataSource: {
                        transport: {
                            read: '<%=ConfigurationManager.AppSettings["insiderapi"]%>getinsiderdetails/?user_id=' +<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID %>+'&customer_id=' +<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID %>+'&fcompanyid=' + xx
                        },
                        schema: {
                            data: function (response) {
                                return response.data;
                            },
                            total: function (response) {
                                return response.total;
                            },
                            model: {
                                fields: {
                                    created_by: { type: "string" },
                                    UPSITypeID: { type: "string" },
                                    InformationSharedDate: { type: "date" },
                                    FirmName: { type: "string" },
                                    ConcernedDesignation: { type: "string" },
                                    EmployeeID: { type: "string" },
                                    IdType: { type: "string" },
                                    SharedWithType: { type: "string" },
                                    Reason: { type: "string" },
                                }
                            }
                        },
                        pageSize: 7
                    },
                    sortable: {
                        mode: "single",
                        allowUnsort: false
                    },
                    //height: 550,
                    //toolbar: ["search"],
                    filterable: true,
                    pageable: {
                        refresh: true,
                        pageSizes: true,
                        buttonCount: 5
                    },
                    columns: [{
                        template: "<div class='customer-name'>#: created_by #</div>",
                        field: "created_by",
                        title: "Name",
                        //locked: true,
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        },
                        width: 140
                    }, {
                        field: "UPSITypeID",
                        title: "UPSI Type",
                        // locked: true,
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        },
                        width: 140
                    }, {
                        field: "InformationType",
                        title: "Information Type",
                        filterable: false,
                        width: 140
                    }, {

                        field: "InformationSharedDate",
                        title: "Shared Date",
                        format: "{0:dd/MM/yyyy}",
                        filterable: false,
                        width: 150

                    }, {

                        field: "InformationSharedTime",
                        title: "Shared Time",
                        filterable: false,
                        width: 140

                    }, {

                        field: "SharedWithType",
                        title: "Shared With",
                        filterable: false,
                        width: 150

                    }, {

                        field: "IdType",
                        title: "ID Name",
                        filterable: false,
                        width: 150

                    }
                        , {

                        field: "EmployeeID",
                        title: "ID Code",
                        filterable: false,
                        width: 150

                    }, {

                        field: "ConcernedDesignation",
                        title: "Designation",
                        filterable: false,
                        width: 150

                    }, {

                        field: "FirmName",
                        title: "Company/Firm",
                        filterable: false,
                        width: 150

                    }, {

                        field: "Reason",
                        title: "Remark",
                        filterable: false,
                        width: 150

                    }]
                });


            });
        </script>


    <script type="text/javascript">
        function setAuditorRelation(cid, firm) {

            $.ajax({
                type: 'post',
                url: "<%=ConfigurationManager.AppSettings["insiderapi"]%>getfirmpcspecific/?customer_id=" +<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID %>,
                data: { "company": cid, "firm": firm },
                success: function (result) {
                    //alert(JSON.stringify(result[0].AuditorType));

                    if (result.length > 0) {
                        $("#dropdownauditorrelation").val(result[0].AuditorType);
                        // var xxdropdownlist1 = $("#dropdownauditorrelation");
                        //var xxdropdownlist1 = $("#dropdownauditorrelation").data("kendoTextBox")
                        // xxdropdownlist1.value(result[0].AuditorType);

                        // xxdropdownlist1.enable(false);

                    }


                },
                error: function (e, t) { alert('something went wrong'); }
            });
        };

        function openPoPup() {
            $('#videoModal1').modal('show');
        }
    </script>
    
    <script type="text/javascript">
        $(document).ready(function () {


            // create DatePicker from input HTML element
            $("#datepicker").kendoDatePicker();
            var todayDate = new Date();
            //$('#datepicker').data("kendoDatePicker").value(todayDate);
            $('#datepicker').kendoDatePicker({
                value: todayDate,
                max: todayDate,
                format: "dd/MM/yyyy"
            }).data('kendoDatePicker');
            $("#datepicker").closest("span.k-datepicker").width('40%');
            //$("#pagetype").text("My Insider Details > Historical Details");
            $("#pagetype").html("<p>My Insider Details <span style='color: #1fd9e1;'> ></span> Historical Details</p>");
            $("#dropdownlistComplianceType").kendoDropDownList({
                dataTextField: "Name",
                dataValueField: "Name",

                dataSource: {
                    transport: {
                        read: {
                            url: "<%=ConfigurationManager.AppSettings["insiderapi"]%>getcompanyall/?customer_id=<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID %>",
                        }
                    }
                },

                change: function (e) {
                    var value = this.value();
                    //alert(value)
                    var dataSource = new kendo.data.DataSource({

                        transport: {
                            read: '<%=ConfigurationManager.AppSettings["insiderapi"]%>getinsiderdetails/?customer_id=' +<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID %>+"&user_id=" +<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID %>+'&fcompanyid=' + this.value()
                        },
                        schema: {
                            data: function (response) {
                                return response.data;
                            },
                            total: function (response) {
                                return response.total;
                            },
                            model: {
                                fields: {
                                    created_by: { type: "string" },
                                    UPSITypeID: { type: "string" },
                                    InformationSharedDate: { type: "date" },
                                    InformationSharedTime: { type: "string" },
                                    FirmName: { type: "string" },
                                    ConcernedDesignation: { type: "string" },
                                    EmployeeID: { type: "string" },
                                    IdType: { type: "string" },
                                    SharedWithType: { type: "string" },
                                    Reason: { type: "string" },
                                }
                            }
                        },
                        pageSize: 7


                    });
                    var grid = $("#grid").data("kendoGrid");
                    grid.setDataSource(dataSource);
                    // Use the value of the widget
                }
            });
            $("#dropdownlistComplianceType").data('kendoDropDownList').select(0);
            $("#dropdownlistComplianceType1").kendoDropDownList({
                dataTextField: "Name",
                dataValueField: "NAME",
                dataSource: {
                    transport: {
                        read: {
                            url: "<%=ConfigurationManager.AppSettings["insiderapi"]%>getcompany/?customer_id=<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID %>",
                        }
                    }
                },
                optionLabel: {
                    Name: "Select Company...",
                    NAME: "none"
                },
                dataBound: function (e) {
                    loadfirm();
                }
            });


            $("#dropdownUPSI").kendoDropDownList({
                dataTextField: "UPSIName",
                dataValueField: "ID",
                dataSource: {
                    transport: {
                        read: {
                            url: "<%=ConfigurationManager.AppSettings["insiderapi"]%>getupsi/?customer_id=<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID %>",
                        }
                    }
                }
            });
            $("#dropdownShared").kendoDropDownList({
                placeholder: "",
                dataTextField: "text",
                dataValueField: "value",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                dataSource: [
                    { text: "Shared Internally", value: "1" },
                    { text: "Auditor", value: "2" },
                    { text: "Other", value: "3" }
                ],
                index: 0,
                change: function (e) {
                    $('#checkA').css('margin-left', '15px');
                    $('#checkA').css('margin-top', '-60px');
                    if (e.sender.value() == 2) {
                        loadfirm()
                        $('#internalType').hide();
                        $('#otherType').hide();
                        $('#internalOtherType').hide();
                        $('#saveBtn').css('margin-left', '0');
                        $('#saveBtn').css('margin-top', '86px');
                        $('#checkA').css('margin-left', '15px');
                        $('#checkA').css('margin-top', '-60px');
                        /*
                        $('#reasondiv').css('margin-top', 14);
                        $('#reasondiv').css('margin-left', '-61%');
                        $('#saveBtn').css('margin-top', 220);
                        $('#saveBtn').css('margin-left', '-61.4%');
                        */
                        $('#auditorType').show();

                    } else if (e.sender.value() == 3) {
                        $('#auditorType').hide();
                        $('#internalType').hide();

                        $('#internalOtherType').hide();
                        /*
                        $('#reasondiv').css('margin-top', '-30px');
                        $('#reasondiv').css('margin-left', 0);
                        $('#saveBtn').css('margin-top', 50);
                        $('#saveBtn').css('margin-left', '1.4%');
                        */
                        $('#saveBtn').css('margin-left', '0');
                        $('#saveBtn').css('margin-top', '86px');
                        $('#checkA').css('margin-left', '15px');
                        $('#checkA').css('margin-top', '-60px');
                        $('#otherType').show();
                    } else {
                        $('#auditorType').hide();
                        $('#otherType').hide();
                        /*
                        $('#reasondiv').css('margin-top', 0);
                        $('#reasondiv').css('margin-left', 0);
                        $('#saveBtn').css('margin-left', '-61.4%');
                        $('#saveBtn').css('margin-top', 243);
                        */
                        $('#saveBtn').css('margin-left', '0');
                        $('#saveBtn').css('margin-top', '86px');
                        $('#checkA').css('margin-left', '15px');
                        $('#checkA').css('margin-top', '-60px');
                        $('#internalType').show();

                        $('#internalOtherType').show();
                    }

                }

            });

            $("#dropdownName").kendoDropDownList({
                dataTextField: "Name",
                dataValueField: "ID",
                dataSource: {
                    transport: {
                        read: {
                            url: "<%=ConfigurationManager.AppSettings["insiderapi"]%>getuserp/?customer_id=<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID %>",
                        }
                    }
                }
            });

            $("#dropdownCName").kendoDropDownList({
                dataTextField: "Name",
                dataValueField: "ID",
                dataSource: {
                    transport: {
                        read: {
                            url: "<%=ConfigurationManager.AppSettings["insiderapi"]%>getuserpother/?customer_id=<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID %>",
                        }
                    }
                },
                change: function (e) {
                    if (e.sender.value() == -1) {
                        $('#internalOtherType').show();
                    } else {
                        $('#internalOtherType').hide();
                    }
                }
            });
            $("#dropdownlistCompany").kendoDropDownList({
                dataTextField: "Name",
                dataValueField: "ID",
                dataSource: {
                    transport: {
                        read: {
                            url: "<%=ConfigurationManager.AppSettings["insiderapi"]%>getcompp/?customer_id=<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID %>",
                        }
                    }
                }
            });
            var dropdownlist1 = $("#dropdownName").data("kendoDropDownList");
            dropdownlist1.value(<% =com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID %>);
            <%    if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.Role != "HDCS") {  %>
            dropdownlist1.enable(false);


            <% } %>
            /*
            $("#dropdownlistComplianceType").kendoDropDownList({
                placeholder: "Select Company",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                dataTextField: "ProductName",
                dataValueField: "ProductID",
                dataSource: {
                    transport: {
                        read: {
                            dataType: "jsonp",
                            url: "https://demos.telerik.com/kendo-ui/service/Products",
                        }
                    }
                }
                index: 0,
                change: function (e) {
                    DataBindDaynamicKendoGriddMain();
                }
            });
            */

        });
        function DataBindDaynamicKendoGriddMain() {
            alert('coming3');
        }
    </script>
    
    <script>
        function DownloadExcl() {
            // http://localhost:8000/api/downloadexcel/?customer_id=1167
            var xx = $('#dropdownlistComplianceType').val();

            var user_id = "<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID %>";
            user_id = user_id.trim();
            console.log("userid:::",user_id)

            window.location = "<%=ConfigurationManager.AppSettings["insiderapi"]%>downloadhistoricalexcel/?user_id=" + user_id + "&customer_id=" +<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID %>+'&fcompanyid=' + xx;
        }
        
    </script>

    <script>

        function OpenAdvanceSearch(e) {

            var myWindowAdv = $("#divAdvanceSearchModel");

            function onClose() {

            }

            myWindowAdv.kendoWindow({
                width: "85%",
                height: "65%",
                title: "Add New ",
                visible: false,
                actions: [
                    //"Pin",
                    //"Minimize",
                    "Maximize",
                    "Close"
                ],
                close: onClose
            });
            $("#divAdvanceSearchModel").data("kendoWindow").wrapper.addClass("myKendoCustomClass");

            myWindowAdv.data("kendoWindow").center().open();
            e.preventDefault();
            return false;
        }

        function OpenAdvanceSearchFilter(e) {
            $('#divAdvanceSearchFilterModel').modal('show');
            e.preventDefault();
            return false;
        }
    </script>
    <script>

        function UploadBtn(e) {

            var myWindowAdv = $("#uploadbtn_popup");

            function onClose() {

            }

            myWindowAdv.kendoWindow({
                width: "53%",
                height: "38%",
                title: "Upload File ",
                visible: false,
                actions: [
                    //"Pin",
                    //"Minimize",
                    "Maximize",
                    "Close"
                ],
                close: onClose
            });
            $("#uploadbtn_popup").data("kendoWindow").wrapper.addClass("myKendoCustomClass");

            myWindowAdv.data("kendoWindow").center().open();
            e.preventDefault();
            return false;
        }

    </script>
    <script>
        function loadfirm() {
            var comid = $('#dropdownlistComplianceType1').val();
            //alert('comid',comid)
            
            $("#dropdownFirmName").kendoDropDownList({
                dataTextField: "FirmName",
                dataValueField: "FirmName",
                dataSource: {
                    transport: {
                        read: {
                            url: "<%=ConfigurationManager.AppSettings["insiderapi"]%>getfirmpc/?customer_id=" +<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID %>+"&company=" + comid,
                        }
                    }
                },
                dataBound: function (e) {
                    loadauditor();
                    
                },
                change: function (e) {
                    //alert(e.sender.value());
                    var adropdownlist1 = $("#dropdownAuditor").data("kendoDropDownList");
                    var xdataSource = new kendo.data.DataSource({
                        transport: {
                            read: "<%=ConfigurationManager.AppSettings["insiderapi"]%>getauditordetailfirm/?customer_id=<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID %>" + "&firm=" + e.sender.value(),
                        }
                    });
                    adropdownlist1.setDataSource(xdataSource);
                    var comid = e.sender.value();
                    var comid1 = $('#dropdownlistComplianceType1').val();
                    setAuditorRelation(comid1, comid);
                    /*
                    if (e.sender.value() == -1) {
                    }
                    */
                }
            });

        }
    </script>
     <script>
         function showBtn() {
             if ($("#showcheckbox1").is(':checked')) {
                 console.log("checkked")
                 $("#saveBtn1").removeAttr("disabled");
             } else {
                 $("#saveBtn1").attr("disabled", true);
             }
         }
     </script>
    <script> 
        function fileValidation() {
            var fileInput =
                document.getElementById('input_file');

            var filePath = fileInput.value;

            // Allowing file type 
            var allowedExtensions =
                /(\.xls)$/i;

            if (!allowedExtensions.exec(filePath)) {
                alert('Invalid file type, Select: .xls File');
                fileInput.value = '';
                return false;
            }
        }
    </script>
    
    <script type="text/javascript">
        const input_file = document.getElementById("input_file");
        function saveDetails1(e) {
            e.preventDefault();
            $("#loading").show()
            const formData = new FormData();
           // var user_id = $("#dropdownName").val();
            var select_company = $("#dropdownlistComplianceType1").val();
            if (select_company == "none") {
                $("#warn_msg").show();
                setTimeout(function () {
                    $('#warn_msg').hide();
                    $("#loading").hide();
                }, 3000);
                return false;
            }
            
            var customer_id = <%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID %>;
            //console.log(customer_id);
            //console.log(select_company);
            //console.log(input_file.files);

            formData.append('input_file', input_file.files[0]);
            formData.append('customer_id', customer_id);
            formData.append('select_company', select_company);

            $.ajax({
                type: 'post',
                processData: false,
                contentType: false,
                cache: false,
                url: '<%=ConfigurationManager.AppSettings["insiderapi"]%>importinsiderdetails/',
                //data: { "file_name": file_name, "user_id": user_id, "customer_id": customer_id, },
                data: formData,

                success: function (result) {
                    if (result["status"] == "success") {
                        $("#loading").hide()
                        alert("Data Added Successfully");
                        location.reload();
                    } else {
                        $("#loading").hide();
                        alert('Please fill the fileds correctly')
                    }

                },
                error: function (e, t) { alert('something went wrong'); }
            });

                return false;
            }
    </script>

</asp:Content>

