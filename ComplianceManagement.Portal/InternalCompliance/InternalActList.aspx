﻿<%@ Page Title="Act List" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true"
    CodeBehind="InternalActList.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.InternalCompliance.InternalActList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   
    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0;
                right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px;
                    position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upActList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <table width="100%">
                <tr>
                    <td align="right">
                     
                    </td >
                     <td align="right" style="width:20%">
                      
                    </td>
                    <td align="right" style="width:25%">
                       
                    </td>
                    <td class="newlink" align="right">
                        <asp:LinkButton Text="Add New" runat="server" ID="btnAddAct" OnClick="btnAddAct_Click" Visible="false"/>
                    </td>
                </tr>
            </table>
            <asp:GridView runat="server" ID="grdAct" AutoGenerateColumns="false" GridLines="Vertical" OnRowDataBound="grdAct_RowDataBound"
                BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true" OnRowCreated="grdAct_RowCreated"
                CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="13" Width="100%" OnSorting="grdAct_Sorting"
                Font-Size="12px" DataKeyNames="ID" OnRowCommand="grdAct_RowCommand" OnPageIndexChanging="grdAct_PageIndexChanging">
                <Columns>
                     <asp:BoundField DataField="ID" HeaderText="Act ID" ItemStyle-VerticalAlign="Top" ItemStyle-Width="10px" />
                    <asp:TemplateField HeaderText ="Name" ItemStyle-Height="25px"  HeaderStyle-Height="20px" SortExpression="Name">
                        <ItemTemplate>
                            <div style="overflow:hidden;text-overflow:ellipsis;white-space:nowrap;width:400px">
                                <asp:Label runat="server" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                            </div>
                        </ItemTemplate>
	                </asp:TemplateField>
                    <asp:TemplateField ItemStyle-Width="60px" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <asp:LinkButton ID="lbtEdit" runat="server" CommandName="EDIT_ACT" CommandArgument='<%# Eval("ID") %>'><img src="../Images/edit_icon.png" alt="Edit Act" title="Edit Act" /></asp:LinkButton>
                        <asp:LinkButton ID="lbtDelete" runat="server" CommandName="DELETE_ACT" CommandArgument='<%# Eval("ID") %>'
                            OnClientClick="return confirm('Are you certain you want to delete this act?');"><img src="../Images/delete_icon.png" alt="Delete Act" title="Delete Act" /></asp:LinkButton>
                             
                    </ItemTemplate>
                    <HeaderTemplate>
                    </HeaderTemplate>
                    <HeaderStyle HorizontalAlign="Left" />
                </asp:TemplateField>
                </Columns>
                <FooterStyle BackColor="#CCCC99" />
                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium"/>
                <pagersettings position="Top" />
                <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                <AlternatingRowStyle BackColor="#E6EFF7" />
                <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                <EmptyDataTemplate>
                    No Records Found.
                </EmptyDataTemplate>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div id="divActDialog">
        <asp:UpdatePanel ID="upAct" runat="server" UpdateMode="Conditional" OnLoad="upAct_Load">
            <ContentTemplate>
                <div style="margin: 5px">
                    <div style="margin-bottom: 4px">
                        <asp:ValidationSummary runat="server" CssClass="vdsummary"
                            ValidationGroup="ActValidationGroup" />
                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                            ValidationGroup="ActValidationGroup" Display="None" />
                    </div>
                   
                    <div style="margin-bottom: 7px"><label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Name</label>
                        <asp:TextBox runat="server" ID="tbxName" Style="height: 30px; width: 390px;" MaxLength="100" ToolTip="" TextMode="MultiLine"/>
                        <asp:RequiredFieldValidator ErrorMessage="Name can not be empty." ControlToValidate="tbxName"
                            runat="server" ValidationGroup="ActValidationGroup" Display="None" />
                    </div>
                    <div style="margin-bottom: 7px; margin-left: 152px; margin-top:10px">
                        <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="button"
                            ValidationGroup="ActValidationGroup" />
                        <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="button" OnClientClick="$('#divActDialog').dialog('close');" />
                    </div>

                     <div style="margin-bottom: 7px; margin-left: 10px; margin-top: 30px;">

                         <p  style="color:red;"><strong>Note:</strong> (*) Fields Are Compulsary</p>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

     <script type="text/javascript">
         $(function () {
             $('#divActDialog').dialog({
                 height: 400,
                 width: 600,
                 autoOpen: false,
                 draggable: true,
                 title: "Act",
                 open: function (type, data) {
                     $(this).parent().appendTo("form");
                 }
             });
         });

         function initializeCombobox() {
         }

         function initializeRadioButtonsList(controlID) {
             $(controlID).buttonset();
         }
    </script>
    <script type = "text/javascript" >
        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);
        window.onunload = function () { null };
</script>
</asp:Content>
