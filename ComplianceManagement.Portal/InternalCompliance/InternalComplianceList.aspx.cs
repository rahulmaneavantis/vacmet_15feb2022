﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Text;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business;
using System.Text.RegularExpressions;
using System.Globalization;
using System.IO;

using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Data;

namespace com.VirtuosoITech.ComplianceManagement.Portal.InternalCompliance
{

    public partial class InternalComplianceList : System.Web.UI.Page
    {
        static int GComplianceID;
        public static int UserID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {      
                     UserID = Convert.ToInt32(AuthenticationHelper.UserID);
                     BindCategories(ddlICategory);
                    BindIType();
                    BindICategories(ddlComplinceCatagory);
                    BindTypes();  
                    BindDueDates();
                    BindDueDays();
                    BindFrequencies();
                    BindFilterFrequencies();
                    BindCompliances();  
                    divAct.Visible = false;
                    CompareValidator1.Visible = false;
                    BindCustomers(UserID); 
                if (AuthenticationHelper.Role!="IMPT")
                      {
                         if (AuthenticationHelper.CustomerID == 63)
                         {
                        BindActs();
                        divAct.Visible = true;
                        CompareValidator1.Visible = true;
                         }
                }
                if(AuthenticationHelper.Role=="CADMN")
                {
                    btnAddCompliance.Visible = true;
                }
                if (AuthenticationHelper.Role == "MGMT")
                {
                    btnAddCompliance.Visible = true;
                }
            }
        }
        private void BindCustomers(int UserID)
        {
            try
            {
                ddlCustomer.DataTextField = "Name";
                ddlCustomer.DataValueField = "ID"; 
                if (AuthenticationHelper.Role == "IMPT")
                {
                    ddlCustomer.DataSource = Assigncustomer.GetAllCustomer(UserID);
                    ddlCustomer.DataBind();
                    ddlCustomer.Items.Insert(0, new ListItem("< Select Customer >", "-1"));
                }
                else
                {
                    int customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                    ddlCustomer.DataSource = Assigncustomer.GetAllCustomerData(customerID);
                    ddlCustomer.SelectedValue = Convert.ToString(customerID);
                    ddlCustomer.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        
     
        protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {  
            BindCategories(ddlICategory);
            btnAddCompliance.Visible = true;
            BindICategories(ddlComplinceCatagory);
            BindTypes();
            BindIType();
            BindCategories(ddlICategory);
            BindDueDates();
            BindDueDays();
            BindFrequencies();
            BindFilterFrequencies();
            BindCompliances();
            upCompliancesList.Update();
        }
        protected void lbtnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("Internal Compliance Master");
                    System.Data.DataTable ExcelData = null;
                    DateTime CurrentDate = DateTime.Today.Date;
                    System.Data.DataView view = new System.Data.DataView((System.Data.DataTable)Session["grdAssignCheckListData"]);

                    ExcelData = view.ToTable("Selected", false, "ID", "IComplianceTypeName", "IComplianceCategoryName", "IShortDescription", "IuploadDoc", "Irisk", "Frequency");

                    exWorkSheet.Cells["A1"].LoadFromDataTable(ExcelData, true);


                    exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["A1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["A1"].Value = "Internal Compliance ID";
                    exWorkSheet.Cells["A1"].AutoFitColumns(20);

                    exWorkSheet.Cells["B1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["B1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["B1"].Value = "Type Name";
                    exWorkSheet.Cells["B1"].AutoFitColumns(20);

                    exWorkSheet.Cells["C1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["C1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["C1"].Value = "Category Name";
                    exWorkSheet.Cells["C1"].AutoFitColumns(30);

                    exWorkSheet.Cells["D1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["D1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["D1"].Value = "Description";
                    exWorkSheet.Cells["D1"].AutoFitColumns(50);

                    exWorkSheet.Cells["E1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["E1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["E1"].Value = "Upload Document";
                    exWorkSheet.Cells["E1"].AutoFitColumns(20); ;

                    exWorkSheet.Cells["F1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["F1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["F1"].Value = "Risk";
                    exWorkSheet.Cells["F1"].AutoFitColumns(15);

                    exWorkSheet.Cells["G1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["G1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["G1"].Value = "Frequency";
                    exWorkSheet.Cells["G1"].AutoFitColumns(15);


                    using (ExcelRange col = exWorkSheet.Cells[1, 1, 1 + ExcelData.Rows.Count, 7])
                    {
                        col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    }

                    Byte[] fileBytes = exportPackge.GetAsByteArray();
                    Response.ClearContent();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=InternalCompliance.xlsx");
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.ms-excel";
                    StringWriter sw = new StringWriter();
                    Response.BinaryWrite(fileBytes);
                    HttpContext.Current.Response.Flush(); 
                    HttpContext.Current.Response.SuppressContent = true; 
                    HttpContext.Current.ApplicationInstance.CompleteRequest(); 

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindActs()
        {
            try
            {
                int customerid = -1;
                if (AuthenticationHelper.Role == "IMPT")
                {
                    customerid = Convert.ToInt32(ddlCustomer.SelectedValue);
                }
                else
                {
                    customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }

                ddlAct.DataTextField = "Name";
                ddlAct.DataValueField = "ID";
                 
                ddlAct.DataSource = ActManagement.GetAllNVPInternalActs(customerid);
                ddlAct.DataBind();

                ddlAct.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindDueDays()
        {
            try
            {
                ddlWeekDueDay.DataTextField = "Name";
                ddlWeekDueDay.DataValueField = "ID";

                ddlWeekDueDay.DataSource = Enumerations.GetAll<Days>();
                ddlWeekDueDay.DataBind();

                ddlWeekDueDay.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnAddCompliance_Click(object sender, EventArgs e)
        {
            try
            {
                saveopo.Value = "false";
                GComplianceID = 0;
                ViewState["Mode"] = 0;
                lblErrorMassage.Text = string.Empty;
                txtShortDescription.Text = string.Empty;
                txtShortForm.Text = string.Empty;

                txtdetaileddescription.Text = string.Empty;
                tbxRequiredForms.Text = string.Empty;
               // dd.SelectedIndex = ddlcustomernew.SelectedIndex = -1;
                ddlIComplinceType.SelectedIndex = ddlICategory.SelectedIndex = -1;
                ddlComplianceType.SelectedIndex = ddlFrequency.SelectedIndex = ddlDueDate.SelectedIndex = 0;
                chkDocument.Checked = false;
                ddlAct.SelectedIndex = -1;
                //lblSampleForm.Text = "< Not selected >";
                divCompType.Visible = true;
                divScheduleType.Visible = true;
                vivWeekDueDays.Visible = false;
                ddlWeekDueDay.SelectedValue = "-1";
                //ddlComplianceType.Visible = true;
                ddlComplianceType_SelectedIndexChanged(null, null);
                rbReminderType.SelectedValue = "0";
                divForCustome.Visible = false;
                txtReminderBefore.Text = string.Empty;
                txtReminderGap.Text = string.Empty;
                ddlRiskType.SelectedIndex = -1;

                divChecklist.Visible = false;
                rbtnlstCompOcc.SelectedValue = "1";
                divOnetimeDate.Visible = false;
                divComplianceDueDays.Visible = false;
                rbReminderType.Items[0].Enabled = true;
                divTimebasedTypes.Visible = false;// added by rahul on 9 JAn 2016
                grdSampleForm.DataSource = null;
                grdSampleForm.DataBind();
                divSampleForm.Visible = false;
                upComplianceDetails.Update();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divInternalComplianceDetailsDialog\").dialog('open')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }  
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                bool chkexists = false;
                int CustomerID = -1;
                if (AuthenticationHelper.Role == "IMPT")
                {
                    CustomerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                }
                else
                {
                    CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                // int CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                var shortFormExist = false;
                if (GComplianceID != 0)
                {
                    chkexists = Business.InternalComplianceManagement.ExistsSavedCompliance(txtShortDescription.Text.Trim(), GComplianceID, CustomerID);
                    shortFormExist = Business.InternalComplianceManagement.ExistsShortDescription(txtShortForm.Text.Trim(), GComplianceID, CustomerID);
                 }
                else
                {
                    chkexists = Business.InternalComplianceManagement.ExistsR(txtShortDescription.Text.Trim(), CustomerID);
                    shortFormExist = Business.InternalComplianceManagement.ExistsShortForm(txtShortForm.Text.Trim(), CustomerID);
                }
                if (chkexists == false)
                {
                    if (shortFormExist == false)
                    {
                        int ActID = 0;
                        if (ddlAct.SelectedValue == "" || ddlAct.SelectedValue == null)
                        {
                            ActID = 0;
                        }
                        else
                        {
                            ActID = Convert.ToInt32(ddlAct.SelectedValue);
                        }

                        bool? EventFlag = null;
                        if (chkEvent.Checked == true)
                        {
                            EventFlag = true;
                        }  
                        int compliancetypeid = Convert.ToInt32(ddlIComplinceType.SelectedValue);
                        Business.Data.InternalCompliance Icompliance = new Business.Data.InternalCompliance()
                        {
                            IComplianceTypeID = compliancetypeid,
                            IComplianceCategoryID = Convert.ToInt32(ddlICategory.SelectedValue),
                            IShortDescription = txtShortDescription.Text,
                            IShortForm = txtShortForm.Text,
                            IUploadDocument = chkDocument.Checked,
                            IComplianceType = Convert.ToByte(ddlComplianceType.SelectedValue),
                            ScheduleType = Convert.ToInt32(ddlScheduleType.SelectedValue),
                            IRequiredFrom = tbxRequiredForms.Text,
                            IRiskType = Convert.ToByte(ddlRiskType.SelectedValue),
                            IComplianceOccurrence = Convert.ToInt32(rbtnlstCompOcc.SelectedValue),
                            IsDocumentRequired = chkComplianceDocumentMandatory.Checked,
                            ActID = ActID,
                            EventFlag = EventFlag,
                            IDetailedDescription = txtdetaileddescription.Text,
                        };
                        #region 
                        if (rbtnlstCompOcc.SelectedValue.ToString() == "1")
                        {
                            if (Icompliance.IComplianceType == 0 || Icompliance.IComplianceType == 2)//function based or time based
                            {
                                if (Icompliance.IComplianceType == 2)
                                {
                                    if (rbTimebasedTypes.SelectedValue.Equals("0"))
                                    {
                                        Icompliance.ISubComplianceType = 0;
                                        Icompliance.IDueDate = Convert.ToInt32(txtEventDueDate.Text);
                                    }
                                    else if (rbTimebasedTypes.SelectedValue.Equals("1"))
                                    {
                                        Icompliance.IFrequency = Convert.ToByte(ddlFrequency.SelectedValue);
                                        if (Icompliance.IFrequency == 8)
                                        {
                                            Icompliance.DueWeekDay = Convert.ToByte(ddlWeekDueDay.SelectedValue);
                                        }
                                        else
                                        {
                                            Icompliance.IDueDate = null;
                                        }
                                        Icompliance.ISubComplianceType = 1;
                                    }   
                                }
                                else
                                {
                                    if (chkEvent.Checked == false)
                                    {
                                        Icompliance.IFrequency = Convert.ToByte(ddlFrequency.SelectedValue);
                                        if (Icompliance.IFrequency == 8)
                                        {
                                            Icompliance.DueWeekDay = Convert.ToByte(ddlWeekDueDay.SelectedValue);
                                        }
                                        else
                                        {
                                            Icompliance.IDueDate = Convert.ToInt32(ddlDueDate.SelectedValue);
                                        }
                                    }
                                }
                            }
                            else  ////////////////////SACHIN added////////////////////////////////////////
                            {
                                Icompliance.CheckListTypeID = Convert.ToInt32(ddlChklstType.SelectedValue); 
                                //------checklist------
                                if (Icompliance.CheckListTypeID == 1 || Icompliance.CheckListTypeID == 2)//function based or time based
                                {
                                    if (Icompliance.CheckListTypeID == 2)//time based
                                    {    
                                        if (rbTimebasedTypes.SelectedValue.Equals("0"))//fixed gap
                                        {

                                            Icompliance.IFrequency = null;
                                            Icompliance.IDueDate = Convert.ToInt32(txtEventDueDate.Text);
                                            Icompliance.ISubComplianceType = 0;
                                        }
                                        else if (rbTimebasedTypes.SelectedValue.Equals("1"))//periodicallly based
                                        {

                                            Icompliance.IFrequency = Convert.ToByte(ddlFrequency.SelectedValue);
                                            if (Icompliance.IFrequency == 8)
                                            {
                                                Icompliance.DueWeekDay = Convert.ToByte(ddlWeekDueDay.SelectedValue);
                                            }
                                            else
                                            {
                                                Icompliance.IDueDate = null;
                                            }
                                            Icompliance.ISubComplianceType = 1;
                                        }
                                        else
                                        {
                                            Icompliance.IFrequency = Convert.ToByte(ddlFrequency.SelectedValue);
                                            if (Icompliance.IFrequency == 8)
                                            {
                                                Icompliance.DueWeekDay = Convert.ToByte(ddlWeekDueDay.SelectedValue);
                                            }
                                            else
                                            {
                                                Icompliance.IDueDate = Convert.ToInt32(txtEventDueDate.Text);
                                            }
                                            Icompliance.ISubComplianceType = 2;
                                        }
                                    }
                                    else
                                    {
                                        Icompliance.IFrequency = Convert.ToByte(ddlFrequency.SelectedValue);
                                        if (Icompliance.IFrequency == 8)
                                        {
                                            Icompliance.DueWeekDay = Convert.ToByte(ddlWeekDueDay.SelectedValue);
                                        }
                                        else
                                        {
                                            Icompliance.IDueDate = Convert.ToInt32(ddlDueDate.SelectedValue);
                                        }
                                    }
                                }
                                else
                                {
                                    string Onetimedate = Request[tbxOnetimeduedate.UniqueID].ToString().Trim();
                                    DateTime dtOneTimeDate = new DateTime();
                                    if (Onetimedate != "")
                                    {
                                        dtOneTimeDate = DateTime.ParseExact(Onetimedate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                        Icompliance.IOneTimeDate = dtOneTimeDate;
                                    }
                                }
                                ////////////////////SACHIN added////////////////////////////////////////

                            }
                        }
                        else
                        {
                            string Onetimedate = Request[tbxOnetimeduedate.UniqueID].ToString().Trim();
                            DateTime dtOneTimeDate = new DateTime();
                            if (Onetimedate != "")
                            {
                                dtOneTimeDate = DateTime.ParseExact(Onetimedate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                Icompliance.IOneTimeDate = dtOneTimeDate;
                            }
                        }
                        List<InternalComplianceForm> ComplianceForms = new List<InternalComplianceForm>();
                        InternalComplianceForm form = null;

                        if (chkDocument.Checked)
                        {
                            if (fuSampleFile.PostedFiles.Count() > 0)//if (fuSampleFile.FileBytes != null && fuSampleFile.FileBytes.LongLength > 0)
                            {
                                HttpFileCollection fileCollection = Request.Files;
                                if (fileCollection.Count > 0)
                                {
                                    for (int i = 0; i < fileCollection.Count; i++)
                                    {
                                        HttpPostedFile uploadfile = null;
                                        uploadfile = fileCollection[i];
                                        string fileName = uploadfile.FileName;
                                        if (!string.IsNullOrEmpty(fileName))
                                        {
                                            Stream fs = uploadfile.InputStream;
                                            BinaryReader br = new BinaryReader(fs);
                                            Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                                            form = new InternalComplianceForm()
                                            {
                                                Name = fileName,
                                                FileData = bytes,
                                            };
                                            ComplianceForms.Add(form);
                                        }
                                    }
                                }
                            }
                        }

                        Icompliance.IReminderType = Convert.ToByte(rbReminderType.SelectedValue);
                        if (rbReminderType.SelectedValue.Equals("1"))
                        {

                            Icompliance.IReminderBefore = Convert.ToInt32(txtReminderBefore.Text);
                            Icompliance.IReminderGap = Convert.ToInt32(txtReminderGap.Text);
                        }


                        if ((int)ViewState["Mode"] == 1)
                        {
                            Icompliance.ID = Convert.ToInt32(ViewState["ComplianceID"]);
                        }
                        if (AuthenticationHelper.Role == "IMPT")
                        {
                            Icompliance.CustomerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                        }
                        else
                        {
                            Icompliance.CustomerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID;
                        }

                        #endregion
                        if ((int)ViewState["Mode"] == 0)
                        {
                            Icompliance.CreatedBy = AuthenticationHelper.UserID;
                            Icompliance.UpdatedBy = AuthenticationHelper.UserID;
                            Business.ComplianceManagement.CreateInternalMultiple(Icompliance, ComplianceForms, Convert.ToInt32(Icompliance.CustomerID));
                        }
                        else if ((int)ViewState["Mode"] == 1)
                        {
                            bool chkexistsComplianceschedule = Business.ComplianceManagement.ExistsInternalComplianceScheduleAssignment(Convert.ToInt64(ViewState["ComplianceID"]));
                            if (chkexistsComplianceschedule == true)
                            {

                                Icompliance.UpdatedBy = AuthenticationHelper.UserID;
                                Business.ComplianceManagement.UpdateIFInstanceIsCreated(Icompliance, ComplianceForms);
                            }
                            else
                            {
                                Icompliance.UpdatedBy = AuthenticationHelper.UserID;
                                Business.ComplianceManagement.UpdateMultiple(Icompliance, ComplianceForms);
                            }
                        }
                        BindCompliances();

                        var complianceForm = Business.ComplianceManagement.GetInternalComplianceFormByIDMultiple(Icompliance.ID);

                        if (complianceForm.Count > 0)
                        {
                            hdnFile.Value = Convert.ToString(Icompliance.ID);
                            divSampleForm.Visible = true;
                            grdSampleForm.DataSource = complianceForm;
                            grdSampleForm.DataBind();
                        }
                        else
                        {
                            divSampleForm.Visible = false;
                        }
                        var details = Business.ComplianceManagement.GetCustomerID(Convert.ToInt32(AuthenticationHelper.CustomerID));
                        if (details != null)
                        {
                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                            {

                                int CID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                                InternalComplianceInfoByCustomer data = (from row1 in entities.InternalComplianceInfoByCustomers
                                                                         where row1.CustomerID == CID
                                                                 && row1.ComplianceID == Icompliance.ID
                                                                         select row1).FirstOrDefault();
                                if (data != null)
                                {
                                    data.UpdatedOn = DateTime.Now;
                                    data.UpdatedBy = Convert.ToInt32(AuthenticationHelper.UserID);
                                    data.IsActive = true;
                                    data.IsDocumentCompulsory = (bool)Icompliance.IsDocumentRequired;
                                    entities.SaveChanges();
                                }
                                else
                                {
                                    InternalComplianceInfoByCustomer IC1 = new InternalComplianceInfoByCustomer()
                                    {
                                        CustomerID = CID,
                                        ComplianceID = Icompliance.ID,
                                        IsDocumentCompulsory = (bool)Icompliance.IsDocumentRequired,
                                        IsApproverCompulsory = true,
                                        CreatedOn = DateTime.Now,
                                        CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                                        UpdatedOn = DateTime.Now,
                                        UpdatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                                        IsActive = true
                                    };
                                    entities.InternalComplianceInfoByCustomers.Add(IC1);
                                    entities.SaveChanges();
                                }
                            }
                        }
                        upCompliancesList.Update();
                        saveopo.Value = "true";
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Record Saved Sucessfully.";
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Internal Compliance Short Form already present in System";
                        saveopo.Value = "true";
                    }
                }
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Internal Compliance Short Description already present in System";
                    saveopo.Value = "true";
                }  
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void OpenScheduleInformation(Business.Data.InternalCompliance compliance)
        {
            try
            {
                saveopo.Value = "true";

                ViewState["ComplianceID"] = compliance.ID;
                ViewState["Frequency"] = compliance.IFrequency.Value;
                if (compliance.IDueDate != null)
                {
                    ViewState["Day"] = compliance.IDueDate.Value;
                }

                if ((compliance.IFrequency.Value == 0 || compliance.IFrequency.Value == 1))
                    divStartMonth.Visible = false;
                else
                    divStartMonth.Visible = true;

                var scheduleList = Business.ComplianceManagement.GetInternalScheduleByIComplianceID(compliance.ID);
                if (scheduleList.Count == 0)
                {
                    Business.ComplianceManagement.GenerateScheduleForInternalCompliance(compliance.ID, compliance.ISubComplianceType);
                    scheduleList = Business.ComplianceManagement.GetInternalScheduleByIComplianceID(compliance.ID);
                }

                int step = 0;
                if (compliance.IFrequency.Value == 0)
                    step = 0;
                else if (compliance.IFrequency.Value == 1)
                    step = 2;
                else if (compliance.IFrequency.Value == 2)
                    step = 5;
                else if (compliance.IFrequency.Value == 4)
                    step = 3;
                else
                    step = 11;

                var dataSource = scheduleList.Select(entry => new
                {
                    ID = entry.ID,
                    ForMonth = entry.ForMonth,
                    ForMonthName = compliance.IFrequency.Value == 0 ? ((Month) entry.ForMonth).ToString() : ((Month) entry.ForMonth).ToString() + " - " + ((Month) ((entry.ForMonth + step) > 12 ? (entry.ForMonth + step) - 12 : (entry.ForMonth + step))).ToString(),
                    SpecialDay = Convert.ToByte(entry.SpecialDate.Substring(0, 2)),
                    SpecialMonth = Convert.ToByte(entry.SpecialDate.Substring(2, 2))
                }).ToList();

                if (divStartMonth.Visible)
                {

                    if (compliance.ISubComplianceType == 1)
                    {
                        if (dataSource.First().ForMonth == 6 || dataSource.First().ForMonth == 3)
                            ddlStartMonth.SelectedValue = Convert.ToString(1);
                        else
                            ddlStartMonth.SelectedValue = Convert.ToString(4);
                    }
                    else
                    {
                        ddlStartMonth.SelectedValue = dataSource.First().ForMonth.ToString();
                    }
                }

                repComplianceSchedule.DataSource = dataSource;
                repComplianceSchedule.DataBind();

                upSchedulerRepeter.Update();
                upComplianceScheduleDialog.Update();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenScheduleDialog", "$(\"#divComplianceScheduleDialog\").dialog('open')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlMonths_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                saveopo.Value = "true";
                var dataSource = new List<object>();
                for (int i = 0; i < repComplianceSchedule.Items.Count; i++)
                {
                    RepeaterItem entry = repComplianceSchedule.Items[i] as RepeaterItem;

                    HiddenField hdnID = ((HiddenField) entry.FindControl("hdnID"));
                    HiddenField hdnForMonth = ((HiddenField) entry.FindControl("hdnForMonth"));
                    DropDownList ddlDays = (DropDownList) entry.FindControl("ddlDays");
                    DropDownList ddlMonths = (DropDownList) entry.FindControl("ddlMonths");


                    dataSource.Add(new
                    {
                        ID = hdnID.Value,
                        ForMonth = Convert.ToByte(hdnForMonth.Value),
                        ForMonthName = ((Month) Convert.ToByte(hdnForMonth.Value)).ToString(),
                        SpecialDay = ddlDays.SelectedValue,
                        SpecialMonth = ddlMonths.SelectedValue
                    });


                    Month month = (Month) Convert.ToInt32(ddlMonths.SelectedValue);
                    int totalDays = 0;
                    switch (month)
                    {
                        case Month.February:
                            totalDays = 28;
                            break;
                        case Month.January:
                        case Month.March:
                        case Month.May:
                        case Month.July:
                        case Month.August:
                        case Month.October:
                        case Month.December:
                            totalDays = 31;
                            break;
                        case Month.April:
                        case Month.June:
                        case Month.September:
                        case Month.November:
                            totalDays = 30;
                            break;
                    }

                    var daysdataSource = new List<object>();

                    for (int j = 1; j <= totalDays; j++)
                    {
                        daysdataSource.Add(new { ID = i, Name = i.ToString() });
                    }

                    ddlDays.DataSource = daysdataSource;
                    ddlDays.DataBind();
                    upSchedulerRepeter.Update();
                }

                repComplianceSchedule.DataSource = dataSource;
                repComplianceSchedule.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlStartMonth_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Frequency frequency = (Frequency) Convert.ToByte(ViewState["Frequency"]);
                byte day = Convert.ToByte(ViewState["Day"]);
                byte startMonth = Convert.ToByte(ddlStartMonth.SelectedValue);
                byte step = 1;
                switch (frequency)
                {
                    case Frequency.Quarterly:
                        step = 3;
                        break;
                    case Frequency.FourMonthly:
                        step = 4;
                        break;
                    case Frequency.HalfYearly:
                        step = 6;
                        break;
                    case Frequency.Annual:
                        step = 12;
                        break;
                    case Frequency.TwoYearly:
                        step = 12;
                        break;
                    case Frequency.SevenYearly:
                        step = 12;
                        break;
                }

                var dataSource = new List<object>();

                for (int i = 0; i < repComplianceSchedule.Items.Count; i++)
                {
                    RepeaterItem entry = repComplianceSchedule.Items[i] as RepeaterItem;

                    HiddenField hdnID = ((HiddenField) entry.FindControl("hdnID"));
                    HiddenField hdnForMonth = ((HiddenField) entry.FindControl("hdnForMonth"));
                    DropDownList ddlDays = (DropDownList) entry.FindControl("ddlDays");
                    DropDownList ddlMonths = (DropDownList) entry.FindControl("ddlMonths");

                    int month = startMonth + (step * i) <= 12 ? startMonth + (step * i) : 1;
                    int specialMonth;
                    if (step == 12)
                    {
                        specialMonth = startMonth + (step * i);
                    }
                    else
                    {
                        if (month < 10)
                        {
                            if (month > 12)
                            {
                                specialMonth = 1;
                            }
                            else
                            {
                                if (frequency == Frequency.FourMonthly)
                                    specialMonth = startMonth + (step * i) + 4;
                                else
                                    specialMonth = startMonth + (step * i) + 6;

                            }
                        }
                        else
                        {
                            if (frequency == Frequency.FourMonthly)
                                specialMonth = 4;
                            else
                                specialMonth = startMonth + (step * i) - 6;
                        }
                    }

                    dataSource.Add(new
                    {
                        ID = hdnID.Value,
                        ForMonth = month,
                        ForMonthName = frequency == Frequency.Monthly ? ((Month) month).ToString() : ((Month) month).ToString() + " - " + ((Month) ((month + (step - 1)) > 12 ? (month + (step - 1)) - 12 : (month + (step - 1)))).ToString(),
                        SpecialDay = day,
                        SpecialMonth = specialMonth
                    });
                }

                repComplianceSchedule.DataSource = dataSource;
                repComplianceSchedule.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void repComplianceSchedule_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {

                    DropDownList ddlDays = (DropDownList) e.Item.FindControl("ddlDays");
                    DropDownList ddlMonths = (DropDownList) e.Item.FindControl("ddlMonths");

                    ScriptManager scriptManager = ScriptManager.GetCurrent(Page);
                    if (scriptManager != null)
                    {
                        scriptManager.RegisterAsyncPostBackControl(ddlMonths);
                    }

                    ddlMonths.DataSource = Enumerations.GetAll<Month>();
                    ddlMonths.DataBind();

                    int totalDays = 28;
                    int day = Convert.ToInt16(Convert.ToByte(e.Item.DataItem.GetType().GetProperty("SpecialDay").GetValue(e.Item.DataItem, null).ToString()));
                    Month month = (Month) Convert.ToByte(e.Item.DataItem.GetType().GetProperty("SpecialMonth").GetValue(e.Item.DataItem, null).ToString());
                    ddlMonths.SelectedValue = ((byte) month).ToString();

                    switch (month)
                    {
                        case Month.February:
                            totalDays = 28;
                            break;
                        case Month.January:
                        case Month.March:
                        case Month.May:
                        case Month.July:
                        case Month.August:
                        case Month.October:
                        case Month.December:
                            totalDays = 31;
                            break;
                        case Month.April:
                        case Month.June:
                        case Month.September:
                        case Month.November:
                            totalDays = 30;
                            break;
                    }

                    var dataSource = new List<object>();

                    for (int i = 1; i <= totalDays; i++)
                    {
                        dataSource.Add(new { ID = i, Name = i.ToString() });
                    }

                    ddlDays.DataSource = dataSource;
                    ddlDays.DataBind();

                    if (day > totalDays)
                    {
                        day = totalDays;
                    }

                    ddlDays.SelectedValue = day.ToString();


                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdCompliances_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                ddlAct.SelectedIndex = -1;

                if (e.CommandName.Equals("EDIT_COMPLIANCE"))
                {
                    int complianceID = Convert.ToInt32(e.CommandArgument);
                    GComplianceID = Convert.ToInt32(e.CommandArgument);
                    lblErrorMassage.Text = "";
                    var compliance = Business.ComplianceManagement.GetInternalComplianceByID(complianceID);
                  
                    if (compliance != null)
                    {
                        hdnFile.Value = "";
                        var complianceForm = Business.ComplianceManagement.GetInternalComplianceFormByIDMultiple(complianceID);
                        hdnFile.Value = Convert.ToString(complianceID);
                        ViewState["Mode"] = 1;
                        ViewState["ComplianceID"] = complianceID;
                        //ddlIComplinceType.SelectedValue =Convert.ToString(compliance.IComplianceTypeID);
                        //ddlICategory.SelectedValue = compliance.IComplianceCategoryID.ToString();

                        if (AuthenticationHelper.Role == "IMPT")
                        {
                            ddlCustomer.SelectedValue = Convert.ToString(compliance.CustomerID);
                        }
                        //   ddlIComplinceType.SelectedValue =Convert.ToString(compliance.IComplianceTypeID).ToString();  
                        ddlIComplinceType.SelectedValue = Convert.ToString(compliance.IComplianceTypeID);
                        //  ddlIComplinceType.SelectedValue = compliance.IComplianceTypeID.ToString();
                        if (compliance.IComplianceCategoryID != null)
                        {
                            ddlICategory.SelectedValue = compliance.IComplianceCategoryID.ToString();
                        }  
                        txtShortDescription.Text = compliance.IShortDescription;
                        txtShortForm.Text = compliance.IShortForm;
                        chkDocument.Checked = compliance.IUploadDocument ?? false;
                        ddlComplianceType.SelectedValue = compliance.IComplianceType.ToString();
                        ddlScheduleType.SelectedValue = compliance.ScheduleType.ToString();
                        tbxRequiredForms.Text = compliance.IRequiredFrom;
                        ddlRiskType.SelectedValue = compliance.IRiskType.ToString();
                        txtdetaileddescription.Text = compliance.IDetailedDescription;

                        if (compliance.ActID != null)
                        {
                            ddlAct.SelectedValue = compliance.ActID.ToString();
                        }
                        if (compliance.IsDocumentRequired != true)
                        {
                            chkComplianceDocumentMandatory.Checked = false;
                        }
                        else
                        {
                            chkComplianceDocumentMandatory.Checked = true;
                        }

                        ddlComplianceType_SelectedIndexChanged(null, null);

                        if (compliance.EventFlag == true)
                        {
                            chkEvent.Checked = true;
                        }
                        else
                        {
                            chkEvent.Checked = false;
                        }

                        chkEvent_CheckedChanged(sender, e);

                        if (complianceForm.Count > 0)
                        {
                            divSampleForm.Visible = true;
                            grdSampleForm.DataSource = complianceForm;
                            grdSampleForm.DataBind();
                        }
                        else
                        {
                            divSampleForm.Visible = false;
                        }
                        if (compliance.IComplianceOccurrence == 1)
                        {
                            divCompType.Visible = true;
                            divScheduleType.Visible = true;
                            divScheduleType.Visible = true;
                            divTimebasedTypes.Visible = false;                            
                            divFunctionBased.Visible = true;
                            divOnetimeDate.Visible = false;
                            rbtnlstCompOcc.SelectedValue = "1";
                            rbReminderType.Items[0].Enabled = false;
                            if (compliance.IComplianceType == 0 || compliance.IComplianceType == 2) //function based or time based
                            {

                                ddlFrequency.SelectedValue = (compliance.IFrequency ?? 0).ToString();

                                if (ddlFrequency.SelectedValue == "7" || ddlFrequency.SelectedValue == "8")
                                {
                                    vivDueDate.Visible = false;
                                }
                                else
                                {
                                    vivDueDate.Visible = true;
                                }
                                vivWeekDueDays.Visible = false;
                                if (ddlFrequency.SelectedValue == "8")
                                {
                                    ddlWeekDueDay.SelectedValue = compliance.DueWeekDay.ToString();
                                    vivWeekDueDays.Visible = true;
                                }
                                else
                                {
                                    vivWeekDueDays.Visible = false;
                                }

                                if (compliance.IDueDate != null && compliance.ISubComplianceType == 0 && compliance.ISubComplianceType == 2)
                                {
                                    if (ddlFrequency.SelectedValue == "7" || ddlFrequency.SelectedValue == "8")
                                    {
                                        vivDueDate.Visible = false;
                                    }
                                    else
                                    {
                                        vivDueDate.Visible = true;
                                        ddlDueDate.SelectedValue = (compliance.IDueDate ?? 0).ToString();
                                    }
                                }
                                else
                                {
                                    if (compliance.IDueDate != null && compliance.IDueDate != -1)
                                    {

                                        if (ddlFrequency.SelectedValue == "7" || ddlFrequency.SelectedValue == "8")
                                        {
                                            vivDueDate.Visible = false;
                                        }
                                        else
                                        {
                                            vivDueDate.Visible = true;
                                            if (compliance.IDueDate <= 31)
                                            {
                                                ddlDueDate.SelectedValue = (compliance.IDueDate ?? 0).ToString();
                                            }
                                        }
                                    }

                                }
                                if (compliance.IComplianceType == 2)
                                {
                                    divTimebasedTypes.Visible = true;
                                    if (compliance.ISubComplianceType == 0)
                                    {
                                        divNonEvents.Visible = false;
                                        divFrequency.Visible = false;
                                        vivDueDate.Visible = false;

                                        cvfrequency.Enabled = false;

                                        divComplianceDueDays.Visible = true;
                                        txtEventDueDate.Text = (compliance.IDueDate ?? 0).ToString();

                                    }
                                    else if (compliance.ISubComplianceType == 1)
                                    {
                                        divNonEvents.Visible = true;
                                        divFrequency.Visible = true;
                                        vivDueDate.Visible = false;

                                        cvfrequency.Enabled = true;

                                        divComplianceDueDays.Visible = false;
                                        txtEventDueDate.Text = null;

                                    }
                                    else
                                    {
                                        divNonEvents.Visible = true;
                                        divFrequency.Visible = true;
                                        vivDueDate.Visible = false;

                                        cvfrequency.Enabled = true;

                                    }
                                    rbTimebasedTypes.Visible = true;
                                    rbTimebasedTypes.SelectedValue = Convert.ToString(compliance.ISubComplianceType);
                                }

                            }

                            else// checklist
                            {
                                ddlChklstType.SelectedValue = Convert.ToString(compliance.CheckListTypeID);
                                //------checklist------
                                if (compliance.CheckListTypeID == 1 || compliance.CheckListTypeID == 2)//function based or time based checklist
                                {
                                    divOnetimeDate.Visible = false;
                                    divFunctionBased.Visible = true;
                                    divFrequency.Visible = true;
                                    divComplianceDueDays.Visible = false;
                                    divNonEvents.Visible = true;
                                    txtEventDueDate.Text = string.Empty;
                                    ddlFrequency.SelectedValue = (compliance.IFrequency ?? 0).ToString();

                                    if (compliance.IDueDate != null && compliance.ISubComplianceType == 0 && compliance.ISubComplianceType == 2)
                                    {
                                        if (ddlFrequency.SelectedValue == "7" || ddlFrequency.SelectedValue == "8")
                                        {
                                            vivDueDate.Visible = false;
                                        }
                                        else
                                        {
                                            vivDueDate.Visible = true;
                                            ddlDueDate.SelectedValue = (compliance.IDueDate ?? 0).ToString();
                                        }
                                        vivWeekDueDays.Visible = false;
                                        if (ddlFrequency.SelectedValue == "8")
                                        {
                                            ddlWeekDueDay.SelectedValue = compliance.DueWeekDay.ToString();
                                            vivWeekDueDays.Visible = true;
                                        }
                                        else
                                        {
                                            vivWeekDueDays.Visible = false;
                                        }

                                    }
                                    else
                                    {
                                        if (compliance.IDueDate != null && compliance.IDueDate != -1)
                                        {

                                            if (ddlFrequency.SelectedValue == "7" || ddlFrequency.SelectedValue == "8")
                                            {
                                                vivDueDate.Visible = false;
                                            }
                                            else
                                            {
                                                vivDueDate.Visible = true;
                                            }

                                            vivWeekDueDays.Visible = false;
                                            if (ddlFrequency.SelectedValue == "8")
                                            {
                                                ddlWeekDueDay.SelectedValue = compliance.DueWeekDay.ToString();
                                                vivWeekDueDays.Visible = true;
                                            }
                                            else
                                            {
                                                vivWeekDueDays.Visible = false;
                                            }

                                            if (compliance.IDueDate <= 31)
                                            {
                                                ddlDueDate.SelectedValue = (compliance.IDueDate ?? 0).ToString();
                                            }
                                            else
                                            {
                                                txtEventDueDate.Text = (compliance.IDueDate ?? 0).ToString();
                                            }
                                        }
                                    }
                                    if (compliance.CheckListTypeID == 2)
                                    {
                                        divTimebasedTypes.Visible = true;
                                        if (compliance.ISubComplianceType == 0)//fixed gap -- time based
                                        {
                                            divNonEvents.Visible = false;
                                            divFunctionBased.Visible = false;
                                            divFrequency.Visible = false;
                                            vivDueDate.Visible = false;
                                            rfvEventDue.Enabled = false;
                                            cvfrequency.Enabled = false;
                                            divComplianceDueDays.Visible = true;
                                            rgexEventDueDate.Enabled = true;
                                            txtEventDueDate.Text = (compliance.IDueDate ?? 0).ToString();
                                        }
                                        else if (compliance.ISubComplianceType == 1)
                                        {
                                            divNonEvents.Visible = true;
                                            divFunctionBased.Visible = true;
                                            divFrequency.Visible = true;                                            
                                            vivDueDate.Visible = false;
                                            rfvEventDue.Enabled = false;
                                            cvfrequency.Enabled = true;
                                            divComplianceDueDays.Visible = false;
                                            rgexEventDueDate.Enabled = false;
                                        }
                                        else
                                        {
                                            divNonEvents.Visible = true;
                                            divFrequency.Visible = true;
                                            vivDueDate.Visible = false;
                                            rfvEventDue.Enabled = false;
                                            cvfrequency.Enabled = true;
                                            divComplianceDueDays.Visible = true;
                                            rgexEventDueDate.Enabled = true;
                                            txtEventDueDate.Text = (compliance.IDueDate ?? 0).ToString();
                                        }

                                        rbTimebasedTypes.SelectedValue = Convert.ToString(compliance.ISubComplianceType);
                                    }
                                    // }

                                }
                                else//one time checklist
                                {
                                                                      
                                    divComplianceDueDays.Visible = false;
                                    divNonEvents.Visible = true;                                    
                                    txtEventDueDate.Text = string.Empty;

                                    divTimebasedTypes.Visible = false;
                                    divFunctionBased.Visible = false;
                                    divOnetimeDate.Visible = true;

                                    tbxOnetimeduedate.Text = compliance.IOneTimeDate != null ? compliance.IOneTimeDate.Value.ToString("dd-MM-yyyy") : " ";
                                    divComplianceDueDays.Visible = false;
                                    txtEventDueDate.Text = null;                                    
                                }
                            }
                        }
                        else
                        {
                            divFunctionBased.Visible = false;
                            divCompType.Visible = false;
                            divScheduleType.Visible = false;
                            rbtnlstCompOcc.SelectedValue = "0";
                            divOnetimeDate.Visible = true;
                            divChecklist.Visible = false;
                            tbxOnetimeduedate.Text = compliance.IOneTimeDate != null ? compliance.IOneTimeDate.Value.ToString("dd-MM-yyyy") : " ";
                        }
                        rbReminderType.SelectedValue = Convert.ToString(compliance.IReminderType);
                        if (compliance.IReminderType == 0)
                        {
                            divForCustome.Visible = false;
                        }
                        else
                        {
                            divForCustome.Visible = true;
                        }


                        if (chkEvent.Checked == true)
                        {
                            divCompType.Visible = false;
                            divScheduleType.Visible = false;
                            divTimebasedTypes.Visible = false;
                            divFunctionBased.Visible = false;
                            divOnetimeDate.Visible = false;
                            rbReminderType.Items[0].Enabled = false;
                            rbReminderType.SelectedValue = "0";
                            divForCustome.Visible = false;
                            divOccourrence.Visible = false;
                        }

                         
                        if (ddlFrequency.SelectedValue == "8")
                        {
                            ddlWeekDueDay.SelectedValue = compliance.DueWeekDay.ToString();
                            vivDueDate.Visible = false;
                            vivWeekDueDays.Visible = true;
                        }
                        else
                        { 
                            vivWeekDueDays.Visible = false;
                        }

                        txtReminderBefore.Text = Convert.ToString(compliance.IReminderBefore);
                        txtReminderGap.Text = Convert.ToString(compliance.IReminderGap);

                        upComplianceDetails.Update();
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divInternalComplianceDetailsDialog\").dialog('open')", true);
                    }
                }
                else if (e.CommandName.Equals("DELETE_COMPLIANCE"))
                {
                    int complianceID = Convert.ToInt32(e.CommandArgument);
                    GComplianceID = Convert.ToInt32(e.CommandArgument);
                    bool chkexistsComplianceschedule = Business.ComplianceManagement.ExistsInternalComplianceScheduleAssignment(complianceID);
                    bool chkAssignCompliance = Business.ComplianceManagement.ExistsInternalComplianceAssignment(complianceID);
                    if (chkexistsComplianceschedule == false && chkAssignCompliance == false)
                    {
                        Business.ComplianceManagement.DeleteInternalCompliance(complianceID);
                        BindCompliances();
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Compliance assigned,can not deleted.";
                        upComplianceDetails.Update();
                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "OpenDialog2", "$(\"#divInternalComplianceDetailsDialog\").dialog('open')", true);
                    }
                }
                else if (e.CommandName.Equals("SHOW_SCHEDULE"))
                {
                    int complianceID = Convert.ToInt32(e.CommandArgument);
                    var compliance = Business.ComplianceManagement.GetInternalComplianceByID(complianceID);
                    OpenScheduleInformation(compliance);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdCompliances_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdCompliances.PageIndex = e.NewPageIndex;
                BindCompliances();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            BindCompliances();
        }

        protected void ddlComplianceType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                divFunctionBased.Visible = vaDueDate.Enabled = (ddlComplianceType.SelectedValue == "0" || ddlComplianceType.SelectedValue == "2");

                if (ddlComplianceType.SelectedValue == "0")//function based
                {
                    divNonEvents.Visible = true;
                    divTimebasedTypes.Visible = false;
                    divComplianceDueDays.Visible = false;
                    divFrequency.Visible = true;


                    if (ddlFrequency.SelectedValue == "7" || ddlFrequency.SelectedValue == "8")
                    {
                        vivDueDate.Visible = false;
                    }
                    else
                    {
                        vivDueDate.Visible = true;
                    }
                                       
                    divChecklist.Visible = false;
                    divOnetimeDate.Visible = false;
                    rbReminderType.SelectedValue = "0";
                    rbReminderType.Enabled = true;
                    divForCustome.Visible = false;

                }
                else if (ddlComplianceType.SelectedValue.Equals("2"))//timebased
                {
                    divNonEvents.Visible = false;
                    divTimebasedTypes.Visible = true;
                    if (rbTimebasedTypes.SelectedValue=="0")
                    {
                        divComplianceDueDays.Visible = true;
                    }
                    else
                    {
                        divComplianceDueDays.Visible = false;
                    }               
                    divFrequency.Visible = false;
                    vivDueDate.Visible = false;
                    divChecklist.Visible = false;
                    divOnetimeDate.Visible = false;
                    rbTimebasedTypes.SelectedValue = "0";
                    rbReminderType.Enabled = true;
                }
                else//checklist
                {
                    divNonEvents.Visible = false;
                    divTimebasedTypes.Visible = false;
                    divComplianceDueDays.Visible = false;
                    divFunctionBased.Visible = false;
                    divTimebasedTypes.Visible = false;
                    divChecklist.Visible = true;
                    divOnetimeDate.Visible = true;
                    ddlChklstType.SelectedValue = "0";
                    if (ddlChklstType.SelectedValue == "0")//one time
                    {                                               
                        rbReminderType.SelectedValue = "1";
                         rbReminderType.Enabled = false;
                        divForCustome.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rbTimebasedTypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (rbTimebasedTypes.SelectedValue.Equals("0"))
                {
                    divNonEvents.Visible = false;
                    divFrequency.Visible = false;
                    vivDueDate.Visible = false;
                    cvfrequency.Enabled = false;
                    divComplianceDueDays.Visible = true;

                }
                else if (rbTimebasedTypes.SelectedValue.Equals("1"))
                {
                    divNonEvents.Visible = true;
                    divFrequency.Visible = true;
                    vivDueDate.Visible = false;
                    cvfrequency.Enabled = true;
                    divComplianceDueDays.Visible = false;

                }
                else
                {
                    divNonEvents.Visible = true;
                    divFrequency.Visible = true;
                    vivDueDate.Visible = false;
                    cvfrequency.Enabled = true;
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnSaveSchedule_Click(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["ComplianceID"].ToString() != null && ViewState["ComplianceID"].ToString() != "")
                {
                    bool chkexistsComplianceschedule = Business.ComplianceManagement.ExistsInternalComplianceScheduleAssignment(Convert.ToInt64(ViewState["ComplianceID"]));
                    if (chkexistsComplianceschedule == true)
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Internal Compliance assigned cannot be change Calender/Financial Year.";
                    }
                    else
                    {

                        List<InternalComplianceSchedule> scheduleList = new List<InternalComplianceSchedule>();

                        for (int i = 0; i < repComplianceSchedule.Items.Count; i++)
                        {
                            RepeaterItem entry = repComplianceSchedule.Items[i] as RepeaterItem;

                            HiddenField hdnID = ((HiddenField) entry.FindControl("hdnID"));
                            HiddenField hdnForMonth = ((HiddenField) entry.FindControl("hdnForMonth"));
                            DropDownList ddlDays = (DropDownList) entry.FindControl("ddlDays");
                            DropDownList ddlMonths = (DropDownList) entry.FindControl("ddlMonths");

                            scheduleList.Add(new InternalComplianceSchedule()
                            {
                                ID = Convert.ToInt32(hdnID.Value),
                                IComplianceID = Convert.ToInt64(ViewState["ComplianceID"]),
                                ForMonth = Convert.ToInt32(hdnForMonth.Value),
                                SpecialDate = string.Format("{0}{1}", Convert.ToByte(ddlDays.SelectedValue).ToString("D2"), Convert.ToByte(ddlMonths.SelectedValue).ToString("D2"))
                            });
                        }

                        Business.ComplianceManagement.UpdateInternalComplianceScheduleInformation(scheduleList);
                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "CloseScheduledDialog", "$(\"#divComplianceScheduleDialog\").dialog('close')", true);
                        upSchedulerRepeter.Update();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void upComplianceDetails_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView", "$(\"#divSubevent\").hide(\"blind\", null, 500, function () { });", true);

                DateTime date = DateTime.MinValue;
                if (DateTime.TryParseExact(tbxOnetimeduedate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", "initializeDatePicker(null);", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void upComplianceScheduleDialog_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdCompliances_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        protected void chkDocument_CheckedChanged(object sender, EventArgs e)
        {
        }

        protected void grdCompliances_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                int CmType = -1;
                //if (rdFunctionBased.Checked)
                //    CmType = 0;
                //if (rdChecklist.Checked)
                //    CmType = 1;
                int CustomerID = Convert.ToInt32(UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID);
                var compliancesData = Business.ComplianceManagement.GetAllInternalCompliances(Convert.ToInt32(ddlFilterComplianceType.SelectedValue), Convert.ToInt32(ddlComplinceCatagory.SelectedValue), Convert.ToInt32(ddlFilterFrequencies.SelectedValue), CmType, CustomerID, tbxFilter.Text);
                List<object> dataSource = new List<object>();
                foreach (var complianceInfo in compliancesData)
                {
                    string risk = "";
                    if (complianceInfo.IRiskType == 0)
                        risk = "High";
                    else if (complianceInfo.IRiskType == 1)
                        risk = "Medium";
                    else if (complianceInfo.IRiskType == 2)
                        risk = "Low";
                    else if (complianceInfo.IRiskType == 3)
                        risk = "Critical";

                    dataSource.Add(new
                    {
                        complianceInfo.ID,
                        complianceInfo.IComplianceTypeName,
                        complianceInfo.IComplianceCategoryName,
                        complianceInfo.IShortDescription,
                        complianceInfo.IComplianceType,
                        complianceInfo.IUploadDocument,
                        complianceInfo.IRequiredFrom,
                        Frequency = Enumerations.GetEnumByID<Frequency>(Convert.ToInt32(complianceInfo.IFrequency != null ? (int) complianceInfo.IFrequency : -1)),
                        Risk = risk,
                        complianceInfo.ISubComplianceType,
                        complianceInfo.CreatedOn,
                        complianceInfo.CheckListTypeID
                    });
                }

                if (direction == SortDirection.Ascending)
                {
                    dataSource = dataSource.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                }
                else
                {
                    dataSource = dataSource.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                }

                foreach (DataControlField field in grdCompliances.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdCompliances.Columns.IndexOf(field);
                    }
                }

                grdCompliances.DataSource = dataSource;
                grdCompliances.DataBind();


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlComplinceCatagory_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindCompliances();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlFilterComplianceType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindCompliances();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rdChecklist_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                BindCompliances();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rdFunctionBased_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                BindCompliances();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        /// <summary>
        /// this function is bind catagory of fiter catagory drop down.
        /// </summary>
        private void BindTypes()
        {
            try
            {
                int CustomerID = -1;
                if (AuthenticationHelper.Role == "IMPT")
                {
                    CustomerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                }
                else
                {
                    CustomerID = Convert.ToInt32(UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID);
                }
                ddlFilterComplianceType.DataTextField = "Name";
                ddlFilterComplianceType.DataValueField = "ID";

                ddlFilterComplianceType.DataSource = ComplianceTypeManagement.GetAllInternalCompliances(CustomerID);
                ddlFilterComplianceType.DataBind();

                ddlFilterComplianceType.Items.Insert(0, new ListItem("< Select Compliance Type >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        /// <summary>
        /// this function is bind catagory for filter drodown.
        /// </summary>
        private void BindCategories(DropDownList ddlCategory)
        {
            try
            {
                int CustomerID = -1;
                if (AuthenticationHelper.Role == "IMPT")
                {
                    CustomerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                }
                else
                {
                    CustomerID = Convert.ToInt32(UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID);
                }
                ddlCategory.DataTextField = "Name";
                ddlCategory.DataValueField = "ID";

                ddlCategory.DataSource = ComplianceCategoryManagement.GetAll(CustomerID);
                ddlCategory.DataBind();

                ddlCategory.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        /// <summary>
        /// this function is bind catagory for filter drodown.
        /// </summary>
        private void BindICategories(DropDownList ddlCategory)
        {
            try
            {
                //  int CustomerID = Convert.ToInt32(UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID);
                int CustomerID = -1;
                if (AuthenticationHelper.Role == "IMPT")
                {
                    CustomerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                }
                else
                {
                    CustomerID = Convert.ToInt32(UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID);
                }
                ddlCategory.DataTextField = "Name";
                ddlCategory.DataValueField = "ID";

                ddlCategory.DataSource = ComplianceCategoryManagement.GetAll(CustomerID);
                ddlCategory.DataBind();

                ddlCategory.Items.Insert(0, new ListItem("< Select Category>", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection) ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        private void BindFrequencies()
        {
            try
            {
                ddlFrequency.DataTextField = "Name";
                ddlFrequency.DataValueField = "ID";

                ddlFilterFrequencies.DataTextField = "Name";
                ddlFilterFrequencies.DataValueField = "ID";


                ddlFrequency.DataSource = Enumerations.GetAll<Frequency>();
                ddlFrequency.DataBind();
                ddlFilterFrequencies.DataSource = Enumerations.GetAll<Frequency>();
                ddlFilterFrequencies.DataBind();

                ddlFrequency.Items.Insert(0, new ListItem("< Select >", "-1"));
                ddlFilterFrequencies.Items.Insert(0, new ListItem("< Select Frequency>", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindFilterFrequencies()
        {
            try
            {

                ddlFilterFrequencies.DataTextField = "Name";
                ddlFilterFrequencies.DataValueField = "ID";

                ddlFilterFrequencies.DataSource = Enumerations.GetAll<Frequency>();
                ddlFilterFrequencies.DataBind();

                ddlFilterFrequencies.Items.Insert(0, new ListItem("< Select Frequency>", "-1"));
                ddlFilterFrequencies.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindDueDates()
        {
            try
            {
                for (int count = 1; count < 32; count++)
                {
                    ddlDueDate.Items.Add(new ListItem() { Text = count.ToString(), Value = count.ToString() });
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private bool CheckInt(string val)
        {
            try
            {
                int i = Convert.ToInt32(val);
                return true;
            }
            catch
            {
                return false;
            }
        }

        private void BindCompliances()
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int CmType = -1;
                    if (rdFunctionBased.Checked)
                        CmType = 0;
                    if (rdChecklist.Checked)
                        CmType = 1;
                    int CustomerID = -1;
                    if(AuthenticationHelper.Role=="IMPT")
                    {
                        if (ddlCustomer.SelectedValue != "-1" && ddlCustomer.SelectedValue!= "")
                        {
                            CustomerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                        } 
                    }
                    else
                    {
                        CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    }

                    var compliancesData = (from row in entities.Sp_GetInternalCompliance_List(CustomerID)
                                           select row).ToList();

                    int risk = -1;
                    if (tbxFilter.Text.ToUpper().Equals("HIGH"))
                        risk = 0;
                    else if (tbxFilter.Text.ToUpper().Equals("MEDIUM"))
                        risk = 1;
                    else if (tbxFilter.Text.ToUpper().Equals("LOW"))
                        risk = 2;
                    else if (tbxFilter.Text.ToUpper().Equals("Critical"))
                        risk = 3;
                   

                    if (risk != -1)
                    {
                        compliancesData = compliancesData.Where(entry => entry.IRiskType == risk).ToList();
                    }

                    string frequency = tbxFilter.Text.ToUpper();
                    if (frequency.Equals("MONTHLY"))
                        frequency = "Monthly";
                    if (frequency.Equals("QUARTERLY"))
                        frequency = "Quarterly";
                    if (frequency.Equals("HALFYEARLY"))
                        frequency = "HalfYearly";
                    if (frequency.Equals("ANNUAL"))
                        frequency = "Annual";
                    if (frequency.Equals("FOURMONTHALY"))
                        frequency = "FourMonthly";
                    if (frequency.Equals("TWOYEARLY"))
                        frequency = "TwoYearly";
                    if (frequency.Equals("SEVENYEARLY"))
                        frequency = "SevenYearly";

                    int frequencyId = Enumerations.GetEnumByName<Frequency>(Convert.ToString(frequency));
                    if (frequencyId == 0 && frequency != "Monthly")
                    {
                        frequencyId = -1;
                    }

                    if (frequencyId != -1)
                    {
                        compliancesData = compliancesData.Where(entry => entry.IFrequency == frequencyId).ToList();
                    }

                    if (!string.IsNullOrEmpty(tbxFilter.Text))
                    {

                        if (CheckInt(tbxFilter.Text))
                        {
                            int a = Convert.ToInt32(tbxFilter.Text);
                            compliancesData = compliancesData.Where(entry => entry.ID == a).ToList();
                        }
                        else
                        {
                            compliancesData = compliancesData.Where(entry => entry.IShortDescription.ToUpper().Contains(tbxFilter.Text.ToUpper())).ToList();
                        }
                    }

                    if (Convert.ToInt32(ddlFilterComplianceType.SelectedValue) != -1 && ddlFilterComplianceType.SelectedValue !="")
                    {
                        compliancesData = compliancesData.Where(entry => entry.IComplianceTypeID == Convert.ToInt32(ddlFilterComplianceType.SelectedValue)).ToList();
                    }

                    if (Convert.ToInt32(ddlComplinceCatagory.SelectedValue) != -1 && ddlComplinceCatagory.SelectedValue != "")
                    {
                        compliancesData = compliancesData.Where(entry => entry.IComplianceCategoryID == Convert.ToInt32(ddlComplinceCatagory.SelectedValue)).ToList();
                    }

                    if (CmType != -1)
                    {
                        compliancesData = compliancesData.Where(entry => entry.IComplianceType == CmType).ToList();
                    }
                    if (Convert.ToInt32(ddlFilterFrequencies.SelectedValue) != -1 && ddlFilterFrequencies.SelectedValue != "")
                    {
                        int Frequency = Convert.ToInt32(ddlFilterFrequencies.SelectedValue);
                        compliancesData = compliancesData.Where(entry => entry.IFrequency == Frequency).ToList();
                    }

                    grdCompliances.DataSource = compliancesData;
                    grdCompliances.DataBind();
                    Session["grdAssignCheckListData"] = (grdCompliances.DataSource as List<Sp_GetInternalCompliance_List_Result>).ToDataTable();
                    upCompliancesList.Update();
                }
            
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        //private void BindCompliances()
        //{
        //    try
        //    {
        //        int CmType = -1;
        //        if (rdFunctionBased.Checked)
        //            CmType = 0;
        //        if (rdChecklist.Checked)
        //            CmType = 1;
        //        int CustomerID = Convert.ToInt32(UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID);
        //var compliancesData = Business.ComplianceManagement.GetAllInternalCompliances(Convert.ToInt32(ddlFilterComplianceType.SelectedValue), Convert.ToInt32(ddlComplinceCatagory.SelectedValue), Convert.ToInt32(ddlFilterFrequencies.SelectedValue), CmType, tbxFilter.Text);
        //        var compliancesData = Business.ComplianceManagement.GetAllInternalCompliances(Convert.ToInt32(ddlFilterComplianceType.SelectedValue), Convert.ToInt32(ddlComplinceCatagory.SelectedValue), Convert.ToInt32(ddlFilterFrequencies.SelectedValue), CmType, CustomerID, tbxFilter.Text);
        //        List<object> dataSource = new List<object>();
        //        foreach (var complianceInfo in compliancesData)
        //        {
        //            string risk = "";
        //            if (complianceInfo.IRiskType == 0)
        //                risk = "High";
        //            else if (complianceInfo.IRiskType == 1)
        //                risk = "Medium";
        //            else if (complianceInfo.IRiskType == 2)
        //                risk = "Low";

        //            string ss = Enumerations.GetEnumByID<Frequency>(Convert.ToInt32(complianceInfo.IFrequency));
        //            dataSource.Add(new
        //            {
        //                complianceInfo.ID,
        //                complianceInfo.IComplianceTypeName,
        //                complianceInfo.IComplianceCategoryName,
        //                complianceInfo.IShortDescription,
        //                complianceInfo.IComplianceType,
        //                complianceInfo.IUploadDocument,
        //                complianceInfo.IRequiredFrom,
        //                Frequency = Enumerations.GetEnumByID<Frequency>(Convert.ToInt32(complianceInfo.IFrequency != null ? (int) complianceInfo.IFrequency : -1)),
        //                Risk = risk,
        //                complianceInfo.ISubComplianceType,
        //                complianceInfo.CreatedOn,
        //                complianceInfo.CheckListTypeID
        //            });
        //        }
        //        grdCompliances.DataSource = dataSource;
        //        grdCompliances.DataBind();
        //        upCompliancesList.Update();
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvDuplicateEntry.IsValid = false;
        //        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
        //    }
        //}

        protected void ddlChklstType_SelectedIndexChanged(object sender, EventArgs e)
        {
            rbReminderType.SelectedValue = "0";
            rbReminderType.Enabled = false;
            divOnetimeDate.Visible = true;
            if (ddlChklstType.SelectedValue == "0")//one time
            {
                divOnetimeDate.Visible = true;
                divTimebasedTypes.Visible = false;
                divFunctionBased.Visible = false;
                divNonEvents.Visible = false;
                rbReminderType.SelectedValue = "1";
                rbReminderType.Enabled = false;
                rbTimebasedTypes.SelectedValue = "0";
                divForCustome.Visible = false;

            }
            else if (ddlChklstType.SelectedValue.Equals("1"))//function based
            {
                divOnetimeDate.Visible = false;
                divTimebasedTypes.Visible = false;
                divFunctionBased.Visible = true;
                divNonEvents.Visible = true;
                divFrequency.Visible = true;
                if (ddlFrequency.SelectedValue == "7" || ddlFrequency.SelectedValue == "8")
                {
                    vivDueDate.Visible = false;
                }
                else
                {
                    vivDueDate.Visible = true;
                }
                divTimebasedTypes.Visible = false;
                divComplianceDueDays.Visible = false;
                divForCustome.Visible = false;
            }
            else if (ddlChklstType.SelectedValue.Equals("2"))//time based
            {

                divFunctionBased.Visible = true;
                divOnetimeDate.Visible = false;
                divTimebasedTypes.Visible = true;
                divNonEvents.Visible = false;
                rbTimebasedTypes.SelectedValue = "0";            
                divNonEvents.Visible = false;
                divTimebasedTypes.Visible = true;
                divComplianceDueDays.Visible = true;
                divFrequency.Visible = false;
                vivDueDate.Visible = false;
                divForCustome.Visible = false;
            }
            else
            {
                divFunctionBased.Visible = true;
                divOnetimeDate.Visible = false;
                divTimebasedTypes.Visible = true;
                divNonEvents.Visible = false;
                rbTimebasedTypes.SelectedValue = "0";
                divNonEvents.Visible = false;
                divTimebasedTypes.Visible = true;
                divComplianceDueDays.Visible = true;
                divFrequency.Visible = false;
                vivDueDate.Visible = false;                
           }
        }

        private string GetParameters(List<ComplianceParameter> parameters)
        {
            try
            {
                StringBuilder paramString = new StringBuilder();
                foreach (var item in parameters)
                {
                    paramString.Append(paramString.Length == 0 ? item.Name : ", " + item.Name);
                }

                return paramString.ToString();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return null;
        }

        private void BindIType()
        {
            try
            {
                int CustomerID = -1;
                if (AuthenticationHelper.Role == "IMPT")
                {
                    CustomerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                }
                else
                {
                    CustomerID = Convert.ToInt32(UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID);
                }
                ddlIComplinceType.DataTextField = "Name";
                ddlIComplinceType.DataValueField = "ID"; 
                ddlIComplinceType.DataSource = ComplianceTypeManagement.GetAllNVP(CustomerID); 
                ddlIComplinceType.DataBind();  
                ddlIComplinceType.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }


        protected bool ViewSchedule(object frequency, object complianceType, object SubComplianceType, object CheckListTypeID)
        {
            try
            {
                if (Convert.ToString(frequency) == "Daily" || Convert.ToString(frequency) == "Weekly")
                {
                    return false;
                }
                else if (Convert.ToByte(complianceType) == 2)
                {
                    return false;
                }
                else
                {
                    if (Convert.ToByte(complianceType) == 1)
                    {
                        if (Convert.ToInt32(CheckListTypeID) == 1 || Convert.ToInt32(CheckListTypeID) == 2)
                        {
                            if (frequency != null)
                            {
                                return true;
                            }
                            else
                            {
                                return false;
                            }
                        }
                        else
                        {
                            return false;
                        }

                    }
                    else if (frequency != null)
                       return true;

                }
               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            try
            {
                saveopo.Value = "true";
                if (ViewState["ComplianceID"].ToString() != null && ViewState["ComplianceID"].ToString() != "")
                {


                    bool chkexistsComplianceschedule = Business.ComplianceManagement.ExistsInternalComplianceScheduleAssignment(Convert.ToInt64(ViewState["ComplianceID"]));
                    if (chkexistsComplianceschedule == true)
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Internal Compliance assigned cannot be change Calender/Financial Year.";
                    }
                    else
                    {
                        Business.ComplianceManagement.ResetInternalComplianceSchedule(Convert.ToInt32(ViewState["ComplianceID"]));
                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "CloseScheduledDialog", "$(\"#divComplianceScheduleDialog\").dialog('close')", true);
                        upSchedulerRepeter.Update();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rbReminderType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (rbReminderType.SelectedValue.Equals("1"))
                {
                    divForCustome.Visible = true;

                }
                else
                {
                    divForCustome.Visible = false;
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rbtnlstCompOcc_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (rbtnlstCompOcc.SelectedValue.Equals("0"))
                {
                    divCompType.Visible = false;
                    divScheduleType.Visible = false;
                    divTimebasedTypes.Visible = false;
                    divFunctionBased.Visible = false;
                    divOnetimeDate.Visible = true;                                       
                    rbReminderType.Items[0].Enabled = false;
                    rbReminderType.SelectedValue = "1";
                    divForCustome.Visible = true;
                }
                else
                {
                    divCompType.Visible = true;
                    divScheduleType.Visible = true;
                    divTimebasedTypes.Visible = true;
                    divFunctionBased.Visible = true;
                    divOnetimeDate.Visible = false;                    
                    rbReminderType.Items[0].Enabled = true;
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }

        protected void ddlFrequency_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlFrequency.SelectedValue == "7" || ddlFrequency.SelectedValue == "8")
                {
                    vivDueDate.Visible = false;
                }
                else
                {
                    vivDueDate.Visible = true;
                }

                if (ddlFrequency.SelectedValue == "8")
                {
                    vaDueWeekDay.Enabled = true;
                    vivWeekDueDays.Visible = true;
                }
                else
                {
                    vaDueWeekDay.Enabled = false;
                    vivWeekDueDays.Visible = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void lnkContract_Click(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divContractDetailsDialog\").dialog('open')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdSampleForm_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            try
            {
                if (e.CommandName.Equals("DownloadSampleForm"))
                {
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    int complianceID = Convert.ToInt32(commandArgs[1]);
                    int ID = Convert.ToInt32(commandArgs[0]);
                    var file = Business.InternalComplianceManagement.GetSelectedInternalComplianceFileName(ID, complianceID);
                    Response.Buffer = true;

                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Clear();
                    Response.ContentType = "application/octet-stream";
                    Response.AddHeader("content-disposition", "attachment; filename= " + file.Name);
                    Response.BinaryWrite(file.FileData);
                    Response.Flush();
                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                }
                else if (e.CommandName.Equals("DeleleSampleForm"))
                {
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    int complianceID = Convert.ToInt32(commandArgs[1]);
                    int ID = Convert.ToInt32(commandArgs[0]);
                    Business.InternalComplianceManagement.DeleteInternalComplianceForm(ID, complianceID);
                    var complianceForm = Business.InternalComplianceManagement.GetMultipleInternalComplianceFormByID(complianceID);
                    grdSampleForm.DataSource = complianceForm;
                    grdSampleForm.DataBind();
                    upCompliancesList.Update();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void chkEvent_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (chkEvent.Checked == true)
                {
                    divCompType.Visible = false;
                    divScheduleType.Visible = false;
                    divTimebasedTypes.Visible = false;
                    divFunctionBased.Visible = false;
                    divOnetimeDate.Visible = false;
                    rbReminderType.Items[0].Enabled = false;
                    rbReminderType.SelectedValue = "0";
                    divForCustome.Visible = false;
                    divOccourrence.Visible = false;
                }
                else
                {
                    divOccourrence.Visible = true;
                    rbtnlstCompOcc_SelectedIndexChanged(sender, e);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        
    }
}