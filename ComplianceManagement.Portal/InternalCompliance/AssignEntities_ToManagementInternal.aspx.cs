﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business;
using System.Globalization;

namespace com.VirtuosoITech.ComplianceManagement.Portal.InternalCompliance
{
    public partial class AssignEntities_ToManagementInternal : System.Web.UI.Page
    {
        private readonly IEnumerable<object> EntitiesAssignmentInternals;
        public static List<long> locationList = new List<long>();
        protected int UserID = -1;
        protected int customerid = -1;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (AuthenticationHelper.Role == "IMPT")
                {
                    UserID = AuthenticationHelper.UserID;
                    divcustomer.Visible = true;
                    BindCustomers(UserID);
                    divassigncustomer.Visible = true;
                    divDeletecustomer.Visible = true;

                }
                else
                {
                    customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    divFilterUsers.Visible = true;
                    FilterLocationdiv.Visible = true;
                    btnAddComplianceType.Visible = true;
                    BindLocationFilter();
                    //BindComplianceEntityInstances();
                    BindCategory(customerid);
                    BindUsers(ddlUsers, customerid);
                    BindFilterUsers(ddlFilterUsers, customerid);
                    BindDelUsers(delddluser, customerid);
                    BindLocation(customerid);
                    TextBox1.Attributes.Add("readonly", "readonly");
                    tbxBranch.Attributes.Add("readonly", "readonly");
                    tbxFilterLocation.Text = "< Select >";
                }
                ForceCloseFilterBranchesTreeView();
            }
        }
        private void BindCustomers(int userid)
        {
            try
            {
                var Customedata = Assigncustomer.GetAllCustomer(userid);
                ddlcustomer.DataTextField = "Name";
                ddlcustomer.DataValueField = "ID";

                ddlcustomer.DataSource = Customedata;
                ddlcustomer.DataBind();

                ddlcustomer.Items.Insert(0, new ListItem("< Select Customer >", "-1"));


                ddlFiltercustomer.DataTextField = "Name";
                ddlFiltercustomer.DataValueField = "ID";

                ddlFiltercustomer.DataSource = Customedata;
                ddlFiltercustomer.DataBind();

                ddlFiltercustomer.Items.Insert(0, new ListItem("< Select Customer >", "-1"));

                delddlCustomer.DataTextField = "Name";
                delddlCustomer.DataValueField = "ID";

                delddlCustomer.DataSource = Customedata;
                delddlCustomer.DataBind();

                delddlCustomer.Items.Insert(0, new ListItem("< Select Customer >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
        }
        private void BindCategory(int customerid)
        {
            try
            {
               
                ddlComplianceCatagory.DataTextField = "Name";
                ddlComplianceCatagory.DataValueField = "ID";
               
                ddlComplianceCatagory.DataSource = ComplianceCategoryManagement.GetAllInternalCompliancesCategories(customerid);
                ddlComplianceCatagory.DataBind();

                ddlComplianceCatagory.Items.Insert(0, new ListItem("< Select >", "-1"));
                ddlComplianceCatagory.Items.Insert(1, new ListItem("Select All", "All"));

                delddlcomcatagory.DataTextField = "Name";
                delddlcomcatagory.DataValueField = "ID";
                // int customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);
                delddlcomcatagory.DataSource = ComplianceCategoryManagement.GetAllInternalCompliancesCategories(customerid);
                delddlcomcatagory.DataBind();

                delddlcomcatagory.Items.Insert(0, new ListItem("< Select >", "-1"));
                delddlcomcatagory.Items.Insert(1, new ListItem("Select All", "All"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void RetrieveNodes(TreeNode node)
        {
            try
            {
                int customerid = -1;
                if (AuthenticationHelper.Role == "IMPT")
                {
                    customerid = Convert.ToInt32(ddlFiltercustomer.SelectedValue);
                }
                else
                {

                    if ((!string.IsNullOrEmpty(ddlFiltercustomer.SelectedValue)) && ddlFiltercustomer.SelectedValue != "-1")
                    {
                        customerid = Convert.ToInt32(ddlFiltercustomer.SelectedValue);
                    }

                }
                if (node.Checked)
                {
                    if (Convert.ToInt32(node.Value) != customerid)
                    {
                        if (!locationList.Contains(Convert.ToInt32(node.Value)))
                        {
                            locationList.Add(Convert.ToInt32(node.Value));
                        }
                    }
                    if (node.ChildNodes.Count != 0)
                    {
                        for (int i = 0; i < node.ChildNodes.Count; i++)
                        {
                            RetrieveNodes(node.ChildNodes[i]);
                        }
                    }
                }
                else
                {
                    foreach (TreeNode tn in node.ChildNodes)
                    {
                        if (tn.Checked)
                        {
                            if (Convert.ToInt32(tn.Value) != customerid)
                            {
                                if (!locationList.Contains(Convert.ToInt32(tn.Value)))
                                {
                                    locationList.Add(Convert.ToInt32(tn.Value));
                                }
                            }
                        }
                        if (tn.ChildNodes.Count != 0)
                        {
                            for (int i = 0; i < tn.ChildNodes.Count; i++)
                            {
                                RetrieveNodes(tn.ChildNodes[i]);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }
        private void BindComplianceEntityInstances()
        {
            try
            {
                int branchID = -1;
                if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1")
                {
                    branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                }
                int userID = -1;
                if ((!string.IsNullOrEmpty(ddlFilterUsers.SelectedValue)))
                {
                    userID = Convert.ToInt32(ddlFilterUsers.SelectedValue);
                }
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }
                else if (AuthenticationHelper.Role == "MGMT")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                if ((!string.IsNullOrEmpty(ddlFiltercustomer.SelectedValue)))
                {
                    customerID = Convert.ToInt32(ddlFiltercustomer.SelectedValue);
                }
             
                locationList.Clear();
                for (int i = 0; i < this.tvFilterLocation.Nodes.Count; i++)
                {
                    RetrieveNodes(this.tvFilterLocation.Nodes[i]);
                }
                List<InternalComplianceAssignmentEntitiesView> masterlist = new List<InternalComplianceAssignmentEntitiesView>();
                if (locationList.Count > 0)
                {
                    masterlist = AssignEntityManagement.SelectAllEntitiesInternalList(userID, customerID, locationList);
                }
                else
                {
                    masterlist = AssignEntityManagement.SelectAllEntitiesInternal(branchID, userID, customerID);
                }
                grdAssignEntities.DataSource = masterlist;
                grdAssignEntities.DataBind();
                upComplianceTypeList.Update();
                ForceCloseFilterBranchesTreeView();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void tvBranches_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxBranch.Text = tvBranches.SelectedNode != null ? tvBranches.SelectedNode.Text : "< Select Location >";
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void upCompliance_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranches", string.Format("initializeJQueryUI('{0}', 'divBranches');", tbxBranch.ClientID), true);
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void upComplianceTypeList_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void upCompliance_LoadDelete(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesDelete", string.Format("initializeJQueryUI('{0}', 'divBranches1');", TextBox1.ClientID), true);
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindLocation(int customerID)
        {
            try
            {
               
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }
                else if (AuthenticationHelper.Role == "MGMT")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }

                tvBranches.Nodes.Clear();
                TreeView1.Nodes.Clear();
                TreeNode node = new TreeNode();
                var bracnhes = CustomerBranchManagement.GetAllHierarchyForMappedLocation(customerID);

                TreeNode firstnode = new TreeNode();
                foreach (var item in bracnhes)
                {
                    firstnode = new TreeNode(item.Name, item.ID.ToString());
                    firstnode.SelectAction = TreeNodeSelectAction.Expand;
                    BindBranchesHierarchy(firstnode, item);
                    tvBranches.Nodes.Add(firstnode);
                }
                tvBranches.CollapseAll();

                TreeNode secondnode = new TreeNode();
                foreach (var item in bracnhes)
                {
                    secondnode = new TreeNode(item.Name, item.ID.ToString());
                    secondnode.SelectAction = TreeNodeSelectAction.Expand;
                    BindBranchesHierarchy(secondnode, item);
                    TreeView1.Nodes.Add(secondnode);
                }
                TreeView1.CollapseAll();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp)
        {
            try
            {
                foreach (var item in nvp.Children)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    BindBranchesHierarchy(node, item);
                    parent.ChildNodes.Add(node);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindLocationFilter()
        {
            try
            {

                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }
                else if (AuthenticationHelper.Role == "MGMT")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "IMPT")
                {
                    customerID = Convert.ToInt32(ddlFiltercustomer.SelectedValue);
                }
                else
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                tvFilterLocation.Nodes.Clear();
                var bracnhes = CustomerBranchManagement.GetAllHierarchy(customerID);
                foreach (var item in bracnhes)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    BindBranchesHierarchy(node, item);
                    tvFilterLocation.Nodes.Add(node);
                }

                tvFilterLocation.CollapseAll();
                //tvFilterLocation_SelectedNodeChanged(null, null);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
            BindComplianceEntityInstances();
        }

        private void ForceCloseFilterBranchesTreeView()
        {
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideFilterTreeView", "$(\"#divFilterLocation\").hide(\"blind\", null, 5, function () { });", true);
        }
        private void BindUsers(DropDownList ddlUserList, int customerID, List<long> ids = null)
        {
            try
            {
               
                int complianceProductType = 0;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    complianceProductType = AuthenticationHelper.ComplianceProductType;
                    //customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }
            else if (AuthenticationHelper.Role == "MGMT")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    complianceProductType = AuthenticationHelper.ComplianceProductType;
                    //customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }

                ddlUserList.DataTextField = "Name";
                ddlUserList.DataValueField = "ID";
                ddlUserList.Items.Clear();

              

                var users = UserManagement.GetAllManagmentUser(customerID, complianceProductType);
               

                users.Insert(0, new { ID = -1, Name = ddlUserList == ddlUsers ? "< Select >" : "< All >" });
                ddlUserList.DataSource = users;
                ddlUserList.DataBind();

               
               

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindFilterUsers(DropDownList ddlFilterUserList,int customerID, List<long> ids = null)
        {
            try
            {
               
                int complianceProductType = 0;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    complianceProductType = AuthenticationHelper.ComplianceProductType;
                    //customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }
                else if (AuthenticationHelper.Role == "MGMT")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    complianceProductType = AuthenticationHelper.ComplianceProductType;
                    //customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }



                ddlFilterUserList.DataTextField = "Name";
                ddlFilterUserList.DataValueField = "ID";
                ddlFilterUserList.Items.Clear();

                var users = UserManagement.GetAllManagmentUser(customerID, complianceProductType);

                users.Insert(0, new { ID = -1, Name = ddlFilterUserList == ddlUsers ? "< Select >" : "< All >" });
                
                ddlFilterUserList.DataSource = users;
                ddlFilterUserList.DataBind();

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindDelUsers(DropDownList deleteUserList, int customerID, List<long> ids = null)
        {
            try
            {
             
                int complianceProductType = 0;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    complianceProductType = AuthenticationHelper.ComplianceProductType;
                    //customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }
                else if (AuthenticationHelper.Role == "MGMT")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    complianceProductType = AuthenticationHelper.ComplianceProductType;
                    //customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }


                deleteUserList.DataTextField = "Name";
                deleteUserList.DataValueField = "ID";
                deleteUserList.Items.Clear();

                var users = UserManagement.GetAllManagmentUser(customerID, complianceProductType);


                users.Insert(0, new { ID = -1, Name = deleteUserList == ddlUsers ? "< Select >" : "< All >" });
              


                deleteUserList.DataSource = users;
                deleteUserList.DataBind();

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlFilterUsers_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindComplianceEntityInstances();
        }
        protected void TreeView1_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                TextBox1.Text = TreeView1.SelectedNode != null ? TreeView1.SelectedNode.Text : "< Select Location >";
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView", "$(\"#divBranches1\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }


        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                //int userID = Convert.ToInt32(ddlUsers.SelectedValue);
                //int branchId = Convert.ToInt32(tvBranches.SelectedNode.Value);
                int userID = Convert.ToInt32(ddlUsers.SelectedValue);
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "MGMT")
                    {
                        customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    }
                    else
                {
                    customerID = Convert.ToInt32(ddlcustomer.SelectedValue);
            }

                if (!ddlComplianceCatagory.SelectedValue.Equals("All"))
                {
                    // if (branchId == -2) {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        var customerBranches = (from row in entities.CustomerBranches
                                                where row.IsDeleted == false && row.CustomerID == customerID
                                                select row);
                        // foreach (var node in customerBranches)
                        foreach (TreeNode node in tvBranches.CheckedNodes)
                        {
                            int CategoryId = Convert.ToInt32(ddlComplianceCatagory.SelectedValue);
                            var data = AssignEntityManagement.SelectEntityInternal(Convert.ToInt32(node.Value), userID, CategoryId);
                            if (data != null)
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Entity already assigned to location for the category.";
                                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideFilterTreeView", "$(\"#divBranches\").hide(\"blind\", null, 5, function () { });", true);
                            }
                            else
                            {
                                EntitiesAssignmentInternal objEntitiesAssignment = new EntitiesAssignmentInternal();
                                objEntitiesAssignment.UserID = userID;
                                objEntitiesAssignment.BranchID = Convert.ToInt32(node.Value);
                                objEntitiesAssignment.ComplianceCatagoryID = CategoryId;
                                objEntitiesAssignment.CreatedOn = DateTime.UtcNow;

                                AssignEntityManagement.CreateInternal(objEntitiesAssignment);
                            }
                            ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divAssignEntitiesDialog\").dialog('close');", true);

                        }
                    }
                }
                //    else
                //    {
                //        int CategoryId = Convert.ToInt32(ddlComplianceCatagory.SelectedValue);
                //        var data = AssignEntityManagement.SelectEntityInternal(branchId, userID, CategoryId);
                //        if (data != null)
                //        {
                //            cvDuplicateEntry.IsValid = false;
                //            cvDuplicateEntry.ErrorMessage = "Entity already assigned to location for the category.";
                //            ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideFilterTreeView", "$(\"#divBranches\").hide(\"blind\", null, 5, function () { });", true);
                //        }
                //        else
                //        {
                //            EntitiesAssignmentInternal objEntitiesAssignment = new EntitiesAssignmentInternal();
                //            objEntitiesAssignment.UserID = userID;
                //            objEntitiesAssignment.BranchID = branchId;
                //            objEntitiesAssignment.ComplianceCatagoryID = CategoryId;
                //            objEntitiesAssignment.CreatedOn = DateTime.UtcNow;
                //            AssignEntityManagement.CreateInternal(objEntitiesAssignment);
                //            ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divAssignEntitiesDialog\").dialog('close');", true);
                //        }
                //    }
                //}
                //else
                //{
                //    if (branchId == -2)
                //    {
                //        int customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);
                //        var CatagoryList = ComplianceCategoryManagement.GetAllInternalCompliancesCategories(customerid);
                //        //.Select(e =>e.CustomerID==AuthenticationHelper.CustomerID).ToList()
                //        CatagoryList = CatagoryList.Where(ea => ea.CustomerID == AuthenticationHelper.CustomerID).ToList();
                //        List<EntitiesAssignmentInternal> assignmentEntities = new List<EntitiesAssignmentInternal>();
                //        foreach (InternalCompliancesCategory catagory in CatagoryList)
                //        {
                //            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                //            {
                //                var customerBranches = (from row in entities.CustomerBranches
                //                                        where row.IsDeleted == false && row.CustomerID == AuthenticationHelper.CustomerID
                //                                        select row);
                //                foreach (var node in customerBranches)
                //                {

                //                    int CategoryId = Convert.ToInt32(catagory.ID);
                //                    var data = AssignEntityManagement.SelectEntityInternal(Convert.ToInt32(node.ID), userID, CategoryId);
                //                    if (data != null)
                //                    {
                //                    }
                //                    else
                //                    {
                //                        EntitiesAssignmentInternal objEntitiesAssignment = new EntitiesAssignmentInternal();
                //                        objEntitiesAssignment.UserID = userID;
                //                        objEntitiesAssignment.BranchID = Convert.ToInt32(node.ID);
                //                        objEntitiesAssignment.ComplianceCatagoryID = catagory.ID;
                //                        objEntitiesAssignment.CreatedOn = DateTime.UtcNow;
                //                        assignmentEntities.Add(objEntitiesAssignment);
                //                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divAssignEntitiesDialog\").dialog('close');", true);
                //                    }
                //                }
                //            }
                //        }
                //        AssignEntityManagement.CreateInternal(assignmentEntities);
                //        ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divAssignEntitiesDialog\").dialog('close');", true);
                //    }
                else
                {
                   
                    if (AuthenticationHelper.Role == "CADMN")
                    {
                        customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    }
                    else if (AuthenticationHelper.Role == "MGMT")
                    {
                        customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    }
                    else
                    {
                        customerID = Convert.ToInt32(ddlcustomer.SelectedValue);
                    }
                    var CatagoryList = ComplianceCategoryManagement.GetAllInternalCompliancesCategories(customerID);
                    //.Select(e =>e.CustomerID==AuthenticationHelper.CustomerID).ToList()
                    CatagoryList = CatagoryList.Where(ea => ea.CustomerID == customerID).ToList();
                    List<EntitiesAssignmentInternal> assignmentEntities = new List<EntitiesAssignmentInternal>();
                    // foreach (InternalCompliancesCategory catagory in CatagoryList)
                    foreach (TreeNode node in tvBranches.CheckedNodes)
                    {
                        int branchId1 = Convert.ToInt32(node.Value);
                        foreach (InternalCompliancesCategory catagory in CatagoryList)
                        {

                            var data = AssignEntityManagement.SelectEntityInternal(branchId1, userID, catagory.ID);
                            if (!(data != null))
                            {
                                EntitiesAssignmentInternal objEntitiesAssignment = new EntitiesAssignmentInternal();
                                objEntitiesAssignment.UserID = userID;
                                objEntitiesAssignment.BranchID = branchId1;
                                objEntitiesAssignment.ComplianceCatagoryID = catagory.ID;
                                objEntitiesAssignment.CreatedOn = DateTime.UtcNow;
                                assignmentEntities.Add(objEntitiesAssignment);

                                AssignEntityManagement.CreateInternal(objEntitiesAssignment);
                                //  AssignEntityManagement.CreateInternal(assignmentEntities);
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Entity already assigned to location for the category.";
                                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideFilterTreeView", "$(\"#divBranches\").hide(\"blind\", null, 5, function () { });", true);
                            }
                        }

                    }

                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divAssignEntitiesDialog\").dialog('close');", true);

                }
                BindComplianceEntityInstances();
            }
            catch (Exception ex)
            {

                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnAddComplianceType_Click(object sender, EventArgs e)
        {
            try
            {
                if (tvBranches.SelectedNode != null)
                {
                    tvBranches.SelectedNode.Selected = false;
                }

                lblErrorMassage.Text = "";
                ddlUsers.SelectedValue = "-1";
                ddlComplianceCatagory.SelectedValue = "-1";
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divAssignEntitiesDialog\").dialog('open');", true);
                tbxBranch.Text = "< Select Location >";
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView", "$(\"#divBranches\").hide(\"blind\", null, 5, function () { });", true);
                ForceCloseFilterBranchesTreeView();
                upCompliance.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void ForceCloseDeleteFilterBranchesTreeView()
        {
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideFilterTreeView", "$(\"#divBranches1\").hide(\"blind\", null, 5, function () { });", true);
        }
        protected void btnDeleteComplianceType_Click(object sender, EventArgs e)
        {
            try
            {
                if (TreeView1.SelectedNode != null)
                {
                    TreeView1.SelectedNode.Selected = false;
                }

                Label3.Text = "";
                delddluser.SelectedValue = "-1";
                delddlcomcatagory.SelectedValue = "-1";
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divDeleteEntitiesDialog\").dialog('open');", true);
                TextBox1.Text = "< Select Location >";
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView", "$(\"#divBranches1\").hide(\"blind\", null, 5, function () { });", true);
                // ForceCloseDeleteFilterBranchesTreeView();
                ForceCloseFilterBranchesTreeView();
                UpdatePanel3.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdAssignEntities_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdAssignEntities.PageIndex = e.NewPageIndex;
            BindComplianceEntityInstances();
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }
        //sandesh code start
        protected void btnlocation_Click(object sender, EventArgs e)
        {
            try
            {
                BindComplianceEntityInstances();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ChkBoxClear(TreeNode node)
        {

            if (node.Checked) // && node.ChildNodes.Count == 0 if (node.Checked)
            {
                node.Checked = false;
            }
            foreach (TreeNode tn in node.ChildNodes)
            {

                if (tn.Checked)//&& tn.ChildNodes.Count == 0)//  && tn.ChildNodes.Count == 0if (tn.Checked)              
                {
                    tn.Checked = false;
                }

                if (tn.ChildNodes.Count != 0)
                {
                    for (int i = 0; i < tn.ChildNodes.Count; i++)
                    {
                        ChkBoxClear(tn.ChildNodes[i]);
                    }
                }
            }
        }
        protected void btnClear1_Click(object sender, EventArgs e)
        {

            for (int i = 0; i < this.tvFilterLocation.Nodes.Count; i++)
            {
                ChkBoxClear(this.tvFilterLocation.Nodes[i]);
            }
        }

        protected void grdAssignEntities_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {

                int branchID = -1;
                if (tvFilterLocation.SelectedValue != "-1")
                {
                    branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                }

                int userID = -1;
                if ((!string.IsNullOrEmpty(ddlFilterUsers.SelectedValue)))
                {
                    userID = Convert.ToInt32(ddlFilterUsers.SelectedValue);
                }

                int customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;

                var assignmentList = AssignEntityManagement.SelectAllEntitiesInternal(branchID, userID, customerID);
                if (direction == SortDirection.Ascending)
                {
                    assignmentList = assignmentList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                }
                else
                {
                    assignmentList = assignmentList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                }

                foreach (DataControlField field in grdAssignEntities.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdAssignEntities.Columns.IndexOf(field);
                    }
                }

                grdAssignEntities.DataSource = assignmentList;
                grdAssignEntities.DataBind();
                tbxFilterLocation.Text = "< Select Location >";
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView", "$(\"#divFilterLocation\").hide(\"blind\", null, 5, function () { });", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdAssignEntities_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        #region[Delete]
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                long Uid = -1;
                long CatagoryId = -1;
                Uid = Convert.ToInt32(delddluser.SelectedValue);
                // if (!TextBox1.Text.Equals("< Select Location >"))
                // {
                if (!delddlcomcatagory.SelectedItem.Text.Equals("< Select >"))
                {
                    //if (TextBox1.Text.Equals("All"))
                    // {

                    if (delddlcomcatagory.SelectedItem.Text.Equals("Select All"))
                    {
                        //Branch and Category All selected
                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        {
                            var data = (from row in entities.EntitiesAssignmentInternals
                                        where row.UserID == Uid
                                        select row).ToList();

                            //foreach (var item in data)
                            //{
                            //    entities.EntitiesAssignmentInternals.RemoveRange(entities.EntitiesAssignmentInternals.Where(x => x.ID == item.ID));
                            //    entities.SaveChanges();
                            //}
                            foreach (TreeNode node in TreeView1.CheckedNodes)
                            {
                                var CID = Convert.ToInt32(node.Value);
                                //entities.EntitiesAssignments.RemoveRange(entities.EntitiesAssignments.Where(x => x.ID == Convert.ToInt32(node.Value)));
                                entities.EntitiesAssignmentInternals.RemoveRange(entities.EntitiesAssignmentInternals.Where(x => x.BranchID == CID && x.UserID == Uid).ToList());
                                entities.SaveChanges();
                            }
                        }
                    }
                    else
                    {
                        CatagoryId = Convert.ToInt32(delddlcomcatagory.SelectedValue);
                        //Branch all and Category selected
                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        {
                            var data = (from row in entities.EntitiesAssignmentInternals
                                        where row.UserID == Uid
                                        && row.ComplianceCatagoryID == CatagoryId
                                        select row).ToList();

                            //foreach (var item in data)
                            //{
                            //    entities.EntitiesAssignmentInternals.RemoveRange(entities.EntitiesAssignmentInternals.Where(x => x.ID == item.ID));
                            //    entities.SaveChanges();
                            //}

                            foreach (TreeNode node in TreeView1.CheckedNodes)
                            {
                                var CID = Convert.ToInt32(node.Value);
                                //entities.EntitiesAssignments.RemoveRange(entities.EntitiesAssignments.Where(x => x.ID == Convert.ToInt32(node.Value)));
                                entities.EntitiesAssignmentInternals.RemoveRange(entities.EntitiesAssignmentInternals.Where(x => x.BranchID == CID && x.UserID == Uid && x.ComplianceCatagoryID == CatagoryId).ToList());
                                entities.SaveChanges();
                            }


                        }
                    }

                    // }


                    //Branch Selected Category all
                    //if (delddlcomcatagory.SelectedItem.Text.Equals("Select All"))
                    //{
                    //    int branchId = Convert.ToInt32(TreeView1.SelectedNode.Value);
                    //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    //    {
                    //        var data = (from row in entities.EntitiesAssignmentInternals
                    //                    where row.UserID == Uid
                    //                    && row.BranchID == branchId
                    //                    select row).ToList();

                    //        foreach (var item in data)
                    //        {
                    //            entities.EntitiesAssignmentInternals.RemoveRange(entities.EntitiesAssignmentInternals.Where(x => x.ID == item.ID));
                    //            entities.SaveChanges();
                    //        }
                    //    }
                    //}
                    //else
                    //{
                    //    //Branch selected category selected
                    //    // int branchId = Convert.ToInt32(tvBranches.SelectedNode.Value);
                    //    if (!String.IsNullOrEmpty(delddluser.SelectedValue))
                    //    {
                    //        if (delddluser.SelectedValue != "-1")
                    //        {
                    //            Uid = Convert.ToInt32(delddluser.SelectedValue);
                    //        }
                    //    }

                    //    if (!String.IsNullOrEmpty(delddlcomcatagory.SelectedValue))
                    //    {
                    //        if (delddlcomcatagory.SelectedValue != "-1")
                    //        {
                    //            CatagoryId = Convert.ToInt32(delddlcomcatagory.SelectedValue);
                    //        }
                    //    }

                    //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    //    {
                    //        int branchId = Convert.ToInt32(TreeView1.SelectedNode.Value);
                    //        var AssignInternalID = (from row in entities.EntitiesAssignmentInternals
                    //                                where row.UserID == Uid && row.BranchID == branchId && row.ComplianceCatagoryID == CatagoryId
                    //                                select row).FirstOrDefault();

                    //        entities.EntitiesAssignmentInternals.Remove(AssignInternalID);
                    //        entities.SaveChanges();
                    //    }

                    //}

                }
                // }
                BindComplianceEntityInstances();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }


        #endregion

        protected void ddlcustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            int customerID = -1;
            if ((!string.IsNullOrEmpty(ddlcustomer.SelectedValue)) && ddlcustomer.SelectedValue != "-1")
            {
                customerID = Convert.ToInt32(ddlcustomer.SelectedValue);
            }
            BindLocation(customerID);
            BindCategory(customerID);
            BindUsers(ddlUsers, customerID);
        }
        protected void ddlFiltercustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            int customerID = -1;
            if ((!string.IsNullOrEmpty(ddlFiltercustomer.SelectedValue)) && ddlFiltercustomer.SelectedValue != "-1")
            {
                customerID = Convert.ToInt32(ddlFiltercustomer.SelectedValue);
            }
            BindComplianceEntityInstances();
            BindLocationFilter();
            BindFilterUsers(ddlFilterUsers, customerID);
        }


        protected void delddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            int customerID = -1;
            if ((!string.IsNullOrEmpty(delddlCustomer.SelectedValue)) && delddlCustomer.SelectedValue != "-1")
            {
                customerID = Convert.ToInt32(delddlCustomer.SelectedValue);
            }
            BindLocation(customerID);
            BindCategory(customerID);
            BindComplianceEntityInstances();
            BindDelUsers(delddluser, customerID);
        }

        protected void btnselect_Click(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView", "$(\"#divBranches\").hide(\"blind\", null, 5, function () { });", true);
                //BindComplianceEntityInstances();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnclear_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < this.tvBranches.Nodes.Count; i++)
            {
                ChkBoxClear(this.tvBranches.Nodes[i]);
            }
        }
    }
}