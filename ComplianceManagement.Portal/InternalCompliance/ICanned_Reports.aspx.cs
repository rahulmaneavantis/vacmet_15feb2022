﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using OfficeOpenXml;
using System.IO;
using OfficeOpenXml.Style;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;

namespace com.VirtuosoITech.ComplianceManagement.Portal.InternalCompliance
{
    public partial class ICanned_Reports : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void rblRole_SelectedIndexChanged(object sender, EventArgs e)
        {
            udcInternalCanned_ReportPerformer.Visible = udcInternalCanned_ReportReviewer.Visible = udcInternalCanned_ReportApprover.Visible = false;

            switch (rblRole.SelectedIndex)
            {
                case 0:
                    udcInternalCanned_ReportPerformer.Visible = true;
                    break;
                case 1:
                    udcInternalCanned_ReportReviewer.Visible = true;
                    break;
                case 2:
                    udcInternalCanned_ReportApprover.Visible = true;
                    break;
            }
        }

        protected void lbtnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add(rblRole.SelectedItem.Text + "[" + udcInternalCanned_ReportPerformer.GetFilter() + "]");

                    DataTable ExcelData = null;
                    string filter = "";
                    if (rblRole.SelectedIndex == 0)
                    {
                        DataView view = new System.Data.DataView(udcInternalCanned_ReportPerformer.GetGrid());
                        //ExcelData = view.ToTable("Selected", false, "Branch", "Description", "Status", "ScheduledOn");
                        ExcelData = view.ToTable("Selected", false, "Branch", "ShortDescription", "Status", "InternalScheduledOn");
                        filter = udcInternalCanned_ReportPerformer.GetFilter();
                    }
                    else if (rblRole.SelectedIndex == 1)
                    {
                        DataView view = new System.Data.DataView(udcInternalCanned_ReportReviewer.GetGrid());
                        //ExcelData = view.ToTable("Selected", false, "Branch", "Description", "Status", "ScheduledOn");
                        ExcelData = view.ToTable("Selected", false, "Branch", "ShortDescription", "Status", "InternalScheduledOn");
                        filter = udcInternalCanned_ReportReviewer.GetFilter();

                    }
                    else if (rblRole.SelectedIndex == 2)
                    {

                        filter = udcInternalCanned_ReportApprover.GetFilter();
                        if (filter.Equals("EntityByCategory"))
                        {
                            DataView view = new System.Data.DataView(udcInternalCanned_ReportApprover.GetGridMatrix());
                            ExcelData = view.ToTable();

                        }
                        else if (filter.Equals("CategoryByEntity"))
                        {
                            DataView view = new System.Data.DataView(udcInternalCanned_ReportApprover.GetGridMatrix());
                            ExcelData = view.ToTable();
                        }
                        else if (filter.Equals("RiskByEntity"))
                        {
                            DataView view = new System.Data.DataView(udcInternalCanned_ReportApprover.GetGridMatrix());
                            ExcelData = view.ToTable();
                        }
                        else
                        {
                            DataView view = new System.Data.DataView(udcInternalCanned_ReportApprover.GetGrid());
                            //ExcelData = view.ToTable("Selected", false, "Branch", "Description", "Status", "ScheduledOn");
                            ExcelData = view.ToTable("Selected", false, "Branch", "ShortDescription", "Status", "InternalScheduledOn");
                        }

                    }

                    exWorkSheet.Cells["A4"].LoadFromDataTable(ExcelData, true);
                    //Heading
                    if (!(filter.Equals("CategoryByEntity") || filter.Equals("RiskByEntity")))
                    {
                        exWorkSheet.Cells["A4"].Value = "Location";
                    }
                    exWorkSheet.Cells["C2"].Style.Font.Bold = true;
                    exWorkSheet.Cells["C2"].Style.Font.Size = 12;

                    exWorkSheet.Cells["A2"].Value = "InternalCannedReport_" + rblRole.SelectedItem.Text + "_" + filter;
                    exWorkSheet.Cells["A2"].Style.Font.Bold = true;
                    exWorkSheet.Cells["A2"].Style.Font.Size = 15;


                    using (ExcelRange col = exWorkSheet.Cells[2, 1, 4 + ExcelData.Rows.Count, 6])
                    {
                        col.Style.Numberformat.Format = "dd/MM/yyyy";
                        col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        col.AutoFitColumns();
                    }

                    Byte[] fileBytes = exportPackge.GetAsByteArray();
                    Response.Clear();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=" + rblRole.SelectedItem.Text + "_" + udcInternalCanned_ReportPerformer.GetFilter() + "_Report.xlsx");
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.ms-excel";
                    StringWriter sw = new StringWriter();
                    Response.BinaryWrite(fileBytes);
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}