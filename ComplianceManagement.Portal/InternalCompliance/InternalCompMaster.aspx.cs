﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using OfficeOpenXml;
using System.Data;
using OfficeOpenXml.Style;
using System.IO;
using System.Text.RegularExpressions;
using System.Globalization;

namespace com.VirtuosoITech.ComplianceManagement.Portal.InternalCompliance
{
    public partial class InternalCompMaster : System.Web.UI.Page
    {
        public static List<long> locationList = new List<long>();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (HttpContext.Current.Request.IsAuthenticated && AuthenticationHelper.Role != "EXCT")
                {
                    BindComplianceDetailsList(Convert.ToInt32(AuthenticationHelper.CustomerID));
                    BindUsers();
                    BindLocationFilter();
                    tbxFilterLocation.Text = "< Select >";
                }
                else
                {
                    FormsAuthentication.SignOut();
                    Session.Abandon();
                    FormsAuthentication.RedirectToLoginPage();
                }
            }
        }
        private void BindLocationFilter()
        {
            try
            {

                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }
                else
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                tvFilterLocation.Nodes.Clear();
                var bracnhes = CustomerBranchManagement.GetAllHierarchy(customerID);
                
                foreach (var item in bracnhes)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    BindBranchesHierarchy(node, item);
                    tvFilterLocation.Nodes.Add(node);
                }

                tvFilterLocation.CollapseAll();

                tvFilterLocation.CollapseAll();
                //tvFilterLocation_SelectedNodeChanged(null, null);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp)
        {
            try
            {
                foreach (var item in nvp.Children)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    BindBranchesHierarchy(node, item);
                    parent.ChildNodes.Add(node);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindUsers()
        {
            try
            {
                int customerID = customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                ddlFilterReviewer.DataTextField = "Name";
                ddlFilterReviewer.DataValueField = "ID";
                ddlFilterReviewer.Items.Clear();

                var users = UserManagement.GetAllNVP(customerID, ids: null, Flags: false);

                ddlFilterReviewer.DataSource = users;
                ddlFilterReviewer.DataBind();

                ddlFilterReviewer.Items.Insert(0, new ListItem("< Select >", "-1"));
            }

            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int ComplianceID = Convert.ToInt32(txtComplinceID.Text);

                    int CID = -1;
                    CID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                    locationList.Clear();
                    for (int i = 0; i < this.tvFilterLocation.Nodes.Count; i++)
                    {
                        RetrieveNodes(this.tvFilterLocation.Nodes[i]);
                    }

                    List<int> branchlst = (from rowI in entities.CustomerBranches
                                           where rowI.CustomerID == CID
                                           && rowI.IsDeleted == false
                                           && locationList.Contains(rowI.ID)
                                           select rowI.ID).ToList();
                    if (branchlst.Count > 0)
                    {
                        bool Firstflagvalidation = false;

                        var Complianceinstancelst = (from row1 in entities.InternalComplianceInstances
                                                     where row1.InternalComplianceID == ComplianceID
                                                       && branchlst.Contains(row1.CustomerBranchID)
                                                     && row1.IsDeleted == false
                                                     select row1).ToList();

                        if (Complianceinstancelst.Count > 0)
                        {
                            Firstflagvalidation = true;
                        }

                        if (!Firstflagvalidation)
                        {
                            //InternalComplianceInfoByCustomer data = (from row1 in entities.InternalComplianceInfoByCustomers
                            //                                         where row1.CustomerID == CID
                            //                                            && row1.ComplianceID == ComplianceID
                            //                                         select row1).FirstOrDefault();
                            //if (data != null)
                            //{
                            //    //data.IsDocumentCompulsory = chkIsDocument.Checked;
                            //    //data.IsApproverCompulsory = chkIsApproverData.Checked;
                            //    //entities.SaveChanges();

                            //    //Business.Data.InternalCompliance complianceToUpdate = (from rowI in entities.InternalCompliances
                            //    //                                                       where rowI.ID == ComplianceID
                            //    //                                                       select rowI).FirstOrDefault();
                            //    //if (complianceToUpdate != null)
                            //    //{
                            //    //    complianceToUpdate.UpdatedOn = DateTime.Now;
                            //    //    complianceToUpdate.UpdatedBy = Convert.ToInt32(AuthenticationHelper.UserID);
                            //    //    complianceToUpdate.IsDeleted = false;
                            //    //    //complianceToUpdate.IsDocumentRequired = chkIsDocument.Checked;
                            //    //    entities.SaveChanges();
                            //    //}
                            //}

                            //ScriptManager.RegisterStartupScript(this, GetType(), "Compliance Updated Successfully.", "Showalert();", true);
                            //upModifyDepartment.Update();
                        }
                        else
                        {
                            int perID = -1;
                            int revID = -1;
                            bool flagonlyrev = false;
                            bool firstflag = false;
                            if (chkIsDocument.Checked == false && string.IsNullOrEmpty(tbxdocumentDate.Text))
                            {
                                flagonlyrev = true;
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(tbxdocumentDate.Text))
                                {
                                    flagonlyrev = true;
                                    firstflag = true;
                                    DateTime getdocdate = Convert.ToDateTime(tbxdocumentDate.Text);
                                    var Complianceinstancelist = (from row1 in entities.InternalComplianceInstances
                                                                  where row1.InternalComplianceID == ComplianceID
                                                                   && branchlst.Contains(row1.CustomerBranchID)
                                                                        && row1.IsDeleted == false
                                                                  select row1).ToList();
                                    foreach (var Complianceinstance in Complianceinstancelist)
                                    {
                                        if (Complianceinstance != null)
                                        {
                                            bool checkexistdt = CheckComplianceDate(Convert.ToInt32(Complianceinstance.ID), Convert.ToDateTime(tbxdocumentDate.Text), chkIsDocument.Checked, true);
                                            if (checkexistdt)
                                            {
                                                CancelledInternalLogDetail obj1 = new CancelledInternalLogDetail();
                                                obj1.ComplianceInstanceID = Complianceinstance.ID;
                                                obj1.IsPerformer = chkIsDocument.Checked;
                                                obj1.IsActive = true;
                                                obj1.CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID);
                                                obj1.CreatedOn = DateTime.Now;
                                                obj1.Statuschanged = getdocdate;
                                                obj1.IsActive = true;
                                                obj1.IsForDocument = true;
                                                obj1.CustomerID = CID;
                                                obj1.CustBranchID = Complianceinstance.CustomerBranchID;
                                                entities.CancelledInternalLogDetails.Add(obj1);
                                                entities.SaveChanges();

                                                //InternalComplianceInfoByCustomer data = (from row1 in entities.InternalComplianceInfoByCustomers
                                                //                                         where row1.CustomerID == CID
                                                //                                            && row1.ComplianceID == ComplianceID
                                                //                                         select row1).FirstOrDefault();
                                                //if (data != null)
                                                //{
                                                //    data.IsDocumentCompulsory = chkIsDocument.Checked;
                                                //    entities.SaveChanges();
                                                //}

                                                //Business.Data.InternalCompliance complianceToUpdate = (from rowI in entities.InternalCompliances
                                                //                                                       where rowI.ID == ComplianceID
                                                //                                                       select rowI).FirstOrDefault();
                                                //if (complianceToUpdate != null)
                                                //{
                                                //    complianceToUpdate.UpdatedOn = DateTime.Now;
                                                //    complianceToUpdate.UpdatedBy = Convert.ToInt32(AuthenticationHelper.UserID);
                                                //    complianceToUpdate.IsDeleted = false;
                                                //    complianceToUpdate.IsDocumentRequired = chkIsDocument.Checked;
                                                //    entities.SaveChanges();
                                                //}
                                            }
                                            else
                                            {
                                                flagonlyrev = false;
                                                CustomModify.IsValid = false;
                                                CustomModify.ErrorMessage = "Please check last updated status and date.";
                                                vsLicenseListPage.CssClass = "alert alert-danger";
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    CustomModify.IsValid = false;
                                    CustomModify.ErrorMessage = "Please select document date.";
                                    vsLicenseListPage.CssClass = "alert alert-danger";
                                }
                            }
                            if (flagonlyrev)
                            {
                                bool flagvalidation = true;
                                if (string.IsNullOrEmpty(tbxStartDate.Text) && chkIsApproverData.Checked == false)
                                {
                                    flagvalidation = false;
                                }
                                else
                                {
                                    if (!string.IsNullOrEmpty(tbxStartDate.Text))
                                    {
                                        var Complianceinstancelist = (from row1 in entities.ComplianceInstances
                                                                      where row1.ComplianceId == ComplianceID
                                                                        && branchlst.Contains(row1.CustomerBranchID)
                                                                        && row1.IsDeleted == false
                                                                      select row1).ToList();

                                        foreach (var item in Complianceinstancelist)
                                        {
                                            bool checkexistdt = CheckComplianceDate(Convert.ToInt32(item.ID), Convert.ToDateTime(tbxStartDate.Text), chkIsApproverData.Checked, false);
                                            if (checkexistdt)
                                            {
                                                if (chkIsApproverData.Checked == true)
                                                {
                                                    if (ddlFilterReviewer.SelectedValue != "-1")
                                                    {
                                                        flagvalidation = true;
                                                    }
                                                    else
                                                    {
                                                        CustomModify.IsValid = false;
                                                        CustomModify.ErrorMessage = "Please select reviewer.";
                                                        vsLicenseListPage.CssClass = "alert alert-danger";
                                                        flagvalidation = false;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                CustomModify.IsValid = false;
                                                CustomModify.ErrorMessage = "Please check last updated status and date.";
                                                vsLicenseListPage.CssClass = "alert alert-danger";
                                                flagvalidation = false;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        CustomModify.IsValid = false;
                                        CustomModify.ErrorMessage = "Please select date.";
                                        vsLicenseListPage.CssClass = "alert alert-danger";
                                        flagvalidation = false;
                                    }
                                }
                                if (flagvalidation)
                                {
                                    DateTime getdate = Convert.ToDateTime(tbxStartDate.Text);

                                    if (ddlFilterReviewer.SelectedValue != "-1")
                                        revID = Convert.ToInt32(ddlFilterReviewer.SelectedValue);

                                    InternalComplianceInfoByCustomer data = (from row1 in entities.InternalComplianceInfoByCustomers
                                                                             where row1.CustomerID == CID
                                                                                && row1.ComplianceID == ComplianceID
                                                                             select row1).FirstOrDefault();
                                    if (data != null)
                                    {
                                        //data.IsApproverCompulsory = chkIsApproverData.Checked;
                                        //entities.SaveChanges();

                                        //Business.Data.InternalCompliance complianceToUpdate = (from rowI in entities.InternalCompliances
                                        //                                                       where rowI.ID == ComplianceID
                                        //                                                       select rowI).FirstOrDefault();
                                        //if (complianceToUpdate != null)
                                        //{
                                        //    complianceToUpdate.UpdatedOn = DateTime.Now;
                                        //    complianceToUpdate.UpdatedBy = Convert.ToInt32(AuthenticationHelper.UserID);
                                        //    complianceToUpdate.IsDeleted = false;
                                        //    complianceToUpdate.IsDocumentRequired = chkIsDocument.Checked;
                                        //    entities.SaveChanges();
                                        //}

                                        var Complianceinstancelist = (from row1 in entities.InternalComplianceInstances
                                                                      where row1.InternalComplianceID == ComplianceID
                                                                           && branchlst.Contains(row1.CustomerBranchID)
                                                                            && row1.IsDeleted == false
                                                                      select row1).ToList();

                                        foreach (var Complianceinstance in Complianceinstancelist)
                                        {
                                            if (Complianceinstance != null)
                                            {
                                                CancelledInternalLogDetail obj1 = new CancelledInternalLogDetail();
                                                obj1.ComplianceInstanceID = Complianceinstance.ID;
                                                obj1.IsPerformer = chkIsApproverData.Checked;
                                                obj1.IsActive = true;
                                                obj1.CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID);
                                                obj1.CreatedOn = DateTime.Now;
                                                obj1.Statuschanged = getdate;
                                                obj1.IsActive = true;
                                                obj1.IsForDocument = false;
                                                obj1.CustomerID = CID;
                                                obj1.CustBranchID = Complianceinstance.CustomerBranchID;
                                                entities.CancelledInternalLogDetails.Add(obj1);
                                                entities.SaveChanges();

                                                var compliancesStatusQuery = InternalComplianceManagement.GetComplianceScheduledOnDetails(Convert.ToInt32(Complianceinstance.ID));

                                                var gerdetailAssignment = (from row1 in entities.InternalComplianceAssignments
                                                                           where row1.InternalComplianceInstanceID == Complianceinstance.ID
                                                                           select row1).ToList();
                                                if (gerdetailAssignment.Count > 0)
                                                {
                                                    if (chkIsApproverData.Checked == false)
                                                    {
                                                        #region checked false

                                                        InternalComplianceAssignment dataassignmentperformerID = (from row1 in gerdetailAssignment
                                                                                                                  where row1.InternalComplianceInstanceID == Complianceinstance.ID
                                                                                                                  && row1.RoleID == 3
                                                                                                                  select row1).FirstOrDefault();
                                                        if (dataassignmentperformerID != null)
                                                        {
                                                            perID = Convert.ToInt32(dataassignmentperformerID.UserID);
                                                        }

                                                        InternalComplianceAssignment dataassignment = (from row1 in gerdetailAssignment
                                                                                                       where row1.InternalComplianceInstanceID == Complianceinstance.ID
                                                                                                       && row1.RoleID == 4
                                                                                                       select row1).FirstOrDefault();
                                                        if (dataassignment != null)
                                                        {
                                                            revID = Convert.ToInt32(dataassignment.UserID);

                                                            #region delete reminders
                                                            var data12 = (from row1 in entities.InternalComplianceReminders
                                                                          where row1.ComplianceAssignmentID == dataassignment.ID
                                                                          select row1).ToList();
                                                            if (data12.Count > 0)
                                                            {
                                                                foreach (var item in data12)
                                                                {
                                                                    InternalComplianceReminder rem = (from row1 in entities.InternalComplianceReminders
                                                                                                      where row1.ID == item.ID
                                                                                                      select row1).FirstOrDefault();
                                                                    entities.InternalComplianceReminders.Remove(rem);
                                                                    entities.SaveChanges();
                                                                }
                                                            }
                                                            #endregion

                                                            #region delete assignment
                                                            entities.InternalComplianceAssignments.Remove(dataassignment);
                                                            entities.SaveChanges();
                                                            #endregion
                                                        }

                                                        var checkexistlogDetails = (from row1 in entities.CancelledInternalComplianceAssignments
                                                                                    where row1.ComplianceInstanceID == Complianceinstance.ID
                                                                                    select row1).ToList();

                                                        if (checkexistlogDetails.Count == 0)
                                                        {
                                                            #region new cancelled log detail 
                                                            var ComplianceScheduledOnDetails = (from row1 in entities.InternalComplianceScheduledOns
                                                                                                where row1.InternalComplianceInstanceID == Complianceinstance.ID
                                                                                                && row1.IsActive == true
                                                                                                select row1).ToList();

                                                            foreach (var item in ComplianceScheduledOnDetails)
                                                            {
                                                                int SID = Convert.ToInt32(item.ID);
                                                                DateTime SDDT = Convert.ToDateTime(item.ScheduledOn);

                                                                int ComplianceStatusID = (from row1 in compliancesStatusQuery
                                                                                          where row1.InternalComplianceScheduledOnID == SID
                                                                                          select row1.ComplianceStatusID).FirstOrDefault();
                                                                //int ComplianceStatusID = Convert.ToInt32(1);



                                                                if (ComplianceStatusID == 1 && item.ScheduledOn >= getdate.Date)
                                                                {
                                                                    CancelledInternalComplianceAssignment Cancelled = (from row1 in entities.CancelledInternalComplianceAssignments
                                                                                                                       where row1.ComplianceInstanceID == Complianceinstance.ID
                                                                                                                       && row1.ScheduleonID == SID
                                                                                                                       && row1.RoleID == 3
                                                                                                                       select row1).FirstOrDefault();
                                                                    if (Cancelled == null)
                                                                    {
                                                                        CancelledInternalComplianceAssignment obj = new CancelledInternalComplianceAssignment();
                                                                        obj.ComplianceInstanceID = Complianceinstance.ID;
                                                                        obj.RoleID = 3;
                                                                        obj.UserID = perID;
                                                                        obj.ScheduleonID = SID;
                                                                        obj.CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID);
                                                                        obj.CreatedOn = DateTime.Now;
                                                                        obj.UpdatedBy = Convert.ToInt32(AuthenticationHelper.UserID);
                                                                        obj.UpdateOn = DateTime.Now;
                                                                        obj.ScheduledOn = SDDT;
                                                                        entities.CancelledInternalComplianceAssignments.Add(obj);
                                                                        entities.SaveChanges();
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    CancelledInternalComplianceAssignment Cancelled = (from row1 in entities.CancelledInternalComplianceAssignments
                                                                                                                       where row1.ComplianceInstanceID == Complianceinstance.ID
                                                                                                                       && row1.ScheduleonID == SID
                                                                                                                       && row1.RoleID == 4
                                                                                                                       select row1).FirstOrDefault();
                                                                    if (Cancelled == null)
                                                                    {
                                                                        CancelledInternalComplianceAssignment obj = new CancelledInternalComplianceAssignment();
                                                                        obj.ComplianceInstanceID = Complianceinstance.ID;
                                                                        obj.RoleID = 4;
                                                                        obj.UserID = revID;
                                                                        obj.ScheduleonID = SID;
                                                                        obj.CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID);
                                                                        obj.CreatedOn = DateTime.Now;
                                                                        obj.UpdatedBy = Convert.ToInt32(AuthenticationHelper.UserID);
                                                                        obj.UpdateOn = DateTime.Now;
                                                                        obj.ScheduledOn = SDDT;
                                                                        entities.CancelledInternalComplianceAssignments.Add(obj);
                                                                        entities.SaveChanges();
                                                                    }
                                                                }
                                                            }
                                                            #endregion
                                                        }
                                                        else
                                                        {
                                                            #region update compliance log detail
                                                            var ComplianceScheduledOnDetails = (from row1 in entities.InternalComplianceScheduledOns
                                                                                                where row1.InternalComplianceInstanceID == Complianceinstance.ID
                                                                                                && row1.IsActive == true
                                                                                                select row1).ToList();
                                                            foreach (var item in ComplianceScheduledOnDetails)
                                                            {
                                                                int SID = Convert.ToInt32(item.ID);
                                                                DateTime SDDT = Convert.ToDateTime(item.ScheduledOn);
                                                                int ComplianceStatusID = (from row1 in compliancesStatusQuery
                                                                                          where row1.InternalComplianceScheduledOnID == SID
                                                                                          select row1.ComplianceStatusID).FirstOrDefault();
                                                                //int ComplianceStatusID = Convert.ToInt32(1);
                                                                if (ComplianceStatusID == 1 && item.ScheduledOn >= getdate.Date)
                                                                {
                                                                    CancelledInternalComplianceAssignment Cancelled = (from row1 in entities.CancelledInternalComplianceAssignments
                                                                                                                       where row1.ComplianceInstanceID == Complianceinstance.ID
                                                                                                                       && row1.ScheduleonID == SID
                                                                                                                       && row1.RoleID == 3
                                                                                                                       select row1).FirstOrDefault();
                                                                    if (Cancelled == null)
                                                                    {
                                                                        CancelledInternalComplianceAssignment obj = new CancelledInternalComplianceAssignment();
                                                                        obj.ComplianceInstanceID = Complianceinstance.ID;
                                                                        obj.RoleID = 3;
                                                                        obj.UserID = perID;
                                                                        obj.ScheduleonID = SID;
                                                                        obj.CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID);
                                                                        obj.CreatedOn = DateTime.Now;
                                                                        obj.UpdatedBy = Convert.ToInt32(AuthenticationHelper.UserID);
                                                                        obj.UpdateOn = DateTime.Now;
                                                                        obj.ScheduledOn = SDDT;
                                                                        entities.CancelledInternalComplianceAssignments.Add(obj);
                                                                        entities.SaveChanges();
                                                                    }

                                                                    CancelledInternalComplianceAssignment Cancelled1 = (from row1 in entities.CancelledInternalComplianceAssignments
                                                                                                                        where row1.ComplianceInstanceID == Complianceinstance.ID
                                                                                                                        && row1.ScheduleonID == SID
                                                                                                                        && row1.RoleID == 4
                                                                                                                        select row1).FirstOrDefault();
                                                                    if (Cancelled1 != null)
                                                                    {
                                                                        entities.CancelledInternalComplianceAssignments.Remove(Cancelled1);
                                                                        entities.SaveChanges();
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    CancelledInternalComplianceAssignment Cancelled = (from row1 in entities.CancelledInternalComplianceAssignments
                                                                                                                       where row1.ComplianceInstanceID == Complianceinstance.ID
                                                                                                                       && row1.ScheduleonID == SID
                                                                                                                       select row1).FirstOrDefault();
                                                                    if (Cancelled == null)
                                                                    {
                                                                        CancelledInternalComplianceAssignment obj = new CancelledInternalComplianceAssignment();
                                                                        obj.ComplianceInstanceID = Complianceinstance.ID;
                                                                        obj.RoleID = 4;
                                                                        obj.UserID = revID;
                                                                        obj.ScheduleonID = SID;
                                                                        obj.CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID);
                                                                        obj.CreatedOn = DateTime.Now;
                                                                        obj.UpdatedBy = Convert.ToInt32(AuthenticationHelper.UserID);
                                                                        obj.UpdateOn = DateTime.Now;
                                                                        obj.ScheduledOn = SDDT;
                                                                        entities.CancelledInternalComplianceAssignments.Add(obj);
                                                                        entities.SaveChanges();
                                                                    }
                                                                }
                                                            }
                                                            #endregion
                                                        }
                                                        #endregion
                                                    }
                                                    else if (chkIsApproverData.Checked == true)
                                                    {
                                                        #region checked true                                   

                                                        InternalComplianceAssignment dataassignmentperformerID = (from row1 in gerdetailAssignment
                                                                                                                  where row1.InternalComplianceInstanceID == Complianceinstance.ID
                                                                                                                  && row1.RoleID == 3
                                                                                                                  select row1).FirstOrDefault();
                                                        if (dataassignmentperformerID != null)
                                                        {
                                                            perID = Convert.ToInt32(dataassignmentperformerID.UserID);
                                                        }

                                                        InternalComplianceAssignment dataassignment = (from row1 in gerdetailAssignment
                                                                                                       where row1.InternalComplianceInstanceID == Complianceinstance.ID
                                                                                                       && row1.RoleID == 4
                                                                                                       select row1).FirstOrDefault();
                                                        if (dataassignment != null)
                                                        {
                                                            dataassignment.UserID = revID;
                                                            dataassignment.InternalComplianceInstanceID = Complianceinstance.ID;
                                                            entities.SaveChanges();
                                                        }
                                                        else
                                                        {
                                                            InternalComplianceAssignment objassignment = new InternalComplianceAssignment();
                                                            objassignment.RoleID = 4;
                                                            objassignment.UserID = revID;
                                                            objassignment.InternalComplianceInstanceID = Complianceinstance.ID;
                                                            entities.InternalComplianceAssignments.Add(objassignment);
                                                            entities.SaveChanges();
                                                        }
                                                        var checkexistlogDetails = (from row1 in entities.CancelledInternalComplianceAssignments
                                                                                    where row1.ComplianceInstanceID == Complianceinstance.ID
                                                                                    select row1).ToList();
                                                        if (checkexistlogDetails.Count > 0)
                                                        {
                                                            #region update compliance log detail
                                                            var ComplianceScheduledOnDetails = (from row1 in entities.InternalComplianceScheduledOns
                                                                                                where row1.InternalComplianceInstanceID == Complianceinstance.ID
                                                                                                && row1.IsActive == true
                                                                                                select row1).ToList();

                                                            if (ComplianceScheduledOnDetails.Count > 0)
                                                            {
                                                                foreach (var item in ComplianceScheduledOnDetails)
                                                                {
                                                                    int SID = Convert.ToInt32(item.ID);
                                                                    DateTime SDDT = Convert.ToDateTime(item.ScheduledOn);
                                                                    int ComplianceStatusID = (from row1 in compliancesStatusQuery
                                                                                              where row1.InternalComplianceScheduledOnID == SID
                                                                                              select row1.ComplianceStatusID).FirstOrDefault();
                                                                    //int ComplianceStatusID = Convert.ToInt32(1);
                                                                    if (ComplianceStatusID == 1 && item.ScheduledOn >= getdate.Date)
                                                                    {
                                                                        CancelledInternalComplianceAssignment Cancelled = (from row1 in entities.CancelledInternalComplianceAssignments
                                                                                                                           where row1.ComplianceInstanceID == Complianceinstance.ID
                                                                                                                           && row1.ScheduleonID == SID
                                                                                                                           && row1.RoleID == 3
                                                                                                                           select row1).FirstOrDefault();
                                                                        if (Cancelled != null)
                                                                        {
                                                                            entities.CancelledInternalComplianceAssignments.Remove(Cancelled);
                                                                            entities.SaveChanges();
                                                                        }
                                                                        CancelledInternalComplianceAssignment Cancelled1 = (from row1 in entities.CancelledInternalComplianceAssignments
                                                                                                                            where row1.ComplianceInstanceID == Complianceinstance.ID
                                                                                                                            && row1.ScheduleonID == SID
                                                                                                                            && row1.RoleID == 4
                                                                                                                            select row1).FirstOrDefault();
                                                                        if (Cancelled1 != null)
                                                                        {
                                                                            entities.CancelledInternalComplianceAssignments.Remove(Cancelled1);
                                                                            entities.SaveChanges();
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            #endregion
                                                        }
                                                        else
                                                        {
                                                            #region New compliance log detail

                                                            var ComplianceScheduledOnDetails = (from row1 in entities.InternalComplianceScheduledOns
                                                                                                where row1.InternalComplianceInstanceID == Complianceinstance.ID
                                                                                                && row1.IsActive == true
                                                                                                select row1).ToList();

                                                            if (ComplianceScheduledOnDetails.Count > 0)
                                                            {
                                                                foreach (var item in ComplianceScheduledOnDetails)
                                                                {
                                                                    int SID = Convert.ToInt32(item.ID);
                                                                    DateTime SDDT = Convert.ToDateTime(item.ScheduledOn);
                                                                    int ComplianceStatusID = (from row1 in compliancesStatusQuery
                                                                                              where row1.InternalComplianceScheduledOnID == SID
                                                                                              select row1.ComplianceStatusID).FirstOrDefault();
                                                                    //int ComplianceStatusID = Convert.ToInt32(1);
                                                                    if (ComplianceStatusID == 1 && item.ScheduledOn >= getdate.Date)
                                                                    {
                                                                        CancelledInternalComplianceAssignment Cancelled = (from row1 in entities.CancelledInternalComplianceAssignments
                                                                                                                           where row1.ComplianceInstanceID == Complianceinstance.ID
                                                                                                                           && row1.ScheduleonID == SID
                                                                                                                           && row1.RoleID == 4
                                                                                                                           select row1).FirstOrDefault();
                                                                        if (Cancelled == null)
                                                                        {
                                                                            entities.CancelledInternalComplianceAssignments.Remove(Cancelled);
                                                                            entities.SaveChanges();
                                                                        }
                                                                        CancelledInternalComplianceAssignment Cancelled1 = (from row1 in entities.CancelledInternalComplianceAssignments
                                                                                                                            where row1.ComplianceInstanceID == Complianceinstance.ID
                                                                                                                            && row1.ScheduleonID == SID
                                                                                                                            && row1.RoleID == 3
                                                                                                                            select row1).FirstOrDefault();

                                                                        if (Cancelled1 != null)
                                                                        {
                                                                            entities.CancelledInternalComplianceAssignments.Remove(Cancelled1);
                                                                            entities.SaveChanges();
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        CancelledInternalComplianceAssignment Cancelled = (from row1 in entities.CancelledInternalComplianceAssignments
                                                                                                                           where row1.ComplianceInstanceID == Complianceinstance.ID
                                                                                                                           && row1.ScheduleonID == SID
                                                                                                                           && row1.RoleID == 4
                                                                                                                           select row1).FirstOrDefault();

                                                                        if (Cancelled != null)
                                                                        {
                                                                            entities.CancelledInternalComplianceAssignments.Remove(Cancelled);
                                                                            entities.SaveChanges();
                                                                        }

                                                                        CancelledInternalComplianceAssignment Cancelled1 = (from row1 in entities.CancelledInternalComplianceAssignments
                                                                                                                            where row1.ComplianceInstanceID == Complianceinstance.ID
                                                                                                                            && row1.ScheduleonID == SID
                                                                                                                            && row1.RoleID == 3
                                                                                                                            select row1).FirstOrDefault();

                                                                        if (Cancelled1 == null)
                                                                        {
                                                                            CancelledInternalComplianceAssignment obj = new CancelledInternalComplianceAssignment();
                                                                            obj.ComplianceInstanceID = Complianceinstance.ID;
                                                                            obj.RoleID = 3;
                                                                            obj.UserID = perID;
                                                                            obj.ScheduleonID = SID;
                                                                            obj.CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID);
                                                                            obj.CreatedOn = DateTime.Now;
                                                                            obj.UpdatedBy = Convert.ToInt32(AuthenticationHelper.UserID);
                                                                            obj.UpdateOn = DateTime.Now;
                                                                            obj.ScheduledOn = SDDT;
                                                                            entities.CancelledInternalComplianceAssignments.Add(obj);
                                                                            entities.SaveChanges();
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            #endregion
                                                        }
                                                        #endregion
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    ScriptManager.RegisterStartupScript(this, GetType(), "Compliance Updated Successfully.", "Showalert();", true);
                                    upModifyDepartment.Update();
                                }
                                else
                                {
                                    if (firstflag)
                                    {
                                        ScriptManager.RegisterStartupScript(this, GetType(), "Compliance Updated Successfully.", "Showalert();", true);
                                        upModifyDepartment.Update();
                                    }
                                    else
                                    {
                                        if (chkIsApproverData.Checked)
                                        {
                                            reviwer1.Style.Add("display", "revert");
                                            reviwer2.Style.Add("display", "revert");
                                        }
                                        else
                                        {
                                            reviwer1.Style.Add("display", "none");
                                            reviwer2.Style.Add("display", "none");
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (chkIsApproverData.Checked)
                                {
                                    reviwer1.Style.Add("display", "revert");
                                    reviwer2.Style.Add("display", "revert");
                                }
                                else
                                {
                                    reviwer1.Style.Add("display", "none");
                                    reviwer2.Style.Add("display", "none");
                                }
                            }
                        }
                    }
                    else
                    {
                        CustomModify.IsValid = false;
                        CustomModify.ErrorMessage = "Please select at least one branch.";
                        vsLicenseListPage.CssClass = "alert alert-danger";                        
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                vsLicenseListPage.CssClass = "alert alert-danger";
            }
        }
        protected void RetrieveNodes(TreeNode node)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                if (node.Checked)
                {
                    if (Convert.ToInt32(node.Value) != customerID)
                    {
                        if (!locationList.Contains(Convert.ToInt32(node.Value)))
                        {
                            locationList.Add(Convert.ToInt32(node.Value));
                        }
                    }
                    if (node.ChildNodes.Count != 0)
                    {
                        for (int i = 0; i < node.ChildNodes.Count; i++)
                        {
                            RetrieveNodes(node.ChildNodes[i]);
                        }
                    }
                }
                else
                {
                    foreach (TreeNode tn in node.ChildNodes)
                    {
                        if (tn.Checked)
                        {
                            if (Convert.ToInt32(tn.Value) != customerID)
                            {
                                if (!locationList.Contains(Convert.ToInt32(tn.Value)))
                                {
                                    locationList.Add(Convert.ToInt32(tn.Value));
                                }
                            }
                        }
                        if (tn.ChildNodes.Count != 0)
                        {
                            for (int i = 0; i < tn.ChildNodes.Count; i++)
                            {
                                RetrieveNodes(tn.ChildNodes[i]);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        private void BindInstanceScheduled(int ComplianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<long> Complianceinstancelst = new List<long>();
                int CID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                locationList.Clear();
                for (int i = 0; i < this.tvFilterLocation.Nodes.Count; i++)
                {
                    RetrieveNodes(this.tvFilterLocation.Nodes[i]);
                }

                List<int> branchlst = (from rowI in entities.CustomerBranches
                                       where rowI.CustomerID == CID
                                       && rowI.IsDeleted == false
                                       && locationList.Contains(rowI.ID)
                                       select rowI.ID).ToList();


                if (branchlst.Count > 0)
                {
                    Complianceinstancelst = (from row1 in entities.InternalComplianceInstances
                                             where row1.InternalComplianceID == ComplianceID
                                             && branchlst.Contains(row1.CustomerBranchID)
                                             && row1.IsDeleted == false
                                             select row1.ID).ToList();
                }
                else
                {
                    Complianceinstancelst = (from row1 in entities.InternalComplianceInstances
                                             where row1.InternalComplianceID == ComplianceID
                                             //&& branchlst.Contains(row1.CustomerBranchID)
                                             && row1.IsDeleted == false
                                             select row1.ID).ToList();
                }
                var compliancesQuery = InternalComplianceManagement.GetComplianceLogDetails(Complianceinstancelst);

                grdrev.DataSource = compliancesQuery;
                grdrev.DataBind();
            }                
        }

        private bool CheckInt(string val)
        {
            try
            {
                int i = Convert.ToInt32(val);
                return true;
            }
            catch
            {
                return false;
            }
        }

        private bool CheckComplianceDate(int ComplianceIntsanceID, DateTime selectedDt,bool Ischecked,bool isdocument)
        {
            bool flag = false;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    
                        var Complianceinstance = (from row1 in entities.InternalComplianceInstances
                                                  where row1.ID == ComplianceIntsanceID
                                                  && row1.IsDeleted == false
                                                  select row1).FirstOrDefault();

                        if (Complianceinstance != null)
                        {
                            var Query = (from row in entities.CancelledInternalLogDetails
                                         where row.ComplianceInstanceID == (int)Complianceinstance.ID
                                         where row.IsForDocument == isdocument
                                         select row).OrderByDescending(x => x.Statuschanged).FirstOrDefault();
                            if (Query != null)
                            {
                                DateTime d1 = Query.Statuschanged;
                                bool laststatus = Query.IsPerformer;

                                if (selectedDt > d1)
                                {
                                    if (Ischecked != laststatus)
                                    {
                                        flag = true;
                                    }
                                }
                            }
                            else
                            {
                                flag = true;
                            }
                        }
                        else
                        {
                            flag = true;
                        }
                   
                }
                return flag;
            }
            catch
            {
                return flag;
            }
        }

        private void BindComplianceDetailsList(int CustomerID)
        {
            try
            {
                int CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
               
                var compliancesQuery = InternalComplianceManagement.GetComplianceManagementMaster(CustId);
               
                if (!string.IsNullOrEmpty(tbxFilter.Text))
                {
                    if (CheckInt(tbxFilter.Text))
                    {
                        int a = Convert.ToInt32(tbxFilter.Text.ToUpper());
                        compliancesQuery = compliancesQuery.Where(entry => entry.ComplianceID == a).ToList();
                    }
                    else
                    {
                        compliancesQuery = compliancesQuery.Where(entry => entry.ShortDescription.ToUpper().Contains(tbxFilter.Text.ToUpper()) || (entry.RISK.ToUpper() == tbxFilter.Text.ToUpper())).ToList();
                    }
                }
                grdCompliances.DataSource = compliancesQuery;
                Session["TotalRows"] = compliancesQuery.Count;
                grdCompliances.DataBind();
                
                upModifyDepartment.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
     
        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                grdCompliances.PageIndex = 0;
                int customerID = -1;

                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                if (customerID != -1)
                {
                    BindComplianceDetailsList(customerID);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void upDepeList_Load(object sender, EventArgs e)
        {
        }
     
        protected void upModifyAssignment_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);

                DateTime date = DateTime.MinValue;
                if (DateTime.TryParseExact(tbxStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeConfirmDatePicker", string.Format("initializeConfirmDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeConfirmDatePicker", "initializeConfirmDatePicker(null);", true);
                }
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeConfirmDatePicker", "initializeConfirmDatePicker();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }      
      
        public void ErrorMessages(List<string> emsg)
        {
            string finalErrMsg = string.Empty;
            finalErrMsg += "<ol type='1'>";
            if (emsg.Count > 0)
            {
                emsg.ForEach(eachErrMsg =>
                {
                    finalErrMsg += "<li>" + eachErrMsg + "</li>";
                });

                finalErrMsg += "</ol>";
            }

            cvDuplicateEntry.IsValid = false;
            cvDuplicateEntry.ErrorMessage = finalErrMsg;
        }

        protected void grdCompliances_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdCompliances.PageIndex = e.NewPageIndex;
                int customerID = -1;

                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                if (customerID != -1)
                {
                    BindComplianceDetailsList(customerID);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdCompliances_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                txtComplinceID.Text = "";
                int ComplianceID = Convert.ToInt32(e.CommandArgument);
                if (e.CommandName.Equals("EDIT_Detail"))
                {
                    chkIsDocument.Checked = true;
                    chkIsApproverData.Checked = true;
                    txtComplinceID.Text = ComplianceID.ToString();
                    GridViewRow row = (GridViewRow)(((Control)e.CommandSource).NamingContainer);
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        int CID = -1;
                        CID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                        InternalComplianceInfoByCustomer data = (from row1 in entities.InternalComplianceInfoByCustomers
                                                                 where row1.CustomerID == CID
                                                         && row1.ComplianceID == ComplianceID
                                                                 select row1).FirstOrDefault();
                        if (data != null)
                        {
                            chkIsDocument.Checked = data.IsDocumentCompulsory;
                            chkIsApproverData.Checked = false;
                            reviwer1.Style.Add("display", "none");
                            reviwer2.Style.Add("display", "none");
                            
                            //chkIsApproverData.Checked = data.IsApproverCompulsory;
                            //if (chkIsApproverData.Checked)
                            //{
                            //    reviwer1.Style.Add("display", "revert");
                            //    reviwer2.Style.Add("display", "revert");
                            //}
                            //else
                            //{
                            //    reviwer1.Style.Add("display", "none");
                            //    reviwer2.Style.Add("display", "none");
                            //}
                        }                        
                    }

                    BindInstanceScheduled(Convert.ToInt32(ComplianceID));

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "OpenDialog", "$(\"#divModifyDepartment\").dialog('open');", true);
                    upModifyDepartment.Update();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdCompliances_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox IsDoc = (CheckBox)e.Row.FindControl("chkIsDocumentData");
                    CheckBox IsApprover = (CheckBox)e.Row.FindControl("chkIsApproverData");

                    TextBox txtIsDocumentCompulsory = (TextBox)e.Row.FindControl("txtIsDocumentCompulsory");
                    TextBox txtIsApproverCompulsory = (TextBox)e.Row.FindControl("txtIsApproverCompulsory");

                    IsDoc.Checked = false;
                    IsApprover.Checked = false;

                    if (txtIsDocumentCompulsory.Text != "" && txtIsApproverCompulsory.Text != "")
                    {
                        if (txtIsDocumentCompulsory.Text == "True")
                        {
                            IsDoc.Checked = true;
                        }
                        if (txtIsApproverCompulsory.Text == "True")
                        {
                            IsApprover.Checked = true;
                        }


                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlFilterAct_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindComplianceDetailsList(Convert.ToInt32(AuthenticationHelper.CustomerID));
        }

        protected void grdrev_PageIndexChanged(object sender, EventArgs e)
        {
           
        }

        protected void grdrev_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdrev.PageIndex = e.NewPageIndex;
                int ComplianceID = Convert.ToInt32(txtComplinceID.Text);
                BindInstanceScheduled(Convert.ToInt32(ComplianceID));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        
        protected void btnbindgrid_Click(object sender, EventArgs e)
        {
            try
            {
                BindComplianceDetailsList(Convert.ToInt32(AuthenticationHelper.CustomerID));
                //using (ComplianceDBEntities entities = new ComplianceDBEntities())
                //{
                //    int ComplianceID = Convert.ToInt32(txtComplinceID.Text);
                //    var datainstance = (from row1 in entities.InternalComplianceInstances
                //                        where row1.InternalComplianceID == ComplianceID
                //                        select row1).FirstOrDefault();
                //    if (datainstance != null)
                //    {
                //        BindInstanceScheduled(Convert.ToInt32(datainstance.ID));
                //    }
                //}
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnlocation_Click(object sender, EventArgs e)
        {
            try
            {
                int ComplianceID = Convert.ToInt32(txtComplinceID.Text);
                BindInstanceScheduled(Convert.ToInt32(ComplianceID));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnClear1_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < this.tvFilterLocation.Nodes.Count; i++)
            {
                ChkBoxClear(this.tvFilterLocation.Nodes[i]);
            }
        }
        protected void ChkBoxClear(TreeNode node)
        {

            if (node.Checked) // && node.ChildNodes.Count == 0 if (node.Checked)
            {
                node.Checked = false;
            }
            foreach (TreeNode tn in node.ChildNodes)
            {

                if (tn.Checked)//&& tn.ChildNodes.Count == 0)//  && tn.ChildNodes.Count == 0if (tn.Checked)              
                {
                    tn.Checked = false;
                }

                if (tn.ChildNodes.Count != 0)
                {
                    for (int i = 0; i < tn.ChildNodes.Count; i++)
                    {
                        ChkBoxClear(tn.ChildNodes[i]);
                    }
                }
            }
        }
    }
}