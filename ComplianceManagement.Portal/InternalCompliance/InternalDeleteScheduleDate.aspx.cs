﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
namespace com.VirtuosoITech.ComplianceManagement.Portal.InternalCompliance
{
    public partial class InternalDeleteScheduleDate : System.Web.UI.Page
    {
        public DateTime? OldscheduleOnDate { get; private set; }
        string scheduleid1 = "";
        string scheduleid = "";
        public static int userID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                userID = Convert.ToInt32(AuthenticationHelper.UserID);
                BindCustomersList(userID);
                tbxFilterLocation.Text = "< Select >";
                tbxFilterLocation.Attributes.Add("readonly", "readonly");
                ddlInternalCompliance.SelectedValue = "-1";

            }
        }

        private void BindCustomersList(int userID)
        {
            ddlCustomer.DataTextField = "Name";
            ddlCustomer.DataValueField = "ID";
           // ddlCustomer.DataSource = CustomerManagement.GetAll(-1);
            ddlCustomer.DataSource = Assigncustomer.GetAllCustomer(userID);
            ddlCustomer.DataBind();
            ddlCustomer.Items.Insert(0, new ListItem("< Select Customer>", "-1"));
        }
        private void BindCompliances(long BranchID)
        {
            try
            {
                ddlInternalCompliance.DataTextField = "Name";
                ddlInternalCompliance.DataValueField = "ID";

                ddlInternalCompliance.DataSource = GetAllAssignedCompliances(BranchID);
                ddlInternalCompliance.DataBind();

                ddlInternalCompliance.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
      
        protected void upAct_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
                DateTime date = DateTime.Now;
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static List<object> GetAllAssignedCompliances( long BranchID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var IShortDescription = (from row1 in entities.InternalCompliances
                                        join row2 in entities.InternalComplianceInstances
                                        on row1.ID equals row2.InternalComplianceID
                                        where row1.IsDeleted == false 
                                        && row2.CustomerBranchID == BranchID && row1.EventFlag == null
                                        orderby row1.IShortDescription ascending
                                        select new { ID = row1.ID, Name = row1.IShortDescription }).Distinct().OrderBy(entry => entry.Name).ToList<object>();

                return IShortDescription;
            }
        }
       
   
      
        protected void ddlInternalCompliance_SelectedIndexChanged(object sender, EventArgs e)
        {
            grdInternalCompliances.DataSource = null;
            grdInternalCompliances.DataBind();
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (!string.IsNullOrEmpty(ddlInternalCompliance.SelectedValue) && !string.IsNullOrEmpty(ddlCustomer.SelectedValue) && !string.IsNullOrEmpty(tvFilterLocation.SelectedValue))
                {
                 
                    long complianced = Convert.ToInt32(ddlInternalCompliance.SelectedValue);
                    long customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                    long Locbranchid = Convert.ToInt32(tvFilterLocation.SelectedValue);

                    var ScheduleList = (from row1 in entities.InternalCompliances
                                        join row2 in entities.InternalComplianceInstances
                                        on row1.ID equals row2.InternalComplianceID
                                        join row4 in entities.InternalComplianceScheduledOns
                                        on row2.ID equals row4.InternalComplianceInstanceID
                                        join row5 in entities.CustomerBranches
                                        on row2.CustomerBranchID equals row5.ID
                                        join row6 in entities.Customers
                                        on row5.CustomerID equals row6.ID
                                        where row1.IsDeleted == false
                                        && row1.ID == complianced && row5.ID == Locbranchid
                                        && row6.ID == customerID
                                        && row4.IsUpcomingNotDeleted == true && row4.IsActive == true
                                        select new
                                        {
                                            CustomerName = row6.Name,
                                            CustomerBranchName = row5.Name,
                                            row1.IShortDescription,
                                            row4.ScheduledOn
                                        }).Distinct();

                    var compliances = ScheduleList.ToList();
                    grdInternalCompliances.DataSource = compliances;
                    grdInternalCompliances.DataBind();
                }
            }
        }
        protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            tvFilterLocation.Nodes.Clear();
            BindLocationFilter();
        }
        private void BindLocationFilter()
        {
            try
            {

                long customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                var bracnhes = CustomerBranchManagement.GetAllHierarchy(Convert.ToInt32(customerID));
                foreach (var item in bracnhes)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    BindBranchesHierarchy(node, item);
                    tvFilterLocation.Nodes.Add(node);
                }

                tvFilterLocation.CollapseAll();


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp)
        {
            try
            {
                foreach (var item in nvp.Children)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    BindBranchesHierarchy(node, item);
                    parent.ChildNodes.Add(node);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            tbxFilterLocation.Text = tvFilterLocation.SelectedNode != null ? tvFilterLocation.SelectedNode.Text : "< Select Location >";
            int Locbranchid = Convert.ToInt32(tvFilterLocation.SelectedValue);
            BindCompliances(Locbranchid);
        }
        public static void DeleteComplianceScheduleOn(long scheduleOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                InternalComplianceScheduledOn IcomplianceScheduleOn = (from row in entities.InternalComplianceScheduledOns
                                                             where row.ID == scheduleOnID// && row.IsActive==true &&
                                                             //row.IsUpcomingNotDeleted==true
                                                             select row).FirstOrDefault();

                IcomplianceScheduleOn.IsActive = false;
                IcomplianceScheduleOn.IsUpcomingNotDeleted = false;
                entities.SaveChanges();

            }

        }
        public void CreateLogForScheduleOn(string ScheduleIDList, DateTime? OldscheduleOnDate, DateTime NewscheduleOnDate)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    Log_InternalComplianceInstance logchangedate = new Log_InternalComplianceInstance()
                    {
                        Schedule_On_ID = Convert.ToString(ScheduleIDList),
                        Previous_Start_Date = OldscheduleOnDate,
                        New_Instance_Start_Date = NewscheduleOnDate,
                        CreatedBy = AuthenticationHelper.UserID,
                        CreatedOn = DateTime.Now,
                    };

                    entities.Log_InternalComplianceInstance.Add(logchangedate);
                    entities.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btndelete_Click(object sender, EventArgs e)
        {
            try
            {
               
                long Icomplianceid = -1;
                long Locbranchid = -1;
                
                if (!string.IsNullOrEmpty(ddlInternalCompliance.SelectedValue))
                {
                    Icomplianceid = Convert.ToInt32(ddlInternalCompliance.SelectedValue);
                }
                if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue))
                {
                    Locbranchid = Convert.ToInt32(tvFilterLocation.SelectedValue);
                }
                if (Icomplianceid != -1 && Locbranchid != -1)
                {

                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        DateTime NewscheduleOnDate = DateTime.ParseExact(tbxStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        var IcomplianceInstances = (from row in entities.InternalComplianceInstances
                                                   where row.IsDeleted == false && row.InternalComplianceID == Icomplianceid && row.CustomerBranchID == Locbranchid
                                                   select row).GroupBy(entity => entity.ID).Select(entity => entity.FirstOrDefault()).ToList();

                        DateTime? OldStartDate = null;
                        IcomplianceInstances.ForEach(IcomplianceInstancesentry =>
                        {
                            var IcomplianceScheduleOns = (from row in entities.InternalComplianceScheduledOns
                                                         where row.InternalComplianceInstanceID == IcomplianceInstancesentry.ID &&
                                                         row.ScheduledOn <= NewscheduleOnDate &&
                                                         row.IsActive == true && row.IsUpcomingNotDeleted == true
                                                         select row).ToList();

                            if (IcomplianceScheduleOns.Count > 0)
                            {
                                IcomplianceScheduleOns.ForEach(IcomplianceScheduleOnssentry =>
                                {

                                    DeleteComplianceScheduleOn(IcomplianceScheduleOnssentry.ID);
                                    scheduleid1 += IcomplianceScheduleOnssentry.ID.ToString() + " ,";
                                });
                                if (IcomplianceScheduleOns.Count == 0)
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Record not found.";
                                }
                                else
                                {
                                    scheduleid1 = scheduleid1.Trim(',');
                                    scheduleid = scheduleid1;
                                    OldStartDate = IcomplianceInstancesentry.StartDate;
                                    CreateLogForScheduleOn(scheduleid, OldStartDate, NewscheduleOnDate);
                                    ChangeDateScheduleOn(IcomplianceInstancesentry.ID, NewscheduleOnDate);
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Record Updated Sucessfully.";
                                }
                            }
                            else
                            {
                                OldStartDate = IcomplianceInstancesentry.StartDate;
                                CreateLogForScheduleOn("0", OldStartDate, NewscheduleOnDate);
                                ChangeDateScheduleOn(IcomplianceInstancesentry.ID, NewscheduleOnDate);
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Record Updated Sucessfully.";
                            }
                        });

                    }
                    grdInternalCompliances.DataSource = null;
                    tbxStartDate.Text = "";
                    grdInternalCompliances.DataBind();
                    ddlCustomer.SelectedValue = "-1";
                    ddlInternalCompliance.SelectedValue = "-1";
                    tbxFilterLocation.Text = "< Select >";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }

        protected void btnKeep_Click(object sender, EventArgs e)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                   
                    long Icomplianceid = -1;
                    long Locbranchid = -1;
                 
                    if (!string.IsNullOrEmpty(ddlInternalCompliance.SelectedValue))
                    {
                        Icomplianceid = Convert.ToInt32(ddlInternalCompliance.SelectedValue);
                    }
                    if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue))
                    {
                        Locbranchid = Convert.ToInt32(tvFilterLocation.SelectedValue);
                    }
                    if ( Icomplianceid != -1 && Locbranchid != -1)
                    {
                        DateTime NewscheduleOnDate = DateTime.ParseExact(tbxStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        var IcomplianceInstances = (from row in entities.InternalComplianceInstances
                                                   where row.IsDeleted == false && row.InternalComplianceID == Icomplianceid && row.CustomerBranchID == Locbranchid
                                                   select row).GroupBy(entity => entity.ID).Select(entity => entity.FirstOrDefault()).ToList();
                        DateTime? OldStartDate = null;

                        IcomplianceInstances.ForEach(IcomplianceInstancesentry =>
                        {
                            var IcomplianceScheduleOns = (from row in entities.InternalComplianceScheduledOns
                                                         where row.InternalComplianceInstanceID == IcomplianceInstancesentry.ID &&
                                                         row.ScheduledOn <= NewscheduleOnDate &&
                                                         row.IsActive == true && row.IsUpcomingNotDeleted == true
                                                         select row).ToList();

                            if (IcomplianceScheduleOns.Count > 0)
                            {
                                IcomplianceScheduleOns.ForEach(IcomplianceScheduleOnssentry =>
                                {
                                    var complianceTransactions = (from row in entities.InternalComplianceTransactions
                                                                  where row.InternalComplianceInstanceID == IcomplianceScheduleOnssentry.InternalComplianceInstanceID &&
                                                                  row.InternalComplianceScheduledOnID == IcomplianceScheduleOnssentry.ID && row.StatusId != 1
                                                                  select row).ToList();

                                    if (complianceTransactions.Count == 0)
                                    {
                                        DeleteComplianceScheduleOn(IcomplianceScheduleOnssentry.ID);
                                        scheduleid1 += IcomplianceScheduleOnssentry.ID.ToString() + " ,";
                                    }
                                });


                                if (IcomplianceScheduleOns.Count == 0)
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Record not found.";
                                }
                                else
                                {
                                    scheduleid1 = scheduleid1.Trim(',');
                                    scheduleid = scheduleid1;
                                    OldStartDate = IcomplianceInstancesentry.StartDate;
                                    CreateLogForScheduleOn(scheduleid, OldStartDate, NewscheduleOnDate);
                                    ChangeDateScheduleOn(IcomplianceInstancesentry.ID, NewscheduleOnDate);
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Record Updated Sucessfully.";
                                }
                            }
                            else
                            {
                                OldStartDate = IcomplianceInstancesentry.StartDate;
                                CreateLogForScheduleOn("0", OldStartDate, NewscheduleOnDate);
                                ChangeDateScheduleOn(IcomplianceInstancesentry.ID, NewscheduleOnDate);
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Record Updated Sucessfully.";
                            }
                        });
                    }
                    grdInternalCompliances.DataSource = null;
                    tbxStartDate.Text = "";
                    grdInternalCompliances.DataBind();
                    ddlCustomer.SelectedValue = "-1";
                    ddlInternalCompliance.SelectedValue = "-1";
                    tbxFilterLocation.Text = "< Select >";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void ChangeDateScheduleOn(long IcomplianceInstanceID, DateTime NewscheduleOnDate)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var IComplianceInstance = (from row in entities.InternalComplianceInstances
                                          where row.ID == IcomplianceInstanceID
                                          select row).FirstOrDefault();

                IComplianceInstance.StartDate = NewscheduleOnDate;
                entities.SaveChanges();
            }
        }
    }
}