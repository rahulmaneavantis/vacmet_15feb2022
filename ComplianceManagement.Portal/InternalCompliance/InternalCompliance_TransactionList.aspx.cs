﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using System.Data;
using OfficeOpenXml;
using System.Web;

namespace com.VirtuosoITech.ComplianceManagement.Portal.InternalCompliance
{
    public partial class InternalCompliance_TransactionList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                BindComplianceTransactions();

            }

            if (Convert.ToInt32(ViewState["RoleID"]) == 3)
            {
                udcStatusTranscatopn.OnSaved += (inputForm, args) => { BindComplianceTransactions(); };
            }
            else
            {
                udcReviewerStatusTransaction.OnSaved += (inputForm, args) => { BindComplianceTransactions(); };
            }
        }

        private void BindComplianceTransactions()
        {
            try
            {

                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lbtnExportExcel);
                //var transactionList = Business.ComplianceManagement.GetTransactionsByUserID(AuthenticationHelper.UserID == 1 ? -1 : AuthenticationHelper.UserID, tbxFilter.Text.Trim());
                var transactionList = Business.InternalComplianceManagement.GetTransactionsByUserID(AuthenticationHelper.UserID == 1 ? -1 : AuthenticationHelper.UserID, tbxFilter.Text.Trim());
                if (!string.IsNullOrEmpty(txtStartDate.Text) && !string.IsNullOrEmpty(txtEndDate.Text))
                {
                    transactionList = transactionList.Where(entry => (entry.InternalScheduledOn >= DateTime.ParseExact(txtStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture) && entry.InternalScheduledOn <= DateTime.ParseExact(txtEndDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture))).ToList();
                }
                else if (!string.IsNullOrEmpty(txtStartDate.Text))
                {
                    transactionList = transactionList.Where(entry => entry.InternalScheduledOn >= DateTime.ParseExact(txtStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture)).ToList();

                }
                else if (!string.IsNullOrEmpty(txtEndDate.Text))
                {
                    transactionList = transactionList.Where(entry => entry.InternalScheduledOn <= DateTime.ParseExact(txtEndDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture)).ToList();
                }

                //grdComplianceTransactions.DataSource = transactionList.Where(entry => (entry.RoleID != 6 && entry.ComplianceStatusID != 7 && entry.ComplianceStatusID != 9)).ToList();
                grdComplianceTransactions.DataSource = transactionList.Where(entry => (entry.RoleID != 6 && entry.InternalComplianceStatusID != 7 && entry.InternalComplianceStatusID != 9)).ToList();
                grdComplianceTransactions.DataBind();
                //upComplianceTransactionsList.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected bool IsFileAvailable(int fileID)
        {
            return fileID != -1;
        }

        protected bool CanChangeStatus(long userID, int roleID, int statusID)
        {
            try
            {
                bool result = false;

                if (userID == AuthenticationHelper.UserID)
                {
                    if (roleID == 3)
                    {
                        result = statusID == 1 || statusID == 6;
                    }
                    else if (roleID == 4)
                    {
                        result = statusID == 2 || statusID == 3;
                    }
                    else if (roleID == 5)
                    {
                        result = statusID == 2 || statusID == 3;
                    }
                    else if (roleID == 6)
                    {
                        result = statusID == 4 || statusID == 5 || statusID == 6;
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                //grdAct.PageIndex = 0;
                //BindActs();
                grdComplianceTransactions.PageIndex = 0;
                BindComplianceTransactions();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdComplianceTransactions_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {

                grdComplianceTransactions.PageIndex = e.NewPageIndex;
                BindComplianceTransactions();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdComplianceTransactions_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                if (e.CommandName.Equals("CHANGE_STATUS"))
                {

                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    int ScheduledOnID = Convert.ToInt32(commandArgs[0]);
                    int complianceInstanceID = Convert.ToInt32(commandArgs[1]);
                    ViewState["RoleID"] = commandArgs[2];
                    if (Convert.ToInt32(commandArgs[2]) == 3)
                    {

                        udcStatusTranscatopn.OpenTransactionPage(ScheduledOnID, complianceInstanceID);
                    }
                    else
                    {

                        udcReviewerStatusTransaction.OpenTransactionPage(ScheduledOnID, complianceInstanceID);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void grdComplianceTransactions_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {

                //var complianceTransactionList = Business.ComplianceManagement.GetTransactionsByUserID(AuthenticationHelper.UserID == 1 ? -1 : AuthenticationHelper.UserID, tbxFilter.Text.Trim());
                var complianceTransactionList = Business.InternalComplianceManagement.GetTransactionsByUserID(AuthenticationHelper.UserID == 1 ? -1 : AuthenticationHelper.UserID, tbxFilter.Text.Trim());
                complianceTransactionList = complianceTransactionList.Where(entry => entry.RoleID != 6).ToList();
                if (direction == SortDirection.Ascending)
                {
                    complianceTransactionList = complianceTransactionList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                }
                else
                {
                    complianceTransactionList = complianceTransactionList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                }

                foreach (DataControlField field in grdComplianceTransactions.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdComplianceTransactions.Columns.IndexOf(field);
                    }
                }

                grdComplianceTransactions.DataSource = complianceTransactionList;
                grdComplianceTransactions.DataBind();


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdComplianceTransactions_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        protected void grdComplianceTransactions_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton btnChangeStatus = (LinkButton)e.Row.FindControl("btnChangeStatus");
                if (btnChangeStatus.Visible == true)
                {
                    //e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(grdComplianceTransactions, "CHANGE_STATUS$" + btnChangeStatus.CommandArgument);
                    e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(btnChangeStatus, "");
                    e.Row.ToolTip = "Click on row to change Compliance Status";

                }

            }
        }

        protected void lbtnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    try
                    {
                        ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("InternalComplianceStatus");
                        DataTable ExcelData = null;
                        BindComplianceTransactions();
                        //DataView view = new System.Data.DataView((grdComplianceTransactions.DataSource as List<ComplianceInstanceTransactionView>).ToDataTable());
                        DataView view = new System.Data.DataView((grdComplianceTransactions.DataSource as List<InternalComplianceInstanceTransactionView>).ToDataTable());
                        //ExcelData = view.ToTable("Selected", false, "Branch", "ShortDescription", "Role", "ScheduledOn", "Status", "StatusChangedOn");
                        ExcelData = view.ToTable("Selected", false, "Branch", "ShortDescription", "Role", "InternalScheduledOn", "Status", "StatusChangedOn");
                        exWorkSheet.Cells["A4"].LoadFromDataTable(ExcelData, true);

                        exWorkSheet.Cells["B2"].Value = "Internal Compliance Status Report";
                        exWorkSheet.Cells["B2"].Style.Font.Bold = true;
                        exWorkSheet.Cells["B2"].Style.Font.Size = 15;


                        exWorkSheet.Cells["A4"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A4"].Style.Font.Size = 12;
                        exWorkSheet.Cells["B4"].Style.Font.Bold = true;
                        exWorkSheet.Cells["B4"].Value = "Short Description";
                        exWorkSheet.Cells["B4"].Style.Font.Size = 12;
                        exWorkSheet.Cells["C4"].Style.Font.Bold = true;
                        exWorkSheet.Cells["C4"].Style.Font.Size = 12;
                        exWorkSheet.Cells["D4"].Style.Font.Bold = true;
                        exWorkSheet.Cells["D4"].Style.Font.Size = 12;
                        exWorkSheet.Cells["D4"].Value = "Scheduled On";
                        exWorkSheet.Cells["E4"].Style.Font.Bold = true;
                        exWorkSheet.Cells["E4"].Style.Font.Size = 12;

                        exWorkSheet.Cells["F4"].Value = "Completed On";
                        exWorkSheet.Cells["F4"].Style.Font.Bold = true;
                        exWorkSheet.Cells["F4"].Style.Font.Size = 12;

                        //exWorkSheet.Cells["G4"].Value = "Start Date";
                        //exWorkSheet.Cells["G4"].Style.Font.Bold = true;
                        //exWorkSheet.Cells["G4"].Style.Font.Size = 12;

                        using (ExcelRange col = exWorkSheet.Cells[2, 1, 4 + ExcelData.Rows.Count, 6])
                        {
                            col.Style.Numberformat.Format = "dd/MM/yyyy";
                            //col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            col.AutoFitColumns();
                        }

                        Byte[] fileBytes = exportPackge.GetAsByteArray();
                        Response.ClearContent();
                        Response.Buffer = true;
                        Response.AddHeader("content-disposition", "attachment;filename=InternalComplianceStatusReport.xlsx");
                        Response.Charset = "";
                        Response.ContentType = "application/vnd.ms-excel";
                        //StringWriter sw = new StringWriter();
                        Response.BinaryWrite(fileBytes);
                        //Response.End();
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                    }
                    catch (Exception)
                    {
                    }
                    //Response.End();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void upComplianceTransactionsList_Load(object sender, EventArgs e)
        {
            try
            {

                DateTime date = DateTime.MinValue;
                if (DateTime.TryParseExact(txtStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date) || DateTime.TryParseExact(txtEndDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "StatusReportInitializeDatePicker", string.Format("StatusReportInitializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);

                }
                else
                {
                    ScriptManager.RegisterStartupScript(Page, typeof(Page), "StatusReportInitializeDatePicker", "StatusReportInitializeDatePicker(null);", true);

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void txtStartDate_TextChanged(object sender, EventArgs e)
        {
            try
            {
                BindComplianceTransactions();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void txtEndDate_TextChanged(object sender, EventArgs e)
        {
            try
            {
                BindComplianceTransactions();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

    }
}