﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="InternalCompMaster.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.InternalCompliance.InternalCompMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <link href="../NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }

        .custom-combobox-input {
            margin: 0;
            padding: 0.3em;
            width: 258px;
        }
          .td1 {
            width: 15%;
        }
    </style>
    <script type="text/javascript">

        function initializeConfirmDatePicker(date) {
            var startDate = new Date();
            $('#<%= tbxStartDate.ClientID %>').datepicker({
                dateFormat: 'mm-dd-yy',
                numberOfMonths: 1
            });
            $('#<%= tbxdocumentDate.ClientID %>').datepicker({
                dateFormat: 'mm-dd-yy',
                numberOfMonths: 1
            });
            
        }
        

        function Showalert() {
            alert('Compliance Updated Successfully.');
            window.location.reload();
            //Response.Redirect(Request.RawUrl);
        }
        function closepopup()
        {
            $('#divModifyDepartment').dialog('close');
            window.location.reload();
            //document.getElementById('<%= btnbindgrid.ClientID %>').click(); 
        }
        <%--
        function closepopup1()
        {
            alert(1);
            //document.getElementById('<%= btnbindgrid.ClientID %>').click(); 
            window.opener.location.href = window.opener.location.href
        }--%>
        $(function () {            
            initializeCombobox();
            //initializeConfirmDatePicker();
        });
          function initializeCombobox() {            
            //$("#<%= ddlFilterReviewer.ClientID %>").combobox();
        }
        $(function () {
            $('#divModifyDepartment').dialog({
                height: 400,
                width: 580,
                top: 100,
                autoOpen: false,
                draggable: true,
                title: "Update Compliance",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });
        });

        function initializeRadioButtonsList(controlID) {
            $(controlID).buttonset();
        }
    </script>
    <script type="text/javascript">
        function Text() {

        }
    </script>
    <script type="text/javascript">

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });

            //$('#BodyContent_tbxFilterLocation').keyup(function () {
            //  //  FnSearch();
            //});
        }

        function test(obj)
        {
            debugger;
            if (obj.checked) {
                document.getElementById('BodyContent_reviwer1').style.display = "revert";
                document.getElementById('BodyContent_reviwer2').style.display = "revert";
            }
            else {
                document.getElementById('BodyContent_reviwer1').style.display = "none";
                document.getElementById('BodyContent_reviwer2').style.display = "none";
            }
            //document.getElementById('<%= btnbindgrid.ClientID %>').click();            
        }
        function checkAll(cb) {

            var ctrls = document.getElementsByTagName('input');
            for (var i = 0; i < ctrls.length; i++) {
                var cbox = ctrls[i];
                if (cbox.type == "checkbox" && cbox.id.indexOf("chkIPData") > -1) {
                    cbox.checked = cb.checked;
                }
            }
        }
        function OnTreeClick(evt) {
          
            var src = window.event != window.undefined ? window.event.srcElement : evt.target;
            var isChkBoxClick = (src.tagName.toLowerCase() == "input" && src.type == "checkbox");
            if (isChkBoxClick) {
                var parentTable = GetParentByTagName("table", src);
                var nxtSibling = parentTable.nextSibling;
                if (nxtSibling && nxtSibling.nodeType == 1)//check if nxt sibling is not null & is an element node
                {
                    if (nxtSibling.tagName.toLowerCase() == "div") //if node has children
                    {
                        //check or uncheck children at all levels
                        CheckUncheckChildren(parentTable.nextSibling, src.checked);
                    }
                }
                //check or uncheck parents at all levels
                CheckUncheckParents(src, src.checked);
            }
        }
        function UncheckHeader() {

            var rowCheckBox = $("#CheckBox input[id*='chkIPData']");
            var rowCheckBoxSelected = $("#CheckBox input[id*='chkIPData']:checked");
            var rowCheckBoxHeader = $("#CheckBox input[id*='chkIPDataHeader']");
            if (rowCheckBox.length == rowCheckBoxSelected.length) {
                rowCheckBoxHeader[0].checked = true;
            } else {
                rowCheckBoxHeader[0].checked = false;
            }
        }
        function showProgress() {
            var updateProgress = $get("<%# updateProgress.ClientID %>");
            updateProgress.style.display = "block";
        }
        function CheckUncheckChildren(childContainer, check) {
            var childChkBoxes = childContainer.getElementsByTagName("input");
            var childChkBoxCount = childChkBoxes.length;
            for (var i = 0; i < childChkBoxCount; i++) {
                childChkBoxes[i].checked = check;
            }
        }

        function CheckUncheckParents(srcChild, check) {
            var parentDiv = GetParentByTagName("div", srcChild);
            var parentNodeTable = parentDiv.previousSibling;

            if (parentNodeTable) {
                var checkUncheckSwitch;

                if (check) //checkbox checked
                {
                    var isAllSiblingsChecked = AreAllSiblingsChecked(srcChild);
                    if (isAllSiblingsChecked)
                        checkUncheckSwitch = true;
                    else
                        return; //do not need to check parent if any(one or more) child not checked
                }
                else //checkbox unchecked
                {
                    checkUncheckSwitch = false;
                }

                var inpElemsInParentTable = parentNodeTable.getElementsByTagName("input");
                if (inpElemsInParentTable.length > 0) {
                    var parentNodeChkBox = inpElemsInParentTable[0];
                    parentNodeChkBox.checked = checkUncheckSwitch;
                    //do the same recursively
                    CheckUncheckParents(parentNodeChkBox, checkUncheckSwitch);
                }
            }
        }

        function AreAllSiblingsChecked(chkBox) {
            var parentDiv = GetParentByTagName("div", chkBox);
            var childCount = parentDiv.childNodes.length;
            for (var i = 0; i < childCount; i++) {
                if (parentDiv.childNodes[i].nodeType == 1) //check if the child node is an element node
                {
                    if (parentDiv.childNodes[i].tagName.toLowerCase() == "table") {
                        var prevChkBox = parentDiv.childNodes[i].getElementsByTagName("input")[0];
                        //if any of sibling nodes are not checked, return false
                        if (!prevChkBox.checked) {
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        //utility function to get the container of an element by tagname
        function GetParentByTagName(parentTagName, childElementObj) {
            var parent = childElementObj.parentNode;
            while (parent.tagName.toLowerCase() != parentTagName.toLowerCase()) {
                parent = parent.parentNode;
            }
            return parent;
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div style="float: right; margin-top: 10px; margin-right: 70px; margin-top: 5px;">
    </div>
    <asp:UpdatePanel ID="UpIpAddress" runat="server" UpdateMode="Conditional" OnLoad="upDepeList_Load">
        <ContentTemplate>
            <div style="margin-bottom: 4px">
                <asp:CustomValidator ID="cvDuplicateEntry" CssClass="vdsummary" runat="server" EnableClientScript="False" />
            </div>
            <table width="100%">                
                <tr>                    
                    <td align="left" style="width: 25%;">
                        <label style="font-size: 13px;">
                            Filter :</label>
                        <asp:TextBox runat="server" ID="tbxFilter" Width="250px" MaxLength="50" AutoPostBack="true"
                            OnTextChanged="tbxFilter_TextChanged" />
                    </td>
                </tr>
            </table>
            <asp:Panel ID="Panel2" Width="100%" Height="450px" ScrollBars="Vertical" runat="server">
                <asp:GridView runat="server" ID="grdCompliances" AutoGenerateColumns="false" GridLines="Vertical" 
                AllowSorting="true"
                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px"
                    CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="10" Width="100%" Font-Size="12px"
                    OnPageIndexChanging="grdCompliances_PageIndexChanging"
                    OnRowDataBound="grdCompliances_RowDataBound"
                    OnRowCommand="grdCompliances_RowCommand">
                    <Columns>
                        <asp:TemplateField HeaderText="ID" ItemStyle-Width="150" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lblID" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-HorizontalAlign="left" ItemStyle-Width="8%" Visible="false">
                            <HeaderTemplate>
                                <asp:CheckBox ID="chkIPDataHeader" Text="Select All" ItemStyle-HorizontalAlign="Left" runat="server" onclick="checkAll(this)" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkIPData" runat="server" Style="padding-left: 11px;" ItemStyle-HorizontalAlign="Left" onclick="UncheckHeader()" />
                            </ItemTemplate>
                        </asp:TemplateField>     
                        <asp:BoundField DataField="ComplianceID" HeaderText="Compliance ID" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="10%" />                                           
                        <asp:BoundField DataField="ShortDescription" HeaderText="Short Description" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="37%" />                        
                        <asp:BoundField DataField="RISK" HeaderText="RISK" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="10%" />                        
                         <asp:TemplateField HeaderText="Is Document Mandatory?" ItemStyle-HorizontalAlign="left" ItemStyle-Width="8%">                            
                            <ItemTemplate>
                                <asp:CheckBox ID="chkIsDocumentData" Enabled="false" runat="server" Style="padding-left: 11px;" ItemStyle-HorizontalAlign="Left" />
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Is Approver Mandatory?" Visible="false" ItemStyle-HorizontalAlign="left" ItemStyle-Width="8%">                            
                            <ItemTemplate>
                                <asp:TextBox ID="txtIsDocumentCompulsory" Text='<%# Eval("IsDocumentCompulsory") %>' runat="server" CssClass="form-control Esclare" style="width:55%;display:none;"></asp:TextBox>
                                <asp:TextBox ID="txtIsApproverCompulsory" Text='<%# Eval("IsApproverCompulsory") %>' runat="server" CssClass="form-control Esclare" style="width:55%;display:none;"></asp:TextBox>
                                <asp:CheckBox ID="chkIsApproverData" Enabled="false" runat="server" Style="padding-left: 11px;" ItemStyle-HorizontalAlign="Left" />
                            </ItemTemplate>
                        </asp:TemplateField>                        
                        <asp:TemplateField HeaderText="Action" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="5%">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton1" runat="server" CommandName="EDIT_Detail"
                                    CommandArgument='<%# Eval("ComplianceID") %>'><img src="../../Images/edit_icon_new.png" alt="Save Details" title="Save Details" /></asp:LinkButton>                                
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle BackColor="#CCCC99" />
                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                    <PagerSettings Position="Top" />
                    <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                    <AlternatingRowStyle BackColor="#E6EFF7" />
                    <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                    <EmptyDataTemplate>
                        No Records Found.
                    </EmptyDataTemplate>
                </asp:GridView>

                <div style="float: left;">
                   
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div id="divModifyDepartment">
        <asp:UpdatePanel ID="upModifyDepartment" runat="server" UpdateMode="Conditional" OnLoad="upModifyAssignment_Load">
            <ContentTemplate>
                <div style="margin: 5px">
                    <table style="width: 100%;">
                        <tr>
                            <td colspan="4">
                                <div style="margin-bottom: 4px">
                                    <asp:ValidationSummary  ID="vsLicenseListPage" runat="server"  Style="padding-left: 5%"  Display="none" class="alert alert-block alert-danger fade in"
                                        ValidationGroup="ModifyIPValidationGroup" />
                                    <asp:CustomValidator ID="CustomModify" runat="server" Style="padding-left: 5%" EnableClientScript="False"
                                        ValidationGroup="ModifyIPValidationGroup" Display="None" class="alert alert-block alert-danger fade in" />
           
                                </div>
                            </td>
                        </tr>
                        <tr>
                        <td style="width: 53%;padding-top: 10px;">
                                <label style="width: 82px; display: block; float: left; font-size: 13px; color: red; text-align: right; margin-bottom: 15px; margin-left: -15px">*</label>
                                <label style="width: 243px; display: block;margin-left: -64px; font-size: 13px; color: #333; text-align: right; margin-bottom: 15px">
                                    Select Location:</label>
                            </td>
                         <td>
                            <asp:TextBox runat="server" ID="tbxFilterLocation" Style="padding: 0px; margin: 0px; height: 22px; width: 260px;"
                                CssClass="txtbox" />
                            <div style="margin-left: 0px; position: absolute; z-index: 10" id="divFilterLocation">
                                <asp:TreeView runat="server" ID="tvFilterLocation" BackColor="White" BorderColor="Black"
                                    BorderWidth="1" SelectedNodeStyle-Font-Bold="true" Height="200px" Width="261px"
                                    Style="overflow: auto" ShowLines="true" ShowCheckBoxes="All" onclick="OnTreeClick(event)" > <%--OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged"--%>
                                </asp:TreeView>
                                <div id="bindelement" style="background: white;height: 292px;display:none; width: 390px;border: 1px solid;overflow: auto;"></div>
    
                                <asp:Button ID="btnlocation" runat="server"  OnClick="btnlocation_Click" Text="Select"/> 
                                    <asp:Button ID="btnClear1" Visible="true" runat="server" OnClick="btnClear1_Click" Text="Clear"  /> 

                            </div>
                        </td>
                        </tr>
                        <tr>
                           <td style="width: 53%;padding-top: 10px;">
                                <label style="width: 82px; display: block; float: left; font-size: 13px; color: red; text-align: right; margin-bottom: 15px; margin-left: -15px">*</label>
                                <label style="width: 243px; display: block; font-size: 13px; color: #333; text-align: right; margin-bottom: 15px">
                                    Is Document Mandatory :</label>
                            </td>
                            <td>
                                <asp:CheckBox ID="chkIsDocument" runat="server" Style="padding-left: 11px;" ItemStyle-HorizontalAlign="Left" />
                                <asp:TextBox runat="server" ID="txtComplinceID" visible="false" CssClass="form-control" />
                            </td>
                        </tr>
                          <tr>
                         <td id="DocDate1" runat="server"  style="width: 53%;padding-top: 10px;">
                                <label style="width: 82px; display: block; float: left; font-size: 13px; color: red; text-align: right; margin-bottom: 15px; margin-left: -15px">*</label>
                                <label style="width: 243px;margin-left: -52px; display: block; font-size: 13px; color: #333; text-align: right; margin-bottom: 15px">
                                     Select Date :</label>
                            </td>
                      
                        <td id="DocDate2" runat="server" >
                            <asp:TextBox runat="server" ID="tbxdocumentDate" Style="height: 20px; width: 153px;" />
                        </td>
                        </tr>
                        <tr style="display:none;">
                           <td style="width: 53%;padding-top: 10px;">
                                <label style="width: 82px; display: block; float: left; font-size: 13px; color: red; text-align: right; margin-bottom: 15px; margin-left: -15px">*</label>
                                <label style="width: 243px; display: block; font-size: 13px; color: #333; text-align: right; margin-bottom: 15px">
                                    Is Approver Mandatory :</label>
                            </td>
                            <td>
                                <asp:CheckBox ID="chkIsApproverData" onclick="javascript:test(this);" runat="server" Style="padding-left: 11px;" ItemStyle-HorizontalAlign="Left" />                                                                
                            </td>
                        </tr>
                          <tr style="display:none;">
                            <td id="reviwer1" runat="server" style="width: 53%;padding-top: 10px;">
                                    <label style="width: 82px; display: block; float: left; font-size: 13px; color: red; text-align: right; margin-bottom: 15px; margin-left: -15px">*</label>
                                    <label style="width: 243px;margin-left: -52px; display: block; font-size: 13px; color: #333; text-align: right; margin-bottom: 15px">
                                         Select Reviewer :</label>
                                </td>
                            <td id="reviwer2" runat="server">
                            <asp:DropDownList runat="server" ID="ddlFilterReviewer" Style="padding: 0px; margin: 0px; min-height: 22px; min-width: 50px;">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr style="display:none;">
                         <td style="width: 53%;padding-top: 10px;">
                                <label style="width: 82px; display: block; float: left; font-size: 13px; color: red; text-align: right; margin-bottom: 15px; margin-left: -15px">*</label>
                                <label style="width: 243px;margin-left: -81px; display: block; font-size: 13px; color: #333; text-align: right; margin-bottom: 15px">
                                     Select Date :</label>
                            </td>
                      
                        <td>
                            <asp:TextBox runat="server" ID="tbxStartDate" Style="height: 20px; width: 153px;" />
                        </td>
                        </tr>
                    </table>
                </div>
                <div>
                   <asp:GridView runat="server" ID="grdrev" AutoGenerateColumns="false" GridLines="Vertical" 
                AllowSorting="true"
                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px"
                    OnPageIndexChanging="grdrev_PageIndexChanging"
                    CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="25" Width="100%" Font-Size="12px">
                    <Columns>
                        <asp:TemplateField HeaderText="Status Changed On" HeaderStyle-Width="20%" ItemStyle-Width="20%" ItemStyle-CssClass="text-center">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("DateChanged") != DBNull.Value ? Convert.ToDateTime(Eval("DateChanged")).ToString("dd-MM-yyyy") : "" %>'
                                        ToolTip='<%# Eval("DateChanged") != DBNull.Value ? Convert.ToDateTime(Eval("DateChanged")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Status" HeaderStyle-Width="20%" ItemStyle-Width="20%" ItemStyle-CssClass="text-left">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 70%">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Status") %>'
                                        ToolTip='<%# Eval("Status") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>   
                         <asp:TemplateField HeaderText="Branch" HeaderStyle-Width="20%" ItemStyle-Width="20%" ItemStyle-CssClass="text-left">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("BranchName") %>'
                                        ToolTip='<%# Eval("BranchName") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>                  
                    </Columns>
                    <FooterStyle BackColor="#CCCC99" />
                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                    <PagerSettings Position="Top" />
                    <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                    <AlternatingRowStyle BackColor="#E6EFF7" />
                    <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                    <EmptyDataTemplate>
                        No Records Found.
                    </EmptyDataTemplate>
                </asp:GridView>

                <div style="margin-bottom: 7px; margin-top: 10px; margin-left: 36%;">
                    <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click"
                        CssClass="button" ValidationGroup="ModifyIPValidationGroup" />
                          <asp:Button Text="btnbindgrid" runat="server" style="display:none;" ID="btnbindgrid" OnClick="btnbindgrid_Click"
                        CssClass="button" />
                    <asp:Button Text="Close"  runat="server" ID="btnClose" Style="margin-left: 10px" CssClass="button" OnClientClick="closepopup();" />
                    <%--OnClientClick="$('#divModifyDepartment').dialog('close');"--%>
                </div>
                </div>
            </ContentTemplate>
            <Triggers>
              <%--  <asp:PostBackTrigger ControlID="lbtnExportExcel" />
                <asp:PostBackTrigger ControlID="btnUploadFile" />--%>
            </Triggers>
        </asp:UpdatePanel>
    </div>

</asp:Content>


