﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.License;

namespace com.VirtuosoITech.ComplianceManagement.Portal.InternalCompliance
{
    public partial class IComplianceTypeList : System.Web.UI.Page
    {
        public static int UserID;
        public static int CustomerID = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            ddlcustomernew.Enabled = true;
            chkIsLicense.Enabled = true;
            if (!IsPostBack)
            {
                UserID = Convert.ToInt32(AuthenticationHelper.UserID);  
                BindCustomers(UserID);
                BindCustomerNew(UserID);
                if(AuthenticationHelper.Role=="IMPT")
                {
                    CustomerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                    customerdiv.Visible = true;
                    divnewcustomer.Visible = true;
                    lblcustomer.Visible = true;
                }
                else
                {
                    lblcustomer.Visible = false;
                    customerdiv.Visible = false;
                    divnewcustomer.Visible = false;
                    ViewState["CustomerID"] = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID;
                }  
                BindComplianceTypes(); 
            }
        }
        protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindComplianceTypes();
        }

        private void BindCustomers(int UserID)
        {
            try
            {
                ddlCustomer.DataTextField = "Name";
                ddlCustomer.DataValueField = "ID";

                ddlCustomer.DataSource = Assigncustomer.GetAllCustomer(UserID);
                ddlCustomer.DataBind();
                ddlCustomer.Items.Insert(0, new ListItem("Select Customer", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindCustomerNew(int UserID)
        {
            try
            {
                ddlcustomernew.DataTextField = "Name";
                ddlcustomernew.DataValueField = "ID";

                ddlcustomernew.DataSource = Assigncustomer.GetAllCustomer(UserID);
                ddlcustomernew.DataBind();
                ddlcustomernew.Items.Insert(0, new ListItem("Select Customer", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindComplianceTypes()
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role=="IMPT")
                { 
                    customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                    grdIComplianceType.DataSource = ComplianceTypeManagement.GetAllInternalCompliances(customerID, tbxFilter.Text);
                    grdIComplianceType.DataBind();
                }
                if (ViewState["CustomerID"] != null)
                {
                    grdIComplianceType.DataSource = ComplianceTypeManagement.GetAllInternalCompliances(Convert.ToInt32(ViewState["CustomerID"]), tbxFilter.Text);
                    grdIComplianceType.DataBind();
                }
                upIComplianceTypeList.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdIComplianceType_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                //int typeID = Convert.ToInt32(e.CommandArgument);  
                int typeID = 0;
                string typeName = "";
                if (!string.IsNullOrEmpty(commandArgs[0]))
                {
                    typeID = Convert.ToInt32(commandArgs[0]);
                }
                if (!string.IsNullOrEmpty(commandArgs[1]))
                {
                    typeName = Convert.ToString(commandArgs[1]);
                }
                ViewState["ComplianceTypeID"] = typeID;
                var userParameter = ComplianceTypeManagement.GetByIDInternalCompliance(typeID);
                if (userParameter != null)
                {
                    var GetLicenseComplianceType = ComplianceTypeManagement.GetByIDInternalComplianceTypeName(typeName);
                    if (GetLicenseComplianceType != null)
                    {
                        ViewState["LicenseComplianceTypeID"] = GetLicenseComplianceType.ID;
                    }
                }
                    if (e.CommandName.Equals("EDIT_COMPLIANCE_TYPE"))
                     {    
                    ViewState["Mode"] = 1;
                    ViewState["ComplianceTypeID"] = typeID;
                   // var userParameter = ComplianceTypeManagement.GetByIDInternalCompliance(typeID);
                    if (userParameter != null)
                    {                  
                        if (userParameter.Is_License == true)
                        {
                            chkIsLicense.Checked = true;
                        }
                        else
                        {
                            chkIsLicense.Checked = false;
                        }
                    }     
                    bool chkbox = ComplianceTypeManagement.InternalLicenseTypeUsed(Convert.ToInt32(ViewState["LicenseComplianceTypeID"]));
                    if (chkbox == false)
                    {
                        chkIsLicense.Enabled = true;
                    }
                    else
                    {
                        if (chkIsLicense.Checked == false)
                        {
                            chkIsLicense.Enabled = true;
                        }
                        else
                        {
                            chkIsLicense.Enabled = false;
                        }
                    }
                    if (AuthenticationHelper.Role == "IMPT")
                    {
                        ddlcustomernew.SelectedValue = Convert.ToString(userParameter.CustomerID);
                        ddlcustomernew.Enabled = false;

                    }
                    else
                    {
                        ViewState["CustomerID"] = userParameter.CustomerID;
                    }
                    tbxName.Text = userParameter.Name;
                    tbxDescription.Text = userParameter.Description;
                    tbxDescription.ToolTip = userParameter.Description;  
                    upIComplianceType.Update();
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divIComplianceTypeDialog\").dialog('open')", true);

                }
                else if (e.CommandName.Equals("DELETE_COMPLIANCE_TYPE"))
                {      
                    bool chkexistsComplianceType = ComplianceTypeManagement.InternalComplianceTypeUsed(typeID);
                    if (chkexistsComplianceType == false)
                    {
                        ComplianceTypeManagement.DeleteInternalCompliance(typeID, Convert.ToInt32(ViewState["LicenseComplianceTypeID"]),Convert.ToInt32(userParameter.CustomerID));
                        BindComplianceTypes();
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "alert('Compliance type is associated with one or more compliances, can not be deleted.')", true);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdIComplianceType_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdIComplianceType.PageIndex = e.NewPageIndex;
                BindComplianceTypes();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnAddIComplianceType_Click(object sender, EventArgs e)
        {
            try
            {
                int customerid = -1;
                if (AuthenticationHelper.Role == "IMPT")
                {
                    customerid = Convert.ToInt32(ddlCustomer.SelectedValue);
                }
                else
                {

                }

                chkIsLicense.Checked = false;
                ViewState["Mode"] = 0;   
                ViewState["CustomerID"] = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID;
                ddlcustomernew.SelectedValue = "-1";
                tbxName.Text = string.Empty;
                tbxDescription.Text = string.Empty;

                upIComplianceType.Update();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divIComplianceTypeDialog\").dialog('open')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                grdIComplianceType.PageIndex = 0;
                BindComplianceTypes();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "IMPT")
                {
                    customerID = Convert.ToInt32(ddlcustomernew.SelectedValue);
                }
                else
                {
                    customerID = Convert.ToInt32(ViewState["CustomerID"]);
                }
                if (customerID == -1)
                {
                    cvDuplicateEntry.ErrorMessage = "Please Select Customer";
                    cvDuplicateEntry.IsValid = false;
                    return;
                }
                InternalComplianceType type = new InternalComplianceType()
                {
                    Name = tbxName.Text.Trim(),
                    Description = tbxDescription.Text,
                    CustomerID = customerID,
                    IsDeleted = false
                };
                if (chkIsLicense.Checked == true)
                {
                    type.Is_License = true;
                }
                else
                {
                    type.Is_License = false;
                }
                if ((int)ViewState["Mode"] == 1)
                {
                    type.ID = Convert.ToInt32(ViewState["ComplianceTypeID"]);
                }    
                if (ComplianceTypeManagement.ExistsInternalCompliances(type, customerID))
                {
                    cvDuplicateEntry.ErrorMessage = "Type name already exists.";
                    cvDuplicateEntry.IsValid = false;
                    return;
                }
              
                if ((int)ViewState["Mode"] == 0)
                {
                    ComplianceTypeManagement.Create(type);
                }
                else if ((int)ViewState["Mode"] == 1)
                {
                    ComplianceTypeManagement.Update(type);
                }

                if (chkIsLicense.Checked == true)
                {
                    var Typedetails = ComplianceTypeManagement.TypeIDByTypeName(type.Name, customerID);
                    Lic_tbl_InternalLicenseType_Master ltlm = new Lic_tbl_InternalLicenseType_Master()
                    {
                        Name = tbxName.Text.Trim(),
                        IsDeleted = false,
                        CreatedBy = AuthenticationHelper.UserID,
                        CreatedOn = DateTime.Now,
                        Is_Statutory_NoStatutory = "I",
                        CustomerID = customerID,
                    };
                    if ((int)ViewState["Mode"] == 1)
                    {
                        ltlm.ID = Convert.ToInt32(ViewState["LicenseComplianceTypeID"]);
                    }
                    if (!LicenseTypeMasterManagement.Exists(ltlm))
                    {
                        LicenseTypeMasterManagement.CreateInternal(ltlm); 
                    }
                    else
                    {
                        LicenseTypeMasterManagement.Update(ltlm);
                    }  
                }       
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divIComplianceTypeDialog\").dialog('close')", true);
                BindComplianceTypes();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdIComplianceType_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {

                var complianceType = ComplianceTypeManagement.GetAllInternalCompliances(Convert.ToInt32(ViewState["CustomerID"]), tbxFilter.Text);
                if (direction == SortDirection.Ascending)
                {
                    complianceType = complianceType.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                }
                else
                {
                    complianceType = complianceType.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                }

                foreach (DataControlField field in grdIComplianceType.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdIComplianceType.Columns.IndexOf(field);
                    }
                }

                grdIComplianceType.DataSource = complianceType;
                grdIComplianceType.DataBind();


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void grdIComplianceType_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }
        
        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }


    }
}