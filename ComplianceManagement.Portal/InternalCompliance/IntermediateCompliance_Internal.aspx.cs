﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Logger;
using System.Reflection;
using System.Collections;
using OfficeOpenXml;
using System.Data;
using OfficeOpenXml.Style;
using System.IO;
using System.Drawing;
using System.Text.RegularExpressions;
namespace com.VirtuosoITech.ComplianceManagement.Portal.InternalCompliance
{
    public partial class IntermediateCompliance_Internal : System.Web.UI.Page
    {
        public static List<long> locationList = new List<long>();
        public static int userID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (AuthenticationHelper.Role == "IMPT")
                {
                    divcustomer.Visible = true;
                    lblcustomer.Visible = true;
                    userID = Convert.ToInt32(AuthenticationHelper.UserID);
                    Bindcustomer(userID);
                }
                else
                {
                    divcustomer.Visible = false;
                    lblcustomer.Visible = false;
                    BindLocationFilter();
                    tbxFilterLocation.Text = "< Select >";
                }
               
            }
        }
        protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindLocationFilter();
        }
        public void Bindcustomer(int UserID)
        {
            try
            {
                ddlCustomer.DataTextField = "Name";
                ddlCustomer.DataValueField = "ID";

                //ddlCustomer.DataSource = GetCustomer();
                ddlCustomer.DataSource =Assigncustomer.GetAllCustomer(UserID);
                ddlCustomer.DataBind();

                ddlCustomer.Items.Insert(0, new ListItem("< Select Customer >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
        }

        private void BindUsers(DropDownList ddlUserList, List<long> ids = null)
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }
               else if (AuthenticationHelper.Role == "IMPT")
                {
                    customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                }
                else
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }

                ddlUserList.DataTextField = "Name";
                ddlUserList.DataValueField = "ID";
                ddlUserList.Items.Clear();

                var users = UserManagement.GetAllNVP(customerID, ids: ids, Flags: false);

                ddlUserList.DataSource = users;
                ddlUserList.DataBind();

                ddlUserList.Items.Insert(0, new ListItem("< Select >", "-1"));
            }

            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindLocationFilter()
        {
            try
            {

                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }
                else if (AuthenticationHelper.Role == "IMPT")
                {
                    customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                }
                else
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                tvFilterLocation.Nodes.Clear();
                tbxFilterLocation.Text = string.Empty;
                var bracnhes = CustomerBranchManagement.GetAllHierarchy(customerID);

                foreach (var item in bracnhes)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    BindBranchesHierarchy(node, item);
                    tvFilterLocation.Nodes.Add(node);
                }

                tvFilterLocation.CollapseAll();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp)
        {
            try
            {
                foreach (var item in nvp.Children)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    BindBranchesHierarchy(node, item);
                    parent.ChildNodes.Add(node);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {

                tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
                int nCustomerBranchID = -1;
                nCustomerBranchID = Convert.ToInt32(tvFilterLocation.SelectedValue);

                BindGrid(nCustomerBranchID);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void upComplianceTypeList_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        
        private void BindGrid(int CustomerBranchID)
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }
                else if (AuthenticationHelper.Role == "IMPT")
                {
                    customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                }
                else
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }

                locationList.Clear();
                for (int i = 0; i < this.tvFilterLocation.Nodes.Count; i++)
                {
                    RetrieveNodes(this.tvFilterLocation.Nodes[i]);
                }
                List<SP_TempAssignmentInternal_Result> dataSource = new List<SP_TempAssignmentInternal_Result>();
                if (locationList.Count > 0)
                {
                    dataSource = Business.ComplianceManagement.GetTempAssignedInternalDetails(customerID, locationList, "");
                }
                else
                {
                    dataSource = Business.ComplianceManagement.GetTempAssignedInternalDetails(customerID, CustomerBranchID, "");
                }

                grdComplianceRoleMatrix.Visible = true;
                grdComplianceRoleMatrix.DataSource = dataSource;
                grdComplianceRoleMatrix.DataBind();


                ////var dataSource = Business.ComplianceManagement.GetTempAssignedInternalDetails(CustomerBranchID);
                //int customerID = -1;
                //if (AuthenticationHelper.Role == "CADMN")
                //{
                //    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                //}
                //var dataSource = Business.ComplianceManagement.GetTempAssignedInternalDetails(customerID, CustomerBranchID);


                //grdComplianceRoleMatrix.Visible = true;
                //grdComplianceRoleMatrix.DataSource = dataSource;
                //grdComplianceRoleMatrix.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnClear1_Click(object sender, EventArgs e)
        {

            for (int i = 0; i < this.tvFilterLocation.Nodes.Count; i++)
            {
                ChkBoxClear(this.tvFilterLocation.Nodes[i]);
            }
        }

        protected void ChkBoxClear(TreeNode node)
        {

            if (node.Checked) // && node.ChildNodes.Count == 0 if (node.Checked)
            {
                node.Checked = false;
            }
            foreach (TreeNode tn in node.ChildNodes)
            {
                if (tn.Checked)//&& tn.ChildNodes.Count == 0)//  && tn.ChildNodes.Count == 0if (tn.Checked)              
                {
                    tn.Checked = false;
                }

                if (tn.ChildNodes.Count != 0)
                {
                    for (int i = 0; i < tn.ChildNodes.Count; i++)
                    {
                        ChkBoxClear(tn.ChildNodes[i]);
                    }
                }
            }
        }

        protected void RetrieveNodes(TreeNode node)
        {
            try
            {
                int customerID = -1;
                  if (AuthenticationHelper.Role == "IMPT")
                {
                    customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                }
                else
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                if (node.Checked)
                {
                    if (Convert.ToInt32(node.Value) != customerID)
                    {
                        if (!locationList.Contains(Convert.ToInt32(node.Value)))
                        {
                            locationList.Add(Convert.ToInt32(node.Value));
                        }
                    }
                    if (node.ChildNodes.Count != 0)
                    {
                        for (int i = 0; i < node.ChildNodes.Count; i++)
                        {
                            RetrieveNodes(node.ChildNodes[i]);
                        }
                    }
                }
                else
                {
                    foreach (TreeNode tn in node.ChildNodes)
                    {
                        if (tn.Checked)
                        {
                            if (Convert.ToInt32(tn.Value) != customerID)
                            {
                                if (!locationList.Contains(Convert.ToInt32(tn.Value)))
                                {
                                    locationList.Add(Convert.ToInt32(tn.Value));
                                }
                            }
                        }
                        if (tn.ChildNodes.Count != 0)
                        {
                            for (int i = 0; i < tn.ChildNodes.Count; i++)
                            {
                                RetrieveNodes(tn.ChildNodes[i]);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        protected void grdComplianceRoleMatrix_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                int nCustomerBranchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                BindGrid(nCustomerBranchID);

                grdComplianceRoleMatrix.PageIndex = e.NewPageIndex;
                grdComplianceRoleMatrix.DataBind();
            
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdComplianceRoleMatrix__RowDataBound(object sender, GridViewRowEventArgs e)
        {
            DataRowView drv = e.Row.DataItem as DataRowView;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if ((e.Row.RowState & DataControlRowState.Edit) > 0)
                {
                    DropDownList dp = (DropDownList)e.Row.FindControl("ddlUserList");
                    BindUsers(dp);
                    dp.SelectedValue = grdComplianceRoleMatrix.DataKeys[e.Row.RowIndex].Values[1].ToString();
                }
            }
        }

        protected void grdComplianceRoleMatrix_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
            //    int nCustomerBranchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
            //    var ComplianceRoleMatrixList = Business.ComplianceManagement.GetTempAssignedDetails(nCustomerBranchID);
               
            //    if (direction == SortDirection.Ascending)
            //    {
            //        ComplianceRoleMatrixList = ComplianceRoleMatrixList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
            //        direction = SortDirection.Descending;
            //    }
            //    else
            //    {
            //        ComplianceRoleMatrixList = ComplianceRoleMatrixList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
            //        direction = SortDirection.Ascending;
            //    }


            //    foreach (DataControlField field in grdComplianceRoleMatrix.Columns)
            //    {
            //        if (field.SortExpression == e.SortExpression)
            //        {
            //            ViewState["MatrixSortIndex"] = grdComplianceRoleMatrix.Columns.IndexOf(field);
            //        }
            //    }


            //    grdComplianceRoleMatrix.DataSource = ComplianceRoleMatrixList;
            //    grdComplianceRoleMatrix.DataBind();
               
              
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdComplianceRoleMatrix_RowCreated(object sender, GridViewRowEventArgs e)
        {
            //if (e.Row.RowType == DataControlRowType.Header)
            //{
            //    int sortColumnIndex = Convert.ToInt32(ViewState["MatrixSortIndex"]);
            //    if (sortColumnIndex != -1)
            //    {
            //        AddMatrixSortImage(sortColumnIndex, e.Row);
            //    }
            //}
        }

        protected void AddMatrixSortImage(int columnIndex, GridViewRow headerRow)
        {
            //System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            //sortImage.ImageAlign = ImageAlign.AbsMiddle;

            //if (direction == SortDirection.Ascending)
            //{
            //    sortImage.ImageUrl = "../Images/SortAsc.gif";
            //    sortImage.AlternateText = "Ascending Order";
            //}
            //else
            //{
            //    sortImage.ImageUrl = "../Images/SortDesc.gif";
            //    sortImage.AlternateText = "Descending Order";
            //}
            //headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        protected void grdComplianceRoleMatrix_RowEditing(object sender, GridViewEditEventArgs e)
        {
            grdComplianceRoleMatrix.EditIndex = e.NewEditIndex;
            int  CustomerBranchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
            BindGrid(CustomerBranchID);
        }

        protected void grdComplianceRoleMatrix_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            grdComplianceRoleMatrix.EditIndex = -1;
            int CustomerBranchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
            BindGrid(CustomerBranchID);
        }

        protected void grdComplianceRoleMatrix_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            //int TempAssignmentID = Convert.ToInt32(grdComplianceRoleMatrix.Rows[e.RowIndex].FindControl("ID"));

            int TempAssignmentID = Convert.ToInt32(grdComplianceRoleMatrix.DataKeys[e.RowIndex].Values[0].ToString());

            DropDownList ddl = (DropDownList)grdComplianceRoleMatrix.Rows[e.RowIndex].FindControl("ddlUserList");
            Business.ComplianceManagement.UpdateAssignedInternalUser(TempAssignmentID,Convert.ToInt32(ddl.SelectedValue));

            grdComplianceRoleMatrix.EditIndex = -1;
            int CustomerBranchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
            BindGrid(CustomerBranchID);
            
        }

        protected void grdComplianceRoleMatrix_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                int TempAssignmentID = Convert.ToInt32(grdComplianceRoleMatrix.DataKeys[e.RowIndex].Values[0].ToString());

                Business.ComplianceManagement.DeleteTempAssignmentInternalTable(TempAssignmentID);

                grdComplianceRoleMatrix.EditIndex = -1;
                int CustomerBranchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                BindGrid(CustomerBranchID);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

    }
}