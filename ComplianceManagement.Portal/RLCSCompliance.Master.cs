﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.ProductMapping;

namespace com.VirtuosoITech.ComplianceManagement.Portal
{
    public partial class RLCSCompliance : System.Web.UI.MasterPage
    {
        protected string LastLoginDate;
        protected string CustomerName;
        protected string user_Roles;
        protected List<Int32> roles;
        protected List<Int32> TaskRoles;
        protected static int customerid;
        protected static int userid;
        protected string Approveruser_Roles;
        protected int checkTaskapplicable = 0;
        protected bool vendorAuditApplicable = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (HttpContext.Current.Request.IsAuthenticated)
                {

                    if (!IsPostBack)
                    {
                        hdnProfileID.Value = AuthenticationHelper.ProfileID;
                        hdnAuthKey.Value = AuthenticationHelper.AuthKey;

                        customerid = Convert.ToInt32(AuthenticationHelper.CustomerID); //UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                        userid = AuthenticationHelper.UserID;

                        user_Roles = AuthenticationHelper.Role;

                        Page.Header.DataBind();

                        if (Session["LastLoginTime"] != null)
                        {
                            LastLoginDate = Session["LastLoginTime"].ToString();
                        }

                        if (Session["vendorAuditApplicable"] != null)
                        {
                            vendorAuditApplicable = Convert.ToBoolean(Session["vendorAuditApplicable"]);
                        }
                        else
                        {
                            vendorAuditApplicable = RLCSManagement.CheckScopeApplicability(customerid, "SOW10");
                            Session["vendorAuditApplicable"] = vendorAuditApplicable;
                        }

                        if (!AuthenticationHelper.Role.Equals("SADMN") && !AuthenticationHelper.Role.Equals("IMPT"))
                        {
                            if (AuthenticationHelper.UserID != -1)
                            {
                                var cname = CustomerManagement.CustomerGetByIDName(Convert.ToInt32(customerid));
                                if (cname != null)
                                {
                                    CustomerName = cname;
                                }
                            }
                        }
                                                
                        User LoggedUser = UserManagement.GetByID(AuthenticationHelper.UserID);

                        if (LoggedUser.ImagePath != null)
                        {
                            ProfilePicTop.Src = LoggedUser.ImagePath;
                        }
                        else
                        {
                            ProfilePicTop.Src = "~/UserPhotos/DefaultImage.png";
                        }

                        string pageName = this.ContentPlaceHolder1.Page.GetType().FullName;
                        bool showDDLCustomers = false;
                        if (LoggedUser.RoleID >= 14)
                        {
                            List<Customer> lstAssignedCustomers = new List<Customer>();

                            if (Session["RLCS_userID"] != null && Session["RLCS_ProfileID"] != null)
                            {
                                if (!string.IsNullOrEmpty(Convert.ToString(Session["RLCS_userID"]))
                                    && !string.IsNullOrEmpty(Convert.ToString(Session["RLCS_ProfileID"])))
                                {
                                    lstAssignedCustomers = RLCSManagement.Get_RLCSUserCustomerMapping(Convert.ToString(Session["RLCS_userID"]), Convert.ToString(Session["RLCS_ProfileID"]));
                                }
                            }
                            
                            if (lstAssignedCustomers != null)
                            {
                                if (lstAssignedCustomers.Count > 1)
                                {
                                    if (pageName == "ASP.rlcs_rlcs_hrmdashboardnew_aspx")
                                    {
                                        showDDLCustomers = true;
                                       
                                        BindCustomers(lstAssignedCustomers);
                                    }                                    
                                }                                
                            }
                        }

                        if (showDDLCustomers)
                        {
                            divCustomer.Visible = true;
                            ddlCustomers.Visible = true;
                        }
                        else
                        {
                            divCustomer.Visible = false;
                            ddlCustomers.Visible = false;
                        }

                        //ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'DivSearch');", txtSearch.ClientID), true);
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#DivSearch\").hide(\"blind\", null, 500, function () { });", true);
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(ddlCustomers.SelectedValue))
                        {
                            customerid = Convert.ToInt32(ddlCustomers.SelectedValue);
                        }
                        else
                        {
                            customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);
                        }

                        // 
                        ddlCustomers.SelectedValue = Convert.ToString(customerid);
                        userid = AuthenticationHelper.UserID;
                        user_Roles = AuthenticationHelper.Role;

                        Page.Header.DataBind();

                        if (Session["LastLoginTime"] != null)
                        {
                            LastLoginDate = Session["LastLoginTime"].ToString();
                        }

                        if (!AuthenticationHelper.Role.Equals("SADMN") && !AuthenticationHelper.Role.Equals("IMPT"))
                        {
                            if (AuthenticationHelper.UserID != -1)
                            {
                                var cname = CustomerManagement.CustomerGetByIDName(Convert.ToInt32(customerid));
                                if (cname != null)
                                {
                                    CustomerName = cname;
                                }
                            }
                        }

                        User LoggedUser = UserManagement.GetByID(AuthenticationHelper.UserID);

                        if (LoggedUser.ImagePath != null)
                        {
                            ProfilePicTop.Src = LoggedUser.ImagePath;
                        }
                        else
                        {
                            ProfilePicTop.Src = "~/UserPhotos/DefaultImage.png";
                        }
                    }
                }
                else
                {
                    //added by rahul on 12 June 2018 Url Sequrity
                    FormsAuthentication.SignOut();
                    Session.Abandon();
                    FormsAuthentication.RedirectToLoginPage();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindCustomers(List<Customer> lstAssignedCustomers)
        {
            try
            {
                ddlCustomers.DataTextField = "Name";
                ddlCustomers.DataValueField = "ID";

                ddlCustomers.DataSource = lstAssignedCustomers;
                ddlCustomers.DataBind();              

                ddlCustomers.ClearSelection(); //making sure the previous selection has been cleared
                ddlCustomers.Items.FindByValue(Convert.ToString(AuthenticationHelper.CustomerID)).Selected = true;
                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlCustomers_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlCustomers.SelectedValue != null)
                {
                    if (!string.IsNullOrEmpty(ddlCustomers.SelectedValue))
                    {
                        int customerID = Convert.ToInt32(ddlCustomers.SelectedValue);
                        user_Roles = AuthenticationHelper.Role;
                        if (customerID != 0)
                        {
                            ProductMappingStructure _obj = new ProductMappingStructure();
                            if (_obj.ReAuthenticate_User(customerID))
                                Response.Redirect("~/RLCS/RLCS_HRMDashboardNew.aspx", false);

                            //string[] userDetails = HttpContext.Current.User.Identity.Name.Split(';');

                            //if (userDetails.Length >= 9)
                            //{
                            //    FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11}",
                            //        userDetails[0], userDetails[1], userDetails[2], userDetails[3], userDetails[4],
                            //        customerID, userDetails[6], userDetails[7], userDetails[8], userDetails[9], userDetails[10], userDetails[11]), false);

                            //    Response.Redirect("~/RLCS/RLCS_HRMDashboardNew.aspx", false);                                
                            //    //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "pageReload", "reloadPage();", true);
                            //}
                        }
                    }
                }                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void upDivLocation_Load(object sender, EventArgs e)
        {
            try
            {
                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'DivSearch');", txtSearch.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#DivSearch\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}