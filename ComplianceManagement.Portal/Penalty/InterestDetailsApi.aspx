﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="InterestDetailsApi.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Penalty.InterestDetailsApi" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" style="background: white;">
<head runat="server">

    <link href="../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../Newjs/jszip.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.14.1/moment.min.js"></script>

    <title></title>

    <style type="text/css">
        .k-list > .k-state-focused.k-state-selected, .k-listview > .k-state-focused.k-state-selected, .k-state-focused.k-state-selected, td.k-state-focused.k-state-selected {
            -webkit-box-shadow: inset 0 0 3px 1px #14699f;
            box-shadow: inset 0 0 1px 1px #14699f;
        }

        .k-grid-header th.k-state-focused, .k-list > .k-state-focused, .k-listview > .k-state-focused, .k-state-focused, td.k-state-focused {
            -webkit-box-shadow: inset 0 0 3px 1px #d7dae0;
            box-shadow: inset 0 0 1px 1px white;
        }

        .div.k-grid-footer, div.k-grid-header {
            border-top-width: 1px;
            margin-right: 0px;
        }

        .k-grid-footer-wrap, .k-grid-header-wrap {
            position: relative;
            width: 100%;
            overflow: hidden;
            border-style: solid;
            border-width: 0 1px 0 0;
            zoom: 1;
        }

        .k-grid-content {
            min-height: 380px !important;
            overflow: hidden;
        }

        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 2.0em;
            border-bottom-width: 1px;
            background-color: white;
            border-width: 0 1px 1px 0px;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
            background-color: white;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: white;
        }

        .k-pager-wrap > .k-link > .k-icon {
            margin-top: -2px;
            color: inherit;
        }

        html .k-grid tr:hover {
            background: white;
        }

        html .k-grid tr.k-alt:hover {
            background: white;
        }

        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            background: white;
            border: none;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
            margin-right: 2px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        input[type=checkbox], input[type=radio] {
            margin: 4px 4px 0;
            line-height: normal;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: white;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 0px 1px 0px;
            text-align: center;
        }

        .k-grid-pager {
            margin-top: -1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-i-arrow-60-down, .k-i-arrow-60-left, .k-i-arrow-60-right, .k-i-arrow-60-up {
            cursor: pointer;
            margin-top: 6px;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            border-width: 0px 0px 1px 0px;
            background: #E9EAEA;
            font-weight: bold;
            margin-right: 18px;
            font-family: 'Roboto',sans-serif;
            font-size: 15px;
            color: rgba(0, 0, 0, 0.5);
            height: 20px;
            vertical-align: middle;
        }

        .k-dropdown-wrap.k-state-default {
            background-color: white;
        }

        .k-popup.k-calendar-container, .k-popup.k-list-container {
            background-color: white;
        }

        label.k-label {
            font-family: roboto,sans-serif !important;
            color: #515967;
            /* font-stretch: 100%; */
            font-style: normal;
            font-weight: 400;
            min-width: 362px;
            white-space: pre-wrap;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: flex;
            margin-bottom: 0px;
        }

        .k-state-default > .k-select {
            border-color: #ceced2;
            margin-top: -4px;
        }

        .k-grid-norecords {
            width: 100%;
            height: 100%;
            text-align: center;
            margin-top: 2%;
        }

        .k-grouping-header {
            font-style: italic;
            background-color: white;
        }

        .k-grid table {
            width: 100.5%;
        }

        .k-active-filter, .k-state-active, .k-state-active:hover {
            background-color: #E9EAEA;
            border-color: #a6a6ad;
            color: #535b6a;
        }

        .k-multiselect-wrap > .k-i-close {
            top: 8px;
            margin-right: 10px;
        }

        .k-multiselect-wrap .k-input {
            /*padding-top:6px;*/
            display: inherit !important;
        }
    </style>

    <script id="templateTooltip" type="text/x-kendo-template">
                <div>
                <div> #:value ? value : "N/A" #</div>
                </div>
    </script>

    <script type="text/x-kendo-template" id="template">      
               
    </script>

    <script type="text/javascript">

        function exportReport(e) {

            var ReportName = "Interest Report";
            var customerName = document.getElementById('CustName').value;
            var todayDate = moment().format('DD-MMM-YYYY');
            var grid = $("#grid").getKendoGrid();

            var rows = [

                {
                    cells: [
                        { value: "Entity/ Location:", bold: true },
                        { value: customerName }
                    ]
                },
                 {
                     cells: [
                         { value: "Report Name:", bold: true },
                         { value: ReportName }
                     ]
                 },
                 {
                     cells: [
                         { value: "Report Generated On:", bold: true },
                         { value: todayDate }
                     ]
                 },
                 {
                     cells: [
                         { value: "" }
                     ]
                 },
                {
                    cells: [
                        { value: "Sr.No.", bold: true },
                        { value: "Location", bold: true },
                        { value: "Compliance ID", bold: true },                       
                        { value: "Act Name", bold: true },
                        { value: "Short Description", bold: true },
                        { value: "Due Date", bold: true },
                        { value: "User Name", bold: true },
                        { value: "Interest", bold: true },
                      
                         //{ value: "Penalty/Additional Fee", bold: true },
                        { value: "Remarks", bold: true },
                    ]
                }
            ];

            var trs = grid.dataSource;
            var filteredDataSource = new kendo.data.DataSource({
                data: trs.data(),
                filter: trs.filter()
            });

            filteredDataSource.read();
            var data = filteredDataSource.view();
            for (var i = 0; i < data.length; i++) {
                var dataItem = data[i];
                rows.push({
                    cells: [ // dataItem."Whatever Your Attributes Are"
                        { value: i + 1 },
                        { value: dataItem.BranchName },
                        { value: dataItem.ComplianceID },
                        { value: dataItem.ActName },
                        { value: dataItem.ShortDescription },
                        { value: dataItem.ScheduledOn, format:"dd-MMM-yyyy" },
                        { value: dataItem.UserName },
                        { value: dataItem.Interest },
                        //{ value: dataItem.Penalty },
                        { value: dataItem.Remarks },
                    ]
                });

            }
            for (var i = 4; i < rows.length; i++) {
                for (var j = 0; j < 9; j++) {
                    rows[i].cells[j].borderBottom = "#000000";
                    rows[i].cells[j].borderLeft = "#000000";
                    rows[i].cells[j].borderRight = "#000000";
                    rows[i].cells[j].borderTop = "#000000";
                    rows[i].cells[j].hAlign = "left";
                    rows[i].cells[j].vAlign = "top";
                    rows[i].cells[j].wrap = true;
                }
            }
            excelExport(rows, ReportName);
            e.preventDefault();
            return false;
        }

        function excelExport(rows, ReportName) {

            var FileName = "Interest Report";
            var workbook = new kendo.ooxml.Workbook({
                sheets: [
                    {
                        columns: [
                            { autoWidth: true },
                            { width: 250 },
                            { width: 200 },
                            { width: 200 },
                            { width: 250 },
                            { width: 200 },
                            { width: 200 },
                            { width: 200 },
                            { width: 300 },
                            { width: 300 },
                            { width: 250 },
                            { width: 150 },
                        ],
                        title: FileName,
                        rows: rows
                    },
                ]
            });

            var nameOfPage = FileName;
            //var nameOfPage = "Test-1"; // insert here however you are getting name of screen
            kendo.saveAs({ dataURI: workbook.toDataURL(), fileName: nameOfPage + " .xlsx" });
            return false;
        }

        function fclosebtn(tbn) {
            $('#' + tbn).css('display', 'none');
            $('#' + tbn).html('');
        }
        var record = 0;
        var total = 0;
        function BindGrid() {
            var grid = $("#grid").kendoGrid({
                dataSource: {
                    transport: {
                        read: {
                            url: '<% =Path%>Data/MGMTDashboradPenaltyDetails?Userid=<% =UId%>&Customerid=<% =CustId%>&CBID=<% =branchid%>&attrbu=<% =attribute%>&Sdatev=<% =FromDate%>&Edatev=<% =Enddate%>&penaltystatus=' + $("#dropdownlistUserRole").val() + '&isapprover=<% =isapprover%>&IS=<% =IS%>&Isdept=<% =IsDeptHead%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                    },
                    schema: {
                        model: {
                            fields: {
                                ComplianceID: { type: "string", },
                                ScheduledOn: { type: "date" }
                            }
                        },
                        data: function (response) {
                            if (<% =ComplianceTypeID%> == 0) {
                                return response[0].penalty;
                            }
                            else if (<% =ComplianceTypeID%> == 1) {
                                return response[0].Deptpenalty;
                            }
                            else if (<% =ComplianceTypeID%> == 2) {
                                return response[0].penaltyapprover;
                            }
                        },
                        total: function (response) {
                            if (<% =ComplianceTypeID%> == 0) {
                                return response[0].penalty.length;
                            }
                            else if (<% =ComplianceTypeID%> == 1) {
                                return response[0].Deptpenalty.length;
                            }
                            else if (<% =ComplianceTypeID%> == 2) {
                                return response[0].penaltyapprover.length;
                            }
                        }
                    },
                    pageSize: 10
                },
                excel: {
                    allPages: true,
                },
                sortable: true,
                groupable: true,
                filterable: true,
                columnMenu: true,
                navigatable: true,
                pageable: {
                    numeric: true,
                    pageSizes: ['All', 5, 10, 20],
                    pageSize: 10,
                    buttonCount: 3,
                },
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                dataBinding: function () {
                   // window.parent.forchild($("body").height() + 70);
                },
                columns: [
                    {
                        field: "BranchName", title: 'Location',
                        width: "10%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    { hidden: true, field: "ComplianceID", title: "Compliance ID", filterable: { multi: true, search: true }, },

                    {
                        field: "ShortDescription", title: 'Compliance',
                        width: "20%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "ActName", title: 'Act',
                        width: "20%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },

                    {

                        field: "ScheduledOn", title: 'Due&nbsp;Date',
                        type: "date",
                        format: "{0:dd-MMM-yyyy}",
                        filterable: {
                            multi: true,
                            search: true,
                            extra: false,
                            operators: {
                                string: {
                                    type: "date",
                                    format: "{0:dd-MMM-yyyy}",
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },

                    {
                        field: "UserName", title: 'User',
                        width: "10%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "Interest", title: 'Interest',
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
         
                    {
                        field: "Remarks", title: 'Remark',
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    }
                ]
            });
        $('#grid').kendoTooltip({
            filter: "td[role=gridcell]",  //what element
            content: function (e) {
                var content = e.target.context.textContent;
                if (content != "") {
                    return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                }
                else
                    e.preventDefault();
            },
            beforeShow: function (e) {
                var target = e.target;

                var isTextBiggerThanElement = findOutSize(target);
                if (!isTextBiggerThanElement) {
                    e.preventDefault();
                }
            },
            position: 'down'
        }).data("kendoTooltip");

        $('#grid').kendoTooltip({
            filter: "th[role=columnheader]",  //what element
            content: function (e) {
                var content = e.target.context.textContent;
                if (content != "") {
                    return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                }
                else
                    e.preventDefault();
            },
            beforeShow: function (e) {
                var target = e.target;

                var isTextBiggerThanElement = findOutSize(target);
                if (!isTextBiggerThanElement) {
                    e.preventDefault();
                }
            },
            position: 'down'
        }).data("kendoTooltip");

        $('#grid').kendoTooltip({
            filter: "th[data-role=droptarget]",  //what element
            content: function (e) {
                var content = e.target.context.textContent;
                if (content != "") {
                    return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                }
                else
                    e.preventDefault();
            },
            beforeShow: function (e) {
                var target = e.target;

                var isTextBiggerThanElement = findOutSize(target);
                if (!isTextBiggerThanElement) {
                    e.preventDefault();
                }
            },
            position: 'down'
        }).data("kendoTooltip");

        $('#grid').kendoTooltip({
            filter: "td[colspan=9]",  //what element
            content: function (e) {
                var content = e.target.context.textContent;
                if (content != "") {
                    return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                }
                else
                    e.preventDefault();
            },
            position: 'down'
        }).data("kendoTooltip");

        $('#grid').kendoTooltip({
            filter: "td[colspan=10]",  //what element
            content: function (e) {
                var content = e.target.context.textContent;
                if (content != "") {
                    return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                }
                else
                    e.preventDefault();
            },
            position: 'down'
        }).data("kendoTooltip");

        $('#grid').kendoTooltip({
            filter: "td[colspan=11]",  //what element
            content: function (e) {
                var content = e.target.context.textContent;
                if (content != "") {
                    return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                }
                else
                    e.preventDefault();
            },
            position: 'down'
        }).data("kendoTooltip");

        $('#grid').kendoTooltip({
            filter: "td[colspan=12]",  //what element
            content: function (e) {
                var content = e.target.context.textContent;
                if (content != "") {
                    return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                }
                else
                    e.preventDefault();
            },
            position: 'down'
        }).data("kendoTooltip");

        $('#grid').kendoTooltip({
            filter: "td[colspan=13]",  //what element
            content: function (e) {
                var content = e.target.context.textContent;
                if (content != "") {
                    return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                }
                else
                    e.preventDefault();
            },
            position: 'down'
        }).data("kendoTooltip");

        $('#grid').kendoTooltip({
            filter: "td[colspan=14]",  //what element
            content: function (e) {
                var content = e.target.context.textContent;
                if (content != "") {
                    return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                }
                else
                    e.preventDefault();
            },
            position: 'down'
        }).data("kendoTooltip");

        $('#grid').kendoTooltip({
            filter: "td[colspan=15]",  //what element
            content: function (e) {
                var content = e.target.context.textContent;
                if (content != "") {
                    return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                }
                else
                    e.preventDefault();
            },
            position: 'down'
        }).data("kendoTooltip");

        $('#grid').kendoTooltip({
            filter: "td[colspan=16]",  //what element
            content: function (e) {
                var content = e.target.context.textContent;
                if (content != "") {
                    return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                }
                else
                    e.preventDefault();
            },
            position: 'down'
        }).data("kendoTooltip");
    }

    $(document).ready(function () {
        $("#txtSearchComplianceID").on('input', function (e) {
            FilterGrid();
        });
        $("#dropdownlistUserRole").kendoDropDownList({
            placeholder: "Role",
            checkboxes: true,
            checkAll: true,
            autoClose: true,
            checkAllTemplate: "Select All",
            autoWidth: true,
            dataTextField: "text",
            dataValueField: "value",
            change: function (e) {
                $("#dropdowntree").data("kendoDropDownTree").value([]);
                $("#grid").data("kendoGrid").dataSource.filter({});
                BindGrid();
            },
            index: 0,
            dataSource: 
                [
                { text: "Submitted", value: "0" },
                { text: "Pending", value: "1" }
                ]
        });

        $("#dropdownfunction").kendoDropDownTree({
            filter: "contains",
            placeholder: "Select Department",
            filter:"contains",
            checkboxes: true,
            checkAll: true,
            autoClose: false,
            autoWidth: true,
            checkAllTemplate: "Select All",
            dataTextField: "Name",
            dataValueField: "Id",
            change: function (e) {
                FilterGrid();
                fCreateStoryBoard('dropdownfunction', 'filterCategory', 'function')
            },
            dataSource: {
                severFiltering: true,
                transport: {
                    read: {
                        url: '<% =Path%>Data/BindStatutoryFunctionList?UserID=<% =UId%>&customerID=<% =CustId%>&Flag=<% =Flag%>',
                        dataType: "json",
                        beforeSend: function (request) {
                            request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                    }
                }
            }
        });

        var IsApprover = 0;
        if ('<%=isapprover%>' == 'False') {
                IsApprover = 0;
            }
            else {
                IsApprover = 1;
            } 
            var DeptHead = 0;
            if ('<% =IsDeptHead%>' == 'False') {
                DeptHead = 0;
            }
            else {
                DeptHead = 1;
            } 
            $("#dropdownACT").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: false,
                dataTextField: "Name",
                dataValueField: "ID",
                optionLabel: "Select Act",
                change: function (e) {
                    FilterGrid();
                    fCreateStoryBoard('dropdownACT', 'filterAct', 'act')
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/BindComplianceWiseActList?UId=<% =UId%>&CId=<% =CustId%>&Flag=MGMT',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                    }
                },
                dataBound: function (e) {
                    e.sender.list.width("790");
                }
            });

        $("#dropdowntree").kendoDropDownTree({
            placeholder: "Entity/Sub-Entity/Location",
            checkboxes: {
                checkChildren: true
            },
            //filter: "contains",
            checkAll: true,
            autoWidth: true,
            checkAllTemplate: "Select All",
            dataTextField: "Name",
            dataValueField: "ID",
            change: function (e) {
                FilterGrid();
                fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc');
                // window.parent.forchild($("body").height() + 70);
            },
            dataSource: {
                severFiltering: true,
                transport: {
                    read: {
                        url: '<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Flag%>&IsStatutoryInternal=S',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Flag%>&IsStatutoryInternal=S"
                    },
                    schema: {
                        data: function (response) {
                            return response[0].locationList;
                        },
                        model: {
                            children: "Children"
                        }
                    }
                }
            });

        BindGrid();
    });
        function FilterGrid() {
            
            var locationsdetails = [];
            if ($("#dropdowntree").data("kendoDropDownTree") != undefined) {
                locationsdetails = $("#dropdowntree").data("kendoDropDownTree")._values;
            }
            //risk Details
            var Riskdetails = [];
            if ($("#dropdownlistRisk").data("kendoDropDownTree") != undefined) {
                Riskdetails = $("#dropdownlistRisk").data("kendoDropDownTree")._values;
            }
            var Actdetails = [];
            if ($("#dropdownACT").data("kendoDropDownTree") != undefined) {
                Actdetails = $("#dropdownACT").data("kendoDropDownTree")._values;
            }
            var Statusdetails = [];
            if ($("#dropdownlistStatus").data("kendoDropDownTree") != undefined) {
                Statusdetails = $("#dropdownlistStatus").data("kendoDropDownTree")._values;
            }
            //  Function Details
            var Functiondetails = [];
            if ($("#dropdownfunction").val() != "" && $("#dropdownfunction").val() != null && $("#dropdownfunction").val() != undefined) {
                Functiondetails.push({
                    field: "ComplianceCategoryId", operator: "eq", value: parseInt($("#dropdownfunction").val())
                });
            }

            var catdetails = [];
            if ($("#dropdownfunction").data("kendoDropDownTree") != undefined) {
                catdetails = $("#dropdownfunction").data("kendoDropDownTree")._values;
            }

            var finalSelectedfilter = { logic: "and", filters: [] };

            if ( Riskdetails.length > 0
               || locationsdetails.length > 0 
                || Statusdetails.length > 0
                || catdetails.length > 0
                || ($("#dropdownACT").val() != "" && $("#dropdownACT").val() != null && $("#dropdownACT").val() != undefined)
                || $("#txtSearchComplianceID").val() != "" && $("#txtSearchComplianceID").val() != undefined) {


                if (Riskdetails.length > 0) {
                    var RiskFilter = { logic: "or", filters: [] };
                    $.each(Riskdetails, function (i, v) {
                        RiskFilter.filters.push({
                            field: "Risk", operator: "eq", value: parseInt(v)
                        });
                    });
                    finalSelectedfilter.filters.push(RiskFilter);
                }
                if (locationsdetails.length > 0) {
                    var LocationFilter = { logic: "or", filters: [] };

                    $.each(locationsdetails, function (i, v) {
                        LocationFilter.filters.push({
                            field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                        });
                    });

                    finalSelectedfilter.filters.push(LocationFilter);
                }
                if (Statusdetails.length > 0) {
                    var StatusFilter = { logic: "or", filters: [] };
                    $.each(Statusdetails, function (i, v) {
                        StatusFilter.filters.push({
                            field: "ComplianceType", operator: "eq", value: v
                        });
                    });

                    finalSelectedfilter.filters.push(StatusFilter);
                }
                if ($("#dropdownACT").val() != "" && $("#dropdownACT").val() != null && $("#dropdownACT").val() != "-1" && $("#dropdownACT").val() != undefined) {
                    var ActFilter = { logic: "or", filters: [] };

                    ActFilter.filters.push({
                        field: "ActID", operator: "eq", value: parseInt($("#dropdownACT").val())
                    });

                    finalSelectedfilter.filters.push(ActFilter);
                }
        
                if (catdetails.length > 0) {
                    var catFilter = { logic: "or", filters: [] };

                    $.each(catdetails, function (i, v) {
                        catFilter.filters.push({
                            field: "ComplianceCategoryId", operator: "eq", value: v
                        });
                    });

                    finalSelectedfilter.filters.push(catFilter);
                }


                if ($("#txtSearchComplianceID").val() != "") {
                    var RiskFilter = { logic: "or", filters: [] };
                    RiskFilter.filters.push({
                        field: "ComplianceID", operator: "contains", value: $("#txtSearchComplianceID").val()
                    });
                    finalSelectedfilter.filters.push(RiskFilter);
                }

                if (finalSelectedfilter.filters.length > 0) {
                    var dataSource = $("#grid").data("kendoGrid").dataSource;
                    dataSource.filter(finalSelectedfilter);
                }
                else {
                    $("#grid").data("kendoGrid").dataSource.filter({});
                }

            }
            else {
                $("#grid").data("kendoGrid").dataSource.filter({});
            }
        }

        function ClearAllFilterMain(e) {
            $("#dropdowntree").data("kendoDropDownTree").value([]);
            $("#txtSearchComplianceID").val('');
            $("#dropdownfunction").data("kendoDropDownTree").value([]);
            $("#dropdownACT").data("kendoDropDownList").select(0);
            $("#grid").data("kendoGrid").dataSource.filter({});
            e.preventDefault();
        }

        function fcloseStory(obj) {
            var DataId = $(obj).attr('data-Id');
            var dataKId = $(obj).attr('data-K-Id');
            var seq = $(obj).attr('data-seq');
            var deepspan = $('#' + DataId + ' > li > span > span.k-i-close')[seq];
            $(deepspan).trigger('click');
            var upperli = $('#' + dataKId);
            $(upperli).remove();

            fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc');
            fCreateStoryBoard('dropdownfunction', 'filterCategory', 'function');
        }

        function fCreateStoryBoard(Id, div, filtername) {

            var IdKReset = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('id');
            $('#' + div).html('');
            $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');
            $('#' + div).css('display', 'block');

            if (div == 'filtersstoryboard') {
                $('#' + div).append('Location&nbsp;&nbsp;&nbsp;:');//Dashboard
            }
            else if (div == 'filterCategory') {
                $('#' + div).append('Category&nbsp;&nbsp;:');
            }

            for (var i = 0; i < $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length; i++) {
                var button = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button')[i];
                $(button).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('placeholder', 'Shree');
                var buttontest = $($(button).find('span')[0]).text();
                $('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="background-color:#EBEBEB; height: 20px;Color:Gray;border-radius:10px;margin-left:4px;margin-top:1px;"><span unselectable="on" title="' + $($(button).find('span')[0]).text() + '">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory(this)" title="clear" aria-label="clear" class="k-select" style="padding-left: 6px;"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory(this)"  class="k-icon k-i-close" style="font-size: 12px;"></span></span></li>');
                //$('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="background-color:#1fd9e1; height: 20px;Color:white;"><span unselectable="on">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory(this)" title="delete" aria-label="delete" class="k-select"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory(this)"  class="k-icon k-i-close"></span></span></li>');
            }

            if ($($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) {
                $('#' + div).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');

            }
        }

    </script>

</head>
<body style="overflow-x: hidden;">
    <form>
        <div id="example">
            <div class="row">
                <h1 id="display" runat="server" style="height: 30px; background-color: #ffffff; margin-top: 0px; color: #666; margin-bottom: 12px; font-size: 19px; padding-left: 5px; padding-top: 5px; font-weight: bold;"></h1>
            </div>
            <div style="margin: 0.5% 0 0.5%;">
                <input id="CustName" type="hidden" value="<% =CustomerName%>" />
                <input id="dropdowntree" style="width: 22%; margin-right: 0.5%;" />
                <input id="dropdownlistUserRole" style="width: 10%; margin-right: 0.5%;" />
                <input id="dropdownACT" style="width: 21%; margin-right: 0.5%;" />
                <input id="dropdownfunction" style="width: 17%; margin-right: 0.5%;" />
                <input id="txtSearchComplianceID" class="k-textbox" onkeydown="return (event.keyCode!=13);" placeholder="Compliance ID" style="width: 14%; margin-right: 0.5%;" />
                <button id="export" onclick="exportReport(event)" style="height: 30px; margin-right: 0.5%;" data-placement="bottom"><span class="k-icon k-i-excel k-grid-edit3"></span>Export</button>
                <button id="ClearfilterMain" style="height: 30px;" onclick="ClearAllFilterMain(event)"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear</button>
            </div>

            <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; font-weight: bold;" id="filtersstoryboard">&nbsp;</div>
            <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; font-weight: bold;" id="filterCategory">&nbsp;</div>

            <div id="grid" style="margin-top: 6px;"></div>
        </div>
    </form>
</body>
</html>
