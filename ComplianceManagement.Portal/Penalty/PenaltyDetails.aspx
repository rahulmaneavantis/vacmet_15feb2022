﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PenaltyDetails.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Penalty.PenaltyDetails" %>

<!DOCTYPE html>


<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../NewCSS/stylenew.css" rel="stylesheet" />
    <!-- Bootstrap CSS -->
    <link href="../NewCSS/bootstrap.min.css" rel="stylesheet" />
    <!-- bootstrap theme -->
    <link href="../NewCSS/bootstrap-theme.css" rel="stylesheet" />
    <script type="text/javascript" src="../Newjs/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="../Newjs/bootstrap.min.js"></script>

    <style type="text/css">
        .ui-widget-header{border:0px !important; background:inherit;font-size: 20px;color: #666666;font-weight: normal;padding-top: 0px;margin-top: 5px;}
        .table > tbody > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

            .table > tbody > tr > th > a {
                vertical-align: bottom;
                color:#666666 !important;
                text-decoration:none !important; 
                border-bottom: 2px solid #dddddd;
            }

        .table tr th {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .clsheadergrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: bottom !important;
            border-bottom: 2px solid #dddddd !important;
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
            text-align: left;
        }

        .clsROWgrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: top !important;
            border-top: 1px solid #dddddd !important;
            color: #666666 !important;
            font-size: 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>

    <script type="text/javascript">
       

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }
      
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

        <asp:UpdatePanel ID="UpDetailView" runat="server" UpdateMode="Conditional" OnLoad="upDivLocation_Load">
            <ContentTemplate>
                <asp:UpdateProgress ID="updateProgress" runat="server">
                    <ProgressTemplate>
                        <div id="updateProgress1" style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                            <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                                AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
                        </div>
                    </ProgressTemplate>
                </asp:UpdateProgress>
                <div class="col-md-2 colpadding0 entrycount">
                    <div class="col-md-3 colpadding0">
                        <p style="color: #999; margin-top: 5px;">Show </p>
                    </div>
                    <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px; float: left"
                        AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                        <asp:ListItem Text="10" Selected="True" />
                        <asp:ListItem Text="20" />
                        <asp:ListItem Text="50" />
                    </asp:DropDownList>
                   <%-- <div class="col-md-3 colpadding0">
                        <p style="color: #999; margin-top: 5px; margin-left: 5px;">Entries</p>
                    </div>--%>
                </div>

                <div class="col-md-10 colpadding0">
                    <div style="float: left; margin-right: 5px">
                        <asp:TextBox runat="server" ID="tbxFilterLocation" Style="padding: 0px; padding-left: 10px; margin: 0px; height: 33px; width: 265px; border: 1px solid #c7c7cc; border-radius: 4px; color: #8e8e93"
                            CssClass="txtbox" />
                        <div style="margin-left: 1px; position: absolute; z-index: 10; margin-top: -20px;" id="divFilterLocation">
                            <asp:TreeView runat="server" ID="tvFilterLocation" SelectedNodeStyle-Font-Bold="true" Width="300px" NodeStyle-ForeColor="#8e8e93"
                                Style="overflow: auto; border-left: 1px solid #c7c7cc; border-right: 1px solid #c7c7cc; border-bottom: 1px solid #c7c7cc; background-color: #ffffff; color: #8e8e93 !important;" ShowLines="true"
                                OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged">
                            </asp:TreeView>
                        </div>
                    </div>

                    <div>
                        <asp:DropDownList runat="server" ID="ddlFunctions" class="form-control m-bot15 select_location" Style="width: 150px;">
                        </asp:DropDownList>
                    </div>

                    <div>
                        <asp:DropDownList runat="server" ID="ddlPenaltyStatus" class="form-control m-bot15 select_location" Style="width: 150px;">
                            <asp:ListItem Text="Submitted" Value="0" />
                            <asp:ListItem Text="Pending" Value="1" />
                        </asp:DropDownList>
                    </div>

                    <div>
                <%--    <asp:DropDownList runat="server" ID="ddlRole" class="form-control m-bot15 select_location" Style="width: 135px;">
                        <asp:ListItem Text="Role" Value="-1" />
                        <asp:ListItem Text="Performer" Value="3" />
                        <asp:ListItem Text="Reviewer" Value="4" />
                    </asp:DropDownList>--%>
                     </div>

                    <div id="DivAct" runat="server">
                        <asp:DropDownList runat="server" ID="ddlAct" class="form-control m-bot15 select_location" Style="width: 200px;">
                        </asp:DropDownList>
                    </div>

                    <asp:Button ID="btnSearch" CausesValidation="false" class="btn btn-search" runat="server" Text="Apply" OnClick="btnSearch_Click" />
                </div>

                <div class="clearfix"></div>

                <div class="col-md-12 AdvanceSearchScrum">
                    <div id="divAdvSearch" runat="server" visible="false">
                        <p>
                            <asp:Label ID="lblAdvanceSearchScrum" runat="server" Text=""></asp:Label>
                        </p>
                        <p>
                            <asp:LinkButton ID="lnkClearAdvanceList" OnClick="lnkClearAdvanceSearch_Click" runat="server">Clear Search</asp:LinkButton>
                        </p>
                    </div>
                    <div runat="server" id="DivRecordsScrum" style="float: right;">
                        <p style="padding-right: 0px !Important;">
                            <asp:Label ID="Label4" runat="server" Text="Showing "></asp:Label>
                            <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>- 
                                <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                                <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                        </p>
                    </div>
                </div>


                <div style="margin-bottom: 10px; font-weight: bold; font-size: 12px;">
                    <asp:Label runat="server" ID="lblDetailViewTitle"></asp:Label>
                </div>

                <asp:GridView runat="server" ID="GridStatutory" AutoGenerateColumns="false" AllowSorting="true" GridLines="None"
                    CssClass="table" AllowPaging="true" PageSize="10" Width="100%">                  
                    <Columns>

                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Sr">
                            <ItemTemplate>
                                <%#Container.DataItemIndex+1 %>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Act">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px;">
                                    <asp:Label ID="lblActName" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ActName") %>' ToolTip='<%# Eval("ActName") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                     <%--   <asp:TemplateField HeaderText="Sections">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                    <asp:Label ID="lblSections" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Section") %>' ToolTip='<%# Eval("Section") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>--%>

                        <asp:TemplateField HeaderText="ShortDescription">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px;">
                                    <asp:Label ID="lblShortDesc" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                          <asp:TemplateField HeaderText="Due Date">
                                            <ItemTemplate>
                                                 <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">   
                                                       <asp:Label ID="lblScheduledOn" runat="server" data-toggle="tooltip" data-placement="bottom"  Text=' <%# Convert.ToDateTime(Eval("ScheduledOn")).ToString("dd-MMM-yyyy") %>'></asp:Label>
                                                     </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="User">
                            <ItemTemplate>
                                <asp:Label ID="lblUser" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("UserName") %>' ToolTip='<%# Eval("UserName") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                          <asp:TemplateField HeaderText="Interest">
                            <ItemTemplate>
                                <asp:Label ID="lblInterest" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Interest") %>' ToolTip='<%# Eval("Interest") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Penalty">
                            <ItemTemplate>
                                <asp:Label ID="lblPenalty" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Penalty") %>' ToolTip='<%# Eval("Penalty") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Remark">
                            <ItemTemplate>
                                <asp:Label ID="lblRemark" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Remarks") %>' ToolTip='<%# Eval("Remarks") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>

                    <RowStyle CssClass="clsROWgrid" />
                    <HeaderStyle CssClass="clsheadergrid" />

                    <PagerTemplate>
                        <table style="display: none">
                            <tr>
                                <td>
                                    <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                </td>
                            </tr>
                        </table>
                    </PagerTemplate>

                    <EmptyDataTemplate>
                        No Records Found.
                    </EmptyDataTemplate>
                </asp:GridView>

                <asp:GridView runat="server" ID="GridInternalCompliance" AutoGenerateColumns="false" GridLines="none" AllowSorting="true"
                    CssClass="table" AllowPaging="true" PageSize="10" Width="100%">
                    <%--OnPageIndexChanging="grdSummaryDetails_PageIndexChanging" DataKeyNames="ID"--%>
                    <Columns>

                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Sr">
                            <ItemTemplate>
                                <%#Container.DataItemIndex+1 %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Description">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                    <asp:Label ID="lblSections" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Type">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                    <asp:Label ID="lblActName" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("InternalComplianceTypeName") %>' ToolTip='<%# Eval("InternalComplianceTypeName") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Category">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                    <asp:Label ID="lblShortDesc" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("InternalComplianceCategoryName") %>' ToolTip='<%# Eval("InternalComplianceCategoryName") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Name">
                            <ItemTemplate>
                                <asp:Label ID="lblUser" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("User") %>' ToolTip='<%# Eval("User") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="clsROWgrid" />
                    <HeaderStyle CssClass="clsheadergrid" />
                    <PagerTemplate>
                        <table style="display: none">
                            <tr>
                                <td>
                                    <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                </td>
                            </tr>
                        </table>
                    </PagerTemplate>

                    <EmptyDataTemplate>
                        No Records Found.
                    </EmptyDataTemplate>
                </asp:GridView>

                <%--</div>
                </div>--%>
                <div class="clearfix"></div>

                <div class="col-md-12 colpadding0">
                    <div class="col-md-6 colpadding0" style="float: right;">
                        <div class="table-paging" style="margin-bottom: 20px;">
                            <asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick="Previous_Click" />

                            <div class="table-paging-text">
                                <p>
                                    <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                            <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>
                                </p>
                            </div>

                            <asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick="Next_Click" />
                            <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
         <script>
             $('#updateProgress1').show();

             $(document).ready(function () {
                 $(document).tooltip({ selector: '[data-toggle="tooltip"]' });
                 $('#updateProgress1').hide();
             });
         </script>
    </form>
</body>
</html>

