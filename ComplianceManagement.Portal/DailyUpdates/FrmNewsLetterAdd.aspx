﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="FrmNewsLetterAdd.aspx.cs" ValidateRequest="false" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.DailyUpdates.FrmNewsLetterAdd" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript" src="../tiny_mce/tiny_mce.js"></script>

    <script type="text/javascript" language="javascript">
        tinyMCE.init({
            mode: "textareas",
            theme: "advanced",
            plugins: "pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups",

        });

        function initializeDatePicker(date) {
            var startDate = new Date();
            $(".StartDate").datepicker({
                dateFormat: 'dd-mm-yy',
                setDate: startDate,
                numberOfMonths: 1
            });
        }

        function setDate() {
            $(".StartDate").datepicker();
        }

    </script>

    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    </style>

    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upEventList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div style="margin-bottom: 4px">
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="vdsummary"
                    ValidationGroup="EventValidationGroup" />
                <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                    ValidationGroup="EventValidationGroup" Display="None" />
            </div>
            <div style="margin: 5px">
                <asp:UpdatePanel ID="upEvent" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div style="margin-bottom: 7px">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                            <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                Title</label>
                            <asp:TextBox runat="server" ID="txtTitle" Style="width: 390px;" MaxLength="500" ToolTip="Name" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Title can not be empty." ControlToValidate="txtTitle"
                                runat="server" ValidationGroup="EventValidationGroup" Display="None" />
                        </div>
                        <asp:UpdatePanel ID="upFileUploadPanel" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div style="margin-bottom: 7px" id="dvSampleForm" runat="server">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                    <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                        Upload Image</label>
                                    <asp:Label runat="server" ID="lblSampleForm" CssClass="txtbox" />
                                    <asp:FileUpload runat="server" ID="fuSampleFile" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Image can not be empty." ControlToValidate="fuSampleFile"
                        runat="server" ValidationGroup="EventValidationGroup" Display="None" Enabled="false" />
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <div style="margin-bottom: 7px">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                            <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                Date
                            </label>
                            <asp:TextBox runat="server" ID="tbxStartDate" CssClass="StartDate" Style="height: 16px; width: 390px;" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="Date can not be empty." ControlToValidate="tbxStartDate"
                                runat="server" ValidationGroup="EventValidationGroup" Display="None" />
                        </div>
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div style="margin-bottom: 7px" id="Div1" runat="server">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                    <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                        Upload Document</label>
                                    <asp:Label runat="server" ID="lblSampleDoc" CssClass="txtbox" />
                                    <asp:FileUpload runat="server" ID="FileUpload1" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ErrorMessage="File can not be empty." ControlToValidate="FileUpload1"
                                        runat="server" ValidationGroup="EventValidationGroup" Display="None" Enabled="false" />
                                </div>
                                <div style="margin-bottom: 7px; margin-left: 210px" id="Div3" runat="server">
                                    <asp:Label ID="Label2" runat="server"></asp:Label>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <div style="margin-bottom: 7px">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                Description</label>
                            <asp:TextBox runat="server" ID="txtDescription" Height="327px" Width="528px" TextMode="MultiLine" />
                         <%--   <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Description can not be empty." ControlToValidate="txtDescription"
                                runat="server" ValidationGroup="EventValidationGroup" Display="None" />--%>
                        </div>

                        <div style="margin-bottom: 7px; margin-left: 152px; margin-top: 10px">
                            <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="button"
                                ValidationGroup="EventValidationGroup" />
                            <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="button" OnClick="btnCancel_Click" />
                        </div>
                        <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 10px;">
                            <p style="color: red;"><strong>Note:</strong> (*) Fields Are Compulsary</p>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                          <asp:PostBackTrigger ControlID="txtTitle" />
                        <asp:PostBackTrigger ControlID="tbxStartDate" />
                        <asp:PostBackTrigger ControlID="txtDescription" />
                        <asp:PostBackTrigger ControlID="btnSave" />
                    </Triggers>
                </asp:UpdatePanel>
        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>
