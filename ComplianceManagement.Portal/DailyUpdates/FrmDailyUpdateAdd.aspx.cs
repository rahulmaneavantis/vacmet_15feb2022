﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Data;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System.Configuration;
using System.IO;

namespace com.VirtuosoITech.ComplianceManagement.Portal.DailyUpdates
{
    public partial class FrmDailyUpdateAdd : System.Web.UI.Page
    {
        public static string Container = "";
        static string FileName = "";

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                divDate.Visible = false;
                rfvDate.Enabled = false;
                ViewState["Mode"] = Session["Mode"];
                ViewState["dailyUpdateID"] = Session["dailyUpdateID"];
                chkAll.Visible = false;
                chkAll.Checked = false;
                txtTitle.Text = "";
                txtSubTitle.Text = "";
                txtDescription.Text = "";
                txtactList.Text = "< Select Act >";
                //txtcompliance.Text = "<Filter ShortDescription/Section>";
                txtState.Text = "< Select State >";
                txtCategory.Text = "< Select Category >";
                txtIndustry.Text = "< Select Industry >";
                txtCategory.Attributes.Add("readonly", "readonly");
                txtState.Attributes.Add("readonly", "readonly");
                txtIndustry.Attributes.Add("readonly", "readonly");
                divState.Visible = false;
                txtMetaKeyword.Text = "";
                txtMetaDesc.Text = "";
                txtDocKeyword.Text = "";
                BindMinistry();
                BindRegulator();
                BindIndustry();
                BindActDepartment();
                BindActDocType();
                IsMiscellaneous.Checked = false;
                chkIsTranslate.Checked = false;
                chkIsCovid.Checked = false;

                ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(this.btndocumentDownload);

                foreach (RepeaterItem aItem in rptState.Items)
                {
                    CheckBox chkState = (CheckBox)aItem.FindControl("chkState");
                    chkState.Checked = false;
                }
                foreach (RepeaterItem aItem in rptComplianceList.Items)
                {
                    CheckBox chkCompliance = (CheckBox)aItem.FindControl("chkCompliance");
                    chkCompliance.Checked = false;
                }

                foreach (RepeaterItem aItem in rptIndustry.Items)
                {
                    CheckBox chkIndustry = (CheckBox)aItem.FindControl("chkIndustry");
                    chkIndustry.Checked = false;
                    CheckBox IndustrySelectAll = (CheckBox)rptIndustry.Controls[0].Controls[0].FindControl("IndustrySelectAll");
                    IndustrySelectAll.Checked = false;
                }

                BindAct();
                BindScheme();
                BindState();
                BindCategory();

                var CheckSchemeSelected = Business.ComplianceManagement.CheckSchemeSelected(Convert.ToInt32(ViewState["dailyUpdateID"]));

                if (CheckSchemeSelected == 0)
                {
                    //Edit Case Act Other Option Selected
                    foreach (RepeaterItem aItem in rptActList.Items)
                    {
                        CheckBox chkAct = (CheckBox)aItem.FindControl("chkAct");
                        if (((Label)aItem.FindControl("lblActID")).Text.Trim() == "-1")
                        {
                            chkAct.Checked = true;
                        }
                    }
                    cmpScheme.Visible = true;
                    divScheme.Visible = true;
                    ddlScheme.SelectedValue = CheckSchemeSelected.ToString();
                   
                }
                else
                {
                    // Edit Case Act Selected
                    var vGetCmplianceMappedIDs = Business.ComplianceManagement.GetActMappedID(Convert.ToInt32(ViewState["dailyUpdateID"]));
                    foreach (RepeaterItem aItem in rptActList.Items)
                    {
                        CheckBox chkAct = (CheckBox)aItem.FindControl("chkAct");
                        chkAct.Checked = false;
                        CheckBox ActSelectAll = (CheckBox)rptActList.Controls[0].Controls[0].FindControl("ActSelectAll");

                        for (int i = 0; i <= vGetCmplianceMappedIDs.Count - 1; i++)
                        {
                            if (((Label)aItem.FindControl("lblActID")).Text.Trim() == vGetCmplianceMappedIDs[i].ToString())
                            {
                                chkAct.Checked = true;
                            }
                        }
                        if ((rptActList.Items.Count) == (vGetCmplianceMappedIDs.Count))
                        {
                            ActSelectAll.Checked = true;
                        }
                        else
                        {
                            ActSelectAll.Checked = false;
                        }
                    }
                    btnRefresh_Click(sender, e);
                }

                //ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeActList", string.Format("initializeJQueryUI('{0}', 'dvActList');", txtactList.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideActList", "$(\"#dvActList\").hide(\"blind\", null, 5, function () { });", true);


                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeComplianceList", string.Format("initializeJQueryUI('{0}', 'dvComplianceList');", txtcompliance.ClientID), true);
                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideComplianceList", "$(\"#dvComplianceList\").hide(\"blind\", null, 5, function () { });", true);


                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeIndustryList", string.Format("initializeJQueryUI('{0}', 'dvIndustry');", txtIndustry.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideIndustryList", "$(\"#dvIndustry\").hide(\"blind\", null, 5, function () { });", true);

                if ((int)ViewState["Mode"] == 1)
                {
                    com.VirtuosoITech.ComplianceManagement.Business.Data.DailyUpdate eventData = DailyUpdateManagment.GetByID(Convert.ToInt32(ViewState["dailyUpdateID"]));

                    txtTitle.Text = eventData.Title;
                    txtSubTitle.Text = eventData.SubTitle;
                    txtDescription.Text = eventData.Description;
                    txtLink.Text = eventData.Link;

                    txtMetaKeyword.Text = eventData.MetaKeyWord;
                    txtMetaDesc.Text = eventData.MetaDesc;
                    txtDocKeyword.Text = eventData.DocKeyword;
                    //Draft button disable
                    btnSave.Enabled = true;
                    if (eventData.IsActive == true)
                    {
                        btnSave.Enabled = false;
                    }

                    if (eventData.type == null)
                    {
                        ddlDailyUpdateType.SelectedValue = "0";
                    }
                    else
                    {
                        if (eventData.type == "C")
                        {
                            divState.Visible = false;
                            ddlDailyUpdateType.SelectedValue = "1";
                        }
                        else
                        {
                            divState.Visible = true;
                            ddlDailyUpdateType.SelectedValue = "2";
                        }
                    }

                    //Main or Sub
                    if (eventData.MainFlag == null)
                    {
                        ddlMainSub.SelectedValue = "0";
                    }
                    else
                    {
                        if (eventData.MainFlag == 1)
                        {
                            ddlMainSub.SelectedValue = "1";
                        }
                        else if (eventData.MainFlag == 2)
                        {
                            ddlMainSub.SelectedValue = "2";
                        }
                        else
                        {
                            ddlMainSub.SelectedValue = "0";
                        }
                    }

                    if (eventData.IsMiscellaneous == true)
                    {
                        txtactList.Enabled = false;
                    }
                    else
                    {
                        txtactList.Enabled = true;
                    }

                    if (eventData.DeptID != null)
                    {
                        ddlDepartment.SelectedValue = Convert.ToString(eventData.DeptID);
                    }

                    if (eventData.RegulatorID != null)
                    {
                        ddlRegulator.SelectedValue = Convert.ToString(eventData.RegulatorID);
                    }

                    if (eventData.MinistryID != null)
                    {
                        ddlMinistry.SelectedValue = Convert.ToString(eventData.MinistryID);
                    }

                    if (eventData.IsMiscellaneous != null)
                    {
                        if (eventData.IsMiscellaneous == true)
                        {
                            IsMiscellaneous.Checked = true;
                        }
                    }

                    if (eventData.IsTranslate != null)
                    {
                        if (eventData.IsTranslate == true)
                        {
                            chkIsTranslate.Checked = true;
                        }
                    }

                    if (eventData.IsCovid != null)
                    {
                        if (eventData.IsCovid == true)
                        {
                            chkIsCovid.Checked = true;
                        }
                    }

                    // Edit Case Category Selected
                    var vGetCategoryMappedIDs = Business.DailyUpdateManagment.GetCategoryMappedID(Convert.ToInt32(ViewState["dailyUpdateID"]));
                    foreach (RepeaterItem aItem in rptCategory.Items)
                    {
                        CheckBox chkCategory = (CheckBox)aItem.FindControl("chkCategory");
                        chkCategory.Checked = false;
                        //CheckBox CategorySelectAll = (CheckBox)rptCategory.Controls[0].Controls[0].FindControl("CategorySelectAll");

                        for (int i = 0; i <= vGetCategoryMappedIDs.Count - 1; i++)
                        {
                            if (((Label)aItem.FindControl("lblCategoryID")).Text.Trim() == vGetCategoryMappedIDs[i].ToString())
                            {
                                chkCategory.Checked = true;
                            }
                        }
                        //if ((rptCategory.Items.Count) == (vGetCategoryMappedIDs.Count))
                        //{
                        //    CategorySelectAll.Checked = true;
                        //}
                        //else
                        //{
                        //    CategorySelectAll.Checked = false;
                        //}
                    }

                    if (eventData.type == "S")
                    {
                        // Edit Case State Selected
                        var vGetStateMappedIDs = Business.DailyUpdateManagment.GetStateMappedID(Convert.ToInt32(ViewState["dailyUpdateID"]));
                        foreach (RepeaterItem aItem in rptState.Items)
                        {
                            CheckBox chkState = (CheckBox)aItem.FindControl("chkState");
                            chkState.Checked = false;
                            //CheckBox StateSelectAll = (CheckBox)rptState.Controls[0].Controls[0].FindControl("StateSelectAll");

                            for (int i = 0; i <= vGetStateMappedIDs.Count - 1; i++)
                            {
                                if (((Label)aItem.FindControl("lblStateID")).Text.Trim() == vGetStateMappedIDs[i].ToString())
                                {
                                    chkState.Checked = true;
                                }
                            }
                            //if ((rptState.Items.Count) == (vGetStateMappedIDs.Count))
                            //{
                            //    StateSelectAll.Checked = true;
                            //}
                            //else
                            //{
                            //    StateSelectAll.Checked = false;
                            //}
                        }
                    }


                    // Edit Case Industry Selected
                    var vGetIndustryMappedIDs = Business.DailyUpdateManagment.GetIndustryMappedID(Convert.ToInt32(ViewState["dailyUpdateID"]));
                    foreach (RepeaterItem aItem in rptIndustry.Items)
                    {
                        CheckBox chkIndustry = (CheckBox)aItem.FindControl("chkIndustry");
                        chkIndustry.Checked = false;

                        for (int i = 0; i <= vGetIndustryMappedIDs.Count - 1; i++)
                        {
                            if (((Label)aItem.FindControl("lblIndustryID")).Text.Trim() == vGetIndustryMappedIDs[i].ToString())
                            {
                                chkIndustry.Checked = true;
                            }
                        }
                    }

                    if (eventData.SchemeID != null)
                    {
                        string schemeID = eventData.SchemeID.ToString();
                        ddlScheme.SelectedValue = schemeID;
                        if (eventData.SchemeID != -1)
                        {
                            ddlScheme_SelectedIndexChanged(sender, e);
                        }
                    }
                    if (eventData.SelectAll != null)
                    {
                        int? chk = Convert.ToInt32(eventData.SelectAll);
                        if (chk == 1)
                        {
                            chkAll.Checked = true;
                        }
                        else
                        {
                            chkAll.Checked = false;
                        }

                        if (ddlScheme.SelectedIndex == 1)
                        {
                            chkAll.Visible = true;
                        }
                    }

                    btndocumentDownload.Visible = false;

                    //Edit Mapped Compliance
                    var vGetComplianceMappedIDs = Business.DailyUpdateManagment.GetComplianceMappedID(Convert.ToInt32(ViewState["dailyUpdateID"]));
                    foreach (RepeaterItem aItem in rptComplianceList.Items)
                    {
                        CheckBox chkCompliance = (CheckBox)aItem.FindControl("chkCompliance");
                        chkCompliance.Checked = false;

                        for (int i = 0; i <= vGetComplianceMappedIDs.Count - 1; i++)
                        {
                            if (((Label)aItem.FindControl("lblComplianceID")).Text.Trim() == vGetComplianceMappedIDs[i].ToString())
                            {
                                chkCompliance.Checked = true;
                            }
                        }

                    }

                    //var complianceList = Business.DailyUpdateManagment.GetComplianceMappedID(Convert.ToInt32(ViewState["dailyUpdateID"]));

                    //if (complianceList != null && complianceList.Count > 0)
                    //{
                    //    foreach (GridViewRow gvrow in grdCompliances.Rows)
                    //    {
                    //        int index = Convert.ToInt32(grdCompliances.DataKeys[gvrow.RowIndex].Value);
                    //         DailyUpdateComplianceMapping rmdata = complianceList.Where(t => t.ComplianceID == index).FirstOrDefault();
                    //        if (rmdata != null)
                    //        {
                    //            CheckBox chkPerformer = (CheckBox)gvrow.FindControl("chkSelect");
                    //            chkPerformer.Checked =true;
                    //        }
                    //    }
                    //}

                    DailyUpdateDocument Doc = Business.DailyUpdateManagment.GetDailyUpdateDocument(Convert.ToInt32(ViewState["dailyUpdateID"]));
                    if (Doc != null)
                    {
                        btndocumentDownload.Visible = true;
                        FileName = Doc.Name;
                        Container = "legalupdatedocs/" + Convert.ToString(Doc.DailyUpdateID);
                    }
                }
            }
        }
        
        private void BindIndustry()
        {
            try
            {
                rptIndustry.DataSource = CustomerBranchManagement.GetAllIndustry();
                rptIndustry.DataBind();

                foreach (RepeaterItem aItem in rptIndustry.Items)
                {
                    CheckBox chkIndustry = (CheckBox)aItem.FindControl("chkIndustry");

                    if (!chkIndustry.Checked)
                    {
                        chkIndustry.Checked = true;
                    }
                }
                CheckBox IndustrySelectAll = (CheckBox)rptIndustry.Controls[0].Controls[0].FindControl("IndustrySelectAll");
                IndustrySelectAll.Checked = true;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindActDepartment()
        {
            try
            {
                ddlDepartment.DataTextField = "Name";
                ddlDepartment.DataValueField = "ID";
                ddlDepartment.DataSource = ActManagement.GetAllActDepartment();
                ddlDepartment.DataBind();
                ddlDepartment.Items.Insert(0, new ListItem("< Select Department>", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static object GetAllActDepartment()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Act_Department
                             where row.IsDeleted == false
                             select new { row.ID, row.Name }).ToList();

                return query;
            }
        }
        private void BindMinistry()
        {
            try
            {
                ddlMinistry.DataTextField = "Name";
                ddlMinistry.DataValueField = "ID";
                ddlMinistry.DataSource = ActManagement.GetAllMinistry();
                ddlMinistry.DataBind();
                ddlMinistry.Items.Insert(0, new ListItem("< Select Ministry>", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindRegulator()
        {
            try
            {
                ddlRegulator.DataTextField = "Name";
                ddlRegulator.DataValueField = "ID";
                ddlRegulator.DataSource = ActManagement.GetAllRegulator();
                ddlRegulator.DataBind();
                ddlRegulator.Items.Insert(0, new ListItem("< Select Regulator>", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int cnt = 0;
                foreach (RepeaterItem aItem in rptActList.Items)
                {
                    CheckBox chkAct = (CheckBox)aItem.FindControl("chkAct");
                    if (chkAct.Checked == true)
                    {
                        cnt += 1;
                        break;
                    }
                }


                Boolean ActSelected = false;
                foreach (RepeaterItem aItem in rptActList.Items)
                {
                    CheckBox chkAct = (CheckBox)aItem.FindControl("chkAct");
                    if (chkAct.Checked == true)
                    {
                        if (((Label)aItem.FindControl("lblActID")).Text.Trim() != "-1")
                        {
                            ActSelected = true;
                            break;
                        }
                    }
                }

                if (IsMiscellaneous.Checked == true)
                {
                    cnt += 1;
                    ActSelected = true;
                }

                Boolean SchemeSelected = false;
                foreach (RepeaterItem aItem in rptActList.Items)
                {
                    CheckBox chkAct = (CheckBox)aItem.FindControl("chkAct");
                    if (chkAct.Checked == true)
                    {
                        if (((Label)aItem.FindControl("lblActID")).Text.Trim() == "-1")
                        {
                            SchemeSelected = true;
                            break;
                        }
                    }
                }

                Boolean CategorySelected = false;
                foreach (RepeaterItem aItem in rptCategory.Items)
                {
                    CheckBox chkCategory = (CheckBox)aItem.FindControl("chkCategory");
                    if (chkCategory.Checked == true)
                    {
                        if (((Label)aItem.FindControl("lblCategoryID")).Text.Trim() != "-1")
                        {
                            CategorySelected = true;
                            break;
                        }
                    }
                }

                Boolean StateSelected = false;
                string dailyupdateType = "";
                //int stateselectcount = 0;
                if (ddlDailyUpdateType.SelectedValue == "2")
                {
                    dailyupdateType = "S";
                    foreach (RepeaterItem aItem in rptState.Items)
                    {
                        CheckBox chkState = (CheckBox)aItem.FindControl("chkState");
                        if (chkState.Checked == true)
                        {
                            //stateselectcount += 1;
                            if (((Label)aItem.FindControl("lblStateID")).Text.Trim() != "-1")
                            {
                                StateSelected = true;
                            }
                        }
                    }
                }
                else if (ddlDailyUpdateType.SelectedValue == "1")
                {
                    //stateselectcount = 1;
                    dailyupdateType = "C";
                    StateSelected = true;
                }

                int MainSub = -1;
                if (ddlMainSub.SelectedValue == "1")
                {
                    MainSub = 1;
                }
                else if (ddlMainSub.SelectedValue == "2")
                {
                    MainSub = 2;
                }
                else
                {
                    MainSub = 0;
                }
                int regulator = 0;
                if (ddlRegulator.SelectedValue != "-1")
                {
                    regulator = Convert.ToInt32(ddlRegulator.SelectedValue);
                }
                int ministry = 0;
                if (ddlMinistry.SelectedValue != "-1")
                {
                    ministry = Convert.ToInt32(ddlMinistry.SelectedValue);
                }

                int DeptID = 0;
                if (ddlDepartment.SelectedValue != "-1")
                {
                    DeptID = Convert.ToInt32(ddlDepartment.SelectedValue);
                }


                bool IsflagSpecialChar = false;
                if (fudocument.FileBytes != null && fudocument.FileBytes.LongLength > 0)
                {
                    var splChars = "*|,\":<>[]{}`\';()@&$#%";
                    var filename = fudocument.FileName;
                    foreach (char cha in filename)
                    {
                        if (splChars.Contains(cha))
                        {
                            IsflagSpecialChar = true;
                            break;
                        }
                        else
                        {
                            IsflagSpecialChar = false;
                        }
                    }

                }

                if (IsflagSpecialChar == false)
                {
                    if (cnt > 0)
                    {
                        if ((SchemeSelected == false && ActSelected == true) || (SchemeSelected == true && ActSelected == false))
                        {
                            if (CategorySelected == true)
                            {
                                if (StateSelected == true)
                                {
                                    DailyUpdate eventData = new DailyUpdate()
                                    {
                                        Title = txtTitle.Text,
                                        SubTitle = txtSubTitle.Text,
                                        Description = txtDescription.Text,
                                        SchemeID = Convert.ToInt64(ddlScheme.SelectedValue),
                                        //CategoryID = Convert.ToInt32(ddlCategory.SelectedValue),
                                        Link = txtLink.Text,
                                        SelectAll = chkAll.Checked,
                                        CreatedBy = AuthenticationHelper.UserID,
                                        UpdatedBy = AuthenticationHelper.UserID,
                                        CreatedDate = DateTime.Now.Date,
                                        UpdatedDate = DateTime.Now.Date,
                                        IsDeleted = false,
                                        type = dailyupdateType,
                                        IsActive = false,
                                        MainFlag = MainSub,
                                        MetaKeyWord = txtMetaKeyword.Text,
                                        MetaDesc = txtMetaDesc.Text,
                                        DocKeyword = txtDocKeyword.Text,
                                        IsMiscellaneous = IsMiscellaneous.Checked,
                                        RegulatorID = regulator,
                                        MinistryID = ministry,
                                        DeptID = DeptID,
                                        IsTranslate = chkIsTranslate.Checked,
                                        IsCovid = chkIsCovid.Checked,
                                    };

                                    if ((int)ViewState["Mode"] == 1)
                                    {
                                        eventData.ID = Convert.ToInt32(ViewState["dailyUpdateID"]);
                                    }

                                    if ((int)ViewState["Mode"] == 0)
                                    {
                                        if (EventManagement.ExistsDailyUpdate(eventData.Title))
                                        {
                                            cvDuplicateEntry.IsValid = false;
                                            cvDuplicateEntry.ErrorMessage = "Title name already exists.";
                                            upEventList.Update();
                                            return;
                                        }
                                        DailyUpdateManagment.CreateDailyUpdate(eventData);
                                        eventData.ID = eventData.ID;
                                    }
                                    else if ((int)ViewState["Mode"] == 1)
                                    {
                                        DailyUpdateManagment.UpdateDailyUpdate(eventData);
                                    }
                                    Boolean IsOtherSelected = false;
                                    foreach (RepeaterItem aItem in rptActList.Items)
                                    {
                                        CheckBox chkAct = (CheckBox)aItem.FindControl("chkAct");
                                        if (chkAct.Checked == true)
                                        {
                                            if (((Label)aItem.FindControl("lblActID")).Text.Trim() == "-1")
                                            {
                                                IsOtherSelected = true;
                                            }
                                        }
                                    }

                                    // Add ActMapping to DailyUpdate
                                    if (IsOtherSelected == true)
                                    {
                                        //For Other Option Selected
                                        List<int> ParentEventIds = new List<int>();
                                        Business.ComplianceManagement.RemoveDailyUpdateActMapping(Convert.ToInt32(eventData.ID));
                                        var SchemeMappedActList = ActManagement.GetSchemeMappedActIDs(Convert.ToInt32(ddlScheme.SelectedValue));
                                        foreach (var aItem in SchemeMappedActList)
                                        {
                                            var schemeid = Convert.ToInt64(ddlScheme.SelectedValue);
                                            DailyUpdateActMapping actMapping = new DailyUpdateActMapping()
                                            {
                                                ActID = (int)aItem,
                                                DailyUpdateID = (int)eventData.ID,
                                                SchemeID = schemeid,
                                                IsActive = true,
                                                CreatedDate = DateTime.Now,
                                                CreatedBy = AuthenticationHelper.UserID,
                                            };
                                            Business.ComplianceManagement.CreateDailyUpdateActMapping(actMapping);
                                        }

                                        #region Daily Update Notification
                                        //using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                        //{
                                        //    //Get All Users reasarch Act  Assigned 
                                        //    List<long> ListUsers = ActManagement.GetResearchMappedDetails(SchemeMappedActList);
                                        //    ListUsers = ListUsers.Distinct().ToList();
                                        //    List<string> details = new List<string>();
                                        //    if (ListUsers.Count > 0)
                                        //    {
                                        //        details.Clear();
                                        //        foreach (var item in ListUsers)
                                        //        {
                                        //            var deviceIDList = (from row in entities.MaintainDeviceIDs
                                        //                                where row.Product.Equals("L") && row.UserID == item && row.Status.Equals("Active")
                                        //                                select row.DeviceToken).FirstOrDefault();

                                        //            if (!string.IsNullOrEmpty(deviceIDList))
                                        //            {
                                        //                details.Add(deviceIDList);
                                        //            }
                                        //        }
                                        //        // details.Add("dVC3g5uLZiA:APA91bG20O-A5OnAPh9Xgq_BY02gFYfRA0FQzQliMxIlVs011Y2CG2CfIDphE0Vx5nGwyKYj0HlOnQF84uRYG_2NuxF1naeKkKqiUBZiZcs6ZH1R-K_M466CB1qoMSWhvNSEnK7vIFiq");
                                        //        details = details.Where(x => x != "null").ToList();
                                        //        string[] arr1 = details.Select(i => i.ToString()).ToArray();
                                        //        if (arr1.Length > 0)
                                        //        {
                                        //            string chk = System.Text.RegularExpressions.Regex.Replace(txtTitle.Text, "<[^>]*>", "");
                                        //            NotificationManagement.SendNotificationDailyUpdateMobile(arr1, chk, "Daily Updates");
                                        //        }
                                        //        details.Clear();
                                        //        details = null;
                                        //        long NotificationID = 0;
                                        //        com.VirtuosoITech.ComplianceManagement.Business.Data.Notification newNotification = new com.VirtuosoITech.ComplianceManagement.Business.Data.Notification()
                                        //        {
                                        //            ActID = 0,
                                        //            ComplianceID = eventData.ID,
                                        //            Type = "DailyUpdate",
                                        //            Remark = System.Text.RegularExpressions.Regex.Replace(txtDescription.Text, "<[^>]*>", ""),
                                        //            CreatedBy = AuthenticationHelper.UserID,
                                        //            CreatedOn = DateTime.Now.Date,
                                        //            UpdatedBy = AuthenticationHelper.UserID,
                                        //            UpdatedOn = DateTime.Now.Date,
                                        //        };
                                        //        Business.ComplianceManagement.CreateNotification(newNotification);
                                        //        NotificationID = newNotification.ID;
                                        //        if (NotificationID != 0)
                                        //        {
                                        //            ListUsers.ForEach(EachUser =>
                                        //            {
                                        //                UserNotification UNF = new UserNotification()
                                        //                {
                                        //                    NotificationID = NotificationID,
                                        //                    UserID = Convert.ToInt32(EachUser),
                                        //                    IsRead = false,
                                        //                };

                                        //                if (!Business.ComplianceManagement.ExistsUserNotification(UNF))
                                        //                    Business.ComplianceManagement.CreateUserNotification(UNF);
                                        //                else
                                        //                    Business.ComplianceManagement.UpdateNotification(newNotification);
                                        //            });
                                        //        }
                                        //    }
                                        //}
                                        #endregion
                                    }
                                    else
                                    {
                                        //For Multiple Acts Selected
                                        List<long> ActIds = new List<long>();
                                        ActIds.Clear();
                                        Business.ComplianceManagement.RemoveDailyUpdateActMapping(Convert.ToInt32(eventData.ID));
                                        foreach (RepeaterItem aItem in rptActList.Items)
                                        {
                                            CheckBox chkCompliance = (CheckBox)aItem.FindControl("chkAct");
                                            if (chkCompliance.Checked)
                                            {
                                                var schemeid = Convert.ToInt64(ddlScheme.SelectedValue);
                                                ActIds.Add(Convert.ToInt32(((Label)aItem.FindControl("lblActID")).Text.Trim()));
                                                DailyUpdateActMapping actMapping = new DailyUpdateActMapping()
                                                {
                                                    ActID = Convert.ToInt32(((Label)aItem.FindControl("lblActID")).Text.Trim()),
                                                    DailyUpdateID = eventData.ID,
                                                    SchemeID = schemeid,
                                                    IsActive = true,
                                                    CreatedDate = DateTime.Now,
                                                    CreatedBy = Convert.ToInt32(Session["userID"]),
                                                };
                                                Business.ComplianceManagement.CreateDailyUpdateActMapping(actMapping);
                                            }
                                        }

                                        #region Daily Update Notification                          
                                        //using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                        //{  //Get All Users reasarch Act  Assigned 
                                        //    List<long> ListUsers = ActManagement.GetResearchMappedDetails(ActIds);
                                        //    ListUsers = ListUsers.Distinct().ToList();
                                        //    List<string> details = new List<string>();
                                        //    if (ListUsers.Count > 0)
                                        //    {
                                        //        details.Clear();
                                        //        foreach (var item in ListUsers)
                                        //        {
                                        //            var deviceIDList = (from row in entities.MaintainDeviceIDs
                                        //                                where row.Product.Equals("L") && row.UserID == item && row.Status.Equals("Active")
                                        //                                select row.DeviceToken).FirstOrDefault();

                                        //            if (!string.IsNullOrEmpty(deviceIDList))
                                        //            {
                                        //                details.Add(deviceIDList);
                                        //            }
                                        //        }
                                        //        // details.Add("dVC3g5uLZiA:APA91bG20O-A5OnAPh9Xgq_BY02gFYfRA0FQzQliMxIlVs011Y2CG2CfIDphE0Vx5nGwyKYj0HlOnQF84uRYG_2NuxF1naeKkKqiUBZiZcs6ZH1R-K_M466CB1qoMSWhvNSEnK7vIFiq");
                                        //        details = details.Where(x => x != "null").ToList();
                                        //        string[] arr1 = details.Select(i => i.ToString()).ToArray();
                                        //        if (arr1.Length > 0)
                                        //        {
                                        //            string chk = System.Text.RegularExpressions.Regex.Replace(txtTitle.Text, "<[^>]*>", "");
                                        //            NotificationManagement.SendNotificationDailyUpdateMobile(arr1, chk, "Daily Updates");
                                        //        }
                                        //        details.Clear();
                                        //        details = null;

                                        //        long NotificationID = 0;
                                        //        com.VirtuosoITech.ComplianceManagement.Business.Data.Notification newNotification = new com.VirtuosoITech.ComplianceManagement.Business.Data.Notification()
                                        //        {
                                        //            ActID = 0,
                                        //            ComplianceID = eventData.ID,
                                        //            Type = "DailyUpdate",
                                        //            Remark = System.Text.RegularExpressions.Regex.Replace(txtDescription.Text, "<[^>]*>", ""),
                                        //            CreatedBy = AuthenticationHelper.UserID,
                                        //            CreatedOn = DateTime.Now.Date,
                                        //            UpdatedBy = AuthenticationHelper.UserID,
                                        //            UpdatedOn = DateTime.Now.Date,
                                        //        };
                                        //        Business.ComplianceManagement.CreateNotification(newNotification);
                                        //        NotificationID = newNotification.ID;
                                        //        if (NotificationID != 0)
                                        //        {
                                        //            ListUsers.ForEach(EachUser =>
                                        //            {
                                        //                UserNotification UNF = new UserNotification()
                                        //                {
                                        //                    NotificationID = NotificationID,
                                        //                    UserID = Convert.ToInt32(EachUser),
                                        //                    IsRead = false,
                                        //                };

                                        //                if (!Business.ComplianceManagement.ExistsUserNotification(UNF))
                                        //                    Business.ComplianceManagement.CreateUserNotification(UNF);
                                        //                else
                                        //                    Business.ComplianceManagement.UpdateNotification(newNotification);
                                        //            });
                                        //        }
                                        //    }
                                        //}
                                        #endregion
                                    }

                                    #region Compliance Mapping
                                    List<long> ComplianceIds = new List<long>();
                                    ComplianceIds.Clear();
                                    Business.DailyUpdateManagment.RemoveDailyUpdateComplianceMapping(Convert.ToInt32(eventData.ID));
                                    var schemeid1 = Convert.ToInt64(ddlScheme.SelectedValue);
                                    foreach (RepeaterItem aItem in rptComplianceList.Items)
                                    {
                                        CheckBox chkCompliance = (CheckBox)aItem.FindControl("chkCompliance");
                                        if (chkCompliance.Checked)
                                        {
                                            DailyUpdateComplianceMapping complianceMapping = new DailyUpdateComplianceMapping()
                                            {
                                                DailyUpdateID = eventData.ID,
                                                ComplianceID = Convert.ToInt32(((Label)aItem.FindControl("lblComplianceID")).Text.Trim()),
                                                SchemeID = schemeid1,
                                                IsActive = true,
                                                CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                                                CreatedDate = DateTime.Now
                                            };
                                            //var Actid = Business.DailyUpdateManagment.GetActid(complianceMapping.ComplianceID);
                                            //complianceMapping.ActID = Convert.ToInt32(Actid);
                                            Business.DailyUpdateManagment.CreateDailyUpdateComplianceMapping(complianceMapping);
                                        }
                                    }
                                    #endregion


                                    #region Category and State 
                                    //Dailyupdate add Category
                                    Business.DailyUpdateManagment.RemoveDailyUpdateCategoryMapping(Convert.ToInt32(eventData.ID));
                                    foreach (RepeaterItem aItem in rptCategory.Items)
                                    {
                                        CheckBox chkCategory = (CheckBox)aItem.FindControl("chkCategory");
                                        if (chkCategory.Checked)
                                        {
                                            DailyUpdate_CategoryMapping categoryMapping = new DailyUpdate_CategoryMapping()
                                            {
                                                CategoryID = Convert.ToInt32(((Label)aItem.FindControl("lblCategoryID")).Text.Trim()),
                                                DailyUpdateID = eventData.ID,
                                                IsDeleted = false,
                                                CreateOn = DateTime.Now,
                                            };
                                            Business.DailyUpdateManagment.CreateDailyUpdateCategoryMapping(categoryMapping);
                                        }
                                    }

                                    //Dailyupdate add State
                                    Business.DailyUpdateManagment.RemoveDailyUpdateStateMapping(Convert.ToInt32(eventData.ID));
                                    foreach (RepeaterItem aItem in rptState.Items)
                                    {
                                        CheckBox chkState = (CheckBox)aItem.FindControl("chkState");
                                        if (chkState.Checked)
                                        {
                                            DailyUpdate_StateMapping stateMapping = new DailyUpdate_StateMapping()
                                            {
                                                StateID = Convert.ToInt32(((Label)aItem.FindControl("lblStateID")).Text.Trim()),
                                                DailyUpdateID = eventData.ID,
                                                IsDeleted = false,
                                                CreateOn = DateTime.Now,
                                            };
                                            Business.DailyUpdateManagment.CreateDailyUpdateStateMapping(stateMapping);
                                        }
                                    }
                                    #endregion

                                    Business.DailyUpdateManagment.RemoveDailyUpdateIndustryMapping(Convert.ToInt32(eventData.ID));
                                    List<DailyUpdateIndustryMapping> mappingInustries = new List<DailyUpdateIndustryMapping>();
                                    foreach (RepeaterItem aItem in rptIndustry.Items)
                                    {
                                        CheckBox chkIndustry = (CheckBox)aItem.FindControl("chkIndustry");

                                        if (chkIndustry.Checked)
                                        {
                                            DailyUpdateIndustryMapping IndustryMapping = new DailyUpdateIndustryMapping()
                                            {
                                                DailyUpdateID = eventData.ID,
                                                IndustryID = Convert.ToInt32(((Label)aItem.FindControl("lblIndustryID")).Text.Trim()),
                                                IsActive = true,
                                                CreatedDate = DateTime.Now,
                                                CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                                            };
                                            Business.DailyUpdateManagment.CreateDailyUpdateIndustryMapping(IndustryMapping);
                                        }
                                    }

                                    if (fudocument.FileBytes != null && fudocument.FileBytes.LongLength > 0)
                                    {
                                        DailyUpdateDocument dailyUpdateDocument = null;
                                        //Add Document
                                        try
                                        {
                                            Stream fileStream = fudocument.PostedFile.InputStream;
                                            using (fileStream)
                                            {

                                                //string containerName = ContainerPrefix;
                                                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ConfigurationManager.AppSettings["StorageConnectionString"].ToString());
                                                CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

                                                var directoryPath = "legalupdatedocs/" + eventData.ID;
                                                CloudBlobContainer container = blobClient.GetContainerReference(directoryPath);
                                                container.CreateIfNotExistsAsync();

                                                //CloudBlobContainer container = blobClient.GetContainerReference(containerName);

                                                CloudBlockBlob blockBlob = container.GetBlockBlobReference(fudocument.FileName);
                                                //using (var fileStream = fudocument.PostedFile.InputStream)

                                                //using (fileStream)
                                                //{
                                                blockBlob.UploadFromStream(fileStream);
                                                //}

                                                Business.DailyUpdateManagment.RemoveDailyUpdateDocument(Convert.ToInt32(eventData.ID));
                                                dailyUpdateDocument = new DailyUpdateDocument()
                                                {
                                                    DailyUpdateID = eventData.ID,
                                                    Name = fudocument.FileName,
                                                    CreatedOn = DateTime.Now,
                                                    UpdatedOn = DateTime.Now,
                                                    FileData = fudocument.FileBytes,
                                                    FilePath = "legalupdatedocs/" + eventData.ID + "/" + fudocument.FileName,
                                                };
                                                Business.DailyUpdateManagment.CreateDailyUpdateDocument(dailyUpdateDocument);


                                                //Act Document Mapping 
                                                if (IsOtherSelected == true)  //Scheme Selected
                                                {
                                                    #region file upload
                                                    Business.ComplianceManagement.RemoveActDocument(Convert.ToInt32(eventData.ID));
                                                    var SchemeMappedActList = ActManagement.GetSchemeMappedActIDs(Convert.ToInt32(ddlScheme.SelectedValue));
                                                    HttpFileCollection fileCollection = Request.Files;
                                                    int ActDocType = -1;
                                                    if (!string.IsNullOrEmpty(ddlActDocType.SelectedValue))
                                                    {
                                                        if (ddlActDocType.SelectedValue != "-1")
                                                        {
                                                            ActDocType = Convert.ToInt32(ddlActDocType.SelectedValue);
                                                        }
                                                    }

                                                    foreach (var aItem in SchemeMappedActList)
                                                    {

                                                        int count = 1;
                                                        for (int i = 0; i < fileCollection.Count; i++)
                                                        {
                                                            HttpPostedFile uploadfile = null;
                                                            uploadfile = fileCollection[i];
                                                            string fileName = uploadfile.FileName;
                                                            string actdirectoryPath = null;
                                                            Act_Document act_Document = null;
                                                            if (!string.IsNullOrEmpty(fileName))
                                                            {
                                                                string version = string.Empty;
                                                                version = ActManagement.GetFileVersion((int)aItem);

                                                                if (!string.IsNullOrEmpty(version))
                                                                {
                                                                    version = version.Substring(0, version.IndexOf("."));
                                                                    Int32.TryParse(version, out count);
                                                                    count++;
                                                                }
                                                                version = Convert.ToString(count) + ".0";

                                                                actdirectoryPath = Server.MapPath("~/ActFiles/" + Convert.ToString(aItem) + "/" + version);

                                                                DocumentManagement.CreateDirectory(actdirectoryPath);
                                                                string finalPath = Path.Combine(actdirectoryPath, uploadfile.FileName);
                                                                finalPath = finalPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");

                                                                fileCollection[i].SaveAs(Server.MapPath(finalPath));

                                                                Stream fs = uploadfile.InputStream;
                                                                BinaryReader br = new BinaryReader(fs);
                                                                Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                                                                DateTime? dtVerstion = null;
                                                                if (txtActDocVerstionDate.Text != "")
                                                                {
                                                                    dtVerstion = Convert.ToDateTime(txtActDocVerstionDate.Text);
                                                                }
                                                                act_Document = new Act_Document()
                                                                {
                                                                    Act_ID = (int)aItem,
                                                                    FileName = fileCollection[i].FileName,
                                                                    //FileData = bytes,
                                                                    FilePath = finalPath,
                                                                    IsActive = true,
                                                                    CreatedOn = DateTime.Now,
                                                                    Createdby = AuthenticationHelper.UserID,
                                                                    UpdatedOn = DateTime.Now,
                                                                    Updatedby = AuthenticationHelper.UserID,
                                                                    Version = version,
                                                                    VersionDate = DateTime.Now,
                                                                    Act_TypeID = ActDocType,
                                                                    Act_TypeVersionDate = dtVerstion,
                                                                    DailyUpdateID = eventData.ID,
                                                                };
                                                                var UploadActdata = ActManagement.SaveActFiledata(act_Document);
                                                                count++;
                                                            }
                                                        }
                                                    }
                                                    #endregion
                                                }
                                                else  //Act Selected
                                                {
                                                    #region file upload
                                                    List<long> ActIds = new List<long>();
                                                    ActIds.Clear();
                                                    Business.ComplianceManagement.RemoveActDocument(Convert.ToInt32(eventData.ID));
                                                    //var SchemeMappedActList = ActManagement.GetSchemeMappedActIDs(Convert.ToInt32(ddlScheme.SelectedValue));
                                                    HttpFileCollection fileCollection = Request.Files;
                                                    int ActDocType = -1;
                                                    if (!string.IsNullOrEmpty(ddlActDocType.SelectedValue))
                                                    {
                                                        if (ddlActDocType.SelectedValue != "-1")
                                                        {
                                                            ActDocType = Convert.ToInt32(ddlActDocType.SelectedValue);
                                                        }
                                                    }
                                                    foreach (RepeaterItem aItem in rptActList.Items)
                                                    {
                                                        CheckBox chkAct = (CheckBox)aItem.FindControl("chkAct");
                                                        if (chkAct.Checked)
                                                        {
                                                            int count = 1;
                                                            for (int i = 0; i < fileCollection.Count; i++)
                                                            {
                                                                HttpPostedFile uploadfile = null;
                                                                uploadfile = fileCollection[i];
                                                                string fileName = uploadfile.FileName;
                                                                string actdirectoryPath = null;
                                                                Act_Document act_Document = null;
                                                                if (!string.IsNullOrEmpty(fileName))
                                                                {
                                                                    string version = string.Empty;
                                                                    version = ActManagement.GetFileVersion(Convert.ToInt32(((Label)aItem.FindControl("lblActID")).Text.Trim()));

                                                                    if (!string.IsNullOrEmpty(version))
                                                                    {
                                                                        version = version.Substring(0, version.IndexOf("."));
                                                                        Int32.TryParse(version, out count);
                                                                        count++;
                                                                    }
                                                                    version = Convert.ToString(count) + ".0";

                                                                    actdirectoryPath = Server.MapPath("~/ActFiles/" + Convert.ToString(((Label)aItem.FindControl("lblActID")).Text.Trim()) + "/" + version);

                                                                    DocumentManagement.CreateDirectory(actdirectoryPath);
                                                                    string finalPath = Path.Combine(actdirectoryPath, uploadfile.FileName);
                                                                    finalPath = finalPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");

                                                                    fileCollection[i].SaveAs(Server.MapPath(finalPath));

                                                                    Stream fs = uploadfile.InputStream;
                                                                    BinaryReader br = new BinaryReader(fs);
                                                                    Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                                                                    DateTime? dtVerstion = null;
                                                                    if (txtActDocVerstionDate.Text != "")
                                                                    {
                                                                        dtVerstion = Convert.ToDateTime(txtActDocVerstionDate.Text);
                                                                    }
                                                                    act_Document = new Act_Document()
                                                                    {
                                                                        Act_ID = Convert.ToInt32(((Label)aItem.FindControl("lblActID")).Text.Trim()),
                                                                        FileName = fileCollection[i].FileName,
                                                                        //FileData = bytes,
                                                                        FilePath = finalPath,
                                                                        IsActive = true,
                                                                        CreatedOn = DateTime.Now,
                                                                        Createdby = AuthenticationHelper.UserID,
                                                                        UpdatedOn = DateTime.Now,
                                                                        Updatedby = AuthenticationHelper.UserID,
                                                                        Version = version,
                                                                        VersionDate = DateTime.Now,
                                                                        Act_TypeID = ActDocType,
                                                                        Act_TypeVersionDate = dtVerstion,
                                                                        DailyUpdateID = eventData.ID,
                                                                    };
                                                                    var UploadActdata = ActManagement.SaveActFiledata(act_Document);
                                                                    count++;
                                                                }
                                                            }
                                                        }
                                                    }
                                                    #endregion
                                                }
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                        }
                                    }
                                    Response.Redirect("~/DailyUpdates/FrmDailyUpdates.aspx", false);
                                }
                                else
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Please select atleast one state";
                                }
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Please select atleast one category";
                            }
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please select Acts or Scheme option";
                        }
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Please select atleast one act or Scheme option";
                    }
                }
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Please remove special charactors in uploaded file(*|,\":<>[]{}`\';()@&$#%)";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btndocumentDownload_Click(object sender, EventArgs e)
        {

                ReadDocFilesAzureFiles(Container, FileName);
        }

        public static void ReadDocFilesAzureFiles(string Container, string FileName)
        {
            try
            {
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ConfigurationManager.AppSettings["StorageConnectionString"].ToString());
                CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

                CloudBlobContainer container = blobClient.GetContainerReference(Container);
                CloudBlockBlob blockBlob = container.GetBlockBlobReference(FileName);
                MemoryStream memStream = new MemoryStream();
                blockBlob.DownloadToStream(memStream);

                HttpContext.Current.Response.ContentType = blockBlob.Properties.ContentType.ToString();
                HttpContext.Current.Response.AddHeader("Content-Disposition", "Attachment; filename=" + FileName);
                HttpContext.Current.Response.BinaryWrite(memStream.ToArray());
                HttpContext.Current.Response.Flush();
                HttpContext.Current.Response.Close();
                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.


            }
            catch (Exception)
            {
            }

        }

        protected void btnSavePublish_Click(object sender, EventArgs e)
        {
            try
            {
                int cnt = 0;
                foreach (RepeaterItem aItem in rptActList.Items)
                {
                    CheckBox chkAct = (CheckBox)aItem.FindControl("chkAct");
                    if (chkAct.Checked == true)
                    {
                        cnt += 1;
                        break;
                    }
                }


                Boolean ActSelected = false;
                foreach (RepeaterItem aItem in rptActList.Items)
                {
                    CheckBox chkAct = (CheckBox)aItem.FindControl("chkAct");
                    if (chkAct.Checked == true)
                    {
                        if (((Label)aItem.FindControl("lblActID")).Text.Trim() != "-1")
                        {
                            ActSelected = true;
                            break;
                        }
                    }
                }

                if (IsMiscellaneous.Checked == true)
                {
                    cnt += 1;
                    ActSelected = true;
                }

                Boolean SchemeSelected = false;
                foreach (RepeaterItem aItem in rptActList.Items)
                {
                    CheckBox chkAct = (CheckBox)aItem.FindControl("chkAct");
                    if (chkAct.Checked == true)
                    {
                        if (((Label)aItem.FindControl("lblActID")).Text.Trim() == "-1")
                        {
                            SchemeSelected = true;
                            break;
                        }
                    }
                }

                Boolean CategorySelected = false;
                foreach (RepeaterItem aItem in rptCategory.Items)
                {
                    CheckBox chkCategory = (CheckBox)aItem.FindControl("chkCategory");
                    if (chkCategory.Checked == true)
                    {
                        if (((Label)aItem.FindControl("lblCategoryID")).Text.Trim() != "-1")
                        {
                            CategorySelected = true;
                            break;
                        }
                    }
                }

                Boolean StateSelected = false;
                string dailyupdateType = "";
                //int stateselectcount = 0;
                if (ddlDailyUpdateType.SelectedValue == "2")
                {
                    dailyupdateType = "S";
                    foreach (RepeaterItem aItem in rptState.Items)
                    {
                        CheckBox chkState = (CheckBox)aItem.FindControl("chkState");
                        if (chkState.Checked == true)
                        {
                            //stateselectcount += 1;
                            if (((Label)aItem.FindControl("lblStateID")).Text.Trim() != "-1")
                            {
                                StateSelected = true;
                            }
                        }
                    }
                }
                else if (ddlDailyUpdateType.SelectedValue == "1")
                {
                    //stateselectcount = 1;
                    dailyupdateType = "C";
                    StateSelected = true;
                }

                int MainSub = -1;
                if (ddlMainSub.SelectedValue == "1")
                {
                    MainSub = 1;
                }
                else if (ddlMainSub.SelectedValue == "2")
                {
                    MainSub = 2;
                }
                else
                {
                    MainSub = 0;
                }

                int regulator = 0;
                if (ddlRegulator.SelectedValue != "-1")
                {
                    regulator = Convert.ToInt32(ddlRegulator.SelectedValue);
                }
                int ministry = 0;
                if (ddlMinistry.SelectedValue != "-1")
                {
                    ministry = Convert.ToInt32(ddlMinistry.SelectedValue);
                }

                int DeptID = 0;
                if (ddlDepartment.SelectedValue != "-1")
                {
                    DeptID = Convert.ToInt32(ddlDepartment.SelectedValue);
                }


                bool IsflagSpecialChar = false;
                if (fudocument.FileBytes != null && fudocument.FileBytes.LongLength > 0)
                {
                    var splChars = "*|,\":<>[]{}`\';()@&$#%";
                    var filename = fudocument.FileName;
                    foreach (char cha in filename)
                    {
                        if (splChars.Contains(cha))
                        {
                            IsflagSpecialChar = true;
                            break;
                        }
                        else
                        {
                            IsflagSpecialChar = false;
                        }
                    }

                }

                if (IsflagSpecialChar == false)
                {
                    if (cnt > 0)
                    {
                        if ((SchemeSelected == false && ActSelected == true) || (SchemeSelected == true && ActSelected == false))
                        {
                            if (CategorySelected == true)
                            {
                                if (StateSelected == true)
                                {
                                    DailyUpdate eventData = new DailyUpdate()
                                    {
                                        Title = txtTitle.Text,
                                        SubTitle = txtSubTitle.Text,
                                        Description = txtDescription.Text,
                                        SchemeID = Convert.ToInt64(ddlScheme.SelectedValue),
                                        //CategoryID = Convert.ToInt32(ddlCategory.SelectedValue),
                                        Link = txtLink.Text,
                                        SelectAll = chkAll.Checked,
                                        CreatedBy = AuthenticationHelper.UserID,
                                        UpdatedBy = AuthenticationHelper.UserID,
                                        CreatedDate = DateTime.Now.Date,
                                        UpdatedDate = DateTime.Now.Date,
                                        IsDeleted = false,
                                        type = dailyupdateType,
                                        IsActive = true,
                                        MainFlag = MainSub,
                                        MetaKeyWord = txtMetaKeyword.Text,
                                        MetaDesc = txtMetaDesc.Text,
                                        DocKeyword = txtDocKeyword.Text,
                                        IsMiscellaneous = IsMiscellaneous.Checked,
                                        RegulatorID = regulator,
                                        MinistryID = ministry,
                                        DeptID = DeptID,
                                        IsTranslate = chkIsTranslate.Checked,
                                        IsCovid = chkIsCovid.Checked,
                                    };

                                    if ((int)ViewState["Mode"] == 1)
                                    {
                                        eventData.ID = Convert.ToInt32(ViewState["dailyUpdateID"]);
                                    }

                                    if ((int)ViewState["Mode"] == 0)
                                    {
                                        if (EventManagement.ExistsDailyUpdate(eventData.Title))
                                        {
                                            cvDuplicateEntry.IsValid = false;
                                            cvDuplicateEntry.ErrorMessage = "Title name already exists.";
                                            upEventList.Update();
                                            return;
                                        }
                                        DailyUpdateManagment.CreateDailyUpdate(eventData);
                                        eventData.ID = eventData.ID;
                                    }
                                    else if ((int)ViewState["Mode"] == 1)
                                    {
                                        DailyUpdateManagment.UpdateDailyUpdate(eventData);
                                    }
                                    Boolean IsOtherSelected = false;
                                    foreach (RepeaterItem aItem in rptActList.Items)
                                    {
                                        CheckBox chkAct = (CheckBox)aItem.FindControl("chkAct");
                                        if (chkAct.Checked == true)
                                        {
                                            if (((Label)aItem.FindControl("lblActID")).Text.Trim() == "-1")
                                            {
                                                IsOtherSelected = true;
                                            }
                                        }
                                    }

                                    // Add ActMapping to DailyUpdate
                                    if (IsOtherSelected == true)
                                    {
                                        //For Other Option Selected
                                        List<int> ParentEventIds = new List<int>();
                                        Business.ComplianceManagement.RemoveDailyUpdateActMapping(Convert.ToInt32(eventData.ID));
                                        var SchemeMappedActList = ActManagement.GetSchemeMappedActIDs(Convert.ToInt32(ddlScheme.SelectedValue));
                                        foreach (var aItem in SchemeMappedActList)
                                        {
                                            var schemeid = Convert.ToInt64(ddlScheme.SelectedValue);
                                            DailyUpdateActMapping actMapping = new DailyUpdateActMapping()
                                            {
                                                ActID = (int)aItem,
                                                DailyUpdateID = (int)eventData.ID,
                                                SchemeID = schemeid,
                                                IsActive = true,
                                                CreatedDate = DateTime.Now,
                                                CreatedBy = AuthenticationHelper.UserID,
                                            };
                                            Business.ComplianceManagement.CreateDailyUpdateActMapping(actMapping);
                                        }
                                        #region Daily Update Notification
                                        if ((int)ViewState["Mode"] == 0)
                                        {
                                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                            {
                                                //Get All Users reasarch Act  Assigned 
                                                List<long> ListUsers = ActManagement.GetResearchMappedDetails(SchemeMappedActList);
                                                ListUsers = ListUsers.Distinct().ToList();
                                                List<string> details = new List<string>();
                                                if (ListUsers.Count > 0)
                                                {
                                                    details.Clear();
                                                    foreach (var item in ListUsers)
                                                    {
                                                        var deviceIDList = (from row in entities.MaintainDeviceIDs
                                                                            where row.Product.Equals("L") && row.UserID == item && row.Status.Equals("Active")
                                                                            select row.DeviceToken).FirstOrDefault();

                                                        if (!string.IsNullOrEmpty(deviceIDList))
                                                        {
                                                            details.Add(deviceIDList);
                                                        }
                                                    }
                                                    // details.Add("dVC3g5uLZiA:APA91bG20O-A5OnAPh9Xgq_BY02gFYfRA0FQzQliMxIlVs011Y2CG2CfIDphE0Vx5nGwyKYj0HlOnQF84uRYG_2NuxF1naeKkKqiUBZiZcs6ZH1R-K_M466CB1qoMSWhvNSEnK7vIFiq");
                                                    details = details.Where(x => x != "null").ToList();
                                                    string[] arr1 = details.Select(i => i.ToString()).ToArray();
                                                    if (arr1.Length > 0)
                                                    {
                                                        string chk = System.Text.RegularExpressions.Regex.Replace(txtTitle.Text, "<[^>]*>", "");
                                                        NotificationManagement.SendNotificationDailyUpdateMobile(arr1, chk, "Daily Updates");
                                                    }
                                                    details.Clear();
                                                    details = null;
                                                    long NotificationID = 0;
                                                    // Remark = System.Text.RegularExpressions.Regex.Replace(txtDescription.Text, "<[^>]*>", ""),
                                                    com.VirtuosoITech.ComplianceManagement.Business.Data.Notification newNotification = new com.VirtuosoITech.ComplianceManagement.Business.Data.Notification()
                                                    {
                                                        ActID = 0,
                                                        ComplianceID = eventData.ID,
                                                        Type = "DailyUpdate",
                                                        Remark = txtDescription.Text,
                                                        CreatedBy = AuthenticationHelper.UserID,
                                                        CreatedOn = DateTime.Now.Date,
                                                        UpdatedBy = AuthenticationHelper.UserID,
                                                        UpdatedOn = DateTime.Now.Date,
                                                    };
                                                    Business.ComplianceManagement.CreateNotification(newNotification);
                                                    NotificationID = newNotification.ID;
                                                    if (NotificationID != 0)
                                                    {
                                                        ListUsers.ForEach(EachUser =>
                                                        {
                                                            UserNotification UNF = new UserNotification()
                                                            {
                                                                NotificationID = NotificationID,
                                                                UserID = Convert.ToInt32(EachUser),
                                                                IsRead = false,
                                                            };

                                                            if (!Business.ComplianceManagement.ExistsUserNotification(UNF))
                                                                Business.ComplianceManagement.CreateUserNotification(UNF);
                                                            else
                                                                Business.ComplianceManagement.UpdateNotification(newNotification);
                                                        });
                                                    }
                                                }
                                            }
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        //For Multiple Acts Selected
                                        List<long> ActIds = new List<long>();
                                        ActIds.Clear();
                                        Business.ComplianceManagement.RemoveDailyUpdateActMapping(Convert.ToInt32(eventData.ID));
                                        foreach (RepeaterItem aItem in rptActList.Items)
                                        {
                                            CheckBox chkCompliance = (CheckBox)aItem.FindControl("chkAct");
                                            if (chkCompliance.Checked)
                                            {
                                                var schemeid = Convert.ToInt64(ddlScheme.SelectedValue);
                                                ActIds.Add(Convert.ToInt32(((Label)aItem.FindControl("lblActID")).Text.Trim()));
                                                DailyUpdateActMapping actMapping = new DailyUpdateActMapping()
                                                {
                                                    ActID = Convert.ToInt32(((Label)aItem.FindControl("lblActID")).Text.Trim()),
                                                    DailyUpdateID = eventData.ID,
                                                    SchemeID = schemeid,
                                                    IsActive = true,
                                                    CreatedDate = DateTime.Now,
                                                    CreatedBy = Convert.ToInt32(Session["userID"]),
                                                };
                                                Business.ComplianceManagement.CreateDailyUpdateActMapping(actMapping);
                                            }
                                        }

                                        #region Daily Update Notification 
                                        if ((int)ViewState["Mode"] == 0)
                                        {
                                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                            {  //Get All Users reasarch Act  Assigned 
                                                List<long> ListUsers = ActManagement.GetResearchMappedDetails(ActIds);
                                                ListUsers = ListUsers.Distinct().ToList();
                                                List<string> details = new List<string>();
                                                if (ListUsers.Count > 0)
                                                {
                                                    details.Clear();
                                                    foreach (var item in ListUsers)
                                                    {
                                                        var deviceIDList = (from row in entities.MaintainDeviceIDs
                                                                            where row.Product.Equals("L") && row.UserID == item && row.Status.Equals("Active")
                                                                            select row.DeviceToken).FirstOrDefault();

                                                        if (!string.IsNullOrEmpty(deviceIDList))
                                                        {
                                                            details.Add(deviceIDList);
                                                        }
                                                    }
                                                    // details.Add("dVC3g5uLZiA:APA91bG20O-A5OnAPh9Xgq_BY02gFYfRA0FQzQliMxIlVs011Y2CG2CfIDphE0Vx5nGwyKYj0HlOnQF84uRYG_2NuxF1naeKkKqiUBZiZcs6ZH1R-K_M466CB1qoMSWhvNSEnK7vIFiq");
                                                    details = details.Where(x => x != "null").ToList();
                                                    string[] arr1 = details.Select(i => i.ToString()).ToArray();
                                                    if (arr1.Length > 0)
                                                    {
                                                        string chk = System.Text.RegularExpressions.Regex.Replace(txtTitle.Text, "<[^>]*>", "");
                                                        NotificationManagement.SendNotificationDailyUpdateMobile(arr1, chk, "Daily Updates");
                                                    }
                                                    details.Clear();
                                                    details = null;

                                                    long NotificationID = 0;
                                                    com.VirtuosoITech.ComplianceManagement.Business.Data.Notification newNotification = new com.VirtuosoITech.ComplianceManagement.Business.Data.Notification()
                                                    {
                                                        ActID = 0,
                                                        ComplianceID = eventData.ID,
                                                        Type = "DailyUpdate",
                                                        Remark = System.Text.RegularExpressions.Regex.Replace(txtDescription.Text, "<[^>]*>", ""),
                                                        CreatedBy = AuthenticationHelper.UserID,
                                                        CreatedOn = DateTime.Now.Date,
                                                        UpdatedBy = AuthenticationHelper.UserID,
                                                        UpdatedOn = DateTime.Now.Date,
                                                    };
                                                    Business.ComplianceManagement.CreateNotification(newNotification);
                                                    NotificationID = newNotification.ID;
                                                    if (NotificationID != 0)
                                                    {
                                                        ListUsers.ForEach(EachUser =>
                                                        {
                                                            UserNotification UNF = new UserNotification()
                                                            {
                                                                NotificationID = NotificationID,
                                                                UserID = Convert.ToInt32(EachUser),
                                                                IsRead = false,
                                                            };

                                                            if (!Business.ComplianceManagement.ExistsUserNotification(UNF))
                                                                Business.ComplianceManagement.CreateUserNotification(UNF);
                                                            else
                                                                Business.ComplianceManagement.UpdateNotification(newNotification);
                                                        });
                                                    }
                                                }
                                            }
                                        }
                                        #endregion


                                    }

                                    #region Category and State 
                                    //Dailyupdate add Category
                                    Business.DailyUpdateManagment.RemoveDailyUpdateCategoryMapping(Convert.ToInt32(eventData.ID));
                                    foreach (RepeaterItem aItem in rptCategory.Items)
                                    {
                                        CheckBox chkCategory = (CheckBox)aItem.FindControl("chkCategory");
                                        if (chkCategory.Checked)
                                        {
                                            DailyUpdate_CategoryMapping categoryMapping = new DailyUpdate_CategoryMapping()
                                            {
                                                CategoryID = Convert.ToInt32(((Label)aItem.FindControl("lblCategoryID")).Text.Trim()),
                                                DailyUpdateID = eventData.ID,
                                                IsDeleted = false,
                                                CreateOn = DateTime.Now,
                                            };
                                            Business.DailyUpdateManagment.CreateDailyUpdateCategoryMapping(categoryMapping);
                                        }
                                    }

                                    //Dailyupdate add State
                                    Business.DailyUpdateManagment.RemoveDailyUpdateStateMapping(Convert.ToInt32(eventData.ID));
                                    foreach (RepeaterItem aItem in rptState.Items)
                                    {
                                        CheckBox chkState = (CheckBox)aItem.FindControl("chkState");
                                        if (chkState.Checked)
                                        {
                                            DailyUpdate_StateMapping stateMapping = new DailyUpdate_StateMapping()
                                            {
                                                StateID = Convert.ToInt32(((Label)aItem.FindControl("lblStateID")).Text.Trim()),
                                                DailyUpdateID = eventData.ID,
                                                IsDeleted = false,
                                                CreateOn = DateTime.Now,
                                            };
                                            Business.DailyUpdateManagment.CreateDailyUpdateStateMapping(stateMapping);
                                        }
                                    }
                                    #endregion

                                    #region Compliance Mapping
                                    List<long> ComplianceIds = new List<long>();
                                    ComplianceIds.Clear();
                                    Business.DailyUpdateManagment.RemoveDailyUpdateComplianceMapping(Convert.ToInt32(eventData.ID));
                                    var schemeid1 = Convert.ToInt64(ddlScheme.SelectedValue);
                                    foreach (RepeaterItem aItem in rptComplianceList.Items)
                                    {
                                        CheckBox chkCompliance = (CheckBox)aItem.FindControl("chkCompliance");
                                        if (chkCompliance.Checked)
                                        {
                                            DailyUpdateComplianceMapping complianceMapping = new DailyUpdateComplianceMapping()
                                            {
                                                DailyUpdateID = eventData.ID,
                                                ComplianceID = Convert.ToInt32(((Label)aItem.FindControl("lblComplianceID")).Text.Trim()),
                                                SchemeID = schemeid1,
                                                IsActive = true,
                                                CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                                                CreatedDate = DateTime.Now
                                            };
                                            //var Actid = Business.DailyUpdateManagment.GetActid(complianceMapping.ComplianceID);
                                            //complianceMapping.ActID = Convert.ToInt32(Actid);
                                            Business.DailyUpdateManagment.CreateDailyUpdateComplianceMapping(complianceMapping);
                                        }
                                    }
                                    #endregion


                                    Business.DailyUpdateManagment.RemoveDailyUpdateIndustryMapping(Convert.ToInt32(eventData.ID));
                                    List<DailyUpdateIndustryMapping> mappingInustries = new List<DailyUpdateIndustryMapping>();
                                    foreach (RepeaterItem aItem in rptIndustry.Items)
                                    {
                                        CheckBox chkIndustry = (CheckBox)aItem.FindControl("chkIndustry");

                                        if (chkIndustry.Checked)
                                        {
                                            DailyUpdateIndustryMapping IndustryMapping = new DailyUpdateIndustryMapping()
                                            {
                                                DailyUpdateID = eventData.ID,
                                                IndustryID = Convert.ToInt32(((Label)aItem.FindControl("lblIndustryID")).Text.Trim()),
                                                IsActive = true,
                                                CreatedDate = DateTime.Now,
                                                CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                                            };
                                            Business.DailyUpdateManagment.CreateDailyUpdateIndustryMapping(IndustryMapping);
                                        }
                                    }

                                    if (fudocument.FileBytes != null && fudocument.FileBytes.LongLength > 0)
                                    {
                                        DailyUpdateDocument dailyUpdateDocument = null;
                                        //Add Document
                                        try
                                        {
                                            Stream fileStream = fudocument.PostedFile.InputStream;
                                            using (fileStream)
                                            {
                                                //string containerName = ContainerPrefix;
                                                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ConfigurationManager.AppSettings["StorageConnectionString"].ToString());
                                                CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

                                                var directoryPath = "legalupdatedocs/" + eventData.ID;
                                                CloudBlobContainer container = blobClient.GetContainerReference(directoryPath);
                                                container.CreateIfNotExistsAsync();

                                                //CloudBlobContainer container = blobClient.GetContainerReference(containerName);

                                                CloudBlockBlob blockBlob = container.GetBlockBlobReference(fudocument.FileName);
                                                //using (var fileStream = fudocument.PostedFile.InputStream)
                                                //{
                                                blockBlob.UploadFromStream(fileStream);
                                                //}

                                                Business.DailyUpdateManagment.RemoveDailyUpdateDocument(Convert.ToInt32(eventData.ID));
                                                dailyUpdateDocument = new DailyUpdateDocument()
                                                {
                                                    DailyUpdateID = eventData.ID,
                                                    Name = fudocument.FileName,
                                                    CreatedOn = DateTime.Now,
                                                    UpdatedOn = DateTime.Now,
                                                    FileData = fudocument.FileBytes,
                                                    FilePath = "legalupdatedocs/" + eventData.ID + "/" + fudocument.FileName,
                                                };
                                                Business.DailyUpdateManagment.CreateDailyUpdateDocument(dailyUpdateDocument);

                                                //Act Document Mapping 
                                                if (IsOtherSelected == true)  //Scheme Selected
                                                {
                                                    #region file upload
                                                    Business.ComplianceManagement.RemoveActDocument(Convert.ToInt32(eventData.ID));
                                                    var SchemeMappedActList = ActManagement.GetSchemeMappedActIDs(Convert.ToInt32(ddlScheme.SelectedValue));
                                                    HttpFileCollection fileCollection = Request.Files;
                                                    int ActDocType = -1;
                                                    if (!string.IsNullOrEmpty(ddlActDocType.SelectedValue))
                                                    {
                                                        if (ddlActDocType.SelectedValue != "-1")
                                                        {
                                                            ActDocType = Convert.ToInt32(ddlActDocType.SelectedValue);
                                                        }
                                                    }

                                                    foreach (var aItem in SchemeMappedActList)
                                                    {

                                                        int count = 1;
                                                        for (int i = 0; i < fileCollection.Count; i++)
                                                        {
                                                            HttpPostedFile uploadfile = null;
                                                            uploadfile = fileCollection[i];
                                                            string fileName = uploadfile.FileName;
                                                            string actdirectoryPath = null;
                                                            Act_Document act_Document = null;
                                                            if (!string.IsNullOrEmpty(fileName))
                                                            {
                                                                string version = string.Empty;
                                                                version = ActManagement.GetFileVersion((int)aItem);

                                                                if (!string.IsNullOrEmpty(version))
                                                                {
                                                                    version = version.Substring(0, version.IndexOf("."));
                                                                    Int32.TryParse(version, out count);
                                                                    count++;
                                                                }
                                                                version = Convert.ToString(count) + ".0";

                                                                actdirectoryPath = Server.MapPath("~/ActFiles/" + Convert.ToString(aItem) + "/" + version);

                                                                DocumentManagement.CreateDirectory(actdirectoryPath);
                                                                string finalPath = Path.Combine(actdirectoryPath, uploadfile.FileName);
                                                                finalPath = finalPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");

                                                                fileCollection[i].SaveAs(Server.MapPath(finalPath));

                                                                Stream fs = uploadfile.InputStream;
                                                                BinaryReader br = new BinaryReader(fs);
                                                                Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                                                                DateTime? dtVerstion = null;
                                                                if (txtActDocVerstionDate.Text != "")
                                                                {
                                                                    dtVerstion = Convert.ToDateTime(txtActDocVerstionDate.Text);
                                                                }
                                                                act_Document = new Act_Document()
                                                                {
                                                                    Act_ID = (int)aItem,
                                                                    FileName = fileCollection[i].FileName,
                                                                    //FileData = bytes,
                                                                    FilePath = finalPath,
                                                                    IsActive = true,
                                                                    CreatedOn = DateTime.Now,
                                                                    Createdby = AuthenticationHelper.UserID,
                                                                    UpdatedOn = DateTime.Now,
                                                                    Updatedby = AuthenticationHelper.UserID,
                                                                    Version = version,
                                                                    VersionDate = DateTime.Now,
                                                                    Act_TypeID = ActDocType,
                                                                    Act_TypeVersionDate = dtVerstion,
                                                                    DailyUpdateID = eventData.ID,
                                                                };
                                                                var UploadActdata = ActManagement.SaveActFiledata(act_Document);
                                                                count++;
                                                            }
                                                        }
                                                    }
                                                    #endregion
                                                }
                                                else  //Act Selected
                                                {
                                                    #region file upload
                                                    List<long> ActIds = new List<long>();
                                                    ActIds.Clear();
                                                    Business.ComplianceManagement.RemoveActDocument(Convert.ToInt32(eventData.ID));
                                                    //var SchemeMappedActList = ActManagement.GetSchemeMappedActIDs(Convert.ToInt32(ddlScheme.SelectedValue));
                                                    HttpFileCollection fileCollection = Request.Files;
                                                    int ActDocType = -1;
                                                    if (!string.IsNullOrEmpty(ddlActDocType.SelectedValue))
                                                    {
                                                        if (ddlActDocType.SelectedValue != "-1")
                                                        {
                                                            ActDocType = Convert.ToInt32(ddlActDocType.SelectedValue);
                                                        }
                                                    }
                                                    foreach (RepeaterItem aItem in rptActList.Items)
                                                    {
                                                        CheckBox chkAct = (CheckBox)aItem.FindControl("chkAct");
                                                        if (chkAct.Checked)
                                                        {
                                                            int count = 1;
                                                            for (int i = 0; i < fileCollection.Count; i++)
                                                            {
                                                                HttpPostedFile uploadfile = null;
                                                                uploadfile = fileCollection[i];
                                                                string fileName = uploadfile.FileName;
                                                                string actdirectoryPath = null;
                                                                Act_Document act_Document = null;
                                                                if (!string.IsNullOrEmpty(fileName))
                                                                {
                                                                    string version = string.Empty;
                                                                    version = ActManagement.GetFileVersion(Convert.ToInt32(((Label)aItem.FindControl("lblActID")).Text.Trim()));

                                                                    if (!string.IsNullOrEmpty(version))
                                                                    {
                                                                        version = version.Substring(0, version.IndexOf("."));
                                                                        Int32.TryParse(version, out count);
                                                                        count++;
                                                                    }
                                                                    version = Convert.ToString(count) + ".0";

                                                                    actdirectoryPath = Server.MapPath("~/ActFiles/" + Convert.ToString(((Label)aItem.FindControl("lblActID")).Text.Trim()) + "/" + version);

                                                                    DocumentManagement.CreateDirectory(actdirectoryPath);
                                                                    string finalPath = Path.Combine(actdirectoryPath, uploadfile.FileName);
                                                                    finalPath = finalPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");

                                                                    fileCollection[i].SaveAs(Server.MapPath(finalPath));

                                                                    Stream fs = uploadfile.InputStream;
                                                                    BinaryReader br = new BinaryReader(fs);
                                                                    Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                                                                    DateTime? dtVerstion = null;
                                                                    if (txtActDocVerstionDate.Text != "")
                                                                    {
                                                                        dtVerstion = Convert.ToDateTime(txtActDocVerstionDate.Text);
                                                                    }
                                                                    act_Document = new Act_Document()
                                                                    {
                                                                        Act_ID = Convert.ToInt32(((Label)aItem.FindControl("lblActID")).Text.Trim()),
                                                                        FileName = fileCollection[i].FileName,
                                                                        //FileData = bytes,
                                                                        FilePath = finalPath,
                                                                        IsActive = true,
                                                                        CreatedOn = DateTime.Now,
                                                                        Createdby = AuthenticationHelper.UserID,
                                                                        UpdatedOn = DateTime.Now,
                                                                        Updatedby = AuthenticationHelper.UserID,
                                                                        Version = version,
                                                                        VersionDate = DateTime.Now,
                                                                        Act_TypeID = ActDocType,
                                                                        Act_TypeVersionDate = dtVerstion,
                                                                        DailyUpdateID = eventData.ID,
                                                                    };
                                                                    var UploadActdata = ActManagement.SaveActFiledata(act_Document);
                                                                    count++;
                                                                }
                                                            }
                                                        }
                                                    }
                                                    #endregion
                                                }
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                        }
                                    }
                                    Response.Redirect("~/DailyUpdates/FrmDailyUpdates.aspx", false);
                                }
                                else
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Please select atleast one state";
                                }
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Please select atleast one category";
                            }
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please select Acts or Scheme option";
                        }
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Please select atleast one act or Scheme option";
                    }
                }
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Please remove special charactors in uploaded file(*|,\":<>[]{}`\';()@&$#%)";
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/DailyUpdates/FrmDailyUpdates.aspx", false);

        }


        public DataTable GetAllActForDailyUpdate()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var acts = (from row in entities.Acts
                            where row.IsDeleted == false
                            orderby row.Name ascending
                            select new { ID = row.ID, Name = row.Name }).OrderBy(entry => entry.Name).ToList();

                DataTable table = new DataTable();
                table.Columns.Add("ID", typeof(int));
                table.Columns.Add("Name", typeof(string));

                DataRow dr1 = table.NewRow();
                dr1["ID"] = -1;
                dr1["Name"] = "< Scheme >";
                table.Rows.Add(dr1);

                if (acts.Count > 0)
                {
                    foreach (var item in acts)
                    {
                        DataRow dr = table.NewRow();
                        dr["ID"] = item.ID;
                        dr["Name"] = item.Name;
                        table.Rows.Add(dr);
                    }
                }

                

                return table;
            }
        }
        private void BindAct()
        {
            try
            {
                DataTable ActList = GetAllActForDailyUpdate();

                rptActList.DataSource = ActList;
                rptActList.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public DataTable GetAllComplianceFromScheme()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {


                var objlocal = ConfigurationManager.AppSettings["WORKINGDIRECTORY"];
                string cashTimeval = string.Empty;
                string cashActval = string.Empty;
                try
                {
                    List<long> actIDs = new List<long>();
                    var SchemeMappedActList = ActManagement.GetSchemeMappedActIDs(Convert.ToInt32(ddlScheme.SelectedValue));
                    foreach (var aItem in SchemeMappedActList)
                    {
                        actIDs.Add(aItem);
                    }

                    if (objlocal == "Local")
                    {
                        cashTimeval = "dailyupdate_CHE" + AuthenticationHelper.UserID;
                        cashActval = "dailyupdate_CHEAct" + AuthenticationHelper.UserID;
                    }
                    else
                    {
                        cashTimeval = "dailyupdateCHE" + AuthenticationHelper.UserID;
                        cashActval = "dailyupdateAct" + AuthenticationHelper.UserID;
                    }


                    List<SP_GetAllCompliance_Result> compliance = new List<SP_GetAllCompliance_Result>();
                    if (CacheHelper.Exists(cashActval))
                    {
                        try
                        {
                            CacheHelper.Get<List<SP_GetAllCompliance_Result>>(cashActval, out compliance);
                        }
                        catch (Exception ex)
                        {
                            LogLibrary.WriteErrorLog("GET Dailyupdate Error exception :" + cashActval);
                            entities.Database.CommandTimeout = 120;
                            compliance = (from row in entities.SP_GetAllCompliance()
                                          select row).ToList();


                            LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                        }
                    }
                    else
                    {

                        compliance = (from row in entities.SP_GetAllCompliance()
                                      select row).ToList();



                        CacheHelper.Set<List<SP_GetAllCompliance_Result>>(cashActval, compliance);
                        if (CacheHelper.Exists(cashTimeval))
                        {
                            CacheHelper.Remove(cashTimeval);
                            CacheHelper.Set(cashTimeval, DateTime.Now);
                        }
                        else
                        {
                            CacheHelper.Set(cashTimeval, DateTime.Now);
                        }
                    }

                    if (actIDs.Count() > 0)
                    {
                        compliance = compliance.Where(entry => actIDs.Contains(entry.ActID)).OrderBy(entry => entry.ShortDescription).Distinct().ToList();
                        //compliance = compliance.Where(entry => actIDs.Contains((int)entry.ActID)).ToList();
                    }

                    if (txtcompliance.Text != "" && txtcompliance.Text != "<Filter ShortDescription/Section>")
                    {
                        compliance = compliance.Where(entry => entry.ShortDescription.Contains(txtcompliance.Text.Trim())).ToList();
                    }

                    if (txtComplianceIDList.Text != "")
                    {
                        var ComplianceTextboxList = txtComplianceIDList.Text.Split(',').ToList();
                        var ComplianceIDList = ComplianceTextboxList.Select(long.Parse).ToList();

                        if (ComplianceIDList.Count() > 0)
                        {
                            compliance = compliance.Where(entry => ComplianceIDList.Contains((int)entry.ID)).ToList();
                        }
                    }

                    //if (actIDs.Count > 0)
                    //{
                    //    compliance = compliance.Where(entry => actIDs.Contains(entry.ActID)).OrderBy(entry => entry.ShortDescription).Distinct().ToList();
                    //}

                    DataTable table = new DataTable();
                    table.Columns.Add("ID", typeof(int));
                    table.Columns.Add("Name", typeof(string));

                    foreach (var item in compliance)
                    {
                        DataRow dr = table.NewRow();
                        dr["ID"] = item.ID;
                        dr["Name"] = item.ShortDescription;
                        table.Rows.Add(dr);
                    }
                    return table;

                }
                catch (Exception ex)
                {
                    LogLibrary.WriteErrorLog("GET Dailyupdate Compliance Error exception :" + cashActval);
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    return null;
                }
            }
        }


        //public List<SP_GetAllCompliance_Result> GetAllComplianceFromScheme()
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {


        //        var objlocal = ConfigurationManager.AppSettings["WORKINGDIRECTORY"];
        //        string cashTimeval = string.Empty;
        //        string cashActval = string.Empty;
        //        try
        //        {
        //            List<long> actIDs = new List<long>();
        //            var SchemeMappedActList = ActManagement.GetSchemeMappedActIDs(Convert.ToInt32(ddlScheme.SelectedValue));
        //            foreach (var aItem in SchemeMappedActList)
        //            {
        //                actIDs.Add(aItem);
        //            }

        //            if (objlocal == "Local")
        //            {
        //                cashTimeval = "dailyupdate_CHE" + AuthenticationHelper.UserID;
        //                cashActval = "dailyupdate_CHEAct" + AuthenticationHelper.UserID;
        //            }
        //            else
        //            {
        //                cashTimeval = "dailyupdateCHE" + AuthenticationHelper.UserID;
        //                cashActval = "dailyupdateAct" + AuthenticationHelper.UserID;
        //            }


        //            List<SP_GetAllCompliance_Result> compliance = new List<SP_GetAllCompliance_Result>();
        //            if (CacheHelper.Exists(cashActval))
        //            {
        //                try
        //                {
        //                    CacheHelper.Get<List<SP_GetAllCompliance_Result>>(cashActval, out compliance);
        //                }
        //                catch (Exception ex)
        //                {
        //                    LogLibrary.WriteErrorLog("GET Dailyupdate Error exception :" + cashActval);
        //                    entities.Database.CommandTimeout = 120;
        //                    compliance = (from row in entities.SP_GetAllCompliance()
        //                                  select row).ToList();


        //                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //                }
        //            }
        //            else
        //            {

        //                compliance = (from row in entities.SP_GetAllCompliance()
        //                              select row).ToList();



        //                CacheHelper.Set<List<SP_GetAllCompliance_Result>>(cashActval, compliance);
        //                if (CacheHelper.Exists(cashTimeval))
        //                {
        //                    CacheHelper.Remove(cashTimeval);
        //                    CacheHelper.Set(cashTimeval, DateTime.Now);
        //                }
        //                else
        //                {
        //                    CacheHelper.Set(cashTimeval, DateTime.Now);
        //                }
        //            }

        //            if (actIDs.Count() > 0)
        //            {
        //                compliance = compliance.Where(entry => actIDs.Contains(entry.ActID)).OrderBy(entry => entry.ShortDescription).Distinct().ToList();
        //                //compliance = compliance.Where(entry => actIDs.Contains((int)entry.ActID)).ToList();
        //            }

        //            //if (txtcompliance.Text != "" && txtcompliance.Text != "<Filter ShortDescription/Section>")
        //            //{
        //            //    compliance = compliance.Where(entry => entry.ShortDescription.Contains(txtcompliance.Text.Trim())).ToList();
        //            //}

        //            if (txtComplianceIDList.Text != "")
        //            {
        //                var ComplianceTextboxList = txtComplianceIDList.Text.Split(',').ToList();
        //                var ComplianceIDList = ComplianceTextboxList.Select(long.Parse).ToList();

        //                if (ComplianceIDList.Count() > 0)
        //                {
        //                    compliance = compliance.Where(entry => ComplianceIDList.Contains((int)entry.ID)).ToList();
        //                }
        //            }

        //            //if (actIDs.Count > 0)
        //            //{
        //            //    compliance = compliance.Where(entry => actIDs.Contains(entry.ActID)).OrderBy(entry => entry.ShortDescription).Distinct().ToList();
        //            //}

        //            //DataTable table = new DataTable();
        //            //table.Columns.Add("ID", typeof(int));
        //            //table.Columns.Add("Name", typeof(string));

        //            //foreach (var item in compliance)
        //            //{
        //            //    DataRow dr = table.NewRow();
        //            //    dr["ID"] = item.ID;
        //            //    dr["Name"] = item.ShortDescription;
        //            //    table.Rows.Add(dr);
        //            //}
        //            return compliance;

        //        }
        //        catch (Exception ex)
        //        {
        //            LogLibrary.WriteErrorLog("GET Dailyupdate Compliance Error exception :" + cashActval);
        //            LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //            return null;
        //        }                
        //    }
        //}

        public DataTable GetAllComplianceFromAct()
        {


            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var objlocal = ConfigurationManager.AppSettings["WORKINGDIRECTORY"];
                string cashTimeval = string.Empty;
                string cashActval = string.Empty;
                try
                {
                    List<long> actIds = new List<long>();

                    foreach (RepeaterItem aItem in rptActList.Items)
                    {
                        CheckBox chkAct = (CheckBox)aItem.FindControl("chkAct");
                        if (chkAct.Checked)
                        {
                            actIds.Add(Convert.ToInt32(((Label)aItem.FindControl("lblActID")).Text.Trim()));
                        }
                    }
                    if (objlocal == "Local")
                    {
                        cashTimeval = "dailyupdate_CHE" + AuthenticationHelper.UserID;
                        cashActval = "dailyupdate_CHEAct" + AuthenticationHelper.UserID;
                    }
                    else
                    {
                        cashTimeval = "dailyupdateCHE" + AuthenticationHelper.UserID;
                        cashActval = "dailyupdateAct" + AuthenticationHelper.UserID;
                    }

                    List<SP_GetAllCompliance_Result> compliance = new List<SP_GetAllCompliance_Result>();
                    if (CacheHelper.Exists(cashActval))
                    {
                        try
                        {
                            CacheHelper.Get<List<SP_GetAllCompliance_Result>>(cashActval, out compliance);
                        }
                        catch (Exception ex)
                        {
                            LogLibrary.WriteErrorLog("GET Dailyupdate Error exception :" + cashActval);
                            entities.Database.CommandTimeout = 120;
                            compliance = (from row in entities.SP_GetAllCompliance()
                                          select row).ToList();

                            LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                        }
                    }
                    else
                    {
                        compliance = (from row in entities.SP_GetAllCompliance()
                                      select row).ToList();

                        CacheHelper.Set<List<SP_GetAllCompliance_Result>>(cashActval, compliance);
                        if (CacheHelper.Exists(cashTimeval))
                        {
                            CacheHelper.Remove(cashTimeval);
                            CacheHelper.Set(cashTimeval, DateTime.Now);
                        }
                        else
                        {
                            CacheHelper.Set(cashTimeval, DateTime.Now);
                        }
                    }

                    if (actIds.Count() > 0)
                    {
                        //compliance = compliance.Where(entry => actIds.Contains((int)entry.ActID)).ToList();
                        compliance = compliance.Where(entry => actIds.Contains(entry.ActID)).OrderBy(entry => entry.ShortDescription).Distinct().ToList();
                    }

                    if (txtcompliance.Text != "" && txtcompliance.Text != "<Filter ShortDescription/Section>")
                    {
                        compliance = compliance.Where(entry => entry.ShortDescription.ToLower().Contains(txtcompliance.Text.ToLower().Trim())).ToList();
                    }

                    if (txtComplianceIDList.Text != "")
                    {
                        var ComplianceTextboxList = txtComplianceIDList.Text.Split(',').ToList();
                        var ComplianceIDList = ComplianceTextboxList.Select(long.Parse).ToList();

                        if (ComplianceIDList.Count() > 0)
                        {
                            compliance = compliance.Where(entry => ComplianceIDList.Contains((int)entry.ID)).ToList();
                        }
                    }

                    DataTable table = new DataTable();
                    table.Columns.Add("ID", typeof(int));
                    table.Columns.Add("Name", typeof(string));

                    foreach (var item in compliance)
                    {
                        DataRow dr = table.NewRow();
                        dr["ID"] = item.ID;
                        dr["Name"] = item.ShortDescription;
                        table.Rows.Add(dr);
                    }
                    return table;
                }
                catch (Exception ex)
                {
                    LogLibrary.WriteErrorLog("GET Dailyupdate Compliance Error exception :" + cashActval);
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    return null;
                }
            }
        }

        //public List<SP_GetAllCompliance_Result> GetAllComplianceFromAct()
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        var objlocal = ConfigurationManager.AppSettings["WORKINGDIRECTORY"];
        //        string cashTimeval = string.Empty;
        //        string cashActval = string.Empty;
        //        try
        //        {
        //            List<long> actIds = new List<long>();

        //            foreach (RepeaterItem aItem in rptActList.Items)
        //            {
        //                CheckBox chkAct = (CheckBox)aItem.FindControl("chkAct");
        //                if (chkAct.Checked)
        //                {
        //                    actIds.Add(Convert.ToInt32(((Label)aItem.FindControl("lblActID")).Text.Trim()));
        //                }
        //            }
        //            if (objlocal == "Local")
        //            {
        //                cashTimeval = "dailyupdate_CHE" + AuthenticationHelper.UserID;
        //                cashActval = "dailyupdate_CHEAct" + AuthenticationHelper.UserID;
        //            }
        //            else
        //            {
        //                cashTimeval = "dailyupdateCHE" + AuthenticationHelper.UserID;
        //                cashActval = "dailyupdateAct" + AuthenticationHelper.UserID;
        //            }

        //            List<SP_GetAllCompliance_Result> compliance = new List<SP_GetAllCompliance_Result>();
        //            if (CacheHelper.Exists(cashActval))
        //            {
        //                try
        //                {
        //                    CacheHelper.Get<List<SP_GetAllCompliance_Result>>(cashActval, out compliance);
        //                }
        //                catch (Exception ex)
        //                {
        //                    LogLibrary.WriteErrorLog("GET Dailyupdate Error exception :" + cashActval);
        //                    entities.Database.CommandTimeout = 120;
        //                    compliance = (from row in entities.SP_GetAllCompliance()
        //                                  select row).ToList();

        //                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //                }
        //            }
        //            else
        //            {
        //                compliance = (from row in entities.SP_GetAllCompliance()
        //                              select row).ToList();

        //                CacheHelper.Set<List<SP_GetAllCompliance_Result>>(cashActval, compliance);
        //                if (CacheHelper.Exists(cashTimeval))
        //                {
        //                    CacheHelper.Remove(cashTimeval);
        //                    CacheHelper.Set(cashTimeval, DateTime.Now);
        //                }
        //                else
        //                {
        //                    CacheHelper.Set(cashTimeval, DateTime.Now);
        //                }
        //            }

        //            if (actIds.Count() > 0)
        //            {
        //                //compliance = compliance.Where(entry => actIds.Contains((int)entry.ActID)).ToList();
        //                compliance = compliance.Where(entry => actIds.Contains(entry.ActID)).OrderBy(entry => entry.ShortDescription).Distinct().ToList();
        //            }

        //            //if (txtcompliance.Text != "" && txtcompliance.Text != "<Filter ShortDescription/Section>")
        //            //{
        //            //    compliance = compliance.Where(entry => entry.ShortDescription.ToLower().Contains(txtcompliance.Text.ToLower().Trim())).ToList();
        //            //}

        //            if (txtComplianceIDList.Text != "")
        //            {
        //                var ComplianceTextboxList = txtComplianceIDList.Text.Split(',').ToList();
        //                var  ComplianceIDList = ComplianceTextboxList.Select(long.Parse).ToList();

        //                if(ComplianceIDList.Count()> 0)
        //                {
        //                    compliance = compliance.Where(entry => ComplianceIDList.Contains((int)entry.ID)).ToList();
        //                }
        //            }

        //            //DataTable table = new DataTable();
        //            //table.Columns.Add("ID", typeof(int));
        //            //table.Columns.Add("Name", typeof(string));

        //            //foreach (var item in compliance)
        //            //{
        //            //    DataRow dr = table.NewRow();
        //            //    dr["ID"] = item.ID;
        //            //    dr["Name"] = item.ShortDescription;
        //            //    table.Rows.Add(dr);
        //            //}
        //            return compliance;
        //        }
        //        catch (Exception ex)
        //        {
        //            LogLibrary.WriteErrorLog("GET Dailyupdate Compliance Error exception :" + cashActval);
        //            LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //            return null;
        //        }
        //    }
        //}

        //public DataTable GetAllComplianceFromScheme()
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        List<long> actIDs = new List<long>();

        //        var SchemeMappedActList = ActManagement.GetSchemeMappedActIDs(Convert.ToInt32(ddlScheme.SelectedValue));
        //        foreach (var aItem in SchemeMappedActList)
        //        {
        //            actIDs.Add(aItem);
        //        }

        //        List<SP_GetAllCompliance_Result> compliance = new List<SP_GetAllCompliance_Result>();
        //        entities.Database.CommandTimeout = 120;
        //        compliance = (from row in entities.SP_GetAllCompliance()
        //                      select row).ToList();

        //        if (actIDs.Count > 0)
        //        {
        //            compliance = compliance.Where(entry => actIDs.Contains(entry.ActID)).OrderBy(entry => entry.ShortDescription).Distinct().ToList();
        //        }

        //        DataTable table = new DataTable();
        //        table.Columns.Add("ID", typeof(int));
        //        table.Columns.Add("Name", typeof(string));

        //        //DataRow dr1 = table.NewRow();
        //        //dr1["ID"] = -1;
        //        //table.Rows.Add(dr1);
        //        foreach (var item in compliance)
        //        {
        //            DataRow dr = table.NewRow();
        //            dr["ID"] = item.ID;
        //            dr["Name"] = item.ShortDescription;
        //            table.Rows.Add(dr);
        //        }
        //        return table;
        //    }
        //}

        private void BindComplianceFromAct()
        {
            try
            {
                //CheckBoxList1.DataSource = null;
                //CheckBoxList1.DataBind();

                var ComplianceList = GetAllComplianceFromAct();

                //CheckBoxList1.DataSource = ComplianceList;
                //CheckBoxList1.DataTextField = "Name";
                //CheckBoxList1.DataValueField = "ID";

                //CheckBoxList1.DataBind();

                rptComplianceList.DataSource = ComplianceList;
                rptComplianceList.DataBind();

                //grdCompliances.DataSource = ComplianceList;
                //grdCompliances.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindComplianceFromScheme()
        {
            try
            {

                //CheckBoxList1.DataSource = null;
                //CheckBoxList1.DataBind();


                var ComplianceList = GetAllComplianceFromScheme();


                //CheckBoxList1.DataSource = ComplianceList;
                //CheckBoxList1.DataTextField = "Name";
                //CheckBoxList1.DataValueField = "ID";

                //CheckBoxList1.DataBind();


                rptComplianceList.DataSource = ComplianceList;
                rptComplianceList.DataBind();

                //grdCompliances.DataSource = ComplianceList;
                //grdCompliances.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public static List<SP_GetEventBasedCompliance_Result> GetAllCompliances( List<long> actIdList = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<long> actIDs = new List<long>();
                List<SP_GetEventBasedCompliance_Result> compliance = new List<SP_GetEventBasedCompliance_Result>();

                compliance = (from row in entities.SP_GetEventBasedCompliance()
                              select row).ToList();

                actIDs = actIdList;
                if (actIDs.Count > 0)
                {
                    //compliance = compliance.Where(entry => actIDs.Contains(entry.ActID)).OrderBy(entry => entry.ShortDescription.Distinct()).Distinct().ToList();

                    compliance = compliance.Where(entry => actIDs.Contains(entry.ActID)).OrderBy(entry => entry.ShortDescription).Distinct().ToList();
                }


                //if (!string.IsNullOrEmpty(filter))
                //{

                //    compliance = compliance.Where(entry => entry.ShortDescription.ToUpper().Contains(filter.ToUpper()) || entry.FileTag.ToUpper().Contains(filter.ToUpper())).OrderBy(entry => entry.ShortDescription).Distinct().ToList();

                //    compliance = (from obj in compliance
                //                  select obj).GroupBy(n => new { n.ID, n.ShortDescription })
                //                               .Select(g => g.FirstOrDefault())
                //                               .ToList();

                //}
                return compliance;
            }
        }

        private void BindState()
        {
            try
            {
                rptState.DataSource = AddressManagement.GetAllStates();
                rptState.DataBind();

                foreach (RepeaterItem aItem in rptState.Items)
                {
                    CheckBox chkState = (CheckBox)aItem.FindControl("chkState");

                        chkState.Checked = false;
                }
                //CheckBox StateSelectAll = (CheckBox)rptState.Controls[0].Controls[0].FindControl("StateSelectAll");
                //StateSelectAll.Checked = false;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindScheme()
        {
            try
            {
                ddlScheme.DataTextField = "Name";
                ddlScheme.DataValueField = "ID";

                ddlScheme.DataSource = ActManagement.GetScheme();
                ddlScheme.DataBind();

                ddlScheme.Items.Insert(0, new ListItem("< Select >", "-1"));
                ddlScheme.Items.Insert(1, new ListItem("< Other >", "0"));

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindCategory(List<int> category =null)
        {
            try
            {
                rptCategory.DataSource = null;
                rptCategory.DataBind();

                List<ComplianceCategory> Category = new List<ComplianceCategory>();
                 Category = ComplianceCategoryManagement.GetAll();

                //Category = Category.Where(a => a.ID == 2 || a.ID == 5 || a.ID == 12 || a.ID == 13 || a.ID == 15 || a.ID == 17).ToList();
                Category = Category.Where(a => a.ID == 2 || a.ID == 5 || a.ID == 12 || a.ID == 20 || a.ID == 15 || a.ID == 17).ToList();

                //if(category !=null)
                //{
                //    Category = Category.Where(a => category.Contains(a.ID)).ToList();
                //}

                rptCategory.DataSource = Category;
                rptCategory.DataBind();
                
                foreach (RepeaterItem aItem in rptCategory.Items)
                {
                    CheckBox chkCategory = (CheckBox)aItem.FindControl("chkCategory");
                    chkCategory.Checked = false;
                    //CheckBox CategorySelectAll = (CheckBox)rptCategory.Controls[0].Controls[0].FindControl("CategorySelectAll");


                    if (category != null)
                    {
                        for (int i = 0; i <= category.Count - 1; i++)
                        {
                            if (((Label)aItem.FindControl("lblCategoryID")).Text.Trim() == category[i].ToString())
                            {
                                chkCategory.Checked = true;
                            }
                        }
                    }
                    //if ((rptCategory.Items.Count) == (category.Count))
                    //{
                    //    CategorySelectAll.Checked = true;

                    //}
                    //else
                    //{
                    //    CategorySelectAll.Checked = false;
                    //}
                }

                //if (category != null)
                //{
                //    foreach (RepeaterItem aItem in rptCategory.Items)
                //    {
                //        CheckBox chkCategory = (CheckBox)aItem.FindControl("chkCategory");
                //        chkCategory.Checked = true;
                //        CheckBox CategorySelectAll = (CheckBox)rptCategory.Controls[0].Controls[0].FindControl("CategorySelectAll");
                //        CategorySelectAll.Checked = true;
                //    }
                //}
                //else
                //{
                //    foreach (RepeaterItem aItem in rptCategory.Items)
                //    {
                //        CheckBox chkCategory = (CheckBox)aItem.FindControl("chkCategory");
                //        chkCategory.Checked = false;
                //        CheckBox CategorySelectAll = (CheckBox)rptCategory.Controls[0].Controls[0].FindControl("CategorySelectAll");
                //        CategorySelectAll.Checked = false;
                //    }
                //}
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                cmpScheme.Visible = false;
                divScheme.Visible = false;
                divcompliance.Visible = true;

                foreach (RepeaterItem aItem in rptComplianceList.Items)
                {
                    CheckBox chkCompliance = (CheckBox)aItem.FindControl("chkCompliance");
                    chkCompliance.Checked = false;
                }

                List<int> actList = new List<int>();
                foreach (RepeaterItem aItem in rptActList.Items)
                {
                    CheckBox chkAct = (CheckBox)aItem.FindControl("chkAct");
                    if (chkAct.Checked == true)
                    {
                        if (((Label)aItem.FindControl("lblActID")).Text.Trim() == "-1")
                        {
                            ddlScheme.SelectedValue = "-1";
                            cmpScheme.Visible = true;
                            divScheme.Visible = true;
                        }
                        else
                        {
                            actList.Add(Convert.ToInt32(((Label)aItem.FindControl("lblActID")).Text.Trim()));
                        }
                    }
                }

                if(actList.Count>0)
                {
                    //divcompliance.Visible = true;
                    BindComplianceFromAct();

                    var vGetComplianceMappedIDs = Business.DailyUpdateManagment.GetComplianceMappedID(Convert.ToInt32(ViewState["dailyUpdateID"]));
                    foreach (RepeaterItem aItem in rptComplianceList.Items)
                    {
                        CheckBox chkCompliance = (CheckBox)aItem.FindControl("chkCompliance");
                        chkCompliance.Checked = false;

                        for (int i = 0; i <= vGetComplianceMappedIDs.Count - 1; i++)
                        {
                            if (((Label)aItem.FindControl("lblComplianceID")).Text.Trim() == vGetComplianceMappedIDs[i].ToString())
                            {
                                chkCompliance.Checked = true;
                            }
                        }

                    }

                    var categoryList = DailyUpdateManagment.GetComplianceCategoryFromAct(actList);
                    BindCategory(categoryList);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
      
        protected void upEventList_Load(object sender, EventArgs e)
        {
            DateTime date = DateTime.Now;
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);

            ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeActList", string.Format("initializeJQueryUI('{0}', 'dvActList');", txtactList.ClientID), true);
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideActList", "$(\"#dvActList\").hide(\"blind\", null, 5, function () { });", true);

            //ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeComplianceList", string.Format("initializeJQueryUI('{0}', 'dvComplianceList');", txtcompliance.ClientID), true);
            //ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideComplianceList", "$(\"#dvComplianceList\").hide(\"blind\", null, 5, function () { });", true);


            ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeCategoryList", string.Format("initializeJQueryUI('{0}', 'dvCategory');", txtCategory.ClientID), true);
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideCategoryList", "$(\"#dvCategory\").hide(\"blind\", null, 5, function () { });", true);

            ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeStateList", string.Format("initializeJQueryUI('{0}', 'dvState');", txtState.ClientID), true);
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideStateList", "$(\"#dvState\").hide(\"blind\", null, 5, function () { });", true);

            ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeIndustryList", string.Format("initializeJQueryUI('{0}', 'dvIndustry');", txtIndustry.ClientID), true);
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideIndustryList", "$(\"#dvIndustry\").hide(\"blind\", null, 5, function () { });", true);
        }

        protected void ddlScheme_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlScheme.SelectedIndex == 1 || ddlScheme.SelectedIndex == 0)
                {
                    chkAll.Visible = true;
                    BindCategory();
                }
                else
                {
                    chkAll.Checked = false;
                    chkAll.Visible = false;
                    //BindCategory();
                    var categoryList = DailyUpdateManagment.GetComplianceCategoryFromScheme(Convert.ToInt32(ddlScheme.SelectedValue));
                    BindCategory(categoryList);
                    BindComplianceFromScheme();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlDailyUpdateType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

                foreach (RepeaterItem aItem in rptState.Items)
                {
                    CheckBox chkState = (CheckBox)aItem.FindControl("chkState");
                    chkState.Checked = false;
                }

                if (ddlDailyUpdateType.SelectedValue == "2") //State
                {
                    divState.Visible = true;

                }
                else //Central
                {
                    divState.Visible = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }

        protected void IsMiscellaneous_CheckedChanged(object sender, EventArgs e)
        {
            try
            {

                if (IsMiscellaneous.Checked == true)
                {
                    txtactList.Text = "< Select Act >";
                    txtactList.Enabled = false;

                    foreach (RepeaterItem aItem in rptActList.Items)
                    {
                        CheckBox chkAct = (CheckBox)aItem.FindControl("chkAct");
                        chkAct.Checked = false;
                        CheckBox ActSelectAll = (CheckBox)rptActList.Controls[0].Controls[0].FindControl("ActSelectAll");
                        ActSelectAll.Checked = false;
                    }
                }
                else
                {
                    txtactList.Text = "< Select Act >";
                    txtactList.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlActDocType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                divDate.Visible = false;
                rfvDate.Enabled = false;
                txtActDocVerstionDate.Text = "";
                if (ddlActDocType.SelectedValue != "-1" && ddlActDocType.SelectedValue != "1")
                {
                    divDate.Visible = true;
                    rfvDate.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindActDocType()
        {
            try
            {
                ddlActDocType.DataTextField = "Name";
                ddlActDocType.DataValueField = "ID";
                ddlActDocType.DataSource = ActManagement.GetAllActDocumentType();
                ddlActDocType.DataBind();
                ddlActDocType.Items.Insert(0, new ListItem("< Select DocumentType>", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                //txtcompliance.Text = "";
                if (ddlScheme.SelectedIndex == -1)
                {
                    BindComplianceFromScheme();
                }
                else
                {
                    BindComplianceFromAct();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void txtcompliance_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlScheme.SelectedIndex == -1)
                {
                    BindComplianceFromScheme();
                }
                else
                {
                    BindComplianceFromAct();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void txtComplianceIDList_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlScheme.SelectedIndex == -1)
                {
                    BindComplianceFromScheme();
                }
                else
                {
                    BindComplianceFromAct();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
    }
}