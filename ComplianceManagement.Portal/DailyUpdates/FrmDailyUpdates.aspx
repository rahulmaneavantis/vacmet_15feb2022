﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true"  CodeBehind="FrmDailyUpdates.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.DailyUpdates.FrmDailyUpdates" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">


    <script type="text/javascript" src="../tiny_mce/tiny_mce.js"></script>

    <script type="text/javascript" language="javascript">
        tinyMCE.init({
            // General options
            mode: "textareas",
            theme: "advanced",
            plugins: "pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups",

        });
    </script>

    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upEventList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <table width="100%">
                <tr>
                    <td align="right"></td>
                    <td align="right" style="width: 25%">Filter :
                        <asp:TextBox runat="server" ID="tbxFilter" Width="250px" MaxLength="50" AutoPostBack="true"
                            OnTextChanged="tbxFilter_TextChanged" />
                    </td>
                    <td class="newlink" align="right" style="width: 10%">
                        <asp:LinkButton Text="Add New" runat="server" ID="btnAddEvent" OnClick="btnAddEvent_Click" Visible="false" />
                    </td>
                     <td class="newlink" align="right" style="width: 20%">
                        <asp:LinkButton ID="lnkCacheRefresh" runat="server" Text='Last updated within the last hour' 
                            OnClick="lnkCacheRefresh_Click" ></asp:LinkButton>
                    </td>
                </tr>
            </table>
             <div style="margin-bottom: 4px">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="vdsummary"
                                ValidationGroup="EventValidationGroup" />
                            <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                ValidationGroup="EventValidationGroup" Display="None" />
                        </div>
            <asp:Panel ID="Panel1" Width="100%" Height="450px" ScrollBars="Vertical" runat="server">
                <asp:GridView runat="server" ID="grdEventList" AutoGenerateColumns="false" GridLines="Vertical" OnRowDataBound="grdEventList_RowDataBound"
                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true" OnRowCreated="grdEventList_RowCreated"
                    CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="100" Width="100%" OnSorting="grdEventList_Sorting"
                    Font-Size="12px" DataKeyNames="ID" OnRowCommand="grdEventList_RowCommand" OnPageIndexChanging="grdEventList_PageIndexChanging">
                    <Columns>
                        <asp:TemplateField HeaderText="Title" ItemStyle-Height="25px" HeaderStyle-Height="20px" SortExpression="Title">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 400px">
                                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("Title") %>' ToolTip='<%# Eval("Title") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Sub Title" ItemStyle-Height="25px" HeaderStyle-Height="20px" SortExpression="SubTitle">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 400px">
                                    <asp:Label ID="lblSubTitle" runat="server" Text='<%# Eval("SubTitle") %>' ToolTip='<%# Eval("SubTitle") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Main/Sub" ItemStyle-Height="25px" HeaderStyle-Height="20px" SortExpression="Status">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 50px">
                                    <asp:Label ID="lblMainFlag" runat="server" Text='<%# Eval("MainFlag") %>' ToolTip='<%# Eval("MainFlag") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Status" ItemStyle-Height="25px" HeaderStyle-Height="20px" SortExpression="Status">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 50px">
                                    <asp:Label ID="lblIsActive" runat="server" Text='<%# Eval("IsActive") %>' ToolTip='<%# Eval("IsActive") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                      <%--  <asp:TemplateField HeaderText="Description" ItemStyle-Height="25px" HeaderStyle-Height="20px" SortExpression="Name">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 400px">
                                    <asp:Label ID="Label2" runat="server" Text='<%# Eval("Description") %>' ToolTip='<%# Eval("Description") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                        <asp:TemplateField ItemStyle-Width="60px" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:LinkButton ID="lbtEdit" runat="server" CommandName="EDIT_ACT" CommandArgument='<%# Eval("ID") %>'><img src="../Images/edit_icon.png" alt="Edit Event" title="Edit DailyUpdate" /></asp:LinkButton>
                                <asp:LinkButton ID="lbtDelete" runat="server" CommandName="DELETE_ACT" CommandArgument='<%# Eval("ID") %>'
                                    OnClientClick="return confirm('Are you certain you want to delete this daily update?');"><img src="../Images/delete_icon.png" alt="Delete Event" title="Delete DailyUpdate" /></asp:LinkButton>
                            </ItemTemplate>
                            <HeaderTemplate>
                            </HeaderTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle BackColor="#CCCC99" />
                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                    <PagerSettings Position="Top" />
                    <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                    <AlternatingRowStyle BackColor="#E6EFF7" />
                    <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                    <EmptyDataTemplate>
                        No Records Found.
                    </EmptyDataTemplate>
                </asp:GridView>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div id="divEventDialog">
     
    </div>

    <script type="text/javascript">

        $(function () {
            $('#divEventDialog').dialog({
                height: 460,
                width: 800,
                autoOpen: false,
                draggable: true,
                title: "Daily Updates",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });
        });

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }
        function initializeCombobox() {
            <%--  $("#<%= ddlCategory.ClientID %>").combobox();
            $("#<%= ddlFrequency.ClientID %>").combobox();--%>
            <%--  $("#<%= ddlAct.ClientID %>").combobox();--%>
           <%-- $("#<%= ddlCompanyType.ClientID %>").combobox();--%>
        }
    </script>
</asp:Content>
