<%@ Page Title="Notifications" Language="C#" MasterPageFile="~/NewCompliance.Master" AutoEventWireup="true" Async="true" EnableEventValidation="false" 

CodeBehind="Notification.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.DailyUpdates.Notification" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .cdesign {
            width: 27px;height: 27px;float: left;border-radius: 134px; margin-right: 5px;margin-top: -4px;
        }
    </style>
    <script type="text/javascript">
        function openModal() {
            $('#NotificationDetails').modal('show');
        }

        function openTicketModal() {
            $('#TicketNotificationDetails').modal('show');
        }
        function hidediv() {
            //var div = document.getElementById('NotificationDetails');
            //div.style.display == "none" ? "block" : "none";
            //$('.modal-backdrop').hide();
            $('#NotificationDetails').modal('hide');
            return true;
        }
       
        function hideTicketdiv() {
            //var div = document.getElementById('NotificationDetails');
            //div.style.display == "none" ? "block" : "none";
            //$('.modal-backdrop').hide();
            $('#TicketNotificationDetails').modal('hide');
            return true;
        }
       
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:UpdatePanel ID="upUserReminderList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel">

                    <div class="clearfix"></div>

      <div class="col-md-2 colpadding0 entrycount">
        <div class="col-md-3 colpadding0" >
            <p style="color: #999; margin-top: 5px;">Show </p>
        </div>

        <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px; float: left" 
            AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged"><%--OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged"  --%>
            <asp:ListItem Text="10" Selected="True"/>
            <asp:ListItem Text="20" />
            <asp:ListItem Text="30" />
            <asp:ListItem Text="50" />
        </asp:DropDownList> 

       <div class="col-md-3 colpadding0" >
        <p style="color: #999; margin-top: 5px;margin-left:30px;"> Entries</p>
       </div>
    </div>

   <div class="col-md-9 colpadding0" style="text-align: right; float: right">
        <div class="col-md-8 colpadding0">             
            <asp:DropDownList runat="server" ID="ddlNotificationType" class="form-control m-bot15 search-select"  style="width:105px;"
                 AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" >               
                <asp:ListItem Text="All" Value="-1" />
                <asp:ListItem Text="Read" Value="1" />
                <asp:ListItem Text="Unread" Value="0" />                
            </asp:DropDownList>           
        </div>        
    </div>

    <div style="margin-bottom: 4px">
            <asp:GridView runat="server" ID="GridNotifications" AutoGenerateColumns="false" GridLines="None" DataKeyNames="NotificationID"
                CssClass="table" AllowSorting="true" BorderWidth="0px" AllowPaging="True" PageSize="10" Width="100%" ShowHeaderWhenEmpty="false" 
                OnRowCommand="GridNotifications_RowCommand"> 
               
                <Columns>

                     <asp:TemplateField ItemStyle-HorizontalAlign="Left">
                        <HeaderTemplate>
                            <asp:CheckBox ID="chkCompliancesHeader" runat="server" AutoPostBack="true" OnCheckedChanged="chkCompliancesHeader_CheckedChanged" /> 
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkCompliances" runat="server"  AutoPostBack="true" OnCheckedChanged="chkCompliances_CheckedChanged" /> 
                        </ItemTemplate>
                    </asp:TemplateField>
                  
                    <asp:TemplateField HeaderText="Sr" ItemStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <%#Container.DataItemIndex+1 %>
                        </ItemTemplate>
                    </asp:TemplateField> 

                     <asp:BoundField HeaderText="Type" DataField="Type"  />                  
                     <asp:TemplateField HeaderText="Details">
                        <ItemTemplate>
                          <%--  <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 400px">--%>
                           <%--<asp:Label runat="Server" ID="lblType" Text='<%# Eval("Type").ToString()== "ticket"?Eval("Remark").ToString(): GetComplianceName(Convert.ToInt32(Eval("ComplianceID"))) %>' />--%>
                                 <asp:Label runat="Server" ID="lblType" Text='<%# Eval("Type").ToString()== "ticket"||Eval("Type").ToString()== "Custom"?Eval("Remark").ToString(): GetComplianceName(Convert.ToInt32(Eval("ComplianceID"))) %>' />
                                <%-- </div>--%>
                        </ItemTemplate>
                    </asp:TemplateField> 

                     <asp:TemplateField HeaderText="Status">
                        <ItemTemplate>
                            <%# Convert.ToBoolean(Eval("IsRead"))==true?"Read":"Unread"%>
                        </ItemTemplate>
                    </asp:TemplateField>

                     <asp:TemplateField HeaderText="Date">
                        <ItemTemplate>
                            <%# Eval("UpdatedOn")!= null?((DateTime)Eval("UpdatedOn")).ToString("dd-MMM-yyyy"):""%>
                        </ItemTemplate>
                    </asp:TemplateField>   
                    
                    <asp:TemplateField ItemStyle-HorizontalAlign="Center"  HeaderText="Action">
                        <ItemTemplate>                           
                           <asp:ImageButton ID="lnkShowDetails" runat="server"  CommandName="ShowDetails" CommandArgument='<%# Eval("NotificationID") %>'
                                    ToolTip="Click Here to View Notification Details" ImageUrl="../Images/View-icon-new.png"></asp:ImageButton>                         
                        </ItemTemplate>
                    </asp:TemplateField>             

                </Columns>                 
                  <PagerTemplate>
                    <table style="display: none">
                        <tr>
                            <td>
                                <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                            </td>
                        </tr>
                    </table>
                 </PagerTemplate>
                  <RowStyle CssClass="clsROWgrid"   />
                                         <HeaderStyle CssClass="clsheadergrid"    />
                <EmptyDataTemplate>
                    No Notifications Found.
                </EmptyDataTemplate>
            </asp:GridView>
          </div>

                    <div class="col-md-12 colpadding0">
                            <div class="col-md-6 colpadding0">                              
                                    <div class="table-Selecteddownload-text">
                                        <p><asp:Label runat="server" ID="lblTotalSelected" Text="" style="color: #999; margin-right: 13px;"></asp:Label></p>
                                    </div>
                                     <asp:Button Text="Mark as Read" ID="btnMarkasRead" OnClick="btnMarkasRead_Click" class="btn btn-search" runat="server"  />         

                          
                             
                            </div>

                            <div class="col-md-6 colpadding0">
                                <div class="table-paging" style="margin-bottom: 20px;">
                                    <asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick ="Previous_Click"/> 
                                  
                                    <div class="table-paging-text">
                                        <p>
                                            <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                            <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>
                                        </p>
                                    </div>

                                    <asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick ="Next_Click"/>                               
                                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                </div>
                            </div>
                        </div>

                </section>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

 <div>
        <div class="modal fade" id="NotificationDetails" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
            <div class="modal-dialog" style="width: 750px;">
                <div class="modal-content">
                    <div class="modal-header" style="background-color: #f7f7f7; height: 34px;">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="font-size: 15px; font-weight:bold;">X</button>
                    </div>

                   <div class="modal-body" style="background-color: #f7f7f7; padding: 0px 15px;">
                        <asp:UpdatePanel ID="upNotification" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                
                                <div class="row Dashboard-white-widget" style="background: none;">
                                    <div id="divRiskType" runat="server" class="circle  cdesign" ></div>
                                    <asp:Label ID="lblRiskType"  Font-Bold="true" Style="width: 300px; margin-left: 0px; font-size: 13px; color: #333;"
                                        maximunsize="300px" autosize="true" runat="server" />
                                </div>

                                <div id="ActDetails" class="row Dashboard-white-widget">
                                <div class="dashboard">
                                    <div class="col-lg-12 col-md-12">
                                        <div class="panel panel-default">

                                            <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;">
                                                <div class="panel-heading">
                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseActDetails">
                                                        <h2>Act Details</h2>
                                                    </a>
                                                    <div class="panel-actions">
                                                        <a class="btn-minimize collapsed" onclick="btnminimize(this)" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseActDetails"><i class="fa fa-chevron-down"></i></a>
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="collapseActDetails" class="collapse">
                                                <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%;">
                                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" style="width: 100%;">
                                                        <table style="width: 100%;">
                                                            <tr>
                                                                <td style="width: 20%; font-weight: bold;">Act Name</td>
                                                                <td style="width: 2%; font-weight: bold;">: </td>
                                                                <td style="width: 78%;">
                                                                    <asp:Label ID="lblActName" Style="width: 88%; font-size: 13px; color: #333;"
                                                                        autosize="true" runat="server" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 20%; font-weight: bold;">Section /Rule</td>
                                                                <td style="width: 2%; font-weight: bold;">: </td>
                                                                <td style="width: 78%;">
                                                                    <asp:Label ID="lblRule" Style="width: 88%; font-size: 13px; color: #333;"
                                                                        autosize="true" runat="server" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                                <div id="ComplianceDetails" class="row Dashboard-white-widget">
                                <div class="dashboard">
                                    <div class="col-lg-12 col-md-12">
                                        <div class="panel panel-default">

                                            <div class="panel panel-default" style="margin-bottom: 1px;">
                                                <div class="panel-heading">
                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseComplianceDetails">
                                                        <h2>Compliance Details</h2>
                                                    </a>
                                                    <div class="panel-actions">
                                                        <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseComplianceDetails"><i class="fa fa-chevron-up"></i></a>
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="collapseComplianceDetails"  class="collapse">
                                                <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%;">
                                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" style="width: 100%;">
                                                        <table style="width: 100%">
                                                            <tr>
                                                                <td style="width: 20%; font-weight: bold;">Compliance ID</td>
                                                                <td style="width: 2%; font-weight: bold;">: </td>
                                                                <td style="width: 78%;">
                                                                    <asp:Label ID="lblComplianceID" Style="width: 300px; font-size: 13px; color: #333;"
                                                                        maximunsize="300px" autosize="true" runat="server" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 20%; font-weight: bold;">Short Description</td>
                                                                <td style="width: 2%; font-weight: bold;">: </td>
                                                                <td style="width: 78%;">
                                                                    <asp:Label ID="lblComplianceDiscription" Style="width: 300px; font-size: 13px; color: #333;"
                                                                        maximunsize="300px" autosize="true" runat="server" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 20%; font-weight: bold;">Detailed Description</td>
                                                                <td style="width: 2%; font-weight: bold;">: </td>
                                                                <td style="width: 78%;">
                                                                    <asp:Label ID="lblDetailedDiscription" Style="width: 300px; font-size: 13px; color: #333;"
                                                                        maximunsize="300px" autosize="true" runat="server" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 20%; font-weight: bold;">Penalty</td>
                                                                <td style="width: 2%; font-weight: bold;">: </td>
                                                                <td style="width: 78%;">
                                                                    <asp:Label ID="lblPenalty" Style="width: 300px; font-size: 13px; color: #333;"
                                                                        maximunsize="300px" autosize="true" runat="server" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 20%; font-weight: bold;">Frequency</td>
                                                                <td style="width: 2%; font-weight: bold;">: </td>
                                                                <td style="width: 78%;">
                                                                    <asp:Label ID="lblFrequency" Style="width: 300px; font-size: 13px; color: #333;"
                                                                        maximunsize="300px" autosize="true" runat="server" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 20%; font-weight: bold;">Risk Type</td>
                                                                <td style="width: 2%; font-weight: bold;">: </td>
                                                                <td style="width: 78%;">
                                                                    <asp:Label ID="lblRisk" Style="width: 300px; font-size: 13px; color: #333;"
                                                                        maximunsize="300px" autosize="true" runat="server" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </fieldset>
                                            </div>                                           

                                        </div>
                                    </div>
                                </div>
                            </div>

                                 <div id="OthersDetails" class="row Dashboard-white-widget">
                                                <div class="dashboard">
                                                    <div class="col-lg-12 col-md-12">
                                                        <div class="panel panel-default">
                                                            <div class="panel panel-default" style="margin-bottom: 1px;">
                                                                <div class="panel-heading">
                                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOthersDetails">
                                                                        <h2>Notification Details</h2>
                                                                    </a>
                                                                    <div class="panel-actions">
                                                                        <a class="btn-minimize collapsed" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseOthersDetails"><i class="fa fa-chevron-down"></i></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div id="collapseOthersDetails" class="panel-collapse collapse in">
                                                                <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%;">
                                                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" style="width: 100%;">
                                                                        <table style="width: 100%">
                                                                            <tr>
                                                                                <td style="width: 20%; font-weight: bold;">Notification Message</td>
                                                                                <td style="width: 2%; font-weight: bold;">: </td>
                                                                                <td style="width: 78%;">
                                                                                    <asp:Label ID="lblNotificationMessage" Style="width: 300px; font-size: 13px; color: #333;"
                                                                                        maximunsize="300px" autosize="true" runat="server" />
                                                                                </td>
                                                                            </tr>                                                                           
                                                                        </table>
                                                                    </div>
                                                                </fieldset>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                <%--<fieldset style="border-style: solid; border-width: 1px; border-color: gray; margin-top: 5px;">
                                    <legend>Compliance Details</legend>
                                    <asp:Label ID="lblShorDescription" runat="server" Style="color:black; display: block; float: left; font-size: 16px; color: #333;margin-bottom: 5px;"></asp:Label>
                                </fieldset>--%>

                                <%--<fieldset style="border-style: solid; border-width: 1px; border-color: gray; margin-top: 5px;">
                                    <legend>Notification Message</legend>                               

                                    <div style="margin-bottom: 7px" class="form-group">                                       
                                        <asp:Label runat="server" ID="lblNotificationMessage" ForeColor="Black" />                                       
                                    </div>
                                </fieldset> --%>
                               
                                 <div class="table-advanceSearch-buttons" style="margin-left: 44%;">
                                        <asp:Button Text="Close" runat="server" ID="btnSave" class="btn btn-search" OnClientClick="javascript: return hidediv();" OnClick="btnClose_Click"  /><%----%>
                                    </div>
                                                               
                            </ContentTemplate>                            
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </div>

     <div>
        <div class="modal fade" id="TicketNotificationDetails" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
            <div class="modal-dialog" style="width: 750px;">
                <div class="modal-content">
                    <div class="modal-header" style="background-color: #f7f7f7; height: 34px;">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>

                   <div class="modal-body" style="background-color: #f7f7f7;">
                        <asp:UpdatePanel ID="upTicketNotification" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                
                                <div>
                                    <div id="div1" runat="server" class="circle"></div>
                                    <asp:Label ID="Label1" Style="width: 300px; margin-left: -17px; font-size: 13px; color: #333;"
                                        maximunsize="300px" autosize="true" runat="server" />
                                </div>

                                 <div id="OthersTicketDetails" class="row Dashboard-white-widget">
                                                <div class="dashboard">
                                                    <div class="col-lg-12 col-md-12">
                                                        <div class="panel panel-default">
                                                            <div class="panel panel-default" style="margin-bottom: 1px;">
                                                                <div class="panel-heading">
                                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOthersDetails">
                                                                        <h2>Notification Details</h2>
                                                                    </a>
                                                                    <div class="panel-actions">
                                                                        <a class="btn-minimize collapsed" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseOthersDetails"><i class="fa fa-chevron-down"></i></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div id="collapseOthersDetails" class="panel-collapse collapse in">
                                                                <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%;">
                                                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" style="width: 100%;">
                                                                        <table style="width: 100%">
                                                                            <tr>
                                                                                <td style="width: 20%; font-weight: bold;">Notification Message</td>
                                                                                <td style="width: 2%; font-weight: bold;">: </td>
                                                                                <td style="width: 78%;">
                                                                                    <asp:Label ID="lblTicketNotificationMessage" Style="width: 300px; font-size: 13px; color: #333;"
                                                                                        maximunsize="300px" autosize="true" runat="server" />
                                                                                </td>
                                                                            </tr>                                                                           
                                                                        </table>
                                                                    </div>
                                                                </fieldset>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>                               
                               
                                 <div class="table-advanceSearch-buttons" style="margin-left: 44%;">
                                        <asp:Button Text="Close" runat="server" ID="Button1" class="btn btn-search" OnClientClick="javascript: return hideTicketdiv();" OnClick="btnClose_Click"  /><%----%>
                                    </div>
                                                               
                            </ContentTemplate>                            
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </div>

     <script>
     $(document).ready(function () {       
         fhead('My Notifications');
     });
 </script>
</asp:Content>

