﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AdvocateBillApprovalMailAccess.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.AdvocateBillApprovalMailAccess" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="AVANTIS - Products that simplify" />
    <meta name="author" content="Avantis - Development Team" />

    <title>Login :: AVANTIS - Products that simplify</title>
    <!-- Bootstrap CSS -->
    <link href="../Style/css/bootstrap.min.css" rel="stylesheet" />
    <!-- bootstrap theme -->
    <link href="../Style/css/bootstrap-theme.css" rel="stylesheet" />
    <!--external css-->
    <!-- font icon -->
    <link href="../Style/css/elegant-icons-style.css" rel="stylesheet" />
    <link href="../Style/css/font-awesome.css" rel="stylesheet" />
    <!-- Custom styles -->
    
   <link href="../NewCSS/stylenew.css" rel="stylesheet" />
    <link href="../Style/css/style-responsive.css" rel="stylesheet" />
    <style>
       .login-form .login-img {
    font-size: 50px;
    font-weight: 300;
    text-align: center;
    margin: 0 0 0px;
}
        body {
         background: #fff; 
        }
        .login-form {
            max-width: 350px;
            margin: 100px auto 0;
            background: #fff;
            webkit-box-shadow: 0 2px 2px 0 rgba(0,0,0,0.14), 0 3px 1px -2px rgba(0,0,0,0.12), 0 1px 5px 0 rgba(0,0,0,0.2);
            box-shadow: 0 2px 2px 0 rgba(0,0,0,0.14), 0 3px 1px -2px rgba(0,0,0,0.12), 0 1px 5px 0 rgba(0,0,0,0.2);
        }
        .login-form h2 {
    font-size: 24px;
    color: #555;
    font-weight: 500;
}
        .login-wrap {
    text-align: center;
}
        .badge {
            display: inline-block;
            min-width: 10px;
            padding: 8px 10px 20px 10px;
            font-size: 14px;
            font-weight: 700;
            line-height: 1;
            color: #fff;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            background-color: #777;
            border-radius: 50%;
        }
    </style>
     <script type="text/javascript">
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-92029518-1', 'auto');
        ga('send', 'pageview');

        function settracknew(e, t, n, r) {
            try {
                ga('send', 'event', e, t, n + "#" + r)
            } catch (t) { } return !0
        }

        function settracknewnonInteraction(e, t, n, r) {
            ga('send', 'event', e, t, n, 0, { 'nonInteraction': 1 })
        }
    </script>
</head>
<body>

    <div class="container">
        <form runat="server" class="login-form" name="login" id="Form1" autocomplete="off">
            <asp:Panel ID="Panel1" runat="server" DefaultButton="btnOTP">
                <asp:ScriptManager ID="ScriptManager1" runat="server" />
                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="col-md-12 login-form-head">
                            <p class="login-img">
                                <img src="../Images/avantil-logo.png" />
                            </p>
                        </div>
                         <asp:TextBox runat="server" ID="UserId" style="display: none;"></asp:TextBox>
                        <asp:TextBox runat="server" ID="CreateDate" style="display: none;"></asp:TextBox>
                        <div class="login-wrap" style="background: #F9F9F9;padding-top: 1px;">
                             <h2>Hi
                            <asp:Label ID="FirstNameUser" runat="server"></asp:Label></h2>
                        
                        <span style="color:black;border: 1px solid #F2F2F2; border-radius: 15px; padding: 7px; box-shadow: 3px 5px 6px 0px #CCCCCD;">
                            <span class="badge" style="background-color: #689F38">
                                <asp:Label ID="Label1" runat="server"></asp:Label>
                            </span>&nbsp;&nbsp;<asp:Label ID="UserEmail" runat="server" style="font-size: 15px!important;"></asp:Label></span>
                                                        
                            <div style="padding-top: 15px;text-align:left;">
                            <span   style="font-family:'Roboto',san-serif;color:#555555; font-size:13.5px;" >
                            One Time Password <asp:Label ID="Labelmsg" runat="server"></asp:Label> to <br />                            
                            Your registered email - <asp:Label ID="email" Font-Bold="true" runat="server"></asp:Label> <br />
                            Your registered Phone No - <asp:Label ID="mobileno" Font-Bold="true" runat="server"></asp:Label> <br /> 
                            <span id="spangenarteotp" runat="server">(Please note that the OTP is valid till <asp:Label ID="Time" runat="server"></asp:Label>  IST)</span></span>
                              </div>
                                <div class="clearfix" style="height: 10px"></div>
                               <div class="clearfix" style="height: 10px"></div>
                           
                            <div id="divgenarteotp" runat="server">
                                <asp:Button ID="BtnGenerate" CssClass="btn btn-primary btn-lg btn-block" Text="Generate OTP" runat="server" OnClick="BtnGenerate_Click"></asp:Button>
                            </div>
                            
                            <div id="divVerifyotp" runat="server">
                            <h1 style="font-size:15px;text-align:left;">Please Enter One Time Password (OTP)</h1>                           
                            
                            <div class="input-group">
                                <span class="input-group-addon"></span>
                                <%--<input type="text" id="txtemail" class="form-control" runat="server" placeholder="Username" autofocus="" />--%>
                                <asp:TextBox ID="txtOTP" CssClass="form-control" runat="server" placeholder="Enter OTP" />
                               <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="None" ErrorMessage="Required OTP."
                                    ControlToValidate="txtOTP" runat="server" ValidationGroup="OTPValidationGroup" />--%>                               
                            </div>

                             <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtOTP" runat="server" 
                                    ErrorMessage="Only Numbers allowed in OTP" ValidationExpression="\d+" ValidationGroup="OTPValidationGroup"></asp:RegularExpressionValidator>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="None" ErrorMessage="Required OTP."
                                    ControlToValidate="txtOTP" runat="server" ValidationGroup="OTPValidationGroup" /> 
                            <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator6" Display="None" runat="server"
                                    ValidationGroup="OTPValidationGroup" ErrorMessage="Please enter only 5 digit."
                                    ControlToValidate="txtOTP" ValidationExpression="[0-9]{10}"></asp:RegularExpressionValidator>--%>

                            <asp:Label ID="lblmsgotp" runat="server" ForeColor="Red" CssClass="pull-left" Text=""></asp:Label>
                            <asp:Button ID="btnOTP" CssClass="btn btn-primary btn-lg btn-block" Text="Verify OTP" OnClick="btnOTP_Click" runat="server" ValidationGroup="OTPValidationGroup"></asp:Button>

                            <div style="margin-top: 15px;">
                                <asp:LinkButton ID="lnkSecurityQA" Font-Underline="false" runat="server"></asp:LinkButton>
                                <asp:LinkButton ID="lnkResendOTP" Font-Underline="false" style="float:right;" OnClick="lnkResendOTP_Click" runat="server">Resend OTP</asp:LinkButton>
                            </div>
                            </div>
                            <div class="clearfix" style="height: 18px"></div>
                            
                            <asp:CustomValidator ID="cvLogin" class="alert alert-block alert-danger fade in" EnableClientScript="False" runat="server" Display="None" ValidationGroup="OTPValidationGroup" />
                            <asp:ValidationSummary ID="vsLogin" class="alert alert-block alert-danger fade in" runat="server" ValidationGroup="OTPValidationGroup" />
                          
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnOTP" />
                    </Triggers>
                </asp:UpdatePanel>
            </asp:Panel>
        </form>
    </div>
    <div class="clearfix" style="height: 10px"></div>
    <!--js-->
       
    <script type='text/javascript' src="../js/bootstrap.min.js"></script>
    <script type='text/javascript' src="../js/google1113_jquery.min.js"></script>
</body>
</html>