﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.LicenseManagement.Dashboard
{
    public partial class LicenseDashboardGrpahDetailsKendo : System.Web.UI.Page
    {
        protected static string Path;
        protected static string Custid;
        protected static int UserID;
        protected static string Role;
        protected static string type;
        protected string Internalsatutory;
        protected int LicenseTypeID;
        protected int branchID;
        protected int statVal;
        protected string StatusFlag;
        protected static string Authorization;
        protected static string FromDate;
        protected static string Enddate;
        protected void Page_Load(object sender, EventArgs e)
        {
            int CacheClearTime = Convert.ToInt32(ConfigurationManager.AppSettings["CacheClearTimeToken"]);
            string CacheName = "CacheGetTokenData_" + Convert.ToString(AuthenticationHelper.UserID) + "_" + Convert.ToString(AuthenticationHelper.CustomerID);
            Authorization = (string)HttpContext.Current.Cache[CacheName];
            if (Authorization == null)
            {
                Authorization = Business.ComplianceManagement.getToken(Convert.ToString(AuthenticationHelper.UserID));
                HttpContext.Current.Cache.Insert(CacheName, Authorization, null, DateTime.Now.AddMinutes(CacheClearTime), System.Web.Caching.Cache.NoSlidingExpiration); // add it to cache
            }                                           
            Path = ConfigurationManager.AppSettings["KendoPathApp"];
            Custid = Convert.ToString(AuthenticationHelper.CustomerID);
            UserID = AuthenticationHelper.UserID;
            Role = AuthenticationHelper.Role;
            LicenseTypeID = -1;
            StatusFlag = Convert.ToString(Request.QueryString["Status"]);
            LicenseTypeID = Convert.ToInt32(Request.QueryString["TID"]); 
            branchID=  Convert.ToInt32(Request.QueryString["BID"]);

            if (!string.IsNullOrEmpty(Request.QueryString["FromDate"]))
            {
                FromDate = Convert.ToString(Request.QueryString["FromDate"]);
            }
            else
            {
                FromDate = "01-01-1900";
            }

            if (!string.IsNullOrEmpty(Request.QueryString["Enddate"]))
            {
                Enddate = Convert.ToString(Request.QueryString["Enddate"]);
            }
            else
            {
                Enddate = "01-01-1900";
            }
            if (!String.IsNullOrEmpty(Request.QueryString["ISI"]))
            {
                var val = Request.QueryString["ISI"].ToString().Trim();
                if (val == "Statutory")
                {
                    Internalsatutory = "S";
                    statVal = 1;

                }
                else if (val == "Internal")
                {
                    Internalsatutory = "I";
                    statVal = 2;
                }
            }

          
        }
    }
}