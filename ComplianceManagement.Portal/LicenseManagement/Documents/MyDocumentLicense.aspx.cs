﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.License;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using com.VirtuosoITech.ComplianceManagement.Business.AWS;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon;

namespace com.VirtuosoITech.ComplianceManagement.Portal.LicenseManagement.Documents
{
    public partial class MyDocumentLicense : System.Web.UI.Page
    {
        public static string LicensenoDocPath = "";
        protected bool flag;
        public static string CompDocReviewPath = "";
        protected static string user_Roles;
        protected static string UploadDocumentLink;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string listCustomer = ConfigurationManager.AppSettings["UploadUsingLinkCustomerlist"];
                string[] listCust = new string[] { };
                if (!string.IsNullOrEmpty(listCustomer))
                    listCust = listCustomer.Split(',');

                if (listCust.Contains(Convert.ToString(AuthenticationHelper.CustomerID)))
                    UploadDocumentLink = "True";
                else
                    UploadDocumentLink = "False";

                if (!IsPostBack)
                {
                    user_Roles = AuthenticationHelper.Role;
                    int customerID = -1;
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    var branchList = CustomerBranchManagement.GetAllHierarchyManagementSatutory(Convert.ToInt32(customerID));
                    BindCustomerBranches(tvFilterLocation, tbxFilterLocation, branchList);
                    BindLicenseType();
                    BindLicenseStatusList();
                    BindGrid();
                    bindPageNumber();
                    SetShowingRecords();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void ddlComplianceType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {            
                BindLicenseType();
                BindGrid();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindCustomerBranches(TreeView treetoBind, TextBox treeTxtBox, List<NameValueHierarchy> branchList)
        {
            try
            {
                treetoBind.Nodes.Clear();

                NameValueHierarchy branch = null;

                if (branchList.Count > 0)
                {
                    branch = branchList[0];
                }

                treeTxtBox.Text = "Select Entity/Branch/Location";

                List<TreeNode> nodes = new List<TreeNode>();

                BindBranchesHierarchy(null, branch, nodes);

                foreach (TreeNode item in nodes)
                {
                    treetoBind.Nodes.Add(item);
                }

                treetoBind.CollapseAll();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvErrorLicenseListPage.IsValid = false;
                cvErrorLicenseListPage.ErrorMessage = "Something went wrong, Please try again.";
            }
        }

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp, List<TreeNode> nodes)
        {
            try
            {
                if (nvp != null)
                {
                    foreach (var item in nvp.Children)
                    {
                        TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                        BindBranchesHierarchy(node, item, nodes);
                        if (parent == null)
                        {
                            nodes.Add(node);
                        }
                        else
                        {
                            parent.ChildNodes.Add(node);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvErrorLicenseListPage.IsValid = false;
                cvErrorLicenseListPage.ErrorMessage = "Something went wrong, Please try again.";
            }
        }

        public void BindLicenseType()
        {
            string isstatutoryinternal = "S";
            if (ddlComplianceType.SelectedItem.Text == "Statutory")
            {
                isstatutoryinternal = "S";
            }
            else if (ddlComplianceType.SelectedItem.Text == "Internal")
            {
                isstatutoryinternal = "I";
            }
            List<Sp_BindLicenseType_Result> data = new List<Sp_BindLicenseType_Result>();
            if (user_Roles.Contains("CADMN") || user_Roles.Contains("IMPT") || user_Roles.Contains("MGMT"))
            {
                data = LicenseTypeMasterManagement.GetLicenseTypeWiseAssinedUser(AuthenticationHelper.UserID, AuthenticationHelper.Role, isstatutoryinternal);                
            }
            else
            {
                data = LicenseTypeMasterManagement.GetLicenseTypeWiseAssinedUser(AuthenticationHelper.UserID, AuthenticationHelper.Role, isstatutoryinternal);
            }
            //Page DropDown
            ddlLicenseType.DataTextField = "Name";
            ddlLicenseType.DataValueField = "ID";

            ddlLicenseType.DataSource = data;
            ddlLicenseType.DataBind();

            ddlLicenseType.Items.Insert(0, new ListItem("< All >", "-1"));
        }
        
        private void BindLicenseStatusList()
        {
            try
            {
                ddlLicenseStatus.DataSource = null;
                ddlLicenseStatus.DataBind();
                ddlLicenseStatus.ClearSelection();
                ddlLicenseStatus.DataTextField = "StatusName";
                ddlLicenseStatus.DataValueField = "ID";                
                var statusList = LicenseMgmt.GetStatusList_All();
                ddlLicenseStatus.DataSource = statusList;
                ddlLicenseStatus.DataBind();
                ddlLicenseStatus.Items.Insert(0, new ListItem("Status", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindGrid()
        {
            try
            {
                long customerID = -1;
                customerID = AuthenticationHelper.CustomerID;

                int branchID = -1;                
                long licenseStatusID = -1;
                long licenseTypeID = -1;
                int pageSize = -1;
                int pageNumber = 0;

                if (!string.IsNullOrEmpty(ddlPageSize.SelectedValue))
                {
                    pageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                }

                pageNumber = grdLicenseList.PageIndex + 1;

                if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1")
                {
                    branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                }              
                if (!string.IsNullOrEmpty(ddlLicenseType.SelectedValue))
                {
                    licenseTypeID = Convert.ToInt32(ddlLicenseType.SelectedValue);
                }

                if (!string.IsNullOrEmpty(ddlLicenseStatus.SelectedValue))
                {
                    licenseStatusID = Convert.ToInt32(ddlLicenseStatus.SelectedValue);
                }

                var branchList = LitigationManagement.GetAllHierarchy(Convert.ToInt32(customerID), branchID);
                string isstatutoryinternal = "S";
                if (ddlComplianceType.SelectedItem.Text == "Statutory")
                {
                    isstatutoryinternal = "S";
                }
                else if (ddlComplianceType.SelectedItem.Text == "Internal")
                {
                    isstatutoryinternal = "I";
                }
                var lstAssignedLicenses = LicenseDocumentManagement.GetAssigned_MyDocuments(Convert.ToInt32(customerID), AuthenticationHelper.UserID, AuthenticationHelper.Role,
                     branchList, licenseStatusID, licenseTypeID, isstatutoryinternal);


                string SortExpr = string.Empty;
                string CheckDirection = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(ViewState["Direction"])))
                    {
                        CheckDirection = Convert.ToString(ViewState["Direction"]);

                        SortExpr = Convert.ToString(ViewState["SortExpression"]);
                        if (CheckDirection == "Ascending")
                        {
                            lstAssignedLicenses = lstAssignedLicenses.OrderBy(entry => entry.GetType().GetProperty(SortExpr).GetValue(entry, null)).ToList();
                        }
                        else
                        {
                            CheckDirection = "Descending";
                            lstAssignedLicenses = lstAssignedLicenses.OrderByDescending(entry => entry.GetType().GetProperty(SortExpr).GetValue(entry, null)).ToList();
                        }
                    }
                }
                flag = true;
                Session["TotalRows"] = null;
                if (lstAssignedLicenses.Count > 0)
                {
                    grdLicenseList.DataSource = lstAssignedLicenses;
                    grdLicenseList.DataBind();
                    Session["TotalRows"] = lstAssignedLicenses.Count;
                }
                else
                {
                    grdLicenseList.DataSource = lstAssignedLicenses;
                    grdLicenseList.DataBind();
                    Session["TotalRows"] = null;
                }
                lstAssignedLicenses.Clear();
                lstAssignedLicenses = null;                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvErrorLicenseListPage.IsValid = false;
                cvErrorLicenseListPage.ErrorMessage = "Something went wrong, Please try again.";
            }
        }

        

        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void SetShowingRecords()
        {
            if (Session["TotalRows"] != null)
            {
                //DivRecordsScrum.Visible = false;
                int PageSize = 0;
                int PageNumber = 0;

                if (!string.IsNullOrEmpty(ddlPageSize.SelectedItem.Text))
                    PageSize = Convert.ToInt32(ddlPageSize.SelectedItem.Text);

                if (!string.IsNullOrEmpty(DropDownListPageNo.SelectedValue))
                    PageNumber = Convert.ToInt32(DropDownListPageNo.SelectedValue);
                var EndRecord = 0;
                var TotalRecord = 0;
                var TotalValue = PageSize * PageNumber;

                TotalRecord = Convert.ToInt32(Session["TotalRows"]);
                if (TotalRecord < TotalValue)
                {
                    EndRecord = TotalRecord;
                }
                else
                {
                    EndRecord = TotalValue;
                }

                if (TotalRecord != 0)
                    lblStartRecord.Text = Convert.ToString(PageSize * PageNumber - PageSize + 1);
                else
                    lblStartRecord.Text = "0";

                lblEndRecord.Text = Convert.ToString(EndRecord) + " ";
                lblTotalRecord.Text = TotalRecord.ToString();
            }
            else
            {
                lblStartRecord.Text = "0 ";
                lblEndRecord.Text = "0 ";
                lblTotalRecord.Text = "0";
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdLicenseList.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);

                BindGrid();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdLicenseList.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }

                SetShowingRecords();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }



       
        protected void grdLicenseList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    ImageButton lblLinkButton = (ImageButton)e.Row.FindControl("lblLinkButton");
                    ImageButton lblDownLoadfile = (ImageButton)e.Row.FindControl("lblDownLoadfile");
                    ImageButton lblViewfile = (ImageButton)e.Row.FindControl("lblViewfile");

                    if (lblLinkButton != null && lblDownLoadfile != null && lblViewfile != null)
                    {

                        if (!string.IsNullOrEmpty(UploadDocumentLink))
                        {
                            if (UploadDocumentLink.Equals("True"))
                            {
                                lblLinkButton.Visible = true;
                                lblDownLoadfile.Visible = false;
                                lblViewfile.Visible = false;
                            }
                            else
                            {
                                lblLinkButton.Visible = false;
                                lblDownLoadfile.Visible = true;
                                lblViewfile.Visible = true;
                            }
                        }
                        else
                        {
                            lblLinkButton.Visible = false;
                            lblDownLoadfile.Visible = true;
                            lblViewfile.Visible = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public static List<GetLicenseInternalComplianceDocumentsView> GetFileDataInternalView(int ScheduledOnID, int FileID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var fileData = (from row in entities.GetLicenseInternalComplianceDocumentsViews
                                where row.ScheduledOnID == ScheduledOnID
                                && row.FileID == FileID
                                select row).ToList();

                return fileData;
            }
        }
        public static List<GetLicenseInternalComplianceDocumentsView> GetDocumnetsInternal(long ScheduledOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var DocumentList = (from row in entities.GetLicenseInternalComplianceDocumentsViews
                                    where row.ScheduledOnID == ScheduledOnID
                                    select row).ToList();

                return DocumentList;
            }
        }
      
        protected void grdLicenseList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandArgument != null)
                {
                    long customerID = -1;
                    customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;
                    int userID = AuthenticationHelper.UserID;

                    if (e.CommandName.Equals("Download"))
                    {
                        if (ddlComplianceType.SelectedItem.Text == "Statutory")
                        {
                            #region  Statutory
                            var AWSData = AmazonS3.GetAWSStorageDetail(AuthenticationHelper.CustomerID);
                            if (AWSData != null)
                            {
                                #region AWS Storage
                                using (ZipFile ComplianceZip = new ZipFile())
                                {
                                    //string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                                    //List<GetComplianceDocumentsView> ComplianceFileData = new List<GetComplianceDocumentsView>();
                                    //List<GetComplianceDocumentsView> ComplianceDocument = new List<GetComplianceDocumentsView>();
                                    //ComplianceDocument = DocumentManagement.GetFileData1(Convert.ToInt32(commandArgs[0])).ToList();
                                    //ComplianceFileData = ComplianceDocument;
                                    //var ComplianceData = DocumentManagement.GetForMonth(Convert.ToInt32(commandArgs[0]));
                                    //ComplianceZip.AddDirectoryByName(ComplianceData.ForMonth + "/" + commandArgs[0]);    

                                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                                    List<GetComplianceDocumentsView> ComplianceFileData = new List<GetComplianceDocumentsView>();
                                    List<GetComplianceDocumentsView> ComplianceDocument = new List<GetComplianceDocumentsView>();

                                    ComplianceDocument = DocumentManagement.GetFileData1(Convert.ToInt32(commandArgs[0])).ToList();
                                    ComplianceFileData = ComplianceDocument;

                                    var ComplianceData = DocumentManagement.GetForMonth(Convert.ToInt32(commandArgs[0]));
                                    ComplianceZip.AddDirectoryByName(ComplianceData.ForMonth + "/" + commandArgs[0]);

                                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                    string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                    string directoryPath = "~/TempDocuments/AWS/" + User;
                                    if (!Directory.Exists(directoryPath))
                                    {
                                        Directory.CreateDirectory(Server.MapPath(directoryPath));
                                    }
                                    if (ComplianceFileData.Count > 0)
                                    {
                                        foreach (var item in ComplianceFileData)
                                        {
                                            using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                            {
                                                GetObjectRequest request = new GetObjectRequest();
                                                request.BucketName = AWSData.BucketName + @"/" + item.FilePath;
                                                request.Key = item.FileName;
                                                GetObjectResponse response = client.GetObject(request);
                                                response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + item.FileName);
                                            }
                                        }

                                        int i = 0;
                                        foreach (var file in ComplianceFileData)
                                        {
                                            string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);
                                            if (file.FilePath != null && File.Exists(filePath))
                                            {
                                                string ext = Path.GetExtension(file.FileName);
                                                string[] filename = file.FileName.Split('.');
                                                string str = filename[0] + i + "." + ext;
                                                ComplianceZip.AddEntry(ComplianceData.ForMonth + "/" + commandArgs[0] + "/" + str,DocumentManagement.ReadDocFiles(filePath));                                                
                                                i++;
                                            }
                                        }
                                    }
                                    var zipMs = new MemoryStream();
                                    ComplianceZip.Save(zipMs);
                                    zipMs.Position = 0;
                                    byte[] data = zipMs.ToArray();

                                    Response.Buffer = true;

                                    Response.ClearContent();
                                    Response.ClearHeaders();
                                    Response.Clear();
                                    Response.ContentType = "application/zip";
                                    Response.AddHeader("content-disposition", "attachment; filename=LicenseDocument_" + commandArgs[0] + ".zip");
                                    Response.BinaryWrite(data);
                                    Response.Flush();
                                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                                }
                                #endregion
                            }
                            else
                            {
                                #region Normal Storage
                                using (ZipFile ComplianceZip = new ZipFile())
                                {
                                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                                    List<GetComplianceDocumentsView> ComplianceFileData = new List<GetComplianceDocumentsView>();
                                    List<GetComplianceDocumentsView> ComplianceDocument = new List<GetComplianceDocumentsView>();

                                    ComplianceDocument = DocumentManagement.GetFileData1(Convert.ToInt32(commandArgs[0])).ToList();
                                    ComplianceFileData = ComplianceDocument;

                                    var ComplianceData = DocumentManagement.GetForMonth(Convert.ToInt32(commandArgs[0]));
                                    ComplianceZip.AddDirectoryByName(ComplianceData.ForMonth + "/" + commandArgs[0]);

                                    if (ComplianceFileData.Count > 0)
                                    {
                                        int i = 0;
                                        foreach (var file in ComplianceFileData)
                                        {
                                            string filePath = string.Empty;
                                            //Change by rahul on 21 JAN 2017 for Azure Drive or Local Drive
                                            if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                            {
                                                filePath = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.FileName));
                                            }
                                            else
                                            {
                                                filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                            }
                                            if (file.FilePath != null && File.Exists(filePath))
                                            {
                                                string ext = Path.GetExtension(file.FileName);
                                                string[] filename = file.FileName.Split('.');
                                                string str = filename[0] + i + "." + ext;
                                                if (file.EnType == "M")
                                                {
                                                    ComplianceZip.AddEntry(ComplianceData.ForMonth + "/" + commandArgs[0] + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                else
                                                {
                                                    ComplianceZip.AddEntry(ComplianceData.ForMonth + "/" + commandArgs[0] + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                i++;
                                            }
                                        }
                                    }

                                    var zipMs = new MemoryStream();
                                    ComplianceZip.Save(zipMs);
                                    zipMs.Position = 0;
                                    byte[] data = zipMs.ToArray();
                                    Response.Buffer = true;
                                    Response.ClearContent();
                                    Response.ClearHeaders();
                                    Response.Clear();
                                    Response.ContentType = "application/zip";
                                    Response.AddHeader("content-disposition", "attachment; filename=LicenseDocument_" + commandArgs[0] + ".zip");
                                    Response.BinaryWrite(data);
                                    Response.Flush();
                                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                                }
                                #endregion
                            }
                            #endregion
                        }
                        else if (ddlComplianceType.SelectedItem.Text == "Internal")
                        {
                            #region Internal
                            var AWSData = AmazonS3.GetAWSStorageDetail(AuthenticationHelper.CustomerID);
                            if (AWSData != null)
                            {
                                #region AWS Storage
                                using (ZipFile ComplianceZip = new ZipFile())
                                {

                                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                                    List<GetLicenseInternalComplianceDocumentsView> ComplianceFileData = new List<GetLicenseInternalComplianceDocumentsView>();
                                    List<GetLicenseInternalComplianceDocumentsView> ComplianceDocument = new List<GetLicenseInternalComplianceDocumentsView>();

                                    ComplianceDocument = GetDocumnetsInternal(Convert.ToInt32(commandArgs[0])).ToList();
                                    ComplianceFileData = ComplianceDocument;

                                    var ComplianceData = DocumentManagement.GetForMonthInternal(Convert.ToInt32(commandArgs[0]));
                                    ComplianceZip.AddDirectoryByName(ComplianceData.ForMonth + "/" + commandArgs[0]);


                                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                    string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                    string directoryPath = "~/TempDocuments/AWS/" + User;
                                    if (!Directory.Exists(directoryPath))
                                    {
                                        Directory.CreateDirectory(Server.MapPath(directoryPath));
                                    }
                                    if (ComplianceFileData.Count > 0)
                                    {
                                        foreach (var item in ComplianceFileData)
                                        {
                                            using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                            {
                                                GetObjectRequest request = new GetObjectRequest();
                                                request.BucketName = AWSData.BucketName + @"/" + item.FilePath;
                                                request.Key = item.FileName;
                                                GetObjectResponse response = client.GetObject(request);
                                                response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + item.FileName);
                                            }
                                        }

                                        int i = 0;
                                        foreach (var file in ComplianceFileData)
                                        {
                                            string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);
                                            if (file.FilePath != null && File.Exists(filePath))
                                            {
                                                string ext = Path.GetExtension(file.FileName);
                                                string[] filename = file.FileName.Split('.');
                                                string str = filename[0] + i + "." + ext;
                                                //ComplianceZip.AddEntry(ComplianceData.ForMonth + "/" + commandArgs[1] + "/" + str, DocumentManagement.ReadDocFiles(filePath));
                                                ComplianceZip.AddEntry(ComplianceData.ForMonth + "/" + commandArgs[0] + "/" + str, DocumentManagement.ReadDocFiles(filePath));
                                                i++;
                                            }
                                        }
                                    }
                                    var zipMs = new MemoryStream();
                                    ComplianceZip.Save(zipMs);
                                    zipMs.Position = 0;
                                    byte[] data = zipMs.ToArray();

                                    Response.Buffer = true;

                                    Response.ClearContent();
                                    Response.ClearHeaders();
                                    Response.Clear();
                                    Response.ContentType = "application/zip";
                                    Response.AddHeader("content-disposition", "attachment; filename=LicenseDocument_" + commandArgs[0] + ".zip");
                                    Response.BinaryWrite(data);
                                    Response.Flush();
                                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                                }
                                #endregion

                            }
                            else
                            {
                                #region Normal Storage
                                using (ZipFile ComplianceZip = new ZipFile())
                                {
                                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                                    List<GetLicenseInternalComplianceDocumentsView> ComplianceFileData = new List<GetLicenseInternalComplianceDocumentsView>();
                                    List<GetLicenseInternalComplianceDocumentsView> ComplianceDocument = new List<GetLicenseInternalComplianceDocumentsView>();

                                    ComplianceDocument = GetDocumnetsInternal(Convert.ToInt32(commandArgs[0])).ToList();
                                    ComplianceFileData = ComplianceDocument;

                                    var ComplianceData = DocumentManagement.GetForMonthInternal(Convert.ToInt32(commandArgs[0]));
                                    ComplianceZip.AddDirectoryByName(ComplianceData.ForMonth + "/" + commandArgs[0]);

                                    if (ComplianceFileData.Count > 0)
                                    {
                                        int i = 0;
                                        foreach (var file in ComplianceFileData)
                                        {
                                            string filePath = string.Empty;
                                            //Change by rahul on 21 JAN 2017 for Azure Drive or Local Drive
                                            if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                            {
                                                filePath = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.FileName));
                                            }
                                            else
                                            {
                                                filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                            }
                                            if (file.FilePath != null && File.Exists(filePath))
                                            {
                                                string ext = Path.GetExtension(file.FileName);
                                                string[] filename = file.FileName.Split('.');
                                                //string str = filename[0] + i + "." + filename[1];
                                                string str = filename[0] + i + "." + ext;
                                                if (file.EnType == "M")
                                                {
                                                    ComplianceZip.AddEntry(ComplianceData.ForMonth + "/" + commandArgs[0] + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                else
                                                {
                                                    ComplianceZip.AddEntry(ComplianceData.ForMonth + "/" + commandArgs[0] + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                i++;
                                            }
                                        }
                                    }

                                    var zipMs = new MemoryStream();
                                    ComplianceZip.Save(zipMs);
                                    zipMs.Position = 0;
                                    byte[] data = zipMs.ToArray();

                                    Response.Buffer = true;
                                    Response.ClearContent();
                                    Response.ClearHeaders();
                                    Response.Clear();
                                    Response.ContentType = "application/zip";
                                    Response.AddHeader("content-disposition", "attachment; filename=LicenseDocument_" + commandArgs[0] + ".zip");
                                    Response.BinaryWrite(data);
                                    Response.Flush();
                                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                                }
                                #endregion
                            }
                            #endregion
                        }
                    }
                    else if (e.CommandName == "View")
                    {
                        if (ddlComplianceType.SelectedItem.Text == "Statutory")
                        {
                            #region Statutory
                            string[] commandArg = e.CommandArgument.ToString().Split(',');

                            var AWSData = AmazonS3.GetAWSStorageDetail(AuthenticationHelper.CustomerID);
                            if (AWSData != null)
                            {
                                #region AWS Storage
                                List<GetComplianceDocumentsView> CMPDocuments = Business.ComplianceManagement.GetDocumnets(Convert.ToInt64(commandArg[0]), 0);

                                if (CMPDocuments != null)
                                {
                                    List<GetComplianceDocumentsView> entitiesData = CMPDocuments.Where(entry => entry.Version != null).ToList();
                                    if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                                    {
                                        GetComplianceDocumentsView entityData = new GetComplianceDocumentsView();
                                        entityData.Version = "1.0";
                                        entityData.ScheduledOnID = Convert.ToInt64(commandArg[0]);
                                        entitiesData.Add(entityData);
                                    }

                                    if (entitiesData.Count > 0)
                                    {
                                        string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                        string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                        string directoryPath = "~/TempDocuments/AWS/" + User;
                                        if (!Directory.Exists(directoryPath))
                                        {
                                            Directory.CreateDirectory(Server.MapPath(directoryPath));
                                        }

                                        foreach (var file in CMPDocuments)
                                        {
                                            using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                            {
                                                GetObjectRequest request = new GetObjectRequest();
                                                request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                                request.Key = file.FileName;
                                                GetObjectResponse response = client.GetObject(request);
                                                response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                                            }
                                            string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);
                                            if (file.FilePath != null && File.Exists(filePath))
                                            {
                                                string extension = System.IO.Path.GetExtension(file.FileName);

                                                if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                                {
                                                    lblMessage.Text = "";
                                                    lblMessage.Text = "Zip file can't view please download it";
                                                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);
                                                }
                                                else
                                                {
                                                    string filePath1 = directoryPath + "/" + file.FileName;
                                                    CompDocReviewPath = filePath1;
                                                    CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                                                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileLicense('" + CompDocReviewPath + "');", true);
                                                    lblMessage.Text = "";

                                                }
                                            }
                                            else
                                            {
                                                lblMessage.Text = "There is no file to preview";
                                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileLicense();", true);
                                            }
                                            break;
                                        }
                                    }
                                }
                                #endregion
                            }
                            else
                            {
                                #region Normal Storage
                                List<GetComplianceDocumentsView> CMPDocuments = Business.ComplianceManagement.GetDocumnets(Convert.ToInt64(commandArg[0]), 0);

                                if (CMPDocuments != null)
                                {
                                    List<GetComplianceDocumentsView> entitiesData = CMPDocuments.Where(entry => entry.Version != null).ToList();
                                    if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                                    {
                                        GetComplianceDocumentsView entityData = new GetComplianceDocumentsView();
                                        entityData.Version = "1.0";
                                        entityData.ScheduledOnID = Convert.ToInt64(commandArg[0]);
                                        entitiesData.Add(entityData);
                                    }

                                    if (entitiesData.Count > 0)
                                    {
                                        rptComplianceVersionView.DataSource = entitiesData.OrderBy(entry => entry.Version);
                                        rptComplianceVersionView.DataBind();

                                        foreach (var file in CMPDocuments)
                                        {
                                            string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                            if (file.FilePath != null && File.Exists(filePath))
                                            {
                                                string Folder = "~/TempFiles";
                                                string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                                string DateFolder = Folder + "/" + File;

                                                string extension = System.IO.Path.GetExtension(filePath);
                                                if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                                {
                                                    lblMessage.Text = "";
                                                    lblMessage.Text = "Zip file can't view please download it";
                                                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);
                                                }
                                                else
                                                {
                                                    Directory.CreateDirectory(Server.MapPath(DateFolder));

                                                    if (!Directory.Exists(DateFolder))
                                                    {
                                                        Directory.CreateDirectory(Server.MapPath(DateFolder));
                                                    }

                                                    string customerIDs = Convert.ToString(Convert.ToInt32(AuthenticationHelper.CustomerID));

                                                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                                    string User = AuthenticationHelper.UserID + "" + customerIDs + "" + FileDate;

                                                    string FileName = DateFolder + "/" + User + "" + extension;

                                                    FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                                    BinaryWriter bw = new BinaryWriter(fs);
                                                    if (file.EnType == "M")
                                                    {
                                                        bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                    }
                                                    else
                                                    {
                                                        bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                    }
                                                    bw.Close();
                                                    CompDocReviewPath = FileName;
                                                    CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                                                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileLicense('" + CompDocReviewPath + "');", true);
                                                    lblMessage.Text = "";
                                                }
                                            }
                                            else
                                            {
                                                lblMessage.Text = "There is no file to preview";
                                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);
                                            }
                                            break;
                                        }
                                    }
                                }
                                #endregion
                            }
                            #endregion
                        }
                        else if (ddlComplianceType.SelectedItem.Text == "Internal")
                        {
                            #region Internal
                            string[] commandArg = e.CommandArgument.ToString().Split(',');
                            var AWSData = AmazonS3.GetAWSStorageDetail(AuthenticationHelper.CustomerID);
                            if (AWSData != null)
                            {
                                #region AWS Storage
                                List<GetLicenseInternalComplianceDocumentsView> CMPDocuments = GetDocumnetsInternal(Convert.ToInt64(commandArg[0]));

                                if (CMPDocuments != null)
                                {
                                    List<GetLicenseInternalComplianceDocumentsView> entitiesData = CMPDocuments.Where(entry => entry.Version != null).ToList();
                                    if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                                    {
                                        GetLicenseInternalComplianceDocumentsView entityData = new GetLicenseInternalComplianceDocumentsView();
                                        entityData.Version = "1.0";
                                        entityData.ScheduledOnID = Convert.ToInt64(commandArg[0]);
                                        entitiesData.Add(entityData);
                                    }

                                    if (entitiesData.Count > 0)
                                    {
                                        string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                        string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                        string directoryPath = "~/TempDocuments/AWS/" + User;
                                        if (!Directory.Exists(directoryPath))
                                        {
                                            Directory.CreateDirectory(Server.MapPath(directoryPath));
                                        }

                                        foreach (var file in CMPDocuments)
                                        {
                                            using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                            {
                                                GetObjectRequest request = new GetObjectRequest();
                                                request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                                request.Key = file.FileName;
                                                GetObjectResponse response = client.GetObject(request);
                                                response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                                            }
                                            string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);
                                            if (file.FilePath != null && File.Exists(filePath))
                                            {
                                                string extension = System.IO.Path.GetExtension(file.FileName);

                                                if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                                {
                                                    lblMessage.Text = "";
                                                    lblMessage.Text = "Zip file can't view please download it";
                                                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);
                                                }
                                                else
                                                {
                                                    string filePath1 = directoryPath + "/" + file.FileName;
                                                    CompDocReviewPath = filePath1;
                                                    CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                                                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileLicense('" + CompDocReviewPath + "');", true);
                                                    lblMessage.Text = "";

                                                }
                                            }
                                            else
                                            {
                                                lblMessage.Text = "There is no file to preview";
                                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileLicense();", true);
                                            }
                                            break;
                                        }
                                    }
                                }
                                #endregion
                            }
                            else
                            {
                                #region Normal Storage
                                List<GetLicenseInternalComplianceDocumentsView> CMPDocuments = GetDocumnetsInternal(Convert.ToInt64(commandArg[0]));

                                if (CMPDocuments != null)
                                {
                                    List<GetLicenseInternalComplianceDocumentsView> entitiesData = CMPDocuments.Where(entry => entry.Version != null).ToList();
                                    if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                                    {
                                        GetLicenseInternalComplianceDocumentsView entityData = new GetLicenseInternalComplianceDocumentsView();
                                        entityData.Version = "1.0";
                                        entityData.ScheduledOnID = Convert.ToInt64(commandArg[0]);
                                        entitiesData.Add(entityData);
                                    }

                                    if (entitiesData.Count > 0)
                                    {
                                        rptComplianceVersionView.DataSource = entitiesData.OrderBy(entry => entry.Version);
                                        rptComplianceVersionView.DataBind();

                                        foreach (var file in CMPDocuments)
                                        {
                                            string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                            if (file.FilePath != null && File.Exists(filePath))
                                            {
                                                string Folder = "~/TempFiles";
                                                string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                                string DateFolder = Folder + "/" + File;

                                                string extension = System.IO.Path.GetExtension(filePath);
                                                if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                                {
                                                    lblMessage.Text = "";
                                                    lblMessage.Text = "Zip file can't view please download it";
                                                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);
                                                }
                                                else
                                                {
                                                    Directory.CreateDirectory(Server.MapPath(DateFolder));

                                                    if (!Directory.Exists(DateFolder))
                                                    {
                                                        Directory.CreateDirectory(Server.MapPath(DateFolder));
                                                    }

                                                    string customerIDs = Convert.ToString(Convert.ToInt32(AuthenticationHelper.CustomerID));

                                                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                                    string User = AuthenticationHelper.UserID + "" + customerIDs + "" + FileDate;

                                                    string FileName = DateFolder + "/" + User + "" + extension;

                                                    FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                                    BinaryWriter bw = new BinaryWriter(fs);
                                                    if (file.EnType == "M")
                                                    {
                                                        bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                    }
                                                    else
                                                    {
                                                        bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                    }
                                                    bw.Close();
                                                    CompDocReviewPath = FileName;
                                                    CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                                                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileLicense('" + CompDocReviewPath + "');", true);
                                                    lblMessage.Text = "";
                                                }
                                            }
                                            else
                                            {
                                                lblMessage.Text = "There is no file to preview";
                                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);
                                            }
                                            break;
                                        }
                                    }
                                }
                                #endregion
                            }
                            #endregion
                        }
                    }
                    else if (e.CommandName == "ADDLINK")
                    {
                        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                        string ScheduledOnID = Convert.ToString(commandArgs[0]);
                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenDocumentOverviewpup('" + ScheduledOnID + "','" + ddlComplianceType.SelectedItem.Text + "');", true);

                    }
                    else if (e.CommandName == "Share")
                    {
                        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                        string FileID = Convert.ToString(commandArgs[0]);
                        string LicenseID = Convert.ToString(commandArgs[1]);
                        string LicenseTitle = Convert.ToString(commandArgs[2]);
                        string LicenseNo  = Convert.ToString(commandArgs[3]);
                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenDocSharePopup('" + FileID + "','" + LicenseID + "','" + LicenseTitle + "', '" + LicenseNo + "');", true);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvErrorLicenseListPage.IsValid = false;
                cvErrorLicenseListPage.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        
        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdLicenseList.PageIndex = chkSelectedPage - 1;

            grdLicenseList.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);

            BindGrid();
        }

        protected void lnkBtnApplyFilter_Click(object sender, EventArgs e)
        {
            try
            {
                BindGrid();
                bindPageNumber();
                SetShowingRecords();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lnkBtnClearFilter_Click(object sender, EventArgs e)
        {
            try
            {
                ddlLicenseType.ClearSelection();
                // ddlVendorPage.ClearSelection();
                ddlLicenseStatus.ClearSelection();

                ClearTreeViewSelection(tvFilterLocation);

                tvFilterLocation_SelectedNodeChanged(sender, e);
                lnkBtnApplyFilter_Click(sender, e);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public static void ClearTreeViewSelection(TreeView tree)
        {
            if (tree.SelectedNode != null)
            {
                tree.SelectedNode.Selected = false;
            }
        }

        protected void upDivLocation_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxFilterLocation.Text = tvFilterLocation.SelectedNode != null ? tvFilterLocation.SelectedNode.Text : "All";
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdLicenseList_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                long customerID = -1;
                customerID = AuthenticationHelper.CustomerID;

                int branchID = -1;             
                long licenseStatusID = -1;
                long licenseTypeID = -1;
                int pageSize = -1;
                int pageNumber = 0;

                if (!string.IsNullOrEmpty(ddlPageSize.SelectedValue))
                {
                    pageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                }

                pageNumber = grdLicenseList.PageIndex + 1;

                if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1")
                {
                    branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                }               
                if (!string.IsNullOrEmpty(ddlLicenseType.SelectedValue))
                {
                    licenseTypeID = Convert.ToInt32(ddlLicenseType.SelectedValue);
                }

                if (!string.IsNullOrEmpty(ddlLicenseStatus.SelectedValue))
                {
                    licenseStatusID = Convert.ToInt32(ddlLicenseStatus.SelectedValue);
                }

                var branchList = LitigationManagement.GetAllHierarchy(Convert.ToInt32(customerID), branchID);
                string isstatutoryinternal = "S";
                if (ddlComplianceType.SelectedItem.Text == "Statutory")
                {
                    isstatutoryinternal = "S";
                }
                else if (ddlComplianceType.SelectedItem.Text == "Internal")
                {
                    isstatutoryinternal = "I";
                }
                var lstAssignedLicenses = LicenseDocumentManagement.GetAssigned_MyDocuments(Convert.ToInt32(customerID), AuthenticationHelper.UserID, AuthenticationHelper.Role,
                    branchList, licenseStatusID, licenseTypeID, isstatutoryinternal);

                string SortExpr = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                {
                    SortExpr = Convert.ToString(ViewState["SortExpression"]);
                    if (SortExpr == e.SortExpression)
                    {
                        if (direction == SortDirection.Ascending)
                        {
                            direction = SortDirection.Descending;
                        }
                        else
                        {
                            direction = SortDirection.Ascending;
                        }
                    }
                    else
                    {
                        direction = SortDirection.Ascending;
                    }
                }

                if (direction == SortDirection.Ascending)
                {
                    ViewState["Direction"] = "Ascending";
                    lstAssignedLicenses = lstAssignedLicenses.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                }
                else
                {
                    ViewState["Direction"] = "Descending";
                    lstAssignedLicenses = lstAssignedLicenses.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                }

                ViewState["SortExpression"] = e.SortExpression;

                foreach (DataControlField field in grdLicenseList.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdLicenseList.Columns.IndexOf(field);
                    }
                }
                flag = true;
                Session["TotalRows"] = null;
                if (lstAssignedLicenses.Count > 0)
                {
                    grdLicenseList.DataSource = lstAssignedLicenses;
                    grdLicenseList.DataBind();
                    Session["TotalRows"] = lstAssignedLicenses.Count;
                }
                else
                {
                    grdLicenseList.DataSource = lstAssignedLicenses;
                    grdLicenseList.DataBind();
                    Session["TotalRows"] = null;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdLicenseList_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
            {
                System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
                sortImage.ImageAlign = ImageAlign.AbsMiddle;

                if (flag == true)
                {
                    if (direction == SortDirection.Ascending)
                    {
                        sortImage.ImageUrl = "../../Images/down_arrow1.png";
                        sortImage.AlternateText = "Ascending Order";
                    }
                    else
                    {
                        sortImage.ImageUrl = "../../Images/up_arrow1.png";
                        sortImage.AlternateText = "Descending Order";
                    }
                    headerRow.Cells[columnIndex].Controls.Add(sortImage);
                }
            }
        }

        protected void rptComplianceVersionView_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                if (e.CommandName.Equals("View"))
                {                    
                    if (ddlComplianceType.SelectedItem.Text == "Statutory")
                    {
                        #region Statutory
                        string[] commandArg = e.CommandArgument.ToString().Split(',');
                        var AWSData = AmazonS3.GetAWSStorageDetail(AuthenticationHelper.CustomerID);
                        if (AWSData != null)
                        {
                            #region AWS Storage
                            List<GetComplianceDocumentsView> CMPDocuments = DocumentManagement.GetFileDataView(Convert.ToInt32(commandArgs[0]), Convert.ToInt32(commandArgs[2])).ToList();                           
                            if (CMPDocuments != null)
                            {
                                List<GetComplianceDocumentsView> entitiesData = CMPDocuments.Where(entry => entry.Version != null).ToList();
                                if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                                {
                                    GetComplianceDocumentsView entityData = new GetComplianceDocumentsView();
                                    entityData.Version = "1.0";
                                    entityData.ScheduledOnID = Convert.ToInt64(commandArg[0]);
                                    entitiesData.Add(entityData);
                                }

                                if (entitiesData.Count > 0)
                                {
                                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                    string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                    string directoryPath = "~/TempDocuments/AWS/" + User;
                                    if (!Directory.Exists(directoryPath))
                                    {
                                        Directory.CreateDirectory(Server.MapPath(directoryPath));
                                    }

                                    foreach (var file in CMPDocuments)
                                    {
                                        using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                        {
                                            GetObjectRequest request = new GetObjectRequest();
                                            request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                            request.Key = file.FileName;
                                            GetObjectResponse response = client.GetObject(request);
                                            response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                                        }
                                        string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);
                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string extension = System.IO.Path.GetExtension(file.FileName);

                                            if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                            {
                                                lblMessage.Text = "";
                                                lblMessage.Text = "Zip file can't view please download it";
                                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);
                                            }
                                            else
                                            {
                                                string filePath1 = directoryPath + "/" + file.FileName;
                                                CompDocReviewPath = filePath1;
                                                CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileLicense('" + CompDocReviewPath + "');", true);                                                
                                                lblMessage.Text = "";

                                            }
                                        }
                                        else
                                        {
                                            lblMessage.Text = "There is no file to preview";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileLicense();", true);
                                        }
                                        break;
                                    }
                                }
                            }
                            #endregion
                        }
                        else
                        {
                            #region Normal Storage
                            List<GetComplianceDocumentsView> CMPDocuments = DocumentManagement.GetFileDataView(Convert.ToInt32(commandArgs[0]), Convert.ToInt32(commandArgs[2])).ToList();

                            if (CMPDocuments != null)
                            {
                                List<GetComplianceDocumentsView> entitiesData = CMPDocuments.Where(entry => entry.Version != null).ToList();
                                if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                                {
                                    GetComplianceDocumentsView entityData = new GetComplianceDocumentsView();
                                    entityData.Version = "1.0";
                                    entityData.ScheduledOnID = Convert.ToInt64(commandArg[0]);
                                    entitiesData.Add(entityData);
                                }

                                if (entitiesData.Count > 0)
                                {
                                    foreach (var file in CMPDocuments)
                                    {
                                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string Folder = "~/TempFiles";
                                            string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                            string DateFolder = Folder + "/" + File;
                                            string extension = System.IO.Path.GetExtension(filePath);

                                            if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                            {
                                                lblMessage.Text = "";
                                                lblMessage.Text = "Zip file can't view please download it";
                                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);
                                            }
                                            else
                                            {
                                                Directory.CreateDirectory(Server.MapPath(DateFolder));

                                                if (!Directory.Exists(DateFolder))
                                                {
                                                    Directory.CreateDirectory(Server.MapPath(DateFolder));
                                                }
                                                string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

                                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                                string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                                string FileName = DateFolder + "/" + User + "" + extension;

                                                FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                                BinaryWriter bw = new BinaryWriter(fs);
                                                if (file.EnType == "M")
                                                {
                                                    bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                else
                                                {
                                                    bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                bw.Close();
                                                CompDocReviewPath = FileName;
                                                CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileLicense('" + CompDocReviewPath + "');", true);
                                                lblMessage.Text = "";

                                            }
                                        }
                                        else
                                        {
                                            lblMessage.Text = "There is no file to preview";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileLicense();", true);
                                        }
                                        break;
                                    }
                                }
                            }
                            #endregion
                        }

                        #endregion
                    }
                    else if (ddlComplianceType.SelectedItem.Text == "Internal")
                    {
                        #region Internal
                        string[] commandArg = e.CommandArgument.ToString().Split(',');
                        var AWSData = AmazonS3.GetAWSStorageDetail(AuthenticationHelper.CustomerID);
                        if (AWSData != null)
                        {
                            #region AWS Storage
                            List<GetLicenseInternalComplianceDocumentsView> CMPDocuments = GetFileDataInternalView(Convert.ToInt32(commandArgs[0]), Convert.ToInt32(commandArgs[2])).ToList();
                            if (CMPDocuments != null)
                            {
                                List<GetLicenseInternalComplianceDocumentsView> entitiesData = CMPDocuments.Where(entry => entry.Version != null).ToList();
                                if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                                {
                                    GetLicenseInternalComplianceDocumentsView entityData = new GetLicenseInternalComplianceDocumentsView();
                                    entityData.Version = "1.0";
                                    entityData.ScheduledOnID = Convert.ToInt64(commandArg[0]);
                                    entitiesData.Add(entityData);
                                }

                                if (entitiesData.Count > 0)
                                {
                                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                    string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                    string directoryPath = "~/TempDocuments/AWS/" + User;
                                    if (!Directory.Exists(directoryPath))
                                    {
                                        Directory.CreateDirectory(Server.MapPath(directoryPath));
                                    }

                                    foreach (var file in CMPDocuments)
                                    {
                                        using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                        {
                                            GetObjectRequest request = new GetObjectRequest();
                                            request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                            request.Key = file.FileName;
                                            GetObjectResponse response = client.GetObject(request);
                                            response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                                        }
                                        string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);
                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string extension = System.IO.Path.GetExtension(file.FileName);

                                            if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                            {
                                                lblMessage.Text = "";
                                                lblMessage.Text = "Zip file can't view please download it";
                                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);
                                            }
                                            else
                                            {
                                                string filePath1 = directoryPath + "/" + file.FileName;
                                                CompDocReviewPath = filePath1;
                                                CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileLicense('" + CompDocReviewPath + "');", true);
                                                lblMessage.Text = "";

                                            }
                                        }
                                        else
                                        {
                                            lblMessage.Text = "There is no file to preview";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileLicense();", true);
                                        }
                                        break;
                                    }
                                }
                            }
                            #endregion
                        }
                        else
                        {
                            #region Normal Storage
                            List<GetLicenseInternalComplianceDocumentsView> CMPDocuments = GetFileDataInternalView(Convert.ToInt32(commandArgs[0]), Convert.ToInt32(commandArgs[2])).ToList();
                            if (CMPDocuments != null)
                            {
                                List<GetLicenseInternalComplianceDocumentsView> entitiesData = CMPDocuments.Where(entry => entry.Version != null).ToList();
                                if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                                {
                                    GetLicenseInternalComplianceDocumentsView entityData = new GetLicenseInternalComplianceDocumentsView();
                                    entityData.Version = "1.0";
                                    entityData.ScheduledOnID = Convert.ToInt64(commandArg[0]);
                                    entitiesData.Add(entityData);
                                }

                                if (entitiesData.Count > 0)
                                {
                                    foreach (var file in CMPDocuments)
                                    {
                                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string Folder = "~/TempFiles";
                                            string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                            string DateFolder = Folder + "/" + File;
                                            string extension = System.IO.Path.GetExtension(filePath);

                                            if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                            {
                                                lblMessage.Text = "";
                                                lblMessage.Text = "Zip file can't view please download it";
                                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);
                                            }
                                            else
                                            {
                                                Directory.CreateDirectory(Server.MapPath(DateFolder));
                                                if (!Directory.Exists(DateFolder))
                                                {
                                                    Directory.CreateDirectory(Server.MapPath(DateFolder));
                                                }
                                                string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);
                                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                                string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;
                                                string FileName = DateFolder + "/" + User + "" + extension;
                                                FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                                BinaryWriter bw = new BinaryWriter(fs);
                                                if (file.EnType == "M")
                                                {
                                                    bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                else
                                                {
                                                    bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                bw.Close();
                                                CompDocReviewPath = FileName;
                                                CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileLicense('" + CompDocReviewPath + "');", true);
                                                lblMessage.Text = "";

                                            }
                                        }
                                        else
                                        {
                                            lblMessage.Text = "There is no file to preview";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileLicense();", true);
                                        }
                                        break;
                                    }
                                }
                            }
                            #endregion
                        }
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvErrorLicenseListPage.IsValid = false;
                cvErrorLicenseListPage.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rptComplianceVersionView_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                // LinkButton btnComplinceVersionDocView = (LinkButton)e.Item.FindControl("btnComplinceVersionDocView");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                //scriptManager.RegisterPostBackControl(btnComplinceVersionDocView);

                LinkButton lblDocumentVersionView = (LinkButton)e.Item.FindControl("lblDocumentVersionView");
                scriptManager.RegisterAsyncPostBackControl(lblDocumentVersionView);
            }
        }
    }
}