﻿<%@ Page Title="My Documents" Language="C#" MasterPageFile="~/LicenseManagement.Master" AutoEventWireup="true" CodeBehind="MyDocumentLicense.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.LicenseManagement.Documents.MyDocumentLicense" %>

<asp:Content ID="Content3" ContentPlaceHolderID="head" runat="server">
    <script src="/Newjs/tag-scrolling.js" type="text/javascript"></script>
    <link href="/NewCSS/tag-scrolling.css" rel="stylesheet" />

    <script type="text/javascript">
        $(document).ready(function () {
            setactivemenu('leftdocumentmenu');
            fhead('My Documents');
            $('input[id*=lstBoxFileTags]').hide();
            $('#AdvanceSearch').on('shown.bs.modal', function () {
                $('#divBranches').hide("blind", null, 100, function () { });
                document.getElementById('<%= tbxFilterLocation.ClientID %>').click();
            });
        });


        function OpenDocSharePopup(FID, CID, LicenseTitle, LicenseNo) {
            var codeTitle = encodeURIComponent(LicenseTitle);
            $('#divDocumentsharePopup').modal('show');
            $('#ContentPlaceHolder1_Iframe_Docshare').attr('src', "../../Litigation/Common/ShareDocsLitigation.aspx?AccessID=" + FID + "&CaseInstanceID=" + CID + "&CaseTitle=" + codeTitle + "&UniqueNumber=" + LicenseNo + '&Page=License');
        }

        function CloseUploadShareDocumentPopup() {
            $('#divDocumentsharePopup').modal('hide');
        }

        function fopendocfileLicense(file) {
            $('#divViewDocument').modal('show');
            $('#ContentPlaceHolder1_docViewerLitigation').attr('src', "../../docviewer.aspx?docurl=" + file);
        }
        function ShowViewDocument() {
            $('#divViewDocument').modal('show');
            return true;
        };

        function OpenDocumentOverviewpup(scheduledonid, Flag) {
            $('#divOverView').modal('show');
            $('#OverViews').attr('width', '500px');
            $('#OverViews').attr('height', '300px');
            $('.modal-dialog').css('width', '550px');
            if (Flag == 'Statutory') {
                
                $('#OverViews').attr('src', "/Common/LicDocumentOverview.aspx?ScheduleID=" + scheduledonid + "&IsFlag=" + Flag);
            }
            else {
                $('#OverViews').attr('src', "/Common/LicDocumentOverviewInternal.aspx?ScheduleID=" + scheduledonid + "&IsFlag=" + Flag);
            }
        }

    </script>

    <style>
        .tag .label {
            font-size: 100%;
        }

        .label-info, .label-info-selected {
            font-size: 100%;
        }

            .label-info:active, .label-info:focus, .label-info:hover {
                color: #007aff;
                border: 1px solid;
                border-color: #007aff;
                background: 0 0;
            }

        .label-info-selected {
            color: #007aff;
            border: 1px solid;
            border-color: #007aff;
            background: 0 0;
        }
    </style>
    <style type="text/css">
        .form-control {
            height: 33px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row Dashboard-white-widget">
        <div class="dashboard">

            <div class="col-md-12 colpadding0">
                <asp:ValidationSummary ID="vsLicenseListPage" runat="server" Display="none" class="alert alert-block alert-danger fade in" Style="padding-left: 5%"
                    ValidationGroup="LicenseListPageValidationGroup" />
                <asp:CustomValidator ID="cvErrorLicenseListPage" runat="server" EnableClientScript="False"
                    ValidationGroup="LicenseListPageValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
            </div>

            <div class="row">
                <div class="col-md-12 colpadding0">
                    <div class="col-md-1 colpadding0" style="padding-right: 3px;">
                        <label for="lblLicenseType" class="filter-label">Show</label>
                        <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 100%; float: right; height: 32px !important; margin-right: 6%"
                            AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                            <asp:ListItem Text="5" />
                            <asp:ListItem Text="10" Selected="True" />
                            <asp:ListItem Text="20" />
                            <asp:ListItem Text="50" />
                        </asp:DropDownList>

                    </div>
                   <div class="col-md-1 colpadding0"  style="width: 10.5%;">
                        <label for="tbxFilterLocation" class="filter-label">Type</label>
                        <asp:DropDownList runat="server" ID="ddlComplianceType" class="form-control" Style="width: 110px;"
                            OnSelectedIndexChanged="ddlComplianceType_SelectedIndexChanged" AutoPostBack="true">
                            <asp:ListItem Text="Statutory" Value="-1" />
                            <asp:ListItem Text="Internal" Value="0" />
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-3 colpadding0" style="width: 25%;">
                        <asp:UpdatePanel ID="upDivLocation" runat="server" UpdateMode="Conditional" OnLoad="upDivLocation_Load">
                            <ContentTemplate>
                                <label for="tbxFilterLocation" class="filter-label">Entity/Branch/Location</label>
                                <asp:TextBox runat="server" ID="tbxFilterLocation" PlaceHolder="Click to Select" autocomplete="off" CssClass="form-control" Width="100%" />
                                <div style="margin-left: 1px; position: absolute; z-index: 10; overflow-y: auto; height: 200px; width: 100%" id="divFilterLocation">
                                    <asp:TreeView runat="server" ID="tvFilterLocation" SelectedNodeStyle-Font-Bold="true" NodeStyle-ForeColor="#8e8e93"
                                        Style="margin-top: -15px; overflow: auto; border-left: 1px solid #c7c7cc; border-right: 1px solid #c7c7cc; border-bottom: 1px solid #c7c7cc; background-color: #ffffff; color: #8e8e93 !important;"
                                        ShowLines="true"
                                        OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged">
                                    </asp:TreeView>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                   <div class="col-md-3 colpadding0" style="width: 25%; padding-left:10px;">
                        <label for="lblLicenseType" class="filter-label">License Type</label>
                        <asp:DropDownListChosen runat="server" ID="ddlLicenseType" AllowSingleDeselect="false" DisableSearchThreshold="3"
                            class="form-control" Width="100%" />
                    </div>
                     <div class="col-md-2 colpadding0" style="width: 12%;  padding-left:10px;">
                        <label for="ddlStatus" class="filter-label">Status</label>
                        <asp:DropDownListChosen runat="server" ID="ddlLicenseStatus" AllowSingleDeselect="false" DisableSearchThreshold="5"
                            class="form-control" Width="100%">
                        </asp:DropDownListChosen>
                    </div>
                    <div class="col-md-2 colpadding0 text-right" style="width: 10%; padding-left:10px;">
                        <div class="col-md-6 colpadding0 float-left">
                            <label for="ddlStatus" class="hidden-label">Filter</label>
                            <asp:LinkButton Text="Apply" CssClass="btn btn-primary" runat="server" ID="lnkBtnApplyFilter"
                                OnClick="lnkBtnApplyFilter_Click" OnClientClick="javascript:$('#updateProgress').show()" />
                        </div>
                         <div class="col-md-6 colpadding0 float-right">
                            <label for="ddlStatus" class="hidden-label">Filter</label>
                        </div>                       
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 colpadding0">
                    <asp:GridView runat="server" ID="grdLicenseList" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                        PageSize="10" AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%" DataKeyNames="LicenseID"
                        OnRowCommand="grdLicenseList_RowCommand" OnRowDataBound="grdLicenseList_RowDataBound" OnSorting="grdLicenseList_Sorting" OnRowCreated="grdLicenseList_RowCreated">
                        <Columns>
                            <asp:TemplateField HeaderText="Location" ItemStyle-Width="10%">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                        <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("CustomerBrach") %>' ToolTip='<%# Eval("CustomerBrach") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="License Type" ItemStyle-Width="20%" SortExpression="LicensetypeName">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px">
                                        <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("LicensetypeName") %>' ToolTip='<%# Eval("LicensetypeName") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="License No." ItemStyle-Width="10%" SortExpression="LicenseNo">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                        <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("LicenseNo") %>' ToolTip='<%# Eval("LicenseNo") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Title" ItemStyle-Width="12%">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                        <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("LicenseTitle") %>' ToolTip='<%# Eval("LicenseTitle") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="App Due Date" ItemStyle-Width="15%" SortExpression="ApplicationDate">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px">
                                        <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ApplicationDate") != DBNull.Value ? Convert.ToDateTime(Eval("ApplicationDate")).ToString("dd-MM-yyyy") : "" %>'
                                            ToolTip='<%# Eval("ApplicationDate") != DBNull.Value ? Convert.ToDateTime(Eval("ApplicationDate")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="End Date" ItemStyle-Width="10%" SortExpression="EndDate">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                        <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("EndDate") != null ? Convert.ToDateTime(Eval("EndDate")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Status" ItemStyle-Width="8%" SortExpression="Status">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                        <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Status") %>' ToolTip='<%# Eval("Status") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Action" HeaderStyle-CssClass="text-right" ItemStyle-CssClass="text-right" ItemStyle-Width="13%">
                                <ItemTemplate>
                                    <asp:UpdatePanel ID="upDownloadFile" runat="server">
                                        <ContentTemplate>
                                              <asp:ImageButton ID="lblLinkButton" runat="server" ImageUrl="~/img/icon-download.png"
                                                CommandArgument='<%# Eval("ComplianceScheduleOnID") %>' CommandName="ADDLINK"
                                                data-toggle="tooltip" data-placement="bottom" ToolTip="Click here for Show Link Document(s)"></asp:ImageButton>

                                            <asp:ImageButton ID="lblDownLoadfile" runat="server" ImageUrl="~/img/icon-download.png"
                                                CommandArgument='<%# Eval("ComplianceScheduleOnID") %>' CommandName="Download"
                                                data-toggle="tooltip" data-placement="bottom" ToolTip="Download Document(s)"></asp:ImageButton>
                                            <asp:ImageButton ID="lblViewfile" runat="server" ImageUrl="~/img/view-doc.png"
                                                CommandArgument='<%# Eval("ComplianceScheduleOnID") %>' CommandName="View"
                                                data-toggle="tooltip" data-placement="bottom" ToolTip="View Document(s)"></asp:ImageButton>

                                            <asp:ImageButton ID="lblSharefile" runat="server" ImageUrl="~/Images/share-icons.png" 
                                            CommandArgument='<%#Eval("FileID")+","+ Eval("LicenseID")+","+ Eval("Licensetitle") +","+ Eval("LicenseNo")   %>' CommandName="Share"
                                            data-toggle="tooltip" data-placement="bottom" ToolTip="Share Document(s)"></asp:ImageButton>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="lblDownLoadfile" />
                                            <asp:PostBackTrigger ControlID="lblViewfile" />
                                             <asp:PostBackTrigger ControlID="lblLinkButton" />
                                            <asp:PostBackTrigger ControlID="lblSharefile" />
                                        </Triggers>

                                    </asp:UpdatePanel>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <RowStyle CssClass="clsROWgrid" />
                        <HeaderStyle CssClass="clsheadergrid" />
                        <PagerSettings Visible="false" />
                        <PagerTemplate>
                        </PagerTemplate>
                        <EmptyDataTemplate>
                            <div class="row align-middle">No Record Found</div>
                        </EmptyDataTemplate>
                    </asp:GridView>
                </div>
            </div>
            <div class="col-md-12 colpadding0">
                <div class="col-md-10 colpadding0">
                    <div runat="server" id="DivRecordsScrum" style="float: left; margin-top: 5px; color: #999">
                        <p style="padding-right: 0px !Important;">
                            <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>- 
                        <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                        <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                        </p>
                    </div>
                </div>
                <div class="col-md-1 colpadding0">
                </div>


                <div class="col-md-1 colpadding0">
                    <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true" DataPlaceHolder="No" AllowSingleDeselect="false"
                        OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged" class="form-control m-bot15" Width="100%" Height="32px">
                    </asp:DropDownListChosen>
                </div>
            </div>
            <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
        </div>
    </div>


    <div>
           <div class="modal fade" id="divOverView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                <div class="modal-dialog" style="width: 1150px;">
                    <div class="modal-content" style="width: 100%;">
                        <div class="modal-header" style="border-bottom: none;">
                            <button type="button" class="close" data-dismiss="modal" onclick="CloseClearOV();" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body">
                            <iframe id="OverViews" src="about:blank" width="1200px" height="100%" frameborder="0"></iframe>
                        </div>
                    </div>
                </div>
            </div>

        <div class="modal fade" id="divViewDocument" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
            <div class="modal-dialog" style="width: 90%;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body" style="height: 570px;">
                        <div class="col-md-12 colpadding0">
                            <asp:Label runat="server" ID="lblMessage" Style="color: red;"></asp:Label>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12 colpadding0">
                            <div class="col-md-2 colpadding0">
                                <table class="col-md-12">
                                    <thead>
                                        <tr>
                                            <td valign="top">
                                                <asp:UpdatePanel ID="upLitigationDetails" runat="server" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <asp:Repeater ID="rptComplianceVersionView" runat="server" OnItemCommand="rptComplianceVersionView_ItemCommand"
                                                            OnItemDataBound="rptComplianceVersionView_ItemDataBound">
                                                            <HeaderTemplate>
                                                                <table id="tblComplianceDocumnets">
                                                                    <thead>
                                                                        <th><b>File Name</b></th>
                                                                    </thead>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td>
                                                                        <asp:LinkButton CommandName="View" CommandArgument='<%# Eval("ScheduledOnID") + ","+ Eval("Version")+ ","+ Eval("FileID") %>' ID="lblDocumentVersionView"
                                                                            runat="server" ToolTip='<%# Eval("FileName")%>' Text='<%# Eval("Version") +" "+ Eval("FileName").ToString().Substring(0,4) %>'></asp:LinkButton>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                </table>
                                                            </FooterTemplate>
                                                        </asp:Repeater>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="rptComplianceVersionView" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                            <div class="col-md-10 colpadding0">
                                <fieldset style="height: 550px; width: 100%;">
                                    <iframe src="about:blank" id="docViewerLitigation" runat="server" width="100%" height="530px"></iframe>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="divDocumentsharePopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog p5" style="width: 82%;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label class="modal-header-custom">
                                Add Sharing Details</label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                            <iframe src="about:blank" id="Iframe_Docshare" frameborder="0" runat="server" width="100%" height="600px"></iframe>
                        </div>
                    </div>
                </div>
            </div>

</asp:Content>
