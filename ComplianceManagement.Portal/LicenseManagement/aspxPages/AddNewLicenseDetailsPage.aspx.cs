﻿using Amazon;
using Amazon.S3;
using Amazon.S3.IO;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.AWS;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.License;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.LicenseManagement.aspxPages
{
    public partial class AddNewLicenseDetailsPage : System.Web.UI.Page
    {
        protected long customerID = 0;
        public static List<int> locationList = new List<int>();
        protected static string user_Roles;
        //bool saveSuccess = false;
        protected long loggedInUserId = AuthenticationHelper.UserID;
        protected void Page_Load(object sender, EventArgs e)
        {

            customerID = Convert.ToInt64(AuthenticationHelper.CustomerID);
            try
            {
                if (!IsPostBack)
                {
                    user_Roles = AuthenticationHelper.Role;
                    BindLicenseType();
                   // BindCompliance(null);
                    BindUsers();
                    string  IsActiveCustomer = Convert.ToString(ConfigurationManager.AppSettings["IsLicensePermanantActive"]);

                    if (IsActiveCustomer == Convert.ToString(AuthenticationHelper.CustomerID))
                    {
                        divIsActive.Visible = true;
                    }
                    
                    var branchList = CustomerBranchManagement.GetAllHierarchy(Convert.ToInt32(customerID));
                    BindCustomerBranches(tvFilterLocation, tbxFilterLocation, branchList);


                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divBranches');", tbxFilterLocation.ClientID), true);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "hideDivTree", "hideDivBranch();", true);

                }

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divBranches');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "hideDivTree", "hideDivBranch();", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindLicenseType()
        {
            try
            {
                 List<Lic_tbl_LicenseType_Master> data = new List<Lic_tbl_LicenseType_Master>();
                //data = LicenseTypeMasterManagement.GetLicenseType();
                if (string.IsNullOrEmpty(user_Roles))
                {
                    user_Roles = AuthenticationHelper.Role;
                }
                if (user_Roles.Contains("CADMN") || user_Roles.Contains("IMPT"))
                {
                    data = LicenseTypeMasterManagement.GetLicenseType();
                }
                else
                {
                    data = LicenseTypeMasterManagement.GetUserWiseLicenseType(AuthenticationHelper.UserID);
                }

                ddlLicenseType.DataTextField = "Name";
                ddlLicenseType.DataValueField = "ID";
                ddlLicenseType.DataSource = data;
                ddlLicenseType.DataBind();
                ddlLicenseType.Items.Insert(0, new ListItem("License Type Name", "-1"));

            }
            catch(Exception ex)
            {
               
            }

        }
        protected void ddlLicenseType_SelectedIndexChanged(object sender, EventArgs e)
        {
            string LicenseType = Convert.ToString(ddlLicenseType.SelectedItem);
            string status = string.Empty;
            
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                status = (from row in entities.Lic_tbl_type
                          where LicenseType.Contains(row.Status)
                          select row.Status).FirstOrDefault();

            }
            if (LicenseType == status)
            {
                divIsRegister.Visible = true;
            }
            else
            {
                divIsRegister.Visible = false;
            }
            BindCompliance(LicenseType);
        }


        private void BindCompliance(string LicenseType)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    ddlCompliance.DataSource = null;
                    ddlCompliance.DataBind();

                    var query = (from LTLCM in entities.Lic_tbl_LicType_Compliance_Mapping
                                 join LLM in entities.Lic_tbl_LicenseType_Master
                                 on LTLCM.LicenseTypeID equals LLM.ID
                                 join C in entities.Compliances
                                 on LTLCM.ComplianceID equals C.ID
                                 where C.Status == null && C.IsDeleted == false
                                 select new
                                 {
                                     C.ID,
                                     C.ShortDescription,
                                     LLM.Name

                                 }).ToList();

                    if (query.Count > 0)
                    {
                        if (LicenseType != null)
                        {
                            query = query.Where(entry => entry.Name == LicenseType).ToList();
                        }

                        var result = query.Select(entry => new { entry.ID, entry.ShortDescription }).ToList();


                        ddlCompliance.DataTextField = "ShortDescription";
                        ddlCompliance.DataValueField = "ID";
                        ddlCompliance.DataSource = result;
                        ddlCompliance.DataBind();
                        ddlCompliance.Items.Insert(0, new ListItem("Select Compliance", "-1"));
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }

        }

        private void BindUsers(List<long> ids = null)
        {
            try
            {
                int customerID = -1;
               customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
              

                ddlPerformer.DataTextField = "Name";
                ddlPerformer.DataValueField = "ID";


                var users = UserManagement.GetAllNVP(customerID, ids: ids, Flags: false);

                ddlPerformer.DataSource = users;
                ddlPerformer.DataBind();
                ddlPerformer.Items.Insert(0, new ListItem("Select Performer", "-1"));

                ddlReviewer.DataTextField = "Name";
                ddlReviewer.DataValueField = "ID";

                ddlReviewer.DataSource = users;
                ddlReviewer.DataBind();
                ddlReviewer.Items.Insert(0, new ListItem("Select Reviewer", "-1"));

                ddlApprover.DataTextField = "Name";
                ddlApprover.DataValueField = "ID";

                ddlApprover.DataSource = users;
                ddlApprover.DataBind();
                ddlApprover.Items.Insert(0, new ListItem("Select Approver", "-1"));
            }

            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnlocation_Click(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "hideDivTree", "hideDivBranch();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
        }

        private void BindCustomerBranches(TreeView treetoBind, TextBox treeTxtBox, List<NameValueHierarchy> branchList)
        {
            try
            {
                treetoBind.Nodes.Clear();
                NameValueHierarchy branch = null;
                if (branchList.Count > 0)
                {
                    branch = branchList[0];
                }
                treeTxtBox.Text = "Select Entity/Branch/Location";
                List<TreeNode> nodes = new List<TreeNode>();
                BindBranchesHierarchy(null, branch, nodes);
                foreach (TreeNode item in nodes)
                {
                    treetoBind.Nodes.Add(item);
                }
                treetoBind.CollapseAll();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
        }

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp, List<TreeNode> nodes)
        {
            try
            {
                if (nvp != null)
                {
                    foreach (var item in nvp.Children)
                    {
                        TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                        BindBranchesHierarchy(node, item, nodes);
                        if (parent == null)
                        {
                            nodes.Add(node);
                        }
                        else
                        {
                            parent.ChildNodes.Add(node);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxFilterLocation.Text = tvFilterLocation.SelectedNode != null ? tvFilterLocation.SelectedNode.Text : "Entity/Sub-Entity/Location";
               
               // ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divBranches');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "hideDivTree", "hideDivBranch();", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnSaveLicense_Click(object sender, EventArgs e)
        {
            try
            {
                bool formValidateSuccess = false;
                bool TempassignmentsaveSuccess = false;
                bool saveSuccess = false;
                bool CompliancesEntrySuccess = false;
                List<string> lstErrorMsg = new List<string>();
                int branchID = -1;

                #region Validation for Start Date End Date

                if (!string.IsNullOrEmpty(txtStartDate.Text))
                {
                    try
                    {
                        bool check = LicenseCommonMethods.CheckValidDate(txtStartDate.Text.Trim());
                        if (!check)
                        {
                            lstErrorMsg.Add("Please Check Start Date or Date should be in DD-MM-YYYY Format");
                        }
                    }
                    catch (Exception)
                    {
                        lstErrorMsg.Add("Please Check Start Date or Date should be in DD-MM-YYYY Format");
                    }
                }

                if (!string.IsNullOrEmpty(txtEndDate.Text))
                {
                    try
                    {
                        bool check = LicenseCommonMethods.CheckValidDate(txtEndDate.Text.Trim());
                        if (!check)
                        {
                            lstErrorMsg.Add("Please Check Expiry Date or Date should be in DD-MM-YYYY Format");
                        }
                    }
                    catch (Exception)
                    {
                        lstErrorMsg.Add("Please Check Expiry Date or Date should be in DD-MM-YYYY Format");
                    }
                }

                if ((!string.IsNullOrEmpty(txtStartDate.Text)) && (!string.IsNullOrEmpty(txtEndDate.Text)))
                {
                    if (DateTime.Compare(DateTimeExtensions.GetDate(txtEndDate.Text.ToString()), DateTimeExtensions.GetDate(txtStartDate.Text.ToString())) <= 0)
                    {
                        lstErrorMsg.Add("Expiry Date should be greater than Start Date.");
                    }
                }


                if (lstErrorMsg.Count > 0)
                {
                    formValidateSuccess = false;
                    showErrorMessages(lstErrorMsg, cvLicPopUp);
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "scrollUpPageScript", "scrollUpPage();", true);
                }
                else
                    formValidateSuccess = true;

                #endregion


                HttpFileCollection LicenseFileUpload1 = Request.Files;
                // HttpFileCollection fileCollection = Request.Files;

                if (LicenseFileUpload1.Count > 0)
                {
                    for (int i = 0; i < LicenseFileUpload1.Count; i++)
                    {
                        HttpPostedFile uploadedFile = LicenseFileUpload1[i];
                        string[] validFileTypes = { "exe", "bat", "dll", "css", "js", };
                        string ext = System.IO.Path.GetExtension(uploadedFile.FileName);
                        if (ext == "")
                        {
                            //lstErrorMsg.Add("Please do not upload virus file or blank files or file has no extention.");
                        }
                        else
                        {
                            for (int j = 0; j < validFileTypes.Length; j++)
                            {
                                if (ext == "." + validFileTypes[j])
                                {
                                    lstErrorMsg.Add("Please do not upload virus file or blank files.");
                                    break;
                                }
                            }


                            if (uploadedFile.ContentLength > 0)
                            {
                            }
                            else
                            {
                                lstErrorMsg.Add("Please do not upload virus file or blank file =>" + uploadedFile.FileName);
                            }

                        }
                    }

                }

                #region Basic Validation

                bool existLicNo = LicenseMgmt.ExistsLicenseNo(Convert.ToInt32(AuthenticationHelper.CustomerID), txtLicenseNo.Text);
                if (!existLicNo)
                {

                }
                else
                {
                    lstErrorMsg.Add("License Details with Same License Number already exists");
                    //cvLicPopUp.IsValid = false;
                    //cvLicPopUp.ErrorMessage = "License Details with Same License Number already exists";
                    //VSLicPopup.CssClass = "alert alert-danger";
                    //formValidateSuccess = false;
                }

                locationList.Clear();
                int perID = -1;
                int RevID = -1;

                if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1" && tvFilterLocation.SelectedNode.Text != "Entity/Sub-Entity/Location")
                {
                    branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                }
                else
                {
                    lstErrorMsg.Add("Please select at least one 'Entity/Sub-Entity/Location' ");
                }

                if ((string.IsNullOrEmpty(ddlPerformer.SelectedValue)) || (ddlPerformer.SelectedValue == "-1"))
                {
                    lstErrorMsg.Add("Please select 'Performer' ");
                }
                else
                {
                    perID = Convert.ToInt32(ddlPerformer.SelectedValue);
                }
                if ((string.IsNullOrEmpty(ddlReviewer.SelectedValue)) || (ddlReviewer.SelectedValue == "-1"))
                {

                    lstErrorMsg.Add("Please select 'Reviewer' ");
                }
                else
                {
                    RevID = Convert.ToInt32(ddlReviewer.SelectedValue);
                }

                if (lstErrorMsg.Count > 0)
                {
                    formValidateSuccess = false;
                    showErrorMessages(lstErrorMsg, cvLicPopUp);
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "scrollUpPageScript", "scrollUpPage();", true);
                }
                else
                    formValidateSuccess = true;


                #endregion

                if (formValidateSuccess == true)
                {
                    long appr = -1;
                    int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    List<TempAssignmentTable> Tempassignments = new List<TempAssignmentTable>();
                    if (!string.IsNullOrEmpty(ddlApprover.SelectedValue) && ddlApprover.SelectedValue != "-1")
                    {
                        appr = Convert.ToInt32(ddlApprover.SelectedValue);

                    }
                    else
                    {
                        appr = -1;
                    }
                    #region Compliance Assignment



                    if (ddlPerformer.SelectedValue != null)
                    {
                        TempAssignmentTable TempAssP = new TempAssignmentTable();
                        TempAssP.ComplianceId = Convert.ToInt32(ddlCompliance.SelectedValue);
                        TempAssP.CustomerBranchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                        TempAssP.RoleID = RoleManagement.GetByCode("PERF").ID;
                        //TempAssP.UserID = Convert.ToInt32(ddlPerformer.SelectedValue);
                        TempAssP.UserID = perID;
                        TempAssP.IsActive = true;
                        TempAssP.CreatedOn = DateTime.Now;
                        TempAssP.DepartmentID = null;

                        Tempassignments.Add(TempAssP);
                    }
                    if (ddlReviewer.SelectedValue != null)
                    {
                        TempAssignmentTable TempAssR = new TempAssignmentTable();
                        TempAssR.ComplianceId = Convert.ToInt32(ddlCompliance.SelectedValue);
                        TempAssR.CustomerBranchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                        TempAssR.RoleID = RoleManagement.GetByCode("RVW1").ID;
                        //TempAssR.UserID = Convert.ToInt32(ddlReviewer.SelectedValue);
                        TempAssR.UserID = RevID;
                        TempAssR.IsActive = true;
                        TempAssR.CreatedOn = DateTime.Now;
                        TempAssR.DepartmentID = null;

                        Tempassignments.Add(TempAssR);

                    }
                    if (ddlApprover.SelectedValue != null)
                    {
                        TempAssignmentTable TempAssA = new TempAssignmentTable();
                        TempAssA.ComplianceId = Convert.ToInt32(ddlCompliance.SelectedValue);
                        TempAssA.CustomerBranchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                        TempAssA.RoleID = RoleManagement.GetByCode("APPR").ID;
                        TempAssA.UserID = appr;
                        TempAssA.IsActive = true;
                        TempAssA.CreatedOn = DateTime.Now;
                        TempAssA.DepartmentID = null;

                        Tempassignments.Add(TempAssA);

                    }


                    if (Tempassignments.Count != 0)
                    {
                        TempassignmentsaveSuccess = Business.ComplianceManagement.AddDetailsTempAssignmentTableLicense(Tempassignments);

                    }

                    #endregion

                    #region License Compliance Activation
                    if (TempassignmentsaveSuccess)
                    {

                        #region Save License Instance and Status
                        //for (int i = 0; i < locationList.Count; i++)
                        //{
                        long newLicenseID = 0;
                        Lic_tbl_LicenseInstance licenseRecord = new Lic_tbl_LicenseInstance()
                        {
                            CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID),
                            IsDeleted = false,
                            CreatedBy = loggedInUserId
                        };
                        Lic_tbl_LicenseInstance_Log newLic_tbl_LicenseInstance_Log = new Lic_tbl_LicenseInstance_Log()
                        {
                            CustomerID = licenseRecord.CustomerID,
                            CreatedBy = loggedInUserId,
                        };
                        //LicenseNo
                        if (!string.IsNullOrEmpty(txtLicenseNo.Text.Trim()))
                        {
                            licenseRecord.LicenseNo = Convert.ToString(txtLicenseNo.Text.Trim());
                            newLic_tbl_LicenseInstance_Log.LicenseNo = Convert.ToString(txtLicenseNo.Text.Trim());
                        }
                        //LicenseTitle
                        if (!string.IsNullOrEmpty(txtTitle.Text.Trim()))
                        {
                            licenseRecord.LicenseTitle = Convert.ToString(txtTitle.Text.Trim());
                            newLic_tbl_LicenseInstance_Log.LicenseTitle = Convert.ToString(txtTitle.Text.Trim());
                        }


                        //licenseRecord.CustomerBranchID = locationList[i];
                        if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1")
                        {
                            licenseRecord.CustomerBranchID = Convert.ToInt32(tvFilterLocation.SelectedValue);

                        }

                        //LicenseTypeID
                        if (!string.IsNullOrEmpty(ddlLicenseType.SelectedValue))
                        {
                            licenseRecord.LicenseTypeID = Convert.ToInt32(ddlLicenseType.SelectedValue);
                            newLic_tbl_LicenseInstance_Log.LicenseTypeID = Convert.ToInt32(ddlLicenseType.SelectedValue);
                        }
                        //StartDate
                        if (!string.IsNullOrEmpty(txtStartDate.Text))
                        {
                            licenseRecord.StartDate = DateTimeExtensions.GetDate(txtStartDate.Text);
                            newLic_tbl_LicenseInstance_Log.StartDate = DateTimeExtensions.GetDate(txtStartDate.Text);
                        }
                        //EndDate
                        if (!string.IsNullOrEmpty(txtEndDate.Text))
                        {
                            licenseRecord.EndDate = DateTimeExtensions.GetDate(txtEndDate.Text);
                            newLic_tbl_LicenseInstance_Log.EndDate = DateTimeExtensions.GetDate(txtEndDate.Text);
                        }
                        //RemindBeforeNoOfDays
                        if (!string.IsNullOrEmpty(txtApplicationDays.Text.Trim()))
                        {
                            licenseRecord.RemindBeforeNoOfDays = Convert.ToInt32(txtApplicationDays.Text);
                            newLic_tbl_LicenseInstance_Log.RemindBeforeNoOfDays = Convert.ToInt32(txtApplicationDays.Text);
                        }
                        else
                        {
                            licenseRecord.RemindBeforeNoOfDays = 0;
                            newLic_tbl_LicenseInstance_Log.RemindBeforeNoOfDays = 0;
                        }
                        licenseRecord.FileNO = txtfileno.Text;
                        licenseRecord.PhysicalLocation = txtfilelocation.Text;
                        //RecurringNoOfDays                       
                        licenseRecord.RecurringNoOfDays = 0;

                        if (!string.IsNullOrEmpty(txtCost.Text.Trim()))
                        {
                            licenseRecord.Cost = Convert.ToDecimal(txtCost.Text);
                            newLic_tbl_LicenseInstance_Log.Cost = Convert.ToDecimal(txtCost.Text);
                        }
                        else
                        {
                            licenseRecord.Cost = 0;
                            newLic_tbl_LicenseInstance_Log.Cost = 0;
                        }

                        //IsStatutory
                        bool IsStatutory = true;
                        IsStatutory = true;

                        if (DateTime.Compare((DateTime.Today.Date), DateTimeExtensions.GetDate(txtEndDate.Text.ToString())) >= 0)
                        {
                            divIsActive.Visible = false;
                            ChkIsActive.Checked = false;
                        }

                        if (ChkIsActive.Checked)
                        {
                            licenseRecord.IsPermanantActive = true;
                            newLic_tbl_LicenseInstance_Log.IsPermanantActive = true;
                        }

                        licenseRecord.IsStatutory = IsStatutory;
                        newLic_tbl_LicenseInstance_Log.IsStatutory = IsStatutory;

                        //bool existLicNo = LicenseMgmt.ExistsLicenseNo(Convert.ToInt32(AuthenticationHelper.CustomerID), licenseRecord.LicenseNo);
                        //if (!existLicNo)
                        //{

                        newLicenseID = LicenseMgmt.CreateLicense(licenseRecord);
                        newLic_tbl_LicenseInstance_Log.LicenseID = newLicenseID;
                        LicenseMgmt.CreateLicenseLog(newLic_tbl_LicenseInstance_Log);
                        int statusId = 0;
                        //start date and end date if not empty


                        if (ChkIsActive.Checked)
                        {
                            statusId = 2;            //Active
                        }
                        else
                        {

                            if (ChkIsRegister.Checked)
                            {
                                statusId = 9;    //Register
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(Convert.ToString(txtStartDate.Text)) || !string.IsNullOrEmpty(Convert.ToString(txtEndDate.Text)))
                                {
                                    if (DateTimeExtensions.GetDate(Convert.ToString(txtEndDate.Text)).Date <= DateTime.Today.Date)
                                    {
                                        statusId = 3;        //Expired
                                    }
                                    else if (DateTimeExtensions.GetDate(Convert.ToString(txtEndDate.Text)).Date <= DateTime.Today.Date.AddDays(30))
                                    {
                                        statusId = 4;            //Expiring
                                    }
                                    else if (DateTimeExtensions.GetDate(Convert.ToString(txtEndDate.Text)).Date > Convert.ToDateTime(DateTime.Now).Date)
                                    {
                                        statusId = 2;            //Active
                                    }
                                    else
                                    {
                                        statusId = 3;        //Expired
                                    }
                                }
                                else
                                    statusId = 1;       //Draft                            

                            }
                        }
                        if (newLicenseID > 0)
                        {
                            licenseRecord.ID = newLicenseID;
                            string status = string.Empty;
                            if (statusId == 2)
                            {
                                status = "Active";
                            }
                            else if (statusId == 3)
                            {
                                status = "Expired";
                            }
                            else if (statusId == 4)
                            {
                                status = "Expiring";
                            }
                            else if (statusId == 9)
                            {
                                status = "Registered";
                            }
                            //Lic_tbl_LicenseStatusTransaction newStatusRecord = new Lic_tbl_LicenseStatusTransaction()
                            //{
                            //    CustomerID = licenseRecord.CustomerID,
                            //    LicenseID = newLicenseID,
                            //    StatusID = statusId,
                            //    StatusChangeOn = DateTime.Now,
                            //    IsActive = true,
                            //    CreatedBy = AuthenticationHelper.UserID,
                            //    CreatedOn = DateTime.Now,
                            //    UpdatedBy = AuthenticationHelper.UserID,
                            //    UpdatedOn = DateTime.Now,
                            //    Remark = "New license created"
                            //};
                            //saveSuccess = LicenseMgmt.CreateLicenseStatusTransaction(newStatusRecord);

                            Lic_tbl_LicenseAudit_Log newLicenseInstance_Log = new Lic_tbl_LicenseAudit_Log()
                            {
                                CustomerID = licenseRecord.CustomerID,
                                LicenseID = newLicenseID,
                                StatusID = statusId,
                                IsActive = true,
                                CreatedBy = AuthenticationHelper.UserID,
                                Remark = "Change license status to " + status + ""
                            };
                            //StartDate
                            if (!string.IsNullOrEmpty(txtStartDate.Text))
                                newLicenseInstance_Log.StartDate = DateTimeExtensions.GetDate(txtStartDate.Text);

                            //EndDate
                            if (!string.IsNullOrEmpty(txtEndDate.Text))
                                newLicenseInstance_Log.EndDate = DateTimeExtensions.GetDate(txtEndDate.Text);

                            saveSuccess = LicenseMgmt.CreateLicenseAuditLog(newLicenseInstance_Log);
                        }
                        if (saveSuccess)
                        {
                            //cvLicPopUp.IsValid = false;
                            //cvLicPopUp.ErrorMessage = "License Details Created Successfully";
                           //VSLicPopup.CssClass = "alert alert-success";
                        }
                        #region Statutory Non Statutory Compliances Entry                       
                        try
                        {
                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                            {
                                long performerId = -1;
                                long reviewerId = -1;
                                long approverId = -1;
                                if (licenseRecord.ID > 0)
                                {
                                    //Statutory Type
                                    if (licenseRecord.IsStatutory == true)
                                    {
                                        #region Statutory
                                        List<long> lstExistingStatCompLst = LicenseMgmt.GetLicenseToComplianceInstanceMappingList(licenseRecord.ID);
                                        if (lstExistingStatCompLst.Count == 0)
                                        {
                                            try
                                            {
                                                long complianceInstanceId = 0;
                                                long complianceScheduleOnId = 0;
                                                long complianceTransactionId = 0;
                                                int ComplianeId = Convert.ToInt32(ddlCompliance.SelectedValue);

                                                var tempdetails = (from TAT in entities.TempAssignmentTables
                                                                   where TAT.ComplianceId == ComplianeId
                                                                   && TAT.CustomerBranchID == licenseRecord.CustomerBranchID
                                                                   && TAT.IsActive == true
                                                                   select TAT).ToList();
                                                if (tempdetails.Count > 0)
                                                {
                                                    performerId = (from pr in tempdetails
                                                                   where pr.RoleID == 3
                                                                   select pr.UserID).FirstOrDefault();

                                                    reviewerId = (from pr in tempdetails
                                                                  where pr.RoleID == 4
                                                                  select pr.UserID).FirstOrDefault();

                                                    approverId = (from pr in tempdetails
                                                                  where pr.RoleID == 6
                                                                  select pr.UserID).FirstOrDefault();

                                                    if (licenseRecord.EndDate != null)
                                                    {
                                                        ComplianceInstance compInstance = new ComplianceInstance()
                                                        {
                                                            ComplianceId = ComplianeId,
                                                            ScheduledOn = licenseRecord.EndDate.Value.Date,
                                                            CustomerBranchID = licenseRecord.CustomerBranchID,
                                                            GenerateSchedule = true,
                                                            IsDeleted = false,
                                                            CreatedOn = DateTime.Now,
                                                            IsDocMan_NonMan=true,
                                                        };
                                                        complianceInstanceId = LicenseMgmt.CreateComplianceInstance(compInstance);
                                                    }

                                                    if (complianceInstanceId > 0)
                                                    {
                                                        Lic_tbl_LicenseComplianceInstanceMapping licToCompInstanceMapping = new Lic_tbl_LicenseComplianceInstanceMapping()
                                                        {
                                                            LicenseID = licenseRecord.ID,
                                                            ComplianceInstanceID = complianceInstanceId,
                                                            IsStatutoryORInternal = "S"
                                                        };
                                                        LicenseMgmt.CreateLicenseComplianceInstanceMapping(licToCompInstanceMapping);

                                                        if (performerId > 0)
                                                        {
                                                            ComplianceAssignment complianceAssignmentForPerformer = new ComplianceAssignment()
                                                            {
                                                                ComplianceInstanceID = complianceInstanceId,
                                                                RoleID = 3,
                                                                UserID = performerId,
                                                            };
                                                            LicenseMgmt.CreateComplianceAssignment(complianceAssignmentForPerformer);
                                                        }
                                                        if (reviewerId > 0)
                                                        {
                                                            ComplianceAssignment complianceAssignmentForReviewer = new ComplianceAssignment()
                                                            {
                                                                ComplianceInstanceID = complianceInstanceId,
                                                                RoleID = 4,
                                                                UserID = reviewerId,
                                                            };
                                                            LicenseMgmt.CreateComplianceAssignment(complianceAssignmentForReviewer);
                                                        }
                                                        if (approverId != -1 && approverId > 0)
                                                        {
                                                            ComplianceAssignment complianceAssignmentForApprover = new ComplianceAssignment()
                                                            {
                                                                ComplianceInstanceID = complianceInstanceId,
                                                                RoleID = 6,
                                                                UserID = approverId,
                                                            };
                                                            LicenseMgmt.CreateComplianceAssignment(complianceAssignmentForApprover);
                                                        }
                                                        var AssignedRole = GetAssignedUsers((int)complianceInstanceId);
                                                        if (licenseRecord.RemindBeforeNoOfDays == 0)
                                                        {
                                                            ComplianceScheduleOn complianceScheduleOn = new ComplianceScheduleOn()
                                                            {
                                                                ScheduleOn = licenseRecord.EndDate.Value.Date,
                                                                ComplianceInstanceID = complianceInstanceId,
                                                                IsActive = true,
                                                                IsUpcomingNotDeleted = true,
                                                                IsDocMan_NonMan = true,
                                                            };
                                                            complianceScheduleOnId = LicenseMgmt.CreateComplianceScheduleOn(complianceScheduleOn);

                                                            Lic_tbl_LicenseStatusTransaction newStatusRecord = new Lic_tbl_LicenseStatusTransaction()
                                                            {
                                                                CustomerID = licenseRecord.CustomerID,
                                                                LicenseID = newLicenseID,
                                                                StatusID = statusId,
                                                                StatusChangeOn = DateTime.Now,
                                                                IsActive = true,
                                                                CreatedBy = AuthenticationHelper.UserID,
                                                                CreatedOn = DateTime.Now,
                                                                UpdatedBy = AuthenticationHelper.UserID,
                                                                UpdatedOn = DateTime.Now,
                                                                Remark = "New license created",
                                                                ComplianceScheduleOnID= complianceScheduleOnId,
                                                            };
                                                            saveSuccess = LicenseMgmt.CreateLicenseStatusTransaction(newStatusRecord);


                                                            Lic_tbl_LicenseComplianceInstanceScheduleOnMapping lictbllicensecomplianceschedulonmapping = new Lic_tbl_LicenseComplianceInstanceScheduleOnMapping()
                                                            {
                                                                LicenseID = licenseRecord.ID,
                                                                ComplianceInstanceID = complianceInstanceId,
                                                                ComplianceScheduleOnID = complianceScheduleOnId,
                                                                IsActivation = "Active",
                                                                IsStatutoryORInternal = "S"
                                                            };
                                                            LicenseMgmt.CreateLicenseLicenseComplianceInstanceScheduleOnMapping(lictbllicensecomplianceschedulonmapping);


                                                            ComplianceTransaction complianceTransaction = new ComplianceTransaction()
                                                            {
                                                                ComplianceInstanceId = complianceInstanceId,
                                                                StatusId = 1,
                                                                Remarks = "New compliance assigned.",
                                                                Dated = DateTime.Now,
                                                                CreatedBy = AuthenticationHelper.UserID,
                                                                CreatedByText = AuthenticationHelper.User,
                                                                ComplianceScheduleOnID = complianceScheduleOnId,
                                                            };
                                                            complianceTransactionId = LicenseMgmt.CreateComplianceTransaction(complianceTransaction);

                                                            if (AssignedRole.Count > 0)
                                                            {
                                                                foreach (var roles in AssignedRole)
                                                                {
                                                                    if (roles.RoleID != 6)
                                                                    {
                                                                        var reminders = (from RT in entities.ReminderTemplates
                                                                                         where RT.Frequency == 2 && RT.IsSubscribed == true
                                                                                         select RT).ToList();

                                                                        reminders.ForEach(day =>
                                                                        {
                                                                            ComplianceReminder reminder = new ComplianceReminder()
                                                                            {
                                                                                ComplianceAssignmentID = roles.ID,
                                                                                ReminderTemplateID = day.ID,
                                                                                ComplianceDueDate = licenseRecord.EndDate.Value.Date,
                                                                                RemindOn = licenseRecord.EndDate.Value.Date.Date.AddDays(-1 * day.TimeInDays),
                                                                            };
                                                                            reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                                                            entities.ComplianceReminders.Add(reminder);

                                                                        });
                                                                    }
                                                                }
                                                                entities.SaveChanges();
                                                            }
                                                        }
                                                        else
                                                        {
                                                            #region Application Days
                                                            DateTime Applicationdate = Convert.ToDateTime(licenseRecord.EndDate);
                                                            Applicationdate = Applicationdate.AddDays(-Convert.ToDouble(licenseRecord.RemindBeforeNoOfDays));
                                                            ComplianceScheduleOn complianceScheduleOn = new ComplianceScheduleOn()
                                                            {
                                                                ScheduleOn = Applicationdate.Date,
                                                                ComplianceInstanceID = complianceInstanceId,
                                                                IsActive = true,
                                                                IsUpcomingNotDeleted = true,
                                                                IsDocMan_NonMan = true,
                                                            };
                                                            complianceScheduleOnId = LicenseMgmt.CreateComplianceScheduleOn(complianceScheduleOn);


                                                            Lic_tbl_LicenseStatusTransaction newStatusRecord = new Lic_tbl_LicenseStatusTransaction()
                                                            {
                                                                CustomerID = licenseRecord.CustomerID,
                                                                LicenseID = newLicenseID,
                                                                StatusID = statusId,
                                                                StatusChangeOn = DateTime.Now,
                                                                IsActive = true,
                                                                CreatedBy = AuthenticationHelper.UserID,
                                                                CreatedOn = DateTime.Now,
                                                                UpdatedBy = AuthenticationHelper.UserID,
                                                                UpdatedOn = DateTime.Now,
                                                                Remark = "New license created",
                                                                ComplianceScheduleOnID = complianceScheduleOnId,
                                                            };
                                                            saveSuccess = LicenseMgmt.CreateLicenseStatusTransaction(newStatusRecord);

                                                            Lic_tbl_LicenseComplianceInstanceScheduleOnMapping lictbllicensecomplianceschedulonmapping = new Lic_tbl_LicenseComplianceInstanceScheduleOnMapping()
                                                            {
                                                                LicenseID = licenseRecord.ID,
                                                                ComplianceInstanceID = complianceInstanceId,
                                                                ComplianceScheduleOnID = complianceScheduleOnId,
                                                                IsActivation = "Application",
                                                                IsStatutoryORInternal = "S"
                                                            };
                                                            LicenseMgmt.CreateLicenseLicenseComplianceInstanceScheduleOnMapping(lictbllicensecomplianceschedulonmapping);

                                                            ComplianceTransaction complianceTransaction = new ComplianceTransaction()
                                                            {
                                                                ComplianceInstanceId = complianceInstanceId,
                                                                StatusId = 1,
                                                                Remarks = "New compliance assigned.",
                                                                Dated = DateTime.Now,
                                                                CreatedBy = AuthenticationHelper.UserID,
                                                                CreatedByText = AuthenticationHelper.User,
                                                                ComplianceScheduleOnID = complianceScheduleOnId,
                                                            };
                                                            complianceTransactionId = LicenseMgmt.CreateComplianceTransaction(complianceTransaction);

                                                            if (AssignedRole.Count > 0)
                                                            {
                                                                foreach (var roles in AssignedRole)
                                                                {
                                                                    if (roles.RoleID != 6)
                                                                    {
                                                                        var reminders = (from RT in entities.ReminderTemplates
                                                                                         where RT.Frequency == 2 && RT.IsSubscribed == true
                                                                                         select RT).ToList();

                                                                        reminders.ForEach(day =>
                                                                        {
                                                                            ComplianceReminder reminder = new ComplianceReminder()
                                                                            {
                                                                                ComplianceAssignmentID = roles.ID,
                                                                                ReminderTemplateID = day.ID,
                                                                                ComplianceDueDate = Applicationdate.Date,
                                                                                RemindOn = Applicationdate.Date.AddDays(-1 * day.TimeInDays),
                                                                            };
                                                                            reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                                                            entities.ComplianceReminders.Add(reminder);

                                                                        });
                                                                    }
                                                                }
                                                                entities.SaveChanges();
                                                            }
                                                            #endregion
                                                        }

                                                        #region Upload File
                                                        List<KeyValuePair<string, Byte[]>> Filelist = new List<KeyValuePair<string, Byte[]>>();
                                                        List<FileData> files = new List<FileData>();
                                                        List<KeyValuePair<string, int>> list = new List<KeyValuePair<string, int>>();
                                                        //FileUpload LicenseFileUpload = null;
                                                        string directoryPath = null;
                                                        HttpFileCollection LicenseFileUpload = Request.Files;
                                                        bool blankfileCount = true;
                                                        int CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                                                        if (LicenseFileUpload.Count > 0)
                                                        {
                                                            var AWSData = AmazonS3.GetAWSStorageDetail(CustomerID);
                                                            if (AWSData != null)
                                                            {
                                                                #region AWS Storage
                                                                var InstanceData = DocumentManagement.GetComplianceInstanceData(Convert.ToInt32(complianceInstanceId));
                                                                
                                                                string version = null;
                                                                version = DocumentManagement.GetDocumnetVersion(Convert.ToInt32(complianceScheduleOnId));
                                                                directoryPath = "avacomdocuments\\" + customerID + "\\" + licenseRecord.CustomerBranchID + "\\" + InstanceData.ID.ToString() + "\\" + complianceScheduleOnId + "\\" + version;

                                                                IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.APSouth1);
                                                                S3DirectoryInfo di = new S3DirectoryInfo(client, AWSData.BucketName, directoryPath);
                                                                if (!di.Exists)
                                                                {
                                                                    di.Create();
                                                                }

                                                                for (int j = 0; j < LicenseFileUpload.Count; j++)
                                                                {
                                                                    HttpPostedFile uploadfile = LicenseFileUpload[j];
                                                                    String fileName = "";
                                                                    fileName = "LicenseDoc_" + uploadfile.FileName;
                                                                    list.Add(new KeyValuePair<string, int>(fileName, 1));
                                                                    Guid fileKey = Guid.NewGuid();
                                                                    string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(uploadfile.FileName));
                                                                    Stream fs = uploadfile.InputStream;
                                                                    BinaryReader br = new BinaryReader(fs);
                                                                    Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                                                                    Filelist.Add(new KeyValuePair<string, Byte[]>(finalPath, bytes));

                                                                    if (uploadfile.ContentLength > 0)
                                                                    {
                                                                        string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                                                        string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;

                                                                        string TdirectoryPath = "~/TempDocuments/AWS/" + User + "/" + uploadfile.FileName;
                                                                        string directoryPath1 = "~/TempDocuments/AWS/" + User;
                                                                        string TFilePath = Server.MapPath(TdirectoryPath);
                                                                        if (File.Exists(TFilePath))
                                                                            File.Delete(TFilePath);
                                                                        if (!Directory.Exists(directoryPath1))
                                                                        {
                                                                            Directory.CreateDirectory(Server.MapPath(directoryPath1));
                                                                        }

                                                                        FileStream objFileStrm = File.Create(TFilePath);
                                                                        objFileStrm.Close();
                                                                        File.WriteAllBytes(TFilePath, bytes);

                                                                        string AWSpath = "";
                                                                        AWSpath = directoryPath + "\\LicenseDoc_" + uploadfile.FileName;

                                                                        FileInfo localFile = new FileInfo(TFilePath);
                                                                        S3FileInfo s3File = new S3FileInfo(client, AWSData.BucketName, AWSpath);
                                                                        if (!s3File.Exists)
                                                                        {
                                                                            using (var s3Stream = s3File.Create()) // <-- create file in S3  
                                                                            {
                                                                                localFile.OpenRead().CopyTo(s3Stream); // <-- copy the content to S3  
                                                                            }
                                                                        }

                                                                        string filepathvalue = string.Empty;
                                                                        string vale = directoryPath.Replace(ConfigurationManager.AppSettings["DriveUrl"], "~");
                                                                        filepathvalue = vale.Replace(@"\", "/");

                                                                        FileData file = new FileData()
                                                                        {
                                                                            Name = fileName,
                                                                            FilePath = filepathvalue,
                                                                            FileKey = fileKey.ToString(),
                                                                            Version = version,
                                                                            VersionDate = DateTime.UtcNow,
                                                                            FileSize = uploadfile.ContentLength,
                                                                        };
                                                                        files.Add(file);
                                                                    }
                                                                    else
                                                                    {
                                                                        if (!string.IsNullOrEmpty(uploadfile.FileName))
                                                                            blankfileCount = false;
                                                                    }
                                                                }
                                                                #endregion
                                                            }
                                                            else
                                                            {
                                                                #region Normal Storage
                                                                //int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                                                                var InstanceData = DocumentManagement.GetComplianceInstanceData(Convert.ToInt32(complianceInstanceId));
                                                               
                                                                string version = null;
                                                                version = DocumentManagement.GetDocumnetVersion(Convert.ToInt32(complianceScheduleOnId));
                                                                if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                                                {
                                                                    directoryPath = ConfigurationManager.AppSettings["DriveUrl"] + "\\AvacomDocuments\\" + customerID + "\\" + licenseRecord.CustomerBranchID + "\\" + InstanceData.ID.ToString() + "\\" + complianceScheduleOnId + "\\" + version;
                                                                }
                                                                else
                                                                {
                                                                    directoryPath = Server.MapPath("~/AvacomDocuments/" + customerID + "/" + licenseRecord.CustomerBranchID + "/" + InstanceData.ID.ToString() + "/" + complianceScheduleOnId + "/" + version);
                                                                }
                                                                DocumentManagement.CreateDirectory(directoryPath);
                                                                for (int j = 0; j < LicenseFileUpload.Count; j++)
                                                                {
                                                                    HttpPostedFile uploadfile = LicenseFileUpload[j];
                                                                    String fileName = "";
                                                                    fileName = "LicenseDoc_" + uploadfile.FileName;
                                                                    list.Add(new KeyValuePair<string, int>(fileName, 1));
                                                                    Guid fileKey = Guid.NewGuid();
                                                                    string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(uploadfile.FileName));
                                                                    Stream fs = uploadfile.InputStream;
                                                                    BinaryReader br = new BinaryReader(fs);
                                                                    Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                                                                    Filelist.Add(new KeyValuePair<string, Byte[]>(finalPath, bytes));
                                                                    if (uploadfile.ContentLength > 0)
                                                                    {
                                                                        string filepathvalue = string.Empty;
                                                                        if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                                                        {
                                                                            string vale = directoryPath.Replace(ConfigurationManager.AppSettings["DriveUrl"], "~");
                                                                            filepathvalue = vale.Replace(@"\", "/");
                                                                        }
                                                                        else
                                                                        {
                                                                            filepathvalue = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                                                        }
                                                                        FileData file = new FileData()
                                                                        {
                                                                            Name = fileName,
                                                                            FilePath = filepathvalue,
                                                                            FileKey = fileKey.ToString(),
                                                                            Version = version,
                                                                            VersionDate = DateTime.UtcNow,
                                                                            FileSize = uploadfile.ContentLength,
                                                                        };
                                                                        files.Add(file);
                                                                    }
                                                                    else
                                                                    {
                                                                        if (!string.IsNullOrEmpty(uploadfile.FileName))
                                                                            blankfileCount = false;
                                                                    }
                                                                }
                                                                #endregion
                                                            }
                                                        }
                                                        bool flag = false;
                                                        if (blankfileCount)
                                                        {
                                                            flag = LicenseMgmt.UploadLicenseCreateDocumentsAWS(complianceTransactionId, complianceScheduleOnId, files, list, Filelist, directoryPath, customerID);
                                                        }
                                                        else
                                                        {
                                                            ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "missingAlert", "alert('Please do not upload virus file or blank files.')", true);
                                                            cvLicPopUp.IsValid = false;
                                                            cvLicPopUp.ErrorMessage = "Please do not upload virus file or blank files.";
                                                        }
                                                        #endregion

                                                        //TemplateAssignment IsActive set to 0
                                                        LicenseMgmt.UpdateTemplateAssignment(ComplianeId, licenseRecord.CustomerBranchID);
                                                    }
                                                }
                                                CompliancesEntrySuccess = true;
                                            }
                                            catch (Exception ex)
                                            {
                                                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                                cvLicPopUp.IsValid = false;
                                                cvLicPopUp.ErrorMessage = "Something went wrong, Please try again";
                                                VSLicPopup.CssClass = "alert alert-danger";
                                                CompliancesEntrySuccess = false;
                                            }
                                        }
                                        else
                                        {
                                            cvLicPopUp.IsValid = false;
                                            cvLicPopUp.ErrorMessage = "Compliance already Assigned to License";
                                            VSLicPopup.CssClass = "alert alert-danger";
                                        }
                                        #endregion
                                    }
                                }
                            }//Using End
                        }
                        catch (Exception ex)
                        {
                            LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                            cvLicPopUp.IsValid = false;
                            cvLicPopUp.ErrorMessage = "Something went wrong, Please try again";
                            VSLicPopup.CssClass = "alert alert-danger";
                        }
                        #endregion

                        //} //location list for loop
                        #endregion
                    }
                    #endregion
                    if (lstErrorMsg.Count > 0)
                    {
                        formValidateSuccess = false;
                        saveSuccess = false;
                        TempassignmentsaveSuccess = false;
                        CompliancesEntrySuccess = false;
                        showErrorMessages(lstErrorMsg, cvLicPopUp);
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "scrollUpPageScript", "scrollUpPage();", true);
                    }
                    if (formValidateSuccess == true && saveSuccess == true && TempassignmentsaveSuccess == true && CompliancesEntrySuccess == true)
                    {
                        clearCaseControls();
                        cvLicPopUp.IsValid = false;
                        cvLicPopUp.ErrorMessage = "License Compliance Assigned and Activated Successfully";
                        VSLicPopup.CssClass = "alert alert-success";
                    }
                } //formValidateSuccess
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void RetrieveNodes(TreeNode node)
        {
            if (node.Checked) // && node.ChildNodes.Count == 0 if (node.Checked)
            {
                if (!locationList.Contains(Convert.ToInt32(node.Value)))
                    locationList.Add(Convert.ToInt32(node.Value));
            }

            foreach (TreeNode tn in node.ChildNodes)
            {
                if (tn.Checked)//&& tn.ChildNodes.Count == 0)//  && tn.ChildNodes.Count == 0if (tn.Checked)
                {
                    if (!locationList.Contains(Convert.ToInt32(tn.Value)))
                        locationList.Add(Convert.ToInt32(tn.Value));
                }

                if (tn.ChildNodes.Count != 0)
                {
                    for (int i = 0; i < tn.ChildNodes.Count; i++)
                    {
                        RetrieveNodes(tn.ChildNodes[i]);
                    }
                }
            }
        }

        public void showErrorMessages(List<string> lstErrMsgs, CustomValidator cvtoShowErrorMsg)
        {
            //string finalErrMsg = string.Join("<br/>", emsg.ToArray());

            string finalErrMsg = string.Empty;

            finalErrMsg += "<ol type='1'>";

            if (lstErrMsgs.Count > 0)
            {
                lstErrMsgs.ForEach(eachErrMsg =>
                {
                    finalErrMsg += "<li>" + eachErrMsg + "</li>";
                });

                finalErrMsg += "</ol>";
            }
            cvtoShowErrorMsg.IsValid = false;
            cvtoShowErrorMsg.ErrorMessage = finalErrMsg;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "scrollUpPageScript", "scrollUpPage();", true);
        }

        public static List<ComplianceAssignment> GetAssignedUsers(int ComplianceInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var AssignedUsers = (from row in entities.ComplianceAssignments
                                     where row.ComplianceInstanceID == ComplianceInstanceID
                                     select row).GroupBy(entry => entry.RoleID).Select(entry => entry.FirstOrDefault()).ToList();

                return AssignedUsers;
            }
        }

        public void clearCaseControls()
        {
            try
            {
                txtTitle.Text = "";
                txtLicenseNo.Text = "";
                txtStartDate.Text = "";
                txtEndDate.Text = "";
                txtApplicationDays.Text = "";
                ddlCompliance.ClearSelection();
                ddlLicenseType.ClearSelection();
                ddlPerformer.ClearSelection();
                ddlReviewer.ClearSelection();
                ddlApprover.ClearSelection();
                locationList.Clear();
                tbxFilterLocation.Text = "Entity/Sub-Entity/Location";
                LicenseFileUpload.Attributes.Clear();
                txtCost.Text = "";
                ddlReviewer.ClearSelection();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}