﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LicenseManagement.Master" AutoEventWireup="true" CodeBehind="EditLicenses.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.LicenseManagement.aspxPages.EditLicenses" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .Dashboard-white-widget {
    background: #fff;
    padding: 5px 5px 5px;
    margin-bottom: 10px;
    border-radius: 10px;
    overflow-x: scroll;
    overflow-y: hidden;
}


        .k-grid-content {
            min-height: 394px !important;
        }

        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 1.0em;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            /*background-color: #1fd9e1;*/
            border-color: #1fd9e1;
            background-color: #f6f6f6;
        }

        #grid .k-grid-toolbar {
            background: white;
        }


        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 5px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: #E4F7FB;
        }

        html .k-grid tr.k-alt:hover {
            background: #E4F7FB;
        }

        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            border-radius: 35px;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow-y: scroll;
        }
          .media, .media-body, .modal-open, .progress {
            overflow-y: scroll;
        }
              .k-auto-scrollable {
            overflow-y: scroll;
        }


        .k-grid-header {
            padding-right: 0px !important;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: inline-block;
            margin-bottom: 0px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            /* border-width: 0 0 0px 0px; */
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: #f8f8f8;
            color: #2b2b2b;
        }
        /*.k-pager-wrap.k-grid-pager.k-widget.k-floatwrap {
            margin-top: 9px;
        }*/
        td.k-command-cell {
            border-width: 0 1px 0 1px;
        }

        .k-grid-pager {
            border-width: 1px 1px 1px 1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-filter-row > th:first-child, .k-grid tbody td:first-child, .k-grid tfoot td:first-child, .k-grid-header th.k-header:first-child {
            border-left-width: 1px;
        }
    </style>
    <style>
        .panel-heading {
            background: #ffffff;
        }

            .panel-heading .nav .active, .panel-heading .nav > li.active > a, .panel-heading .nav > li > a:hover {
                color: #1fd9e1;
                background-color: #fff;
            }
    </style>
    <style type="text/css">
        .form-control {
            height: 33px !important;
        }
    </style>

     <script type="text/javascript">
        //$(function () {
        //    $('#divLicenseDialog').dialog({
        //        height: 550,
        //        width: 630,
        //        autoOpen: false,
        //        draggable: true,
        //        title: "License Details",
        //        open: function (type, data) {
        //            $(this).parent().appendTo("form");
        //        }
        //    });
        //});
        function initializeRadioButtonsList(controlID) {
            $(controlID).buttonset();
        }
    </script>
    <script type="text/javascript">
        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);
        window.onunload = function () { null };

        function ShowDialog(LicenseID, CID, SOID, isstatutoryinternal) {
            debugger;
            $('#divShowDialog').modal('show');
            $('.modal-dialog').css('width', '80%');
            $('#showdetails').attr('width', '100%');
            $('#showdetails').attr('height', '500px');
            $('#showdetails').attr('src', "../aspxPages/EditLicensesPopup.aspx?LicenseID=" + LicenseID + "&CID=" + CID + "&SOID=" + SOID + "&isstatutoryinternal=" + isstatutoryinternal);
            // $('#divShowDialog').modal('handleUpdate');
        };

    </script>

    <script type="text/javascript">
        $(document).on("click", function (event) {
            if (event.target.id == "") {
                var idvid = $(event.target).closest('div');
                if ($(idvid).attr('id') != undefined && $(idvid).attr('id') != '' && $(idvid).attr('id').indexOf('<%= tvFilterLocation.ClientID %>') > -1) {
                    $("#divFilterLocation").show();
                } else {
                    $("#divFilterLocation").hide();
                }
            } else if (event.target.id != "" && event.target.id.indexOf('<%= tvFilterLocation.ClientID %>') > -1) {
                $("#divFilterLocation").show();
            } else if (event.target.id != '<%= tbxFilterLocation.ClientID %>') {
                $("#divFilterLocation").hide();
            } else if (event.target.id == '<%= tbxFilterLocation.ClientID %>') {
                $('<%= tbxFilterLocation.ClientID %>').unbind('click');

                $('<%= tbxFilterLocation.ClientID %>').click(function () {
                    $("#divFilterLocation").toggle("blind", null, 500, function () { });
                });
            }
        });



        $(document).ready(function () {
            setactivemenu('leftworkspacemenu');
            fhead('Edit License');
    if ($("#<%=tbxFilterLocation.ClientID %>") != null) {
        $("#<%=tbxFilterLocation.ClientID %>").unbind('click');

        $("#<%=tbxFilterLocation.ClientID %>").click(function () {
            $("#divFilterLocation").toggle("blind", null, 500, function () { });
        });
    }
});
function InitializeRequest(sender, args) { }


function closeModal() {
    document.getElementById('<%= lnkBtnBindGrid.ClientID %>').click();
}

function hideDivBranch() {
    $('#divFilterLocationPopup').hide("blind", null, 0, function () { });
}

function initializeJQueryUI(textBoxID, divID) {
    $("#" + textBoxID).unbind('click');

    $("#" + textBoxID).click(function () {
        $("#" + divID).toggle("blind", null, 500, function () { });
    });
}

function checkAll(cb) {
    var ctrls = document.getElementsByTagName('input');
    for (var i = 0; i < ctrls.length; i++) {
        var cbox = ctrls[i];
        if (cbox.type == "checkbox" && cbox.id.indexOf("chkIndustry") > -1) {
            cbox.checked = cb.checked;
        }
    }
}


function UncheckHeader() {
    var rowCheckBox = $("#RepeaterTable input[id*='chkIndustry']");
    var rowCheckBoxSelected = $("#RepeaterTable input[id*='chkIndustry']:checked");
    var rowCheckBoxHeader = $("#RepeaterTable input[id*='IndustrySelectAll']");
    if (rowCheckBox.length == rowCheckBoxSelected.length) {
        rowCheckBoxHeader[0].checked = true;
    } else {

        rowCheckBoxHeader[0].checked = false;
    }
}



                function checkAll(cb) {
                    var ctrls = document.getElementsByTagName('input');
                    for (var i = 0; i < ctrls.length; i++) {
                        var cbox = ctrls[i];
                        if (cbox.type == "checkbox" && cbox.id.indexOf("chkIndustry") > -1) {
                            cbox.checked = cb.checked;
                        }
                    }
                }
                function UncheckHeader() {
                    var rowCheckBox = $("#RepeaterTable input[id*='chkIndustry']");
                    var rowCheckBoxSelected = $("#RepeaterTable input[id*='chkIndustry']:checked");
                    var rowCheckBoxHeader = $("#RepeaterTable input[id*='IndustrySelectAll']");
                    if (rowCheckBox.length == rowCheckBoxSelected.length) {
                        rowCheckBoxHeader[0].checked = true;
                    } else {

                        rowCheckBoxHeader[0].checked = false;
                    }
                }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row Dashboard-white-widget">
        <div class="dashboard">

             <div class="col-md-12 colpadding0 text-right">
                <div class="modal fade" id="divShowDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header bgColor-gray" style="height: 35px; text-align: left;">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                <label class="modal-header-custom col-md-6 plr0">
                                   Edit License Detail(s)</label>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="closeModal();">&times;</button>
                            </div>

                            <div class="modal-body" style="background-color: #f7f7f7;">
                                <iframe id="showdetails" src="about:blank" width="100%" height="100%" frameborder="0"></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel-body">
                        <div class="col-md-12 colpadding0">
                            <asp:ValidationSummary ID="vsLicenseListPage" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                ValidationGroup="LicenseListPageValidationGroup" />
                            <asp:CustomValidator ID="cvErrorLicenseListPage" runat="server" EnableClientScript="False"
                                ValidationGroup="LicenseListPageValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                        </div>
                         <asp:UpdatePanel ID="upLicenseList" runat="server" UpdateMode="Conditional" OnLoad="upLicenseList_Load">
                            <ContentTemplate>
                        <div class="row">
                            <div class="col-md-12 colpadding0">
                                <div class="col-md-1 form-group plr0" style="padding-right: 11px;">
                                    <label for="lblLicenseType" class="filter-label">Show</label>
                                    <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 100%; float: right; height: 32px !important; margin-right: 6%"
                                        AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                                        <asp:ListItem Text="5" />
                                        <asp:ListItem Text="10" Selected="True" />
                                        <asp:ListItem Text="20" />
                                        <asp:ListItem Text="50" />
                                    </asp:DropDownList>
                                </div>

                                <asp:UpdatePanel ID="upDivLocation" runat="server" UpdateMode="Conditional" OnLoad="upDivLocation_Load">
                                    <ContentTemplate>
                                        <div class="col-md-3 pl0">
                                            <label for="tbxFilterLocation" class="filter-label">Entity/Branch/Location</label>
                                            <asp:TextBox runat="server" ID="tbxFilterLocation" PlaceHolder="Click to Select" autocomplete="off" CssClass="form-control" Width="100%" />
                                            <div style="margin-left: 1px; position: absolute; z-index: 10; overflow-y: auto; height: 200px; width: 90%" id="divFilterLocation">
                                                <asp:TreeView runat="server" ID="tvFilterLocation" SelectedNodeStyle-Font-Bold="true" NodeStyle-ForeColor="#8e8e93"
                                                    Style="margin-top: -20px; overflow: auto; border-left: 1px solid #c7c7cc; border-right: 1px solid #c7c7cc; border-bottom: 1px solid #c7c7cc; background-color: #ffffff; color: #8e8e93 !important;"
                                                    ShowLines="true"
                                                    OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged">
                                                </asp:TreeView>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <div class="col-md-2 pl0 form-group">
                                    <label for="lblLicenseType" class="filter-label">Type</label>
                                    <asp:DropDownList runat="server" ID="ddlfilterStatutoryNonStatutory"
                                        class="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlfilterStatutoryNonStatutory_SelectedIndexChanged">
                                        <asp:ListItem Text="Statutory" Value="S"></asp:ListItem>
                                        <asp:ListItem Text="Internal" Value="I"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <div class="col-md-2 pl0 form-group">
                                    <label for="lblLicenseType" class="filter-label">License Type</label>
                                    <asp:DropDownListChosen runat="server" ID="ddlLicenseType"
                                        AllowSingleDeselect="false" DisableSearchThreshold="3"
                                        OnSelectedIndexChanged="ddlLicenseType_SelectedIndexChanged" AutoPostBack="true"
                                        class="form-control" Width="100%" />
                                </div>

                                <div class="col-md-2 pl0 form-group" style="width: 16%;">
                                    <label for="ddlStatus" class="filter-label">Status</label>
                                    <asp:DropDownListChosen runat="server" ID="ddlLicenseStatus"
                                        AllowSingleDeselect="false" DisableSearchThreshold="5"
                                        OnSelectedIndexChanged="ddlLicenseStatus_SelectedIndexChanged" AutoPostBack="true"
                                        class="form-control" Width="100%">
                                        <asp:ListItem Text="Status" Value="Status" />
                                        <asp:ListItem Text="Active" Value="Active" />
                                        <asp:ListItem Text="Expired" Value="Expired" />
                                        <asp:ListItem Text="Expiring" Value="Expiring" />
                                        <asp:ListItem Text="Applied" Value="Applied" />
                                        <asp:ListItem Text="PendingForReview" Value="PR" />
                                        <asp:ListItem Text="Rejected" Value="Rejected" />
                                        <asp:ListItem Text="Registered" Value="Registered" />
                                        <asp:ListItem Text="Registered & Renewal Filed" Value="RRF" />
                                        <asp:ListItem Text="Validity Expired" Value="VE" />
                                    </asp:DropDownListChosen>
                                </div>

                               
                            </div>
                                 <div class="row">
                                        <div class="col-md-1 form-group plr0" style="padding-right: 11px;">
                                            <label for="ddlDepartmentType" class="filter-label">Department</label>
                                  <asp:DropDownListChosen runat="server" ID="ddlDepartment"
                                                AllowSingleDeselect="false" DisableSearchThreshold="3" OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged"  AutoPostBack="true"
                                                class="form-control" Width="150%" />
                                </div>
                                   
                                </div>
                                
                                <div class="row">
                            <div class="col-md-12 colpadding0">
                                <div class="col-md-6 pl0" style="margin-top: -10px;">
                                    <asp:TextBox ID="tbxtypeTofilter" runat="server" OnTextChanged="tbxtypeTofilter_TextChanged" PlaceHolder="Type for Search"
                                        CssClass="form-control" AutoPostBack="true" Width="67%" Style="margin-left: -5px;"> </asp:TextBox>
                                </div>
                            </div>

 				<div class="col-md-1 colpadding0 float-right">
                                    
                                    <asp:UpdatePanel ID="UpdatePanel11" runat="server" UpdateMode="Conditional" style="margin-top: -10px;">
                                        <ContentTemplate>
                                            <asp:LinkButton Text="Apply" runat="server" ID="btnExportExcel" OnClick="btnExportExcel_Click"
                                                Width="80%"
                                                data-toggle="tooltip" data-placement="bottom" ToolTip="Export to Excel">
                                    <img src="/Images/Excel _icon.png" alt="Export" /> 
                                            </asp:LinkButton>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnExportExcel" />
                                        </Triggers>
                                    </asp:UpdatePanel>


                                </div>
                        </div>
                        </div>


                        <div class="row">
                            <div class="col-md-12 colpadding0" style="width: 100%;">
                                <asp:GridView runat="server" ID="grdLicenseList" AutoGenerateColumns="false" AllowSorting="true" AutoPostBack="true" GridLines="none"
                                    CssClass="table" Width="100%" ShowHeaderWhenEmpty="true" PageSize="10" AllowPaging="true" DataKeyNames="LicenseID"
                                    OnRowCommand="grdLicenseList_RowCommand"
                                    OnPageIndexChanging="grdLicenseList_PageIndexChanging"
                                    OnSorting="grdLicenseList_Sorting"
                                     OnRowCreated="grdLicenseList_RowCreated">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Location" ItemStyle-Width="15%" SortExpression="CustomerBrach">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("CustomerBrach") %>' ToolTip='<%# Eval("CustomerBrach") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="License Type" ItemStyle-Width="15%" SortExpression="LicensetypeName">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("LicensetypeName") %>' ToolTip='<%# Eval("LicensetypeName") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="License No." ItemStyle-Width="15%" SortExpression="LicenseNo">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("LicenseNo") %>' ToolTip='<%# Eval("LicenseNo") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Title" ItemStyle-Width="15%" SortExpression="LicenseTitle">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("LicenseTitle") %>' ToolTip='<%# Eval("LicenseTitle") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%--    <asp:TemplateField HeaderText="Department" ItemStyle-Width="10%" SortExpression="Department">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                            <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Department") %>' ToolTip='<%# Eval("Department") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>--%>

                                         <asp:TemplateField HeaderText="Performer" ItemStyle-Width="15%" SortExpression="PerformerName">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("PerformerName") %>' ToolTip='<%# Eval("PerformerName") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                           <asp:TemplateField HeaderText="Reviewer" ItemStyle-Width="15%" SortExpression="ReviewerName">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ReviewerName") %>' ToolTip='<%# Eval("ReviewerName") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="App Due Date" ItemStyle-Width="15%" SortExpression="ApplicationDate">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px">
                                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ApplicationDate") != DBNull.Value ? Convert.ToDateTime(Eval("ApplicationDate")).ToString("dd-MM-yyyy") : "" %>'
                                                        ToolTip='<%# Eval("ApplicationDate") != DBNull.Value ? Convert.ToDateTime(Eval("ApplicationDate")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="End Date" ItemStyle-Width="15%" SortExpression="EndDate">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px">
                                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("EndDate") != DBNull.Value ? Convert.ToDateTime(Eval("EndDate")).ToString("dd-MM-yyyy") : "" %>'
                                                        ToolTip='<%# Eval("EndDate") != DBNull.Value ? Convert.ToDateTime(Eval("EndDate")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Status" ItemStyle-Width="15%" SortExpression="MGRStatus">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 95px">
                                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("MGRStatus") %>' ToolTip='<%# Eval("MGRStatus") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="15%">
                                            <ItemTemplate>
                                                <div class="btndiv" style="width: 55px;">
                                                    <asp:LinkButton ID="lbtEdit" runat="server" OnClick="lbtEdit_Click"
                                                         CommandArgument='<%# Eval("LicenseID")+","+Eval("ComplianceInstanceID")+","+Eval("ComplianceScheduleOnID")+","+Eval("MGRStatus") %>'
                                                        Visible='<%# CanChangeStatus((long)Eval("LicenseID"),(long)Eval("ComplianceInstanceID"),(long)Eval("ComplianceScheduleOnID"),(string)Eval("MGRStatus") ,(bool)Eval("IsDeleted") ) %>'
                                                        data-toggle="tooltip" data-placement="bottom" ToolTip="Edit"> 
                                                        <img src='<%# ResolveUrl("~/Images/edit_icon_new.png")%>' alt="Edit"/></asp:LinkButton>

                                                    <asp:LinkButton ID="lbtDelete" runat="server" CommandName="DELETE_License"
                                                        CommandArgument='<%# Eval("LicenseID") %>'
                                                        Visible='<%# ActionShowHide((bool)Eval("IsDeleted")) %>'
                                                        data-toggle="tooltip" data-placement="bottom" ToolTip="DELETE"
                                                        OnClientClick="return confirm('Are you certain you want to delete this License?');">
                                                        <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="Delete"/></asp:LinkButton>


                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <RowStyle CssClass="clsROWgrid" />
                                    <HeaderStyle CssClass="clsheadergrid" />
                                    <PagerSettings Visible="false" />
                                    <PagerTemplate>
                                    </PagerTemplate>
                                    <EmptyDataTemplate>
                                        No Record Found
                                    </EmptyDataTemplate>
                                </asp:GridView>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 colpadding0">
                                <div class="col-md-10 colpadding0">
                                    <div runat="server" id="DivRecordsScrum" style="color: #999">
                                        <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>-
                                                <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                                                 <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                                    </div>
                                </div>
                                <div class="col-md-1 text-right colpadding0">
                                </div>
                                <div class="col-md-1 text-right colpadding0">
                                    <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true" DataPlaceHolder="No" AllowSingleDeselect="false"
                                        OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged" class="form-control m-bot15" Width="100%" Height="32px">
                                    </asp:DropDownListChosen>
                                </div>
                                <asp:LinkButton ID="lnkBtnBindGrid" OnClick="lnkBtnBindGrid_Click" Style="float: right; display: none;" Width="100%" runat="server">
                                </asp:LinkButton>
                                <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                            </div>
                        </div>
                    </div>
                       </ContentTemplate>
                        </asp:UpdatePanel>


                    


              

            </div>
        </div>

    </div>
</asp:Content>
