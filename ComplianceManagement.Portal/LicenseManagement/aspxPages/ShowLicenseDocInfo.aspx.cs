﻿using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.LicenseManagement.aspxPages
{
    public partial class ShowLicenseDocInfo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["AccessID"]))
                    {
                        BindContractDocType();

                        long fileID = Convert.ToInt64(Request.QueryString["AccessID"]);

                        var fileRecord = ContractDocumentManagement.GetDocumentDetailByFileID(Convert.ToInt64(fileID));

                        if (fileRecord != null)
                        {
                            lblFileID.Text = fileRecord.FileID.ToString();
                            lblDocFileName.Text = fileRecord.FileName;

                            lblFileType.Text = Path.GetExtension(fileRecord.FileName).Trim().ToUpper();
                            lblUpdloadedBy.Text = fileRecord.UploadedByName;

                            lblUpdloadedOn.Text = fileRecord.CreatedOn.Value.ToString("dd-MM-yyyy HH:mm:ss tt");

                            if (ddlDocType.Items.FindByValue(fileRecord.DocTypeID.ToString()) != null)
                            {
                                ddlDocType.ClearSelection();
                                ddlDocType.Items.FindByValue(fileRecord.DocTypeID.ToString()).Selected = true;
                            }

                            txtFileTags.Text = fileRecord.FileTags;
                            lblFileTags.Text = fileRecord.FileTags;
                        }
                    }

                    if (!string.IsNullOrEmpty(Request.QueryString["CID"]))
                    {
                        ViewState["ContractID"] = Convert.ToInt64(Request.QueryString["CID"]);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindContractDocType()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                var lstdocTypes = ContractMastersManagement.GetContractDocTypes_All(customerID);

                ddlDocType.DataTextField = "TypeName";
                ddlDocType.DataValueField = "ID";

                ddlDocType.DataSource = lstdocTypes;
                ddlDocType.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnUpdateDocInfo_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(lblFileID.Text))
                {
                    long fileID = Convert.ToInt64(lblFileID.Text);
                    bool validationSuccess = false;
                    bool updateSuccess = false;

                    if (string.IsNullOrEmpty(ddlDocType.SelectedValue))
                    {
                        cvDocInfo.IsValid = false;
                        cvDocInfo.ErrorMessage = "Select Document Type";
                        validationSuccess = false;
                        return;
                    }
                    else
                        validationSuccess = true;

                    if (validationSuccess)
                    {
                        Cont_tbl_FileData _objFileRecord = new Cont_tbl_FileData()
                        {
                            ID = fileID,
                            DocTypeID = Convert.ToInt64(ddlDocType.SelectedValue)                           
                        };

                        updateSuccess = ContractDocumentManagement.UpdateContractDocumentMapping(_objFileRecord);

                        if (updateSuccess)
                        {
                            bool matchSuccess = true;

                            string[] prevSavedFileTags = lblFileTags.Text.Trim().Split(',').Where(row => !string.IsNullOrEmpty(row)).ToArray();
                            string[] currentFileTags = txtFileTags.Text.Trim().Split(',').Where(row => !string.IsNullOrEmpty(row)).ToArray();

                            if (prevSavedFileTags.Length != currentFileTags.Length)
                            {
                                matchSuccess = false;
                            }
                            else
                            {
                                matchSuccess = prevSavedFileTags.Except(currentFileTags).ToList().Count > 0 ? false : true;
                            }

                            if (!matchSuccess)
                            {
                                updateSuccess = ContractDocumentManagement.Delete_FileTagsMapping(fileID, AuthenticationHelper.UserID);
                                
                                if (currentFileTags.Length > 0)
                                {
                                    List<Cont_tbl_FileDataTagsMapping> lstFileTagMapping = new List<Cont_tbl_FileDataTagsMapping>();

                                    for (int j = 0; j < currentFileTags.Length; j++)
                                    {
                                        Cont_tbl_FileDataTagsMapping objFileTagMapping = new Cont_tbl_FileDataTagsMapping()
                                        {
                                            FileID = fileID,
                                            FileTag = currentFileTags[j].Trim(),
                                            IsActive = true,
                                            CreatedBy = AuthenticationHelper.UserID,
                                            CreatedOn = DateTime.Now,
                                            UpdatedBy = AuthenticationHelper.UserID,
                                            UpdatedOn = DateTime.Now,
                                        };

                                        lstFileTagMapping.Add(objFileTagMapping);
                                    }

                                    if (lstFileTagMapping.Count > 0)
                                    {                                        
                                        updateSuccess = ContractDocumentManagement.CreateUpdate_FileTagsMapping(lstFileTagMapping);
                                    }
                                }
                            }                            
                        }

                        if (updateSuccess)
                        {
                            if (ViewState["ContractID"] != null)
                            {
                                long contractID = Convert.ToInt64(ViewState["ContractID"]);
                                ContractManagement.CreateAuditLog("C", contractID, "Cont_tbl_FileData", "Update", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Document Details Updated", true, Convert.ToInt32(contractID));
                            }

                            cvDocInfo.IsValid = false;
                            cvDocInfo.ErrorMessage = "Documemt Details Updated Successfully";
                            vsDocInfo.CssClass = "alert alert-success";
                        }
                        else
                        {
                            cvDocInfo.IsValid = false;
                            cvDocInfo.ErrorMessage = "Something went wrong, Please try again";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDocInfo.IsValid = false;
                cvDocInfo.ErrorMessage = "Something went wrong, Please try again";
            }
        }
    }
}