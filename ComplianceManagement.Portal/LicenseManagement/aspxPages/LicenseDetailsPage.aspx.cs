﻿using Amazon;
using Amazon.S3;
using Amazon.S3.IO;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.AWS;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.License;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Ionic.Zip;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.LicenseManagement.aspxPages
{
    public partial class LicenseDetailsPage : System.Web.UI.Page
    {
        public static string CompDocReviewPath = "";
        static string sampleFormPath = "";
        public static List<long> Branchlist = new List<long>();
        protected int LoggedInUserCustomerID = 0;
        protected long UserID = AuthenticationHelper.UserID;
        protected string UserName = AuthenticationHelper.User;
        protected string productID = AuthenticationHelper.ProductApplicableLogin;
        protected string UploadDocumentLink;
        protected string user_Roles;

        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string listCustomer = ConfigurationManager.AppSettings["UploadUsingLinkCustomerlist"];
                string[] listCust = new string[] { };
                if (!string.IsNullOrEmpty(listCustomer))
                    listCust = listCustomer.Split(',');

                if (listCust.Contains(Convert.ToString(AuthenticationHelper.CustomerID)))
                    UploadDocumentLink = "True";
                else
                    UploadDocumentLink = "False";

                user_Roles = AuthenticationHelper.Role;
                VSLicensePopup.CssClass = "alert alert-danger";
                if (!IsPostBack)
                {
                    if (!AuthenticationHelper.Role.Equals("IMPT"))
                    {
                        if (Convert.ToInt32(AuthenticationHelper.CustomerID) != 0)
                        {
                            LoggedInUserCustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                        }
                    }
                    if (!string.IsNullOrEmpty(Request.QueryString["AccessID"]))
                    {
                        var licenseInstanceID = Request.QueryString["AccessID"];
                        if (licenseInstanceID != "")
                        {
                            ViewState["LicenseInstanceID"] = licenseInstanceID;
                            btnEditLicense_Click(sender, e); //Edit Detail                                                       
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        #endregion

        #region Common

        public void showErrorMessages(List<string> lstErrMsgs, CustomValidator cvtoShowErrorMsg)
        {

            string finalErrMsg = string.Empty;

            finalErrMsg += "<ol type='1'>";

            if (lstErrMsgs.Count > 0)
            {
                lstErrMsgs.ForEach(eachErrMsg =>
                {
                    finalErrMsg += "<li>" + eachErrMsg + "</li>";
                });

                finalErrMsg += "</ol>";
            }

            cvtoShowErrorMsg.IsValid = false;
            cvtoShowErrorMsg.ErrorMessage = finalErrMsg;
        }

        #endregion

        #region License 

        private void BindTransactions(long LicenseID)
        {
            try
            {
                grdTransactionHistory.DataSource = LicenseMgmt.GetAllTransactionLog(LicenseID);
                grdTransactionHistory.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdTransactionHistory_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdTransactionHistory.PageIndex = e.NewPageIndex;
                BindTransactions(Convert.ToInt32(ViewState["LicenseInstanceID"]));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void lbDownloadSample_Click(object sender, EventArgs e)
        {
            try
            {
                var file = Business.ComplianceManagement.GetComplianceFormByID(Convert.ToInt32(lbDownloadSample.CommandArgument));
                Response.Buffer = true;
                Response.Clear();
                Response.ClearContent();
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                Response.BinaryWrite(file.FileData); // create the file
                Response.Flush(); // send it to the client to download

                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void lnkSampleForm_Click(object sender, EventArgs e)
        {
            try
            {
                if (lnkSampleForm.Text != "")
                {
                    string url = lnkSampleForm.Text;

                    string fullURL = "window.open('" + url + "', '_blank', 'height=500,width=800,status=yes,toolbar=no,menubar=no,location=no,scrollbars=yes,resizable=no,titlebar=no' );";
                    lnkSampleForm.Attributes.Add("OnClick", fullURL);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void rptLicenseVersion_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lblSlashReview = (Label)e.Item.FindControl("lblSlashReview");
                Label lblpathIlink = (Label)e.Item.FindControl("lblpathIlink");
                LinkButton lblViewfile = (LinkButton)e.Item.FindControl("lnkViewDoc");
                LinkButton lblfilePath = (LinkButton)e.Item.FindControl("lblpathDownload");
                LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnLicenseVersionDoc");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);

                LinkButton lblDocumentVersion = (LinkButton)e.Item.FindControl("lblLicenseDocumentVersion");
                scriptManager.RegisterAsyncPostBackControl(lblDocumentVersion);
                if (lblpathIlink.Text == "True")
                {
                    lblViewfile.Visible = false;
                    lblfilePath.Visible = true;
                    lblDownLoadfile.Visible = false;
                    lblSlashReview.Visible = false;
                }
                else
                {
                    lblViewfile.Visible = true;
                    lblfilePath.Visible = false;
                    lblDownLoadfile.Visible = true;
                    lblSlashReview.Visible = true;
                }
            }
            //if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            //{
            //    LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnLicenseVersionDoc");
            //    var scriptManager = ScriptManager.GetCurrent(this.Page);
            //    scriptManager.RegisterPostBackControl(lblDownLoadfile);

            //    LinkButton lblLicenseDocumentVersion = (LinkButton)e.Item.FindControl("lblLicenseDocumentVersion");
            //    scriptManager.RegisterAsyncPostBackControl(lblLicenseDocumentVersion);
            //}
        }
        protected void rptLicenseVersion_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("RedirectURL"))
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "enableControls1()", true);
                }
                else
                {
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                List<SP_GetLicenseDocument_Result> ComplianceFileData = new List<SP_GetLicenseDocument_Result>();
                List<SP_GetLicenseDocument_Result> ComplianceDocument = new List<SP_GetLicenseDocument_Result>();
                ComplianceDocument = LicenseDocumentManagement.GetFileData(Convert.ToInt32(commandArgs[0]), -1).ToList();

                if (commandArgs[2].Equals("1.0"))
                {
                    ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == commandArgs[2]).ToList();
                    if (ComplianceFileData.Count <= 0)
                    {
                        ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == null).ToList();
                    }
                }
                else
                {
                    ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == commandArgs[2]).ToList();
                }

                    if (e.CommandName.Equals("version"))
                    {
                        if (e.CommandName.Equals("version"))
                        {
                            rptLicenseDocumnets.DataSource = ComplianceFileData.Where(entry => entry.FileType == 1).ToList();
                            rptLicenseDocumnets.DataBind();

                            rptLicenseWorkingFiles.DataSource = ComplianceFileData.Where(entry => entry.FileType == 2).ToList();
                            rptLicenseWorkingFiles.DataBind();

                            var documentVersionData = ComplianceDocument.Select(x => new
                            {
                                ID = x.ID,
                                ScheduledOnID = x.ScheduledOnID,
                                LicenseID = x.LicenseID,
                                Version = string.IsNullOrEmpty(x.Version) ? "1.0" : x.Version,
                                VersionDate = x.VersionDate,
                                VersionComment = string.IsNullOrEmpty(x.VersionComment) ? "" : x.VersionComment,
                                FileID = x.FileID,
                                ISLink = x.ISLink,
                                FilePath = x.FilePath,
                                FileName = x.FileName
                            }).GroupBy(entry => new { entry.Version, entry.ISLink }).Select(entry => entry.FirstOrDefault()).ToList();

                            rptLicenseVersion.DataSource = documentVersionData;
                            rptLicenseVersion.DataBind();

                            //upComplianceDetails1.Update();                    
                        }
                    }
                    else if (e.CommandName.Equals("Download"))
                    {
                        using (ZipFile ComplianceZip = new ZipFile())
                        {
                            var LicenseData = LicenseDocumentManagement.GetLicense(Convert.ToInt32(commandArgs[0]));

                            ComplianceZip.AddDirectoryByName(LicenseData.LicenseNo);
                            ComplianceFileData = ComplianceFileData.Where(x => x.ISLink == false).ToList();
                            if (ComplianceFileData.Count > 0)
                            {
                                int i = 0;
                                foreach (var file in ComplianceFileData)
                                {
                                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        string ext = Path.GetExtension(file.FileName);
                                        string[] filename = file.FileName.Split('.');
                                        string str = filename[0] + i + "." + ext;
                                        if (file.EnType == "M")
                                        {
                                            ComplianceZip.AddEntry(LicenseData.LicenseNo + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        else
                                        {
                                            ComplianceZip.AddEntry(LicenseData.LicenseNo + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        i++;
                                    }
                                }
                            }
                            var zipMs = new MemoryStream();
                            ComplianceZip.Save(zipMs);
                            zipMs.Position = 0;
                            byte[] data = zipMs.ToArray();
                            Response.Buffer = true;
                            Response.ClearContent();
                            Response.ClearHeaders();
                            Response.Clear();
                            Response.ContentType = "application/zip";
                            Response.AddHeader("content-disposition", "attachment; filename=LicenseDocuments.zip");
                            Response.BinaryWrite(data);
                            Response.Flush();
                            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                        }
                    }
                    else if (e.CommandName.Equals("View"))
                    {

                        string[] commandArg = e.CommandArgument.ToString().Split(',');
                        List<SP_GetLicenseDocument_Result> CMPDocuments = LicenseDocumentManagement.GetFileData(Convert.ToInt32(commandArgs[0]), -1).ToList();
                        Session["LicenseID"] = commandArg[0];
                        if (CMPDocuments != null)
                        {
                            List<SP_GetLicenseDocument_Result> entitiesData = CMPDocuments.Where(entry => entry.Version != null).ToList();
                            if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                            {
                                SP_GetLicenseDocument_Result entityData = new SP_GetLicenseDocument_Result();
                                entityData.Version = "1.0";
                                entityData.LicenseID = Convert.ToInt64(commandArg[0]);
                                entitiesData.Add(entityData);
                            }

                            if (entitiesData.Count > 0)
                            {
                                CMPDocuments = CMPDocuments.Where(x => x.ISLink == false).ToList();
                                foreach (var file in CMPDocuments)
                                {
                                    rptLicenseVersionView.DataSource = entitiesData.OrderBy(entry => entry.Version);
                                    rptLicenseVersionView.DataBind();
                                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        string Folder = "~/TempFiles";
                                        string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                        string DateFolder = Folder + "/" + File;

                                        string extension = System.IO.Path.GetExtension(filePath);
                                        if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                        {
                                            lblMessageReviewer1.Text = "";
                                            lblMessageReviewer1.Text = "Zip file can't view please download it";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileLicenseAllShowPopUp();", true);
                                        }
                                        else
                                        {
                                            Directory.CreateDirectory(Server.MapPath(DateFolder));

                                            if (!Directory.Exists(DateFolder))
                                            {
                                                Directory.CreateDirectory(Server.MapPath(DateFolder));
                                            }

                                            string customerID = Convert.ToString(AuthenticationHelper.CustomerID);

                                            string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                            string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                            string FileName = DateFolder + "/" + User + "" + extension;

                                            FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                            BinaryWriter bw = new BinaryWriter(fs);
                                            if (file.EnType == "M")
                                            {
                                                bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            bw.Close();
                                            CompDocReviewPath = FileName;

                                            CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);

                                            lblMessageReviewer1.Text = "";

                                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfilelicense('" + CompDocReviewPath + "');", true);
                                        }
                                    }
                                    else
                                    {
                                        lblMessageReviewer1.Text = "There is no file to preview";
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileLicenseAllShowPopUp();", true);
                                    }
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void rptLicenseDocumnets_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lblpathCompIlink = (Label)e.Item.FindControl("lblCompDocpathIlink");
                LinkButton lblRedirectfile = (LinkButton)e.Item.FindControl("lblCompDocpathDownload");
                LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnLicenseDocumnets");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);
                if (lblpathCompIlink.Text == "True")
                {
                    lblDownLoadfile.Visible = false;
                    lblRedirectfile.Visible = true;
                }
                else
                {
                    lblDownLoadfile.Visible = true;
                    lblRedirectfile.Visible = false;
                }
            }
            //if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            //{
            //    LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnLicenseDocumnets");
            //    var scriptManager = ScriptManager.GetCurrent(this.Page);
            //    scriptManager.RegisterPostBackControl(lblDownLoadfile);
            //}
        }
        protected void rptLicenseDocumnets_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("RedirectURL"))
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "enableControls1()", true);
                else
                    DownloadFile(Convert.ToInt32(e.CommandArgument));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rptLicenseWorkingFiles_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lblWorkCompDocpathIlink = (Label)e.Item.FindControl("lblWorkCompDocpathIlink");
                LinkButton lblWorkCompDocpathDownload = (LinkButton)e.Item.FindControl("lblWorkCompDocpathDownload");
                LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnLicenseWorkingFiles");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);

                if (lblWorkCompDocpathIlink.Text == "True")
                {
                    lblDownLoadfile.Visible = false;
                    lblWorkCompDocpathDownload.Visible = true;
                }
                else
                {
                    lblDownLoadfile.Visible = true;
                    lblWorkCompDocpathDownload.Visible = false;
                }
            }
            //if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            //{
            //    LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnLicenseWorkingFiles");
            //    var scriptManager = ScriptManager.GetCurrent(this.Page);
            //    scriptManager.RegisterPostBackControl(lblDownLoadfile);
            //}
        }
        protected void rptLicenseWorkingFiles_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("RedirectURL"))
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "enableControls1()", true);
                else
                    DownloadFile(Convert.ToInt32(e.CommandArgument));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void rptLicenseVersionView_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);

                LinkButton lblDocumentVersionView = (LinkButton)e.Item.FindControl("lblLicenseVersionView");
                scriptManager.RegisterAsyncPostBackControl(lblDocumentVersionView);
            }
        }
        protected void rptLicenseVersionView_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                if (e.CommandName.Equals("View"))
                {

                    string[] commandArg = e.CommandArgument.ToString().Split(',');
                    List<SP_GetLicenseDocument_Result> CMPDocuments = LicenseDocumentManagement.GetFileData(Convert.ToInt32(commandArgs[0]), Convert.ToInt32(commandArgs[2])).ToList();
                    Session["LicenseID"] = commandArg[0];

                    if (CMPDocuments != null)
                    {
                        List<SP_GetLicenseDocument_Result> entitiesData = CMPDocuments.Where(entry => entry.Version != null).ToList();
                        if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                        {
                            SP_GetLicenseDocument_Result entityData = new SP_GetLicenseDocument_Result();
                            entityData.Version = "1.0";
                            entityData.LicenseID = Convert.ToInt64(commandArg[0]);
                            entitiesData.Add(entityData);
                        }

                        if (entitiesData.Count > 0)
                        {
                            foreach (var file in CMPDocuments)
                            {
                                string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                if (file.FilePath != null && File.Exists(filePath))
                                {
                                    string Folder = "~/TempFiles";
                                    string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                    string DateFolder = Folder + "/" + File;
                                    string extension = System.IO.Path.GetExtension(filePath);

                                    if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                    {
                                        lblMessageReviewer1.Text = "";
                                        lblMessageReviewer1.Text = "Zip file can't view please download it";
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileLicenseAllShowPopUp();", true);
                                    }
                                    else
                                    {
                                        Directory.CreateDirectory(Server.MapPath(DateFolder));

                                        if (!Directory.Exists(DateFolder))
                                        {
                                            Directory.CreateDirectory(Server.MapPath(DateFolder));
                                        }

                                        string customerID = Convert.ToString(AuthenticationHelper.CustomerID);

                                        string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                        string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                        string FileName = DateFolder + "/" + User + "" + extension;

                                        FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                        BinaryWriter bw = new BinaryWriter(fs);
                                        if (file.EnType == "M")
                                        {
                                            bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        else
                                        {
                                            bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        bw.Close();
                                        CompDocReviewPath = FileName;
                                        CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfilelicense('" + CompDocReviewPath + "');", true);
                                        lblMessageReviewer1.Text = "";

                                    }
                                }
                                else
                                {
                                    lblMessageReviewer1.Text = "There is no file to preview";
                                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileLicenseAllShowPopUp();", true);
                                }
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void DownloadFile(int fileId)
        {
            try
            {
                var file = Business.ComplianceManagement.GetFile(fileId);

                if (file.FilePath != null)
                {
                    string filePath = string.Empty;
                    //Change by rahul on 21 JAN 2017 for Azure Drive or Local Drive
                    if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                    {
                        string pathvalue = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.Name));
                        filePath = pathvalue.Replace("~", ConfigurationManager.AppSettings["DriveUrl"]);
                    }
                    else
                    {
                        filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
                    }
                    if (filePath != null && File.Exists(filePath))
                    {
                        Response.Buffer = true;
                        Response.Clear();
                        Response.ClearContent();
                        Response.ContentType = "application/octet-stream";
                        Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                        if (file.EnType == "M")
                        {
                            Response.BinaryWrite(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                        }
                        else
                        {
                            Response.BinaryWrite(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                        }
                        Response.Flush(); // send it to the client to download
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                    }
                }


            }
            catch (Exception)
            {
                throw;
                //LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnEditLicense_Click(object sender, EventArgs e)
        {
            try
            {
                long licenseInstanceID = 0;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["LicenseInstanceID"])))
                {
                    licenseInstanceID = Convert.ToInt32(ViewState["LicenseInstanceID"]);
                    if (licenseInstanceID != 0)
                    {
                        var licenseRecord = LicenseMgmt.GetLicenseByID(licenseInstanceID);
                        if (licenseRecord != null)
                        {
                            var MasterTransction = LicenseMgmt.GetPreviousLicenseDetail(licenseInstanceID);
                            BindTransactions(licenseInstanceID);

                            var scheduledetails = LicenseMgmt.GetLicenseComplianceInstanceScheduleOnByID(licenseInstanceID);
                            if (scheduledetails != null)
                            {
                                if (MasterTransction != null)
                                {

                                    hdnlicremindbeforenoofdays.Value = licenseRecord.RemindBeforeNoOfDays.ToString();
                                    hdnlicensetypeid.Value = licenseRecord.LicenseTypeID.ToString();
                                    lblLicenseType.Text = MasterTransction.LicensetypeName;
                                    lblLicenseNumber.Text = MasterTransction.LicenseNo;
                                    lblLicenseTitle.Text = MasterTransction.Licensetitle;
                                    lblApplicationDate.Text = Convert.ToDateTime(MasterTransction.ApplicationDate).ToString("dd-MMM-yyyy");
                                    lblStartdate.Text = Convert.ToDateTime(MasterTransction.StartDate).ToString("dd-MMM-yyyy");
                                    lblEndDate.Text = Convert.ToDateTime(MasterTransction.EndDate).ToString("dd-MMM-yyyy");
                                    txtCost.Text = Convert.ToString(MasterTransction.Cost);
                                    var LICInfo = Business.ActManagement.GetByLICID(MasterTransction.LicenseID);
                                    txtFileno.Text = LICInfo.FileNO;
                                    txtphysicalLocation.Text = LICInfo.PhysicalLocation;

                                    var documentVersionData = LicenseDocumentManagement.GetFileData(licenseInstanceID, -1).Select(x => new
                                    {
                                        ID = x.ID,
                                        ScheduledOnID = x.ScheduledOnID,
                                        LicenseID = x.LicenseID,
                                        FileID = x.FileID,
                                        Version = string.IsNullOrEmpty(x.Version) ? "1.0" : x.Version,
                                        VersionDate = x.VersionDate,
                                        VersionComment = string.IsNullOrEmpty(x.VersionComment) ? "" : x.VersionComment,                                        
                                        ISLink = x.ISLink,
                                        FilePath = x.FilePath,
                                        FileName = x.FileName
                                    }).GroupBy(entry => new { entry.Version, entry.ISLink }).Select(entry => entry.FirstOrDefault()).ToList();


                                    rptLicenseVersion.DataSource = documentVersionData;
                                    rptLicenseVersion.DataBind();

                                    rptLicenseDocumnets.DataSource = null;
                                    rptLicenseDocumnets.DataBind();
                                }
                                var complianceInfo = Business.ComplianceManagement.GetComplianceByInstanceID((int)scheduledetails.ComplianceScheduleOnID);
                                var complianceForm = Business.ComplianceManagement.GetComplianceFormByID(complianceInfo.ID);
                                if (complianceInfo != null)
                                {
                                    var complianceCategoryInfo = Business.ComplianceManagement.GetComplianceCategoryByInstanceID(complianceInfo.ActID);
                                    lblComplianceID.Text = Convert.ToString(complianceInfo.ID);
                                    lblCategory.Text = Convert.ToString(complianceCategoryInfo.Name);
                                    lnkSampleForm.Text = Convert.ToString(complianceInfo.SampleFormLink);
                                    lblComplianceDiscription.Text = complianceInfo.ShortDescription;
                                    lblDetailedDiscription.Text = complianceInfo.Description;
                                    lblFrequency.Text = Enumerations.GetEnumByID<Frequency>(Convert.ToInt32(complianceInfo.Frequency != null ? (int)complianceInfo.Frequency : -1));
                                    lblPenalty.Text = complianceInfo.PenaltyDescription;
                                    lblRule.Text = complianceInfo.Sections;
                                    lblFormNumber.Text = complianceInfo.RequiredForms;
                                    lblRefrenceText.Text = WebUtility.HtmlEncode(complianceInfo.ReferenceMaterialText);
                                    lblPenalty.Text = complianceInfo.PenaltyDescription;
                                    
                                    string risk = Business.ComplianceManagement.GetRiskType(complianceInfo, scheduledetails.ComplianceInstanceID);
                                    lblRisk.Text = risk;
                                    var ActInfo = Business.ActManagement.GetByID(complianceInfo.ActID);
                                    if (ActInfo != null)
                                    {
                                        lblActName.Text = ActInfo.Name;
                                    }
                                    if (complianceInfo.UploadDocument == true && complianceForm != null)
                                    {
                                        lbDownloadSample.Text = "Download";
                                        lbDownloadSample.CommandArgument = complianceForm.ComplianceID.ToString();
                                        sampleFormPath = complianceForm.FilePath;
                                        sampleFormPath = sampleFormPath.Substring(2, sampleFormPath.Length - 2);
                                        lnkViewSampleForm.Visible = true;
                                        lblSlash.Visible = true;
                                        if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                        {
                                            lblpathsample.Text = sampleFormPath;
                                        }
                                        else
                                        {
                                            lblpathsample.Text = sampleFormPath;
                                        }
                                    }
                                    else
                                    {
                                        lblSlash.Visible = false;
                                        lnkViewSampleForm.Visible = false;
                                        sampleFormPath = "";
                                    }
                                }
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public static List<ComplianceAssignment> GetAssignedUsers(int ComplianceInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var AssignedUsers = (from row in entities.ComplianceAssignments
                                     where row.ComplianceInstanceID == ComplianceInstanceID
                                     select row).GroupBy(entry => entry.RoleID).Select(entry => entry.FirstOrDefault()).ToList();

                return AssignedUsers;
            }
        }
        protected void btnSaveLicense_Click(object sender, EventArgs e)
        {
            if (UploadDocumentLink.Equals("True"))
            {
                bool formValidateSuccess = false;
                bool saveSuccess = false;
                List<string> lstErrorMsg = new List<string>();
                List<Tuple<int, int>> lstLicenseUserMapping = new List<Tuple<int, int>>();
                #region Validation for Start Date End Date  

                int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                if (String.IsNullOrEmpty(txtLicenseNo.Text))
                    lstErrorMsg.Add("Required license number");
                else
                    formValidateSuccess = true;

                if (String.IsNullOrEmpty(txtLicenseTitle.Text))
                    lstErrorMsg.Add("Required license title");
                else
                    formValidateSuccess = true;

                //var aaa = txtEndDate.Text;
                if (!string.IsNullOrEmpty(txtStartDate.Text))
                {
                    try
                    {
                        bool check = LicenseCommonMethods.CheckValidDate(txtStartDate.Text);
                        if (!check)
                        {
                            lstErrorMsg.Add("Please Check Start Date or Date should be in DD-MM-YYYY Format");
                        }
                    }
                    catch (Exception)
                    {
                        lstErrorMsg.Add("Please Check Start Date or Date should be in DD-MM-YYYY Format");
                    }
                }


                if (!string.IsNullOrEmpty(txtEndDate.Text))
                {
                    try
                    {
                        bool check = LicenseCommonMethods.CheckValidDate(txtEndDate.Text);
                        if (!check)
                            lstErrorMsg.Add("Please Check End Date or Date should be in DD-MM-YYYY Format");
                    }
                    catch (Exception)
                    {
                        lstErrorMsg.Add("Please Check End Date or Date should be in DD-MM-YYYY Format");
                    }
                }

                if (!string.IsNullOrEmpty(txtStartDate.Text)
                    && !string.IsNullOrEmpty(txtEndDate.Text))
                {
                    if (DateTime.Compare(DateTimeExtensions.GetDate(txtEndDate.Text.ToString()),
                        DateTimeExtensions.GetDate(txtStartDate.Text.ToString())) <= 0)
                    {
                        lstErrorMsg.Add("End Date should be greater than Start Date.");

                    }
                }
                if (!string.IsNullOrEmpty(TxtUploadocumentlnkLIc.Text))
                {
                    lstErrorMsg.Add("Please add Link for document.");
                }
                if (lstErrorMsg.Count > 0)
                {
                    formValidateSuccess = false;
                    showErrorMessages(lstErrorMsg, cvDuplicateEntry);
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "scrollUpPageScript", "scrollUpPage();", true);
                }
                else
                    formValidateSuccess = true;

                #endregion

                if (formValidateSuccess == true)
                {
                    if (!string.IsNullOrEmpty(txtLicenseNo.Text))
                    {
                        bool existLicNo = LicenseMgmt.ExistsLicenseNo(Convert.ToInt32(AuthenticationHelper.CustomerID), Convert.ToString(txtLicenseNo.Text));
                        //if (!existLicNo)
                        //{
                            long LicenseID = -1;
                            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["LicenseInstanceID"])))
                            {
                                LicenseID = Convert.ToInt32(ViewState["LicenseInstanceID"]);
                            }
                            else
                            {
                                LicenseID = Convert.ToInt32(Request.QueryString["AccessID"]);
                            }
                            long recurringdays = 0;
                            long licensetypeid = 0;
                            if (!string.IsNullOrEmpty(hdnlicremindbeforenoofdays.Value))
                            {
                                recurringdays = Convert.ToInt64(hdnlicremindbeforenoofdays.Value);
                            }
                            if (!string.IsNullOrEmpty(hdnlicensetypeid.Value))
                            {
                                licensetypeid = Convert.ToInt64(hdnlicensetypeid.Value);
                            }
                            DateTime ScheduleonDate = new DateTime();
                            if (!string.IsNullOrEmpty(txtEndDate.Text))
                            {
                                ScheduleonDate = DateTimeExtensions.GetDate(txtEndDate.Text);
                            }
                            if (recurringdays != 0)
                            {
                                ScheduleonDate = ScheduleonDate.AddDays(-Convert.ToInt32(recurringdays));
                            }
                            bool checklicexist = LicenseMgmt.ExistsComplianceScheduleOn(LicenseID, ScheduleonDate.Date);
                            if (!checklicexist)
                            {
                                bool checkdatexist = LicenseMgmt.ExistsActivationDate(LicenseID, DateTimeExtensions.GetDate(txtEndDate.Text));
                                if (!checkdatexist)
                                {
                                    var scheduledetails = LicenseMgmt.GetLicenseComplianceInstanceScheduleOnByID(LicenseID);
                                    long complianceInstanceId = -1;
                                    if (scheduledetails != null)
                                    {
                                        complianceInstanceId = scheduledetails.ComplianceInstanceID;
                                        if (LicenseID != -1)
                                        {
                                            string status = string.Empty;
                                            #region Save License Instance and Status                   
                                            Lic_tbl_LicenseInstance licenseRecord = new Lic_tbl_LicenseInstance()
                                            {
                                                UpdatedBy = UserID
                                            };
                                            Lic_tbl_LicenseInstance_Log newLic_tbl_LicenseInstance_Log = new Lic_tbl_LicenseInstance_Log()
                                            {
                                                CustomerID = AuthenticationHelper.CustomerID,
                                                UpdatedBy = AuthenticationHelper.UserID,
                                                CreatedBy = AuthenticationHelper.UserID,
                                                UpdatedOn = DateTime.Now,
                                                LicenseID = LicenseID,
                                                RemindBeforeNoOfDays = recurringdays,
                                                LicenseTypeID = licensetypeid,
                                                IsStatutory = true,
                                            };
                                            if (!string.IsNullOrEmpty(txtLicenseNo.Text))
                                            {
                                                licenseRecord.LicenseNo = Convert.ToString(txtLicenseNo.Text);
                                                newLic_tbl_LicenseInstance_Log.LicenseNo = Convert.ToString(txtLicenseNo.Text);
                                            }
                                            if (!string.IsNullOrEmpty(txtLicenseTitle.Text))
                                            {
                                                licenseRecord.LicenseTitle = Convert.ToString(txtLicenseTitle.Text);
                                                newLic_tbl_LicenseInstance_Log.LicenseTitle = Convert.ToString(txtLicenseTitle.Text);
                                            }
                                            if (!string.IsNullOrEmpty(txtStartDate.Text))
                                            {
                                                licenseRecord.StartDate = DateTimeExtensions.GetDate(txtStartDate.Text);
                                                newLic_tbl_LicenseInstance_Log.StartDate = DateTimeExtensions.GetDate(txtStartDate.Text);
                                            }
                                            if (!string.IsNullOrEmpty(txtEndDate.Text))
                                            {
                                                licenseRecord.EndDate = DateTimeExtensions.GetDate(txtEndDate.Text);
                                                newLic_tbl_LicenseInstance_Log.EndDate = DateTimeExtensions.GetDate(txtEndDate.Text);
                                            }

                                            if (!string.IsNullOrEmpty(txtFileno.Text))
                                            {
                                                licenseRecord.FileNO = Convert.ToString(txtFileno.Text);
                                                
                                            }

                                            if (!string.IsNullOrEmpty(txtphysicalLocation.Text))
                                            {
                                                licenseRecord.PhysicalLocation = Convert.ToString(txtphysicalLocation.Text);
                                               
                                            }

                                            if (!string.IsNullOrEmpty(txtCost.Text))
                                            {
                                                licenseRecord.Cost = Convert.ToDecimal(txtCost.Text);
                                                newLic_tbl_LicenseInstance_Log.Cost = Convert.ToDecimal(txtCost.Text);
                                            }
                                            licenseRecord.RemindBeforeNoOfDays = Convert.ToInt32(recurringdays);
                                            licenseRecord.ID = LicenseID;

                                            LicenseMgmt.UpdateLicense(licenseRecord);
                                            LicenseMgmt.CreateLicenseLog(newLic_tbl_LicenseInstance_Log);
                                            int statusId = 0;

                                            if (!string.IsNullOrEmpty(Convert.ToString(txtStartDate.Text)) || !string.IsNullOrEmpty(Convert.ToString(txtEndDate.Text)))
                                            {
                                                if (DateTimeExtensions.GetDate(Convert.ToString(txtEndDate.Text)).Date <= DateTime.Today.Date)
                                                {
                                                    statusId = 3;        //Expired
                                                }
                                                else if (DateTimeExtensions.GetDate(Convert.ToString(txtEndDate.Text)).Date <= DateTime.Today.Date.AddDays(30))
                                                {
                                                    statusId = 4;            //Expiring
                                                }
                                                else if (DateTimeExtensions.GetDate(Convert.ToString(txtEndDate.Text)).Date > Convert.ToDateTime(DateTime.Now).Date)
                                                {
                                                    statusId = 2;            //Active
                                                }
                                                else
                                                {
                                                    statusId = 3;        //Expired
                                                }
                                            }
                                            else
                                                statusId = 1;       //Draft                            


                                            if (LicenseID > 0)
                                            {
                                              
                                                if (statusId == 2)
                                                {
                                                    status = "Active";
                                                }
                                                else if (statusId == 3)
                                                {
                                                    status = "Expired";
                                                }
                                                else if (statusId == 4)
                                                {
                                                    status = "Expiring";
                                                }
                                                //Lic_tbl_LicenseStatusTransaction newStatusRecord = new Lic_tbl_LicenseStatusTransaction()
                                                //{
                                                //    CustomerID = (int)AuthenticationHelper.CustomerID,
                                                //    LicenseID = LicenseID,
                                                //    StatusID = statusId,
                                                //    StatusChangeOn = DateTime.Now,
                                                //    IsActive = true,
                                                //    CreatedBy = AuthenticationHelper.UserID,
                                                //    CreatedOn = DateTime.Now,
                                                //    UpdatedBy = AuthenticationHelper.UserID,
                                                //    UpdatedOn = DateTime.Now,
                                                //    Remark = "Change license status to " + status + ""
                                                //};
                                                //saveSuccess = LicenseMgmt.CreateLicenseStatusTransaction(newStatusRecord);



                                                Lic_tbl_LicenseAudit_Log newLicenseInstance_Log = new Lic_tbl_LicenseAudit_Log()
                                                {
                                                    CustomerID = (int)AuthenticationHelper.CustomerID,
                                                    LicenseID = LicenseID,
                                                    StatusID = statusId,
                                                    IsActive = true,
                                                    CreatedBy = AuthenticationHelper.UserID,
                                                    Remark = "Change license status to " + status + ""
                                                };


                                                if (!string.IsNullOrEmpty(txtStartDate.Text))
                                                    newLicenseInstance_Log.StartDate = DateTimeExtensions.GetDate(txtStartDate.Text);

                                                if (!string.IsNullOrEmpty(txtEndDate.Text))
                                                    newLicenseInstance_Log.EndDate = DateTimeExtensions.GetDate(txtEndDate.Text);

                                                saveSuccess = LicenseMgmt.CreateLicenseAuditLog(newLicenseInstance_Log);
                                            }
                                            if (saveSuccess)
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "License Details Created Sucessfully";
                                                VSLicensePopup.CssClass = "alert alert-success";
                                            }
                                            #endregion

                                            #region Statutory Non Statutory Compliances Entry                       
                                            try
                                            {
                                                bool statutorySuccess = false;
                                                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                                {
                                                    if (formValidateSuccess)
                                                    {
                                                        #region Statutory
                                                        try
                                                        {
                                                            long complianceScheduleOnId = 0;
                                                            statutorySuccess = false;
                                                            if (complianceInstanceId > 0)
                                                            {
                                                                var AssignedRole = GetAssignedUsers((int)complianceInstanceId);
                                                                if (licenseRecord.RemindBeforeNoOfDays == 0)
                                                                {
                                                                    ComplianceScheduleOn complianceScheduleOn = new ComplianceScheduleOn()
                                                                    {
                                                                        ScheduleOn = licenseRecord.EndDate.Value.Date,
                                                                        ComplianceInstanceID = complianceInstanceId,
                                                                        IsActive = true,
                                                                        IsUpcomingNotDeleted = true,
                                                                    };
                                                                    complianceScheduleOnId = LicenseMgmt.CreateComplianceScheduleOn(complianceScheduleOn);

                                                                    Lic_tbl_LicenseStatusTransaction newStatusRecord = new Lic_tbl_LicenseStatusTransaction()
                                                                    {
                                                                        CustomerID = (int)AuthenticationHelper.CustomerID,
                                                                        LicenseID = LicenseID,
                                                                        StatusID = statusId,
                                                                        StatusChangeOn = DateTime.Now,
                                                                        IsActive = true,
                                                                        CreatedBy = AuthenticationHelper.UserID,
                                                                        CreatedOn = DateTime.Now,
                                                                        UpdatedBy = AuthenticationHelper.UserID,
                                                                        UpdatedOn = DateTime.Now,
                                                                        Remark = "Change license status to " + status + "",
                                                                        ComplianceScheduleOnID= complianceScheduleOnId,
                                                                    };
                                                                    saveSuccess = LicenseMgmt.CreateLicenseStatusTransaction(newStatusRecord);

                                                                    #region Save Document Against Current ComplianceScheduleonID
                                                                    if (complianceScheduleOnId > 0)
                                                                    {
                                                                        Lic_tbl_LicenseComplianceInstanceScheduleOnMapping lictbllicensecomplianceschedulonmapping = new Lic_tbl_LicenseComplianceInstanceScheduleOnMapping()
                                                                        {
                                                                            LicenseID = licenseRecord.ID,
                                                                            ComplianceInstanceID = complianceInstanceId,
                                                                            ComplianceScheduleOnID = complianceScheduleOnId,
                                                                            IsActivation = "Active",
                                                                            IsStatutoryORInternal = "S"
                                                                        };
                                                                        LicenseMgmt.CreateLicenseLicenseComplianceInstanceScheduleOnMapping(lictbllicensecomplianceschedulonmapping);



                                                                        ComplianceTransaction transaction = new ComplianceTransaction()
                                                                        {
                                                                            ComplianceInstanceId = complianceInstanceId,
                                                                            StatusId = 1,
                                                                            Remarks = "New compliance assigned.",
                                                                            Dated = DateTime.Now,
                                                                            CreatedBy = UserID,
                                                                            CreatedByText = UserName,
                                                                            ComplianceScheduleOnID = complianceScheduleOnId,
                                                                        };
                                                                        
                                                                        List<KeyValuePair<string, Byte[]>> Filelist = new List<KeyValuePair<string, Byte[]>>();
                                                                        List<FileData> files = new List<FileData>();
                                                                        List<KeyValuePair<string, int>> list = new List<KeyValuePair<string, int>>();
                                                                        HttpFileCollection fileCollection = Request.Files;
                                                                        bool blankfileCount = true;
                                                                        string version = DocumentManagement.GetDocumnetVersion(Convert.ToInt32(complianceScheduleOnId));
                                                                        if (!string.IsNullOrEmpty(TxtUploadocumentlnkLIc.Text))
                                                                        {
                                                                            String fileName = string.Empty;
                                                                            fileName = "ComplianceDoc_" + TxtUploadocumentlnkLIc.Text;
                                                                            list.Add(new KeyValuePair<string, int>(fileName, 1));

                                                                            //String fileName = "LicenseUploadDoc_" + TxtUploadocumentlnkLIc.Text;
                                                                            FileData file = new FileData()
                                                                            {
                                                                                Name = fileName,
                                                                                FilePath = TxtUploadocumentlnkLIc.Text,
                                                                                Version = version,
                                                                                VersionDate = DateTime.Now,
                                                                                ISLink = true
                                                                            };
                                                                            files.Add(file);
                                                                        }
                                                                        else
                                                                        {
                                                                            blankfileCount = false;
                                                                        }
                                                                        
                                                                        bool flag = false;
                                                                        if (blankfileCount)
                                                                        {
                                                                            flag = Business.ComplianceManagement.CreateTransaction(transaction, files, list, Filelist);
                                                                        }
                                                                        else
                                                                        {
                                                                            ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "missingAlert", "alert('Please do not upload virus file or blank files.')", true);
                                                                            cvDuplicateEntry.IsValid = false;
                                                                            VSLicensePopup.CssClass = "alert alert-danger";
                                                                            cvDuplicateEntry.ErrorMessage = "Please do not upload virus file or blank files.";
                                                                        }


                                                                        if (AssignedRole.Count > 0)
                                                                        {
                                                                            foreach (var roles in AssignedRole)
                                                                            {
                                                                                if (roles.RoleID != 6)
                                                                                {
                                                                                    var reminders = (from RT in entities.ReminderTemplates
                                                                                                     where RT.Frequency == 2 && RT.IsSubscribed == true
                                                                                                     select RT).ToList();

                                                                                    reminders.ForEach(day =>
                                                                                    {
                                                                                        ComplianceReminder reminder = new ComplianceReminder()
                                                                                        {
                                                                                            ComplianceAssignmentID = roles.ID,
                                                                                            ReminderTemplateID = day.ID,
                                                                                            ComplianceDueDate = licenseRecord.EndDate.Value.Date,
                                                                                            RemindOn = licenseRecord.EndDate.Value.Date.Date.AddDays(-1 * day.TimeInDays),
                                                                                        };
                                                                                        reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                                                                        entities.ComplianceReminders.Add(reminder);

                                                                                    });
                                                                                }
                                                                            }
                                                                            entities.SaveChanges();
                                                                        }
                                                                    }//complianceScheduleOnId
                                                                    #endregion
                                                                }
                                                                else
                                                                {
                                                                    #region Application Days
                                                                    DateTime Applicationdate = Convert.ToDateTime(licenseRecord.EndDate);
                                                                    Applicationdate = Applicationdate.AddDays(-Convert.ToDouble(licenseRecord.RemindBeforeNoOfDays));
                                                                    ComplianceScheduleOn complianceScheduleOn = new ComplianceScheduleOn()
                                                                    {
                                                                        ScheduleOn = Applicationdate.Date,
                                                                        ComplianceInstanceID = complianceInstanceId,
                                                                        IsActive = true,
                                                                        IsUpcomingNotDeleted = true,
                                                                    };
                                                                    complianceScheduleOnId = LicenseMgmt.CreateComplianceScheduleOn(complianceScheduleOn);

                                                                    Lic_tbl_LicenseStatusTransaction newStatusRecord = new Lic_tbl_LicenseStatusTransaction()
                                                                    {
                                                                        CustomerID = (int)AuthenticationHelper.CustomerID,
                                                                        LicenseID = LicenseID,
                                                                        StatusID = statusId,
                                                                        StatusChangeOn = DateTime.Now,
                                                                        IsActive = true,
                                                                        CreatedBy = AuthenticationHelper.UserID,
                                                                        CreatedOn = DateTime.Now,
                                                                        UpdatedBy = AuthenticationHelper.UserID,
                                                                        UpdatedOn = DateTime.Now,
                                                                        Remark = "Change license status to " + status + "",
                                                                        ComplianceScheduleOnID = complianceScheduleOnId,
                                                                    };
                                                                    saveSuccess = LicenseMgmt.CreateLicenseStatusTransaction(newStatusRecord);

                                                                    #region Save Document Against Current ComplianceScheduleonID
                                                                    if (complianceScheduleOnId > 0)
                                                                    {
                                                                        Lic_tbl_LicenseComplianceInstanceScheduleOnMapping lictbllicensecomplianceschedulonmapping = new Lic_tbl_LicenseComplianceInstanceScheduleOnMapping()
                                                                        {
                                                                            LicenseID = licenseRecord.ID,
                                                                            ComplianceInstanceID = complianceInstanceId,
                                                                            ComplianceScheduleOnID = complianceScheduleOnId,
                                                                            IsActivation = "Application",
                                                                            IsStatutoryORInternal = "S"
                                                                        };
                                                                        LicenseMgmt.CreateLicenseLicenseComplianceInstanceScheduleOnMapping(lictbllicensecomplianceschedulonmapping);

                                                                        ComplianceTransaction transaction = new ComplianceTransaction()
                                                                        {
                                                                            ComplianceInstanceId = complianceInstanceId,
                                                                            StatusId = 1,
                                                                            Remarks = "New compliance assigned.",
                                                                            Dated = DateTime.Now,
                                                                            CreatedBy = UserID,
                                                                            CreatedByText = UserName,
                                                                            ComplianceScheduleOnID = complianceScheduleOnId,
                                                                        };
                                                                        List<KeyValuePair<string, Byte[]>> Filelist = new List<KeyValuePair<string, Byte[]>>();
                                                                        List<FileData> files = new List<FileData>();
                                                                        List<KeyValuePair<string, int>> list = new List<KeyValuePair<string, int>>();
                                                                        HttpFileCollection fileCollection = Request.Files;
                                                                        bool blankfileCount = true;
                                                                        string version = DocumentManagement.GetDocumnetVersion(Convert.ToInt32(complianceScheduleOnId));
                                                                        if (!string.IsNullOrEmpty(TxtUploadocumentlnkLIc.Text))
                                                                        {
                                                                            String fileName = string.Empty;
                                                                            fileName = "ComplianceDoc_" + TxtUploadocumentlnkLIc.Text;
                                                                            list.Add(new KeyValuePair<string, int>(fileName, 1));


                                                                            //String fileName = "LicenseUploadDoc_" + TxtUploadocumentlnkLIc.Text;
                                                                            FileData file = new FileData()
                                                                            {
                                                                                Name = fileName,
                                                                                FilePath = TxtUploadocumentlnkLIc.Text,
                                                                                Version = version,
                                                                                VersionDate = DateTime.Now,
                                                                                ISLink = true
                                                                            };
                                                                            files.Add(file);
                                                                        }
                                                                        else
                                                                        {
                                                                            blankfileCount = false;
                                                                        }

                                                                        
                                                                        bool flag = false;
                                                                        if (blankfileCount)
                                                                        {
                                                                            flag = Business.ComplianceManagement.CreateTransaction(transaction, files, list, Filelist);
                                                                        }
                                                                        else
                                                                        {
                                                                            ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "missingAlert", "alert('Please do not upload virus file or blank files.')", true);
                                                                            cvDuplicateEntry.IsValid = false;
                                                                            VSLicensePopup.CssClass = "alert alert-danger";
                                                                            cvDuplicateEntry.ErrorMessage = "Please do not upload virus file or blank files.";
                                                                        }


                                                                        if (AssignedRole.Count > 0)
                                                                        {
                                                                            foreach (var roles in AssignedRole)
                                                                            {
                                                                                if (roles.RoleID != 6)
                                                                                {
                                                                                    var reminders = (from RT in entities.ReminderTemplates
                                                                                                     where RT.Frequency == 2 && RT.IsSubscribed == true
                                                                                                     select RT).ToList();

                                                                                    reminders.ForEach(day =>
                                                                                    {
                                                                                        ComplianceReminder reminder = new ComplianceReminder()
                                                                                        {
                                                                                            ComplianceAssignmentID = roles.ID,
                                                                                            ReminderTemplateID = day.ID,
                                                                                            ComplianceDueDate = Applicationdate.Date,
                                                                                            RemindOn = Applicationdate.Date.AddDays(-1 * day.TimeInDays),
                                                                                        };
                                                                                        reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                                                                        entities.ComplianceReminders.Add(reminder);

                                                                                    });
                                                                                }
                                                                            }
                                                                            entities.SaveChanges();
                                                                        }
                                                                    }// end complianceScheduleOnId
                                                                    #endregion
                                                                    #endregion
                                                                }
                                                            }
                                                            if (complianceInstanceId > 0)
                                                                statutorySuccess = true;
                                                            saveSuccess = statutorySuccess;

                                                        }
                                                        catch (Exception ex)
                                                        {
                                                            LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                                            cvDuplicateEntry.IsValid = false;
                                                            cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
                                                            VSLicensePopup.CssClass = "alert alert-danger";
                                                            saveSuccess = false;
                                                        }

                                                        #endregion

                                                        if (saveSuccess)
                                                        {
                                                            cvDuplicateEntry.IsValid = false;
                                                            cvDuplicateEntry.ErrorMessage = "Compliance Created and Assigned Sucessfully";
                                                            VSLicensePopup.CssClass = "alert alert-success";
                                                        }
                                                        else
                                                        {
                                                            cvDuplicateEntry.IsValid = false;
                                                            cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
                                                            VSLicensePopup.CssClass = "alert alert-danger";
                                                        }
                                                    }
                                                }//Using End
                                            }
                                            catch (Exception ex)
                                            {
                                                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
                                                VSLicensePopup.CssClass = "alert alert-danger";
                                            }
                                            #endregion
                                        }
                                    } //scheduledetails end
                                }//  checklicexist
                                else
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Same license period already present in the system";
                                    VSLicensePopup.CssClass = "alert alert-danger";
                                }
                            }// Scheduleon checklicexist
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Same schedule already present in the system.";
                                VSLicensePopup.CssClass = "alert alert-danger";
                            }
                       // }
                        //else
                        //{
                        //    cvDuplicateEntry.IsValid = false;
                        //    cvDuplicateEntry.ErrorMessage = "License Details with Same License Number already exists";
                        //    formValidateSuccess = false;
                        //}
                    }
                }//Formvalidation end
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Please Select Filters Properly. ";
                    VSLicensePopup.CssClass = "alert alert-danger";
                    if (lstErrorMsg.Count > 0)
                    {
                        formValidateSuccess = false;
                        showErrorMessages(lstErrorMsg, cvDuplicateEntry);
                    }
                }
            }
            else
            {
                bool formValidateSuccess = false;
                bool saveSuccess = false;
                List<string> lstErrorMsg = new List<string>();
                List<Tuple<int, int>> lstLicenseUserMapping = new List<Tuple<int, int>>();
                #region Validation for Start Date End Date  

                int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                if (String.IsNullOrEmpty(txtLicenseNo.Text))
                    lstErrorMsg.Add("Required license number");
                else
                    formValidateSuccess = true;

                if (String.IsNullOrEmpty(txtLicenseTitle.Text))
                    lstErrorMsg.Add("Required license title");
                else
                    formValidateSuccess = true;

                //var aaa = txtEndDate.Text;
                if (!string.IsNullOrEmpty(txtStartDate.Text))
                {
                    try
                    {
                        bool check = LicenseCommonMethods.CheckValidDate(txtStartDate.Text);
                        if (!check)
                        {
                            lstErrorMsg.Add("Please Check Start Date or Date should be in DD-MM-YYYY Format");
                        }
                    }
                    catch (Exception)
                    {
                        lstErrorMsg.Add("Please Check Start Date or Date should be in DD-MM-YYYY Format");
                    }
                }


                if (!string.IsNullOrEmpty(txtEndDate.Text))
                {
                    try
                    {
                        bool check = LicenseCommonMethods.CheckValidDate(txtEndDate.Text);
                        if (!check)
                            lstErrorMsg.Add("Please Check End Date or Date should be in DD-MM-YYYY Format");
                    }
                    catch (Exception)
                    {
                        lstErrorMsg.Add("Please Check End Date or Date should be in DD-MM-YYYY Format");
                    }
                }

                if (!string.IsNullOrEmpty(txtStartDate.Text)
                    && !string.IsNullOrEmpty(txtEndDate.Text))
                {
                    if (DateTime.Compare(DateTimeExtensions.GetDate(txtEndDate.Text.ToString()),
                        DateTimeExtensions.GetDate(txtStartDate.Text.ToString())) <= 0)
                    {
                        lstErrorMsg.Add("End Date should be greater than Start Date.");

                    }
                }
                if (fuSampleFile.HasFile)
                {
                    string[] validFileTypes = { "exe", "bat", "dll", "css", "js", };
                    string ext = System.IO.Path.GetExtension(fuSampleFile.PostedFile.FileName);
                    if (ext == "")
                    {
                        lstErrorMsg.Add("Please do not upload virus file or blank files or file has no extention.");
                    }
                    else
                    {
                        for (int i = 0; i < validFileTypes.Length; i++)
                        {
                            if (ext == "." + validFileTypes[i])
                            {
                                lstErrorMsg.Add("Please do not upload virus file or blank files.");
                                break;
                            }
                        }
                    }
                }
                else
                {
                    lstErrorMsg.Add("Please select file to upload.");

                }
                if (lstErrorMsg.Count > 0)
                {
                    formValidateSuccess = false;
                    showErrorMessages(lstErrorMsg, cvDuplicateEntry);
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "scrollUpPageScript", "scrollUpPage();", true);
                }
                else
                    formValidateSuccess = true;

                #endregion

                if (formValidateSuccess == true)
                {
                    if (!string.IsNullOrEmpty(txtLicenseNo.Text))
                    {
                        bool existLicNo = LicenseMgmt.ExistsLicenseNo(Convert.ToInt32(AuthenticationHelper.CustomerID), Convert.ToString(txtLicenseNo.Text));
                        //if (!existLicNo)
                        //{
                            long LicenseID = -1;
                            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["LicenseInstanceID"])))
                            {
                                LicenseID = Convert.ToInt32(ViewState["LicenseInstanceID"]);
                            }
                            else
                            {
                                LicenseID = Convert.ToInt32(Request.QueryString["AccessID"]);
                            }
                            long recurringdays = 0;
                            long licensetypeid = 0;
                            if (!string.IsNullOrEmpty(hdnlicremindbeforenoofdays.Value))
                            {
                                recurringdays = Convert.ToInt64(hdnlicremindbeforenoofdays.Value);
                            }
                            if (!string.IsNullOrEmpty(hdnlicensetypeid.Value))
                            {
                                licensetypeid = Convert.ToInt64(hdnlicensetypeid.Value);
                            }
                            DateTime ScheduleonDate = new DateTime();
                            if (!string.IsNullOrEmpty(txtEndDate.Text))
                            {
                                ScheduleonDate = DateTimeExtensions.GetDate(txtEndDate.Text);
                            }
                            if (recurringdays != 0)
                            {
                                ScheduleonDate = ScheduleonDate.AddDays(-Convert.ToInt32(recurringdays));
                            }
                            bool checklicexist = LicenseMgmt.ExistsComplianceScheduleOn(LicenseID, ScheduleonDate.Date);
                            if (!checklicexist)
                            {
                                bool checkdatexist = LicenseMgmt.ExistsActivationDate(LicenseID, DateTimeExtensions.GetDate(txtEndDate.Text));
                                if (!checkdatexist)
                                {
                                    var scheduledetails = LicenseMgmt.GetLicenseComplianceInstanceScheduleOnByID(LicenseID);
                                    long complianceInstanceId = -1;
                                    if (scheduledetails != null)
                                    {
                                        complianceInstanceId = scheduledetails.ComplianceInstanceID;
                                        if (LicenseID != -1)
                                        {
                                            string status = string.Empty;
                                            #region Save License Instance and Status                   
                                            Lic_tbl_LicenseInstance licenseRecord = new Lic_tbl_LicenseInstance()
                                            {
                                                UpdatedBy = UserID
                                            };
                                            Lic_tbl_LicenseInstance_Log newLic_tbl_LicenseInstance_Log = new Lic_tbl_LicenseInstance_Log()
                                            {
                                                CustomerID = AuthenticationHelper.CustomerID,
                                                UpdatedBy = AuthenticationHelper.UserID,
                                                CreatedBy = AuthenticationHelper.UserID,
                                                UpdatedOn = DateTime.Now,
                                                LicenseID = LicenseID,
                                                RemindBeforeNoOfDays = recurringdays,
                                                LicenseTypeID = licensetypeid,
                                                IsStatutory = true,
                                            };
                                            if (!string.IsNullOrEmpty(txtLicenseNo.Text))
                                            {
                                                licenseRecord.LicenseNo = Convert.ToString(txtLicenseNo.Text);
                                                newLic_tbl_LicenseInstance_Log.LicenseNo = Convert.ToString(txtLicenseNo.Text);
                                            }
                                            if (!string.IsNullOrEmpty(txtLicenseTitle.Text))
                                            {
                                                licenseRecord.LicenseTitle = Convert.ToString(txtLicenseTitle.Text);
                                                newLic_tbl_LicenseInstance_Log.LicenseTitle = Convert.ToString(txtLicenseTitle.Text);
                                            }
                                            if (!string.IsNullOrEmpty(txtStartDate.Text))
                                            {
                                                licenseRecord.StartDate = DateTimeExtensions.GetDate(txtStartDate.Text);
                                                newLic_tbl_LicenseInstance_Log.StartDate = DateTimeExtensions.GetDate(txtStartDate.Text);
                                            }
                                            if (!string.IsNullOrEmpty(txtEndDate.Text))
                                            {
                                                licenseRecord.EndDate = DateTimeExtensions.GetDate(txtEndDate.Text);
                                                newLic_tbl_LicenseInstance_Log.EndDate = DateTimeExtensions.GetDate(txtEndDate.Text);
                                            }

                                            if (!string.IsNullOrEmpty(txtFileno.Text))
                                            {
                                                licenseRecord.FileNO = Convert.ToString(txtFileno.Text);

                                            }

                                            if (!string.IsNullOrEmpty(txtphysicalLocation.Text))
                                            {
                                                licenseRecord.PhysicalLocation = Convert.ToString(txtphysicalLocation.Text);

                                            }

                                            if (!string.IsNullOrEmpty(txtCost.Text))
                                            {
                                                licenseRecord.Cost = Convert.ToDecimal(txtCost.Text);
                                                newLic_tbl_LicenseInstance_Log.Cost = Convert.ToDecimal(txtCost.Text);
                                            }

                                            licenseRecord.RemindBeforeNoOfDays = Convert.ToInt32(recurringdays);
                                            licenseRecord.ID = LicenseID;

                                            LicenseMgmt.UpdateLicense(licenseRecord);
                                            LicenseMgmt.CreateLicenseLog(newLic_tbl_LicenseInstance_Log);
                                            int statusId = 0;

                                            if (!string.IsNullOrEmpty(Convert.ToString(txtStartDate.Text)) || !string.IsNullOrEmpty(Convert.ToString(txtEndDate.Text)))
                                            {
                                                if (DateTimeExtensions.GetDate(Convert.ToString(txtEndDate.Text)).Date <= DateTime.Today.Date)
                                                {
                                                    statusId = 3;        //Expired
                                                }
                                                else if (DateTimeExtensions.GetDate(Convert.ToString(txtEndDate.Text)).Date <= DateTime.Today.Date.AddDays(30))
                                                {
                                                    statusId = 4;            //Expiring
                                                }
                                                else if (DateTimeExtensions.GetDate(Convert.ToString(txtEndDate.Text)).Date > Convert.ToDateTime(DateTime.Now).Date)
                                                {
                                                    statusId = 2;            //Active
                                                }
                                                else
                                                {
                                                    statusId = 3;        //Expired
                                                }
                                            }
                                            else
                                                statusId = 1;       //Draft                            


                                            if (LicenseID > 0)
                                            {
                                               
                                                if (statusId == 2)
                                                {
                                                    status = "Active";
                                                }
                                                else if (statusId == 3)
                                                {
                                                    status = "Expired";
                                                }
                                                else if (statusId == 4)
                                                {
                                                    status = "Expiring";
                                                }
                                                //Lic_tbl_LicenseStatusTransaction newStatusRecord = new Lic_tbl_LicenseStatusTransaction()
                                                //{
                                                //    CustomerID = (int)AuthenticationHelper.CustomerID,
                                                //    LicenseID = LicenseID,
                                                //    StatusID = statusId,
                                                //    StatusChangeOn = DateTime.Now,
                                                //    IsActive = true,
                                                //    CreatedBy = AuthenticationHelper.UserID,
                                                //    CreatedOn = DateTime.Now,
                                                //    UpdatedBy = AuthenticationHelper.UserID,
                                                //    UpdatedOn = DateTime.Now,
                                                //    Remark = "Change license status to " + status + ""
                                                //};
                                                //saveSuccess = LicenseMgmt.CreateLicenseStatusTransaction(newStatusRecord);



                                                Lic_tbl_LicenseAudit_Log newLicenseInstance_Log = new Lic_tbl_LicenseAudit_Log()
                                                {
                                                    CustomerID = (int)AuthenticationHelper.CustomerID,
                                                    LicenseID = LicenseID,
                                                    StatusID = statusId,
                                                    IsActive = true,
                                                    CreatedBy = AuthenticationHelper.UserID,
                                                    Remark = "Change license status to " + status + ""
                                                };


                                                if (!string.IsNullOrEmpty(txtStartDate.Text))
                                                    newLicenseInstance_Log.StartDate = DateTimeExtensions.GetDate(txtStartDate.Text);

                                                if (!string.IsNullOrEmpty(txtEndDate.Text))
                                                    newLicenseInstance_Log.EndDate = DateTimeExtensions.GetDate(txtEndDate.Text);

                                                saveSuccess = LicenseMgmt.CreateLicenseAuditLog(newLicenseInstance_Log);
                                            }
                                            if (saveSuccess)
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "License Details Created Sucessfully";
                                                VSLicensePopup.CssClass = "alert alert-success";
                                            }
                                            #endregion

                                            #region Statutory Non Statutory Compliances Entry                       
                                            try
                                            {
                                                bool statutorySuccess = false;
                                                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                                {
                                                    if (formValidateSuccess)
                                                    {
                                                        #region Statutory
                                                        try
                                                        {
                                                            long complianceScheduleOnId = 0;
                                                            statutorySuccess = false;
                                                            if (complianceInstanceId > 0)
                                                            {
                                                                var AssignedRole = GetAssignedUsers((int)complianceInstanceId);
                                                                if (licenseRecord.RemindBeforeNoOfDays == 0)
                                                                {
                                                                    ComplianceScheduleOn complianceScheduleOn = new ComplianceScheduleOn()
                                                                    {
                                                                        ScheduleOn = licenseRecord.EndDate.Value.Date,
                                                                        ComplianceInstanceID = complianceInstanceId,
                                                                        IsActive = true,
                                                                        IsUpcomingNotDeleted = true,
                                                                    };
                                                                    complianceScheduleOnId = LicenseMgmt.CreateComplianceScheduleOn(complianceScheduleOn);
                                                                    Lic_tbl_LicenseStatusTransaction newStatusRecord = new Lic_tbl_LicenseStatusTransaction()
                                                                    {
                                                                        CustomerID = (int)AuthenticationHelper.CustomerID,
                                                                        LicenseID = LicenseID,
                                                                        StatusID = statusId,
                                                                        StatusChangeOn = DateTime.Now,
                                                                        IsActive = true,
                                                                        CreatedBy = AuthenticationHelper.UserID,
                                                                        CreatedOn = DateTime.Now,
                                                                        UpdatedBy = AuthenticationHelper.UserID,
                                                                        UpdatedOn = DateTime.Now,
                                                                        Remark = "Change license status to " + status + "",
                                                                        ComplianceScheduleOnID = complianceScheduleOnId,
                                                                    };
                                                                    saveSuccess = LicenseMgmt.CreateLicenseStatusTransaction(newStatusRecord);
                                                                    #region Save Document Against Current ComplianceScheduleonID
                                                                    if (complianceScheduleOnId > 0)
                                                                    {
                                                                        Lic_tbl_LicenseComplianceInstanceScheduleOnMapping lictbllicensecomplianceschedulonmapping = new Lic_tbl_LicenseComplianceInstanceScheduleOnMapping()
                                                                        {
                                                                            LicenseID = licenseRecord.ID,
                                                                            ComplianceInstanceID = complianceInstanceId,
                                                                            ComplianceScheduleOnID = complianceScheduleOnId,
                                                                            IsActivation = "Active",
                                                                            IsStatutoryORInternal = "S"
                                                                        };
                                                                        LicenseMgmt.CreateLicenseLicenseComplianceInstanceScheduleOnMapping(lictbllicensecomplianceschedulonmapping);



                                                                        ComplianceTransaction transaction = new ComplianceTransaction()
                                                                        {
                                                                            ComplianceInstanceId = complianceInstanceId,
                                                                            StatusId = 1,
                                                                            Remarks = "New compliance assigned.",
                                                                            Dated = DateTime.Now,
                                                                            CreatedBy = UserID,
                                                                            CreatedByText = UserName,
                                                                            ComplianceScheduleOnID = complianceScheduleOnId,
                                                                        };
                                                                        List<KeyValuePair<string, Byte[]>> Filelist = new List<KeyValuePair<string, Byte[]>>();
                                                                        List<FileData> files = new List<FileData>();
                                                                        List<KeyValuePair<string, int>> list = new List<KeyValuePair<string, int>>();
                                                                        HttpFileCollection fileCollection = Request.Files;
                                                                        bool blankfileCount = true;
                                                                        string directoryPath = null;
                                                                        if (fileCollection.Count > 0)
                                                                        {
                                                                            var AWSData = AmazonS3.GetAWSStorageDetail((int)AuthenticationHelper.CustomerID);
                                                                            if (AWSData != null)
                                                                            {
                                                                                #region AWS Storage
                                                                                var InstanceData = DocumentManagement.GetComplianceInstanceData(Convert.ToInt32(complianceInstanceId));
                                                                               
                                                                                string version = null;
                                                                                version = DocumentManagement.GetDocumnetVersion(Convert.ToInt32(complianceScheduleOnId));
                                                                                if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                                                                {
                                                                                    directoryPath = ConfigurationManager.AppSettings["DriveUrl"] + "\\AvacomDocuments\\" + customerID + "\\" + InstanceData.CustomerBranchID + "\\" + InstanceData.ID.ToString() + "\\" + complianceScheduleOnId + "\\" + version;
                                                                                }
                                                                                else
                                                                                {
                                                                                    directoryPath = Server.MapPath("~/AvacomDocuments/" + customerID + "/" + InstanceData.CustomerBranchID + "/" + InstanceData.ID.ToString() + "/" + complianceScheduleOnId + "/" + version);
                                                                                }


                                                                                IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.APSouth1);
                                                                                S3DirectoryInfo di = new S3DirectoryInfo(client, AWSData.BucketName, directoryPath);
                                                                                if (!di.Exists)
                                                                                {
                                                                                    di.Create();
                                                                                }

                                                                                for (int i = 0; i < fileCollection.Count; i++)
                                                                                {
                                                                                    HttpPostedFile uploadfile = fileCollection[i];
                                                                                    string[] keys = fileCollection.Keys[i].Split('$');
                                                                                    String fileName = "";
                                                                                    if (keys[keys.Count() - 1].Equals("fuSampleFile"))
                                                                                    {
                                                                                        fileName = "ComplianceDoc_" + uploadfile.FileName;
                                                                                        list.Add(new KeyValuePair<string, int>(fileName, 1));
                                                                                    }
                                                                                    Guid fileKey = Guid.NewGuid();
                                                                                    string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(uploadfile.FileName));

                                                                                    Stream fs = uploadfile.InputStream;
                                                                                    BinaryReader br = new BinaryReader(fs);
                                                                                    Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                                                                                    Filelist.Add(new KeyValuePair<string, Byte[]>(finalPath, bytes));

                                                                                    if (uploadfile.ContentLength > 0)
                                                                                    {                                                                                       
                                                                                        string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                                                                        string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;

                                                                                        string TdirectoryPath = "~/TempDocuments/AWS/" + User + "/" + uploadfile.FileName;
                                                                                        string directoryPath1 = "~/TempDocuments/AWS/" + User;
                                                                                        string TFilePath = Server.MapPath(TdirectoryPath);
                                                                                        if (File.Exists(TFilePath))
                                                                                            File.Delete(TFilePath);
                                                                                        if (!Directory.Exists(directoryPath1))
                                                                                        {
                                                                                            Directory.CreateDirectory(Server.MapPath(directoryPath1));
                                                                                        }

                                                                                        FileStream objFileStrm = File.Create(TFilePath);
                                                                                        objFileStrm.Close();
                                                                                        File.WriteAllBytes(TFilePath, bytes);

                                                                                        string AWSpath = "";
                                                                                        AWSpath = directoryPath + "\\LicenseDoc_" + uploadfile.FileName;

                                                                                        FileInfo localFile = new FileInfo(TFilePath);
                                                                                        S3FileInfo s3File = new S3FileInfo(client, AWSData.BucketName, AWSpath);
                                                                                        if (!s3File.Exists)
                                                                                        {
                                                                                            using (var s3Stream = s3File.Create()) // <-- create file in S3  
                                                                                            {
                                                                                                localFile.OpenRead().CopyTo(s3Stream); // <-- copy the content to S3  
                                                                                            }
                                                                                        }

                                                                                        string filepathvalue = string.Empty;
                                                                                        string vale = directoryPath.Replace(ConfigurationManager.AppSettings["DriveUrl"], "~");
                                                                                        filepathvalue = vale.Replace(@"\", "/");


                                                                                        FileData file = new FileData()
                                                                                        {
                                                                                            Name = fileName,
                                                                                            FilePath = filepathvalue,
                                                                                            FileKey = fileKey.ToString(),
                                                                                            //Version = "1.0",
                                                                                            Version = version,
                                                                                            VersionDate = DateTime.UtcNow,
                                                                                            FileSize = uploadfile.ContentLength,
                                                                                        };

                                                                                        files.Add(file);
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        if (!string.IsNullOrEmpty(uploadfile.FileName))
                                                                                            blankfileCount = false;
                                                                                    }
                                                                                }
                                                                                #endregion
                                                                            }
                                                                            else
                                                                            {
                                                                                #region Normal
                                                                                var InstanceData = DocumentManagement.GetComplianceInstanceData(Convert.ToInt32(complianceInstanceId));
                                                                              
                                                                                string version = null;
                                                                                version = DocumentManagement.GetDocumnetVersion(Convert.ToInt32(complianceScheduleOnId));
                                                                                if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                                                                {
                                                                                    directoryPath = ConfigurationManager.AppSettings["DriveUrl"] + "\\AvacomDocuments\\" + customerID + "\\" + InstanceData.CustomerBranchID + "\\" + InstanceData.ID.ToString() + "\\" + complianceScheduleOnId + "\\" + version;
                                                                                }
                                                                                else
                                                                                {
                                                                                    directoryPath = Server.MapPath("~/AvacomDocuments/" + customerID + "/" + InstanceData.CustomerBranchID + "/" + InstanceData.ID.ToString() + "/" + complianceScheduleOnId + "/" + version);
                                                                                }
                                                                                DocumentManagement.CreateDirectory(directoryPath);
                                                                                for (int i = 0; i < fileCollection.Count; i++)
                                                                                {
                                                                                    HttpPostedFile uploadfile = fileCollection[i];
                                                                                    string[] keys = fileCollection.Keys[i].Split('$');
                                                                                    String fileName = "";
                                                                                    if (keys[keys.Count() - 1].Equals("fuSampleFile"))
                                                                                    {
                                                                                        fileName = "ComplianceDoc_" + uploadfile.FileName;
                                                                                        list.Add(new KeyValuePair<string, int>(fileName, 1));
                                                                                    }
                                                                                    Guid fileKey = Guid.NewGuid();
                                                                                    string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(uploadfile.FileName));

                                                                                    Stream fs = uploadfile.InputStream;
                                                                                    BinaryReader br = new BinaryReader(fs);
                                                                                    Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                                                                                    Filelist.Add(new KeyValuePair<string, Byte[]>(finalPath, bytes));

                                                                                    if (uploadfile.ContentLength > 0)
                                                                                    {
                                                                                        //Change by rahul on 21 JAN 2017 for Azure Drive or Local Drive
                                                                                        string filepathvalue = string.Empty;
                                                                                        if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                                                                        {
                                                                                            string vale = directoryPath.Replace(ConfigurationManager.AppSettings["DriveUrl"], "~");
                                                                                            filepathvalue = vale.Replace(@"\", "/");
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            filepathvalue = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                                                                        }
                                                                                        FileData file = new FileData()
                                                                                        {
                                                                                            Name = fileName,
                                                                                            FilePath = filepathvalue,
                                                                                            FileKey = fileKey.ToString(),
                                                                                            //Version = "1.0",
                                                                                            Version = version,
                                                                                            VersionDate = DateTime.UtcNow,
                                                                                            FileSize = uploadfile.ContentLength,
                                                                                        };

                                                                                        files.Add(file);
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        if (!string.IsNullOrEmpty(uploadfile.FileName))
                                                                                            blankfileCount = false;
                                                                                    }
                                                                                }
                                                                                #endregion
                                                                            }
                                                                        }
                                                                        bool flag = false;
                                                                        if (blankfileCount)
                                                                        {                                                                     
                                                                            flag = Business.ComplianceManagement.CreateTransaction(transaction, files, list, Filelist, directoryPath, complianceScheduleOnId, (int)AuthenticationHelper.CustomerID);
                                                                        }
                                                                        else
                                                                        {
                                                                            ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "missingAlert", "alert('Please do not upload virus file or blank files.')", true);
                                                                            cvDuplicateEntry.IsValid = false;
                                                                            VSLicensePopup.CssClass = "alert alert-danger";
                                                                            cvDuplicateEntry.ErrorMessage = "Please do not upload virus file or blank files.";
                                                                        }


                                                                        if (AssignedRole.Count > 0)
                                                                        {
                                                                            foreach (var roles in AssignedRole)
                                                                            {
                                                                                if (roles.RoleID != 6)
                                                                                {
                                                                                    var reminders = (from RT in entities.ReminderTemplates
                                                                                                     where RT.Frequency == 2 && RT.IsSubscribed == true
                                                                                                     select RT).ToList();

                                                                                    reminders.ForEach(day =>
                                                                                    {
                                                                                        ComplianceReminder reminder = new ComplianceReminder()
                                                                                        {
                                                                                            ComplianceAssignmentID = roles.ID,
                                                                                            ReminderTemplateID = day.ID,
                                                                                            ComplianceDueDate = licenseRecord.EndDate.Value.Date,
                                                                                            RemindOn = licenseRecord.EndDate.Value.Date.Date.AddDays(-1 * day.TimeInDays),
                                                                                        };
                                                                                        reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                                                                        entities.ComplianceReminders.Add(reminder);

                                                                                    });
                                                                                }
                                                                            }
                                                                            entities.SaveChanges();
                                                                        }
                                                                    }//complianceScheduleOnId
                                                                    #endregion
                                                                }
                                                                else
                                                                {
                                                                    #region Application Days
                                                                    DateTime Applicationdate = Convert.ToDateTime(licenseRecord.EndDate);
                                                                    Applicationdate = Applicationdate.AddDays(-Convert.ToDouble(licenseRecord.RemindBeforeNoOfDays));
                                                                    ComplianceScheduleOn complianceScheduleOn = new ComplianceScheduleOn()
                                                                    {
                                                                        ScheduleOn = Applicationdate.Date,
                                                                        ComplianceInstanceID = complianceInstanceId,
                                                                        IsActive = true,
                                                                        IsUpcomingNotDeleted = true,
                                                                    };
                                                                    complianceScheduleOnId = LicenseMgmt.CreateComplianceScheduleOn(complianceScheduleOn);
                                                                    Lic_tbl_LicenseStatusTransaction newStatusRecord = new Lic_tbl_LicenseStatusTransaction()
                                                                    {
                                                                        CustomerID = (int)AuthenticationHelper.CustomerID,
                                                                        LicenseID = LicenseID,
                                                                        StatusID = statusId,
                                                                        StatusChangeOn = DateTime.Now,
                                                                        IsActive = true,
                                                                        CreatedBy = AuthenticationHelper.UserID,
                                                                        CreatedOn = DateTime.Now,
                                                                        UpdatedBy = AuthenticationHelper.UserID,
                                                                        UpdatedOn = DateTime.Now,
                                                                        Remark = "Change license status to " + status + "",
                                                                        ComplianceScheduleOnID = complianceScheduleOnId,
                                                                    };
                                                                    saveSuccess = LicenseMgmt.CreateLicenseStatusTransaction(newStatusRecord);
                                                                    #region Save Document Against Current ComplianceScheduleonID
                                                                    if (complianceScheduleOnId > 0)
                                                                    {
                                                                        Lic_tbl_LicenseComplianceInstanceScheduleOnMapping lictbllicensecomplianceschedulonmapping = new Lic_tbl_LicenseComplianceInstanceScheduleOnMapping()
                                                                        {
                                                                            LicenseID = licenseRecord.ID,
                                                                            ComplianceInstanceID = complianceInstanceId,
                                                                            ComplianceScheduleOnID = complianceScheduleOnId,
                                                                            IsActivation = "Application",
                                                                            IsStatutoryORInternal = "S"
                                                                        };
                                                                        LicenseMgmt.CreateLicenseLicenseComplianceInstanceScheduleOnMapping(lictbllicensecomplianceschedulonmapping);

                                                                        ComplianceTransaction transaction = new ComplianceTransaction()
                                                                        {
                                                                            ComplianceInstanceId = complianceInstanceId,
                                                                            StatusId = 1,
                                                                            Remarks = "New compliance assigned.",
                                                                            Dated = DateTime.Now,
                                                                            CreatedBy = UserID,
                                                                            CreatedByText = UserName,
                                                                            ComplianceScheduleOnID = complianceScheduleOnId,
                                                                        };
                                                                        List<KeyValuePair<string, Byte[]>> Filelist = new List<KeyValuePair<string, Byte[]>>();
                                                                        List<FileData> files = new List<FileData>();
                                                                        List<KeyValuePair<string, int>> list = new List<KeyValuePair<string, int>>();
                                                                        HttpFileCollection fileCollection = Request.Files;
                                                                        bool blankfileCount = true;
                                                                        if (fileCollection.Count > 0)
                                                                        {
                                                                            var InstanceData = DocumentManagement.GetComplianceInstanceData(Convert.ToInt32(complianceInstanceId));
                                                                            string directoryPath = null;
                                                                            string version = null;
                                                                            version = DocumentManagement.GetDocumnetVersion(Convert.ToInt32(complianceScheduleOnId));
                                                                            if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                                                            {
                                                                                directoryPath = ConfigurationManager.AppSettings["DriveUrl"] + "\\AvacomDocuments\\" + customerID + "\\" + InstanceData.CustomerBranchID + "\\" + InstanceData.ID.ToString() + "\\" + complianceScheduleOnId + "\\" + version;
                                                                            }
                                                                            else
                                                                            {
                                                                                directoryPath = Server.MapPath("~/AvacomDocuments/" + customerID + "/" + InstanceData.CustomerBranchID + "/" + InstanceData.ID.ToString() + "/" + complianceScheduleOnId + "/" + version);
                                                                            }
                                                                            DocumentManagement.CreateDirectory(directoryPath);
                                                                            for (int i = 0; i < fileCollection.Count; i++)
                                                                            {
                                                                                HttpPostedFile uploadfile = fileCollection[i];
                                                                                string[] keys = fileCollection.Keys[i].Split('$');
                                                                                String fileName = "";
                                                                                if (keys[keys.Count() - 1].Equals("fuSampleFile"))
                                                                                {
                                                                                    fileName = "ComplianceDoc_" + uploadfile.FileName;
                                                                                    list.Add(new KeyValuePair<string, int>(fileName, 1));
                                                                                }
                                                                                Guid fileKey = Guid.NewGuid();
                                                                                string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(uploadfile.FileName));

                                                                                Stream fs = uploadfile.InputStream;
                                                                                BinaryReader br = new BinaryReader(fs);
                                                                                Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                                                                                Filelist.Add(new KeyValuePair<string, Byte[]>(finalPath, bytes));

                                                                                if (uploadfile.ContentLength > 0)
                                                                                {
                                                                                    //Change by rahul on 21 JAN 2017 for Azure Drive or Local Drive
                                                                                    string filepathvalue = string.Empty;
                                                                                    if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                                                                    {
                                                                                        string vale = directoryPath.Replace(ConfigurationManager.AppSettings["DriveUrl"], "~");
                                                                                        filepathvalue = vale.Replace(@"\", "/");
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        filepathvalue = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                                                                    }
                                                                                    FileData file = new FileData()
                                                                                    {
                                                                                        Name = fileName,
                                                                                        FilePath = filepathvalue,
                                                                                        FileKey = fileKey.ToString(),
                                                                                        //Version = "1.0",
                                                                                        Version = version,
                                                                                        VersionDate = DateTime.UtcNow,
                                                                                        FileSize = uploadfile.ContentLength,
                                                                                    };

                                                                                    files.Add(file);
                                                                                }
                                                                                else
                                                                                {
                                                                                    if (!string.IsNullOrEmpty(uploadfile.FileName))
                                                                                        blankfileCount = false;
                                                                                }
                                                                            }
                                                                        }
                                                                        bool flag = false;
                                                                        if (blankfileCount)
                                                                        {
                                                                            flag = Business.ComplianceManagement.CreateTransaction(transaction, files, list, Filelist);
                                                                        }
                                                                        else
                                                                        {
                                                                            ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "missingAlert", "alert('Please do not upload virus file or blank files.')", true);
                                                                            cvDuplicateEntry.IsValid = false;
                                                                            VSLicensePopup.CssClass = "alert alert-danger";
                                                                            cvDuplicateEntry.ErrorMessage = "Please do not upload virus file or blank files.";
                                                                        }


                                                                        if (AssignedRole.Count > 0)
                                                                        {
                                                                            foreach (var roles in AssignedRole)
                                                                            {
                                                                                if (roles.RoleID != 6)
                                                                                {
                                                                                    var reminders = (from RT in entities.ReminderTemplates
                                                                                                     where RT.Frequency == 2 && RT.IsSubscribed == true
                                                                                                     select RT).ToList();

                                                                                    reminders.ForEach(day =>
                                                                                    {
                                                                                        ComplianceReminder reminder = new ComplianceReminder()
                                                                                        {
                                                                                            ComplianceAssignmentID = roles.ID,
                                                                                            ReminderTemplateID = day.ID,
                                                                                            ComplianceDueDate = Applicationdate.Date,
                                                                                            RemindOn = Applicationdate.Date.AddDays(-1 * day.TimeInDays),
                                                                                        };
                                                                                        reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                                                                        entities.ComplianceReminders.Add(reminder);

                                                                                    });
                                                                                }
                                                                            }
                                                                            entities.SaveChanges();
                                                                        }
                                                                    }// end complianceScheduleOnId
                                                                    #endregion
                                                                    #endregion
                                                                }
                                                            }
                                                            if (complianceInstanceId > 0)
                                                                statutorySuccess = true;
                                                            saveSuccess = statutorySuccess;

                                                        }
                                                        catch (Exception ex)
                                                        {
                                                            LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                                            cvDuplicateEntry.IsValid = false;
                                                            cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
                                                            VSLicensePopup.CssClass = "alert alert-danger";
                                                            saveSuccess = false;
                                                        }

                                                        #endregion

                                                        if (saveSuccess)
                                                        {
                                                            cvDuplicateEntry.IsValid = false;
                                                            cvDuplicateEntry.ErrorMessage = "Compliance Created and Assigned Sucessfully";
                                                            VSLicensePopup.CssClass = "alert alert-success";
                                                        }
                                                        else
                                                        {
                                                            cvDuplicateEntry.IsValid = false;
                                                            cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
                                                            VSLicensePopup.CssClass = "alert alert-danger";
                                                        }
                                                    }
                                                }//Using End
                                            }
                                            catch (Exception ex)
                                            {
                                                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
                                                VSLicensePopup.CssClass = "alert alert-danger";
                                            }
                                            #endregion
                                        }
                                    } //scheduledetails end
                                }//  checklicexist
                                else
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Same license period already present in the system";
                                    VSLicensePopup.CssClass = "alert alert-danger";
                                }
                            }// Scheduleon checklicexist
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Same schedule already present in the system.";
                                VSLicensePopup.CssClass = "alert alert-danger";
                            }
                       // }
                        //else
                        //{
                        //    cvDuplicateEntry.IsValid = false;
                        //    cvDuplicateEntry.ErrorMessage = "License Details with Same License Number already exists";
                        //    formValidateSuccess = false;
                        //}
                    }
                }//Formvalidation end
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Please Select Filters Properly. ";
                    VSLicensePopup.CssClass = "alert alert-danger";
                    if (lstErrorMsg.Count > 0)
                    {
                        formValidateSuccess = false;
                        showErrorMessages(lstErrorMsg, cvDuplicateEntry);
                    }
                }
            }
            #endregion
        }
    }
}