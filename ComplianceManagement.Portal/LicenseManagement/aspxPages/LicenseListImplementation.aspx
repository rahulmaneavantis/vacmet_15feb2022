﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="LicenseListImplementation.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.LicenseManagement.aspxPages.LicenseListImplementation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%--<script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>--%>
    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upLicenseList" runat="server" UpdateMode="Conditional" OnLoad="upLicenseList_Load">
        <ContentTemplate>
            <div style="margin-bottom: 4px">
                <asp:ValidationSummary runat="server" CssClass="vdsummary"
                    ValidationGroup="LicenseValidationGroupTop" />
                <asp:CustomValidator ID="cvDuplicateEntryTop" runat="server" EnableClientScript="False"
                    ValidationGroup="LicenseValidationGroupTop" Display="None" />
            </div>

            <table width="100%">
                <tr>
                    <td align="left" style="width: 20%">
                        <asp:DropDownList runat="server" ID="ddlCustomer"
                            DisableSearchThreshold="5" Style="padding: 0px; margin: 0px; height: 22px; width: 220px;"
                            AutoPostBack="true" class="form-control m-bot15" Width="90%" OnSelectedIndexChanged="ddlCustomer_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>

                    <td style="width: 15%;">
                        <div id="FilterLocationdiv" runat="server">
                            <asp:TextBox runat="server" ID="tbxFilterLocation" Style="padding: 0px; margin: 0px; height: 17px; width: 390px;"
                                class="form-control m-bot15" />
                            <div style="position: absolute; z-index: 10" id="divFilterLocation">
                                <asp:TreeView runat="server" ID="tvFilterLocation" BackColor="White" BorderColor="Black"
                                    BorderWidth="1" SelectedNodeStyle-Font-Bold="true" Height="200px" Width="390px" NodeStyle-ForeColor="Black"
                                    Style="overflow: auto;" ShowLines="true" OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged">
                                </asp:TreeView>
                            </div>
                        </div>
                    </td>

                    <td align="left" style="width: 20%">
                        <asp:DropDownList runat="server" ID="ddlfilterStatutoryNonStatutory" Style="padding: 0px; margin: 0px; height: 22px; width: 220px;"
                            class="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlfilterStatutoryNonStatutory_SelectedIndexChanged">                            
                            <asp:ListItem Text="Statutory" Value="S"></asp:ListItem>
                            <asp:ListItem Text="Internal" Value="I"></asp:ListItem>
                        </asp:DropDownList>
                        
                    </td>

                    <td align="left" style="width: 20%">
                        <asp:DropDownList runat="server" ID="ddlLicenseType"
                            DisableSearchThreshold="5" Style="padding: 0px; margin: 0px; height: 22px; width: 220px;"
                            OnSelectedIndexChanged="ddlLicenseType_SelectedIndexChanged" AutoPostBack="true"
                            class="form-control m-bot15" Width="90%">
                        </asp:DropDownList>

                        <asp:DropDownListChosen runat="server" ID="ddlLicenseStatus" AllowSingleDeselect="false" Visible="false"
                            DisableSearchThreshold="5"
                            OnSelectedIndexChanged="ddlLicenseStatus_SelectedIndexChanged" AutoPostBack="true"
                            class="form-control m-bot15" Width="90%" Style="color: #8e8e93 !important;">
                            <asp:ListItem Text="Status" Value="Status" />
                            <asp:ListItem Text="Active" Value="Active" />
                            <asp:ListItem Text="Expired" Value="Expired" />
                            <asp:ListItem Text="Expiring" Value="Expiring" />
                            <asp:ListItem Text="Applied" Value="Applied" />
                            <asp:ListItem Text="PendingForReview" Value="PR" />
                            <asp:ListItem Text="Rejected" Value="Rejected" />
                        </asp:DropDownListChosen>
                    </td>

                    <td align="right" style="width: 20%">
                        <asp:TextBox runat="server" ID="tbxFilter" Width="250px" MaxLength="50" AutoPostBack="true"
                            OnTextChanged="tbxFilter_TextChanged" Visible="false" />
                    </td>
                    <td class="newlink" align="right">
                        <asp:LinkButton Text="Add New" runat="server" ID="btnAddLicense" Visible="false" />
                    </td>
                </tr>
            </table>
            <asp:GridView runat="server" ID="grdLicenseList" AutoGenerateColumns="false" GridLines="Vertical"                
                BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true"
                CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="13" Width="100%"
                Font-Size="12px" DataKeyNames="LicenseID" OnRowCommand="grdLicenseList_RowCommand"
                OnPageIndexChanging="grdLicenseList_PageIndexChanging">
                <Columns>
                    <asp:TemplateField HeaderText="Location" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="15%" SortExpression="CustomerBrach">
                        <ItemTemplate>
                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("CustomerBrach") %>' ToolTip='<%# Eval("CustomerBrach") %>'></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="License Type" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="20%" SortExpression="LicensetypeName">
                        <ItemTemplate>
                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("LicensetypeName") %>' ToolTip='<%# Eval("LicensetypeName") %>'></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="License No." ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="20%" SortExpression="LicenseNo">
                        <ItemTemplate>
                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("LicenseNo") %>' ToolTip='<%# Eval("LicenseNo") %>'></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Title" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="15%" SortExpression="LicenseTitle">
                        <ItemTemplate>
                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("LicenseTitle") %>' ToolTip='<%# Eval("LicenseTitle") %>'></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="App Due Date" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="10%" SortExpression="ApplicationDate">
                        <ItemTemplate>
                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px">
                                <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ApplicationDate") != DBNull.Value ? Convert.ToDateTime(Eval("ApplicationDate")).ToString("dd-MM-yyyy") : "" %>'
                                    ToolTip='<%# Eval("ApplicationDate") != DBNull.Value ? Convert.ToDateTime(Eval("ApplicationDate")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>


                    <asp:TemplateField HeaderText="End Date" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="10%" SortExpression="EndDate">
                        <ItemTemplate>
                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px">
                                <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("EndDate") != DBNull.Value ? Convert.ToDateTime(Eval("EndDate")).ToString("dd-MM-yyyy") : "" %>'
                                    ToolTip='<%# Eval("EndDate") != DBNull.Value ? Convert.ToDateTime(Eval("EndDate")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Status" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="15%" SortExpression="Status">
                        <ItemTemplate>
                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 95px">
                                <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("MGRStatus") %>' ToolTip='<%# Eval("MGRStatus") %>'></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="15%">
                        <ItemTemplate>
                            <div class="btndiv" style="width: 55px;">
                         
                                  <asp:LinkButton ID="lbtEdit" runat="server" CommandName="EDIT_COMPLIANCE"
                                    CommandArgument='<%# Eval("LicenseID")+","+Eval("ComplianceInstanceID")+","+Eval("ComplianceScheduleOnID")+","+Eval("MGRStatus") %>'
                                    Visible='<%# CanChangeStatus((long)Eval("LicenseID"),(long)Eval("ComplianceInstanceID"),(long)Eval("ComplianceScheduleOnID"),(string)Eval("MGRStatus")) %>'
                                    data-toggle="tooltip" data-placement="bottom" ToolTip="Edit">    
                                     <img src="../../Images/edit_icon.png" alt="Edit License"/></asp:LinkButton>

                                <asp:LinkButton ID="lbtDelete" runat="server" CommandName="DELETE_License"
                                    CommandArgument='<%# Eval("LicenseID") %>'  Visible="false"
                                    OnClientClick="return confirm('Are you certain you want to delete this License?');">
                                    <img src="../../Images/delete_icon.png" alt="Delete License" title="Delete License" /></asp:LinkButton>


                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <FooterStyle BackColor="#CCCC99" />
                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                <PagerSettings Position="Top" />
                <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                <AlternatingRowStyle BackColor="#E6EFF7" />
                <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                <EmptyDataTemplate>
                    No Records Found.
                </EmptyDataTemplate>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div id="divLicenseDialog">
        <asp:UpdatePanel ID="upLicense" runat="server" UpdateMode="Conditional" OnLoad="upLicense_Load">
            <ContentTemplate>
                <div style="margin: 5px">
                    <div style="margin-bottom: 4px">
                        <asp:ValidationSummary runat="server" CssClass="vdsummary"
                            ValidationGroup="LicenseValidationGroup" />
                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                            ValidationGroup="LicenseValidationGroup" Display="None" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 1%; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 24%; display: block; float: left; font-size: 13px; color: #333;">
                            License Type</label>
                        <asp:DropDownList runat="server" ID="ddlLicenseTypePopup" Style="padding: 0px; margin: 0px; height: 22px; width: 74%;" Enabled="false"
                            CssClass="txtbox" AutoPostBack="true">
                            <asp:ListItem Text="Statutory" Value="S"></asp:ListItem>
                            <asp:ListItem Text="Internal" Value="I"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 1%; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 24%; display: block; float: left; font-size: 13px; color: #333;">
                            Location</label>
                        <div id="divBranchesPopup" runat="server">
                            <asp:TextBox runat="server" ID="tbxFilterLocationPopup" Style="padding: 0px; margin: 0px; height: 22px; width: 74%;"
                                CssClass="txtbox" Enabled="false" />
                            <div style="position: absolute; z-index: 10; display: none;" id="divFilterLocationPopup">
                                <asp:TreeView runat="server" ID="tvFilterLocationPopup" BackColor="White" BorderColor="Black" OnSelectedNodeChanged="tvFilterLocationPopup_SelectedNodeChanged"
                                    BorderWidth="1" SelectedNodeStyle-Font-Bold="true" Height="200px" Width="390px" NodeStyle-ForeColor="Black"
                                    Style="overflow: auto; display: none; margin-left: 37.5%" ShowLines="true">
                                </asp:TreeView>
                            </div>
                        </div>
                    </div>
                    <div style="margin-bottom: 7px; display: none;">
                        <label style="width: 1%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 24%; display: block; float: left; font-size: 13px; color: #333;">
                            Department</label>
                        <asp:DropDownList runat="server" ID="ddlDepartment" Style="padding: 0px; margin: 0px; height: 22px; width: 74%;"
                            CssClass="txtbox" AutoPostBack="true" Enabled="false" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 1%; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 24%; display: block; float: left; font-size: 13px; color: #333;">
                            License Number</label>
                        <asp:TextBox runat="server" CssClass="txtbox" ID="tbxLicenseNo" Style="padding: 0px; margin: 0px; height: 22px; width: 74%;"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" ErrorMessage="License number can not be empty."
                            ControlToValidate="tbxLicenseNo" runat="server" ValidationGroup="LicenseValidationGroup"
                            Display="None" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 1%; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 24%; display: block; float: left; font-size: 13px; color: #333;">
                            License Title</label>
                        <asp:TextBox runat="server" CssClass="txtbox" ID="tbxLicenseTitle" Style="padding: 0px; margin: 0px; height: 22px; width: 74%;"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="License title can not be empty."
                            ControlToValidate="tbxLicenseTitle" runat="server" ValidationGroup="LicenseValidationGroup"
                            Display="None" />
                    </div>

                    <div style="margin-bottom: 7px">
                        <label style="width: 1%; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 24%; display: block; float: left; font-size: 13px; color: #333;">
                            License Start Date</label>
                        <asp:TextBox runat="server" CssClass="txtbox" ID="tbxStartDate" Style="padding: 0px; margin: 0px; height: 22px; width: 74%;"></asp:TextBox>
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 1%; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 24%; display: block; float: left; font-size: 13px; color: #333;">
                            License End Date</label>
                        <asp:TextBox runat="server" CssClass="txtbox"  ID="tbxEndDate" Style="padding: 0px; margin: 0px; height: 22px; width: 74%;"></asp:TextBox>
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 1%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 24%; display: block; float: left; font-size: 13px; color: #333;">
                            Application (Days)</label>
                        <asp:TextBox runat="server" ID="tbxApplicationdays" CssClass="txtbox" Style="padding: 0px; margin: 0px; height: 22px; width: 74%;"></asp:TextBox>
                    </div>
                    <div>
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 23%; display: block; float: left; font-size: 13px; color: #333;">Versions</td>                                
                                <td style="width: 77%;">
                                    <table width="100%" style="text-align: left">
                                        <thead>
                                            <tr>
                                                <td valign="top">
                                                    <asp:Repeater ID="rptLicenseVersion" runat="server"
                                                        OnItemCommand="rptLicenseVersion_ItemCommand"
                                                        OnItemDataBound="rptLicenseVersion_ItemDataBound">
                                                        <HeaderTemplate>
                                                            <table id="tblldoc1">
                                                                <thead>
                                                                </thead>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                                                <ContentTemplate>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:LinkButton CommandName="version" CommandArgument='<%# Eval("LicenseID")+","+ Eval("ScheduledOnID") + ","+ Eval("Version") %>'
                                                                                ID="lblLicenseDocumentVersion" runat="server" Text='<%# Eval("Version")%>' Style="color: blue;"></asp:LinkButton></td>
                                                                        <td>
                                                                            <asp:LinkButton CommandName="Download" CommandArgument='<%# Eval("LicenseID")+","+ Eval("ScheduledOnID") + ","+ Eval("Version") %>'
                                                                                ID="btnLicenseVersionDoc" runat="server" Text="Download" Style="color: blue;">
                                                                            </asp:LinkButton>
                                                                            <asp:Label ID="lblSlashReview1" Text="/" Style="color: blue; display: none;" runat="server" />
                                                                            <asp:LinkButton CommandName="View" ID="lnkViewDoc1" CommandArgument='<%# Eval("LicenseID")+","+ Eval("ScheduledOnID") + ","+ Eval("Version") %>'
                                                                                Text="View" Style="width: 150px; font-size: 13px; color: blue; display: none;"
                                                                                runat="server" Font-Underline="false" />
                                                                            <asp:Label ID="lblpathReviewDoc1" runat="server" Style="display: none"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </ContentTemplate>
                                                                <Triggers>
                                                                    <asp:PostBackTrigger ControlID="btnLicenseVersionDoc" />
                                                                </Triggers>
                                                            </asp:UpdatePanel>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            </table>
                                                        </FooterTemplate>
                                                    </asp:Repeater>

                                                </td>
                                                <td valign="top">
                                                    <asp:Repeater ID="rptLicenseDocumnets" runat="server" OnItemCommand="rptLicenseDocumnets_ItemCommand"
                                                        OnItemDataBound="rptLicenseDocumnets_ItemDataBound">
                                                        <HeaderTemplate>
                                                            <table id="tblLicenseDocumnets">
                                                                <thead>
                                                                    <th>Compliance Related Documents</th>
                                                                </thead>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td>
                                                                    <asp:LinkButton
                                                                        CommandArgument='<%# Eval("FileID")%>'
                                                                        ID="btnLicenseDocumnets" runat="server" Text='<%# Eval("FileName") %>'>
                                                                    </asp:LinkButton></td>
                                                            </tr>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            </table>
                                                        </FooterTemplate>
                                                    </asp:Repeater>
                                                    <asp:Repeater ID="rptLicenseWorkingFiles" runat="server" OnItemCommand="rptLicenseWorkingFiles_ItemCommand"
                                                        OnItemDataBound="rptLicenseWorkingFiles_ItemDataBound">
                                                        <HeaderTemplate>
                                                            <table id="tblLicenseWorkingFiles">
                                                                <thead>
                                                                    <th>Compliance Working Files</th>
                                                                </thead>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td>
                                                                    <asp:LinkButton
                                                                        CommandArgument='<%# Eval("FileID")%>'
                                                                        ID="btnLicenseWorkingFiles" runat="server" Text='<%# Eval("FileName")%>'>
                                                                    </asp:LinkButton></td>
                                                            </tr>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            </table>
                                                        </FooterTemplate>
                                                    </asp:Repeater>
                                                </td>
                                            </tr>
                                        </thead>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>

                    <div style="margin-bottom: 7px" runat="server" id="divUploadDocument">
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 23%;">                                    
                                    <label style="display: block; float: left; font-size: 13px; color: #333; vertical-align: text-top;">Upload  Document(s)</label>
                                </td>                                
                                <td style="width: 77%;">
                                    <asp:FileUpload ID="fuSampleFile" Multiple="Multiple" runat="server" Style="color: black" />
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div style="margin-bottom: 7px; margin-left: 150px; margin-top: 10px">
                        <asp:Button Text="Save" runat="server" ID="btnSave" CssClass="button" OnClick="btnSave_Click"
                            ValidationGroup="LicenseValidationGroup" />
                        <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="button"
                            OnClientClick="$('#divLicenseDialog').dialog('close');" />
                    </div>

                    <div style="margin-bottom: 7px; margin-left: 10px; margin-top: 30px;">

                        <p style="color: red;"><strong>Note:</strong> (*) Fields Are Compulsary</p>
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnSave" />
            </Triggers>
        </asp:UpdatePanel>
    </div>


    <script type="text/javascript">
        $(function () {
            $('#divLicenseDialog').dialog({
                height: 550,
                width: 630,
                autoOpen: false,
                draggable: true,
                title: "License Details",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });
        });
        function initializeRadioButtonsList(controlID) {
            $(controlID).buttonset();
        }
    </script>
    <script type="text/javascript">
        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);
        window.onunload = function () { null };
    </script>

    <script type="text/javascript">
        $(document).on("click", function (event) {
            if (event.target.id == "") {
                var idvid = $(event.target).closest('div');
                if ($(idvid).attr('id') != undefined && $(idvid).attr('id') != '' && $(idvid).attr('id').indexOf('<%= tvFilterLocation.ClientID %>') > -1) {
                    $("#divFilterLocation").show();
                } else {
                    $("#divFilterLocation").hide();
                }
            } else if (event.target.id != "" && event.target.id.indexOf('<%= tvFilterLocation.ClientID %>') > -1) {
                $("#divFilterLocation").show();
            } else if (event.target.id != '<%= tbxFilterLocation.ClientID %>') {
                $("#divFilterLocation").hide();
            } else if (event.target.id == '<%= tbxFilterLocation.ClientID %>') {
                $('<%= tbxFilterLocation.ClientID %>').unbind('click');

                $('<%= tbxFilterLocation.ClientID %>').click(function () {
                    $("#divFilterLocation").toggle("blind", null, 500, function () { });
                });
            }
        });

$(document).on("click", function (event) {
    if (event.target.id == "") {
        var idvid = $(event.target).closest('div');
        if ($(idvid).attr('id') != undefined && $(idvid).attr('id') != '' && $(idvid).attr('id').indexOf('<%= tvFilterLocationPopup.ClientID %>') > -1) {
            $("#divFilterLocationPopup").show();
        } else {
            $("#divFilterLocationPopup").hide();
        }
    } else if (event.target.id != "" && event.target.id.indexOf('<%= tbxFilterLocationPopup.ClientID %>') > -1) {
        $("#divFilterLocationPopup").show();
    } else if (event.target.id != '<%= tbxFilterLocationPopup.ClientID %>') {
        $("#divFilterLocationPopup").hide();
    } else if (event.target.id == '<%= tbxFilterLocationPopup.ClientID %>') {
        $('<%= tbxFilterLocationPopup.ClientID %>').unbind('click');

        $('<%= tbxFilterLocationPopup.ClientID %>').click(function () {
            $("#divFilterLocationPopup").toggle("blind", null, 500, function () { });
        });
    }
});


$(document).ready(function () {
    if ($("#<%=tbxFilterLocation.ClientID %>") != null) {
        $("#<%=tbxFilterLocation.ClientID %>").unbind('click');

        $("#<%=tbxFilterLocation.ClientID %>").click(function () {
            $("#divFilterLocation").toggle("blind", null, 500, function () { });
        });
    }
});
$(document).ready(function () {
    if ($("#<%=tbxFilterLocationPopup.ClientID %>") != null) {
        $("#<%=tbxFilterLocationPopup.ClientID %>").unbind('click');

        $("#<%=tbxFilterLocationPopup.ClientID %>").click(function () {
            $("#divFilterLocationPopup").toggle("blind", null, 500, function () { });
        });
    }
});
function InitializeRequest(sender, args) { }




function hideDivBranch() {
    $('#divFilterLocationPopup').hide("blind", null, 0, function () { });
}

function initializeJQueryUI(textBoxID, divID) {
    $("#" + textBoxID).unbind('click');

    $("#" + textBoxID).click(function () {
        $("#" + divID).toggle("blind", null, 500, function () { });
    });
}

function checkAll(cb) {
    var ctrls = document.getElementsByTagName('input');
    for (var i = 0; i < ctrls.length; i++) {
        var cbox = ctrls[i];
        if (cbox.type == "checkbox" && cbox.id.indexOf("chkIndustry") > -1) {
            cbox.checked = cb.checked;
        }
    }
}


function UncheckHeader() {
    var rowCheckBox = $("#RepeaterTable input[id*='chkIndustry']");
    var rowCheckBoxSelected = $("#RepeaterTable input[id*='chkIndustry']:checked");
    var rowCheckBoxHeader = $("#RepeaterTable input[id*='IndustrySelectAll']");
    if (rowCheckBox.length == rowCheckBoxSelected.length) {
        rowCheckBoxHeader[0].checked = true;
    } else {

        rowCheckBoxHeader[0].checked = false;
    }
}

function initializeDatePicker(date) {

    var startDate = new Date();
    $("#<%= tbxStartDate.ClientID %>").datepicker({
        dateFormat: 'dd-mm-yy',
        defaultDate: startDate,
        //numberOfMonths: 1,
        //minDate: startDate,
       <%-- onClose: function (startDate) {
            $("#<%= tbxStartDate.ClientID %>").datepicker("option", "minDate", startDate);
        }--%>
    });
        if (date != null) {
            $("#<%= tbxStartDate.ClientID %>").datepicker("option", "defaultDate", date);

        }

        var EndDate = new Date();
        $("#<%= tbxEndDate.ClientID %>").datepicker({
                    dateFormat: 'dd-mm-yy',
                    defaultDate: EndDate,
                    //numberOfMonths: 1,
                    //minDate: EndDate,
                   <%-- onClose: function (EndDate) {
                        $("#<%= tbxEndDate.ClientID %>").datepicker("option", "minDate", EndDate);
                    }--%>
                });
                    if (date != null) {
                        $("#<%= tbxEndDate.ClientID %>").datepicker("option", "defaultDate", date);

                    }
                }

                function checkAll(cb) {
                    var ctrls = document.getElementsByTagName('input');
                    for (var i = 0; i < ctrls.length; i++) {
                        var cbox = ctrls[i];
                        if (cbox.type == "checkbox" && cbox.id.indexOf("chkIndustry") > -1) {
                            cbox.checked = cb.checked;
                        }
                    }
                }
                function UncheckHeader() {
                    var rowCheckBox = $("#RepeaterTable input[id*='chkIndustry']");
                    var rowCheckBoxSelected = $("#RepeaterTable input[id*='chkIndustry']:checked");
                    var rowCheckBoxHeader = $("#RepeaterTable input[id*='IndustrySelectAll']");
                    if (rowCheckBox.length == rowCheckBoxSelected.length) {
                        rowCheckBoxHeader[0].checked = true;
                    } else {

                        rowCheckBoxHeader[0].checked = false;
                    }
                }
    </script>
</asp:Content>
