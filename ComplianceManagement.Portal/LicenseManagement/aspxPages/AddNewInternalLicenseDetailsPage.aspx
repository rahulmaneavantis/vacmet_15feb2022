﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddNewInternalLicenseDetailsPage.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.LicenseManagement.aspxPages.AddNewInternalLicenseDetailsPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Internal License Details</title>

    <!-- Bootstrap CSS -->
    <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />

    <!-- bootstrap theme -->
    <link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />

    <!-- font icon -->
    <link href="~/NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!--external css-->

    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <%--<link href="~/NewCSS/jquery-ui.css" rel="stylesheet" type="text/css" />--%>

    <%--<script src="https://code.jquery.com/jquery-1.11.3.js"></script>--%>
    <script type="text/javascript" src="../../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>
    <%--<script type="text/javascript" src="../../Newjs/jquery-1.8.3.min.js"></script>--%>
    <!-- nice scroll -->
    <script type="text/javascript" src="../../Newjs/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery.nicescroll.js"></script>
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
    <script src="../../Newjs/bootstrap-multiselect.js" type="text/javascript"></script>

    <link href="~/NewCSS/litigation_custom_style.css" rel="stylesheet" />
    <script src="../../Newjs/tag-scrolling.js" type="text/javascript"></script>
    <link href="../../NewCSS/tag-scrolling.css" rel="stylesheet" />


    <script type="text/javascript">

        $(document).ready(function () {

            BindControls();

           <%-- if ($("#<%=tbxFilterLocation.ClientID %>") != null) {
                $("#<%=tbxFilterLocation.ClientID %>").unbind('click');

                $("#<%=tbxFilterLocation.ClientID %>").click(function () {
                    $("#divBranches").toggle("blind", null, 500, function () { });
                });
            }--%>

        });

        $(document).ready(function () {

            $("#<%=tbxFilterLocation.ClientID %>").unbind('click');

              $("#<%=tbxFilterLocation.ClientID %>").click(function () {
                  $("#divBranches").toggle("blind", null, 500, function () { });
              });
              $(document).tooltip({ selector: '[data-toggle="tooltip"]' });
              try {
                  //    window.parent.setIframeHeight(document.body.scrollHeight + "px");
              } catch (e) { }

              if ($("#<%=tbxFilterLocation.ClientID %>") != null) {
                $("#<%=tbxFilterLocation.ClientID %>").unbind('click');

                $("#<%=tbxFilterLocation.ClientID %>").click(function () {
                    $("#divBranches").toggle("blind", null, 500, function () { });
                });
            }

          });
        function InitializeRequest(sender, args) { }
        function EndRequest(sender, args) { BindControls(); }

        function BindControls() {
            var startDate = new Date();
            $(function () {

                $('input[id*=txtStartDate]').datepicker({
                    dateFormat: 'dd-mm-yy',
                    numberOfMonths: 1,
                    changeMonth: true,
                    changeYear: true,
                });
            });

            $(function () {

                $('input[id*=txtEndDate]').datepicker({
                    dateFormat: 'dd-mm-yy',
                    numberOfMonths: 1,
                    changeMonth: true,
                    changeYear: true,
                });
            });

        }

        function hideDivBranch() {
            $('#divBranches').hide("blind", null, 500, function () { });
        }


        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }

        $(document).on("click", function (event) {
            if (event.target.id == "") {
                var idvid = $(event.target).closest('div');
                if ($(idvid).attr('id') != undefined && $(idvid).attr('id') != '' && $(idvid).attr('id').indexOf('<%= tvFilterLocation.ClientID %>') > -1) {
                    $("#divFilterLocation").show();
                } else {
                    $("#divFilterLocation").hide();
                }
            } else if (event.target.id != "" && event.target.id.indexOf('<%= tvFilterLocation.ClientID %>') > -1) {
                $("#divFilterLocation").show();
            } else if (event.target.id != '<%= tbxFilterLocation.ClientID %>') {
                $("#divFilterLocation").hide();
            } else if (event.target.id == '<%= tbxFilterLocation.ClientID %>') {
                $('<%= tbxFilterLocation.ClientID %>').unbind('click');

                $('<%= tbxFilterLocation.ClientID %>').click(function () {
                    $("#divFilterLocation").toggle("blind", null, 500, function () { });
                });
            }

        });

function OnTreeClick(evt) {
    var src = window.event != window.undefined ? window.event.srcElement : evt.target;
    var isChkBoxClick = (src.tagName.toLowerCase() == "input" && src.type == "checkbox");
    if (isChkBoxClick) {
        var parentTable = GetParentByTagName("table", src);
        var nxtSibling = parentTable.nextSibling;
        if (nxtSibling && nxtSibling.nodeType == 1)//check if nxt sibling is not null & is an element node
        {
            if (nxtSibling.tagName.toLowerCase() == "div") //if node has children
            {
                //check or uncheck children at all levels
                CheckUncheckChildren(parentTable.nextSibling, src.checked);
            }
        }
        //check or uncheck parents at all levels
        CheckUncheckParents(src, src.checked);
    }
}

function CheckUncheckChildren(childContainer, check) {
    var childChkBoxes = childContainer.getElementsByTagName("input");
    var childChkBoxCount = childChkBoxes.length;
    for (var i = 0; i < childChkBoxCount; i++) {
        childChkBoxes[i].checked = check;
    }
}

function CheckUncheckParents(srcChild, check) {
    var parentDiv = GetParentByTagName("div", srcChild);
    var parentNodeTable = parentDiv.previousSibling;

    if (parentNodeTable) {
        var checkUncheckSwitch;

        if (check) //checkbox checked
        {
            var isAllSiblingsChecked = AreAllSiblingsChecked(srcChild);
            if (isAllSiblingsChecked)
                checkUncheckSwitch = true;
            else
                return; //do not need to check parent if any(one or more) child not checked
        }
        else //checkbox unchecked
        {
            checkUncheckSwitch = false;
        }

        var inpElemsInParentTable = parentNodeTable.getElementsByTagName("input");
        if (inpElemsInParentTable.length > 0) {
            var parentNodeChkBox = inpElemsInParentTable[0];
            parentNodeChkBox.checked = checkUncheckSwitch;
            //do the same recursively
            CheckUncheckParents(parentNodeChkBox, checkUncheckSwitch);
        }
    }
}

function AreAllSiblingsChecked(chkBox) {
    var parentDiv = GetParentByTagName("div", chkBox);
    var childCount = parentDiv.childNodes.length;
    for (var i = 0; i < childCount; i++) {
        if (parentDiv.childNodes[i].nodeType == 1) //check if the child node is an element node
        {
            if (parentDiv.childNodes[i].tagName.toLowerCase() == "table") {
                var prevChkBox = parentDiv.childNodes[i].getElementsByTagName("input")[0];
                //if any of sibling nodes are not checked, return false
                if (!prevChkBox.checked) {
                    return false;
                }
            }
        }
    }
    return true;
}
//utility function to get the container of an element by tagname
function GetParentByTagName(parentTagName, childElementObj) {
    var parent = childElementObj.parentNode;
    while (parent.tagName.toLowerCase() != parentTagName.toLowerCase()) {
        parent = parent.parentNode;
    }
    return parent;
}

function checkName(thisvalues) {
    var count = 0;
    var currentval = $('#' + thisvalues.id).val();

    if (currentval != "" && thisvalues.id.search("txtLicenseNo") != -1) {
        $('.gridtextbox').each(function () {
            console.log(this.value + ',' + this.id);
            if (currentval.trim().toLowerCase() == this.value.trim().toLowerCase() && thisvalues.id != this.id) {
                count++;
            }
        });

        if (count > 0) {
            alert('License No =  ' + currentval + ' already exists');
            $('#' + thisvalues.id).val("");
        }
    }
    return false;
        }

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 46 && charCode > 31
                && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }

    </script>

    <style type="text/css">
         </style>

</head>
<body style="background: none !important; overflow-y: hidden;">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

        <input type="hidden" id="hiddenColor" style="display: none;" />

        <div class="mainDiv" style="background-color: #f7f7f7;">

            <asp:UpdateProgress ID="updateProgress" runat="server">
                <ProgressTemplate>
                    <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.3;">
                        <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                            AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 40%; left: 40%;" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>

            <div id="divMainView" class="boxscroll do-nicescroll4" style="height: 500px; overflow-y: auto;">
                <div style="width: 100%; float: left; margin-bottom: 15px">
                    <div class="container">

                        <div class="col-md-12 colpadding0" style="min-height: 0px; max-height: 240px; overflow-y: auto;">
                            <asp:Panel ID="vdpanel1" runat="server" ScrollBars="Auto">
                                <asp:ValidationSummary ID="VSLicPopup" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                    ValidationGroup="LicPopUpValidationGroup" />
                                <asp:CustomValidator ID="cvLicPopUp" runat="server" EnableClientScript="False"
                                    ValidationGroup="LicPopUpValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                            </asp:Panel>
                        </div>

                        <div id="divLicDetails" class="row Dashboard-white-widget">

                            <div class="row col-lg-12 col-md-12">
                                <div class="form-group required col-md-4">
                                    <label for="ddlLicenseType" class="control-label">License Type</label>
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;" class="control-label">*</label>
                                    <asp:DropDownListChosen runat="server" ID="ddlLicenseType" AutoPostBack="true" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                        class="form-control" Width="100%" DataPlaceHolder="Select License Type" OnSelectedIndexChanged="ddlLicenseType_SelectedIndexChanged">
                                    </asp:DropDownListChosen>
                                    <asp:RequiredFieldValidator ID="rfvLicenseType" ErrorMessage="Please Select License Type"
                                        ControlToValidate="ddlLicenseType" runat="server" ValidationGroup="LicPopUpValidationGroup" Display="None" />
                                </div>
                                <div class="form-group col-md-4 ">
                                    <label for="ddlDepartment" class="control-label">Compliance</label>
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;" class="control-label">*</label>
                                    <asp:DropDownListChosen runat="server" ID="ddlCompliance" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                        DataPlaceHolder="Select Compliance" class="form-control" Width="100%" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="Please Select Compliance"
                                        ControlToValidate="ddlCompliance" runat="server" ValidationGroup="LicPopUpValidationGroup" Display="None" />

                                </div>
                                <div class="form-group col-md-4">
                                    <label for="tvFilterLocation" class="control-label">
                                        Entity/Sub-Entity/Location</label>
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;" class="control-label">*</label>
                                    <asp:TextBox runat="server" ID="tbxFilterLocation" CssClass="form-control bgColor-white" ReadOnly="true" autocomplete="off" AutoCompleteType="None" />

                                    <div style="position: absolute; z-index: 10; width: 92%" id="divBranches">
                                        <asp:TreeView runat="server" ID="tvFilterLocation" BackColor="White" BorderColor="Black"
                                            BorderWidth="1" SelectedNodeStyle-Font-Bold="true" Height="150px" OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged"
                                            Style="overflow: auto; margin-top: -20px; border: 1px solid #c7c7cc; background-color: #ffffff; color: #8e8e93 !important;"
                                            ShowLines="true" AutoPostBack="true">
                                        </asp:TreeView>
                                        <%--ShowCheckBoxes="All" onclick="OnTreeClick(event)" --%>
                                        <%--<asp:Button ID="btnlocation" runat="server" OnClick="btnlocation_Click" Text="select" />--%>
                                    </div>
                                    <asp:RequiredFieldValidator ID="rfvBranch" ErrorMessage="Please Select Entity/Sub-Entity/Location."
                                        ControlToValidate="tbxFilterLocation" runat="server" ValidationGroup="LicPopUpValidationGroup" Display="None" />
                                </div>

                            </div>

                            <div class="clearfix"></div>

                            <div class="row col-lg-12 col-md-12">

                                <div class="form-group required col-md-4">
                                    <label for="ddlPerformer" class="control-label">Performer</label>
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;" class="control-label">*</label>
                                    <asp:DropDownListChosen runat="server" ID="ddlPerformer" AutoPostBack="true" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                        class="form-control" Width="100%" DataPlaceHolder="Select Performer">
                                    </asp:DropDownListChosen>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Please Select Performer"
                                        ControlToValidate="ddlPerformer" runat="server" ValidationGroup="LicPopUpValidationGroup" Display="None" />
                                </div>

                                <div class="form-group required col-md-4">
                                    <label for="ddlReviewer" class="control-label">Reviewer</label>
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;" class="control-label">*</label>
                                    <asp:DropDownListChosen runat="server" ID="ddlReviewer" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                        DataPlaceHolder="Select Reviewer" class="form-control" Width="100%" AutoPostBack="true">
                                    </asp:DropDownListChosen>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Please Select Reviewer"
                                        ControlToValidate="ddlReviewer" runat="server" ValidationGroup="LicPopUpValidationGroup" Display="None" />
                                </div>

                                <div class="form-group col-md-4">
                                    <label for="ddlApprover" class="control-label">Approver</label>

                                    <asp:DropDownListChosen runat="server" ID="ddlApprover" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                        DataPlaceHolder="Select Approver" class="form-control" Width="100%">
                                    </asp:DropDownListChosen>
                                    <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="Please Select Approver"
                                        ControlToValidate="ddlApprover" runat="server" ValidationGroup="LicPopUpValidationGroup" Display="None" />--%>
                                </div>
                            </div>

                            <div class="clearfix"></div>

                            <div class="row col-lg-12 col-md-12">
                                <div class="form-group required col-md-4">
                                    <label for="txtTitle" class="control-label">License Title</label>
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;" class="control-label">*</label>
                                    <asp:TextBox runat="server" ID="txtTitle" placeholder="License Title" TextMode="SingleLine" CssClass="form-control" autocomplete="off" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ErrorMessage="Please Add License Title"
                                        ControlToValidate="txtTitle" runat="server" ValidationGroup="LicPopUpValidationGroup" Display="None" />
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="txtLicenseNo" class="control-label">License No</label>
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;" class="control-label">*</label>
                                    <asp:TextBox runat="server" ID="txtLicenseNo" placeholder="License No" TextMode="SingleLine" CssClass="form-control" autocomplete="off" onfocusout="checkName(this)" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ErrorMessage="Please Add License No."
                                        ControlToValidate="txtLicenseNo" runat="server" ValidationGroup="LicPopUpValidationGroup" Display="None" />
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="txtApplicationDays" class="control-label">Application Days</label>
                                    <asp:TextBox runat="server" ID="txtApplicationDays" placeholder="Application Days" CssClass="form-control" autocomplete="off" />
                                    <%--<asp:TextBox ID="txtApplicationDays" TextMode="Number"  runat="server" placeholder="" class="form-control col-md-8 gridtextbox date" autocomplete="off" onkeydown="return ((event.keyCode>=65 && event.keyCode>=96 && event.keyCode<=105)||!(event.keyCode>=65) && event.keyCode!=32);" />--%>
                                    <asp:CompareValidator ID="cvApplicationDays" runat="server" ControlToValidate="txtApplicationDays" ErrorMessage="Only Numbers in Application Days."
                                        ValidationGroup="LicPopUpValidationGroup" Display="None" Type="Integer" Operator="DataTypeCheck"></asp:CompareValidator>
                                </div>
                            </div>

                            <div class="clearfix"></div>

                            <div class="row col-lg-12 col-md-12">
                                <div class="form-group required col-md-4">
                                    <label for="txtStartDate" class="control-label">Start Date</label>
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;" class="control-label">*</label>
                                    <asp:TextBox ID="txtStartDate" runat="server" placeholder="DD-MM-YYYY" class="form-control" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ErrorMessage="Please Select Start Date"
                                        ControlToValidate="txtStartDate" runat="server" ValidationGroup="LicPopUpValidationGroup" Display="None" />
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="txtEndDate" class="control-label">Expiry Date</label>
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;" class="control-label">*</label>
                                    <asp:TextBox ID="txtEndDate" runat="server" placeholder="DD-MM-YYYY" class="form-control" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ErrorMessage="Please Select Expiry Date"
                                        ControlToValidate="txtEndDate" runat="server" ValidationGroup="LicPopUpValidationGroup" Display="None" />
                                </div>
                                <div class="form-group required col-md-4">
                                    <label for="LicenseFileUpload" class="control-label">Upload License Files</label>
                                    <asp:FileUpload ID="LicenseFileUpload" runat="server" AllowMultiple="true" CssClass="fileUploadClass" />
                                </div>
                            </div>

                            <div class="clearfix"></div>
                             <div class="row col-lg-12 col-md-12">

                               <div class="form-group col-md-4">
                                    <label for="txtCost" class="control-label">Cost</label>
                                     <asp:TextBox runat="server" ID="txtCost" placeholder="Cost" onkeypress="return isNumberKey(event)" CssClass="form-control" autocomplete="off" />
                                 <%--<asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txtCost" ErrorMessage="Only Numbers in Cost."
                                 ValidationGroup="LicPopUpValidationGroup" Display="None" Type="Double" Operator="DataTypeCheck"></asp:CompareValidator>--%>
                                </div>
                                <div class="form-group required col-md-4">
                                    <label for="txtfileno" class="control-label">File No</label>
                                     <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;" class="control-label">&nbsp;</label>
                                    <asp:TextBox runat="server" ID="txtfileno" placeholder="File No" TextMode="SingleLine" CssClass="form-control" autocomplete="off" />
                                   
                                </div>

                                  <div class="form-group required col-md-4">
                                    <label for="txtfilelocation" class="control-label">File Physical Location</label>
                                     <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;" class="control-label">&nbsp;</label>
                                    <asp:TextBox runat="server" ID="txtfilelocation" placeholder="File Physical Location" TextMode="SingleLine" CssClass="form-control" autocomplete="off" />
                                  
                                </div>
                            </div>

                             <div class="clearfix"></div>

                             <div class="row col-lg-12 col-md-12">
                                 <div class="form-group required col-md-4" id="divIsRegister" runat="server" visible="false">
                                      <label for="ChkIsRegister" class="control-label">IsRegister</label>
                                     <asp:CheckBox ID="ChkIsRegister" runat="server" />
                                 </div>
                                 
                             </div>
                            <div class="clearfix"></div>

                            <div class="form-group col-md-12 text-center" id="Div3" runat="server" style="margin-top: 25px;">
                                <asp:Button Text="Save" runat="server" ID="btnSaveLicense" CssClass="btn btn-primary" OnClick="btnSaveLicense_Click"
                                    ValidationGroup="LicPopUpValidationGroup"></asp:Button>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            </div>
    </form>
</body>
</html>
