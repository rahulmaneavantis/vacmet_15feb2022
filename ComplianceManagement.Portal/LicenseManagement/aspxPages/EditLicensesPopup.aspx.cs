﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.License;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Web;
using System.IO;
using Ionic.Zip;
using System.Configuration;
using Amazon.S3.IO;
using com.VirtuosoITech.ComplianceManagement.Business.AWS;
using Amazon.S3;
using Amazon;

namespace com.VirtuosoITech.ComplianceManagement.Portal.LicenseManagement.aspxPages
{
    public partial class EditLicensesPopup : System.Web.UI.Page
    {
        protected bool flag;
        protected static List<Int32> roles;
        protected static string ClickChangeflag = "P";
        protected static string user_Roles;
        public static string LicenseDocPath = "";
        protected long customerID = 0;
        protected string isstatutoryinternal = "S";
        protected string LicenseID = "-1";
        protected string CID = "-1";
        protected string SOID = "-1";
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {


                    if (!String.IsNullOrEmpty(Request.QueryString["isstatutoryinternal"]))
                    {
                        isstatutoryinternal = Request.QueryString["isstatutoryinternal"].ToString().Trim();
                    }
                    if (isstatutoryinternal == "S")
                    {
                        isstatutoryinternal = "S";
                    }
                    else if (isstatutoryinternal == "I")
                    {
                        isstatutoryinternal = "I";
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["LicenseID"])))
                    {
                        LicenseID = Request.QueryString["LicenseID"].ToString().Trim();
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["CID"])))
                    {
                        CID = Request.QueryString["CID"].ToString().Trim();
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["SOID"])))
                    {
                        SOID = Request.QueryString["SOID"].ToString().Trim();
                    }

                    EditAllData();

                   
                    user_Roles = AuthenticationHelper.Role;
                    roles = CustomerBranchManagement.GetAssignedroleid(AuthenticationHelper.UserID);


                    try
                    {
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocationPopup.ClientID), true);
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
                    }
                    catch (Exception ex)
                    {
                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvLicPopUp.IsValid = false;
                cvLicPopUp.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }

        public void EditAllData()
        {
            long licenseID = 0;
            long complianceinstanceid = 0;
            long compliancescheduleonid = 0;

            clearLicenseControls();
            BindDepartments();
            BindLocationFilterPopup();
            if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["LicenseID"])))
            {

                if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["CID"])))
                {

                    if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["SOID"])))
                    {


                        if (isstatutoryinternal == "S")
                        {
                            #region Staturory
                            licenseID = Convert.ToInt32(Request.QueryString["LicenseID"]);
                            complianceinstanceid = Convert.ToInt32(Request.QueryString["CID"]);
                            compliancescheduleonid = Convert.ToInt32(Request.QueryString["SOID"]);

                            ViewState["LicenseID"] = licenseID;
                            ViewState["complianceinstanceid"] = complianceinstanceid;
                            ViewState["compliancescheduleonid"] = compliancescheduleonid;
                            if (licenseID != 0)
                            {
                                var LicensedocumentVersionData = LicenseDocumentManagement.GetFileData(licenseID, -1).Select(x => new
                                {
                                    ID = x.ID,
                                    ScheduledOnID = x.ScheduledOnID,
                                    LicenseID = x.LicenseID,
                                    FileID = x.FileID,
                                    Version = string.IsNullOrEmpty(x.Version) ? "1.0" : x.Version,
                                    VersionDate = x.VersionDate,
                                    VersionComment = string.IsNullOrEmpty(x.VersionComment) ? "" : x.VersionComment
                                }).GroupBy(entry => entry.Version).Select(entry => entry.FirstOrDefault()).ToList();


                                rptLicenseVersion.DataSource = LicensedocumentVersionData;
                                rptLicenseVersion.DataBind();

                                rptLicenseDocumnets.DataSource = null;
                                rptLicenseDocumnets.DataBind();


                                var licenseRecord = LicenseMgmt.GetLicenseByID(licenseID);
                                if (licenseRecord != null)
                                {
                                    if (licenseRecord.IsStatutory == true)
                                        ddlLicenseTypePopup.SelectedItem.Value = "1";
                                    else
                                        ddlLicenseTypePopup.SelectedItem.Value = "0";

                                    if (Convert.ToString(licenseRecord.LicenseTypeID) != null)
                                        ddlLicenseTypePopup.SelectedItem.Value = Convert.ToString(licenseRecord.LicenseTypeID);

                                    tbxLicenseNo.Text = licenseRecord.LicenseNo;
                                    tbxLicenseTitle.Text = licenseRecord.LicenseTitle;

                                    if (licenseRecord.CustomerBranchID != 0)
                                    {
                                        foreach (TreeNode node in tvFilterLocationPopup.Nodes)
                                        {
                                            if (node.Value == licenseRecord.CustomerBranchID.ToString())
                                            {
                                                node.Selected = true;
                                            }
                                            foreach (TreeNode item1 in node.ChildNodes)
                                            {
                                                if (item1.Value == licenseRecord.CustomerBranchID.ToString())
                                                    item1.Selected = true;
                                            }
                                        }
                                    }

                                    tvFilterLocationPopup_SelectedNodeChanged(null, null);

                                    ddlDepartment.ClearSelection();

                                    if (licenseRecord.DepartmentID != null)
                                        ddlDepartment.SelectedItem.Value = Convert.ToString(licenseRecord.DepartmentID);


                                    if (licenseRecord.StartDate != null)
                                    {
                                        tbxStartDate.Text = Convert.ToDateTime(licenseRecord.StartDate).ToString("dd-MM-yyyy");
                                        ViewState["StartDate"] = Convert.ToDateTime(licenseRecord.StartDate).ToString("dd-MM-yyyy");
                                    }
                                    else
                                        tbxStartDate.Text = string.Empty;

                                    if (licenseRecord.EndDate != null)
                                    {
                                        tbxEndDate.Text = Convert.ToDateTime(licenseRecord.EndDate).ToString("dd-MM-yyyy");
                                        ViewState["EndDate"] = Convert.ToDateTime(licenseRecord.EndDate).ToString("dd-MM-yyyy");
                                    }
                                    else
                                        tbxEndDate.Text = string.Empty;


                                    if (licenseRecord.EndDate != null && licenseRecord.RemindBeforeNoOfDays > 0)
                                        tbxApplicationdays.Text = Convert.ToString(licenseRecord.RemindBeforeNoOfDays);
                                    else
                                        tbxApplicationdays.Text = string.Empty;

                                }
                                upLicense.Update();
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "OpenDialog2", "$(\"#divLicenseDialog\").dialog('open')", true);
                                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocationPopup');", tbxFilterLocationPopup.ClientID), true);
                                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter3", "$(\"#divFilterLocationPopup\").hide(\"blind\", null, 500, function () { });", true);
                            }
                            #endregion
                        }
                        else if (isstatutoryinternal == "I")
                        {
                            #region Internal
                            licenseID = Convert.ToInt32(Request.QueryString["LicenseID"]);
                            complianceinstanceid = Convert.ToInt32(Request.QueryString["CID"]);
                            compliancescheduleonid = Convert.ToInt32(Request.QueryString["SOID"]);

                            ViewState["LicenseID"] = licenseID;
                            ViewState["complianceinstanceid"] = complianceinstanceid;
                            ViewState["compliancescheduleonid"] = compliancescheduleonid;
                            if (licenseID != 0)
                            {
                                var LicensedocumentVersionData = InternalLicenseMgmt.GetInternalFileData(licenseID, -1).Select(x => new
                                {
                                    ID = x.ID,
                                    ScheduledOnID = x.ScheduledOnID,
                                    LicenseID = x.LicenseID,
                                    FileID = x.FileID,
                                    Version = string.IsNullOrEmpty(x.Version) ? "1.0" : x.Version,
                                    VersionDate = x.VersionDate,
                                    VersionComment = string.IsNullOrEmpty(x.VersionComment) ? "" : x.VersionComment
                                }).GroupBy(entry => entry.Version).Select(entry => entry.FirstOrDefault()).ToList();


                                rptLicenseVersion.DataSource = LicensedocumentVersionData;
                                rptLicenseVersion.DataBind();

                                rptLicenseDocumnets.DataSource = null;
                                rptLicenseDocumnets.DataBind();


                                var licenseRecord = InternalLicenseMgmt.GetInternalLicenseByID(licenseID);
                                if (licenseRecord != null)
                                {
                                    if (licenseRecord.IsStatutory == true)
                                        ddlLicenseTypePopup.SelectedItem.Value = "1";
                                    else
                                        ddlLicenseTypePopup.SelectedItem.Value = "0";

                                    if (Convert.ToString(licenseRecord.LicenseTypeID) != null)
                                        ddlLicenseTypePopup.SelectedItem.Value = Convert.ToString(licenseRecord.LicenseTypeID);

                                    tbxLicenseNo.Text = licenseRecord.LicenseNo;
                                    tbxLicenseTitle.Text = licenseRecord.LicenseTitle;

                                    if (licenseRecord.CustomerBranchID != 0)
                                    {
                                        foreach (TreeNode node in tvFilterLocationPopup.Nodes)
                                        {
                                            if (node.Value == licenseRecord.CustomerBranchID.ToString())
                                            {
                                                node.Selected = true;
                                            }
                                            foreach (TreeNode item1 in node.ChildNodes)
                                            {
                                                if (item1.Value == licenseRecord.CustomerBranchID.ToString())
                                                    item1.Selected = true;
                                            }
                                        }
                                    }

                                    tvFilterLocationPopup_SelectedNodeChanged(null, null);

                                    ddlDepartment.ClearSelection();

                                    if (licenseRecord.DepartmentID != null)
                                        ddlDepartment.SelectedItem.Value = Convert.ToString(licenseRecord.DepartmentID);


                                    if (licenseRecord.StartDate != null)
                                    {     
                                        tbxStartDate.Text = Convert.ToDateTime(licenseRecord.StartDate).ToString("dd-MM-yyyy");
                                        ViewState["IStartDate"] = Convert.ToDateTime(licenseRecord.StartDate).ToString("dd-MM-yyyy");
                                    }
                                    else
                                        tbxStartDate.Text = string.Empty;

                                    if (licenseRecord.EndDate != null)
                                    { 
                                        tbxEndDate.Text = Convert.ToDateTime(licenseRecord.EndDate).ToString("dd-MM-yyyy");
                                        ViewState["IEndDate"] = Convert.ToDateTime(licenseRecord.EndDate).ToString("dd-MM-yyyy");
                                    }
                                    else
                                        tbxEndDate.Text = string.Empty;


                                    if (licenseRecord.EndDate != null && licenseRecord.RemindBeforeNoOfDays > 0)
                                        tbxApplicationdays.Text = Convert.ToString(licenseRecord.RemindBeforeNoOfDays);
                                    else
                                        tbxApplicationdays.Text = string.Empty;

                                }
                                upLicense.Update();
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "OpenDialog2", "$(\"#divLicenseDialog\").dialog('open')", true);
                                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocationPopup');", tbxFilterLocationPopup.ClientID), true);
                                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter3", "$(\"#divFilterLocationPopup\").hide(\"blind\", null, 500, function () { });", true);
                            }
                            #endregion
                        }
                    }
                }
                       
            }
        }

        public void clearLicenseControls()
        {
            try
            {
                tbxLicenseNo.Text = "";
                tbxLicenseTitle.Text = "";
                tbxFilterLocationPopup.Text = "";
                tbxFilterLocationPopup.Text = "Select Entity/Location";
                ddlDepartment.ClearSelection();
                tbxStartDate.Text = "";
                tbxEndDate.Text = "";
                tbxApplicationdays.Text = "";
                ddlLicenseTypePopup.ClearSelection();
                ddlDepartment.ClearSelection();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void BindDepartments()
        {
            var lstDepts = CompDeptManagement.GetAllDepartmentMasterList(0);
            if (lstDepts.Count > 0)
                lstDepts = lstDepts.OrderBy(row => row.Name).ToList();
            ddlDepartment.DataTextField = "Name";
            ddlDepartment.DataValueField = "ID";
            ddlDepartment.DataSource = lstDepts;
            ddlDepartment.DataBind();
        }
        private void BindLocationFilterPopup()
        {
            try
            {
                tvFilterLocationPopup.Nodes.Clear();
                int CustomerID = 0;
                //if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                //{
                //    CustomerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                //}
                CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                var bracnhes = CustomerBranchManagement.GetAllHierarchyManagementSatutory(Convert.ToInt32(CustomerID));
                var LocationList = new List<int>();
                TreeNode node = new TreeNode("Entity/Sub-Entity/Location", "-1");
                node.Selected = true;
                tvFilterLocationPopup.Nodes.Add(node);
                foreach (var item in bracnhes)
                {
                    node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    CustomerBranchManagement.BindBranchesHierarchy(node, item);
                    tvFilterLocationPopup.Nodes.Add(node);
                }
                tvFilterLocationPopup.CollapseAll();
                tbxFilterLocationPopup.Text = tvFilterLocationPopup.SelectedNode != null ? tvFilterLocationPopup.SelectedNode.Text : "All";
                tvFilterLocationPopup_SelectedNodeChanged(null, null);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void tvFilterLocationPopup_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxFilterLocationPopup.Text = tvFilterLocationPopup.SelectedNode != null ? tvFilterLocationPopup.SelectedNode.Text : "All";
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void showErrorMessages(List<string> lstErrMsgs, CustomValidator cvtoShowErrorMsg)
        {
            //string finalErrMsg = string.Join("<br/>", emsg.ToArray());

            string finalErrMsg = string.Empty;

            finalErrMsg += "<ol type='1'>";

            if (lstErrMsgs.Count > 0)
            {
                lstErrMsgs.ForEach(eachErrMsg =>
                {
                    finalErrMsg += "<li>" + eachErrMsg + "</li>";
                });

                finalErrMsg += "</ol>";
            }
            cvLicPopUp.IsValid = false;
            cvLicPopUp.ErrorMessage = finalErrMsg;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "scrollUpPageScript", "scrollUpPage();", true);
        }
        protected void upLicense_Load(object sender, EventArgs e)
        {
            DateTime date = DateTime.MinValue;
            if (DateTime.TryParseExact(tbxStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month, date.Day), true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", "initializeDatePicker(null);", true);
            }

            DateTime date1 = DateTime.MinValue;
            if (DateTime.TryParseExact(tbxEndDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date1))
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date1.Year, date1.Month, date1.Day), true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", "initializeDatePicker(null);", true);
            }

            ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocationPopup');", tbxFilterLocationPopup.ClientID), true);
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter6", "$(\"#divFilterLocationPopup\").hide(\"blind\", null, 500, function () { });", true);
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            bool formValidateSuccess = false;
            bool saveSuccess = false;
            bool LICsaveSuccess = false;
            bool LICDocsaveSuccess = false;
            List<string> lstErrorMsg = new List<string>();
            List<Tuple<int, int>> lstLicenseUserMapping = new List<Tuple<int, int>>();
            #region Basic Validation  
            int customerID = 0;
            //if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
            //{
            //    customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
            //}
            customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            if (String.IsNullOrEmpty(tbxLicenseNo.Text))
            {
                lstErrorMsg.Add("Required license number");
                return;
            }
            else
            {
                formValidateSuccess = true;
            }
            if (String.IsNullOrEmpty(tbxLicenseTitle.Text))
            {
                lstErrorMsg.Add("Required license title");
                return;
            }
            else
            {
                formValidateSuccess = true;
            }
            if (ddlLicenseTypePopup.SelectedValue != "" && ddlLicenseTypePopup.SelectedValue != "-1")
            {
                formValidateSuccess = true;
            }
            else
            {
                lstErrorMsg.Add("Please select license type");
                return;
            }

            #region Validation for Start Date End Date  
            if (!string.IsNullOrEmpty(tbxStartDate.Text))
            {
                try
                {
                    bool check = LicenseCommonMethods.CheckValidDate(tbxStartDate.Text.Trim());
                    if (!check)
                    {
                        lstErrorMsg.Add("Please Check Start Date or Date should be in DD-MM-YYYY Format");
                    }
                    else
                    {
                        formValidateSuccess = true;
                    }
                }
                catch (Exception)
                {
                    lstErrorMsg.Add("Please Check Start Date or Date should be in DD-MM-YYYY Format");
                }
            }
            else
            {
                lstErrorMsg.Add("Start Date Should not be blank or Date should be in DD-MM-YYYY Format");
            }

            if (!string.IsNullOrEmpty(tbxEndDate.Text))
            {
                try
                {
                    bool check = LicenseCommonMethods.CheckValidDate(tbxEndDate.Text.Trim());
                    if (!check)
                    {
                        lstErrorMsg.Add("Please Check Expiry Date or Date should be in DD-MM-YYYY Format");
                    }
                    else
                    {
                        formValidateSuccess = true;
                    }
                }
                catch (Exception)
                {
                    lstErrorMsg.Add("Please Check Expiry Date or Date should be in DD-MM-YYYY Format");
                }
            }
            else
            {
                lstErrorMsg.Add("Expiry Date Should not be blank or Date should be in DD-MM-YYYY Format");
            }

            if ((!string.IsNullOrEmpty(tbxStartDate.Text)) && (!string.IsNullOrEmpty(tbxEndDate.Text)))
            {
                if (DateTime.Compare(DateTimeExtensions.GetDate(tbxEndDate.Text.ToString()), DateTimeExtensions.GetDate(tbxStartDate.Text.ToString())) <= 0)
                {
                    lstErrorMsg.Add("Expiry Date should be greater than Start Date.");
                }
                else
                {
                    formValidateSuccess = true;
                }
            }
             

            #endregion

            //if (fuSampleFile.HasFile)
            if (fuSampleFile.HasFile)
            {
                //{
                string[] validFileTypes = { "exe", "bat", "dll", "css", "js", };
                string ext = System.IO.Path.GetExtension(fuSampleFile.PostedFile.FileName);
                if (ext == "")
                {
                    //lstErrorMsg.Add("Please do not upload virus file or blank files or file has no extention.");
                }
                else
                {
                    for (int i = 0; i < validFileTypes.Length; i++)
                    {
                        if (ext == "." + validFileTypes[i])
                        {
                            lstErrorMsg.Add("Please do not upload virus file or blank files.");
                            break;
                        }
                    }
                }
            }
            if (lstErrorMsg.Count > 0)
            {
                formValidateSuccess = false;
                showErrorMessages(lstErrorMsg, cvLicPopUp);
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "scrollUpPageScript", "scrollUpPage();", true);

            }
            else
                formValidateSuccess = true;

            #endregion

            if (formValidateSuccess == true)
            {
                long LicenseID = -1;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["LicenseID"])))
                {
                    LicenseID = Convert.ToInt32(ViewState["LicenseID"]);
                }
                else
                {
                    LicenseID = Convert.ToInt32(Request.QueryString["AccessID"]);
                }
                if (LicenseID != -1)
                {
                    if (!String.IsNullOrEmpty(Request.QueryString["isstatutoryinternal"]))
                    {
                        isstatutoryinternal = Request.QueryString["isstatutoryinternal"].ToString().Trim();
                    }
                    if (isstatutoryinternal == "S")
                    {
                        isstatutoryinternal = "S";
                    }
                    else if (isstatutoryinternal == "I")
                    {
                        isstatutoryinternal = "I";
                    }
                    if (isstatutoryinternal == "S")
                    {
                        #region Save License Instance and Status  Old                 
                        //Lic_tbl_LicenseInstance licenseRecord = new Lic_tbl_LicenseInstance()
                        //{
                        //    UpdatedBy = AuthenticationHelper.UserID,
                        //};
                        //if (!string.IsNullOrEmpty(tbxLicenseNo.Text))
                        //    licenseRecord.LicenseNo = Convert.ToString(tbxLicenseNo.Text);

                        //if (!string.IsNullOrEmpty(tbxLicenseTitle.Text))
                        //    licenseRecord.LicenseTitle = Convert.ToString(tbxLicenseTitle.Text);

                        //if (!string.IsNullOrEmpty(tbxStartDate.Text))
                        //    licenseRecord.StartDate = DateTimeExtensions.GetDate(tbxStartDate.Text);

                        //if (!string.IsNullOrEmpty(tbxEndDate.Text))
                        //    licenseRecord.EndDate = DateTimeExtensions.GetDate(tbxEndDate.Text);

                        //if (!string.IsNullOrEmpty(tbxApplicationdays.Text))
                        //    licenseRecord.RemindBeforeNoOfDays = Convert.ToInt32(tbxApplicationdays.Text);
                        //else
                        //    licenseRecord.RemindBeforeNoOfDays = 0;

                        //if (!string.IsNullOrEmpty(Convert.ToString(tvFilterLocationPopup.SelectedNode.Value)))
                        //{
                        //    licenseRecord.CustomerBranchID = Convert.ToInt32(tvFilterLocationPopup.SelectedNode.Value);
                        //}
                        //if (!string.IsNullOrEmpty(ddlDepartment.SelectedValue))
                        //{
                        //    licenseRecord.DepartmentID = Convert.ToInt32(ddlDepartment.SelectedValue);
                        //}

                        //licenseRecord.ID = LicenseID;

                        //bool existLicNo = LicenseMgmt.ExistsLicenseNo(Convert.ToInt32(customerID), licenseRecord.LicenseNo);
                        //if (!existLicNo)
                        //{
                        //    saveSuccess = LicenseMgmt.UpdateLicense(licenseRecord);
                        //    if (saveSuccess)
                        //    {

                        //        clearLicenseControls();
                        //        cvLicPopUp.IsValid = false;
                        //        cvLicPopUp.ErrorMessage = "License Details Updated Sucessfully";
                        //       // BindGrid();
                        //    }
                        //    else
                        //    {
                        //        cvLicPopUp.IsValid = false;
                        //        cvLicPopUp.ErrorMessage = "Something went wrong.";
                        //    }
                        //}
                        //else
                        //{
                        //    cvLicPopUp.IsValid = false;
                        //    cvLicPopUp.ErrorMessage = "License Details with Same License Number already exists";
                        //    formValidateSuccess = false;
                        //}
                        #endregion

                       
                        int licenseID = Convert.ToInt32(ViewState["LicenseID"]);
                        int complianceinstanceid = Convert.ToInt32(ViewState["complianceinstanceid"]);
                        int compliancescheduleonid = Convert.ToInt32(ViewState["compliancescheduleonid"]);
                        var sdate = DateTimeExtensions.GetDate(ViewState["StartDate"].ToString());
                        var edate = DateTimeExtensions.GetDate(ViewState["EndDate"].ToString());


                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        {
                            try
                            {

                                var query = (from row in entities.RecentLicenseTransactionViews
                                             where row.LicenseID == licenseID
                                             select row).FirstOrDefault();

                                var Licensedata = (from row in entities.Lic_tbl_LicenseInstance
                                                   where row.ID == licenseID
                                                   select row).FirstOrDefault();

                                var OldScheduleOnDate = (from row in entities.ComplianceScheduleOns
                                                         where row.ID == compliancescheduleonid
                                                         select row.ScheduleOn).FirstOrDefault();

                                Lic_tbl_LicenseUpdate_Log LicenseLog = new Lic_tbl_LicenseUpdate_Log
                                {
                                    LicenseID = licenseID,
                                    ScheduleOnID = compliancescheduleonid,
                                    OldStartdate = sdate,
                                    OldEndDate = edate,
                                    OldLicenseStatusID = Convert.ToInt32(query.StatusID),
                                    OldScheduleOnDate = OldScheduleOnDate,
                                    Createdby = AuthenticationHelper.UserID,
                                    Createdon = DateTime.Now,

                                };

                                long UpdateID = LicenseMgmt.CreateLicenseLog(LicenseLog);

                                Lic_tbl_LicenseInstance licenseRecord = new Lic_tbl_LicenseInstance()
                                {
                                    UpdatedBy = AuthenticationHelper.UserID,
                                };
                                Lic_tbl_LicenseInstance_Log newLic_tbl_LicenseInstance_Log = new Lic_tbl_LicenseInstance_Log()
                                {
                                    CustomerID = Convert.ToInt64(AuthenticationHelper.CustomerID),
                                    LicenseTypeID = Licensedata.LicenseTypeID,
                                    CreatedBy = AuthenticationHelper.UserID,
                                    UpdatedBy = AuthenticationHelper.UserID,
                                    UpdatedOn = DateTime.Now,
                                };

                                if (!string.IsNullOrEmpty(tbxLicenseNo.Text))
                                {
                                    licenseRecord.LicenseNo = Convert.ToString(tbxLicenseNo.Text);
                                    newLic_tbl_LicenseInstance_Log.LicenseNo = Convert.ToString(tbxLicenseNo.Text.Trim());
                                }

                                if (!string.IsNullOrEmpty(tbxLicenseTitle.Text))
                                {
                                    licenseRecord.LicenseTitle = Convert.ToString(tbxLicenseTitle.Text);
                                    newLic_tbl_LicenseInstance_Log.LicenseTitle = Convert.ToString(tbxLicenseTitle.Text.Trim());
                                }
                                if (!string.IsNullOrEmpty(tbxStartDate.Text))
                                {
                                    licenseRecord.StartDate = DateTimeExtensions.GetDate(tbxStartDate.Text);
                                    newLic_tbl_LicenseInstance_Log.StartDate = DateTimeExtensions.GetDate(tbxStartDate.Text);
                                }

                                if (!string.IsNullOrEmpty(tbxEndDate.Text))
                                {
                                    licenseRecord.EndDate = DateTimeExtensions.GetDate(tbxEndDate.Text);
                                    newLic_tbl_LicenseInstance_Log.EndDate = DateTimeExtensions.GetDate(tbxEndDate.Text);
                                }

                                if (!string.IsNullOrEmpty(tbxApplicationdays.Text))
                                {
                                    licenseRecord.RemindBeforeNoOfDays = Convert.ToInt32(tbxApplicationdays.Text);
                                    newLic_tbl_LicenseInstance_Log.RemindBeforeNoOfDays = Convert.ToInt32(tbxApplicationdays.Text);
                                }
                                else
                                {
                                    licenseRecord.RemindBeforeNoOfDays = 0;
                                    newLic_tbl_LicenseInstance_Log.RemindBeforeNoOfDays = 0;
                                }
                                bool IsStatutory = true;
                                newLic_tbl_LicenseInstance_Log.IsStatutory = IsStatutory;


                                if (!string.IsNullOrEmpty(ddlDepartment.SelectedValue))
                                {
                                    licenseRecord.DepartmentID = Convert.ToInt32(ddlDepartment.SelectedValue);
                                }

                                licenseRecord.ID = LicenseID;
                                //bool existLicNo = LicenseMgmt.ExistsLicenseNo(Convert.ToInt32(customerID), licenseRecord.LicenseNo);
                                //if (!existLicNo)
                                //{
                                LICsaveSuccess = LicenseMgmt.UpdateLicense(licenseRecord);
                                newLic_tbl_LicenseInstance_Log.LicenseID = LicenseID;
                                //LicenseMgmt.UpdateLicenseLog(newLic_tbl_LicenseInstance_Log);
                                LicenseMgmt.CreateLicenseLog(newLic_tbl_LicenseInstance_Log);

                                //if (((DateTimeExtensions.GetDate(tbxStartDate.Text)) != (DateTimeExtensions.GetDate(ViewState["StartDate"].ToString()))) ||
                                //    ((DateTimeExtensions.GetDate(tbxEndDate.Text)) != (DateTimeExtensions.GetDate(ViewState["EndDate"].ToString())))
                                //    || query.StatusID == 5)
                                //{
                                    if (query.StatusID == 2)  // if Active
                                    {
                                        Lic_tbl_LicenseStatusTransaction LicenseStatusTransaction = new Lic_tbl_LicenseStatusTransaction
                                        {
                                            LicenseID = licenseID,
                                            CustomerID = customerID,
                                            //StatusID = 2, //License Status Active
                                            StatusChangeOn = DateTime.Now,
                                            IsActive = true,
                                            CreatedBy = AuthenticationHelper.UserID,
                                            UpdatedBy = AuthenticationHelper.UserID,
                                            CreatedOn = DateTime.Now,
                                            UpdatedOn = DateTime.Now,
                                            Remark = "License Details Updated By User Externaly",
                                            IsAutoUpdate = false,
                                            ComplianceScheduleOnID = compliancescheduleonid,

                                        };
                                        if (DateTimeExtensions.GetDate(Convert.ToString(tbxEndDate.Text)).Date <= DateTime.Today.Date)
                                        {
                                            LicenseStatusTransaction.StatusID = 3;        //Expired
                                        }
                                        else if (DateTimeExtensions.GetDate(Convert.ToString(tbxEndDate.Text)).Date <= DateTime.Today.Date.AddDays(30))
                                        {
                                            LicenseStatusTransaction.StatusID = 4;            //Expiring
                                        }
                                        else
                                            LicenseStatusTransaction.StatusID = 2;          //Active



                                        entities.Lic_tbl_LicenseStatusTransaction.Add(LicenseStatusTransaction);
                                        entities.SaveChanges();
                                        DateTime Applicationdate = Convert.ToDateTime(licenseRecord.EndDate);
                                        Applicationdate = Applicationdate.AddDays(-Convert.ToDouble(licenseRecord.RemindBeforeNoOfDays));

                                        ComplianceScheduleOn complianceScheduleOn = new ComplianceScheduleOn()
                                        {
                                            ScheduleOn = Applicationdate.Date,
                                            ComplianceInstanceID = complianceinstanceid,
                                            IsActive = true,
                                            IsUpcomingNotDeleted = true,
                                        };
                                        complianceScheduleOn.ID = compliancescheduleonid;

                                        LICsaveSuccess = LicenseMgmt.UpdateComplianceScheduleOn(complianceScheduleOn);

                                    }
                                    else if (query.StatusID == 3) //if Expired
                                    {
                                        Lic_tbl_LicenseStatusTransaction LicenseStatusTransaction = new Lic_tbl_LicenseStatusTransaction
                                        {
                                            LicenseID = licenseID,
                                            CustomerID = customerID,
                                            //StatusID = 2, //License Status Active
                                            StatusChangeOn = DateTime.Now,
                                            IsActive = true,
                                            CreatedBy = AuthenticationHelper.UserID,
                                            UpdatedBy = AuthenticationHelper.UserID,
                                            CreatedOn = DateTime.Now,
                                            UpdatedOn = DateTime.Now,
                                            Remark = "License Details Updated By User Externaly",
                                            IsAutoUpdate = false,
                                            ComplianceScheduleOnID = compliancescheduleonid,
                                        };

                                        if (DateTimeExtensions.GetDate(Convert.ToString(tbxEndDate.Text)).Date <= DateTime.Today.Date)
                                        {
                                            LicenseStatusTransaction.StatusID = 3;        //Expired
                                        }
                                        else if (DateTimeExtensions.GetDate(Convert.ToString(tbxEndDate.Text)).Date <= DateTime.Today.Date.AddDays(30))
                                        {
                                            LicenseStatusTransaction.StatusID = 4;            //Expiring
                                        }
                                        else
                                            LicenseStatusTransaction.StatusID = 2;              //Active

                                        entities.Lic_tbl_LicenseStatusTransaction.Add(LicenseStatusTransaction);
                                        entities.SaveChanges();

                                        DateTime Applicationdate = Convert.ToDateTime(licenseRecord.EndDate);
                                        Applicationdate = Applicationdate.AddDays(-Convert.ToDouble(licenseRecord.RemindBeforeNoOfDays));
                                        ComplianceScheduleOn complianceScheduleOn = new ComplianceScheduleOn()
                                        {
                                            ScheduleOn = Applicationdate.Date,
                                            ComplianceInstanceID = complianceinstanceid,
                                            IsActive = true,
                                            IsUpcomingNotDeleted = true,
                                        };
                                        complianceScheduleOn.ID = compliancescheduleonid;

                                        LICsaveSuccess = LicenseMgmt.UpdateComplianceScheduleOn(complianceScheduleOn);

                                    }
                                    else if (query.StatusID == 4) //if Expiring
                                    {
                                        Lic_tbl_LicenseStatusTransaction LicenseStatusTransaction = new Lic_tbl_LicenseStatusTransaction
                                        {
                                            LicenseID = licenseID,
                                            CustomerID = customerID,
                                            //StatusID = 2, //License Status Active
                                            StatusChangeOn = DateTime.Now,
                                            IsActive = true,
                                            CreatedBy = AuthenticationHelper.UserID,
                                            UpdatedBy = AuthenticationHelper.UserID,
                                            CreatedOn = DateTime.Now,
                                            UpdatedOn = DateTime.Now,
                                            Remark = "License Details Updated By User Externaly",
                                            IsAutoUpdate = false,
                                            ComplianceScheduleOnID = compliancescheduleonid,
                                        };
                                        if (DateTimeExtensions.GetDate(Convert.ToString(tbxEndDate.Text)).Date <= DateTime.Today.Date)
                                        {
                                            LicenseStatusTransaction.StatusID = 3;        //Expired
                                        }
                                        else if (DateTimeExtensions.GetDate(Convert.ToString(tbxEndDate.Text)).Date <= DateTime.Today.Date.AddDays(30))
                                        {
                                            LicenseStatusTransaction.StatusID = 4;            //Expiring
                                        }
                                        else
                                            LicenseStatusTransaction.StatusID = 2;

                                        entities.Lic_tbl_LicenseStatusTransaction.Add(LicenseStatusTransaction);
                                        entities.SaveChanges();

                                        DateTime Applicationdate = Convert.ToDateTime(licenseRecord.EndDate);
                                        Applicationdate = Applicationdate.AddDays(-Convert.ToDouble(licenseRecord.RemindBeforeNoOfDays));
                                        ComplianceScheduleOn complianceScheduleOn = new ComplianceScheduleOn()
                                        {
                                            ScheduleOn = Applicationdate.Date,
                                            ComplianceInstanceID = complianceinstanceid,
                                            IsActive = true,
                                            IsUpcomingNotDeleted = true,
                                        };
                                        complianceScheduleOn.ID = compliancescheduleonid;

                                        LICsaveSuccess = LicenseMgmt.UpdateComplianceScheduleOn(complianceScheduleOn);
                                    }
                                    else if (query.StatusID == 5) //if Applied
                                    {
                                        //if (DateTimeExtensions.GetDate(Convert.ToString(tbxEndDate.Text)).Date > DateTime.Today.Date)
                                        //{
                                        long complianceScheduleOnIdNew = 0;
                                        DateTime Applicationdate = Convert.ToDateTime(licenseRecord.EndDate);
                                        Applicationdate = Applicationdate.AddDays(-Convert.ToDouble(licenseRecord.RemindBeforeNoOfDays));
                                        ComplianceScheduleOn complianceScheduleOn = new ComplianceScheduleOn()
                                        {
                                            //ScheduleOn = licenseRecord.EndDate.Value.Date,
                                            ScheduleOn = Applicationdate.Date,
                                            ComplianceInstanceID = complianceinstanceid,
                                            IsActive = true,
                                            IsUpcomingNotDeleted = true,
                                        };
                                        complianceScheduleOnIdNew = LicenseMgmt.CreateComplianceScheduleOn(complianceScheduleOn);

                                        Lic_tbl_LicenseStatusTransaction LicenseStatusTransaction = new Lic_tbl_LicenseStatusTransaction
                                        {
                                            LicenseID = licenseID,
                                            CustomerID = customerID,
                                            //StatusID = 2, //License Status Active
                                            StatusChangeOn = DateTime.Now,
                                            IsActive = true,
                                            CreatedBy = AuthenticationHelper.UserID,
                                            UpdatedBy = AuthenticationHelper.UserID,
                                            CreatedOn = DateTime.Now,
                                            UpdatedOn = DateTime.Now,
                                            Remark = "License Details Added By User Externaly",
                                            IsAutoUpdate = false,
                                            ComplianceScheduleOnID = complianceScheduleOnIdNew,
                                        };

                                        if (DateTimeExtensions.GetDate(Convert.ToString(tbxEndDate.Text)).Date <= DateTime.Today.Date)
                                        {
                                            LicenseStatusTransaction.StatusID = 3;        //Expired
                                        }
                                        else if (DateTimeExtensions.GetDate(Convert.ToString(tbxEndDate.Text)).Date <= DateTime.Today.Date.AddDays(30))
                                        {
                                            LicenseStatusTransaction.StatusID = 4;            //Expiring
                                        }
                                        else
                                            LicenseStatusTransaction.StatusID = 2;

                                        entities.Lic_tbl_LicenseStatusTransaction.Add(LicenseStatusTransaction);
                                        entities.SaveChanges();

                                        if (complianceScheduleOnIdNew > 0)
                                        {
                                            Lic_tbl_LicenseComplianceInstanceScheduleOnMapping lictbllicensecomplianceschedulonmapping = new Lic_tbl_LicenseComplianceInstanceScheduleOnMapping()
                                            {
                                                LicenseID = licenseRecord.ID,
                                                ComplianceInstanceID = complianceinstanceid,
                                                ComplianceScheduleOnID = complianceScheduleOnIdNew,
                                                IsActivation = "Application",
                                                IsStatutoryORInternal = "S"
                                            };
                                            LicenseMgmt.CreateLicenseLicenseComplianceInstanceScheduleOnMapping(lictbllicensecomplianceschedulonmapping);

                                            ComplianceTransaction transaction = new ComplianceTransaction()
                                            {
                                                ComplianceInstanceId = complianceinstanceid,
                                                StatusId = 1,
                                                Remarks = "New compliance assigned.",
                                                Dated = DateTime.Now,
                                                CreatedBy = Convert.ToInt64(AuthenticationHelper.UserID),
                                                CreatedByText = Convert.ToString(AuthenticationHelper.User),
                                                ComplianceScheduleOnID = complianceScheduleOnIdNew,
                                            };

                                            entities.ComplianceTransactions.Add(transaction);
                                            entities.SaveChanges();
                                        }


                                    }
                                    else if (query.StatusID == 6) //Appleid But Pending For Renewal
                                    {


                                        DateTime Applicationdate = Convert.ToDateTime(licenseRecord.EndDate);
                                        Applicationdate = Applicationdate.AddDays(-Convert.ToDouble(licenseRecord.RemindBeforeNoOfDays));
                                        ComplianceScheduleOn complianceScheduleOn = new ComplianceScheduleOn()
                                        {
                                            ScheduleOn = Applicationdate.Date,
                                            ComplianceInstanceID = complianceinstanceid,
                                            IsActive = true,
                                            IsUpcomingNotDeleted = true,
                                        };
                                        complianceScheduleOn.ID = compliancescheduleonid;

                                        LICsaveSuccess = LicenseMgmt.UpdateComplianceScheduleOn(complianceScheduleOn);

                                    }
                                    else if (query.StatusID == 7) //Rejected
                                    {


                                        DateTime Applicationdate = Convert.ToDateTime(licenseRecord.EndDate);
                                        Applicationdate = Applicationdate.AddDays(-Convert.ToDouble(licenseRecord.RemindBeforeNoOfDays));
                                        ComplianceScheduleOn complianceScheduleOn = new ComplianceScheduleOn()
                                        {
                                            ScheduleOn = Applicationdate.Date,
                                            ComplianceInstanceID = complianceinstanceid,
                                            IsActive = true,
                                            IsUpcomingNotDeleted = true,
                                        };
                                        complianceScheduleOn.ID = compliancescheduleonid;

                                        LICsaveSuccess = LicenseMgmt.UpdateComplianceScheduleOn(complianceScheduleOn);

                                    }
                                //}
                            }
                            catch (Exception ex)
                            {
                                LicenseMgmt.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                            }
                             
                        }
                        #region Save Document Against Current ComplianceScheduleonID
                            long complianceInstanceId = 0;
                            long complianceScheduleOnId = 0;
                            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["complianceinstanceid"])))
                            {
                                complianceInstanceId = Convert.ToInt32(ViewState["complianceinstanceid"]);
                            }
                            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["compliancescheduleonid"])))
                            {
                                complianceScheduleOnId = Convert.ToInt32(ViewState["compliancescheduleonid"]);
                            }
                            if (complianceScheduleOnId > 0)
                            {
                                List<KeyValuePair<string, Byte[]>> Filelist = new List<KeyValuePair<string, Byte[]>>();
                                List<FileData> files = new List<FileData>();
                                List<KeyValuePair<string, int>> list = new List<KeyValuePair<string, int>>();
                                HttpFileCollection fileCollection = Request.Files;
                                bool blankfileCount = true;
                                string directoryPath = null;
                                if (fileCollection.Count > 0)
                                {
                                    var AWSData = AmazonS3.GetAWSStorageDetail(customerID);
                                    if (AWSData != null)
                                    {
                                        #region AWS Storage
                                        var InstanceData = DocumentManagement.GetComplianceInstanceData(Convert.ToInt32(complianceInstanceId));

                                        string version = null;
                                        version = DocumentManagement.GetDocumnetVersion(Convert.ToInt32(complianceScheduleOnId));
                                        directoryPath = "avacomdocuments\\" + customerID + "\\" + InstanceData.CustomerBranchID + "\\" + InstanceData.ID.ToString() + "\\" + complianceScheduleOnId + "\\" + version;

                                        IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.APSouth1);
                                        S3DirectoryInfo di = new S3DirectoryInfo(client, AWSData.BucketName, directoryPath);
                                        if (!di.Exists)
                                        {
                                            di.Create();
                                        }

                                        for (int i = 0; i < fileCollection.Count; i++)
                                        {
                                            HttpPostedFile uploadfile = fileCollection[i];
                                            //HttpPostedFile uploadfile = LicenseFileUpload[j];
                                            String fileName = "";
                                            fileName = "ComplianceDoc_" + uploadfile.FileName;
                                            list.Add(new KeyValuePair<string, int>(fileName, 1));
                                            Guid fileKey = Guid.NewGuid();
                                            string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(uploadfile.FileName));
                                            Stream fs = uploadfile.InputStream;
                                            BinaryReader br = new BinaryReader(fs);
                                            Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                                            Filelist.Add(new KeyValuePair<string, Byte[]>(finalPath, bytes));

                                            if (uploadfile.ContentLength > 0)
                                            {
                                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                                string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;

                                                string TdirectoryPath = "~/TempDocuments/AWS/" + User + "/" + uploadfile.FileName;
                                                string directoryPath1 = "~/TempDocuments/AWS/" + User;
                                                string TFilePath = Server.MapPath(TdirectoryPath);
                                                if (File.Exists(TFilePath))
                                                    File.Delete(TFilePath);
                                                if (!Directory.Exists(directoryPath1))
                                                {
                                                    Directory.CreateDirectory(Server.MapPath(directoryPath1));
                                                }

                                                FileStream objFileStrm = File.Create(TFilePath);
                                                objFileStrm.Close();
                                                File.WriteAllBytes(TFilePath, bytes);

                                                string AWSpath = "";
                                                AWSpath = directoryPath + "\\ComplianceDoc_" + uploadfile.FileName;

                                                FileInfo localFile = new FileInfo(TFilePath);
                                                S3FileInfo s3File = new S3FileInfo(client, AWSData.BucketName, AWSpath);
                                                if (!s3File.Exists)
                                                {
                                                    using (var s3Stream = s3File.Create()) // <-- create file in S3  
                                                    {
                                                        localFile.OpenRead().CopyTo(s3Stream); // <-- copy the content to S3  
                                                    }
                                                }

                                                string filepathvalue = string.Empty;
                                                string vale = directoryPath.Replace(ConfigurationManager.AppSettings["DriveUrl"], "~");
                                                filepathvalue = vale.Replace(@"\", "/");

                                                FileData file = new FileData()
                                                {
                                                    Name = fileName,
                                                    FilePath = filepathvalue,
                                                    FileKey = fileKey.ToString(),
                                                    Version = version,
                                                    VersionDate = DateTime.UtcNow,
                                                    FileSize = uploadfile.ContentLength,
                                                };
                                                files.Add(file);
                                            }
                                            else
                                            {
                                                if (!string.IsNullOrEmpty(uploadfile.FileName))
                                                    blankfileCount = false;
                                            }
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        #region Normal Storage
                                        var InstanceData = DocumentManagement.GetComplianceInstanceData(Convert.ToInt32(complianceInstanceId));

                                        string version = null;
                                        version = DocumentManagement.GetDocumnetVersion(Convert.ToInt32(complianceScheduleOnId));
                                        if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                        {
                                            directoryPath = ConfigurationManager.AppSettings["DriveUrl"] + "\\AvacomDocuments\\" + customerID + "\\" + InstanceData.CustomerBranchID + "\\" + InstanceData.ID.ToString() + "\\" + complianceScheduleOnId + "\\" + version;
                                        }
                                        else
                                        {
                                            directoryPath = Server.MapPath("~/AvacomDocuments/" + customerID + "/" + InstanceData.CustomerBranchID + "/" + InstanceData.ID.ToString() + "/" + complianceScheduleOnId + "/" + version);
                                        }
                                        DocumentManagement.CreateDirectory(directoryPath);
                                        for (int i = 0; i < fileCollection.Count; i++)
                                        {
                                            HttpPostedFile uploadfile = fileCollection[i];
                                            string[] keys = fileCollection.Keys[i].Split('$');
                                            String fileName = "";
                                            if (keys[keys.Count() - 1].Equals("fuSampleFile"))
                                            {
                                                fileName = "ComplianceDoc_" + uploadfile.FileName;
                                                list.Add(new KeyValuePair<string, int>(fileName, 1));
                                            }
                                            Guid fileKey = Guid.NewGuid();
                                            string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(uploadfile.FileName));

                                            Stream fs = uploadfile.InputStream;
                                            BinaryReader br = new BinaryReader(fs);
                                            Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                                            Filelist.Add(new KeyValuePair<string, Byte[]>(finalPath, bytes));

                                            if (uploadfile.ContentLength > 0)
                                            {
                                                //Change by rahul on 21 JAN 2017 for Azure Drive or Local Drive
                                                string filepathvalue = string.Empty;
                                                if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                                {
                                                    string vale = directoryPath.Replace(ConfigurationManager.AppSettings["DriveUrl"], "~");
                                                    filepathvalue = vale.Replace(@"\", "/");
                                                }
                                                else
                                                {
                                                    filepathvalue = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                                }

                                                FileData file = new FileData()
                                                {
                                                    Name = fileName,
                                                    FilePath = filepathvalue,
                                                    FileKey = fileKey.ToString(),
                                                    Version = version,
                                                    VersionDate = DateTime.UtcNow,
                                                    FileSize = uploadfile.ContentLength,
                                                };
                                                files.Add(file);
                                            }
                                            else
                                            {
                                                if (!string.IsNullOrEmpty(uploadfile.FileName))
                                                    blankfileCount = false;
                                            }
                                        }
                                        #endregion
                                    }
                                }
                                else
                                {
                                    LICDocsaveSuccess = true;
                                }
                            bool flag = false;
                                if (blankfileCount)
                                {
                                    var tranid = GetTransactionID(complianceScheduleOnId);
                                    flag = CreateTransaction(complianceScheduleOnId, tranid, files, list, Filelist, directoryPath, complianceScheduleOnId, customerID);
                                    if (flag == true)
                                    {
                                        LICDocsaveSuccess = true;
                                    }
                            }
                                else
                                {
                                    ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "missingAlert", "alert('Please do not upload virus file or blank files.')", true);
                                    cvLicPopUp.IsValid = false;
                                    cvLicPopUp.ErrorMessage = "Please do not upload virus file or blank files.";
                                }
                            }//complianceScheduleOnId
                        #endregion

                        if (LICsaveSuccess == true && LICDocsaveSuccess == true)
                        {
                            //clearLicenseControls();
                            cvLicPopUp.IsValid = false;
                            cvLicPopUp.ErrorMessage = "License Details Updated Sucessfully";
                            VSLicPopup.CssClass = "alert alert-success";

                        }
                    }
                    else if (isstatutoryinternal == "I")
                    {
                        #region Save License Instance and Status old                
                        //Lic_tbl_InternalLicenseInstance licenseRecord = new Lic_tbl_InternalLicenseInstance()
                        //{
                        //    UpdatedBy = AuthenticationHelper.UserID,
                        //};
                        //if (!string.IsNullOrEmpty(tbxLicenseNo.Text))
                        //    licenseRecord.LicenseNo = Convert.ToString(tbxLicenseNo.Text);

                        //if (!string.IsNullOrEmpty(tbxLicenseTitle.Text))
                        //    licenseRecord.LicenseTitle = Convert.ToString(tbxLicenseTitle.Text);

                        //if (!string.IsNullOrEmpty(tbxStartDate.Text))
                        //    licenseRecord.StartDate = DateTimeExtensions.GetDate(tbxStartDate.Text);

                        //if (!string.IsNullOrEmpty(tbxEndDate.Text))
                        //    licenseRecord.EndDate = DateTimeExtensions.GetDate(tbxEndDate.Text);

                        //if (!string.IsNullOrEmpty(tbxApplicationdays.Text))
                        //    licenseRecord.RemindBeforeNoOfDays = Convert.ToInt32(tbxApplicationdays.Text);
                        //else
                        //    licenseRecord.RemindBeforeNoOfDays = 0;

                        //if (!string.IsNullOrEmpty(Convert.ToString(tvFilterLocationPopup.SelectedNode.Value)))
                        //{
                        //    licenseRecord.CustomerBranchID = Convert.ToInt32(tvFilterLocationPopup.SelectedNode.Value);
                        //}
                        //if (!string.IsNullOrEmpty(ddlDepartment.SelectedValue))
                        //{
                        //    licenseRecord.DepartmentID = Convert.ToInt32(ddlDepartment.SelectedValue);
                        //}

                        //licenseRecord.ID = LicenseID;

                        //bool existLicNo = InternalLicenseMgmt.ExistsInternalLicenseNo(Convert.ToInt32(customerID), licenseRecord.LicenseNo);
                        //if (!existLicNo)
                        //{
                        //    saveSuccess = InternalLicenseMgmt.UpdateLicense(licenseRecord);
                        //    if (saveSuccess)
                        //    {

                        //        clearLicenseControls();
                        //        cvLicPopUp.IsValid = false;
                        //        cvLicPopUp.ErrorMessage = "License Details Updated Sucessfully";

                        //    }
                        //    else
                        //    {
                        //        cvLicPopUp.IsValid = false;
                        //        cvLicPopUp.ErrorMessage = "Something went wrong.";
                        //    }
                        //}
                        //else
                        //{
                        //    cvLicPopUp.IsValid = false;
                        //    cvLicPopUp.ErrorMessage = "License Details with Same License Number already exists";
                        //    formValidateSuccess = false;
                        //}
                        #endregion

                        #region Save License Instance and Status  New
                        int licenseID = Convert.ToInt32(ViewState["LicenseID"]);
                        int complianceinstanceid = Convert.ToInt32(ViewState["complianceinstanceid"]);
                        int compliancescheduleonid = Convert.ToInt32(ViewState["compliancescheduleonid"]);
                        var sdate = DateTimeExtensions.GetDate(ViewState["IStartDate"].ToString());
                        var edate = DateTimeExtensions.GetDate(ViewState["IEndDate"].ToString());


                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        {
                            try
                            {

                                var query = (from row in entities.RecentInternalLicenseTransactionViews
                                             where row.LicenseID == licenseID
                                             select row).FirstOrDefault();

                                var Licensedata = (from row in entities.Lic_tbl_InternalLicenseInstance
                                                   where row.ID == licenseID
                                                   select row).FirstOrDefault();

                                var OldScheduleOnDate = (from row in entities.InternalComplianceScheduledOns
                                                         where row.ID == compliancescheduleonid
                                                         select row.ScheduledOn).FirstOrDefault();

                                Lic_tbl_LicenseUpdate_Log LicenseLog = new Lic_tbl_LicenseUpdate_Log
                                {
                                    LicenseID = licenseID,
                                    ScheduleOnID = compliancescheduleonid,
                                    OldStartdate = sdate,
                                    OldEndDate = edate,
                                    OldLicenseStatusID = Convert.ToInt32(query.StatusID),
                                    OldScheduleOnDate = OldScheduleOnDate,
                                    Createdby = AuthenticationHelper.UserID,
                                    Createdon = DateTime.Now,

                                };

                                long UpdateID = LicenseMgmt.CreateLicenseLog(LicenseLog);

                                Lic_tbl_InternalLicenseInstance licenseRecord = new Lic_tbl_InternalLicenseInstance()
                                {
                                    UpdatedBy = AuthenticationHelper.UserID,
                                };
                                Lic_tbl_InternalLicenseInstance_Log newLic_tbl_LicenseInstance_Log = new Lic_tbl_InternalLicenseInstance_Log()
                                {
                                    CustomerID = Convert.ToInt64(AuthenticationHelper.CustomerID),
                                    LicenseTypeID = Licensedata.LicenseTypeID,
                                    CreatedBy = AuthenticationHelper.UserID,
                                    UpdatedBy = AuthenticationHelper.UserID,
                                    UpdatedOn = DateTime.Now,
                                };

                                if (!string.IsNullOrEmpty(tbxLicenseNo.Text))
                                {
                                    licenseRecord.LicenseNo = Convert.ToString(tbxLicenseNo.Text);
                                    newLic_tbl_LicenseInstance_Log.LicenseNo = Convert.ToString(tbxLicenseNo.Text.Trim());
                                }

                                if (!string.IsNullOrEmpty(tbxLicenseTitle.Text))
                                {
                                    licenseRecord.LicenseTitle = Convert.ToString(tbxLicenseTitle.Text);
                                    newLic_tbl_LicenseInstance_Log.LicenseTitle = Convert.ToString(tbxLicenseTitle.Text.Trim());
                                }
                                if (!string.IsNullOrEmpty(tbxStartDate.Text))
                                {
                                    licenseRecord.StartDate = DateTimeExtensions.GetDate(tbxStartDate.Text);
                                    newLic_tbl_LicenseInstance_Log.StartDate = DateTimeExtensions.GetDate(tbxStartDate.Text);
                                }

                                if (!string.IsNullOrEmpty(tbxEndDate.Text))
                                {
                                    licenseRecord.EndDate = DateTimeExtensions.GetDate(tbxEndDate.Text);
                                    newLic_tbl_LicenseInstance_Log.EndDate = DateTimeExtensions.GetDate(tbxEndDate.Text);
                                }

                                if (!string.IsNullOrEmpty(tbxApplicationdays.Text))
                                {
                                    licenseRecord.RemindBeforeNoOfDays = Convert.ToInt32(tbxApplicationdays.Text);
                                    newLic_tbl_LicenseInstance_Log.RemindBeforeNoOfDays = Convert.ToInt32(tbxApplicationdays.Text);
                                }
                                else
                                {
                                    licenseRecord.RemindBeforeNoOfDays = 0;
                                    newLic_tbl_LicenseInstance_Log.RemindBeforeNoOfDays = 0;
                                }
                                bool IsStatutory = false;
                                newLic_tbl_LicenseInstance_Log.IsStatutory = IsStatutory;

                                if (!string.IsNullOrEmpty(ddlDepartment.SelectedValue))
                                {
                                    licenseRecord.DepartmentID = Convert.ToInt32(ddlDepartment.SelectedValue);
                                }

                                licenseRecord.ID = LicenseID;
                                //bool existLicNo = LicenseMgmt.ExistsLicenseNo(Convert.ToInt32(customerID), licenseRecord.LicenseNo);
                                //if (!existLicNo)
                                //{
                                LICsaveSuccess = LicenseMgmt.UpdateInternalLicense(licenseRecord);
                                newLic_tbl_LicenseInstance_Log.LicenseID = LicenseID;
                                //LicenseMgmt.UpdateInternalLicenseLog(newLic_tbl_LicenseInstance_Log);
                                InternalLicenseMgmt.CreateInternalLicenseLog(newLic_tbl_LicenseInstance_Log);

                                //if (((DateTimeExtensions.GetDate(tbxStartDate.Text)) != (DateTimeExtensions.GetDate(ViewState["IStartDate"].ToString()))) ||
                                //   ((DateTimeExtensions.GetDate(tbxEndDate.Text)) != (DateTimeExtensions.GetDate(ViewState["IEndDate"].ToString())))
                                //   || query.StatusID == 5)
                                //{

                                    if (query.StatusID == 2)  // if Active
                                    {
                                        Lic_tbl_InternalLicenseStatusTransaction LicenseStatusTransaction = new Lic_tbl_InternalLicenseStatusTransaction
                                        {
                                            LicenseID = licenseID,
                                            CustomerID = customerID,
                                            //StatusID = 2, //License Status Active
                                            StatusChangeOn = DateTime.Now,
                                            IsActive = true,
                                            CreatedBy = AuthenticationHelper.UserID,
                                            UpdatedBy = AuthenticationHelper.UserID,
                                            CreatedOn = DateTime.Now,
                                            UpdatedOn = DateTime.Now,
                                            Remark = "License Details Updated By User Externaly",
                                            IsAutoUpdate = false,
                                            InternalComplianceScheduleOnID = compliancescheduleonid,

                                        };
                                        if (DateTimeExtensions.GetDate(Convert.ToString(tbxEndDate.Text)).Date <= DateTime.Today.Date)
                                        {
                                            LicenseStatusTransaction.StatusID = 3;        //Expired
                                        }
                                        else if (DateTimeExtensions.GetDate(Convert.ToString(tbxEndDate.Text)).Date <= DateTime.Today.Date.AddDays(30))
                                        {
                                            LicenseStatusTransaction.StatusID = 4;            //Expiring
                                        }
                                        else
                                            LicenseStatusTransaction.StatusID = 2;              //Active

                                        entities.Lic_tbl_InternalLicenseStatusTransaction.Add(LicenseStatusTransaction);
                                        entities.SaveChanges();

                                        DateTime Applicationdate = Convert.ToDateTime(licenseRecord.EndDate);
                                        Applicationdate = Applicationdate.AddDays(-Convert.ToDouble(licenseRecord.RemindBeforeNoOfDays));

                                        InternalComplianceScheduledOn complianceScheduleOn = new InternalComplianceScheduledOn()
                                        {
                                            ScheduledOn = Applicationdate.Date,
                                            InternalComplianceInstanceID = complianceinstanceid,
                                            IsActive = true,
                                            IsUpcomingNotDeleted = true,
                                        };
                                        complianceScheduleOn.ID = compliancescheduleonid;

                                        LICsaveSuccess = LicenseMgmt.UpdateInternalComplianceScheduleOn(complianceScheduleOn);

                                    }
                                    else if (query.StatusID == 3) //if Expired
                                    {
                                        Lic_tbl_InternalLicenseStatusTransaction LicenseStatusTransaction = new Lic_tbl_InternalLicenseStatusTransaction
                                        {
                                            LicenseID = licenseID,
                                            CustomerID = customerID,
                                            //StatusID = 2, //License Status Active
                                            StatusChangeOn = DateTime.Now,
                                            IsActive = true,
                                            CreatedBy = AuthenticationHelper.UserID,
                                            UpdatedBy = AuthenticationHelper.UserID,
                                            CreatedOn = DateTime.Now,
                                            UpdatedOn = DateTime.Now,
                                            Remark = "License Details Updated By User Externaly",
                                            IsAutoUpdate = false,
                                            InternalComplianceScheduleOnID = compliancescheduleonid,
                                        };
                                        if (DateTimeExtensions.GetDate(Convert.ToString(tbxEndDate.Text)).Date <= DateTime.Today.Date)
                                        {
                                            LicenseStatusTransaction.StatusID = 3;        //Expired
                                        }
                                        else if (DateTimeExtensions.GetDate(Convert.ToString(tbxEndDate.Text)).Date <= DateTime.Today.Date.AddDays(30))
                                        {
                                            LicenseStatusTransaction.StatusID = 4;            //Expiring
                                        }
                                        else
                                            LicenseStatusTransaction.StatusID = 2;

                                        entities.Lic_tbl_InternalLicenseStatusTransaction.Add(LicenseStatusTransaction);
                                        entities.SaveChanges();

                                        DateTime Applicationdate = Convert.ToDateTime(licenseRecord.EndDate);
                                        Applicationdate = Applicationdate.AddDays(-Convert.ToDouble(licenseRecord.RemindBeforeNoOfDays));
                                        InternalComplianceScheduledOn complianceScheduleOn = new InternalComplianceScheduledOn()
                                        {
                                            ScheduledOn = Applicationdate.Date,
                                            InternalComplianceInstanceID = complianceinstanceid,
                                            IsActive = true,
                                            IsUpcomingNotDeleted = true,
                                        };
                                        complianceScheduleOn.ID = compliancescheduleonid;

                                        LICsaveSuccess = LicenseMgmt.UpdateInternalComplianceScheduleOn(complianceScheduleOn);

                                    }
                                    else if (query.StatusID == 4) //if Expiring
                                    {
                                        Lic_tbl_InternalLicenseStatusTransaction LicenseStatusTransaction = new Lic_tbl_InternalLicenseStatusTransaction
                                        {
                                            LicenseID = licenseID,
                                            CustomerID = customerID,
                                            //StatusID = 2, //License Status Active
                                            StatusChangeOn = DateTime.Now,
                                            IsActive = true,
                                            CreatedBy = AuthenticationHelper.UserID,
                                            UpdatedBy = AuthenticationHelper.UserID,
                                            CreatedOn = DateTime.Now,
                                            UpdatedOn = DateTime.Now,
                                            Remark = "License Details Updated By User Externaly",
                                            IsAutoUpdate = false,
                                            InternalComplianceScheduleOnID = compliancescheduleonid,
                                        };
                                        if (DateTimeExtensions.GetDate(Convert.ToString(tbxEndDate.Text)).Date <= DateTime.Today.Date)
                                        {
                                            LicenseStatusTransaction.StatusID = 3;        //Expired
                                        }
                                        else if (DateTimeExtensions.GetDate(Convert.ToString(tbxEndDate.Text)).Date <= DateTime.Today.Date.AddDays(30))
                                        {
                                            LicenseStatusTransaction.StatusID = 4;            //Expiring
                                        }
                                        else
                                            LicenseStatusTransaction.StatusID = 2;

                                        entities.Lic_tbl_InternalLicenseStatusTransaction.Add(LicenseStatusTransaction);
                                        entities.SaveChanges();

                                        DateTime Applicationdate = Convert.ToDateTime(licenseRecord.EndDate);
                                        Applicationdate = Applicationdate.AddDays(-Convert.ToDouble(licenseRecord.RemindBeforeNoOfDays));
                                        InternalComplianceScheduledOn complianceScheduleOn = new InternalComplianceScheduledOn()
                                        {
                                            ScheduledOn = Applicationdate.Date,
                                            InternalComplianceInstanceID = complianceinstanceid,
                                            IsActive = true,
                                            IsUpcomingNotDeleted = true,
                                        };
                                        complianceScheduleOn.ID = compliancescheduleonid;

                                        LICsaveSuccess = LicenseMgmt.UpdateInternalComplianceScheduleOn(complianceScheduleOn);
                                    }
                                    else if (query.StatusID == 5) //if Applied
                                    {

                                         
                                        long complianceScheduleOnIdNew = 0;
                                        DateTime Applicationdate = Convert.ToDateTime(licenseRecord.EndDate);
                                        Applicationdate = Applicationdate.AddDays(-Convert.ToDouble(licenseRecord.RemindBeforeNoOfDays));
                                        InternalComplianceScheduledOn complianceScheduleOn = new InternalComplianceScheduledOn()
                                        {
                                            //ScheduleOn = licenseRecord.EndDate.Value.Date,
                                            ScheduledOn = Applicationdate.Date,
                                            InternalComplianceInstanceID = complianceinstanceid,
                                            IsActive = true,
                                            IsUpcomingNotDeleted = true,
                                        };
                                        complianceScheduleOnIdNew = InternalLicenseMgmt.CreateInternalComplianceScheduleOn(complianceScheduleOn);

                                        Lic_tbl_InternalLicenseStatusTransaction LicenseStatusTransaction = new Lic_tbl_InternalLicenseStatusTransaction
                                        {
                                            LicenseID = licenseID,
                                            CustomerID = customerID,
                                            //StatusID = 2, //License Status Active
                                            StatusChangeOn = DateTime.Now,
                                            IsActive = true,
                                            CreatedBy = AuthenticationHelper.UserID,
                                            UpdatedBy = AuthenticationHelper.UserID,
                                            CreatedOn = DateTime.Now,
                                            UpdatedOn = DateTime.Now,
                                            Remark = "License Details Added By User Externaly",
                                            IsAutoUpdate = false,
                                            InternalComplianceScheduleOnID = complianceScheduleOnIdNew,
                                        };

                                        if (DateTimeExtensions.GetDate(Convert.ToString(tbxEndDate.Text)).Date <= DateTime.Today.Date)
                                        {
                                            LicenseStatusTransaction.StatusID = 3;        //Expired
                                        }
                                        else if (DateTimeExtensions.GetDate(Convert.ToString(tbxEndDate.Text)).Date <= DateTime.Today.Date.AddDays(30))
                                        {
                                            LicenseStatusTransaction.StatusID = 4;            //Expiring
                                        }
                                        else
                                            LicenseStatusTransaction.StatusID = 2;

                                        entities.Lic_tbl_InternalLicenseStatusTransaction.Add(LicenseStatusTransaction);
                                        entities.SaveChanges();

                                        if (complianceScheduleOnIdNew > 0)
                                        {
                                            Lic_tbl_LicenseInternalComplianceInstanceScheduleOnMapping lictbllicensecomplianceschedulonmapping = new Lic_tbl_LicenseInternalComplianceInstanceScheduleOnMapping()
                                            {
                                                LicenseID = licenseRecord.ID,
                                                ComplianceInstanceID = complianceinstanceid,
                                                ComplianceScheduleOnID = complianceScheduleOnIdNew,
                                                IsActivation = "Application",
                                                IsStatutoryORInternal = "S"
                                            };
                                            InternalLicenseMgmt.CreateLicenseLicenseInternalComplianceInstanceScheduleOnMapping(lictbllicensecomplianceschedulonmapping);

                                            InternalComplianceTransaction transaction = new InternalComplianceTransaction()
                                            {
                                                InternalComplianceInstanceID = complianceinstanceid,
                                                StatusId = 1,
                                                Remarks = "New compliance assigned.",
                                                Dated = DateTime.Now,
                                                CreatedBy = Convert.ToInt64(AuthenticationHelper.UserID),
                                                CreatedByText = Convert.ToString(AuthenticationHelper.User),
                                                InternalComplianceScheduledOnID = complianceScheduleOnIdNew,
                                            };

                                            entities.InternalComplianceTransactions.Add(transaction);
                                            entities.SaveChanges();
                                        }

                                    }
                                    else if (query.StatusID == 6) //Appleid But Pending For Renewal
                                    {
                                         


                                        DateTime Applicationdate = Convert.ToDateTime(licenseRecord.EndDate);
                                        Applicationdate = Applicationdate.AddDays(-Convert.ToDouble(licenseRecord.RemindBeforeNoOfDays));
                                        InternalComplianceScheduledOn complianceScheduleOn = new InternalComplianceScheduledOn()
                                        {
                                            ScheduledOn = Applicationdate.Date,
                                            InternalComplianceInstanceID = complianceinstanceid,
                                            IsActive = true,
                                            IsUpcomingNotDeleted = true,
                                        };
                                        complianceScheduleOn.ID = compliancescheduleonid;

                                        LICsaveSuccess = LicenseMgmt.UpdateInternalComplianceScheduleOn(complianceScheduleOn);
                                    }
                                    else if (query.StatusID == 7) //Rejected
                                    {


                                        DateTime Applicationdate = Convert.ToDateTime(licenseRecord.EndDate);
                                        Applicationdate = Applicationdate.AddDays(-Convert.ToDouble(licenseRecord.RemindBeforeNoOfDays));
                                        InternalComplianceScheduledOn complianceScheduleOn = new InternalComplianceScheduledOn()
                                        {
                                            ScheduledOn = Applicationdate.Date,
                                            InternalComplianceInstanceID = complianceinstanceid,
                                            IsActive = true,
                                            IsUpcomingNotDeleted = true,
                                        };
                                        complianceScheduleOn.ID = compliancescheduleonid;

                                        LICsaveSuccess = LicenseMgmt.UpdateInternalComplianceScheduleOn(complianceScheduleOn);

                                    }
                               // }
                            }
                            catch (Exception ex)
                            {
                                LicenseMgmt.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                            }
                        }

                        #endregion

                        #region Save Document Against Current ComplianceScheduleonID
                        long complianceInstanceId = 0;
                        string directoryPath = null;
                        long complianceScheduleOnId = 0;
                        if (!string.IsNullOrEmpty(Convert.ToString(ViewState["complianceinstanceid"])))
                        {
                            complianceInstanceId = Convert.ToInt32(ViewState["complianceinstanceid"]);
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(ViewState["compliancescheduleonid"])))
                        {
                            complianceScheduleOnId = Convert.ToInt32(ViewState["compliancescheduleonid"]);
                        }
                        if (complianceScheduleOnId > 0)
                        {
                            List<KeyValuePair<string, Byte[]>> Filelist = new List<KeyValuePair<string, Byte[]>>();
                            List<InternalFileData> files = new List<InternalFileData>();
                            List<KeyValuePair<string, int>> list = new List<KeyValuePair<string, int>>();
                            HttpFileCollection fileCollection = Request.Files;
                            bool blankfileCount = true;
                            if (fileCollection.Count > 0)
                            {
                                var AWSData = AmazonS3.GetAWSStorageDetail(customerID);
                                if (AWSData != null)
                                {
                                    #region AWS Storage
                                    var InstanceData = DocumentManagement.GetInternalComplianceInstanceData(Convert.ToInt32(complianceInstanceId));

                                    string version = null;
                                    version = DocumentManagement.GetDocumnetVersionInternal(Convert.ToInt32(complianceScheduleOnId));
                                    directoryPath = "InternalAvacomDocuments\\" + customerID + "\\" + InstanceData.CustomerBranchID + "\\" + InstanceData.ID.ToString() + "\\" + complianceScheduleOnId + "\\" + version;

                                    IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.APSouth1);
                                    S3DirectoryInfo di = new S3DirectoryInfo(client, AWSData.BucketName, directoryPath);
                                    if (!di.Exists)
                                    {
                                        di.Create();
                                    }

                                    for (int i = 0; i < fileCollection.Count; i++)
                                    {
                                        HttpPostedFile uploadfile = fileCollection[i];
                                        //HttpPostedFile uploadfile = LicenseFileUpload[j];
                                        String fileName = "";
                                        fileName = "InternalLicenseDoc_" + uploadfile.FileName;
                                        list.Add(new KeyValuePair<string, int>(fileName, 1));
                                        Guid fileKey = Guid.NewGuid();
                                        string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(uploadfile.FileName));
                                        Stream fs = uploadfile.InputStream;
                                        BinaryReader br = new BinaryReader(fs);
                                        Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                                        Filelist.Add(new KeyValuePair<string, Byte[]>(finalPath, bytes));

                                        if (uploadfile.ContentLength > 0)
                                        {
                                            string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                            string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;

                                            string TdirectoryPath = "~/TempDocuments/AWS/" + User + "/" + uploadfile.FileName;
                                            string directoryPath1 = "~/TempDocuments/AWS/" + User;
                                            string TFilePath = Server.MapPath(TdirectoryPath);
                                            if (File.Exists(TFilePath))
                                                File.Delete(TFilePath);
                                            if (!Directory.Exists(directoryPath1))
                                            {
                                                Directory.CreateDirectory(Server.MapPath(directoryPath1));
                                            }

                                            FileStream objFileStrm = File.Create(TFilePath);
                                            objFileStrm.Close();
                                            File.WriteAllBytes(TFilePath, bytes);

                                            string AWSpath = "";
                                            AWSpath = directoryPath + "\\InternalLicenseDoc_" + uploadfile.FileName;

                                            FileInfo localFile = new FileInfo(TFilePath);
                                            S3FileInfo s3File = new S3FileInfo(client, AWSData.BucketName, AWSpath);
                                            if (!s3File.Exists)
                                            {
                                                using (var s3Stream = s3File.Create()) // <-- create file in S3  
                                                {
                                                    localFile.OpenRead().CopyTo(s3Stream); // <-- copy the content to S3  
                                                }
                                            }

                                            string filepathvalue = string.Empty;
                                            string vale = directoryPath.Replace(ConfigurationManager.AppSettings["DriveUrl"], "~");
                                            filepathvalue = vale.Replace(@"\", "/");

                                            InternalFileData file = new InternalFileData()
                                            {
                                                Name = fileName,
                                                FilePath = filepathvalue,
                                                FileKey = fileKey.ToString(),
                                                Version = version,
                                                VersionDate = DateTime.UtcNow,
                                                FileSize = uploadfile.ContentLength,
                                            };
                                            files.Add(file);
                                        }
                                        else
                                        {
                                            if (!string.IsNullOrEmpty(uploadfile.FileName))
                                                blankfileCount = false;
                                        }
                                    }
                                    #endregion
                                }
                                else
                                {
                                    #region Normal Storage
                                    var InstanceData = DocumentManagement.GetInternalComplianceInstanceData(Convert.ToInt32(complianceInstanceId));
                                  
                                    string version = null;
                                    version = DocumentManagement.GetDocumnetVersionInternal(Convert.ToInt32(complianceScheduleOnId));
                                    if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                    {
                                        directoryPath = ConfigurationManager.AppSettings["DriveUrl"] + "\\InternalAvacomDocuments\\" + customerID + "\\" + InstanceData.CustomerBranchID + "\\" + InstanceData.ID.ToString() + "\\" + complianceScheduleOnId + "\\" + version;
                                    }
                                    else
                                    {
                                        directoryPath = Server.MapPath("~/InternalAvacomDocuments/" + customerID + "/" + InstanceData.CustomerBranchID + "/" + InstanceData.ID.ToString() + "/" + complianceScheduleOnId + "/" + version);
                                    }
                                    DocumentManagement.CreateDirectory(directoryPath);
                                    for (int i = 0; i < fileCollection.Count; i++)
                                    {
                                        HttpPostedFile uploadfile = fileCollection[i];
                                        string[] keys = fileCollection.Keys[i].Split('$');
                                        String fileName = "";
                                        if (keys[keys.Count() - 1].Equals("fuSampleFile"))
                                        {
                                            fileName = "InternalLicenseDoc_" + uploadfile.FileName;
                                            list.Add(new KeyValuePair<string, int>(fileName, 1));
                                        }
                                        Guid fileKey = Guid.NewGuid();
                                        string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(uploadfile.FileName));

                                        Stream fs = uploadfile.InputStream;
                                        BinaryReader br = new BinaryReader(fs);
                                        Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                                        Filelist.Add(new KeyValuePair<string, Byte[]>(finalPath, bytes));

                                        if (uploadfile.ContentLength > 0)
                                        {
                                            //Change by rahul on 21 JAN 2017 for Azure Drive or Local Drive
                                            string filepathvalue = string.Empty;
                                            if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                            {
                                                string vale = directoryPath.Replace(ConfigurationManager.AppSettings["DriveUrl"], "~");
                                                filepathvalue = vale.Replace(@"\", "/");
                                            }
                                            else
                                            {
                                                filepathvalue = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                            }
                                            InternalFileData file = new InternalFileData()
                                            {
                                                Name = fileName,
                                                FilePath = filepathvalue,
                                                FileKey = fileKey.ToString(),
                                                Version = version,
                                                VersionDate = DateTime.UtcNow,
                                            };
                                            files.Add(file);
                                        }
                                        else
                                        {
                                            if (!string.IsNullOrEmpty(uploadfile.FileName))
                                                blankfileCount = false;
                                        }
                                    }
                                    #endregion
                                }
                            }
                            else
                            {
                                LICDocsaveSuccess = true;
                            }
                            bool flag = false;
                            if (blankfileCount)
                            {
                                var tranid = GetInternalTransactionID(complianceScheduleOnId);
                                flag = CreateInternalTransaction(complianceScheduleOnId, tranid, files, list, Filelist, directoryPath, complianceScheduleOnId, customerID);
                                if (flag == true)
                                {
                                    LICDocsaveSuccess = true;
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "missingAlert", "alert('Please do not upload virus file or blank files.')", true);
                                cvLicPopUp.IsValid = false;
                                cvLicPopUp.ErrorMessage = "Please do not upload virus file or blank files.";
                            }
                        }//complianceScheduleOnId
                        #endregion

                        if (LICsaveSuccess == true && LICDocsaveSuccess == true)
                        {
                            //clearLicenseControls();
                            cvLicPopUp.IsValid = false;
                            cvLicPopUp.ErrorMessage = "License Details Updated Sucessfully";
                            VSLicPopup.CssClass = "alert alert-success";

                        }
                    }
                }
            }// Scheduleon checklicexist
            else
            {
                cvLicPopUp.IsValid = false;
                //cvLicPopUp.ErrorMessage = "Something went wrong.";
                showErrorMessages(lstErrorMsg, cvLicPopUp);
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "scrollUpPageScript", "scrollUpPage();", true);
            }
        }
        public static bool CreateTransaction(long complinaceScheduleonID, long ComplianceTransactionID, List<FileData> files, List<KeyValuePair<string, int>> list, List<KeyValuePair<string, Byte[]>> filesList, string DirectoryPath, long ScheduleOnID, int CustId)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                    {
                        try
                        {
                            var AWSData = AmazonS3.GetAWSStorageDetail(CustId);
                            if (AWSData != null)
                            {
                                AmazonS3.SaveDocFilesAWSStorage(filesList, DirectoryPath, ScheduleOnID, AWSData.BucketName, AWSData.AccessKeyID, AWSData.SecretKeyID);
                            }
                            else
                            {
                                DocumentManagement.Statutory_SaveDocFiles(filesList);
                            }
                            if (files != null)
                            {
                                foreach (FileData fl in files)
                                {
                                    fl.IsDeleted = false;
                                    fl.EnType = "A";
                                    entities.FileDatas.Add(fl);
                                    entities.SaveChanges();

                                    FileDataMapping fileMapping = new FileDataMapping();
                                    fileMapping.TransactionID = ComplianceTransactionID;
                                    fileMapping.FileID = fl.ID;
                                    fileMapping.ScheduledOnID = complinaceScheduleonID;
                                    if (list != null)
                                    {
                                        fileMapping.FileType = Convert.ToInt16(list.Where(ent => ent.Key.Equals(fl.Name)).FirstOrDefault().Value);
                                    }
                                    entities.FileDataMappings.Add(fileMapping);
                                }
                            }
                            entities.SaveChanges();
                            dbtransaction.Commit();
                            return true;
                        }
                        catch (Exception)
                        {
                            dbtransaction.Rollback();
                            if (filesList != null)
                            {
                                foreach (var dfile in filesList)
                                {
                                    if (System.IO.File.Exists(dfile.Key))
                                    {
                                        System.IO.File.Delete(dfile.Key);
                                    }
                                }
                            }
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage> ReminderUserList = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage>();
                LogMessage msg = new LogMessage();
                msg.ClassName = "ComplianceManagement";
                msg.FunctionName = "CreateTransaction";
                msg.CreatedOn = DateTime.Now;
                msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                msg.StackTrace = ex.StackTrace;
                msg.LogLevel = (byte)com.VirtuosoITech.Logger.Loglevel.Error;
                ReminderUserList.Add(msg);
                InsertLogToDatabase(ReminderUserList);

                List<string> TO = new List<string>();
                TO.Add("sachin@avantis.info");
                TO.Add("narendra@avantis.info");
                TO.Add("rahul@avantis.co.in");
                EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(TO), new List<String>(TO),
                    null, "Error Occured as CreateTransaction Function", "CreateTransaction");
                return false;
            }
        }
        public static bool CreateInternalTransaction(long complinaceScheduleonID, long ComplianceTransactionID, List<InternalFileData> files, List<KeyValuePair<string, int>> list, List<KeyValuePair<string, Byte[]>> filesList, string DirectoryPath, long ScheduleOnID, int CustId)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                    {
                        try
                        {
                            var AWSData = AmazonS3.GetAWSStorageDetail(CustId);
                            if (AWSData != null)
                            {
                                AmazonS3.SaveDocFilesAWSStorageInternal(filesList, DirectoryPath, ScheduleOnID, AWSData.BucketName, AWSData.AccessKeyID, AWSData.SecretKeyID);
                            }
                            else
                            {
                                DocumentManagement.Internal_SaveDocFiles(filesList);
                            }
                            if (files != null)
                            {
                                foreach (InternalFileData fl in files)
                                {
                                    fl.IsDeleted = false;
                                    fl.EnType = "A";
                                    entities.InternalFileDatas.Add(fl);
                                    entities.SaveChanges();

                                    InternalFileDataMapping fileMapping = new InternalFileDataMapping();
                                    fileMapping.TransactionID = ComplianceTransactionID;
                                    fileMapping.FileID = fl.ID;
                                    fileMapping.InternalScheduledOnID = complinaceScheduleonID;
                                    if (list != null)
                                    {
                                        fileMapping.FileType = Convert.ToInt16(list.Where(ent => ent.Key.Equals(fl.Name)).FirstOrDefault().Value);
                                    }
                                    entities.InternalFileDataMappings.Add(fileMapping);
                                }
                            }
                            entities.SaveChanges();
                            dbtransaction.Commit();
                            return true;
                        }
                        catch (Exception)
                        {
                            dbtransaction.Rollback();
                            if (filesList != null)
                            {
                                foreach (var dfile in filesList)
                                {
                                    if (System.IO.File.Exists(dfile.Key))
                                    {
                                        System.IO.File.Delete(dfile.Key);
                                    }
                                }
                            }
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage> ReminderUserList = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage>();
                LogMessage msg = new LogMessage();
                msg.ClassName = "ComplianceManagement";
                msg.FunctionName = "CreateTransaction";
                msg.CreatedOn = DateTime.Now;
                msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                msg.StackTrace = ex.StackTrace;
                msg.LogLevel = (byte)com.VirtuosoITech.Logger.Loglevel.Error;
                ReminderUserList.Add(msg);
                InsertLogToDatabase(ReminderUserList);

                List<string> TO = new List<string>();
                TO.Add("sachin@avantis.info");
                TO.Add("narendra@avantis.info");
                TO.Add("rahul@avantis.co.in");
                EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(TO), new List<String>(TO),
                    null, "Error Occured as CreateTransaction Function", "CreateTransaction");
                return false;
            }
        }
        public static void InsertLogToDatabase(List<LogMessage> objEscalation)
        {

            using (com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities entities = new com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities())
            {
                objEscalation.ForEach(entry =>
                {
                    entities.LogMessages.Add(entry);
                    entities.SaveChanges();

                });
            }

        }
        public static long GetTransactionID(long scheduledOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                long tranid = 0;
                tranid = (from row in entities.ComplianceTransactions
                          where row.ComplianceScheduleOnID == scheduledOnID
                          select row.ID).OrderByDescending(a => a).FirstOrDefault();
                if (tranid > 0)
                    return tranid;
                else
                    return 0;
            }
        }
        public static long GetInternalTransactionID(long scheduledOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                long tranid = 0;
                tranid = (from row in entities.InternalComplianceTransactions
                          where row.InternalComplianceScheduledOnID == scheduledOnID
                          select row.ID).OrderByDescending(a => a).FirstOrDefault();
                if (tranid > 0)
                    return tranid;
                else
                    return 0;
            }
        }
        #region License Documents
        public void DownloadFile(int fileId)
        {
            try
            {
                if (isstatutoryinternal == "S")
                {
                    #region Statutory
                    var file = Business.ComplianceManagement.GetFile(fileId);
                    if (file.FilePath != null)
                    {
                        string filePath = string.Empty;
                        //Change by rahul on 21 JAN 2017 for Azure Drive or Local Drive
                        if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                        {
                            string pathvalue = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.Name));
                            filePath = pathvalue.Replace("~", ConfigurationManager.AppSettings["DriveUrl"]);
                        }
                        else
                        {
                            filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
                        }
                        if (filePath != null && File.Exists(filePath))
                        {
                            Response.Buffer = true;
                            Response.Clear();
                            Response.ClearContent();
                            Response.ContentType = "application/octet-stream";
                            Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                            if (file.EnType == "M")
                            {
                                Response.BinaryWrite(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                            }
                            else
                            {
                                Response.BinaryWrite(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                            }
                            Response.Flush(); // send it to the client to download
                            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                        }
                    }
                    #endregion
                }
                else if (isstatutoryinternal == "I")
                {
                    #region Internal
                    var file = Business.InternalComplianceManagement.GetFile(fileId);
                    if (file.FilePath != null)
                    {
                        string filePath = string.Empty;
                        //Change by rahul on 21 JAN 2017 for Azure Drive or Local Drive
                        if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                        {
                            string pathvalue = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.Name));
                            filePath = pathvalue.Replace("~", ConfigurationManager.AppSettings["DriveUrl"]);
                        }
                        else
                        {
                            filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
                        }
                        if (filePath != null && File.Exists(filePath))
                        {
                            Response.Buffer = true;
                            Response.Clear();
                            Response.ClearContent();
                            Response.ContentType = "application/octet-stream";
                            Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                            if (file.EnType == "M")
                            {
                                Response.BinaryWrite(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                            }
                            else
                            {
                                Response.BinaryWrite(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                            }
                            Response.Flush(); // send it to the client to download
                            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                        }
                    }
                    #endregion
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        protected void rptLicenseVersion_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnLicenseVersionDoc");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);

                LinkButton lblLicenseDocumentVersion = (LinkButton)e.Item.FindControl("lblLicenseDocumentVersion");
                scriptManager.RegisterAsyncPostBackControl(lblLicenseDocumentVersion);
            }
        }
        protected void rptLicenseVersion_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {

                if (!String.IsNullOrEmpty(Request.QueryString["isstatutoryinternal"]))
                {
                    isstatutoryinternal = Request.QueryString["isstatutoryinternal"].ToString().Trim();
                }
                if (isstatutoryinternal == "S")
                {
                    isstatutoryinternal = "S";
                }
                else if (isstatutoryinternal == "I")
                {
                    isstatutoryinternal = "I";
                }
                if (isstatutoryinternal == "S")
                {
                    #region Statutory
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    List<SP_GetLicenseDocument_Result> ComplianceFileData = new List<SP_GetLicenseDocument_Result>();
                    List<SP_GetLicenseDocument_Result> ComplianceDocument = new List<SP_GetLicenseDocument_Result>();
                    ComplianceDocument = LicenseDocumentManagement.GetFileData(Convert.ToInt32(commandArgs[0]), -1).ToList();

                    if (commandArgs[2].Equals("1.0"))
                    {
                        ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == commandArgs[2]).ToList();
                        if (ComplianceFileData.Count <= 0)
                        {
                            ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == null).ToList();
                        }
                    }
                    else
                    {
                        ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == commandArgs[2]).ToList();
                    }

                    if (e.CommandName.Equals("version"))
                    {
                        if (e.CommandName.Equals("version"))
                        {
                            rptLicenseDocumnets.DataSource = ComplianceFileData.Where(entry => entry.FileType == 1).ToList();
                            rptLicenseDocumnets.DataBind();

                            rptLicenseWorkingFiles.DataSource = ComplianceFileData.Where(entry => entry.FileType == 2).ToList();
                            rptLicenseWorkingFiles.DataBind();

                            var documentVersionData = ComplianceDocument.Select(x => new
                            {
                                ID = x.ID,
                                ScheduledOnID = x.ScheduledOnID,
                                LicenseID = x.LicenseID,
                                Version = string.IsNullOrEmpty(x.Version) ? "1.0" : x.Version,
                                VersionDate = x.VersionDate,
                                VersionComment = string.IsNullOrEmpty(x.VersionComment) ? "" : x.VersionComment
                            }).GroupBy(entry => entry.Version).Select(entry => entry.FirstOrDefault()).ToList();

                            rptLicenseVersion.DataSource = documentVersionData;
                            rptLicenseVersion.DataBind();

                            upLicense.Update();                    
                        }
                    }
                    else if (e.CommandName.Equals("Download"))
                    {
                        using (ZipFile ComplianceZip = new ZipFile())
                        {
                            var LicenseData = LicenseDocumentManagement.GetLicense(Convert.ToInt32(commandArgs[0]));

                            ComplianceZip.AddDirectoryByName(LicenseData.LicenseNo);

                            if (ComplianceFileData.Count > 0)
                            {
                                int i = 0;
                                foreach (var file in ComplianceFileData)
                                {
                                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        string ext = Path.GetExtension(file.FileName);
                                        string[] filename = file.FileName.Split('.');
                                        //string str = filename[0] + i + "." + filename[1];
                                        string str = filename[0] + i + "." + ext;
                                        if (file.EnType == "M")
                                        {
                                            ComplianceZip.AddEntry(LicenseData.LicenseNo + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        else
                                        {
                                            ComplianceZip.AddEntry(LicenseData.LicenseNo + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        i++;
                                    }
                                }
                            }
                            var zipMs = new MemoryStream();
                            ComplianceZip.Save(zipMs);
                            zipMs.Position = 0;
                            byte[] data = zipMs.ToArray();
                            Response.Buffer = true;
                            Response.ClearContent();
                            Response.ClearHeaders();
                            Response.Clear();
                            Response.ContentType = "application/zip";
                            Response.AddHeader("content-disposition", "attachment; filename=LicenseDocuments.zip");
                            Response.BinaryWrite(data);
                            Response.Flush();
                            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                        }
                    }
                    #endregion
                }
                else if (isstatutoryinternal == "I")
                {
                    #region Internal
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    List<SP_GetInternalLicenseDocument_Result> ComplianceFileData = new List<SP_GetInternalLicenseDocument_Result>();
                    List<SP_GetInternalLicenseDocument_Result> ComplianceDocument = new List<SP_GetInternalLicenseDocument_Result>();
                    ComplianceDocument = InternalLicenseMgmt.GetInternalFileData(Convert.ToInt32(commandArgs[0]), -1).ToList();

                    if (commandArgs[2].Equals("1.0"))
                    {
                        ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == commandArgs[2]).ToList();
                        if (ComplianceFileData.Count <= 0)
                        {
                            ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == null).ToList();
                        }
                    }
                    else
                    {
                        ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == commandArgs[2]).ToList();
                    }

                    if (e.CommandName.Equals("version"))
                    {
                        if (e.CommandName.Equals("version"))
                        {
                            rptLicenseDocumnets.DataSource = ComplianceFileData.Where(entry => entry.FileType == 1).ToList();
                            rptLicenseDocumnets.DataBind();

                            rptLicenseWorkingFiles.DataSource = ComplianceFileData.Where(entry => entry.FileType == 2).ToList();
                            rptLicenseWorkingFiles.DataBind();

                            var documentVersionData = ComplianceDocument.Select(x => new
                            {
                                ID = x.ID,
                                ScheduledOnID = x.ScheduledOnID,
                                LicenseID = x.LicenseID,
                                Version = string.IsNullOrEmpty(x.Version) ? "1.0" : x.Version,
                                VersionDate = x.VersionDate,
                                VersionComment = string.IsNullOrEmpty(x.VersionComment) ? "" : x.VersionComment
                            }).GroupBy(entry => entry.Version).Select(entry => entry.FirstOrDefault()).ToList();

                            rptLicenseVersion.DataSource = documentVersionData;
                            rptLicenseVersion.DataBind();

                            upLicense.Update();                    
                        }
                    }
                    else if (e.CommandName.Equals("Download"))
                    {
                        using (ZipFile ComplianceZip = new ZipFile())
                        {
                            var LicenseData = InternalLicenseMgmt.GetLicense(Convert.ToInt32(commandArgs[0]));

                            ComplianceZip.AddDirectoryByName(LicenseData.LicenseNo);

                            if (ComplianceFileData.Count > 0)
                            {
                                int i = 0;
                                foreach (var file in ComplianceFileData)
                                {
                                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        string ext = Path.GetExtension(file.FileName);
                                        string[] filename = file.FileName.Split('.');
                                        //string str = filename[0] + i + "." + filename[1];
                                        string str = filename[0] + i + "." + ext;
                                        if (file.EnType == "M")
                                        {
                                            ComplianceZip.AddEntry(LicenseData.LicenseNo + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        else
                                        {
                                            ComplianceZip.AddEntry(LicenseData.LicenseNo + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        i++;
                                    }
                                }
                            }
                            var zipMs = new MemoryStream();
                            ComplianceZip.Save(zipMs);
                            zipMs.Position = 0;
                            byte[] data = zipMs.ToArray();
                            Response.Buffer = true;
                            Response.ClearContent();
                            Response.ClearHeaders();
                            Response.Clear();
                            Response.ContentType = "application/zip";
                            Response.AddHeader("content-disposition", "attachment; filename=LicenseDocuments.zip");
                            Response.BinaryWrite(data);
                            Response.Flush();
                            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                        }
                    }
                    #endregion
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvLicPopUp.IsValid = false;
                cvLicPopUp.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void rptLicenseDocumnets_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnLicenseDocumnets");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);
            }
        }
        protected void rptLicenseDocumnets_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                DownloadFile(Convert.ToInt32(e.CommandArgument));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvLicPopUp.IsValid = false;
                cvLicPopUp.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void rptLicenseWorkingFiles_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnLicenseWorkingFiles");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);
            }
        }
        protected void rptLicenseWorkingFiles_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                DownloadFile(Convert.ToInt32(e.CommandArgument));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        #endregion
    }
}