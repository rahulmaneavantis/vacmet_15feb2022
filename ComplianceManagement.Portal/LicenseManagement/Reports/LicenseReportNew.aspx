﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LicenseManagement.Master" AutoEventWireup="true" CodeBehind="LicenseReportNew.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.LicenseManagement.Reports.LicenseReportNew" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />
    <script type="text/javascript" src="../../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jszip.min.js"></script>

    <style type="text/css">
        .k-multiselect-wrap .k-input {
            display: inherit !important;
        }

        .k-active-filter, .k-state-active, .k-state-active:hover {
            background-color: #EBEBEB;
            border-color: #a6a6ad;
            color: #535b6a;
        }

        .div.k-grid-footer, div.k-grid-header {
            border-top-width: 1px;
            margin-right: 0.3px;
        }

        input[type=checkbox], input[type=radio] {
            margin: 4px 6px 0;
            margin-top: 1px\9;
            line-height: normal;
        }

        .k-grid-footer-wrap, .k-grid-header-wrap {
            position: relative;
            width: 100%;
            overflow: hidden;
            border-style: solid;
            border-width: 0 1px 0 0;
            zoom: 1;
        }

        .k-grid-content {
            min-height: 180px !important;
        }

        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 2.0em;
            border-bottom-width: 1px;
            background-color: white;
            border-width: 0 1px 1px 0px;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
            background-color: white;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: white;
        }

        #grid .k-grid-toolbar {
            background: white;
        }

        .k-pager-wrap > .k-link > .k-icon {
            margin-top: -1px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: white;
        }

        html .k-grid tr.k-alt:hover {
            background: white;
        }

        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            background: white;
            border: none;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
            margin-right: 2px;
        }

        /*label {
          display: inline-block;
           margin-bottom: 0px;
         }*/

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: white;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 0px 1px 0px;
            text-align: center;
        }

        .k-grid-pager {
            margin-top: -1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-i-arrow-60-down, .k-i-arrow-60-left, .k-i-arrow-60-right, .k-i-arrow-60-up {
            cursor: pointer;
            margin-left: 3px;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            border-width: 0px 0px 1px 0px;
            background: #E9EAEA;
            font-weight: bold;
            margin-right: 18px;
            font-family: 'Roboto',sans-serif;
            font-size: 15px;
            color: rgba(0, 0, 0, 0.5);
            height: 20px;
            vertical-align: middle;
        }

        .k-dropdown-wrap.k-state-default {
            background-color: white;
        }

        .k-popup.k-calendar-container, .k-popup.k-list-container {
            background-color: white;
        }

        .k-list > .k-state-focused.k-state-selected, .k-listview > .k-state-focused.k-state-selected, .k-state-focused.k-state-selected, td.k-state-focused.k-state-selected {
            -webkit-box-shadow: inset 0 0 3px 1px rgba(0, 0, 0, 0.5);
            box-shadow: inset 0 0 3px 1px rgba(0, 0, 0, 0.5);
        }

        label.k-label {
            font-family: roboto,sans-serif !important;
            color: #515967;
            font-style: normal;
            font-weight: 400;
            min-width: 362px;
            white-space: pre-wrap;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: flex;
            margin-bottom: 0px;
        }

        .k-state-default > .k-select {
            border-color: #ceced2;
            margin-top: 0px;
        }

        .k-grid-norecords {
            width: 100%;
            height: 100%;
            text-align: center;
            margin-top: 2%;
        }

        .k-grouping-header {
            background-color: white;
            font-style: italic;
        }

        .k-grid-toolbar {
            background: white;
            border: none;
        }

        .k-grid table {
            width: 100.5%;
        }

        .k-grid-header th.k-state-focused, .k-list > .k-state-focused, .k-listview > .k-state-focused, .k-state-focused, td.k-state-focused {
            -webkit-box-shadow: inset 0 0 3px 1px white;
            box-shadow: inset 0 0 3px 1px white;
        }

        .k-treeview .k-i-collapse, .k-treeview .k-i-expand, .k-treeview .k-i-minus, .k-treeview .k-i-plus {
            margin-left: -16px;
            cursor: pointer;
        }
    </style>

    <script type="text/javascript">

        setTimeout(printSomething, 1000);

        function printSomething() {
            window.scrollTo(0, document.body.scrollHeight);
        }
    </script>
    <script type="text/javascript">

        $(document).ready(function () {
            setactivemenu('leftworkspacemenu');
            fhead('My Reports');
            BindTypeofUser();
            LicenseType();
            DepartmentType();
            BindUsers();
            Bindgrid();
        });

        var record = 0;
        function Bindgrid() {
            var grid = $('#grid').data("kendoGrid");
            if (grid != undefined || grid != null)
                $('#grid').empty();

            var Userdetails = [];
            if ($("#dropdownUser").data("kendoDropDownTree") != undefined && $("#dropdownUser").data("kendoDropDownTree") != null) {
                var list3 = $("#dropdownUser").data("kendoDropDownTree")._values;
                $.each(list3, function (i, v) {
                    Userdetails.push(v);
                });
            }
            var Branchdetails = [];
            if ($("#dropdowntree").data("kendoDropDownTree") != undefined && $("#dropdowntree").data("kendoDropDownTree") != null) {

                var list3 = $("#dropdowntree").data("kendoDropDownTree")._values;
                $.each(list3, function (i, v) {
                    Branchdetails.push(v);
                });
            }
            var Deptdetails = [];
            if ($("#dropdownDept").data("kendoDropDownTree") != undefined && $("#dropdownDept").data("kendoDropDownTree") != null) {

                var list3 = $("#dropdownDept").data("kendoDropDownTree")._values;
                $.each(list3, function (i, v) {
                    Deptdetails.push(v);
                });
            }

            var grid = $("#grid").kendoGrid({
                dataSource: {
                    transport: {
                        read: {
                            url: '<% =Path%>License/GetLicenseData?customerID=<% =Custid%>&loggedInUserID=<% =UserID %>&loggedInUserRole=<% =Role%>&isstatutoryinternal=' + $("#dropdownlistComplianceType").val() + '&Userdetail=' + JSON.stringify(Userdetails) + '&Statusdetail=' + $("#dropdownlistStatus").val() + '&LicenseTypedetail=' + $("#dropdownlistLicenseType").val() + '&Branchdetail=' + JSON.stringify(Branchdetails) + '&Deptdetail=' + JSON.stringify(Deptdetails) + '',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: '<% =Path%>Litigation/kendomyReminder?customerID=<% =Custid%>&loggedInUserID=<% =UserID %>&loggedInUserRole=<% =Role%>&reminderType=' + $("#dropdownlist1").val() +'&instanceID=-1'
                    },
                    pageSize: 10
                },
                excelExport: function (e) {
                    var columns = e.workbook.sheets[0].columns;
                    columns.forEach(function (column) {
                        delete column.width;
                        column.autoWidth = true;
                    });
                    e.workbook.fileName = "License Reports.xlsx";
                    e.workbook.sheets[0].title = $("#dropdownlistComplianceType").data("kendoDropDownList").text() + "_License Reports"
                },
                excel: {
                    allPages: true,
                },
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                sortable: true,
                filterable: true,
                columnMenu: true,
                dataBound: function () {
                    for (var i = 0; i < this.columns.length; i++) {
                        this.autoWidth;
                    }
                },
                pageable: {
                    numeric: true,
                    pageSizes: ['All', 5, 10, 20],
                    pageSize: 10,
                    buttonCount: 3,
                },
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                dataBinding: function () {
                    var total = this.dataSource._pristineTotal;
                    if (this.dataSource.pageSize() == undefined) {
                        this.dataSource.pageSize(total);
                        record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                    }
                    else {
                        record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                    }
                },
                columns: [
                    {
                        field: "CustomerBrach", title: 'Location',
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains",
                                }
                            }
                        }, width: "10%",
                    },
                    {
                        field: "LicensetypeName", title: 'License Type',
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "13%",
                    },
                    {
                        field: "ComplianceID", title: 'Compliance ID',
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "13%",
                    },
                    {
                        field: "LicenseNo", title: 'License No.',
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "12%",
                    },
                    {
                        field: "Licensetitle", title: 'Title',
                        attributes: {
                            style: 'white-space: nowrap;'
                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "10%",
                    },
                    {
                        hidden: true,
                        field: "DepartmentName", title: 'Department',
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "10%",
                    },
                    {
                        field: "PerformerName", title: 'Performer',
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "10%",
                    },
                    {
                        field: "ReviewerName", title: 'Reviewer',
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "10%",
                    },
                    {
                        field: "ApplicationDate", title: 'App Due Date',
                        type: "date",
                        format: "{0:dd-MMM-yyyy}",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    type: "date",
                                    format: "{0:dd-MMM-yyyy}",
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            },
                        }, width: "13%",
                    },
                    {
                        field: "EndDate", title: 'End Date',
                        type: "date",
                        format: "{0:dd-MMM-yyyy}",
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    type: "date",
                                    format: "{0:dd-MMM-yyyy}",
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "10%;",
                    },
                    {
                        field: "Status", title: 'Status',
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "10%;",
                    },
                    {
                        hidden: true,
                        field: "FileNO", title: 'File NO',
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "10%;",
                    },
                    {
                        hidden: true,
                        field: "PhysicalLocation", title: 'Physical Location',
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "10%;",
                    },
                    {
                        hidden: true,
                        field: "Cost", title: 'Cost',
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "10%;",
                    },

                    {
                        command: [
                            { name: "edit", text: "", iconClass: "k-icon k-i-eye", className: "ob-edit" },
                        ], title: "Action", lock: true, width: 100, width: "8%",
                        headerAttributes: {
                            style: "text-align: center;"
                        }
                    }
                ]

            });

            $("#grid").kendoTooltip({
                filter: "th",
                content: function (e) {
                    var target = e.target; // element for which the tooltip is shown 
                    return $(target).text();
                }
            });

            $("#grid").kendoTooltip({
                filter: "td", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");

            $("#grid").kendoTooltip({
                filter: ".k-grid-edit",
                content: function (e) {
                    return "View";
                }
            });

            $(document).on("click", "#grid tbody tr .ob-edit", function (e) {
                $('#divShowReminderDialog').modal('show');
                var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
                if ($("#dropdownlistComplianceType").val() == "S") {

                    $('#ContentPlaceHolder1_showReminderDetail').attr('src', "/LicenseManagement/aspxPages/LicenseDetailsPageView.aspx?AccessID=" + item.LicenseID);
                }
                else {
                    $('#ContentPlaceHolder1_showReminderDetail').attr('src', "/LicenseManagement/aspxPages/InternalLicenseDetailsPage.aspx?AccessID=" + item.LicenseID);
                }

            });
        }
        function BindUsers() {
            if ($('#dropdownUser').data('kendoDropDownTree')) {
                $('#dropdownUser').data('kendoDropDownTree').destroy();
                $('#divBranchUser').empty();
                $('#divBranchUser').append('<input id="dropdownUser" style="width: 145px; margin-right: 5px;" />');
            }
            $("#dropdownUser").kendoDropDownTree({
                placeholder: "User",
                checkboxes: true,
                checkAll: true,
                autoClose: false,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "FullName",
                dataValueField: "UID",
                change: function () {
                    /*FilterAll();*/
                    fCreateStoryBoard('dropdownUser', 'filterUser', 'user');
                    $('input[id=chkAllMain]').prop('checked', false);
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            <%--url: '<% =Path%>Data/KendoUserListNew?UserId=<% =UId%>&CustId=<% =CustId%>&Flag=<% =RoleKey%>&status=-1',--%>
                            url: '<% =Path%>Data/KendoUserListNew_License?UserId=<% =UId%>&CustId=<% =CustId%>&Flag=<% =RoleKey%>&status=-1&customerID=<% =Custid%>&loggedInUserID=<% =UserID %>&loggedInUserRole=<% =Role%>&isstatutoryinternal=' + $("#dropdownlistComplianceType").val() + '',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                    },
                }
            });
        }
        function BindTypeofUser() {

            /*Bindgrid();*/

            $("#dropdowntree").kendoDropDownTree({
                placeholder: "Entity/Sub-Entity/Location",
                checkboxes: {
                    checkChildren: true
                },
                autoClose: false,
                //checkAll: true,                
                autoWidth: true,
                //checkAllTemplate: "Select All",
                dataTextField: "Name",
                dataValueField: "ID",
                change: function (e) {
                    /*FilterAll();*/
                    fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc');
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Litigation/GetLocationList?customerId=<% =Custid%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Litigation/GetLocationList?customerId=<% =Custid%>"
                    },
                    schema: {
                        data: function (response) {
                            return response[0].locationList;
                        },
                        model: {
                            children: "Children"
                        }
                    }
                }
            });

            $("#dropdownlistComplianceType").kendoDropDownList({
                placeholder: "Compliance Type",
                dataTextField: "text",
                dataValueField: "value",
                autoClose: true,
                dataSource: [
                    { text: "Statutory", value: "S" },
                    { text: "Internal", value: "I" },
                ],
                index: 0,
                change: function (e) {
                    /*Bindgrid();*/
                    ClearAllSubFilter(e);
                    e.preventDefault();
                    BindUsers();
                    DepartmentType();
                    LicenseType();
                }
            });

            $("#dropdownlistStatus").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: true,
                dataTextField: "StatusName",
                dataValueField: "ID",
                optionLabel: "Select Status",
                change: function (e) {
                    /*FilterAll();*/
                    fCreateStoryBoard('dropdownlistStatus', 'filtersstatus', 'status');
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>License/StatusList',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }

                    }
                }, dataBound: function (e) {
                    e.sender.list.width("150");
                }
            });

        }
        function DepartmentType() {
            if ($('#dropdownDept').data('kendoDropDownTree')) {
                $('#dropdownDept').data('kendoDropDownTree').destroy();
                $('#divBranchDept').empty();
                $('#divBranchDept').append('<input id="dropdownDept" style="width: 145px; margin-right: 5px;" />');
            }
            $("#dropdownDept").kendoDropDownTree({
                placeholder: "Department",
                checkboxes: true,
                checkAll: true,
                autoClose: false,
                autoWidth: true,
                dataTextField: "DepartmentName",
                dataValueField: "DepartmentID",
                checkAllTemplate: "Select All",
                change: function (e) {
                    //DataBindDaynamicKendoGriddMain();
                    /*FilterAll();*/ 
                    fCreateStoryBoard('dropdownDept', 'filterdept', 'dept');
                    $('input[id=chkAllMain]').prop('checked', false);
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            <%--url: '<% =Path%>Litigation/KendoDeptList?CustId=<% =Custid%>',--%>
                            url: '<% =Path%>Data/KendoDeptListNew_License?customerID=<% =Custid%>&loggedInUserID=<% =UserID %>&loggedInUserRole=<% =Role%>&isstatutoryinternal=' + $("#dropdownlistComplianceType").val() + '',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }

                    }
                }
            });
        }
        function LicenseType() {
            $("#dropdownlistLicenseType").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: true,
                dataTextField: "Name",
                dataValueField: "ID",
                optionLabel: "Select License Type",
                change: function (e) {
                    /*FilterAll();*/ 
                    fCreateStoryBoard('dropdownlistLicenseType', 'filterstype', 'type');
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>License/LicenseTypeList?userID=<% =UserID%>&role=<% =Role %>&isstatutoryinternal=' + $("#dropdownlistComplianceType").val() + '',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');

                            },
                        }

                    }
                }, dataBound: function (e) {
                    e.sender.list.width("200");
                }
            });
        }
        function CloseMyReminderPopup() {
            $('#divShowReminderDialog').modal('hide');
            Bindgrid();
        }
        function onChangeSD() {
            //FilterGrid();
        }
        function onChangeLD() {
            //FilterGrid();
        }
        function BindGridApply(e) {
            BindTypeofUser();
            BindGrid();
            FilterAll();
            e.preventDefault();
        }
        function FilterAll() {
            var locationlist = $("#dropdowntree").data("kendoDropDownTree")._values;
            var Departmentlist = $("#dropdownDept").data("kendoDropDownTree")._values;

            var userdetails = [];
                  <%if (RoleFlag == 1)%>
                   <%{%>
            if ($("#dropdownUser").data("kendoDropDownTree") != undefined) {
                userdetails = $("#dropdownUser").data("kendoDropDownTree")._values;
            }
                <%}%>

            var finalSelectedfilter = { logic: "and", filters: [] };

            if (locationlist.length > 0
                || Departmentlist.length > 0
                || userdetails.length > 0 
                || ($("#dropdownlistStatus").val() != "" && $("#dropdownlistStatus").val() != null && $("#dropdownlistStatus").val() != undefined)
                || ($("#dropdownlistLicenseType").val() != "" && $("#dropdownlistLicenseType").val() != null && $("#dropdownlistLicenseType").val() != undefined)
                || ($("#dropdownDept").val() != "" && $("#dropdownDept").val() != null && $("#dropdownDept").val() != undefined)) {

                if ($("#dropdownlistStatus").val() != "" && $("#dropdownlistStatus").val() != null && $("#dropdownlistStatus").val() != undefined) {
                    var StatusFilter = { logic: "or", filters: [] };
                    StatusFilter.filters.push({
                        field: "StatusID", operator: "eq", value: parseInt($("#dropdownlistStatus").val())
                    });
                    finalSelectedfilter.filters.push(StatusFilter);
                }

                if (Departmentlist.length > 0) {
                    var DeptFilter = { logic: "or", filters: [] };

                    $.each(Departmentlist, function (i, v) {
                        DeptFilter.filters.push({
                            //   field: "departmentID1", operator: "eq", value: parseInt(v)
                            field: "DepartmentID", operator: "eq", value: parseInt(v)
                        });
                    });

                    finalSelectedfilter.filters.push(DeptFilter);
                }
                if ($("#dropdownlistLicenseType").val() != "" && $("#dropdownlistLicenseType").val() != null && $("#dropdownlistLicenseType").val() != undefined) {
                    var SeqFilter = { logic: "or", filters: [] };

                    if ($("#dropdownlistComplianceType").val() == "S") {
                        SeqFilter.filters.push({
                            field: "LicenseTypeID", operator: "eq", value: $("#dropdownlistLicenseType").val()
                        });
                    }
                    else {
                        SeqFilter.filters.push({
                            field: "LicenseTypeID", operator: "eq", value: $("#dropdownlistLicenseType").val()
                        });
                    }

                    finalSelectedfilter.filters.push(SeqFilter);
                }


                if (userdetails.length > 0) {
                    var UserFilter = { logic: "or", filters: [] };
                    $.each(userdetails, function (i, v) {
                        UserFilter.filters.push({
                            field: "PerformerID", operator: "eq", value: parseInt(v)
                        });
                    });
                    $.each(userdetails, function (i, v) {
                        UserFilter.filters.push({
                            field: "ReviewerID", operator: "eq", value: parseInt(v)
                        });
                    });
                    $.each(userdetails, function (i, v) {
                        UserFilter.filters.push({
                            field: "ApproverID", operator: "eq", value: parseInt(v)
                        });
                    });

                    finalSelectedfilter.filters.push(UserFilter);
                }

                if (locationlist.length > 0) {
                    var locFilter = { logic: "or", filters: [] };

                    $.each(locationlist, function (i, v) {
                        locFilter.filters.push({
                            field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                        });
                    });

                    finalSelectedfilter.filters.push(locFilter);
                }


                if (finalSelectedfilter.filters.length > 0) {
                    var dataSource = $("#grid").data("kendoGrid").dataSource;
                    dataSource.filter(finalSelectedfilter);
                }
                else {
                    $("#grid").data("kendoGrid").dataSource.filter({});
                }
            }
            else {
                $("#grid").data("kendoGrid").dataSource.filter({});
            }
        }
        function ClearAllFilterMain(e) {
            $("#dropdowntree").data("kendoDropDownTree").value([]);
            $("#dropdownDept").data("kendoDropDownTree").value([]);
            $("#dropdownlistStatus").data("kendoDropDownList").value([]);
            $("#dropdownlistLicenseType").data("kendoDropDownList").value([]);
            //$('#ClearfilterMain').css('display', 'none');
            $("#grid").data("kendoGrid").dataSource.filter({});
            $('#grid').empty();
            <%if (RoleFlag == 1)%><%{%>
            $("#dropdownUser").data("kendoDropDownTree").value([]);
            <%}%>
            $("#dropdownlistComplianceType").data("kendoDropDownList").select(0);
            DepartmentType();
            BindUsers();
            Bindgrid();
            e.preventDefault();
        }
        function fcloseStory(obj) {

            var DataId = $(obj).attr('data-Id');
            var dataKId = $(obj).attr('data-K-Id');
            var seq = $(obj).attr('data-seq');
            var deepspan = $('#' + DataId + ' > li > span > span.k-i-close')[seq];
            $(deepspan).trigger('click');
            var upperli = $('#' + dataKId);
            $(upperli).remove();

            //for rebind if any pending filter is present (Main Grid)
            fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc');
            fCreateStoryBoard('dropdownDept', 'filterdept', 'dept');
            fCreateStoryBoard('dropdownlistStatus', 'filtersstatus', 'status');
            fCreateStoryBoard('dropdownlistLicenseType', 'filterstype', 'type');
            fCreateStoryBoard('dropdownUser', 'filterUser', 'user');
            CheckFilterClearorNotMain();
        };
        function CheckFilterClearorNotMain() {
            if (($($($('#dropdowntree').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
                ($($($('#dropdownDept').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
                ($($($('#dropdownUser').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0)) {
                $('#ClearfilterMain').css('display', 'none');
            }
        }
        function fCreateStoryBoard(Id, div, filtername) {

            var IdKReset = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('id');
            $('#' + div).html('');
            $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');
            $('#' + div).css('display', 'block');

            if (div == 'filtersstoryboard') {
                $('#' + div).append('Location&nbsp;&nbsp;:');
                $('#ClearfilterMain').css('display', 'block');
            }
            //else if (div == 'filterdept') {
            //    $('#' + div).append('Department&nbsp;&nbsp;&nbsp;:');//Department              
            //}
            else if (div == 'filterdept') {
                $('#' + div).append('Department&nbsp;&nbsp;&nbsp;:');
                $('#ClearfilterMain').css('display', 'block');
            }
            else if (div == 'filterUser') {
                $('#' + div).append('User&nbsp;&nbsp;&nbsp;:');
                $('#ClearfilterMain').css('display', 'block');
            }
            else if (div == 'validatecompliancetype') {
                $('#ClearfilterMain').css('display', 'block');
            }

            for (var i = 0; i < $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length; i++) {
                var button = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button')[i];
                $(button).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('placeholder', 'Shree');
                var buttontest = $($(button).find('span')[0]).text();
                if (buttontest.length > 10) {
                    buttontest = buttontest.substring(0, 10).concat("...");
                }
                $('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="background-color:#EBEBEB; height: 20px;Color:Gray;border-radius:10px;margin-left:4px;margin-top:4px;vertical-align: baseline;"><span unselectable="on" title="' + $($(button).find('span')[0]).text() + '">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory(this)" title="clear" aria-label="clear" class="k-select" style="padding-left: 6px;"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory(this)"  class="k-icon k-i-close" style="font-size: 12px;"></span></span></li>');

            }

            if ($($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) {
                $('#' + div).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');

            }
            CheckFilterClearorNotMain();
        }
        function exportReportGenerate(e) {

            var grid = $("#grid").data("kendoGrid");
            grid.saveAsExcel();
            e.preventDefault();
            return false;
        }
        function Applybtndata(e) {
            Bindgrid();
            e.preventDefault();
            return false;
        }
        function ClearAllSubFilter(e) {
            $("#dropdownlistStatus").data("kendoDropDownList").value([]);
            $("#dropdownlistLicenseType").data("kendoDropDownList").value([]);
            $("#dropdownDept").data("kendoDropDownTree").value([]);

           
            if ($("#dropdownUser").val() != "" && $("#dropdownUser").val() != null && $("#dropdownUser").val() != undefined) {
                $("#dropdownUser").data("kendoDropDownTree").value([]);
            }
            
            e.preventDefault();
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row" style="margin-top: 10px;">
        <div class="toolbar">
            <input id="dropdowntree" data-placeholder="Entity/Sub-Entity/Location" style="width: 250px; margin-right: 5px;" />
            <input id="dropdownlistComplianceType" style="width: 172px; margin-right: 5px;" />
            <input id="dropdownlistStatus" style="width: 172px; margin-right: 5px;" />
            <input id="dropdownlistLicenseType" style="width: 172px; margin-right: 5px;" />
            <%if (RoleFlag == 1)%>
            <%{%>
            <div id="divBranchUser" style="float: right">
                <input id="dropdownUser" data-placeholder="User" style="width: 145px; margin-right: 5px;" />
            </div>
            <%}%>
            <div id="divBranchDept" style="float: right">
                <input id="dropdownDept" data-placeholder="Department" style="width: 145px; margin-right: 5px;" />
            </div>
            <div class="row" style="margin-right: 5px; margin-top: 10px">
                <button type="button" id="Applybtn" style="float: right; height: 28px;" onclick="Applybtndata(event)"><span class="k-icon k-i-filter" onclick="javascript:return false;"></span>Apply</button>
                <button id="exportReport" style="margin-right: 10px; float: right; float: right; height: 28px;" onclick="exportReportGenerate(event)"><span class="k-icon k-i-excel k-grid-edit3"></span>Export</button>
                <button id="ClearfilterMain" style="display: none; float: right; margin-right: 10px; height: 28px;" onclick="ClearAllFilterMain(event)"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear Filter</button>
            </div>
        </div>
    </div>



    <div class="row">
        <%-- <input id="dropdownDept" data-placeholder="Department" style="width: 145px;margin-left:11px;margin-top:10px"/>--%>
    </div>

    <div class="row" style="padding-top: 12px;">
        <div class="row" style="padding-bottom: 4px; font-size: 12px; font-weight: bold; display: none; color: #535b6a;" id="filtersstoryboard">&nbsp;</div>
        <div class="row" style="padding-bottom: 4px; font-size: 12px; font-weight: bold; display: none; color: #535b6a;" id="filterdept">&nbsp;</div>
        <div class="row" style="padding-bottom: 4px; font-size: 12px; font-weight: bold; display: none; color: #535b6a;" id="filterUser">&nbsp;</div>
        <div class="row" style="padding-bottom: 4px; font-size: 12px; font-weight: bold; display: none; color: #535b6a;" id="filtersstoryStatus">&nbsp;</div>
        <div id="grid" style="margin-bottom: 10px; width: 99.2%"></div>
    </div>

    <div class="modal fade" id="divShowReminderDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog" style="width: 92%;">
            <div class="modal-content">
                <div class="modal-header" style="height: 30px;">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label class="modal-header-custom col-md-6 plr0">
                        License Detail(s)</label>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="CloseMyReminderPopup();">&times;</button>
                </div>

                <div class="modal-body">
                    <iframe src="about:blank" id="showReminderDetail" frameborder="0" runat="server" width="100%" height="600px"></iframe>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
