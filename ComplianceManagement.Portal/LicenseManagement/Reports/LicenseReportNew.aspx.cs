﻿using System;
using System.Collections.Generic;
using System.Configuration;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;

namespace com.VirtuosoITech.ComplianceManagement.Portal.LicenseManagement.Reports
{
    public partial class LicenseReportNew : System.Web.UI.Page
    {
        //10/12/2021
        protected int RoleFlag;
        protected List<Int32> roles;
        protected int UId;
        protected int CustId;
        protected string RoleKey;
        //10/12/2021

        protected static string Path;
        protected static string Custid;
        protected static int UserID;
        protected static string Role;
        protected static string type;
        protected string Internalsatutory;
        protected int LicenseTypeID;
        protected int StatusFlagID;
        protected static string Authorization;
        protected void Page_Load(object sender, EventArgs e)
        {
            //10/12/2021
            RoleFlag = 0;
            UId = Convert.ToInt32(AuthenticationHelper.UserID);
            CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            //10/12/2021


            int CacheClearTime = Convert.ToInt32(ConfigurationManager.AppSettings["CacheClearTimeToken"]);
            string CacheName = "CacheGetTokenData_" + Convert.ToString(AuthenticationHelper.UserID) + "_" + Convert.ToString(AuthenticationHelper.CustomerID);
            Authorization = (string)HttpContext.Current.Cache[CacheName];
            if (Authorization == null)
            {
                Authorization = Business.ComplianceManagement.getToken(Convert.ToString(AuthenticationHelper.UserID));
                HttpContext.Current.Cache.Insert(CacheName, Authorization, null, DateTime.Now.AddMinutes(CacheClearTime), System.Web.Caching.Cache.NoSlidingExpiration); // add it to cache
            }
            Path = ConfigurationManager.AppSettings["KendoPathApp"];
            Custid = Convert.ToString(AuthenticationHelper.CustomerID);
            UserID = AuthenticationHelper.UserID;
            Role = AuthenticationHelper.Role;
            LicenseTypeID = -1;

            if (!string.IsNullOrEmpty(Request.QueryString["Internalsatutory"]))
            {
                Internalsatutory = Request.QueryString["Internalsatutory"];
                if (Internalsatutory == "Statutory")
                {
                    LicenseTypeID = -1;
                }
                else
                {
                    LicenseTypeID = 0;
                }

            }

            //10/12/2021
            #region User ddl
            var UserDetails = UserManagement.GetByID(AuthenticationHelper.UserID);
            if (!string.IsNullOrEmpty(Convert.ToString(UserDetails.IsHead)))
            {
                if ((bool)UserDetails.IsHead)
                {
                    RoleKey = "DEPT";
                }
            }

            if (Session["User_comp_Roles"] != null)
            {
                roles = Session["User_comp_Roles"] as List<int>;
            }
            else
            {
                roles = CustomerBranchManagement.GetAssignedroleid(AuthenticationHelper.UserID);
                Session["User_comp_Roles"] = roles;
            }

            if (roles != null)
            {
                if (roles.Contains(3))
                {
                    RoleKey = "PRA";
                }
                if (roles.Contains(4))
                {
                    RoleKey = "REV";
                }
                if (roles.Contains(6))
                {
                    RoleKey = "APPR";
                }
            }
            if (AuthenticationHelper.Role == "MGMT")
            {
                RoleFlag = 1;
                RoleKey = "MGMT";
            }
            if (AuthenticationHelper.Role == "AUDT")
            {
                RoleKey = "AUD";
            }
            else if (AuthenticationHelper.Role == "EXCT")
            {
                if (roles != null && roles.Contains(4))
                {
                    RoleFlag = 1;
                }
            }
            else if (AuthenticationHelper.Role == "CADMN")
            {
                roles = CustomerBranchManagement.GetAssignedroleid(AuthenticationHelper.UserID);
                if (roles.Contains(4))
                {
                    RoleFlag = 1;
                }
            }
            #endregion
            //10/12/2021

            //if (StatusFlagID == -1)
            //{
            //    LicenseTypeID = 0;//statutory        
            //}
            //if (StatusFlagID == 0)
            //{
            //    LicenseTypeID = 2;//Internal
            //}
        }
    }
}