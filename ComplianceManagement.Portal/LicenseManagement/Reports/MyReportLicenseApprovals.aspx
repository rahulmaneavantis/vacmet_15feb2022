﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LicenseManagement.Master" AutoEventWireup="true" CodeBehind="MyReportLicenseApprovals.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.LicenseManagement.Reports.MyReportLicenseApprovals" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jszip.min.js"></script>

   <style type="text/css">
        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
        }

        .k-textbox .k-icon {
            top: 50%;
            margin: -7px 5px 0px;
            position: absolute;
        }

        .k-active-filter, .k-state-active, .k-state-active:hover {
            background-color: #E9EAEA;
            border-color: #a6a6ad;
            color: #535b6a;
        }

        .div.k-grid-footer, div.k-grid-header {
            border-top-width: 1px;
            margin-right: 0px;
        }

        .k-grid-footer-wrap, .k-grid-header-wrap {
            position: relative;
            width: 100%;
            overflow: hidden;
            border-style: solid;
            border-width: 0 1px 0 0;
            zoom: 1;
        }

        .k-grid-content {
            min-height: 400px !important;
        }

        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 2.0em;
            border-bottom-width: 1px;
            background-color: white;
            border-width: 0 1px 1px 0px;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
            background-color: white;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: white;
        }

        .toolbar {
            float: left;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: white;
            color: #2b2b2b;
        }
            .k-pager-wrap > .k-link > .k-icon {
                margin-top: 6px;
                color: inherit;
            }

        td.k-command-cell {
            border-width: 0 0px 1px 0px;
            text-align: center;
        }

        .k-grid-pager {
            margin-top: -1px;
        }
        .col-md-2 {
            width: 20%;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            border-width: 0px 0px 1px 0px;
            background: #E9EAEA;
            font-weight: bold;
            margin-right: 18px;
            font-family: 'Roboto',sans-serif;
            font-size: 15px;
            height: 20px;
            vertical-align: middle;
        }

        .k-dropdown-wrap.k-state-default {
            background-color: white;
        }

        .k-popup.k-calendar-container, .k-popup.k-list-container {
            background-color: white;
        }

        .k-list > .k-state-focused.k-state-selected, .k-listview > .k-state-focused.k-state-selected, .k-state-focused.k-state-selected, td.k-state-focused.k-state-selected {
            -webkit-box-shadow: inset 0 0 3px 1px #14699f;
            box-shadow: none;
        }

        label.k-label {
            font-family: roboto,sans-serif !important;
            color: #515967;
            /* font-stretch: 100%; */
            font-style: normal;
            font-weight: 400;
            min-width: 362px;
            white-space: pre-wrap;
            background: white;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: flex;
            margin-bottom: 0px;
            margin-top: 6px;
        }

        .k-state-default > .k-select {
            border-color: #ceced2;
            margin-top: 0px;
        }

        .k-grid-norecords {
            width: 100%;
            height: 100%;
            text-align: center;
            margin-top: 2%;
        }

        .k-grouping-header {
            font-style: italic;
            background-color: white;
        }

        .k-grid-toolbar {
            background: white;
            border: none;
            margin: 5px;
        }

        .k-grid table {
            width: 100.5%;
        }
        .k-i-arrow-60-down, .k-i-arrow-60-left, .k-i-arrow-60-right, .k-i-arrow-60-up {
            cursor: pointer;
            margin-top: 6px;
        }
        .k-widget > span.k-invalid,
        input.k-invalid {
            border: 1px solid red !important;
        }

        .k-widget > span.k-invalid,
        textarea.k-invalid {
            border: 1px solid red !important;
        }

        .k-widget.k-tooltip-validation {
            display: none !important;
        }
       .MainWrapper {
           width: 20%;
           display: block;
           float: left;
           color: #212121;
       }
    </style>

    <script type="text/javascript">
        $(document).ready(function () {
            setactivemenu('leftmastermenu');
            fhead('My Reports/Approvals');

            $("#dropdownlistComplianceType").kendoDropDownList({
                placeholder: "Compliance Type",
                dataTextField: "text",
                dataValueField: "value",
                autoClose: true,
                dataSource: [
                    { text: "Statutory", value: "S" },
                    { text: "Internal", value: "I" },
                ],
                index: 0,
                change: function (e) {
                    Bindgrid();
                }
            });

            Bindgrid();

            $("#txtApproveTill").kendoDatePicker({
                format: "dd-MM-yyyy",
                dateInput: true
            });
        });

        var record = 0;
        var _dateFormat = "dd-MM-yyyy"

        function Bindgrid() {
            var grid = $('#grid').data("kendoGrid");
            if (grid != undefined || grid != null)
                $('#grid').empty();

            var grid = $("#grid").kendoGrid({
                dataSource: {
                    transport: {
                        read: {
                            url: "/Main/GetLicenseApprovalsData?customerID=<% =CustId%>&loggedInUserRole=<%=FlagIsApp%>&isstatutoryinternal=" + $("#dropdownlistComplianceType").val(),
                            dataType: "json",
                        },
                    },
                    pageSize: 10,

                },
                excel: {
                    allPages: true,
                },
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                sortable: true,
                filterable: true,
                columnMenu: true,
                pageable: true,
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                groupable: true,
                dataBound: OnGridDataBound,
                pageable: {
                    numeric: true,
                    pageSizes: ['All', 5, 10, 20],
                    pageSize: 10,
                    buttonCount: 3,
                },
                dataBinding: function () {
                    record = 0;
                    var total = this.dataSource._pristineTotal;
                    if (this.dataSource.pageSize() == undefined) {
                        this.dataSource.pageSize(total);
                        record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                    }
                    else {
                        record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                    }
                },
                columns: [
                    {
                        title: "Sr. No.",
                        template: "#= ++record #",
                        width: "7%",
                    },
                    {
                        field: "CustomerBrach", title: 'Location',
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains",
                                }
                            }
                        }, width: "10%",
                    },
                    {
                        field: "LicensetypeName", title: 'License Type',
                        hidden: true,
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "13%",
                    },
                    {
                        field: "ComplianceID", title: 'Compliance ID',
                        hidden: true,
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "13%",
                    },
                    {
                        field: "LicenseNo", title: 'License No.',
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "12%",
                    },
                    {
                        field: "Licensetitle", title: 'Title',
                        attributes: {
                            style: 'white-space: nowrap;'
                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "10%",
                    },
                    {
                        hidden: true,
                        field: "DepartmentName", title: 'Department',
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "10%",
                    },
                    {
                        field: "PerformerName", title: 'Performer',
                        hidden: true,
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "10%",
                    },
                    {
                        field: "ReviewerName", title: 'Reviewer',
                        hidden:true,
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "10%",
                    },
                    {
                        field: "ApplicationDate", title: 'App Due Date',
                        hidden: true,
                        type: "date",
                        format: "{0:dd-MMM-yyyy}",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    type: "date",
                                    format: "{0:dd-MMM-yyyy}",
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            },
                        }, width: "13%",
                    },
                    {
                        field: "EndDate", title: 'End Date',
                        type: "date",
                        format: "{0:dd-MMM-yyyy}",
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    type: "date",
                                    format: "{0:dd-MMM-yyyy}",
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "10%;",
                    },
                    {
                        field: "Status", title: 'Status',
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "10%;",
                    },
                    {
                        field: "Validtill", title: 'Request Validity',
                        template: '<div>#= kendo.toString(kendo.parseDate(Validtill), "dd-MM-yyyy")=="01-01-0001" ||  kendo.toString(kendo.parseDate(Validtill), "dd-MM-yyyy")=="01-01-1900"?"": kendo.toString(kendo.parseDate(Validtill), "' + _dateFormat + '") #</div>',
                        type: "date",
                        format: "{0:dd-MM-yyyy}",
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    type: "date",
                                    format: "{0:dd-MM-yyyy}",
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "15%"
                    },
                    {
                        field: "RequestStatus", title: 'Request Status',
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "12%"
                    },
                    {
                        command: [
                            { name: "view", text: "", iconClass: "k-icon k-i-eye", className: "ob-view" },
                              <% if (FlagIsApp=="MGMT"){%>
                            { name: "edit", text: "", iconClass: "k-icon k-i-edit", className: "ob-edit" },
                              <%}%>
                            <% if (FlagIsApp!="MGMT"){%>
                            { name: "mail", text: "", iconClass: "k-icon k-i-envelop", className: "ob-mail" },
                            <%}%>
                        ], title: "Action", lock: true, width: "13%", headerAttributes: {
                            style: "text-align: center;"
                        }

                    }
                ]
            });

            $("#grid").kendoTooltip({
                filter: ".k-grid-mail",
                content: function (e) {
                    return "Request Again";
                }
            });

            $("#grid").kendoTooltip({
                filter: ".k-grid-edit",
                content: function (e) {
                    return "Edit Request";
                }
            });

            $("#grid").kendoTooltip({
                filter: ".k-grid-view",
                content: function (e) {
                    return "View Contract Details";
                }
            });

            $("#grid").kendoTooltip({
                filter: "td",
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");

            $("#grid").kendoTooltip({
                filter: "th",
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");

            function OnGridDataBound(e) {
                for (var i = 0; i < this.columns.length; i++) {
                    this.autoWidth;
                }
                var grid = $("#grid").data("kendoGrid");
                var gridData = grid.dataSource.view();
                for (var i = 0; i < gridData.length; i++) {
                    var currentUid = gridData[i].uid;
                    var currentRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                    if (gridData[i].RequestStatus == "Expired" || gridData[i].RequestStatus == "Rejected") {
                        var mailButton = $(currentRow).find(".ob-mail");
                        mailButton.show();
                    } else {
                        var mailButton = $(currentRow).find(".ob-mail");
                        mailButton.hide();
                    }
                }
            }
        }

        $(document).on("click", "#grid tbody tr .ob-view", function (e) {
            var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
            var modalHeight = screen.height - 150;

            if (modalHeight < 0)
                modalHeight = 200;

            $('#divShowDialog').modal('show');
            $('.modal-dialog').css('width', '95%');
            $('#showdetails').attr('width', '100%');
            $('#showdetails').attr('height', modalHeight + "px");

            if ($("#dropdownlistComplianceType").val() == "S") {

                $('#showdetails').attr('src', "/LicenseManagement/aspxPages/LicenseDetailsPageView.aspx?AccessID=" + item.LicenseID);
            }
            else {
                $('#showdetails').attr('src', "/LicenseManagement/aspxPages/InternalLicenseDetailsPage.aspx?AccessID=" + item.LicenseID);
            }

        });

        $(document).on("click", "#grid tbody tr .ob-edit", function (e) {
            var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
            $("#txtLicenseInstanceID").val(item.LicenseID);
            $("#txtManagementRemarks").val(item.Remarks);
            $("#txtApproveTill").data("kendoDatePicker").value(item.Validtill);


            $("#drpRequestedBy").kendoDropDownList({
                autoClose: true,
                autoWidth: true,
                filter: "contains",
                dataTextField: "FirstName",
                dataValueField: "ID",
                template: '<span style="width:10px" class="k-state-default">#: data.FirstName#</span>' + " " +
                    '<span class="k-state-default">#: data.LastName#</span>',
                valueTemplate: '<span class="selected-value">#: data.FirstName#</span>' + " " + '<span class="selected-value">#: data.LastName#</span>',
                change: function (e) {
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: "/Main/GetAllManagementUserForContract?&customerID=<% =CustId%>&RoleID=0&UserID=" + item.CreatedBy,
                            dataType: "json",
                        }
                    },
                }
            });

            $("#txtLicenseStatustblid").val(item.ContractStatusTblID);
            $("#divForApproveRejectLicenseEdit").kendoWindow({
                modal: true,
                pinned: true,
                width: "54%",
                height: "37%",
                title: "Accept/Reject Requests",
                visible: false,
                draggable: false,
                refresh: true,
                actions: [
                    "Close"
                ], close: Bindgrid

            }).data("kendoWindow").open().center();

        });

        $(document).on("click", "#grid tbody tr .ob-mail", function (e) {

            var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
            $.ajax({
                type: 'POST',
                url: '/Main/ResendMailToLicenseMgmtUser?ContractInstanceID=' + item.ID + '&UpdateID=' + item.ContractStatusTblID,
                dataType: "json",
                success: function (result) {
                    alert("Request Successfully Sent");
                    if ($('#grid').data('kendoGrid')) {
                        $('#grid').data('kendoGrid').dataSource.read();
                        $('#grid').data('kendoGrid').refresh();
                    }
                },
                error: function (result) {
                    console.log(result);
                }
            });
            return true;
        });

        function btnApprove_click() {
            if (($("#txtManagementRemarks").val()).trim() == "") {
                alert("Enter Remarks");
            }
            if ($("#txtApproveTill").val() == "day-month-year" || $("#txtApproveTill").val() == "") {
                alert("Enter Valid till date");
            }
            if (($("#txtManagementRemarks").val()).trim() != "" && $("#txtApproveTill").val() != "day-month-year" && $("#txtApproveTill").val() != "") {
                $.ajax({
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    type: 'POST',
                    url: '/Main/UpdateLicenseApprovalStatus?LicenseInstanceID=' + $("#txtLicenseInstanceID").val() + '&RequestStatus=Approved&Remarks=' + $("#txtManagementRemarks").val() + '&Validtill=' + $("#txtApproveTill").val() + '&UpdateID=' + $("#txtLicenseStatustblid").val(),
                    success: function (response) {
                        if (response == "Success") {
                            alert("Request Updated Successfully");

                            if ($("#divForApproveRejectLicenseEdit").data("kendoWindow")) {
                                $("#divForApproveRejectLicenseEdit").data("kendoWindow").close();
                            }
                        }
                        else {
                            alert("Error");
                        }
                    }
                });
            }
        }

        function btnReject_click() {
            if (($("#txtManagementRemarks").val()).trim() == "") {
                alert("Enter Remarks");
            }
            if (($("#txtManagementRemarks").val()).trim() != "") {
                $.ajax({
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    type: 'POST',
                    url: '/Main/UpdateLicenseApprovalStatus?LicenseInstanceID=' + $("#txtLicenseInstanceID").val() + '&RequestStatus=Rejected&Remarks=' + $("#txtManagementRemarks").val() + '&Validtill=&UpdateID=' + $("#txtLicenseStatustblid").val(),
                    success: function (response) {
                        if (response == "Success") {
                            alert("Request Updated Successfully");

                            if ($("#divForApproveRejectLicenseEdit").data("kendoWindow")) {
                                $("#divForApproveRejectLicenseEdit").data("kendoWindow").close();
                            }
                        }
                        else {
                            alert("Error");
                        }
                    }
                });
            }
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
       <div class="row" style="margin-left: 11px;margin-top: 10px;">
        <div class="toolbar">
            <input id="dropdownlistComplianceType" style="width:172px;margin-right: 10px;"/>
        </div>
    </div>
    <div class="margin-6">
        <div id="grid" style="width: 98.4%; margin: 10px;"></div>
    </div>
    <div id="divForApproveRejectLicenseEdit" style="display:none;">
         <div class="col-md-11" style="margin-bottom: 1.5%;">
            <label class="MainWrapper" style="width: 33%">Requested By<span style="color: red;">*</span></label>
             <input id="drpRequestedBy" disabled="disabled" style="width: 30%;" />
            <input id="txtLicenseInstanceID"  class="k-textbox" style="display:none;"/>
             <input id="txtLicenseStatustblid"  class="k-textbox" style="display:none;"/>
        </div>
           <div class="col-md-11" style="margin-bottom: 1.5%;">
            <label class="MainWrapper" style="width: 33%">Remarks<span style="color: red;">*</span></label>
             <textarea id="txtManagementRemarks" required="required" placeholder="Remarks" class="k-textbox" style="width: 63%;height: 72px;"></textarea>
        </div>
         <div class="col-md-11" style="margin-bottom: 1.5%;">
            <label class="MainWrapper" style="width: 33%">Approve till</label>
            <input id="txtApproveTill" required="required" style="width: 30%;" />
        </div>
        <div class="row" style="text-align: center;">
             <button id="btnApprove" type="button" style="height:29px;margin:10px" onclick="btnApprove_click()" class="btn btn-primary">Approve</button>
             <button id="btnreject" type="button" style="height:29px" onclick="btnReject_click()" class="btn btn-primary">Reject</button>
           </div>
    </div>
    <div class="modal fade" id="divShowDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bgColor-gray" style="height: 35px;">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label id="lblname" runat="server" class="modal-header-custom col-md-6 plr0"></label>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>

                <div class="modal-body" style="background-color: #f7f7f7;">
                    <iframe id="showdetails" src="about:blank" width="95%" height="80%" frameborder="0"></iframe>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
