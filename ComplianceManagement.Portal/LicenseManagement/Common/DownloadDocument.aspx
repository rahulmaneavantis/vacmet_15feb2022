﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DownloadDocument.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.LicenseManagement.Common.DownloadDocument" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript">
        function NoDcoumentAlert() {
            alert("No Document Available for Download.");
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>

        <div class="col-md-12 colpadding0">
                <asp:ValidationSummary ID="vsLicenseListPage" runat="server" Display="none" class="alert alert-block alert-danger fade in" Style="padding-left: 5%"
                    ValidationGroup="LicenseListPageValidationGroup" />
                <asp:CustomValidator ID="cvErrorLicenseListPage" runat="server" EnableClientScript="False"
                    ValidationGroup="LicenseListPageValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
            </div>

    </div>
    </form>
</body>
</html>
