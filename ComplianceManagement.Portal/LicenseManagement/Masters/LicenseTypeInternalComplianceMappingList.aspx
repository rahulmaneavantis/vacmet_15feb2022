﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true"
    CodeBehind="LicenseTypeInternalComplianceMappingList.aspx.cs"
    Inherits="com.VirtuosoITech.ComplianceManagement.Portal.LicenseManagement.Masters.LicenseTypeInternalComplianceMappingList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
        .custom-combobox-input{
            width: 258px;
        }
    </style>   

    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">

    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upLicenseTypeComplianceMappingList" runat="server" UpdateMode="Conditional" OnLoad="upLicenseTypeComplianceMappingList_Load">
        <ContentTemplate>
            <div style="margin-bottom: 4px">
                <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                    ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
            </div>
            <table width="100%">
                <tr>
                    <td style="width:5%;">
                        <label style="width: 100px; display: block; float: left; font-size: 13px; padding: 0px; margin: 0px;">
                            Customer :</label>
                    </td>
                    <td style="width:25%;">
                        <asp:DropDownList ID="ddlCustomer" runat="server" AutoPostBack="true"
                            OnSelectedIndexChanged="ddlCustomer_SelectedIndexChanged"
                            Style="padding: 0px; margin: 0px; height: 22px; width: 280px;"
                            CssClass="txtbox">
                        </asp:DropDownList>
                    </td>
                    <td style="width:5%;">
                        <label style="width: 120px; display: block; float: left; font-size: 13px; padding: 0px; margin: 0px;">
                            License Type :</label>
                    </td>
                    <td style="width:25%;">
                        <asp:DropDownList runat="server" ID="ddlFilterLicenseType"
                             Style="padding: 0px; margin: 0px; height: 22px; width: 280px;"
                            CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlFilterLicenseType_SelectedIndexChanged" />
                    </td>
                                        
                    <td style="width:5%;">
                        <label style="width: 100px; display: block; float: left; font-size: 13px;">
                            Filter :</label>
                    </td>
                    <td style="width:25%;">
                        <asp:TextBox runat="server" ID="tbxFilter" Width="200px" MaxLength="50" AutoPostBack="true"
                            OnTextChanged="tbxFilter_TextChanged" />
                    </td>
                    <td style="width:5%;">
                        <asp:LinkButton Text="Apply" runat="server" ID="btnExportExcel" OnClick="btnExportExcel_Click"
                            Width="80%" data-toggle="tooltip" data-placement="bottom" ToolTip="Export to Excel">
                            <img src="/Images/Excel _icon.png" alt="Export" /> 
                        </asp:LinkButton>
                    </td>                    
                </tr>
            </table>
            <asp:Panel ID="Panel1" Width="100%" Height="450px" ScrollBars="Vertical" runat="server">
                <asp:GridView runat="server" ID="grdMapping" AutoGenerateColumns="false" GridLines="Vertical" AllowSorting="true"
                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" OnRowCreated="grdMapping_RowCreated"
                    CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="100" Width="100%" OnSorting="grdMapping_Sorting"
                    Font-Size="12px" DataKeyNames="LicenseTypeId" OnRowCommand="grdMapping_RowCommand" OnPageIndexChanging="grdMapping_PageIndexChanging"
                    OnRowDataBound="grdMapping_RowDataBound">
                    <Columns>
                        <asp:BoundField DataField="ComplianceID" HeaderText="ComplianceID" ItemStyle-Height="20px" HeaderStyle-Height="20px" HeaderStyle-HorizontalAlign="Left" SortExpression="ComplianceID" />
                        <asp:BoundField DataField="LicenseTypeName" HeaderText="LicenseType Name" HeaderStyle-HorizontalAlign="Left" SortExpression="LicenseTypeName" />
                        <asp:BoundField DataField="ShortDescription" HeaderText="ShortDescription" ItemStyle-Height="20px" HeaderStyle-Height="20px" HeaderStyle-HorizontalAlign="Left" SortExpression="ShortDescription" />
                        <asp:BoundField DataField="Type" HeaderText="Type" ItemStyle-Height="20px" HeaderStyle-Height="20px" HeaderStyle-HorizontalAlign="Left" SortExpression="Type" />
                    </Columns>
                    <FooterStyle BackColor="#CCCC99" />
                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                    <PagerSettings Position="Top" />
                    <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                    <AlternatingRowStyle BackColor="#E6EFF7" />
                    <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                    <EmptyDataTemplate>
                        No Records Found.
                    </EmptyDataTemplate>
                </asp:GridView>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnExportExcel" />
        </Triggers>
    </asp:UpdatePanel>
     <script type="text/javascript">

         
        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);
        window.onunload = function () { null };


        $(function () {

            initializeCombobox();

        });
        function initializeCombobox() {
            $("#<%= ddlCustomer.ClientID %>").combobox();
            $("#<%= ddlFilterLicenseType.ClientID %>").combobox();            
        }

         function initializeJQueryUI(textBoxID, divID) {
             $("#" + textBoxID).unbind('click');

             $("#" + textBoxID).click(function () {
                 $("#" + divID).toggle("blind", null, 500, function () { });
             });
         }

    </script>
</asp:Content>
