﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true"
    CodeBehind="LicenseTypeComplianceMappingList.aspx.cs"
    Inherits="com.VirtuosoITech.ComplianceManagement.Portal.LicenseManagement.Masters.LicenseTypeComplianceMappingList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    </style>

    <script type="text/javascript">
        $(function () {
            $('#divLicenseTypeDialog').dialog({
                height: 380,
                width: 700,
                autoOpen: false,
                draggable: true,
                title: "License Type Compliance Mapping",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">

    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upLicenseTypeComplianceMappingList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div style="margin-bottom: 4px">
                <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                    ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
            </div>
            <table width="100%">
                <tr>
                    <td style="width:10%;">
                        <label style="width: 150px; display: block; float: left; font-size: 13px; padding: 0px; margin: 0px;">
                            Select License Type :</label>
                    </td>
                    <td style="width:45%;">
                        <asp:DropDownList runat="server" ID="ddlFilterLicenseType" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                            CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlFilterLicenseType_SelectedIndexChanged" />
                    </td>
                    <td style="width:5%;">
                        <label style="width: 100px; display: block; float: left; font-size: 13px;">
                            Filter :</label>
                    </td>
                    <td style="width:30%;">
                        <asp:TextBox runat="server" ID="tbxFilter" Width="250px" MaxLength="50" AutoPostBack="true"
                            OnTextChanged="tbxFilter_TextChanged" />
                    </td>
                    <td style="width:5%;">
                        <asp:LinkButton Text="Apply" runat="server" ID="btnExportExcel" OnClick="btnExportExcel_Click"
                            Width="80%" data-toggle="tooltip" data-placement="bottom" ToolTip="Export to Excel">
                            <img src="/Images/Excel _icon.png" alt="Export" /> 
                        </asp:LinkButton>
                    </td>
                    <td align="right" class="newlink" style="width:5%;">
                        <asp:LinkButton Text="Back" runat="server" ID="btnAddUser" OnClick="btnAddUser_Click" />
                    </td>
                </tr>
            </table>
            <asp:Panel ID="Panel1" Width="100%" Height="450px" ScrollBars="Vertical" runat="server">
                <asp:GridView runat="server" ID="grdMapping" AutoGenerateColumns="false" GridLines="Vertical" AllowSorting="true"
                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" OnRowCreated="grdMapping_RowCreated"
                    CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="100" Width="100%" OnSorting="grdMapping_Sorting"
                    Font-Size="12px" DataKeyNames="LicenseTypeId" OnRowCommand="grdMapping_RowCommand" OnPageIndexChanging="grdMapping_PageIndexChanging"
                    OnRowDataBound="grdMapping_RowDataBound">
                    <Columns>
                        <asp:BoundField DataField="ComplianceID" HeaderText="ComplianceID" ItemStyle-Height="20px" HeaderStyle-Height="20px" HeaderStyle-HorizontalAlign="Left" SortExpression="ComplianceID" />
                        <asp:BoundField DataField="LicenseTypeName" HeaderText="LicenseType Name" HeaderStyle-HorizontalAlign="Left" SortExpression="LicenseTypeName" />
                        <asp:BoundField DataField="ShortDescription" HeaderText="ShortDescription" ItemStyle-Height="20px" HeaderStyle-Height="20px" HeaderStyle-HorizontalAlign="Left" SortExpression="ShortDescription" />
                        <asp:BoundField DataField="Type" HeaderText="Type" ItemStyle-Height="20px" HeaderStyle-Height="20px" HeaderStyle-HorizontalAlign="Left" SortExpression="Type" />
                    </Columns>
                    <FooterStyle BackColor="#CCCC99" />
                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                    <PagerSettings Position="Top" />
                    <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                    <AlternatingRowStyle BackColor="#E6EFF7" />
                    <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                    <EmptyDataTemplate>
                        No Records Found.
                    </EmptyDataTemplate>
                </asp:GridView>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnExportExcel" />
        </Triggers>
    </asp:UpdatePanel>
    <div id="divLicenseTypeDialog">
        <asp:UpdatePanel ID="upLicenseTypeComplianceMapping" runat="server" UpdateMode="Conditional" OnLoad="upLicenseTypeComplianceMapping_Load">
            <ContentTemplate>
                <div style="margin: 5px">
                    <div style="margin-bottom: 4px">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="vdsummary" ValidationGroup="LicenseComplianceValidationGroup" />

                        <asp:CustomValidator ID="cvEmailError" runat="server" EnableClientScript="False"
                            ErrorMessage="Email already exists." ValidationGroup="LicenseComplianceValidationGroup" Display="None" />
                    </div>
                    <div runat="server" id="divCustomer" style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            License Type</label>
                        <asp:DropDownList runat="server" ID="ddlLicenseType" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                            CssClass="txtbox" AutoPostBack="true" />
                        <asp:CompareValidator ID="CompareValidator2" ErrorMessage="Please select license type."
                            ControlToValidate="ddlLicenseType" runat="server" ValueToCompare="-1" Operator="NotEqual"
                            ValidationGroup="LicenseComplianceValidationGroup" Display="None" />
                    </div>
                    <div runat="server" id="divProduct" style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Comma Separated Compliance ID:
                        </label>
                        <asp:TextBox runat="server" ID="txtComplianceIDList" TextMode="MultiLine" Style="height: 100px; width: 390px;" />

                        <asp:RegularExpressionValidator ID="regValidator" runat="server"
                            ControlToValidate="txtComplianceIDList" ErrorMessage="Please Enter Valid Comma Separated ComplianceID !"
                            ValidationGroup="LicenseComplianceValidationGroup" Display="None"
                            ValidationExpression="^([0-9\,]+)">Please Enter Valid Compliance ID</asp:RegularExpressionValidator>

                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                            ControlToValidate="txtComplianceIDList" Display="None"
                            ValidationGroup="LicenseComplianceValidationGroup"
                            ErrorMessage="please enter comma separated ComplianceID"></asp:RequiredFieldValidator>


                    </div>
                    <div style="margin-bottom: 7px; float: right; margin-right: 257px; margin-top: 10px;">
                        <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="button"
                            ValidationGroup="LicenseComplianceValidationGroup" />
                        <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="button" OnClientClick="$('#divLicenseTypeDialog').dialog('close');" />
                    </div>
                </div>

                <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 10px;">
                    <p style="color: red;"><strong>Note:</strong> (*) fields are Compulsory</p>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
