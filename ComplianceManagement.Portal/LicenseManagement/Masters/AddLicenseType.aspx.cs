﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Logger;
using System.Reflection;
using System.Linq;
using System.Web.Security;
using System.Web;
using com.VirtuosoITech.ComplianceManagement.Business.License;
using System.Collections.Generic;

namespace com.VirtuosoITech.ComplianceManagement.Portal.LicenseManagement.Masters
{
    public partial class AddLicenseType : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (HttpContext.Current.Request.IsAuthenticated && AuthenticationHelper.Role != "EXCT")
                {
                    if (HttpContext.Current.Request.IsAuthenticated && AuthenticationHelper.Role != "MGMT")
                    {
                        BindLicense();
                        if ((AuthenticationHelper.Role.Equals("SADMN") || AuthenticationHelper.Role.Equals("IMPT")))
                        {
                            btnAddLicense.Visible = true;
                        }
                    }
                    else
                    {
                        //added by rahul on 12 June 2018 Url Sequrity
                        FormsAuthentication.SignOut();
                        Session.Abandon();
                        FormsAuthentication.RedirectToLoginPage();
                    }
                }
                else
                {
                    //added by rahul on 12 June 2018 Url Sequrity
                    FormsAuthentication.SignOut();
                    Session.Abandon();
                    FormsAuthentication.RedirectToLoginPage();
                }
            }
        }
        protected void rdbLicenseType_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        private void BindLicense()
        {
            try
            {
                grdLicense.DataSource = null;
                grdLicense.DataBind();
                grdLicense.DataSource = LicenseTypeMasterManagement.GetAll("S", tbxFilter.Text);
                grdLicense.DataBind();
                upLicenseList.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdLicense_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("EDIT_License"))
                {
                    int licenseId = Convert.ToInt32(e.CommandArgument);
                    ViewState["Mode"] = 1;
                    ViewState["LicenseID"] = licenseId;
                    txtComplianceIDList.Text = string.Empty;
                    var complianceList = LicenseTypeMasterManagement.GetLicenseTypeCompliance(licenseId, "S");
                    if (complianceList.Count > 0)
                    {
                        var str = String.Join(",", complianceList);
                        txtComplianceIDList.Text = str.Trim();
                    }

                    var ltlm = LicenseTypeMasterManagement.GetByID(licenseId);
                    if (ltlm.Is_Statutory_NoStatutory == "S")
                    {
                        rdbLicenseType.ClearSelection();
                        rdbLicenseType.Items.FindByValue("S").Selected = true;
                    }
                    else
                    {
                        rdbLicenseType.ClearSelection();
                        rdbLicenseType.Items.FindByValue("N").Selected = true;
                    }
                    tbxName.Text = ltlm.Name;
                    tbxName.ToolTip = ltlm.Name;
                    upLicense.Update();
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divLicenseDialog\").dialog('open')", true);
                }
                else if (e.CommandName.Equals("DELETE_License"))
                {
                    int licenseId = Convert.ToInt32(e.CommandArgument);
                    if (!CheckLicenseTypeTransactionExists(licenseId))
                    {
                        LicenseTypeMasterManagement.Delete(licenseId, AuthenticationHelper.UserID);
                        BindLicense();
                    }
                    else
                    {
                        cvDuplicateEntryTop.IsValid = false;
                        cvDuplicateEntryTop.ErrorMessage = "License type can not be deleted ,because it is tagged to compliances which are already performed.";
                    }

                }
                else if (e.CommandName.Equals("VIEW_Compliances"))
                {
                    int licenseId = Convert.ToInt32(e.CommandArgument);
                    Response.Redirect("~/LicenseManagement/Masters/LicenseTypeComplianceMappingList.aspx?LTMLID=" + licenseId, false);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdLicense_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdLicense.PageIndex = e.NewPageIndex;
                BindLicense();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnAddLicense_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["Mode"] = 0;
                tbxName.Text = txtComplianceIDList.Text = string.Empty;
                upLicense.Update();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divLicenseDialog\").dialog('open')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                grdLicense.PageIndex = 0;
                BindLicense();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static List<string> UpdateComplianceMapping(long LicenseTypeID, long Updatedby)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<string> emsg = new List<string>();
                var ids = (from row in entities.Lic_tbl_LicType_Compliance_Mapping
                           where row.LicenseTypeID == LicenseTypeID
                           select row).ToList();

                ids.ForEach(entry =>
                {
                    if (!CheckTransactionExists(entry.ComplianceID))
                    {
                        var prevmappedids = (from row in entities.Lic_tbl_LicType_Compliance_Mapping
                                             where row.ID == entry.ID
                                             select row).FirstOrDefault();

                        prevmappedids.IsDeleted = true;
                        prevmappedids.UpdatedBy = Updatedby;
                        prevmappedids.UpdatedOn = DateTime.Now;
                    }
                    else
                    {
                        emsg.Add("This compliance " + entry.ComplianceID + " has transaction so it could not be deleted");
                    }
                });
                entities.SaveChanges();
                return emsg.ToList();
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                List<string> ComplianceTextboxList = new List<string>();
                if (!string.IsNullOrEmpty(rdbLicenseType.SelectedValue))
                {
                    List<string> emsg = new List<string>();
                    Lic_tbl_LicenseType_Master ltlm = new Lic_tbl_LicenseType_Master()
                    {
                        Name = tbxName.Text.Trim(),
                        IsDeleted = false,
                        CreatedBy = AuthenticationHelper.UserID,
                        CreatedOn = DateTime.Now,
                        Is_Statutory_NoStatutory = rdbLicenseType.SelectedValue,
                    };
                    if ((int)ViewState["Mode"] == 1)
                    {
                        ltlm.ID = Convert.ToInt32(ViewState["LicenseID"]);
                    }
                    if (LicenseTypeMasterManagement.Exists(ltlm))
                    {
                        cvDuplicateEntry.ErrorMessage = "License name already exists.";
                        cvDuplicateEntry.IsValid = false;
                        return;
                    }
                    ComplianceTextboxList = txtComplianceIDList.Text.Trim().Split(',').ToList();
                    string complianceIDs = checkComplianceType(ComplianceTextboxList);
                    if (complianceIDs!="")
                    {
                        cvDuplicateEntry.ErrorMessage = complianceIDs + " Checklist Compliance ID's Please Remove it. ";
                        cvDuplicateEntry.IsValid = false;
                    }

                    else if ((int)ViewState["Mode"] == 0)
                    {
                        #region Create
                        LicenseTypeMasterManagement.Create(ltlm);

                        List<Lic_tbl_LicType_Compliance_Mapping> LTLCMlist = new List<Lic_tbl_LicType_Compliance_Mapping>();
                        
                        List<long> ComplianceList = new List<long>();
                        ComplianceTextboxList = txtComplianceIDList.Text.Trim().Split(',').ToList();
                      
                        ComplianceList = ComplianceTextboxList.Select(long.Parse).Distinct().ToList();
                        var NatureofCompliancelist = NatureOfComplianceExists(ComplianceList);
                        var checkcompliance = Exists(NatureofCompliancelist);
                        if (checkcompliance.Count == 0)
                        {
                            foreach (long clist in NatureofCompliancelist)
                            {
                                if (!LTLCMlist.Any(x => x.ComplianceID == Convert.ToInt32(clist) && x.LicenseTypeID == Convert.ToInt32(ltlm.ID)))
                                {
                                    Lic_tbl_LicType_Compliance_Mapping LTLCM = new Lic_tbl_LicType_Compliance_Mapping();
                                    LTLCM.LicenseTypeID = Convert.ToInt32(ltlm.ID);
                                    LTLCM.ComplianceID = Convert.ToInt32(clist);
                                    LTLCM.CreatedBy = AuthenticationHelper.UserID;
                                    LTLCM.IsDeleted = false;
                                    LTLCMlist.Add(LTLCM);
                                }
                            }
                            bool checkFlag = false;
                            checkFlag = LicenseTypeMasterManagement.BlukLTLCMComplianceMapping(LTLCMlist);
                            if (checkFlag)
                            {
                                if (emsg.Count > 0)
                                {
                                    ErrorMessages(emsg.ConvertAll<string>(row => row.ToString()).ToList());
                                }
                                else
                                {

                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Record Save Sucessfully";

                                    //ScriptManager.RegisterStartupScript(this.upLicense, this.upLicense.GetType(), "CloseDialog", "$(\"#divLicenseTypeDialog\").dialog('close')", true);
                                }
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                            }
                        }
                        else
                        {
                            if (NatureofCompliancelist.Count > 0)
                            {
                                ErrorMessages(NatureofCompliancelist.ConvertAll<string>(row => row.ToString()).ToList());
                            }
                        }
                        #endregion
                    }
                    else if ((int)ViewState["Mode"] == 1)
                    {
                        List<string> firstemsg = new List<string>();
                        List<string> finalemsg = new List<string>();
                        LicenseTypeMasterManagement.Update(ltlm);
                        List<Lic_tbl_LicType_Compliance_Mapping> LTLCMlist = new List<Lic_tbl_LicType_Compliance_Mapping>();
                      //  List<string> ComplianceTextboxList = new List<string>();
                        List<long> ComplianceList = new List<long>();
                        ComplianceTextboxList = txtComplianceIDList.Text.Split(',').ToList();
                        ComplianceList = ComplianceTextboxList.Select(long.Parse).Distinct().ToList();
                        firstemsg = UpdateComplianceMapping(ltlm.ID, AuthenticationHelper.UserID);
                        if (firstemsg.Count > 0)
                        {
                            finalemsg.Add(ErrorMessages1(firstemsg.ConvertAll<string>(row => row.ToString()).ToList()));
                        }
                        var NatureofCompliancelist = NatureOfComplianceExists(ComplianceList);
                        var checkcompliance = Exists(NatureofCompliancelist);
                        if (checkcompliance.Count == 0)
                        {
                            foreach (long clist in NatureofCompliancelist)
                            {
                                if (!LTLCMlist.Any(x => x.ComplianceID == Convert.ToInt32(clist) && x.LicenseTypeID == Convert.ToInt32(ltlm.ID)))
                                {
                                    Lic_tbl_LicType_Compliance_Mapping LTLCM = new Lic_tbl_LicType_Compliance_Mapping();
                                    LTLCM.LicenseTypeID = Convert.ToInt32(ltlm.ID);
                                    LTLCM.ComplianceID = Convert.ToInt32(clist);
                                    LTLCM.CreatedBy = AuthenticationHelper.UserID;
                                    LTLCM.IsDeleted = false;
                                    LTLCMlist.Add(LTLCM);
                                }
                            }//foreach end
                        }
                        else
                        {
                            var complianceList = ComplianceList.Except(NatureofCompliancelist);
                            foreach (long clist in NatureofCompliancelist)
                            {
                                if (!LTLCMlist.Any(x => x.ComplianceID == Convert.ToInt32(clist) && x.LicenseTypeID == Convert.ToInt32(ltlm.ID)))
                                {
                                    Lic_tbl_LicType_Compliance_Mapping LTLCM = new Lic_tbl_LicType_Compliance_Mapping();
                                    LTLCM.LicenseTypeID = Convert.ToInt32(ltlm.ID);
                                    LTLCM.ComplianceID = Convert.ToInt32(clist);
                                    LTLCM.CreatedBy = AuthenticationHelper.UserID;
                                    LTLCM.IsDeleted = false;
                                    LTLCMlist.Add(LTLCM);
                                }
                            }//foreach end

                            if (NatureofCompliancelist.Count > 0)
                            {
                                finalemsg.Add(ErrorMessages(NatureofCompliancelist.ConvertAll<string>(row => row.ToString()).ToList()));
                            }
                        }
                        bool checkFlag = false;
                        checkFlag = LicenseTypeMasterManagement.BlukLTLCMComplianceMapping(LTLCMlist);
                        if (checkFlag)
                        {
                            if (finalemsg.Count > 0)
                            {
                                FinalErrorMessages(finalemsg);
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Record Updated Sucessfully";
                            }
                            //ScriptManager.RegisterStartupScript(this.upLicense, this.upLicense.GetType(), "CloseDialog", "$(\"#divLicenseTypeDialog\").dialog('close')", true);
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                        }
                    }
                    BindLicense();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public string checkComplianceType(List<string> _obj)
        {
           
            string complianceIDs = "";
          
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var result = (from row in entities.Compliances
                                  where _obj.Contains(row.ID.ToString())
                                  select row).ToList();

                    foreach (var d in result)
                    {
                        if (d.ComplianceType == 1)
                        {
                           // isvaild = false;
                            complianceIDs = complianceIDs + d.ID.ToString() + ',';
                           
                        }
                    }
                    if (complianceIDs!="") {
                        complianceIDs = complianceIDs.Substring(0, complianceIDs.Length - 1);
                    }
                }
            }
            catch (Exception ex)
            { complianceIDs = ""; }
            return complianceIDs;

        }

        public static List<long> NatureOfComplianceExists(List<long> complianceIDlist)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<long> existscompliancelist = new List<long>();
                entities.Database.CommandTimeout = 180;
                var query = (from row in entities.Compliances
                             where row.IsDeleted == false &&
                             complianceIDlist.Contains(row.ID)
                             //&&
                             //(row.NatureOfCompliance==8 ||row.NatureOfCompliance==13)
                             select row.ID).Distinct().ToList();
                if (query.Count > 0)
                {

                    existscompliancelist = query.ToList();
                }
                return existscompliancelist;
            }
        }
        public static List<long> Exists(List<long> complianceIDlist)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<long> existscompliancelist = new List<long>();
                var query = (from row in entities.Lic_tbl_LicType_Compliance_Mapping
                             where row.IsDeleted == false
                             && complianceIDlist.Contains(row.ComplianceID)
                             select row.ComplianceID).Distinct().ToList();
                if (query.Count > 0)
                {
                    existscompliancelist = query.ToList();
                }
                return existscompliancelist;
            }
        }
        public static bool UpdateExists(long complianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Lic_tbl_LicType_Compliance_Mapping
                             where row.IsDeleted == false
                             && row.ComplianceID == complianceID
                             select row).ToList();
                if (query.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public static bool CheckLicenseTypeTransactionExists(long LicenseType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.LIC_Sp_CheckLicenseTypeISAssigned(LicenseType)
                             select row).ToList();
                if (query.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public static bool CheckTransactionExists(long complianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.LIC_Sp_CheckComplianceISAssigned(complianceID)
                             select row).ToList();
                if (query.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public string ErrorMessages1(List<string> emsg)
        {

            string finalErrMsg = string.Empty;

            //finalErrMsg += "<ol type='1'>";
            finalErrMsg += "<ul>";
            //finalErrMsg += "<li>Following complianceid already mapped with license type</li>";
            if (emsg.Count > 0)
            {
                emsg.ForEach(eachErrMsg =>
                {
                    finalErrMsg += "<li>" + eachErrMsg + "</li>";
                });

                finalErrMsg += "</ul>";
            }
            return finalErrMsg;
            //cvDuplicateEntry.IsValid = false;
            //cvDuplicateEntry.ErrorMessage = finalErrMsg;
        }
        public string ErrorMessages(List<string> emsg)
        {

            string finalErrMsg = string.Empty;

            //finalErrMsg += "<ol type='1'>";
            finalErrMsg += "<ul>";
            finalErrMsg += "<li>Following complianceid already mapped with license type</li>";
            if (emsg.Count > 0)
            {
                emsg.ForEach(eachErrMsg =>
                {
                    finalErrMsg += "<li>" + eachErrMsg + "</li>";
                });

                finalErrMsg += "</ul>";
            }
            return finalErrMsg;
            //cvDuplicateEntry.IsValid = false;
            //cvDuplicateEntry.ErrorMessage = finalErrMsg;
        }

        public void FinalErrorMessages(List<string> emsg)
        {
            string finalErrMsg = string.Empty;
            finalErrMsg += "<ul>";
            if (emsg.Count > 0)
            {
                emsg.ForEach(eachErrMsg =>
                {
                    finalErrMsg += "<li>" + eachErrMsg + "</li>";
                });

                finalErrMsg += "</ul>";
            }
            cvDuplicateEntry.IsValid = false;
            cvDuplicateEntry.ErrorMessage = finalErrMsg;
        }
        protected void upLicense_Load(object sender, EventArgs e)
        {
        }
        protected void ddlfilterStatutoryNonStatutory_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindLicense();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void grdLicense_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                var licenseList = LicenseTypeMasterManagement.GetAll("S", tbxFilter.Text);
                if (direction == SortDirection.Ascending)
                {
                    licenseList = licenseList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                }
                else
                {
                    licenseList = licenseList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                }
                foreach (DataControlField field in grdLicense.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdLicense.Columns.IndexOf(field);
                    }
                }
                grdLicense.DataSource = licenseList;
                grdLicense.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdLicense_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        protected void grdLicense_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    LinkButton lbtEdit = (LinkButton)e.Row.FindControl("lbtEdit");
                    LinkButton lbtDelete = (LinkButton)e.Row.FindControl("lbtDelete");
                    lbtEdit.Visible = false;
                    lbtDelete.Visible = false;
                    if (AuthenticationHelper.Role.Equals("SADMN"))
                    {
                        lbtEdit.Visible = true;
                        lbtDelete.Visible = true;
                    }
                    if (AuthenticationHelper.Role.Equals("IMPT"))
                    {
                        lbtEdit.Visible = true;
                        lbtDelete.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
    }
}