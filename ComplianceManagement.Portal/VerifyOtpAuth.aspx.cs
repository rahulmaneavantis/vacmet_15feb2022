﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal
{
    public partial class VerifyOtpAuth : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                lblmsg.Visible = false;
                lblmsg.Text = "";
                bool ValidUserFlag = false;

                string Email = string.Empty;
                string UserEmail = string.Empty;
                
                if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["Email"])))
                {
                    Email = Convert.ToString(Request.QueryString["Email"]);

                    if (!string.IsNullOrEmpty(Email))
                        UserEmail = getDecryptedText(Email);
                }
                if (!string.IsNullOrEmpty(UserEmail))
                {
                    User user = null;
                    if (UserManagement.IsValidUser(UserEmail, out user))
                    {
                        try
                        {
                            string ipaddress = string.Empty;
                            string Macaddress = string.Empty;

                            Macaddress = Util.GetMACAddress();
                            ipaddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                            if (ipaddress == "" || ipaddress == null)
                                ipaddress = Request.ServerVariables["REMOTE_ADDR"];
                            try
                            {                               
                                UserLoginTrack obj = new UserLoginTrack()
                                {
                                    Email = UserEmail,
                                    LoginDate = DateTime.Now,
                                    IPAddress = ipaddress,
                                    MACAddress = Macaddress,
                                    LoginFlag = false
                                };
                                UserManagement.CreateUserLoginTrack(obj);
                                var blockip = UserManagement.GetBlockIpAddress((int)user.CustomerID, user.ID, ipaddress);
                                if (!blockip.Item1)
                                {
                                    lblmsg.Visible = true;
                                    lblmsg.Text = "Your Account is Disabled. Please Contact to Admin.";
                                    return;
                                }
                                //var blockip = UserManagement.GetBlockIpAddress();
                                //if (blockip.Contains(ipaddress))
                                //{
                                //    lblmsg.Visible = true;
                                //    lblmsg.Text = "Your Account is Disabled. Please Contact to Admin.";
                                //    return;
                                //}
                            }
                            catch (Exception ex)
                            {
                                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                            }
                          

                            bool Auditorexpired = false;
                            if (user.RoleID == 9)
                            {
                                if (user.Enddate == null)
                                {
                                    Auditorexpired = true;
                                }
                                if (DateTime.Now.Date > Convert.ToDateTime(user.Enddate).Date || DateTime.Now < Convert.ToDateTime(user.Startdate).Date)
                                {
                                    Auditorexpired = true;
                                }
                            }
                            if (!Auditorexpired)
                            {
                                if (!(user.WrongAttempt >= 3)) /*UserManagement.WrongAttemptCount(txtemail.Text.Trim())*/
                                {
                                    if (user.IsActive)
                                    {
                                        int? otpchk = 0;
                                        VerifyOTP OTPData = VerifyOTPManagement.GetByID(Convert.ToInt32(user.ID));
                                        if (OTPData != null)
                                        {
                                            if (OTPData.IsVerified == false)
                                            {
                                                otpchk = OTPData.OTP;
                                            }
                                        }
                                        Session["ResendOTP"] = Convert.ToString(otpchk);
                                        Session["userID"] = user.ID;
                                        Session["ContactNo"] = user.ContactNumber;
                                        Session["Email"] = user.Email;
                                        Session["CustomerID_new"] = user.CustomerID;
                                        if (user.RoleID == 9)
                                        {
                                            Session["Auditstartdate"] = user.AuditStartPeriod;
                                            Session["Auditenddate"] = user.AuditEndPeriod;
                                        }
                                        DateTime LastPasswordChangedDate = Convert.ToDateTime(user.ChangPasswordDate); /*UserManagement.GetByID(Convert.ToInt32(user.ID)*/
                                        DateTime currentDate = DateTime.Now;
                                        LastPasswordChangedDate = LastPasswordChangedDate != null ? LastPasswordChangedDate : DateTime.Now;
                                        int noDays = Convert.ToInt32(ConfigurationManager.AppSettings["ChangedPasswordDays"]);
                                        int customerID = 0;
                                        customerID = Convert.ToInt32(user.CustomerID);//UserManagement.GetByID(Convert.ToInt32(user.ID)).CustomerID ?? 0;
                                        if (customerID == 6 || customerID == 5 || customerID == 14)
                                        {
                                            noDays = 90;
                                        }
                                        int dateDifference = Convert.ToInt32((currentDate - LastPasswordChangedDate).TotalDays);

                                        if (dateDifference >= noDays)
                                        {
                                            Session["ChangePassword"] = true;
                                            Response.Redirect("~/Account/ChangePassword.aspx", false);
                                        }

                                        else if (user.LastLoginTime != null)
                                        {
                                            bool Success = true;
                                            MaintainLoginDetail objData = new MaintainLoginDetail()
                                            {
                                                UserId = Convert.ToInt32(user.ID),
                                                Email = user.Email,
                                                CreatedOn = DateTime.Now,
                                                IPAddress = ipaddress,
                                                MACAddress = Macaddress,
                                                LoginFrom = "WC",
                                                //ProfileID=user.ID
                                            };
                                            UserManagement.Create(objData);

                                            if (Success)
                                            {
                                                Session["RM"] = false;//cbRememberMe.Checked;
                                                Session["EA"] = Util.CalculateAESHash(UserEmail);
                                                Session["MA"] = Util.CalculateAESHash(Macaddress.Trim());
                                                Response.Redirect("~/Users/OTPVerify.aspx", false);
                                            }
                                        }
                                        else
                                        {
                                            Response.Redirect("~/Account/ChangePassword.aspx", false);
                                        }
                                    }
                                    else
                                    {
                                        lblmsg.Visible = true;
                                        lblmsg.Text = "Your Account is Disabled.";
                                    }
                                }
                                else
                                {
                                    Session["otpvrifyWattp"] = true;
                                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "SignIn", "settracknew('Login', 'LoginUnsucessful', 'Page-Login', '0');", true);
                                }
                            }
                            else
                            {
                                lblmsg.Visible = true;
                                lblmsg.Text = "Your Account is Disabled.";
                            }

                        }
                        catch (Exception ex)
                        {
                            LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                        }
                    }
                    else {
                        //cvLogin.IsValid = false;
                        //cvLogin.ErrorMessage = "Incorrect User.";
                        lblmsg.Visible = true;
                        lblmsg.Text = "Incorrect User.";
                    }
                }

            }

        }


        public static byte[] DecryptIOS(byte[] encryptedData, RijndaelManaged rijndaelManaged)
        {
            byte[] decode = null;
            try
            {
                rijndaelManaged = GetRijndaelManagedIOS(key);
                decode = rijndaelManaged.CreateDecryptor()
               .TransformFinalBlock(encryptedData, 0, encryptedData.Length);
            }
            catch { }
            return decode;
        }
        public static string key = "avantis";
        public static string getDecryptedText(string text)
        {
            string DecryptedString = string.Empty;
            text = text.Replace(" ", "+");
            var encryptedBytes = Convert.FromBase64String(text);
            
                try
                {
                DecryptedString = Encoding.UTF8.GetString(DecryptIOS(encryptedBytes, GetRijndaelManagedIOS(key)));
                //DecryptedString = Encoding.UTF8.GetString(Decrypt(encryptedBytes, GetRijndaelManaged(key))); for android
                }
                catch { }
            
            return DecryptedString;
        }
        public static RijndaelManaged GetRijndaelManaged(String secretKey)
        {
            var keyBytes = new byte[16];
            var secretKeyBytes = Encoding.UTF8.GetBytes(secretKey);
            Array.Copy(secretKeyBytes, keyBytes, Math.Min(keyBytes.Length, secretKeyBytes.Length));
            return new RijndaelManaged
            {
                Mode = CipherMode.CBC,
                Padding = PaddingMode.PKCS7,
                KeySize = 128,
                BlockSize = 128,
                Key = keyBytes,
                IV = keyBytes
            };
        }
        public static byte[] Decrypt(byte[] encryptedData, RijndaelManaged rijndaelManaged)
        {
            byte[] decode = null;
            try
            {
                decode = rijndaelManaged.CreateDecryptor()
                .TransformFinalBlock(encryptedData, 0, encryptedData.Length);
            }
            catch
            {
                rijndaelManaged = GetRijndaelManagedIOS(key);
                decode = rijndaelManaged.CreateDecryptor()
               .TransformFinalBlock(encryptedData, 0, encryptedData.Length);
            }
            return decode;
        }
        public static RijndaelManaged GetRijndaelManagedIOS(String secretKey)
        {
            var keyBytes = new byte[16];
            var secretKeyBytes = Encoding.UTF8.GetBytes(secretKey);
            Array.Copy(secretKeyBytes, keyBytes, Math.Min(keyBytes.Length, secretKeyBytes.Length));
            return new RijndaelManaged
            {
                Mode = CipherMode.ECB,
                Padding = PaddingMode.PKCS7,
                KeySize = 128,
                BlockSize = 128,
                Key = keyBytes,
                IV = keyBytes
            };
        }




    }
}