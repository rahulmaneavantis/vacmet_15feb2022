﻿using System;
using System.Web.UI;
using System.Linq;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Globalization;
using System.Reflection;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Users
{
    public partial class UserReminders : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (AuthenticationHelper.CustomerID == 63)
                {
                    ddlDocType.SelectedValue = "0";
                    ddlDocType.Items.Remove(ddlDocType.Items.FindByValue("-1"));

                    ddlComType.SelectedValue = "0";

                    ddlComType.Items.Remove(ddlComType.Items.FindByValue("-1"));

                }

                BindUserReminders();
                BindInputLists();
                BindLocationFilter();

                FillUserReminders();

                tbxDate.Attributes.Add("readonly", "readonly");

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }           
        }

        protected void upUserReminders_Load(object sender, EventArgs e)
        {
            try
            {
                //if (ViewState["AssignedCompliances"] != null)//commented by Manisha
                //{
                    List<ComplianceInstanceAssignmentView> assignedCompliances = (List<ComplianceInstanceAssignmentView>)ViewState["AssignedCompliances"];

                    DateTime date = DateTime.MinValue;
                    if (DateTime.TryParseExact(tbxDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                    {
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", "initializeDatePicker(null);", true);
                    }

                   // ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
                //}
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void upUserReminderList_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindLocationFilter()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                var bracnhes = CustomerBranchManagement.GetAllHierarchySatutory(customerID);
                tvFilterLocation.Nodes.Clear();
                string isstatutoryinternal = "";
                if (ddlDocType.SelectedItem.Text == "Statutory")
                {
                    isstatutoryinternal = "S";
                }
                else if (ddlDocType.SelectedItem.Text == "Internal")
                {
                    isstatutoryinternal = "I";
                }
               
                var LocationList = CustomerBranchManagement.GetAssignedLocationList(AuthenticationHelper.UserID, customerID, AuthenticationHelper.Role, isstatutoryinternal);

                TreeNode node = new TreeNode("Entity/Sub-Entity/Location", "-1");
                node.Selected = true;
                tvFilterLocation.Nodes.Add(node);

                foreach (var item in bracnhes)
                {
                    node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    CustomerBranchManagement.BindBranchesHierarchy(node, item, LocationList);
                    tvFilterLocation.Nodes.Add(node);
                }

                tvFilterLocation.CollapseAll();
                tvFilterLocation_SelectedNodeChanged(null, null);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp)
        {
            try
            {
                foreach (var item in nvp.Children)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    BindBranchesHierarchy(node, item);
                    parent.ChildNodes.Add(node);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        //private void BindLocation()
        //{
        //    try
        //    {
        //        ddlLocation.DataTextField = "Name";
        //        ddlLocation.DataValueField = "ID";

        //        ddlLocation.DataSource = CustomerManagement.GetCustomerBranchList(AuthenticationHelper.UserID);
        //        ddlLocation.DataBind();

        //        ddlLocation.Items.Insert(0, new ListItem("Location", "-1"));
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //}

        private void BindInputLists()
        {
            try
            {
                List<ComplianceInstanceAssignmentView> assignedCompliances = Business.ComplianceManagement.GetAllInstances(userID: AuthenticationHelper.UserID);

                //ViewState["AssignedCompliances"] = assignedCompliances;//commented by manisha

                //if ((assignedCompliances != null) && (assignedCompliances.Count > 0))
                //{
                //    ViewState["AssignedCompliances"] = assignedCompliances;//added by Manisha and commented by Manisha
                //}

                ddlBranch.DataSource = assignedCompliances.Select(entry => new { ID = entry.CustomerBranchID, Name = entry.Branch }).Distinct().OrderBy(entry => entry.Name).ToList();
                ddlBranch.DataBind();
                ddlBranch.Items.Insert(0, new ListItem("< Select >", "-1"));

                ddlBranch_SelectedIndexChanged(null, null);

                ////--------added by Manisha-------------------------------------------------------------------------
                //ddlCompliance.DataSource = assignedCompliances.Select(entry => new { ID = entry.ComplianceID, Name = entry.ShortDescription }).Distinct().OrderBy(entry => entry.Name).ToList();
                //ddlCompliance.DataBind();
                //ddlCompliance.Items.Insert(0, new ListItem("< Select >", "-1"));

                //ddlCompliance_SelectedIndexChanged(null, null);
                ////---------added by Manisha-----------------------------------------------------------
                //ddlRole.DataSource = assignedCompliances.Select(entry => new { ID = entry.RoleID, Name = entry.Role }).Distinct().OrderBy(entry => entry.Name).ToList();
                //ddlRole.DataBind();
                //ddlRole.Items.Insert(0, new ListItem("< Select >", "-1"));
                              

                //----------------------------------------------------------------------------------
               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindInputListsInternal()
        {
            try
            {
                List<InternalComplianceInstanceAssignmentView> assignedCompliances = Business.InternalComplianceManagement.GetAllInstances(userID: AuthenticationHelper.UserID);

                //ViewState["AssignedCompliances"] = assignedCompliances;//commented by manisha

                //if ((assignedCompliances != null) && (assignedCompliances.Count > 0))
                //{
                //    ViewState["AssignedCompliances"] = assignedCompliances;//added by Manisha and commented by Manisha
                //}

                ddlBranch.DataSource = assignedCompliances.Select(entry => new { ID = entry.CustomerBranchID, Name = entry.Branch }).Distinct().OrderBy(entry => entry.Name).ToList();
                ddlBranch.DataBind();
                ddlBranch.Items.Insert(0, new ListItem("< Select >", "-1"));

                ddlBranch_SelectedIndexChanged(null, null);

                ////--------added by Manisha-------------------------------------------------------------------------
                //ddlCompliance.DataSource = assignedCompliances.Select(entry => new { ID = entry.ComplianceID, Name = entry.ShortDescription }).Distinct().OrderBy(entry => entry.Name).ToList();
                //ddlCompliance.DataBind();
                //ddlCompliance.Items.Insert(0, new ListItem("< Select >", "-1"));

                //ddlCompliance_SelectedIndexChanged(null, null);
                ////---------added by Manisha-----------------------------------------------------------
                //ddlRole.DataSource = assignedCompliances.Select(entry => new { ID = entry.RoleID, Name = entry.Role }).Distinct().OrderBy(entry => entry.Name).ToList();
                //ddlRole.DataBind();
                //ddlRole.Items.Insert(0, new ListItem("< Select >", "-1"));


                //----------------------------------------------------------------------------------

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            SelectedPageNo.Text = "1";

            grdUserReminder.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            grdUserReminder.PageIndex = 0;

            FillUserReminders();            
        }

        protected void ddlComType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlComType.SelectedValue == "-1")
                {
                    BindInputLists();
                }
                else if (ddlComType.SelectedValue == "0")
                {
                    BindInputListsInternal();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //if (!IsValid()) { return; };

                SelectedPageNo.Text = "1";

                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo).ToString();
                }

                grdUserReminder.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                grdUserReminder.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                
                //Reload the Grid
                FillUserReminders();

                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                // ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        

        public void FillUserReminders()
        {
            try
            {  
                int risk = Convert.ToInt32(ddlRiskType.SelectedValue);                

                String location = tvFilterLocation.SelectedNode.Text;

                if (ddlDocType.SelectedValue == "-1")
                {
                    var UserReminders = Business.ComplianceManagement.GetComplianceReminderNotificationsByUserID(AuthenticationHelper.UserID, tbxFilter.Text).ToList();

                    if (risk != -1)
                        UserReminders = UserReminders.Where(entry => entry.RiskType== risk).ToList();

                    if (location != "Entity/Sub-Entity/Location")
                        UserReminders = UserReminders.Where(entry => entry.CustomerBranchName == location).ToList();

                    grdUserReminder.DataSource = UserReminders;
                    Session["TotalRows"] = UserReminders.Count;
                    grdUserReminder.DataBind();
                }

                else if(ddlDocType.SelectedValue == "0")
                {
                    var UserReminders = Business.InternalComplianceManagement.GetComplianceReminderNotificationsByUserID(AuthenticationHelper.UserID, tbxFilter.Text).ToList();

                    if (risk != -1)
                        UserReminders = UserReminders.Where(entry => entry.IRiskType== risk).ToList();

                    if (location != "Entity/Sub-Entity/Location")
                        UserReminders = UserReminders.Where(entry => entry.CustomerBranchName == location).ToList();

                    grdUserReminder.DataSource = UserReminders;
                    Session["TotalRows"] = UserReminders.Count;
                    grdUserReminder.DataBind();
                }

                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlBranch_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
               if(ddlComType.SelectedValue=="-1")
                { 
                    List<ComplianceInstanceAssignmentView> assignedCompliances = Business.ComplianceManagement.GetAllInstances(userID: AuthenticationHelper.UserID);

                    ddlCompliance.ClearSelection();
                    ddlCompliance.DataSource = null;
                    ddlCompliance.DataBind();

                    ddlCompliance.DataSource = assignedCompliances.Where(entry => entry.CustomerBranchID == Convert.ToInt32(ddlBranch.SelectedValue)).Select(entry => new { ID = entry.ComplianceID, Name = entry.ShortDescription })
                        .OrderBy(entry => entry.Name).Distinct().ToList();
                    ddlCompliance.DataBind();

                    ddlCompliance.Items.Insert(0, new ListItem("< Select >", "-1"));
                    ddlCompliance_SelectedIndexChanged(null, null);
                }

               else if (ddlComType.SelectedValue == "0")
                {
                    List<InternalComplianceInstanceAssignmentView> assignedCompliances = Business.InternalComplianceManagement.GetAllInstances(userID: AuthenticationHelper.UserID);

                    ddlCompliance.ClearSelection();
                    ddlCompliance.DataSource = null;
                    ddlCompliance.DataBind();

                    ddlCompliance.DataSource = assignedCompliances.Where(entry => entry.CustomerBranchID == Convert.ToInt32(ddlBranch.SelectedValue)).Select(entry => new { ID = entry.ComplianceID, Name = entry.IShortDescription })
                        .OrderBy(entry => entry.Name).Distinct().ToList();
                    ddlCompliance.DataBind();

                    ddlCompliance.Items.Insert(0, new ListItem("< Select >", "-1"));
                    ddlCompliance_SelectedIndexChanged(null, null);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlCompliance_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlComType.SelectedValue == "-1")
                {
                    List<ComplianceInstanceAssignmentView> assignedCompliances = Business.ComplianceManagement.GetAllInstances(userID: AuthenticationHelper.UserID);
                    ddlRole.ClearSelection();
                    ddlRole.DataSource = null;
                    ddlRole.DataBind();

                    //if (ViewState["AssignedCompliances"] != null)//added by manisha
                    if (assignedCompliances != null)//added by manisha
                    {
                        if (assignedCompliances.Count > 0)//added by manisha
                        {
                            if ((ddlCompliance.SelectedValue != null) && (ddlCompliance.SelectedValue != string.Empty) && (ddlCompliance.SelectedValue != "-1")) //added by manisha
                            {
                                ddlRole.DataSource = assignedCompliances.Where(entry => entry.CustomerBranchID == Convert.ToInt32(ddlBranch.SelectedValue) && entry.ComplianceID == Convert.ToInt32(ddlCompliance.SelectedValue)).Select(entry => new { ID = entry.RoleID, Name = entry.Role })
                                    .OrderBy(entry => entry.Name).Distinct().ToList();
                                ddlRole.DataBind();

                                ddlRole.Items.Insert(0, new ListItem("< Select >", "-1"));


                                DateTime scheduledOn = assignedCompliances.Where(entry => entry.ComplianceID == Convert.ToInt32(ddlCompliance.SelectedValue) && entry.CustomerBranchID == Convert.ToInt32(ddlBranch.SelectedValue)).Select(entry => entry.ScheduledOn).FirstOrDefault();
                                if (scheduledOn != DateTime.MinValue)
                                {
                                    litScheduledOn.Text = scheduledOn == null ? string.Empty : scheduledOn.ToString("dd-MM-yyyy");
                                }
                            }
                        }
                    }
                }

                else if (ddlComType.SelectedValue == "0")
                {
                    List<InternalComplianceInstanceAssignmentView> assignedCompliances = Business.InternalComplianceManagement.GetAllInstances(userID: AuthenticationHelper.UserID);
                    ddlRole.ClearSelection();
                    ddlRole.DataSource = null;
                    ddlRole.DataBind();

                    //if (ViewState["AssignedCompliances"] != null)//added by manisha
                    if (assignedCompliances != null)//added by manisha
                    {
                        if (assignedCompliances.Count > 0)//added by manisha
                        {
                            if ((ddlCompliance.SelectedValue != null) && (ddlCompliance.SelectedValue != string.Empty) && (ddlCompliance.SelectedValue != "-1")) //added by manisha
                            {
                                ddlRole.DataSource = assignedCompliances.Where(entry => entry.CustomerBranchID == Convert.ToInt32(ddlBranch.SelectedValue) && entry.ComplianceID == Convert.ToInt32(ddlCompliance.SelectedValue)).Select(entry => new { ID = entry.RoleID, Name = entry.Role })
                                    .OrderBy(entry => entry.Name).Distinct().ToList();
                                ddlRole.DataBind();

                                ddlRole.Items.Insert(0, new ListItem("< Select >", "-1"));


                                DateTime scheduledOn = assignedCompliances.Where(entry => entry.ComplianceID == Convert.ToInt32(ddlCompliance.SelectedValue) && entry.CustomerBranchID == Convert.ToInt32(ddlBranch.SelectedValue)).Select(entry => entry.ScheduledOn).FirstOrDefault();
                                if (scheduledOn != DateTime.MinValue)
                                {
                                    litScheduledOn.Text = scheduledOn == null ? string.Empty : scheduledOn.ToString("dd-MM-yyyy");
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }        

        private void BindUserReminders()
        {
            try
            {
                grdUserReminder.DataSource = Business.ComplianceManagement.GetComplianceReminderNotificationsByUserID(AuthenticationHelper.UserID, tbxFilter.Text).ToList();
                grdUserReminder.DataBind();

                upUserReminderList.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        private void GetPageDisplaySummary()
        {
            try
            {
                DivRecordsScrum.Visible = true;

                lblTotalRecord.Text = " " + Session["TotalRows"].ToString();

                lTotalCount.Text = GetTotalPagesCount().ToString();

                if (lTotalCount.Text != "0")
                {
                    if (SelectedPageNo.Text == "" || SelectedPageNo.Text == "0" || SelectedPageNo.Text == "1")
                    {
                        SelectedPageNo.Text = "1";
                        lblStartRecord.Text = "1";

                        if (!(Convert.ToInt32(ddlPageSize.SelectedValue) > Convert.ToInt32(Session["TotalRows"].ToString())))
                            lblEndRecord.Text = ddlPageSize.SelectedValue + " ";
                        else
                            lblEndRecord.Text = Session["TotalRows"].ToString() + " ";
                    }
                }
                else if (lTotalCount.Text == "0")
                {
                    SelectedPageNo.Text = "0";
                    DivRecordsScrum.Visible = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void grdUserReminder_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int userReminderID = Convert.ToInt32(e.CommandArgument);
                if (e.CommandName.Equals("EDIT_USER_REMINDER"))
                {
                    ViewState["Mode"] = 1;
                    ViewState["UserReminderID"] = userReminderID;

                    var userReminder = Business.ComplianceManagement.GetComplianceReminderNotificationsByID(userReminderID);

                    ddlBranch.SelectedValue = userReminder.CustomerBranchID.ToString();
                    ddlBranch_SelectedIndexChanged(null, null);

                    ddlCompliance.SelectedValue = userReminder.ComplianceID.ToString();
                    ddlCompliance_SelectedIndexChanged(null, null);

                    ddlRole.SelectedValue = userReminder.RoleID.ToString();
                    tbxDate.Text = userReminder.RemindOn.ToString("dd-MM-yyyy");

                    ////---------added by Manisha------------------------------------
                    //litScheduledOn.Text = userReminder.ScheduledOn.ToString("dd-MM-yyyy");
                    ////-------------------------------------------------------------

                    upUserReminders.Update();
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divUserRemindersDialog\").dialog('open')", true);
                }
                else if (e.CommandName.Equals("DELETE_USER_REMINDER"))
                {
                    UserManagement.DeleteUserReminder(userReminderID);
                    BindUserReminders();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdUserReminder_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdUserReminder.PageIndex = e.NewPageIndex;
                BindUserReminders();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnAddUserReminder_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["Mode"] = 0;
                ddlComType_SelectedIndexChanged(null, null);
                ddlBranch.SelectedValue = "-1";
                ddlBranch_SelectedIndexChanged(null, null);

                ddlCompliance.SelectedValue = "-1";
                ddlCompliance_SelectedIndexChanged(null, null);

                tbxDate.Text = string.Empty;
                litScheduledOn.Text = string.Empty;
                upUserReminders.Update();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divUserRemindersDialog\").dialog('open')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void Next_Click(object sender, EventArgs e)
        {
            try
            {
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;

                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo + 1).ToString();
                }
                else
                {

                }

                if (!(StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) > Convert.ToInt32(Session["TotalRows"])))
                    StartRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue);

                EndRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalRows"]))
                    EndRecord = Convert.ToInt32(Session["TotalRows"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";

                grdUserReminder.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                grdUserReminder.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                //Reload the Grid
                FillUserReminders();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void Previous_Click(object sender, EventArgs e)
        {
            try
            {
                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;

                if (Convert.ToInt32(SelectedPageNo.Text) > 1)
                {
                    SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
                }

                StartRecord = StartRecord - Convert.ToInt32(ddlPageSize.SelectedValue);

                if (StartRecord < 1)
                    StartRecord = 1;

                EndRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalRows"]))
                    EndRecord = Convert.ToInt32(Session["TotalRows"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";

                grdUserReminder.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                grdUserReminder.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                //Reload the Grid
                FillUserReminders();               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                grdUserReminder.PageIndex = 0;
                FillUserReminders();
                //BindUserReminders();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }


        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddlComType.SelectedValue == "-1")
                {
                    List<ComplianceInstanceAssignmentView> assignedCompliances = Business.ComplianceManagement.GetAllInstances(userID: AuthenticationHelper.UserID);
                    var assignment = assignedCompliances.Where(entry => entry.ComplianceID == Convert.ToInt32(ddlCompliance.SelectedValue) && entry.CustomerBranchID == Convert.ToInt32(ddlBranch.SelectedValue) && entry.RoleID == Convert.ToInt32(ddlRole.SelectedValue)).FirstOrDefault();

                    ComplianceReminder reminder = new ComplianceReminder()
                    {
                        ComplianceDueDate = (DateTime)assignment.ScheduledOn,
                        RemindOn = DateTime.ParseExact(tbxDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture),
                        ComplianceAssignmentID = assignment.ComplianceAssignmentID
                    };

                    if ((int)ViewState["Mode"] == 1)
                    {
                        reminder.ID = Convert.ToInt32(ViewState["UserReminderID"]);
                    }

                    if ((int)ViewState["Mode"] == 0)
                    {
                        if (!Business.ComplianceManagement.ExistsComplianceReminder(reminder))
                        {
                            Business.ComplianceManagement.CreateReminder(reminder);
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Reminder Saved Sucessfully.";
                           
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Same Reminder Already Exists.";
                        }
                    }
                    else if ((int)ViewState["Mode"] == 1)
                    {
                        Business.ComplianceManagement.UpdateReminder(reminder);
                    }
                }

                else if (ddlComType.SelectedValue == "0")
                {
                    List<InternalComplianceInstanceAssignmentView> assignedCompliances = Business.InternalComplianceManagement.GetAllInstances(userID: AuthenticationHelper.UserID);
                    var assignment = assignedCompliances.Where(entry => entry.ComplianceID == Convert.ToInt32(ddlCompliance.SelectedValue) && entry.CustomerBranchID == Convert.ToInt32(ddlBranch.SelectedValue) && entry.RoleID == Convert.ToInt32(ddlRole.SelectedValue)).FirstOrDefault();

                    InternalComplianceReminder reminder = new InternalComplianceReminder()
                    {
                        ComplianceDueDate = (DateTime)assignment.ScheduledOn,
                        RemindOn = DateTime.ParseExact(tbxDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture),
                        ComplianceAssignmentID = assignment.ComplianceAssignmentID
                    };

                    if ((int)ViewState["Mode"] == 1)
                    {
                        reminder.ID = Convert.ToInt32(ViewState["UserReminderID"]);
                    }

                    if ((int)ViewState["Mode"] == 0)
                    {
                        if (!Business.ComplianceManagement.ExistsInternalComplianceReminder(reminder))
                        {
                            Business.InternalComplianceManagement.CreateReminder(reminder);

                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Reminder Saved Sucessfully.";
                           // ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", " $('#Newaddremider').modal('hide')", true);

                            ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "javascript:caller()", true);
                            FillUserReminders();
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Same Reminder Already Exists.";
                        }
                    }
                    else if ((int)ViewState["Mode"] == 1)
                    {
                        Business.InternalComplianceManagement.UpdateReminder(reminder);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void grdUserReminder_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {

                var reminderList = Business.ComplianceManagement.GetComplianceReminderNotificationsByUserID(AuthenticationHelper.UserID, tbxFilter.Text).ToList();
                if (direction == SortDirection.Ascending)
                {
                    reminderList = reminderList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                }
                else
                {
                    reminderList = reminderList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                }


                foreach (DataControlField field in grdUserReminder.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdUserReminder.Columns.IndexOf(field);
                    }
                }

                grdUserReminder.DataSource = reminderList;
                grdUserReminder.DataBind();


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdUserReminder_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            //System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            //sortImage.ImageAlign = ImageAlign.AbsMiddle;

            //if (direction == SortDirection.Ascending)
            //{
            //    sortImage.ImageUrl = "../Images/SortAsc.gif";
            //    sortImage.AlternateText = "Ascending Order";
            //}
            //else
            //{
            //    sortImage.ImageUrl = "../Images/SortDesc.gif";
            //    sortImage.AlternateText = "Descending Order";
            //}
            //headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", " $('#Newaddremider').modal('hide')", true);
            FillUserReminders();
        }

        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
        }
    }
}