﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PerformerReviewerStatus.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Users.PerformerReviewerStatus" %>


<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="https://avacdn.azureedge.net/newcss/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap theme -->
    <link href="https://avacdn.azureedge.net/newcss/bootstrap-theme.css" rel="stylesheet" type="text/css" />

    <link href="https://avacdn.azureedge.net/newcss/responsive-calendar.css" rel="stylesheet" type="text/css" />

    <link href="https://avacdn.azureedge.net/newcss/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- full calendar css-->
    <link href="https://avacdn.azureedge.net/newcss/bootstrap-fullcalendar.css" rel="stylesheet" type="text/css" />
    <!-- owl carousel -->
    <link rel="stylesheet" href="https://avacdn.azureedge.net/newcss/owl.carousel.css" type="text/css" />
    <!-- Custom styles -->
    <link rel="stylesheet" href="https://avacdn.azureedge.net/newcss/fullcalendar.css" type="text/css" />
    <link href="https://avacdn.azureedge.net/newcss/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="https://avacdn.azureedge.net/newcss/jquery-ui-1.10.7.min.css" rel="stylesheet" type="text/css" />
    <link href="https://avacdn.azureedge.net/newcss/jquery.bxslider.css" rel="stylesheet" type="text/css" />
    <style>
        .table > thead > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

        .table > tbody > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

        .table > thead > tr > th > a {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

        .table tr th {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .clsheadergrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: bottom !important;
            border-bottom: 2px solid #dddddd !important;
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
            text-align: left;
        }

        .clsROWgrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: top !important;
            border-top: 1px solid #dddddd !important;
            color: #666666 !important;
            font-size: 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>
  
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:GridView runat="server" ID="grdComplianceTransactions" AutoGenerateColumns="false" CssClass="table"
                GridLines="none" OnRowCommand="grdComplianceTransactions_RowCommand" OnRowEditing="grdComplianceTransactions_RowEditing"
                AllowPaging="True" BorderWidth="0px" PageSize="200" DataKeyNames="ScheduledOnID">
                <HeaderStyle CssClass="clsheadergrid" />
                <RowStyle CssClass="clsROWgrid" />
                <Columns>

                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Sr">
                        <ItemTemplate>
                            <%#Container.DataItemIndex+1 %>
                        </ItemTemplate>
                    </asp:TemplateField>
                      <asp:BoundField DataField="ForMonth" HeaderText="Period" />
                    <asp:BoundField DataField="ScheduledOnID" HeaderText="Period" Visible="false" />

                    <asp:TemplateField HeaderText="Due Date">
                        <ItemTemplate>
                            <asp:Label ID="lblScheduledOn" runat="server" Text='<%# Convert.ToDateTime(Eval("ScheduledOn")).ToString("dd-MMM-yyyy") %>'></asp:Label>
                            <asp:Label ID="lblScheduledOnID" runat="server" Text='<%# Eval("ScheduledOnID") %>' Visible="false"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                      <asp:BoundField DataField="Dated" HeaderText="Dated" />
                      <asp:BoundField DataField="StatusChangedOn" HeaderText="StatusChangedOn" />
                    <asp:BoundField DataField="Status" HeaderText="Status" />
                   <asp:TemplateField ShowHeader="False" HeaderText="Action">
                        <ItemTemplate>                               
                            <asp:LinkButton  Visible='<%# CanChangeStatus((long)Eval("ScheduledOnID")) %>' CommandArgument='<%# Eval("ScheduledOnID") %>'
                                AutoPostBack="true" CommandName="RowEdit"  ID="btnEdit" runat="server">
                                 <img src='<%# ResolveUrl("~/Images/edit_icon_new.png")%>' alt="Edit" title="Edit Schedule" />
                            </asp:LinkButton>

                            <asp:LinkButton CommandArgument='<%# Eval("ScheduledOnID") %>'  CommandName="Delete_COMPLIANCE" OnClientClick="return confirm('Are you sure!! You want to Delete schedules ?');"
                             AutoPostBack="true"  ID="btndelete" runat="server" data-placement="bottom" title="delete Schedule">
                                    <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="delete" />
                            </asp:LinkButton>                                                     
                        </ItemTemplate>
                    </asp:TemplateField>                
                </Columns>
                <PagerStyle HorizontalAlign="Right" />
                <PagerTemplate>
                    <table style="display: none">
                        <tr>
                            <td>
                                <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                            </td>
                        </tr>
                    </table>
                </PagerTemplate>
                <EmptyDataTemplate>
                    No Records Found
                </EmptyDataTemplate>
            </asp:GridView>                      
        </div>
    </form>
</body>
</html>
