﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Users
{
    public partial class ModifyAssignments : System.Web.UI.Page
    {
        public static List<long> locationList = new List<long>();
        protected int UserID = -1;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (AuthenticationHelper.Role == "IMPT")
                    {
                        UserID = AuthenticationHelper.UserID;
                        customerdiv.Visible = true;
                        lblcustomer.Visible = true;
                        BindCustomers(UserID);
                    }
                    else
                    {
                        customerdiv.Visible = false;
                        lblcustomer.Visible = false;
                        BindLocationFilter((int)AuthenticationHelper.CustomerID);
                        BindApprover((int)AuthenticationHelper.CustomerID);
                        BindUserList((int)AuthenticationHelper.CustomerID);
                        tbxFilterLocation.Text = "< Select Location >";                        
                        BindNewUserList((int)AuthenticationHelper.CustomerID, ddlNewUsers);                  
                        int userID = -1;                       
                        if (!string.IsNullOrEmpty(ddlUserList.SelectedValue))
                        {
                            userID = Convert.ToInt32(ddlUserList.SelectedValue);
                        }                      
                        if (rbtSelectionType.SelectedValue == "0")
                        {
                            BindComplianceList((int)AuthenticationHelper.CustomerID, userID);
                            BindComplianceInstances((int)AuthenticationHelper.CustomerID, userID);
                        }
                        else
                        {
                            BindInternalComplianceList((int)AuthenticationHelper.CustomerID, userID);
                            BindInternalComplianceInstances((int)AuthenticationHelper.CustomerID, userID);
                        }
                    }                    
                }
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeApproverList", string.Format("initializeJQueryUI1('{0}', 'dvapprover');", txtapprover.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideAprroverList", "$(\"#dvapprover\").hide(\"blind\", null, 5, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindCustomers(int userid)
        {
            try
            {
                ddlCustomer.DataTextField = "Name";
                ddlCustomer.DataValueField = "ID";
                ddlCustomer.DataSource = Assigncustomer.GetAllCustomer(userid);
                ddlCustomer.DataBind();
                ddlCustomer.Items.Insert(0, new ListItem("< Select Customer >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int userID = -1;
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "MGMT")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else
                {
                    if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                    {
                        customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlUserList.SelectedValue))
                {
                    userID = Convert.ToInt32(ddlUserList.SelectedValue);
                }
                if (customerID !=-1)
                {
                    BindLocationFilter(customerID);
                    BindApprover(customerID);
                    BindUserList(customerID);
                    tbxFilterLocation.Text = "< Select Location >";
                    BindNewUserList(customerID, ddlNewUsers);

                    if (rbtSelectionType.SelectedValue == "0")
                    {
                        BindComplianceList(customerID, userID);
                        BindComplianceInstances(customerID, userID);
                    }
                    else
                    {
                        BindInternalComplianceList(customerID, userID);
                        BindInternalComplianceInstances(customerID, userID);
                    }
                }              
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void BindUserList(int cusid)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {                   
                    var uselist = UserManagement.GetAllUser(cusid, string.Empty);

                    var user = (from row in uselist
                                select new UserName()
                                {
                                    ID = row.ID,
                                    Name = row.FirstName + " " + row.LastName
                                }).ToList();

                    user = user.OrderBy(x => x.Name).ToList();
                    ddlUserList.DataTextField = "Name";
                    ddlUserList.DataValueField = "ID";
                    ddlUserList.Items.Clear();
                    ddlUserList.DataSource = user;
                    ddlUserList.DataBind();
                    ddlUserList.Items.Insert(0, new ListItem("< Select User >", "-1"));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindApprover(int cusid)
        {
            try
            {               
                var data = UserManagement.GetAllUserCustomerID(cusid, null);
                rptApprover.DataSource = data;
                rptApprover.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindLocationFilter(int cusid)
        {
            try
            {               
                tvFilterLocation.Nodes.Clear();
                tbxFilterLocation.Text = string.Empty;
                var bracnhes = CustomerBranchManagement.GetAllHierarchy(cusid);              
                foreach (var item in bracnhes)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());           
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    BindBranchesHierarchy(node, item);
                    tvFilterLocation.Nodes.Add(node);
                }
                tvFilterLocation.CollapseAll();                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);             
            }
        }

        private void BindNewUserList(int customerID, DropDownList ddllist)
        {
            try
            {
                var data = UserManagement.GetAllUserCustomerID(customerID, null);
                ddllist.DataTextField = "Name";
                ddllist.DataValueField = "ID";

                ddlNewReviewerUsers.DataTextField = "Name";
                ddlNewReviewerUsers.DataValueField = "ID";
                if (data != null)
                {
                    ddllist.DataSource = data;
                    ddllist.DataBind();
                    ddlNewReviewerUsers.DataSource = data;
                    ddlNewReviewerUsers.DataBind();
                }
                ddllist.Items.Insert(0, new ListItem("< Select User >", "-1"));
                ddlNewReviewerUsers.Items.Insert(0, new ListItem("< Select User >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
        }
        public void BindComplianceList(int customerID,int userID)
        {
            try
            {
                bool FlagIsStatutory = true;               
                List<ComplianceDeatil> objCompliance = new List<ComplianceDeatil>();
                try
                {                   
                    int branchID = -1;
                    if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1")
                    {
                        branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                    }
                    if (customerID != -1 )
                    {
                        if (rbtSelectType.SelectedValue == "0")
                        {
                            List<Sp_ComplianceAssignedInstanceNew_Result> datasource = new List<Sp_ComplianceAssignedInstanceNew_Result>();
                            if (chkEvent.Checked == true)
                            {
                                chkCheckList.Checked = false;
                                datasource = ComplianceExclude_Reassign.Statutory_GetAllAssignedInstancesReassginExcludeNew("Y", customerID, userID, "No", branchID, filter: txtAssigmnetFilter.Text).GroupBy(entry => entry.ComplianceInstanceID).Select(entry => entry.FirstOrDefault()).ToList();
                                objCompliance.Clear();
                                foreach (var item in datasource)
                                {
                                    objCompliance.Add(new ComplianceDeatil { ID = item.ComplianceID, Name = item.ShortDescription });
                                }
                            }
                            else if (chkCheckList.Checked == true)
                            {
                                chkEvent.Checked = false;
                                datasource = ComplianceExclude_Reassign.Statutory_GetAllAssignedInstancesReassginExcludeNew("NA", customerID, userID, "Yes", branchID, filter: txtAssigmnetFilter.Text).GroupBy(entry => entry.ComplianceInstanceID).Select(entry => entry.FirstOrDefault()).ToList();
                                objCompliance.Clear();
                                foreach (var item in datasource)
                                {
                                    objCompliance.Add(new ComplianceDeatil { ID = item.ComplianceID, Name = item.ShortDescription });
                                }
                            }
                            else
                            {
                                chkEvent.Checked = false;
                                chkCheckList.Checked = false;
                                datasource = ComplianceExclude_Reassign.Statutory_GetAllAssignedInstancesReassginExcludeNew("N", customerID, userID, "No", branchID, filter: txtAssigmnetFilter.Text).GroupBy(entry => entry.ComplianceInstanceID).Select(entry => entry.FirstOrDefault()).ToList();
                                objCompliance.Clear();
                                foreach (var item in datasource)
                                {
                                    objCompliance.Add(new ComplianceDeatil { ID = item.ComplianceID, Name = item.ShortDescription });
                                }

                            }
                        }
                        else if (rbtSelectType.SelectedValue == "1")
                        {
                            if (rbtSelectionType.SelectedValue == "0")
                            {
                                List<Sp_TASK_Statutory_InternalComplianceAssignedInstanceNew_Result> datasource = new List<Sp_TASK_Statutory_InternalComplianceAssignedInstanceNew_Result>();
                                datasource = ComplianceExclude_Reassign.GetAllTaskAssignedInstancesReassginExcludeStatutory_InternalNew(customerID, userID, "S", txtAssigmnetFilter.Text, branchID).GroupBy(entry => entry.TaskInstanceID).Select(entry => entry.FirstOrDefault()).ToList();
                                objCompliance.Clear();
                                foreach (var item in datasource)
                                {
                                    objCompliance.Add(new ComplianceDeatil { ID = item.ComplianceID, Name = item.ShortDescription });
                                }

                            }
                            if (rbtSelectionType.SelectedValue == "1")
                            {
                                List<Sp_TASK_Statutory_InternalComplianceAssignedInstanceNew_Result> datasource = new List<Sp_TASK_Statutory_InternalComplianceAssignedInstanceNew_Result>();
                                datasource = ComplianceExclude_Reassign.GetAllTaskAssignedInstancesReassginExcludeStatutory_InternalNew(customerID, userID, "I", txtAssigmnetFilter.Text, branchID).GroupBy(entry => entry.TaskInstanceID).Select(entry => entry.FirstOrDefault()).ToList();
                                objCompliance.Clear();
                                foreach (var item in datasource)
                                {
                                    objCompliance.Add(new ComplianceDeatil { ID = item.ComplianceID, Name = item.ShortDescription });
                                }
                            }
                        }
                        else
                        {
                            FlagIsStatutory = false;
                            BindInternalComplianceList(customerID,userID);
                        }
                    }

                    if (FlagIsStatutory)
                    {
                        var data = objCompliance.GroupBy(entity => entity.ID).Select(entity => entity.FirstOrDefault()).ToList();
                        ddlCompliance.DataTextField = "Name";
                        ddlCompliance.DataValueField = "ID";
                        ddlCompliance.Items.Clear();
                        ddlCompliance.DataSource = data;
                        ddlCompliance.DataBind();
                        ddlCompliance.Items.Insert(0, new ListItem("< Select Compliance >", "-1"));
                    }

                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CustomModifyAsignment.IsValid = false;
                CustomModifyAsignment.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp)
        {
            try
            {
                foreach (var item in nvp.Children)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    BindBranchesHierarchy(node, item);
                    parent.ChildNodes.Add(node);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);                
            }
        }
        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                int cuid = -1;
                int userID = -1;            
                if (AuthenticationHelper.Role == "CADMN")
                {
                    cuid = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "MGMT")
                {
                    cuid = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else
                {
                    if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                    {
                        cuid = Convert.ToInt32(ddlCustomer.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlUserList.SelectedValue))
                {
                    userID = Convert.ToInt32(ddlUserList.SelectedValue);
                }                               
                if (rbtSelectionType.SelectedValue == "0")
                {
                    BindComplianceList(cuid, userID);
                    BindComplianceInstances(cuid,userID);
                }
                else
                {
                    BindInternalComplianceList(cuid, userID);
                    BindInternalComplianceInstances(cuid,userID);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
           
        }
       
        public class UserName
        {
            public long ID { get; set; }
            public string Name { get; set; }
        }

        public class ComplianceDeatil
        {
            public long ID { get; set; }
            public string Name { get; set; }
            public long BID { get; set; }
        }
     
        
        protected void RetrieveNodes(TreeNode node)
        {            
            try
            {
                int customerID = -1;              
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "MGMT")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else
                {
                    if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                    {
                        customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                    }
                }
               
                if (node.Checked)
                {
                    if (Convert.ToInt32(node.Value) != customerID)
                    {
                        if (!locationList.Contains(Convert.ToInt32(node.Value)))
                        {
                            locationList.Add(Convert.ToInt32(node.Value));
                        }
                    }
                    if (node.ChildNodes.Count != 0)
                    {
                        for (int i = 0; i < node.ChildNodes.Count; i++)
                        {
                            RetrieveNodes(node.ChildNodes[i]);
                        }
                    }
                }
                else
                {
                    foreach (TreeNode tn in node.ChildNodes)
                    {
                        if (tn.Checked)
                        {
                            if (Convert.ToInt32(tn.Value) != customerID)
                            {
                                if (!locationList.Contains(Convert.ToInt32(tn.Value)))
                                {
                                    locationList.Add(Convert.ToInt32(tn.Value));
                                }
                            }
                        }
                        if (tn.ChildNodes.Count != 0)
                        {
                            for (int i = 0; i < tn.ChildNodes.Count; i++)
                            {
                                RetrieveNodes(tn.ChildNodes[i]);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {                
            }
        }
        public static bool FindNodeExists1(NameValueHierarchy item, List<int> ListIDs)
        {
            bool result = false;
            try
            {
                if (ListIDs.Contains(item.ID))
                {
                    result = true;
                    return result;
                }
                else
                {
                    foreach (var childNode in item.Children)
                    {
                        if (ListIDs.Contains(childNode.ID))
                        {
                            result = true;
                        }
                        else
                        {
                            result = FindNodeExists1(childNode, ListIDs);
                        }

                        if (result)
                        {
                            break;
                        }
                    }

                }
                return result;
            }
            catch (Exception ex)
            {
                return result;
            }
        }
      
        private void BindComplianceInstances(int customerID, int userID)
        {
            try
            {                                              
                int CompID = -1;
                if (ddlCompliance.SelectedValue != "-1")
                {
                    CompID = Convert.ToInt32(ddlCompliance.SelectedValue);
                }
                int branchID = -1;
                if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1")
                {
                    branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                }
                locationList.Clear();

                for (int i = 0; i < this.tvFilterLocation.Nodes.Count; i++)
                {                    
                    RetrieveNodes(this.tvFilterLocation.Nodes[i]);
                }

               
                if (customerID != -1)
                {
                    grdTask.DataSource = null;
                    grdTask.DataBind();
                    if (rbtSelectType.SelectedValue == "0")
                    {
                        List<Sp_ComplianceAssignedInstanceNew_Result> datasource = new List<Sp_ComplianceAssignedInstanceNew_Result>();
                        if (chkEvent.Checked == true)
                        {
                            chkCheckList.Checked = false;
                            grdComplianceInstances.DataSource = null;
                            grdComplianceInstances.DataBind();
                            datasource = ComplianceExclude_Reassign.Statutory_GetAllAssignedInstancesReassginExcludeNew("Y", customerID, userID, "No", branchID, filter: txtAssigmnetFilter.Text).GroupBy(entry => entry.ComplianceInstanceID).Select(entry => entry.FirstOrDefault()).ToList();
                            
                            if (branchID > 0)
                            {
                                datasource = datasource.Where(x => x.CustomerBranchID == branchID).ToList();

                            }

                            if (locationList.Count > 0)
                            {
                                datasource = datasource.Where(x => locationList.Contains(x.CustomerBranchID)).ToList();
                            }
                            
                            if (CompID > 0)
                            {
                                datasource = datasource.Where(x => x.ComplianceID == CompID).ToList();
                            }
                            grdComplianceInstances.DataSource = datasource;
                            grdComplianceInstances.DataBind();
                            
                        }
                        else if (chkCheckList.Checked == true)
                        {
                            chkEvent.Checked = false;
                            grdComplianceInstances.DataSource = null;
                            grdComplianceInstances.DataBind();
                            datasource = ComplianceExclude_Reassign.Statutory_GetAllAssignedInstancesReassginExcludeNew("NA", customerID, userID, "Yes", branchID, filter: txtAssigmnetFilter.Text).GroupBy(entry => entry.ComplianceInstanceID).Select(entry => entry.FirstOrDefault()).ToList();
                            
                            if (branchID > 0)
                            {
                                datasource = datasource.Where(x => x.CustomerBranchID == branchID).ToList();
                            }


                            if (locationList.Count > 0)
                            {
                                datasource = datasource.Where(x => locationList.Contains(x.CustomerBranchID)).ToList();

                            }
                           
                            if (CompID > 0)
                            {
                                datasource = datasource.Where(x => x.ComplianceID == CompID).ToList();
                            }
                            grdComplianceInstances.DataSource = datasource;
                            grdComplianceInstances.DataBind();
                            
                        }
                        else
                        {
                            chkEvent.Checked = false;
                            chkCheckList.Checked = false;
                            grdComplianceInstances.DataSource = null;
                            grdComplianceInstances.DataBind();
                            datasource = ComplianceExclude_Reassign.Statutory_GetAllAssignedInstancesReassginExcludeNew("N", customerID, userID, "No", branchID, filter: txtAssigmnetFilter.Text).GroupBy(entry => entry.ComplianceInstanceID).Select(entry => entry.FirstOrDefault()).ToList();
                            
                            if (branchID > 0)
                            {
                                datasource = datasource.Where(x => x.CustomerBranchID == branchID).ToList();
                            }


                            if (locationList.Count > 0)
                            {
                                datasource = datasource.Where(x => locationList.Contains(x.CustomerBranchID)).ToList();

                            }
                    
                            if (CompID > 0)
                            {
                                datasource = datasource.Where(x => x.ComplianceID == CompID).ToList();
                            }
                            grdComplianceInstances.DataSource = datasource;
                            grdComplianceInstances.DataBind();
                            
                        }
                        grdInternalComplianceInstances.DataSource = null;
                        grdInternalComplianceInstances.DataBind();
                    }
                    else if (rbtSelectType.SelectedValue == "1")
                    {
                        grdComplianceInstances.DataSource = null;
                        grdComplianceInstances.DataBind();
                        grdInternalComplianceInstances.DataSource = null;
                        grdInternalComplianceInstances.DataBind();

                        if (rbtSelectionType.SelectedValue == "0")
                        {
                            grdTask.DataSource = null;
                            grdTask.DataBind();
                            List<Sp_TASK_Statutory_InternalComplianceAssignedInstanceNew_Result> datasource = new List<Sp_TASK_Statutory_InternalComplianceAssignedInstanceNew_Result>();
                            datasource = ComplianceExclude_Reassign.GetAllTaskAssignedInstancesReassginExcludeStatutory_InternalNew(customerID, userID, "S", txtAssigmnetFilter.Text).GroupBy(entry => entry.TaskInstanceID).Select(entry => entry.FirstOrDefault()).ToList();
                            
                            if (branchID > 0)
                            {
                                datasource = datasource.Where(x => x.CustomerBranchID == branchID).ToList();
                            }


                            if (locationList.Count > 0)
                            {
                                datasource = datasource.Where(x => locationList.Contains(x.CustomerBranchID)).ToList();

                            }
                           
                            if (CompID > 0)
                            {
                                datasource = datasource.Where(x => x.ComplianceID == CompID).ToList();
                            }
                            grdTask.DataSource = datasource;
                            grdTask.DataBind();
                            
                        }
                        if (rbtSelectionType.SelectedValue == "1")
                        {
                            grdTask.DataSource = null;
                            grdTask.DataBind();
                            List<Sp_TASK_Statutory_InternalComplianceAssignedInstanceNew_Result> datasource = new List<Sp_TASK_Statutory_InternalComplianceAssignedInstanceNew_Result>();
                            datasource = ComplianceExclude_Reassign.GetAllTaskAssignedInstancesReassginExcludeStatutory_InternalNew(customerID, userID, "I", txtAssigmnetFilter.Text).GroupBy(entry => entry.TaskInstanceID).Select(entry => entry.FirstOrDefault()).ToList();
                            
                            if (branchID > 0)
                            {
                                datasource = datasource.Where(x => x.CustomerBranchID == branchID).ToList();
                            }

                            if (locationList.Count > 0)
                            {
                                datasource = datasource.Where(x => locationList.Contains(x.CustomerBranchID)).ToList();

                            }
                       
                            if (CompID > 0)
                            {
                                datasource = datasource.Where(x => x.ComplianceID == CompID).ToList();
                            }
                            grdTask.DataSource = datasource;
                            grdTask.DataBind();
                            
                        }
                    }
                    else
                    {
                        BindInternalComplianceInstances(customerID,userID);                        
                    }                   
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);              
            }
        }
     
        public void BindInternalComplianceList(int customerID,int userID)
        {
            try
            {
                bool FlagIsInternal = true;
                List<ComplianceDeatil> objCompliance = new List<ComplianceDeatil>();               
                if (customerID != -1)
                {
                    if (rbtSelectType.SelectedValue == "0")
                    {
                        if (chkCheckList.Checked == true)
                        {
                            var datasource = ComplianceExclude_Reassign.Internal_GetAllAssignedInstancesNew("Y", customerID, userID, filter: txtAssigmnetFilter.Text).ToList();
                            objCompliance.Clear();
                            foreach (var item in datasource)
                            {
                                objCompliance.Add(new ComplianceDeatil { ID = item.InternalComplianceID, Name = item.IShortDescription });
                            }
                        }
                        else if (chkEvent.Checked == true)
                        {
                            objCompliance.Clear();                            
                        }
                        else
                        {
                            var datasource = ComplianceExclude_Reassign.Internal_GetAllAssignedInstancesNew("N", customerID, userID, filter: txtAssigmnetFilter.Text).ToList();
                            objCompliance.Clear();
                            foreach (var item in datasource)
                            {
                                objCompliance.Add(new ComplianceDeatil { ID = item.InternalComplianceID, Name = item.IShortDescription });
                            }
                        }
                    }
                    else
                    {
                        FlagIsInternal = false;
                        BindComplianceList(customerID, userID);
                    }
                }
                if (FlagIsInternal)
                {
                    var data = objCompliance.GroupBy(entity => entity.ID).Select(entity => entity.FirstOrDefault()).ToList();
                    ddlCompliance.DataTextField = "Name";
                    ddlCompliance.DataValueField = "ID";
                    ddlCompliance.Items.Clear();
                    ddlCompliance.DataSource = data;
                    ddlCompliance.DataBind();
                    ddlCompliance.Items.Insert(0, new ListItem("Select Compliance", "-1"));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindInternalComplianceInstances(int customerID, int userID)
        {
            try
            {                               
                int branchID = -1;
                if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1")
                {
                    branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                }                
                locationList.Clear();
                for (int i = 0; i < this.tvFilterLocation.Nodes.Count; i++)
                {                    
                    RetrieveNodes(this.tvFilterLocation.Nodes[i]);
                }
                int CompID = -1;
                if (ddlCompliance.SelectedValue != "-1")
                {
                    CompID = Convert.ToInt32(ddlCompliance.SelectedValue);
                }
                if (customerID != -1)
                {
                    if (rbtSelectType.SelectedValue == "0")
                    {
                        if (chkCheckList.Checked == true)
                        {
                            var datasource = ComplianceExclude_Reassign.Internal_GetAllAssignedInstancesNew("Y", customerID, userID, branchID, filter: txtAssigmnetFilter.Text).ToList();
                            if (branchID > 0)
                            {
                                datasource = datasource.Where(x => x.CustomerBranchID == branchID).ToList();
                            }

                            if (locationList.Count > 0)
                            {
                                datasource = datasource.Where(x => locationList.Contains(x.CustomerBranchID)).ToList();
                            }
                            if (CompID > 0)
                            {
                                datasource = datasource.Where(x => x.InternalComplianceID == CompID).ToList();
                            }
                            grdInternalComplianceInstances.DataSource = datasource;
                            grdInternalComplianceInstances.DataBind();

                            grdComplianceInstances.DataSource = null;
                            grdComplianceInstances.DataBind();

                        }
                        else if (chkEvent.Checked == true)
                        {
                            grdComplianceInstances.DataSource = null;
                            grdComplianceInstances.DataBind();

                            grdInternalComplianceInstances.DataSource = null;
                            grdInternalComplianceInstances.DataBind();
                        }
                        else
                        {
                            var datasource = ComplianceExclude_Reassign.Internal_GetAllAssignedInstancesNew("N", customerID, userID, branchID, filter: txtAssigmnetFilter.Text).ToList();
                            if (branchID > 0)
                            {
                                datasource = datasource.Where(x => x.CustomerBranchID == branchID).ToList();
                            }

                            if (locationList.Count > 0)
                            {
                                datasource = datasource.Where(x => locationList.Contains(x.CustomerBranchID)).ToList();
                            }

                            if (CompID > 0)
                            {
                                datasource = datasource.Where(x => x.InternalComplianceID == CompID).ToList();
                            }
                            grdInternalComplianceInstances.DataSource = datasource;
                            grdInternalComplianceInstances.DataBind();

                            grdComplianceInstances.DataSource = null;
                            grdComplianceInstances.DataBind();

                        }
                    }
                    else
                    {
                        BindComplianceInstances(customerID,userID);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
             
            }
        }
     
        protected void rbtSelectType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int cuid = -1;
                int userID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    cuid = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "MGMT")
                {
                    cuid = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else
                {
                    if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                    {
                        cuid = Convert.ToInt32(ddlCustomer.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlUserList.SelectedValue))
                {
                    userID = Convert.ToInt32(ddlUserList.SelectedValue);
                }
                if (cuid != -1)
                {
                    if (rbtSelectionType.SelectedValue == "0")
                    {
                        if (rbtSelectType.SelectedValue == "0")
                        {
                            divEventBase.Visible = true;
                            BindComplianceList(cuid, userID);
                            BindComplianceInstances(cuid, userID);
                        }
                        else
                        {
                            divEventBase.Visible = false;
                            BindComplianceList(cuid, userID);
                            BindComplianceInstances(cuid, userID);
                        }
                    }
                    else
                    {
                        BindInternalComplianceList(cuid, userID);
                        BindInternalComplianceInstances(cuid, userID);
                    }
                }
            }
            catch (Exception ex)
            {

                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }                    
        }

        protected void rbtSelectionType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int cuid = -1;
                int userID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    cuid = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "MGMT")
                {
                    cuid = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else
                {
                    if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                    {
                        cuid = Convert.ToInt32(ddlCustomer.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlUserList.SelectedValue))
                {
                    userID = Convert.ToInt32(ddlUserList.SelectedValue);
                }
                if (cuid != -1)
                {
                    if (rbtSelectionType.SelectedValue == "0")
                    {
                        BindComplianceList(cuid, userID);
                        BindComplianceInstances(cuid, userID);
                    }
                    else
                    {
                        BindInternalComplianceList(cuid, userID);
                        BindInternalComplianceInstances(cuid, userID);
                    }
                }
            }
            catch (Exception ex)
            {

                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void rbtModifyAction_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string action = Convert.ToString(rbtModifyAction.SelectedValue);
                if (action.Equals("Delete") || action.Equals("ApproverDelete"))
                {
                    divNewUser.Visible = false;
                }
                else
                {
                    divNewUser.Visible = true;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void chkEvent_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                int cuid = -1;
                int userID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    cuid = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "MGMT")
                {
                    cuid = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else
                {
                    if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                    {
                        cuid = Convert.ToInt32(ddlCustomer.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlUserList.SelectedValue))
                {
                    userID = Convert.ToInt32(ddlUserList.SelectedValue);
                }
                if (cuid != -1)
                {
                    chkCheckList.Checked = false;

                    if (rbtSelectionType.SelectedValue == "0")
                    {
                        BindComplianceList(cuid, userID);
                        BindComplianceInstances(cuid, userID);
                    }
                    else
                    {
                        BindInternalComplianceList(cuid, userID);
                        BindInternalComplianceInstances(cuid, userID);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void chkCheckList_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                int cuid = -1;
                int userID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    cuid = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "MGMT")
                {
                    cuid = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else
                {
                    if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                    {
                        cuid = Convert.ToInt32(ddlCustomer.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlUserList.SelectedValue))
                {
                    userID = Convert.ToInt32(ddlUserList.SelectedValue);
                }
                if (cuid != -1)
                {
                    chkEvent.Checked = false;
                    if (rbtSelectionType.SelectedValue == "0")
                    {
                        BindComplianceList(cuid, userID);
                        BindComplianceInstances(cuid, userID);
                    }
                    else
                    {
                        BindInternalComplianceList(cuid, userID);
                        BindInternalComplianceInstances(cuid, userID);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void txtAssigmnetFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                int cuid = -1;
                int userID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    cuid = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "MGMT")
                {
                    cuid = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else
                {
                    if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                    {
                        cuid = Convert.ToInt32(ddlCustomer.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlUserList.SelectedValue))
                {
                    userID = Convert.ToInt32(ddlUserList.SelectedValue);
                }
                if (cuid != -1)//&& userID != -1
                {
                    if (rbtSelectionType.SelectedValue == "0")
                    {
                        BindComplianceInstances(cuid, userID);
                    }
                    else
                    {
                        BindInternalComplianceInstances(cuid, userID);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CustomModifyAsignment.IsValid = false;
                CustomModifyAsignment.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
                     
        protected void grdComplianceInstances_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    int sortColumnIndex = Convert.ToInt32(ViewState["InstancesSortIndex"]);
                    if (sortColumnIndex != 0 || sortColumnIndex != -1)
                    {
                        if (!(sortColumnIndex == 0))
                            AddInstancesSortImage(sortColumnIndex, e.Row);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CustomModifyAsignment.IsValid = false;
                CustomModifyAsignment.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdComplianceInstances_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {               
                int cuid = -1;
                int userID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    cuid = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "MGMT")
                {
                    cuid = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else
                {
                    if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                    {
                        cuid = Convert.ToInt32(ddlCustomer.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlUserList.SelectedValue))
                {
                    userID = Convert.ToInt32(ddlUserList.SelectedValue);
                }
                if (cuid != -1 && userID != -1)
                {
                    CheckBoxValueSaved();
                    grdComplianceInstances.PageIndex = e.NewPageIndex;
                    BindComplianceInstances(cuid, userID);
                    AssignCheckBoxexValue();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CustomModifyAsignment.IsValid = false;
                CustomModifyAsignment.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void AssignCheckBoxexValue()
        {

            List<Tuple<long, string>> chkList = (List<Tuple<long, string>>)ViewState["AssignedCompliancesID"];
            if (chkList != null && chkList.Count > 0)
            {
                foreach (GridViewRow gvrow in grdComplianceInstances.Rows)
                {
                    int index = Convert.ToInt32(grdComplianceInstances.DataKeys[gvrow.RowIndex].Value);                    
                    string role = "Performer";
                    var checkedData = chkList.Where(entry => entry.Item1 == index && entry.Item2.Equals(role)).FirstOrDefault();
                    if (chkList.Contains(checkedData))
                    {
                        CheckBox myCheckBox = (CheckBox)gvrow.FindControl("chkCompliances");
                        myCheckBox.Checked = true;
                    }
                }
            }
        }

        protected void grdComplianceInstances_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                int customerID = -1;
                int userID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "MGMT")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else
                {
                    if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                    {
                        customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlUserList.SelectedValue))
                {
                    userID = Convert.ToInt32(ddlUserList.SelectedValue);
                }
                if (customerID != -1 && userID != -1)
                {
                    List<Sp_ComplianceAssignedInstanceNew_Result> assignmentList = new List<Sp_ComplianceAssignedInstanceNew_Result>();
                    if (chkEvent.Checked == true)
                    {
                        chkCheckList.Checked = false;
                        assignmentList = ComplianceExclude_Reassign.Statutory_GetAllAssignedInstancesReassginExcludeNew("Y", customerID, userID, "No", filter: txtAssigmnetFilter.Text).GroupBy(entry => entry.ComplianceInstanceID).Select(entry => entry.FirstOrDefault()).ToList();
                    }
                    else if (chkCheckList.Checked == true)
                    {
                        chkEvent.Checked = false;
                        assignmentList = ComplianceExclude_Reassign.Statutory_GetAllAssignedInstancesReassginExcludeNew("NA", customerID, userID, "Yes", filter: txtAssigmnetFilter.Text).GroupBy(entry => entry.ComplianceInstanceID).Select(entry => entry.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        chkEvent.Checked = false;
                        chkCheckList.Checked = false;
                        assignmentList = ComplianceExclude_Reassign.Statutory_GetAllAssignedInstancesReassginExcludeNew("N", customerID, userID, "No", filter: txtAssigmnetFilter.Text).GroupBy(entry => entry.ComplianceInstanceID).Select(entry => entry.FirstOrDefault()).ToList();
                    }
                    if (direction == SortDirection.Ascending)
                    {
                        assignmentList = assignmentList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                        direction = SortDirection.Descending;
                    }
                    else
                    {
                        assignmentList = assignmentList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                        direction = SortDirection.Ascending;
                    }
                    foreach (DataControlField field in grdComplianceInstances.Columns)
                    {
                        if (field.SortExpression == e.SortExpression)
                        {
                            ViewState["InstancesSortIndex"] = grdComplianceInstances.Columns.IndexOf(field);
                        }
                    }
                    grdComplianceInstances.DataSource = assignmentList;
                    grdComplianceInstances.DataBind();
                }
                else
                {
                    CustomModifyAsignment.IsValid = false;
                    CustomModifyAsignment.ErrorMessage = "Something went wrong, Please check customer selection & try again.";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CustomModifyAsignment.IsValid = false;
                CustomModifyAsignment.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void grdInternalComplianceInstances_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    int sortColumnIndex = Convert.ToInt32(ViewState["InternalInstancesSortIndex"]);
                    if (sortColumnIndex != 0 || sortColumnIndex != -1)
                    {
                        if (!(sortColumnIndex == 0))
                            AddInstancesSortImage(sortColumnIndex, e.Row);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CustomModifyAsignment.IsValid = false;
                CustomModifyAsignment.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdInternalComplianceInstances_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                int cuid = -1;
                int userID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    cuid = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "MGMT")
                {
                    cuid = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else
                {
                    if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                    {
                        cuid = Convert.ToInt32(ddlCustomer.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlUserList.SelectedValue))
                {
                    userID = Convert.ToInt32(ddlUserList.SelectedValue);
                }
                if (cuid != -1 && userID != -1)
                {
                    InternalCheckBoxValueSaved();
                    grdInternalComplianceInstances.PageIndex = e.NewPageIndex;
                    BindInternalComplianceInstances(cuid, userID);
                    InternalAssignCheckBoxexValue();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CustomModifyAsignment.IsValid = false;
                CustomModifyAsignment.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void InternalCheckBoxValueSaved()
        {
            List<Tuple<long, string>> chkIList = new List<Tuple<long, string>>();
            int index = -1;
            foreach (GridViewRow gvrow in grdInternalComplianceInstances.Rows)
            {
                index = Convert.ToInt32(grdInternalComplianceInstances.DataKeys[gvrow.RowIndex].Value);
                bool result = ((CheckBox)gvrow.FindControl("chkICompliances")).Checked;
                string role = "Performer";

                if (ViewState["AssignedInternalCompliancesID"] != null)
                    chkIList = (List<Tuple<long, string>>)ViewState["AssignedInternalCompliancesID"];

                var checkedData = chkIList.Where(entry => entry.Item1 == index && entry.Item2.Equals(role)).FirstOrDefault();
                if (result)
                {
                    if (checkedData == null)
                        chkIList.Add(new Tuple<long, string>(index, role));
                }
                else
                    chkIList.Remove(checkedData);
            }
            if (chkIList != null && chkIList.Count > 0)
                ViewState["AssignedInternalCompliancesID"] = chkIList;
        }
        private void InternalAssignCheckBoxexValue()
        {

            List<Tuple<long, string>> chkIList = (List<Tuple<long, string>>)ViewState["AssignedInternalCompliancesID"];
            if (chkIList != null && chkIList.Count > 0)
            {
                foreach (GridViewRow gvrow in grdInternalComplianceInstances.Rows)
                {
                    int index = Convert.ToInt32(grdInternalComplianceInstances.DataKeys[gvrow.RowIndex].Value);
                    string role = "Performer";
                    var checkedIData = chkIList.Where(entry => entry.Item1 == index && entry.Item2.Equals(role)).FirstOrDefault();
                    if (chkIList.Contains(checkedIData))
                    {
                        CheckBox myCheckBox = (CheckBox)gvrow.FindControl("chkICompliances");
                        myCheckBox.Checked = true;
                    }
                }
            }
        }
        protected void grdInternalComplianceInstances_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                int customerID = -1;
                int userID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "MGMT")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else
                {
                    if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                    {
                        customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlUserList.SelectedValue))
                {
                    userID = Convert.ToInt32(ddlUserList.SelectedValue);
                }
                if (customerID != -1 && userID != -1)
                {
                    if (chkCheckList.Checked == true)
                    {
                        var InternalassignmentList = ComplianceExclude_Reassign.Internal_GetAllAssignedInstancesNew("Y", customerID, userID, filter: txtAssigmnetFilter.Text).OrderBy(entry => entry.Branch).ToList();

                        if (direction == SortDirection.Ascending)
                        {
                            InternalassignmentList = InternalassignmentList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                            direction = SortDirection.Descending;
                        }
                        else
                        {
                            InternalassignmentList = InternalassignmentList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                            direction = SortDirection.Ascending;
                        }
                        foreach (DataControlField field in grdInternalComplianceInstances.Columns)
                        {
                            if (field.SortExpression == e.SortExpression)
                            {
                                ViewState["InternalInstancesSortIndex"] = grdInternalComplianceInstances.Columns.IndexOf(field);
                            }
                        }

                        grdInternalComplianceInstances.DataSource = InternalassignmentList;
                        grdInternalComplianceInstances.DataBind();
                    }
                    else
                    {
                        var InternalassignmentList = ComplianceExclude_Reassign.Internal_GetAllAssignedInstancesNew("N", customerID, userID, filter: txtAssigmnetFilter.Text).OrderBy(entry => entry.Branch).ToList();
                        if (direction == SortDirection.Ascending)
                        {
                            InternalassignmentList = InternalassignmentList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                            direction = SortDirection.Descending;
                        }
                        else
                        {
                            InternalassignmentList = InternalassignmentList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                            direction = SortDirection.Ascending;
                        }
                        foreach (DataControlField field in grdInternalComplianceInstances.Columns)
                        {
                            if (field.SortExpression == e.SortExpression)
                            {
                                ViewState["InternalInstancesSortIndex"] = grdInternalComplianceInstances.Columns.IndexOf(field);
                            }
                        }
                        grdInternalComplianceInstances.DataSource = InternalassignmentList;
                        grdInternalComplianceInstances.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CustomModifyAsignment.IsValid = false;
                CustomModifyAsignment.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdTask_RowCreated(object sender, GridViewRowEventArgs e)
        {

        }

        protected void grdTask_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                int cuid = -1;
                int userID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    cuid = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "MGMT")
                {
                    cuid = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else
                {
                    if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                    {
                        cuid = Convert.ToInt32(ddlCustomer.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlUserList.SelectedValue))
                {
                    userID = Convert.ToInt32(ddlUserList.SelectedValue);
                }
                if (cuid != -1 && userID != -1)
                {
                    Task_CheckBoxValueSaved();
                    grdTask.PageIndex = e.NewPageIndex;
                    BindComplianceInstances(cuid, userID);
                    Task_AssignCheckBoxexValue();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CustomModifyAsignment.IsValid = false;
                CustomModifyAsignment.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void Task_CheckBoxValueSaved()
        {
            List<Tuple<long, int>> chkList = new List<Tuple<long, int>>();
            int index = -1;
            foreach (GridViewRow gvrow in grdTask.Rows)
            {
                index = Convert.ToInt32(grdTask.DataKeys[gvrow.RowIndex].Value);
                bool result = ((CheckBox)gvrow.FindControl("chkTask")).Checked;
                int role = 3;
                if (ViewState["AssignedTaskID"] != null)
                    chkList = (List<Tuple<long, int>>)ViewState["AssignedTaskID"];

                var checkedData = chkList.Where(entry => entry.Item1 == index && entry.Item2.Equals(role)).FirstOrDefault();
                if (result)
                {
                    if (checkedData == null)
                        chkList.Add(new Tuple<long, int>(index, Convert.ToInt32(role)));
                }
                else
                    chkList.Remove(checkedData);
            }
            if (chkList != null && chkList.Count > 0)
                ViewState["AssignedTaskID"] = chkList;
        }
        private void Task_AssignCheckBoxexValue()
        {

            List<Tuple<long, string>> chkList = (List<Tuple<long, string>>)ViewState["AssignedTaskID"];
            if (chkList != null && chkList.Count > 0)
            {
                foreach (GridViewRow gvrow in grdTask.Rows)
                {
                    int index = Convert.ToInt32(grdTask.DataKeys[gvrow.RowIndex].Value);
                    int role = 3;
                    var checkedData = chkList.Where(entry => entry.Item1 == index && entry.Item2.Equals(role)).FirstOrDefault();
                    if (chkList.Contains(checkedData))
                    {
                        CheckBox myCheckBox = (CheckBox)gvrow.FindControl("chkTask");
                        myCheckBox.Checked = true;
                    }
                }
            }
        }
        protected void grdTask_Sorting(object sender, GridViewSortEventArgs e)
        {

        }
        protected void AddInstancesSortImage(int columnIndex, GridViewRow headerRow)
        {
            try
            {
                System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
                sortImage.ImageAlign = ImageAlign.AbsMiddle;

                if (direction == SortDirection.Ascending)
                {
                    sortImage.ImageUrl = "../Images/SortAsc.gif";
                    sortImage.AlternateText = "Ascending Order";
                }
                else
                {
                    sortImage.ImageUrl = "../Images/SortDesc.gif";
                    sortImage.AlternateText = "Descending Order";
                }
                headerRow.Cells[columnIndex].Controls.Add(sortImage);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CustomModifyAsignment.IsValid = false;
                CustomModifyAsignment.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void CheckBoxValueSaved()
        {
            List<Tuple<long, string>> chkList = new List<Tuple<long, string>>();
            int index = -1;
            foreach (GridViewRow gvrow in grdComplianceInstances.Rows)
            {
                index = Convert.ToInt32(grdComplianceInstances.DataKeys[gvrow.RowIndex].Value);
                bool result = ((CheckBox)gvrow.FindControl("chkCompliances")).Checked;
                string role = "Performer";
                if (ViewState["AssignedCompliancesID"] != null)
                    chkList = (List<Tuple<long, string>>)ViewState["AssignedCompliancesID"];

                var checkedData = chkList.Where(entry => entry.Item1 == index && entry.Item2.Equals(role)).FirstOrDefault();
                if (result)
                {
                    if (checkedData == null)
                        chkList.Add(new Tuple<long, string>(index, role));
                }
                else
                    chkList.Remove(checkedData);
            }
            if (chkList != null && chkList.Count > 0)
                ViewState["AssignedCompliancesID"] = chkList;
        }

        protected void ddlUserList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int cuid = -1;
                int userID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    cuid = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "MGMT")
                {
                    cuid = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else
                {
                    if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                    {
                        cuid = Convert.ToInt32(ddlCustomer.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlUserList.SelectedValue))
                {
                    userID = Convert.ToInt32(ddlUserList.SelectedValue);
                }
                if (cuid != -1 && userID != -1)
                {
                    chkEvent.Checked = false;
                    chkCheckList.Checked = false;
                    txtAssigmnetFilter.Text = string.Empty;
                    ViewState["UserID"] = userID;
                    BindNewUserList(cuid, ddlNewUsers);
                    rbtSelectionType.SelectedValue = "0";
                    BindComplianceList(cuid, userID);
                    BindComplianceInstances(cuid, userID);
                    int IsIComplianceApp = 0;
                    Customer customer = CustomerManagement.GetByID(cuid);
                    if (customer.IComplianceApplicable != null)
                    {
                        IsIComplianceApp = Convert.ToInt32(customer.IComplianceApplicable);
                    }
                    if (IsIComplianceApp.ToString() == "1")
                    {
                        divSelectionType.Visible = true;
                    }
                    else
                    {
                        divSelectionType.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {

                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CustomModifyAsignment.IsValid = false;
                CustomModifyAsignment.ErrorMessage = "Server Error Occured. Please try again.";
            }
            
        }
        protected void btnSaveAssignment_Click(object sender, EventArgs e)
        {
            try
            {
                List<string> c = new List<string>();
                c.Add("rahul@avantis.co.in");
                c.Add("Narendra@avantis.info");
                c.Add("deepali@avantis.info");
                c.Add("bhagyesh@avantis.net.in");

                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
               else if (AuthenticationHelper.Role == "MGMT")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                if (AuthenticationHelper.Role == "IMPT")
                {
                    customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                }               
                int userID = -1;
                if (!string.IsNullOrEmpty(ddlUserList.SelectedValue))
                {
                    userID = Convert.ToInt32(ddlUserList.SelectedValue);
                }
              
                if (customerID != -1 && userID !=-1)
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        string[] confirmValue = Request.Form["confirm_value"].Split(',');

                        if (confirmValue[confirmValue.Length - 1] == "Yes")
                        {
                            if (rbtSelectType.SelectedValue == "0")
                            {
                                if (rbtSelectionType.SelectedValue == "0")
                                {
                                    #region Statutory
                                    CheckBoxValueSaved();
                                    List<Tuple<long, string>> chkList = (List<Tuple<long, string>>)ViewState["AssignedCompliancesID"];
                                    if (chkList != null)
                                    {
                                        string action = Convert.ToString(rbtModifyAction.SelectedValue);
                                        if (action.Equals("ApproverDelete"))
                                        {
                                            #region Delete Approver Compliance Assignment                                            
                                            try
                                            {
                                                if (chkList.Count > 0)
                                                {
                                                    ComplianceExclude_Reassign.StatutoryApprover_DeleteComplianceAssignment(chkList, customerID, AuthenticationHelper.UserID);
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                            }
                                            #endregion
                                        }
                                        else if (action.Equals("Delete"))
                                        {
                                            #region Delete Assignment
                                            try
                                            {
                                                
                                                    if (chkList.Count > 0)
                                                    {
                                                        ComplianceExclude_Reassign.Statutory_DeleteComplianceAssignment(userID, chkList, customerID, AuthenticationHelper.UserID);
                                                    }
                                                
                                            }
                                            catch (Exception ex)
                                            {
                                                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                            }
                                            #endregion
                                        }
                                        else
                                        {
                                            #region Ressign Performer /Reviewer
                                            long pID = -1;
                                            long rID = -1;
                                            if (!String.IsNullOrEmpty(ddlNewUsers.SelectedValue))
                                            {
                                                if (ddlNewUsers.SelectedValue != "-1")
                                                {
                                                    pID = Convert.ToInt32(ddlNewUsers.SelectedValue);
                                                }
                                            }
                                            if (!String.IsNullOrEmpty(ddlNewReviewerUsers.SelectedValue))
                                            {
                                                if (ddlNewReviewerUsers.SelectedValue != "-1")
                                                {
                                                    rID = Convert.ToInt32(ddlNewReviewerUsers.SelectedValue);
                                                }
                                            }

                                            List<int> aproverIds = new List<int>();
                                            foreach (RepeaterItem aItem in rptApprover.Items)
                                            {
                                                CheckBox chkApprover = (CheckBox)aItem.FindControl("chkApprover");
                                                if (chkApprover.Checked)
                                                {
                                                    aproverIds.Add(Convert.ToInt32(((Label)aItem.FindControl("lblapproverid")).Text.Trim()));
                                                }
                                            }
                                            if (pID == -1 && rID == -1 && aproverIds.Count < 0)
                                            {
                                                CustomModifyAsignment.IsValid = false;
                                                CustomModifyAsignment.ErrorMessage = "Please select at list one User to proceed.";
                                                return;
                                            }
                                            else
                                            {
                                                ComplianceExclude_Reassign.Statutory_ReplaceUserForComplianceAssignmentNew(pID, rID, aproverIds, AuthenticationHelper.UserID, chkList);
                                            }
                                            #endregion
                                        }
                                      
                                        if (rbtSelectionType.SelectedValue == "0")
                                        {
                                            BindComplianceList(customerID, userID);
                                            BindComplianceInstances(customerID, userID);
                                        }
                                        else
                                        {
                                            BindInternalComplianceList(customerID, userID);
                                            BindInternalComplianceInstances(customerID, userID);
                                        }
                                        ViewState["AssignedCompliancesID"] = null;
                                    }
                                    else
                                    {
                                        ViewState["AssignedCompliancesID"] = null;
                                        CustomModifyAsignment.IsValid = false;
                                        CustomModifyAsignment.ErrorMessage = "Please select at list one Compliance to proceed.";
                                    }
                                    #endregion
                                }
                                else//internal
                                {
                                    #region internal
                                    InternalCheckBoxValueSaved();
                                    List<Tuple<long, string>> chkIList = (List<Tuple<long, string>>)ViewState["AssignedInternalCompliancesID"];
                                    if (chkIList != null)
                                    {
                                        string action = Convert.ToString(rbtModifyAction.SelectedValue);
                                        if (action.Equals("ApproverDelete"))
                                        {
                                            #region Delete Approver Internal Assignment


                                            try
                                            {
                                                if (chkIList.Count > 0)
                                                {
                                                    ComplianceExclude_Reassign.InternalApprover_DeleteComplianceAssignment(chkIList, customerID, AuthenticationHelper.UserID);
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                            }

                                            #endregion
                                        }
                                        else if (action.Equals("Delete"))
                                        {
                                            #region Delete Assignment                                                                                    
                                            try
                                            {

                                                if (chkIList.Count > 0)
                                                {
                                                    ComplianceExclude_Reassign.Internal_DeleteComplianceAssignment(userID, chkIList, customerID, AuthenticationHelper.UserID);
                                                }

                                            }
                                            catch (Exception ex)
                                            {
                                                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                            }
                                            #endregion
                                        }
                                        else
                                        {
                                            #region Ressign Performer /Reviewer
                                            long pID = -1;
                                            long rID = -1;
                                            long aID = -1;
                                            if (!String.IsNullOrEmpty(ddlNewUsers.SelectedValue))
                                            {
                                                if (ddlNewUsers.SelectedValue != "-1")
                                                {
                                                    pID = Convert.ToInt32(ddlNewUsers.SelectedValue);
                                                }
                                            }
                                            if (!String.IsNullOrEmpty(ddlNewReviewerUsers.SelectedValue))
                                            {
                                                if (ddlNewReviewerUsers.SelectedValue != "-1")
                                                {
                                                    rID = Convert.ToInt32(ddlNewReviewerUsers.SelectedValue);
                                                }
                                            }

                                            List<int> aproverIds = new List<int>();
                                            foreach (RepeaterItem aItem in rptApprover.Items)
                                            {
                                                CheckBox chkApprover = (CheckBox)aItem.FindControl("chkApprover");
                                                if (chkApprover.Checked)
                                                {
                                                    aproverIds.Add(Convert.ToInt32(((Label)aItem.FindControl("lblapproverid")).Text.Trim()));
                                                }
                                            }
                                            if (pID == -1 && rID == -1 && aproverIds.Count < 0)
                                            {
                                                CustomModifyAsignment.IsValid = false;
                                                CustomModifyAsignment.ErrorMessage = "Please select at list one User to proceed.";
                                                return;
                                            }
                                            else
                                            {
                                                ComplianceExclude_Reassign.Internal_ReplaceUserForComplianceAssignmentNew(pID, rID, aproverIds, AuthenticationHelper.UserID, chkIList);
                                            }
                                            #endregion
                                        }
                                        if (rbtSelectionType.SelectedValue == "0")
                                        {
                                            BindComplianceList(customerID, userID);
                                            BindComplianceInstances(customerID, userID);
                                        }
                                        else
                                        {
                                            BindInternalComplianceList(customerID, userID);
                                            BindInternalComplianceInstances(customerID, userID);
                                        }
                                        ViewState["AssignedInternalCompliancesID"] = null;
                                    }
                                    else
                                    {
                                        ViewState["AssignedInternalCompliancesID"] = null;
                                        CustomModifyAsignment.IsValid = false;
                                        CustomModifyAsignment.ErrorMessage = "Please select at list one Compliance to proceed.";
                                    }
                                    #endregion
                                }
                            }//Task
                            else
                            {
                                #region Task
                                Task_CheckBoxValueSaved();
                                List<Tuple<long, int>> chkList = (List<Tuple<long, int>>)ViewState["AssignedTaskID"];
                                if (chkList != null)
                                {
                                    string action = Convert.ToString(rbtModifyAction.SelectedValue);
                                    if (action.Equals("ApproverDelete"))
                                    {
                                        #region Delete Approver Internal Assignment

                                        try
                                        {
                                            if (chkList.Count > 0)
                                            {
                                                ComplianceExclude_Reassign.TaskApprover_DeleteComplianceAssignment(chkList, customerID, AuthenticationHelper.UserID);
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                        }

                                        #endregion
                                    }
                                    else if (action.Equals("Delete"))
                                    {
                                        #region Delete Assignment
                                        try
                                        {

                                            if (chkList.Count > 0)
                                            {
                                                bool taskresult = ComplianceExclude_Reassign.DeleteTaskAssignmentTask(userID, chkList, customerID, AuthenticationHelper.UserID);
                                                if (taskresult)
                                                {
                                                    if (rbtSelectionType.SelectedValue == "0")
                                                    {
                                                        BindComplianceList(customerID, userID);
                                                        BindComplianceInstances(customerID, userID);
                                                    }
                                                    else
                                                    {
                                                        BindInternalComplianceList(customerID, userID);
                                                        BindInternalComplianceInstances(customerID, userID);
                                                    }
                                                }
                                                else
                                                {
                                                    ViewState["AssignedTaskID"] = null;
                                                    CustomModifyAsignment.IsValid = false;
                                                    CustomModifyAsignment.ErrorMessage = "Subtask is exist.";
                                                }
                                            }

                                        }
                                        catch (Exception ex)
                                        {
                                            LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                        }

                                        #endregion
                                    }
                                    else
                                    {
                                       
                                        #region Ressign Performer /Reviewer
                                        long pID = -1;
                                        long rID = -1;
                                        long aID = -1;
                                        if (!String.IsNullOrEmpty(ddlNewUsers.SelectedValue))
                                        {
                                            if (ddlNewUsers.SelectedValue != "-1")
                                            {
                                                pID = Convert.ToInt32(ddlNewUsers.SelectedValue);
                                            }
                                        }
                                        if (!String.IsNullOrEmpty(ddlNewReviewerUsers.SelectedValue))
                                        {
                                            if (ddlNewReviewerUsers.SelectedValue != "-1")
                                            {
                                                rID = Convert.ToInt32(ddlNewReviewerUsers.SelectedValue);
                                            }
                                        }

                                        List<int> aproverIds = new List<int>();
                                        foreach (RepeaterItem aItem in rptApprover.Items)
                                        {
                                            CheckBox chkApprover = (CheckBox)aItem.FindControl("chkApprover");
                                            if (chkApprover.Checked)
                                            {
                                                aproverIds.Add(Convert.ToInt32(((Label)aItem.FindControl("lblapproverid")).Text.Trim()));
                                            }
                                        }
                                        if (pID == -1 && rID == -1 && aproverIds.Count < 0)
                                        {
                                            CustomModifyAsignment.IsValid = false;
                                            CustomModifyAsignment.ErrorMessage = "Please select at list one User to proceed.";
                                            return;
                                        }
                                        else
                                        {
                                            ComplianceExclude_Reassign.ReassignTaskNew(pID, rID, aproverIds, AuthenticationHelper.UserID, chkList);
                                        }

                                        if (rbtSelectionType.SelectedValue == "0")
                                        {
                                            BindComplianceList(customerID, userID);
                                            BindComplianceInstances(customerID, userID);
                                        }
                                        else
                                        {
                                            BindInternalComplianceList(customerID, userID);
                                            BindInternalComplianceInstances(customerID, userID);
                                        }
                                        #endregion
                                    }

                                   
                                    if (rbtSelectionType.SelectedValue == "0")
                                    {                                       
                                        BindComplianceList(customerID, userID);
                                        BindComplianceInstances(customerID, userID);
                                    }
                                    else
                                    {
                                        BindInternalComplianceList(customerID, userID);
                                        BindInternalComplianceInstances(customerID, userID);
                                    }
                                    ViewState["AssignedTaskID"] = null;
                                }
                                else
                                {
                                    ViewState["AssignedTaskID"] = null;
                                    CustomModifyAsignment.IsValid = false;
                                    CustomModifyAsignment.ErrorMessage = "Please select at list one Compliance to proceed.";
                                }
                                #endregion
                            }
                        }
                        else
                        {
                            CustomModifyAsignment.IsValid = false;
                            CustomModifyAsignment.ErrorMessage = "Please select User and Compliance to proceed.";
                        }
                    }
                }
                else
                {
                    CustomModifyAsignment.IsValid = false;
                    CustomModifyAsignment.ErrorMessage = "Please select Customer and User to proceed.";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CustomModifyAsignment.IsValid = false;
                CustomModifyAsignment.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void upModifyAssignment_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void ddlCompliance_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int cuid = -1;
                int userID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    cuid = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "MGMT")
                {
                    cuid = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else
                {
                    if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                    {
                        cuid = Convert.ToInt32(ddlCustomer.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlUserList.SelectedValue))
                {
                    userID = Convert.ToInt32(ddlUserList.SelectedValue);
                }
                if (cuid != -1 && userID != -1)
                {
                    if (rbtSelectionType.SelectedValue == "0")
                    {
                        if (rbtSelectType.SelectedValue == "0")
                        {
                            divEventBase.Visible = true;
                            BindComplianceInstances(cuid,userID);
                        }
                        else
                        {
                            divEventBase.Visible = false;
                            BindComplianceInstances(cuid,userID);
                        }
                    }
                    else
                    {
                        BindInternalComplianceInstances(cuid,userID);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

        }
    }
}