﻿<%@ Page Title="User Parameters" Language="C#" MasterPageFile="~/Compliance.Master"
    AutoEventWireup="true" CodeBehind="UserParameterList.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Users.UserParameterList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   
    <style type="text/css">
        #progressbar .ui-progressbar-value
        {
            background-color: #ccc;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0;
                right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px;
                    position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upUserParameterList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <table width="100%">
                <tr>
                    <td style="width: 25%">
                        <asp:LinkButton Text="Add New" runat="server" ID="btnAddUserParameter" OnClick="btnAddUserParameter_Click" />
                    </td>
                    <td align="right" style="width: 75%; padding-right: 20px">
                        Filter :
                        <asp:TextBox runat="server" ID="tbxFilter" Width="250px" MaxLength="50" AutoPostBack="true"
                            OnTextChanged="tbxFilter_TextChanged" />
                    </td>
                </tr>
            </table>
            <asp:GridView runat="server" ID="grdUserParameter" AutoGenerateColumns="false" GridLines="Vertical"
                BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px"
                CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="12" Width="100%"
                Font-Size="11px" DataKeyNames="ID" OnRowCommand="grdUserParameter_RowCommand"
                OnPageIndexChanging="grdUserParameter_PageIndexChanging">
                <Columns>
                    <asp:TemplateField ItemStyle-Width="60px">
                        <ItemTemplate>
                            <asp:LinkButton runat="server" CommandName="EDIT_USER_PARAMETER" CommandArgument='<%# Eval("ID") %>'><img src="../Images/edit_icon.png" alt="Edit User Parameter" title="Edit User Parameter" /></asp:LinkButton>
                            <asp:LinkButton runat="server" CommandName="DELETE_USER_PARAMETER" CommandArgument='<%# Eval("ID") %>'
                                OnClientClick="return confirm('Are you certain you want to delete this User Parameter?');"><img src="../Images/delete_icon.png" alt="Delete User Parameter" title="Delete User Parameter" /></asp:LinkButton>
                        </ItemTemplate>
                        <HeaderTemplate>
                        </HeaderTemplate>
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:BoundField DataField="Name" HeaderText="Name" />
                    <asp:TemplateField HeaderText="Data Type">
                        <ItemTemplate>
                            <%# ((com.VirtuosoITech.ComplianceManagement.Business.Data.DataType)Convert.ToByte(Eval("DataType"))).ToString()%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Length" HeaderText="Length"  ItemStyle-Height="20px"  HeaderStyle-Height="20px" />
                    <asp:TemplateField HeaderText="Created On">
                        <ItemTemplate>
                            <%# ((DateTime)Eval("CreatedOn")).ToString("dd-MMM-yy HH:mm")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <FooterStyle BackColor="#CCCC99" />
                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium"/>
                <pagersettings position="Top" />
                <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                <HeaderStyle CssClass="ui-widget-header"></HeaderStyle>
                <AlternatingRowStyle BackColor="#e1e1e1" />
                <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                <EmptyDataTemplate>
                    No Records Found.
                </EmptyDataTemplate>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div id="divUserParametersDialog">
        <asp:UpdatePanel ID="upUserParameters" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div style="margin: 5px">
                    <div style="margin-bottom: 4px">
                        <asp:ValidationSummary runat="server" CssClass="vdsummary"
                            ValidationGroup="UserParameterValidationGroup" />
                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                            ValidationGroup="UserParameterValidationGroup" Display="None" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Name:</label>
                        <asp:TextBox runat="server" ID="tbxName" Style="height: 16px; width: 200px;" MaxLength="100" />
                        <asp:RequiredFieldValidator ErrorMessage="Name can not be empty." ControlToValidate="tbxName"
                            runat="server" ValidationGroup="UserParameterValidationGroup" Display="None" />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="None" runat="server"
                            ValidationGroup="UserParameterValidationGroup" ErrorMessage="Please enter a valid parameter name."
                            ControlToValidate="tbxName" ValidationExpression="^[a-zA-Z_]+[a-zA-Z0-9_ .]*$"></asp:RegularExpressionValidator>
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Data Type:</label>
                        <asp:DropDownList runat="server" ID="ddlDataType" Style="padding: 0px; margin: 0px;
                            height: 22px; width: 200px;" CssClass="txtbox" />
                        <asp:CompareValidator ErrorMessage="Please select data type." ControlToValidate="ddlDataType"
                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="UserParameterValidationGroup"
                            Display="None" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Length:</label>
                        <asp:TextBox runat="server" ID="tbxLength" Style="height: 16px; width: 200px;" MaxLength="4" />
                        <asp:RequiredFieldValidator ID="rfvLength" ErrorMessage="Length can not be empty."
                            ControlToValidate="tbxLength" runat="server" ValidationGroup="UserParameterValidationGroup"
                            Display="None" />
                        <asp:CompareValidator ID="cvLength" ErrorMessage="Parameter length is not valid."
                            ControlToValidate="tbxLength" Operator="DataTypeCheck" Type="Integer" runat="server"
                            Display="None" ValidationGroup="UserParameterValidationGroup" />
                    </div>
                    <div style="margin-bottom: 7px; float: right;">
                        <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="button"
                            ValidationGroup="UserParameterValidationGroup" />
                        <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="button" OnClientClick="$('#divUserParametersDialog').dialog('close');" />
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

     <script type="text/javascript">
         $(function () {
             $('#divUserParametersDialog').dialog({
                 height: 300,
                 width: 500,
                 autoOpen: false,
                 draggable: true,
                 dialogClass: 'no-close',
                 title: "User Parameter",
                 open: function (type, data) {
                     $(this).parent().appendTo("form");
                 }
             });
         });

         function initializeRadioButtonsList(controlID) {
             $(controlID).buttonset();
         }
    </script>
</asp:Content>
