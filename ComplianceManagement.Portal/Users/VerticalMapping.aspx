﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VerticalMapping.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Users.VerticalMapping1" %>

<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls"  TagPrefix="asp" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../NewCSS/stylenew.css" rel="stylesheet" />
    <!-- Bootstrap CSS -->
    <link href="../NewCSS/bootstrap.min.css" rel="stylesheet" />
    <!-- bootstrap theme -->
    <link href="../NewCSS/bootstrap-theme.css" rel="stylesheet" />
    <script type="text/javascript" src="../Newjs/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="../Newjs/bootstrap.min.js"></script>
     <style type="text/css">
        .dd_chk_select {
            
            height: 81px;
            height: 3px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc!important;        
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family:Roboto sans-serif !important;
            margin-bottom: 0px !important;
        }
         .chosen-results {  height: 75px !important
         }
        .chosen-container-single .chosen-single{background:none !important;border:1px solid #c7c7cc   !important;height: 32px  !important;padding: 3px 0px 0px 14px !important; font-family:'Roboto', sans-serif !important; }
    </style> 

    <style type="text/css">
        .dd_chk_select {
            height: 34px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc !important;
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family: Roboto sans-serif !important;
            width:80% !important;
        }
    </style>
    <style type="text/css">
        .ui-widget-header {
            border: 0px !important;
            background: inherit;
            font-size: 20px;
            color: #666666;
            font-weight: normal;
            padding-top: 0px;
            margin-top: 5px;
        }

        .table > tbody > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

            .table > tbody > tr > th > a {
                vertical-align: bottom;
                color: #666666 !important;
                text-decoration: none !important;
                border-bottom: 2px solid #dddddd;
            }

        .table tr th {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .clsheadergrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: bottom !important;
            border-bottom: 2px solid #dddddd !important;
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
            text-align: left;
        }

        .clsROWgrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: top !important;
            border-top: 1px solid #dddddd !important;
            color: #666666 !important;
            font-size: 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>
    <style type="text/css">
        .td1  {
        width:15%
        }
         .td2  {
        width:35%
        }
          .td3  {
        width:15%
        }
           .td4  {
        width:35%
        }
           .chosen-container-single .chosen-single{background:none !important;border:1px solid #c7c7cc   !important;height: 32px  !important;padding: 3px 0px 0px 14px !important; font-family:'Roboto', sans-serif !important; }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="asp" %>
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

         <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
        <asp:UpdatePanel ID="upCompliance" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="col-lg-12 col-md-12 ">
                    <section class="panel">
                        <div class="col-md-12 colpadding0">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server"  class="alert alert-block alert-danger fade in" ValidationGroup="oplValidationGroup" />
                            <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
                            <asp:Label ID="LblErormessage" runat="server" Text="" ForeColor="red"></asp:Label>
                            <div style="padding-left: 30px;">
                                <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                ValidationGroup="oplValidationGroup" Display="None" Enabled="true" ShowSummary="true" />
                            </div>                          
                        </div>
                      
                        <div style="width: 100%; float: left">
                            <div style="width: 13%; float: left">
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 100%;">
                                    <div class="col-md-1 colpadding0" style="width: 39%;">
                                        <p style="color: #999;margin-top: 5px;">Show </p>
                                    </div>
                                    <div class="col-md-2 colpadding0">
                                        <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px; float: left; margin-bottom: 0px;"
                                            AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                                            <asp:ListItem Text="5" Selected="True" />
                                            <asp:ListItem Text="10" />
                                            <asp:ListItem Text="20" />
                                            <asp:ListItem Text="50" />
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div style="width: 87%; float: left">
                                <div class="col-md-12 colpadding0">
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                        <asp:DropDownListChosen runat="server" ID="ddlLegalEntity" class="form-control m-bot15" Width="80%" Height="32px"
                                            AutoPostBack="true" Style="background: none;" OnSelectedIndexChanged="ddlLegalEntity_SelectedIndexChanged" DataPlaceHolder="Legal Entity">
                                        </asp:DropDownListChosen>
                                          <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ErrorMessage="Please Select Entity."
                        ControlToValidate="ddlLegalEntity" runat="server" ValidationGroup="oplValidationGroup"
                        Display="None" />
                                           <asp:CompareValidator ID="CVDept" ErrorMessage="Please Select Entity." ControlToValidate="ddlLegalEntity"
                        runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="oplValidationGroup"
                        Display="None" />
                                    </div>
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                        <asp:DropDownListChosen runat="server" ID="ddlSubEntity1" class="form-control m-bot15" Width="80%" Height="32px"
                                            AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity1_SelectedIndexChanged" DataPlaceHolder="Sub Entity 1">
                                        </asp:DropDownListChosen>
                                    </div>
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                        <asp:DropDownListChosen runat="server" ID="ddlSubEntity2" class="form-control m-bot15" Width="80%" Height="32px"
                                            AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity2_SelectedIndexChanged" DataPlaceHolder="Sub Entity 2">
                                        </asp:DropDownListChosen>
                                    </div>
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                        <asp:DropDownListChosen runat="server" ID="ddlSubEntity3" class="form-control m-bot15" Width="80%" Height="32px"
                                            AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity3_SelectedIndexChanged" DataPlaceHolder="Sub Entity 3">
                                        </asp:DropDownListChosen>
                                    </div>
                                </div>
                                <div class="col-md-12 colpadding0">

                                    <div class="col-md-3 colpadding0" style="margin-top: 5px;">
                                        <asp:DropDownListChosen runat="server" ID="ddlFilterLocation" AutoPostBack="true" DataPlaceHolder="Location"
                                            OnSelectedIndexChanged="ddlFilterLocation_SelectedIndexChanged" Width="80%" Height="32px" class="form-control m-bot15">
                                        </asp:DropDownListChosen>
                                    </div>

                                    <div class="col-md-3 colpadding0" style="margin-top: 5px;">
                                        <asp:DropDownCheckBoxes ID="ddlVerticalBranch" runat="server" AutoPostBack="true" Visible="true" CssClass="form-control m-bot15"
                                            AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True" Style="padding: 0px; margin: 0px; width: 80%; height: 50px;">
                                            <Style SelectBoxWidth="320" DropDownBoxBoxWidth="300" DropDownBoxBoxHeight="130" />
                                            <Texts SelectBoxCaption="Select Vertical" />
                                        </asp:DropDownCheckBoxes>
                                           
 
                                    </div>

                                    <div class="col-md-3 colpadding0" style="margin-top: 5px;">                                        
                                            <asp:Button Text="Save" runat="server" ID="btnSave" ValidationGroup="oplValidationGroup" OnClick="btnSave_Click" style="margin-left:40px;" Width="100px" CssClass="btn btn-primary"/>                                        
                                        </div>

                                    <div class="col-md-3 colpadding0" style="margin-top: 5px;">
                                        <center>
                                            <asp:Button ID="btnUploadFile1" runat="server" Text="Download" OnClick="btnUploadFile1_Click" Width="100px" CssClass="btn btn-primary" style="margin-left:-48px;"/>
                                        </center>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <table style="width: 100%;">
                        <tr>
                            <td style="text-align: center;" colspan="4">
                                
                            </td>
                            
                        </tr>
                         <tr>
                        <td style="text-align: right;" colspan="4">
                             
                            </td>
                             </tr>
                        <tr>
                            <td colspan="4">
                                <asp:GridView runat="server" ID="grdRiskActivityMatrix" AutoGenerateColumns="false" AutoPostBack="true" AllowSorting="true"
                                    PageSize="5" AllowPaging="true" DataKeyNames="ID" CssClass="table" GridLines="none" Width="100%"                                   
                                    OnPageIndexChanging="grdRiskActivityMatrix_PageIndexChanging">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Sr">
                                            <ItemTemplate>
                                                <%#Container.DataItemIndex+1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Customer">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                    <asp:Label ID="lblControlNo" runat="server" Text='<%# Eval("CustomerName") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Entity/Location">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                    <asp:Label ID="ItemTemplateProcessName" runat="server" Text='<%# Eval("CustomerBranchName") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Vertical">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                    <asp:Label ID="ItemTemplateSubProcessName" runat="server" Text='<%# Eval("VerticalName") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <RowStyle CssClass="clsROWgrid" />
                                    <HeaderStyle CssClass="clsheadergrid" />
                                       <PagerSettings Visible="false" />   
                                    <PagerTemplate>
                                       <%-- <table style="display: none">
                                            <tr>
                                                <td>
                                                    <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                </td>
                                            </tr>
                                        </table>--%>
                                    </PagerTemplate>
                                      </asp:GridView>
                                    <div style="float: right;">
                                     <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true" AllowSingleDeselect="false"
                                          class="form-control m-bot15"  Width="120%" Height="30px"  OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">                                   
                                    </asp:DropDownListChosen>  
                                   </div>
                              
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <div class="col-md-12 colpadding0">
                                    <div class="col-md-5 colpadding0">
                                        <div class="table-Selecteddownload">
                                            <div class="table-Selecteddownload-text">
                                                <p>
                                                    <asp:Label runat="server" ID="lblTotalSelected" Text="" Style="color: #999; margin-right: 10px;"></asp:Label>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 colpadding0" style="float: right">
                                        <div class="table-paging" style="margin-bottom: 10px;">
                                            <%--<asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick="lBPrevious_Click" />--%>
                                            <div class="table-paging-text" style="margin-top:-35px;margin-left:19px;"  >
                                                <p>
                                                    Page
                                                   <%-- <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                            <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>--%>
                                                </p>
                                            </div>
                                            <%--<asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick="lBNext_Click" />--%>
                                            <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                        </div>
                                    </div>
                                </div>
                            
                            </td>
                        </tr>
                    </table>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnUploadFile1" />
            </Triggers>
        </asp:UpdatePanel>
    </form>
</body>
</html>
