﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Users
{
    public partial class PerformerReviewerStatus : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
             
                string str = Request.QueryString["CID"].ToString();
                //int CustomerID = Convert.ToInt32(Request.QueryString["CustomerID"]);

                if (!string.IsNullOrEmpty(str))
                {
                    string[] s2 = str.Split(',');

                    long ComplianceInstanceID = Convert.ToInt64(s2[0]);
                    int CustomerID = Convert.ToInt32(s2[2]);
                    String flag = Convert.ToString(s2[1]);
                    ViewState["Complianceinstanceid"] = ComplianceInstanceID;
                    if (flag == "I")
                    {

                        #region Internal                        
                        var GridInternalData = GetInternalCannedReportDataForPerformer(CustomerID, (int)ComplianceInstanceID);    //(int)AuthenticationHelper.
                        GridInternalData = GridInternalData.Where(row => row.InternalComplianceInstanceID == ComplianceInstanceID).ToList();
                        grdComplianceTransactions.DataSource = GridInternalData;
                        grdComplianceTransactions.DataBind();
                        #endregion

                    }
                    else if (flag == "S")
                    {                        
                        var GridData = GetCannedReportDataForPerformer(CustomerID, (int)ComplianceInstanceID);     //(int)AuthenticationHelper.
                        GridData = GridData.Where(row => row.ComplianceInstanceID == ComplianceInstanceID).ToList();
                        
                        grdComplianceTransactions.DataSource = GridData;
                        grdComplianceTransactions.DataBind();
                        
                    }
                }
                //if (!string.IsNullOrEmpty(Request.QueryString["CID"]))
                //{                    
                //    long ComplianceInstanceId = Convert.ToInt32(Request.QueryString["CID"].ToString());                   
                //    var GridData = GetCannedReportDataForPerformer((int)AuthenticationHelper.CustomerID, (int)ComplianceInstanceId);
                //    GridData = GridData.Where(row => row.ComplianceInstanceID == ComplianceInstanceId).ToList(); 
                //    grdComplianceTransactions.DataSource = GridData;
                //    grdComplianceTransactions.DataBind();
                //}
            }
        }
        public static List<SP_GetCompliancesStatusSummary_Result> GetCannedReportDataForPerformer(int Customerid, int ComplianceInstanceId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<SP_GetCompliancesStatusSummary_Result> transactionsQuery = new List<SP_GetCompliancesStatusSummary_Result>();
                transactionsQuery = (entities.SP_GetCompliancesStatusSummary(Customerid, ComplianceInstanceId)).ToList();
                return transactionsQuery.ToList();
            }
        }
        public static List<SP_GetInternalCompliancesStatusSummary_Result> GetInternalCannedReportDataForPerformer(int Customerid, int ComplianceInstanceId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<SP_GetInternalCompliancesStatusSummary_Result> transactionsQuery = new List<SP_GetInternalCompliancesStatusSummary_Result>();
                transactionsQuery = (entities.SP_GetInternalCompliancesStatusSummary(Customerid, ComplianceInstanceId)).ToList();
                
                return transactionsQuery.ToList();
            }
        }
        protected void grdComplianceTransactions_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                string str = Request.QueryString["CID"].ToString();
                string[] s2 = str.Split(',');
                long ComplianceInstanceID = Convert.ToInt64(s2[0]);
                String flag = Convert.ToString(s2[1]);
                int cominstanceid = Convert.ToInt32(ViewState["Complianceinstanceid"]);
                long complianceScheduleOnID = Convert.ToInt32(e.CommandArgument);
                if (flag=="S")
                {
                    
                    if(e.CommandName.Equals("Delete_COMPLIANCE"))
                    {
                        DeleteComplianceScheduleOn(complianceScheduleOnID, cominstanceid);
                    }
                    else if (e.CommandName.Equals("RowEdit"))
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "onclick", "javascript:window.open('/Controls/compliancestatusreviewernew.aspx?sId=" + complianceScheduleOnID + "&InId=" + cominstanceid + "', '_blank', 'height=300px,width=600px,scrollbars=1'); ", true);
                    }

                    var GridData = GetCannedReportDataForPerformer((int)AuthenticationHelper.CustomerID, (int)ComplianceInstanceID);
                    GridData = GridData.Where(row => row.ComplianceInstanceID == ComplianceInstanceID).ToList();

                    grdComplianceTransactions.DataSource = GridData;
                    grdComplianceTransactions.DataBind();
                }
                else if(flag=="I")
                {
                    if (e.CommandName.Equals("Delete_COMPLIANCE"))
                    {
                        InternalDeleteComplianceScheduleOn(complianceScheduleOnID, cominstanceid);
                    }
                    else if (e.CommandName.Equals("RowEdit"))
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "onclick", "javascript:window.open('/Controls/InternalComplianceStatusReviewerNew.aspx?sId=" + complianceScheduleOnID + "&InId=" + cominstanceid + "', '_blank', 'height=300px,width=800px,scrollbars=1'); ", true);
                    }
                    var GridInternalData = GetInternalCannedReportDataForPerformer((int)AuthenticationHelper.CustomerID, (int)ComplianceInstanceID);
                    GridInternalData = GridInternalData.Where(row => row.InternalComplianceInstanceID == ComplianceInstanceID).ToList();
                    grdComplianceTransactions.DataSource = GridInternalData;
                    grdComplianceTransactions.DataBind();

                }
               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static void RejectedInternalCompliance(long scheduleonId, long cominstanceid)
        {
            string flag = "I";
            string ISrejectdelete = "R";
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                      var IcomplianceScheduleOns = (from row in entities.InternalComplianceScheduledOns
                                                 where row.InternalComplianceInstanceID == cominstanceid &&
                                                 row.ID==scheduleonId &&
                                                 row.IsActive == true && row.IsUpcomingNotDeleted == true
                                                 select row).ToList();

                CreateLogForScheduleOn(scheduleonId, flag, ISrejectdelete);
                CreateInternalComplianceTransactionEntry(scheduleonId, cominstanceid);

            }
        }
        public static void RejectedStatutoryCompliance(long scheduleonId,long cominstanceid)
        {
            string flag = "S";
            string ISrejectdelete = "R";
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                   var complianceScheduleOns = (from row in entities.ComplianceScheduleOns
                                             where row.ComplianceInstanceID == cominstanceid && row.ID==scheduleonId &&
                                             row.IsActive == true && row.IsUpcomingNotDeleted == true
                                             select row).ToList();

                   CreateLogForScheduleOn(scheduleonId, flag, ISrejectdelete);
                   CreateComplianceTransactionEntry(scheduleonId, cominstanceid);
            }
        }
     
        public static void InternalDeleteComplianceScheduleOn(long scheduleOnID,long instanceid)
        {
            string flag = "I";
            string ISrejectdelete = "D";
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                InternalComplianceScheduledOn complianceScheduleOn = (from row in entities.InternalComplianceScheduledOns
                                                             where row.ID == scheduleOnID && row.InternalComplianceInstanceID==instanceid
                                                             select row).FirstOrDefault();

                
                complianceScheduleOn.IsActive = false;
                complianceScheduleOn.IsUpcomingNotDeleted = false;
                entities.SaveChanges();
                CreateLogForScheduleOn(scheduleOnID, flag, ISrejectdelete);

            }

        }
        public static void DeleteComplianceScheduleOn(long scheduleOnID,long instanceid)
        {
            string flag = "S";
            string ISrejectdelete = "D";
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                ComplianceScheduleOn complianceScheduleOn = (from row in entities.ComplianceScheduleOns
                                                             where row.ID == scheduleOnID && row.ComplianceInstanceID==instanceid
                                                             select row).FirstOrDefault();
              

                complianceScheduleOn.IsActive = false;
                complianceScheduleOn.IsUpcomingNotDeleted = false;
                entities.SaveChanges();
                CreateLogForScheduleOn(scheduleOnID, flag, ISrejectdelete);

            }

        }
   
        public static void CreateLogForScheduleOn(long scheduleid,string flag,string ISrejectdelete)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    Log_RejectDeleteSchedule logchange = new Log_RejectDeleteSchedule()
                    {
                        ScheduleOnID =scheduleid,
                        IsStatutory = flag,
                        IsRejectDelete= ISrejectdelete,
                        CreatedBy = AuthenticationHelper.UserID,
                        CreatedOn = DateTime.Now,
                    };

                    entities.Log_RejectDeleteSchedule.Add(logchange);
                    entities.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static void CreateComplianceTransactionEntry(long scheduleOnID,long complianceinstanceId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                ComplianceTransaction com = new ComplianceTransaction()
                {
                    ComplianceScheduleOnID = Convert.ToInt64(scheduleOnID),
                    ComplianceInstanceId = Convert.ToInt64(complianceinstanceId),
                    CreatedBy = AuthenticationHelper.UserID,
                    CreatedByText = AuthenticationHelper.User,
                    StatusId = 6,
                    Dated= DateTime.Now,
                    StatusChangedOn = DateTime.Now,
                    Remarks ="status changed"
                };
                entities.ComplianceTransactions.Add(com);
                entities.SaveChanges();
            }
        }

        public static void CreateInternalComplianceTransactionEntry(long scheduleOnID, long complianceinstanceId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                InternalComplianceTransaction com = new InternalComplianceTransaction()
                {
                    InternalComplianceScheduledOnID = Convert.ToInt64(scheduleOnID),
                    InternalComplianceInstanceID = Convert.ToInt64(complianceinstanceId),
                    CreatedBy = AuthenticationHelper.UserID,
                    CreatedByText = AuthenticationHelper.User,
                    StatusId = 6,
                    Dated = DateTime.Now,
                    StatusChangedOn = DateTime.Now,
                    Remarks = "status changed"
                };
                entities.InternalComplianceTransactions.Add(com);
                entities.SaveChanges();
            }
        }

        protected bool CanChangeStatus(long ComplianceScheduleOnID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    string str = Request.QueryString["CID"].ToString();

                    string[] s2 = str.Split(',');
                    long ComplianceInstanceID = Convert.ToInt64(s2[0]);
                    String flag = Convert.ToString(s2[1]);
                    long custid = AuthenticationHelper.CustomerID;
                    bool result = false;
                    if (flag == "S")
                    {
                        var status = (from row in entities.SP_GetCompliancesStatusSummary(Convert.ToInt32(custid), Convert.ToInt32(ComplianceInstanceID))
                                      where row.ScheduledOnID == ComplianceScheduleOnID
                                      select row.Status).FirstOrDefault();
                        if (status != null)
                        {
                            if (status == "Open")
                            {
                                result = false;
                            }
                            else if (status == "Rejected")
                            {
                                result = false;
                            }
                            else if (status == "Approved")
                            {
                                result = false;
                            }
                            else if (status == "Submitted For Interim Review")
                            {
                                result = false;
                            }
                            else if (status == "Interim Review Approved")
                            {
                                result = false;
                            }
                            else
                            {
                                result = true;
                            }
                        }

                    }
                    else if (flag == "I")
                    {
                        var status = (from row in entities.SP_GetInternalCompliancesStatusSummary(Convert.ToInt32(custid), Convert.ToInt32(ComplianceInstanceID))
                                      where row.ScheduledOnID == ComplianceScheduleOnID
                                      select row.Status).FirstOrDefault();
                        if (status != null)
                        {
                            if (status == "Open")
                            {
                                result = false;
                            }
                            else if (status == "Rejected")
                            {
                                result = false;
                            }
                            else if (status == "Approved")
                            {
                                result = false;
                            }
                            else if (status == "Submitted For Interim Review")
                            {
                                result = false;
                            }
                            else if (status == "Interim Review Approved")
                            {
                                result = false;
                            }
                            else
                            {
                                result = true;
                            }
                        }

                    }

                    return result;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        protected void grdComplianceTransactions_RowEditing(object sender, GridViewEditEventArgs e)
        {

        }
    }
}