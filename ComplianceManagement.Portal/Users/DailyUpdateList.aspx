﻿<%@ Page Title="Daily Updates" Language="C#" MasterPageFile="~/NewCompliance.Master" AutoEventWireup="true" CodeBehind="DailyUpdateList.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Users.DailyUpdateList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upDocumentDownload" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:UpdateProgress ID="updateProgress" runat="server">
                <ProgressTemplate>
                    <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.2;display:none;">
                        <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif" Visible="false"
                            AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;"  />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel"> 
                            <div class="panel-body"> 
                                 <!--Search Filter-->
                                <div class="col-md-12 colpadding0">
                                    <div class="col-md-12 colpadding0" style="text-align: right; float: right">
                                         <div class="col-md-10 colpadding0" style="width:100%;">                                               
                                                <div class="col-md-2 colpadding0 entrycount" style="float:left;margin-right: -1%;width:12%;">
                                                    <%--<div class="col-md-3 colpadding0">
                                                        <p style="color: #999; margin-top: 5px;float: left">Show </p>
                                                    </div>--%>
                                                  <%--  <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 64px; float: left;margin-left:10px;"
                                                        AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" >                        
                                                        <asp:ListItem Text="5" Selected="True" />
                                                        <asp:ListItem Text="10" />
                                                        <asp:ListItem Text="20" />
                                                        <asp:ListItem Text="50" />
                                                    </asp:DropDownList> --%>                 
                                                </div>
                                                <div class="col-md-2 colpadding0 entrycount" style="float:left;margin-left: -99px;">
                                                      <asp:DropDownList runat="server" ID="ddlComplianceCatagory" class="form-control m-bot15" style="width:150px;" AutoPostBack="true" OnSelectedIndexChanged ="ddlComplianceCatagory_SelectedIndexChanged"/>
                                                 </div>
                                                <div class="col-md-2 colpadding0 entrycount" style="float:left;margin-left: -8px;">
                                                      <asp:DropDownList runat="server" ID="ddlState" class="form-control m-bot15" style="width:160px;" AutoPostBack="true" OnSelectedIndexChanged ="ddlState_SelectedIndexChanged"/>
                                                 </div>
                                                <div class="col-md-2 colpadding0 entrycount" style="float:left;margin-right: -1%;width:20%;">                                                         
                                                       <asp:DropDownList runat="server" ID="ddlAct" class="form-control m-bot15" style="width:180px;margin-left: 2px;" AutoPostBack="true" OnSelectedIndexChanged ="ddlAct_SelectedIndexChanged"/>
                                                </div>
                                              <div class="col-md-1 colpadding0 entrycount" style="float:left;margin-right: -1%;width:20%;">                                                         
                                                       <asp:DropDownList runat="server" ID="ddlFinancialYear" class="form-control m-bot15" style="width:180px;" AutoPostBack="true" OnSelectedIndexChanged="ddlFinancialYear_SelectedIndexChanged">
                                                           <asp:ListItem Text="Select Year" Value="-1"></asp:ListItem>
                                                             <asp:ListItem Text="2021" Value="1"></asp:ListItem>
                                                            <asp:ListItem Text="2020" Value="2"></asp:ListItem>
                                                            <asp:ListItem Text="2019" Value="3"></asp:ListItem>
                                                            <asp:ListItem Text="2018" Value="4"></asp:ListItem>
                                                           </asp:DropDownList>
                                                </div>
                                                <div class="col-md-2 colpadding0 entrycount" style="margin-left: -1px;">
                                                    <asp:TextBox runat="server"  style="width:211px;" placeholder="Type to Search" class="form-group form-control" ID="txtSearchType" CssClass="form-control"  onkeydown = "return (event.keyCode!=13);"/>
                                                </div>                                            
                                               
                                        </div>

                                          <div class="col-md-12 colpadding0" style="width:100%; margin-top:10px;float: right">
                                               <div class="col-md-6 colpadding0 entrycount" >                                                  
                                                    <%--<asp:Button ID="btnSearch" CausesValidation="false" class="btn btn-search"  runat="server" Text="Search" OnClick="btnSearch_Click" Style="margin-left: 876px;" />--%>
                                                  <%-- <asp:LinkButton runat="server" ID="btnSearch1" CssClass="btn btn-primary" Text="Search" OnClick="btnSearch1_Click" ></asp:LinkButton>--%>
                                                </div>
                                              <div class="col-md-6 colpadding0 entrycount" >
                                                   <asp:LinkButton runat="server" ID="btnSearch1" CssClass="btn btn-primary" Text="Search" OnClick="btnSearch1_Click" Style="margin-right: 22px;" ></asp:LinkButton>
                                                   <asp:LinkButton Text="Clear" CssClass="btn btn-primary" runat="server" ID="lnkBtnClearFilter"
                                                     OnClick="lnkBtnClearFilter_Click" OnClientClick="javascript:$('#updateProgress').show()" />
                                              </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"> 
                                    </div>
                                </div>
                                <br />
                                <!--Grid Display-->
                                <div class="col-md-12 colpadding0">
                                    <div style="margin-bottom: 4px">  
                                          <asp:GridView runat="server" ID="grdDailyUpdateList" AutoGenerateColumns="false" PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="none"  OnPageIndexChanging="grdDailyUpdateList_PageIndexChanging" Width="100%"  DataKeyNames="ID">            
                                      
                                       <%-- <asp:GridView runat="server" ID="grdDailyUpdateList" AutoGenerateColumns="false" PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="none"  OnPageIndexChanging="grdDailyUpdateList_PageIndexChanging" Width="100%"  DataKeyNames="ID">            
                                         --%>   <columns>
                                                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Sr.No.">
                                                    <ItemTemplate>
                                                        <%#Container.DataItemIndex+1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="ID" HeaderText="ID" Visible="false" />
                                                <asp:BoundField DataField="Title" HeaderText="Title" ItemStyle-Height="20px" HeaderStyle-Height="20px" ItemStyle-Width="780px" />  
                                                <asp:TemplateField  Visible="false">
                                                    <HeaderTemplate>Description</HeaderTemplate>
                                                    <ItemTemplate>
                                                        <%# Eval ("Description").ToString ().Length > 60 ? Eval ("Description").ToString ().Substring (0, 60) + "..." : Eval("Description").ToString ()%>
                                                    </ItemTemplate>
                                                </asp:TemplateField> 
                                               <asp:TemplateField HeaderText ="Date" >
                                                 <ItemTemplate >
                                                       <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("CreatedDate")   != DBNull.Value ? Convert.ToDateTime(Eval("CreatedDate")).ToString("dd-MMM-yyyy") : "" %>' ToolTip='<%# Eval("CreatedDate") != DBNull.Value ? Convert.ToDateTime(Eval("CreatedDate")).ToString("dd-MMM-yyyy") : "" %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>     
                                                <asp:TemplateField HeaderText = "View" ItemStyle-Width="30">
                                            <ItemTemplate>
                                                <a  onclick="fmodaldailyupdate(this);settracknew('Dashboard','LegalUpdate','Detailedview','');" data-title="<%#Eval("Title")%>" data-desc='<%#Eval("Description").ToString ().Replace("'","\"")%>' data-date="<%#Eval("CreatedDate")%>" title="View" data-toggle="tooltip" style="height:65px;width:60px; cursor:pointer;" ><img src="../Images/view-icon-new.png" /></a>
                                            </ItemTemplate>    
                                            </asp:TemplateField>                       
                                            </columns>    
                                            <RowStyle CssClass="clsROWgrid"   />
                                            <HeaderStyle CssClass="clsheadergrid"    />
                                            <PagerTemplate>
                                                <table style="display: none">
                                                    <tr>
                                                        <td>
                                                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </PagerTemplate>         
                                        </asp:GridView>  
                                    </div>   
                                </div>
                                <div class="col-md-12 colpadding0">
                                   <div class="col-md-1 colpadding0">
                                       
                                        <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 64px; float: left; margin-left: 10px;"
                                            AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                                            <asp:ListItem Text="5" Selected="True" />
                                            <asp:ListItem Text="10" />
                                            <asp:ListItem Text="20" />
                                            <asp:ListItem Text="50" />
                                        </asp:DropDownList>
                   
                                   </div>
                                    <div class="col-md-5 colpadding0">
                                        <div class="table-Selecteddownload">
                                            <div class="table-Selecteddownload-text">
                                                <p><asp:Label runat="server" ID="lblTotalSelected" Text="" style="color: #999; margin-right: 10px;"></asp:Label></p>
                                            </div>                                   
                                        </div>
                                    </div>
                                    <div class="col-md-6 colpadding0" style="float:right;">
                                        <div class="table-paging" style="margin-bottom: 10px;">
                                            <asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick ="lBPrevious_Click"/>                                  
                                            <div class="table-paging-text">
                                                <p>
                                                    <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                                    <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>
                                                </p>
                                            </div>
                                            <asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick ="lBNext_Click" />                                   
                                            <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>

                   
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
        </Triggers>
    </asp:UpdatePanel>

    <div class="modal fade" id="NewsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 900px">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body" id="dailyupdatecontent">
                    <h2 id="dailyupdatedate" style="margin-top: 1px">Daily Updates: November 3, 2016</h2>
                    <h3 id="dailyupdatedateInner">Daily Updates: November 3,
                    </h3>
                    <p id="dailytitle"></p>
                    <div id="contents" style="color: #666666;">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        function timeconvert1(ds) {
            var D, dtime, T, tz, off,
            dobj = ds.match(/(\d+)|([+-])|(\d{4})/g);
            T = parseInt(dobj[0]);
            tz = dobj[1];
            off = dobj[2];
            if (off) {
                off = (parseInt(off.substring(0, 2), 10) * 3600000) +
    (parseInt(off.substring(2), 10) * 60000);
                if (tz == '-') off *= -1;
            }
            else off = 0;
            return new Date(T += off).toUTCString();
        }
        function getmonths1(mon) {
            try {
                return mon.toString().split(' ')[1];
            } catch (e) { }
        }
        function fmodaldailyupdate(obj) {
            var title = $(obj).attr('data-title');
            var desc = $(obj).attr('data-desc');
            var date = $(obj).attr('data-date');
            var objDate = new Date(date);
            //var mon_I = getmonths1(objDate); $("#dailyupdatedate").html('Daily Updates:' + mon_I + ' ' + objDate.getDate() + ', ' + objDate.getFullYear());
            var mon_I = getmonths1(objDate); $("#dailyupdatedate").html('Daily Updates:' + objDate.getDate() + ' ' + mon_I + ', ' + objDate.getFullYear());
            $("#dailyupdatedateInner").html('Daily Updates:' + mon_I + ' ' + objDate.getDate());
            $("#contents").html(desc);
            $("#dailytitle").html(title); $('#NewsModal').modal('show');
        }
        $(document).ready(function () {

            fhead('Daily Updates');
        });
    </script>
    <script type="text/javascript">
        var _paq = window._paq || [];
        /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
        _paq.push(['trackPageView']);
        _paq.push(['enableLinkTracking']);
        (function () {
            var u = "//analytics.avantis.co.in/";
            _paq.push(['setTrackerUrl', u + 'matomo.php']);
            _paq.push(['setSiteId', '1']);
            var d = document, g = d.createElement('script'), s = d.getElementsByTagName('script')[0];
            g.type = 'text/javascript'; g.async = true; g.defer = true; g.src = u + 'matomo.js'; s.parentNode.insertBefore(g, s);
        })();

        function settracknew(e, t, n, r) {
            debugger;
         try {
             _paq.push(['trackEvent', e, t, n])
         } catch (t) { } return !0
     }
</script>
</asp:Content>
