﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Users
{
    public partial class UserParameterList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindDataTypes();
                BindUserParameters();
            }
        }

        private void BindDataTypes()
        {
            try
            {
                ddlDataType.DataTextField = "Name";
                ddlDataType.DataValueField = "ID";

                ddlDataType.DataSource = Enumerations.GetAll<DataType>();
                ddlDataType.DataBind();

                ddlDataType.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindUserParameters()
        {
            try
            {
                grdUserParameter.DataSource = UserManagement.GetAllUserParameters(tbxFilter.Text);
                grdUserParameter.DataBind();
                upUserParameterList.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }


        protected void grdUserParameter_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int userParameterID = Convert.ToInt32(e.CommandArgument);
                if (e.CommandName.Equals("EDIT_USER_PARAMETER"))
                {
                    ViewState["Mode"] = 1;
                    ViewState["UserParameterID"] = userParameterID;

                    UserParameter userParameter = UserManagement.GetUserParameterByID(userParameterID);

                    tbxName.Text = userParameter.Name;
                    ddlDataType.SelectedValue = userParameter.DataType.ToString();
                    tbxLength.Text = userParameter.Length.ToString();

                    upUserParameters.Update();
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divUserParametersDialog\").dialog('open')", true);
                }
                else if (e.CommandName.Equals("DELETE_USER_PARAMETER"))
                {
                    UserManagement.DeleteUserParameter(userParameterID);
                    BindUserParameters();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdUserParameter_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdUserParameter.PageIndex = e.NewPageIndex;
                BindUserParameters();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }


        protected void btnAddUserParameter_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["Mode"] = 0;

                tbxName.Text = string.Empty;
                ddlDataType.SelectedIndex = -1;
                tbxLength.Text = string.Empty;

                upUserParameters.Update();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divUserParametersDialog\").dialog('open')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                grdUserParameter.PageIndex = 0;
                BindUserParameters();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                UserParameter userParameter = new UserParameter()
                {
                    Name = tbxName.Text,
                    DataType = Convert.ToInt32(ddlDataType.SelectedValue),
                    Length = tbxLength.Text.Length > 0 ? Convert.ToInt32(tbxLength.Text) : 0
                };

                if ((int)ViewState["Mode"] == 1)
                {
                    userParameter.ID = Convert.ToInt32(ViewState["UserParameterID"]);
                }

                if (UserManagement.UserParameterExists(userParameter))
                {
                    cvDuplicateEntry.ErrorMessage = "Parameter name already exists.";
                    cvDuplicateEntry.IsValid = false;
                    return;
                }

                if ((int)ViewState["Mode"] == 0)
                {
                    UserManagement.CreateUserParameter(userParameter);
                }
                else if ((int)ViewState["Mode"] == 1)
                {
                    UserManagement.UpdateUserParameter(userParameter);
                }

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divUserParametersDialog\").dialog('close')", true);
                BindUserParameters();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
    }
}