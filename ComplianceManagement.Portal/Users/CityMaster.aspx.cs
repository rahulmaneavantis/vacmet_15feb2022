﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Globalization;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using System.Windows.Forms;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Users
{
    public partial class CityMaster : System.Web.UI.Page
    {

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.ProductApplicableLogin == "C")
                this.MasterPageFile = "~/NewCompliance.Master";
            else if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.ProductApplicableLogin == "L")
                this.MasterPageFile = "~/LitigationMaster.Master";
            else
                this.MasterPageFile = "~/AuditTool.Master";
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["SortOrder"] = "Asc";
                ViewState["SortExpression"] = "Name";
                BindCityData(); BindState();
                bindPageNumber(); BindCountry();
                // GetPageDisplaySummary();
            }
        }

        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdAuditor.PageIndex = chkSelectedPage - 1;

            //SelectedPageNo.Text = (chkSelectedPage).ToString();
            grdAuditor.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            //grdUser.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

            BindCityData();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                mst_City objCity = new mst_City()
                {
                    Name = txtFName.Text,
                    StateId = Convert.ToInt32(ddlState.SelectedValue)
                };

                City objCitycom = new City()
                {
                    Name = txtFName.Text,
                    StateId = Convert.ToInt32(ddlState.SelectedValue)
                };

                if ((int) ViewState["Mode"] == 1)
                {
                    objCity.ID = Convert.ToInt32(ViewState["CityID"]);//Need to change DeptID
                }

                if ((int) ViewState["Mode"] == 1)
                {
                    objCitycom.ID = Convert.ToInt32(ViewState["CityID"]);//Need to change DeptID
                }


                if ((int) ViewState["Mode"] == 0)
                {
                    if (CityStateContryManagementRisk.CityExist(objCity.Name))
                    {
                        cvDuplicateLocation.IsValid = false;
                        cvDuplicateLocation.ErrorMessage = "City Already Exists";
                    }
                    else
                    {
                        objCity.IsDeleted = false;
                        CityStateContryManagementRisk.CreateCity(objCity);
                        cvDuplicateLocation.IsValid = false;
                        cvDuplicateLocation.ErrorMessage = "City Saved Successfully";
                        txtFName.Text = string.Empty;
                    }

                    if (CityStateCountryManagement.CityExist(objCitycom.Name))
                    {
                        cvDuplicateLocation.IsValid = false;
                        cvDuplicateLocation.ErrorMessage = "City Already Exists";
                    }
                    else
                    {
                        objCitycom.IsDeleted = false;
                        CityStateCountryManagement.CreateCity(objCitycom);
                        cvDuplicateLocation.IsValid = false;
                        cvDuplicateLocation.ErrorMessage = "City Saved Successfully";
                        txtFName.Text = string.Empty;
                    }
                }

                else if ((int) ViewState["Mode"] == 1)
                {
                    if (CityStateContryManagementRisk.CityExist(objCity.Name))
                    {
                        cvDuplicateLocation.IsValid = false;
                        cvDuplicateLocation.ErrorMessage = "City Already Exists";
                    }
                    else
                    {
                        CityStateContryManagementRisk.UpdateCity(objCity);
                        cvDuplicateLocation.IsValid = false;
                        cvDuplicateLocation.ErrorMessage = "City Updated Successfully";
                    }

                    if (CityStateCountryManagement.CityExist(objCitycom.Name))
                    {
                        cvDuplicateLocation.IsValid = false;
                        cvDuplicateLocation.ErrorMessage = "City Already Exists";
                    }
                    else
                    {
                        CityStateCountryManagement.UpdateCity(objCitycom);
                        cvDuplicateLocation.IsValid = false;
                        cvDuplicateLocation.ErrorMessage = "City Updated Successfully";
                    }
                }
                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divAuditorDialog\").dialog('close')", true);
                BindCityData();
                bindPageNumber();

                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdAuditor.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
                ddlState.Items.Insert(0, new ListItem("Select State", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindCityData()
        {
            try
            {
                var CityList = CityStateCountryManagement.GetAllcity();
                if (!string.IsNullOrEmpty(ddlCountry.SelectedValue))
                {
                    if (ddlCountry.SelectedValue != "-1")
                    {
                        CityList = CityList.Where(Entry => Entry.CountryID == Convert.ToInt32(ddlCountry.SelectedValue)).Distinct().ToList();
                    }
                }
                if (!string.IsNullOrEmpty(ddlStateList.SelectedValue))
                {
                    if (ddlStateList.SelectedValue != "-1")
                    {
                        CityList = CityList.Where(Entry => Entry.StateID == Convert.ToInt32(ddlStateList.SelectedValue)).Distinct().ToList();
                    }
                }
                if (!string.IsNullOrEmpty(tbxSearchCity.Text))
                {
                    CityList = CityList.Where(Entry => Entry.CityName.Equals(tbxSearchCity.Text)).Distinct().ToList();
                }
                grdAuditor.DataSource = CityList;
                Session["TotalRows"] = CityList.Count;
                grdAuditor.DataBind();
                upPromotorList.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void BindState()
        {
            ddlState.DataTextField = "Name";
            ddlState.DataValueField = "ID";
            var StateList = CityStateCountryManagement.GetAllState();
            ddlState.DataSource = StateList;
            ddlState.DataBind();
            ddlState.Items.Insert(0, new ListItem("Select State", "-1"));
        }

        public void BindStateCountryWise(int CountryID)
        {
            ddlStateList.DataTextField = "Name";
            ddlStateList.DataValueField = "ID";
            var StateListcountrywise = CityStateCountryManagement.GetAllStateCountryWise(CountryID);
            ddlStateList.DataSource = StateListcountrywise;
            ddlStateList.DataBind();
            ddlStateList.Items.Insert(0, new ListItem("Select State", "-1"));
            BindCityData();
        }

        public void BindCountry()
        {
            var CountryList = CityStateCountryManagement.GetAllContryData();

            ddlCountry.DataTextField = "Name";
            ddlCountry.DataValueField = "ID";

            ddlCountry.DataSource = CountryList;
            ddlCountry.DataBind();
            ddlCountry.Items.Insert(0, new ListItem("Select Country", "-1"));

        }

        protected void btnAddPromotor_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["Mode"] = 0;
                txtFName.Text = string.Empty;
                upPromotor.Update();
                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divAuditorDialog\").dialog('open')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //if (!IsValid()) { return; };
                grdAuditor.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                //SelectedPageNo.Text = "1";
                //int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                //if (currentPageNo <= GetTotalPagesCount())
                //{
                //    SelectedPageNo.Text = (currentPageNo).ToString();
                //    grdAuditor.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                //    grdAuditor.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                //}
                //else
                //{

                //}
                //Reload the Grid
                BindCityData();
                bindPageNumber();

                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdAuditor.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
            }
            catch (Exception ex)
            {
                // ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }
        protected void grdAuditor_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        protected void grdAuditor_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int ID = Convert.ToInt32(e.CommandArgument);

                if (e.CommandName.Equals("Edit_City"))
                {
                    ViewState["Mode"] = 1;
                    ViewState["CityID"] = ID;
                    City objCity = CityStateCountryManagement.CityGetById(ID);
                    // mst_Department RPD = ProcessManagement.DepartmentMasterGetByID(ID);
                    txtFName.Text = objCity.Name;
                    ddlState.SelectedValue = Convert.ToString(objCity.StateId);
                    upPromotor.Update();
                    //ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divAuditorDialog\").dialog('open')", true);

                }
                else if (e.CommandName.Equals("DELETE_City"))
                {
                    CityStateCountryManagement.DeleteCity(ID);
                    CityStateContryManagementRisk.DeleteCity(ID);
                    BindCityData();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        
        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlCountry.SelectedValue))
            {
                if (ddlCountry.SelectedValue != "-1")
                {
                    BindStateCountryWise(Convert.ToInt32(ddlCountry.SelectedValue));
                }
            }
        }

        protected void ddlStateList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlStateList.SelectedValue))
            {
                if (ddlStateList.SelectedValue != "-1")
                {
                    BindCityData();
                }
            }
        }


        protected void tbxSearchCity_TextChanged(object sender, EventArgs e)
        {
            BindCityData();
        }
    }
}