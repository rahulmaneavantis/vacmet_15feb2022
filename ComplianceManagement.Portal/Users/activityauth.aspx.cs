﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Web.Security;
using System.Configuration;
using System.Drawing;
using com.VirtuosoITech.ComplianceManagement.Portal.ProductMapping;
using System.Text;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Users
{
    public partial class ActivityAuth : System.Web.UI.Page
    {
        static int WrongOTPCount = 0;
        static int ResendOTPAttemp = 3;

        protected string Approveruser_Roles;
        protected List<Int32> roles;
        public int SId;
        public int InId;
        public string type;
        public string role1;
        public string email1;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string cookie = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(Request.Cookies["dld"])))
                {
                    cookie = Convert.ToString(Request.Cookies["dld"].Value);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["sId"]) && !string.IsNullOrEmpty(Request.QueryString["InId"]))
                {
                    SId = Convert.ToInt32(CryptographyManagement.Decrypt(Convert.ToString(Request.QueryString["sId"])));
                    InId = Convert.ToInt32(CryptographyManagement.Decrypt(Convert.ToString(Request.QueryString["InId"])));
                    type = Convert.ToString(CryptographyManagement.Decrypt(Convert.ToString(Request.QueryString["T"])));
                    role1 = Convert.ToString(CryptographyManagement.Decrypt(Convert.ToString(Request.QueryString["r"])));
                    email1 = Convert.ToString(CryptographyManagement.Decrypt(Convert.ToString(Request.QueryString["data"])));
                    bool validate = false;
                    if (!string.IsNullOrEmpty(cookie))
                    {
                        cookie = Convert.ToString(CryptographyManagement.Decrypt(Convert.ToString(Request.Cookies["dld"].Value)));
                        if (email1 == cookie)
                        {
                            //btnOTP_Click(null, null);
                            if (!string.IsNullOrEmpty(Convert.ToString(Session["userID"])))
                            {
                                User user = UserManagement.GetByID(Convert.ToInt32(Session["userID"].ToString()));
                                ProcessAuthenticationInformation(user);
                            }
                            else
                            {
                                validate = true;
                            }
                        }
                        else
                        {
                            validate = true;
                        }
                    }
                    else
                    {
                        validate = true;
                    }

                    if (validate)
                    {
                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        {
                            User user = new User();
                            if (type.ToLower() == "statutory")
                            {                            
                                user = (from row in entities.ComplianceAssignments
                                        join ass in entities.Users on row.UserID equals ass.ID
                                        where row.ComplianceInstanceID == InId && ass.Email == email1
                                        select ass).FirstOrDefault();
                            }
                            if (type.ToLower() == "taskstatutory")
                            {
                                user = (from row in entities.TaskAssignments
                                        join ass in entities.Users on row.UserID equals ass.ID
                                        where row.TaskInstanceID == InId && ass.Email == email1
                                        select ass).FirstOrDefault();
                            }
                            else if (type.ToLower() == "internal")
                            {
                                user = (from row in entities.InternalComplianceAssignments
                                        join ass in entities.Users on row.UserID equals ass.ID
                                        where row.InternalComplianceInstanceID == InId && ass.Email == email1
                                        select ass).FirstOrDefault();
                            }
                            else if (type.ToLower() == "taskinternal")
                            {
                                user = (from row in entities.TaskAssignments
                                        join ass in entities.Users on row.UserID equals ass.ID
                                        where row.TaskInstanceID == InId && ass.Email == email1
                                        select ass).FirstOrDefault();
                            }
                                
                            if (user != null)
                            {
                                Session["userID"] = user.ID;
                                Session["ContactNo"] = user.ContactNumber;
                                Session["Email"] = user.Email;
                                Session["CustomerID_new"] = user.CustomerID;
                               
                            }
                        }
                    }
                    //Util.CalculateAESHashDecrypt();
                }
            }
        }
    

        protected void DisplayOTPMessage()
        {
            if (!string.IsNullOrEmpty(Convert.ToString(Session["Email"])))
            {
                try
                {
                    // For Email Id Logic Set.
                    string[] spl = Convert.ToString(Session["Email"]).Split('@');
                    int lenthusername = (spl[0].Length / 2);
                    int mod = spl[0].Length % 2;
                    if (mod > 0)
                    {
                        lenthusername = lenthusername - mod;
                    }
                    string beforemanupilatedemail = "";
                    if (lenthusername == 1)
                    {
                        beforemanupilatedemail = spl[0].Substring(lenthusername);
                    }
                    else
                    {
                        beforemanupilatedemail = spl[0].Substring(lenthusername + 1);
                    }

                    string appendstar = "";
                    for (int i = 0; i < beforemanupilatedemail.Length; i++) /*lenthusername*/
                    {
                        appendstar += "*";
                    }

                    string manupilatedemail = spl[0].Replace(beforemanupilatedemail, appendstar);

                    string[] spl1 = spl[1].Split('.');
                    int lenthatname = (spl1[0].Length / 2);
                    int modat = spl1[0].Length % 2;
                    if (modat > 0)
                    {
                        lenthatname = lenthatname - modat;
                    }

                    string beforatemail = "";
                    if (lenthatname == 1)
                    {
                        beforatemail = spl1[0].Substring(lenthatname);
                    }
                    else
                    {
                        beforatemail = spl1[0].Substring(lenthatname + 1);
                    }
                    string appendstar1 = "";
                    for (int i = 0; i < beforatemail.Length; i++) /*lenthatname*/
                    {
                        appendstar1 += "*";
                    }

                    string manupilatedatemail = spl1[0].Replace(beforatemail, appendstar1);
                    string emailid = manupilatedemail;

                    DateTime NewTime = DateTime.Now.AddMinutes(30);
                    Time.Text = NewTime.Hour + ":" + NewTime.Minute + NewTime.ToString(" tt");
                    email.Text = Convert.ToString(Session["Email"]).Replace(spl[0], manupilatedemail).Replace(spl1[0], manupilatedatemail);
                    if (!string.IsNullOrEmpty(Convert.ToString(Session["ContactNo"])))
                    {
                        // For Mobile Number Logic Set.
                        if (Convert.ToString(Session["ContactNo"]).Length == 10 && (Convert.ToString(Session["ContactNo"]).StartsWith("9") || Convert.ToString(Session["ContactNo"]).StartsWith("8") || Convert.ToString(Session["ContactNo"]).StartsWith("7")))
                        {
                            string s = Convert.ToString(Session["ContactNo"]);
                            int l = s.Length;
                            s = s.Substring(l - 4);
                            string r = new string('*', l - 4);
                            //r = r + s;
                            mobileno.Text = r + s;
                        }
                        else
                        {
                            mobileno.Text = "";
                            cvLogin.IsValid = false;
                            cvLogin.ErrorMessage = "You don't seem to have a correct registered mobile number with us. However OTP sent on your registered email. Please update your mobile number to use this feature in future.";
                        }
                    }
                    else
                    {
                        mobileno.Text = "";
                        cvLogin.IsValid = false;
                        cvLogin.ErrorMessage = "You don't seem to have a correct registered mobile number with us. However OTP sent on your registered email. Please update your mobile number to use this feature in future.";
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvLogin.IsValid = false;
                    cvLogin.ErrorMessage = "Server Error Occured. Please try again.";
                }
            }
        }

        protected void btnGenerate_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(Convert.ToString(Session["userID"])))
                {

                    long Uid = (long)Session["userID"];
                    User user = new User();
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {

                        user = (from ass in entities.Users

                                where ass.ID == Uid
                                    select ass).FirstOrDefault();
                       
                    }
                    if (user != null)
                    {
                        DateTime dt = new DateTime();
                        int seed =(int)Uid + dt.Day + dt.Minute +dt.Second;
                        Random random = new Random();
                        int value = random.Next(1000000);
                        if(value==0)
                        {
                            value = random.Next(1000000);
                        }
                        Session["ResendOTP"] = Convert.ToString(value);
                        long Contact;
                        string ipaddress = string.Empty;
                        string Macaddress = string.Empty;

                        Macaddress = Util.GetMACAddress();
                        ipaddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                        if (ipaddress == "" || ipaddress == null)
                            ipaddress = Request.ServerVariables["REMOTE_ADDR"];
                        VerifyOTP OTPData = new VerifyOTP()
                        {
                            UserId = Convert.ToInt32(user.ID),
                            EmailId = user.Email,
                            OTP = Convert.ToInt32(value),
                            CreatedOn = DateTime.Now,
                            IsVerified = false,
                            IPAddress = ipaddress
                        };
                      
                        string SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"].ToString();

                        try
                        {
                            if (ConfigurationManager.AppSettings["SkipOTPQA"].ToString() != "1")
                            {
                                //Send Email on User Mail Id.
                                if (user.CustomerID != 5)
                                {
                                    EmailManager.SendMail(SenderEmailAddress, new List<String>(new String[] { user.Email }), null, null, "OTP Verification", "Your One Time Password for Avantis login is:" + Convert.ToString(value) + "<br>" + "<br>" + "Thank you," + "<br>" + "Team Teamlease");                                    
                                    //SendGridEmailManager.SendGridMail(ConfigurationManager.AppSettings["SenderEmailAddress"], ConfigurationManager.AppSettings["ReplyEmailAddressName"],new List<String>(new String[] { user.Email }), null, null, "OTP Verification", "Your One Time Password for Teamlease Regtech login is:" + Convert.ToString(value) + "<br>" + "<br>" + "Thank you," + "<br>" + "Team Teamlease Regtech");
                                }
                            }
                        }
                        catch (Exception ex)
                        {

                            LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                        }
                        bool OTPresult = long.TryParse(user.ContactNumber, out Contact);
                        if (OTPresult)
                        {
                            OTPData.MobileNo = Contact;
                        }
                        else
                        {
                            OTPData.MobileNo = 0;
                        }
                        if (OTPresult && user.RoleID != 12 && user.ContactNumber.Length == 10 && (user.ContactNumber.StartsWith("9") || user.ContactNumber.StartsWith("8") || user.ContactNumber.StartsWith("7") || user.ContactNumber.StartsWith("6")))
                        {
                            try
                            {
                                if (ConfigurationManager.AppSettings["SkipOTPQA"].ToString() != "1")
                                {
                                    //Send SMS on User Mobile No.
                                    var data = VerifyOTPManagement.GetSMSConfiguration("Avacom");
                                    if (data != null)
                                    {
                                        SendSms.sendsmsto(user.ContactNumber, "Your One Time Password for Avantis login is: " + Convert.ToString(value) + ".  " + "Thank you, " + "Avantis Team.", data.TEMPLATEID_DLT_TE_ID, data.authkey, data.Header_sender, data.route);
                                    }
                                    else
                                    {
                                        SendSms.sendsmsto(user.ContactNumber, "Your One Time Password for Avantis login is: " + Convert.ToString(value) + ".  " + "Thank you, " + "Avantis Team.", "1207161856353769674");
                                    }
                                    
                                }
                            }
                            catch (Exception ex)
                            {

                                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                            }
                        }

                        VerifyOTPManagement.Create(OTPData); // Insert Data in OTP Table.
                        MaintainLoginDetail objData = new MaintainLoginDetail()
                        {
                            UserId = Convert.ToInt32(user.ID),
                            Email = user.Email,
                            CreatedOn = DateTime.Now,
                            IPAddress = ipaddress,
                            MACAddress = Macaddress,
                            LoginFrom = "AA",
                            //ProfileID=user.ID
                        };
                        UserManagement.Create(objData);

                        User userToUpdate = new User();
                        userToUpdate.ID = user.ID;
                        userToUpdate.LastLoginTime = DateTime.UtcNow;
                        userToUpdate.WrongAttempt = 0;
                        UserManagement.Update(userToUpdate);
                        
                        WrongOTPCount = 0;
                        DisplayOTPMessage();
                        CheckResendOTPCount();
                        OtpPanel.Visible = true;
                        Generatepanel.Visible = false;
                    }
                    else
                    {

                    }
                }
            }
            catch(Exception ex)
            {

            }

        }

        protected void btnOTP_Click(object sender, EventArgs e)
        {
            try
            {
                SId = Convert.ToInt32(CryptographyManagement.Decrypt(Convert.ToString(Request.QueryString["sId"])));
                InId = Convert.ToInt32(CryptographyManagement.Decrypt(Convert.ToString(Request.QueryString["InId"])));
                type = Convert.ToString(CryptographyManagement.Decrypt(Convert.ToString(Request.QueryString["T"])));
                role1 = Convert.ToString(CryptographyManagement.Decrypt(Convert.ToString(Request.QueryString["r"])));
                email1 = Convert.ToString(CryptographyManagement.Decrypt(Convert.ToString(Request.QueryString["data"])));
                
                if (!string.IsNullOrEmpty(Session["userID"].ToString()))
                {
                    if (VerifyOTPManagement.verifyOTP(Convert.ToInt32(txtOTP.Text), Convert.ToInt32(Session["userID"].ToString())))
                    {
                        User user = UserManagement.GetByID(Convert.ToInt32(Session["userID"].ToString()));

                        int UserId;
                        UserId = int.Parse(Session["userID"].ToString());
                        VerifyOTP OTPData = VerifyOTPManagement.GetByID(UserId);                        
                        TimeSpan span = DateTime.Now.Subtract(OTPData.CreatedOn.Value);                       
                        if (0 <= span.Minutes && span.Minutes <= 30)
                        {
                            if (Convert.ToBoolean(Session["RM"]))
                            {
                                if (FormsAuthentication.CookiesSupported)
                                {
                                    HttpCookie loginCookie = new HttpCookie("ALC");
                                    //let the cookie expire after 60 days
                                    loginCookie.Expires = DateTime.Now.AddDays(60);
                                    loginCookie.Values["EA"] = Convert.ToString(Session["EA"]);
                                    loginCookie.Values["MA"] = Convert.ToString(Session["MA"]);
                                    loginCookie.Secure =true;
                                    loginCookie.HttpOnly = true;

                                    HttpCookie temp = new HttpCookie("ALCA11");
                                    //let the cookie expire after 60 days

                                    temp.Values["LAK"] ="1";

                                    temp.Secure = true;
                                    temp.HttpOnly = true;

                                    Response.Cookies.Add(loginCookie);
                                    Response.Cookies.Add(temp);

                                }
                            }
                            else
                            {
                                Response.Cookies["ALC"].Expires = DateTime.Now.AddDays(-1);
                               
                            }
                            Response.Cookies["dld"].Value = Convert.ToString(CryptographyManagement.Encrypt(email1));
                               
                          
                            VerifyOTPManagement.UpdateVerifiedFlag(Convert.ToInt32(Session["userID"].ToString()), Convert.ToInt32(txtOTP.Text));
                            ProcessAuthenticationInformation(user);
                            Session["RM"] = null;
                            Session["EA"] = null;
                            Session["MA"] = null;
                        }
                        else
                        {
                            cvLogin.ErrorMessage = "OTP Expired.";
                            cvLogin.IsValid = false;
                            return;
                        }
                    }
                    else
                    {
                        if (WrongOTPCount < 3)
                        {
                            cvLogin.ErrorMessage = "Invalid OTP, Please Enter Again.";
                            cvLogin.IsValid = false;
                            WrongOTPCount++;
                            CheckResendOTPCount();
                            return;
                        }
                        else
                        {
                            User user = UserManagement.GetByID(Convert.ToInt32(Session["userID"].ToString()));

                            if (UserManagement.HasUserSecurityQuestion(user.ID))
                            {
                                Session["QuestionBank"] = false;
                                Session["RM"] = Convert.ToBoolean(Session["RM"]);                                
                                Response.Redirect("~/SecurityQuestion/AddSecurityQuestions.aspx", false);
                            }
                            else
                            {
                                Session["QuestionBank"] = true;
                                Response.Redirect("~/SecurityQuestion/AddSecurityQuestions.aspx", false);
                            }
                        }
                    }
                }
                else
                {
                    cvLogin.ErrorMessage = "Please Enter Correct OTP.";
                    cvLogin.IsValid = false;
                    return;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvLogin.IsValid = false;
                cvLogin.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void lnkSecurityQA_Click(object sender, EventArgs e)
        {
            User user = UserManagement.GetByID(Convert.ToInt32(Session["userID"].ToString()));

            if (UserManagement.HasUserSecurityQuestion(user.ID))
            {
                Session["QuestionBank"] = false;
                Session["RM"] = Convert.ToBoolean(Session["RM"]);                
                Response.Redirect("~/SecurityQuestion/AddSecurityQuestions.aspx", false);
            }
            else
            {
                Session["QuestionBank"] = true;
                Response.Redirect("~/SecurityQuestion/AddSecurityQuestions.aspx", false);
            }
        }
        public static bool Exists(long compliancescheduleonid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Lic_tbl_LicenseComplianceInstanceScheduleOnMapping
                             where row.ComplianceScheduleOnID == compliancescheduleonid
                             select row.ID).FirstOrDefault();

                if (query != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        private void ProcessAuthenticationInformation(User user)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                try
                {
                    string name = string.Format("{0} {1}", user.FirstName, user.LastName);
                    string role = RoleManagement.GetByID(user.RoleID).Code;
                    int checkInternalapplicable = 0;
                    int checkTaskapplicable = 0;
                    int checkLabelApplicable = 0;
                    int checkVerticalapplicable = 0;
                    bool IsPaymentCustomer = false;
                    int complianceProdType = 0;
                    if (user.CustomerID == null)
                    {
                        checkInternalapplicable = 2;
                        checkTaskapplicable = 2;
                        checkVerticalapplicable = 2;
                        checkLabelApplicable = 2;
                    }
                    else
                    {
                        Customer c = CustomerManagement.GetByID(Convert.ToInt32(user.CustomerID));
                        var IsInternalComplianceApplicable = c.IComplianceApplicable;
                        if (IsInternalComplianceApplicable != -1)
                        {
                            checkInternalapplicable = Convert.ToInt32(IsInternalComplianceApplicable);
                        }
                        var IsTaskApplicable = c.TaskApplicable;
                        if (IsTaskApplicable != -1)
                        {
                            checkTaskapplicable = Convert.ToInt32(IsTaskApplicable);
                        }
                        var IsVerticlApplicable = c.VerticalApplicable;
                        if (IsVerticlApplicable != null)
                        {
                            if (IsVerticlApplicable != -1)
                            {
                                checkVerticalapplicable = Convert.ToInt32(IsVerticlApplicable);
                            }
                        }
                        if (c.IsPayment != null)
                        {
                            IsPaymentCustomer = Convert.ToBoolean(c.IsPayment);
                        }
                        var IsLabelApplicable = c.IsLabelApplicable;
                        if (IsLabelApplicable != -1)
                        {
                            checkLabelApplicable = Convert.ToInt32(IsLabelApplicable);
                        }
                        if (c.ComplianceProductType != null)
                        {
                            complianceProdType = Convert.ToInt32(c.ComplianceProductType);
                        }
                    }

                    User userToUpdate = new User();
                    userToUpdate.ID = user.ID;
                    userToUpdate.LastLoginTime = DateTime.Now;
                    userToUpdate.WrongAttempt = 0;
                    UserManagement.Update(userToUpdate);

                    if (role1.ToLower().Equals("performer"))
                    {
                        FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12}", user.ID, role, name, checkInternalapplicable, "C", user.CustomerID, checkTaskapplicable, string.Empty, string.Empty, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType), false);

                       // FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12}", user.ID, role, name, checkInternalapplicable, "C", user.CustomerID, checkTaskapplicable, userProfileID, authkey, IsVerticlApplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType), false);

                        TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);                        
                        Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);
                        if (type.ToLower().Equals("statutory"))
                        {
                            var query = (from row in entities.Lic_tbl_LicenseComplianceInstanceScheduleOnMapping
                                         where row.ComplianceScheduleOnID == SId
                                         select row).FirstOrDefault();
                            if (query != null)
                            {
                                Response.Redirect("~/Compliances/PerCompliancestatusTransaction.aspx?SOID=" + SId + "&CID=" + InId + "&status=&ISM=1", false);
                            }
                            else
                            {                                
                                Response.Redirect("~/controls/compliancestatusperformer.aspx?sId=" + SId + "&InId=" + InId + "&ISM=1", false);
                            }
                        }
                        else if (type.ToLower().Equals("taskstatutory"))
                        {
                            Response.Redirect("~/task/TaskStatusTransactionPerformer.aspx?TSOID=" + SId + "&TID=" + InId + "&ISM=1", false);
                        }
                        else if (type.ToLower().Equals("internal"))
                        {
                            var query = (from row in entities.Lic_tbl_LicenseInternalComplianceInstanceScheduleOnMapping
                                         where row.ComplianceScheduleOnID == SId
                                         select row).FirstOrDefault();
                            if (query != null)
                            {
                                Response.Redirect("~/Compliances/PerInternalCompliancestatusTransaction.aspx?SOID=" + SId + "&CID=" + InId + "&status=&ISM=1", false);
                            }
                            else
                            {
                                Response.Redirect("~/controls/InternalComplianceStatusperformer.aspx?sId=" + SId + "&InId=" + InId + "&ISM=1", false);
                            }
                        }
                        else if (type.ToLower().Equals("taskinternal"))
                        {
                            Response.Redirect("~/task/TaskStatusTransactionPerformer.aspx?TSOID=" + SId + "&TID=" + InId + "&ISM=1", false);
                        }
                    }
                    else if (role1.ToLower().Equals("reviewer"))
                    {
                        FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), Convert.ToBoolean(Session["RM"]));
                        FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12}", user.ID, role, name, checkInternalapplicable, "C", user.CustomerID, checkTaskapplicable, string.Empty, string.Empty, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType), false);

                        TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);                        
                        Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);
                        if (type.ToLower().Equals("statutory"))
                        {
                            var query = (from row in entities.Lic_tbl_LicenseComplianceInstanceScheduleOnMapping
                                         where row.ComplianceScheduleOnID == SId
                                         select row).FirstOrDefault();
                            if (query != null)
                            {
                                Response.Redirect("~/Compliances/RevComplianceStatusTransaction.aspx?SOID=" + SId + "&CID=" + InId + "&status=&ISM=1", false);
                            }
                            else
                            {
                                Response.Redirect("~/controls/compliancestatusreviewer.aspx?sId=" + SId + "&InId=" + InId + "&ISM=1", false);
                            }
                        }
                        else if (type.ToLower().Equals("taskstatutory"))
                        {
                            Response.Redirect("~/task/TaskStatusTransactionReviewer.aspx?TSOID=" + SId + "&TID=" + InId + "&ISM=1", false);
                        }
                        else if (type.ToLower().Equals("internal"))
                        {
                            var query = (from row in entities.Lic_tbl_LicenseInternalComplianceInstanceScheduleOnMapping
                                         where row.ComplianceScheduleOnID == SId
                                         select row).FirstOrDefault();
                            if (query != null)
                            {
                                Response.Redirect("~/Compliances/RevInternalComplianceStatusTransaction.aspx?SOID=" + SId + "&CID=" + InId + "&status=&ISM=1", false);
                            }
                            else
                            {
                                Response.Redirect("~/controls/InternalComplianceStatusReviewer.aspx?sId=" + SId + "&InId=" + InId + "&ISM=1", false);
                            }
                        }
                        else if (type.ToLower().Equals("taskinternal"))
                        {
                            Response.Redirect("~/task/TaskStatusTransactionReviewer.aspx?TSOID=" + SId + "&TID=" + InId + "&ISM=1", false);
                        }
                    }

                    DateTime LastPasswordChangedDate = Convert.ToDateTime(UserManagement.GetByID(Convert.ToInt32(user.ID)).ChangPasswordDate);
                    DateTime currentDate = DateTime.Now;
                    LastPasswordChangedDate = LastPasswordChangedDate != null ? LastPasswordChangedDate : DateTime.Now;
                    int noDays = Convert.ToInt32(ConfigurationManager.AppSettings["ChangedPasswordDays"]);
                    int dateDifference = Convert.ToInt32((currentDate - LastPasswordChangedDate).TotalDays);
                    if (dateDifference == noDays || dateDifference > noDays)
                    {
                        Session["ChangePassword"] = true;
                        Response.Redirect("~/Account/ChangePassword.aspx", false);
                    }
                    else
                    {
                        Session["ChangePassword"] = false;
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
            }
        }

        protected void lnkResendOTP_Click(object sender, EventArgs e)
        {
            try
            {
                User user = new User();
                string OTPValue = string.Empty;
                string ipaddress = string.Empty;
                long Contact;
                try
                {
                    ipaddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                    if (ipaddress == "" || ipaddress == null)
                        ipaddress = Request.ServerVariables["REMOTE_ADDR"];
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
                if (!string.IsNullOrEmpty(Convert.ToString(Session["userID"])))
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(Session["ResendOTP"])))
                    {
                        user = UserManagement.GetByID(Convert.ToInt32(Session["userID"].ToString()));

                        OTPValue = Convert.ToString(Session["ResendOTP"]);
                        if (ConfigurationManager.AppSettings["SkipOTPQA"].ToString() != "1")
                        {
                            VerifyOTP OTPData = new VerifyOTP()
                            {
                                UserId = Convert.ToInt32(user.ID),
                                EmailId = user.Email,
                                OTP = Convert.ToInt32(OTPValue),
                                CreatedOn = DateTime.Now,
                                IsVerified = false,
                                IPAddress = ipaddress
                            };
                            bool OTPresult = long.TryParse(user.ContactNumber, out Contact);
                            if (OTPresult)
                            {
                                OTPData.MobileNo = Contact;
                            }
                            else
                            {
                                OTPData.MobileNo = 0;
                            }
                            
                                VerifyOTPManagement.Create(OTPData); // Insert Data in OTP Table.
                                WrongOTPCount = 0;
                            //Send Email on User Mail Id.
                            string SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"].ToString();
                            if (user.CustomerID != 5)
                            {
                                EmailManager.SendMail(SenderEmailAddress, new List<String>(new String[] { user.Email }), null, null, "OTP Verification", "Your One Time Password for Avantis login is:" + Convert.ToString(OTPValue) + "<br>" + "<br>" + "Thank you," + "<br>" + "Team Teamlease");
                                //SendGridEmailManager.SendGridMail(ConfigurationManager.AppSettings["SenderEmailAddress"], ConfigurationManager.AppSettings["ReplyEmailAddressName"], new List<String>(new String[] { user.Email }), null, null, "OTP Verification", "Your One Time Password for Teamlease Regtech login is:" + Convert.ToString(OTPValue) + "<br>" + "<br>" + "Thank you," + "<br>" + "Team Teamlease Regtech");
                            }
                            //Send SMS on User Mobile No.
                            if (OTPresult && user.RoleID != 12 && user.ContactNumber.Length == 10 && (user.ContactNumber.StartsWith("9") || user.ContactNumber.StartsWith("8") || user.ContactNumber.StartsWith("7") || user.ContactNumber.StartsWith("6")))
                            {
                                //SendSms.sendsmsto(user.ContactNumber, "Your One Time Password for Avantis login is: " + OTPValue + ".  " + "Thank you, " + "Avantis Team.", "1207161856353769674");

                                var data = VerifyOTPManagement.GetSMSConfiguration("Avacom");
                                if (data != null)
                                {
                                    SendSms.sendsmsto(user.ContactNumber, "Your One Time Password for Avantis login is: " + Convert.ToString(OTPValue) + ".  " + "Thank you, " + "Avantis Team.", data.TEMPLATEID_DLT_TE_ID, data.authkey, data.Header_sender, data.route);
                                }
                                else
                                {
                                    SendSms.sendsmsto(user.ContactNumber, "Your One Time Password for Avantis login is: " + Convert.ToString(OTPValue) + ".  " + "Thank you, " + "Avantis Team.", "1207161856353769674");
                                }
                            }
                            CheckResendOTPCount();
                            txtOTP.Text = string.Empty;
                            DisplayOTPMessage();
                            cvLogin.ErrorMessage = "OTP Sent Successfully. You are left with " + ResendOTPAttemp + " more attempts.";
                            cvLogin.IsValid = false;

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void CheckResendOTPCount()
        {
            try
            {
                int OTPValue = 0;
                int UserID = 0;
                if (!string.IsNullOrEmpty(Convert.ToString(Session["userID"])))
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(Session["ResendOTP"])))
                    {
                        UserID = int.Parse(Session["userID"].ToString());
                        OTPValue = int.Parse(Session["ResendOTP"].ToString());
                        var CheckOTPCount = VerifyOTPManagement.GetResendOTPCount(UserID, OTPValue);
                        if (CheckOTPCount.Count() > 3)
                        {
                            lnkResendOTP.Visible = false;
                        }
                        ResendOTPAttemp = 4 - CheckOTPCount.Count();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}