﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;

namespace com.VirtuosoITech.ComplianceManagement.Portal
{
    public partial class maildetails : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["id"]))
                    {
                        Business.Data.emailview email = new Business.Data.emailview();
                        email = com.VirtuosoITech.ComplianceManagement.Business.ComplianceManagement.GetEmailById(Convert.ToInt16(Request.QueryString["id"]));
                        lblsender.Text = email.sendemail;
                        lblsubject.Text = email.Subjecttext;
                        lblmessage.Text = email.MessageBody;
                        lbldate.Text = Convert.ToString(email.CreatedDate);
                        lblattachment.Text = email.Attachment;
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }                               
            }        
        }
    }
}