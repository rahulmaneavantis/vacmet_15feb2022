﻿<%@ Page Title="Dashboard" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true"
    CodeBehind="UserSummary.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Users.UserSummary" %>

<%@ Register Src="~/Controls/CustomerDetailsControl.ascx" TagName="CustomerDetailsControl"
    TagPrefix="vit" %>
<%@ Register Src="~/Controls/UserDetailsControl.ascx" TagName="UserDetailsControl"
    TagPrefix="vit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdatePanel runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div style="float: left; clear: both; font-family: Tahoma; font-size: 13px; background-color: #418CF1;
                height: 35px; width: 100%">
                <div style="float: left; clear: both; margin-top: 5px; margin-left: 10px; font-family: Tahoma;
                    font-size: 13px;">
                    <asp:HyperLink CssClass="sublink" ID="hpAssignLocation" runat="server" NavigateUrl="UserList.aspx">View Users</asp:HyperLink>
                    <asp:HyperLink CssClass="sublink" ID="hpAssignUser" runat="server" NavigateUrl="~/Customers/CustomerList.aspx">View Customers</asp:HyperLink>
                    <asp:LinkButton ID="lbNewUser" CssClass="sublink" Text="Add New User" runat="server"
                        OnClick="lbNewUser_Click" />
                    <asp:LinkButton ID="lbNewCustomer" CssClass="sublink" Text="Add New Customer" runat="server"
                        OnClick="lbNewCustomer_Click" />
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <%--<vit:UserSummarySuperAdmin runat="server" />--%>
    <asp:PlaceHolder runat="server" ID="phSummaryControl" />
    <vit:CustomerDetailsControl runat="server" ID="udcCustomerInputForm" />
    <vit:UserDetailsControl runat="server" ID="udcUserInputForm" />
</asp:Content>
