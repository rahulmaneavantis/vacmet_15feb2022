﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NewCompliance.Master" AutoEventWireup="true" CodeBehind="EscalationRemindersKendo.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Users.EscalationRemindersKendo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />
    <script type="text/javascript" src="../../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jszip.min.js"></script>
    <style type="text/css">
      
        .k-active-filter, .k-state-active, .k-state-active:hover {
            background-color: #E9EAEA;
            border-color: #a6a6ad;
            color: #535b6a;
        }

        .k-list > .k-state-focused.k-state-selected, .k-listview > .k-state-focused.k-state-selected, .k-state-focused.k-state-selected, td.k-state-focused.k-state-selected {
            -webkit-box-shadow: inset 0 0 3px 1px rgba(0, 0, 0, 0.5);
            box-shadow: inset 0 0 3px 1px rgba(0, 0, 0, 0.5);
        }

        input[type=checkbox], input[type=radio] {
            margin: 4px 6px 0;
            margin-top: 1px\9;
            line-height: normal;
        }

        .div.k-grid-footer, div.k-grid-header {
            border-top-width: 1px;
            margin-right: -2px;
        }

        .k-grid-footer-wrap, .k-grid-header-wrap {
            position: relative;
            width: 100%;
            overflow: hidden;
            border-style: solid;
            border-width: 0 1px 0 0;
            zoom: 1;
        }

        .k-grid-content {
            min-height: 390px !important;
        }

        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .k-checkbox-wrapper {
            display: inline-block;
            vertical-align: middle;
            margin-left: -13px;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 2.0em;
            border-bottom-width: 1px;
            background-color: white;
            border-width: 0 1px 1px 0px;
            font-size:12px;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
            background-color: white;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: white;
        }

        #grid .k-grid-toolbar {
            background: white;
        }

        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 6px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: white;
        }

        html .k-grid tr.k-alt:hover {
            background: white;
        }

        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            background: white;
            border: none;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
            margin-right: 2px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: white;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 0px 1px 0px;
            text-align: center;
        }

        .k-grid-pager {
            margin-top: -1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-i-arrow-60-down, .k-i-arrow-60-left, .k-i-arrow-60-right, .k-i-arrow-60-up {
            cursor: pointer;
            margin-top: 6px;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            border-width: 0px 0px 1px 0px;
            background: #E9EAEA;
            font-weight: bold;
            margin-right: 18px;
            font-family: 'Roboto',sans-serif;
            font-size: 15px;
            color: rgba(0, 0, 0, 0.5);
            height: 20px;
            vertical-align: middle;
        }

        .k-dropdown-wrap.k-state-default {
            background-color: white;
        }

        .k-popup.k-calendar-container, .k-popup.k-list-container {
            background-color: white;
        }

        .k-grid-header th.k-state-focused, .k-list > .k-state-focused, .k-listview > .k-state-focused, .k-state-focused, td.k-state-focused {
            -webkit-box-shadow: inset 0 0 3px 1px white;
            box-shadow: inset 0 0 3px 1px white;
        }

        label.k-label {
            font-family: roboto,sans-serif !important;
            color: #515967;
            /* font-stretch: 100%; */
            font-style: normal;
            font-weight: 400;
            min-width: 362px;
            white-space: pre-wrap;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: flex;
            margin-bottom: 0px;
        }

        .k-state-default > .k-select {
            border-color: #ceced2;
            margin-top: 0px;
        }

        .k-grid-norecords {
            width: 100%;
            height: 100%;
            text-align: center;
            margin-top: 2%;
        }

        .k-grouping-header {
            font-style: italic;
            background-color: white;
        }

        .k-grid-toolbar {
            background: white;
            border: none;
        }

        .k-grid table {
            width: 100.5%;
        }

        input[type=checkbox], input[type=radio] {
            margin: 4px 4px 0;
            margin-top: 1px\9;
            line-height: normal;
        }

        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            border-radius: 35px;
            background-color: white;
            border-color: white;
            color: black;
        }

        .k-treeview .k-i-collapse, .k-treeview .k-i-expand, .k-treeview .k-i-minus, .k-treeview .k-i-plus {
            margin-left: -16px;
            cursor: pointer;
            margin-right: 10px;
        }

         .k-multiselect-wrap .k-input {
               /*padding-top:6px;*/
               display: inherit !important;
           }
    </style>
    <script id="StatusTemplate" type="text/x-kendo-template">                 
            <span class='k-progress'></span>
            <div class='file-wrapper'> 
            #=GetStatusType(IntreimDays)# 
    </script>
    <script type="text/javascript">
        function GetStatusType(value) {
            if (value == null) {
                return '';
            }
            else {
                return value;
            }
        }
        $(document).ready(function () {
            setactivemenu('leftremindersmenu');
            $("#txtSearchComplianceID").on('input', function (e) {
                FilterAll();
            });
            fhead('Reviewer');
            Bindgrid();
            $("#txtSearchfilter").on('input', function (e) {
                var grid = $('#grid').data('kendoGrid');
                var columns = grid.columns;
                var filter = { logic: 'or', filters: [] };
                columns.forEach(function (x) {
                    if (x.field == "ActName" || x.field == "ShortDescription" || x.field == "Branch" || x.field == "Frequency" || x.field == "User" || x.field == "DueDate" || x.field == "IntreimDays" || x.field == "EscDays") {
                        filter.filters.push({
                            field: x.field,
                            operator: 'contains',
                            value: e.target.value
                        });
                    }
                });
                grid.dataSource.filter(filter);
            });
            $("#dropdownlistUserRole").kendoDropDownList({
                placeholder: "Period",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    DataBindDaynamicKendoGriddMain();
                },
                //index: 1,
                dataSource: [
                    <%--<%if (PerformerFlagID == 1)%>
                    <%{%>
                        { text: "Performer", value: "3" },
                     <%}%>--%>
                    <%if (ReviewerFlagID == 1)%>
                    <%{%>
                        { text: "Reviewer", value: "4" }
                    <%}%>
                ]
             });

            $("#dropdownlistComplianceType1").kendoDropDownList({
                //placeholder: "Type",
                dataTextField: "text",
                dataValueField: "value",
                //optionLabel: "Select",
                autoClose: true,
                dataSource: [
		        { text: "All", value: "0" },
                    { text: "Statutory", value: "-1" },
                    { text: "Event Based", value: "1" } 		   
                ],
                index: 0,
                change: function (e) {
                 
                    FilterAll();

                }
            });

            $("#dropdownlistRisk").kendoDropDownTree({
                placeholder: "Risk",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",

                change: function () {
                    FilterAll();
                    fCreateStoryBoard('dropdownlistRisk', 'filterrisk', 'risk');

                },
                dataSource: [
                    { text: "Critical", value: "3" },
                    { text: "High", value: "0" },
                    { text: "Medium", value: "1" },
                    { text: "Low", value: "2" }
                ]
            });


            $("#dropdowntree").kendoDropDownTree({
                placeholder: "Entity/Sub-Entity/Location",
                checkboxes: {
                    checkChildren: true
                },
              //  filter: "contains",
                checkAll: true,
                autoWidth: true,
                checkAllTemplate: "Select All",
                dataTextField: "Name",
                dataValueField: "ID",
                change: function (e) {
                    FilterAll();
                    fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc')
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =RoleFlag%>&IsStatutoryInternal=S',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                    },
                    schema: {
                        data: function (response) {
                            return response[0].locationList;
                        },
                        model: {
                            children: "Children"
                        }
                    }
                }
            });

        });
            var dataSource = new kendo.data.DataSource({
                transport: {
                    read: {
                        url: '<% =Path%>data/GetEscalationReminder?UserId=<% =UId%>&CustomerID=<% =CustId%>',
                    dataType: "json",
                    beforeSend: function (request) {
                        request.setRequestHeader('Authorization', '<% =Authorization%>');
                    },
                },
                update: {
                    url: "<% =Path%>Data/EditEscalationReminder" + '?UserID=<%=UId%>',
                    dataType: "json",
                    beforeSend: function (request) {
                        request.setRequestHeader('Authorization', '<% =Authorization%>');
                    },
                },
                create: {
                },
                destroy: {
                },
            },
            pageSize: 10,
            schema: {
                model: {
                    id: "complianceID",
                    fields: {
                        complianceID: { type: "string", },
                        ActName: { editable: false, validation: { required: true } },
                        ShortDescription: { editable: false, validation: { required: true } },
                        Branch: { editable: false, validation: { required: true } },
                        Frequency: { editable: false, validation: { required: true } },
                        User: { editable: false, validation: { required: true } },
                        DueDate: { editable: false, validation: { required: true } },
                        IntreimDays: { editable: true, validation: { required: true } },
                        EscDays: { editable: true, validation: { required: true } },
                    }
                }
            },
            requestEnd: onRequestEnd
        });
            function onRequestEnd(e) {
                if (e.type == "create") {
                    e.sender.read();
                }
                else if (e.type == "update") {
                    e.sender.read();
                }
            }
            function onEdit(e) {
                e.container.find(".k-grid-cancel").bind("click", function () {
                    $('#grid').data("kendoGrid").cancelChanges();
                })
            }
            var total = 0;
            var record = 0;
            function Bindgrid() {
                var grid = $('#grid').data("kendoGrid");
                if (grid != undefined || grid != null)
                    $('#grid').empty();
                var grid = $("#grid").kendoGrid({
                    dataSource: dataSource,
                    excel: {
                        allPages: true,
                    },
                    noRecords: true,
                    messages: {
                        noRecords: "No records found"
                    },
                    sortable: true,
                    filterable: true,
                    columnMenu: true,
                    groupable: true,
                    pageable: {
                        numeric: true,
                        pageSizes: ['All', 5, 10, 20],
                        pageSize: 10,
                        buttonCount: 3,
                    },
                    reorderable: true,
                    resizable: true,
                    persistSelection: true,
                    multi: true,
                    dataBound: function () {
                        var theGrid = $("#grid").data("kendoGrid");
                        $("#grid tbody").find('tr').each(function () {
                            var model = theGrid.dataItem(this);
                            kendo.bind(this, model);
                        });
                        $("#grid").focus();
                    },
                    dataBinding: function () {
                        record = 0;
                        total = this.dataSource._total;

                        if (this.dataSource.pageSize() == undefined || this.dataSource.pageSize() == null) {
                            this.dataSource.pageSize(this.dataSource._total);
                        }
                        if (this.dataSource.pageSize() > 10 && this.dataSource.pageSize() > 20 && this.dataSource.pageSize() != this.dataSource._total) {
                            if (this.dataSource._total <= 10) {
                                this.dataSource.pageSize(10)

                            }
                            else if (this.dataSource._total >= 10 && this.dataSource._total <= 20) {
                                this.dataSource.pageSize(20)
                            }
                            else {
                                this.dataSource.pageSize(total);
                            }
                        }
                        //if (this.dataSource.pageSize() < 10 && this.dataSource.pageSize() != total) {
                        //    this.dataSource.pageSize(10)
                        //}
                        record = (this.dataSource.page() - 1) * this.dataSource.pageSize();

                    },

                    columns: [
                        {
                            selectable: true,
                            width: "50px",
                        },
                          {
                              field: "Branch", title: 'Location',
                              attributes: {
                                  style: 'white-space: nowrap;'

                              }, filterable: {
                                  multi: true,
                                  extra: false,
                                  search: true,
                                  operators: {
                                      string: {
                                          eq: "Is equal to",
                                          neq: "Is not equal to",
                                          contains: "Contains"
                                      }
                                  }
                              }
                          },

                     
                       { hidden: true, field: "complianceID", title: "Compliance ID", filterable: { multi: true, search: true }, width: "10%;", },

                        {
                            field: "ShortDescription", title: 'Description',
                            attributes: {
                                style: 'white-space: nowrap;'

                            }, filterable: {
                                multi: true,
                                extra: false,
                                search: true,
                                operators: {
                                    string: {
                                        eq: "Is equal to",
                                        neq: "Is not equal to",
                                        contains: "Contains"
                                    }
                                }
                            }
                        },
                           {
                               field: "ActName", title: 'Act',
                               editable: false,
                               attributes: {
                                   style: 'white-space: nowrap;'

                               }, filterable: {
                                   multi: true,
                                   extra: false,
                                   search: true,
                                   operators: {
                                       string: {
                                           eq: "Is equal to",
                                           neq: "Is not equal to",
                                           contains: "Contains"
                                       }
                                   }
                               }
                           },
				{
                              field: "ReportType", title: 'Report Type',
                              editable: false,
                              hidden: true,
                              attributes: {
                                  style: 'white-space: nowrap;'

                              }, filterable: {
                                  multi: true,
                                  extra: false,
                                  search: true,
                                  operators: {
                                      string: {
                                          eq: "Is equal to",
                                          neq: "Is not equal to",
                                          contains: "Contains"
                                      }
                                  }
                              }
                          },
                      
                        {
                            field: "Frequency", title: 'Frequency',
                            attributes: {
                                style: 'white-space: nowrap;'

                            }, filterable: {
                                multi: true,
                                extra: false,
                                search: true,
                                operators: {
                                    string: {
                                        eq: "Is equal to",
                                        neq: "Is not equal to",
                                        contains: "Contains"
                                    }
                                }
                            }
                        },
                        {
                            field: "PerformerName", title: 'Performer',
                            width: "10%",
                            attributes: {
                                style: 'white-space: nowrap;'

                            }, filterable: {
                                multi: true,
                                extra: false,
                                search: true, 
                                operators: {
                                    string: {
                                        eq: "Is equal to",
                                        neq: "Is not equal to",
                                        contains: "Contains"
                                    }
                                }
                            }
                        },
                        {
                            field: 'ReviewerName', title: 'Reviewer',
                            hidden: true,
                            width: "10%",
                            attributes: {
                                style: 'white-space: nowrap;'

                            }, filterable: {
                                multi: true,
                                extra: false,
                                search: true,
                                operators: {
                                    string: {
                                        eq: "Is equal to",
                                        neq: "Is not equal to",
                                        contains: "Contains"
                                    }
                                }
                            }
                        },
                        {
                            field: "DueDate", title: 'Due Date of Month',
                            attributes: {
                                style: 'white-space: nowrap;'

                            }, filterable: {
                                multi: true,
                                extra: false,
                                search: true,
                                operators: {
                                    string: {
                                        eq: "Is equal to",
                                        neq: "Is not equal to",
                                        contains: "Contains"
                                    }
                                }
                            }
                        },
                        {
                            title: "Work file timeline",
                            //type: "number",
                            template: "<input onkeypress='return /[0-9]/i.test(event.key)' data-bind='value: IntreimDays' class='k-textbox' style='width: 70px;' value=#=(IntreimDays == null ? '': IntreimDays)#>",
                        },
                        {
                            title: "Escalation",
                            //type: "number",
                            template: "<input onkeypress='return /[0-9]/i.test(event.key)' data-bind='value: EscDays' class='k-textbox' data-type='int' style='width: 70px;' value=#=(EscDays == null ? '': EscDays)#>",
                            //template: kendo.template($('#StatusTemplate').html()),
                        },
                        {
                            command: [
                                { name: "edit", text: "", iconClass: "k-icon k-i-edit", className: "ob-edit" }], title: "Action", lock: true
                        }
                    ], editable: "inline"

                });
                $("#grid").kendoTooltip({
                    filter: ("th:nth-child(n+2)"),
                    content: function (e) {
                        var target = e.target; // element for which the tooltip is shown 
                        return $(target).text();
                    }
                });
                $("#grid").kendoTooltip({
                    filter: ("td:nth-child(n+2)"),
                    position: "down",
                    content: function (e) {
                        var content = e.target.context.textContent;
                        if (content != "") {
                            return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                        }
                        else
                            e.preventDefault();
                    }
                }).data("kendoTooltip");

                $("#grid").kendoTooltip({
                    filter: ".k-grid-edit",
                    content: function (e) {
                        return "Edit";
                    }
                });

            }

            function FilterAll() {
                var Riskdetails = [];
                if ($("#dropdownlistRisk").data("kendoDropDownTree") != undefined) {
                    Riskdetails = $("#dropdownlistRisk").data("kendoDropDownTree")._values;
                }
                var locationlist = [];
                if ($("#dropdowntree").data("kendoDropDownTree") != undefined) {
                    locationlist = $("#dropdowntree").data("kendoDropDownTree")._values;
                }
             

                if (locationlist.length > 0 || Riskdetails.length > 0 || $("#txtSearchComplianceID").val() != ""
                    || ($("#dropdownlistComplianceType1").val() != 0 && $("#dropdownlistComplianceType1").val() != "")) {
                    var finalSelectedfilter = { logic: "and", filters: [] };

                    if ($("#dropdownlistComplianceType1").val() != 0 && $("#dropdownlistComplianceType1").val() != "")
                    {
                        var FYFilter = { logic: "or", filters: [] };
                        if ($("#dropdownlistComplianceType1").val() == -1)
                        {
                            FYFilter.filters.push({
                                field: "ReportType", operator: "eq", value: "Statutory"
                            });
                        }
                        else if ($("#dropdownlistComplianceType1").val() == 2) {
                            FYFilter.filters.push({
                                field: "ReportType", operator: "eq", value: "StatutoryChecklist"
                            });
                        }
                        else {
                            FYFilter.filters.push({
                                field: "ReportType", operator: "eq", value: "EventBased"
                            });
                        }
                        finalSelectedfilter.filters.push(FYFilter);
                        //if ($("#dropdownlistComplianceType1").val() == -1) {
                        //    FYFilter.filters.push({
                        //        field: "EventFlag", operator: "eq", value: null
                        //    });
                        //}
                        //else {
                        //    FYFilter.filters.push({
                        //        field: "EventFlag", operator: "eq", value: parseInt(1)
                        //    });
                        //}
                        //finalSelectedfilter.filters.push(FYFilter);
                    }
                  
                    if (locationlist.length > 0) {
                        var locFilter = { logic: "or", filters: [] };

                        $.each(locationlist, function (i, v) {
                            locFilter.filters.push({
                                field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                            });
                        });

                        finalSelectedfilter.filters.push(locFilter);
                    }
                    if ($("#txtSearchComplianceID").val() != "") {
                        var RiskFilter = { logic: "or", filters: [] };
                        RiskFilter.filters.push({
                            field: "complianceID", operator: "contains", value: $("#txtSearchComplianceID").val()
                        });
                        finalSelectedfilter.filters.push(RiskFilter);
                    }
                    if (Riskdetails.length > 0) {
                        var CategoryFilter = { logic: "or", filters: [] };

                        $.each(Riskdetails, function (i, v) {

                            CategoryFilter.filters.push({
                                field: "RiskType", operator: "eq", value: parseInt(v)
                            });
                        });
                        finalSelectedfilter.filters.push(CategoryFilter);
                    }
                    if (finalSelectedfilter.filters.length > 0) {
                        var dataSource = $("#grid").data("kendoGrid").dataSource;
                        dataSource.filter(finalSelectedfilter);
                    }
                    else {
                        $("#grid").data("kendoGrid").dataSource.filter({});
                    }
                }
                else {
                    $("#grid").data("kendoGrid").dataSource.filter({});
                }
                var dataSource = $("#grid").data("kendoGrid").dataSource;
                if (dataSource._total > 20 && dataSource.pageSize == undefined) {
                    dataSource.pageSize(total);
                }
            }
            function btn_SaveAll(e) {
                debugger;
                e.preventDefault();
               
                var grid = $('#grid').data("kendoGrid");
                var rows = $('#grid :checkbox:checked');

                if (rows.length == 0) {
                    alert('Please select compliance(s) checkbox to Escalate.');
                    return;
                }
                if (rows.length > 0) {
                    var validatopnFlag = 0;
                    var EscalationData = [];
                    $.each(rows, function () {
                        debugger
                        // var item = grid.dataItem($(this).closest("tr"));
                        var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
                        if (item.IntreimDays == undefined || item.IntreimDays == null &&
                            item.EscDays == undefined || item.EscDays == null) {
                            validatopnFlag = 1;
                        }
                        item.userID =<% =UId%>
                        EscalationData.push(item);
                    });
                    if (validatopnFlag == 0) {
                        if (EscalationData.length > 0) {
                            $.ajax({
                                contentType: 'application/json; charset=utf-8',
                                dataType: 'json',
                                type: 'POST',
                                url: '/Escalation/SaveAllEscalationData',
                                data: JSON.stringify(EscalationData),
                                beforeSend: function (request) {
                                    request.setRequestHeader('Authorization', '<% =Authorization%>');
                                },
                                create: {
                                },
                                destroy: {
                                },
                                success: function (json) {
                                    if (json.message == "Saved Successfully") {
                                        alert('Selected Compliance(s) Escalated Successfully.');
                                        $("#grid").data("kendoGrid").dataSource.read();
                                        $("#grid").data("kendoGrid").refresh();
                                        $('#grid').data('kendoGrid')._selectedIds = {};
                                        $('#grid').data('kendoGrid').clearSelection();
                                    }
                                },
                                failure: function (response) {
                                    $('#result').html(response);
                                    $('#grid').data('kendoGrid').clearSelection();
                                }
                            });
                        }
                        else
                        {
                            alert('Please enter Work File Timeline and/or Escalation Days values.');
                            return;
                        }
                    }
                    else {
                        alert('Please enter Work File Timeline and/or Escalation Days values.');
                        return;
                    }
                    }
                }
                function AddnewTest(e) {
                    $('#divShowReminderDialog').modal('show');
                    $('#ContentPlaceHolder1_showReminderDetail').attr('src', "../Litigation/aspxPages/AddEditReminder.aspx?AccessID=0")
                    e.preventDefault();
                }
                function CloseMyReminderPopup() {
                    $('#divShowReminderDialog').modal('hide');
                    Bindgrid();
                }
                function CloseClearPopup() {
                    $('#APIOverView').attr('src', "../Common/blank.html");
                }
                function ClearAllFilterMain(e) {
                    $("#txtSearchComplianceID").val('');
                    $('#dvdropdownEventNature').css('display', 'none');
                    $('#dvdropdownEventName').css('display', 'none');
                    $("#dropdowntree").data("kendoDropDownTree").value([]);
                    $("#dropdownlistRisk").data("kendoDropDownTree").value([]);
                   // $("#dropdownlistComplianceType1").data("kendoDropDownList").select(0);
                    //$('#txtSearchfilter').val('');
                    $('#ClearfilterMain').css('display', 'none');
                    $('#grid').data('kendoGrid').clearSelection();
                    $("#grid").data("kendoGrid").dataSource.filter({});
                    var dataSource = $("#grid").data("kendoGrid").dataSource;
                    dataSource.pageSize(10);
                    e.preventDefault();
        }
        function fcloseStory(obj) {
            var DataId = $(obj).attr('data-Id');
            var dataKId = $(obj).attr('data-K-Id');
            var seq = $(obj).attr('data-seq');
            var deepspan = $('#' + DataId + ' > li > span > span.k-i-close')[seq];
            $(deepspan).trigger('click');
            var upperli = $('#' + dataKId);
            $(upperli).remove();

            fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc');
            fCreateStoryBoard('dropdownlistRisk', 'filterrisk', 'risk');
            CheckFilterClearorNotMain();
        };
        function CheckFilterClearorNotMain() {
            if (($($($('#dropdowntree').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
                ($($($('#dropdownlistRisk').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0)) {
                $('#ClearfilterMain').css('display', 'none');
            }
        }
        function fCreateStoryBoard(Id, div, filtername) {
            var IdKReset = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('id');
            $('#' + div).html('');
            $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');
            $('#' + div).css('display', 'block');
            if (div == 'filterrisk') {
                $('#' + div).append('Risk&nbsp;&nbsp;:');//Dashboard
                $('#ClearfilterMain').css('display', 'block');
            }
            else if (div == 'filtersstoryboard') {
                $('#' + div).append('Location&nbsp;&nbsp;:');
                $('#ClearfilterMain').css('display', 'block');
            }
            for (var i = 0; i < $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length; i++) {
                var button = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button')[i];
                $(button).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('placeholder', 'Shree');
                var buttontest = $($(button).find('span')[0]).text();
                if (buttontest.length > 10) {
                    buttontest = buttontest.substring(0, 10).concat("...");
                }
                $('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="background-color:#EBEBEB; height: 20px;Color:Gray;border-radius:10px;margin-left:4px;margin-top:-1px;"><span unselectable="on" title="' + $($(button).find('span')[0]).text() + '">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory(this)" title="clear" aria-label="clear" class="k-select" style="padding-left: 6px;"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory(this)"  class="k-icon k-i-close" style="font-size: 12px;"></span></span></li>');
                //$('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="background-color:#1fd9e1; height: 20px;Color:white;"><span unselectable="on">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory(this)" title="delete" aria-label="delete" class="k-select"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory(this)"  class="k-icon k-i-close"></span></span></li>');
            }
            if ($($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) {
                $('#' + div).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');
            }
            CheckFilterClearorNotMain();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="example">
        <div class="row">
            <div class="row" style="padding-bottom: 4px; padding-right: 138px;">
                <div class="toolbar">
                    <input id="dropdownlistUserRole" data-placeholder="Role" style="width: 290px;" />
                </div>
            </div>
            <div class="row" style="margin-top: 5px;">
                <input id="dropdowntree" data-placeholder="Entity/Sub-Entity/Location" style="width: 290px; margin-right: 15px;" />
                <input id="dropdownlistComplianceType1" style="width: 255px; margin-right: 15px;" />
                  <input id="dropdownlistRisk" data-placeholder="Risk" style="width: 208px;margin-right: 15px;" />
                <input id='txtSearchComplianceID' onkeydown="return(event.keyCode!=13);" class='k-textbox' placeholder="Compliance ID" style="width: 175px; margin-right: 15px;"/>

               <button id="btnsave" style="float: right; height: 29px; width: 7%; margin-top: 0px;margin-right:3px;" onclick="btn_SaveAll(event)"><span onclick="javascript:return false;"></span>Save</button>
                <button id="ClearfilterMain" style="float: right; margin-left: 9px;height: 29px;margin-right: 4px;margin-top:10px; display: none;" onclick="ClearAllFilterMain(event)"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear Filter</button>
            </div>
        </div>

        <div class="clearfix" style="height: 10px;"></div>
        <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a; font-weight: bold;" id="filterrisk">&nbsp;</div>
        <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a; font-weight: bold; margin-top: 4px;" id="filtersstoryboard">&nbsp;</div>
        <div id="grid"></div>
    </div>

    <div style="float: left; width: 100%">
        <div style="float: left; color: #666666;">
            <b>Work File Timeline</b> - This is the day/date on which performer should start sharing/submitting the compliance working file to reviewer.
        </div>
        <div style="float: left; color: #666666;">
            <b>Escalation</b> - From this day/date the reviewer will start receiving the mail alerts for compliance.
        </div>
    </div>
    <div class="modal fade" id="divShowReminderDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="height: 30px;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="CloseMyReminderPopup();">&times;</button>
                </div>

                <div class="modal-body">
                    <iframe src="about:blank" id="showReminderDetail" frameborder="0" runat="server" width="100%" height="300px"></iframe>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            fhead('My Escalation');
        });
    </script>
</asp:Content>
