﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Configuration;
using System.Threading;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using System.Data;
using System.ComponentModel;
using System.Collections;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using System.IO;
using System.Web;
using OfficeOpenXml.Style;
using OfficeOpenXml;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Users
{
    public partial class AuditUsers_List : System.Web.UI.Page
    {
        DataSet ds = new DataSet();
        protected string IsAuditManager = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            IsAuditManager = UserManagementRisk.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).IsAuditHeadOrMgr;
            if (!IsPostBack)
            {               
                BindCustomers();               
                BindUsers();
                Session["CurrentRole"] = AuthenticationHelper.Role;
                Session["CurrentUserId"] = AuthenticationHelper.UserID;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    divCustomerfilter.Visible = false;
                }
                else
                {
                    divCustomerfilter.Visible = true;
                }
                if (AuthenticationHelper.Role == "SADMN" || AuthenticationHelper.Role == "IMPT")
                {
                    btnAddUser.Visible = false;
                }
                ViewState["AssignedCompliancesID"] = null;                
                bindPageNumber();
            }
            udcInputForm.OnSaved += (inputForm, args) => { BindUsers(); };
        }

        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }
                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";
                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count==0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();                    
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {         
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdUser.PageIndex = chkSelectedPage - 1;            
            grdUser.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);            
            BindUsers();            
        }

        #region user Detail

        public void BindUsers()
        {
            try
            {
                int customerID = -1;                
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                if (ddlCustomerList.SelectedValue != "-1")
                {
                    customerID = Convert.ToInt32(ddlCustomerList.SelectedValue);
                }
                var uselist = UserManagementRisk.GetAllUser(customerID, tbxFilter.Text);                               
                grdUser.DataSource = uselist;
                Session["TotalRows"] = uselist.Count;
                grdUser.DataBind();
                upUserList.Update();                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
       
      
        protected void grdUser_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                string[] arg = e.CommandArgument.ToString().Split(',');
                int userID = Convert.ToInt32(arg[0]);
                string UserType = Convert.ToString(arg[0]);
                if (e.CommandName.Equals("EDIT_USER"))
                {
                    udcInputForm.EditUserInformation(userID);
                }
                else if (e.CommandName.Equals("DELETE_USER"))
                {
                    if (UserManagement.HasCompliancesAssigned(userID))
                    {
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "InformUser", "alert('Account can not be deleted. One or more Compliances are assigned to user, please re-assign to other user.');", true);
                    }
                    else if (EventManagement.GetAllAssignedInstancesByUser(userID).Count > 0)
                    {
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "EventInform", "alert('Account can not be deleted. One or more Event are assigned to user, please re-assign to other user.');", true);
                    }
                    else
                    {
                        UserManagement.Delete(userID);
                        UserManagementRisk.Delete(userID);
                    }
                    BindUsers();
                }
                else if (e.CommandName.Equals("CHANGE_STATUS"))
                {
                    if (UserManagement.IsActive(userID) && UserManagement.HasCompliancesAssigned(userID))
                    {
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "InformUser", "alert('Account can not be deactivated. One or more Compliances are assigned to user, please re-assign to other user.');", true);
                    }
                    else
                    {
                        UserManagement.ToggleStatus(userID);
                        UserManagementRisk.ToggleStatus(userID);

                    }
                    BindUsers();
                }
                else if (e.CommandName.Equals("RESET_PASSWORD"))
                {
                    User user = UserManagement.GetByID(userID);
                    mst_User mstuser = UserManagementRisk.GetByID_OnlyEditOption(userID);
                    string passwordText = Util.CreateRandomPassword(10);
                    user.Password = Util.CalculateAESHash(passwordText);
                    mstuser.Password = Util.CalculateAESHash(passwordText);

                    // added by sudarshan for comman notification
                    int customerID = -1;
                    string ReplyEmailAddressName = "";
                    if (AuthenticationHelper.Role == "SADMN" || AuthenticationHelper.Role == "IMPT")
                    {
                        ReplyEmailAddressName = "Avantis";
                    }
                    else
                    {
                        customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                        ReplyEmailAddressName = CustomerManagement.GetByID(customerID).Name;
                    }
                    string portalURL = string.Empty;
                    URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(AuthenticationHelper.CustomerID));
                    if (Urloutput != null)
                    {
                        portalURL = Urloutput.URL;
                    }
                    else
                    {
                        portalURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                    }
                    string message = EmailNotations.SendPasswordResetNotificationEmail(user, passwordText, Convert.ToString(portalURL), ReplyEmailAddressName);
                    //string message = EmailNotations.SendPasswordResetNotificationEmail(user, passwordText, Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]), ReplyEmailAddressName);
                    bool result = UserManagement.ChangePassword(user);
                    bool result1 = UserManagementRisk.ChangePassword(mstuser);
                    if (result && result1)
                    {
                        EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<string>(new string[] { user.Email }), null, null, "AVACOM account password changed", message);
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "InformUser", "alert('Password reset successfully.');", true);
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                    }
                }
                else if (e.CommandName.Equals("MODIFY_AUDIT_ASSIGNMENT"))
                {
                    lblReAssign.Text = string.Empty;
                    ViewState["ReAssignID"] = userID;
                    User user = UserManagement.GetByID(userID);
                    lblReAssign.Text = user.FirstName + " " + user.LastName;
                    BindNewUserList(user.CustomerID ?? -1, ddlREAssignUser);                   
                    ReAssign.Update();
                    BindReAssingGrid();
                    GetPageDisplaySummaryReAssign();
                    BindProcess();
                }
                else if (e.CommandName.Equals("UNLOCK_USER"))
                {
                    UserManagement.WrongAttemptCountUpdate(userID);
                    UserManagementRisk.WrongAttemptCountUpdate(userID);
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "Unloack", "alert('User unlocked successfully.');", true);
                    BindUsers();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindNewUserList(int customerID, DropDownList ddllist)
        {
            try
            {
                ddllist.DataTextField = "Name";
                ddllist.DataValueField = "ID";
                ddllist.DataSource = UserManagement.GetAllByCustomerID(customerID, null);
                ddllist.DataBind();
                ddllist.Items.Insert(0, new ListItem("Select user", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdUser_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdUser.PageIndex = e.NewPageIndex;
                BindUsers();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void upUserList_Load(object sender, EventArgs e)
        {
            try
            {
                // ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeComboboxForAssignmentsDialog", "initializeComboboxForAssignmentsDialog();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdUser_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }

                if (ddlCustomerList.SelectedValue != "-1")
                {
                    customerID = Convert.ToInt32(ddlCustomerList.SelectedValue);
                }
                var users = UserManagement.GetAllUser(customerID, tbxFilter.Text);                                            
                grdUser.DataSource = users;
                Session["TotalRows"] = users.Count;
                grdUser.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdUser_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {                  
                    int id = Convert.ToInt32(((GridView)sender).DataKeys[e.Row.RowIndex].Value);                   
                    LinkButton lbtnDelete = (LinkButton)e.Row.FindControl("lbtnDelete");                    
                    LinkButton lbtnChangeStatus = (LinkButton)e.Row.FindControl("lbtnChangeStatus");                    
                    LinkButton lbtnReassignment = (LinkButton)e.Row.FindControl("lbtnReassignment");

                    if (UserManagement.GetByID(id).RoleID == 1)
                    {
                        lbtnDelete.Visible = false;                        
                        lbtnChangeStatus.Enabled = false;
                        lbtnChangeStatus.Attributes.Add("href", "#");
                        lbtnChangeStatus.OnClientClick = string.Empty;
                    }

                    if (id == AuthenticationHelper.UserID)
                    {
                        lbtnChangeStatus.Enabled = false;
                        lbtnChangeStatus.Attributes.Add("href", "#");
                        lbtnChangeStatus.OnClientClick = string.Empty;
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "removeclass", "javascript:remoDisabledclass();", true);
                    }                                        
                    var x = 0;
                    var y = 0;
                    var z = 0;

                    x = UserManagementRisk.GetAllProcessID(id).Count;
                    y= UserManagementRisk.GetAllImplementationID(id).Count;
                    z= UserManagementRisk.GetAllICFRID(id).Count;

                    if (x <= 0 && y <= 0 && z <= 0)
                    {
                        lbtnReassignment.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdUser_RowCreated(object sender, GridViewRowEventArgs e)
        {            
        }        
        #endregion       
        protected void btnAddUser_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlCustomerList.SelectedValue))
            {                
                if (ddlCustomerList.SelectedValue != "-1")
                {
                    udcInputForm.AddNewUser(Convert.ToInt32(ddlCustomerList.SelectedValue));
                }
                else
                {
                    int customerID = -1;
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    udcInputForm.AddNewUser(customerID);
                }
            }

        }

        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                grdUser.PageIndex = 0;
                BindUsers();
                bindPageNumber();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
      
        protected void upModifyAssignment_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "removeclass", "javascript:remoDisabledclass()", true);                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }



        protected void ddlCustomerList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (AuthenticationHelper.Role == "SADMN" || AuthenticationHelper.Role == "IMPT")
                {
                    if (ddlCustomerList.SelectedItem.Text == "Select Customer")
                    {
                        btnAddUser.Visible = false;
                    }
                    else
                    {
                        btnAddUser.Visible = true;
                    }
                }
                BindUsers();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindCustomers()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                ddlCustomerList.DataTextField = "Name";
                ddlCustomerList.DataValueField = "ID";

                ddlCustomerList.DataSource = CustomerManagement.GetAll(customerID);
                ddlCustomerList.DataBind();

                ddlCustomerList.Items.Insert(0, new ListItem("Select Customer", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }                   
        protected bool IsLocked(string emailID)
        {
            try
            {
                if (UserManagement.WrongAttemptCount(emailID.Trim()) >= 3)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }           
        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();
                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);
                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);
                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdUser.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);              
                ////Reload the Grid
                BindUsers();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdUser.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }                
            }
            catch (Exception ex)
            {
                // ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }
        
        protected void ddlPageSizeReAssignment_SelectedIndexChanged(object sender, EventArgs e)
        {
        }
        protected void grdReassign_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
        }
        protected void grdReassign_RowCreated(object sender, GridViewRowEventArgs e)
        {
        }
        protected void grdReassign_Sorting(object sender, GridViewSortEventArgs e)
        {
        }
        protected void ReAssign_Load(object sender, EventArgs e)
        {

            int UserID = -1;
            if (ViewState["ReAssignID"] != null)
            {
                UserID = Convert.ToInt32(ViewState["ReAssignID"]);
            }
            BindProcess(); BindReAssingGrid();
            lblReAssign.Text = string.Empty;
            User user = UserManagement.GetByID(UserID);
            lblReAssign.Text = user.FirstName + " " + user.LastName;
            BindNewUserList(user.CustomerID ?? -1, ddlREAssignUser);
            GetPageDisplaySummaryReAssign();
        }
        public void BindReAssingGrid()
        {
            int UserID = -1;
            if (ViewState["ReAssignID"] != null)
            {
                UserID= Convert.ToInt32(ViewState["ReAssignID"]);
            }
            int processid = -1;
            string processName = string.Empty;
            string action = Convert.ToString(rbtEventReAssignment.SelectedValue);
            if (!string.IsNullOrEmpty(txtReAssignFilter.Text))
            {
                processName = txtReAssignFilter.Text.Trim().ToLower();
            }
            if (action.Equals("Process"))
            {
                if (!string.IsNullOrEmpty(ddlProcess.SelectedValue))
                {
                    processid = Convert.ToInt32(ddlProcess.SelectedValue);
                }
                Session["TotalRowsReAssging"] = null;
                grdReassign.DataSource = UserManagementRisk.GetAllReassignData(UserID, processid, processName);
                Session["TotalRowsReAssging"] = UserManagementRisk.GetAllReassignData(UserID, processid, processName).Count;
                grdReassign.DataBind();
            }
            if (action.Equals("Implementation"))
            {
                if (!string.IsNullOrEmpty(ddlProcess.SelectedValue))
                {
                    processid = Convert.ToInt32(ddlProcess.SelectedValue);
                }
                Session["TotalRowsReAssging"] = null;
                grdReassign.DataSource = UserManagementRisk.GetAllReassignDataImplement(UserID, processid, processName);
                Session["TotalRowsReAssging"] = UserManagementRisk.GetAllReassignDataImplement(UserID, processid, processName).Count;
                grdReassign.DataBind();
            }
            if (action.Equals("ICFR"))
            {
                if (!string.IsNullOrEmpty(ddlProcess.SelectedValue))
                {
                    processid = Convert.ToInt32(ddlProcess.SelectedValue);
                }
                Session["TotalRowsReAssging"] = null;
                grdReassign.DataSource = UserManagementRisk.GetAllReassignDataICFR(UserID, processid, processName);
                Session["TotalRowsReAssging"] = UserManagementRisk.GetAllReassignDataICFR(UserID, processid, processName).Count;
                grdReassign.DataBind();
            }
        }
        protected void txtReAssignFilter_TextChanged(object sender, EventArgs e)
        {
            int UserID = -1;
            if (ViewState["ReAssignID"] != null)
            {
                UserID = Convert.ToInt32(ViewState["ReAssignID"]);
            }
            int processid = -1;
            string processName = string.Empty;
            if (!string.IsNullOrEmpty(txtReAssignFilter.Text))
            {
                processName = txtReAssignFilter.Text.Trim().ToLower();
            }
            
            string action = Convert.ToString(rbtEventReAssignment.SelectedValue);
            if (action.Equals("Process"))
            {
                if (!string.IsNullOrEmpty(ddlProcess.SelectedValue))
                {
                    processid = Convert.ToInt32(ddlProcess.SelectedValue);
                }
                Session["TotalRowsReAssging"] = null;
                grdReassign.DataSource = UserManagementRisk.GetAllReassignData(UserID, processid, processName);
                Session["TotalRowsReAssging"] = UserManagementRisk.GetAllReassignData(UserID, processid, processName).Count;
                grdReassign.DataBind();
            }
            if (action.Equals("Implementation"))
            {
                if (!string.IsNullOrEmpty(ddlProcess.SelectedValue))
                {
                    processid = Convert.ToInt32(ddlProcess.SelectedValue);
                }
                Session["TotalRowsReAssging"] = null;
                grdReassign.DataSource = UserManagementRisk.GetAllReassignDataImplement(UserID, processid, processName);
                Session["TotalRowsReAssging"] = UserManagementRisk.GetAllReassignDataImplement(UserID, processid, processName).Count;
                grdReassign.DataBind();
            }
            if (action.Equals("ICFR"))
            {
                if (!string.IsNullOrEmpty(ddlProcess.SelectedValue))
                {
                    processid = Convert.ToInt32(ddlProcess.SelectedValue);
                }
                Session["TotalRowsReAssging"] = null;
                grdReassign.DataSource = UserManagementRisk.GetAllReassignDataICFR(UserID, processid, processName);
                Session["TotalRowsReAssging"] = UserManagementRisk.GetAllReassignDataICFR(UserID, processid, processName).Count;
                grdReassign.DataBind();
            }
        }
        public void BindProcess()
        {
            try
            {
                int UserID = -1;
                if (ViewState["ReAssignID"] != null)
                {
                    UserID = Convert.ToInt32(ViewState["ReAssignID"]);
                }
                string action = Convert.ToString(rbtEventReAssignment.SelectedValue);
                if (action.Equals("Process"))
                {
                    ddlProcess.DataTextField = "Name";
                    ddlProcess.DataValueField = "ProcessId";
                    var ProcessList = UserManagementRisk.GetddlProcessDataProc(UserID);
                    ddlProcess.DataSource = ProcessList;
                    ddlProcess.DataBind();
                    ddlProcess.Items.Insert(0, new ListItem("Select Process", "-1"));
                }
                if (action.Equals("Implementation"))
                {
                    ddlProcess.DataTextField = "Name";
                    ddlProcess.DataValueField = "ProcessId";
                    var ProcessList = UserManagementRisk.GetddlProcessDataImp(UserID);
                    ddlProcess.DataSource = ProcessList;
                    ddlProcess.DataBind();
                    ddlProcess.Items.Insert(0, new ListItem("Select Process", "-1"));
                }
                if (action.Equals("ICFR"))
                {
                    ddlProcess.DataTextField = "Name";
                    ddlProcess.DataValueField = "ProcessId";
                    var ProcessList = UserManagementRisk.GetddlProcessDataICFR(UserID);
                    ddlProcess.DataSource = ProcessList;
                    ddlProcess.DataBind();
                    ddlProcess.Items.Insert(0, new ListItem("Select Process", "-1"));
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void ddlProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindReAssingGrid();
        }
        protected void rbtEventReAssignment_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindProcess(); BindReAssingGrid(); GetPageDisplaySummaryReAssign();
        }
        private void ReAssignCheckBoxValueSaved()
        {
            List<Tuple<int, int>> chkList = new List<Tuple<int, int>>();
            int AuditID = -1;
            foreach (GridViewRow gvrow in grdReassign.Rows)
            {
                AuditID = Convert.ToInt32(grdReassign.DataKeys[gvrow.RowIndex].Value);
                bool result = ((CheckBox)gvrow.FindControl("chkEvent")).Checked;
                int BranchID = Convert.ToInt32(((Label)gvrow.FindControl("lblBrachID")).Text);

                //Tuple<int, string>  = new Tuple<int, string>();
                if (ViewState["ReAssignedEventID"] != null)
                    chkList = (List<Tuple<int, int>>)ViewState["ReAssignedEventID"];

                var checkedData = chkList.Where(entry => entry.Item1 == AuditID && entry.Item2 == BranchID).FirstOrDefault();
                if (result)
                {
                    if (checkedData == null)
                        chkList.Add(new Tuple<int, int>(AuditID, BranchID));
                }
                else
                    chkList.Remove(checkedData);
            }
            if (chkList != null && chkList.Count > 0)
                ViewState["ReAssignedEventID"] = chkList;
        }
        protected void btnReassign_Click(object sender, EventArgs e)
        {
            try
            {
                string[] confirmValue = Request.Form["confirm_Event_value"].Split(',');

                if (confirmValue[confirmValue.Length - 1] == "Yes")
                {
                    ReAssignCheckBoxValueSaved();
                    List<Tuple<int, int>> chkList = (List<Tuple<int, int>>)ViewState["ReAssignedEventID"];
                    if (chkList != null)
                    {
                        string action = Convert.ToString(rbtEventReAssignment.SelectedValue);
                        if (action.Equals("Process"))
                        {
                            UserManagementRisk.UpdateReAssignedProcess(Convert.ToInt32(ViewState["ReAssignID"]),Convert.ToInt32(ddlREAssignUser.SelectedValue), chkList);
                            CustomModifyAsignment.IsValid = false;
                            CustomModifyAsignment.ErrorMessage = "User Reasigned Successfull.";
                        }
                        if (action.Equals("Implementation"))
                        {
                            UserManagementRisk.UpdateReAssignedImplementation(Convert.ToInt32(ViewState["ReAssignID"]), Convert.ToInt32(ddlREAssignUser.SelectedValue), chkList);
                            CustomModifyAsignment.IsValid = false;
                            CustomModifyAsignment.ErrorMessage = "User Reasigned Successfull.";
                        }
                        if (action.Equals("ICFR"))
                        {
                            UserManagementRisk.UpdateReAssignedICFR(Convert.ToInt32(ViewState["ReAssignID"]), Convert.ToInt32(ddlREAssignUser.SelectedValue), chkList);
                            CustomModifyAsignment.IsValid = false;
                            CustomModifyAsignment.ErrorMessage = "User Reasigned Successfull.";
                        }
                        BindReAssingGrid();
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divReAssign\").dialog('close')", true);
                        ViewState["ReAssignedEventID"] = null;
                    }
                    else
                    {
                        CustomModifyAsignment.IsValid = false;
                        CustomModifyAsignment.ErrorMessage = "Please select at list one event for proceed.";
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divAssignEvent\").dialog('close')", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CustomModifyAsignment.IsValid = false;
                CustomModifyAsignment.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void lBPreviousReAssign_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                if (Convert.ToInt32(SelectedPageNoReAssign.Text) > 1)
                {
                    SelectedPageNoReAssign.Text = (Convert.ToInt32(SelectedPageNoReAssign.Text) - 1).ToString();
                    grdReassign.PageSize = Convert.ToInt32(ddlPageSizeReAssign.SelectedValue);
                    grdReassign.PageIndex = Convert.ToInt32(SelectedPageNoReAssign.Text) - 1;
                }
                else
                {

                }
                //Reload the Grid
                BindReAssingGrid();
                GetPageDisplaySummaryReAssign();
            }
            catch (Exception ex)
            {
                //ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        protected void lBNextReAssign_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                int currentPageNo = Convert.ToInt32(SelectedPageNoReAssign.Text);

                if (currentPageNo < GetTotalPagesCountReAssign())
                {
                    SelectedPageNoReAssign.Text = (currentPageNo + 1).ToString();
                    grdReassign.PageSize = Convert.ToInt32(ddlPageSizeReAssign.SelectedValue);
                    grdReassign.PageIndex = Convert.ToInt32(SelectedPageNoReAssign.Text) - 1;
                }
                else
                {
                }
                BindReAssingGrid();
            }
            catch (Exception ex)
            {
                //  ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        private void GetPageDisplaySummaryReAssign()
        {
            try
            {
                lTotalCountReAssign.Text = GetTotalPagesCountReAssign().ToString();

                if (lTotalCountReAssign.Text != "0")
                {
                    if (SelectedPageNoReAssign.Text == "")
                        SelectedPageNoReAssign.Text = "1";

                    if (SelectedPageNoReAssign.Text == "0")
                        SelectedPageNoReAssign.Text = "1";
                }
                else if (lTotalCountReAssign.Text == "0")
                {
                    SelectedPageNoReAssign.Text = "0";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private int GetTotalPagesCountReAssign()
        {
            try
            {
                TotalRowsReAssign.Value = Session["TotalRowsReAssging"].ToString();
                int totalPages = Convert.ToInt32(TotalRowsReAssign.Value) / Convert.ToInt32(ddlPageSizeReAssign.SelectedValue);
                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRowsReAssign.Value) % Convert.ToInt32(ddlPageSizeReAssign.SelectedValue);
                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        protected void ddlPageSizeReAssign_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                SelectedPageNoReAssign.Text = "1";
                int currentPageNo = Convert.ToInt32(SelectedPageNoReAssign.Text);

                if (currentPageNo <= GetTotalPagesCountReAssign())
                {
                    //SelectedPageNo.Text = (currentPageNo).ToString();
                    grdReassign.PageSize = Convert.ToInt32(ddlPageSizeReAssign.SelectedValue);
                    grdReassign.PageIndex = Convert.ToInt32(SelectedPageNoReAssign.Text) - 1;
                }               
                //Reload the Grid
                BindReAssingGrid();
                GetPageDisplaySummaryReAssign();
            }
            catch (Exception ex)
            {
                // ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        protected void lnkExportUserlist_Click(object sender, EventArgs e)
        {
            using (ExcelPackage exportPackge = new ExcelPackage())
            {
                try
                {
                    int CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    String FileName = String.Empty;
                    ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("UserList");
                    var listOfBranch = UserManagementRisk.GetUserReportList(CustomerId);
                    DataTable ExcelData = null;
                    DataView view = new System.Data.DataView((listOfBranch as List<SP_UserListReport_Result>).ToDataTable());
                    ExcelData = view.ToTable("Selected", false, "FirstName", "LastName", "Email", "ContactNumber", "RoleCode", "CustomerBranchName", "IsDepartmentHead", "DepartmentName", "ISAuditHead", "ISAuditManager", "CreatedOn");
                    string CustomerName = CustomerManagement.CustomerGetByIDName(Convert.ToInt32(CustomerId));

                    //foreach (DataRow item in ExcelData.Rows)
                    //{
                    //    if (item["CreatedOn"] != null && item["CreatedOn"] != DBNull.Value)
                    //    {
                    //        DateTime CreatedOnDate = Convert.ToDateTime(item["CreatedOn"]);
                    //        string DateVal = CreatedOnDate.ToString("dd-MMM-yyyy");
                    //        string TimeVal = CreatedOnDate.ToString("HH:mm:ss");
                    //        item["CreatedOn"] = Convert.ToString(DateVal + ", " + TimeVal);
                    //        //item["CreatedOn"] = Convert.ToDateTime(item["CreatedOn"]).ToString("dd-MMM-yyyy");
                    //    }
                    //}

                    FileName = "User List";
                    exWorkSheet.Cells["A1"].LoadFromDataTable(ExcelData, true);

                    exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["A1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["A1"].Value = "First Name";
                    exWorkSheet.Cells["A1"].AutoFitColumns(20);
                    //exWorkSheet.Cells["A1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));

                    exWorkSheet.Cells["B1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["B1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["B1"].Value = "Last Name";
                    exWorkSheet.Cells["B1"].AutoFitColumns(20);
                    //exWorkSheet.Cells["B1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));

                    exWorkSheet.Cells["C1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["C1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["C1"].Value = "Email";
                    exWorkSheet.Cells["C1"].AutoFitColumns(30);
                    //exWorkSheet.Cells["C1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));

                    exWorkSheet.Cells["D1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["D1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["D1"].Value = "Contact Number";
                    exWorkSheet.Cells["D1"].AutoFitColumns(20);
                    // exWorkSheet.Cells["D1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));

                    exWorkSheet.Cells["E1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["E1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["E1"].Value = "Role Code";
                    exWorkSheet.Cells["E1"].AutoFitColumns(15);
                    // exWorkSheet.Cells["E1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));

                    exWorkSheet.Cells["F1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["F1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["F1"].Value = "Customer / Branch Name";
                    exWorkSheet.Cells["F1"].AutoFitColumns(30);
                    // exWorkSheet.Cells["F1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));

                    exWorkSheet.Cells["G1"].Value = "Is Department Head";
                    exWorkSheet.Cells["G1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["G1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["G1"].AutoFitColumns(20);
                    // exWorkSheet.Cells["G1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));

                    exWorkSheet.Cells["H1"].Value = "Department Name";
                    exWorkSheet.Cells["H1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["H1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["H1"].AutoFitColumns(30);
                    // exWorkSheet.Cells["H1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));

                    exWorkSheet.Cells["I1"].Value = "Is Audit Head";
                    exWorkSheet.Cells["I1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["I1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["I1"].AutoFitColumns(20);
                    // exWorkSheet.Cells["I1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));

                    exWorkSheet.Cells["J1"].Value = "Is Audit Manager";
                    exWorkSheet.Cells["J1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["J1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["J1"].AutoFitColumns(20);
                    //  exWorkSheet.Cells["J1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));

                    exWorkSheet.Cells["K1"].Value = "Created On";
                    exWorkSheet.Cells["K1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["K1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["K1"].AutoFitColumns(20);
                    //  exWorkSheet.Cells["J1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));

                    using (ExcelRange col = exWorkSheet.Cells[1, 1, 1 + ExcelData.Rows.Count, 11])
                    {
                        col.Style.WrapText = true;
                        col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                        // Assign borders
                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    }

                    Byte[] fileBytes = exportPackge.GetAsByteArray();
                    Response.ClearContent();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=UserList.xlsx");
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.ms-excel";
                    StringWriter sw = new StringWriter();
                    Response.BinaryWrite(fileBytes);
                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                }
            }
        }
    }
}