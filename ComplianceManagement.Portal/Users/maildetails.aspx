﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NewCompliance.Master" AutoEventWireup="true" CodeBehind="maildetails.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.maildetails" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div style="float:left;width:100%">
          <div style="float:left;width:100%;color: #2b2b2b;">
              <div style="float:left;font-weight: 600;">From:</div> <div style="float:left;margin-left: 2%;"><asp:Label ID="lblsender" runat="server"></asp:Label></div>
          </div>
        <div class="clearfix"> </div>
        <div style="float:left;width:100%;margin-top: 2%;color: #2b2b2b;">
              <div style="float:left;font-weight: 600;">Date Time:</div> <div style="float:left;margin-left: 2%;"><asp:Label ID="lbldate" runat="server"></asp:Label></div>
          </div>
        <div class="clearfix"> </div>
            <div style="float:left;width:100%;margin-top: 2%; color: #2b2b2b;">
                <div style="float:left;font-weight: 600;">Subject:</div> <div style="float:left;margin-left: 2%;"><asp:Label ID="lblsubject" runat="server"></asp:Label></div>
          </div>
         <div class="clearfix"> </div>
           <div style="float:left;width:100%;margin-top: 2%; color: #2b2b2b;">
               <div style="float:left;font-weight: 600;">Message:</div> <div style="float:left;margin-left: 2%;"><asp:Label ID="lblmessage" runat="server"></asp:Label></div>
          </div>
           <div class="clearfix"> </div>
           <div style="float:left;width:100%;margin-top: 2%; color: #2b2b2b;">
               <div style="float:left;font-weight: 600;">Attachment:</div> <div style="float:left;margin-left: 2%;"><asp:Label ID="lblattachment" runat="server"></asp:Label></div>
          </div>

    </div>
</asp:Content>
