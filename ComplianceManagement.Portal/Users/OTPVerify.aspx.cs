﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Web.Security;
using System.Configuration;
using System.Drawing;
using com.VirtuosoITech.ComplianceManagement.Portal.ProductMapping;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using System.Text;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Users
{
    public partial class OTPVerify : System.Web.UI.Page
    {
        static int WrongOTPCount = 0;
        static int ResendOTPAttemp = 3;
        protected string LogoName;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {


                if (Session["CustomerID_new"] != null)
                {
                    int CustomerID = Convert.ToInt32(Session["CustomerID_new"]);
                    Customer objCust = UserManagement.GetCustomerforLogoCustomer(CustomerID);
                    if (objCust != null)
                    {
                        if (objCust.LogoPath != null)
                        {
                            var CustomerLogo = objCust.LogoPath;
                            LogoName = CustomerLogo.Replace("~", "..");
                        }
                    }
                }

                if (!string.IsNullOrEmpty(Request.QueryString["RLCS_UID"]))
                {
                    string key = "avantis";
                    string Email = Request.QueryString["RLCS_UID"];
                    Email = Email.Replace(" ", "+");
                    var encryptedBytes = Convert.FromBase64String(Email);
                    try
                    {
                        Email = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
                    }
                    catch { }


                    string EmailIOS = string.Empty;
                    try
                    {
                        EmailIOS = Encoding.UTF8.GetString(DataEncryption.DecryptIOS(encryptedBytes, DataEncryption.GetRijndaelManagedIOS(key)));
                    }
                    catch { }

                    RLCS_User_Mapping user = UserManagement.GetByRLCSUsername(Email.Trim());
                    if (user == null)
                    {
                        user = UserManagement.GetByRLCSUsername(EmailIOS.Trim());
                    }
                    if (user != null)
                    {
                        string ipaddress = string.Empty;
                        string Macaddress = string.Empty;
                        try
                        {
                            Macaddress = Util.GetMACAddress();
                            if (ipaddress == "" || ipaddress == null)
                                ipaddress = Request.ServerVariables["REMOTE_ADDR"];

                        }
                        catch (Exception ex)
                        {
                            LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                        }

                        RLCSProcessAuthenticationInformation(user, ipaddress, Macaddress);
                    }
                }
                else
                {

                    var demail = Convert.ToString(Request.QueryString["d_email"]);
                    if (!string.IsNullOrEmpty(demail))
                    {
                        try
                        {
                            var email = Util.DCalculateAESHash(demail.Trim());
                            Business.Data.User user = null;
                            if (UserManagement.IsValidUser(email.Trim(), out user))
                            {
                                ProcessAuthenticationInformation(user);
                            }
                        }
                        catch (Exception ex)
                        {
                            LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                        }
                    }
                    else
                    {
                        var skipdetails = GetByID(Convert.ToInt32(Session["userID"].ToString()));
                        if (ConfigurationManager.AppSettings["SkipOTPQA"].ToString() == "1" || skipdetails != null)
                        {
                            User user = UserManagement.GetByID(Convert.ToInt32(Session["userID"].ToString()));
                            int UserId;
                            UserId = int.Parse(Session["userID"].ToString());
                            ProcessAuthenticationInformation(user);
                        }
                    }
                    WrongOTPCount = 0;
                    DisplayOTPMessage();
                    CheckResendOTPCount();
                    if (Session["CustomerID_new"] != null)
                    {
                        cid.Value = Session["CustomerID_new"].ToString();
                    }
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvLogin.IsValid = false;
                cvLogin.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static tbl_SkipOtp GetByID(int userID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var user = (from row in entities.tbl_SkipOtp
                            where row.UserId == userID
                            select row).SingleOrDefault();

                return user;
            }
        }
        protected void DisplayOTPMessage()
        {
            if (!string.IsNullOrEmpty(Convert.ToString(Session["Email"])))
            {
                try
                {
                    // For Email Id Logic Set.
                    string[] spl = Convert.ToString(Session["Email"]).Split('@');
                    int lenthusername = (spl[0].Length / 2);
                    int mod = spl[0].Length % 2;
                    if (mod > 0)
                    {
                        lenthusername = lenthusername - mod;
                    }
                    string beforemanupilatedemail = "";
                    if (lenthusername == 1)
                    {
                        beforemanupilatedemail = spl[0].Substring(lenthusername);
                    }
                    else
                    {
                        beforemanupilatedemail = spl[0].Substring(lenthusername + 1);
                    }

                    string appendstar = "";
                    for (int i = 0; i < beforemanupilatedemail.Length; i++) /*lenthusername*/
                    {
                        appendstar += "*";
                    }

                    string manupilatedemail = spl[0].Replace(beforemanupilatedemail, appendstar);

                    string[] spl1 = spl[1].Split('.');
                    int lenthatname = (spl1[0].Length / 2);
                    int modat = spl1[0].Length % 2;
                    if (modat > 0)
                    {
                        lenthatname = lenthatname - modat;
                    }

                    string beforatemail = "";
                    if (lenthatname == 1)
                    {
                        beforatemail = spl1[0].Substring(lenthatname);
                    }
                    else
                    {
                        beforatemail = spl1[0].Substring(lenthatname + 1);
                    }
                    string appendstar1 = "";
                    for (int i = 0; i < beforatemail.Length; i++) /*lenthatname*/
                    {
                        appendstar1 += "*";
                    }

                    string manupilatedatemail = spl1[0].Replace(beforatemail, appendstar1);
                    string emailid = manupilatedemail;

                    DateTime NewTime = DateTime.Now.AddMinutes(30);
                    Time.Text = NewTime.Hour + ":" + NewTime.Minute + NewTime.ToString(" tt");
                    email.Text = Convert.ToString(Session["Email"]).Replace(spl[0], manupilatedemail).Replace(spl1[0], manupilatedatemail);
                    if (!string.IsNullOrEmpty(Convert.ToString(Session["ContactNo"])))
                    {
                        // For Mobile Number Logic Set.
                        if (Convert.ToString(Session["ContactNo"]).Length == 10 && (Convert.ToString(Session["ContactNo"]).StartsWith("9") || Convert.ToString(Session["ContactNo"]).StartsWith("8") || Convert.ToString(Session["ContactNo"]).StartsWith("7")))
                        {
                            string s = Convert.ToString(Session["ContactNo"]);
                            int l = s.Length;
                            s = s.Substring(l - 4);
                            string r = new string('*', l - 4);
                            //r = r + s;
                            mobileno.Text = r + s;
                        }
                        else
                        {
                            mobileno.Text = "";
                            cvLogin.IsValid = false;
                            cvLogin.ErrorMessage = "You don't seem to have a correct registered mobile number with us. However OTP sent on your registered email. Please update your mobile number to use this feature in future.";
                        }
                    }
                    else
                    {
                        mobileno.Text = "";
                        cvLogin.IsValid = false;
                        cvLogin.ErrorMessage = "You don't seem to have a correct registered mobile number with us. However OTP sent on your registered email. Please update your mobile number to use this feature in future.";
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvLogin.IsValid = false;
                    cvLogin.ErrorMessage = "Server Error Occured. Please try again.";
                }
            }
        }

        protected void btnOTP_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(Session["userID"].ToString()))
                {
                    if (VerifyOTPManagement.verifyOTP(Convert.ToInt32(txtOTP.Text), Convert.ToInt32(Session["userID"].ToString())))
                    {
                        User user = UserManagement.GetByID(Convert.ToInt32(Session["userID"].ToString()));

                        int UserId;
                        UserId = int.Parse(Session["userID"].ToString());
                        #region[add customerwise expire days]
                        Customer c = CustomerManagement.GetByID(Convert.ToInt32(user.CustomerID));
                        int expdays = 0;
                        if (c == null)
                        {
                            expdays = 60;
                        }
                        else
                        {                                                       
                            int? Expirydaysgroup =CustomerManagement.GetExpirydays(Convert.ToInt32(user.CustomerID), Convert.ToInt32(user.ID));
                            if (Expirydaysgroup == null || Expirydaysgroup == 0)
                            {
                                int? Expirydays = CustomerManagement.GetExpirydays(Convert.ToInt32(c.ID));
                                if (Expirydays == null || Expirydays == 0)
                                {
                                    expdays = 60;
                                }
                                else
                                {
                                    expdays = Convert.ToInt32(Expirydays);
                                }
                            }
                            else
                            {
                                expdays = Convert.ToInt32(Expirydaysgroup);
                            }
                        }
                        #endregion
                        VerifyOTP OTPData = VerifyOTPManagement.GetByID(UserId);                        
                        TimeSpan span = DateTime.Now.Subtract(OTPData.CreatedOn.Value);                       
                        if (0 <= span.Minutes && span.Minutes <= 30)
                        {
                            if (Convert.ToBoolean(Session["RM"]))
                            {
                                if (FormsAuthentication.CookiesSupported)
                                {
                                    HttpCookie loginCookie = new HttpCookie("ALC");
                                    //let the cookie expire after 60 days
                                    // loginCookie.Expires = DateTime.Now.AddDays(60);
                                    loginCookie.Expires = DateTime.Now.AddDays(expdays);
                                    loginCookie.Values["EA"] = Convert.ToString(Session["EA"]);
                                    loginCookie.Values["MA"] = Convert.ToString(Session["MA"]);
                                    loginCookie.Secure =true;
                                    loginCookie.HttpOnly = true;
                                    Response.Cookies.Add(loginCookie);
                                }
                            }
                            else
                            {
                                Response.Cookies["ALC"].Expires = DateTime.Now.AddDays(-1);
                            }
                            VerifyOTPManagement.UpdateVerifiedFlag(Convert.ToInt32(Session["userID"].ToString()), Convert.ToInt32(txtOTP.Text));
                            ProcessAuthenticationInformation(user);
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "AlertMessage3", "settracknewSucess();", true);
                            Session["RM"] = null;
                            Session["EA"] = null;
                            Session["MA"] = null;
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "AlertMessage6", "settracknewFailed();", true);
                            cvLogin.ErrorMessage = "OTP Expired.";
                            cvLogin.IsValid = false;
                            return;
                        }
                    }
                    else
                    {
                        if (WrongOTPCount < 3)
                        {
                            cvLogin.ErrorMessage = "Invalid OTP, Please Enter Again.";
                            cvLogin.IsValid = false;
                            WrongOTPCount++;
                            CheckResendOTPCount();
                            return;
                        }
                        else
                        {
                            User user = UserManagement.GetByID(Convert.ToInt32(Session["userID"].ToString()));

                            if (UserManagement.HasUserSecurityQuestion(user.ID))
                            {
                                Session["QuestionBank"] = false;
                                Session["RM"] = Convert.ToBoolean(Session["RM"]);                                
                                Response.Redirect("~/SecurityQuestion/AddSecurityQuestions.aspx", false);
                            }
                            else
                            {
                                Session["QuestionBank"] = true;
                                Response.Redirect("~/SecurityQuestion/AddSecurityQuestions.aspx", false);
                            }
                        }
                    }
                }
                else
                {
                    cvLogin.ErrorMessage = "Please Enter Correct OTP.";
                    cvLogin.IsValid = false;
                    return;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvLogin.IsValid = false;
                cvLogin.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void lnkSecurityQA_Click(object sender, EventArgs e)
        {
            User user = UserManagement.GetByID(Convert.ToInt32(Session["userID"].ToString()));

            if (UserManagement.HasUserSecurityQuestion(user.ID))
            {
                Session["QuestionBank"] = false;
                Session["RM"] = Convert.ToBoolean(Session["RM"]);                
                Response.Redirect("~/SecurityQuestion/AddSecurityQuestions.aspx", false);
            }
            else
            {
                Session["QuestionBank"] = true;
                Response.Redirect("~/SecurityQuestion/AddSecurityQuestions.aspx", false);
            }
        }

        private void ProcessAuthenticationInformation(User user)
        {
            try
            {
                mst_User objmstuser = UserManagementRisk.GetByID(Convert.ToInt32(user.ID));
                string mstrole = RoleManagementRisk.GetByID(objmstuser.RoleID).Code;
                string name = string.Format("{0} {1}", user.FirstName, user.LastName);
                string role = RoleManagement.GetByID(user.RoleID).Code;
                int checkInternalapplicable = 0;
                int checkTaskapplicable = 0;
                int checkVerticalapplicable = 0;
                int checkLabelApplicable = 0;
                bool IsPaymentCustomer = false;

                int complianceProdType = 0;

                if (user.CustomerID == null)
                {
                    checkInternalapplicable = 2;
                    checkTaskapplicable = 2;
                    checkVerticalapplicable = 2;
                    checkLabelApplicable = 2;
                }
                else
                {
                    Customer c = CustomerManagement.GetByID(Convert.ToInt32(user.CustomerID));
                    //var IsInternalComplianceApplicable = CustomerManagement.GetByIcompilanceapplicableID(Convert.ToInt32(user.CustomerID));
                    var IsInternalComplianceApplicable = c.IComplianceApplicable;
                    if (IsInternalComplianceApplicable != -1)
                    {
                        checkInternalapplicable = Convert.ToInt32(IsInternalComplianceApplicable);
                    }

                    //var IsTaskApplicable = CustomerManagement.GetByTaskApplicableID(Convert.ToInt32(user.CustomerID));
                    var IsTaskApplicable = c.TaskApplicable;
                    if (IsTaskApplicable != -1)
                    {
                        checkTaskapplicable = Convert.ToInt32(IsTaskApplicable);
                    }
                    var IsVerticlApplicable = c.VerticalApplicable;
                    if (IsVerticlApplicable != null)
                    {
                        if (IsVerticlApplicable != -1)
                        {
                            checkVerticalapplicable = Convert.ToInt32(IsVerticlApplicable);
                        }
                    }
                    if (c.IsPayment != null)
                    {
                        IsPaymentCustomer = Convert.ToBoolean(c.IsPayment);
                    }
                    var IsLabelApplicable = c.IsLabelApplicable;
                    if (IsLabelApplicable != -1)
                    {
                        checkLabelApplicable = Convert.ToInt32(IsLabelApplicable);
                    }
                    if (c.ComplianceProductType != null)
                    {
                        complianceProdType = Convert.ToInt32(c.ComplianceProductType);
                    }
                }                             

                User userToUpdate = new User();
                userToUpdate.ID = user.ID;
                userToUpdate.LastLoginTime = DateTime.Now;
                userToUpdate.WrongAttempt = 0;
                UserManagement.Update(userToUpdate);

                if (role.Equals("SADMN"))
                {
                    FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), Convert.ToBoolean(Session["RM"]));
                    FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12}", user.ID, role, name, checkInternalapplicable, "C", user.CustomerID, checkTaskapplicable, string.Empty, string.Empty, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType), false);
                    TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);                    
                    Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);
                    Response.Redirect("~/Users/UserSummary.aspx", false);
                }
                else if (role.Equals("IMPT"))
                {
                    FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), Convert.ToBoolean(Session["RM"]));
                    FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12}", user.ID, role, name, checkInternalapplicable, "C", user.CustomerID, checkTaskapplicable, string.Empty, string.Empty, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType), false);

                    TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);                    
                    Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);
                    Response.Redirect("~/Common/CompanyStructure.aspx", false);
                }
                else if (role.Equals("UPDT"))
                {
                    FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), Convert.ToBoolean(Session["RM"]));
                    FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12}", user.ID, role, name, checkInternalapplicable, "C", user.CustomerID, checkTaskapplicable, string.Empty, string.Empty, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType), false);
                    TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);                    
                    Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);
                    Response.Redirect("~/Common/LegalUpdateAdmin.aspx", false);
                }
                else if (role.Equals("RPER"))
                {
                    FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), Convert.ToBoolean(Session["RM"]));
                    FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12}", user.ID, role, name, checkInternalapplicable, "C", user.CustomerID, checkTaskapplicable, string.Empty, string.Empty, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType), false);
                    TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);                    
                    Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);
                    Response.Redirect("~/Common/ResearchPerformerAdmin.aspx", false);
                }
                else if (role.Equals("RREV"))
                {
                    FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), Convert.ToBoolean(Session["RM"]));
                    FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12}", user.ID, role, name, checkInternalapplicable, "C", user.CustomerID, checkTaskapplicable, string.Empty, string.Empty, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType), false);
                    TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);                    
                    Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);
                    Response.Redirect("~/Common/ResearchReviewerAdmin.aspx", false);
                }
                else if (role.Equals("HVADM"))
                {
                    FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), Convert.ToBoolean(Session["RM"]));
                    FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12}", user.ID, role, name, checkInternalapplicable, "C", user.CustomerID, checkTaskapplicable, string.Empty, string.Empty, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType), false);
                    TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
                    Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);
                    Response.Redirect("~/RLCSVendorAudit/RLCSUploadChecklist.aspx", false);
                }               
                else
                {
                    ProductMappingStructure _obj = new ProductMappingStructure();
                    var ProductMappingDetails = UserManagement.GetByProductIDList(Convert.ToInt32(user.CustomerID));                 
                    if (ProductMappingDetails.Count == 1)
                    {                        
                        if (ProductMappingDetails.Contains(1))
                        {
                            _obj.FormsAuthenticationRedirect_Compliance(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);                          
                        }
                        else if (ProductMappingDetails.Contains(2))
                        {
                            _obj.FormsAuthenticationRedirect_Litigation(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable,IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                        }
                        else if (ProductMappingDetails.Contains(3))
                        {
                            _obj.FormsAuthenticationRedirect_IFC(objmstuser, mstrole, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                        }
                        else if (ProductMappingDetails.Contains(4))
                        {
                            _obj.FormsAuthenticationRedirect_ARS(objmstuser, mstrole, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                        }
                        else if (ProductMappingDetails.Contains(5))
                        {
                            _obj.FormsAuthenticationRedirect_Contract(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                        }
                        else if (ProductMappingDetails.Contains(6))
                        {
                            _obj.FormsAuthenticationRedirect_License(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                        }
                        else if (ProductMappingDetails.Contains(7))
                        {
                            _obj.FormsAuthenticationRedirect_RLCSVendor(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                        }
                        else if (ProductMappingDetails.Contains(8))
                        {
                            _obj.FormsAuthenticationRedirect_Secretarial(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                        }
                        else if (ProductMappingDetails.Contains(11))
                        {
                            _obj.FormsAuthenticationRedirect_RLCSVendorAuditNew(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            //_obj.FormsAuthenticationRedirect_RLCSVendor(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                        }
                    }
                    else if (ProductMappingDetails.Count > 1)
                    {
                        if (ProductMappingDetails.Count == 2 && ProductMappingDetails.Contains(2) && user.LitigationRoleID == null)
                        {
                            if (ProductMappingDetails.Contains(1))
                            {
                                _obj.FormsAuthenticationRedirect_Compliance(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }                            
                            else if (ProductMappingDetails.Contains(3))
                            {
                                _obj.FormsAuthenticationRedirect_IFC(objmstuser, mstrole, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(4))
                            {
                                _obj.FormsAuthenticationRedirect_ARS(objmstuser, mstrole, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(5))
                            {
                                _obj.FormsAuthenticationRedirect_Contract(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(2))
                            {
                                _obj.FormsAuthenticationRedirect_Litigation(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(6))
                            {
                                _obj.FormsAuthenticationRedirect_License(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(7))
                            {
                                _obj.FormsAuthenticationRedirect_RLCSVendor(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(8))
                            {
                                _obj.FormsAuthenticationRedirect_Secretarial(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);                                     
                            }
                            else if (ProductMappingDetails.Contains(11))
                            {
                                _obj.FormsAuthenticationRedirect_RLCSVendorAuditNew(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                                //_obj.FormsAuthenticationRedirect_RLCSVendor(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                        }
                        else if (ProductMappingDetails.Count == 2 && ProductMappingDetails.Contains(5) && user.ContractRoleID == null)
                        {
                            if (ProductMappingDetails.Contains(1))
                            {
                                _obj.FormsAuthenticationRedirect_Compliance(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(2)) 
                            {
                                _obj.FormsAuthenticationRedirect_Litigation(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(3))
                            {
                                _obj.FormsAuthenticationRedirect_IFC(objmstuser, mstrole, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(4))
                            {
                                _obj.FormsAuthenticationRedirect_ARS(objmstuser, mstrole, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(5))
                            {
                                _obj.FormsAuthenticationRedirect_Contract(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(6))
                            {
                                _obj.FormsAuthenticationRedirect_License(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(7))
                            {
                                _obj.FormsAuthenticationRedirect_RLCSVendor(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(8))
                            {
                                _obj.FormsAuthenticationRedirect_Secretarial(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(11))
                            {
                                _obj.FormsAuthenticationRedirect_RLCSVendorAuditNew(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                                //_obj.FormsAuthenticationRedirect_RLCSVendor(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                        }
                        else if (ProductMappingDetails.Count == 2 && ProductMappingDetails.Contains(6) && user.LicenseRoleID == null)
                        {
                            if (ProductMappingDetails.Contains(1))
                            {
                                _obj.FormsAuthenticationRedirect_Compliance(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(2))
                            {
                                _obj.FormsAuthenticationRedirect_Litigation(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(3))
                            {
                                _obj.FormsAuthenticationRedirect_IFC(objmstuser, mstrole, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(4))
                            {
                                _obj.FormsAuthenticationRedirect_ARS(objmstuser, mstrole, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(5))
                            {
                                _obj.FormsAuthenticationRedirect_Contract(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(6))
                            {
                                _obj.FormsAuthenticationRedirect_License(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(7))
                            {
                                _obj.FormsAuthenticationRedirect_RLCSVendor(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(8))
                            {
                                _obj.FormsAuthenticationRedirect_Secretarial(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(11))
                            {
                                _obj.FormsAuthenticationRedirect_RLCSVendorAuditNew(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                                //_obj.FormsAuthenticationRedirect_RLCSVendor(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                        }
                        else if (ProductMappingDetails.Count == 2 && ProductMappingDetails.Contains(7) && user.VendorRoleID == null)
                        {
                            if (ProductMappingDetails.Contains(1))
                            {
                                _obj.FormsAuthenticationRedirect_Compliance(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(2))
                            {
                                _obj.FormsAuthenticationRedirect_Litigation(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(3))
                            {
                                _obj.FormsAuthenticationRedirect_IFC(objmstuser, mstrole, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(4))
                            {
                                _obj.FormsAuthenticationRedirect_ARS(objmstuser, mstrole, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(5))
                            {
                                _obj.FormsAuthenticationRedirect_Contract(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(6))
                            {
                                _obj.FormsAuthenticationRedirect_License(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(7))
                            {
                                _obj.FormsAuthenticationRedirect_RLCSVendor(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(8))
                            {
                                _obj.FormsAuthenticationRedirect_Secretarial(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(11))
                            {
                                _obj.FormsAuthenticationRedirect_RLCSVendorAuditNew(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                                //_obj.FormsAuthenticationRedirect_RLCSVendor(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                        }
                        else
                        {
                            FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), Convert.ToBoolean(Session["RM"]));
                            FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12}", user.ID, role, name, checkInternalapplicable, "C", user.CustomerID, checkTaskapplicable, string.Empty, string.Empty, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType), false);                          
                            TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);                            
                            Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);
                            Response.Redirect("~/ProductMapping/ProductMappingStructure.aspx", false);
                        }
                    }
                    else
                    {
                        FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), Convert.ToBoolean(Session["RM"]));
                        FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12}", user.ID, role, name, checkInternalapplicable, "C", user.CustomerID, checkTaskapplicable, string.Empty, string.Empty, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType), false);
                        TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);                        
                        Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);
                        Response.Redirect("~/ProductMapping/ProductMappingStructure.aspx", false);
                    }
                }
                DateTime LastPasswordChangedDate = Convert.ToDateTime(UserManagement.GetByID(Convert.ToInt32(user.ID)).ChangPasswordDate);
                DateTime currentDate = DateTime.Now;
                LastPasswordChangedDate = LastPasswordChangedDate != null ? LastPasswordChangedDate : DateTime.Now;
                int noDays = Convert.ToInt32(ConfigurationManager.AppSettings["ChangedPasswordDays"]);
                int dateDifference = Convert.ToInt32((currentDate - LastPasswordChangedDate).TotalDays);
                if (dateDifference == noDays || dateDifference > noDays)
                {
                    Session["ChangePassword"] = true;
                    Response.Redirect("~/Account/ChangePassword.aspx", false);
                }
                else
                {
                    Session["ChangePassword"] = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lnkResendOTP_Click(object sender, EventArgs e)
        {
            try
            {
                User user = new User();
                string OTPValue = string.Empty;
                string ipaddress = string.Empty;
                long Contact;
                try
                {
                    ipaddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                    if (ipaddress == "" || ipaddress == null)
                        ipaddress = Request.ServerVariables["REMOTE_ADDR"];
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
                if (!string.IsNullOrEmpty(Convert.ToString(Session["userID"])))
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(Session["ResendOTP"])))
                    {
                        user = UserManagement.GetByID(Convert.ToInt32(Session["userID"].ToString()));

                        OTPValue = Convert.ToString(Session["ResendOTP"]);
                        if (ConfigurationManager.AppSettings["SkipOTPQA"].ToString() != "1")
                        {
                            VerifyOTP OTPData = new VerifyOTP()
                            {
                                UserId = Convert.ToInt32(user.ID),
                                EmailId = user.Email,
                                OTP = Convert.ToInt32(OTPValue),
                                CreatedOn = DateTime.Now,
                                IsVerified = false,
                                IPAddress = ipaddress
                            };
                            bool OTPresult = long.TryParse(user.ContactNumber, out Contact);
                            if (OTPresult)
                            {
                                OTPData.MobileNo = Contact;
                            }
                            else
                            {
                                OTPData.MobileNo = 0;
                            }
                            
                                VerifyOTPManagement.Create(OTPData); // Insert Data in OTP Table.
                                WrongOTPCount = 0;
                            //Send Email on User Mail Id.
                            string SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"].ToString();
                            if (user.CustomerID != 5)
                            {
                                EmailManager.SendMail(SenderEmailAddress, new List<String>(new String[] { user.Email }), null, null, "OTP Verification", "Your One Time Password for Avantis login is:" + Convert.ToString(OTPValue) + "<br>" + "<br>" + "Thank you," + "<br>" + "Team Teamlease");
                            }
                            //Send SMS on User Mobile No.
                            if (OTPresult && user.RoleID != 12 && user.ContactNumber.Length == 10 && (user.ContactNumber.StartsWith("9") || user.ContactNumber.StartsWith("8") || user.ContactNumber.StartsWith("7") || user.ContactNumber.StartsWith("6")))
                            {
                                var data = VerifyOTPManagement.GetSMSConfiguration("Avacom");
                                if (data != null)
                                {
                                    SendSms.sendsmsto(user.ContactNumber, "Your One Time Password for Avantis login is: " + Convert.ToString(OTPValue) + ".  " + "Thank you, " + "Avantis Team.", data.TEMPLATEID_DLT_TE_ID, data.authkey, data.Header_sender, data.route);
                                }
                                else
                                {
                                    SendSms.sendsmsto(user.ContactNumber, "Your One Time Password for Avantis login is: " + Convert.ToString(OTPValue) + ".  " + "Thank you, " + "Avantis Team.", "1207161856353769674");
                                }
                            }
                            CheckResendOTPCount();
                            txtOTP.Text = string.Empty;
                            DisplayOTPMessage();
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "AlertMessage", "settracknewResend();", true);
                            cvLogin.ErrorMessage = "OTP Sent Successfully. You are left with " + ResendOTPAttemp + " more attempts.";
                            cvLogin.IsValid = false;

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void CheckResendOTPCount()
        {
            try
            {
                int OTPValue = 0;
                int UserID = 0;
                if (!string.IsNullOrEmpty(Convert.ToString(Session["userID"])))
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(Session["ResendOTP"])))
                    {
                        UserID = int.Parse(Session["userID"].ToString());
                        OTPValue = int.Parse(Session["ResendOTP"].ToString());
                        var CheckOTPCount = VerifyOTPManagement.GetResendOTPCount(UserID, OTPValue);
                        if (CheckOTPCount.Count() > 3)
                        {
                            lnkResendOTP.Visible = false;
                        }
                        ResendOTPAttemp = 4 - CheckOTPCount.Count();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void RLCSProcessAuthenticationInformation(RLCS_User_Mapping user, string ipaddress, string Macaddress)
        {
            try
            {
                int cid = UserManagement.GetCustomerIDRLCS(user.CustomerID);
                if (cid != -1)
                {
                    string name = string.Format("{0} {1}", user.FirstName, user.LastName);
                    string role = user.AVACOM_UserRole;
                    int checkInternalapplicable = 2;
                    int checkTaskapplicable = 2;
                    int checkVerticalapplicable = 2;
                    int checkLabelApplicable = 2;
                    bool IsPaymentCustomer = false;

                    int complianceProdType = 1;

                    Business.Data.Customer c = CustomerManagement.GetByID(cid);

                    if (c != null)
                    {
                        var IsInternalComplianceApplicable = c.IComplianceApplicable;
                        if (IsInternalComplianceApplicable != null && IsInternalComplianceApplicable != -1)
                        {
                            checkInternalapplicable = Convert.ToInt32(IsInternalComplianceApplicable);
                        }

                        var IsTaskApplicable = c.TaskApplicable;
                        if (IsTaskApplicable != null && IsTaskApplicable != -1)
                        {
                            checkTaskapplicable = Convert.ToInt32(IsTaskApplicable);
                        }

                        var IsVerticlApplicable = c.VerticalApplicable;
                        if (IsVerticlApplicable != null && IsVerticlApplicable != -1)
                        {
                            checkVerticalapplicable = Convert.ToInt32(IsVerticlApplicable);
                        }

                        if (c.IsPayment != null)
                        {
                            IsPaymentCustomer = Convert.ToBoolean(c.IsPayment);
                        }

                        var IsLabelApplicable = c.IsLabelApplicable;
                        if (IsLabelApplicable != -1)
                        {
                            checkLabelApplicable = Convert.ToInt32(IsLabelApplicable);
                        }

                        if (c.ComplianceProductType != null && c.ServiceProviderID != 94)
                        {
                            complianceProdType = Convert.ToInt32(c.ComplianceProductType);
                        }
                    }

                    if (complianceProdType == 1)
                    {

                        if (role.Equals("SADMN"))
                        {
                            FormsAuthentication.RedirectFromLoginPage(user.AVACOM_UserID.ToString(), Convert.ToBoolean(Session["RM"]));
                            FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12}", user.AVACOM_UserID, role, name, checkInternalapplicable, 'C', cid, checkTaskapplicable, user.AVACOM_UserID, string.Empty, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType), false);
                            TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
                            Response.Redirect("~/Users/UserSummary.aspx", false);
                        }
                        else if (role.Equals("IMPT"))
                        {
                            FormsAuthentication.RedirectFromLoginPage(user.AVACOM_UserID.ToString(), Convert.ToBoolean(Session["RM"]));
                            FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12}", user.AVACOM_UserID, role, name, checkInternalapplicable, 'C', cid, checkTaskapplicable, user.AVACOM_UserID, string.Empty, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType), false);
                            TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
                            Response.Redirect("~/Common/CompanyStructure.aspx", false);
                        }
                        else
                        {
                            ProductMappingStructure _obj = new ProductMappingStructure();
                            var ProductMappingDetails = UserManagement.GetByProductIDList(Convert.ToInt32(cid));
                            if (ProductMappingDetails.Count >= 1)
                            {
                                if (ProductMappingDetails.Contains(1))
                                {
                                    _obj.FormsAuthenticationRedirect_RLCSCompliance(cid, user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                                }
                            }
                            else
                            {
                                FormsAuthentication.RedirectFromLoginPage(user.AVACOM_UserID.ToString(), Convert.ToBoolean(Session["RM"]));
                                FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12}", user.AVACOM_UserID, role, name, checkInternalapplicable, "C", cid, checkTaskapplicable, string.Empty, string.Empty, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType), false);
                                TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
                                Response.Redirect("~/ProductMapping/ProductMappingStructure.aspx", false);
                            }
                        }

                        Session["ChangePassword"] = false;
                    }
                    else
                    {
                        if (user.AVACOM_UserID != null)
                        {
                            User avacomUserRecord = UserManagement.GetByID(Convert.ToInt32(user.AVACOM_UserID));

                            if (avacomUserRecord != null)
                            {
                                ProceedLogin(avacomUserRecord, ipaddress, Macaddress);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void ProceedLogin(User user, string ipaddress, string Macaddress1)
        {
            try
            {
                if (user != null)
                {
                    Tuple<bool, List<SP_CheckValidUserForIPAddress_Result>> blockip = null;
                    bool Success = true;
                    try
                    {
                        if (user.CustomerID != null)
                        {
                            blockip = UserManagement.GetBlockIpAddress((int)user.CustomerID, user.ID, ipaddress);
                            if (!blockip.Item1)
                            {
                                cvLogin.IsValid = false;
                                cvLogin.ErrorMessage = "Your Account is Disabled. Please Contact to Admin.";
                                return;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }

                    bool Auditorexpired = false;
                    if (user.RoleID == 9)
                    {
                        if (user.Enddate == null)
                        {
                            Auditorexpired = true;
                        }
                        if (DateTime.Now.Date > Convert.ToDateTime(user.Enddate).Date || DateTime.Now < Convert.ToDateTime(user.Startdate).Date)
                        {
                            Auditorexpired = true;
                        }
                    }
                    if (!Auditorexpired)
                    {
                        if (!(user.WrongAttempt >= 3)) /*UserManagement.WrongAttemptCount(txtemail.Text.Trim())*/
                        {
                            if (user.IsActive)
                            {
                                Session["userID"] = user.ID;
                                Session["ContactNo"] = user.ContactNumber;
                                Session["Email"] = user.Email;
                                Session["CustomerID_new"] = user.CustomerID;
                                if (user.RoleID == 9)
                                {
                                    Session["Auditstartdate"] = user.AuditStartPeriod;
                                    Session["Auditenddate"] = user.AuditEndPeriod;
                                }
                                DateTime LastPasswordChangedDate = Convert.ToDateTime(user.ChangPasswordDate); /*UserManagement.GetByID(Convert.ToInt32(user.ID)*/
                                DateTime currentDate = DateTime.Now;
                                LastPasswordChangedDate = LastPasswordChangedDate != null ? LastPasswordChangedDate : DateTime.Now;
                                int noDays = 0;
                                if (blockip != null && blockip.Item2.Count > 0)
                                {
                                    var data = blockip.Item2.Where(a => a.PasswordExpiry != null).ToList();
                                    if (data.Count > 0)
                                    {
                                        noDays = blockip.Item2.Select(entry => (int)entry.PasswordExpiry).FirstOrDefault();
                                    }
                                    else
                                    {
                                        noDays = Convert.ToInt32(ConfigurationManager.AppSettings["ChangedPasswordDays"]);
                                    }
                                }
                                else
                                {
                                    noDays = Convert.ToInt32(ConfigurationManager.AppSettings["ChangedPasswordDays"]);
                                }
                                int customerID = 0;
                                customerID = Convert.ToInt32(user.CustomerID);
                                if (customerID == 6 || customerID == 5 || customerID == 14)
                                {
                                    noDays = 90;
                                }
                                int dateDifference = Convert.ToInt32((currentDate - LastPasswordChangedDate).TotalDays);
                                if (dateDifference >= noDays)
                                {
                                    Session["ChangePassword"] = true;
                                    Response.Redirect("~/Account/ChangePassword.aspx", false);
                                }
                                else if (user.LastLoginTime != null)
                                {
                                    //Generate Random Number 6 Digit For OTP.
                                    Random random = new Random();
                                    int value = random.Next(1000000);
                                    if (value == 0)
                                    {
                                        value = random.Next(1000000);
                                    }
                                    Session["ResendOTP"] = Convert.ToString(value);
                                    long Contact;
                                    VerifyOTP OTPData = new VerifyOTP()
                                    {
                                        UserId = Convert.ToInt32(user.ID),
                                        EmailId = user.Email,
                                        OTP = Convert.ToInt32(value),
                                        CreatedOn = DateTime.Now,
                                        IsVerified = false,
                                        IPAddress = ipaddress
                                    };
                                    bool OTPresult = false;
                                    try
                                    {
                                        OTPresult = long.TryParse(user.ContactNumber, out Contact);
                                        if (OTPresult)
                                        {
                                            OTPData.MobileNo = Contact;
                                        }
                                        else
                                        {
                                            OTPData.MobileNo = 0;
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        OTPData.MobileNo = 0;
                                        OTPresult = false;
                                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                    }
                                    VerifyOTPManagement.Create(OTPData); // Insert Data in OTP Table.                                                   
                                    string SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"].ToString();
                                    string ReplyEmailAddressName = ConfigurationManager.AppSettings["ReplyEmailAddressName"].ToString();
                                    
                                    try
                                    {
                                        if (ConfigurationManager.AppSettings["SkipOTPQA"].ToString() != "1")
                                        {
                                            //Send Email on User Mail Id.
                                            if (user.CustomerID != 5)
                                            {
                                                EmailManager.SendMail(SenderEmailAddress, new List<String>(new String[] { user.Email }), null, null, "OTP Verification", "Your One Time Password for Teamlease login is:" + Convert.ToString(value) + "<br>" + "<br>" + "Thank you," + "<br>" + "Team Teamlease");
                                                //SendGridEmailManager.SendGridMail(SenderEmailAddress, ReplyEmailAddressName, new List<String>(new String[] { user.Email }), null, null, "OTP Verification", "Your One Time Password for Teamlease Regtech login is:" + Convert.ToString(value) + "<br>" + "<br>" + "Thank you," + "<br>" + "Team Teamlease Regtech");
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        Success = false;
                                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                    }


                                    if (OTPresult && user.RoleID != 12 && user.ContactNumber.Length == 10 && (user.ContactNumber.StartsWith("9") || user.ContactNumber.StartsWith("8") || user.ContactNumber.StartsWith("7") || user.ContactNumber.StartsWith("6")))
                                    {
                                        try
                                        {
                                            if (ConfigurationManager.AppSettings["SkipOTPQA"].ToString() != "1")
                                            {
                                                //Send SMS on User Mobile No.
                                                var data = VerifyOTPManagement.GetSMSConfiguration("Avacom");
                                                if (data != null)
                                                {
                                                    SendSms.sendsmsto(user.ContactNumber, "Your One Time Password for Avantis login is: " + Convert.ToString(value) + ".  " + "Thank you, " + "Avantis Team.", data.TEMPLATEID_DLT_TE_ID, data.authkey, data.Header_sender, data.route);
                                                }
                                                else
                                                {
                                                    SendSms.sendsmsto(user.ContactNumber, "Your One Time Password for Avantis login is: " + Convert.ToString(value) + ".  " + "Thank you, " + "Avantis Team.", "1207161856353769674");
                                                }

                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            Success = false;
                                            LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                        }
                                    }
                                    #region check
                                    //MaintainLoginDetail objData = new MaintainLoginDetail()
                                    //{
                                    //    UserId = Convert.ToInt32(user.ID),
                                    //    Email = txtemail.Text,
                                    //    CreatedOn = DateTime.Now,
                                    //    IPAddress = ipaddress,
                                    //    MACAddress = ipaddress,
                                    //    LoginFrom = "WC",
                                    //    //ProfileID=user.ID
                                    //};
                                    //UserManagement.Create(objData);
                                    //if (Success)
                                    //{
                                    //    Session["RM"] = false;
                                    //    Session["EA"] = Util.CalculateAESHash(txtemail.Text.Trim());
                                    //    Session["MA"] = Util.CalculateAESHash(ipaddress.Trim());
                                    //    Response.Redirect("~/Users/OTPVerify.aspx", false);
                                    //    ScriptManager.RegisterStartupScript(this, Page.GetType(), "SignIn", "settracknewForSuccess();", true);
                                    //}
                                    //else
                                    //{
                                    //    if (UserManagement.HasUserSecurityQuestion(user.ID))
                                    //    {
                                    //        Session["RM"] = false;
                                    //        Session["EA"] = Util.CalculateAESHash(txtemail.Text.Trim());
                                    //        Session["MA"] = Util.CalculateAESHash(ipaddress.Trim());
                                    //        Response.Redirect("~/SecurityQuestion/AddSecurityQuestions.aspx", false);
                                    //    }
                                    //    else
                                    //    {
                                    //        Session["QuestionBank"] = true;
                                    //        Response.Redirect("~/SecurityQuestion/AddSecurityQuestions.aspx", false);
                                    //    }
                                    //}
                                    #endregion
                                }
                                else
                                {
                                    Response.Redirect("~/Account/ChangePassword.aspx", false);
                                }
                            }
                            else
                            {
                                cvLogin.IsValid = false;
                                cvLogin.ErrorMessage = "Your Account is Disabled.";
                            }
                        }
                        else
                        {
                            //Session["otpvrifyWattp"] = true;
                            //ScriptManager.RegisterStartupScript(this, Page.GetType(), "SignIn", "settracknewForFailed();", true);
                            ////sandesh
                            //var plainBytes = Encoding.UTF8.GetBytes(txtemail.Text.Trim());
                            //string EncryptEmail = Convert.ToBase64String(DataEncryption.Encrypt(plainBytes, DataEncryption.GetRijndaelManaged(key)));
                            //userlogout u1 = new userlogout() { email = EncryptEmail };
                            //JavaScriptSerializer js = new JavaScriptSerializer();
                            //var EncryptEmailbody = js.Serialize(u1);
                            //string requestUrl = "https://api.teamleaseregtech.com/api/v2/logout/";
                            //ServicePointManager.Expect100Continue = true;
                            //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                            //string responseData = WebAPIUtility.Invoke("POST", requestUrl, EncryptEmailbody.ToString());
                        }
                    }
                    else
                    {
                        cvLogin.IsValid = false;
                        cvLogin.ErrorMessage = "Your Account is Disabled.";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}