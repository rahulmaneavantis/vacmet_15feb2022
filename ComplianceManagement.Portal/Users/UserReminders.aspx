﻿<%@ Page Title="My Reminders" Language="C#" MasterPageFile="~/NewCompliance.Master" AutoEventWireup="true" EnableEventValidation="false"
    CodeBehind="UserReminders.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Users.UserReminders" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   
<style type="text/css">
    #progressbar .ui-progressbar-value
    {
        background-color: #ccc;
    }
</style>
<script type="text/javascript">

    $(document).on("click", "#ContentPlaceHolder1_upUserReminderList", function (event) {

        if (event.target.id == "") {
            var idvid = $(event.target).closest('div');
            if ($(idvid).attr('id') != undefined && $(idvid).attr('id') != '' && $(idvid).attr('id').indexOf('tvFilterLocation') > -1) {
                $("#divFilterLocation").show();
            } else {
                $("#divFilterLocation").hide();
            }
        }
        else if (event.target.id != "ContentPlaceHolder1_tbxFilterLocation") {
            $("#divFilterLocation").hide();
        } else if (event.target.id != "" && event.target.id.indexOf('tvFilterLocation') > -1) {
            $("#divFilterLocation").show();
        } else if (event.target.id == "ContentPlaceHolder1_tbxFilterLocation") {
            $("#ContentPlaceHolder1_tbxFilterLocation").unbind('click');

            $("#ContentPlaceHolder1_tbxFilterLocation").click(function () {
                $("#divFilterLocation").toggle("blind", null, 500, function () { });
            });

        }
    });
    function openModal() {
        $('#Newaddremider').modal('show');
        return true;
    }
    function CloseWin() {
        $('#Newaddremider').modal('hide');
    };
    function caller() { 
        setInterval(CloseWin, 3000);
    };
    function initializeJQueryUI(textBoxID, divID) {
        $("#" + textBoxID).unbind('click');

        $("#" + textBoxID).click(function () {
            $("#" + divID).toggle("blind", null, 500, function () { });
        });
    }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server"> 
    <asp:UpdatePanel ID="upUserReminderList" runat="server" UpdateMode="Conditional" OnLoad="upUserReminderList_Load">
        <ContentTemplate>
           <%-- <asp:UpdateProgress ID="updateProgress" runat="server">
                <ProgressTemplate>
                    <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0;
                        right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.3;">
                        <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                            AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px;
                            position: fixed; top: 45%; left: 50%;" />
                    </div>
                </ProgressTemplate> 
            </asp:UpdateProgress>--%>

            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel">
                        <div class="clearfix"></div>
                            <div class="col-md-2 colpadding0 entrycount" style="width:11%">
                                <div class="col-md-3 colpadding0" >
                                    <p style="color: #999; margin-top: 5px;">Show </p>
                                </div>
                                <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 60px; float: left;margin-left: 11px;" 
                                AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                                <asp:ListItem Text="5" Selected="True"/>
                                <asp:ListItem Text="10" />
                                <asp:ListItem Text="20" />
                                <asp:ListItem Text="50" />
                                </asp:DropDownList>                                   
                           </div>
                        <div class="col-md-9 colpadding0" style="text-align: right; float: left;width:89%">
                           <div class="col-md-8 colpadding0" style="width: 58%;">  
                              <div style="float:left;margin-right: 1%;">
                                <asp:DropDownList runat="server" ID="ddlDocType" class="form-control m-bot15 search-select" style="width:105px;">                  
                                <asp:ListItem Text="Statutory" Value="-1" />
                                <asp:ListItem Text="Internal" Value="0" />
                                </asp:DropDownList>         
                               </div>

                               <div style="float:left;margin-right: 1%;">
                                <asp:DropDownList runat="server" ID="ddlRiskType" class="form-control m-bot15 search-select"  style="width:76px;" >               
                                <asp:ListItem Text="Risk" Value="-1" />
                                     <asp:ListItem Text="Critical" Value="3" />  
                                <asp:ListItem Text="High" Value="0" />
                                <asp:ListItem Text="Medium" Value="1" />
                                <asp:ListItem Text="Low" Value="2" />                               
                                </asp:DropDownList>
                                </div>

                               <div style="float:left;margin-right: 1%;">                               
                                 <asp:TextBox runat="server" AutoCompleteType="None" ID="tbxFilterLocation" Style="padding: 0px;padding-left: 10px; margin: 0px; height: 35px; width: 314px; border: 1px solid #c7c7cc;border-radius: 4px;color:#8e8e93"
                                    CssClass="txtbox" />                                                       
                                <div style="margin-left: 1px; position: absolute; z-index: 10;display: inherit;" id="divFilterLocation">
                                    <asp:TreeView runat="server" ID="tvFilterLocation"   SelectedNodeStyle-Font-Bold="true"  Width="314px"   NodeStyle-ForeColor="#8e8e93"
                                    Style="overflow: auto; border-left:1px solid #c7c7cc; border-right:1px solid #c7c7cc; border-bottom:1px solid #c7c7cc; background-color: #ffffff; color:#8e8e93 !important;" ShowLines="true" 
                                        OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged">
                                    </asp:TreeView>
                                </div> 
                                </div>            

                                <%--<asp:DropDownList runat="server" ID="ddlLocation" class="form-control m-bot15 select_location"  style="width:110px;" >
                                </asp:DropDownList>--%>

                              
                                </div>
                            
                            <div class="col-md-4 colpadding0" style="width:42%">
                                    <div class="col-md-6 colpadding0">
                                        <asp:TextBox runat="server" placeholder="Type to Filter" class="form-control" Width="150px" ID="tbxFilter" MaxLength="50" AutoPostBack="true" OnTextChanged="tbxFilter_TextChanged" />
                           
                                    </div>
                                 <div class="col-md-6 colpadding0" style="float:left;margin-right: 1%;width:17%;">
                                <asp:Button ID="btnSearch" CausesValidation="false" class="btn btn-search"  runat="server" Text="Apply" OnClick="btnSearch_Click" />
                               </div>
                                    <div class="col-md-4 colpadding0" style="margin-right: 0px;float: right;width:20%;">
                                        <asp:Button runat="server"  OnClientClick="return openModal()" ID="btnAddNew" OnClick="btnAddUserReminder_Click" Text="Add New" class="btn btn-advanceSearch" />  
                                    </div>
                            </div>                        
                            </div>                            
                    </div>
                            
                        <div class="col-md-12 AdvanceSearchScrum">              
                            <div runat="server" id="DivRecordsScrum" style="float: right;">
                                 <p style="padding-right: 0px !Important;">
                                    <asp:Label ID="Label4" runat="server" Text="Showing "></asp:Label>
                                    <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>- 
                                <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                                <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                                </p>
                            </div>
                        </div>   
                                                                                         
                        <div style="margin-bottom: 4px">
                            <asp:GridView runat="server" ID="grdUserReminder" AutoGenerateColumns="false" GridLines="none" CssClass="table" AllowSorting="true" BorderWidth="0px"
                            OnRowCreated="grdUserReminder_RowCreated" OnSorting="grdUserReminder_Sorting" OnRowCommand="grdUserReminder_RowCommand"
                            AllowPaging="True" PageSize="5" Width="100%"  DataKeyNames="ComplianceReminderID" OnPageIndexChanging="grdUserReminder_PageIndexChanging">
                                <Columns>                  
                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Sr">
                                        <ItemTemplate>
                                        <%#Container.DataItemIndex+1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>    
                                    <asp:TemplateField HeaderText ="Description" ItemStyle-Width="300px">
                                        <ItemTemplate>
                                            <div style="overflow:hidden;text-overflow:ellipsis;white-space:nowrap;width:300px;">
                                                <asp:Label  runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# ddlDocType.SelectedValue=="0"? Eval("IShortDescription"):Eval("ShortDescription") %>' ToolTip='<%# Eval("Description") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText ="Location">
                                        <ItemTemplate>
                                            <div style="overflow:hidden;text-overflow:ellipsis;white-space:nowrap;width:150px;">
                                                <asp:Label  runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("CustomerBranchName") %>' ToolTip='<%# Eval("CustomerBranchName") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:BoundField DataField="Role" HeaderText="Role"  ItemStyle-Height="20px"  HeaderStyle-Height="20px" />

                                    <asp:TemplateField HeaderText="Due Date">
                                        <ItemTemplate>
                                             <%# ((DateTime)Eval("ScheduledOn")).ToString("dd-MMM-yyyy")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Reminder On">
                                        <ItemTemplate>
                                            <%# ((DateTime)Eval("RemindOn")).ToString("dd-MMM-yyyy")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Status">
                                        <ItemTemplate>
                                                <%# ((com.VirtuosoITech.ComplianceManagement.Business.Data.ReminderStatus)Convert.ToByte(Eval("Status"))).ToString()%>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField ItemStyle-Width="60px" Visible="false">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkButton1" runat="server" CommandName="EDIT_USER_REMINDER" CommandArgument='<%# Eval("ComplianceReminderID") %>'><img src="../Images/edit_icon.png" alt="Edit Reminder" title="Edit Reminder" /></asp:LinkButton>
                                            <asp:LinkButton ID="LinkButton2" runat="server" CommandName="DELETE_USER_REMINDER" CommandArgument='<%# Eval("ComplianceReminderID") %>'
                                            OnClientClick="return confirm('Are you certain you want to delete this reminder?');"><img src="../Images/delete_icon.png" alt="Delete Reminder" title="Delete Reminder" /></asp:LinkButton>
                                        </ItemTemplate>                                                           
                                    </asp:TemplateField>
                                </Columns>
                                <RowStyle CssClass="clsROWgrid"   />
                                <HeaderStyle CssClass="clsheadergrid"    />
                                <PagerTemplate>
                                <table style="display: none">
                                    <tr>
                                        <td>
                                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                        </td>
                                    </tr>
                                </table>
                                </PagerTemplate>
                                <EmptyDataTemplate>
                                    No Records Found.
                                </EmptyDataTemplate>
                              </asp:GridView>
                        </div>

                        <div class="col-md-12 colpadding0">
                        <div class="col-md-6 colpadding0"></div>                                                        
                            <div class="col-md-6 colpadding0">
                            <div class="table-paging" style="margin-bottom: 20px;">
                            <asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick ="Previous_Click" /> <%----%>
                                  
                            <div class="table-paging-text">
                            <p>
                            <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                            <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>
                            </p>
                            </div>
                            <asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick ="Next_Click"/>   <%--  --%>           
                            <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                            </div>
                            </div>
                        </div>
                        </section>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div class="modal fade" id="Newaddremider" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 650px">

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>

                <div class="modal-body">
                    <asp:UpdatePanel ID="upUserReminders" runat="server" UpdateMode="Conditional" OnLoad="upUserReminders_Load">
                        <ContentTemplate>
                            <div style="margin: 5px">
                                <div style="margin-bottom: 4px">
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" class="alert alert-block alert-danger fade in" ValidationGroup="UserReminderValidationGroup" />
                                    <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                        ValidationGroup="UserReminderValidationGroup" Display="None" />
                                </div>

                                <div style="margin-bottom: 7px; margin-left:20px;">
                                    <label style="width: 10px; display: block; float: left; font-size: 14px; color: red;">*</label>
                                    <label style="width: 200px; display: block; float: left; font-size: 14px; color:#666;">
                                        Compliance Type</label>
                                    <asp:DropDownList runat="server" ID="ddlComType" Style="padding: 0px; margin: 0px; height: 28px; width: 300px; border-radius: 5px; border-color: #c7c7cc;color: #666;"
                                        CssClass="txtbox"  AutoPostBack="true" OnSelectedIndexChanged="ddlComType_SelectedIndexChanged">
                                        <asp:ListItem Text="Statutory" Value="-1" />
                                        <asp:ListItem Text="Internal" Value="0" />
                                    </asp:DropDownList>
                                </div>

                                <div style="margin-bottom: 7px; margin-left:20px;">
                                    <label style="width: 10px; display: block; float: left; font-size: 14px; color: red;">*</label>
                                    <label style="width: 200px; display: block; float: left; font-size: 14px; color:#666;">
                                        Location</label>

                                    <asp:DropDownList runat="server" ID="ddlBranch" Style="padding: 0px; margin: 0px; height: 28px; width: 300px; border-radius: 5px; border-color: #c7c7cc;color: #666;"
                                        CssClass="txtbox" DataValueField="ID" DataTextField="Name"
                                        AutoPostBack="true" OnSelectedIndexChanged="ddlBranch_SelectedIndexChanged" />
                                    <asp:CompareValidator ErrorMessage="Please select Location." ControlToValidate="ddlBranch"
                                        runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="UserReminderValidationGroup"
                                        Display="None" />
                                </div>

                                <div style="margin-bottom: 7px; margin-left:20px;">
                                    <label style="width: 10px; display: block; float: left; font-size: 14px; color: red;">*</label>
                                    <label style="width: 200px; display: block; float: left; font-size: 14px; color:#666;">
                                        Compliance</label>
                                    <asp:DropDownList runat="server" ID="ddlCompliance" Style="padding: 0px; margin: 0px; height: 28px; width: 300px; border-radius: 5px; border-color: #c7c7cc;color: #666;"
                                        CssClass="txtbox" DataValueField="ID" DataTextField="Name"
                                        AutoPostBack="true" OnSelectedIndexChanged="ddlCompliance_SelectedIndexChanged" />
                                    <asp:CompareValidator ID="CompareValidator1" ErrorMessage="Please select Compliance."
                                        ControlToValidate="ddlCompliance" runat="server" ValueToCompare="-1" Operator="NotEqual"
                                        ValidationGroup="UserReminderValidationGroup" Display="None" />
                                </div>

                                <div style="margin-bottom: 7px; margin-left:20px;">
                                    <label style="width: 10px; display: block; float: left; font-size: 14px; color: red;">*</label>
                                    <label style="width: 200px; display: block; float: left; font-size: 14px; color:#666;">
                                        Role</label>
                                    <asp:DropDownList runat="server" ID="ddlRole" Style="padding: 0px; margin: 0px; height: 28px; width: 300px; border-radius: 5px; border-color: #c7c7cc;color: #666;"
                                        CssClass="txtbox" DataValueField="ID" DataTextField="Name" />
                                    <asp:CompareValidator ID="CompareValidator2" ErrorMessage="Please select Role." ControlToValidate="ddlRole"
                                        runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="UserReminderValidationGroup"
                                        Display="None" />
                                </div>

                                <div style="margin-bottom: 7px; height: 20px; color:#666; margin-left:20px;">
                                    <label style="width: 10px; display: block; float: left; font-size: 14px; color: red;">&nbsp;</label>
                                    <label style="width: 200px; display: block; float: left; font-size: 14px; color:#666;">
                                        Due Date</label>
                                    <asp:Literal ID="litScheduledOn" runat="server" />
                                </div>

                                <div style="margin-bottom: 7px; clear: both; margin-left:20px;">
                                    <label style="width: 10px; display: block; float: left; font-size: 14px; color: red;">*</label>
                                    <label style="width: 200px; display: block; float: left; font-size: 14px; color:#666;">
                                        Remind Me On</label>
                                    <asp:TextBox runat="server" ID="tbxDate" Style="padding: 0px; margin: 0px; height: 28px; width: 150px; border-radius: 3px; border-color: #c7c7cc;color: #666; " />
                                    <asp:RequiredFieldValidator ErrorMessage="Please select Remind On." ControlToValidate="tbxDate"
                                        runat="server" ValidationGroup="UserReminderValidationGroup" Display="None" />
                                </div>

                                <div style="margin-bottom: 30px; margin-left: 200px;height: 30px; margin-top: 15px;margin-bottom: 10px;">
                                    <div style="float: left">
                                        <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="btn-search" Style="border: 0px; width: 70px; height: 30px; border-radius: 3px;"
                                            ValidationGroup="UserReminderValidationGroup" />
                                    </div>
                                    <div style="float: left; margin-left: 5px;">
                                    <%--    <button type="button" class="btn-search" data-dismiss="modal" style="border: 0px; width: 100px; height: 25px;">Close</button>--%>
                                        <asp:Button Text="Close" runat="server" ID="btnClose" CssClass="btn-search" Style="border: 0px; width: 70px;  height: 30px; border-radius: 3px;" OnClick="btnClose_Click" />
                                    </div>
                                </div>
                            </div>                            
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
         $(function () {
             $('#divUserRemindersDialog').dialog({
                 height: 450,
                 width: 600,
                 autoOpen: false,
                 draggable: true,
                 title: "User Reminder",
                 open: function (type, data) {
                     $(this).parent().appendTo("form");
                 }
             });
         });


         function initializeCombobox() {
             $("#<%= ddlBranch.ClientID %>").combobox();
            $("#<%= ddlCompliance.ClientID %>").combobox();
            $("#<%= ddlRole.ClientID %>").combobox();
        }

        function initializeDatePicker(date) {
            var startDate = new Date();
            $('#<%= tbxDate.ClientID %>').datepicker({
                dateFormat: 'dd-mm-yy',
                numberOfMonths: 1,
                defaultDate: startDate,
                minDate: startDate
            });

            if (date != null) {
                $("#<%= tbxDate.ClientID %>").datepicker("option", "defaultDate", date);
            }
        }
       
        $(document).ready(function () {
            setactivemenu('leftremindersmenu');
            fhead('My Reminders');
        });
        function instructionsDashboard() {
            var enjoyhint_instance = new EnjoyHint({});

            //simple config. 
            //Only one step - highlighting(with description) "New" button 
            //hide EnjoyHint after a click on the button.
            var enjoyhint_script_steps = [
                {
                    "next #ContentPlaceHolder1_ddlDocType": "Select type of compliance internal/Statutory",
                    showSkip: true,
                }, {
                    "next #ContentPlaceHolder1_tbxFilterLocation": "Select location to see filtered data by location",
                    showSkip: true,
                }, {
                    "next #ContentPlaceHolder1_tbxFilter": "type keyword to find compliance(s)",
                    showSkip: true,
                }, {
                    "next #ContentPlaceHolder1_ddlPageSize": "Change no. of tasks/compliance in the table",
                    showSkip: true,
                }
                , {
                    "next #ContentPlaceHolder1_btnAddNew": "Click to add new reminder",
                    showSkip: true,
                }
            ];


            //set script config
            enjoyhint_instance.set(enjoyhint_script_steps);

            //run Enjoyhint script
            enjoyhint_instance.run();
        }

    </script>
</asp:Content>
