﻿<%@ Page Title="Escalation Reminders" Language="C#" MasterPageFile="~/NewCompliance.Master" AutoEventWireup="true" CodeBehind="EscalationReminders.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.ComplianceDocument.EscalationReminders" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">                               
        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }
        // For Enter Only Number Only.
        function validatenumerics(key) {
            //getting key code of pressed key
            var keycode = (key.which) ? key.which : key.keyCode;
            //comparing pressed keycodes
            if (keycode > 31 && (keycode < 48 || keycode > 57) && keycode != 46) {
                return false;
            }
            else return true;
        }
        function ValidateDaysAndIntermDays(obj) {
            var esc = $(obj).parent('td').parent('tr').find('.Esclare');
            var escI = $(obj).parent('td').parent('tr').find('.EsclareI');
            if ($(escI).val() == "" || $(esc).val() == "") {
                alert("Please enter value of days and interimdays.!");
                return false;
            }
            else {
                return true;
            }
        }

    </script>
    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    </style>
    <style type="text/css">
        .ui-widget-header {
            border: 0px !important;
            background: inherit;
            font-size: 20px;
            color: #666666;
            font-weight: normal;
            padding-top: 0px;
            margin-top: 5px;
        }

        .table > tbody > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

            .table > tbody > tr > th > a {
                vertical-align: bottom;
                color: #666666 !important;
                text-decoration: none !important;
                border-bottom: 2px solid #dddddd;
            }

        .table tr th {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .clsheadergrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: bottom !important;
            border-bottom: 2px solid #dddddd !important;
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
            text-align: left;
        }

        .clsROWgrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: top !important;
            border-top: 1px solid #dddddd !important;
            color: #666666 !important;
            font-size: 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upDocumentDownload" runat="server" UpdateMode="Conditional" OnLoad="upComplianceDetails_Load">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <div style="margin-bottom: 4px">
                            <asp:CustomValidator ID="cvDuplicateEntry" CssClass="alert alert-block alert-danger fade in" runat="server" EnableClientScript="False"
                                ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                            <asp:Label ID="lblMsg1" CssClass="alert alert-block alert-danger fade in" runat="server" Visible="false"></asp:Label>
                        </div>
                        <section class="panel">
                           <header class="panel-heading tab-bg-primary ">
                                <ul id="rblRole1" class="nav nav-tabs">                                       
                                <%if (roles.Contains(4))%>
                                <%{%>
                                    <li class=""  id="liReviewer" runat="server">
                                        <asp:LinkButton ID="lnkReviewer" OnClick="ShowReviewer" runat="server">Reviewer</asp:LinkButton>                                        
                                    </li>
                                <%}%>
                                </ul>
                            </header>
                            <div class="clearfix"></div>
                            <div class="panel-body">
                            <div class="col-md-12 colpadding0">
                                <div class="col-md-2 colpadding0 entrycount" style="width:11%;">
                                    <div class="col-md-3 colpadding0" >
                                        <p style="color: #999; margin-top: 5px;">Show </p>
                                    </div>
                                    <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 60px;margin-left:10px; float: left" 
                                        AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" >
                                        <asp:ListItem Text="5" Selected="True"/>
                                        <asp:ListItem Text="10" />
                                        <asp:ListItem Text="20" />
                                        <asp:ListItem Text="50" />
                                    </asp:DropDownList>      
                                </div>

                                <div class="col-md-10 colpadding0" style="text-align: right; float: left;width: 89%;">
                                    <div class="col-md-8 colpadding0">              
                                        <div style="float:left;margin-right: 1%;">
                                            <asp:DropDownList runat="server" ID="ddlDocType" class="form-control m-bot15 search-select" style="width:105px;" >                
                                            <asp:ListItem Text="Statutory" Value="-1" />            
                                            <asp:ListItem Text="Event Based" Value="1" />
                                            </asp:DropDownList>         
                                        </div>
                                        <div style="float:left;margin-right: 1%;">
                                            <asp:DropDownList runat="server" ID="ddlRiskType" class="form-control m-bot15 search-select"  style="width:80px;" >               
                                            <asp:ListItem Text="Risk" Value="-1" />
                                                <asp:ListItem Text="Critical" Value="3" />  
                                            <asp:ListItem Text="High" Value="0" />
                                            <asp:ListItem Text="Medium" Value="1" />
                                            <asp:ListItem Text="Low" Value="2" />                
                                            </asp:DropDownList>
                                        </div>
                                        <div style="float:left;margin-right: 1%;">       
                                        </div>
                                        <div style="float:left;margin-right: 1%;">
                                            <asp:TextBox runat="server" AutoCompleteType="None" ID="tbxFilterLocation" Style="padding: 0px;padding-left: 10px; margin: 0px; height: 35px; width: 373px; border: 1px solid #c7c7cc;border-radius: 4px;color:#8e8e93"
                                            CssClass="txtbox" />                                                       
                                            <div style="margin-left: 1px; position: absolute; z-index: 10;display: inherit;" id="divFilterLocation">
                                                <asp:TreeView runat="server" ID="tvFilterLocation"   SelectedNodeStyle-Font-Bold="true"  Width="325px"   NodeStyle-ForeColor="#8e8e93"
                                                Style="overflow: auto; border-left:1px solid #c7c7cc; border-right:1px solid #c7c7cc; border-bottom:1px solid #c7c7cc; background-color: #ffffff; color:#8e8e93 !important;" ShowLines="true" 
                                                OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged">
                                            </asp:TreeView>
                                        </div>  
                                        </div>           
                                    </div>
                                    <div class="col-md-4 colpadding0">
                                        <div class="col-md-6 colpadding0" style="margin-left: 10px;width: 41%;">               
                                        <asp:Button ID="btnSearch" CausesValidation="false" class="btn btn-search"  runat="server" Text="Apply" OnClick="btnSearch_Click"/> 
                                        </div>
                                        <div class="col-md-6 colpadding0">             
                                        </div>
                                    </div>
                                </div>  
                             
                               
                                <!-- Advance Search scrum-->
                                 <div class="clearfix"></div>
                            <div class="col-md-12 AdvanceSearchScrum">
                                 
                                    <div runat="server" id="DivRecordsScrum" style="float: right;">
                                        <p style="padding-right: 0px !Important;">
                                            <asp:Label ID="Label4" runat="server" Text="Showing "></asp:Label>
                                            <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>- 
                                            <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                                            <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                                        </p>
                                    </div>
                            </div>
                            </div>
                            </div>
                        <div class="clearfix"></div>                                                                      
                        
                        <div id="ReviewerGrids" runat="server">                               
                            <div style="margin-bottom: 4px">
                                <asp:GridView runat="server" ID="grdReviewerComplianceDocument" AutoGenerateColumns="false" CssClass="table" GridLines="None" BorderWidth="0px"
                                    CellPadding="4" Width="100%" OnRowDataBound="grdReviewerComplianceDocument_RowDataBound"
                                    OnPageIndexChanging="grdReviewerComplianceDocument_PageIndexChanging" AllowPaging="True" PageSize="5" AutoPostBack="true">
                                    <Columns>                                        
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                            <ItemTemplate>
                                                <%#Container.DataItemIndex+1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>     
                                        <asp:TemplateField HeaderText="Act">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 130px;">
                                                <asp:Label ID="lblActName" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ActName") %>' ToolTip='<%# Eval("ActName") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Description">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width:130px; ">
                                                    <asp:Label ID="lblShortDesc" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                                                   <asp:Label ID="lblComplianceId" runat="server" Text='<%# Eval("complianceID") %>' Visible="false"></asp:Label>
                                                      <asp:Label ID="lblUstomerBranchID" runat="server" Text='<%# Eval("CustomerBranchID") %>' Visible="false"></asp:Label>
                                               
                                                     </div>
                                            </ItemTemplate>
                                            <ItemStyle Wrap="false" Width="150" /> 
                                        </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Location">
                                            <ItemTemplate>
                                                 <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width:90px; ">
                                                <asp:Label ID="lblBranch" runat="server"  data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Branch") %>' ToolTip='<%# Eval("Branch") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                          <asp:TemplateField HeaderText="Frequency">
                                            <ItemTemplate>
                                                 <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width:50px; ">
                                                <asp:Label ID="lblFrequency" runat="server"  data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Frequency") %>' ToolTip='<%# Eval("Frequency") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                       <asp:TemplateField HeaderText="Performer">
                                                        <ItemTemplate>
                                                            <div style="overflow: hidden; text-align:center; text-overflow: ellipsis; white-space: nowrap; width: 80px;">
                                                                <asp:Label ID="lblPerformer" runat="server"  data-toggle="tooltip" data-placement="bottom"  Text='<%# GetPerformer((long)Eval("complianceID") ,(int)Eval("CustomerBranchID")) %>' ToolTip='<%#Performername%>'></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                       </asp:TemplateField>

                                        <asp:TemplateField HeaderText="DueDateOfMonth">
                                            <ItemTemplate>                                             
                                                  <div style="overflow: hidden; text-align:center; text-overflow: ellipsis; white-space: nowrap; width: 70px;">
                                                <asp:Label ID="lblScheduledOn" runat="server" Text='<%# Eval("DueDate") %>'></asp:Label>   
                                                      </div>                                        
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                         <asp:TemplateField HeaderText="Work File Timeline">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-align:center; width: 80px;"> 
                                                <asp:TextBox ID="txtInterimDays" Text='<%# Eval("IntreimDays") %>' runat="server" CssClass="form-control EsclareI" style="width:55%"></asp:TextBox>
                                            </div>
                                                    </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Escalation">
                                            <ItemTemplate>
                                                 <div style="overflow: hidden; text-align:center; width: 80px;"> 
                                                <asp:TextBox ID="txtEscDays" Text='<%# Eval("EscDays") %>' runat="server" CssClass="form-control Esclare" style="width:55%"></asp:TextBox>
                                                     </div> 
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:Button ID="btnAdd" Width="72px" ValidationGroup="ComplianceInstanceValidationGroup" Text="Save" runat="server" OnClientClick="return ValidateDaysAndIntermDays(this);" OnClick="btnAdd_Click" CssClass="btn btn-primary" />                                             
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                    <PagerStyle HorizontalAlign="Right" />
                                    <PagerTemplate>
                                        <table style="display: none">
                                            <tr>
                                                <td>
                                                    <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                </td>
                                            </tr>
                                        </table>
                                    </PagerTemplate>
                                    <EmptyDataTemplate>
                                        No Record Found
                                    </EmptyDataTemplate>
                                </asp:GridView>                               
                            </div>                                                     
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12 colpadding0">
                            <div class="col-md-6 colpadding0">
                                <div class="table-Selecteddownload">
                                    <div class="table-Selecteddownload-text">
                                        <p><asp:Label runat="server" ID="lblTotalSelected" Text="" style="color: #999; margin-right: 15px;"></asp:Label></p>
                                    </div>                                    
                                </div>
                            </div>

                            <div class="col-md-6 colpadding0">
                                <div class="table-paging" style="margin-bottom: 20px;">
                                    <asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick ="Previous_Click"/>
                                  
                                    <div class="table-paging-text">
                                        <p>
                                            <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                            <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>
                                        </p>
                                    </div>

                                    <asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick ="Next_Click" />                                   
                                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div style="float:left;width:100%">
                                    <div style="float: left;color: #666666;">
                                        <b>Work File Timeline</b> - This is the day/date on which performer should start sharing/submits the complianace working file to reviewer.
                                    </div>
                                     <div style="float: left;color: #666666;">
                                        <b>Escalation</b> - From this day/date the reviewer will start receiving the mail alerts for compliance.
                                    </div>
                                </div>
                         </section>
                    </div>
                </div>
            </div>
        </ContentTemplate>
       
    </asp:UpdatePanel>
    <script type="text/javascript">
        $(document).ready(function () {
            fhead('My Escalation');
            setactivemenu('leftescalationmenu');
        });
        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }
    </script>
</asp:Content>
