﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Text;
using System.Globalization;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Text.RegularExpressions;
using System.Data;
using System.Configuration;
using System.IO;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Task
{
    public partial class PerformerSubTaskDetails : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    ViewState["SortOrder"] = "Asc";
                    ViewState["SortExpression"] = "ID";

                    ViewState["TaskID"] = Session["TaskID"];
                    ViewState["MainTaskID"] = Session["MainTaskID"];
                    ViewState["ParentID"] = Session["TaskID"];
                    ViewState["DueDays"] = Session["DueDays"];

                    ViewState["TaskType"] = Session["TaskType"];
                    ViewState["CustomerID"] = Session["CustomerID"];
                    
                    //BindLocationFilter();
                    BindUsers(ddlPerformerAssign);
                    BindUsers(ddlReviewerAssign);
                    txtTask.Attributes.Add("readonly", "readonly");

                    long parentID = -1;
                    if (ViewState["ParentID"] != null)
                    {
                        long.TryParse(ViewState["ParentID"].ToString(), out parentID);
                    }

                    BindSubTasks(parentID, "");
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occurred. Please try again.";
                }
            }
        }

        protected void dlBreadcrumb_ItemCommand(object source, DataListCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "ITEM_CLICKED")
                {
                    if (e.Item.ItemIndex == 0)
                    {
                        try
                        {
                            ViewState["ParentID"] = null;
                            Response.Redirect("~/Task/Mytask.aspx?istaskCreation=1", false);
                        }
                        catch (Exception)
                        {
                        }
                    }
                    else
                    {
                        ViewState["ParentID"] = e.CommandArgument.ToString();
                    }
                    long parentID = -1;
                    if (ViewState["ParentID"] != null)
                    {
                        long.TryParse(ViewState["ParentID"].ToString(), out parentID);
                        BindSubTasks(parentID, "");
                        upTaskDetails.Update();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        private void BindSubTasks(long parentID, string filter)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var tasks = (from row in entities.Tasks
                             where row.Isdeleted == false && row.ParentID == parentID
                             && row.CreatedBy == AuthenticationHelper.UserID
                             select new
                             {
                                 row.ID,
                                 row.Title,
                                 row.Description,
                                 row.ParentID,
                             });

                //if (parentID != -1)
                //{
                //    tasks = tasks.Where(entry => entry.ParentID == parentID);
                //}
                //else
                //{
                //    tasks = tasks.Where(entry => entry.ParentID == null);
                //}

                dlBreadcrumb.DataSource = TaskManagment.GetHierarchyTask(Convert.ToInt32(ViewState["TaskID"]), Convert.ToInt32(ViewState["ParentID"]));
                dlBreadcrumb.DataBind();

                grdTask.DataSource = tasks.ToList();
                Session["TotalRows"] = tasks.ToList().Count;
                grdTask.DataBind();
                GetPageDisplaySummary();
                upTaskList.Update();
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection) ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }


        private void BindUsers(DropDownList ddlUserList, List<long> ids = null)
        {
            try
            {
                int customerID = -1;
                //if (AuthenticationHelper.Role == "CADMN")
                //{
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);// UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                //}

                ddlUserList.DataTextField = "Name";
                ddlUserList.DataValueField = "ID";
                ddlUserList.Items.Clear();

                var users = UserManagement.GetAllNVP(customerID, ids: ids, Flags: true);

                ddlUserList.DataSource = users;
                ddlUserList.DataBind();

                ddlUserList.Items.Insert(0, new ListItem("< Select >", "-1"));
            }

            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }
        private void ClearSelection()
        {
            txtTaskTitle.Text = string.Empty;
            txtDescription.Text = string.Empty;
            txtDueDays.Text = string.Empty;
            //ddlPerformer.SelectedValue = "-1";
            //ddlReviewer.SelectedValue = "-1";
        }
        protected void btnAddSubTask_Click(object sender, EventArgs e)
        {
            try
            {
                ClearSelection();
                ViewState["Mode"] = 0;
                lblErrorMassage.Text = string.Empty;
                PopulateInputForm();
                upTaskDetails.Update();
                txtTask.Text = "< Select Task >";
                BindDropDownTasks(Convert.ToInt32(ViewState["MainTaskID"]));
                showSampleFormDetails(0);
                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog1", "$(\"#divTaskDetailsDialog\").dialog('open')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }
        private void BindDropDownTasks(long? MainTaskID, long? TaskID = null)
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            int Userid = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var taskQuery = (from row in entities.Tasks
                                 where row.Isdeleted == false
                                 && row.CustomerID == customerID
                                 && row.MainTaskID == MainTaskID
                                 && row.ID != TaskID
                                 && row.CreatedBy == Userid
                                 select new
                                 {
                                     row.ID,
                                     row.Title,
                                     row.TaskType,
                                 });

                var task = taskQuery.ToList();

                var type = Convert.ToByte(ViewState["TaskType"]);
                if (type == 1)
                {
                    task = task.Where(entry => entry.TaskType == 1).ToList();
                }
                else if (type == 2)
                {
                    task = task.Where(entry => entry.TaskType == 2).ToList();
                }

                if (ViewState["SortOrder"].ToString() == "Asc")
                {
                    if (ViewState["SortExpression"].ToString() == "ID")
                    {
                        task = task.OrderBy(entry => entry.ID).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "Title")
                    {
                        task = task.OrderBy(entry => entry.Title).ToList();
                    }
                    direction = SortDirection.Descending;
                }
                else
                {
                    if (ViewState["SortExpression"].ToString() == "ID")
                    {
                        task = task.OrderByDescending(entry => entry.ID).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "Title")
                    {
                        task = task.OrderByDescending(entry => entry.Title).ToList();
                    }
                    direction = SortDirection.Ascending;
                }

                rptTask.DataSource = task;
                rptTask.DataBind();

                foreach (RepeaterItem aItem in rptTask.Items)
                {
                    CheckBox chkCompliance = (CheckBox)aItem.FindControl("chkTask");
                    chkCompliance.Checked = false;
                    CheckBox chkTaskSelectAll = (CheckBox)rptTask.Controls[0].Controls[0].FindControl("TaskSelectAll");
                    chkTaskSelectAll.Checked = false;
                }

                upTaskList.Update();
            }
        }
        protected void btnTaskRepeater_Click(object sender, EventArgs e)
        {

        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int actualDueDay = Convert.ToInt32(ViewState["DueDays"]) + Convert.ToInt32(txtDueDays.Text);
                bool ComplValidateSuccess = false;
                bool saveSuccess = false;
                byte comFreq = 2;
                var type = Convert.ToByte(ViewState["TaskType"]);
                if (type == 1)
                {
                    comFreq = Convert.ToByte(TaskManagment.SubTaskComplianceFrequency(Convert.ToInt32(ViewState["MainTaskID"])));
                }
                if (type == 2)
                {
                    comFreq = Convert.ToByte(TaskManagment.SubTaskComplianceFrequencyInternal(Convert.ToInt32(ViewState["MainTaskID"])));
                }

                var TaskDetails = Business.TaskManagment.GetByTaskDetails(Convert.ToInt32(ViewState["MainTaskID"]));
                if (TaskDetails != null)
                {
                    if (comFreq != null)
                    {
                        if (comFreq != 0)
                            ComplValidateSuccess = true;
                        else
                        {
                            if (Convert.ToString(TaskDetails.IsAfter) == "False")
                            {
                                if (Convert.ToInt32(txtDueDays.Text) <= 10)
                                {
                                    ComplValidateSuccess = true;
                                }
                                else
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Due Days can not be more than 10 for monthly compliance.";
                                    ComplValidateSuccess = false;
                                    return;
                                }
                            }
                            else
                            {
                                ComplValidateSuccess = true;
                            }
                        }
                    }
                    else
                        ComplValidateSuccess = true;

                    Boolean IsYesNo;
                    if (ChkIsTaskConditional.Checked == true)
                        IsYesNo = true;
                    else
                        IsYesNo = false;

                    Boolean IsYes;
                    Boolean IsNo;
                    if (rbTrueCondtion.SelectedValue == "0")
                    {
                        IsYes = true;
                        IsNo = false;
                    }
                    else
                    {
                        IsYes = false;
                        IsNo = true;
                    }

                    Boolean IsBothMessage;
                    if (rbBothMessage.SelectedValue == "0")
                    {
                        IsBothMessage = true;
                    }
                    else
                    {
                        IsBothMessage = false;
                    }

                    if (ComplValidateSuccess)
                    {
                        Business.Data.Task Task = new Business.Data.Task()
                        {
                            Title = txtTaskTitle.Text.Trim(),
                            Description = txtDescription.Text.Trim(),
                            MainTaskID = Convert.ToInt32(ViewState["MainTaskID"]),
                            TaskType = Convert.ToByte(ViewState["TaskType"]),
                            CustomerID = Convert.ToInt32(ViewState["CustomerID"]),
                            ParentID = ViewState["ParentID"] == null ? (int?) null : Convert.ToInt32(ViewState["ParentID"]),
                            IsActive = true,
                            Isdeleted = false,
                            DueDays = Convert.ToInt32(txtDueDays.Text),
                            ActualDueDays = actualDueDay,
                            CreatedBy = AuthenticationHelper.UserID,
                            CreatedDate = Convert.ToDateTime(DateTime.Now),
                            UpdatedDate = Convert.ToDateTime(DateTime.Now),
                            IsAfter = TaskDetails.IsAfter,
                            TaskTypeID = TaskDetails.TaskTypeID,
                            SubTaskID = TaskDetails.SubTaskID,
                            IsYesNo = IsYesNo,
                            Message = TxtConditionalMessage.Text.Trim(),
                            IsYes = IsYes,
                            IsNo = IsNo,
                            Yesmessage = txtYesMessage.Text.Trim(),
                            Nomessage = txtNoMessage.Text.Trim(),
                            IsBothYesNo =  IsBothMessage,
                        };

                        if ((int) ViewState["Mode"] == 1)
                        {
                            Task.ID = Convert.ToInt32(ViewState["TaskID"]);
                        }

                        if (ViewState["ParentID"] != null)
                        {
                            Task.ParentID = Convert.ToInt32(ViewState["ParentID"]);
                        }

                        if ((int) ViewState["Mode"] == 0)
                        {
                            if (Business.TaskManagment.SubTaskNameExistOrnot(Task))
                            {
                                if (Business.TaskManagment.CreateSubTask(Task))
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Subtask Saved Successfully.";
                                    saveSuccess = true;
                                }
                                else
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Something went wrong, try again.";
                                    saveSuccess = false;
                                }
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "SubTask with same title already exists.";
                                saveSuccess = false;
                            }
                        }
                        else if ((int) ViewState["Mode"] == 1)
                        {
                            if (Business.TaskManagment.SubTaskNameExistOrnot(Task))
                            {
                                Boolean Check = Business.TaskManagment.CheckTaskScheduleOnPresentFromTask(Task.ID);
                                if (Check == false)
                                {
                                    Task.UpdatedBy = AuthenticationHelper.UserID;
                                    if (Business.TaskManagment.UpdateTask(Task))
                                    {
                                        cvDuplicateEntry.IsValid = false;
                                        cvDuplicateEntry.ErrorMessage = "Subask Updated Successfully.";
                                        saveSuccess = true;
                                    }
                                    else
                                    {
                                        cvDuplicateEntry.IsValid = false;
                                        cvDuplicateEntry.ErrorMessage = "Something went wrong, try again.";
                                        saveSuccess = false;
                                    }
                                }
                                else
                                { 
                                    //Mapping Task Detail Update
                                    if (Business.TaskManagment.UpdateMappingTask(Task))
                                    {
                                        cvDuplicateEntry.IsValid = false;
                                        cvDuplicateEntry.ErrorMessage = "Subask Updated Successfully.";
                                        saveSuccess = true;
                                    }
                                    //cvDuplicateEntry.IsValid = false;
                                    //cvDuplicateEntry.ErrorMessage = "Sub task alredy scheduled, you can not update task";
                                }
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "SubTask with same title already exists.";
                                saveSuccess = false;
                            }
                        }

                        if (saveSuccess)
                        {
                            // Task Linking
                            List<int> ParentTaskIds = new List<int>();
                            Business.ComplianceManagement.UpdateTaskMappedID(Convert.ToInt32(Task.ID));
                            foreach (RepeaterItem aItem in rptTask.Items)
                            {
                                CheckBox chkCompliance = (CheckBox)aItem.FindControl("chkTask");
                                if (chkCompliance.Checked)
                                {
                                    ParentTaskIds.Add(Convert.ToInt32(((Label)aItem.FindControl("lblTaskID")).Text.Trim()));
                                    TaskLinking taskLinking = new TaskLinking()
                                    {
                                        TaskLinkID = Convert.ToInt32(((Label)aItem.FindControl("lblTaskID")).Text.Trim()),
                                        TaskID = Convert.ToInt32(Task.ID),
                                        IsActive = true,
                                        CreatedDate = DateTime.Now,
                                        CreatedBy = Convert.ToInt32(Session["userID"]),
                                    };
                                    Business.ComplianceManagement.CreateTaskMapping(taskLinking);
                                }
                            }

                            //Upload Task Sample Form                        
                            if (fuSampleFile.FileBytes != null && fuSampleFile.FileBytes.LongLength > 0)
                            {
                                string directoryPath = string.Empty;
                                string filePath = string.Empty;

                                directoryPath = Server.MapPath("~/TaskFiles/" + Task.ID);

                                if (!Directory.Exists(directoryPath))
                                    Directory.CreateDirectory(directoryPath);

                                filePath = directoryPath + "/" + fuSampleFile.FileName;

                                fuSampleFile.SaveAs(filePath);

                                TaskForm newform = new TaskForm()
                                {
                                    TaskID = Task.ID,
                                    FileName = fuSampleFile.FileName,
                                    FileData = fuSampleFile.FileBytes,
                                    FilePath = "~/TaskFiles/" + Task.ID + "/" + fuSampleFile.FileName,
                                    CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                                    UpdatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                                };

                                bool uploadSuccess = TaskManagment.CreateUpdateTaskForm(newform);

                                if (uploadSuccess)
                                {
                                    //Added
                                    showSampleFormDetails(Task.ID);
                                }
                            }

                        }
                    }
                }
                long parentID = -1;
                if (ViewState["ParentID"] != null)
                {
                    long.TryParse(ViewState["ParentID"].ToString(), out parentID);
                }
                BindSubTasks(parentID, "");
                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "scriptTaskDialogOpen", "fopenpopup()", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        protected void upTaskDetails_Load(object sender, EventArgs e)
        {
            //ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeTask", string.Format("initializeJQueryUI('{0}', 'dvTask');", txtTask.ClientID), true);
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTask", "$(\"#dvTask\").hide(\"blind\", null, 5, function () { });", true);

        }
        private void PopulateInputForm()
        {
            try
            {
                litTask.Text = TaskManagment.GetTaskByID(Convert.ToInt32(ViewState["ParentID"])).Title;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

                SelectedPageNo.Text = "1";

                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo).ToString();
                }

                grdTask.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                grdTask.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                //Reload the Grid
                long parentID = -1;
                if (ViewState["ParentID"] != null)
                {
                    long.TryParse(ViewState["ParentID"].ToString(), out parentID);
                }

                BindSubTasks(parentID, "");

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }
        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        protected void Next_Click(object sender, EventArgs e)
        {
            try
            {
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;

                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo + 1).ToString();
                }
                else
                {

                }

                if (!(StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) > Convert.ToInt32(Session["TotalRows"])))
                    StartRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue);

                EndRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalRows"]))
                    EndRecord = Convert.ToInt32(Session["TotalRows"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";

                grdTask.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                grdTask.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                //Reload the Grid
                long parentID = -1;
                if (ViewState["ParentID"] != null)
                {
                    long.TryParse(ViewState["ParentID"].ToString(), out parentID);
                }

                BindSubTasks(parentID, "");
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void Previous_Click(object sender, EventArgs e)
        {
            try
            {
                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;

                if (Convert.ToInt32(SelectedPageNo.Text) > 1)
                {
                    SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
                }

                StartRecord = StartRecord - Convert.ToInt32(ddlPageSize.SelectedValue);

                if (StartRecord < 1)
                    StartRecord = 1;

                EndRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalRows"]))
                    EndRecord = Convert.ToInt32(Session["TotalRows"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";

                grdTask.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                grdTask.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                //Reload the Grid
                long parentID = -1;
                if (ViewState["ParentID"] != null)
                {
                    long.TryParse(ViewState["ParentID"].ToString(), out parentID);
                }

                BindSubTasks(parentID, "");
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void GetPageDisplaySummary()
        {
            try
            {
                DivRecordsScrum.Visible = true;

                lblTotalRecord.Text = " " + Session["TotalRows"].ToString();

                lTotalCount.Text = GetTotalPagesCount().ToString();

                if (lTotalCount.Text != "0")
                {
                    if (SelectedPageNo.Text == "" || SelectedPageNo.Text == "0" || SelectedPageNo.Text == "1")
                    {
                        SelectedPageNo.Text = "1";
                        lblStartRecord.Text = "1";

                        if (!(Convert.ToInt32(ddlPageSize.SelectedValue) > Convert.ToInt32(Session["TotalRows"].ToString())))
                            lblEndRecord.Text = ddlPageSize.SelectedValue + " ";
                        else
                            lblEndRecord.Text = Session["TotalRows"].ToString() + " ";
                    }
                }
                else if (lTotalCount.Text == "0")
                {
                    SelectedPageNo.Text = "0";
                    DivRecordsScrum.Visible = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void grdTask_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int subTaskID = Convert.ToInt32(e.CommandArgument);

            ViewState["TaskID"] = subTaskID;
            ddlPerformerAssign.ClearSelection();
            ddlReviewerAssign.ClearSelection();
            tbxStartDate.Text = "";
            ClearTreeViewSelection(tvFilterLocation);
            tbxFilterLocationTask.Text = "Select Task Applicable Location";
            if (e.CommandName.Equals("EDIT_SUBTASK"))
            {
                lblErrorMassage.Text = "";
                var subTask = Business.TaskManagment.GetByTaskDetails(subTaskID);

                BindDropDownTasks(Convert.ToInt32(subTask.MainTaskID), subTaskID);

                int? ActualDueDay = 0;
                if (subTask != null)
                {
                    if (subTask.ParentID != null)
                    {
                        ActualDueDay = Business.TaskManagment.GetActualDays(Convert.ToInt32(subTask.ParentID));
                    }
                    else
                    {
                        ActualDueDay = Convert.ToInt32(subTask.DueDays);
                    }
                    ViewState["DueDays"] = ActualDueDay;

                    PopulateInputForm();

                    ViewState["Mode"] = 1;
                    txtTaskTitle.Text = subTask.Title;
                    txtDescription.Text = subTask.Description;
                    txtDueDays.Text = Convert.ToString(subTask.DueDays);

                    TxtConditionalMessage.Text = subTask.Message;
                    txtYesMessage.Text = subTask.Yesmessage;
                    txtNoMessage.Text = subTask.Nomessage;

                    Boolean IsTaskConditional;
                    if (subTask.IsYesNo != null)
                    {
                        if (subTask.IsYesNo == true)
                            IsTaskConditional = true;
                        else
                            IsTaskConditional = false;
                    }
                    else
                    {
                        IsTaskConditional = false;
                    }
                    ChkIsTaskConditional.Checked = IsTaskConditional;

                    if (subTask.IsYes != null && subTask.IsNo != null)
                    {
                        if (subTask.IsYes == true)
                            rbTrueCondtion.SelectedValue = "0";
                        else
                            rbTrueCondtion.SelectedValue = "1";
                    }
                    if (subTask.IsBothYesNo != null && subTask.IsBothYesNo != null)
                    {
                        if (subTask.IsBothYesNo == true)
                            rbBothMessage.SelectedValue = "0";
                        else
                            rbBothMessage.SelectedValue = "1";
                    }
                    txtTask.Text = "< Select >";
                    var vGetCmplianceMappedIDs = Business.ComplianceManagement.GetTaskLinkingID(subTaskID);
                    foreach (RepeaterItem aItem in rptTask.Items)
                    {
                        CheckBox chkCompliance = (CheckBox)aItem.FindControl("chkTask");
                        chkCompliance.Checked = false;
                        CheckBox ComplianceSelectAll = (CheckBox)rptTask.Controls[0].Controls[0].FindControl("TaskSelectAll");

                        for (int i = 0; i <= vGetCmplianceMappedIDs.Count - 1; i++)
                        {
                            if (((Label)aItem.FindControl("lblTaskID")).Text.Trim() == vGetCmplianceMappedIDs[i].ToString())
                            {
                                chkCompliance.Checked = true;
                            }
                        }
                        if ((rptTask.Items.Count) == (vGetCmplianceMappedIDs.Count))
                        {
                            ComplianceSelectAll.Checked = true;
                        }
                        else
                        {
                            ComplianceSelectAll.Checked = false;
                        }
                    }

                    upTaskDetails.Update();
                    //ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "OpenDialog2", "$(\"#divTaskDetailsDialog\").dialog('open')", true);
                }
            }
            else if (e.CommandName.Equals("SHOW_ASSIGNMENT"))
            {
                ViewState["TaskType"] = TaskManagment.GetTaskType(Convert.ToInt32(ViewState["MainTaskID"]));
                var complianceList = TaskManagment.GetTaskMappingComplianceList(Convert.ToInt32(ViewState["MainTaskID"]));

                ClearTreeViewSelection(tvFilterLocation);

                ddlPerformerAssign.SelectedValue = "-1";
                ddlReviewerAssign.SelectedValue = "-1";
                tbxStartDate.Text = string.Empty;

                bool? IsTaskAfter = TaskManagment.GetTaskBeforeAfter(subTaskID);

                if (IsTaskAfter != null)
                {
                    Session["IsTaskAfter"] = IsTaskAfter;
                }
                else
                {
                    Session["IsTaskAfter"] = 0;
                }
                //BindLocationFilter(Convert.ToInt32(ViewState["TaskType"]), complianceList);
                BindLocationFilterRahul(Convert.ToInt32(ViewState["TaskType"]), complianceList);

                BindLocationFilterTask();

                BindAssignment(subTaskID);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilterTask", string.Format("initializeJQueryUI('{0}', 'divFilterLocationTask');", tbxFilterLocationTask.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilterTask", "$(\"#divFilterLocationTask\").hide(\"blind\", null, 500, function () { });", true);

                 upTaskAssignmentDetails.Update();
                //ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "OpenDialog2", "$(\"#divAssignmentDetailsDialog\").dialog('open')", true);

            }
            else if (e.CommandName.Equals("DELETE_TASK"))
            {
                //Business.TaskManagment.DeleteTask(subTaskID);

                long parentID = -1;
                if (ViewState["ParentID"] != null)
                {
                    long.TryParse(ViewState["ParentID"].ToString(), out parentID);
                }

                bool CheckTaskAssinged = false;
                bool CheckSubTaskDeleted = false;
                CheckTaskAssinged = TaskManagment.GetTaskAssignedData(subTaskID);
                CheckSubTaskDeleted = TaskManagment.CheckSubtaskDataDeleted(subTaskID);
                if (CheckTaskAssinged)
                {
                    if (CheckSubTaskDeleted)
                    {
                        TaskManagment.DeleteTaskRecord(subTaskID);
                        CvSubTask.IsValid = false;
                        CvSubTask.ErrorMessage = "SubTask Deleted Successfully";
                        BindSubTasks(parentID, "");
                    }
                    else
                    {
                        CvSubTask.IsValid = false;
                        CvSubTask.ErrorMessage = "First delete subtask.";
                    }
                }
                else
                {
                    CvSubTask.IsValid = false;
                    CvSubTask.ErrorMessage = "Sub Task can not delete, Sub task already assigned";
                }
            }
            else if (e.CommandName.Equals("VIEW_CHILDREN"))
            {
                try
                {
                    ViewState["ParentID"] = subTaskID;

                    int? duedays = TaskManagment.GetDueDays(subTaskID);

                    ViewState["DueDays"] = Convert.ToInt32(ViewState["DueDays"]) + duedays;

                    long parentID = -1;
                    if (ViewState["ParentID"] != null)
                    {
                        long.TryParse(ViewState["ParentID"].ToString(), out parentID);
                    }

                    BindSubTasks(parentID, "");
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
            }
        }

        protected void grdTask_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdTask.PageIndex = e.NewPageIndex;

                long parentID = -1;
                if (ViewState["ParentID"] != null)
                {
                    long.TryParse(ViewState["ParentID"].ToString(), out parentID);
                }

                BindSubTasks(parentID, "");
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        protected void grdTask_Sorting(object sender, GridViewSortEventArgs e)
        {

        }

        protected void grdTask_RowCreated(object sender, GridViewRowEventArgs e)
        {

        }

        protected void grdTask_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        public static void ClearTreeViewSelection(TreeView tree)
        {
            foreach (TreeNode node in tree.Nodes)
            {
                if (node.Checked && node.ChildNodes.Count == 0) // if (node.Checked)
                {
                    node.Checked = false;
                }

                foreach (TreeNode tn in node.ChildNodes)
                {
                    if (tn.Checked && tn.ChildNodes.Count == 0) //if (tn.Checked)
                    {
                        tn.Checked = false;
                    }

                    if (tn.ChildNodes.Count != 0)
                    {
                        for (int i = 0; i < tn.ChildNodes.Count; i++)
                        {
                            tn.ChildNodes[i].Checked = false;
                        }
                    }
                }
            }
        }

        #region Assignment
        private void BindLocationFilterRahul(int ComplianceType, List<long> ComplianceList)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int customerID = -1;
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    var bracnhes = CustomerBranchManagement.GetAllHierarchyManagementSatutory(customerID);

                    tvFilterLocation.Nodes.Clear();
                    tvFilterLocationTask.Nodes.Clear();

                    List<int> LocationList = new List<int>();

                    int TaskId = -1;
                    if (!string.IsNullOrEmpty(ViewState["MainTaskID"].ToString()))
                    {
                        TaskId = Convert.ToInt32(ViewState["MainTaskID"]);
                    }

                    
                    if (AuthenticationHelper.Role == "MGMT")
                    {
                        if (ComplianceType == 1)
                        {
                            LocationList = TaskManagment.GetSubTaskComplianceMappedLocationMgmt(ComplianceList, customerID, TaskId);
                        }
                        else
                        {
                            LocationList = TaskManagment.GetTaskInternalComplianceMappedLocationMgmt(ComplianceList, customerID);
                        }
                    }
                    else
                    {
                        if (ComplianceType == 1)
                        {
                            LocationList = TaskManagment.GetSubTaskComplianceMappedLocations(ComplianceList, customerID, TaskId);
                        }
                        else
                        {
                            LocationList = TaskManagment.GetSubTaskInternalComplianceMappedLocations(ComplianceList, customerID, TaskId);
                        }

                    }


                    TreeNode node = new TreeNode("Entity/Sub-Entity/Location", "-1");
                    node.ShowCheckBox = false;
                    node.Selected = true;
                    tvFilterLocation.Nodes.Add(node);

                    foreach (var item in bracnhes)
                    {
                        node = new TreeNode(item.Name, item.ID.ToString());
                        node.SelectAction = TreeNodeSelectAction.Expand;
                        if (!LocationList.Contains(item.ID))
                        {
                            node.ShowCheckBox = false;
                        }
                        CustomerBranchManagement.TaskBindBranchesHierarchy(node, item, LocationList);
                        tvFilterLocation.Nodes.Add(node);
                    }
                    tvFilterLocation.CollapseAll();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindLocationFilterTask()
        {
            try
            {
                long customerID = -1;
                customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;
                tvFilterLocationTask.Nodes.Clear();
                var bracnhes = TaskManagment.GetAllHierarchySatutory(customerID);

                TreeNode node = new TreeNode();
                foreach (var item in bracnhes)
                {
                    node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    CustomerBranchManagementRisk.BindBranchesHierarchy(node, item);
                    tvFilterLocationTask.Nodes.Add(node);
                }
                tvFilterLocationTask.CollapseAll();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindLocationFilter(int ComplianceType, List<long> ComplianceList)
        {
            try
            {
                long customerID = -1;
                customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;
                tvFilterLocation.Nodes.Clear();
                var bracnhes = TaskManagment.GetAllHierarchyRestrictLocationForTask(ComplianceType, ComplianceList, customerID);

                TreeNode node = new TreeNode();
                foreach (var item in bracnhes)
                {
                    node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    CustomerBranchManagementRisk.BindBranchesHierarchy(node, item);
                    tvFilterLocation.Nodes.Add(node);
                }
                tvFilterLocation.CollapseAll();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


        //private void BindLocationFilter()
        //{
        //    try
        //    {
        //        int customerID = -1;
        //        int TaskId = -1;
        //        if (!string.IsNullOrEmpty(ViewState["MainTaskID"].ToString()))
        //        {
        //            TaskId = Convert.ToInt32(ViewState["MainTaskID"]);
        //        }
        //        customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

        //        tvFilterLocation.Nodes.Clear();
        //        var bracnhes = TaskManagment.GetAllHierarchySatutory(customerID);
        //        var LocationList = TaskManagment.GetAssignedLocationSUBTASKList(AuthenticationHelper.UserID, customerID, TaskId);

        //        TreeNode node = new TreeNode();

        //        foreach (var item in bracnhes)
        //        {
        //            node = new TreeNode(item.Name, item.ID.ToString());
        //            node.SelectAction = TreeNodeSelectAction.Expand;
        //            CustomerBranchManagement.BindBranchesHierarchy(node, item, LocationList);
        //            tvFilterLocation.Nodes.Add(node);
        //        }

        //        tvFilterLocation.CollapseAll();
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //}
        protected void upTaskAssignmentDetails_Load(object sender, EventArgs e)
        {
            try
            {
                DateTime date = DateTime.MinValue;
                if (DateTime.TryParseExact(tbxStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeConfirmDatePicker", string.Format("initializeConfirmDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeConfirmDatePicker", "initializeConfirmDatePicker(null);", true);
                }

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilterTask", string.Format("initializeJQueryUI('{0}', 'divFilterLocationTask');", tbxFilterLocationTask.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilterTask", "$(\"#divFilterLocationTask\").hide(\"blind\", null, 500, function () { });", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }
        protected void btnSaveTaskAssignment_Click(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["TaskID"] != null)
                {
                    bool validationSuccess = false;
                    bool saveSuccess = false;

                    List<long> selectedLocationList = new List<long>();

                    if (!String.IsNullOrEmpty(ddlPerformerAssign.SelectedValue) && !String.IsNullOrEmpty(ddlReviewerAssign.SelectedValue))
                    {
                        if (ddlPerformerAssign.SelectedValue != "-1" && ddlReviewerAssign.SelectedValue != "-1")
                        {
                            for (int i = 0; i < this.tvFilterLocation.Nodes.Count; i++)
                            {
                                selectedLocationList = RetrieveNodes(this.tvFilterLocation.Nodes[i], selectedLocationList);
                            }

                            if (selectedLocationList.Count > 0)
                                validationSuccess = true;
                            else
                            {
                                CustomValidator1.IsValid = false;
                                CustomValidator1.ErrorMessage = "Select location to Assign.";
                            }
                        }
                        else
                        {
                            CustomValidator1.IsValid = false;
                            CustomValidator1.ErrorMessage = "Select Performer/Reviewer to Assign.";
                        }
                    }
                    else
                    {
                        CustomValidator1.IsValid = false;
                        CustomValidator1.ErrorMessage = "Select Performer/Reviewer to Assign.";
                    }

                    if (validationSuccess)
                    {
                        int TaskbranchID = CustomerBranchManagement.GetByName(tbxFilterLocationTask.Text.Trim(), Convert.ToInt32(AuthenticationHelper.CustomerID)).ID;

                        bool Email = false;
                        string listCustomer = ConfigurationManager.AppSettings["TaskAssignmentEmailCustomerID"];
                        string[] listCust = new string[] { };
                        if (!string.IsNullOrEmpty(listCustomer))
                            listCust = listCustomer.Split(',');

                        if (listCust.Contains(Convert.ToString(AuthenticationHelper.CustomerID)))
                            Email = true;
                        else
                            Email = false;

                        selectedLocationList.ForEach(eachSelectedBranch =>
                        {
                            saveSuccess = SaveAssignment(Convert.ToInt32(ViewState["TaskID"]), Convert.ToInt32(eachSelectedBranch), TaskbranchID, Convert.ToInt32(ddlPerformerAssign.SelectedValue), Convert.ToInt32(ddlReviewerAssign.SelectedValue), Convert.ToBoolean(Session["IsTaskAfter"]), Email);
                        });

                        if (saveSuccess)
                        {
                            CustomValidator1.IsValid = false;
                            CustomValidator1.ErrorMessage = "Task Assigned to User Successfully.";
                        }
                        else
                        {
                            CustomValidator1.IsValid = false;
                            CustomValidator1.ErrorMessage = "Something went wrong, try again.";
                        }

                        BindAssignment(Convert.ToInt32(ViewState["TaskID"]));
                    }
                }

                upTaskAssignmentDetails.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CustomValidator1.IsValid = false;
                CustomValidator1.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        protected List<long> RetrieveNodes(TreeNode node, List<long> locationList)
        {
            if (node.Checked) // if (node.Checked)
            {
                if (!locationList.Contains(Convert.ToInt32(node.Value)))
                    locationList.Add(Convert.ToInt32(node.Value));
            }
            //if (node.Checked && node.ChildNodes.Count == 0) // if (node.Checked)
            //{
            //    if (!locationList.Contains(Convert.ToInt32(node.Value)))
            //        locationList.Add(Convert.ToInt32(node.Value));
            //}

            foreach (TreeNode tn in node.ChildNodes)
            {
                if (tn.Checked && tn.ChildNodes.Count == 0) //if (tn.Checked)
                {
                    if (!locationList.Contains(Convert.ToInt32(tn.Value)))
                        locationList.Add(Convert.ToInt32(tn.Value));
                }

                if (tn.ChildNodes.Count != 0)
                {
                    for (int i = 0; i < tn.ChildNodes.Count; i++)
                    {
                        RetrieveNodes(tn.ChildNodes[i], locationList);
                    }
                }
            }

            return locationList;
        }

        //protected void AddSelectedNodes(TreeNode node)
        //{
        //    //locationList.Clear();
        //    if (node.Checked && node.ChildNodes.Count == 0) // if (node.Checked)
        //    {
        //        if (!locationList.Contains(Convert.ToInt32(node.Value)))
        //        {
        //            locationList.Add(Convert.ToInt32(node.Value));
        //            SaveAssignment(Convert.ToInt32(ViewState["TaskID"]), Convert.ToInt32(node.Value), Convert.ToInt32(ddlPerformerAssign.SelectedValue), Convert.ToInt32(ddlReviewerAssign.SelectedValue));
        //        }
        //    }

        //    foreach (TreeNode tn in node.ChildNodes)
        //    {
        //        if (tn.Checked && tn.ChildNodes.Count == 0) //if (tn.Checked)
        //        {
        //            if (!locationList.Contains(Convert.ToInt32(tn.Value)))
        //            {
        //                locationList.Add(Convert.ToInt32(tn.Value));
        //                SaveAssignment(Convert.ToInt32(ViewState["TaskID"]), Convert.ToInt32(tn.Value), Convert.ToInt32(ddlPerformerAssign.SelectedValue), Convert.ToInt32(ddlReviewerAssign.SelectedValue));
        //            }
        //        }

        //        if (tn.ChildNodes.Count != 0)
        //        {
        //            for (int i = 0; i < tn.ChildNodes.Count; i++)
        //            {
        //                AddSelectedNodes(tn.ChildNodes[i]);
        //            }
        //        }
        //    }
        //}

        private bool SaveAssignment(int taskID, int branchID,int TaskbranchID, int PerformerID, int ReviewerID,Boolean IsAfter,bool Email)
        {
            try
            {
                List<Tuple<TaskInstance, TaskAssignment>> assignments = new List<Tuple<TaskInstance, TaskAssignment>>();

                TaskInstance instance = new TaskInstance();
                instance.TaskId = taskID;
                instance.CustomerBranchID = branchID;
                instance.TaskCustomerBranchID = TaskbranchID;
                instance.ScheduledOn = DateTime.ParseExact(tbxStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                TaskAssignment assignment = new TaskAssignment();
                assignment.UserID = PerformerID;
                assignment.RoleID = 3;
                assignments.Add(new Tuple<TaskInstance, TaskAssignment>(instance, assignment));

                TaskAssignment assignment1 = new TaskAssignment();
                assignment1.UserID = ReviewerID;
                assignment1.RoleID = 4;
                assignments.Add(new Tuple<TaskInstance, TaskAssignment>(instance, assignment1));

                if (assignments.Count != 0)
                {
                    Business.TaskManagment.CreateTaskInstances(assignments, AuthenticationHelper.UserID, AuthenticationHelper.User, Convert.ToInt32(Session["TaskType"]),IsAfter);
                    if (Email)
                    {
                        try
                        {
                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                            {
                                TaskAssignment_EmailDetail eml = new TaskAssignment_EmailDetail();
                                eml.ComplianceLocationID = branchID;
                                eml.ReportingLocationID = TaskbranchID;
                                eml.PerformerID = PerformerID;
                                eml.ReviewerID = ReviewerID;
                                eml.TaskID = taskID;
                                eml.StartDate = DateTime.Now;
                                eml.EmailFlag = false;
                                eml.CreatedBy = AuthenticationHelper.UserID;
                                eml.CreatedOn = DateTime.Now;
                                entities.TaskAssignment_EmailDetail.Add(eml);
                                entities.SaveChanges();
                            }
                            return true;
                        }
                        catch (Exception ex)
                        {
                            LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                            return false;
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occurred. Please try again.";
                return false;
            }
        }

        private void BindAssignment(int TaskID)
        {
            GrdAssigment.DataSource = null;
            GrdAssigment.DataBind();

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var taskQuery = (from row in entities.TaskAssignedInstancesViews
                                 where row.TaskID == TaskID
                                 select new
                                 {
                                     row.TaskAssignmentID
                                        ,
                                     row.TaskInstanceID
                                         ,
                                     row.CustomerBranchID
                                         ,
                                     row.Branch
                                            ,
                                     row.TaskCustomerBranchID
                                         ,
                                     row.TaskCustomerBranchName
                                            ,
                                     row.Role
                                         ,
                                     row.User
                                      ,
                                     row.UserID
                                          ,
                                     row.ScheduledOn
                                 });

                //var taskQuery = (from row in entities.TaskAssignedInstancesViews
                //                 where row.TaskID == TaskID
                //                 select row).ToList();

                var task = taskQuery.OrderBy(entry => entry.TaskInstanceID).ToList();
                //var task = taskQuery.ToList();

                GrdAssigment.DataSource = task;
                GrdAssigment.DataBind();

                upTaskAssignmentDetails.Update();
            }
        }


        protected void GrdAssigment_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            DataRowView drv = e.Row.DataItem as DataRowView;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if ((e.Row.RowState & DataControlRowState.Edit) > 0)
                {
                    DropDownList dp = (DropDownList) e.Row.FindControl("ddlUserList");
                    BindUsers(dp);
                    dp.SelectedValue = GrdAssigment.DataKeys[e.Row.RowIndex].Values[1].ToString();
                }
            }
        }

        protected void GrdAssigment_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GrdAssigment.EditIndex = e.NewEditIndex;
            BindAssignment(Convert.ToInt32(ViewState["TaskID"]));
        }

        protected void GrdAssigment_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GrdAssigment.EditIndex = -1;
            BindAssignment(Convert.ToInt32(ViewState["TaskID"]));
        }

        protected void GrdAssigment_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            int AssignmentID = Convert.ToInt32(GrdAssigment.DataKeys[e.RowIndex].Values[0].ToString());

            Boolean Check = Business.TaskManagment.CheckTaskScheduleOnPresent(AssignmentID);
            if (Check == false)
            {
                DropDownList ddl = (DropDownList) GrdAssigment.Rows[e.RowIndex].FindControl("ddlUserList");
                Business.TaskManagment.UpdateAssignedUserTAsk(AssignmentID, Convert.ToInt32(ddl.SelectedValue));

                CustomValidator1.IsValid = false;
                CustomValidator1.ErrorMessage = "User updated successfully";
            }
            else
            {
                CustomValidator1.IsValid = false;
                CustomValidator1.ErrorMessage = "Task allredy scheduled, you can not change user";
            }

            GrdAssigment.EditIndex = -1;
            BindAssignment(Convert.ToInt32(ViewState["TaskID"]));
        }

        protected void showSampleFormDetails(long taskID)
        {
            if (taskID != 0)
            {
                var taskForm = Business.TaskManagment.GetTaskFormByTaskID(taskID);

                if (taskForm != null)
                {
                    if (taskForm != null)
                    {
                        string fileName = taskForm.FileName;
                        string ext = Path.GetExtension(fileName);
                        //lblSampleForm.Text = taskForm.FileName.Substring(0, 10) + "..." + ext;
                        lblSampleForm.Text = fileName;
                        lblSampleForm.ToolTip = taskForm.FileName;
                    }
                    else
                    {
                        lblSampleForm.Text = "No Form Uploaded";
                        lblSampleForm.ToolTip = "No Sample Form Uploaded";
                    }
                }
                else
                {
                    lblSampleForm.Text = "No Form Uploaded";
                    lblSampleForm.ToolTip = "No Sample Form Uploaded";
                }
            }
            else
            {
                lblSampleForm.Text = "No Form Uploaded";
                lblSampleForm.ToolTip = "No Sample Form Uploaded";
            }
        }
        #endregion

        protected void tvFilterLocationTask_SelectedNodeChanged(object sender, EventArgs e)
        {
            tbxFilterLocationTask.Text = tvFilterLocationTask.SelectedNode.Text;
        }
    }
}