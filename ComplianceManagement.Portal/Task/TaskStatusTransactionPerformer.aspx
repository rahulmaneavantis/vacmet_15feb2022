﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TaskStatusTransactionPerformer.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Task.TaskStatusTransactionPerformer" %>

<!DOCTYPE html>
<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="GleamTech" Namespace="GleamTech.DocumentUltimate.Web" Assembly="GleamTech.DocumentUltimate" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <!-- Bootstrap CSS -->
    <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap theme -->
    <link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <!-- font icon -->
    <link href="~/NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!--external css-->
    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>


    <script type="text/javascript">
         function openInNewTab(url) {
              var win = window.open(url, '_blank');
              win.focus();
          }
        function fFilesubmit() {
            fileUpload = document.getElementById('fuTaskDoc');
            if (fileUpload.value != '') {
                document.getElementById("<%=UploadDocument.ClientID %>").click();
       }
   }
      $(document).ready(function () {
            initializeDatePicker();

            $(document).tooltip({ selector: '[data-toggle="tooltip"]' });
            try{
                window.parent.setIframeHeight(document.body.scrollHeight+"px");
            }catch(e){}
           
        });
        function btnminimize(obj) {
            var s1 = $(obj).find('i');
            if ($(obj).hasClass('collapsed')) {

                $(s1).removeClass('fa-chevron-up');
                $(s1).addClass('fa-chevron-down');
            } else {
                $(s1).removeClass('fa-chevron-down');
                $(s1).addClass('fa-chevron-up');
            }
        }
        function initializeDatePicker() {
            var startDate = new Date();
            $('#<%= tbxDate.ClientID %>').datepicker({
                dateFormat: 'dd-mm-yy',
                maxDate: startDate,
                numberOfMonths: 1,
            });
        }

        function fopenDocumentPriview(file) {
            $('#DocumentPriview').modal('show');
            $('#docPriview').attr('src', "../docviewer.aspx?docurl=" + file);
        }

        function fopendocfile() {
            $('#DocumentPopUp').modal('show');
            $("#<%= docViewerAll.ClientID %>").attr('src', "../docviewer.aspx?docurl=" + $("#<%= lblpathsample.ClientID %>").text());
        }
        function Workingdocumentlnk() {  
           
        WorkingDocumenttextboxempty = document.getElementById('Txtworkingdocumentlnk');
            if (WorkingDocumenttextboxempty.value != '') {
               
                document.getElementById("<%=UploadlinkWorkingfile.ClientID %>").click();
            }
    }
    </script>

    <style type="text/css">
        .topdivlive {
        width: 100%;
        }
        .topdivlivetext {
        width: 69%;
        float: left;
        }
        .topdivliveperformer {
    width: 16%;
}
        .topdivliveimage {
        width: 6%;
        float: right !important;
        }
         .subhead {
    font-size: 14px;
    font-weight: 500;
}
         .topdivlive.topline {
    margin-top: 5px;
    border-top: 1px solid #dddddd;
    padding-top: 5px;
}
          .doctaks{background-image: url(../img/icon-download.png)!important;width: 17px!important;display: block !important;}
        .dd_chk_select {
            height: 81px;
            height: 3px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc !important;
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family: Roboto sans-serif !important;
            margin-bottom: 0px !important;
        }
           .btnss {
            background-image: url(../Images/edit_icon_new.png);
            border: 0px;
            width: 24px;
            height: 24px;
            background-color: transparent;
        }

        .chosen-results {
            height: 75px !important;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>
</head>
<body style="background-color: #f7f7f7;">
    <form id="form1" runat="server">
        <div>
            <asp:ScriptManager ID="ScriptManagerTaskPerformer" runat="server"></asp:ScriptManager>
            <asp:UpdatePanel ID="upTaskPerformer" runat="server" UpdateMode="Conditional" OnLoad="upTaskPerformer_Load">
                <ContentTemplate>
                    <div style="margin: 5px">
                        <div style="margin-bottom: 4px">
                            <asp:ValidationSummary ID="vsTaskPerformer" runat="server"
                                class="alert alert-block alert-danger fade in" ValidationGroup="TaskPerformerValidationGroup" />

                            <asp:CustomValidator ID="cvTaskPerformer" runat="server" class="alert alert-block alert-danger fade in"
                                EnableClientScript="true" ValidationGroup="TaskPerformerValidationGroup" Display="None" style="padding-left:48px" />

                            <asp:HiddenField runat="server" ID="hdnTaskInstanceID" />
                            <asp:HiddenField runat="server" ID="hdnTaskScheduledOnID" />
                            <asp:HiddenField runat="server" ID="hdnComplianceScheduledOnID" />
                        </div>
                        <div class="clearfix" style="margin-bottom: 10px"></div>
                        <div id="divTaskDetails" class="row Dashboard-white-widget">
                            <div class="dashboard">
                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseTaskDetails">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTaskDetails">
                                                <h2>Task Details</h2>
                                            </a>
                                            <div class="panel-actions">
                                                <a class="btn-minimize" onclick="btnminimize(this)"><i class="fa fa-chevron-up"></i></a>
                                            </div>
                                        </div>
                                           <% if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID == 63)
                                    { %>
                                <div id="collapseTaskDetails" class="in">
                                 <%}
                                    else
                                 { %>
                                    <div id="collapseTaskDetails" class="collapse">
                                <%} %>
                                            <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%;">
                                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" style="width: 100%;">
                                                    <table style="width: 100%;">
                                                        <tr class="spaceUnder">
                                                            <td style="width: 15%; font-weight: bold; vertical-align: top;">Task</td>
                                                            <td style="width: 2%; font-weight: bold; vertical-align: top;">: </td>
                                                            <td style="width: 83%;">
                                                                <asp:Label ID="lblTaskTitle" Style="width: 88%; font-size: 13px; color: #333;" autosize="true" runat="server" />
                                                            </td>
                                                        </tr>

                                                        <tr class="spaceUnder">
                                                            <td style="width: 15%; font-weight: bold; vertical-align: top;">Description</td>
                                                            <td style="width: 2%; font-weight: bold; vertical-align: top;">: </td>
                                                            <td style="width: 83%;">
                                                                <asp:Label ID="lblTaskDesc" Style="width: 88%; font-size: 13px; color: #333;" autosize="true" runat="server" />
                                                            </td>
                                                        </tr>

                                                        <tr class="spaceUnder">
                                                            <td style="width: 15%; font-weight: bold; vertical-align: top;">Compliance</td>
                                                            <td style="width: 2%; font-weight: bold; vertical-align: top;">: </td>
                                                            <td style="width: 83%;">
                                                                <asp:Label ID="lblTaskCompliance" Style="width: 88%; font-size: 13px; color: #333;" autosize="true" runat="server" />
                                                            </td>
                                                        </tr>

                                                        <tr class="spaceUnder">
                                                            <td style="width: 15%; font-weight: bold; vertical-align: top;">Reviewer</td>
                                                            <td style="width: 2%; font-weight: bold; vertical-align: top;">: </td>
                                                            <td style="width: 83%;">
                                                                <asp:Label ID="lblTaskReviewer" Style="width: 88%; font-size: 13px; color: #333;" autosize="true" runat="server" />
                                                            </td>
                                                        </tr>

                                                        <tr class="spaceUnder">
                                                            <td style="width: 15%; font-weight: bold; vertical-align: top;">Due Date</td>
                                                            <td style="width: 2%; font-weight: bold; vertical-align: top;">: </td>
                                                            <td style="width: 83%;">
                                                                <asp:Label ID="lblTaskDueDate" Style="width: 88%; font-size: 13px; color: #333;" autosize="true" runat="server" />
                                                            </td>
                                                        </tr>

                                                        <tr class="spaceUnder">
                                                            <td style="width: 15%; font-weight: bold; vertical-align: top;">Location</td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:Label ID="lblLocation" Style="width: 300px; font-size: 13px; color: #333;"
                                                                    maximunsize="300px" autosize="true" runat="server" />
                                                            </td>
                                                        </tr>

                                                        <tr class="spaceUnder">
                                                            <td style="width: 15%; font-weight: bold; vertical-align: top;">Period</td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:Label ID="lblPeriod" Style="width: 300px; font-size: 13px; color: #333;display:none;"
                                                                    maximunsize="300px" autosize="true" runat="server" />
                                                                <asp:Label ID="lblPeriod1" Style="width: 300px; font-size: 13px; color: #333;"
                                                                    maximunsize="300px" autosize="true" runat="server" />
                                                            </td>
                                                        </tr>

                                                        <tr class="spaceUnder">
                                                            <td style="width: 15%; font-weight: bold;">Sample Form</td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:UpdatePanel ID="upsample" runat="server" UpdateMode="Conditional">
                                                                    <ContentTemplate>
                                                                        <asp:Label ID="lblFormNumber" Style="width: 150px; font-size: 13px; color: #333;"
                                                                            maximunsize="300px" autosize="true" runat="server" />
                                                                        <asp:LinkButton ID="lbDownloadSample" Style="width: 150px; font-size: 13px; color: blue"
                                                                            runat="server" Font-Underline="false" OnClick="lbDownloadSample_Click" />
                                                                        <asp:Label ID="lblSlash" Text="/" Style="color: blue;" runat="server" />
                                                                        <asp:LinkButton ID="lnkViewSampleForm" Text="View" Style="width: 150px; font-size: 13px; color: blue"
                                                                            runat="server" Font-Underline="false" OnClientClick="fopendocfile();" />
                                                                        <asp:Label ID="lblpathsample" runat="server" Style="display: none"></asp:Label>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                        </tr>

                                                    </table>
                                                </div>
                                            </fieldset>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="divTaskSubTask" runat="server" class="row Dashboard-white-widget">
                            <div class="dashboard">
                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseTaskSubTask">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTaskSubTask">
                                                <h2>Sub Task Details</h2>
                                            </a>
                                            <div class="panel-actions">
                                                <a class="btn-minimize" onclick="btnminimize(this)"><i class="fa fa-chevron-up"></i></a>
                                            </div>
                                        </div>

                                        <div id="collapseTaskSubTask" class="collapse in">
                                            <div style="margin-bottom: 7px; clear: both; margin-top: 10px">
                                                <asp:GridView runat="server" ID="gridSubTask" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                    AllowPaging="false" PageSize="50" CssClass="table" GridLines="None" BorderWidth="0px" DataKeyNames="TaskID"
                                                    OnRowCommand="gridSubTask_RowCommand" OnRowDataBound="gridSubTask_RowDataBound" AutoPostBack="true">
                                                    <Columns>
                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                                            <ItemTemplate>
                                                                <%#Container.DataItemIndex+1 %>
                                                                <asp:Label ID="lblTaskScheduledOnID" runat="server" Visible="false" Text='<%# Eval("TaskScheduledOnID") %>'></asp:Label>
                                                                <asp:Label ID="lblIsTaskClose" Visible="false" runat="server" Text='<%# Eval("IsTaskClose") %>'></asp:Label>
                                                                <asp:Label ID="lblTaskInstanceID" runat="server" Visible="false" Text='<%# Eval("TaskInstanceID") %>'></asp:Label>
                                                                <asp:Label ID="lblMainTaskID" runat="server" Visible="false" Text='<%# Eval("MainTaskID") %>'></asp:Label>
                                                                <asp:Label ID="lblForMonth" runat="server" Visible="false" Text='<%# Eval("ForMonth") %>'></asp:Label>
                                                                <asp:Label ID="lblComplianceScheduleOnID" runat="server" Visible="false" Text='<%# Eval("ComplianceScheduleOnID") %>'></asp:Label>
                                                                <asp:Label ID="lblTaskID" runat="server" Visible="false" Text='<%# Eval("TaskID") %>'></asp:Label>
                                                                <asp:Label ID="lblCbranchId" runat="server" Visible="false" Text='<%# Eval("CustomerBranchID") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Task">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; width: 250px;">
                                                                    <asp:Label ID="lblTaskTitle" runat="server" data-toggle="tooltip" data-placement="bottom"
                                                                        Text='<%# Eval("TaskTitle") %>' ToolTip='<%# Eval("TaskTitle") %>'></asp:Label>
                                                                       
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Compliance">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                    <asp:Label ID="lblShortDescription" runat="server" data-toggle="tooltip" data-placement="bottom"
                                                                        Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Location">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                    <asp:Label ID="lblLocation" runat="server" Text='<%# Eval("Branch") %>' data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Branch") %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Period">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 80px;">
                                                                    <asp:Label ID="lblPeriod" runat="server" Text='<%# Eval("ForMonth") %>' data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("ForMonth") %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Performer">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 70px;">
                                                                    <asp:Label ID="lblPerformer" runat="server" data-toggle="tooltip" data-placement="bottom"
                                                                        ToolTip='<%# GetUserName((long)Eval("TaskInstanceID"),(long)Eval("TaskScheduledOnID"),3,(byte)Eval("TaskType")) %>'
                                                                        Text='<%# GetUserName((long)Eval("TaskInstanceID"),(long)Eval("TaskScheduledOnID"),3,(byte)Eval("TaskType")) %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Reviewer">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 70px;">
                                                                    <asp:Label ID="lblReviewer" runat="server" data-toggle="tooltip" data-placement="bottom" ToolTip='<%# GetUserName((long)Eval("TaskInstanceID"),(long)Eval("TaskScheduledOnID"),4,(byte)Eval("TaskType")) %>'
                                                                        Text='<%# GetUserName((long)Eval("TaskInstanceID"),(long)Eval("TaskScheduledOnID"),4,(byte)Eval("TaskType")) %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Due Date">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                    <asp:Label ID="lblScheduledOn" runat="server" data-toggle="tooltip" data-placement="bottom" Text=' <%# Convert.ToDateTime(Eval("ScheduledOn")).ToString("dd-MMM-yyyy") %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Status">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 50px;">
                                                                    <asp:Label ID="lblStatus" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Status") %>' ToolTip='<%# Eval("Status") %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Action">
                                                            <ItemTemplate>
                                                                <asp:UpdatePanel ID="upSubTaskDownloadView" runat="server">
                                                                    <ContentTemplate>
                                                                        <asp:LinkButton ID="btnSubTaskDocDownload"  runat="server" CommandName="Download" CommandArgument='<%# Eval("TaskScheduledOnID") %>'
                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip="Download task Documents" Text="Download" Style="color: blue;">
                                                                        </asp:LinkButton>
                                                                        <asp:Label ID="lblSlashReview" Text="/" Style="color: blue;" runat="server" />
                                                                        <asp:LinkButton CommandName="View" runat="server" ID="btnSubTaskDocView" CommandArgument='<%# Eval("TaskScheduledOnID") %>'
                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip="View Documents"
                                                                            Text="View" Style="width: 150px; font-size: 13px; color: blue" />
                                                                        <asp:Label ID="CompDocReviewPath" runat="server" Style="display: none"></asp:Label>
                                                                        <asp:CheckBox ID="chkTask" Width="30px" data-toggle="tooltip" CssClass="sbtask" style="display:none;float:left" ToolTip="Click to close if not applicable" runat="server"  data-yesmessage='<%# Eval("Yesmessage") %>' data-nomessage='<%# Eval("Nomessage") %>' data-attr='<%# Eval("IsYesNo") %>'  data-msg='<%# Eval("Message") %>' data-yes='<%# Eval("IsYes") %>' data-no='<%# Eval("IsNo")  %>' data-IsBothYesNo='<%# Eval("IsBothYesNo")  %>' onclick="javascript:SelectheaderCheckboxes(this)" />
                                                                        <asp:Image ID="chkDocument" ImageUrl="../Images/View-icon-new.png"   data-toggle="tooltip" CssClass="Documentsbtask"  ToolTip="Click to download subtasks documents" runat="server" OnClick="javascript:SelectheaderDOCCheckboxes(this)"   /> 
                                                                 
                                                                         <asp:Label ID="lblsubtasks" CssClass="subtasklist" runat="server" style="display:none;"></asp:Label>

                                                                        <asp:Label ID="lblsubtaskDocuments" CssClass="subtaskDocumentlist" runat="server" style="display:none;"></asp:Label>
                                                                    </ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:PostBackTrigger ControlID="btnSubTaskDocDownload" />
                                                                        <asp:PostBackTrigger ControlID="btnSubTaskDocView" />
                                                                        <asp:PostBackTrigger ControlID="chkTask" />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <PagerStyle HorizontalAlign="Right" />
                                                    <EmptyDataTemplate>
                                                        No Record Found
                                                    </EmptyDataTemplate>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="divLinkTaskSubTask" runat="server" class="row Dashboard-white-widget">
                            <div class="dashboard">
                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseLinkTaskSubTask">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseLinkTaskSubTask">
                                                <h2>Link Task Details</h2>
                                            </a>
                                            <div class="panel-actions">
                                                <a class="btn-minimize" onclick="btnminimize(this)"><i class="fa fa-chevron-up"></i></a>
                                            </div>
                                        </div>

                                        <div id="collapseLinkTaskSubTask" class="collapse in">
                                            <div style="margin-bottom: 7px; clear: both; margin-top: 10px">
                                                <asp:GridView runat="server" ID="gridLinkSubTask" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                    AllowPaging="false" PageSize="50" CssClass="table" GridLines="None" BorderWidth="0px" DataKeyNames="TaskID"
                                                    OnRowCommand="gridLinkSubTask_RowCommand" OnRowDataBound="gridLinkSubTask_RowDataBound" AutoPostBack="true">
                                                    <Columns>
                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                                            <ItemTemplate>
                                                                <%#Container.DataItemIndex+1 %>
                                                                <asp:Label ID="lblLinkTaskScheduledOnID" runat="server" Visible="false" Text='<%# Eval("TaskScheduledOnID") %>'></asp:Label>
                                                                <asp:Label ID="lblLinkIsTaskClose" Visible="false" runat="server" Text='<%# Eval("IsTaskClose") %>'></asp:Label>
                                                                <asp:Label ID="lblLinkTaskInstanceID" runat="server" Visible="false" Text='<%# Eval("TaskInstanceID") %>'></asp:Label>
                                                                <asp:Label ID="lblLinkMainTaskID" runat="server" Visible="false" Text='<%# Eval("MainTaskID") %>'></asp:Label>
                                                                <asp:Label ID="lblLinkForMonth" runat="server" Visible="false" Text='<%# Eval("ForMonth") %>'></asp:Label>
                                                                <asp:Label ID="lblLinkComplianceScheduleOnID" runat="server" Visible="false" Text='<%# Eval("ComplianceScheduleOnID") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Task">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                    <asp:Label ID="lblLinkTaskTitle" runat="server" data-toggle="tooltip" data-placement="bottom"
                                                                        Text='<%# Eval("TaskTitle") %>' ToolTip='<%# Eval("TaskTitle") %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Compliance">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                    <asp:Label ID="lblLinkShortDescription" runat="server" data-toggle="tooltip" data-placement="bottom"
                                                                        Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Location">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                    <asp:Label ID="lblLinkLocation" runat="server" Text='<%# Eval("Branch") %>' data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Branch") %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Period">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 80px;">
                                                                    <asp:Label ID="lblLinkPeriod" runat="server" Text='<%# Eval("ForMonth") %>' data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("ForMonth") %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Performer">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 70px;">
                                                                    <asp:Label ID="lblLinkPerformer" runat="server" data-toggle="tooltip" data-placement="bottom"
                                                                        ToolTip='<%# GetUserName((long)Eval("TaskInstanceID"),(long)Eval("TaskScheduledOnID"),3,(byte)Eval("TaskType")) %>'
                                                                        Text='<%# GetUserName((long)Eval("TaskInstanceID"),(long)Eval("TaskScheduledOnID"),3,(byte)Eval("TaskType")) %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Reviewer">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 70px;">
                                                                    <asp:Label ID="lblLinkReviewer" runat="server" data-toggle="tooltip" data-placement="bottom" ToolTip='<%# GetUserName((long)Eval("TaskInstanceID"),(long)Eval("TaskScheduledOnID"),4,(byte)Eval("TaskType")) %>'
                                                                        Text='<%# GetUserName((long)Eval("TaskInstanceID"),(long)Eval("TaskScheduledOnID"),4,(byte)Eval("TaskType")) %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Due Date">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                    <asp:Label ID="lblLinkScheduledOn" runat="server" data-toggle="tooltip" data-placement="bottom" Text=' <%# Convert.ToDateTime(Eval("ScheduledOn")).ToString("dd-MMM-yyyy") %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Status">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 50px;">
                                                                    <asp:Label ID="lblLinkStatus" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Status") %>' ToolTip='<%# Eval("Status") %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Action">
                                                            <ItemTemplate>
                                                                <asp:UpdatePanel ID="upLinkSubTaskDownloadView" runat="server">
                                                                    <ContentTemplate>
                                                                        <asp:LinkButton ID="btnLinkSubTaskDocDownload" runat="server" CommandName="Download" CommandArgument='<%# Eval("TaskScheduledOnID") %>'
                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip="Download Documents" Text="Download" Style="color: blue;">
                                                                        </asp:LinkButton>
                                                                        <asp:Label ID="lblLinkSlashReview" Text="/" Style="color: blue;" runat="server" />
                                                                        <asp:LinkButton CommandName="View" runat="server" ID="btnLinkSubTaskDocView" CommandArgument='<%# Eval("TaskScheduledOnID") %>'
                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip="View Documents"
                                                                            Text="View" Style="width: 150px; font-size: 13px; color: blue" />
                                                                        <asp:Label ID="LinkCompDocReviewPath" runat="server" Style="display: none"></asp:Label>
                                                                        <%--                                                                        <asp:CheckBox ID="chkLinkTask" Width="30px" data-toggle="tooltip" ToolTip="Click to close if not applicable" runat="server" onclick="javascript:SelectheaderCheckboxes(this)" />--%>
                                                                    </ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:PostBackTrigger ControlID="btnLinkSubTaskDocDownload" />
                                                                        <asp:PostBackTrigger ControlID="btnLinkSubTaskDocView" />
                                                                        <%--<asp:PostBackTrigger ControlID="chkLinkTask" />--%>
                                                                    </Triggers>
                                                                </asp:UpdatePanel>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <PagerStyle HorizontalAlign="Right" />
                                                    <EmptyDataTemplate>
                                                        No Record Found
                                                    </EmptyDataTemplate>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="UpdateTaskStatus" class="row Dashboard-white-widget">
                            <div class="dashboard">
                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseUpdateTaskStatus">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseUpdateTaskStatus">
                                                <h2>Update Task Status</h2>
                                            </a>
                                            <div class="panel-actions">
                                                <a class="btn-minimize" onclick="btnminimize(this)"><i class="fa fa-chevron-up"></i></a>
                                            </div>
                                        </div>


                                        <div id="collapseUpdateTaskStatus" class="panel-collapse collapse in">
                                            <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%;">
                                                <div style="margin-bottom: 7px">
                                                    <table style="width: 100%">
                                                        <tr>
                                                            <td style="width: 25%;">
                                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="font-weight: bold; vertical-align: text-top;">Status</label>
                                                            </td>

                                                            <td style="width: 2%; font-weight: bold; vertical-align: text-top;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:DropDownList runat="server" ID="ddlStatus" class="form-control m-bot15" Style="width: 280px;" AutoPostBack="true" />
                                                                <%--OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged"--%>
                                                                <asp:CompareValidator ID="cvTaskStatus" ErrorMessage="Please Select Status." ControlToValidate="ddlStatus"
                                                                    runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="TaskPerformerValidationGroup" Display="None" />

                                                            </td>
                                                        </tr>
                                                         <% if (UploadDocumentLink == "True")
                                                        {%>
                                                        <tr>
                                                            <td style="width: 25%;">
                                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="font-weight: bold; vertical-align: text-top;">Upload Files(s)</label>
                                                            </td>

                                                            <td style="width: 2%; font-weight: bold; vertical-align: text-top;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:TextBox runat="server" ID="Txtworkingdocumentlnk" class="form-control" />
                                                                </td> <td>
                                                                <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                                                                    <ContentTemplate>
                                                                        <asp:Button ID="UploadlinkWorkingfile" runat="server" Text="Add Link"  OnClick="UploadlinkWorkingfile_Click"
                                                                            class="btn btn-search" data-toggle="tooltip" data-placement="top" ToolTip="Add Link" />
                                                                    </ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:PostBackTrigger ControlID="UploadlinkWorkingfile" />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                        </tr>                                                       
                                                        <%}%>
                                                    </table>
                                                </div>
                                                <div style="margin-bottom: 7px" runat="server" id="divUploadDocument">
                                                    <table style="width: 100%">
                                                       <% if (UploadDocumentLink != "True")
                                                        {%>
                                                        <tr>
                                                            <td style="width: 25%;">
                                                                <asp:Label ID="lblTaskDocument" runat="server" Style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</asp:Label>
                                                                <label style="font-weight: bold; vertical-align: text-top;">Upload Document(s)</label>
                                                            </td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 60%;">
                                                                <asp:FileUpload ID="fuTaskDoc" AllowMultiple="true" onchange="fFilesubmit()" runat="server" Style="color: black" />
                                                              <%--  <asp:RequiredFieldValidator ErrorMessage="Please select documents for upload." ControlToValidate="fuTaskDoc"
                                                                    runat="server" ID="rfvTaskDoc" ValidationGroup="TaskPerformerValidationGroup" Display="None" />--%>
                                                            </td>
                                                            <td style="width: 13%;">
                                                                <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                                                                    <ContentTemplate>
                                                                        <asp:Button ID="UploadDocument" runat="server" Text="Upload Document" style="display:none;" OnClick="UploadDocument_Click"
                                                                            class="btn btn-search" data-toggle="tooltip" data-placement="top" ToolTip="Upload Document"
                                                                            CausesValidation="true" />
                                                                    </ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:PostBackTrigger ControlID="UploadDocument" />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                        </tr>
                                                             <%}%>
                                                    </table>
                                                </div>
                                                         <div style="margin-bottom: 7px" runat="server" id="divgrdFiles">
                                            <table style="width: 100%">
                                                <tr>
                                                    <td style="width: 100%;">
                                                        <asp:GridView runat="server" ID="grdDocument" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                            PageSize="100" AllowPaging="true" OnRowCommand="grdDocument_RowCommand" OnRowDataBound="grdDocument_RowDataBound" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%">
                                                            <Columns>
                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="2%">
                                                                    <ItemTemplate>
                                                                        <%#Container.DataItemIndex+1 %>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Document Name" ItemStyle-Width="10%">
                                                                    <ItemTemplate>
                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 500px;">
                                                                            <asp:Label ID="lblDocumentName" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("DocName") %>' ToolTip='<%# Eval("DocName") %>'></asp:Label>
                                                                             <asp:LinkButton ID="lnkRedirectDocument" runat="server" data-toggle="tooltip" Style="color: blue;text-decoration: underline;"
                                                                                    OnClientClick=<%# "openInNewTab('" + Eval("DocPath") + "')" %>  Text='<%# Eval("DocName") %>' />
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Document Type" ItemStyle-Width="10%">
                                                                    <ItemTemplate>
                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                                            <asp:Label ID="lblIsLinkTrue" Visible="false" runat="server" data-placement="bottom" Text='<%# Eval("ISLink") %>' ToolTip='<%# Eval("ISLink") %>'></asp:Label>
                                                                            <asp:Label ID="lblDocType" runat="server" data-placement="bottom" Text='<%# Eval("DocType") %>' ToolTip='<%# Eval("DocType") %>'></asp:Label>
                                                                            <asp:Label ID="lblScheduleOnID" runat="server" Visible="false" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ScheduleOnID") %>'></asp:Label>
                                                                            <asp:Label ID="lblComplianceInstanceID" runat="server" Visible="false" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ComplianceInstanceID") %>'></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Action" HeaderStyle-CssClass="text-right" ItemStyle-CssClass="text-right" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="8%">
                                                                    <ItemTemplate>
                                                                        <asp:UpdatePanel ID="upgrid" runat="server" UpdateMode="Conditional">
                                                                            <ContentTemplate>
                                                                                <asp:LinkButton ID="lnkDownloadDocument" runat="server" CommandName="Download Document" ToolTip="Download Document" data-toggle="tooltip"
                                                                                    CommandArgument='<%# Eval("Id") %>'>
                                                        <img src='<%# ResolveUrl("~/Images/download_icon_new.png")%>' alt="Download Document" /></asp:LinkButton>
                                                                                <asp:LinkButton ID="lnkViewDocument" runat="server" CommandName="View Document" ToolTip="View Document" data-toggle="tooltip"
                                                                                    CommandArgument='<%# Eval("Id") %>'>
                                                        <img src='<%# ResolveUrl("~/Images/View-icon-new.png")%>' alt="View Document" /></asp:LinkButton>
                                                                                <asp:LinkButton ID="lnkDeleteDocument" runat="server" CommandName="Delete Document" ToolTip="Delete Document" data-toggle="tooltip"
                                                                                    CommandArgument='<%# Eval("Id") %>'>
                                                        <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="Delete Document" /></asp:LinkButton>
                                                                            </ContentTemplate>
                                                                            <Triggers>
                                                                                <asp:PostBackTrigger ControlID="lnkDownloadDocument" />
                                                                            </Triggers>
                                                                        </asp:UpdatePanel>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                            </Columns>
                                                            <RowStyle CssClass="clsROWgrid" />
                                                            <HeaderStyle CssClass="clsheadergrid" />
                                                            <PagerSettings Visible="false" />
                                                            <PagerTemplate>
                                                            </PagerTemplate>
                                                            <EmptyDataTemplate>
                                                                No Record Found
                                                            </EmptyDataTemplate>
                                                        </asp:GridView>

                                                    </td>
                                                </tr>
                                            </table>
                                        </div>



                                                <div style="margin-bottom: 7px">
                                                    <table style="width: 100%">
                                                        <tr class="spaceUnder">
                                                            <td style="width: 25%;">
                                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="font-weight: bold; vertical-align: text-top;">Date</label>
                                                            </td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:TextBox runat="server" ID="tbxDate" placeholder="DD-MM-YYYY" class="form-control" AutoComplete="off"
                                                                    Style="text-align: center; background: none; cursor: pointer; width: 115px;" />
                                                                <asp:RequiredFieldValidator ErrorMessage="Please Select Date." ControlToValidate="tbxDate"
                                                                    runat="server" ID="RequiredFieldValidator1" ValidationGroup="TaskPerformerValidationGroup" Display="None" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>

                                                <div style="margin-bottom: 7px">
                                                    <table style="width: 100%">
                                                        <tr>
                                                            <td style="width: 25%;">
                                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                                                <label style="font-weight: bold; vertical-align: text-top;">Remarks</label>
                                                            </td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:TextBox runat="server" ID="tbxRemarks" TextMode="MultiLine" class="form-control" Rows="2" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>

                                                <div style="margin-bottom: 7px; text-align: center; margin-top: 10px;">
                                                    <asp:Button Text="Submit" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="btn btn-primary"
                                                        ValidationGroup="TaskPerformerValidationGroup" />
                                                    <asp:Button Text="Close" ID="btnClose" Style="margin-left: 15px; display: none" CssClass="btn btn-primary" data-dismiss="modal" runat="server" />
                                                     <asp:Button Text="Go To Portal" ID="lnkgotoportal"  Visible="false" OnClick="lnkgotoportal_Click"
                                                        Style="margin-left: 15px;" CssClass="btn btn-search" data-dismiss="modal" runat="server" />
                                                </div>

                                                <asp:Label ID="lblStatusNote" runat="server" Text="<b>Note- </b> You are not able to change the status of task until all sub-task status are not closed." Style="font-size: 10px; color: #666;"></asp:Label>
                                            </fieldset>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="divTaskAuditLog" class="row Dashboard-white-widget">
                            <div class="dashboard">
                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-default" data-toggle="collapse" data-parent="#accordion" href="#collapseTaskAuditLog">

                                        <div class="panel-heading">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTaskAuditLog">
                                                <h2>Audit Log</h2>
                                            </a>
                                            <div class="panel-actions">
                                                <a class="btn-minimize" onclick="btnminimize(this)"><i class="fa fa-chevron-up"></i></a>
                                            </div>
                                        </div>


                                        <div id="collapseTaskAuditLog" class="collapse">
                                            <div runat="server" id="log" style="text-align: left;">
                                                <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; margin-top: 5px;">
                                                    <div style="margin-bottom: 7px; clear: both; margin-top: 10px">
                                                        <asp:GridView runat="server" ID="grdTransactionLogHistory" AutoGenerateColumns="false" AllowSorting="true"
                                                            AllowPaging="true" PageSize="5" CssClass="table" GridLines="None" OnPageIndexChanging="grdTransactionLogHistory_OnPageIndexChanging"
                                                            BorderWidth="0px" DataKeyNames="TaskTransactionID">
                                                            <%--OnRowCommand="grdTransactionHistory_RowCommand"--%>
                                                            <Columns>
                                                                <asp:BoundField DataField="CreatedByText" HeaderText="Changed By" />
                                                                <asp:TemplateField HeaderText="Date">
                                                                    <ItemTemplate>
                                                                        <%# Eval("Dated") != null ? Convert.ToDateTime(Eval("Dated")).ToString() : ""%>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField DataField="Remarks" HeaderText="Remarks" />
                                                                <asp:BoundField DataField="Status" HeaderText="Status" />
                                                            </Columns>
                                                            <PagerStyle HorizontalAlign="Right" />
                                                        </asp:GridView>
                                                    </div>
                                                </fieldset>
                                                <asp:Label ID="lblNote" runat="server" Text="*Please download the attached document to verify and then changed the status." Style="font-size: 10px; color: #666;" Visible="false"></asp:Label>
                                                <div></div>
                                                <asp:HiddenField runat="server" ID="hdlSelectedDocumentID" />
                                                <asp:Button ID="btnDownload" runat="server" Style="display: none" />
                                                <%--OnClick="btnDownload_Click"--%>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnDownload" />
                    <asp:PostBackTrigger ControlID="btnSave" />
                    <asp:PostBackTrigger ControlID="lbDownloadSample" />
                </Triggers>
            </asp:UpdatePanel>

            <div>
                <div class="modal fade" id="modalDocumentPerformerViewer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden; top: 5%;">
                    <div class="modal-dialog" style="width: 100%">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss-modal="modal2" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body" style="height: 570px;">
                                <div style="width: 100%;">
                                    <div style="float: left; width: 10%">
                                        <table width="100%" style="text-align: left; margin-left: 5%;">
                                            <thead>
                                                <tr>
                                                    <td valign="top">
                                                        <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <asp:Repeater ID="rptComplianceVersionView" runat="server" OnItemCommand="rptComplianceVersionView_ItemCommand">
                                                                    <HeaderTemplate>
                                                                        <table id="tblComplianceDocumnets">
                                                                            <thead>
                                                                                <th>Versions</th>
                                                                            </thead>
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdateMode="Conditional">
                                                                                    <ContentTemplate>
                                                                                        <asp:LinkButton CommandName="View" CommandArgument='<%# Eval("TaskScheduleOnID") + ","+ Eval("Version") + ","+ Eval("FileID") %>' ID="lblDocumentVersionView"
                                                                                            runat="server" ToolTip='<%# Eval("FileName")%>' Text='<%# Eval("Version") +" "+ Eval("FileName").ToString().Substring(0,4) %>'></asp:LinkButton>
                                                                                    </ContentTemplate>
                                                                                    <Triggers>
                                                                                        <asp:AsyncPostBackTrigger ControlID="lblDocumentVersionView" />
                                                                                    </Triggers>
                                                                                </asp:UpdatePanel>
                                                                            </td>
                                                                        </tr>
                                                                    </ItemTemplate>
                                                                    <FooterTemplate>
                                                                        </table>
                                                                    </FooterTemplate>
                                                                </asp:Repeater>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:AsyncPostBackTrigger ControlID="rptComplianceVersionView" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                    <div style="float: left; width: 90%">
                                        <asp:Label runat="server" ID="lblMessage" Style="color: red;"></asp:Label>
                                        <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; height: 550px; width: 100%;">
                                            <iframe src="about:blank" id="docViewerPerformerAll" runat="server" width="100%" height="535px"></iframe>
                                        </fieldset>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="ConfirmationModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog" style="width: 85%;">
                    <div class="modal-content" style="width: 100%;">
                        <div class="modal-header">
                            <div style="width: 10%; float: right">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div style="width: 90%; align-content: center; margin-left: 39%; float: left;">
                                <p style="font-size: 20px">Confirmation</p>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div id="DivYesNoConf">
                                <asp:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div  >
                                            <div style="width:100%; margin-left: 3%;">
                                                <p id="idProceed" style="margin-left: 11%;width:65%;">Proceeds to subsequent checks?</p>
                                                 <div id="taskslower" style="padding: 4px; width:98%; overflow:auto; color: #333; padding-left: 0px;display:none;">

                                                </div>
                                                <div id="yesnobtn" style="margin-left: 45%;">
                                                    <asp:LinkButton ID="lblYes" runat="server" Text="Yes" CssClass="btn btn-primary" OnClick="lblNo_Click" OnClientClick="javascript:return callOnButtonYes()"></asp:LinkButton>
                                                    <asp:LinkButton ID="lblNo" runat="server" Text="No" style="margin-left:15px;" CssClass="btn btn-primary" OnClick="lblNo_Click" OnClientClick="javascript:return callOnButtonNo()"></asp:LinkButton>
                                                </div>
                                               
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:PostBackTrigger ControlID="lblNo" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            

            <div class="modal fade" id="ConfirmationDocumentModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog" style="width: 85%;">
                    <div class="modal-content" style="width: 100%;">
                        <div class="modal-header">
                            <div style="width: 10%; float: right">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div style="width: 90%; align-content: center; margin-left: 39%; float: left;">
                                <p style="font-size: 20px">Document(s)</p>
                            </div>
                        </div>
                        <div class="modal-body">                            
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div  >
                                            <div style="width:100%; margin-left: 3%;">                                                
                                                 <div id="taskDocslower" style="padding: 4px; width:98%; overflow:auto; color: #333; padding-left: 0px;display:none;">

                                                </div>                                              
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <%--<asp:PostBackTrigger ControlID="lblNo" />--%>
                                    </Triggers>
                                </asp:UpdatePanel>                           
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="DocumentPopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden; top: 5%;">
                <div class="modal-dialog" style="width: 100%;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" onclick="$('#DocumentPopUp').modal('hide');" aria-hidden="true">×</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                            <iframe src="about:blank" id="docViewerAll" runat="server" width="100%" height="550px"></iframe>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="DocumentPriview" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden; top: 5%;">
                <div class="modal-dialog" style="width: 100%">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" onclick="$('#DocumentPriview').modal('hide');" aria-hidden="true">×</button>
                        </div>
                        <div class="modal-body" style="height: 570px;">
                            <div style="width: 100%;">
                                <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; height: 550px; width: 100%;">
                                    <iframe src="about:blank" id="docPriview" runat="server" width="100%" height="535px"></iframe>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <iframe id="filedownload" style="display:none;" src="about:blank"></iframe>
        <script type="text/javascript">

            
            function SelectheaderDOCCheckboxes(headerchk) {
              
                $('#taskDocslower').hide();
                var spanparent = $(headerchk).parent('div');
               // var tr = $(spanparent).parent('.subtaskDocumentlist');
                var subtaskDocumentlistobj = $(spanparent).find('.subtaskDocumentlist');
                
                var count = 0;
                 
                $('#taskDocslower').html($(subtaskDocumentlistobj).html());
                $('#ConfirmationDocumentModel').modal('show');
                $('#taskDocslower').show();
                  
            }
            var nomessage = '';
            var yesmessage = '';
            var IsBothYesNo = '';

            function SelectheaderCheckboxes(headerchk) {
                $('#taskslower').hide();
                $('#lblYes').show();
                $('#lblNo').show();
                var spanparent = $(headerchk).parent('.sbtask');
                var tr = $(spanparent).parent('div');
                var subtasklistobj = $(tr).find('.subtasklist');
                
                var count = 0;
                var gvcheck = document.getElementById("<%=gridSubTask.ClientID %>");
                var confirm_value = document.createElement("INPUT");
                confirm_value.type = "hidden";
                confirm_value.name = "confirm_value";
                var gv = document.getElementById("<%= gridSubTask.ClientID %>");
                var inputList = gv.getElementsByTagName("input");
           
                for (var i = 0; i < inputList.length; i++) {
                    if (inputList[i].type == "checkbox" && inputList[i].checked) {
                        count = count + 1;
                         
                        $('#taskslower').html($(subtasklistobj).html());
                        if ($(spanparent).attr('data-msg') == '') {
                            $('#lblYes').removeAttr("href");
                        } else {
                            $('#idProceed').html($(spanparent).attr('data-msg'));
                            if ($(spanparent).attr('data-IsBothYesNo') == "True" || $(spanparent).attr('data-IsBothYesNo') == "1") {
                                $('#lblNo').removeAttr("href");
                                $('#lblYes').removeAttr("href");
                                IsBothYesNo = $(spanparent).attr('data-IsBothYesNo');
                            }

                            yesmessage = $(spanparent).attr('data-yesmessage');
                            nomessage = $(spanparent).attr('data-nomessage');

                            if ($(spanparent).attr('data-yes') == "True" || $(spanparent).attr('data-yes') == "1") {
                                $('#lblNo').removeAttr("href");
                               // $('#lblYes').attr('href', "javascript:__doPostBack('lblNo', '')");
                            }
                            if ($(spanparent).attr('data-no') == "True" || $(spanparent).attr('data-no') == "1") {
                                $('#lblYes').removeAttr("href");
                                //$('#lblNo').attr('href', "javascript:__doPostBack('lblNo', '')");
                            }
                        }
                        $('#ConfirmationModel').modal('show');
                       
                    }
                }
            }

            function callOnButtonYes() {
                if (IsBothYesNo == "True" || IsBothYesNo == 1) {
                    var r = confirm(yesmessage);
                }
                else {
                    var count = 0;
                    var gvcheck = document.getElementById("<%=gridSubTask.ClientID %>");
                    var confirm_value = document.createElement("INPUT");
                    confirm_value.type = "hidden";
                    confirm_value.name = "confirm_value";
                    var gv = document.getElementById("<%= gridSubTask.ClientID %>");
                    var inputList = gv.getElementsByTagName("input");
                    for (var i = 0; i < inputList.length; i++) {
                        if (inputList[i].type == "checkbox" && inputList[i].checked) {
                            //    inputList[i].checked = false;
                        }
                    }
                    if ($('#lblYes').attr("href") == null || $('#lblYes').attr("href") == undefined || $('#lblYes').attr("href") == '') {
                        $('#taskslower').show();
                        $('#lblYes').hide();
                        $('#lblNo').hide();
                        $('#ConfirmationModel').modal('show');
                        return false;
                    }
                    else {
                        if (IsBothYesNo == "True" || IsBothYesNo == 1) {
                            var r = confirm(yesmessage);
                        }
                        else {
                            var r = confirm(yesmessage);
                            if (r == true) {
                                $('#ConfirmationModel').modal('hide');
                                return true;
                            } else {
                                $('#ConfirmationModel').modal('show');
                                return false;
                            }
                        }
                    }
                }
                $('#ConfirmationModel').modal('hide');
                initializeDatePicker();
            }
            function callOnButtonNo() {
                if (IsBothYesNo == "True" || IsBothYesNo == 1) {
                    var r = confirm(nomessage);
                }
                else {
                    var count = 0;
                    var gvcheck = document.getElementById("<%=gridSubTask.ClientID %>");
                    var confirm_value = document.createElement("INPUT");
                    confirm_value.type = "hidden";
                    confirm_value.name = "confirm_value";
                    var gv = document.getElementById("<%= gridSubTask.ClientID %>");
                    var inputList = gv.getElementsByTagName("input");
                    for (var i = 0; i < inputList.length; i++) {
                        if (inputList[i].type == "checkbox" && inputList[i].checked) {
                            count = count + 1;
                        }
                    }

                    initializeDatePicker();

                    if ($('#lblNo').attr("href") == null || $('#lblNo').attr("href") == undefined || $('#lblNo').attr("href") == '') {
                        $('#taskslower').show();
                        $('#lblYes').hide();
                        $('#lblNo').hide();
                        $('#ConfirmationModel').modal('show');
                        return false;
                    }

                    else {
                        if (IsBothYesNo == "True" || IsBothYesNo == 1) {
                            var r = confirm(nomessage);
                        }
                        else {
                            var r = confirm(nomessage);

                            if (r == true) {
                                $('#ConfirmationModel').modal('hide');
                                return true;
                            } else {
                                $('#ConfirmationModel').modal('show');

                                return false;
                            }
                        }
                    }
                }
                $('#ConfirmationModel').modal('hide');
            }

            function fopendocfileReview(file) {
                $('#modalDocumentPerformerViewer').modal('show');
                $('#docViewerPerformerAll').attr('src', "../docviewer.aspx?docurl=" + file);
            }
            function downloadTaskSummary(obj)
            {
              var formonth=$(obj).attr('data-formonth');
              var instanceId = $(obj).attr('data-inid');
              var scheduleOnID = $(obj).attr('data-scid');
              $('#filedownload').attr("src","/task/downloadtaskdoc.aspx?taskScheduleOnID=" + scheduleOnID + "&r=" + Math.random());
              
            }
            $(document).ready(function () {
                $("button[data-dismiss-modal=modal2]").click(function () {
                    $('#modalDocumentPerformerViewer').modal('hide');
                });
            });
            for (var k = 0; k < $('.sbtask').length; k++) {
                var obj = $('.sbtask')[k];
                if ($(obj).attr('data-attr') == "True" || $(obj).attr('data-attr') == "1")
                {
                    $(obj).css("display", "block");
                }
            }
           function openTaskSummary(obj) {
              
               var taskInstanceID = $(obj).attr('data-inid');
               var taskScheduleOnID = $(obj).attr('data-scid');
                ShowDialog(taskInstanceID, taskScheduleOnID)
            } 

            function ShowDialog(taskInstanceID, taskScheduleOnID) {
                $('#DocumentPopUp').modal('show');
                $('.modal-dialog').css('width', '95%');
                $('#docViewerAll').attr('width', '100%');
                $('#docViewerAll').attr('height', '550px');
                $('#docViewerAll').attr('src', "/Task/TaskStatusTransactionPerformer.aspx?TID=" + taskInstanceID + "&TSOID=" + taskScheduleOnID);
            };
        </script>

        <%--<script type="text/javascript">

           function SelectheaderCheckboxes(headerchk) {
                debugger;
                var count = 0;
                var gvcheck = document.getElementById("<%=gridSubTask.ClientID %>");
                var confirm_value = document.createElement("INPUT");
                confirm_value.type = "hidden";
                confirm_value.name = "confirm_value";
                //my
                var gv = document.getElementById("<%= gridSubTask.ClientID %>");
                var inputList = gv.getElementsByTagName("input");

                for (var i = 0; i < inputList.length; i++) {
                    if (inputList[i].type == "checkbox" && inputList[i].checked) {
                        count = count + 1;
                    }
                }
                if (count > 0) {
                    if (confirm("Are you sure you want close subtask?")) {
                        confirm_value.value = "Yes";
                    }
                    else {
                        confirm_value.value = "No";
                    }
                    document.forms[0].appendChild(confirm_value);
                }
            }

            function fopendocfileReview(file) {
                $('#modalDocumentPerformerViewer').modal('show');
                $('#docViewerPerformerAll').attr('src', "../docviewer.aspx?docurl=" + file);
            }

            $(document).ready(function () {
                $("button[data-dismiss-modal=modal2]").click(function () {
                    $('#modalDocumentPerformerViewer').modal('hide');
                });

            });
        </script>--%>
        
    </form>
</body>
</html>
