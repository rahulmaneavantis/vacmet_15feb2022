﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NewCompliance.Master" AutoEventWireup="true" CodeBehind="TaskDocumentList.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Task.TaskDocumentList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(document).on("click", "#ContentPlaceHolder1_upDocumentDownload", function (event) {

            if (event.target.id == "") {
                var idvid = $(event.target).closest('div');
                if ($(idvid).attr('id') != undefined && $(idvid).attr('id') != '' && $(idvid).attr('id').indexOf('tvFilterLocation') > -1) {
                    $("#divFilterLocation").show();
                } else {
                    $("#divFilterLocation").hide();
                }
            }

            else if (event.target.id != "ContentPlaceHolder1_tbxFilterLocation") {
                $("#divFilterLocation").hide();
            } else if (event.target.id != "" && event.target.id.indexOf('tvFilterLocation') > -1) {
                $("#divFilterLocation").show();
            } else if (event.target.id == "ContentPlaceHolder1_tbxFilterLocation") {
                $("#ContentPlaceHolder1_tbxFilterLocation").unbind('click');

                $("#ContentPlaceHolder1_tbxFilterLocation").click(function () {
                    $("#divFilterLocation").toggle("blind", null, 500, function () { });
                });

            }
        });

        function initializeDatePicker11(date2) {
            var startDate = new Date();
            $('#<%= txtStartDate.ClientID %>').datepicker({
                dateFormat: 'dd-mm-yy',
                maxDate: startDate,
                numberOfMonths: 1
            });

            if (date2 != null) {
                $("#<%= txtStartDate.ClientID %>").datepicker("option", "defaultDate", date2);
            }
        }

        function initializeDatePicker12(date1) {
            var startDate = new Date();
            $('#<%= txtEndDate.ClientID %>').datepicker({
                dateFormat: 'dd-mm-yy',
                maxDate: startDate,
                numberOfMonths: 1
            });

            if (date1 != null) {
                $("#<%= txtEndDate.ClientID %>").datepicker("option", "defaultDate", date1);
            }
        }

        function ShowDownloadDocument() {
            $('#divDownloadDocument').modal('show');
            return true;
        };

        function ShowViewDocument() {
            $('#divViewDocument').modal('show');
            return true;
        };
        function hidediv() {
            var div = document.getElementById('AdvanceSearch');
            div.style.display == "none" ? "block" : "none";
            $('.modal-backdrop').hide();
            return true;
        }

        function showdiv() {
            var div = document.getElementById('AdvanceSearch');
            div.style.display == "block";
            $('.modal-backdrop').show();
            return true;
        }

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }

        function checkAll(cb) {
            var ctrls = document.getElementsByTagName('input');
            for (var i = 0; i < ctrls.length; i++) {
                var cbox = ctrls[i];
                if (cbox.type == "checkbox" && cbox.id.indexOf("chkAct") > -1) {
                    cbox.checked = cb.checked;
                }
            }
        }

        function UncheckHeader() {
            var rowCheckBox = $("#RepeaterTable input[id*='chkAct']");
            var rowCheckBoxSelected = $("#RepeaterTable input[id*='chkAct']:checked");
            var rowCheckBoxHeader = $("#RepeaterTable input[id*='actSelectAll']");
            if (rowCheckBox.length == rowCheckBoxSelected.length) {
                rowCheckBoxHeader[0].checked = true;
            } else {

                rowCheckBoxHeader[0].checked = false;
            }
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {

             fhead('My Documents / Task Documents');
              setactivemenu('TaskDocument');
            fmaters()

            //fhead('Task Documents');
            //setactivemenu('leftdocumentsmenu');
        });

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }
    </script>
    <script type="text/javascript">
        function fopendocfileReview(file) {

            $('#divViewDocument').modal('show');
            $('#ContentPlaceHolder1_docTaskViewer').attr('src', "../docviewer.aspx?docurl=" + file);
        }

        $(document).ready(function () {
            $("button[data-dismiss-modal=modal2]").click(function () {
                $('#divViewDocument').modal('hide');
            });

        });
    </script>
    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    </style>
    <style type="text/css">
        .ui-widget-header {
            border: 0px !important;
            background: inherit;
            font-size: 20px;
            color: #666666;
            font-weight: normal;
            padding-top: 0px;
            margin-top: 5px;
        }

        .table > tbody > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

            .table > tbody > tr > th > a {
                vertical-align: bottom;
                color: #666666 !important;
                text-decoration: none !important;
                border-bottom: 2px solid #dddddd;
            }

        .table tr th {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .clsheadergrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: bottom !important;
            border-bottom: 2px solid #dddddd !important;
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
            text-align: left;
        }

        .clsROWgrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: top !important;
            border-top: 1px solid #dddddd !important;
            color: #666666 !important;
            font-size: 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upDocumentDownload" runat="server" UpdateMode="Conditional" OnLoad="upDocumentDownload_Load">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel">
                              <header class="panel-heading tab-bg-primary ">
                                          <ul id="rblRole1" class="nav nav-tabs">
                                               <%if (Taskroles.Contains(3))%>
                                               <%{%>
                                            <li class="active" id="liPerformer" runat="server">
                                                <asp:LinkButton ID="lnkPerformer" OnClick="ShowPerformer" runat="server">Performer</asp:LinkButton>                                           
                                            </li>
                                               <%}%>
                                                <%if (Taskroles.Contains(4))%>
                                               <%{%>
                                            <li class=""  id="liReviewer" runat="server">
                                                <asp:LinkButton ID="lnkReviewer" OnClick="ShowReviewer" runat="server">Reviewer</asp:LinkButton>                                        
                                            </li>
                                              <%}%>
                                        </ul>
                                </header>

                              <div class="clearfix"></div>
                              <div class="panel-body">
                                <div class="col-md-12 colpadding0">
                                    <div class="col-md-2 colpadding0 entrycount">
                                        <div class="col-md-3 colpadding0" >
                                            <p style="color: #999; margin-top: 5px;">Show </p>
                                        </div>
                                        <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px; float: left" 
                                            AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" >
                                            <asp:ListItem Text="5" Selected="True"/>
                                            <asp:ListItem Text="10" />
                                            <asp:ListItem Text="20" />
                                            <asp:ListItem Text="50" />
                                        </asp:DropDownList> 
                                    </div>
                                    <div class="col-md-10 colpadding0" style="text-align: right; float: right">
                                        <div class="col-md-8 colpadding0">              
                                            <div style="float:left;margin-right: 2%;">
                                                <asp:DropDownList runat="server" ID="ddlDocType" class="form-control m-bot15 search-select" style="width:105px;" 
                                                    OnSelectedIndexChanged="ddlDocType_SelectedIndexChanged" AutoPostBack="true">                
                                                <asp:ListItem Text="Statutory" Value="1" />
                                                <asp:ListItem Text="Internal" Value="2" />
                                            </asp:DropDownList>         
                                            </div>
                                            <div style="float:left;margin-right: 2%;">
                                                <asp:DropDownList runat="server" ID="ddlStatus" class="form-control m-bot15 search-select"  style="width:105px;" >
                                                </asp:DropDownList>
                                            </div>
                                            <div style="float:left;margin-right: 4%;">
                                                <asp:TextBox runat="server" AutoCompleteType="None" ID="tbxFilterLocation" Style="padding: 0px;padding-left: 10px; margin: 0px; height: 35px; width: 285px; border: 1px solid #c7c7cc;border-radius: 4px;color:#8e8e93" CssClass="txtbox" />                                                       
                                                <div style="margin-left: 1px; position: absolute; z-index: 10;display: inherit;" id="divFilterLocation">
                                                    <asp:TreeView runat="server" ID="tvFilterLocation"   SelectedNodeStyle-Font-Bold="true"  Width="285px"   NodeStyle-ForeColor="#8e8e93"
                                                    Style="overflow: auto; border-left:1px solid #c7c7cc; border-right:1px solid #c7c7cc; border-bottom:1px solid #c7c7cc; background-color: #ffffff; color:#8e8e93 !important;" ShowLines="true" 
                                                        OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged">
                                                    </asp:TreeView>
                                                </div>  
                                            </div>
                                        </div>
                                        <div class="col-md-4 colpadding0">
                                        <div class="col-md-6 colpadding0" style="margin-left: 50px;width: 31%;">               
                                            <asp:Button ID="btnSearch" CausesValidation="false" class="btn btn-search"  runat="server" Text="Apply" OnClick="btnSearch_Click"/> 
                                        </div>
                                        <div class="col-md-6 colpadding0">
                                            <a class="btn btn-advanceSearch" data-toggle="modal" href="#AdvanceSearch" title="Search">Advanced Search</a>
                                        </div>
                                    </div>
                                    </div>  
                                    <!--advance search starts-->
                                    <div class="col-md-12 AdvanceSearchScrum">
                                        <div id="divAdvSearch" runat="server" visible="false">
                                            <p><asp:Label ID="lblAdvanceSearchScrum" runat="server" Text=""></asp:Label></p>
                                            <p> <asp:LinkButton ID="lnkClearAdvanceList" OnClick="lnkClearAdvanceList_Click"  runat="server">Clear Advanced Search Filter(s)</asp:LinkButton> </p>
                                        </div>
                                        <div runat="server" id="DivRecordsScrum" style="float: right;">
                                            <p style="padding-right: 0px !Important;">
                                                <asp:Label ID="Label4" runat="server" Text="Showing "></asp:Label>
                                                <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>- 
                                                <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                                                <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="modal fade" id="AdvanceSearch" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                       <div class="modal-dialog" style="width: 56%">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                </div>
                                                <div class="modal-body" style="margin-left: 40px;">
                                                    <h2 style="text-align: center">Advanced Search</h2>
                                                    <div class="col-md-12 colpadding0" style="display:none;">
                                                        <div class="table-advanceSearch-selectOpt">
                                                            <asp:DropDownListChosen runat="server" ID="ddlType" class="form-control" Width="95%" AllowSingleDeselect="false" DisableSearchThreshold="2">
                                                            </asp:DropDownListChosen>
                                                        </div>
                                                        <asp:Panel ID="Panelsubtype" runat="server">
                                                            <div id="DivComplianceSubTypeList" runat="server" class="table-advanceSearch-selectOpt">
                                                                <asp:DropDownListChosen runat="server" ID="ddlComplianceSubType" class="form-control" Width="95%"  AllowSingleDeselect="false" DisableSearchThreshold="2">
                                                                </asp:DropDownListChosen>
                                                            </div>
                                                        </asp:Panel>                                                   
                                                        <asp:Panel ID="PanelAct" runat="server">
                                                            <div id="DivAct" runat="server" class="table-advanceSearch-selectOpt">
                                                                <asp:DropDownListChosen runat="server" ID="ddlAct" class="form-control" Width="95%"  AllowSingleDeselect="false" DisableSearchThreshold="2">
                                                                </asp:DropDownListChosen>
                                                            </div>
                                                        </asp:Panel>
                                                        <div class="table-advanceSearch-selectOpt">
                                                            <asp:DropDownListChosen runat="server" ID="ddlCategory" class="form-control" Width="95%"  AllowSingleDeselect="false" DisableSearchThreshold="2">
                                                            </asp:DropDownListChosen>
                                                        </div>                                                   
                                                     </div>
                                                    <div class="clearfix"></div>
                                                    <div class="col-md-12 colpadding0" style="margin-top: 1%;">
                                                        <asp:Panel ID="PanelPerformerList" runat="server">
                                                            <div id="Div3" runat="server" class="table-advanceSearch-selectOpt" style="display:none;">
                                                                <asp:DropDownListChosen runat="server" ID="ddlPerfRevUser" class="form-control" Width="95%" 
                                                                    DataPlaceHolder="Select" AllowSingleDeselect="false" DisableSearchThreshold="2">
                                                                </asp:DropDownListChosen>
                                                            </div>
                                                        </asp:Panel>
                                                        <asp:Panel ID="Panel1" runat="server">
                                                            <div id="Div4" runat="server" class="table-advanceSearch-selectOpt">
                                                                <asp:TextBox runat="server" Height="35px" Width="95%" Style="padding-left: 7px; border-radius: 5px;" 
                                                                    placeholder="Due Date(From)" CssClass="form-control" ID="txtStartDate" />
                                                            </div>
                                                        </asp:Panel>
                                                        <asp:Panel ID="Panel2" runat="server">
                                                            <div id="Div5" runat="server" class="table-advanceSearch-selectOpt">
                                                                <asp:TextBox runat="server" Height="35px" Width="95%" Style="padding-left: 7px; border-radius: 5px;" 
                                                                    placeholder="Due Date(To)" CssClass="form-control" ID="txtEndDate" />
                                                            </div>
                                                        </asp:Panel>
                                                        <asp:Panel ID="PanelSearchType" runat="server">
                                                            <div id="Div6" runat="server" class="table-advanceSearch-selectOpt">
                                                                <asp:TextBox runat="server" Style="padding-left: 7px;" placeholder="Type to Filter" Width="95%"  
                                                                    ID="txtSearchType" CssClass="form-control" onkeydown="return (event.keyCode!=13);" />
                                                            </div>
                                                        </asp:Panel>                                                   
                                                      </div>
                                                    <div class="clearfix"></div>
                                                    <div class="table-advanceSearch-buttons" style="height: 30px; margin: 10px auto;">
                                                            <table id="tblSearch">
                                                                <tr>
                                                                    <td>
                                                                        <asp:Button ID="btnAdvSearch" Text="Search" class="btn btn-search" OnClick="btnAdvSearch_Click" runat="server" OnClientClick="return hidediv();" />
                                                                    </td>
                                                                    <td>
                                                                        <button id="btnAdvClose" type="button" class="btn btn-search" data-dismiss="modal">Close</button>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                </div>
                                            </div>
                                       </div>
                                    </div>
                                    <!--advance search ends-->
                                    <!-- Advance Search scrum-->
                                    <div class="clearfix"></div>         
                                    </div>
                               </div>
                              <div class="clearfix"></div>
                              <div style="margin-bottom: 4px">
                                <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                    ValidationGroup="TaskInstanceValidationGroup" Display="None" />
                              </div>

                              <div id="PerformerGrids" runat="server">
                                <div style="margin-bottom: 4px">
                                    <asp:GridView runat="server" ID="grdPerformerTaskDocument" AutoGenerateColumns="false" CssClass="table" GridLines="none" BorderWidth="0px"
                                        OnRowCommand="grdPerformerTaskDocument_RowCommand" CellPadding="4" Width="100%" OnRowDataBound="grdPerformerTaskDocument_RowDataBound"
                                        OnPageIndexChanging="grdPerformerTaskDocument_PageIndexChanging" DataKeyNames="TaskID" AllowPaging="True" PageSize="5" AutoPostBack="true">
                                        <Columns>
                                           <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left">
                                                <HeaderTemplate>
                                                    <asp:CheckBox ID="chkTaskHeader" runat="server" OnCheckedChanged="chkTaskHeader_CheckedChanged" AutoPostBack="true"  />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkTask" runat="server" OnCheckedChanged="chkTask_CheckedChanged" AutoPostBack="true" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                                <ItemTemplate>
                                                    <%#Container.DataItemIndex+1 %>
                                                </ItemTemplate>
                                            </asp:TemplateField>                                       
                                            <asp:TemplateField HeaderText="Location">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblBranch" runat="server" Text='<%# Eval("Branch") %>' ToolTip='<%# Eval("Branch") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Task">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width:200px; ">
                                                        <asp:Label ID="lblShortDesc" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("TaskTitle") %>' ToolTip='<%# Eval("TaskTitle") %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>                                           
                                            </asp:TemplateField>                                        
                                            <asp:TemplateField HeaderText="Reviewer">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">    
                                                    <asp:Label ID="lblReviewer" runat="server"  data-toggle="tooltip" data-placement="bottom"  Text='<%# GetReviewer((long)Eval("TaskScheduleOnID"), (long)Eval("TaskInstanceID"),(byte)Eval("TaskType")) %>' ToolTip='<%#Reviewername%>'></asp:Label>
                                                        </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                           <asp:TemplateField HeaderText="Due Date">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblScheduledOn" runat="server" Text='<%# Convert.ToDateTime(Eval("ScheduledOn")).ToString("dd-MMM-yyyy") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Period" HeaderStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <%# Eval("ForMonth")%>
                                                </ItemTemplate>
                                            </asp:TemplateField>                                     
                                               <asp:TemplateField HeaderText="Status">
                                                <ItemTemplate>
                                                      <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;"> 
                                                           <asp:Label ID="lblStatus" runat="server"  data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Status") %>' ToolTip='<%# Eval("Status") %>'></asp:Label>
                                                      </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>                                        
                                            <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:UpdatePanel ID="upDownloadFile" runat="server">
                                                       <ContentTemplate>
                                                    <asp:ImageButton ID="lblDownLoadfile" runat="server" ImageUrl="~/img/icon-download.png" CommandName="Download" 
                                                        CommandArgument='<%# Eval("TaskScheduleOnID") + " , " + Eval("TaskTransactionID") +" , "+Eval("FileID") %>' ToolTip="Download"></asp:ImageButton>
                                                             <asp:ImageButton ID="lblViewfile" runat="server" ImageUrl="~/Images/View-icon-new.png" CommandName="View" 
                                                        CommandArgument='<%# Eval("TaskScheduleOnID") + " , " + Eval("TaskTransactionID") +" , "+Eval("FileID") %>' ToolTip="View"></asp:ImageButton>
                                                    </ContentTemplate>
                                                        <Triggers>
                                                            <asp:PostBackTrigger ControlID="lblDownLoadfile" />
                                                            <asp:PostBackTrigger ControlID="lblViewfile" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <PagerStyle HorizontalAlign="Right" />
                                             <RowStyle CssClass="clsROWgrid"   />
                                          <HeaderStyle CssClass="clsheadergrid"    />
                                        <PagerTemplate>
                                            <table style="display: none">
                                                <tr>
                                                    <td>
                                                        <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                    </td>
                                                </tr>
                                            </table>
                                        </PagerTemplate>
                                         <EmptyDataTemplate>
                                            No Record Found
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                </div>
                              </div>

                              <div id="ReviewerGrids" runat="server">
                                <div style="margin-bottom: 4px">
                                    <asp:GridView runat="server" ID="grdReviewerTaskDocument" AutoGenerateColumns="false" CssClass="table" GridLines="None" BorderWidth="0px"
                                         CellPadding="4" Width="100%" DataKeyNames="TaskID" AllowPaging="True" PageSize="5" AutoPostBack="true" OnRowDataBound="grdReviewerTaskDocument_RowDataBound" 
                                        OnRowCommand="grdReviewerTaskDocument_RowCommand" OnPageIndexChanging="grdReviewerTaskDocument_PageIndexChanging">
                                        <Columns>
                                             <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left">
                                                <HeaderTemplate>
                                                    <asp:CheckBox ID="chkReviewerTaskHeader" runat="server" OnCheckedChanged="chkTaskHeader_CheckedChanged" AutoPostBack="true"  />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkReviewerTask" runat="server" OnCheckedChanged="chkTask_CheckedChanged" AutoPostBack="true" />
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                                <ItemTemplate>
                                                    <%#Container.DataItemIndex+1 %>
                                                </ItemTemplate>
                                            </asp:TemplateField>                                       
                                              <asp:TemplateField HeaderText="Location">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblBranch" runat="server" Text='<%# Eval("Branch") %>' ToolTip='<%# Eval("Branch") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Task">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width:200px; ">
                                                        <asp:Label ID="lblShortDesc" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("TaskTitle") %>' ToolTip='<%# Eval("TaskTitle") %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                                <ItemStyle Wrap="false" Width="200" /> 
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Performer">
                                                <ItemTemplate>
                                                     <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;"> 
                                                    <asp:Label ID="lblPerformer" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# GetPerformer((long)Eval("TaskScheduleOnID"), (long)Eval("TaskInstanceID"),(byte)Eval("TaskType")) %>' ToolTip='<%#Performername%>'></asp:Label>
                                                         </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Due Date">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblScheduledOn" runat="server" Text='<%# Convert.ToDateTime(Eval("ScheduledOn")).ToString("dd-MMM-yyyy") %>'></asp:Label>                                              
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Period" HeaderStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <%# Eval("ForMonth")%>
                                                </ItemTemplate>
                                            </asp:TemplateField>                                     

                                             <asp:TemplateField HeaderText="Status">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;"> 
                                                    <asp:Label ID="lblStatusReviewer" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Status") %>' ToolTip='<%# Eval("Status") %>'></asp:Label>
                                                        </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:UpdatePanel ID="upDownloadFileReviewer" runat="server">
                                                       <ContentTemplate>
                                                    <asp:ImageButton ID="lblDownLoadfileReviewer" runat="server" ImageUrl="~/img/icon-download.png" CommandName="Download" 
                                                        CommandArgument='<%# Eval("TaskScheduleOnID") + " , " + Eval("TaskTransactionID") +" , "+Eval("FileID") %>' ToolTip="Download"></asp:ImageButton>
                                                         <asp:ImageButton ID="lblViewfileReviewer" runat="server" ImageUrl="~/Images/View-icon-new.png" CommandName="View" 
                                                        CommandArgument='<%# Eval("TaskScheduleOnID") + " , " + Eval("TaskTransactionID") +" , "+Eval("FileID") %>' ToolTip="View"></asp:ImageButton>
                                                    </ContentTemplate>
                                                        <Triggers>
                                                            <asp:PostBackTrigger ControlID="lblDownLoadfileReviewer" />
                                                            <asp:PostBackTrigger ControlID="lblViewfileReviewer" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                        </Columns>
                                        <PagerStyle HorizontalAlign="Right" />
                                        <PagerTemplate>
                                            <table style="display: none">
                                                <tr>
                                                    <td>
                                                        <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                    </td>
                                                </tr>
                                            </table>
                                        </PagerTemplate>
                                        <EmptyDataTemplate>
                                            No Record Found
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                </div>
                              </div>

                             <div class="clearfix"></div>

                             <div class="col-md-12 colpadding0">
                                <div class="col-md-6 colpadding0">
                                    <div class="table-Selecteddownload">
                                        <div class="table-Selecteddownload-text">
                                            <p><asp:Label runat="server" ID="lblTotalSelected" Text="" style="color: #999; margin-right: 15px;"></asp:Label></p>
                                        </div>
                                         <asp:Button Text="Download" ID="btnDownload" class="btn btn-search" runat="server" OnClick="btnDownload_Click"/>   
                                    </div>
                                </div>

                                <div class="col-md-6 colpadding0">
                                    <div class="table-paging" style="margin-bottom: 20px;">
                                        <asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick="lBPrevious_Click"/>                                  
                                        <div class="table-paging-text">
                                            <p>
                                                <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                                <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>
                                            </p>
                                        </div>
                                        <asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick="lBNext_Click"/>                                   
                                        <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                    </div>
                                </div>
                             </div>
                        </section>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnDownload" />
        </Triggers>
    </asp:UpdatePanel>

    <div>
        <div class="modal fade" id="divDownloadDocument" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
            <div class="modal-dialog" style="width: 500px;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">
                        <table width="100%" style="text-align: left; margin-left: 25%;">
                            <thead>
                                <tr>
                                    <td valign="top">
                                        <asp:Repeater ID="rptDownloadTaskVersion" runat="server" OnItemCommand="rptDownloadTaskVersion_ItemCommand"
                                            OnItemDataBound="rptDownloadTaskVersion_ItemDataBound">
                                            <HeaderTemplate>
                                                <table id="tblDownloadTaskVersionDocumnets">
                                                    <thead>
                                                        <th>Versions</th>
                                                    </thead>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td>
                                                        <asp:LinkButton CommandName="version" CommandArgument='<%# Eval("TaskScheduleOnID") + ","+ Eval("Version") %>' ID="lblDownloadTaskDocumentVersion"
                                                            runat="server" Text='<%# Eval("Version")%>'></asp:LinkButton>
                                                    </td>
                                                    <td>
                                                        <asp:LinkButton class="btn btn-search" CommandName="Download" CommandArgument='<%# Eval("TaskScheduleOnID") + ","+ Eval("Version") + ","+ Eval("FileID") %>' OnClientClick='javascript:enableControls()'
                                                            ID="btnDownloadTaskVersionDoc" runat="server" Text="Download" Style="margin-top: 10%;">
                                                        </asp:LinkButton>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </table>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </td>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div>
        <div class="modal fade" id="divViewDocument" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
            <div class="modal-dialog" style="width: 70%;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body" style="height: 570px;">
                        <div style="width: 100%;">
                            <div style="float: left; width: 10%">
                                <table width="100%" style="text-align: left; margin-left: 25%;">
                                    <thead>
                                        <tr>
                                            <td valign="top">
                                                <asp:UpdatePanel ID="upTaskDetails1" runat="server" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <asp:Repeater ID="rptViewTaskVersion" runat="server"
                                                            OnItemCommand="rptViewTaskVersion_ItemCommand" OnItemDataBound="rptViewTaskVersion_ItemDataBound">
                                                            <HeaderTemplate>
                                                                <table id="tblViewTaskVersionDocumnets">
                                                                    <thead>
                                                                        <th>Versions</th>
                                                                    </thead>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td>
                                                                        <asp:LinkButton CommandName="View" CommandArgument='<%# Eval("TaskScheduleOnID") + ","+ Eval("Version") + ","+ Eval("FileID") %>' ID="lblViewTaskDocumentVersion"
                                                                            runat="server" ToolTip='<%# Eval("FileName")%>' Text='<%# Eval("Version") +" "+ Eval("FileName").ToString().Substring(0,4) %>'></asp:LinkButton>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                </table>
                                                            </FooterTemplate>
                                                        </asp:Repeater>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="rptViewTaskVersion" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                            <div style="float: left; width: 90%">
                                <asp:Label runat="server" ID="lblMessage" Style="color: red;"></asp:Label>
                                <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; height: 530px; width: 100%;">
                                    <iframe src="about:blank" id="docTaskViewer" runat="server" width="100%" height="510px"></iframe>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
