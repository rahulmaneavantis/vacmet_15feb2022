﻿<%@ Page Title="Task Details" Language="C#" MasterPageFile="~/NewCompliance.Master" AutoEventWireup="true" CodeBehind="PerformerTaskDetail.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Task.PerformerTaskDetail" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    </style>
    <style type="text/css">
        .dd_chk_select {
            height: 81px;
            height: 3px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc !important;
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family: Roboto sans-serif !important;
            margin-bottom: 0px !important;
        }

        .chosen-results {
            height: 75px !important;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>

    <script type="text/javascript">
        function fopenpopup() {
            $('#divTaskDetailsDialog').modal('show');
        }
        function OpenTaskType() {
            $('#divTaskTypeDetailDialog').modal('show');
        }
        function OpenSubTaskType() {
            $('#divSubTaskTypeDetailDialog').modal('show');
        }
        function fclosepopup() {
            $('#divTaskDetailsDialog').modal('hide');
        }

        function fopenpopup1() {
            $('#divAssignmentDetailsDialog').modal('show');
        }
        function fclosepopup1() {
            $('#divAssignmentDetailsDialog').modal('hide');
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:UpdatePanel ID="upTaskList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>

            
            <div>
                <asp:ValidationSummary runat="server" Display="none" class="alert alert-block alert-danger fade in"
                    ValidationGroup="ComplianceValidationGroupMainTask" />
                <asp:CustomValidator ID="CvMainTask" runat="server" EnableClientScript="False"
                    ValidationGroup="ComplianceValidationGroupMainTask" Display="None" class="alert alert-block alert-danger fade in" />
                <asp:Label runat="server" ID="Label3" ForeColor="Red"></asp:Label>
            </div>

            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel">                 
                            <div style="margin-bottom: 4px"/> 
                            <div class="col-md-2 colpadding0 entrycount" style="margin-top: 5px;">
                            <div class="col-md-3 colpadding0">
                                <p style="color: #999; margin-top: 5px;">Show </p>
                            </div>
                            <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px; float: left"
                                AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" >                        
                                <asp:ListItem Text="5"/>
                                <asp:ListItem Text="10" Selected="True" />
                                <asp:ListItem Text="20" />
                                <asp:ListItem Text="50" />
                            </asp:DropDownList>
                            </div>
                             <div class="col-md-2 colpadding0 entrycount" style="margin-top: 5px;">
                                   <asp:DropDownList runat="server" ID="ddlTask" class="form-control"
                                        AutoPostBack="true" OnSelectedIndexChanged="ddlTask_SelectedIndexChanged">
                                    </asp:DropDownList>
                              </div>
                             <div class="col-md-2 colpadding0 entrycount" style="margin-top: 5px; margin-left: 10px;">
                                    <asp:DropDownList runat="server" ID="ddlTaskType" class="form-control" AutoPostBack="true"
                                    OnSelectedIndexChanged="ddlTaskType_SelectedIndexChanged">
                                    <asp:ListItem Text="All" Value="-1" />
                                    <asp:ListItem Text="Statutory" Value="1" />
                                    <asp:ListItem Text="Internal" Value="2" />
                                    </asp:DropDownList>
                                 </div>
                            <div class="col-md-2 colpadding0 entrycount" style="margin-top: 5px; margin-left: 10px;"> 
                                <asp:TextBox runat="server" ID="tbxFilter" CssClass="form-control" MaxLength="50" PlaceHolder="Type to Search" AutoPostBack="true" Width="200px" OnTextChanged="tbxFilter_TextChanged" />                              
                            </div>
                             <div style="float:right;margin-right:5px; margin-top:-34px;">
                                    <asp:LinkButton runat="server" id="linkbutton" CssClass="btn btn-search" onclick="linkbutton_onclick" >Back</asp:LinkButton>  
                             </div>
                            <div style="float:right; margin-top:6px;">  
                            <asp:LinkButton Text="Add New" CssClass="btn btn-primary" runat="server" OnClientClick="fopenpopup()" ID="btnAddPromotor" OnClick="btnAddPromotor_Click" /> 
                            </div>

                            <div class="col-md-12 AdvanceSearchScrum">              
                            <div runat="server" id="DivRecordsScrum" style="float: right;">
                                 <p style="padding-top: 10px;">
                                    <asp:Label ID="Label4" runat="server" Text="Showing "></asp:Label>
                                    <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>- 
                                <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                                <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                                </p>
                            </div>
                        </div>   
                            <div style="margin-bottom: 4px">                                                 
                                <asp:GridView runat="server" ID="grdTask" AutoGenerateColumns="false" AllowSorting="true" 
                                    PageSize="10" AllowPaging="True"  AutoPostBack="true" CssClass="table" 
                                        GridLines="none" Width="100%"   DataKeyNames="ID" OnRowDataBound="grdTask_RowDataBound"
                                    OnRowCreated="grdTask_RowCreated" OnSorting="grdTask_Sorting" OnRowCommand="grdTask_RowCommand"
                                        OnPageIndexChanging="grdTask_PageIndexChanging">
                                    <Columns>
                                        <asp:BoundField DataField="ID" HeaderText="ID" ItemStyle-HorizontalAlign="Center"  />

                                        <asp:TemplateField HeaderText="Title"  ItemStyle-Width="200px">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                    <asp:Label runat="server" Text='<%# Eval("Title") %>' ToolTip='<%# Eval("Title") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Description">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                    <asp:Label runat="server" Text='<%# Eval("Description") %>' ToolTip='<%# Eval("Description") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:BoundField DataField="TaskTypeName" HeaderText="Task Type"   />

                                        <asp:TemplateField  HeaderText="Sub Task" >
                                            <ItemTemplate>
                                                <asp:LinkButton runat="server" Style="color:blue" CommandName="VIEW_TASKS" CommandArgument='<%# Eval("ID") %>'>sub-tasks</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbtEdit" runat="server" CommandName="EDIT_TASK" OnClientClick="fopenpopup()" CommandArgument='<%# Eval("ID") %>' ToolTip="Edit Task"><img src="../Images/edit_icon_new.png" alt="Edit Task"/></asp:LinkButton>
                                                <asp:LinkButton ID="LinkButton3" runat="server" CommandName="SHOW_ASSIGNMENT" OnClientClick="fopenpopup1()" CommandArgument='<%# Eval("ID") %>' ToolTip="Show Assingment">
                                                    <img src="../Images/View-icon-new.png" alt="Display Schedule Information"/></asp:LinkButton>
                                                <asp:LinkButton ID="lbtDelete" runat="server" CommandName="DELETE_TASK" CommandArgument='<%# Eval("ID") %>'
                                                    OnClientClick="return confirm('Are you certain you want to delete this Task?');" ToolTip="Delete Task"><img src="../Images/delete_icon_new.png" alt="Delete Task"/></asp:LinkButton>
                                            </ItemTemplate>
                                            <HeaderTemplate>
                                            </HeaderTemplate>
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>

                                    </Columns>
                                   <RowStyle CssClass="clsROWgrid"   />
                                <HeaderStyle CssClass="clsheadergrid"    />
                                <PagerTemplate>
                                <table style="display: none">
                                    <tr>
                                        <td>
                                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                        </td>
                                    </tr>
                                </table>
                                </PagerTemplate>
                                <EmptyDataTemplate>
                                    No Records Found.
                                </EmptyDataTemplate>
                                </asp:GridView>
                            </div>
                                
                             <div class="col-md-12 colpadding0">
                                <div class="col-md-6 colpadding0"></div>                                                        
                                <div class="col-md-6 colpadding0">
                                    <div class="table-paging" style="margin-bottom: 20px;">
                                        <asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick ="Previous_Click" />                           
                                        <div class="table-paging-text">
                                            <p>
                                                <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                                <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>
                                            </p>
                                        </div>
                                        <asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick ="Next_Click"/>            
                                        <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                    </div>
                                </div>
                            </div>
                       </section>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div class="modal fade" id="divTaskDetailsDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 650px;">
            <div class="modal-content" style="width: 650px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                </div>
                <div class="modal-body">
                    <div id="divTaskDetails">
                        <asp:UpdatePanel ID="upTaskDetails" runat="server" UpdateMode="Conditional" OnLoad="upTaskDetails_Load">
                            <ContentTemplate>
                                <div>
                                    <asp:ValidationSummary runat="server"
                                        ValidationGroup="ComplianceValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                                    <asp:CustomValidator ID="CustomValidator1" runat="server" EnableClientScript="False"
                                        ValidationGroup="ComplianceValidationGroup" Display="None" class="alert alert-block alert-danger fade in" />
                                    <asp:Label runat="server" ID="Label1" ForeColor="Red"></asp:Label>
                                </div>
                                <div>

                                    <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div style="margin-bottom: 7px" id="Div2" runat="server">
                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                                    Type
                                                </label>
                                                <asp:RadioButton ID="rdoStatutory" Text="Statutory" Font-Bold="true" ForeColor="Black" GroupName="radiotype" runat="server" AutoPostBack="true" OnCheckedChanged="rdoStatutory_CheckedChanged" Checked="true" />
                                                <asp:RadioButton ID="rdoInternal" Text="Internal" Font-Bold="true" ForeColor="Black" GroupName="radiotype" runat="server" AutoPostBack="true" OnCheckedChanged="rdoStatutory_CheckedChanged" />
                                            </div>
                                            <div runat="server" id="divStatutory">
                                                <div style="margin-bottom: 7px">
                                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                    <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                                        Act Filter
                                                    </label>
                                                    <asp:DropDownList runat="server" ID="ddlAct" CssClass="form-control"
                                                        Style="padding: 0px; margin: 0px; width: 390px;" AutoPostBack="true" OnSelectedIndexChanged="ddlAct_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                </div>

                                                <div style="margin-bottom: 7px">
                                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                    <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                                        Compliance
                                                    </label>
                                                    <asp:DropDownList runat="server" ID="ddlCompliance" OnSelectedIndexChanged="ddlCompliance_SelectedIndexChanged"
                                                        CssClass="form-control" Style="padding: 0px; margin: 0px; width: 390px;" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div runat="server" id="divInternal">
                                                <div style="margin-bottom: 7px">
                                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                    <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                                        Internal Compliance
                                                    </label>
                                                    <asp:DropDownList runat="server" ID="ddlInternalCompliance" OnSelectedIndexChanged="ddlInternalCompliance_SelectedIndexChanged"
                                                        CssClass="form-control" Style="padding: 0px; margin: 0px; width: 390px;" AutoPostBack="true" />
                                                </div>
                                            </div>

                                            <div style="margin-bottom: 7px">
                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                                <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                                    Compliance Type</label>
                                                <asp:TextBox runat="server" ID="txtComplianceType" Text="" ReadOnly="true" Style="width: 390px;" CssClass="form-control" />
                                            </div>

                                            <div style="margin-bottom: 7px;">
                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                                <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                                    Compliance Due Day</label>
                                                <asp:TextBox runat="server" ID="txtComplianceDueDays" Text="" ReadOnly="true" Style="width: 80px;" CssClass="form-control" />
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>

                                    <div style="margin-bottom: 7px; margin-top: 7px;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                            *</label>
                                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                            Task Title</label>
                                        <asp:TextBox runat="server" ID="txtTaskTitle" Style="width: 390px;" CssClass="form-control" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" ErrorMessage="Task title can not be empty."
                                            ControlToValidate="txtTaskTitle" runat="server" ValidationGroup="ComplianceValidationGroup"
                                            Display="None" />
                                    </div>

                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                            *</label>
                                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                            Description</label>
                                        <asp:TextBox runat="server" ID="txtDescription" TextMode="MultiLine" MaxLength="100" Style="width: 390px;" CssClass="form-control" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator14" ErrorMessage="Description can not be empty."
                                            ControlToValidate="txtDescription" runat="server" ValidationGroup="ComplianceValidationGroup"
                                            Display="None" />
                                    </div>

                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div style="margin-bottom: 7px" id="vivDueDate" runat="server">
                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                                    Due Day</label>
                                                <asp:TextBox runat="server" ID="txtDueDays" MaxLength="4" Width="80px" type="number" CssClass="form-control" />
                                                <asp:RequiredFieldValidator ID="rfvEventDue" ErrorMessage="Please enter Due(Day)"
                                                    ControlToValidate="txtDueDays" runat="server" ValidationGroup="ComplianceValidationGroup"
                                                    Display="None" />
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator5" Display="None" runat="server"
                                                    ValidationGroup="ComplianceValidationGroup" ErrorMessage="Please enter a valid Due Days."
                                                    ControlToValidate="txtDueDays" ValidationExpression="^[\+\d]+(?:[\d-.\s()]*)$"></asp:RegularExpressionValidator>
                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers" TargetControlID="txtDueDays" />
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>

                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                            Task mapping
                                        </label>
                                        <asp:TextBox runat="server" ID="txtTask" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                            CssClass="txtbox" />
                                        <div style="margin-left: 209px; position: absolute; z-index: 50; overflow-y: auto; background: white; width: 392px; border: 1px solid gray; height: 200px;" id="dvTask">
                                            <asp:Repeater ID="rptTask" runat="server">
                                                <HeaderTemplate>
                                                    <table class="detailstable FadeOutOnEdit" id="RepeaterTable">
                                                        <tr>
                                                            <td style="width: 100px;">
                                                                <asp:CheckBox ID="TaskSelectAll" Text="Select All" runat="server" onclick="checkAllTask(this)" /></td>
                                                            <td style="width: 282px;">
                                                                <asp:Button runat="server" ID="btnTaskRepeater" Text="Ok" Style="float: left" OnClick="btnTaskRepeater_Click" /></td>
                                                        </tr>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td style="width: 20px;">
                                                            <asp:CheckBox ID="chkTask" runat="server" onclick="UncheckTaskHeader();" /></td>
                                                        <td style="width: 470px;">
                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 460px; padding-bottom: 5px;">
                                                                <asp:Label ID="lblTaskID" runat="server" Visible="false" Text='<%# Eval("ID")%>' ToolTip='<%# Eval("ID") %>'></asp:Label>
                                                                <asp:Label ID="lblTaskName" runat="server" Text='<%# Eval("Title")%>' ToolTip='<%# Eval("Title") %>'></asp:Label>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </table>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                        </div>
                                    </div>

                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                            Task Type
                                        </label>
                                        <asp:DropDownList runat="server" ID="ddlAllTaskType" Style="padding: 0px; margin: 0px; width: 380px;" OnSelectedIndexChanged="ddlAllTaskType_SelectedIndexChanged"
                                            CssClass="form-control" AutoPostBack="true">
                                        </asp:DropDownList>
                                        <asp:LinkButton ID="lblAddTaskType" runat="server" Visible="false" OnClientClick="OpenTaskType()" OnClick="lblAddTaskType_Click" ToolTip="Add Task Type"><img src="../Images/add_icon_new.png" alt="Add Task Type" style="margin-top:-29px;float: right;"/></asp:LinkButton>
                                    </div>

                                    <div style="margin-bottom: 7px;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                            Sub Task Type
                                        </label>
                                        <asp:DropDownList runat="server" ID="ddlSubTaskType" Style="padding: 0px; margin: 0px; width: 380px;"
                                            CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSubTaskType_SelectedIndexChanged">
                                        </asp:DropDownList>
                                        <asp:LinkButton ID="lblAddSubTask" runat="server" Visible="false" OnClientClick="OpenSubTaskType()" OnClick="lblAddSubTask_Click" ToolTip="Add SubTask Type"><img src="../Images/add_icon_new.png" alt="Add SubTask Type" style="margin-top:-29px;float: right;"/></asp:LinkButton>
                                    </div>

                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                            IsAfter/Before
                                        </label>
                                        <asp:DropDownList runat="server" ID="ddlIsAfter" Style="padding: 0px; margin: 0px; width: 380px;"
                                            CssClass="form-control" AutoPostBack="true">
                                            <asp:ListItem Text="Before" Value="False" />
                                            <asp:ListItem Text="After" Value="True" />
                                        </asp:DropDownList>
                                    </div>

                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                            Is Task Conditional
                                        </label>
                                        <asp:CheckBox ID="ChkIsTaskConditional" runat="server" />
                                    </div>
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                            Conditional Message</label>
                                        <asp:TextBox runat="server" ID="TxtConditionalMessage" Style="width: 390px;" CssClass="form-control"  />
                                    </div>
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                            True Condtion</label>
                                        <asp:RadioButtonList ID="rbTrueCondtion" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem Text="Yes" Value="0" Selected="True" />
                                            <asp:ListItem Text="No" Value="1" />
                                        </asp:RadioButtonList>
                                    </div>
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                            Display Both Message</label>
                                        <asp:RadioButtonList ID="rbBothMessage" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem Text="Yes" Value="0" />
                                            <asp:ListItem Text="No" Value="1" Selected="True"  />
                                        </asp:RadioButtonList>
                                    </div>
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                            Yes Message</label>
                                        <asp:TextBox runat="server" ID="txtYesMessage" Style="width: 390px;" CssClass="form-control" />
                                    </div>
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                            No Message</label>
                                        <asp:TextBox runat="server" ID="txtNoMessage" Style="width: 390px;" CssClass="form-control"/>
                                    </div>

                                    <asp:UpdatePanel ID="upFileUploadPanel" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div style="margin-bottom: 7px">
                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                                    Sample Form</label>
                                                <asp:Label runat="server" ID="lblSampleForm" CssClass="txtbox" Style="color: black;" />
                                                <br />
                                                <asp:FileUpload runat="server" ID="fuSampleFile" Style="color: black;" />
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>

                                    <div style="margin-bottom: 7px; text-align: center; margin-top: 5%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="btn btn-primary"
                                            ValidationGroup="ComplianceValidationGroup" OnClientClick="fopenpopup()" />
                                        <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="btn btn-primary" OnClientClick="fclosepopup()" />
                                    </div>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="btnSave" />
                            </Triggers>
                        </asp:UpdatePanel>

                        <div style="margin-bottom: 7px; margin-left: 10px; margin-top: 30px;">
                            <p style="color: red;"><strong>Note:</strong> (*) fields are Compulsory</p>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="divAssignmentDetailsDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 800px;">
            <div class="modal-content" style="width: 800px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                </div>
                <div class="modal-body">
                    <div id="divAssignmentDetails">
                        <asp:UpdatePanel ID="upTaskAssignmentDetails" runat="server" UpdateMode="Conditional" OnLoad="upTaskAssignmentDetails_Load">
                            <ContentTemplate>
                                <div style="margin: 5px 5px 80px;">
                                    <div style="margin-bottom: 4px">
                                        <asp:ValidationSummary runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                            ValidationGroup="TaskAssignmentValidationGroup" />
                                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                            class="alert alert-block alert-danger fade in" ValidationGroup="TaskAssignmentValidationGroup1" Display="None" />
                                        <asp:Label runat="server" ID="lblErrorMassage" ForeColor="Red"></asp:Label>
                                    </div>
                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div style="width: 100%; float: left; margin-right: 2%;" runat="server" id="Divbranchlist">
                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                                   Compliance Location
                                                </label>
                                                <div style="width: 50%; float: left; margin-bottom: 15px">
                                                    <asp:TextBox runat="server" AutoCompleteType="None" ID="tbxFilterLocation" PlaceHolder="Select Compliance Applicable Location" autocomplete="off"
                                                        Style="padding: 0px; padding-left: 10px; margin: 0px; height: 35px; width: 390px; border: 1px solid #c7c7cc; border-radius: 4px; color: #8e8e93"
                                                        CssClass="txtbox" />
                                                    <div style="margin-left: 1px; position: absolute; z-index: 10; display: inherit;" id="divFilterLocation">
                                                        <asp:TreeView runat="server" ID="tvFilterLocation" ShowCheckBoxes="All" Width="390px" NodeStyle-ForeColor="#8e8e93"
                                                            Style="overflow: auto; height: 250px; border-left: 1px solid #c7c7cc; border-right: 1px solid #c7c7cc; border-bottom: 1px solid #c7c7cc; background-color: #ffffff; color: #8e8e93 !important;" ShowLines="true">
                                                        </asp:TreeView>
                                                    </div>
                                                </div>

                                                <div style="width: 50%; float: right;">
                                                </div>
                                            </div>

                                             <div style="width: 100%; float: left; margin-right: 2%;" runat="server" id="DivbranchlistTask">
                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                                   Reporting Location
                                                </label>
                                                <div style="width: 50%; float: left; margin-bottom: 15px">
                                                    <asp:TextBox runat="server" AutoCompleteType="None" ID="tbxFilterLocationTask" PlaceHolder="Select Task Applicable Location" autocomplete="off"
                                                        Style="padding: 0px; padding-left: 10px; margin: 0px; height: 35px; width: 390px; border: 1px solid #c7c7cc; border-radius: 4px; color: #8e8e93"
                                                        CssClass="txtbox" />
                                                    <div style="margin-left: 1px; position: absolute; z-index: 10; display: inherit;" id="divFilterLocationTask">
                                                        <asp:TreeView runat="server" ID="tvFilterLocationTask" Width="390px" NodeStyle-ForeColor="#8e8e93"
                                                            OnSelectedNodeChanged="tvFilterLocationTask_SelectedNodeChanged" Style="overflow: auto; height: 250px; border-left: 1px solid #c7c7cc; border-right: 1px solid #c7c7cc; border-bottom: 1px solid #c7c7cc; background-color: #ffffff; color: #8e8e93 !important;" ShowLines="true">
                                                        </asp:TreeView>
                                                        <asp:CompareValidator ControlToValidate="tbxFilterLocationTask" ErrorMessage="Please select Task Applicable Location."
                                                            runat="server" ValueToCompare="Select Task Applicable Location" Operator="NotEqual" ValidationGroup="TaskAssignmentValidationGroup"
                                                            Display="None" />
                                                    </div>
                                                </div>

                                                <div style="width: 50%; float: right;">
                                                </div>
                                            </div>
                                            <div style="margin-bottom: 7px">
                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                                    Start Date</label>
                                                <asp:TextBox runat="server" ID="tbxStartDate" Style="width: 390px;" CssClass="form-control" AutoPostBack="true" />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Please select start date"
                                                    ControlToValidate="tbxStartDate" runat="server" ValidationGroup="TaskAssignmentValidationGroup"
                                                    Display="None" />
                                            </div>

                                            <div style="margin-bottom: 7px">
                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                                    Performer</label>
                                                <asp:DropDownListChosen runat="server" ID="ddlPerformer" Width="390px" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                    CssClass="form-control">
                                                </asp:DropDownListChosen>
                                            </div>
                                            <div style="margin-bottom: 7px">
                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                                    Reviewer</label>
                                                <asp:DropDownListChosen runat="server" ID="ddlReviewer" Width="390px" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                    CssClass="form-control">
                                                </asp:DropDownListChosen>
                                            </div>
                                            <div style="margin-bottom: 7px; text-align: center; margin-top: 10px;">
                                                <asp:Button Text="Save" runat="server" ID="btnSaveTaskAssignment" OnClick="btnSaveTaskAssignment_Click" CssClass="btn btn-primary"
                                                    ValidationGroup="TaskAssignmentValidationGroup" />
                                            </div>
                                            <div style="margin-bottom: 7px">
                                                <asp:Panel ID="Panel2" Width="100%" runat="server" Style="min-height: 250px; max-height: 400px; width: 100%; overflow-y: auto;">
                                                    <asp:GridView runat="server" ID="GrdAssigment" AutoGenerateColumns="false" GridLines="None" ShowHeaderWhenEmpty="true"
                                                        AllowSorting="true"
                                                        DataKeyNames="TaskAssignmentID,UserID" OnRowEditing="GrdAssigment_RowEditing" OnRowCancelingEdit="GrdAssigment_RowCancelingEdit"
                                                        OnRowUpdating="GrdAssigment_RowUpdating" OnRowDataBound="GrdAssigment_RowDataBound" AllowPaging="True" PageSize="100" Width="100%">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Compliance Branch">
                                                                <ItemTemplate>
                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                                        <asp:Label ID="lblBranch" runat="server" Text='<%# Eval("Branch")%>'></asp:Label>
                                                                    </div>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                              <asp:TemplateField HeaderText="Reporting Location">
                                                                <ItemTemplate>
                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                                        <asp:Label ID="lblTaskBranch" runat="server" Text='<%# Eval("TaskCustomerBranchName")%>'></asp:Label>
                                                                    </div>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Start Date">
                                                                <ItemTemplate>
                                                                    <%# Eval("ScheduledOn")!= null?((DateTime)Eval("ScheduledOn")).ToString("dd-MMM-yyyy"):""%>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="User">
                                                                <ItemTemplate>
                                                                    <div style="overflow: hidden; width: 100px; text-overflow: ellipsis; white-space: nowrap;">
                                                                        <asp:Label ID="Label2" runat="server" Text='<%# Eval("User") %>' data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("User") %>'></asp:Label>
                                                                    </div>
                                                                </ItemTemplate>
                                                                <EditItemTemplate>
                                                                    <div style="overflow: hidden; width: 100px; text-overflow: ellipsis; white-space: nowrap;">
                                                                        <asp:DropDownList ID="ddlUserList" Width="100px" runat="server">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </EditItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Role">
                                                                <ItemTemplate>
                                                                    <div style="overflow: hidden; width: 100px; text-overflow: ellipsis; white-space: nowrap;">
                                                                        <asp:Label ID="lblRole" runat="server" Text='<%# Eval("Role") %>'></asp:Label>
                                                                    </div>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <%--<asp:TemplateField HeaderText="Edit" ShowHeader="false">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="btnedit" runat="server" CommandName="Edit" Text="Edit"></asp:LinkButton>
                                                                </ItemTemplate>
                                                                <EditItemTemplate>
                                                                    <asp:LinkButton ID="btnupdate" runat="server" CommandName="Update" Text="Update"></asp:LinkButton>
                                                                    <asp:LinkButton ID="btncancel" runat="server" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                                                                </EditItemTemplate>
                                                            </asp:TemplateField>--%>
                                                        </Columns>
                                                        <RowStyle CssClass="clsROWgrid" />
                                                        <HeaderStyle CssClass="clsheadergrid" />
                                                        <PagerTemplate>
                                                            <table style="display: none">
                                                                <tr>
                                                                    <td>
                                                                        <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </PagerTemplate>
                                                        <EmptyDataTemplate>
                                                            No Records Found.
                                                        </EmptyDataTemplate>
                                                    </asp:GridView>
                                                </asp:Panel>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <div style="margin-bottom: 7px; margin-left: 10px; margin-top: 30px;">
                                        <p style="color: red;"><strong>Note:</strong> (*) Fields Are Compulsary</p>
                                    </div>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="divTaskTypeDetailDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 500px;">
            <div class="modal-content" style="width: 500px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                </div>
                <div class="modal-body">
                    <div id="divTaskTypeDetail">
                        <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional" OnLoad="UpdatePanel4_Load">
                            <ContentTemplate>
                                <div>
                                    <asp:ValidationSummary runat="server"
                                        ValidationGroup="ComValidationAddTaskType" Display="none" class="alert alert-block alert-danger fade in" />
                                    <asp:CustomValidator ID="CuValAddTaskType" runat="server" EnableClientScript="False"
                                        ValidationGroup="ComValidationAddTaskType" Display="None" />
                                    <asp:Label runat="server" ID="Label5" ForeColor="Red"></asp:Label>
                                </div>
                                <div style="margin: 5px 5px 80px;">
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                            *</label>
                                        <label style="width: 100px; display: block; float: left; font-size: 13px; color: #333;">
                                            Task Type</label>
                                        <asp:TextBox runat="server" ID="tbxTaskType" Style="width: 300px;" CssClass="form-control" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Task type can not be empty."
                                            ControlToValidate="tbxTaskType" runat="server" ValidationGroup="ComValidationAddTaskType"
                                            Display="None" />
                                    </div>
                                    <div style="margin-bottom: 7px; text-align: center; margin-top: 5%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <asp:Button Text="Save" runat="server" ID="btnAddTaskType" CssClass="btn btn-primary" OnClick="btnAddTaskType_Click"
                                            ValidationGroup="ComValidationAddTaskType" />
                                        <asp:Button Text="Close" runat="server" ID="Close" CssClass="btn btn-primary" data-dismiss="modal" />
                                    </div>
                                    <div style="margin-bottom: 7px; margin-left: 10px; margin-top: 30px;">
                                        <p style="color: red;"><strong>Note:</strong> (*) Fields Are Compulsary</p>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="divSubTaskTypeDetailDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 500px;">
            <div class="modal-content" style="width: 500px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                </div>
                <div class="modal-body">
                    <div id="divSubTaskTypeDetail">
                        <asp:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Conditional" OnLoad="UpdatePanel5_Load">
                            <ContentTemplate>
                                <div>
                                    <asp:ValidationSummary runat="server"
                                        ValidationGroup="ComValidationAddSubTaskType" Display="none" class="alert alert-block alert-danger fade in" />

                                    <asp:CustomValidator ID="CuValSubTaskType" runat="server" EnableClientScript="False"
                                        ValidationGroup="ComValidationAddSubTaskType" Display="None" />
                                    <asp:Label runat="server" ID="Label6" ForeColor="Red"></asp:Label>
                                </div>
                                <div style="margin: 5px 5px 80px;">
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                            *</label>
                                        <label style="width: 100px; display: block; float: left; font-size: 13px; color: #333;">
                                            Sub Task Type</label>
                                        <asp:TextBox runat="server" ID="tbxSubTaskType" Style="width: 300px;" CssClass="form-control" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="Sub task type can not be empty."
                                            ControlToValidate="tbxSubTaskType" runat="server" ValidationGroup="ComValidationAddSubTaskType"
                                            Display="None" />
                                    </div>
                                    <div style="margin-bottom: 7px; text-align: center; margin-top: 5%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <asp:Button Text="Save" runat="server" ID="btnTaskSubType" CssClass="btn btn-primary" OnClick="btnTaskSubType_Click"
                                            ValidationGroup="ComValidationAddSubTaskType" />
                                        <asp:Button Text="Close" runat="server" ID="btnCloseSubTaskType" CssClass="btn btn-primary" data-dismiss="modal" />
                                    </div>
                                    <div style="margin-bottom: 7px; margin-left: 10px; margin-top: 30px;">
                                        <p style="color: red;"><strong>Note:</strong> (*) Fields Are Compulsary</p>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">

        function initializeConfirmDatePicker(date) {
            var startDate = new Date();
            $('#<%= tbxStartDate.ClientID %>').datepicker({
                dateFormat: 'dd-mm-yy',
                numberOfMonths: 1
            });
        }


        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');
            $("#" + textBoxID).click(function () {
                initializeJQueryUI
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }

        $(function () {
            $("[id*=tvFilterLocation] input[type=checkbox]").bind("click", function () {
                var table = $(this).closest("table");
                if (table.next().length > 0 && table.next()[0].tagName == "DIV") {
                    //Is Parent CheckBox
                    var childDiv = table.next();
                    var isChecked = $(this).is(":checked");
                    $("input[type=checkbox]", childDiv).each(function () {
                        if (isChecked) {
                            $(this).attr("checked", "checked");
                        } else {
                            $(this).removeAttr("checked");
                        }
                    });
                } else {
                    //Is Child CheckBox
                    var parentDIV = $(this).closest("DIV");
                    if ($("input[type=checkbox]", parentDIV).length == $("input[type=checkbox]:checked", parentDIV).length) {
                        $("input[type=checkbox]", parentDIV.prev()).attr("checked", "checked");
                    } else {
                        $("input[type=checkbox]", parentDIV.prev()).removeAttr("checked");
                    }
                }
            });
        })

    </script>

    <script language="javascript" type="text/javascript">

        function UncheckTaskHeader() {
            var rowCheckBox = $("#RepeaterTable input[id*='chkTask']");
            var rowCheckBoxSelected = $("#RepeaterTable input[id*='chkTask']:checked");
            var rowCheckBoxHeader = $("#RepeaterTable input[id*='TaskSelectAll']");
            if (rowCheckBox.length == rowCheckBoxSelected.length) {
                rowCheckBoxHeader[0].checked = true;
            } else {

                rowCheckBoxHeader[0].checked = false;
            }
        }
        function checkAllTask(cb) {
            var ctrls = document.getElementsByTagName('input');
            for (var i = 0; i < ctrls.length; i++) {
                var cbox = ctrls[i];
                if (cbox.type == "checkbox" && cbox.id.indexOf("chkTask") > -1) {
                    cbox.checked = cb.checked;
                }
            }
        }

        function OnTreeClick(evt) {
            var src = window.event != window.undefined ? window.event.srcElement : evt.target;
            var isChkBoxClick = (src.tagName.toLowerCase() == "input" && src.type == "checkbox");
            if (isChkBoxClick) {
                var parentTable = GetParentByTagName("table", src);
                var nxtSibling = parentTable.nextSibling;
                if (nxtSibling && nxtSibling.nodeType == 1)//check if nxt sibling is not null & is an element node
                {
                    if (nxtSibling.tagName.toLowerCase() == "div") //if node has children
                    {
                        //check or uncheck children at all levels
                        CheckUncheckChildren(parentTable.nextSibling, src.checked);
                    }
                }
                //check or uncheck parents at all levels
                CheckUncheckParents(src, src.checked);
            }
        }

        function CheckUncheckChildren(childContainer, check) {
            var childChkBoxes = childContainer.getElementsByTagName("input");
            var childChkBoxCount = childChkBoxes.length;
            for (var i = 0; i < childChkBoxCount; i++) {
                childChkBoxes[i].checked = check;
            }
        }

        function CheckUncheckParents(srcChild, check) {
            var parentDiv = GetParentByTagName("div", srcChild);
            var parentNodeTable = parentDiv.previousSibling;

            if (parentNodeTable) {
                var checkUncheckSwitch;

                if (check) //checkbox checked
                {
                    var isAllSiblingsChecked = AreAllSiblingsChecked(srcChild);
                    if (isAllSiblingsChecked)
                        checkUncheckSwitch = true;
                    else
                        return; //do not need to check parent if any(one or more) child not checked
                }
                else //checkbox unchecked
                {
                    checkUncheckSwitch = false;
                }

                var inpElemsInParentTable = parentNodeTable.getElementsByTagName("input");
                if (inpElemsInParentTable.length > 0) {
                    var parentNodeChkBox = inpElemsInParentTable[0];
                    parentNodeChkBox.checked = checkUncheckSwitch;
                    //do the same recursively
                    CheckUncheckParents(parentNodeChkBox, checkUncheckSwitch);
                }
            }
        }

        function AreAllSiblingsChecked(chkBox) {
            var parentDiv = GetParentByTagName("div", chkBox);
            var childCount = parentDiv.childNodes.length;
            for (var i = 0; i < childCount; i++) {
                if (parentDiv.childNodes[i].nodeType == 1) //check if the child node is an element node
                {
                    if (parentDiv.childNodes[i].tagName.toLowerCase() == "table") {
                        var prevChkBox = parentDiv.childNodes[i].getElementsByTagName("input")[0];
                        //if any of sibling nodes are not checked, return false
                        if (!prevChkBox.checked) {
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        //utility function to get the container of an element by tagname
        function GetParentByTagName(parentTagName, childElementObj) {
            var parent = childElementObj.parentNode;
            while (parent.tagName.toLowerCase() != parentTagName.toLowerCase()) {
                parent = parent.parentNode;
            }
            return parent;
        }

    </script>

    <script type="text/javascript">
        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);
        window.onunload = function () { null };


        $(document).ready(function () {
            setactivemenu('Task Details');
            fhead('Task Details');

        });
    </script>
</asp:Content>
