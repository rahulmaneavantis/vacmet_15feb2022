﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Task
{
    public partial class TaskDocumentListNew : System.Web.UI.Page
    {
        public static string CompDocReviewPath = "";
        protected static string CId;
        protected static string UserId;
        protected static int CustId;
        protected static int UId;
        protected static List<Int32> roles;
        //protected static int RoleDropdDisplay;
        protected static int RoleID;
        protected static int RoleFlag;
        protected static string Falg;
        protected static bool DisableFalg;
        protected static String SDate;
        protected static String LDate;
        protected static String IsMonthID;
        protected static string Path;
        protected static string UploadDocumentLink;
        protected static string Authorization;
        protected void Page_Load(object sender, EventArgs e)
        {
            int CacheClearTime = Convert.ToInt32(ConfigurationManager.AppSettings["CacheClearTimeToken"]);
            string CacheName = "CacheGetTokenData_" + Convert.ToString(AuthenticationHelper.UserID) + "_" + Convert.ToString(AuthenticationHelper.CustomerID);
            Authorization = (string)HttpContext.Current.Cache[CacheName];
            if (Authorization == null)
            {
                Authorization = Business.ComplianceManagement.getToken(Convert.ToString(AuthenticationHelper.UserID));
                HttpContext.Current.Cache.Insert(CacheName, Authorization, null, DateTime.Now.AddMinutes(CacheClearTime), System.Web.Caching.Cache.NoSlidingExpiration); // add it to cache
            }

            string listCustomer = ConfigurationManager.AppSettings["UploadUsingLinkCustomerlist"];
            string[] listCust = new string[] { };
            if (!string.IsNullOrEmpty(listCustomer))
                listCust = listCustomer.Split(',');

            if (listCust.Contains(Convert.ToString(AuthenticationHelper.CustomerID)))
                UploadDocumentLink = "1";
            else
                UploadDocumentLink = "0";

            Path = ConfigurationManager.AppSettings["KendoPathApp"];
            //Path = "http://avacompcdr.cloudapp.net/AndroidAppServiceData/";
            CId = Convert.ToString(AuthenticationHelper.CustomerID);
            UserId = Convert.ToString(AuthenticationHelper.UserID);
            UId = Convert.ToInt32(AuthenticationHelper.UserID);
            CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            RoleID = ActManagement.GetUserRoleID(AuthenticationHelper.UserID);
            RoleFlag = 0;
            IsMonthID = "3";
            if (AuthenticationHelper.Role == "MGMT")
            {
                Falg = "MGMT";
                RoleFlag = 1;
                DisableFalg = false;
            }
            if (AuthenticationHelper.Role == "AUDT")
            {
                IsMonthID = "AUD";
                SDate = Convert.ToDateTime(Session["Auditstartdate"].ToString()).Date.ToString("dd-MM-yyyy");
                LDate = Convert.ToDateTime(Session["Auditenddate"].ToString()).Date.ToString("dd-MM-yyyy");
                Falg = "AUD";
                DisableFalg = true;
            }
            else if (AuthenticationHelper.Role == "EXCT")
            {
                DisableFalg = false;
                Falg = "PRA";
                roles = CustomerBranchManagement.GetAssignedroleid(AuthenticationHelper.UserID);

                if (roles.Contains(4))
                {
                    RoleFlag = 1;
                }
            }
            else if (AuthenticationHelper.Role == "CADMN")
            {
                DisableFalg = false;
                Falg = "PRA";
                roles = CustomerBranchManagement.GetAssignedroleid(AuthenticationHelper.UserID);

                if (roles.Contains(4))
                {
                    RoleFlag = 1;
                }
            }
        }
    }
}