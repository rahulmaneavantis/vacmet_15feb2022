﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using Ionic.Zip;
using System.IO;
using System.Collections;
using System.Globalization;
using System.Configuration;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Data;
using System.Drawing;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Task
{
    public partial class TaskReport : System.Web.UI.Page
    {
        static bool PerformerFlag;
        static bool ReviewerFlag;

        protected static List<Int32> Taskroles;
        protected string Reviewername;
        protected string Performername;

        protected string InternalReviewername;
        protected string InternalPerformername;
        //static int CustomerID;
        public static string CompDocReviewPath = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    PerformerFlag = false;
                    ReviewerFlag = false;

                    ddlAct.Enabled = true;

                    //CustomerID = Convert.ToInt32(UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID);

                    BindStatus();
                    BindLocationFilter();

                    if (AuthenticationHelper.CustomerID == 63)
                    {
                        ddlTaskType.SelectedValue = "2";
                        ddlTaskType.Items.Remove(ddlTaskType.Items.FindByValue("1"));
                    }
                   
                    bool isManagement = false;
                    if (AuthenticationHelper.Role.Contains("MGMT"))
                    {
                        isManagement = true;
                        PerformerFlag = true;
                    }

                    if (ddlTaskType.SelectedValue == "1") //1--Statutory
                    {
                        ddlAct.ClearSelection();
                        BindActList(isManagement);
                        BindTypes(isManagement);
                        BindCategories(isManagement);
                    }
                    else if (ddlTaskType.SelectedValue == "2") //2--Internal
                    {
                        BindInternalActList(isManagement); ;
                        BindTypesInternal(isManagement);
                        BindCategoriesInternal(isManagement);
                    }

                    BindComplianceSubTypeList(isManagement);

                    Taskroles = TaskManagment.GetAssignedTaskUserRole(AuthenticationHelper.UserID);

                    if (Taskroles.Contains(3))
                    {
                        PerformerFlag = true;
                        ShowPerformer(sender, e);
                    }
                    else if (Taskroles.Contains(4))
                    {
                        ReviewerFlag = true;
                        ShowReviewer(sender, e);
                    }
                    else
                    {
                        PerformerFlag = true;
                        ShowPerformer(sender, e);
                    }

                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
            }
        }

        private void BindStatus()
        {
            try
            {
                foreach (CannedReportFilterNewStatus r in Enum.GetValues(typeof(CannedReportFilterNewStatus)))
                {
                    ListItem item = new ListItem(Enum.GetName(typeof(CannedReportFilterNewStatus), r), r.ToString());
                    ddlStatus.Items.Add(item);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindLocationFilter()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                tvFilterLocation.Nodes.Clear();
                var bracnhes = TaskManagment.GetAllHierarchySatutory(customerID);
                var LocationList = TaskManagment.GetAssignedLocationList(AuthenticationHelper.UserID, customerID, AuthenticationHelper.Role);

                TreeNode node = new TreeNode("Entity/Sub-Entity/Location", "-1");
                node.Selected = true;
                tvFilterLocation.Nodes.Add(node);

                foreach (var item in bracnhes)
                {
                    node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    CustomerBranchManagement.BindBranchesHierarchy(node, item, LocationList);
                    // BindBranchesHierarchy(node, item, LocationList);
                    tvFilterLocation.Nodes.Add(node);
                }

                tvFilterLocation.CollapseAll();
                tvFilterLocation_SelectedNodeChanged(null, null);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp)
        {
            try
            {
                List<int> ListIDs = new List<int>();
                ListIDs.Add(10);
                ListIDs.Add(12);
                foreach (var item in nvp.Children)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());

                    if (FindNodeExists(item, ListIDs))
                    {
                        BindBranchesHierarchy(node, item);
                        parent.ChildNodes.Add(node);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindTypesInternal(bool ismgmt)
        {
            try
            {
                ddlType.DataSource = null;
                ddlType.DataBind();

                ddlType.DataTextField = "Name";
                ddlType.DataValueField = "ID";
                if (ismgmt)
                {
                    var TypleList = TaskManagment.GetAllAssignedInternalType(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "MGMT");
                    ddlType.DataSource = TypleList;
                    ddlType.DataBind();
                }
                else
                {
                    var TypleList = TaskManagment.GetAllAssignedInternalType(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "PR");
                    ddlType.DataSource = TypleList;
                    ddlType.DataBind();
                }
                ddlType.Items.Insert(0, new ListItem("Select Type", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindTypes(bool ismgmt)
        {
            try
            {
                ddlType.DataSource = null;
                ddlType.DataBind();
                ddlType.DataTextField = "Name";
                ddlType.DataValueField = "ID";
                if (ismgmt)
                {
                    var TypeList = TaskManagment.GetAllAssignedType(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "MGMT");
                    ddlType.DataSource = TypeList;
                    ddlType.DataBind();
                }
                else
                {
                    var TypeList = TaskManagment.GetAllAssignedType(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "PR");
                    ddlType.DataSource = TypeList;
                    ddlType.DataBind();
                }

                ddlType.Items.Insert(0, new ListItem("Select Type", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindCategories(bool ismgmt)
        {
            try
            {
                ddlCategory.DataSource = null;
                ddlCategory.DataBind();

                ddlCategory.DataTextField = "Name";
                ddlCategory.DataValueField = "ID";
                if (ismgmt)
                {
                    var CatList = TaskManagment.GetAllTaskAssignedCategory(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "MGMT");
                    ddlCategory.DataSource = CatList;
                    ddlCategory.DataBind();
                }
                else
                {
                    var CatList = TaskManagment.GetAllTaskAssignedCategory(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "PR");
                    ddlCategory.DataSource = CatList;
                    ddlCategory.DataBind();

                }
                ddlCategory.Items.Insert(0, new ListItem("Select Category", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindCategoriesInternal(bool ismgmt)
        {
            try
            {
                ddlCategory.DataSource = null;
                ddlCategory.DataBind();
                ddlCategory.DataTextField = "Name";
                ddlCategory.DataValueField = "ID";
                if (ismgmt)
                {
                    var CatList = TaskManagment.GetAllTaskAssignedInternalCategory(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "MGMT");
                    ddlCategory.DataSource = CatList;
                    ddlCategory.DataBind();
                }
                else
                {
                    var CatList = TaskManagment.GetAllTaskAssignedInternalCategory(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "PR");
                    ddlCategory.DataSource = CatList;
                    ddlCategory.DataBind();
                }
                ddlCategory.Items.Insert(0, new ListItem("Select Category", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindComplianceSubTypeList(bool ismgmt)
        {
            try
            {
                ddlComplianceSubType.Items.Clear();
                List<Sp_StatuoryTaskSubType_Result> ComplianceSubTypeList = new List<Sp_StatuoryTaskSubType_Result>();

                if (ismgmt)
                {
                    ComplianceSubTypeList = TaskManagment.GetSubTypeByUserID(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "MGMT");
                }
                else
                {
                    ComplianceSubTypeList = TaskManagment.GetSubTypeByUserID(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "PR");
                }

                ddlComplianceSubType.DataTextField = "Name";
                ddlComplianceSubType.DataValueField = "ID";
                ddlComplianceSubType.DataSource = ComplianceSubTypeList;
                ddlComplianceSubType.DataBind();
                ddlComplianceSubType.Items.Insert(0, new ListItem("Select Sub Type", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindActList(bool ismgmt)
        {
            try
            {
                List<Sp_StatuoryTaskAct_Result> ActList = new List<Sp_StatuoryTaskAct_Result>();
                ddlAct.Items.Clear();

                if (ismgmt)
                {
                    ActList = TaskManagment.GetActsByUserID(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "MGMT");
                }
                else
                {
                    ActList = TaskManagment.GetActsByUserID(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "PR");
                }

                ddlAct.DataTextField = "Name";
                ddlAct.DataValueField = "ID";
                ddlAct.DataSource = ActList;
                ddlAct.DataBind();

                ddlAct.Items.Insert(0, new ListItem("Act", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindInternalActList(bool ismgmt)
        {
            try
            {
                List<Sp_StatuoryTaskActInternal_Result> ActList = new List<Sp_StatuoryTaskActInternal_Result>();
                ddlAct.Items.Clear();

                if (ismgmt)
                {
                    ActList = TaskManagment.GetInternalActsByUserID(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "MGMT");
                }
                else
                {
                    ActList = TaskManagment.GetInternalActsByUserID(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "PR");
                }

                ddlAct.DataTextField = "Name";
                ddlAct.DataValueField = "ID";
                ddlAct.DataSource = ActList;
                ddlAct.DataBind();

                ddlAct.Items.Insert(0, new ListItem("Act", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void BindPerfRevUserDropDown()
        {
            try
            {
                int tasktype = -1;
                int roleID = 3;
                roleID = Convert.ToInt32(ViewState["selectedRole"]);

                bool ismanagement = false;
                if (AuthenticationHelper.Role.Contains("MGMT"))
                    ismanagement = true;

                ddlPerfRevUser.DataSource = null;
                ddlPerfRevUser.DataBind();

                ddlPerfRevUser.DataTextField = "Name";
                ddlPerfRevUser.DataValueField = "ID";

                if (ddlTaskType.SelectedValue == "1") //1--Statutory                
                    tasktype = 1;
                else if (ddlTaskType.SelectedValue == "2") //2--Internal                
                    tasktype = 2;

                if (ismanagement)
                {
                    var TypeList = TaskManagment.GetTaskStatutoryPerformerReviewerList(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, roleID, tasktype, "MGMT");
                    ddlPerfRevUser.DataSource = TypeList;
                    ddlPerfRevUser.DataBind();
                }
                else
                {
                    var TypeList = TaskManagment.GetTaskStatutoryPerformerReviewerList(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, tasktype, roleID, "PR");
                    ddlPerfRevUser.DataSource = TypeList;
                    ddlPerfRevUser.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public DateTime GetDate(string date)
        {
            string date1 = "";
            if (date.Contains("/"))
            {
                date1 = date.Trim().Substring(date.IndexOf("/") + 4, 4) + "-" + date.Substring(date.IndexOf("/") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains("-"))
            {
                date1 = date.Substring(date.IndexOf("-") + 4, 4) + "-" + date.Substring(date.IndexOf("-") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains(" "))
            {
                date1 = date.Substring(date.IndexOf(" ") + 4, 4) + "-" + date.Substring(date.IndexOf(" ") + 1, 2) + "-" + date.Substring(0, 2);
            }
            return Convert.ToDateTime(date1);
        }

        protected string GetPerformer(long TaskScheduledOnID, long TaskInstanceID, byte TaskType)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                string result = "";
                result = TaskManagment.GetTaskAssignedUser(customerID, Convert.ToInt32(TaskType), TaskInstanceID, TaskScheduledOnID, 3);
                Performername = result;
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }

        protected string GetReviewer(long TaskScheduledOnID, long TaskInstanceID, byte TaskType)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                string result = "";
                result = TaskManagment.GetTaskAssignedUser(customerID, Convert.ToInt32(TaskType), TaskInstanceID, TaskScheduledOnID, 4);
                Reviewername = result;
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }

        protected void ShowReviewer(object sender, EventArgs e)
        {
            liReviewer.Attributes.Add("class", "active");
            liPerformer.Attributes.Add("class", "");

            PerformerFlag = false;
            ReviewerFlag = true;

            ViewState["selectedRole"] = 4;

            BindPerfRevUserDropDown();

            FillTaskReports();
        }

        protected void ShowPerformer(object sender, EventArgs e)
        {
            liReviewer.Attributes.Add("class", "");
            liPerformer.Attributes.Add("class", "active");

            PerformerFlag = true;
            ReviewerFlag = false;

            ViewState["selectedRole"] = 3;

            BindPerfRevUserDropDown();

            FillTaskReports();
        }

        private bool FindNodeExists(NameValueHierarchy item, List<int> ListIDs)
        {
            bool result = false;

            try
            {
                if (ListIDs.Contains(item.ID))
                {
                    result = true;
                    return result;
                }
                else
                {
                    foreach (var childNode in item.Children)
                    {
                        if (ListIDs.Contains(childNode.ID))
                        {
                            result = true;
                            break;
                        }
                        else if (childNode.Children.Count > 0)
                        {
                            FindNodeExists(childNode, ListIDs);
                        }
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return result;

            }
        }

        private bool IsNumeric(string PageNo)
        {
            try
            {
                int i = Convert.ToInt32(PageNo);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        private bool IsValid()
        {
            try
            {
                if (String.IsNullOrEmpty(SelectedPageNo.Text.Trim()) || (SelectedPageNo.Text == "0"))
                {
                    SelectedPageNo.Text = "1";
                    return false;
                }
                else if (!IsNumeric(SelectedPageNo.Text))
                {
                    //ShowGridViewPagingErrorMessage("Please Insert Valid Page No.");
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (FormatException)
            {
                return false;
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        private void GetPageDisplaySummary()
        {
            try
            {
                DivRecordsScrum.Visible = true;

                lblTotalRecord.Text = " " + Session["TotalRows"].ToString();

                lTotalCount.Text = GetTotalPagesCount().ToString();

                if (lTotalCount.Text != "0")
                {
                    if (SelectedPageNo.Text == "" || SelectedPageNo.Text == "0" || SelectedPageNo.Text == "1")
                    {
                        SelectedPageNo.Text = "1";
                        lblStartRecord.Text = "1";

                        if (!(Convert.ToInt32(ddlPageSize.SelectedValue) > Convert.ToInt32(Session["TotalRows"].ToString())))
                            lblEndRecord.Text = ddlPageSize.SelectedValue + " ";
                        else
                            lblEndRecord.Text = Session["TotalRows"].ToString() + " ";
                    }
                }
                else if (lTotalCount.Text == "0")
                {
                    SelectedPageNo.Text = "0";
                    DivRecordsScrum.Visible = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                // throw ex;
            }
        }

        private void FillTaskReports()
        {
            try
            {
                int category = -1;
                int ActID = -1;
                int SubTypeID = -1;
                int selectedUserID = -1;
                int customerID = -1;
                customerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
               
                CannedReportFilterNewStatus status = (CannedReportFilterNewStatus) Convert.ToInt16(ddlStatus.SelectedIndex);

                int location = -1;
                int taskType = -1;
                int typeID = -1;

                int roleID = 3;

                if (ViewState["selectedRole"] != null)
                {
                    roleID = Convert.ToInt32(ViewState["selectedRole"]);
                }

                if (!string.IsNullOrEmpty(ddlTaskType.SelectedValue)) // Statutory--Internal
                {
                    taskType = Convert.ToInt32(ddlTaskType.SelectedValue);
                }

                if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue))
                {
                    location = Convert.ToInt32(tvFilterLocation.SelectedValue);
                }

                if (!string.IsNullOrEmpty(ddlType.SelectedValue)) //Central -- State
                {
                    typeID = Convert.ToInt32(ddlType.SelectedValue);
                }

                if (!string.IsNullOrEmpty(ddlComplianceSubType.SelectedValue))
                {
                    SubTypeID = Convert.ToInt32(ddlComplianceSubType.SelectedValue);
                }

                if (!string.IsNullOrEmpty(ddlCategory.SelectedValue))
                {
                    category = Convert.ToInt32(ddlCategory.SelectedValue);
                }

                if (!string.IsNullOrEmpty(ddlAct.SelectedValue))
                {
                    ActID = Convert.ToInt32(ddlAct.SelectedValue);
                }

                //DateTime dtfrom = DateTime.Now;
                //DateTime dtTo = DateTime.Now;

                if (!string.IsNullOrEmpty(ddlPerfRevUser.SelectedValue))
                {
                    selectedUserID = Convert.ToInt32(ddlPerfRevUser.SelectedValue);
                }

                DateTime? dtfrom = null;
                DateTime? dtTo = null;

                if (txtStartDate.Text != "")
                {
                    dtfrom = DateTimeExtensions.GetDate(txtStartDate.Text);
                }

                if (txtEndDate.Text != "")
                {
                    dtTo = DateTimeExtensions.GetDate(txtEndDate.Text);
                }

                //if (txtStartDate.Text == "")               
                //    dtfrom = DateTime.ParseExact("01-01-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);                
                //else
                //    dtfrom = DateTime.ParseExact(txtStartDate.Text.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);

                //if (txtEndDate.Text == "")                
                //    dtTo = DateTime.ParseExact("01-01-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);                
                //else                
                //    dtTo = DateTime.ParseExact(txtEndDate.Text.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);

                string txtToSearch = "";
                txtToSearch = txtSearchType.Text;

                if (taskType == 1) //1 -- Statutory
                {
                    var GridData = TaskManagment.GetCannedReportTask(customerID, AuthenticationHelper.UserID, AuthenticationHelper.Role, status, location, taskType, category, ActID, dtfrom, dtTo, typeID, SubTypeID, txtToSearch, roleID, selectedUserID);

                    if (PerformerFlag)
                    {
                        grdTaskReportPerformer.DataSource = null;
                        grdTaskReportPerformer.DataBind();

                        grdTaskReportPerformer.DataSource = GridData;
                        grdTaskReportPerformer.DataBind();

                        grdTaskReportPerformer.Visible = true;

                        Session["TotalRows"] = GridData.Count;

                        grdTaskReportReviewer.Visible = false;
                    }
                    else if (ReviewerFlag)
                    {
                        //var GridData = TaskManagment.GetCannedReportDataForPerformer(customerid, AuthenticationHelper.UserID, status, location, type, category, ActID, dtfrom, dtTo, SubTypeID, txtToSearch, 4);

                        grdTaskReportReviewer.DataSource = null;
                        grdTaskReportReviewer.DataBind();

                        grdTaskReportReviewer.DataSource = GridData;
                        Session["TotalRows"] = GridData.Count;
                        grdTaskReportReviewer.DataBind();

                        grdTaskReportPerformer.Visible = false;
                        grdTaskReportReviewer.Visible = true;
                    }
                }
                else if (taskType == 2)
                {
                    var GridData = TaskManagment.GetCannedReportTaskInternal(customerID, AuthenticationHelper.UserID, AuthenticationHelper.Role, status, location, taskType, category,ActID, dtfrom, dtTo, typeID, SubTypeID, txtToSearch, roleID, selectedUserID);

                    if (PerformerFlag)
                    {
                        grdTaskReportPerformer.DataSource = null;
                        grdTaskReportPerformer.DataBind();

                        grdTaskReportPerformer.DataSource = GridData;
                        grdTaskReportPerformer.DataBind();

                        grdTaskReportPerformer.Visible = true;

                        Session["TotalRows"] = GridData.Count;

                        grdTaskReportReviewer.Visible = false;
                    }
                    else if (ReviewerFlag)
                    {
                        //var GridData = TaskManagment.GetCannedReportDataForPerformer(customerid, AuthenticationHelper.UserID, status, location, type, category, ActID, dtfrom, dtTo, SubTypeID, StringType, 4);

                        grdTaskReportReviewer.DataSource = null;
                        grdTaskReportReviewer.DataBind();

                        grdTaskReportReviewer.DataSource = GridData;
                        Session["TotalRows"] = GridData.Count;
                        grdTaskReportReviewer.DataBind();

                        grdTaskReportPerformer.Visible = false;
                        grdTaskReportReviewer.Visible = true;
                    }
                }


                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void upDocumentDownload_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);

                DateTime date = DateTime.MinValue;
                if (DateTime.TryParseExact(txtStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "InitializeDatePicker11", string.Format("initializeDatePicker11(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "InitializeDatePicker11", "initializeDatePicker11(null);", true);
                }

                DateTime date1 = DateTime.MinValue;
                if (DateTime.TryParseExact(txtEndDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "InitializeDatePicker12", string.Format("initializeDatePicker12(new Date({0}, {1}, {2}));", date1.Year, date1.Month - 1, date1.Day), true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "InitializeDatePicker12", "initializeDatePicker12(null);", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlTaskType.SelectedValue == "-1" || ddlTaskType.SelectedValue == "1" || ddlTaskType.SelectedValue == "2")
                {
                    if (!IsValid()) { return; };

                    SelectedPageNo.Text = "1";
                    int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                    if (currentPageNo < GetTotalPagesCount())
                    {
                        SelectedPageNo.Text = (currentPageNo).ToString();
                    }

                    if (PerformerFlag)
                    {
                        grdTaskReportPerformer.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                        grdTaskReportPerformer.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                    }

                    else if (ReviewerFlag)
                    {
                        grdTaskReportReviewer.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                        grdTaskReportReviewer.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                    }
                }
                //Reload the Grid
                FillTaskReports();

                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlTaskType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                bool ismanagement = false;
                if (AuthenticationHelper.Role.Contains("MGMT"))
                {
                    ismanagement = true;
                }

                if (ddlTaskType.SelectedValue == "1") //1--Statutory
                {
                    ddlAct.ClearSelection();
                    //PanelAct.Visible = true;
                    Panelsubtype.Visible = true;
                    //DivAct.Attributes.Remove("disabled");

                    BindTypes(ismanagement);
                    BindCategories(ismanagement);

                    BindPerfRevUserDropDown();
                }
                else if (ddlTaskType.SelectedValue == "2") //2--Internal
                {
                    ddlAct.ClearSelection();
                    //PanelAct.Enabled = false;
                    Panelsubtype.Visible = false;
                    //DivAct.Attributes.Add("disabled", "true");

                    //Panelsubtype.Enabled = false;

                    BindTypesInternal(ismanagement);
                    BindCategoriesInternal(ismanagement);

                    BindPerfRevUserDropDown();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            FillTaskReports();
        }

        protected void btnAdvSearch_Click(object sender, EventArgs e)
        {
            try
            {
                lblAdvanceSearchScrum.Text = String.Empty;

                int type = 0;
                int Category = 0;
                int Act = 0;
                int SubCategory = 0;

                if (txtStartDate.Text != "" && txtEndDate.Text == "" || txtStartDate.Text == "" && txtEndDate.Text != "")
                {
                    if (txtStartDate.Text == "")
                        txtStartDate.BorderColor = System.Drawing.ColorTranslator.FromHtml("red");
                    else if (txtEndDate.Text == "")
                        txtEndDate.BorderColor = System.Drawing.ColorTranslator.FromHtml("red");

                    return;
                }

                if (ddlType.SelectedValue != "-1")
                {
                    type = ddlType.SelectedItem.Text.Length;

                    if (type >= 20)
                        lblAdvanceSearchScrum.Text = "<b>Type: </b>" + ddlType.SelectedItem.Text.Substring(0, 20) + "...";
                    else
                        lblAdvanceSearchScrum.Text = "<b>Type: </b>" + ddlType.SelectedItem.Text;
                }

                if (ddlCategory.SelectedValue != "-1")
                {
                    Category = ddlCategory.SelectedItem.Text.Length;

                    if (lblAdvanceSearchScrum.Text != "")
                    {
                        if (Category >= 20)
                            lblAdvanceSearchScrum.Text += "     " + "<b>Category: </b>" + ddlCategory.SelectedItem.Text.Substring(0, 20) + "...";
                        else
                            lblAdvanceSearchScrum.Text += "     " + "<b>Category: </b>" + ddlCategory.SelectedItem.Text;
                    }
                    else
                    {
                        if (Category >= 20)
                            lblAdvanceSearchScrum.Text += "<b>Category: </b>" + ddlCategory.SelectedItem.Text.Substring(0, 20) + "...";
                        else
                            lblAdvanceSearchScrum.Text += "<b>Category: </b>" + ddlCategory.SelectedItem.Text;
                    }
                }

                if (ddlAct.SelectedValue != "-1")
                {
                    Act = ddlAct.SelectedItem.Text.Length;

                    if (lblAdvanceSearchScrum.Text != "")
                    {
                        if (Act >= 30)
                            lblAdvanceSearchScrum.Text += "     " + "<b>Act: </b>" + ddlAct.SelectedItem.Text.Substring(0, 30) + "...";
                        else
                            lblAdvanceSearchScrum.Text += "     " + "<b>Act: </b>" + ddlAct.SelectedItem.Text;
                    }
                    else
                    {
                        if (Act >= 30)
                            lblAdvanceSearchScrum.Text += "<b>Act: </b>" + ddlAct.SelectedItem.Text.Substring(0, 30) + "...";
                        else
                            lblAdvanceSearchScrum.Text += "<b>Act: </b>" + ddlAct.SelectedItem.Text;
                    }
                }

                if (ddlComplianceSubType.SelectedValue != "-1")
                {
                    SubCategory = ddlComplianceSubType.SelectedItem.Text.Length;

                    if (lblAdvanceSearchScrum.Text != "")
                    {
                        if (SubCategory >= 30)
                            lblAdvanceSearchScrum.Text += "     " + "<b>Sub Category : </b>" + ddlComplianceSubType.SelectedItem.Text.Substring(0, 30) + "...";
                        else
                            lblAdvanceSearchScrum.Text += "     " + "<b>Sub Category: </b>" + ddlComplianceSubType.SelectedItem.Text;
                    }
                    else
                    {
                        if (SubCategory >= 30)
                            lblAdvanceSearchScrum.Text += "<b>Sub Category: </b>" + ddlComplianceSubType.SelectedItem.Text.Substring(0, 30) + "...";
                        else
                            lblAdvanceSearchScrum.Text += "<b>Sub Category: </b>" + ddlComplianceSubType.SelectedItem.Text;
                    }
                }

                if (txtStartDate.Text != "" && txtEndDate.Text != "")
                {
                    if (lblAdvanceSearchScrum.Text != "")
                    {
                        lblAdvanceSearchScrum.Text += "     " + "<b>Start Date: </b>" + txtStartDate.Text;
                        lblAdvanceSearchScrum.Text += "     " + "<b>End Date: </b>" + txtEndDate.Text;
                    }
                    else
                    {
                        lblAdvanceSearchScrum.Text += "<b>Start Date: </b>" + txtStartDate.Text;
                        lblAdvanceSearchScrum.Text += "<b> End Date: </b>" + txtEndDate.Text;
                    }
                }

                if (ddlPerfRevUser.SelectedValue != "-1" && ddlPerfRevUser.SelectedValue != "")
                {
                    lblAdvanceSearchScrum.Text += "<b>User: </b>" + ddlPerfRevUser.SelectedItem.Text;
                }

                if (txtSearchType.Text != "")
                {
                    lblAdvanceSearchScrum.Text += "<b>Type To Search: </b>" + txtSearchType.Text;
                }

                if (lblAdvanceSearchScrum.Text != "")
                {
                    divAdvSearch.Visible = true;
                }
                else
                {
                    divAdvSearch.Visible = false;
                }

                FillTaskReports();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lnkClearAdvanceList_Click(object sender, EventArgs e)
        {
            try
            {
                lblAdvanceSearchScrum.Text = "";
                divAdvSearch.Visible = false;
                ClearAdvanceSearchModalControls();

                FillTaskReports();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void ClearAdvanceSearchModalControls()
        {
            try
            {
                ddlType.ClearSelection();
                ddlCategory.ClearSelection();
                ddlAct.ClearSelection();
                ddlComplianceSubType.ClearSelection();
                ddlPerfRevUser.ClearSelection();

                txtSearchType.Text = "";
                txtStartDate.Text = "";
                txtEndDate.Text = "";


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdTaskReportPerformer_RowCommand(object sender, GridViewCommandEventArgs e)
        {

        }

        protected void grdTaskReportPerformer_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DateTime CurrentDate = DateTime.Today.Date;

                    Label lblScheduledOn = (Label) e.Row.FindControl("lblScheduledOn");
                    Label lblStatus = (Label) e.Row.FindControl("lblStatus");

                    if (lblScheduledOn != null && lblStatus != null)
                    {
                        String GridStatus = lblStatus.Text;

                        if (GridStatus == "Open" && Convert.ToDateTime(lblScheduledOn.Text).Date >= CurrentDate)
                        {
                            lblStatus.Text = "Upcoming"; lblStatus.ToolTip = "Upcoming";
                        }
                        else if (GridStatus == "Open" && Convert.ToDateTime(lblScheduledOn.Text).Date < CurrentDate)
                        {
                            lblStatus.Text = "Overdue"; lblStatus.ToolTip = "Overdue";
                        }
                        else if (GridStatus == "Complied but pending review")
                        {
                            lblStatus.Text = "Pending For Review"; lblStatus.ToolTip = "Pending For Review";
                        }
                        else if (GridStatus == "Complied Delayed but pending review")
                        {
                            lblStatus.Text = "Pending For Review"; lblStatus.ToolTip = "Pending For Review";
                        }
                        else if (GridStatus == "In Progress" && Convert.ToDateTime(lblScheduledOn.Text).Date >= CurrentDate)
                        {
                            lblStatus.Text = "Upcoming"; lblStatus.ToolTip = "Upcoming";
                        }
                        else if (GridStatus == "In Progress" && Convert.ToDateTime(lblScheduledOn.Text).Date < CurrentDate)
                        {
                            lblStatus.Text = "Overdue"; lblStatus.ToolTip = "Overdue";
                        }
                        else if (GridStatus == "Not Complied")
                        {
                            lblStatus.Text = "Rejected"; lblStatus.ToolTip = "Rejected";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdTaskReportPerformer_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            FillTaskReports();
        }

        protected void grdTaskReportReviewer_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DateTime CurrentDate = DateTime.Today.Date;

                    Label lblScheduledOn = (Label) e.Row.FindControl("lblScheduledOn");
                    Label lblStatus = (Label) e.Row.FindControl("lblStatus");

                    if (lblScheduledOn != null && lblStatus != null)
                    {
                        String GridStatus = lblStatus.Text;

                        if (GridStatus == "Open" && Convert.ToDateTime(lblScheduledOn.Text).Date >= CurrentDate)
                        {
                            lblStatus.Text = "Upcoming"; lblStatus.ToolTip = "Upcoming";
                        }
                        else if (GridStatus == "Open" && Convert.ToDateTime(lblScheduledOn.Text).Date < CurrentDate)
                        {
                            lblStatus.Text = "Overdue"; lblStatus.ToolTip = "Overdue";
                        }
                        else if (GridStatus == "Complied but pending review")
                        {
                            lblStatus.Text = "Pending For Review"; lblStatus.ToolTip = "Pending For Review";
                        }
                        else if (GridStatus == "Complied Delayed but pending review")
                        {
                            lblStatus.Text = "Pending For Review"; lblStatus.ToolTip = "Pending For Review";
                        }
                        else if (GridStatus == "In Progress" && Convert.ToDateTime(lblScheduledOn.Text).Date > CurrentDate)
                        {
                            lblStatus.Text = "Upcoming"; lblStatus.ToolTip = "Upcoming";
                        }
                        else if (GridStatus == "In Progress" && Convert.ToDateTime(lblScheduledOn.Text).Date <= CurrentDate)
                        {
                            lblStatus.Text = "Overdue"; lblStatus.ToolTip = "Overdue";
                        }
                        else if (GridStatus == "Not Complied")
                        {
                            lblStatus.Text = "Rejected"; lblStatus.ToolTip = "Rejected";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdTaskReportReviewer_RowCommand(object sender, GridViewCommandEventArgs e)
        {

        }

        protected void grdTaskReportReviewer_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

        }

        protected void lBPrevious_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                if (!IsValid()) { return; };
                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;

                if (Convert.ToInt32(SelectedPageNo.Text) > 1)
                {
                    SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
                }

                StartRecord = StartRecord - Convert.ToInt32(ddlPageSize.SelectedValue);

                if (StartRecord < 1)
                    StartRecord = 1;

                EndRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalRows"]))
                    EndRecord = Convert.ToInt32(Session["TotalRows"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";

                if (ddlTaskType.SelectedValue == "-1" || ddlTaskType.SelectedValue == "1" || ddlTaskType.SelectedValue == "2")
                {
                    if (PerformerFlag)
                    {
                        grdTaskReportPerformer.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                        grdTaskReportPerformer.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                    }
                    else if (ReviewerFlag)
                    {
                        grdTaskReportReviewer.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                        grdTaskReportReviewer.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                    }
                }
                //Reload the Grid
                FillTaskReports();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        protected void lBNext_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;

                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo + 1).ToString();
                }
                else
                {

                }

                if (!(StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) > Convert.ToInt32(Session["TotalRows"])))
                    StartRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue);

                EndRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalRows"]))
                    EndRecord = Convert.ToInt32(Session["TotalRows"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";

                if (ddlTaskType.SelectedValue == "-1" || ddlTaskType.SelectedValue == "1" || ddlTaskType.SelectedValue == "2")
                {
                    if (PerformerFlag)
                    {
                        grdTaskReportPerformer.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                        grdTaskReportPerformer.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                    }

                    else if (ReviewerFlag)
                    {
                        grdTaskReportReviewer.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                        grdTaskReportReviewer.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                    }
                }

                //Reload the Grid
                FillTaskReports();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //  ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            try
            {
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    int category = -1;
                    int ActID = -1;
                    int typeID = -1;
                    int SubTypeID = -1;
                    int taskType = -1;
                    int selectedUserID = -1;

                    int customerID = -1;
                    customerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
                    int location = Convert.ToInt32(tvFilterLocation.SelectedValue);

                    DateTime CurrentDate = DateTime.Today.Date;

                    string txtToSearch = String.Empty;
                    string LocationName = String.Empty;
                    LocationName = tvFilterLocation.SelectedNode.Text;

                    ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("TaskReport");
                    DataTable ExcelData = null;

                    DataView view = new DataView();

                    CannedReportFilterNewStatus status = (CannedReportFilterNewStatus) Convert.ToInt16(ddlStatus.SelectedIndex);

                    if (!string.IsNullOrEmpty(ddlTaskType.SelectedValue))
                    {
                        taskType = Convert.ToInt32(ddlTaskType.SelectedValue);
                    }

                    if (!string.IsNullOrEmpty(ddlCategory.SelectedValue))
                    {
                        category = Convert.ToInt32(ddlCategory.SelectedValue);
                    }

                    if (!string.IsNullOrEmpty(ddlAct.SelectedValue))
                    {
                        ActID = Convert.ToInt32(ddlAct.SelectedValue);
                    }

                    if (!string.IsNullOrEmpty(ddlType.SelectedValue)) //Central -- State
                    {
                        typeID = Convert.ToInt32(ddlType.SelectedValue);
                    }

                    if (!string.IsNullOrEmpty(ddlComplianceSubType.SelectedValue))
                    {
                        SubTypeID = Convert.ToInt32(ddlComplianceSubType.SelectedValue);
                    }

                    //DateTime dtfrom = DateTime.Now;
                    //DateTime dtTo = DateTime.Now;

                    //if (!string.IsNullOrEmpty(ddlPerfRevUser.SelectedValue))
                    //{
                    //    selectedUserID = Convert.ToInt32(ddlPerfRevUser.SelectedValue);
                    //}

                    DateTime? dtfrom = null;
                    DateTime? dtTo = null;

                    if (txtStartDate.Text != "")
                    {
                        dtfrom = DateTimeExtensions.GetDate(txtStartDate.Text);
                    }

                    if (txtEndDate.Text != "")
                    {
                        dtTo = DateTimeExtensions.GetDate(txtEndDate.Text);
                    }

                    //if (txtStartDate.Text == "")               
                    //    dtfrom = DateTime.ParseExact("01-01-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);                
                    //else
                    //    dtfrom = DateTime.ParseExact(txtStartDate.Text.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);

                    //if (txtEndDate.Text == "")                
                    //    dtTo = DateTime.ParseExact("01-01-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);                
                    //else                
                    //    dtTo = DateTime.ParseExact(txtEndDate.Text.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);

                    txtToSearch = txtSearchType.Text;
                    string DocType = ddlTaskType.SelectedItem.Text;

                    #region Statutory

                    if (DocType == "Statutory")
                    {
                        if (PerformerFlag)
                        {
                            var GridData = TaskManagment.GetCannedReportTask(customerID, AuthenticationHelper.UserID, AuthenticationHelper.Role, status, location, taskType, category, ActID, dtfrom, dtTo, typeID, SubTypeID, txtToSearch, 3, selectedUserID);
                            view = new System.Data.DataView((GridData).ToDataTable());
                        }

                        if (ReviewerFlag)
                        {
                            var GridData = TaskManagment.GetCannedReportTask(customerID, AuthenticationHelper.UserID, AuthenticationHelper.Role, status, location, taskType, category, ActID, dtfrom, dtTo, typeID, SubTypeID, txtToSearch, 4, selectedUserID);
                            view = new System.Data.DataView((GridData).ToDataTable());
                        }

                        ExcelData = view.ToTable("Selected", false, "TaskID", "Branch", "TaskTitle", "TaskDescription", "TaskTypeName", "TaskSubTypeName", "ActName", "ShortDescription", "DetailDescription", "ForMonth", "ScheduledOn", "CloseDate", "Status", "TaskScheduledOnID", "TaskInstanceID", "TaskType");

                        ExcelData.Columns.Add("SNo", typeof(string)).SetOrdinal(0);

                        if (PerformerFlag)
                        {
                            if (AuthenticationHelper.Role != "MGMT")
                                ExcelData.Columns.Add("Reviewer");
                            else
                            {
                                ExcelData.Columns.Add("Performer");
                                ExcelData.Columns.Add("Reviewer");
                            }
                        }
                        if (ReviewerFlag)
                        {
                            ExcelData.Columns.Add("Performer");
                        }

                        int rowCount = 0;
                        foreach (DataRow item in ExcelData.Rows)
                        {
                            item["SNo"] = (++rowCount).ToString();

                            if (PerformerFlag)
                            {
                                if (AuthenticationHelper.Role != "MGMT")
                                    item["Reviewer"] = GetReviewer(Convert.ToInt32(item["TaskScheduledOnID"].ToString()), Convert.ToInt32(item["TaskInstanceID"].ToString()), Convert.ToByte(item["TaskType"].ToString()));
                                else
                                {
                                    item["Performer"] = GetPerformer(Convert.ToInt32(item["TaskScheduledOnID"].ToString()), Convert.ToInt32(item["TaskInstanceID"].ToString()), Convert.ToByte(item["TaskType"].ToString()));
                                    item["Reviewer"] = GetReviewer(Convert.ToInt32(item["TaskScheduledOnID"].ToString()), Convert.ToInt32(item["TaskInstanceID"].ToString()), Convert.ToByte(item["TaskType"].ToString()));
                                }
                            }

                            if (ReviewerFlag)
                                item["Performer"] = GetPerformer(Convert.ToInt32(item["TaskScheduledOnID"].ToString()), Convert.ToInt32(item["TaskInstanceID"].ToString()), Convert.ToByte(item["TaskType"].ToString()));


                            //if (!string.IsNullOrEmpty(Convert.ToString(item["ScheduledOn"])))
                            //{
                            //    item["ScheduledOn"] = Convert.ToDateTime(item["ScheduledOn"]).ToString("dd/MMM/yyyy");
                            //}
                            //if (!string.IsNullOrEmpty(Convert.ToString(item["CloseDate"])))
                            //{
                            //    item["CloseDate"] = Convert.ToDateTime(item["CloseDate"]).ToString("dd/MMM/yyyy");
                            //}

                            if (item["Status"].ToString() == "Open" && Convert.ToDateTime(item["ScheduledOn"]).Date >= CurrentDate)
                                item["Status"] = "Upcoming";
                            else if (item["Status"].ToString() == "Open" && Convert.ToDateTime(item["ScheduledOn"]).Date < CurrentDate)
                                item["Status"] = "Overdue";
                            else if (item["Status"].ToString() == "Complied but pending review")
                                item["Status"] = "Pending For Review";
                            else if (item["Status"].ToString() == "Complied Delayed but pending review")
                                item["Status"] = "Pending For Review";
                            else if (item["Status"].ToString() == "In Progress" && Convert.ToDateTime(item["ScheduledOn"]).Date >= CurrentDate)
                                item["Status"] = "Upcoming";
                            else if (item["Status"].ToString() == "In Progress" && Convert.ToDateTime(item["ScheduledOn"]).Date < CurrentDate)
                                item["Status"] = "Overdue";
                            else if (item["Status"].ToString() == "Not Complied")
                                item["Status"] = "Rejected";
                        }

                        ExcelData.Columns.Remove("TaskID");
                        ExcelData.Columns.Remove("TaskType");
                        ExcelData.Columns.Remove("TaskScheduledOnID");
                        ExcelData.Columns.Remove("TaskInstanceID");

                        exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A1"].Value = "Entity/ Location:";

                        exWorkSheet.Cells["B1:C1"].Merge = true;
                        exWorkSheet.Cells["B1"].Value = LocationName;

                        exWorkSheet.Cells["A2"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A2"].Value = "Report Name:";

                        exWorkSheet.Cells["B2:C2"].Merge = true;

                        exWorkSheet.Cells["B2"].Value = "Report of " + ddlTaskType.SelectedItem.Text + " Task";

                        exWorkSheet.Cells["A3"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A3"].Value = "Report Generated On:";

                        exWorkSheet.Cells["B3"].Value = DateTime.Today.Date.ToString("dd/MMM/yyyy");

                        //Load Data From Excel
                        exWorkSheet.Cells["A5"].LoadFromDataTable(ExcelData, true);

                        exWorkSheet.Cells["A5"].Value = "S.No.";
                        exWorkSheet.Cells["A5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A5"].Style.Font.Size = 12;
                        exWorkSheet.Cells["A5"].AutoFitColumns(20);
                        exWorkSheet.Cells["A5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["A5"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);

                        exWorkSheet.Cells["B5"].Value = "Location";
                        exWorkSheet.Cells["B5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["B5"].Style.Font.Size = 12;
                        exWorkSheet.Cells["B5"].AutoFitColumns(30);
                        exWorkSheet.Cells["B5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["B5"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);

                        exWorkSheet.Cells["C5"].Value = "Task";
                        exWorkSheet.Cells["C5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["C5"].Style.Font.Size = 12;
                        exWorkSheet.Cells["C5"].AutoFitColumns(30);
                        exWorkSheet.Cells["C5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["C5"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);

                        exWorkSheet.Cells["D5"].Value = "Task Description";
                        exWorkSheet.Cells["D5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["D5"].Style.Font.Size = 12;
                        exWorkSheet.Cells["D5"].AutoFitColumns(40);
                        exWorkSheet.Cells["D5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["D5"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);

                        exWorkSheet.Cells["E5"].Value = "Task Type Name";
                        exWorkSheet.Cells["E5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["E5"].Style.Font.Size = 12;
                        exWorkSheet.Cells["E5"].AutoFitColumns(40);
                        exWorkSheet.Cells["E5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["E5"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);

                        exWorkSheet.Cells["F5"].Value = "Subtask Type Name";
                        exWorkSheet.Cells["F5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["F5"].Style.Font.Size = 12;
                        exWorkSheet.Cells["F5"].AutoFitColumns(40);
                        exWorkSheet.Cells["F5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["F5"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);

                        exWorkSheet.Cells["G5"].Value = "Act";
                        exWorkSheet.Cells["G5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["G5"].Style.Font.Size = 12;
                        exWorkSheet.Cells["G5"].AutoFitColumns(40);
                        exWorkSheet.Cells["G5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["G5"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);

                        exWorkSheet.Cells["H5"].Value = "Compliance";
                        exWorkSheet.Cells["H5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["H5"].Style.Font.Size = 12;
                        exWorkSheet.Cells["H5"].AutoFitColumns(40);
                        exWorkSheet.Cells["H5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["H5"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);

                        exWorkSheet.Cells["I5"].Value = "Compliance Description";
                        exWorkSheet.Cells["I5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["I5"].Style.Font.Size = 12;
                        exWorkSheet.Cells["I5"].AutoFitColumns(50);
                        exWorkSheet.Cells["I5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["I5"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);

                        exWorkSheet.Cells["J5"].Value = "Period";
                        exWorkSheet.Cells["J5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["J5"].Style.Font.Size = 12;
                        exWorkSheet.Cells["J5"].AutoFitColumns(20);
                        exWorkSheet.Cells["J5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["J5"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);

                        exWorkSheet.Cells["K5"].Value = "Due Date";
                        exWorkSheet.Cells["K5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["K5"].Style.Font.Size = 12;
                        exWorkSheet.Cells["K5"].AutoFitColumns(20);
                        exWorkSheet.Cells["K5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["K5"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);

                        exWorkSheet.Cells["L5"].Value = "Close Date";
                        exWorkSheet.Cells["L5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["L5"].Style.Font.Size = 12;
                        exWorkSheet.Cells["L5"].AutoFitColumns(20);
                        exWorkSheet.Cells["L5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["L5"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);

                        exWorkSheet.Cells["M5"].Value = "Status";
                        exWorkSheet.Cells["M5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["M5"].Style.Font.Size = 12;
                        exWorkSheet.Cells["M5"].AutoFitColumns(20);
                        exWorkSheet.Cells["M5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["M5"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);

                        if (PerformerFlag)
                        {
                            if (AuthenticationHelper.Role != "MGMT")
                            {
                                exWorkSheet.Cells["N5"].Value = "Reviewer";
                                exWorkSheet.Cells["N5"].Style.Font.Bold = true;
                                exWorkSheet.Cells["N5"].Style.Font.Size = 12;
                                exWorkSheet.Cells["N5"].AutoFitColumns(25);
                                exWorkSheet.Cells["N5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet.Cells["N5"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            }
                            else
                            {
                                exWorkSheet.Cells["N5"].Value = "Performer";
                                exWorkSheet.Cells["N5"].Style.Font.Bold = true;
                                exWorkSheet.Cells["N5"].Style.Font.Size = 12;
                                exWorkSheet.Cells["N5"].AutoFitColumns(25);
                                exWorkSheet.Cells["N5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet.Cells["N5"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);

                                exWorkSheet.Cells["O5"].Value = "Reviewer";
                                exWorkSheet.Cells["O5"].Style.Font.Bold = true;
                                exWorkSheet.Cells["O5"].Style.Font.Size = 12;
                                exWorkSheet.Cells["O5"].AutoFitColumns(25);
                                exWorkSheet.Cells["O5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet.Cells["O5"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            }                      
                        }

                        if (ReviewerFlag)
                        {
                            exWorkSheet.Cells["N5"].Value = "Performer";
                            exWorkSheet.Cells["N5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["N5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["N5"].AutoFitColumns(25);
                            exWorkSheet.Cells["N5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["N5"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                        }

                        int columnCount = 0;
                        if (AuthenticationHelper.Role != "MGMT")
                            columnCount = 14;
                        else
                            columnCount = 15;

                        using (ExcelRange col = exWorkSheet.Cells[5, 1, 5 + ExcelData.Rows.Count, columnCount])
                        {
                            col.Style.WrapText = true;
                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            col.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //col.AutoFitColumns();

                            // Assign borders
                            col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        }

                        using (ExcelRange col = exWorkSheet.Cells[5, 2, 5 + ExcelData.Rows.Count, columnCount])
                        {
                            col.Style.Numberformat.Format = "dd/MMM/yyyy";
                        }

                        Byte[] fileBytes = exportPackge.GetAsByteArray();
                        Response.Clear();
                        Response.Buffer = true;
                        Response.AddHeader("content-disposition", "attachment;filename=" + ddlTaskType.SelectedItem.Text + "_TaskReport.xlsx");
                        Response.Charset = "";
                        Response.ContentType = "application/vnd.ms-excel";                        
                        Response.BinaryWrite(fileBytes);
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                    }

                    #endregion

                    #region Internal

                    if (DocType == "Internal")
                    {
                        if (PerformerFlag)
                        {
                            var GridData = TaskManagment.GetCannedReportTaskInternal(customerID, AuthenticationHelper.UserID, AuthenticationHelper.Role, status, location, taskType, category,ActID, dtfrom, dtTo, typeID, SubTypeID, txtToSearch, 3, selectedUserID);
                            view = new System.Data.DataView((GridData).ToDataTable());
                        }

                        if (ReviewerFlag)
                        {
                            var GridData = TaskManagment.GetCannedReportTaskInternal(customerID, AuthenticationHelper.UserID, AuthenticationHelper.Role, status, location, taskType, category, ActID, dtfrom, dtTo, typeID, SubTypeID, txtToSearch, 4, selectedUserID);
                            view = new System.Data.DataView((GridData).ToDataTable());
                        }

                        //ExcelData = view.ToTable("Selected", false, "TaskID", "Branch", "TaskTitle", "TaskDescription", "TaskTypeName", "TaskSubTypeName", "ShortDescription", "ForMonth", "ScheduledOn", "CloseDate", "Status", "TaskScheduledOnID", "TaskInstanceID", "TaskType");
                        ExcelData = view.ToTable("Selected", false, "TaskID", "Branch", "TaskTitle", "TaskDescription", "ShortDescription", "ForMonth", "ScheduledOn", "CloseDate", "Status", "TaskScheduledOnID", "TaskInstanceID", "TaskType", "Remarks");


                        ExcelData.Columns.Add("SNo", typeof(string)).SetOrdinal(0);

                        if (PerformerFlag)
                        {
                            if (AuthenticationHelper.Role != "MGMT")
                                ExcelData.Columns.Add("Reviewer");
                            else
                            {
                                ExcelData.Columns.Add("Performer");
                                ExcelData.Columns.Add("Reviewer");
                            }
                        }                      
                        if (ReviewerFlag)                        
                            ExcelData.Columns.Add("Performer");

                        int rowCount = 0;

                        foreach (DataRow item in ExcelData.Rows)
                        {
                            item["SNo"] = (++rowCount).ToString();

                            if (PerformerFlag)
                            {
                                if (AuthenticationHelper.Role != "MGMT")
                                    item["Reviewer"] = GetReviewer(Convert.ToInt32(item["TaskScheduledOnID"].ToString()), Convert.ToInt32(item["TaskInstanceID"].ToString()), Convert.ToByte(item["TaskType"].ToString()));
                                else
                                {
                                    item["Performer"] = GetPerformer(Convert.ToInt32(item["TaskScheduledOnID"].ToString()), Convert.ToInt32(item["TaskInstanceID"].ToString()), Convert.ToByte(item["TaskType"].ToString()));
                                    item["Reviewer"] = GetReviewer(Convert.ToInt32(item["TaskScheduledOnID"].ToString()), Convert.ToInt32(item["TaskInstanceID"].ToString()), Convert.ToByte(item["TaskType"].ToString()));
                                }
                            }
                            if (ReviewerFlag)
                            {
                                item["Performer"] = GetPerformer(Convert.ToInt32(item["TaskScheduledOnID"].ToString()), Convert.ToInt32(item["TaskInstanceID"].ToString()), Convert.ToByte(item["TaskType"].ToString()));
                            }

                            if (item["Status"].ToString() == "Open" && Convert.ToDateTime(item["ScheduledOn"]).Date >= CurrentDate)
                                item["Status"] = "Upcoming";
                            else if (item["Status"].ToString() == "Open" && Convert.ToDateTime(item["ScheduledOn"]).Date < CurrentDate)
                                item["Status"] = "Overdue";
                            else if (item["Status"].ToString() == "Complied but pending review")
                                item["Status"] = "Pending For Review";
                            else if (item["Status"].ToString() == "Complied Delayed but pending review")
                                item["Status"] = "Pending For Review";
                            else if (item["Status"].ToString() == "In Progress" && Convert.ToDateTime(item["ScheduledOn"]).Date >= CurrentDate)
                                item["Status"] = "Upcoming";
                            else if (item["Status"].ToString() == "In Progress" && Convert.ToDateTime(item["ScheduledOn"]).Date < CurrentDate)
                                item["Status"] = "Overdue";
                            else if (item["Status"].ToString() == "Not Complied")
                                item["Status"] = "Rejected";
                        }

                        ExcelData.Columns.Remove("TaskID");
                        ExcelData.Columns.Remove("TaskType");
                        ExcelData.Columns.Remove("TaskScheduledOnID");
                        ExcelData.Columns.Remove("TaskInstanceID");

                        exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A1"].Value = "Entity/ Location:";

                        exWorkSheet.Cells["B1:C1"].Merge = true;
                        exWorkSheet.Cells["B1"].Value = LocationName;

                        exWorkSheet.Cells["A2"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A2"].Value = "Report Name:";

                        exWorkSheet.Cells["B2:C2"].Merge = true;

                        exWorkSheet.Cells["B2"].Value = "Report of " + ddlTaskType.SelectedItem.Text + " Task";


                        exWorkSheet.Cells["A3"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A3"].Value = "Report Generated On:";

                        exWorkSheet.Cells["B3"].Value = DateTime.Today.Date.ToString("dd/MMM/yyyy");

                        //Load Data From Excel
                        exWorkSheet.Cells["A5"].LoadFromDataTable(ExcelData, true);

                        exWorkSheet.Cells["A5"].Value = "S.No.";
                        exWorkSheet.Cells["A5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A5"].Style.Font.Size = 12;
                        exWorkSheet.Cells["A5"].AutoFitColumns(20);
                        exWorkSheet.Cells["A5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["A5"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);

                        exWorkSheet.Cells["B5"].Value = "Location";
                        exWorkSheet.Cells["B5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["B5"].Style.Font.Size = 12;
                        exWorkSheet.Cells["B5"].AutoFitColumns(30);
                        exWorkSheet.Cells["B5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["B5"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);

                        exWorkSheet.Cells["C5"].Value = "Task";
                        exWorkSheet.Cells["C5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["C5"].Style.Font.Size = 12;
                        exWorkSheet.Cells["C5"].AutoFitColumns(30);
                        exWorkSheet.Cells["C5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["C5"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);

                        exWorkSheet.Cells["D5"].Value = "Task Description";
                        exWorkSheet.Cells["D5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["D5"].Style.Font.Size = 12;
                        exWorkSheet.Cells["D5"].AutoFitColumns(40);
                        exWorkSheet.Cells["D5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["D5"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);

                        //exWorkSheet.Cells["E5"].Value = "Task Type Name";
                        //exWorkSheet.Cells["E5"].Style.Font.Bold = true;
                        //exWorkSheet.Cells["E5"].Style.Font.Size = 12;
                        //exWorkSheet.Cells["E5"].AutoFitColumns(40);
                        //exWorkSheet.Cells["E5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        //exWorkSheet.Cells["E5"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);

                        //exWorkSheet.Cells["F5"].Value = "Subtask Type Name";
                        //exWorkSheet.Cells["F5"].Style.Font.Bold = true;
                        //exWorkSheet.Cells["F5"].Style.Font.Size = 12;
                        //exWorkSheet.Cells["F5"].AutoFitColumns(20);
                        //exWorkSheet.Cells["F5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        //exWorkSheet.Cells["F5"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);

                        exWorkSheet.Cells["E5"].Value = "Compliance";
                        exWorkSheet.Cells["E5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["E5"].Style.Font.Size = 12;
                        exWorkSheet.Cells["E5"].AutoFitColumns(40);
                        exWorkSheet.Cells["E5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["E5"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);

                        exWorkSheet.Cells["F5"].Value = "Period";
                        exWorkSheet.Cells["F5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["F5"].Style.Font.Size = 12;
                        exWorkSheet.Cells["F5"].AutoFitColumns(20);
                        exWorkSheet.Cells["F5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["F5"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);

                        exWorkSheet.Cells["G5"].Value = "Due Date";
                        exWorkSheet.Cells["G5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["G5"].Style.Font.Size = 12;
                        exWorkSheet.Cells["G5"].AutoFitColumns(20);
                        exWorkSheet.Cells["G5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["G5"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);

                        exWorkSheet.Cells["H5"].Value = "Close Date";
                        exWorkSheet.Cells["H5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["H5"].Style.Font.Size = 12;
                        exWorkSheet.Cells["H5"].AutoFitColumns(20);
                        exWorkSheet.Cells["H5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["H5"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);

                        exWorkSheet.Cells["I5"].Value = "Status";
                        exWorkSheet.Cells["I5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["I5"].Style.Font.Size = 12;
                        exWorkSheet.Cells["I5"].AutoFitColumns(20);
                        exWorkSheet.Cells["I5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["I5"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);

                        if (PerformerFlag)
                        {
                            if (AuthenticationHelper.Role != "MGMT")
                            {
                                exWorkSheet.Cells["J5"].Value = "Remarks";
                                exWorkSheet.Cells["J5"].Style.Font.Bold = true;
                                exWorkSheet.Cells["J5"].Style.Font.Size = 12;
                                exWorkSheet.Cells["J5"].AutoFitColumns(25);
                                exWorkSheet.Cells["J5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet.Cells["J5"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);

                                exWorkSheet.Cells["k5"].Value = "Reviewer";
                                exWorkSheet.Cells["k5"].Style.Font.Bold = true;
                                exWorkSheet.Cells["k5"].Style.Font.Size = 12;
                                exWorkSheet.Cells["k5"].AutoFitColumns(25);
                                exWorkSheet.Cells["k5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet.Cells["k5"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);

                            }
                            else
                            {
                                exWorkSheet.Cells["J5"].Value = "Remarks";
                                exWorkSheet.Cells["J5"].Style.Font.Bold = true;
                                exWorkSheet.Cells["J5"].Style.Font.Size = 12;
                                exWorkSheet.Cells["J5"].AutoFitColumns(25);
                                exWorkSheet.Cells["J5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet.Cells["J5"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);

                                exWorkSheet.Cells["K5"].Value = "Performer";
                                exWorkSheet.Cells["K5"].Style.Font.Bold = true;
                                exWorkSheet.Cells["K5"].Style.Font.Size = 12;
                                exWorkSheet.Cells["K5"].AutoFitColumns(25);
                                exWorkSheet.Cells["K5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet.Cells["K5"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);

                                exWorkSheet.Cells["L5"].Value = "Reviewer";
                                exWorkSheet.Cells["L5"].Style.Font.Bold = true;
                                exWorkSheet.Cells["L5"].Style.Font.Size = 12;
                                exWorkSheet.Cells["L5"].AutoFitColumns(25);
                                exWorkSheet.Cells["L5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet.Cells["L5"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);

                            }
                        }

                        if (ReviewerFlag)
                        {
                            exWorkSheet.Cells["J5"].Value = "Remarks";
                            exWorkSheet.Cells["J5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["J5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["J5"].AutoFitColumns(25);
                            exWorkSheet.Cells["J5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["J5"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);

                            exWorkSheet.Cells["k5"].Value = "Performer";
                            exWorkSheet.Cells["k5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["k5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["k5"].AutoFitColumns(25);
                            exWorkSheet.Cells["k5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["k5"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);

                        }



                        int columnCount = 0;
                        if (AuthenticationHelper.Role != "MGMT")
                            columnCount = 11;
                        else
                            columnCount = 12;

                        using (ExcelRange col = exWorkSheet.Cells[5, 1, 5 + ExcelData.Rows.Count, columnCount])
                        {
                            col.Style.WrapText = true;
                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            col.Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            //Assign borders
                            col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        }

                        using (ExcelRange col = exWorkSheet.Cells[5, 2, 5 + ExcelData.Rows.Count, columnCount])
                        {
                            col.Style.Numberformat.Format = "dd/MMM/yyyy";
                        }

                        Byte[] fileBytes = exportPackge.GetAsByteArray();
                        Response.Clear();
                        Response.Buffer = true;
                        Response.AddHeader("content-disposition", "attachment;filename=" + ddlTaskType.SelectedItem.Text + "_TaskReport.xlsx");
                        Response.Charset = "";
                        Response.ContentType = "application/vnd.ms-excel";                        
                        Response.BinaryWrite(fileBytes);
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                    }

                    #endregion
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}