﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MobileAuth.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.MobileAuth" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript" src="/Newjs/jquery.js"></script>
    <script type="text/javascript" src="/Newjs/jquery-ui-1.9.2.custom.min.js"></script>

    <script type="text/javascript">
        function pushDetails() {
            debugger;
            alert("called");
            var status = $('#<%= hdnStatus.ClientID %>').val();
            var errMessage = $('#<%= hdnErrMessage.ClientID %>').val();
                      
            var value = { "status": status, "message": errMessage }
            alert(value);
            try {
                webkit.messageHandlers.sumbitToiOS.postMessage(value); //for iOS
                // webkit.messageHandlers.refreshWebPage.postMessage(dictionary);

            } catch (err) {
                console.log('error');
            }

            try {
                AndroidFunction.showToast(JSON.stringify(value));     // for Android

            } catch (err) {
                console.log('error');
            }
        }       

        var standalone = window.navigator.standalone,
            userAgent = window.navigator.userAgent.toLowerCase(),
            safari = /safari/.test(userAgent),
            ios = /iphone|ipod|ipad/.test(userAgent);

        if (ios) {
            if (!standalone && safari) {
                alert("safari");
            } else if (!standalone && !safari) {
                alert("iOS webview");// iOS webview
            };
        } else {
            if (userAgent.includes('wv')) {
                alert("Android webview");// Android webview
            } else {
                alert("Chrome");// Chrome
            }
        };
    </script>
</head>
<body>
    <form id="form1" runat="server">
         <asp:ScriptManager ID="smPage" runat="server"></asp:ScriptManager>
        <div>
            <asp:HiddenField ID="hdnStatus" runat="server" Value="" />
            <asp:HiddenField ID="hdnErrMessage" runat="server" Value="" />
        </div>
    </form>
</body>
</html>
