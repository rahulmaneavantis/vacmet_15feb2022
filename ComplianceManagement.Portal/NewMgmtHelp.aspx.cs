﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal
{
    public partial class NewMgmtHelp : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {

                //if (AuthenticationHelper.Role.Equals("MGMT") && AuthenticationHelper.Role.Equals("PERF") && AuthenticationHelper.Role.Equals("RVW1"))
                //{
                //    Mgmt.Visible = true;
                //    perf.Visible = true;
                //}
                if (AuthenticationHelper.Role.Equals("MGMT"))
                {
                    Mgmt.Visible = true;
                    perf.Visible = true;
                }
                else
                {
                    Mgmt.Visible = false;
                    perf.Visible = false;
                }
            }

        }
    }
}