﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LogoutSuccessfully.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.LogoutSuccessfully" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<%--<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>                      
            <asp:ValidationSummary ID="validationSummary2" class="alert alert-block alert-danger fade in" runat="server" />
            <asp:CustomValidator ID="CustomValidator1" runat="server" class="alert alert-block alert-danger fade in" Display="None" EnableClientScript="False"></asp:CustomValidator>
        </div>
         <p style="margin-right:250px;">
                To login please
                <asp:LinkButton ID="lnklogin" CausesValidation="false" runat="server" Text="click here." OnClick="lnklogin_Click"></asp:LinkButton>
            </p> 
    </form>
</body>
</html>--%>



<head id="Head1" runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="AVANTIS - Products that simplify" />
    <meta name="author" content="FortuneCookie - Development Team" />

    <title>Login :: AVANTIS - Products that simplify</title>
    <!-- Bootstrap CSS -->
    <link href="NewCSS/bootstrap.min.css" rel="stylesheet" />
    <!-- bootstrap theme -->
    <link href="NewCSS/bootstrap-theme.css" rel="stylesheet" />
    <!--external css-->
    <!-- font icon -->
    <link href="NewCSS/elegant-icons-style.css" rel="stylesheet" />
    <link href="NewCSS/font-awesome.css" rel="stylesheet" />
    <!-- Custom styles -->
    <link href="NewCSS/stylenew.css" rel="stylesheet" />
    <link href="NewCSS/style-responsive.css" rel="stylesheet" />
    <style>
        .login-form1 {
            max-width: 350px;
            height: 450px;
            margin: 100px auto 0;
            background: white;
        }

        .login-form-head1 {
            /* max-width: 350px; */
            min-height: 80px;
            background: white;
            text-align: center;
        }

        .alert-success1 {
            background-color: white;
            border-color: #caf4ca;
            color: #4cd964;
        }
    </style>
    <script type="text/javascript">
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-92029518-1', 'auto');
        ga('send', 'pageview');

        function settracknew(e, t, n, r) {
            try {
                ga('send', 'event', e, t, n + "#" + r)
            } catch (t) { } return !0
        }

        function settracknewnonInteraction(e, t, n, r) {
            ga('send', 'event', e, t, n, 0, { 'nonInteraction': 1 })
        }
    </script>
</head>
<body>
    <div class="container">
        <%--<form runat="server" class="login-form" name="login" id="loginForm" autocomplete="off">--%>

        <form runat="server" class="login-form" name="login">

            <div class="col-md-12 login-form-head">
                <p class="login-img">
                    <a href="https://teamleaseregtech.com">
                        <img src="Images/avantil-logo1.png" /></a>
                </p>
            </div>
            <div class="login-wrap" style="height: 240px;">
                <h1></h1>
                <div class="otpdiv" style="float: left; color: #2b2b2b; font-family: 'Roboto',sans-serif;">
                    You have been successfully logout from avantis portal...
                </div>
                <div class="clearfix"></div>

                <div class="clearfix"></div>
                <div id="dvOr" style="width: 163px; font-family: 'Roboto',sans-serif; display: block; /* margin-left: 100px; */ /* text-align: center; */ font-size: 14px; color: #333;">
                    <asp:LinkButton ID="lnklogin" CausesValidation="false" runat="server" Font-Bold="true" OnClick="lnklogin_Click"> Click here </asp:LinkButton>to login.
                   
                </div>


                <div class="clearfix" style="height: 10px"></div>

            </div>

        </form>
    </div>
    <%-- <div>
        <div class="container">
            <form runat="server" class="login-form1" name="login" autocomplete="off">
                             
                            <div class="col-md-12 login-form-head1">
                                <p class="login-img">
                                    <img src="Images/avantil-logo.png" />
                                </p>
                            </div>
                    <div class="login-wrap">
                        You have been successfully logout from avantis portal...
                           <p style="margin-left: 90px;">
                            <font color="black">To login please</font>
                            <asp:LinkButton ID="lnklogin" CausesValidation="false" runat="server" Font-Bold="true" OnClick="lnklogin_Click"> Click here.</asp:LinkButton>
                        </p>
                        </div>
                </asp:Panel>
            </form>
        </div>
    </div>--%>
</body>
