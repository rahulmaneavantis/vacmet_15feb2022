﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="EventComplianceAssignDays.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Event.EventComplianceAssignDays" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script language="javascript" type="text/javascript">
        function divexpandcollapse(divname) {
            var div = document.getElementById(divname);
            var img = document.getElementById('img' + divname);
            if (div.style.display == "none") {
                div.style.display = "inline";
                img.src = "../Images/minus.gif";
            } else {
                div.style.display = "none";
                img.src = "../Images/plus.gif";
            }
        }
        function divexpandcollapseChild(divname) {
            var div1 = document.getElementById(divname);
            var img = document.getElementById('img' + divname);
            if (div1.style.display == "none") {
                div1.style.display = "inline";
                img.src = "../Images/minus.gif";
            } else {
                div1.style.display = "none";
                img.src = "../Images/plus.gif";;
            }
        }

        $(function () {

            initializeCombobox();

        });

        function initializeCombobox() {
            $("#<%= ddlparentEvent.ClientID %>").combobox();
            $("#<%= ddlState.ClientID %>").combobox();
        }

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }

        $(function () {
            $('#divAutoTriggerDialog').dialog({
                height: 200,
                width: 600,
                autoOpen: false,
                draggable: true,
                title: "Auto Trigger",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            })
        });
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
      <asp:UpdateProgress ID="updateProgress" runat="server" >
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div>
        <asp:Panel ID="Panel1" Width="100%" Height="450px" ScrollBars="Vertical" runat="server">
            <div style="margin-bottom: 4px">
                <asp:ValidationSummary runat="server" CssClass="vdsummary"
                    ValidationGroup="ComplianceValidationGroup" />
                <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                    ValidationGroup="ComplianceValidationGroup" Display="None" />
                <asp:Label runat="server" ID="lblErrorMassage" ForeColor="Red"></asp:Label>
            </div>
            <div style="margin-bottom: 7px; float: right; margin-right: 520px; margin-top: 10px;">
                <asp:RadioButtonList runat="server" ID="rblRelationship" RepeatDirection="Horizontal" RepeatLayout="Flow"
                    OnSelectedIndexChanged="rblRelationship_SelectedIndexChanged" Font-Bold="true" AutoPostBack="true">
                    <%--<asp:ListItem Text="Public" Value="0" Selected="True" />
                    <asp:ListItem Text="Private" Value="1" />
                    <asp:ListItem Text="Listed" Value="2" />
                    <asp:ListItem Text="Non-Secretarial" Value="3" />      --%> 
                    <asp:ListItem Text="Public" Value="1" Selected="True" />
                    <asp:ListItem Text="Private" Value="2" />
                    <asp:ListItem Text="Listed" Value="3" />
                    <asp:ListItem Text="Non-Secretarial" Value="4" />
                    <asp:ListItem Text="LLP" Value="5" />
                    <asp:ListItem Text="Public Section 8" Value="12" />
                    <asp:ListItem Text="Private Section 8" Value="13" />
                </asp:RadioButtonList>
            </div>
            <div runat="server" id="divState" style="margin-bottom: 7px; float: right; margin-right: 750px; margin-top: 10px;">
                <asp:DropDownList runat="server" ID="ddlState" AutoPostBack="true" OnSelectedIndexChanged="ddlState_SelectedIndexChanged" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                    CssClass="txtbox" />
            </div>
            <div style="margin-bottom: 7px; float: right; margin-right: 750px; margin-top: 10px;">
                <asp:DropDownList runat="server" ID="ddlparentEvent" AutoPostBack="true" OnSelectedIndexChanged="ddlparentEvent_SelectedIndexChanged" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                    CssClass="txtbox" />
                <asp:CompareValidator ID="CompareValidator1" ErrorMessage="Please select parent Event Name."
                    ControlToValidate="ddlparentEvent" runat="server" ValueToCompare="-1" Operator="NotEqual"
                    ValidationGroup="ComplianceValidationGroup" Display="None" />
            </div>
            <table width="100%" align="center">
                <tr>
                    <td colspan="2">
                        <asp:GridView ID="gvParentGrid" GridLines="None" runat="server" AutoGenerateColumns="false"
                            ShowFooter="true" Width="900px" DataKeyNames="Type,SequenceID"
                            OnRowDataBound="gvParentGrid_OnRowDataBound">
                            <Columns>
                                <asp:TemplateField ItemStyle-Width="20px">
                                    <ItemTemplate>
                                        <a href="JavaScript:divexpandcollapse('div<%# Eval("EventType") %>');">
                                            <img id="imgdiv<%# Eval("EventType") %>" width="9px" border="0"
                                                src="../Images/plus.gif" alt="" /></a>
                                    </ItemTemplate>
                                    <ItemStyle Width="20px" VerticalAlign="Middle"></ItemStyle>
                                </asp:TemplateField>
                                <asp:BoundField ItemStyle-Width="150px" DataField="ParentEventID" HeaderText="ID" />
                                <asp:BoundField ItemStyle-Width="750px" DataField="Name" HeaderText="Parent Event Name" />
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <tr>
                                            <td colspan="100%">
                                                <div id="div<%# Eval("EventType") %>" style="overflow: auto; display: none; position: relative; left: 15px; overflow: visible">
                                                    <asp:GridView ID="gvParentToComplianceGrid" GridLines="None" runat="server" Width="95%" DataKeyNames="EventType,SequenceID"
                                                        AutoGenerateColumns="false" OnRowDataBound="gvParentToComplianceGrid_OnRowDataBound">
                                                        <Columns>
                                                            <asp:TemplateField ItemStyle-Width="20px">
                                                                <ItemTemplate>
                                                                    <a href="JavaScript:divexpandcollapse('div1<%# Eval("EventType") %>');">
                                                                        <img id="imgdiv1<%# Eval("EventType") %>" width="9px" border="0" src="../Images/minus.gif"
                                                                            alt="" /></a>
                                                                </ItemTemplate>
                                                                <ItemStyle Width="20px" VerticalAlign="Middle"></ItemStyle>
                                                            </asp:TemplateField>
                                                            <asp:BoundField ItemStyle-Width="122px" DataField="ComplianceID" HeaderText="ID" />
                                                            <asp:BoundField ItemStyle-Width="750px" DataField="Name" HeaderText="Compliance Name" />
                                                            <asp:TemplateField ItemStyle-Width="25px" HeaderText="Days" ItemStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtEventComplianceDays" Text='<%# Eval("Days") %>' Width="50px" runat="server"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator15" ErrorMessage="Please enter compliance trigger Days"
                                                                        ControlToValidate="txtEventComplianceDays" runat="server" ValidationGroup="ComplianceValidationGroup" Display="None" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                                    </asp:GridView>
                                                </div>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <tr>
                                            <td colspan="100%">
                                                <div id="div<%# Eval("EventType") %>" style="overflow: auto; display: inline; position: relative; left: 15px; overflow: auto">
                                                    <asp:GridView ID="gvChildGrid" GridLines="None" runat="server" Width="95%"
                                                        AutoGenerateColumns="false" OnRowUpdating="gvChildGrid_RowUpdating" DataKeyNames="ParentEventID,SequenceID"
                                                        OnRowDataBound="gvChildGrid_OnRowDataBound">
                                                        <Columns>
                                                            <asp:TemplateField ItemStyle-Width="20px">
                                                                <ItemTemplate>
                                                                    <a href="JavaScript:divexpandcollapse('div1<%# Eval("EventType") %>');">
                                                                        <img id="imgdiv1<%# Eval("EventType") %>" width="9px" border="0" src="../Images/plus.gif"
                                                                            alt="" /></a>
                                                                </ItemTemplate>
                                                                <ItemStyle Width="20px" VerticalAlign="Middle"></ItemStyle>
                                                            </asp:TemplateField>
                                                            <asp:BoundField ItemStyle-Width="130px" DataField="SubEventID" HeaderText="ID" />
                                                            <asp:BoundField ItemStyle-Width="750px" DataField="Name" HeaderText="Sub Event Name" />
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="IntermediateEventID" runat="server" Text='<%# Eval("IntermediateEventID") %>' Style="display: none;"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField ItemStyle-Width="255px" ItemStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                                                                        <ContentTemplate>
                                                                            <asp:Button ID="BtnChildAutoTrigger" runat="server" Style="margin-left: 6px;" Visible='<%# visibleBtnChildAutoTrigger(Convert.ToInt32(Eval("ParentEventID")),Convert.ToInt32(Eval("IntermediateEventID")),Convert.ToInt32(Eval("SubEventID")),Convert.ToInt32(Eval("SequenceID")))%>' class="btn btn-search" Text="Set Auto Trigger" CommandName="Update" ValidationGroup="ComplianceValidationGroup" />
                                                                            <asp:Button ID="BtnChildUpdateAutoTrigger" runat="server" Style="margin-left: 6px;" class="btn btn-search" Text="Auto Triggered" Visible='<%# visibleUpdateBtnChildAutoTrigger(Convert.ToInt32(Eval("ParentEventID")),Convert.ToInt32(Eval("IntermediateEventID")),Convert.ToInt32(Eval("SubEventID")),Convert.ToInt32(Eval("SequenceID")))%>' CommandName="Update" ValidationGroup="ComplianceValidationGroup" />
                                                                        </ContentTemplate>
                                                                    </asp:UpdatePanel>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <tr>
                                                                        <td colspan="100%">
                                                                            <div id="div1<%# Eval("EventType") %>" style="overflow: auto; display: none; position: relative; left: 15px; overflow: auto">
                                                                                <asp:GridView ID="gvIntermediateSubEventGrid" GridLines="None" OnRowDataBound="gvIntermediateSubEventGrid_RowDataBound" runat="server" Width="95%" DataKeyNames="IntermediateEventID"
                                                                                    AutoGenerateColumns="false" OnRowUpdating="gvIntermediateSubEventGrid_RowUpdating">
                                                                                    <Columns>
                                                                                        <asp:TemplateField ItemStyle-Width="20px">
                                                                                            <ItemTemplate>
                                                                                                <a href="JavaScript:divexpandcollapse('div1<%# Eval("EventType") %>');">
                                                                                                    <img id="imgdiv1<%# Eval("EventType") %>" width="9px" border="0" src="../Images/plus.gif"
                                                                                                        alt="" /></a>
                                                                                            </ItemTemplate>
                                                                                            <ItemStyle Width="20px" VerticalAlign="Middle"></ItemStyle>
                                                                                        </asp:TemplateField>
                                                                                        <asp:BoundField ItemStyle-Width="120px" DataField="SubEventID" HeaderText="ID" />
                                                                                        <asp:BoundField ItemStyle-Width="650px" DataField="Name" HeaderText="Sub Event Name" />
                                                                                        <asp:TemplateField>
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="IntermediateEventID1" runat="server" Text='<%# Eval("IntermediateEventID") %>' Style="display: none;"></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField ItemStyle-Width="255px" ItemStyle-HorizontalAlign="Center">
                                                                                            <ItemTemplate>
                                                                                                <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                                                                                                    <ContentTemplate>
                                                                                                        <asp:Button ID="BtnIntermediateAutoTrigger" runat="server" Style="margin-left: 6px;" class="btn btn-search" Text="Set Auto Trigger" CommandName="Update" Visible='<%# visibleBtnIntermediateAutoTrigger(Convert.ToInt32(Eval("ParentEventID")),Convert.ToInt32(Eval("IntermediateEventID")),Convert.ToInt32(Eval("SubEventID")),Convert.ToInt32(Eval("SequenceID")))%>' ValidationGroup="ComplianceValidationGroup" />
                                                                                                        <asp:Button ID="BtnIntermediateUpdateAutoTrigger" runat="server" Style="margin-left: 6px;" class="btn btn-search" Text="Auto Triggered" CommandName="Update" Visible='<%# visibleUpdateBtnIntermediateAutoTrigger(Convert.ToInt32(Eval("ParentEventID")),Convert.ToInt32(Eval("IntermediateEventID")),Convert.ToInt32(Eval("SubEventID")),Convert.ToInt32(Eval("SequenceID")))%>' ValidationGroup="ComplianceValidationGroup" />
                                                                                                    </ContentTemplate>
                                                                                                </asp:UpdatePanel>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField>
                                                                                            <ItemTemplate>
                                                                                                <tr>
                                                                                                    <td colspan="100%">
                                                                                                        <div id="div1<%# Eval("EventType") %>" style="overflow: auto; display: none; position: relative; left: 15px; overflow: auto">
                                                                                                            <asp:GridView ID="gvIntermediateComplainceGrid" GridLines="None" runat="server" Width="95%"
                                                                                                                AutoGenerateColumns="false" OnRowDataBound="gvIntermediateComplainceGrid_OnRowDataBound">
                                                                                                                <Columns>
                                                                                                                    <asp:BoundField ItemStyle-Width="120px" DataField="ComplianceID" HeaderText="ID" />
                                                                                                                    <asp:BoundField ItemStyle-Width="650px" DataField="Name" HeaderText="Compliance Name" />
                                                                                                                    <asp:TemplateField ItemStyle-Width="25px" HeaderText="Days" ItemStyle-HorizontalAlign="Center">
                                                                                                                        <ItemTemplate>
                                                                                                                            <asp:TextBox ID="txtIntermediateSubEventComlianceDays" Visible='<%# visibleTextDays(Convert.ToInt32(Eval("ComplianceID")))%>' Text='<%# Eval("Days") %>' Width="50px" runat="server"></asp:TextBox>
                                                                                                                            <asp:Label ID="lblFrequency" Visible='<%# visiblelblFrequency(Convert.ToInt32(Eval("ComplianceID")))%>' Text='<%# GetFrequency(Convert.ToInt32(Eval("ComplianceID")))%>' runat="server"></asp:Label>
                                                                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator17" ErrorMessage="Please enter compliance trigger days"
                                                                                                                                ControlToValidate="txtIntermediateSubEventComlianceDays" Enabled='<%# visibleTextDays(Convert.ToInt32(Eval("ComplianceID")))%>' runat="server" ValidationGroup="ComplianceValidationGroup" Display="None" />
                                                                                                                        </ItemTemplate>
                                                                                                                    </asp:TemplateField>
                                                                                                                </Columns>
                                                                                                                <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                                                                                            </asp:GridView>
                                                                                                        </div>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                    </Columns>
                                                                                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                                                                </asp:GridView>
                                                                                <%--Complaince--%>
                                                                                <asp:GridView ID="gvComplianceGrid" GridLines="None" runat="server" Width="95%"
                                                                                    AutoGenerateColumns="false" DataKeyNames="SequenceID" OnRowDataBound="gvComplianceGrid_OnRowDataBound">
                                                                                    <Columns>
                                                                                        <asp:BoundField ItemStyle-Width="120px" DataField="ComplianceID" HeaderText="ID" />
                                                                                        <asp:BoundField ItemStyle-Width="650px" DataField="Name" HeaderText="Compliance Name" />
                                                                                        <asp:TemplateField ItemStyle-Width="25px" HeaderText="Days" ItemStyle-HorizontalAlign="Center">
                                                                                            <ItemTemplate>
                                                                                                <asp:TextBox ID="txtSubEventComlianceDays" Text='<%# Eval("Days") %>' Visible='<%# visibleTextDays(Convert.ToInt32(Eval("ComplianceID")))%>'  Width="50px" runat="server"></asp:TextBox>
                                                                                                <asp:Label ID="lblFrequency1" Visible='<%# visiblelblFrequency(Convert.ToInt32(Eval("ComplianceID")))%>' Text='<%# GetFrequency(Convert.ToInt32(Eval("ComplianceID")))%>' runat="server"></asp:Label>
                                                                                                 <asp:RequiredFieldValidator ID="RequiredFieldValidator17" ErrorMessage="Please enter compliance trigger days"
                                                                                                 ControlToValidate="txtSubEventComlianceDays" Enabled='<%# visibleTextDays(Convert.ToInt32(Eval("ComplianceID")))%>' runat="server" ValidationGroup="ComplianceValidationGroup" Display="None" />
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                    </Columns>
                                                                                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                                                                </asp:GridView>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                                    </asp:GridView>
                                                </div>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                            <AlternatingRowStyle BackColor="#E6EFF7" />
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <div style="margin-bottom: 7px; float: right; margin-right: 750px; margin-top: 10px;">
            <div style="height: 30px; width: 30px; background-color: Chocolate; float: left; margin-right: 5px"></div>
            <div style="float: left; color: #666666; padding: 9px;">
                Informative checklist no action required.
            </div>

            <div style="height: 30px; width: 30px; background-color: SlateBlue; float: left; margin-right: 5px"></div>
            <div style="float: left; color: #666666; padding: 9px;">
                Actionable checklist.
            </div>

            <asp:Button Text="Save" runat="server" ID="btnSave" Visible="false" OnClick="btnSave_Click" CssClass="button"
                ValidationGroup="ComplianceValidationGroup" />
        </div>
    </div>

    <div>
        <div id="divAutoTriggerDialog">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div style="margin-bottom: 4px; margin-left: 95px">
                        <asp:Label runat="server" ID="lblImsg" ForeColor="Red"></asp:Label>
                        <asp:CustomValidator ID="CustomValidator1" runat="server" class="alert alert-block alert-danger fade in" EnableClientScript="False"
                            ValidationGroup="InternalComplianceValidationGroup" Display="None" />
                        <asp:ValidationSummary ID="ValidationSummary2" runat="server" class="alert alert-block alert-danger fade in"
                            ValidationGroup="InternalComplianceValidationGroup" />
                    </div>
                    <div>
                        <div style="margin-bottom: 7px">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                            <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                Is Auto Trigger
                            </label>
                            <asp:CheckBox ID="chkAutoTrigger" AutoPostBack="true" OnCheckedChanged="chkAutoTrigger_CheckedChanged" runat="server" />
                        </div>
                        <div id="divAutoTrigger" runat="server" style="margin-bottom: 7px">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                            <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                Auto Trigger Days
                            </label>
                            <asp:TextBox ID="txtAutoTriggerDays" Width="80px" runat="server"></asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers"
                                TargetControlID="txtAutoTriggerDays" />
                            <asp:RequiredFieldValidator ID="rfvAutoTriggerDays" ErrorMessage="Please enter Days."
                                ControlToValidate="txtAutoTriggerDays" runat="server" ValidationGroup="InternalComplianceValidationGroup"
                                Display="None" />
                        </div>
                        <div style="margin-top: 50px; margin-left: 215px">
                            <asp:Button Text="Save" runat="server" class="btn btn-search" Style="margin-top: -30px;" ValidationGroup="InternalComplianceValidationGroup" ID="btnSaveAutoTrigger" OnClick="btnSaveAutoTrigger_Click" />
                        </div>
                    </div>
                    <asp:HiddenField ID="HiddenField2" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>

