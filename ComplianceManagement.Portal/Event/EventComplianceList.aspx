﻿<%@ Page Title="Compliance List" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" EnableEventValidation="true"
    CodeBehind="EventComplianceList.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Event.EventComplianceList" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../NewCSS/ComplienceStyleSeet.css" rel="stylesheet" />
    <script src="../Newjs/tagging.js" type="text/javascript"></script>
    <link href="../NewCSS/bootstrap-tagsinput.css" rel="stylesheet" />
    <link href="../NewCSS/bootstrap.min.css" rel="stylesheet" />
    <link href="../NewCSS/bootstrap-theme.css" rel="stylesheet" />


    <script type="text/javascript" defer="defer">
        $(document).ready(function () {
            function WireUpValues() {
                //$(window).keydown(function (event) {
                //    if (event.keyCode == 13) {
                //        event.preventDefault();
                //        return false;
                //    }
                //});             
                $('input[data-role="tagsinput"]').tagsinput({
                });
            }
        });


        function fopendocfileReview() {
            if ($('#BodyContent_hdnFile').val() != '') {
                window.open('../docviewerSample.aspx?docurl=' + $('#BodyContent_hdnFile').val() + "&Internalsatutory=S", '_blank', '', '');
            }
        }
        function ResolveUrl() {
            $('input[data-role="tagsinput"]').tagsinput({
            });
            $('input#BodyContent_isOnline').change(function () { if ($('input#BodyContent_isOnline').is(':checked')) { $('#BodyContent_ddlDueDateType').attr('disabled', 'disabled') } else { $('#BodyContent_ddlDueDateType').removeAttr('disabled') } });
        }
    </script>
    <style type="text/css">
        .bootstrap-tagsinput {
            width: 390px;
        }

        [class="bootstrap-tagsinput"] input {
            width: 390px;
        }

        .label {
            width: auto;
            color: black;
        }

        tr.spaceUnder > td {
            padding-bottom: 1em;
        }


        .bootstrap-tagsinput .tag {
            margin-top: 3px;
            color: black;
        }
    </style>

    <script type="text/javascript">
        $(function () {
            $(document).tooltip();
        });

        $(document).ready(function () {
            if (document.getElementById('BodyContent_saveopo').value == "true") {

                $('#divComplianceDetailsDialog').dialog({
                    height: 670,
                    width: 900,
                    autoOpen: false,
                    draggable: true,
                    title: "Compliance Details",
                    open: function (type, data) {
                        $(this).parent().appendTo("form");
                    }
                });
                newfun();
            }
        });
        function newfun() {

            $("#divComplianceDetailsDialog").dialog('open');

        }

        function initializeDatePicker1(date) {
            var startDate = new Date();
            $(".StartDate").datepicker({
                dateFormat: 'dd-mm-yy',
                setDate: startDate,
                numberOfMonths: 1
            });
        }

        function setDate() {
            $(".StartDate").datepicker();
        }


        function initializeDatePickerStart() {

            var startDate = new Date();
            var maxyr = new Date().getFullYear().toString();
            maxyr = parseInt(maxyr) + 100;

            $("#<%= tbxStartDate.ClientID %>").datepicker({
                dateFormat: 'dd-mm-yy',
                numberOfMonths: 1,
                changeYear: true,
                yearRange: '1900:' + maxyr.toString()
            });
        }
        var validFilesTypes = ["exe", "bat", "dll"];
        function ValidateFile() {

            var label = document.getElementById("<%=Label2.ClientID%>");
            var fuSampleFile = $("#<%=fuSampleFile.ClientID%>").get(0).files;
            var isValidFile = true;

            for (var i = 0; i < fuSampleFile.length; i++) {
                var fileExtension = fuSampleFile[i].name.split('.').pop();
                if (validFilesTypes.indexOf(fileExtension) != -1) {
                    isValidFile = false;
                    break;
                }
            }

            if (!isValidFile) {
                label.style.color = "red";
                //label.innerHTML = "Invalid file uploded. .exe,.zip,.bat formats not supported.";
                label.innerHTML = "Invalid file uploded. .exe,.bat formats not supported.";
            }
            return isValidFile;
        }

    </script>
    <style type="text/css">
        .label {
            display: inline-block;
            font-weight: normal;
            font-size: 12px;
        }

        .ui-tooltip {
            max-width: 700px;
            font-weight: normal;
            font-size: 12px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upCompliancesList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <table width="100%">
                <tr>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlComplinceCatagory" Style="padding: 0px; margin: 0px; height: 22px; width: 220px;"
                            CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlComplinceCatagory_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlFilterComplianceType" Style="padding: 0px; margin: 0px; height: 22px; width: 220px;"
                            CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlFilterComplianceType_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:DropDownList runat="server" Visible="false" ID="ddlFilterFrequencies" Style="padding: 0px; margin: 0px; height: 22px; width: 220px;"
                            CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlFilterComplianceType_SelectedIndexChanged">
                        </asp:DropDownList>
                        <asp:DropDownList runat="server" ID="ddlAct1" Style="padding: 0px; margin: 0px; height: 22px; width: 220px;"
                            OnSelectedIndexChanged="ddlAct1_SelectedIndexChanged" CssClass="txtbox" AutoPostBack="true" />

                    </td>
                    <td>
                        <asp:RadioButton ID="rdFunctionBased" Visible="false" Text="Function Based" AutoPostBack="true" Width="110px" GroupName="ComplianceType" runat="server"
                            OnCheckedChanged="rdFunctionBased_CheckedChanged" />
                    </td>
                    <td>
                        <asp:RadioButton ID="rdChecklist" Visible="false" Text="Checklist" AutoPostBack="true" Width="100px" GroupName="ComplianceType" runat="server"
                            OnCheckedChanged="rdChecklist_CheckedChanged" />
                    </td>
                    <td style="width: 25%; padding-right: 20px;" align="right">Filter :
                        <asp:TextBox runat="server" ID="tbxFilter" Width="250px" MaxLength="50" AutoPostBack="true"
                            OnTextChanged="tbxFilter_TextChanged" />
                    </td>
                    <td class="newlink" align="right" style="width: 80px">
                        <asp:LinkButton Text="Add New" runat="server" ID="btnAddCompliance" OnClick="btnAddCompliance_Click" Visible="false" />
                    </td>
                </tr>
            </table>
            <asp:Panel ID="Panel1" Width="100%" Height="500px" ScrollBars="Vertical" runat="server">
                <asp:GridView runat="server" ID="grdCompliances" AutoGenerateColumns="false" GridLines="Vertical" OnRowDataBound="grdCompliances_RowDataBound"
                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true" OnRowCreated="grdCompliances_RowCreated"
                    CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="100" Width="100%" OnSorting="grdCompliances_Sorting"
                    Font-Size="12px" DataKeyNames="ID" OnRowCommand="grdCompliances_RowCommand" OnPageIndexChanging="grdCompliances_PageIndexChanging">
                    <Columns>
                        <asp:BoundField DataField="ID" HeaderText="ID" ItemStyle-Width="50px" SortExpression="ID" />
                        <asp:TemplateField HeaderText="Act Name" ItemStyle-Height="20px" HeaderStyle-Height="20px" ItemStyle-Width="100px" SortExpression="ActName">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                    <asp:Label runat="server" Text='<%# Eval("ActName") %>' ToolTip='<%# Eval("ActName") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Description" ItemStyle-Width="240px" SortExpression="ShortDescription">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px;">
                                    <asp:Label runat="server" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("Description") %>' CssClass="label"></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Sections" HeaderText="Sections" ItemStyle-Width="240px" SortExpression="Sections" />
                        <asp:TemplateField HeaderText="Upload Document" ItemStyle-Width="140px" SortExpression="UploadDocument">
                            <ItemTemplate>
                                <%# Convert.ToBoolean(Eval("UploadDocument")) ? "Yes" : "No" %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Risk" HeaderText="Risk Type" ItemStyle-Width="150px" SortExpression="Risk" />
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Status" ItemStyle-Width="50px">
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%# ComplianceActiveOrInActive(Convert.ToInt32(Eval("ID"))) %>' CommandArgument='<%# Eval("ID") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:LinkButton ID="lbtEdit" runat="server" CommandName="EDIT_COMPLIANCE" CommandArgument='<%# Eval("ID") %>'><img src="../Images/edit_icon.png" alt="Edit Compliance"/></asp:LinkButton>
                                <asp:LinkButton ID="lbtDelete" runat="server" CommandName="DELETE_COMPLIANCE" CommandArgument='<%# Eval("ID") %>'
                                    OnClientClick="return confirm('Are you certain you want to delete this compliance?');"><img src="../Images/delete_icon.png" alt="Delete Compliance"/></asp:LinkButton>
                                <asp:LinkButton ID="lnkStatus" runat="server" Visible='<%# ButtonDisplayComplianceActiveOrInActive(Convert.ToInt32(Eval("ID"))) %>' CommandName="STATUS" CommandArgument='<%# Eval("ID") %>'><img src="../Images/change_status_icon.png" alt="Status change"/></asp:LinkButton>
                            </ItemTemplate>
                            <HeaderTemplate>
                            </HeaderTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle BackColor="#CCCC99" />
                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                    <PagerSettings Position="Top" />
                    <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                    <AlternatingRowStyle BackColor="#E6EFF7" />
                    <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                    <EmptyDataTemplate>
                        No Records Found.
                    </EmptyDataTemplate>
                </asp:GridView>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div id="divComplianceDetailsDialog">
        <asp:UpdatePanel ID="upComplianceDetails" runat="server" UpdateMode="Conditional" OnLoad="upComplianceDetails_Load">
            <ContentTemplate>
                <div style="margin: 5px 5px 80px;">
                    <div style="margin-bottom: 4px">
                        <asp:ValidationSummary runat="server" CssClass="vdsummary"
                            ValidationGroup="ComplianceValidationGroup" />
                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                            ValidationGroup="ComplianceValidationGroup" Display="None" />
                        <asp:Label runat="server" ID="lblErrorMassage" ForeColor="Red"></asp:Label>
                    </div>
                    <div style="margin-bottom: 7px; position: relative; display: inline-block;">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                            *</label>
                        <label style="width: 240px; display: block; float: left; font-size: 13px; color: #333;">
                            Act Name</label>
                        <asp:DropDownList runat="server" ID="ddlAct" OnSelectedIndexChanged="ddlAct_SelectedIndexChanged" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                            CssClass="txtbox" />
                        <asp:CompareValidator ID="CompareValidator1" ErrorMessage="Please select Act Name."
                            ControlToValidate="ddlAct" runat="server" ValueToCompare="-1" Operator="NotEqual"
                            ValidationGroup="ComplianceValidationGroup" Display="None" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                            *</label>
                        <label style="width: 240px; display: block; float: left; font-size: 13px; color: #333;">
                            Section(s) / Rule(s)</label>
                        <asp:TextBox runat="server" ID="tbxSections" Style="height: 30px; width: 390px;" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" ErrorMessage="Section(s) / Rule(s) can not be empty."
                            ControlToValidate="tbxSections" runat="server" ValidationGroup="ComplianceValidationGroup"
                            Display="None" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                            *</label>
                        <label style="width: 240px; display: block; float: left; font-size: 13px; color: #333;">
                            Short Description</label>
                        <asp:TextBox runat="server" ID="txtShortDescription" TextMode="MultiLine" MaxLength="100" Style="height: 30px; width: 390px;" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator14" ErrorMessage="Short Description can not be empty."
                            ControlToValidate="txtShortDescription" runat="server" ValidationGroup="ComplianceValidationGroup"
                            Display="None" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                            *</label>
                        <label style="width: 240px; display: block; float: left; font-size: 13px; color: #333;">
                            Short Form</label>
                        <asp:TextBox runat="server" ID="txtshortform" TextMode="MultiLine" Style="height: 50px; width: 390px;" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator13" ErrorMessage="Short form can not be empty."
                            ControlToValidate="txtshortform" runat="server" ValidationGroup="ComplianceValidationGroup"
                            Display="None" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                            *</label>
                        <label style="width: 240px; display: block; float: left; font-size: 13px; color: #333;">
                            Detailed Description</label>
                        <asp:TextBox runat="server" ID="tbxDescription" TextMode="MultiLine" Style="height: 50px; width: 390px;" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator15" ErrorMessage="Description can not be empty."
                            ControlToValidate="tbxDescription" runat="server" ValidationGroup="ComplianceValidationGroup"
                            Display="None" />
                    </div>
                    <div runat="server" id="div1" style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 240px; display: block; float: left; font-size: 13px; color: #333;">
                            Start Date</label>
                        <asp:TextBox runat="server" ID="tbxStartDate" Style="padding: 0px; margin: 0px; height: 30px; width: 150px;" CssClass="StartDate" />
                        <asp:RequiredFieldValidator ID="rfvStartDate" ErrorMessage="Please Select Start Date."
                            ControlToValidate="tbxStartDate" runat="server" ValidationGroup="ComplianceValidationGroup"
                            Display="None" />
                    </div>
                    <div runat="server" id="div2" style="margin-bottom: 12px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 240px; display: block; float: left; font-size: 13px; color: #333;">
                            Is Online?</label>
                        <asp:CheckBox ID="isOnline" runat="server" />
                    </div>
                     <div runat="server" id="div11" style="margin-bottom: 15px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 240px; display: block; float: left; font-size: 13px; color: #333;">
                            Is Mapping for customer?</label>
                        <asp:CheckBox ID="ChkIsMapped" runat="server" />
                    </div>
                    <div runat="server" id="div7" style="margin-bottom: 7px;">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 240px; display: block; float: left; font-size: 13px; color: #333;">
                            Due Date Type</label>
                        <asp:DropDownList runat="server" ID="ddlDueDateType" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;"
                            CssClass="txtbox" AutoPostBack="true">
                            <asp:ListItem Value="0">After Due Date </asp:ListItem>
                            <asp:ListItem Value="1">Before Due Date </asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 240px; display: block; float: left; font-size: 13px; color: #333;">
                            Compliance Tag</label>
                        <asp:TextBox runat="server" ID="txtCompliancetag" CssClass="txtbox" ClientIDMode="Static" data-role="tagsinput"
                            Style="padding: 0px; margin: 0px; height: 22px; width: 390px;" autocomplete="off" Rows="1" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 240px; display: block; float: left; font-size: 13px; color: #333;">
                            Industry
                        </label>
                        <asp:TextBox runat="server" ID="txtIndustry" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                            CssClass="txtbox" />
                        <div style="margin-left: 240px; position: absolute; z-index: 50; overflow-y: auto; background: white; border: 1px solid gray; height: 240px;" id="dvIndustry">

                            <asp:Repeater ID="rptIndustry" runat="server">
                                <HeaderTemplate>
                                    <table class="detailstable FadeOutOnEdit" id="RepeaterTable">
                                        <tr>
                                            <td style="width: 100px;">
                                                <asp:CheckBox ID="IndustrySelectAll" Text="Select All" runat="server" onclick="checkAll(this)" /></td>
                                            <td style="width: 282px;">
                                                <asp:Button runat="server" ID="btnRepeater" Text="Ok" Style="float: left" OnClick="btnRefresh_Click" /></td>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td style="width: 20px;">
                                            <asp:CheckBox ID="chkIndustry" runat="server" onclick="UncheckHeader();" /></td>
                                        <td style="width: 240px;">
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 250px; padding-bottom: 5px;">
                                                <asp:Label ID="lblIndustryID" runat="server" Visible="false" Text='<%# Eval("ID")%>' ToolTip='<%# Eval("ID") %>'></asp:Label>
                                                <asp:Label ID="lblIndustryName" runat="server" Text='<%# Eval("Name")%>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                            </div>
                                        </td>

                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>

                            </asp:Repeater>

                        </div>
                    </div>
                    <div style="margin-bottom: 12px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 240px; display: block; float: left; font-size: 13px; color: #333;">
                            Legal Entity Type
                        </label>
                        <asp:TextBox runat="server" ID="txtEntityType" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                            CssClass="txtbox" />
                        <div style="margin-left: 240px; position: absolute; z-index: 50; overflow-y: auto; background: white; border: 1px solid gray; height: 240px;" id="dvEntityType">

                            <asp:Repeater ID="rptEntityType" runat="server">
                                <HeaderTemplate>
                                    <table class="detailstable FadeOutOnEdit" id="RepeaterTable">
                                        <tr>
                                            <td style="width: 100px;">
                                                <asp:CheckBox ID="EntityTypeSelectAll" Text="Select All" runat="server" onclick="checkAllET(this)" /></td>
                                            <td style="width: 282px;">
                                                <asp:Button runat="server" ID="btnRepeaterEntityType" Text="Ok" Style="float: left" OnClick="btnRefresh_Click" /></td>
                                            <%--OnClick="btnRefresh_Click" --%>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td style="width: 20px;">
                                            <asp:CheckBox ID="chkEntityType" runat="server" onclick="UncheckHeaderET();" /></td>
                                        <td style="width: 240px;">
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 250px; padding-bottom: 5px;">
                                                <asp:Label ID="lblEntityTypeID" runat="server" Visible="false" Text='<%# Eval("ID")%>' ToolTip='<%# Eval("ID") %>'></asp:Label>
                                                <asp:Label ID="lblEntityTypeName" runat="server" Text='<%# Eval("EntityTypeName")%>' ToolTip='<%# Eval("EntityTypeName") %>'></asp:Label>
                                            </div>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                    </div>

                      <div runat="server" id="div9" style="margin-bottom: 12px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;"></label>
                        <label style="width: 240px; display: block; float: left; font-size: 13px; color: #333;">
                           Is ForSecretarial ?</label>
                         <asp:CheckBox ID="ChkIsForSecretarial" runat="server" OnCheckedChanged="ChkIsForSecretarial_Changed" AutoPostBack="true" />
                    </div>

                <%--    Secretarial Tag --%>

                      <div id="divSecretarial" runat="server" visible="false"  style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 240px; display: block; float: left; font-size: 13px; color: #333;">
                           Secretarial Tag Mapping
                        </label>
                        <asp:TextBox runat="server" ID="txtIsForSecretarial" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                            CssClass="txtbox" />
                        <div style="margin-left: 240px; position: absolute; z-index: 50; overflow-y: auto; background: white; border: 1px solid gray; height: 200px;" id="dvIsForSecretarial">

                            <asp:Repeater ID="rptIsForSecretarial" runat="server">
                                <HeaderTemplate>
                                    <table class="detailstable FadeOutOnEdit" id="RepeaterTable">
                                        <tr>
                                            <td style="width: 100px;">
                                                <asp:CheckBox ID="IsForSecretarialSelectAll" Text="Select All" runat="server" onclick="checkAllIsForSecretarial(this)" /></td>
                                            <td style="width: 282px;">
                                                <asp:Button runat="server" ID="btnRepeaterIsForSecretarial" Text="Ok" Style="float: left" OnClick="btnRefresh_Click" /></td>
                                       
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td style="width: 20px;">
                                            <asp:CheckBox ID="chkIsForSecretarial" runat="server" onclick="UncheckHeaderIsForSecretarial();" /></td>
                                        <td style="width: 200px;">
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 250px; padding-bottom: 5px;">
                                                <asp:Label ID="lblIsForSecretarialID" runat="server" Visible="false" Text='<%# Eval("ID")%>' ToolTip='<%# Eval("ID") %>'></asp:Label>
                                                <asp:Label ID="lblIsForSecretarialName" runat="server" Text='<%# Eval("Tag")%>'></asp:Label>
                                              
                                            </div>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                    </div>

                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 240px; display: block; float: left; font-size: 13px; color: #333;">
                            Compliance Type</label>
                        <asp:DropDownList runat="server" ID="ddlComplianceType" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                            CssClass="txtbox" OnSelectedIndexChanged="ddlComplianceType_SelectedIndexChanged">
                            <asp:ListItem Text="Function based" Value="0" />
                            <asp:ListItem Text="Checklist" Value="1" />
                            <asp:ListItem Text="Time Based" Value="2" />
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator12" ErrorMessage="Please Select Compliance Type."
                            ControlToValidate="ddlComplianceType" runat="server" ValidationGroup="ComplianceValidationGroup"
                            Display="None" />
                    </div>
                    <div runat="server" id="divFreqCheckbox" style="margin-bottom: 12px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;"></label>
                        <label style="width: 240px; display: block; float: left; font-size: 13px; color: #333;">
                            Is Frequency Based?</label>
                        <asp:CheckBox ID="chkFrequency" AutoPostBack="true" OnCheckedChanged="chkFrequency_CheckedChanged" runat="server" />
                    </div>
                     <div runat="server" id="divForcefulClosure" style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;"></label>
                        <label style="width: 240px; display: block; float: left; font-size: 13px; color: #333;">
                            Is Forceful Closure?</label>
                        <asp:CheckBox ID="chkForcefulClosure" AutoPostBack="true" runat="server" />
                    </div>
                    <div id="divFrequencyDuedate" runat="server">
                        <div style="margin-bottom: 7px" id="divFrequency" runat="server">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                *</label>
                            <label style="width: 240px; display: block; float: left; font-size: 13px; color: #333;">
                                Frequency</label>
                            <asp:DropDownList runat="server" ID="ddlFrequency" AutoPostBack="true" OnSelectedIndexChanged="ddlFrequency_SelectedIndexChanged" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                CssClass="txtbox">
                            </asp:DropDownList>
                            <asp:CompareValidator ErrorMessage="Please select Frequency." ControlToValidate="ddlFrequency" ID="cvddlFrequency"
                                runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceValidationGroup"
                                Display="None" />
                        </div>
                        <div style="margin-bottom: 7px" id="vivDueDate" runat="server">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                            <label style="width: 240px; display: block; float: left; font-size: 13px; color: #333;">
                                Due Date</label>
                            <asp:DropDownList runat="server" ID="ddlDueDate" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                CssClass="txtbox">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="reqddlDueDate" ErrorMessage="Please Select Due Date."
                                ControlToValidate="ddlDueDate" runat="server" ValidationGroup="ComplianceValidationGroup"
                                Display="None" Enabled="false" />
                        </div>
                        <div style="margin-bottom: 7px" id="vivWeekDueDays" runat="server" visible="false">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                            <label style="width: 240px; display: block; float: left; font-size: 13px; color: #333;">
                                Week Due Day</label>
                            <asp:DropDownList runat="server" ID="ddlWeekDueDay" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                CssClass="txtbox">
                            </asp:DropDownList>
                            <asp:CompareValidator ErrorMessage="Please Select Week Due Day." ControlToValidate="ddlWeekDueDay" ID="vaDueWeekDay"
                                runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceValidationGroup"
                                Display="None" />
                        </div>
                        <div style="margin-bottom: 7px" id="Div8" runat="server">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                            <label style="width: 240px; display: block; float: left; font-size: 13px; color: #333;">
                                Trigger Number
                            </label>
                                <asp:TextBox runat="server" ID="txtTriggerNo" Style="padding: 0px; margin: 0px; height: 30px; width: 150px;" />
                            <asp:RequiredFieldValidator ID="reqtxtTriggerNo" ErrorMessage="Trigger Number can not be empty."
                                ControlToValidate="txtTriggerNo" runat="server" ValidationGroup="ComplianceValidationGroup"
                                Display="None" />
                            <asp:CompareValidator ID="CompareValidator14" runat="server" ValidationGroup="ComplianceValidationGroup" 
                                ControlToValidate="txtTriggerNo" ErrorMessage="Trigger Number must greater than zero"
                                Operator="GreaterThan" Type="Integer" ValueToCompare="0" />
                                 <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Numbers"
                                        TargetControlID="txtTriggerNo" />
                        </div>
                    </div>

                    <div id="divTimeBased" runat="server">
                        <div style="margin-bottom: 11px" id="div10" runat="server">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                *</label>
                            <label style="width: 240px; display: block; float: left; font-size: 13px; color: #333;">
                                Number Of Days Compliance Triggered
                            </label>
                             <asp:TextBox runat="server" ID="txttriggerNoOfDays" Style="padding: 0px; margin: 0px; height: 30px; width: 150px;"/>
                            <asp:RequiredFieldValidator ID="reqtxttriggerNoOfDays" ErrorMessage="Number Of Days Compliance Triggered can not be empty."
                                ControlToValidate="txttriggerNoOfDays" runat="server" ValidationGroup="ComplianceValidationGroup"
                                Display="None" />
                            <asp:CompareValidator ID="CompareValidator16" runat="server"
                                ControlToValidate="txttriggerNoOfDays" ValidationGroup="ComplianceValidationGroup" ErrorMessage="Number Of Days Compliance Triggered must greater than zero"
                                Operator="GreaterThan" Type="Integer" ValueToCompare="0" />
                             <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers"
                                        TargetControlID="txttriggerNoOfDays" />
                        </div>
                        <div style="margin-bottom: 7px" id="Div12" runat="server">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                            <label style="width: 240px; display: block; float: left; font-size: 13px; color: #333;">
                                Interval Days
                            </label>
                            <asp:TextBox runat="server" ID="txtIntervalDays" Style="padding: 0px; margin: 0px; height: 30px; width: 150px;"/>
                            <asp:RequiredFieldValidator ID="reqtxtIntervalDays" ErrorMessage="Interval Days can not be empty."
                                ControlToValidate="txtIntervalDays" runat="server" ValidationGroup="ComplianceValidationGroup"
                                Display="None" />
                            <asp:CompareValidator ID="CompareValidator15" runat="server"
                                ControlToValidate="txtIntervalDays" ValidationGroup="ComplianceValidationGroup" ErrorMessage="Interval Days must greater than zero"
                                Operator="GreaterThan" Type="Integer" ValueToCompare="0" />
                            <asp:CompareValidator runat="server" id="cmpNumbers" controltovalidate="txtIntervalDays" 
                                controltocompare="txttriggerNoOfDays" ValidationGroup="ComplianceValidationGroup" operator="LessThanEqual" type="Integer" errormessage="Number of days compliance triggered greater than or equal to interval days" /><br />
                             <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers"
                                        TargetControlID="txtIntervalDays" />
                        </div>
                    </div>
                    <div runat="server" id="divNatureOfCompliance" style="margin-bottom: 7px">
                        <label id="lblNature" runat="server" style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                            *</label>
                        <label style="width: 240px; display: block; float: left; font-size: 13px; color: #333;">
                            Nature Of Compliance</label>
                        <asp:DropDownList runat="server" ID="ddlNatureOfCompliance" OnSelectedIndexChanged="ddlNatureOfCompliance_SelectedIndexChanged" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                            CssClass="txtbox">
                        </asp:DropDownList>
                        <asp:CompareValidator ID="cmpValnature" ErrorMessage="Please select Nature of Compliance." ControlToValidate="ddlNatureOfCompliance"
                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceValidationGroup"
                            Display="None" />
                    </div>

                    <div runat="server" id="divNatureOfSubCompliance" style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                        <label style="width: 240px; display: block; float: left; font-size: 13px; color: #333;">
                            Nature Of Compliance Sub Type</label>
                        <asp:DropDownList runat="server" ID="ddlComplianceSubType" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                            CssClass="txtbox">
                        </asp:DropDownList>
                    </div>

                    <div style="margin-bottom: 7px" id="divUploadDocument" runat="server">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 240px; display: block; float: left; font-size: 13px; color: #333;">
                            Is Document Mandatory(Performer)</label>
                        <asp:RadioButton ID="rdoUploadDoc" Checked="true" Text="Yes" Font-Bold="true" GroupName="radioUplaod" runat="server" />
                        <asp:RadioButton ID="rdoNotUploadDoc" Text="No" Font-Bold="true" GroupName="radioUplaod" runat="server" />
                    </div>

                    <div style="margin-bottom: 7px" id="divActionable" runat="server">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 240px; display: block; float: left; font-size: 13px; color: #333;">
                            Compliance Actionable/Informational
                        </label>
                        <asp:RadioButton ID="rdoComplianceVisible" Checked="true" Text="Actionable" Font-Bold="true" GroupName="radioComplianceVisible" runat="server" />
                        <asp:RadioButton ID="rdoNotComplianceVisible" Text="Informational" Font-Bold="true" GroupName="radioComplianceVisible" runat="server" />
                    </div>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div runat="server" id="divFunctionBased">
                                <div style="margin-bottom: 7px;margin-top: 2%;" id="divNonComplianceType1" runat="server">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                    <label style="width: 240px; display: block; float: left; font-size: 13px; color: #333;">
                                        Non Compliance Type</label>
                                    <asp:DropDownList runat="server" ID="ddlNonComplianceType" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                        CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlNonComplianceType_SelectedIndexChanged">
                                        <asp:ListItem Text="< Select >" Value="-1" />
                                        <asp:ListItem Text="Both" Value="2" />
                                        <asp:ListItem Text="Monetary" Value="0" />
                                        <asp:ListItem Text="Non-Monetary" Value="1" />
                                    </asp:DropDownList>
                                </div>
                                <div runat="server" id="divMonetary" style="border: 1px solid grey; padding: 5px; margin-top: 10px; margin-bottom: 10px">
                                    <div style="margin-bottom: 25px">
                                        <label style="display: block; float: left; font-size: 13px; color: #333; font-weight: bold">
                                            Monetory</label>
                                    </div>
                                    <div style="margin-bottom: 7px; clear: both;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                            *</label>
                                        <label style="width: 240px; display: block; float: left; font-size: 13px; color: #333;">
                                            Fixed Minimum</label>
                                        <asp:TextBox runat="server" ID="tbxFixedMinimum" Style="height: 30px; width: 390px;"
                                            MaxFixedMinimum="4" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Fixed minimum can not be empty."
                                            ControlToValidate="tbxFixedMinimum" runat="server" ValidationGroup="ComplianceValidationGroup"
                                            Display="None" />
                                        <asp:CompareValidator ID="CompareValidator2" ErrorMessage="Fixed minimum is a not valid number."
                                            ControlToValidate="tbxFixedMinimum" Operator="DataTypeCheck" Type="Double" runat="server"
                                            Display="None" ValidationGroup="ComplianceValidationGroup" />
                                    </div>
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                            *</label>
                                        <label style="width: 240px; display: block; float: left; font-size: 13px; color: #333;">
                                            Fixed Maximum</label>
                                        <asp:TextBox runat="server" ID="tbxFixedMaximum" Style="height: 30px; width: 390px;"
                                            MaxFixedMaximum="4" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="Fixed maximum can not be empty."
                                            ControlToValidate="tbxFixedMaximum" runat="server" ValidationGroup="ComplianceValidationGroup"
                                            Display="None" />
                                        <asp:CompareValidator ID="CompareValidator3" ErrorMessage="Fixed maximum is a not valid number."
                                            ControlToValidate="tbxFixedMaximum" Operator="DataTypeCheck" Type="Double" runat="server"
                                            Display="None" ValidationGroup="ComplianceValidationGroup" />
                                        <asp:CompareValidator ID="CompareValidatormaximum" runat="server" ErrorMessage="Fixed maximum should be greater than fixed minimum." Type="Double"
                                            ControlToCompare="tbxFixedMinimum" ControlToValidate="tbxFixedMaximum" Display="None" Operator="GreaterThanEqual" ValidationGroup="ComplianceValidationGroup" />
                                    </div>
                                    <div style="margin-bottom: 7px; margin-left: 240px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <asp:DropDownList runat="server" ID="ddlPerDayMonth" Style="padding: 0px; margin: 0px; margin-left: 240px; height: 22px; width: 390px;"
                                            CssClass="txtbox" AutoPostBack="true">
                                            <asp:ListItem Text="< Select >" Value="-1" />
                                            <asp:ListItem Text="Day" Value="0" />
                                            <asp:ListItem Text="Month" Value="1" />
                                            <asp:ListItem Text="Per Instance" Value="2" />
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Please select Day or Month."
                                            InitialValue="-1" ControlToValidate="ddlPerDayMonth" runat="server" ValidationGroup="ComplianceValidationGroup"
                                            Display="None" />
                                    </div>
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                            *</label>
                                        <label style="width: 240px; display: block; float: left; font-size: 13px; color: #333;">
                                            Variable Amount Rs.</label>
                                        <asp:TextBox runat="server" ID="tbxVariableAmountPerDay" Style="height: 30px; width: 390px;"
                                            MaxVariableAmountPerDay="4" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ErrorMessage="Variable amount can not be empty."
                                            ControlToValidate="tbxVariableAmountPerDay" runat="server" ValidationGroup="ComplianceValidationGroup"
                                            Display="None" />
                                        <asp:CompareValidator ID="CompareValidator4" ErrorMessage="Variable amount is a not valid number."
                                            ControlToValidate="tbxVariableAmountPerDay" Operator="DataTypeCheck" Type="Double"
                                            runat="server" Display="None" ValidationGroup="ComplianceValidationGroup" />
                                    </div>
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                            *</label>
                                        <label style="width: 240px; display: block; float: left; font-size: 13px; color: #333;">
                                            Variable Amount (Max)</label>
                                        <asp:TextBox runat="server" ID="tbxVariableAmountPerDayMax" Style="height: 30px; width: 390px;"
                                            MaxVariableAmountPerDayMax="4" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ErrorMessage="Variable amount (Max) can not be empty."
                                            ControlToValidate="tbxVariableAmountPerDayMax" runat="server" ValidationGroup="ComplianceValidationGroup"
                                            Display="None" />
                                        <asp:CompareValidator ID="CompareValidator5" ErrorMessage="Variable amount (max) is a not valid number."
                                            ControlToValidate="tbxVariableAmountPerDayMax" Operator="DataTypeCheck" Type="Double"
                                            runat="server" Display="None" ValidationGroup="ComplianceValidationGroup" />
                                        <asp:CompareValidator ID="CompareValidator12" runat="server" ErrorMessage="Variable Amount (Max) should be greater than Variable Amount Rs." Type="Double"
                                            ControlToCompare="tbxVariableAmountPerDay" ControlToValidate="tbxVariableAmountPerDayMax" Display="None" Operator="GreaterThanEqual" ValidationGroup="ComplianceValidationGroup" />
                                    </div>
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                            *</label>
                                        <label style="width: 240px; display: block; float: left; font-size: 13px; color: #333;">
                                            Variable Amount (%)</label>
                                        <asp:TextBox runat="server" ID="tbxVariableAmountPercent" Style="height: 30px; width: 390px;"
                                            MaxVariableAmountPercent="4" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ErrorMessage="Variable amount (%) can not be empty."
                                            ControlToValidate="tbxVariableAmountPercent" runat="server" ValidationGroup="ComplianceValidationGroup"
                                            Display="None" />
                                       <%-- <asp:CompareValidator ID="CompareValidator6" ErrorMessage="Variable amount (%) is a not valid number."
                                            ControlToValidate="tbxVariableAmountPercent" Operator="DataTypeCheck" Type="Double"
                                            runat="server" Display="None" ValidationGroup="ComplianceValidationGroup" />--%>
                                    </div>
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                            *</label>
                                        <label style="width: 240px; display: block; float: left; font-size: 13px; color: #333;">
                                            Variable Amount (% Max)</label>
                                        <asp:TextBox runat="server" ID="tbxVariableAmountPercentMaximum" Style="height: 30px; width: 390px;"
                                            MaxVariableAmountPercentMaximum="4" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ErrorMessage="Variable amount (% max) can not be empty."
                                            ControlToValidate="tbxVariableAmountPercentMaximum" runat="server" ValidationGroup="ComplianceValidationGroup"
                                            Display="None" />
                                      <%--  <asp:CompareValidator ID="CompareValidator7" ErrorMessage="Variable amount (% max) is a not valid number."
                                            ControlToValidate="tbxVariableAmountPercentMaximum" Operator="DataTypeCheck"
                                            Type="Double" runat="server" Display="None" ValidationGroup="ComplianceValidationGroup" />--%>
                                      <%--  <asp:CompareValidator ID="CompareValidator13" runat="server" ErrorMessage="Variable Amount (% Max) should be greater than Variable Amount Variable Amount (%)." Type="Double"
                                            ControlToCompare="tbxVariableAmountPerDay" ControlToValidate="tbxVariableAmountPerDayMax" Display="None" Operator="GreaterThanEqual" ValidationGroup="ComplianceValidationGroup" />--%>
                                    </div>
                                </div>
                                <div runat="server" id="divNonMonetary" style="border: 1px solid grey; padding: 5px; margin-top: 10px; margin-bottom: 10px">
                                    <div style="margin-bottom: 25px">
                                        <label style="display: block; float: left; font-size: 13px; color: #333; font-weight: bold">
                                            Non-Monetory</label>
                                    </div>
                                    <div style="margin-bottom: 7px; clear: both">
                                        <label style="width: 240px; display: block; float: left; font-size: 13px; color: #333;">
                                            Imprisonment</label>
                                        <asp:CheckBox runat="server" ID="chbImprisonment" CssClass="txtbox" AutoPostBack="true"
                                            OnCheckedChanged="chbImprisonment_CheckedChanged" />
                                    </div>
                                    <div id="divImprisonmentDetails" runat="server" visible="false">
                                        <div style="margin-bottom: 7px">
                                            <label style="width: 240px; display: block; float: left; font-size: 13px; color: #333;">
                                                Designation</label>
                                            <asp:TextBox runat="server" ID="tbxDesignation" Style="height: 30px; width: 390px;"
                                                MaxDesignation="4" />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ErrorMessage="Designation can not be empty."
                                                ControlToValidate="tbxDesignation" runat="server" ValidationGroup="ComplianceValidationGroup"
                                                Display="None" />
                                        </div>
                                        <div style="margin-bottom: 7px">
                                            <label style="width: 240px; display: block; float: left; font-size: 13px; color: #333;">
                                                Minimum
                                            </label>
                                            <asp:TextBox runat="server" ID="tbxMinimumYears" Style="height: 30px; width: 155px;"
                                                MaxMinimumYears="4" />
                                            <asp:DropDownList runat="server" ID="ddlMinimumYear" Style="padding: 0px; margin: 0px; margin-left: 9px; height: 22px; width: 120px;"
                                                CssClass="txtbox" AutoPostBack="true">
                                                <asp:ListItem Text="< Select >" Value="-1" />
                                                <asp:ListItem Text="Month" Value="0" />
                                                <asp:ListItem Text="Year" Value="1" />
                                            </asp:DropDownList>
                                            <asp:CompareValidator ID="CompareValidator17" ErrorMessage="Please select Minimum Year/Month"
                                                ControlToValidate="ddlMinimumYear" runat="server" ValueToCompare="-1" Operator="NotEqual"
                                                ValidationGroup="ComplianceValidationGroup" Display="None" />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" ErrorMessage="Minimum months  can not be empty."
                                                ControlToValidate="tbxMinimumYears" runat="server" ValidationGroup="ComplianceValidationGroup"
                                                Display="None" />
                                            <asp:CompareValidator ID="CompareValidator9" ErrorMessage="Minimum months  is a not valid number."
                                                ControlToValidate="tbxMinimumYears" Operator="DataTypeCheck" Type="Integer" runat="server"
                                                Display="None" ValidationGroup="ComplianceValidationGroup" />
                                        </div>
                                        <div style="margin-bottom: 7px">
                                            <label style="width: 240px; display: block; float: left; font-size: 13px; color: #333;">
                                                Maximum</label>
                                            <asp:TextBox runat="server" ID="tbxMaximumYears" Style="height: 30px; width: 155px;"
                                                MaxMaximumYears="4" />
                                            <asp:DropDownList runat="server" ID="ddlMaximumYear" Style="padding: 0px; margin: 0px; margin-left: 9px; height: 22px; width: 120px;"
                                                CssClass="txtbox" AutoPostBack="true">
                                                <asp:ListItem Text="< Select >" Value="-1" />
                                                <asp:ListItem Text="Month" Value="0" />
                                                <asp:ListItem Text="Year" Value="1" />
                                            </asp:DropDownList>
                                            <asp:CompareValidator ID="CompareValidator18" ErrorMessage="Please select Maximum Year/Month"
                                                ControlToValidate="ddlMaximumYear" runat="server" ValueToCompare="-1" Operator="NotEqual"
                                                ValidationGroup="ComplianceValidationGroup" Display="None" />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator10" ErrorMessage="Maximum months can not be empty."
                                                ControlToValidate="tbxMaximumYears" runat="server" ValidationGroup="ComplianceValidationGroup"
                                                Display="None" />
                                            <asp:CompareValidator ID="CompareValidator10" ErrorMessage="Maximum months is not a valid number."
                                                ControlToValidate="tbxMaximumYears" Operator="DataTypeCheck" Type="Integer" runat="server"
                                                Display="None" ValidationGroup="ComplianceValidationGroup" />
                                            <asp:CompareValidator ID="CompareValidator11" runat="server" ErrorMessage="Maximum year should be greater than minimum year." Type="Double"
                                                ControlToCompare="tbxMinimumYears" ControlToValidate="tbxMaximumYears" Display="None" Operator="GreaterThanEqual" ValidationGroup="ComplianceValidationGroup" />
                                        </div>
                                    </div>
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 240px; display: block; float: left; font-size: 13px; color: #333;">
                                            Others</label>
                                        <asp:TextBox runat="server" ID="tbxNonComplianceEffects" Style="height: 30px; width: 390px;" />
                                    </div>
                                </div>
                            </div>
                            <div style="padding: 10px;">
                                <div style="margin-bottom: 7px">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                    <label style="width: 240px; display: block; float: left; font-size: 13px; color: #333;">
                                        Penalty Description</label>
                                    <asp:TextBox runat="server" ID="txtPenaltyDescription" TextMode="MultiLine" Style="height: 50px; width: 390px;" />
                                </div>

                                <div style="margin-bottom: 7px">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                    <label style="width: 240px; display: block; float: left; font-size: 13px; color: #333;">
                                        Reference Material Text</label>
                                    <asp:TextBox runat="server" ID="txtReferenceMaterial" TextMode="MultiLine" Style="height: 50px; width: 390px;" />
                                </div>
                                <div style="margin-bottom: 7px">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                        *</label>
                                    <label style="width: 240px; display: block; float: left; font-size: 13px; color: #333;">
                                        Risk Type</label>
                                    <asp:DropDownList runat="server" ID="ddlRiskType" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                        CssClass="txtbox">
                                        <asp:ListItem Text="< Select >" Value="-1" />
                                        <asp:ListItem Text="High" Value="0" />
                                        <asp:ListItem Text="Low" Value="2" />
                                        <asp:ListItem Text="Medium" Value="1" />
                                        <asp:ListItem Text="Critical" Value="3" />
                                    </asp:DropDownList>
                                    <asp:CompareValidator ID="CompareValidator8" ErrorMessage="Please select Risk Type."
                                        ControlToValidate="ddlRiskType" runat="server" ValueToCompare="-1" Operator="NotEqual"
                                        ValidationGroup="ComplianceValidationGroup" Display="None" />
                                </div>

                                <asp:UpdatePanel ID="upFileUploadPanel" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div style="margin-bottom: 12px" id="dvUploadDoc" runat="server" visible="true">
                                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                            <label style="width: 240px; display: block; float: left; font-size: 13px; color: #333;">
                                                Upload Sample Document</label>
                                            <asp:CheckBox runat="server" ID="chkDocument" CssClass="txtbox" Checked="true" OnCheckedChanged="chkDocument_CheckedChanged" AutoPostBack="true" />
                                        </div>
                                        <div style="margin-bottom: 7px" id="dvReqForms" runat="server">
                                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                            <label style="width: 240px; display: block; float: left; font-size: 13px; color: #333;">
                                                Required Forms</label>
                                            <asp:TextBox runat="server" ID="tbxRequiredForms" Style="height: 30px; width: 390px;" />
                                        </div>
                                        <div style="margin-bottom: 7px" id="Div6" runat="server">
                                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                            <label style="width: 240px; display: block; float: left; font-size: 13px; color: #333;">
                                                Sample Form Link</label>
                                            <asp:TextBox runat="server" ID="txtSampleFormLink" Style="height: 30px; width: 390px;" />
                                            <asp:HiddenField runat="server" ID="hdnFile" />
                                        </div>
                                        <div style="margin-bottom: 7px" id="dvSampleForm" runat="server">
                                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                            <label style="width: 240px; display: block; float: left; font-size: 13px; color: #333;">
                                                Sample Form</label>
                                            <%--<asp:Label runat="server" ID="lblSampleForm" CssClass="txtbox" />--%>
                                            <asp:FileUpload AllowMultiple="true" runat="server" ID="fuSampleFile" />
                                        </div>
                                        <div style="margin-bottom: 7px" id="divSampleForm" runat="server">
                                            <asp:Panel ID="Panel2" Width="100%" Height="100px" ScrollBars="Vertical" runat="server">
                                                <asp:GridView runat="server" ID="grdSampleForm" AutoGenerateColumns="false" GridLines="Vertical"
                                                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true"
                                                    CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="10" Width="100%"
                                                    Font-Size="12px" DataKeyNames="ID" OnRowCommand="grdSampleForm_RowCommand">
                                                    <Columns>
                                                        <asp:BoundField DataField="ID" HeaderText="ID" ItemStyle-Width="50px" SortExpression="ID" />
                                                        <asp:TemplateField HeaderText="Form Name" ItemStyle-Height="20px" HeaderStyle-Height="20px" ItemStyle-Width="100px">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                                                    <asp:Label runat="server" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Action" ItemStyle-Width="140px" ItemStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <asp:UpdatePanel ID="upFileUploadPanel" runat="server">
                                                                    <ContentTemplate>
                                                                        <asp:LinkButton ID="lbkDownload" runat="server" CommandName="DownloadSampleForm" CommandArgument='<%# Eval("ID") + "," + Eval("ComplianceID") %>'><img src="../Images/downloaddoc.png" alt="Download"/></asp:LinkButton>
                                                                        <asp:LinkButton ID="lbkDelete" runat="server" CommandName="DeleleSampleForm" CommandArgument='<%# Eval("ID") + "," + Eval("ComplianceID") %>'
                                                                            OnClientClick="return confirm('Are you sure you want to delete this Sampleform?');"><img src="../Images/delete_icon.png" alt="Delete Form"/></asp:LinkButton>
                                                                        <asp:LinkButton ID="lbkView" runat="server" OnClientClick="fopendocfileReview()" CommandArgument='<%# Eval("ID") + "," + Eval("ComplianceID") %>'><img src="../Images/package_icon.png" alt="View Form"/></asp:LinkButton>
                                                                    </ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:PostBackTrigger ControlID="lbkDownload" />
                                                                        <asp:AsyncPostBackTrigger ControlID="lbkDelete" />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>
                                                            </ItemTemplate>
                                                            <HeaderTemplate>
                                                            </HeaderTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <FooterStyle BackColor="#CCCC99" />
                                                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                                                    <PagerSettings Position="Top" />
                                                    <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                                                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                                    <AlternatingRowStyle BackColor="#E6EFF7" />
                                                    <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                                                    <EmptyDataTemplate>
                                                        No Records Found.
                                                    </EmptyDataTemplate>
                                                </asp:GridView>
                                            </asp:Panel>
                                        </div>
                                        <div style="margin-bottom: 7px; margin-left: 210px" id="Div5" runat="server">
                                            <asp:Label ID="Label2" runat="server"></asp:Label>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>

                                <div style="margin-bottom: 7px; margin-left: 203px; margin-top: 10px;">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                    <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" OnClientClick="if (!ValidateFile()) return false;" CssClass="button"
                                        ValidationGroup="ComplianceValidationGroup" />
                                    <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="button" OnClientClick="$('#divComplianceDetailsDialog').dialog('close');" />
                                </div>
                        </ContentTemplate>

                    </asp:UpdatePanel>
                    <div style="margin-bottom: 7px; margin-left: 10px; margin-top: 30px;">
                        <p style="color: red;"><strong>Note:</strong> (*) Fields Are Compulsary</p>
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnSave" />
            </Triggers>
        </asp:UpdatePanel>
    </div>

    <div id="divComplianceStatusDialog">
        <asp:UpdatePanel ID="upComplianceStatusDetails" runat="server" UpdateMode="Conditional" OnLoad="upComplianceDetailsStatus_Load">
            <ContentTemplate>
                <div style="margin: 5px 5px 80px;">
                    <div style="margin-bottom: 4px">
                        <asp:ValidationSummary runat="server" CssClass="vdsummary"
                            ValidationGroup="ComplianceValidationGroup1" />
                        <asp:CustomValidator ID="CustomValidator2" runat="server" EnableClientScript="False"
                            ValidationGroup="ComplianceValidationGroup1" Display="None" />
                        <asp:Label runat="server" ID="Label1" ForeColor="Red"></asp:Label>
                    </div>
                    <div style="margin-bottom: 7px; position: relative; display: inline-block;">
                        <label style="width: 240px; display: block; float: left; font-size: 13px; color: #333;">
                            Act Name</label>
                        <asp:DropDownList runat="server" ID="ddlStatusAct" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                            CssClass="txtbox" Enabled="false" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 240px; display: block; float: left; font-size: 13px; color: #333;">
                            Section(s) / Rule(s)</label>
                        <asp:TextBox runat="server" ID="tbxSectionsStatus" Style="height: 30px; width: 390px;" Enabled="false" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 240px; display: block; float: left; font-size: 13px; color: #333;">
                            Short Description</label>
                        <asp:TextBox runat="server" ID="txtShortDescriptionStatus" TextMode="MultiLine" MaxLength="100" Style="height: 30px; width: 390px;" Enabled="false" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 240px; display: block; float: left; font-size: 13px; color: #333;">
                            Detailed Description</label>
                        <asp:TextBox runat="server" ID="tbxDescriptionStatus" TextMode="MultiLine" Style="height: 50px; width: 390px;" Enabled="false" />
                    </div>
                    <div style="margin-bottom: 7px" id="div3" runat="server">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                            *</label>
                        <label style="width: 190px; display: block; float: left; font-size: 13px; color: #333;">
                            Deactivate Date</label>
                        <asp:TextBox runat="server" CssClass="StartDate" ID="txtDeactivateDate" Style="height: 30px; width: 150px;" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator21" ErrorMessage="Please enter deactivate Date."
                            ControlToValidate="txtDeactivateDate" runat="server" ValidationGroup="ComplianceValidationGroup1"
                            Display="None" />
                    </div>
                    <div style="margin-bottom: 7px" id="Div4" runat="server">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 190px; display: block; float: left; font-size: 13px; color: #333;">
                            Document Upload</label>
                        <asp:Label runat="server" ID="lblDeactivate" CssClass="txtbox" />
                        <asp:FileUpload runat="server" ID="FileUploadDeactivateDoc" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                            *</label>
                        <label style="width: 190px; display: block; float: left; font-size: 13px; color: #333;">
                            Description</label>
                        <asp:TextBox runat="server" ID="txtDeactivateDesc" TextMode="MultiLine" Style="height: 50px; width: 390px;" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator16" ErrorMessage="Deactivate Description can not be empty."
                            ControlToValidate="txtDeactivateDesc" runat="server" ValidationGroup="ComplianceValidationGroup1"
                            Display="None" />
                    </div>
                    <div style="margin-bottom: 7px; margin-left: 203px; margin-top: 10px;">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <asp:Button Text="Save" runat="server" ID="btnSaveDeactivate" OnClick="btnSaveDeactivate_Click" CssClass="button"
                            ValidationGroup="ComplianceValidationGroup1" />
                        <asp:Button Text="Close" runat="server" ID="Button3" CssClass="button" OnClientClick="$('#divComplianceStatusDialog').dialog('close');" />
                    </div>
                    <div style="margin-bottom: 7px; margin-left: 10px; margin-top: 30px;">
                        <p style="color: red;"><strong>Note:</strong> (*) Fields Are Compulsary</p>
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnSaveDeactivate" />
            </Triggers>
        </asp:UpdatePanel>
    </div>

    <asp:HiddenField ID="saveopo" runat="server" Value="false" />
    <script type="text/javascript">
        $(function () {
            $('#divComplianceDetailsDialog').dialog({
                height: 670,
                width: 900,
                autoOpen: false,
                draggable: true,
                title: "Event Compliance Details",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });

            $('#divComplianceStatusDialog').dialog({
                height: 550,
                width: 800,
                autoOpen: false,
                draggable: true,
                title: "Compliance Details",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });

            $('#divComplianceScheduleDialog').dialog({
                height: 600,
                width: 600,
                autoOpen: false,
                draggable: true,
                title: "Compliance Details",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });
        });

        function initializeCombobox() {
            $("#<%= ddlAct.ClientID %>").combobox();
            $("#<%= ddlComplianceType.ClientID %>").combobox();
            $("#<%= ddlNatureOfCompliance.ClientID %>").combobox();
            $("#<%= ddlRiskType.ClientID %>").combobox();
            $("#<%= ddlNonComplianceType.ClientID %>").combobox();
            $("#<%= ddlPerDayMonth.ClientID %>").combobox();
            $("#<%= ddlStatusAct.ClientID %>").combobox();
            $("#<%= ddlComplianceSubType.ClientID %>").combobox();

           <%--  $("#<%= ddlMinimumYear.ClientID %>").combobox();
            $("#<%= ddlMaximumYear.ClientID %>").combobox();--%>
        }

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }

        function checkAll(cb) {
            var ctrls = document.getElementsByTagName('input');
            for (var i = 0; i < ctrls.length; i++) {
                var cbox = ctrls[i];
                if (cbox.type == "checkbox" && cbox.id.indexOf("chkIndustry") > -1) {
                    cbox.checked = cb.checked;
                }
            }
        }

        function UncheckHeader() {
            var rowCheckBox = $("#RepeaterTable input[id*='chkIndustry']");
            var rowCheckBoxSelected = $("#RepeaterTable input[id*='chkIndustry']:checked");
            var rowCheckBoxHeader = $("#RepeaterTable input[id*='IndustrySelectAll']");
            if (rowCheckBox.length == rowCheckBoxSelected.length) {
                rowCheckBoxHeader[0].checked = true;
            } else {

                rowCheckBoxHeader[0].checked = false;
            }
        }

        function checkAllET(cb) {
            var ctrls = document.getElementsByTagName('input');
            for (var i = 0; i < ctrls.length; i++) {
                var cbox = ctrls[i];
                if (cbox.type == "checkbox" && cbox.id.indexOf("chkEntityType") > -1) {
                    cbox.checked = cb.checked;
                }
            }
        }

        function UncheckHeaderET() {
            var rowCheckBox = $("#RepeaterTable input[id*='chkEntityType']");
            var rowCheckBoxSelected = $("#RepeaterTable input[id*='chkEntityType']:checked");
            var rowCheckBoxHeader = $("#RepeaterTable input[id*='EntityTypeSelectAll']");
            if (rowCheckBox.length == rowCheckBoxSelected.length) {
                rowCheckBoxHeader[0].checked = true;
            } else {

                rowCheckBoxHeader[0].checked = false;
            }
        }
        function checkAllIsForSecretarial(cb) {
            var ctrls = document.getElementsByTagName('input');
            for (var i = 0; i < ctrls.length; i++) {
                var cbox = ctrls[i];
                if (cbox.type == "checkbox" && cbox.id.indexOf("chkIsForSecretarial") > -1) {
                    cbox.checked = cb.checked;
                }
            }
        }

        function UncheckHeaderIsForSecretarial() {
            var rowCheckBox = $("#RepeaterTable input[id*='chkIsForSecretarial']");
            var rowCheckBoxSelected = $("#RepeaterTable input[id*='chkIsForSecretarial']:checked");
            var rowCheckBoxHeader = $("#RepeaterTable input[id*='IsForSecretarialSelectAll']");
            if (rowCheckBox.length == rowCheckBoxSelected.length) {
                rowCheckBoxHeader[0].checked = true;
            } else {

                rowCheckBoxHeader[0].checked = false;
            }
        }

        function initializeDatePicker(date) {
        }
    </script>
    <script type="text/javascript">
        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);
        window.onunload = function () { null };
    </script>
</asp:Content>
