﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="EventComplianceMapping.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Event.EventComplianceMapping" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">

    <div>
        <asp:UpdateProgress ID="updateProgress" runat="server">
            <ProgressTemplate>
                <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                    <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                        AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    

        <asp:UpdatePanel ID="upEvent" runat="server" UpdateMode="Conditional" OnLoad="upEvent_Load">
            <ContentTemplate>
                <div style="margin-bottom: 4px">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="vdsummary"
                        ValidationGroup="EventValidationGroup" />
                    <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                        ValidationGroup="EventValidationGroup" Display="None" />
                </div>
                <table runat="server" width="70%">
                    <tr>
                        <td class="td1">
                            <div style="margin: 10px 20px 10px 294px">
                                <div>
                                    <asp:RadioButtonList runat="server" ID="rblRelationship" RepeatDirection="Horizontal" RepeatLayout="Flow"
                                        OnSelectedIndexChanged="rblRelationship_SelectedIndexChanged" Font-Bold="true" AutoPostBack="true">
                                      <%--  <asp:ListItem Text="Public" Value="0" Selected="True" />
                                        <asp:ListItem Text="Private" Value="1" />
                                        <asp:ListItem Text="Listed" Value="2" />
                                        <asp:ListItem Text="Non-Secretarial" Value="3" />--%>

                                            <asp:ListItem Text="Public" Value="1" Selected="True" />
                                            <asp:ListItem Text="Private" Value="2" />
                                            <asp:ListItem Text="Listed" Value="3" />
                                            <asp:ListItem Text="Non-Secretarial" Value="4" />
                                            <asp:ListItem Text="LLP" Value="5" />
                                            <asp:ListItem Text="Public Section 8" Value="12" />
                                            <asp:ListItem Text="Private Section 8" Value="13" />
                                    </asp:RadioButtonList>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
                <asp:Panel ID="Panel1" Width="100%" Height="100px" runat="server">
                    <div style="margin-bottom: 7px; float: right; margin-right: 750px; margin-top: 10px;">
                        <asp:DropDownList runat="server" ID="ddlParentEvent" AutoPostBack="true" OnSelectedIndexChanged="ddlParentEvent_SelectedIndexChanged" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                            CssClass="txtbox" />
                        <asp:CompareValidator ID="CompareValidator2" ErrorMessage="Please select Event Name."
                            ControlToValidate="ddlParentEvent" runat="server" ValueToCompare="-1" Operator="NotEqual"
                            ValidationGroup="EventValidationGroup" Display="None" />
                    </div>
                    <div style="margin-bottom: 7px; float: right; margin-right: 750px; margin-top: 10px;">
                        <asp:DropDownList runat="server" ID="ddlEvent" AutoPostBack="true" OnSelectedIndexChanged="ddlEvent_SelectedIndexChanged" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                            CssClass="txtbox" />
                        <asp:CompareValidator ID="CompareValidator1" ErrorMessage="Please select sub Event Name."
                            ControlToValidate="ddlEvent" runat="server" ValueToCompare="-1" Operator="NotEqual"
                            ValidationGroup="EventValidationGroup" Display="None" />
                    </div>
                    <div style="margin-bottom: 7px; float: right; margin-right: 750px; margin-top: 10px;">
                        <asp:DropDownList runat="server" ID="ddlState" AutoPostBack="true" OnSelectedIndexChanged="ddlState_SelectedIndexChanged" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                            CssClass="txtbox" />
                    </div>
                    <div style="margin-bottom: 7px; float: right; margin-right: 719px; margin-top: 10px;">
                        <asp:TextBox runat="server" ID="txtactList" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                            CssClass="txtbox" />
                        <div style="margin-left: 0px; position: absolute; z-index: 50; overflow-y: auto; background: white; border: 1px solid gray; width: 950px; height: 200px;" id="dvActList">
                            <asp:Repeater ID="rptActList" runat="server">
                                <HeaderTemplate>
                                    <table class="detailstable FadeOutOnEdit" id="RepeaterTable">
                                        <tr>
                                            <td style="width: 282px;">
                                                <asp:Button runat="server" ID="btnRefresh" Text="Ok" Style="float: left" OnClick="btnRefresh_Click" /></td>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td style="width: 20px;">
                                            <asp:CheckBox ID="chkAct" runat="server" /></td>
                                        <td style="width: 200px;">
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 900px; padding-bottom: 5px;">
                                                <asp:Label ID="lblActID" runat="server" Visible="false" Text='<%# Eval("ID")%>' ToolTip='<%# Eval("ID") %>'></asp:Label>
                                                <asp:Label ID="lblActName" runat="server" Text='<%# Eval("Name")%>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                            </div>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                    </div>

                    <div style="margin-bottom: 7px; float: right; margin-right: 720px; margin-top: 10px;">
                        <asp:TextBox runat="server" ID="tbxFilter" placeholder="Filter" AutoPostBack="true" OnTextChanged="tbxFilter_TextChanged" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                            CssClass="txtbox" />
                    </div>

                </asp:Panel>
                <table style="margin-left: 30px;" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <asp:Label ID="Label1" Text="Mapping Compliance List" runat="server" ForeColor="Black"></asp:Label>
                            <asp:ListBox ID="lstLeft" runat="server" SelectionMode="Multiple" Width="580" Height="290px"></asp:ListBox>
                        </td>
                        <td>
                            <asp:Button ID="btnRightMove" runat="server" Text=">>" Width="45px" Style="margin-bottom: 10px;" OnClick="btnRightMove_Click" />
                            <asp:Button ID="btnLeftMove" runat="server" Text="<<" Width="45px" OnClick="btnLeftMove_Click" />
                        </td>
                        <td>
                            <asp:Label ID="Label2" Text="Mapped Compliance List" runat="server" ForeColor="Black"></asp:Label>
                            <asp:ListBox ID="lstRight" runat="server" SelectionMode="Multiple" Width="580" Height="290px"></asp:ListBox>
                        </td>
                    </tr>

                </table>
                <div style="margin-bottom: 7px; margin-left: 613px; margin-top: 10px">
                    <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="button"
                        ValidationGroup="EventValidationGroup" />
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <script type="text/javascript">
        //$(function () {
        //    $("#left").bind("click", function () {
        //        var options = $("[id*=lstRight] option:selected");
        //        for (var i = 0; i < options.length; i++) {
        //            var opt = $(options[i]).clone();
        //            $(options[i]).remove();
        //            $("[id*=lstLeft]").append(opt);
        //        }
        //    });
        //    $("#right").bind("click", function () {
        //        var options = $("[id*=lstLeft] option:selected");
        //        for (var i = 0; i < options.length; i++) {
        //            var opt = $(options[i]).clone();
        //            $(options[i]).remove();
        //            $("[id*=lstRight]").append(opt);
        //        }
        //    });
        //});

        //$(function () {
        //    $("[id*=btnSubmit]").bind("click", function () {
        //        $("[id*=lstLeft] option").attr("selected", "selected");
        //        $("[id*=lstRight] option").attr("selected", "selected");
        //    });
        //});

        $(function () {

            initializeCombobox();

        });
        function initializeCombobox() {
            $("#<%= ddlEvent.ClientID %>").combobox();
            $("#<%= ddlParentEvent.ClientID %>").combobox();
            $("#<%= ddlState.ClientID %>").combobox();
        }
        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }

        //function checkAllComplince(cb) {
        //    var ctrls = document.getElementsByTagName('input');
        //    for (var i = 0; i < ctrls.length; i++) {
        //        var cbox = ctrls[i];
        //        if (cbox.type == "checkbox" && cbox.id.indexOf("chkCompliance") > -1) {
        //            cbox.checked = cb.checked;
        //        }
        //    }
        //}

        //function checkAll(cb) {
        //    var ctrls = document.getElementsByTagName('input');
        //    for (var i = 0; i < ctrls.length; i++) {
        //        var cbox = ctrls[i];
        //        if (cbox.type == "checkbox" && cbox.id.indexOf("chkAct") > -1) {
        //            cbox.checked = cb.checked;
        //        }
        //    }
        //}

        //function UncheckHeader() {
        //    var rowCheckBox = $("#RepeaterTable input[id*='chkAct']");
        //    var rowCheckBoxSelected = $("#RepeaterTable input[id*='chkAct']:checked");
        //    //var rowCheckBoxHeader = $("#RepeaterTable input[id*='actSelectAll']");
        //    if (rowCheckBox.length == rowCheckBoxSelected.length) {
        //        rowCheckBoxHeader[0].checked = true;
        //    } else {

        //        rowCheckBoxHeader[0].checked = false;
        //    }
        //}
    </script>
</asp:Content>
