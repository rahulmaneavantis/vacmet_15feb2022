﻿<%@ Page Title="Event List" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="EventList.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Event.EventList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upEventList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <table width="100%">
                <tr>
                    <td align="right">
                    </td>
                     <td>
                        <asp:DropDownList runat="server" ID="ddlFilterComplianceType" Style="padding: 0px; margin: 0px; height: 22px; width: 220px;"
                            CssClass="txtbox"  AutoPostBack="true" OnSelectedIndexChanged="ddlFilterComplianceType_SelectedIndexChanged">
                                <asp:ListItem Value="-1" Selected="True"> < Select Type > </asp:ListItem>
                                <asp:ListItem Value="1">Secretarial </asp:ListItem>
                                <asp:ListItem Value="2">Non-Secretarial</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                      <td>
                         <%-- <div id="subComplianceType" visible="false">--%>
                            <asp:DropDownList runat="server" Visible="false" ID="ddlsecretrialsubtype" Style="padding: 0px; margin: 0px; height: 22px; width: 220px;"
                            CssClass="txtbox"  AutoPostBack="true" OnSelectedIndexChanged="ddlsecretrialsubtype_SelectedIndexChanged">
                                <asp:ListItem Value="-1" Selected="True"> < Select Type > </asp:ListItem>
                                <asp:ListItem Value="1">Public </asp:ListItem>
                                <asp:ListItem Value="2">Private</asp:ListItem>
                                <asp:ListItem Value="3">Listed</asp:ListItem>
                                <asp:ListItem Value="5">LLP</asp:ListItem>
                                <asp:ListItem Value="12">Public Section 8</asp:ListItem>
                                <asp:ListItem Value="13">Private Section 8</asp:ListItem>
                        </asp:DropDownList>
                          <%--</div>--%>
                       
                    </td>
                    <td>
                        <asp:RadioButton ID="rdoParentEvent" Text="Parent Event" GroupName="EventType" AutoPostBack="true" Width="120px" runat="server"
                            OnCheckedChanged="rdoParentEvent_CheckedChanged" />
                    </td>
                    <td>
                        <asp:RadioButton ID="rdoSubEvent" Text="Sub Event" AutoPostBack="true" GroupName="EventType" Width="100px" runat="server"
                            OnCheckedChanged="rdoSubEvent_CheckedChanged" />
                    </td>
                    <td align="right" style="width: 25%">Filter :
                        <asp:TextBox runat="server" ID="tbxFilter" Width="250px" MaxLength="50" AutoPostBack="true"
                            OnTextChanged="tbxFilter_TextChanged" />
                    </td>
                    <td class="newlink" align="right" style="width: 10%">
                        <asp:LinkButton Text="Add New" runat="server" ID="btnAddEvent" OnClick="btnAddEvent_Click" Visible="false" />
                    </td>
                     <td class="newlink" align="right" style="width: 10%; padding-right:0px !important">
                    <asp:LinkButton ID="lnkMapping" runat="server" Text='Compliance Mapping' Visible="false" PostBackUrl="EventComplianceMapping.aspx"></asp:LinkButton>
                    </td>
                    <td style ="padding-left:10px">
                        <asp:LinkButton ID="lnkCacheRefresh" runat="server"  Visible="false" Text='Last updated within the last hour' 
                            OnClick="lnkCacheRefresh_Click" ></asp:LinkButton>
                    </td>
                </tr>
            </table>
            <asp:Panel ID="Panel1" Width="100%" Height="450px" ScrollBars="Vertical" runat="server">
                <asp:GridView runat="server" ID="grdEventList" AutoGenerateColumns="false" GridLines="Vertical" OnRowDataBound="grdEventList_RowDataBound"
                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true" OnRowCreated="grdEventList_RowCreated"
                    CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="100" Width="100%" OnSorting="grdEventList_Sorting"
                    Font-Size="12px" OnRowCommand="grdEventList_RowCommand" OnPageIndexChanging="grdEventList_PageIndexChanging">
                    <Columns>
                        <asp:TemplateField HeaderText="ID" ItemStyle-Height="25px" HeaderStyle-Height="20px">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 50px">
                                    <asp:Label ID="Label222" runat="server" Text='<%# Eval("ID") %>' ToolTip='<%# Eval("ID") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Event Name" ItemStyle-Height="25px" HeaderStyle-Height="20px" SortExpression="Name">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 500px">
                                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Parent Event Name" ItemStyle-Height="25px" HeaderStyle-Height="20px" SortExpression="Name">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 400px">
                                    <asp:Label ID="lblParentEvent" runat="server" Text='<%# Eval("ParentEventName") %>' ToolTip='<%# Eval("ParentEventName") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%--<asp:TemplateField HeaderText="Description" ItemStyle-Height="25px" HeaderStyle-Height="20px" SortExpression="Description">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                    <asp:Label ID="Label2" runat="server" Text='<%# Eval("Description") %>' ToolTip='<%# Eval("Description") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Type" ItemStyle-Width="100px">
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%# EventType(Convert.ToInt32(Eval("Type"))) %>' CommandArgument='<%# Eval("Type") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Width="60px" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:LinkButton ID="lbtEdit" runat="server" CommandName="EDIT_ACT" CommandArgument='<%# Eval("ID") %>'><img src="../Images/edit_icon.png" alt="Edit Event" title="Edit Event" /></asp:LinkButton>
                                <asp:LinkButton ID="lbtDelete" runat="server" CommandName="DELETE_ACT" CommandArgument='<%# Eval("ID") %>'
                                    OnClientClick="return confirm('Are you certain you want to delete this Event?');"><img src="../Images/delete_icon.png" alt="Delete Event" title="Delete Event" /></asp:LinkButton>
                            </ItemTemplate>
                            <HeaderTemplate>
                            </HeaderTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle BackColor="#CCCC99" />
                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                    <PagerSettings Position="Top" />
                    <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                    <AlternatingRowStyle BackColor="#E6EFF7" />
                    <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                    <EmptyDataTemplate>
                        No Records Found.
                    </EmptyDataTemplate>
                </asp:GridView>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div id="divEventDialog">
        <asp:UpdatePanel ID="upEvent" runat="server" UpdateMode="Conditional" OnLoad="upEvent_Load">
            <ContentTemplate>
                <div style="margin: 5px">
                    <div style="margin-bottom: 4px">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="vdsummary"
                            ValidationGroup="EventValidationGroup" />
                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                            ValidationGroup="EventValidationGroup" Display="None" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 180px; display: block; float: left; font-size: 13px; color: #333;">
                            Name</label>
                        <asp:TextBox runat="server" ID="tbxName" Style="width: 390px;" MaxLength="500" ToolTip="Name" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Name can not be empty." ControlToValidate="tbxName"
                            runat="server" ValidationGroup="EventValidationGroup" Display="None" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 180px; display: block; float: left; font-size: 13px; color: #333;">
                            Description</label>
                        <asp:TextBox runat="server" ID="txtDescription" Style="height: 30px; width: 390px;" MaxLength="100" ToolTip="" TextMode="MultiLine" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 180px; display: block; float: left; font-size: 13px; color: #333;">
                            Is Parent Event
                        </label>
                        <asp:CheckBox Checked="true" runat="server" ID="chkVisible" AutoPostBack="true" OnCheckedChanged="chkVisible_CheckedChanged" CssClass="txtbox" />
                    </div>

                    <div style="margin-bottom: 7px" runat="server" id="diveventtype">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label id="lblEventType" style="width: 180px; display: block; float: left; font-size: 13px; color: #333;">
                            Event Type</label>
                         <asp:DropDownList runat="server" ID="ddlEventType"  AutoPostBack="true" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                            CssClass="txtbox" >
                            <asp:ListItem Text="< Select >" Value="-1" />
                            <asp:ListItem Value="3">State </asp:ListItem>
                            <asp:ListItem Value="2">Central & State</asp:ListItem>
                            
                        </asp:DropDownList>
                            <asp:CompareValidator ErrorMessage="Please select Event Type." ControlToValidate="ddlEventType"
                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="EventValidationGroup"
                            Display="None" />
                    </div>

                     <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label id="lblEventClassification" style="width: 180px; display: block; float: left; font-size: 13px; color: #333;">
                            Event Classification</label>
                        <asp:DropDownList runat="server" ID="ddlEventClassification" AutoPostBack="true" 
                            OnSelectedIndexChanged="ddlEventClassification_SelectedIndexChanged" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                            CssClass="txtbox" />
                            <asp:CompareValidator ErrorMessage="Please select Event Classification." ControlToValidate="ddlEventClassification"
                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="EventValidationGroup"
                            Display="None" />
                    </div>

                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label id="lblType" style="width: 180px; display: block; float: left; font-size: 13px; color: #333;">
                            Type</label>
                        <asp:DropDownList runat="server" ID="ddlCompanyType" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                            CssClass="txtbox" />
                        <asp:CompareValidator ErrorMessage="Please select Type." ControlToValidate="ddlCompanyType"
                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="EventValidationGroup"
                            Display="None" />
                    </div>

                     <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label id="lblEventSubClassification" style="width: 180px; display: block; float: left; font-size: 13px; color: #333;">
                            Event Sub Classification</label>
                        <asp:DropDownList runat="server" ID="ddlEventSubClassification" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                            CssClass="txtbox" />
                    </div>


                      <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 120px; display: block; float: left; font-size: 13px; color: #333;">
                             Location wise classification
                        </label>
                        <asp:TextBox runat="server" ID="txtEntityLevel" Style="padding: 0px; margin: 0px;margin-left: 60px; height: 22px; width: 390px;"
                            CssClass="txtbox" />
                        <div style="margin-left: 240px; position: absolute; z-index: 50; overflow-y: auto; background: white; border: 1px solid gray; height: 240px;" id="dvEntityLevel">

                            <asp:Repeater ID="rptEventType" runat="server">
                                <HeaderTemplate>
                                    <table class="detailstable FadeOutOnEdit" id="RepeaterTable">
                                        <tr>
                                            <td style="width: 100px;">
                                                <asp:CheckBox ID="EventTypeSelectAll" Text="Select All" runat="server" onclick="checkAllET(this)" /></td>
                                          <%--  <td style="width: 282px;">
                                                <asp:Button runat="server" ID="btnRepeaterEntityType" Text="Ok" Style="float: left" OnClick="btnRefresh_Click" /></td>
                                          --%> 
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td style="width: 20px;">
                                            <asp:CheckBox ID="chkEventType" runat="server" onclick="UncheckHeaderET();" /></td>
                                        <td style="width: 240px;">
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 250px; padding-bottom: 5px;">
                                                <asp:Label ID="lblEntityTypeID" runat="server" Visible="false" Text='<%# Eval("ID")%>' ToolTip='<%# Eval("ID") %>'></asp:Label>
                                                <asp:Label ID="lblEntityTypeName" runat="server" Text='<%# Eval("Name")%>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                            </div>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                    </div>

                    <%--<div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 180px; display: block; float: left; font-size: 13px; color: #333;">
                            Act Filter
                        </label>
                        <asp:TextBox runat="server" ID="txtactList" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                            CssClass="txtbox" />
                        <div style="margin-left: 180px; position: absolute; z-index: 50; overflow-y: auto; background: white; border: 1px solid gray; height: 200px;" id="dvActList">
                            <asp:Repeater ID="rptActList" runat="server">
                                <HeaderTemplate>
                                    <table class="detailstable FadeOutOnEdit" id="RepeaterTable">
                                        <tr>
                                            <td style="width: 100px;">
                                                <asp:CheckBox ID="actSelectAll" Text="Select All" runat="server" onclick="checkAll(this)" /></td>
                                            <td style="width: 282px;">
                                                <asp:Button runat="server" ID="btnRefresh" Text="Ok" Style="float: left" OnClick="btnRefresh_Click" /></td>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td style="width: 20px;">
                                            <asp:CheckBox ID="chkAct" runat="server" onclick="UncheckHeader();" /></td>
                                        <td style="width: 200px;">
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 250px; padding-bottom: 5px;">
                                                <asp:Label ID="lblActID" runat="server" Visible="false" Text='<%# Eval("ID")%>' ToolTip='<%# Eval("ID") %>'></asp:Label>
                                                <asp:Label ID="lblActName" runat="server" Text='<%# Eval("Name")%>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                            </div>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                    </div>--%>
                    <%--             <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 180px; display: block; float: left; font-size: 13px; color: #333;">
                            Compliance
                        </label>
                        <asp:TextBox runat="server" ID="txtCompliance" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                            CssClass="txtbox" />
                        <div style="margin-left: 160px; position: absolute; z-index: 50; overflow-y: auto; background: white; width: 570px; border: 1px solid gray; height: 200px;" id="dvCompliance">
                            <asp:Repeater ID="rptCompliance" runat="server">
                                <HeaderTemplate>
                                    <table class="detailstable FadeOutOnEdit" id="RepeaterTable">
                                        <tr>
                                            <td style="width: 100px;">
                                                <asp:CheckBox ID="ComplianceSelectAll" Text="Select All" runat="server" onclick="checkAllComplince(this)" /></td>
                                            <td style="width: 282px;">
                                                <asp:Button runat="server" ID="btnComplianceRepeater" Text="Ok" Style="float: left" OnClick="btnComplianceRepeater_Click" /></td>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td style="width: 20px;">
                                            <asp:CheckBox ID="chkCompliance" runat="server" onclick="UncheckComplinceHeader();" /></td>
                                        <td style="width: 470px;">
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 460px; padding-bottom: 5px;">
                                                <asp:Label ID="lblComplianceID" runat="server" Visible="false" Text='<%# Eval("ID")%>' ToolTip='<%# Eval("ID") %>'></asp:Label>
                                                <asp:Label ID="lblComplianceName" runat="server" Text='<%# Eval("ShortDescription")%>' ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                                            </div>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                    </div>--%>
                    <div style="margin-bottom: 7px; margin-left: 152px; margin-top: 10px">
                        <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="button"
                            ValidationGroup="EventValidationGroup" />
                        <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="button" OnClientClick="$('#divEventDialog').dialog('close');" />
                    </div>
                </div>
                <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 10px;">

                    <p style="color: red;"><strong>Note:</strong> (*) Fields Are Compulsary</p>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>


   <div id="divParentEventDelete">
        <asp:UpdatePanel ID="updivParentEventDelete" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                 <div style="margin-bottom: 4px">
                        <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="vdsummary"
                            ValidationGroup="EventValidationGroup1" />
                        <asp:CustomValidator ID="CustomValidator1" runat="server" EnableClientScript="False"
                            ValidationGroup="EventValidationGroup1" Display="None" />
                    </div>
                <div style="margin: 5px">
                    This event has been mapped below clients.Please deactivate event first.
                </div>

                 <div style="margin: 5px">
                     <asp:Label ID="lblEventName" runat="server"></asp:Label>
                </div>
                <div style="margin: 5px">
                    <asp:Panel ID="Panel4" Height="400px" ScrollBars="Auto" runat="server">
                        <asp:GridView runat="server" ID="grdActiveEvent" AutoGenerateColumns="false" GridLines="Vertical"
                            BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true"
                            CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="100" Width="100%"
                            Font-Size="12px">
                            <Columns>
                                <asp:TemplateField HeaderText="Customer Name" ItemStyle-Height="15px">
                                    <ItemTemplate>
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                            <asp:Label ID="lblCustomerName" runat="server" Text='<%# Eval("CustomerName") %>' ToolTip='<%# Eval("CustomerName") %>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Location Name" ItemStyle-Height="15px">
                                    <ItemTemplate>
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                            <asp:Label ID="lblLocationName" runat="server" Text='<%# Eval("LocationName") %>' ToolTip='<%# Eval("LocationName") %>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Event Nature" ItemStyle-Height="15px">
                                    <ItemTemplate>
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                            <asp:Label ID="lblEventNature" runat="server" Text='<%# Eval("EventNature") %>' ToolTip='<%# Eval("EventNature") %>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <FooterStyle BackColor="#CCCC99" />
                            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                            <PagerSettings Position="Top" />
                            <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                            <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                            <AlternatingRowStyle BackColor="#E6EFF7" />
                            <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                            <EmptyDataTemplate>
                                No Records Found.
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </asp:Panel>
                    <div style="margin-bottom: 7px; float: right; margin-right: 310px; margin-top: 10px; clear: both">
                        <asp:Button Text="Delete Event" runat="server" Width="166px" ID="btnDeleteEvent" CssClass="button" ValidationGroup="EventValidationGroup1"  OnClientClick="return Confirm();" OnClick="btnDeleteEvent_Click" />
                        <asp:Button Text="Close" runat="server" ID="btnbtnDeleteEventClose" CssClass="button" OnClick="btnbtnDeleteEventClose_Click" />
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    
   <div id="divSubEventDelete">
        <asp:UpdatePanel ID="updivSubEventDelete" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                  <div style="margin-bottom: 4px">
                        <asp:ValidationSummary ID="ValidationSummary3" runat="server" CssClass="vdsummary"
                            ValidationGroup="EventValidationGroup2" />
                        <asp:CustomValidator ID="CustomValidator2" runat="server" EnableClientScript="False"
                            ValidationGroup="EventValidationGroup2" Display="None" />
                    </div>
                <div style="margin: 5px">
                    This event has been mapped to below parent event.Please remove event mapping.
                </div>
                 <div style="margin: 5px">
                     <asp:Label ID="lblSubEventName" runat="server"></asp:Label>
                </div>
                <div style="margin: 5px">
                    <asp:Panel ID="Panel2" Height="400px" ScrollBars="Auto" runat="server">
                        <asp:GridView runat="server" ID="grdParentEvent" AutoGenerateColumns="false" GridLines="Vertical"
                            BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true"
                            CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="100" Width="100%"
                            Font-Size="12px">
                            <Columns>
                                <asp:TemplateField HeaderText="Event Name" ItemStyle-Height="15px">
                                    <ItemTemplate>
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px;">
                                            <asp:Label ID="lblEventName" runat="server" Text='<%# Eval("EvenetName") %>' ToolTip='<%# Eval("EvenetName") %>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <FooterStyle BackColor="#CCCC99" />
                            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                            <PagerSettings Position="Top" />
                            <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                            <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                            <AlternatingRowStyle BackColor="#E6EFF7" />
                            <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                            <EmptyDataTemplate>
                                No Records Found.
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </asp:Panel>
                    <div style="margin-bottom: 7px; float: right; margin-right: 310px; margin-top: 10px; clear: both">
                        <asp:Button Text="Delete Event" runat="server" Width="166px" ID="btnDeleteSubEvent"  ValidationGroup="EventValidationGroup2" CssClass="button" OnClick="btnDeleteSubEvent_Click" />
                        <asp:Button Text="Close" runat="server" ID="btnCloseSubEvent" CssClass="button" OnClick="btnCloseSubEvent_Click" />
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <script type="text/javascript">
        $(function () {
            $('#divEventDialog').dialog({
                height: 460,
                width: 800,
                autoOpen: false,
                draggable: true,
                title: "Event",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });
        });

        $(function () {
            $('#divParentEventDelete').dialog({
                height: 460,
                width: 800,
                autoOpen: false,
                draggable: true,
                title: "Activated Event List",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });
        });


        $(function () {
            $('#divSubEventDelete').dialog({
                height: 460,
                width: 800,
                autoOpen: false,
                draggable: true,
                title: "Parent Event mapping List",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });
        });
        
        function initializeCombobox() {
            $("#<%= ddlCompanyType.ClientID %>").combobox();
            $("#<%= ddlEventClassification.ClientID %>").combobox();
            $("#<%= ddlEventSubClassification.ClientID %>").combobox();
            $("#<%= ddlEventType.ClientID %>").combobox();
        }

        function initializeRadioButtonsList(controlID) {
            $(controlID).buttonset();
        }

        function UncheckComplinceHeader() {
            var rowCheckBox = $("#RepeaterTable input[id*='chkCompliance']");
            var rowCheckBoxSelected = $("#RepeaterTable input[id*='chkCompliance']:checked");
            var rowCheckBoxHeader = $("#RepeaterTable input[id*='ComplianceSelectAll']");
            if (rowCheckBox.length == rowCheckBoxSelected.length) {
                rowCheckBoxHeader[0].checked = true;
            } else {

                rowCheckBoxHeader[0].checked = false;
            }
        }

       function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }

        function checkAllComplince(cb) {
            var ctrls = document.getElementsByTagName('input');
            for (var i = 0; i < ctrls.length; i++) {
                var cbox = ctrls[i];
                if (cbox.type == "checkbox" && cbox.id.indexOf("chkCompliance") > -1) {
                    cbox.checked = cb.checked;
                }
            }
        }

        function checkAll(cb) {
            var ctrls = document.getElementsByTagName('input');
            for (var i = 0; i < ctrls.length; i++) {
                var cbox = ctrls[i];
                if (cbox.type == "checkbox" && cbox.id.indexOf("chkAct") > -1) {
                    cbox.checked = cb.checked;
                }
            }
        }

        function UncheckHeader() {
            var rowCheckBox = $("#RepeaterTable input[id*='chkAct']");
            var rowCheckBoxSelected = $("#RepeaterTable input[id*='chkAct']:checked");
            var rowCheckBoxHeader = $("#RepeaterTable input[id*='actSelectAll']");
            if (rowCheckBox.length == rowCheckBoxSelected.length) {
                rowCheckBoxHeader[0].checked = true;
            } else {

                rowCheckBoxHeader[0].checked = false;
            }
        }
        //function checkAllCompanyType(cb) {
        //    var ctrls = document.getElementsByTagName('input');
        //    for (var i = 0; i < ctrls.length; i++) {
        //        var cbox = ctrls[i];
        //        if (cbox.type == "checkbox" && cbox.id.indexOf("chkCompanyType") > -1) {
        //            cbox.checked = cb.checked;
        //        }
        //    }
        //}



        function checkAllET(cb) {
            var ctrls = document.getElementsByTagName('input');
            for (var i = 0; i < ctrls.length; i++) {
                var cbox = ctrls[i];
                if (cbox.type == "checkbox" && cbox.id.indexOf("chkEventType") > -1) {
                    cbox.checked = cb.checked;
                }
            }
        }

        function UncheckHeaderET() {
            var rowCheckBox = $("#RepeaterTable input[id*='chkEventType']");
            var rowCheckBoxSelected = $("#RepeaterTable input[id*='chkEventType']:checked");
            var rowCheckBoxHeader = $("#RepeaterTable input[id*='EventTypeSelectAll']");
            if (rowCheckBox.length == rowCheckBoxSelected.length) {
                rowCheckBoxHeader[0].checked = true;
            } else {

                rowCheckBoxHeader[0].checked = false;
            }
        }

        function Confirm() {
           
                var confirm_value = document.createElement("INPUT");
                confirm_value.type = "hidden";
                confirm_value.name = "confirm_value";
               
                confirm_value.value = "";
                confirm_value
                if (confirm("Are you sure to delete event? continue to click OK!!")) {
                    return true;
                } else {
                    return false;
                }
            
        };
    </script>
</asp:Content>
