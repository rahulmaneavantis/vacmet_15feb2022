﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="EventDetailsCA.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Event.EventDetailsCA" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>

    </asp:UpdateProgress>
    <div style="margin-top: 40px">
        <div style="margin-bottom: 4px">
            <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
        </div>
        <div runat="server" id="divEventDetail">
            <asp:UpdatePanel ID="upEventDetail" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <table>
                        <tr>
                            <td style="width: 95%">
                                <asp:Label ID="lblEventName" Text="Event Name" runat="server" Font-Bold="true" Font-Size="Medium" ForeColor="#996633"></asp:Label>
                            </td>
                            <td style="width: 50px">
                                <asp:Button ID="btnBack" CssClass="button" runat="server" Text="Back" OnClick="btnBack_Click" />
                            </td>
                        </tr>
                    </table>
                   <%-- <asp:Panel ID="Panel5" Width="100%" Height="450px" ScrollBars="Vertical" runat="server">--%>
                     <div style="background-color: steelblue; font:  font-size:13px; height: 30px; width: 100%; margin: 0; padding: 0">
                           <%-- <table cellspacing="0" cellpadding="0" rules="all" border="1" id="tblHeader"
                                style="font-family: Arial; font-size: 10pt; width: 100%; color: white; border-collapse: collapse; height: 15%;">--%>
                             <table cellspacing="0" cellpadding="0" id="tblHeader"
                                style="font-family: Arial; font-size: 10pt; width: 100%; color: white; border-collapse: collapse; height: 30px;">
                                <tr>
                                    <td style="width: 24px; text-align: center">Seq No</td>
                                    <td style="width: 42px; text-align: center">Sub Event</td>
                                    <td style="width: 64px; text-align: center">Act Name</td>
                                    <td style="width: 51px; text-align: center">Section</td>
                                    <td style="width: 37px; text-align: center">Short Description</td>
                                    <td style="width: 135px; text-align: center">Description</td>
                                    <td style="width: 200px; text-align: center">Penalty Description</td>
                                </tr>
                            </table>
                        </div>
                        <div style="height: 430px; width: 100%; overflow: auto;">
                        <asp:GridView runat="server" ID="grdEventDetail" AutoGenerateColumns="false" GridLines="Vertical"
                            BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true" ShowHeader="false"
                            CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="200" Width="100%" 
                            Font-Size="12px" DataKeyNames="ID">
                            <Columns>
                                  <%--<asp:TemplateField HeaderText="Seq No" ItemStyle-Height="20px" HeaderStyle-Height="20px" ItemStyle-Width="25px" ItemStyle-VerticalAlign="Top">
                                    <ItemTemplate>
                                        <asp:Label runat="server" Text='<%# Eval("rowID") %>' ToolTip='<%# Eval("rowID") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Sub Event" ItemStyle-Height="20px" HeaderStyle-Height="20px" ItemStyle-Width="50px" ItemStyle-VerticalAlign="Top">
                                    <ItemTemplate>
                                        <asp:Label runat="server" Text='<%# Eval("IntermediatesubEvent") %>' ToolTip='<%# Eval("IntermediatesubEvent") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Act Name" ItemStyle-Height="20px" HeaderStyle-Height="20px" ItemStyle-Width="50px" ItemStyle-VerticalAlign="Top">
                                    <ItemTemplate>
                                        <asp:Label runat="server" Text='<%# Eval("ActName") %>' ToolTip='<%# Eval("ActName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Section" ItemStyle-Height="20px" HeaderStyle-Height="20px" ItemStyle-Width="50px" ItemStyle-VerticalAlign="Top">
                                    <ItemTemplate>
                                        <asp:Label runat="server" Text='<%# Eval("Sections") %>' ToolTip='<%# Eval("Sections") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Short Description" ItemStyle-Height="20px" HeaderStyle-Height="20px" ItemStyle-Width="50px" ItemStyle-VerticalAlign="Top">
                                    <ItemTemplate>
                                        <asp:Label runat="server" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Description" ItemStyle-HorizontalAlign="Justify" ItemStyle-Height="20px" HeaderStyle-Height="20px" ItemStyle-Width="190px" ItemStyle-VerticalAlign="Top">
                                    <ItemTemplate>
                                        <asp:Label runat="server" Text='<%# Eval("Description") %>' ToolTip='<%# Eval("Description") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Penalty Description" ItemStyle-HorizontalAlign="Justify" ItemStyle-Height="20px" HeaderStyle-Height="20px" ItemStyle-Width="260px" ItemStyle-VerticalAlign="Top">
                                    <ItemTemplate>
                                        <asp:Label runat="server" Text='<%# Eval("PenaltyDescription") %>' ToolTip='<%# Eval("PenaltyDescription") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>--%>
                                   <asp:BoundField ItemStyle-Width="25px" HeaderStyle-Height="20px" ItemStyle-VerticalAlign="Top" DataField="rowID" />
                                    <asp:BoundField ItemStyle-Height="20px" HeaderStyle-Height="20px" ItemStyle-Width="50px" ItemStyle-VerticalAlign="Top" DataField="IntermediatesubEvent" />
                                    <asp:BoundField ItemStyle-Height="20px" HeaderStyle-Height="20px" ItemStyle-Width="50px" ItemStyle-VerticalAlign="Top" DataField="ActName"/>
                                    <asp:BoundField ItemStyle-Height="20px" HeaderStyle-Height="20px" ItemStyle-Width="50px" ItemStyle-VerticalAlign="Top" DataField="Sections" />
                                    <asp:BoundField ItemStyle-Height="20px" HeaderStyle-Height="20px" ItemStyle-Width="50px" ItemStyle-VerticalAlign="Top" DataField="ShortDescription" />
                                    <asp:BoundField ItemStyle-HorizontalAlign="Justify" ItemStyle-Height="20px" HeaderStyle-Height="20px" ItemStyle-Width="190px" ItemStyle-VerticalAlign="Top" DataField="Description" />
                                    <asp:BoundField ItemStyle-HorizontalAlign="Justify" ItemStyle-Height="20px" HeaderStyle-Height="20px" ItemStyle-Width="260px" ItemStyle-VerticalAlign="Top" DataField="PenaltyDescription" />

                            </Columns>
                            <FooterStyle BackColor="#CCCC99" />
                            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                            <PagerSettings Position="Top" />
                            <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                            <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                            <AlternatingRowStyle BackColor="#E6EFF7" />
                            <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                            <EmptyDataTemplate>
                                No Records Found.
                            </EmptyDataTemplate>
                        </asp:GridView>
                            </div> 
                  <%--  </asp:Panel>--%>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
