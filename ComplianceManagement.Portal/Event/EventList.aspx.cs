﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Web.Security;
using System.Configuration;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Event
{
    public partial class EventList : System.Web.UI.Page
    {
        public bool MGM_KEy;
        protected static int customerid;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (AuthenticationHelper.Role == "MGMT")
            {
                customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);
            }
            bool ISCADMN = false;
            if (AuthenticationHelper.Role == "CADMN")
            {
                ISCADMN = true;
            }
            bool ISIMPL = false;
            if (AuthenticationHelper.Role == "IMPT" || AuthenticationHelper.Role == "RREV" || AuthenticationHelper.Role == "RPER" || AuthenticationHelper.Role == "UPDT" || AuthenticationHelper.Role == "SADMN")
            {
                ISIMPL = true;
            }
            MGM_KEy = CaseManagement.CheckForClient(customerid, "MGMT_CompanyAdmin");
            if (!IsPostBack)
            {
                bool ISMGMT = false;
                if (MGM_KEy && AuthenticationHelper.Role != "EXCT")
                {
                    ISMGMT = true;
                }
                else
                {
                    ISMGMT = false;
                }

                if (HttpContext.Current.Request.IsAuthenticated && (ISMGMT || ISCADMN || ISIMPL))
                {
                        ViewState["SortOrder"] = "Asc";
                        ViewState["SortExpression"] = "Name";
                        BindEvents();                       
                        BindEventClassification();                       
                        BindEntityLevel();
                        txtEntityLevel.Attributes.Add("readonly", "readonly");
                        if ((AuthenticationHelper.Role.Equals("SADMN") || AuthenticationHelper.Role.Equals("RPER") || AuthenticationHelper.Role.Equals("RREV")))
                        {
                            btnAddEvent.Visible = true;
                            lnkMapping.Visible = true;
                            lnkCacheRefresh.Visible = true;
                        }
                        var objlocal = ConfigurationManager.AppSettings["WORKINGDIRECTORY"];
                        string cashTimeval = string.Empty;
                        if (objlocal == "Local")
                        {
                            cashTimeval = "Event_CHE" + AuthenticationHelper.UserID;                            
                        }
                        else
                        {
                            cashTimeval = "EventCHE" + AuthenticationHelper.UserID;                            
                        }

                        if (CacheHelper.Exists(cashTimeval))
                        {
                            DateTime ss;
                            CacheHelper.Get<DateTime>(cashTimeval, out ss);                                                              
                            TimeSpan span = DateTime.Now - ss;
                            if (span.Hours == 0)
                            {
                                //Label1.Text = "last updated :" + ss.ToString("dd-MMM-yyyy hh:mm:ss tt");
                                lnkCacheRefresh.Text = "Last updated within the last hour";
                            }
                            else
                            {
                                lnkCacheRefresh.Text = "Last updated " + span.Hours + " hour ago";
                            }
                        }
                        else
                        {
                            lnkCacheRefresh.Text = "Last updated within the last hour";
                        }
                    }
                    else
                    {
                        //added by rahul on 12 June 2018 Url Sequrity
                        FormsAuthentication.SignOut();
                        Session.Abandon();
                        FormsAuthentication.RedirectToLoginPage();
                    }
              
            }
        }

        

        private void BindCompanyType()
        {
            try
            {
                ddlCompanyType.DataTextField = "Name";
                ddlCompanyType.DataValueField = "ID";
                ddlCompanyType.DataSource = CustomerBranchManagement.GetAllComanyType();
                ddlCompanyType.DataBind();
                ddlCompanyType.Items.Insert(0, new ListItem("< Select >", "-1"));

                if(ddlEventClassification.SelectedValue =="1")
                {
                    ddlCompanyType.Items.Remove(ddlCompanyType.Items.FindByValue("4"));
                }
                else
                {                   
                    ddlCompanyType.Items.Remove(ddlCompanyType.Items.FindByValue("1"));
                    ddlCompanyType.Items.Remove(ddlCompanyType.Items.FindByValue("2"));
                    ddlCompanyType.Items.Remove(ddlCompanyType.Items.FindByValue("3"));
                    ddlCompanyType.Items.Remove(ddlCompanyType.Items.FindByValue("5"));
                    ddlCompanyType.Items.Remove(ddlCompanyType.Items.FindByValue("12"));
                    ddlCompanyType.Items.Remove(ddlCompanyType.Items.FindByValue("13"));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindEventClassification()
        {
            try
            {
                ddlEventClassification.DataTextField = "Name";
                ddlEventClassification.DataValueField = "ID";
                ddlEventClassification.DataSource = EventManagement.GetEventClassification();
                ddlEventClassification.DataBind();
                ddlEventClassification.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindEventSubClassification(long EventClassificationID)
        {
            try
            {
                ddlEventSubClassification.DataSource = null;
                ddlEventSubClassification.DataBind();

                ddlEventSubClassification.DataTextField = "Name";
                ddlEventSubClassification.DataValueField = "ID";
                ddlEventSubClassification.DataSource = EventManagement.GetEventSubClassification(EventClassificationID);
                ddlEventSubClassification.DataBind();
                ddlEventSubClassification.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }         
        private void BindEvents()
        {
            try
            {
                List<Business.Data.SP_GetEventList_Result> EventList = null;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    long? customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID;
                    List<int> CompTypeList = CustomerManagement.GetCustomerBranchTypeList(customerID);

                    EventList = EventManagement.GetCompanyTypeWiseEvents(tbxFilter.Text, CompTypeList);
                }
                else
                {
                    EventList = EventManagement.GetAllEventsNew(tbxFilter.Text);
                }
               
                string isparent = "";
                if (rdoParentEvent.Checked)
                    isparent = "Y";
                if (rdoSubEvent.Checked)
                    isparent = "N";

                if (Convert.ToInt32(ddlFilterComplianceType.SelectedValue) != -1)
                {
                    int a = Convert.ToInt32(ddlFilterComplianceType.SelectedValue);
                    EventList = EventList.Where(entry => entry.EventClassificationID == a).ToList();

                    if (ddlFilterComplianceType.SelectedValue == "1")
                    {
                        if (Convert.ToInt32(ddlsecretrialsubtype.SelectedValue) != -1 && AuthenticationHelper.Role == "CADMN")
                        {
                            long? customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID;
                            List<int> CompTypeList = CustomerManagement.GetCustomerBranchTypeList(customerID);
                            string str = Convert.ToString(ddlsecretrialsubtype.SelectedItem.Text);

                            EventList = EventManagement.GetCompanyTypeWiseEvents(str, CompTypeList);
                        }
                      else  if (Convert.ToInt32(ddlsecretrialsubtype.SelectedValue) != -1 && AuthenticationHelper.Role != "CADMN")
                        {
                            string str = Convert.ToString(ddlsecretrialsubtype.SelectedItem.Text);
                            EventList = EventManagement.GetAllEventsNew(str);
                        }
                    }
                }
               
                if( isparent  != "")
                {
                    if (isparent == "Y")
                    {
                        EventList = EventList.Where(entry => entry.Visible == "Y").ToList();
                    }
                    else
                    {
                        EventList = EventList.Where(entry => entry.Visible != "Y").ToList();
                    }
                }

                if (ViewState["SortOrder"].ToString() == "Asc")
                {
                    if (ViewState["SortExpression"].ToString() == "Name")
                    {
                        EventList = EventList.OrderBy(entry => entry.Name).ToList();
                    }
                   
                    direction = SortDirection.Descending;
                }
                else
                {
                    if (ViewState["SortExpression"].ToString() == "Name")
                    {
                        EventList = EventList.OrderByDescending(entry => entry.Name).ToList();
                    }
                    
                    direction = SortDirection.Ascending;
                }
                

                grdEventList.DataSource = EventList;
                grdEventList.DataBind();
                upEventList.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }       
        private void BindClientEventActivateData(int EventID)
        {
            try
            {
                grdActiveEvent.DataSource = null;
                grdActiveEvent.DataBind();

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var auditDetail = (from row in entities.EventScheduleOns
                                       join row1 in entities.EventInstances on row.EventInstanceID equals row1.ID
                                       join row2 in entities.Events on row1.EventID equals row2.ID
                                       join row3 in entities.CustomerBranches on row1.CustomerBranchID equals row3.ID
                                       join row4 in entities.Customers on row3.CustomerID equals row4.ID
                                       where row.IsDeleted == false && row2.ID == EventID
                                       select new
                                       {
                                           CustomerName = row4.Name
                                                    ,
                                           LocationName = row3.Name
                                                    ,
                                           EvenetName = row2.Name
                                                        ,
                                           EventNature = row.Description
                                       });

                    var AuditDetail = auditDetail.OrderBy(x => x.CustomerName).Distinct().ToList();

                    btnDeleteEvent.Enabled = true;
                    if (AuditDetail.Count() > 0)
                    {
                        btnDeleteEvent.Enabled = false;
                        grdActiveEvent.DataSource = AuditDetail;
                        grdActiveEvent.DataBind();

                    }
                    updivParentEventDelete.Update();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindParentEvent(int EventID)
        {
            try
            {
                grdParentEvent.DataSource = null;
                grdParentEvent.DataBind();

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var auditDetail = (from row in entities.EventSubEventMappings
                                       join row1 in entities.Events on row.SubEventID equals row1.ID
                                       join row2 in entities.Events on row.ParentEventID equals row2.ID
                                       where row.IsActive == true && row1.ID == EventID
                                       select new
                                       {
                                           EvenetName = row2.Name
                                       });

                    var AuditDetail = auditDetail.OrderBy(x => x.EvenetName).Distinct().ToList();

                    btnDeleteSubEvent.Enabled = true;
                    if (AuditDetail.Count() > 0)
                    {
                        btnDeleteSubEvent.Enabled = false;
                        grdParentEvent.DataSource = AuditDetail;
                        grdParentEvent.DataBind();

                    }
                    updivSubEventDelete.Update();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public string EventType(int type)
        {
            try
            {
                string result = "";
                if ( type == 1)
                {
                    result = "Public";
                }
                else if (type == 2)
                {
                    result = "Private";
                }
                else if (type == 3)
                {
                    result = "Listed";
                }
                else if (type == 4)
                {
                    result = "Non-Secretarial";
                }
                else if (type == 12)
                {
                    result = "Public Section 8";
                }
                else if (type == 13)
                {
                    result = "Private Section 8";
                }
                else
                {
                    result = "";
                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return "DeActive";
        }
       

        protected void grdEventList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("EDIT_ACT"))
                {
                    int eventID = Convert.ToInt32(e.CommandArgument);
                    ViewState["Mode"] = 1;
                    ViewState["ActID"] = eventID;
                    chkVisible.Checked = false;
                    com.VirtuosoITech.ComplianceManagement.Business.Data.Event eventData = EventManagement.GetByID(eventID);
                    if (eventData.Visible == "Y")
                    {
                        chkVisible.Checked = true;
                        diveventtype.Visible = true;
                        ddlEventType.SelectedValue = "-1";
                        if (eventData.EType != null)
                        {
                            ddlEventType.SelectedValue = eventData.EType.ToString();
                        }
                    }
                    tbxName.Text = eventData.Name;
                    tbxName.ToolTip = eventData.Name;
                    txtDescription.Text = eventData.Description;

                    ddlEventClassification.SelectedValue = "-1";
                    BindEventSubClassification(-1);
                    ddlEventSubClassification.SelectedValue = "-1";

                    if (eventData.EventClassificationID != null)
                    {
                        if (eventData.EventClassificationID != -1)
                        {
                            ddlEventClassification.SelectedValue = Convert.ToString(eventData.EventClassificationID);
                            BindCompanyType();
                            BindEventSubClassification(Convert.ToInt64(ddlEventClassification.SelectedValue));
                        }
                    }

                    if (eventData.EventSubClassificationID != null)
                    {
                        if (eventData.EventSubClassificationID != -1)
                        {
                            ddlEventSubClassification.SelectedValue = Convert.ToString(eventData.EventSubClassificationID);
                        }
                    }

                    ddlCompanyType.SelectedValue =Convert.ToString(eventData.Type);
                    upEvent.Update();


                    var vGetEventTypeMappedID = Business.ComplianceManagement.GetEventTypeMappedID(eventID);
                    foreach (RepeaterItem aItem in rptEventType.Items)
                    {
                        CheckBox chkEventType = (CheckBox)aItem.FindControl("chkEventType");
                        chkEventType.Checked = false;
                        CheckBox EventTypeSelectAll = (CheckBox)rptEventType.Controls[0].Controls[0].FindControl("EventTypeSelectAll");
                        for (int i = 0; i <= vGetEventTypeMappedID.Count - 1; i++)
                        {
                            if (((Label)aItem.FindControl("lblEntityTypeID")).Text.Trim() == vGetEventTypeMappedID[i].ToString())
                            {
                                chkEventType.Checked = true;
                            }
                        }
                        if ((rptEventType.Items.Count) == (vGetEventTypeMappedID.Count))
                        {
                            EventTypeSelectAll.Checked = true;

                        }
                        else
                        {
                            EventTypeSelectAll.Checked = false;

                        }
                    }
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "initializeCombobox(); $(\"#divEventDialog\").dialog('open')", true);
                }
                else if (e.CommandName.Equals("DELETE_ACT"))
                {
                    int eventID = Convert.ToInt32(e.CommandArgument);
                    ViewState["eventID"] = eventID;
                    bool isEventparent = EventManagement.CheckEvent(eventID);
                    if (isEventparent)
                    {
                        BindClientEventActivateData(eventID);

                        lblEventName.Text = EventManagement.GetEventName(eventID); 

                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "initializeCombobox(); $(\"#divParentEventDelete\").dialog('open')", true);
                    }
                    else
                    {
                        BindParentEvent(eventID);
                        lblSubEventName.Text = EventManagement.GetEventName(eventID);
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "initializeCombobox(); $(\"#divSubEventDelete\").dialog('open')", true);
                    }                   
                    BindEvents();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdEventList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdEventList.PageIndex = e.NewPageIndex;
                BindEvents();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void grdEventList_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                var actList = EventManagement.GetAllEventsNew(tbxFilter.Text);
                if (direction == SortDirection.Ascending)
                {
                    actList = actList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                    ViewState["SortOrder"] = "Asc";
                    ViewState["SortExpression"] = e.SortExpression.ToString();
                }
                else
                {
                    actList = actList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                    ViewState["SortOrder"] = "Desc";
                    ViewState["SortExpression"] = e.SortExpression.ToString();
                }

                foreach (DataControlField field in grdEventList.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdEventList.Columns.IndexOf(field);
                    }
                }

                grdEventList.DataSource = actList;
                grdEventList.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdEventList_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        protected void grdEventList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    LinkButton lbtEdit = (LinkButton)e.Row.FindControl("lbtEdit");
                    LinkButton lbtDelete = (LinkButton)e.Row.FindControl("lbtDelete");
                    lbtEdit.Visible = false;
                    lbtDelete.Visible = false;
                    if (AuthenticationHelper.Role.Equals("SADMN"))
                    {
                        lbtEdit.Visible = true;
                        lbtDelete.Visible = true;
                    }
                    if (AuthenticationHelper.Role.Equals("IMPT"))
                    {
                        lbtEdit.Visible = true;
                        lbtDelete.Visible = true;
                    }
                    if (AuthenticationHelper.Role.Equals("RPER"))
                    {
                        lbtEdit.Visible = true;
                        lbtDelete.Visible = false;
                    }
                    if (AuthenticationHelper.Role.Equals("RREV"))
                    {
                        lbtEdit.Visible = true;
                        lbtDelete.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }

        protected void lnkCacheRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    List<SP_GetEventBasedCompliance_Result> MasterQuery = new List<SP_GetEventBasedCompliance_Result>();
                    var objlocal = ConfigurationManager.AppSettings["WORKINGDIRECTORY"];
                    string cashTimeval = string.Empty;
                    string cashEventval = string.Empty;
                    string cashEventmasterval = string.Empty;
                    string cashActval = string.Empty;
                    if (objlocal == "Local")
                    {
                        cashTimeval = "Event_CHE" + AuthenticationHelper.UserID;
                        cashEventval = "Event_PSD" + AuthenticationHelper.UserID;
                        cashEventmasterval = "Event_CHMaster" + AuthenticationHelper.UserID;
                        cashActval = "Event_CHEAct" + AuthenticationHelper.UserID;
                    }
                    else
                    {
                        cashTimeval = "EventCHE" + AuthenticationHelper.UserID;
                        cashEventval = "EventPSD" + AuthenticationHelper.UserID;
                        cashEventmasterval = "Event_Master" + AuthenticationHelper.UserID;
                        cashActval = "Event_Act" + AuthenticationHelper.UserID;
                    }

                    CacheHelper.Remove(cashEventval);
                    CacheHelper.Remove(cashTimeval);
                    CacheHelper.Remove(cashEventmasterval);
                    CacheHelper.Remove(cashActval);

                    MasterQuery = (from row in entities.SP_GetEventBasedCompliance()
                                   select row).ToList();
                    CacheHelper.Set<List<SP_GetEventBasedCompliance_Result>>(cashEventval, MasterQuery);



                   var eventMasterQuery = (from row in entities.Events
                                   where row.Visible == "Y"
                                    && row.IsDeleted == false
                                   select row).OrderBy(entry => entry.Name).ToList();

                    CacheHelper.Set<List<Business.Data.Event>>(cashEventmasterval, eventMasterQuery);


                    var ActMasterQuery = (from row in entities.Acts
                                       join row1 in entities.Compliances
                                       on row.ID equals row1.ActID
                                       where row.IsDeleted == false
                                       && row1.EventFlag == true || row1.ComplianceType == 1
                                       select row).Distinct().ToList();

                    CacheHelper.Set<List<Act>>(cashActval, ActMasterQuery);

                    if (CacheHelper.Exists(cashTimeval))
                    {
                        CacheHelper.Remove(cashTimeval);
                        CacheHelper.Set(cashTimeval, DateTime.Now);
                    }
                    else
                    {
                        CacheHelper.Set(cashTimeval, DateTime.Now);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnAddEvent_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["Mode"] = 0;
                tbxName.Text = string.Empty;
                txtDescription.Text = string.Empty;
                ddlCompanyType.SelectedValue = "-1";
                ddlEventClassification.SelectedValue = "-1";
                ddlEventSubClassification.SelectedValue = "-1";               
                chkVisible.Checked = false;
                chkVisible_CheckedChanged(null, null);               
                foreach (RepeaterItem aItem in rptEventType.Items)
                {
                    CheckBox chkEventType = (CheckBox)aItem.FindControl("chkEventType");
                    chkEventType.Checked = false;
                    CheckBox EventTypeSelectAll = (CheckBox)rptEventType.Controls[0].Controls[0].FindControl("EventTypeSelectAll");
                    EventTypeSelectAll.Checked = false;
                }
                upEvent.Update();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "initializeCombobox(); $(\"#divEventDialog\").dialog('open')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void chkVisible_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (chkVisible.Checked == true)
                {
                    diveventtype.Visible = true;
                }
                else
                {
                    diveventtype.Visible = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                grdEventList.PageIndex = 0;
                BindEvents();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string visibleflag = "";
                if (chkVisible.Checked == true)
                {
                    visibleflag = "Y";
                }
                long EventClassificationID = -1;
                if (ddlEventClassification.SelectedValue != "-1")
                {
                    EventClassificationID = Convert.ToInt64(ddlEventClassification.SelectedValue);
                }
                long EventSubClassificationID = -1;
                if (ddlEventSubClassification.SelectedValue != "")
                {
                    if (ddlEventSubClassification.SelectedValue != "-1")
                    {
                        EventSubClassificationID = Convert.ToInt64(ddlEventSubClassification.SelectedValue);
                    }
                }

                int? Etype = null;
                if (ddlEventType.SelectedValue != "" && !string.IsNullOrEmpty(ddlEventType.SelectedValue))
                {
                    if (ddlEventType.SelectedValue != "-1")
                    {
                        Etype = Convert.ToInt32(ddlEventType.SelectedValue);
                    }
                }
                com.VirtuosoITech.ComplianceManagement.Business.Data.Event eventData = new com.VirtuosoITech.ComplianceManagement.Business.Data.Event()
                {
                    Name = tbxName.Text,
                    Description = txtDescription.Text,
                    CreatedBy = AuthenticationHelper.UserID,
                    UpdatedBy = AuthenticationHelper.UserID,
                    CreatedOn = DateTime.UtcNow,
                    UpdatedOn = DateTime.UtcNow,
                    IsDeleted = false,
                    Visible = visibleflag,
                    Type = Convert.ToInt32(ddlCompanyType.SelectedValue),
                    EventClassificationID = EventClassificationID,
                    EventSubClassificationID = EventSubClassificationID,
                    EType = Etype,

                };
                if ((int)ViewState["Mode"] == 1)
                {
                    eventData.ID = Convert.ToInt32(ViewState["ActID"]);
                }

                if ((int)ViewState["Mode"] == 0)
                {
                    if (EventManagement.Exists(eventData.Name, eventData.Type))
                    {
                        cvDuplicateEntry.ErrorMessage = "Event name already exists.";
                        cvDuplicateEntry.IsValid = false;
                        return;
                    }
                    EventManagement.Create(eventData);

                    //---------add Entity type  Mapping--------------------------------------------
                    List<int> EntityTypeIds = new List<int>();
                    foreach (RepeaterItem aItem in rptEventType.Items)
                    {
                        CheckBox chkEventType = (CheckBox)aItem.FindControl("chkEventType");
                        if (chkEventType.Checked)
                        {
                            EntityTypeIds.Add(Convert.ToInt32(((Label)aItem.FindControl("lblEntityTypeID")).Text.Trim()));

                            EventTypeDataMapping eventTypeMapping = new EventTypeDataMapping()
                            {
                                EventId = eventData.ID,
                                EventTypeID = Convert.ToInt32(((Label)aItem.FindControl("lblEntityTypeID")).Text.Trim()),
                                IsActive = true,
                                EditedDate = DateTime.UtcNow,
                                EditedBy = Convert.ToInt32(Session["userID"]),

                            };
                            Business.ComplianceManagement.CreateEventTypeMapping(eventTypeMapping);
                        }
                    }

                }
                else if ((int)ViewState["Mode"] == 1)
                {
                    if (eventData.Visible == "Y")
                    {
                        diveventtype.Visible = true;
                    }
                    EventManagement.Update(eventData);

                    List<int> EventmappingID = new List<int>();
                    Business.ComplianceManagement.UpdateEventTypeMappedID(eventData.ID);
                    foreach (RepeaterItem aItem in rptEventType.Items)
                    {
                        CheckBox chkEventType = (CheckBox)aItem.FindControl("chkEventType");
                        if (chkEventType.Checked)
                        {
                            EventmappingID.Add(Convert.ToInt32(((Label)aItem.FindControl("lblEntityTypeID")).Text.Trim()));

                            EventTypeDataMapping eventTypeMapping = new EventTypeDataMapping()
                            {
                                EventId = eventData.ID,
                                EventTypeID = Convert.ToInt32(((Label)aItem.FindControl("lblEntityTypeID")).Text.Trim()),
                                IsActive = true,
                                EditedDate = DateTime.UtcNow,
                                EditedBy = Convert.ToInt32(Session["userID"]),

                            };
                            Business.ComplianceManagement.CreateEventTypeMapping(eventTypeMapping);

                        }
                    }
                }
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divEventDialog\").dialog('close')", true);
                BindEvents();

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                GetEvents();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static List<EventTypeData> GetAllLegalEntityType()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var LegalEntityType = entities.EventTypeDatas;
                return LegalEntityType.OrderBy(entry => entry.Name).ToList();

            }
        }
        private void BindEntityLevel()
        {
            try
            {

               rptEventType.DataSource = GetAllLegalEntityType();
                rptEventType.DataBind();

                foreach (RepeaterItem aItem in rptEventType.Items)
                {
                    CheckBox chkEventType = (CheckBox)aItem.FindControl("chkEventType");

                    if (!chkEventType.Checked)
                    {
                        chkEventType.Checked = true;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }      
        private void GetEvents()
        {
            try
            {             
                DateTime date = DateTime.Now;
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                upEvent.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }        
        protected void upEvent_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeEntityTypeList", string.Format("initializeJQueryUI('{0}', 'dvEntityLevel');", txtEntityLevel.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideEntityTypeList", "$(\"#dvEntityLevel\").hide(\"blind\", null, 5, function () { });", true);               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlFilterCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindEvents();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public string ShowFrequency(int frq)
        {
            return Enumerations.GetEnumByID<EventFrequency>(frq);
        }
        public string ShowType(int typ)
        {
            return Enumerations.GetEnumByID<EventType>(typ);
        }        
        protected void btnComplianceRepeater_Click(object sender, EventArgs e)
        {

        }
        protected void btnCompanyTypeRepeater_Click(object sender, EventArgs e)
        {

        }
        protected void ddlEventClassification_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindCompanyType();
                BindEventSubClassification(Convert.ToInt64(ddlEventClassification.SelectedValue));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlFilterComplianceType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if(ddlFilterComplianceType.SelectedValue=="1")
                {
                    ddlsecretrialsubtype.Visible = true;
                }
                else
                {
                    ddlsecretrialsubtype.Visible =false;
                }
                BindEvents();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlsecretrialsubtype_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindEvents();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void rdoParentEvent_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                BindEvents();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void rdoSubEvent_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                BindEvents();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnDeleteEvent_Click(object sender, EventArgs e)
        {
            try
            {
                int eventID = Convert.ToInt32(ViewState["eventID"]);
                Business.EventManagement.DeleteEventAssignment(eventID);
                EventManagement.Delete(eventID);

                CustomValidator1.ErrorMessage = "Event deleted successfully";
                CustomValidator1.IsValid = false;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CustomValidator1.IsValid = false;
                CustomValidator1.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnbtnDeleteEventClose_Click(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "divParentEventDelete", "$(\"#divParentEventDelete\").dialog('close')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnDeleteSubEvent_Click(object sender, EventArgs e)
        {
            try
            {
                int eventID = Convert.ToInt32(ViewState["eventID"]);
                EventManagement.Delete(eventID);
                CustomValidator2.ErrorMessage = "Sub event deleted successfully";
                CustomValidator2.IsValid = false;
                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }           
        }
        protected void btnCloseSubEvent_Click(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "divSubEventDelete", "$(\"#divSubEventDelete\").dialog('close')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }           
        }
    }
}