﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="EventRelationship.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Event.EventRelationship" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
     <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div>
        <asp:UpdatePanel ID="upEvent" runat="server" UpdateMode="Conditional" OnLoad="upEvent_Load">
            <ContentTemplate>
                <div style="margin-bottom: 4px">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="vdsummary"
                        ValidationGroup="EventValidationGroup" />
                    <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                        ValidationGroup="EventValidationGroup" Display="None" />
                </div>
                <asp:Panel ID="Panel1" Width="100%" Height="100px" runat="server">
                    <table runat="server" width="70%">
                        <tr>
                            <td class="td1">
                                <div style="margin: 10px 20px 10px 294px">
                                    <div>
                                        <asp:RadioButtonList runat="server" ID="rblRelationship" RepeatDirection="Horizontal" RepeatLayout="Flow"
                                            OnSelectedIndexChanged="rblRelationship_SelectedIndexChanged" Font-Bold="true" AutoPostBack="true">
                                          <%--  <asp:ListItem Text="Public" Value="0" Selected="True" />
                                            <asp:ListItem Text="Private" Value="1" />
                                            <asp:ListItem Text="Listed" Value="2" />
                                            <asp:ListItem Text="Non-Secretarial" Value="3" />
                                            <asp:ListItem Text="LLP" Value="5" />
                                            <asp:ListItem Text="Public Section 8" Value="12" />
                                            <asp:ListItem Text="Private Section 8" Value="13" />--%>
                                            <asp:ListItem Text="Public" Value="1" Selected="True" />
                                            <asp:ListItem Text="Private" Value="2" />
                                            <asp:ListItem Text="Listed" Value="3" />
                                            <asp:ListItem Text="Non-Secretarial" Value="4" />
                                            <asp:ListItem Text="LLP" Value="5" />
                                            <asp:ListItem Text="Public Section 8" Value="12" />
                                            <asp:ListItem Text="Private Section 8" Value="13" />
                                        </asp:RadioButtonList>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <asp:Label runat="server" ID="lblEventType" Text="Event :" Style="width: 200px; display: block; float: left; font-size: 13px; color: #333;"></asp:Label>
                        <asp:DropDownList runat="server" ID="ddlEvent" AutoPostBack="true" Style="padding: 0px; margin: 0px; height: 22px; width: 542px;"
                            CssClass="txtbox" OnSelectedIndexChanged="ddlEvent_SelectedIndexChanged">
                        </asp:DropDownList>
                        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                        <asp:CompareValidator ErrorMessage="Please Select Event." ControlToValidate="ddlEvent" ID="rfEvents"
                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceValidationGroup"
                            Display="None" />
                    </div>
                </asp:Panel>
                <table style="margin-left: 30px;" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <asp:Label ID="Label1" Style="color: #3127c5; font-weight: bold;" Text="Mapping Intermediate Event List" runat="server" ForeColor="Black"></asp:Label>
                            <asp:ListBox ID="lstLeftIntermediate" runat="server" SelectionMode="Multiple" Width="580" Height="160px"></asp:ListBox>
                        </td>
                        <td>
                            <asp:Button ID="btnRightIntermediateMove" runat="server" Text=">>" Width="45px" Style="margin-bottom: 10px;" OnClick="btnRightIntermediateMove_Click" />
                            <asp:Button ID="btnLeftIntermediateMove" runat="server" Text="<<" Width="45px" OnClick="btnLeftIntermediateMove_Click" />
                        </td>
                        <td>
                            <asp:Label ID="Label2" Style="color: #3127c5; font-weight: bold;" Text="Mapped Intermediate Event List" runat="server" ForeColor="Black"></asp:Label>
                            <asp:ListBox ID="lstRightIntermediate" runat="server" SelectionMode="Multiple" Width="580" Height="160px"></asp:ListBox>
                        </td>
                    </tr>

                </table>

                <table style="margin-left: 30px; margin-top:10px" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <asp:Label ID="Label3" Style="color: #3127c5; font-weight: bold;" Text="Mapping Sub Event List" runat="server" ForeColor="Black"></asp:Label>
                            <asp:ListBox ID="lstLeftSubEvent" runat="server" SelectionMode="Multiple" Width="580" Height="160px"></asp:ListBox>
                        </td>
                        <td>
                            <asp:Button ID="btnRightSubEventMove" runat="server" Text=">>" Width="45px" Style="margin-bottom: 10px;" OnClick="btnRightSubEventMove_Click" />
                            <asp:Button ID="btnLeftSubEventMove" runat="server" Text="<<" Width="45px" OnClick="btnLeftSubEventMove_Click" />
                        </td>
                        <td>
                            <asp:Label ID="Label4" Style="color: #3127c5; font-weight: bold;" Text="Mapped Sub Event List" runat="server" ForeColor="Black"></asp:Label>
                            <asp:ListBox ID="lstRightSubEvent" runat="server" SelectionMode="Multiple" Width="580" Height="160px"></asp:ListBox>
                        </td>
                    </tr>
                </table>
                <div style="margin-bottom: 7px; margin-left: 613px; margin-top: 10px">
                    <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="button"
                        ValidationGroup="EventValidationGroup" />
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <script type="text/javascript">
        
        $(function () {
            initializeCombobox();
        });
        function initializeCombobox() {
            $("#<%= ddlEvent.ClientID %>").combobox();
        }
        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }
    </script>
</asp:Content>
