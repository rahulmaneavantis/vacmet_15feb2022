﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Configuration;
using System.Threading;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using System.Data;
using System.ComponentModel;
using System.Collections;
using System.Data.SqlClient;
using System.Data.Objects;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;


namespace com.VirtuosoITech.ComplianceManagement.Portal.Event
{
    public partial class EventStructure : System.Web.UI.Page
    {
        protected static string DataString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindParentEvent();
                DataString = "";
            }
        }
        private void BindParentEvent()
        {
            try
            {
                ComplianceDBEntities entities = new ComplianceDBEntities();
                ddlparentEvent.DataTextField = "Name";
                ddlparentEvent.DataValueField = "ParentEventID";

                int Type = 0;
                if (!String.IsNullOrEmpty(rblRelationship.SelectedValue))
                {
                    Type = Convert.ToInt32(rblRelationship.SelectedValue);
                }
                //if (rblRelationship.SelectedValue == "0")
                //{
                //    Type = 1;
                //}
                //if (rblRelationship.SelectedValue == "1")
                //{
                //    Type = 2;
                //}
                //if (rblRelationship.SelectedValue == "2")
                //{
                //    Type = 3;
                //}
                //if (rblRelationship.SelectedValue == "3")
                //{
                //    Type = 4;
                //}

                ddlparentEvent.DataSource = entities.SP_GetParentEvent(Type).ToList(); ;
                ddlparentEvent.DataBind();
                ddlparentEvent.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        
        protected void gvParentGrid_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int Type = 0;
                if (!String.IsNullOrEmpty(rblRelationship.SelectedValue))
                {
                    Type = Convert.ToInt32(rblRelationship.SelectedValue);
                }
                //if (rblRelationship.SelectedValue == "0")
                //{
                //    Type = 1;
                //}
                //if (rblRelationship.SelectedValue == "1")
                //{
                //    Type = 2;
                //}
                //if (rblRelationship.SelectedValue == "2")
                //{
                //    Type = 3;
                //}
                //if (rblRelationship.SelectedValue == "3")
                //{
                //    Type = 4;
                //}

                int ParentEventID = Convert.ToInt32(e.Row.Cells[1].Text);
                ComplianceDBEntities entities = new ComplianceDBEntities();
               
                GridView gv = (GridView)e.Row.FindControl("gvChildGrid");
                string type = Convert.ToString(gvParentGrid.DataKeys[e.Row.RowIndex]["Type"]);

                var SubEvent = entities.SP_GetEventData(ParentEventID,type,Type).ToList();
                gv.DataSource = SubEvent;
                gv.DataBind();

               
            }
        }

        protected void gvChildGrid_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int Type = 0;
                if (!String.IsNullOrEmpty(rblRelationship.SelectedValue))
                {
                    Type = Convert.ToInt32(rblRelationship.SelectedValue);
                }
                //if (rblRelationship.SelectedValue == "0")
                //{
                //    Type = 1;
                //}
                //if (rblRelationship.SelectedValue == "1")
                //{
                //    Type = 2;
                //}
                //if (rblRelationship.SelectedValue == "2")
                //{
                //    Type = 3;
                //}
                //if (rblRelationship.SelectedValue == "3")
                //{
                //    Type = 4;
                //}
                Label lblIntermediateEventID =(Label) e.Row.FindControl("IntermediateEventID");
                if (lblIntermediateEventID.Text == "0")
                {

                    GridView gv = (GridView)e.Row.FindControl("gvComplianceGrid");
                    int SubEventID = Convert.ToInt32(e.Row.Cells[1].Text);
                    GridView childGrid1 = (GridView)sender;
                    int Parentid = Convert.ToInt32(childGrid1.DataKeys[e.Row.RowIndex].Value.ToString());
                    ComplianceDBEntities entities = new ComplianceDBEntities();
                    var Compliance = entities.SP_GetCompliance(Parentid, SubEventID, Type).ToList();
                    if (Compliance.Count() > 0)
                    {
                        gv.DataSource = Compliance;
                        gv.DataBind();
                    }
                }
                else
                {
                    GridView gv1 = (GridView)e.Row.FindControl("gvIntermediateSubEventGrid");
                    int IntermediateEventID = Convert.ToInt32(e.Row.Cells[1].Text);

                    GridView childGrid = (GridView)sender;
                    int Parentid1 = Convert.ToInt32(childGrid.DataKeys[e.Row.RowIndex].Value.ToString());
                    ComplianceDBEntities entities1 = new ComplianceDBEntities();
                    var SubSubEvent = entities1.SP_GetIntermediateSubEvent(IntermediateEventID, Parentid1, Type).ToList();
                    if (SubSubEvent.Count() > 0)
                    {
                        gv1.DataSource = SubSubEvent;
                        gv1.DataBind();
                    }

                }

            }
        }

        protected void gvIntermediateSubEventGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    int Type = 0;
                    if (!String.IsNullOrEmpty(rblRelationship.SelectedValue))
                    {
                        Type = Convert.ToInt32(rblRelationship.SelectedValue);
                    }
                    //if (rblRelationship.SelectedValue == "0")
                    //{
                    //    Type = 1;
                    //}
                    //if (rblRelationship.SelectedValue == "1")
                    //{
                    //    Type = 2;
                    //}
                    //if (rblRelationship.SelectedValue == "2")
                    //{
                    //    Type = 3;
                    //}
                    //if (rblRelationship.SelectedValue == "3")
                    //{
                    //    Type = 4;
                    //}
                    GridView gv = (GridView)e.Row.FindControl("gvIntermediateComplainceGrid");
                  
                    int SubEventID = Convert.ToInt32(e.Row.Cells[1].Text);
                    GridView childGrid1 = (GridView)sender;
                    int intermediateEventID = Convert.ToInt32(childGrid1.DataKeys[e.Row.RowIndex].Value.ToString());
                    ComplianceDBEntities entities = new ComplianceDBEntities();
                    int Parentid = Convert.ToInt32(ddlparentEvent.SelectedValue);
                    var Compliance = entities.SP_GetIntermediateCompliance(Parentid,intermediateEventID, SubEventID, Type).ToList();
                    if (Compliance.Count() > 0)
                    {
                        gv.DataSource = Compliance;
                        gv.DataBind();
                    }                  
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindParentEventData(int ParentEventID)
        {
            try
            {
                //int Type = 0;
                //if (rblRelationship.SelectedValue == "0")
                //{
                //    Type = 1;
                //}
                //if (rblRelationship.SelectedValue == "1")
                //{
                //    Type = 2;
                //}
                //if (rblRelationship.SelectedValue == "2")
                //{
                //    Type = 3;
                //}
                //if (rblRelationship.SelectedValue == "3")
                //{
                //    Type = 4;
                //}
                int Type = 0;
                if (!String.IsNullOrEmpty(rblRelationship.SelectedValue))
                {
                    Type = Convert.ToInt32(rblRelationship.SelectedValue);
                }
                ComplianceDBEntities entities = new ComplianceDBEntities();
                var ParentEvent = entities.SP_GetParentEventSelected(ParentEventID,Type).ToList();
                gvParentGrid.DataSource = ParentEvent;
                gvParentGrid.DataBind();                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlparentEvent_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindParentEventData(Convert.ToInt32(ddlparentEvent.SelectedValue));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void rblRelationship_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                gvParentGrid.DataSource = null;
                gvParentGrid.DataBind();
                BindParentEvent();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
        }

       
    }
}