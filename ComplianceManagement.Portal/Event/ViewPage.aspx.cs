﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Event
{
    public partial class ViewPage : System.Web.UI.Page
    {
        int Type = 0;
        static int glParentID = 0;
        static int glsubEventID = 0;
        static int glIntermediateID = 0;
        static int PageCount = 0;

        public static int EventSchID;
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["ID"])
                    && !string.IsNullOrEmpty(Request.QueryString["EventScheduledOnID"])
                    && !string.IsNullOrEmpty(Request.QueryString["Type"])
                    && !string.IsNullOrEmpty(Request.QueryString["CustomerBranchID"])
                    && !string.IsNullOrEmpty(Request.QueryString["EventClassificationID"])
                    && !string.IsNullOrEmpty(Request.QueryString["EventDescription"])
                    && !string.IsNullOrEmpty(Request.QueryString["ComplianceID"]))
                {                    
                    int ParentID = Convert.ToInt32(Request.QueryString["ID"]);

                    if (!string.IsNullOrEmpty(Request.QueryString["EventScheduledOnID"]))
                    {
                         EventSchID = Convert.ToInt32(Request.QueryString["EventScheduledOnID"]);
                    }

                  
                    int Type = Convert.ToInt32(Request.QueryString["Type"]);
                    int CBID = Convert.ToInt32(Request.QueryString["CustomerBranchID"]);
                    int ECID = Convert.ToInt32(Request.QueryString["EventClassificationID"]);
                    BindParentEventData(ParentID,EventSchID,Type,CBID);
                    int UID = Convert.ToInt32(Request.QueryString["UserID"]);
                    int CustID = Convert.ToInt32(Request.QueryString["CustomerID"]);
                    string CompID = Convert.ToString(Request.QueryString["ComplianceID"]);
                    string EvntD = Convert.ToString(Request.QueryString["EventDescription"]);
                    txtDescription.Text = EvntD;
                }
              
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }
        }
        private string getEventName(int eventRoleID)
        {
            int ERID = Convert.ToInt32(Session["eventRoleID"]);
            ComplianceDBEntities entities = new ComplianceDBEntities();
            var eventName = (from row in entities.SP_GetAllActivatedEvent(eventRoleID, Convert.ToInt32(Session["UserID"]), Convert.ToInt32(Session["CustomerID"]))
                             where row.ID == eventRoleID
                             select row.EventDescription).SingleOrDefault();
            return eventName;
        }
        private void BindParentEventData(int ParentEventID,int EventSchID,int Type,int CBID)
        {
            try
            {
                ComplianceDBEntities entities = new ComplianceDBEntities();
                var ParentEvent = entities.SP_GetParentActivatedEvent(ParentEventID, EventSchID, Type).ToList();
                gvParentGrid.DataSource = ParentEvent;
                gvParentGrid.DataBind();

                upOptionalCompliances.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void upEventInstanceList_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeFilterLocation", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');"), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void gvIntermediateSubEventGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DateTime date = DateTime.Now;
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);

                    GridView gv = (GridView)e.Row.FindControl("gvIntermediateComplainceGrid");
                    int SubEventID = Convert.ToInt32(e.Row.Cells[1].Text);
                    glsubEventID = SubEventID;
                    GridView childGrid1 = (GridView)sender;
                    int intermediateEventID = Convert.ToInt32(childGrid1.DataKeys[e.Row.RowIndex].Value.ToString());
                    ComplianceDBEntities entities = new ComplianceDBEntities();
                    int parentEventID = Convert.ToInt32(Session["eventId"]);
                    int CustomerBranchID = Convert.ToInt32(Session["Rbranch"]);

                    int EventClassificationID = Convert.ToInt32(Session["EventClassificationID"]);

                    if (EventClassificationID == 1)
                    {
                        var Compliance = entities.SP_GetIntermediateComplianceAssignWithShortNoticeDays(parentEventID, intermediateEventID, SubEventID, EventSchID, Type, CustomerBranchID).ToList();
                        gv.DataSource = Compliance;
                        gv.DataBind();
                    }
                    else if (EventClassificationID == 2)
                    {
                        var Compliance = entities.SP_GetNonSecretrialIntermediateComplianceAssignWithShortNoticeDays(parentEventID, intermediateEventID, SubEventID, EventSchID, Type, CustomerBranchID).ToList();
                        gv.DataSource = Compliance;
                        gv.DataBind();

                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void gvIntermediateComplainceGrid_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int ComplianceID = Convert.ToInt32(e.Row.Cells[0].Text);

                string type = Business.EventManagement.CheckEventComplianceType(ComplianceID);

                Label lblIntermediateStatus = (e.Row.FindControl("lblIntermediateStatus") as Label);

                if (type == "Informational")
                {
                    e.Row.ForeColor = System.Drawing.Color.Chocolate;
                    e.Row.CssClass = "Inforamative";
                    lblIntermediateStatus.Visible = false;
                }
                else if (type == "Actionable")
                {
                    e.Row.ForeColor = System.Drawing.Color.SlateBlue;
                    e.Row.CssClass = "Actionable";
                    lblIntermediateStatus.Visible = true;
                    Boolean transcomplete = CheckCompleteTransactionComplete(glParentID, glIntermediateID, glsubEventID, ComplianceID, EventSchID);
                    if (transcomplete == true)
                    {
                        lblIntermediateStatus.Visible = true;
                    }
                    else
                    {
                        lblIntermediateStatus.Visible = false;
                    }
                }
                else
                {
                    Boolean transcomplete = CheckCompleteTransactionComplete(glParentID, glIntermediateID, glsubEventID, ComplianceID, EventSchID);
                    if (transcomplete == true)
                    {
                        lblIntermediateStatus.Visible = true;
                    }
                    else
                    {
                        lblIntermediateStatus.Visible = false;
                    }
                }

            }
        }

        //protected void txtDescription_OnRowDataBound(object sender, GridViewRowEventArgs  e)
        //{
        //    try
        //    {
        //          txtDescription.Text = Convert.ToString((e.Row.FindControl("lbleventDesc") as Label).Text);
        //        //BindParentEventData(Convert.ToInt32(eventId));
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvDuplicateEntry.IsValid = false;
        //        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
        //    }
        //}
        protected void gvIntermediateGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DateTime date = DateTime.Now;
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);

                GridView gv = (GridView)e.Row.FindControl("gvIntermediateSubEventGrid");
                int IntermediateEventID = Convert.ToInt32(e.Row.Cells[1].Text);
                glIntermediateID = IntermediateEventID;
                GridView childGrid1 = (GridView)sender;
                int Parentid = Convert.ToInt32(childGrid1.DataKeys[e.Row.RowIndex].Value.ToString());
                ComplianceDBEntities entities = new ComplianceDBEntities();

                var Compliance = entities.SP_GetActivatedIntermediateSubEvent(IntermediateEventID, EventSchID, Parentid, Type).ToList();
                gv.DataSource = Compliance;
                gv.DataBind();
            }
        }
        protected void gvChildGrid_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DateTime date = DateTime.Now;
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);

                GridView gv = (GridView)e.Row.FindControl("gvComplianceGrid");
                int SubEventID = Convert.ToInt32(e.Row.Cells[1].Text);
                glsubEventID = SubEventID;
                GridView childGrid1 = (GridView)sender;
                //Retreiving the GridView DataKey Value
                int Parentid = Convert.ToInt32(childGrid1.DataKeys[e.Row.RowIndex].Value.ToString());
                ComplianceDBEntities entities = new ComplianceDBEntities();
                //var Compliance = entities.SP_GetComplianceWithDays(Parentid, SubEventID, Type).ToList();
                //int CustomerBranchID = Convert.ToInt32(Session["Rbranch"]);
                int CustomerBranchID = Convert.ToInt32(Request.QueryString["CustomerBranchID"]);
                //int EventClassificationID = Convert.ToInt32(Session["EventClassificationID"]);
                int ECID = Convert.ToInt32(Request.QueryString["EventClassificationID"]);
                //int ParentID = Convert.ToInt32(Request.QueryString["ID"]);
              
                int Type = Convert.ToInt32(Request.QueryString["Type"]);
                //int CBID = Convert.ToInt32(Request.QueryString["CustomerBranchID"]);
                if (ECID == 1)
                {
                    var Compliance = entities.SP_GetComplianceWithShortNoticeDays(Parentid, SubEventID, Convert.ToInt32(EventSchID), Type, CustomerBranchID).ToList();
                    gv.DataSource = Compliance;
                    gv.DataBind();
                }
                else if (ECID == 2)
                {
                    var Compliance = entities.SP_GetNonSecretrialComplianceWithShortNoticeDays(Parentid, SubEventID, Convert.ToInt32(EventSchID), Type, CustomerBranchID).ToList();
                    gv.DataSource = Compliance;
                    gv.DataBind();
                }
            }
        }
        protected void gvComplianceGrid_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int ComplianceID = Convert.ToInt32(e.Row.Cells[0].Text);

                string type = Business.EventManagement.CheckEventComplianceType(ComplianceID);

                Label lblStatus = (e.Row.FindControl("lblStatus") as Label);

                if (type == "Informational")
                {
                    e.Row.ForeColor = System.Drawing.Color.Chocolate;
                    e.Row.CssClass = "Inforamative";
                    lblStatus.Visible = false;
                }
                else if (type == "Actionable")
                {
                    e.Row.ForeColor = System.Drawing.Color.SlateBlue;
                    e.Row.CssClass = "Actionable";
                    lblStatus.Visible = false;
                    Boolean transcomplete = CheckCompleteTransactionComplete(glParentID, 0, glsubEventID, ComplianceID, EventSchID);
                    if (transcomplete == true)
                    {
                        lblStatus.Visible = true;
                    }
                    else
                    {
                        lblStatus.Visible = false;
                    }
                }
                else
                {
                    Boolean transcomplete = CheckCompleteTransactionComplete(glParentID, 0, glsubEventID, ComplianceID, EventSchID);
                    if (transcomplete == true)
                    {
                        lblStatus.Visible = true;
                    }
                    else
                    {
                        lblStatus.Visible = false;
                    }
                }

            }
        }
        protected void gvParentGrid_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DateTime date = DateTime.Now;
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);

                int ParentEventID = Convert.ToInt32(e.Row.Cells[1].Text);
                glParentID = ParentEventID;
                ViewState["ParentEventID"] = ParentEventID;

                ComplianceDBEntities entities = new ComplianceDBEntities();
                GridView gv1 = (GridView)e.Row.FindControl("gvParentToComplianceGrid");
                //int CustomerBranchID = 7;
                //int CustomerBranchID = Convert.ToInt32(Session["CBID"]);
                int CustomerBranchID = Convert.ToInt32(Session["Rbranch"]);

                //int Type = Convert.ToInt32(Session["Type"]);
                //var compliance = entities.SP_GetParentEventToComplianceAssignDays(ParentEventID, Type).ToList();
                //int Type = 2;
                //var compliance = entities.SP_GetParentEventToComplianceAssignWithShortNoticeDays(ParentEventID, Convert.ToInt32(Session["EventScheduledOnID"]), Type, CustomerBranchID).ToList();

                int ParentID = Convert.ToInt32(Request.QueryString["ID"]);
               
                int Type = Convert.ToInt32(Request.QueryString["Type"]);
                int CBID = Convert.ToInt32(Request.QueryString["CustomerBranchID"]);

                var compliance = entities.SP_GetParentEventToComplianceAssignWithShortNoticeDays(ParentEventID, Convert.ToInt32(EventSchID), Type, CBID).ToList();
                gv1.DataSource = compliance;
                gv1.DataBind();

                GridView gv = (GridView)e.Row.FindControl("gvChildGrid");
                string type = Convert.ToString(gvParentGrid.DataKeys[e.Row.RowIndex]["Type"]);

                var SubEvent = entities.SP_GetActivatedSubEvent(ParentEventID, Convert.ToInt32(EventSchID), Type).ToList();
                //var SubEvent = entities.SP_GetActivatedSubEvent(ParentEventID, Convert.ToInt32(Session["EventScheduledOnID"]), Type).ToList();
                gv.DataSource = SubEvent;
                gv.DataBind();

                GridView gv2 = (GridView)e.Row.FindControl("gvIntermediateGrid");
                string type2 = Convert.ToString(gvParentGrid.DataKeys[e.Row.RowIndex]["Type"]);

                var Intermediate = entities.SP_GetActivatedIntermediateEvent(ParentEventID, Convert.ToInt32(EventSchID), Type).ToList();
                gv2.DataSource = Intermediate;
                gv2.DataBind();
            }
        }
        protected void gvParentToComplianceGrid_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int ComplianceID = Convert.ToInt32(e.Row.Cells[1].Text);

                string type = Business.EventManagement.CheckEventComplianceType(ComplianceID);

                Label lblParentStatus = (e.Row.FindControl("lblParentStatus") as Label);

                if (type == "Informational")
                {
                    e.Row.ForeColor = System.Drawing.Color.Chocolate;
                    e.Row.CssClass = "Inforamative";
                    lblParentStatus.Visible = false;
                }
                else if (type == "Actionable")
                {
                    e.Row.ForeColor = System.Drawing.Color.SlateBlue;
                    e.Row.CssClass = "Actionable";
                    lblParentStatus.Visible = true;
                    Boolean transcomplete = CheckCompleteTransactionComplete(glParentID, 0, 0, ComplianceID, EventSchID);
                    if (transcomplete == true)
                    {
                        lblParentStatus.Visible = true;
                    }
                    else
                    {
                        lblParentStatus.Visible = false;
                    }
                }
                else
                {
                    Boolean transcomplete = CheckCompleteTransactionComplete(glParentID, 0, 0, ComplianceID, EventSchID);
                    if (transcomplete == true)
                    {
                        lblParentStatus.Visible = true;
                    }
                    else
                    {
                        lblParentStatus.Visible = false;
                    }
                }

            }
        }
        protected bool CheckCompleteTransactionComplete(int ParentEventID, int IntermediateEventID, int SubEventID, int ComplianceID, int EventScheduledOnId)
        {
            try
            {
                bool result = false;
                ComplianceDBEntities entities = new ComplianceDBEntities();

                var ComplainceTransCompleteList = (from row in entities.ComplianceScheduleOns
                                                   join row1 in entities.ComplianceTransactions
                                                   on row.ID equals row1.ComplianceScheduleOnID
                                                   join row2 in entities.ComplianceInstances
                                                   on row1.ComplianceInstanceId equals row2.ID
                                                   where row.ParentEventD == ParentEventID && row.IntermediateEventID == IntermediateEventID
                                                   && row.SubEventID == SubEventID && row2.ComplianceId == ComplianceID && row.EventScheduledOnID == EventScheduledOnId
                                                   && (row1.StatusId == 4 || row1.StatusId == 5)
                                                   select row).ToList();

                if (ComplainceTransCompleteList.Count >= 1)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }
        protected bool visibleParentEventUpdateButton(int ParentEventID, int EventScheduledOnId)
        {
            try
            {
                bool result = false;
                ComplianceDBEntities entities = new ComplianceDBEntities();
                var data1 = (from row in entities.EventCompAssignDays
                             join row1 in entities.EventAssignDates
                             on row.ParentEventID equals row1.ParentEventID
                             where row.ParentEventID == ParentEventID && row.IntermediateEventID == 0
                             && row.SubEventID == 0 && row1.EventScheduleOnID == EventScheduledOnId
                             select row).FirstOrDefault();
                if (data1 == null)
                {
                    result = false;
                }
                else
                {
                    var data = (from row in entities.ComplianceScheduleOns
                                join row1 in entities.ComplianceTransactions
                                on row.ID equals row1.ComplianceScheduleOnID
                                where row.ParentEventD == ParentEventID && row.IntermediateEventID == 0
                                && row.SubEventID == 0 && row.EventScheduledOnID == EventScheduledOnId
                                && row1.StatusId != 1
                                select row).FirstOrDefault();

                    if (data == null)
                    {
                        result = EventManagement.CheckParentEventDateAssigned(ParentEventID, EventScheduledOnId);

                        if (result == false)
                        {
                            result = true;
                        }
                    }
                    else
                    {
                        result = false;
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }
        protected bool visibleParentEventAddButton(int ParentEventID, int EventScheduledOnId)
        {
            try
            {
                try
                {
                    bool result = false;
                    ComplianceDBEntities entities = new ComplianceDBEntities();
                    int CustomerBranchID = Convert.ToInt32(Session["Rbranch"]);
                    int EventClassificationID = Convert.ToInt32(Session["EventClassificationID"]);

                    if (EventClassificationID == 1)
                    {
                        var data = (from row in entities.EventCompAssignDays
                                    join row1 in entities.Compliances on row.ComplianceID equals row1.ID
                                    join row2 in entities.IndustryMappings on row1.ID equals row2.ComplianceId
                                    join row3 in entities.CustomerBranchIndustryMappings on row2.IndustryID equals row3.IndustryID
                                    join row4 in entities.CustomerBranches on row3.CustomerBranchID equals row4.ID
                                    where row.ParentEventID == ParentEventID && row.IntermediateEventID == 0
                                    && row.SubEventID == 0 && row1.ComplinceVisible == true
                                    && row4.ID == CustomerBranchID
                                    select row).Distinct().FirstOrDefault();

                        if (data == null)
                        {
                            result = false;
                        }
                        else
                        {
                            var data1 = (from row in entities.ComplianceScheduleOns
                                         join row1 in entities.ComplianceTransactions
                                         on row.ComplianceInstanceID equals row1.ComplianceInstanceId
                                         where row.ParentEventD == ParentEventID && row.IntermediateEventID == 0
                                         && row.SubEventID == 0 && row.EventScheduledOnID == EventScheduledOnId
                                         && row1.StatusId != 1
                                         select row).FirstOrDefault();

                            if (data1 == null)
                            {
                                result = EventManagement.CheckParentEventDateAssigned(ParentEventID, EventScheduledOnId);

                                return result;
                            }
                            else
                            {
                                result = false;
                            }
                        }
                    }
                    else if (EventClassificationID == 2)
                    {
                        var data = (from row in entities.EventCompAssignDays
                                    join row1 in entities.Compliances on row.ComplianceID equals row1.ID
                                    join row6 in entities.Acts on row1.ActID equals row6.ID
                                    join row2 in entities.IndustryMappings on row1.ID equals row2.ComplianceId
                                    join row3 in entities.CustomerBranchIndustryMappings on row2.IndustryID equals row3.IndustryID
                                    join row4 in entities.CustomerBranches on row3.CustomerBranchID equals row4.ID
                                    where row.ParentEventID == ParentEventID && row.IntermediateEventID == 0
                                    && row.SubEventID == 0 && row1.ComplinceVisible == true
                                    && (row6.StateID == row4.StateID || row6.ComplianceTypeId == 2)
                                    && row4.ID == CustomerBranchID
                                    select row).Distinct().FirstOrDefault();

                        if (data == null)
                        {
                            result = false;
                        }
                        else
                        {
                            var data1 = (from row in entities.ComplianceScheduleOns
                                         join row1 in entities.ComplianceTransactions
                                         on row.ComplianceInstanceID equals row1.ComplianceInstanceId
                                         where row.ParentEventD == ParentEventID && row.IntermediateEventID == 0
                                         && row.SubEventID == 0 && row.EventScheduledOnID == EventScheduledOnId
                                         && row1.StatusId != 1
                                         select row).FirstOrDefault();

                            if (data1 == null)
                            {
                                result = EventManagement.CheckParentEventDateAssigned(ParentEventID, EventScheduledOnId);

                                return result;
                            }
                            else
                            {
                                result = false;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                }
                return false;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }
        protected bool visibleTxtgvChildGridDate(int ParentEventID, int SubEventID, int EventScheduledOnId)
        {
            try
            {

                bool result = false;
                ComplianceDBEntities entities = new ComplianceDBEntities();
                int CustomerBranchID = Convert.ToInt32(Session["Rbranch"]);
                int EventClassificationID = Convert.ToInt32(Session["EventClassificationID"]);

                if (EventClassificationID == 1)
                {
                    var data = (from row in entities.EventCompAssignDays
                                join row1 in entities.Compliances on row.ComplianceID equals row1.ID
                                join row2 in entities.IndustryMappings on row1.ID equals row2.ComplianceId
                                join row3 in entities.CustomerBranchIndustryMappings on row2.IndustryID equals row3.IndustryID
                                join row4 in entities.CustomerBranches on row3.CustomerBranchID equals row4.ID
                                where row.ParentEventID == ParentEventID && row.IntermediateEventID == 0
                                && row.SubEventID == SubEventID && row1.ComplinceVisible == true
                                && row4.ID == CustomerBranchID
                                select row).Distinct().FirstOrDefault();

                    if (data == null)
                    {
                        result = false;
                    }
                    else
                    {
                        result = true;
                    }
                }
                else if (EventClassificationID == 2)
                {
                    var data = (from row in entities.EventCompAssignDays
                                join row1 in entities.Compliances on row.ComplianceID equals row1.ID
                                join row6 in entities.Acts on row1.ActID equals row6.ID
                                join row2 in entities.IndustryMappings on row1.ID equals row2.ComplianceId
                                join row3 in entities.CustomerBranchIndustryMappings on row2.IndustryID equals row3.IndustryID
                                join row4 in entities.CustomerBranches on row3.CustomerBranchID equals row4.ID
                                where row.ParentEventID == ParentEventID && row.IntermediateEventID == 0
                                && row.SubEventID == SubEventID && row1.ComplinceVisible == true
                                && (row6.StateID == row4.StateID || row6.ComplianceTypeId == 2)
                                && row4.ID == CustomerBranchID
                                select row).Distinct().FirstOrDefault();

                    if (data == null)
                    {
                        result = false;
                    }
                    else
                    {
                        result = true;
                    }

                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }
        protected bool visibleTxtgvIntermediateSubEventGridDate(int ParentEventID, int IntermediateEventID, int SubEventID, int EventScheduledOnId)
        {
            try
            {
                bool result = false;

                ComplianceDBEntities entities = new ComplianceDBEntities();
                int CustomerBranchID = Convert.ToInt32(Session["Rbranch"]);
                int EventClassificationID = Convert.ToInt32(Session["EventClassificationID"]);

                if (EventClassificationID == 1)
                {
                    var data = (from row in entities.EventCompAssignDays
                                join row1 in entities.Compliances on row.ComplianceID equals row1.ID
                                join row2 in entities.IndustryMappings on row1.ID equals row2.ComplianceId
                                join row3 in entities.CustomerBranchIndustryMappings on row2.IndustryID equals row3.IndustryID
                                join row4 in entities.CustomerBranches on row3.CustomerBranchID equals row4.ID
                                where row.ParentEventID == ParentEventID && row.IntermediateEventID == IntermediateEventID
                                && row.SubEventID == SubEventID && row1.ComplinceVisible == true
                                && row4.ID == CustomerBranchID
                                select row).Distinct().FirstOrDefault();

                    if (data == null)
                    {
                        result = false;
                    }
                    else
                    {
                        result = true;
                    }

                }
                else if (EventClassificationID == 2)
                {
                    var data = (from row in entities.EventCompAssignDays
                                join row1 in entities.Compliances on row.ComplianceID equals row1.ID
                                join row6 in entities.Acts on row1.ActID equals row6.ID
                                join row2 in entities.IndustryMappings on row1.ID equals row2.ComplianceId
                                join row3 in entities.CustomerBranchIndustryMappings on row2.IndustryID equals row3.IndustryID
                                join row4 in entities.CustomerBranches on row3.CustomerBranchID equals row4.ID
                                where row.ParentEventID == ParentEventID && row.IntermediateEventID == IntermediateEventID
                                && row.SubEventID == SubEventID && row1.ComplinceVisible == true
                                && (row6.StateID == row4.StateID || row6.ComplianceTypeId == 2)
                                && row4.ID == CustomerBranchID
                                select row).Distinct().FirstOrDefault();

                    if (data == null)
                    {
                        result = false;
                    }
                    else
                    {
                        result = true;
                    }

                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }
        protected bool visibleIntermediateSubEventAddButton(int ParentEventID, int IntermediateEventID, int SubEventID, int EventScheduledOnId)
        {
            try
            {
                bool result = false;

                ComplianceDBEntities entities = new ComplianceDBEntities();
                int CustomerBranchID = Convert.ToInt32(Session["Rbranch"]);

                int EventClassificationID = Convert.ToInt32(Session["EventClassificationID"]);

                if (EventClassificationID == 1)
                {
                    var data = (from row in entities.EventCompAssignDays
                                join row1 in entities.Compliances on row.ComplianceID equals row1.ID
                                join row2 in entities.IndustryMappings on row1.ID equals row2.ComplianceId
                                join row3 in entities.CustomerBranchIndustryMappings on row2.IndustryID equals row3.IndustryID
                                join row4 in entities.CustomerBranches on row3.CustomerBranchID equals row4.ID
                                where row.ParentEventID == ParentEventID && row.IntermediateEventID == IntermediateEventID
                                && row.SubEventID == SubEventID && row1.ComplinceVisible == true
                                && row4.ID == CustomerBranchID
                                select row).Distinct().FirstOrDefault();

                    if (data == null)
                    {
                        result = false;
                    }
                    else
                    {
                        var data1 = (from row in entities.ComplianceScheduleOns
                                     join row1 in entities.ComplianceTransactions
                                     on row.ID equals row1.ComplianceScheduleOnID
                                     where row.ParentEventD == ParentEventID && row.IntermediateEventID == IntermediateEventID
                                     && row.SubEventID == SubEventID && row.EventScheduledOnID == EventScheduledOnId
                                     && row1.StatusId != 1
                                     select row).FirstOrDefault();

                        if (data1 == null)
                        {
                            result = EventManagement.CheckIntermediateSubEventDateAssigned(ParentEventID, IntermediateEventID, SubEventID, EventScheduledOnId);
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
                else if (EventClassificationID == 2)
                {
                    var data = (from row in entities.EventCompAssignDays
                                join row1 in entities.Compliances on row.ComplianceID equals row1.ID
                                join row6 in entities.Acts on row1.ActID equals row6.ID
                                join row2 in entities.IndustryMappings on row1.ID equals row2.ComplianceId
                                join row3 in entities.CustomerBranchIndustryMappings on row2.IndustryID equals row3.IndustryID
                                join row4 in entities.CustomerBranches on row3.CustomerBranchID equals row4.ID
                                where row.ParentEventID == ParentEventID && row.IntermediateEventID == IntermediateEventID
                                && row.SubEventID == SubEventID && row1.ComplinceVisible == true
                                && (row6.StateID == row4.StateID || row6.ComplianceTypeId == 2)
                                && row4.ID == CustomerBranchID
                                select row).Distinct().FirstOrDefault();

                    if (data == null)
                    {
                        result = false;
                    }
                    else
                    {
                        var data1 = (from row in entities.ComplianceScheduleOns
                                     join row1 in entities.ComplianceTransactions
                                     on row.ID equals row1.ComplianceScheduleOnID
                                     where row.ParentEventD == ParentEventID && row.IntermediateEventID == IntermediateEventID
                                     && row.SubEventID == SubEventID && row.EventScheduledOnID == EventScheduledOnId
                                     && row1.StatusId != 1
                                     select row).FirstOrDefault();

                        if (data1 == null)
                        {
                            result = EventManagement.CheckIntermediateSubEventDateAssigned(ParentEventID, IntermediateEventID, SubEventID, EventScheduledOnId);
                        }
                        else
                        {
                            result = false;
                        }
                    }

                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }
        protected bool visibleIntermediateSubEventUpdateButton(int ParentEventID, int IntermediateEventID, int SubEventID, int EventScheduledOnId)
        {
            try
            {
                bool result = false;
                ComplianceDBEntities entities = new ComplianceDBEntities();
                int CustomerBranchID = Convert.ToInt32(Session["Rbranch"]);
                int EventClassificationID = Convert.ToInt32(Session["EventClassificationID"]);

                if (EventClassificationID == 1)
                {
                    var data = (from row in entities.EventCompAssignDays
                                join row1 in entities.Compliances on row.ComplianceID equals row1.ID
                                join row2 in entities.IndustryMappings on row1.ID equals row2.ComplianceId
                                join row3 in entities.CustomerBranchIndustryMappings on row2.IndustryID equals row3.IndustryID
                                join row4 in entities.CustomerBranches on row3.CustomerBranchID equals row4.ID
                                where row.ParentEventID == ParentEventID && row.IntermediateEventID == IntermediateEventID
                                && row.SubEventID == SubEventID && row1.ComplinceVisible == true
                                && row4.ID == CustomerBranchID
                                select row).Distinct().FirstOrDefault();

                    if (data == null)
                    {
                        result = false;
                    }
                    else
                    {
                        var data1 = (from row in entities.ComplianceScheduleOns
                                     join row1 in entities.ComplianceTransactions
                                     on row.ID equals row1.ComplianceScheduleOnID
                                     where row.ParentEventD == ParentEventID && row.IntermediateEventID == IntermediateEventID
                                     && row.SubEventID == SubEventID && row.EventScheduledOnID == EventScheduledOnId
                                     && row1.StatusId != 1
                                     select row).FirstOrDefault();

                        if (data1 == null)
                        {
                            result = EventManagement.CheckIntermediateSubEventDateAssigned(ParentEventID, IntermediateEventID, SubEventID, EventScheduledOnId);

                            if (result == true)
                            {
                                result = false;
                            }
                            else
                            {
                                result = true;
                            }
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
                else if (EventClassificationID == 2)
                {
                    var data = (from row in entities.EventCompAssignDays
                                join row1 in entities.Compliances on row.ComplianceID equals row1.ID
                                join row6 in entities.Acts on row1.ActID equals row6.ID
                                join row2 in entities.IndustryMappings on row1.ID equals row2.ComplianceId
                                join row3 in entities.CustomerBranchIndustryMappings on row2.IndustryID equals row3.IndustryID
                                join row4 in entities.CustomerBranches on row3.CustomerBranchID equals row4.ID
                                where row.ParentEventID == ParentEventID && row.IntermediateEventID == IntermediateEventID
                                && row.SubEventID == SubEventID && row1.ComplinceVisible == true
                                 && (row6.StateID == row4.StateID || row6.ComplianceTypeId == 2)
                                && row4.ID == CustomerBranchID
                                select row).Distinct().FirstOrDefault();

                    if (data == null)
                    {
                        result = false;
                    }
                    else
                    {
                        var data1 = (from row in entities.ComplianceScheduleOns
                                     join row1 in entities.ComplianceTransactions
                                     on row.ID equals row1.ComplianceScheduleOnID
                                     where row.ParentEventD == ParentEventID && row.IntermediateEventID == IntermediateEventID
                                     && row.SubEventID == SubEventID && row.EventScheduledOnID == EventScheduledOnId
                                     && row1.StatusId != 1
                                     select row).FirstOrDefault();

                        if (data1 == null)
                        {
                            result = EventManagement.CheckIntermediateSubEventDateAssigned(ParentEventID, IntermediateEventID, SubEventID, EventScheduledOnId);

                            if (result == true)
                            {
                                result = false;
                            }
                            else
                            {
                                result = true;
                            }
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }
        protected bool visibleSubEventAddButton(int ParentEventID, int SubEventID, int EventScheduledOnId)
        {
            try
            {
                bool result = false;
                ComplianceDBEntities entities = new ComplianceDBEntities();
                int CustomerBranchID = Convert.ToInt32(Session["Rbranch"]);
                int EventClassificationID = Convert.ToInt32(Session["EventClassificationID"]);

                if (EventClassificationID == 1)
                {
                    var data = (from row in entities.EventCompAssignDays
                                join row1 in entities.Compliances on row.ComplianceID equals row1.ID
                                join row2 in entities.IndustryMappings on row1.ID equals row2.ComplianceId
                                join row3 in entities.CustomerBranchIndustryMappings on row2.IndustryID equals row3.IndustryID
                                join row4 in entities.CustomerBranches on row3.CustomerBranchID equals row4.ID
                                where row.ParentEventID == ParentEventID && row.IntermediateEventID == 0
                                && row.SubEventID == SubEventID && row1.ComplinceVisible == true
                                && row4.ID == CustomerBranchID
                                select row).Distinct().FirstOrDefault();

                    if (data == null)
                    {
                        result = false;
                    }
                    else
                    {

                        var data1 = (from row in entities.ComplianceScheduleOns
                                     join row1 in entities.ComplianceTransactions
                                     on row.ComplianceInstanceID equals row1.ComplianceInstanceId
                                     where row.ParentEventD == ParentEventID && row.IntermediateEventID == 0
                                     && row.SubEventID == SubEventID && row.EventScheduledOnID == EventScheduledOnId
                                     && row1.StatusId != 1
                                     select row).FirstOrDefault();

                        if (data1 == null)
                        {
                            result = EventManagement.CheckSubEventDateAssigned(ParentEventID, SubEventID, EventScheduledOnId);
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
                else if (EventClassificationID == 2)
                {
                    var data = (from row in entities.EventCompAssignDays
                                join row1 in entities.Compliances on row.ComplianceID equals row1.ID
                                join row6 in entities.Acts on row1.ActID equals row6.ID
                                join row2 in entities.IndustryMappings on row1.ID equals row2.ComplianceId
                                join row3 in entities.CustomerBranchIndustryMappings on row2.IndustryID equals row3.IndustryID
                                join row4 in entities.CustomerBranches on row3.CustomerBranchID equals row4.ID
                                where row.ParentEventID == ParentEventID && row.IntermediateEventID == 0
                                && row.SubEventID == SubEventID && row1.ComplinceVisible == true
                                && (row6.StateID == row4.StateID || row6.ComplianceTypeId == 2)
                                && row4.ID == CustomerBranchID
                                select row).Distinct().FirstOrDefault();

                    if (data == null)
                    {
                        result = false;
                    }
                    else
                    {

                        var data1 = (from row in entities.ComplianceScheduleOns
                                     join row1 in entities.ComplianceTransactions
                                     on row.ComplianceInstanceID equals row1.ComplianceInstanceId
                                     where row.ParentEventD == ParentEventID && row.IntermediateEventID == 0
                                     && row.SubEventID == SubEventID && row.EventScheduledOnID == EventScheduledOnId
                                     && row1.StatusId != 1
                                     select row).FirstOrDefault();

                        if (data1 == null)
                        {
                            result = EventManagement.CheckSubEventDateAssigned(ParentEventID, SubEventID, EventScheduledOnId);
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }
        protected bool enableSubEventUpdateButton(int ParentEventID, int SubEventID, int EventScheduledOnId)
        {
            try
            {
                bool result = false;
                ComplianceDBEntities entities = new ComplianceDBEntities();

                var data = (from row in entities.ComplianceScheduleOns
                            join row1 in entities.ComplianceTransactions
                            on row.ID equals row1.ComplianceScheduleOnID
                            where row.ParentEventD == ParentEventID && row.IntermediateEventID == 0
                            && row.SubEventID == SubEventID && row.EventScheduledOnID == EventScheduledOnId
                            && row1.StatusId != 1
                            select row).FirstOrDefault();

                if (data == null)
                {
                    result = EventManagement.CheckSubEventDateAssigned(ParentEventID, SubEventID, EventScheduledOnId);
                    if (result == false)
                    {
                        result = true;
                    }
                }
                else
                {
                    result = false;
                }

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }
        protected bool CheckIntermediateEventTransactionComplete(int ParentEventID, int IntermediateID, int SubEventID, int EventScheduledOnId)
        {
            try
            {
                bool result = false;
                ComplianceDBEntities entities = new ComplianceDBEntities();
                int CustomerBranchID = Convert.ToInt32(Session["Rbranch"]);
                int EventClassificationID = Convert.ToInt32(Session["EventClassificationID"]);

                if (EventClassificationID == 1)
                {
                    var ComplainceTransCompleteList = (from row in entities.ComplianceScheduleOns
                                                       join row1 in entities.ComplianceTransactions
                                                       on row.ID equals row1.ComplianceScheduleOnID
                                                       where row.ParentEventD == ParentEventID && row.IntermediateEventID == IntermediateID
                                                       && row.SubEventID == SubEventID && row.EventScheduledOnID == EventScheduledOnId
                                                       && (row1.StatusId == 4 || row1.StatusId == 5)
                                                       select row).ToList();

                    var SubeventComplainceList = (from row in entities.EventCompAssignDays
                                                  join row1 in entities.Compliances on row.ComplianceID equals row1.ID
                                                  join row2 in entities.IndustryMappings on row1.ID equals row2.ComplianceId
                                                  join row3 in entities.CustomerBranchIndustryMappings on row2.IndustryID equals row3.IndustryID
                                                  join row4 in entities.CustomerBranches on row3.CustomerBranchID equals row4.ID
                                                  where row.ParentEventID == ParentEventID && row.IntermediateEventID == IntermediateID
                                                  && row.SubEventID == SubEventID && row1.ComplinceVisible == true
                                                  && row4.ID == CustomerBranchID
                                                  select row).Distinct().ToList();


                    if (ComplainceTransCompleteList.Count >= SubeventComplainceList.Count)
                    {
                        result = false;
                    }
                    else
                    {
                        result = true;
                    }
                }
                else if (EventClassificationID == 2)
                {
                    var ComplainceTransCompleteList = (from row in entities.ComplianceScheduleOns
                                                       join row1 in entities.ComplianceTransactions
                                                       on row.ID equals row1.ComplianceScheduleOnID
                                                       where row.ParentEventD == ParentEventID && row.IntermediateEventID == IntermediateID
                                                       && row.SubEventID == SubEventID && row.EventScheduledOnID == EventScheduledOnId
                                                       && (row1.StatusId == 4 || row1.StatusId == 5)
                                                       select row).ToList();

                    var SubeventComplainceList = (from row in entities.EventCompAssignDays
                                                  join row1 in entities.Compliances on row.ComplianceID equals row1.ID
                                                  join row6 in entities.Acts on row1.ActID equals row6.ID
                                                  join row2 in entities.IndustryMappings on row1.ID equals row2.ComplianceId
                                                  join row3 in entities.CustomerBranchIndustryMappings on row2.IndustryID equals row3.IndustryID
                                                  join row4 in entities.CustomerBranches on row3.CustomerBranchID equals row4.ID
                                                  where row.ParentEventID == ParentEventID && row.IntermediateEventID == IntermediateID
                                                  && row.SubEventID == SubEventID && row1.ComplinceVisible == true
                                                  && (row6.StateID == row4.StateID || row6.ComplianceTypeId == 2)
                                                  && row4.ID == CustomerBranchID
                                                  select row).Distinct().ToList();


                    if (ComplainceTransCompleteList.Count >= SubeventComplainceList.Count)
                    {
                        result = false;
                    }
                    else
                    {
                        result = true;
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }
        protected bool CheckSubEventTransactionComplete(int ParentEventID, int SubEventID, int EventScheduledOnId)
        {
            try
            {
                bool result = false;
                ComplianceDBEntities entities = new ComplianceDBEntities();
                int CustomerBranchID = Convert.ToInt32(Session["Rbranch"]);
                int EventClassificationID = Convert.ToInt32(Session["EventClassificationID"]);

                if (EventClassificationID == 1)
                {
                    var ComplainceTransCompleteList = (from row in entities.ComplianceScheduleOns
                                                       join row1 in entities.ComplianceTransactions
                                                       on row.ID equals row1.ComplianceScheduleOnID
                                                       where row.ParentEventD == ParentEventID && row.IntermediateEventID == 0
                                                       && row.SubEventID == SubEventID && row.EventScheduledOnID == EventScheduledOnId
                                                       && (row1.StatusId == 4 || row1.StatusId == 5)
                                                       select row).ToList();

                    var SubeventComplainceList = (from row in entities.EventCompAssignDays
                                                  join row1 in entities.Compliances on row.ComplianceID equals row1.ID
                                                  join row2 in entities.IndustryMappings on row1.ID equals row2.ComplianceId
                                                  join row3 in entities.CustomerBranchIndustryMappings on row2.IndustryID equals row3.IndustryID
                                                  join row4 in entities.CustomerBranches on row3.CustomerBranchID equals row4.ID
                                                  where row.ParentEventID == ParentEventID && row.IntermediateEventID == 0
                                                  && row.SubEventID == SubEventID && row1.ComplinceVisible == true
                                                  && row4.ID == CustomerBranchID
                                                  select row).Distinct().ToList();


                    if (ComplainceTransCompleteList.Count >= SubeventComplainceList.Count)
                    {
                        result = false;
                    }
                    else
                    {
                        result = true;
                    }
                }
                else if (EventClassificationID == 2)
                {
                    var ComplainceTransCompleteList = (from row in entities.ComplianceScheduleOns
                                                       join row1 in entities.ComplianceTransactions
                                                       on row.ID equals row1.ComplianceScheduleOnID
                                                       where row.ParentEventD == ParentEventID && row.IntermediateEventID == 0
                                                       && row.SubEventID == SubEventID && row.EventScheduledOnID == EventScheduledOnId
                                                       && (row1.StatusId == 4 || row1.StatusId == 5)
                                                       select row).ToList();

                    var SubeventComplainceList = (from row in entities.EventCompAssignDays
                                                  join row1 in entities.Compliances on row.ComplianceID equals row1.ID
                                                  join row6 in entities.Acts on row1.ActID equals row6.ID
                                                  join row2 in entities.IndustryMappings on row1.ID equals row2.ComplianceId
                                                  join row3 in entities.CustomerBranchIndustryMappings on row2.IndustryID equals row3.IndustryID
                                                  join row4 in entities.CustomerBranches on row3.CustomerBranchID equals row4.ID
                                                  where row.ParentEventID == ParentEventID && row.IntermediateEventID == 0
                                                  && row.SubEventID == SubEventID && row1.ComplinceVisible == true
                                                  && (row6.StateID == row4.StateID || row6.ComplianceTypeId == 2)
                                                  && row4.ID == CustomerBranchID
                                                  select row).Distinct().ToList();


                    if (ComplainceTransCompleteList.Count >= SubeventComplainceList.Count)
                    {
                        result = false;
                    }
                    else
                    {
                        result = true;
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }
        protected bool visibleSubEventUpdateButton(int ParentEventID, int SubEventID, int EventScheduledOnId)
        {
            try
            {
                bool result = false;
                ComplianceDBEntities entities = new ComplianceDBEntities();
                int CustomerBranchID = Convert.ToInt32(Session["Rbranch"]);
                int EventClassificationID = Convert.ToInt32(Session["EventClassificationID"]);

                if (EventClassificationID == 1)
                {
                    var data = (from row in entities.EventCompAssignDays
                                join row1 in entities.Compliances on row.ComplianceID equals row1.ID
                                join row2 in entities.IndustryMappings on row1.ID equals row2.ComplianceId
                                join row3 in entities.CustomerBranchIndustryMappings on row2.IndustryID equals row3.IndustryID
                                join row4 in entities.CustomerBranches on row3.CustomerBranchID equals row4.ID
                                where row.ParentEventID == ParentEventID && row.IntermediateEventID == 0
                                && row.SubEventID == SubEventID && row1.ComplinceVisible == true
                                && row4.ID == CustomerBranchID
                                select row).Distinct().FirstOrDefault();

                    if (data == null)
                    {
                        result = false;
                    }
                    else
                    {

                        var data1 = (from row in entities.ComplianceScheduleOns
                                     join row1 in entities.ComplianceTransactions
                                     on row.ID equals row1.ComplianceScheduleOnID
                                     where row.ParentEventD == ParentEventID && row.IntermediateEventID == 0
                                     && row.SubEventID == SubEventID && row.EventScheduledOnID == EventScheduledOnId
                                     && row1.StatusId != 1
                                     select row).FirstOrDefault();

                        if (data1 == null)
                        {
                            result = EventManagement.CheckSubEventDateAssigned(ParentEventID, SubEventID, EventScheduledOnId);

                            if (result == true)
                            {
                                result = false;
                            }
                            else
                            {
                                result = true;
                            }
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
                else if (EventClassificationID == 2)
                {
                    var data = (from row in entities.EventCompAssignDays
                                join row1 in entities.Compliances on row.ComplianceID equals row1.ID
                                join row6 in entities.Acts on row1.ActID equals row6.ID
                                join row2 in entities.IndustryMappings on row1.ID equals row2.ComplianceId
                                join row3 in entities.CustomerBranchIndustryMappings on row2.IndustryID equals row3.IndustryID
                                join row4 in entities.CustomerBranches on row3.CustomerBranchID equals row4.ID
                                where row.ParentEventID == ParentEventID && row.IntermediateEventID == 0
                                && row.SubEventID == SubEventID && row1.ComplinceVisible == true
                                && (row6.StateID == row4.StateID || row6.ComplianceTypeId == 2)
                                && row4.ID == CustomerBranchID
                                select row).Distinct().FirstOrDefault();

                    if (data == null)
                    {
                        result = false;
                    }
                    else
                    {

                        var data1 = (from row in entities.ComplianceScheduleOns
                                     join row1 in entities.ComplianceTransactions
                                     on row.ID equals row1.ComplianceScheduleOnID
                                     where row.ParentEventD == ParentEventID && row.IntermediateEventID == 0
                                     && row.SubEventID == SubEventID && row.EventScheduledOnID == EventScheduledOnId
                                     && row1.StatusId != 1
                                     select row).FirstOrDefault();

                        if (data1 == null)
                        {
                            result = EventManagement.CheckSubEventDateAssigned(ParentEventID, SubEventID, EventScheduledOnId);

                            if (result == true)
                            {
                                result = false;
                            }
                            else
                            {
                                result = true;
                            }
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }
    }
}