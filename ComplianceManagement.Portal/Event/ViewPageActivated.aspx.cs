﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Event
{
    public partial class ViewPageActivated : System.Web.UI.Page
    {
        static int Type = 0;
        static int glParentID = 0;
        static int glsubEventID = 0;
        static int glIntermediateID = 0;
        static int PageCount = 0;
        protected static int EventScheduledOnID;
        protected static int CustomerId;
        protected static int EventId;
        protected static int ParentID;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ParentID = Convert.ToInt32(Request.QueryString["ID"]);
                EventScheduledOnID = Convert.ToInt32(Request.QueryString["EventScheduledOnID"]);
                Type = Convert.ToInt32(Request.QueryString["Type"]);
                CustomerId = Convert.ToInt32(Request.QueryString["CustomerBranchID"]);
                EventId = Convert.ToInt32(Request.QueryString["EventClassificationID"]);
                string description = Convert.ToString(Request.QueryString["EventDescription"]);
                txtDescription.Text = description;
                BindParentEventData(ParentID);
                //BindInternalCompliance(CustomerId);
                DateTime date = DateTime.Now;
              
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                upOptionalCompliances.Update();

            }
        }
        private void BindParentEventData(int ParentEventID)
        {
            try
            {
                ComplianceDBEntities entities = new ComplianceDBEntities();
                int Type = Convert.ToInt32(Request.QueryString["Type"]);
                int EventScheduledOnID = Convert.ToInt32(Request.QueryString["EventScheduledOnID"]);

                //Type = EventManagement.GetCompanyType(Convert.ToInt32(branch));
                //var ParentEvent = entities.SP_GetParentActivatedEvent(ParentEventID, Convert.ToInt32(Session["EventScheduledOnID"]), Type).ToList();
                var ParentEvent = entities.SP_GetParentActivatedEvent(ParentEventID, EventScheduledOnID, Type).ToList();
                gvParentGrid.DataSource = ParentEvent;
                gvParentGrid.DataBind();
                int CustomerbranchID = Convert.ToInt32(Request.QueryString["CustomerBranchID"]);
                BindInternalCompliance(CustomerbranchID);
                upOptionalCompliances.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
       
        protected void gvParentGrid_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    int ParentEventID = Convert.ToInt32(gvParentGrid.Rows[e.RowIndex].Cells[1].Text);

                    // int EventScheduledOnID = Convert.ToInt32(Session["EventScheduledOnID"]);
                    int EventScheduledOnID = Convert.ToInt32(Request.QueryString["EventScheduledOnID"]);

                    ViewState["interIndex"] = 0;
                    ViewState["RowIndex"] = e.RowIndex;
                    ViewState["ParentEventID"] = ParentEventID;
                    ViewState["IntermediateEventID"] = 0;
                    ViewState["SubEventID"] = 0;
                    ViewState["EventScheduledOnID"] = EventScheduledOnID;

                    //Check shorter notice Compliance
                    List<long> ShorterNoticeComplianceList = Business.ComplianceManagement.GetShorterNotice();
                    Boolean shorterFlag = false;
                    List<long> lstShortCompliance = new List<long>();
                    foreach (GridViewRow Eventrow in gvParentGrid.Rows)
                    {
                        int ComplinaceID = Convert.ToInt32(Eventrow.Cells[0].Text);
                        bool ExistShorterNotice = ShorterNoticeComplianceList.Contains(ComplinaceID);

                        if (ExistShorterNotice == true)
                        {
                            shorterFlag = true;
                            lstShortCompliance.Add(ComplinaceID);
                            //break;
                        }
                    }

                    if (shorterFlag == true)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "divShortNotice", "$(\"#divShortNotice\").dialog('open')", true);
                    }
                    else
                    {

                        foreach (GridViewRow Eventrow in gvParentGrid.Rows)
                        {
                            var textbox2 = Eventrow.FindControl("txtgvParentGridDate") as TextBox;
                            DateTime Date = DateTime.ParseExact(textbox2.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            // int ParentEventID = Convert.ToInt32(Eventrow.Cells[1].Text);
                            EventAssignDate eventAssignDate = new EventAssignDate()
                            {
                                ParentEventID = ParentEventID,
                                EventScheduleOnID = EventScheduledOnID,
                                IntermediateEventID = 0,
                                SubEventID = 0,
                                Date = Date,
                                IsActive = true,
                                CreatedDate = DateTime.Now,
                                CreatedBy = Convert.ToInt32(Session["userID"]),
                                Type = Type,
                            };
                            Business.EventManagement.UpdateEventAssignDates(eventAssignDate);

                            GridView gvEvenToCompliance = Eventrow.FindControl("gvParentToComplianceGrid") as GridView;
                            // Direct Compliance 
                            foreach (GridViewRow EvenToCompliancerow in gvEvenToCompliance.Rows)
                            {
                                int ComplinaceID = Convert.ToInt32(EvenToCompliancerow.Cells[1].Text);

                                Boolean VisibleFlag = Business.EventManagement.CheckComplianceVisible(ComplinaceID);

                                if (VisibleFlag == true)  //If complaince visible true then Schudule generated
                                {
                                    string days = Convert.ToString(EvenToCompliancerow.Cells[3].Text);
                                    int ComplianceInstanceID = Convert.ToInt32(EventManagement.GetComplianceInstance(ComplinaceID, Convert.ToInt32(Request.QueryString["CustomerBranchID"])).ID);
                                    Boolean IsComplianceChecklistStatutory = EventManagement.CheckComplianceStatutoryChecklist(ComplinaceID);

                                    if (IsComplianceChecklistStatutory == true)
                                    {
                                        //Change Generate flag Schedule change
                                        EventManagement.UpdateComplianceInstanceScheduleFlag(ComplinaceID, ComplianceInstanceID);
                                    }
                                    Boolean FlgCheck = false;
                                    FlgCheck = EventManagement.CheckEventComplianceAssigned(ParentEventID, 0, 0, Type, ComplinaceID, EventScheduledOnID, ComplianceInstanceID);
                                    if (FlgCheck == false)
                                    {
                                        EventManagement.GenerateEventComplianceScheduele(ParentEventID, 0, 0, EventScheduledOnID, Date, ComplinaceID, ComplianceInstanceID, days);
                                    }
                                    else
                                    {
                                        var compdetails = EventManagement.GetCompliance(ComplinaceID);
                                        int IncludeDays = 0;
                                        if ((compdetails.ComplianceType == 0 && (compdetails.IsFrequencyBased == false || compdetails.IsFrequencyBased == null)) || compdetails.ComplianceType == 1)
                                        {
                                            int Days = Convert.ToInt32(days);
                                            if (Days == 0)
                                            {
                                                IncludeDays = 0;
                                            }
                                            else
                                            {
                                                if (Days > 0)
                                                {
                                                    IncludeDays = Days - 1;
                                                }
                                                else
                                                {
                                                    IncludeDays = Days + 1;
                                                }
                                            }
                                            DateTime UpdatedDate = Date.AddDays(IncludeDays);
                                            EventManagement.UpdateReminderSheduleOnDates(EventScheduledOnID, ComplinaceID, UpdatedDate, ComplianceInstanceID, Date);
                                            EventManagement.UpdateComplianceSheduleOnDates(ParentEventID, 0, 0, EventScheduledOnID, ComplinaceID, ComplianceInstanceID, UpdatedDate);
                                        }
                                        else
                                        {
                                            EventManagement.RemoveComplianceScheduleEventDateUpdation(ComplinaceID, ParentEventID, 0, 0, ComplianceInstanceID, EventScheduledOnID);
                                            EventManagement.GenerateEventComplianceScheduele(ParentEventID, 0, 0, EventScheduledOnID, Date, ComplinaceID, ComplianceInstanceID, days);
                                        }
                                    }
                                }
                            }
                        }
                        //    BindParentEventData(Convert.ToInt32(ParentEventID),Convert.ToInt32(EventScheduledOnID),Convert.ToInt32(Type));
                        BindParentEventData(Convert.ToInt32(ParentEventID));
                        
                        //int ParentID = Convert.ToInt32(Request.QueryString["ID"]);
                        //BindParentEventData(ParentID);
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Saved Successfully";
                    }
                }
                DateTime date = DateTime.Now;
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;
            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }
        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }


        protected void gvParentGrid_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DateTime date = DateTime.Now;
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);

                int ParentEventID = Convert.ToInt32(e.Row.Cells[1].Text);
                glParentID = ParentEventID;
                ViewState["ParentEventID"] = ParentEventID;

                ComplianceDBEntities entities = new ComplianceDBEntities();
                GridView gv1 = (GridView)e.Row.FindControl("gvParentToComplianceGrid");
                // int CustomerBranchID = Convert.ToInt32(Session["Rbranch"]);
                //int CustomerBranchID = CustomerBranchID;
                int CustomerBranchID = Convert.ToInt32(Request.QueryString["CustomerBranchID"]);
                // string Type = Convert.ToString(gv1.DataKeys[e.Row.RowIndex]["Type"]);
                int Type = Convert.ToInt32(Request.QueryString["Type"]);
                int EventScheduledOnID = Convert.ToInt32(Request.QueryString["EventScheduledOnID"]);
                var compliance = entities.SP_GetParentEventToComplianceAssignWithShortNoticeDays(ParentEventID, EventScheduledOnID, Type, CustomerBranchID).ToList();
                gv1.DataSource = compliance;
                gv1.DataBind();

                GridView gv = (GridView)e.Row.FindControl("gvChildGrid");
                string type = Convert.ToString(gvParentGrid.DataKeys[e.Row.RowIndex]["Type"]);

                var SubEvent = entities.SP_GetActivatedEventData(ParentEventID, EventScheduledOnID, Type).ToList();
                gv.DataSource = SubEvent;
                gv.DataBind();

                //  upOptionalCompliances.Update();
            }

                
        }
        protected bool visibleParentEventUpdateButton(int ParentEventID, int EventScheduledOnId)
        {
            try
            {
                bool result = false;
                 EventScheduledOnId = Convert.ToInt32(Request.QueryString["EventScheduledOnID"]);
                ComplianceDBEntities entities = new ComplianceDBEntities();
                var data1 = (from row in entities.EventCompAssignDays
                             join row1 in entities.EventAssignDates
                             on row.ParentEventID equals row1.ParentEventID
                             where row.ParentEventID == ParentEventID && row.IntermediateEventID == 0
                             && row.SubEventID == 0 && row1.EventScheduleOnID == EventScheduledOnId
                             select row).FirstOrDefault();
                if (data1 == null)
                {
                    result = false;
                }
                else
                {
                    var data = (from row in entities.ComplianceScheduleOns
                                join row1 in entities.ComplianceTransactions
                                on row.ID equals row1.ComplianceScheduleOnID
                                where row.ParentEventD == ParentEventID && row.IntermediateEventID == 0
                                && row.SubEventID == 0 && row.EventScheduledOnID == EventScheduledOnId
                                && row1.StatusId != 1
                                 && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                select row).FirstOrDefault();

                    if (data == null)
                    {
                        result = EventManagement.CheckParentEventDateAssigned(ParentEventID, EventScheduledOnId);

                        if (result == false)
                        {
                            result = true;
                        }
                    }
                    else
                    {
                        result = false;
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }
        protected bool visibleParentEventAddButton(int ParentEventID, int EventScheduledOnID)
        {
            try
            {
                try
                {
                    bool result = false;
                    ComplianceDBEntities entities = new ComplianceDBEntities();
                    //int CustomerBranchID = Convert.ToInt32(Session["Rbranch"]);
                    //int EventClassificationID = Convert.ToInt32(Session["EventClassificationID"]);
                    int CustomerBranchID = Convert.ToInt32(Request.QueryString["CustomerBranchID"]);
                    int EventClassificationID = Convert.ToInt32(Request.QueryString["EventClassificationID"]);
                    if (EventClassificationID == 1)
                    {
                        var data = (from row in entities.EventCompAssignDays
                                    join row1 in entities.Compliances on row.ComplianceID equals row1.ID
                                    join row2 in entities.IndustryMappings on row1.ID equals row2.ComplianceId
                                    join row3 in entities.CustomerBranchIndustryMappings on row2.IndustryID equals row3.IndustryID
                                    join row4 in entities.CustomerBranches on row3.CustomerBranchID equals row4.ID
                                    where row.ParentEventID == ParentEventID && row.IntermediateEventID == 0
                                    && row.SubEventID == 0 && row1.ComplinceVisible == true
                                    && row4.ID == CustomerBranchID
                                    select row).Distinct().FirstOrDefault();

                        if (data == null)
                        {
                            result = false;
                        }
                        else
                        {
                            var data1 = (from row in entities.ComplianceScheduleOns
                                         join row1 in entities.ComplianceTransactions
                                         on row.ComplianceInstanceID equals row1.ComplianceInstanceId
                                         where row.ParentEventD == ParentEventID && row.IntermediateEventID == 0
                                         && row.SubEventID == 0 && row.EventScheduledOnID == EventScheduledOnID
                                         && row1.StatusId != 1
                                          && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         select row).FirstOrDefault();

                            if (data1 == null)
                            {
                                result = EventManagement.CheckParentEventDateAssigned(ParentEventID, EventScheduledOnID);

                                return result;
                            }
                            else
                            {
                                result = false;
                            }
                        }
                    }
                    else if (EventClassificationID == 2)
                    {
                        var data = (from row in entities.EventCompAssignDays
                                    join row1 in entities.Compliances on row.ComplianceID equals row1.ID
                                    join row6 in entities.Acts on row1.ActID equals row6.ID
                                    join row2 in entities.IndustryMappings on row1.ID equals row2.ComplianceId
                                    join row3 in entities.CustomerBranchIndustryMappings on row2.IndustryID equals row3.IndustryID
                                    join row4 in entities.CustomerBranches on row3.CustomerBranchID equals row4.ID
                                    where row.ParentEventID == ParentEventID && row.IntermediateEventID == 0
                                    && row.SubEventID == 0 && row1.ComplinceVisible == true
                                    && (row6.StateID == row4.StateID || row6.ComplianceTypeId == 2)
                                    && row4.ID == CustomerBranchID
                                    select row).Distinct().FirstOrDefault();

                        if (data == null)
                        {
                            result = false;
                        }
                        else
                        {
                            var data1 = (from row in entities.ComplianceScheduleOns
                                         join row1 in entities.ComplianceTransactions
                                         on row.ComplianceInstanceID equals row1.ComplianceInstanceId
                                         where row.ParentEventD == ParentEventID && row.IntermediateEventID == 0
                                         && row.SubEventID == 0 && row.EventScheduledOnID == EventScheduledOnID
                                         && row1.StatusId != 1
                                          && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         select row).FirstOrDefault();

                            if (data1 == null)
                            {
                                result = EventManagement.CheckParentEventDateAssigned(ParentEventID, EventScheduledOnID);

                                return result;
                            }
                            else
                            {
                                result = false;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                }
                return false;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }
        protected void gvParentToComplianceGrid_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int CompliaceID = Convert.ToInt32(e.Row.Cells[1].Text);

                string type = Business.EventManagement.CheckEventComplianceType(CompliaceID);

                Label lblParentStatus = (e.Row.FindControl("lblParentStatus") as Label);

                if (type == "Informational")
                {
                    e.Row.ForeColor = System.Drawing.Color.Chocolate;
                    lblParentStatus.Visible = false;
                }
                else if (type == "Actionable")
                {
                    e.Row.ForeColor = System.Drawing.Color.SlateBlue;
                    lblParentStatus.Visible = true;

                    Boolean transcomplete = CheckCompleteTransactionComplete(glParentID, 0, 0, CompliaceID, Convert.ToInt32(Request.QueryString["EventScheduledOnID"]));
                    if (transcomplete == true)
                    {
                        lblParentStatus.Visible = true;
                    }
                    else
                    {
                        lblParentStatus.Visible = false;
                    }
                }
                else
                {
                    Boolean transcomplete = CheckCompleteTransactionComplete(glParentID, 0, 0, CompliaceID, Convert.ToInt32(Request.QueryString["EventScheduledOnID"]));
                    if (transcomplete == true)
                    {
                        lblParentStatus.Visible = true;
                    }
                    else
                    {
                        lblParentStatus.Visible = false;
                    }
                }

            }
        }
        protected bool CheckCompleteTransactionComplete(int ParentEventID, int IntermediateEventID, int SubEventID, int ComplianceID, int EventScheduledOnId)
        {
            try
            {
                bool result = false;
                ComplianceDBEntities entities = new ComplianceDBEntities();
                //  int CustomerBranchID = Convert.ToInt32(Session["Rbranch"]);
                int CustomerBranchID = Convert.ToInt32(Request.QueryString["CustomerBranchID"]);

                var compliance = ComplianceManagement.Business.ComplianceManagement.GetCompliance(ComplianceID);

                string type = "";
                if (compliance.ComplianceType == 0 && (compliance.IsFrequencyBased == false || compliance.IsFrequencyBased == null))
                {
                    type = "NormalEventBased";
                }
                else if (compliance.ComplianceType == 0 && compliance.IsFrequencyBased == true)
                {
                    type = "FrequencyBased";
                }
                if (compliance.ComplianceType == 1)
                {
                    type = "Checklist";
                }
                else if (compliance.ComplianceType == 2)
                {
                    type = "TimeBased";
                }


                System.Data.Entity.Core.Objects.ObjectParameter returnFlag = new ObjectParameter("returnFlag", typeof(bool));
                entities.SP_CheckComplianceTransactionComplete(ParentEventID, IntermediateEventID, SubEventID, EventScheduledOnId, ComplianceID, type, CustomerBranchID, returnFlag);
                result = Convert.ToBoolean(returnFlag.Value);


                //    var ComplainceTransCompleteList = (from row in entities.ComplianceScheduleOns
                //                                   join row1 in entities.ComplianceTransactions
                //                                   on row.ID equals row1.ComplianceScheduleOnID
                //                                   join row2 in entities.ComplianceInstances
                //                                   on row1.ComplianceInstanceId equals row2.ID
                //                                   where row.ParentEventD == ParentEventID && row.IntermediateEventID == IntermediateEventID
                //                                   && row.SubEventID == SubEventID && row2.ComplianceId == ComplianceID && row.EventScheduledOnID == EventScheduledOnId
                //                                   && (row1.StatusId == 4 || row1.StatusId == 5)
                //                                    && row.IsActive == true && row.IsUpcomingNotDeleted == true
                //                                   select row).ToList();

                //if (ComplainceTransCompleteList.Count >= 1)
                //{
                //    result = true;
                //}
                //else
                //{
                //    result = false;
                //}
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }
        protected void gvChildGrid_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DateTime date = DateTime.Now;
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);

                GridView gv = (GridView)e.Row.FindControl("gvComplianceGrid");
                int SubEventID = Convert.ToInt32(e.Row.Cells[1].Text);
                glsubEventID = SubEventID;
                GridView childGrid1 = (GridView)sender;
                //Retreiving the GridView DataKey Value
                int Parentid = Convert.ToInt32(childGrid1.DataKeys[e.Row.RowIndex].Value.ToString());
                ComplianceDBEntities entities = new ComplianceDBEntities();
                //var Compliance = entities.SP_GetComplianceWithDays(Parentid, SubEventID, Type).ToList();

                //  int CustomerBranchID = Convert.ToInt32(Session["Rbranch"]);
                int CustomerBranchID = Convert.ToInt32(Request.QueryString["CustomerBranchID"]);
                Label lblIntermediateEventID = (Label)e.Row.FindControl("IntermediateEventID");
                if (lblIntermediateEventID.Text == "0")
                {
                    // int EventClassificationID = Convert.ToInt32(Session["EventClassificationID"]);
                    int EventClassificationID = Convert.ToInt32(Request.QueryString["EventClassificationID"]);

                    if (EventClassificationID == 1)
                    {
                        var Compliance = entities.SP_GetComplianceWithShortNoticeDays(Parentid, SubEventID, Convert.ToInt32(Request.QueryString["EventScheduledOnID"]), Type, CustomerBranchID).ToList();
                        gv.DataSource = Compliance;
                        gv.DataBind();
                    }
                    else if (EventClassificationID == 2)
                    {
                        var Compliance = entities.SP_GetNonSecretrialComplianceWithShortNoticeDays(Parentid, SubEventID, Convert.ToInt32(Request.QueryString["EventScheduledOnID"]), Type, CustomerBranchID).ToList();
                        gv.DataSource = Compliance;
                        gv.DataBind();
                    }
                }
                else
                {
                    GridView gv1 = (GridView)e.Row.FindControl("gvIntermediateSubEventGrid");
                    int IntermediateEventID = Convert.ToInt32(e.Row.Cells[1].Text);
                    glIntermediateID = IntermediateEventID;
                    GridView childGrid2 = (GridView)sender;
                    int Parentid1 = Convert.ToInt32(childGrid1.DataKeys[e.Row.RowIndex].Value.ToString());
                    ComplianceDBEntities entities1 = new ComplianceDBEntities();

                    var Compliance = entities1.SP_GetActivatedIntermediateSubEvent(IntermediateEventID, Convert.ToInt32(Request.QueryString["EventScheduledOnID"]), Parentid, Type).ToList();
                    gv1.DataSource = Compliance;
                    gv1.DataBind();
                    upOptionalCompliances.Update();
                }
            }
        }
        protected void gvChildGrid_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                txtShortNoticeDays.Text = "";
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    GridView childGrid = (GridView)gvParentGrid.Rows[0].Cells[0].FindControl("gvChildGrid");
                    int ParentEventID = Convert.ToInt32(childGrid.DataKeys[e.RowIndex].Value.ToString());
                    int SubEventID = Convert.ToInt32(childGrid.Rows[e.RowIndex].Cells[1].Text);
                    // int EventScheduledOnID = Convert.ToInt32(Session["EventScheduledOnID"]);
                    int EventScheduledOnID = Convert.ToInt32(Request.QueryString["EventScheduledOnID"]);
                   // int EventScheduledOnID =EventScheduledOnID;
                    var txtDate = childGrid.Rows[e.RowIndex].FindControl("txtgvChildGridDate") as TextBox;
                    if (txtDate.Text != "")
                    {
                        ViewState["interIndex"] = 0;
                        ViewState["RowIndex"] = e.RowIndex;
                        ViewState["ParentEventID"] = ParentEventID;
                        ViewState["IntermediateEventID"] = 0;
                        ViewState["SubEventID"] = SubEventID;
                        ViewState["EventScheduledOnID"] = EventScheduledOnID;
                        GridView gvEvenToCompliance = (GridView)childGrid.Rows[e.RowIndex].Cells[0].FindControl("gvComplianceGrid");
                        //Check shorter notice Compliance
                        List<long> ShorterNoticeComplianceList = Business.ComplianceManagement.GetShorterNotice();
                        Boolean shorterFlag = false;
                        List<long> lstShortCompliance = new List<long>();
                        foreach (GridViewRow EvenToCompliancerow in gvEvenToCompliance.Rows)
                        {
                            int ComplinaceID = Convert.ToInt32(EvenToCompliancerow.Cells[0].Text);

                            bool ExistShorterNotice = ShorterNoticeComplianceList.Contains(ComplinaceID);

                            if (ExistShorterNotice == true)
                            {
                                shorterFlag = true;
                                lstShortCompliance.Add(ComplinaceID);
                                //break;
                            }
                        }

                        if (shorterFlag == true)
                        {
                            // ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "divShortNotice", "$(\"#divShortNotice\").dialog('open')", true);
                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#modalShortNotice').modal();", true);
                            upOptionalCompliances.Update();
                       
                        }
                        else
                        {
                            var textbox2 = childGrid.Rows[e.RowIndex].FindControl("txtgvChildGridDate") as TextBox;
                            DateTime Date = DateTime.ParseExact(textbox2.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            EventAssignDate eventAssignDate = new EventAssignDate()
                            {
                                ParentEventID = ParentEventID,
                                EventScheduleOnID = EventScheduledOnID,
                                IntermediateEventID = 0,
                                SubEventID = SubEventID,
                                Date = Date,
                                IsActive = true,
                                CreatedDate = DateTime.Now,
                                CreatedBy = Convert.ToInt32(Session["userID"]),
                                Type = Type,
                            };
                            Business.EventManagement.UpdateEventAssignDates(eventAssignDate);
                            // Child Compliance 
                            foreach (GridViewRow EvenToCompliancerow in gvEvenToCompliance.Rows)
                            {
                                int ComplinaceID = Convert.ToInt32(EvenToCompliancerow.Cells[0].Text);
                                int IsInternal = Convert.ToInt32(EvenToCompliancerow.Cells[3].Text);
                                if (IsInternal == 0) //Statutory
                                {
                                    Boolean VisibleFlag = Business.EventManagement.CheckComplianceVisible(ComplinaceID);
                                    if (VisibleFlag == true)  //If complaince visible true then Schudule generated
                                    {
                                        int ComplianceInstanceID = Convert.ToInt32(EventManagement.GetComplianceInstance(ComplinaceID, Convert.ToInt32(Request.QueryString["CustomerBranchID"])).ID);
                                        Boolean IsComplianceChecklistStatutory = EventManagement.CheckComplianceStatutoryChecklist(ComplinaceID);

                                        if (IsComplianceChecklistStatutory == true)
                                        {
                                            //Change Generate flag Schedule change
                                            EventManagement.UpdateComplianceInstanceScheduleFlag(ComplinaceID, ComplianceInstanceID);
                                        }
                                        string days = Convert.ToString(EvenToCompliancerow.Cells[2].Text);
                                        Boolean FlgCheck = false;
                                        FlgCheck = EventManagement.CheckEventComplianceAssigned(ParentEventID, 0, SubEventID, Type, ComplinaceID, EventScheduledOnID, ComplianceInstanceID);
                                        if (FlgCheck == false)
                                        {
                                            EventManagement.GenerateEventComplianceScheduele(ParentEventID, 0, SubEventID, EventScheduledOnID, Date, ComplinaceID, ComplianceInstanceID, days);
                                        }
                                        else
                                        {
                                            int IncludeDays = 0;
                                            var compdetails = EventManagement.GetCompliance(ComplinaceID);
                                            if ((compdetails.ComplianceType == 0 && (compdetails.IsFrequencyBased == false || compdetails.IsFrequencyBased == null)) || compdetails.ComplianceType == 1)
                                            {
                                                int Days = Convert.ToInt32(days);
                                                if (Days == 0)
                                                {
                                                    IncludeDays = 0;
                                                }
                                                else
                                                {
                                                    if (Days > 0)
                                                    {
                                                        IncludeDays = Days - 1;
                                                    }
                                                    else
                                                    {
                                                        IncludeDays = Days + 1;
                                                    }
                                                }
                                                DateTime UpdatedDate = Date.AddDays(IncludeDays);
                                                EventManagement.UpdateReminderSheduleOnDates(EventScheduledOnID, ComplinaceID, UpdatedDate, ComplianceInstanceID, Date);
                                                EventManagement.UpdateComplianceSheduleOnDates(ParentEventID, 0, SubEventID, EventScheduledOnID, ComplinaceID, ComplianceInstanceID, UpdatedDate);
                                            }
                                            else
                                            {
                                                EventManagement.RemoveComplianceScheduleEventDateUpdation(ComplinaceID, ParentEventID, 0, SubEventID, ComplianceInstanceID, EventScheduledOnID);
                                                EventManagement.GenerateEventComplianceScheduele(ParentEventID, 0, SubEventID, EventScheduledOnID, Date, ComplinaceID, ComplianceInstanceID, days);
                                            }
                                        }
                                    }
                                }
                                else if (IsInternal == 1) //Internal
                                {
                                    int ComplianceInstanceID = Convert.ToInt32(EventManagement.GetInternalComplianceInstance(ComplinaceID, Convert.ToInt32(Request.QueryString["CustomerBranchID"])).ID);

                                    int days = Convert.ToInt32(EvenToCompliancerow.Cells[2].Text);
                                    Boolean FlgCheck = false;
                                    FlgCheck = EventManagement.CheckEventInternalComplianceAssigned(ParentEventID, 0, SubEventID, ComplinaceID, EventScheduledOnID, ComplianceInstanceID);
                                    if (FlgCheck == false)
                                    {
                                        EventManagement.GenerateEventInternalComplianceScheduele(ParentEventID, 0, SubEventID, EventScheduledOnID, Date, ComplinaceID, ComplianceInstanceID, days);
                                    }
                                    else
                                    {
                                        int IncludeDays = 0;
                                        if (days == 0)
                                        {
                                            IncludeDays = 0;
                                        }
                                        else
                                        {
                                            if (days > 0)
                                            {
                                                IncludeDays = days - 1;
                                            }
                                            else
                                            {
                                                IncludeDays = days + 1;
                                            }
                                        }
                                        DateTime UpdatedDate = Date.AddDays(IncludeDays);
                                        EventManagement.UpdateInternalReminderSheduleOnDates(EventScheduledOnID, ComplinaceID, UpdatedDate, ComplianceInstanceID, Date);
                                        EventManagement.UpdateInternalComplianceSheduleOnDates(ParentEventID, 0, SubEventID, EventScheduledOnID, ComplinaceID, ComplianceInstanceID, UpdatedDate);
                                    }
                                }
                            }
                            BindParentEventData(Convert.ToInt32(ParentEventID));
                            // BindParentEventData(Convert.ToInt32(ParentEventID), Convert.ToInt32(EventScheduledOnID), Convert.ToInt32(Type));
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Saved Successfully.";
                            
                        }
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Please enter activated date";
                    }
                }
                DateTime date = DateTime.Now;
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void gvChildGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("NotApplicable"))
                {
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    int ParentEventID = Convert.ToInt32(commandArgs[0]);
                    int SubEventID = Convert.ToInt32(commandArgs[1]);
                    int EventScheduledOnID = Convert.ToInt32(commandArgs[2]);

                    GridViewRow gvChildGrid = (GridViewRow)((Control)e.CommandSource).NamingContainer;

                    GridView gvEvenToCompliance = (GridView)gvChildGrid.FindControl("gvComplianceGrid");

                    EventAssignDate eventAssignDate = new EventAssignDate()
                    {
                        ParentEventID = ParentEventID,
                        EventScheduleOnID = EventScheduledOnID,
                        IntermediateEventID = 0,
                        SubEventID = SubEventID,
                        Date = DateTime.Now.Date,
                        IsActive = true,
                        CreatedDate = DateTime.Now,
                        CreatedBy = AuthenticationHelper.UserID,
                        Type = Type,
                    };
                    Business.EventManagement.UpdateEventAssignDates(eventAssignDate);

                    // Child Compliance 
                    foreach (GridViewRow EvenToCompliancerow in gvEvenToCompliance.Rows)
                    {
                        int ComplinaceID = Convert.ToInt32(EvenToCompliancerow.Cells[0].Text);

                        Boolean VisibleFlag = Business.EventManagement.CheckComplianceVisible(ComplinaceID);
                        if (VisibleFlag == true)  //If complaince visible true then Schudule generated
                        {
                            try
                            {
                                int ComplianceInstanceID = Convert.ToInt32(EventManagement.GetComplianceInstance(ComplinaceID, Convert.ToInt32(Request.QueryString["CustomerBranchID"])).ID);
                                Boolean IsComplianceChecklistStatutory = EventManagement.CheckComplianceStatutoryChecklist(ComplinaceID);

                                if (IsComplianceChecklistStatutory == true)
                                {
                                    //Change Generate flag Schedule change
                                    EventManagement.UpdateComplianceInstanceScheduleFlag(ComplinaceID, ComplianceInstanceID);
                                }
                                int days = Convert.ToInt32(EvenToCompliancerow.Cells[2].Text);
                                Boolean FlgCheck = false;
                                FlgCheck = EventManagement.CheckEventComplianceAssigned(ParentEventID, 0, SubEventID, Type, ComplinaceID, EventScheduledOnID, ComplianceInstanceID);
                                if (FlgCheck == false)
                                {
                                    long ComplianceSchedueleID = EventManagement.GenerateEventComplianceSchedueleEntryNotApplicable(ParentEventID, 0, SubEventID, EventScheduledOnID, ComplinaceID, ComplianceInstanceID);
                                    EventManagement.CreateEventComplianceTransactionNotApplicable(ComplianceSchedueleID, ParentEventID, 0, SubEventID, EventScheduledOnID, ComplinaceID, ComplianceInstanceID);
                                }
                                else
                                {
                                    long ComplianceSchedueleID = EventManagement.GetComplianceSheduleOnID(ParentEventID, 0, SubEventID, EventScheduledOnID, ComplinaceID, ComplianceInstanceID);
                                    Boolean ClosedTransaction = EventManagement.CheckClosedTransaction(ComplianceSchedueleID, ParentEventID, 0, SubEventID, EventScheduledOnID, ComplinaceID, ComplianceInstanceID);
                                    if (ClosedTransaction == false)
                                    {
                                        EventManagement.CreateEventComplianceTransactionNotApplicable(ComplianceSchedueleID, ParentEventID, 0, SubEventID, EventScheduledOnID, ComplinaceID, ComplianceInstanceID);
                                    }
                                }
                            }
                            catch (Exception ex)
                            {

                            }
                        }
                    }
                    BindParentEventData(Convert.ToInt32(ParentEventID));
                    // BindParentEventData(Convert.ToInt32(ParentEventID), Convert.ToInt32(EventScheduledOnID), Convert.ToInt32(Type));
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Saved Successfully.";
                }
                else if (e.CommandName.Equals("InternalCompliance"))
                {
                    ddlCompliance.SelectedValue = "-1";
                    txtDays.Text = "";
                    rbtInternalOccurance.SelectedValue = "1";
                    lblImsg.Text = "";

                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    int ParentEventID = Convert.ToInt32(commandArgs[0]);
                    int SubEventID = Convert.ToInt32(commandArgs[1]);
                    int EventScheduledOnID = Convert.ToInt32(commandArgs[2]);
                    DateTime Date = Convert.ToDateTime(commandArgs[3]);
                    int CustomerBranchID = Convert.ToInt32(Request.QueryString["CustomerBranchID"]);

                    ViewState["IParentEventID"] = ParentEventID;
                    ViewState["IIntermediateEventID"] = 0;
                    ViewState["ISubEventID"] = SubEventID;
                    ViewState["IEventScheduledOnID"] = EventScheduledOnID;
                    ViewState["IDate"] = Date;
                    UpdatePanel1.Update();
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#modalInternalCompliance').modal();", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected bool visibleTxtgvChildGridDate(int ParentEventID, int SubEventID, int EventScheduledOnID)
        {
            try
            {

                bool result = false;
                ComplianceDBEntities entities = new ComplianceDBEntities();
                //int CustomerBranchID = Convert.ToInt32(Session["Rbranch"]);
                //int EventClassificationID = Convert.ToInt32(Session["EventClassificationID"]);
                int CustomerBranchID = Convert.ToInt32(Request.QueryString["CustomerBranchID"]);
                int EventClassificationID = Convert.ToInt32(Request.QueryString["EventClassificationID"]);

                if (EventClassificationID == 1)
                {
                    var data = (from row in entities.EventCompAssignDays
                                join row1 in entities.Compliances on row.ComplianceID equals row1.ID
                                join row2 in entities.IndustryMappings on row1.ID equals row2.ComplianceId
                                join row3 in entities.CustomerBranchIndustryMappings on row2.IndustryID equals row3.IndustryID
                                join row4 in entities.CustomerBranches on row3.CustomerBranchID equals row4.ID
                                where row.ParentEventID == ParentEventID && row.IntermediateEventID == 0
                                && row.SubEventID == SubEventID && row1.ComplinceVisible == true
                                && row4.ID == CustomerBranchID
                                select row).Distinct().FirstOrDefault();

                    if (data == null)
                    {
                        result = false;
                    }
                    else
                    {
                        result = true;
                    }
                }
                else if (EventClassificationID == 2)
                {
                    var data = (from row in entities.EventCompAssignDays
                                join row1 in entities.Compliances on row.ComplianceID equals row1.ID
                                join row6 in entities.Acts on row1.ActID equals row6.ID
                                join row2 in entities.IndustryMappings on row1.ID equals row2.ComplianceId
                                join row3 in entities.CustomerBranchIndustryMappings on row2.IndustryID equals row3.IndustryID
                                join row4 in entities.CustomerBranches on row3.CustomerBranchID equals row4.ID
                                where row.ParentEventID == ParentEventID && row.IntermediateEventID == 0
                                && row.SubEventID == SubEventID && row1.ComplinceVisible == true
                                && (row6.StateID == row4.StateID || row6.ComplianceTypeId == 2)
                                && row4.ID == CustomerBranchID
                                select row).Distinct().FirstOrDefault();

                    if (data == null)
                    {
                        result = false;
                    }
                    else
                    {
                        result = true;
                    }

                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }
        protected bool visibleSubEventAddButton(int ParentEventID, int SubEventID, int EventScheduledOnId)
        {
            try
            {
                bool result = false;
                ComplianceDBEntities entities = new ComplianceDBEntities();
                //int CustomerBranchID = Convert.ToInt32(Session["Rbranch"]);
                //int EventClassificationID = Convert.ToInt32(Session["EventClassificationID"]);
                int CustomerBranchID = Convert.ToInt32(Request.QueryString["CustomerBranchID"]);
                int EventClassificationID = Convert.ToInt32(Request.QueryString["EventClassificationID"]);

                if (EventClassificationID == 1)
                {
                    var data = (from row in entities.EventCompAssignDays
                                join row1 in entities.Compliances on row.ComplianceID equals row1.ID
                                join row2 in entities.IndustryMappings on row1.ID equals row2.ComplianceId
                                join row3 in entities.CustomerBranchIndustryMappings on row2.IndustryID equals row3.IndustryID
                                join row4 in entities.CustomerBranches on row3.CustomerBranchID equals row4.ID
                                where row.ParentEventID == ParentEventID && row.IntermediateEventID == 0
                                && row.SubEventID == SubEventID && row1.ComplinceVisible == true
                                && row4.ID == CustomerBranchID
                                select row).Distinct().FirstOrDefault();

                    if (data == null)
                    {
                        result = false;
                    }
                    else
                    {
                        var data1 = (from row in entities.ComplianceScheduleOns
                                     join row1 in entities.ComplianceTransactions
                                     on row.ComplianceInstanceID equals row1.ComplianceInstanceId
                                     where row.ParentEventD == ParentEventID && row.IntermediateEventID == 0
                                     && row.SubEventID == SubEventID && row.EventScheduledOnID == EventScheduledOnId
                                     && row1.StatusId != 1
                                     && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                     select row).FirstOrDefault();

                        if (data1 == null)
                        {
                            result = EventManagement.CheckSubEventDateAssigned(ParentEventID, SubEventID, EventScheduledOnId);
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
                else if (EventClassificationID == 2)
                {
                    var data = (from row in entities.EventCompAssignDays
                                join row1 in entities.Compliances on row.ComplianceID equals row1.ID
                                join row6 in entities.Acts on row1.ActID equals row6.ID
                                join row2 in entities.IndustryMappings on row1.ID equals row2.ComplianceId
                                join row3 in entities.CustomerBranchIndustryMappings on row2.IndustryID equals row3.IndustryID
                                join row4 in entities.CustomerBranches on row3.CustomerBranchID equals row4.ID
                                where row.ParentEventID == ParentEventID && row.IntermediateEventID == 0
                                && row.SubEventID == SubEventID && row1.ComplinceVisible == true
                                && (row6.StateID == row4.StateID || row6.ComplianceTypeId == 2)
                                && row4.ID == CustomerBranchID
                                select row).Distinct().FirstOrDefault();

                    if (data == null)
                    {
                        result = false;
                    }
                    else
                    {

                        var data1 = (from row in entities.ComplianceScheduleOns
                                     join row1 in entities.ComplianceTransactions
                                     on row.ComplianceInstanceID equals row1.ComplianceInstanceId
                                     where row.ParentEventD == ParentEventID && row.IntermediateEventID == 0
                                     && row.SubEventID == SubEventID && row.EventScheduledOnID == EventScheduledOnId
                                     && row1.StatusId != 1
                                     && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                     select row).FirstOrDefault();

                        if (data1 == null)
                        {
                            result = EventManagement.CheckSubEventDateAssigned(ParentEventID, SubEventID, EventScheduledOnId);
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }
        protected bool visibleSubEventUpdateButton(int ParentEventID, int SubEventID, int EventScheduledOnID)
        {
            try
            {
                bool result = false;
                ComplianceDBEntities entities = new ComplianceDBEntities();
                // int CustomerBranchID = Convert.ToInt32(Session["Rbranch"]);
                //int EventClassificationID = Convert.ToInt32(Session["EventClassificationID"]);
                int CustomerBranchID = Convert.ToInt32(Request.QueryString["CustomerBranchID"]);
                int EventClassificationID = Convert.ToInt32(Request.QueryString["EventClassificationID"]);
                if (EventClassificationID == 1)
                {
                    var data = (from row in entities.EventCompAssignDays
                                join row1 in entities.Compliances on row.ComplianceID equals row1.ID
                                join row2 in entities.IndustryMappings on row1.ID equals row2.ComplianceId
                                join row3 in entities.CustomerBranchIndustryMappings on row2.IndustryID equals row3.IndustryID
                                join row4 in entities.CustomerBranches on row3.CustomerBranchID equals row4.ID
                                where row.ParentEventID == ParentEventID && row.IntermediateEventID == 0
                                && row.SubEventID == SubEventID && row1.ComplinceVisible == true
                                && row4.ID == CustomerBranchID

                                select row).Distinct().FirstOrDefault();

                    if (data == null)
                    {
                        result = false;
                    }
                    else
                    {

                        var data1 = (from row in entities.ComplianceScheduleOns
                                     join row1 in entities.ComplianceTransactions
                                     on row.ID equals row1.ComplianceScheduleOnID
                                     where row.ParentEventD == ParentEventID && row.IntermediateEventID == 0
                                     && row.SubEventID == SubEventID && row.EventScheduledOnID == EventScheduledOnID
                                     && row1.StatusId != 1
                                      && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                     select row).FirstOrDefault();

                        if (data1 == null)
                        {
                            result = EventManagement.CheckSubEventDateAssigned(ParentEventID, SubEventID, EventScheduledOnID);

                            if (result == true)
                            {
                                result = false;
                            }
                            else
                            {
                                result = true;
                            }
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
                else if (EventClassificationID == 2)
                {
                    var data = (from row in entities.EventCompAssignDays
                                join row1 in entities.Compliances on row.ComplianceID equals row1.ID
                                join row6 in entities.Acts on row1.ActID equals row6.ID
                                join row2 in entities.IndustryMappings on row1.ID equals row2.ComplianceId
                                join row3 in entities.CustomerBranchIndustryMappings on row2.IndustryID equals row3.IndustryID
                                join row4 in entities.CustomerBranches on row3.CustomerBranchID equals row4.ID
                                where row.ParentEventID == ParentEventID && row.IntermediateEventID == 0
                                && row.SubEventID == SubEventID && row1.ComplinceVisible == true
                                && (row6.StateID == row4.StateID || row6.ComplianceTypeId == 2)
                                && row4.ID == CustomerBranchID
                                select row).Distinct().FirstOrDefault();

                    if (data == null)
                    {
                        result = false;
                    }
                    else
                    {
                        var data1 = (from row in entities.ComplianceScheduleOns
                                     join row1 in entities.ComplianceTransactions
                                     on row.ID equals row1.ComplianceScheduleOnID
                                     where row.ParentEventD == ParentEventID && row.IntermediateEventID == 0
                                     && row.SubEventID == SubEventID && row.EventScheduledOnID == EventScheduledOnID
                                     && row1.StatusId != 1
                                     && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                     select row).FirstOrDefault();

                        if (data1 == null)
                        {
                            result = EventManagement.CheckSubEventDateAssigned(ParentEventID, SubEventID, EventScheduledOnID);

                            if (result == true)
                            {
                                result = false;
                            }
                            else
                            {
                                result = true;
                            }
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }
        protected bool CheckSubEventTransactionComplete(int ParentEventID, int SubEventID, int EventScheduledOnID)
        {
            try
            {
                bool result = false;
                ComplianceDBEntities entities = new ComplianceDBEntities();
                //int CustomerBranchID = Convert.ToInt32(Session["Rbranch"]);
                //int EventClassificationID = Convert.ToInt32(Session["EventClassificationID"]);
                int CustomerBranchID = Convert.ToInt32(Request.QueryString["CustomerBranchID"]);
                int EventClassificationID = Convert.ToInt32(Request.QueryString["EventClassificationID"]);

                if (EventClassificationID == 1)
                {
                    //System.Data.Objects.ObjectParameter returnFlag = new ObjectParameter("output", "Boolean");
                    System.Data.Entity.Core.Objects.ObjectParameter returnFlag = new ObjectParameter("returnFlag", typeof(bool));
                    entities.SP_CheckSubEventTransactionComplete(EventClassificationID, ParentEventID, 0, SubEventID, EventScheduledOnID, CustomerBranchID, returnFlag);
                    result = Convert.ToBoolean(returnFlag.Value);

                    //var ComplainceTransCompleteList = (from row in entities.ComplianceScheduleOns
                    //                                   join row1 in entities.ComplianceTransactions
                    //                                   on row.ID equals row1.ComplianceScheduleOnID
                    //                                   where row.ParentEventD == ParentEventID && row.IntermediateEventID == 0
                    //                                   && row.SubEventID == SubEventID && row.EventScheduledOnID == EventScheduledOnId
                    //                                   && row.IsActive ==true && row.IsUpcomingNotDeleted == true
                    //                                   && (row1.StatusId == 4 || row1.StatusId == 5)
                    //                                   select row).ToList();

                    //var SubeventComplainceList = (from row in entities.EventCompAssignDays
                    //                              join row1 in entities.Compliances on row.ComplianceID equals row1.ID
                    //                              join row2 in entities.IndustryMappings on row1.ID equals row2.ComplianceId
                    //                              join row3 in entities.CustomerBranchIndustryMappings on row2.IndustryID equals row3.IndustryID
                    //                              join row4 in entities.CustomerBranches on row3.CustomerBranchID equals row4.ID
                    //                              where row.ParentEventID == ParentEventID && row.IntermediateEventID == 0
                    //                              && row.SubEventID == SubEventID && row1.ComplinceVisible == true
                    //                              && ((row1.ComplianceType == 0 && (row1.IsFrequencyBased == false || row1.IsFrequencyBased == null))
                    //                              || row1.ComplianceType ==2)
                    //                              && row4.ID == CustomerBranchID
                    //                              select row).Distinct().ToList();
                    //if (ComplainceTransCompleteList.Count >= SubeventComplainceList.Count)
                    //{
                    //    result = false;
                    //}
                    //else
                    //{
                    //    result = true;
                    //}
                }
                else if (EventClassificationID == 2)
                {
                    System.Data.Entity.Core.Objects.ObjectParameter returnFlag = new ObjectParameter("returnFlag", typeof(bool));
                    entities.SP_CheckSubEventTransactionComplete(EventClassificationID, ParentEventID, 0, SubEventID, EventScheduledOnID, CustomerBranchID, returnFlag);
                    result = Convert.ToBoolean(returnFlag.Value);

                    //var ComplainceTransCompleteList = (from row in entities.ComplianceScheduleOns
                    //                                   join row1 in entities.ComplianceTransactions
                    //                                   on row.ID equals row1.ComplianceScheduleOnID
                    //                                   where row.ParentEventD == ParentEventID && row.IntermediateEventID == 0
                    //                                   && row.SubEventID == SubEventID && row.EventScheduledOnID == EventScheduledOnId
                    //                                   && (row1.StatusId == 4 || row1.StatusId == 5)
                    //                                    && row.IsActive == true && row.IsUpcomingNotDeleted == true
                    //                                   select row).ToList();

                    //var SubeventComplainceList = (from row in entities.EventCompAssignDays
                    //                              join row1 in entities.Compliances on row.ComplianceID equals row1.ID
                    //                              join row6 in entities.Acts on row1.ActID equals row6.ID
                    //                              join row2 in entities.IndustryMappings on row1.ID equals row2.ComplianceId
                    //                              join row3 in entities.CustomerBranchIndustryMappings on row2.IndustryID equals row3.IndustryID
                    //                              join row4 in entities.CustomerBranches on row3.CustomerBranchID equals row4.ID
                    //                              where row.ParentEventID == ParentEventID && row.IntermediateEventID == 0
                    //                              && row.SubEventID == SubEventID && row1.ComplinceVisible == true
                    //                              && (row6.StateID == row4.StateID || row6.ComplianceTypeId == 2)
                    //                              && row4.ID == CustomerBranchID
                    //                              select row).Distinct().ToList();


                    //if (ComplainceTransCompleteList.Count >= SubeventComplainceList.Count)
                    //{
                    //    result = false;
                    //}
                    //else
                    //{
                    //    result = true;
                    //}
                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }
        protected bool enableSubEventUpdateButton(int ParentEventID, int SubEventID, int EventScheduledOnID)
        {
            try
            {
                bool result = false;
                ComplianceDBEntities entities = new ComplianceDBEntities();

                var data = (from row in entities.ComplianceScheduleOns
                            join row1 in entities.ComplianceTransactions
                            on row.ID equals row1.ComplianceScheduleOnID
                            where row.ParentEventD == ParentEventID && row.IntermediateEventID == 0
                            && row.SubEventID == SubEventID && row.EventScheduledOnID == EventScheduledOnID
                            && row1.StatusId != 1
                             && row.IsActive == true && row.IsUpcomingNotDeleted == true
                            select row).FirstOrDefault();

                if (data == null)
                {
                    result = EventManagement.CheckSubEventDateAssigned(ParentEventID, SubEventID, EventScheduledOnID);
                    if (result == false)
                    {
                        result = true;
                    }
                }
                else
                {
                    result = false;
                }

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }
        protected bool visibleInternalComplianceAddButton(int ParentEventID, int IntermediateEventID, int SubEventID, int EventScheduledOnID)
        {
            try
            {
                bool result = false;
                ComplianceDBEntities entities = new ComplianceDBEntities();
                //int CustomerBranchID = Convert.ToInt32(Session["Rbranch"]);
                //int EventClassificationID = Convert.ToInt32(Session["EventClassificationID"]);
                int CustomerBranchID = Convert.ToInt32(Request.QueryString["CustomerBranchID"]);
                int EventClassificationID = Convert.ToInt32(Request.QueryString["EventClassificationID"]);

                if (IntermediateEventID == 0)
                {
                    if (EventClassificationID == 1)
                    {

                        var data1 = (from row in entities.ComplianceScheduleOns
                                     join row1 in entities.ComplianceTransactions
                                     on row.ID equals row1.ComplianceScheduleOnID
                                     where row.ParentEventD == ParentEventID && row.IntermediateEventID == 0
                                     && row.SubEventID == SubEventID && row.EventScheduledOnID == EventScheduledOnID
                                     && row1.StatusId != 1
                                     && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                     select row).FirstOrDefault();

                        if (data1 == null)
                        {
                            result = true;
                        }
                        else
                        {
                            result = false;
                        }

                    }
                    else if (EventClassificationID == 2)
                    {
                        var data1 = (from row in entities.ComplianceScheduleOns
                                     join row1 in entities.ComplianceTransactions
                                     on row.ID equals row1.ComplianceScheduleOnID
                                     where row.ParentEventD == ParentEventID && row.IntermediateEventID == 0
                                     && row.SubEventID == SubEventID && row.EventScheduledOnID == EventScheduledOnID
                                     && row1.StatusId != 1
                                     && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                     select row).FirstOrDefault();

                        if (data1 == null)
                        {
                            result = true;
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
                else
                {
                    result = false;
                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }
        protected void gvIntermediateSubEventGrid_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                txtShortNoticeDays.Text = "";
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    GridViewRow masterrow = (GridViewRow)(sender as Control).Parent.Parent;

                    int interIndex = masterrow.RowIndex;

                    GridView intermediateGrid = (GridView)gvParentGrid.Rows[0].Cells[0].FindControl("gvChildGrid");

                    int ParentEventID = Convert.ToInt32(intermediateGrid.DataKeys[interIndex].Value.ToString());

                    GridView childGrid = (GridView)intermediateGrid.Rows[interIndex].Cells[0].FindControl("gvIntermediateSubEventGrid");

                    int SubEventID = Convert.ToInt32(childGrid.Rows[e.RowIndex].Cells[1].Text);

                    int lblIntermediateEventID = Convert.ToInt32(childGrid.DataKeys[e.RowIndex].Value.ToString());

                    //  int EventScheduledOnID = Convert.ToInt32(Session["EventScheduledOnID"]);
                    int EventScheduledOnID = Convert.ToInt32(Request.QueryString["EventScheduledOnID"]);

                    var txtDate = childGrid.Rows[e.RowIndex].FindControl("txtgvIntermediateSubEventGridDate") as TextBox;

                    if (txtDate.Text != "")
                    {
                        ViewState["interIndex"] = interIndex;
                        ViewState["RowIndex"] = e.RowIndex;
                        ViewState["ParentEventID"] = ParentEventID;
                        ViewState["IntermediateEventID"] = lblIntermediateEventID;
                        ViewState["SubEventID"] = SubEventID;
                        ViewState["EventScheduledOnID"] = EventScheduledOnID;

                        GridView gvIntermediateComplainceGrid = (GridView)childGrid.Rows[e.RowIndex].Cells[0].FindControl("gvIntermediateComplainceGrid");

                        //Check shorter notice Compliance
                        List<long> ShorterNoticeComplianceList = Business.ComplianceManagement.GetShorterNotice();
                        Boolean shorterFlag = false;
                        List<long> lstShortCompliance = new List<long>();
                        foreach (GridViewRow EvenToCompliancerow in gvIntermediateComplainceGrid.Rows)
                        {
                            int ComplinaceID = Convert.ToInt32(EvenToCompliancerow.Cells[0].Text);
                            bool ExistShorterNotice = ShorterNoticeComplianceList.Contains(ComplinaceID);

                            if (ExistShorterNotice == true)
                            {
                                shorterFlag = true;
                                lstShortCompliance.Add(ComplinaceID);
                                //break;
                            }
                        }

                        if (shorterFlag == true)
                        {
                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#modalShortNotice').modal();", true);
                            upOptionalCompliances.Update();
                        }
                        else
                        {

                            var textbox2 = childGrid.Rows[e.RowIndex].FindControl("txtgvIntermediateSubEventGridDate") as TextBox;
                            DateTime Date = DateTime.ParseExact(textbox2.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            EventAssignDate eventAssignDate = new EventAssignDate()
                            {
                                ParentEventID = ParentEventID,
                                EventScheduleOnID = EventScheduledOnID,
                                IntermediateEventID = Convert.ToInt32(lblIntermediateEventID),
                                SubEventID = SubEventID,
                                Date = Date,
                                IsActive = true,
                                CreatedDate = DateTime.Now,
                                CreatedBy = Convert.ToInt32(Session["userID"]),
                                Type = Type,
                            };
                            Business.EventManagement.UpdateEventAssignDates(eventAssignDate);

                            //GridView gvIntermediateComplainceGrid = (GridView)childGrid.Rows[e.RowIndex].Cells[0].FindControl("gvIntermediateComplainceGrid");
                            // Intermediate Sub event Compliance 
                            foreach (GridViewRow EvenToCompliancerow in gvIntermediateComplainceGrid.Rows)
                            {
                                int ComplinaceID = Convert.ToInt32(EvenToCompliancerow.Cells[0].Text);
                                int IsInternal = Convert.ToInt32(EvenToCompliancerow.Cells[3].Text);
                                if (IsInternal == 0) //Statutory
                                {
                                    Boolean VisibleFlag = Business.EventManagement.CheckComplianceVisible(ComplinaceID);

                                    if (VisibleFlag == true)  //If complaince visible true then Schudule generated
                                    {
                                        int ComplianceInstanceID = Convert.ToInt32(EventManagement.GetComplianceInstance(ComplinaceID, Convert.ToInt32(Request.QueryString["CustomerBranchID"])).ID);
                                        Boolean IsComplianceChecklistStatutory = EventManagement.CheckComplianceStatutoryChecklist(ComplinaceID);

                                        if (IsComplianceChecklistStatutory == true)
                                        {
                                            //Change Generate flag Schedule change
                                            EventManagement.UpdateComplianceInstanceScheduleFlag(ComplinaceID, ComplianceInstanceID);
                                        }
                                        string days = Convert.ToString(EvenToCompliancerow.Cells[2].Text);
                                        Boolean FlgCheck = false;
                                        FlgCheck = EventManagement.CheckEventComplianceAssigned(ParentEventID, lblIntermediateEventID, SubEventID, Type, ComplinaceID, EventScheduledOnID, ComplianceInstanceID);
                                        if (FlgCheck == false)
                                        {
                                            EventManagement.GenerateEventComplianceScheduele(ParentEventID, Convert.ToInt32(lblIntermediateEventID), SubEventID, EventScheduledOnID, Date, ComplinaceID, ComplianceInstanceID, days);
                                        }
                                        else
                                        {
                                            int IncludeDays = 0;
                                            var compdetails = EventManagement.GetCompliance(ComplinaceID);
                                            if ((compdetails.ComplianceType == 0 && (compdetails.IsFrequencyBased == false || compdetails.IsFrequencyBased == null)) || compdetails.ComplianceType == 1)
                                            {
                                                int Days = Convert.ToInt32(days);
                                                if (Days == 0)
                                                {
                                                    IncludeDays = 0;
                                                }
                                                else
                                                {
                                                    if (Days > 0)
                                                    {
                                                        IncludeDays = Days - 1;
                                                    }
                                                    else
                                                    {
                                                        IncludeDays = Days + 1;
                                                    }
                                                }
                                                DateTime UpdatedDate = Date.AddDays(IncludeDays);
                                                EventManagement.UpdateReminderSheduleOnDates(EventScheduledOnID, ComplinaceID, UpdatedDate, ComplianceInstanceID, Date);
                                                EventManagement.UpdateComplianceSheduleOnDates(ParentEventID, Convert.ToInt32(lblIntermediateEventID), SubEventID, EventScheduledOnID, ComplinaceID, ComplianceInstanceID, UpdatedDate);
                                            }
                                            else
                                            {
                                                EventManagement.RemoveComplianceScheduleEventDateUpdation(ComplinaceID, ParentEventID, Convert.ToInt32(lblIntermediateEventID), SubEventID, ComplianceInstanceID, EventScheduledOnID);
                                                EventManagement.GenerateEventComplianceScheduele(ParentEventID, Convert.ToInt32(lblIntermediateEventID), SubEventID, EventScheduledOnID, Date, ComplinaceID, ComplianceInstanceID, days);
                                            }
                                        }
                                    }
                                }
                                else if (IsInternal == 1) //Internal
                                {

                                    int ComplianceInstanceID = Convert.ToInt32(EventManagement.GetInternalComplianceInstance(ComplinaceID, Convert.ToInt32(Request.QueryString["CustomerBranchID"])).ID);
                                    int days = Convert.ToInt32(EvenToCompliancerow.Cells[2].Text);
                                    Boolean FlgCheck = false;
                                    FlgCheck = EventManagement.CheckEventInternalComplianceAssigned(ParentEventID, lblIntermediateEventID, SubEventID, ComplinaceID, EventScheduledOnID, ComplianceInstanceID);
                                    if (FlgCheck == false)
                                    {
                                        EventManagement.GenerateEventInternalComplianceScheduele(ParentEventID, Convert.ToInt32(lblIntermediateEventID), SubEventID, EventScheduledOnID, Date, ComplinaceID, ComplianceInstanceID, days);
                                    }
                                    else
                                    {
                                        int IncludeDays = 0;
                                        if (days == 0)
                                        {
                                            IncludeDays = 0;
                                        }
                                        else
                                        {
                                            if (days > 0)
                                            {
                                                IncludeDays = days - 1;
                                            }
                                            else
                                            {
                                                IncludeDays = days + 1;
                                            }
                                        }
                                        DateTime UpdatedDate = Date.AddDays(IncludeDays);
                                        EventManagement.UpdateInternalReminderSheduleOnDates(EventScheduledOnID, ComplinaceID, UpdatedDate, ComplianceInstanceID, Date);
                                        EventManagement.UpdateInternalComplianceSheduleOnDates(ParentEventID, Convert.ToInt32(lblIntermediateEventID), SubEventID, EventScheduledOnID, ComplinaceID, ComplianceInstanceID, UpdatedDate);
                                    }
                                }
                            }
                            BindParentEventData(Convert.ToInt32(ParentEventID));
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Saved Successfully";
                            DateTime date = DateTime.Now;
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);


                        }
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Please enter activated date";
                    }
                }


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void gvIntermediateSubEventGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DateTime date = DateTime.Now;
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);

                    GridView gv = (GridView)e.Row.FindControl("gvIntermediateComplainceGrid");
                    int SubEventID = Convert.ToInt32(e.Row.Cells[1].Text);
                    glsubEventID = SubEventID;
                    GridView childGrid1 = (GridView)sender;
                    int intermediateEventID = Convert.ToInt32(childGrid1.DataKeys[e.Row.RowIndex].Value.ToString());
                    ComplianceDBEntities entities = new ComplianceDBEntities();
                    //int parentEventID = Convert.ToInt32(Session["eventId"]);
                    int parentEventID = Convert.ToInt32(Request.QueryString["ID"]);
                        int CustomerBranchID = Convert.ToInt32(Request.QueryString["CustomerBranchID"]);
                    int EventClassificationID = Convert.ToInt32(Request.QueryString["EventClassificationID"]);
                    //int CustomerBranchID = Convert.ToInt32(Session["Rbranch"]);
                    //int EventClassificationID = Convert.ToInt32(Session["EventClassificationID"]);

                    if (EventClassificationID == 1)
                    {
                        var Compliance = entities.SP_GetIntermediateComplianceAssignWithShortNoticeDays(parentEventID, intermediateEventID, SubEventID, Convert.ToInt32(Request.QueryString["EventScheduledOnID"]), Type, CustomerBranchID).ToList();
                        gv.DataSource = Compliance;
                        gv.DataBind();
                    }
                    else if (EventClassificationID == 2)
                    {
                        var Compliance = entities.SP_GetNonSecretrialIntermediateComplianceAssignWithShortNoticeDays(parentEventID, intermediateEventID, SubEventID, Convert.ToInt32(Request.QueryString["EventScheduledOnID"]), Type, CustomerBranchID).ToList();
                        gv.DataSource = Compliance;
                        gv.DataBind();

                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void gvIntermediateSubEventGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("IntermediateNotApplicable"))
                {
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    int ParentEventID = Convert.ToInt32(commandArgs[0]);
                    int IntermediateEventID = Convert.ToInt32(commandArgs[1]);
                    int SubEventID = Convert.ToInt32(commandArgs[2]);
                    int EventScheduledOnID = Convert.ToInt32(commandArgs[3]);
                    GridViewRow gvIntermediateSubEventGrid = (GridViewRow)((Control)e.CommandSource).NamingContainer;

                    GridView gvIntermediateComplainceGrid = (GridView)gvIntermediateSubEventGrid.FindControl("gvIntermediateComplainceGrid");

                    EventAssignDate eventAssignDate = new EventAssignDate()
                    {
                        ParentEventID = ParentEventID,
                        EventScheduleOnID = EventScheduledOnID,
                        IntermediateEventID = Convert.ToInt32(IntermediateEventID),
                        SubEventID = SubEventID,
                        Date = DateTime.Now.Date,
                        IsActive = true,
                        CreatedDate = DateTime.Now,
                        CreatedBy = AuthenticationHelper.UserID,
                        Type = Type,
                    };
                    Business.EventManagement.UpdateEventAssignDates(eventAssignDate);

                    foreach (GridViewRow EvenToCompliancerow in gvIntermediateComplainceGrid.Rows)
                    {
                        int ComplinaceID = Convert.ToInt32(EvenToCompliancerow.Cells[0].Text);

                        Boolean VisibleFlag = Business.EventManagement.CheckComplianceVisible(ComplinaceID);
                        if (VisibleFlag == true)  //If complaince visible true then Schudule generated
                        {
                            try
                            {
                                int ComplianceInstanceID = Convert.ToInt32(EventManagement.GetComplianceInstance(ComplinaceID, Convert.ToInt32(Request.QueryString["CustomerBranchID"])).ID);
                                Boolean IsComplianceChecklistStatutory = EventManagement.CheckComplianceStatutoryChecklist(ComplinaceID);

                                if (IsComplianceChecklistStatutory == true)
                                {
                                    //Change Generate flag Schedule change
                                    EventManagement.UpdateComplianceInstanceScheduleFlag(ComplinaceID, ComplianceInstanceID);
                                }
                                int days = Convert.ToInt32(EvenToCompliancerow.Cells[2].Text);
                                Boolean FlgCheck = false;
                                FlgCheck = EventManagement.CheckEventComplianceAssigned(ParentEventID, IntermediateEventID, SubEventID, Type, ComplinaceID, EventScheduledOnID, ComplianceInstanceID);
                                if (FlgCheck == false)
                                {
                                    long ComplianceSchedueleID = EventManagement.GenerateEventComplianceSchedueleEntryNotApplicable(ParentEventID, IntermediateEventID, SubEventID, EventScheduledOnID, ComplinaceID, ComplianceInstanceID);
                                    EventManagement.CreateEventComplianceTransactionNotApplicable(ComplianceSchedueleID, ParentEventID, IntermediateEventID, SubEventID, EventScheduledOnID, ComplinaceID, ComplianceInstanceID);
                                }
                                else
                                {
                                    long ComplianceSchedueleID = EventManagement.GetComplianceSheduleOnID(ParentEventID, IntermediateEventID, SubEventID, EventScheduledOnID, ComplinaceID, ComplianceInstanceID);
                                    Boolean ClosedTransaction = EventManagement.CheckClosedTransaction(ComplianceSchedueleID, ParentEventID, IntermediateEventID, SubEventID, EventScheduledOnID, ComplinaceID, ComplianceInstanceID);
                                    if (ClosedTransaction == false)
                                    {
                                        EventManagement.CreateEventComplianceTransactionNotApplicable(ComplianceSchedueleID, ParentEventID, IntermediateEventID, SubEventID, EventScheduledOnID, ComplinaceID, ComplianceInstanceID);
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                            }
                        }
                    }
                   // BindParentEventData(Convert.ToInt32(ParentEventID), Convert.ToInt32(EventScheduledOnID), Convert.ToInt32(Type));
                    BindParentEventData(Convert.ToInt32(ParentEventID));
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Saved Successfully.";
                }
                else if (e.CommandName.Equals("IntermediateInternalCompliance"))
                {
                    ddlCompliance.SelectedValue = "-1";
                    txtDays.Text = "";
                    rbtInternalOccurance.SelectedValue = "1";
                    lblImsg.Text = "";

                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    int ParentEventID = Convert.ToInt32(commandArgs[0]);
                    int IntermediateEventID = Convert.ToInt32(commandArgs[1]);
                    int SubEventID = Convert.ToInt32(commandArgs[2]);
                    int EventScheduledOnID = Convert.ToInt32(commandArgs[3]);
                    DateTime Date = Convert.ToDateTime(commandArgs[4]);

                    ViewState["IParentEventID"] = ParentEventID;
                    ViewState["IIntermediateEventID"] = IntermediateEventID;
                    ViewState["ISubEventID"] = SubEventID;
                    ViewState["IEventScheduledOnID"] = EventScheduledOnID;
                    ViewState["IDate"] = Date;

                    UpdatePanel1.Update();
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#modalInternalCompliance').modal();", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected bool visibleTxtgvIntermediateSubEventGridDate(int ParentEventID, int IntermediateEventID, int SubEventID, int EventScheduledOnId)
        {
            try
            {
                bool result = false;

                ComplianceDBEntities entities = new ComplianceDBEntities();
                //int CustomerBranchID = Convert.ToInt32(Session["Rbranch"]);
                //int EventClassificationID = Convert.ToInt32(Session["EventClassificationID"]);
                int CustomerBranchID = Convert.ToInt32(Request.QueryString["CustomerBranchID"]);
                int EventClassificationID = Convert.ToInt32(Request.QueryString["EventClassificationID"]);
                if (EventClassificationID == 1)
                {
                    var data = (from row in entities.EventCompAssignDays
                                join row1 in entities.Compliances on row.ComplianceID equals row1.ID
                                join row2 in entities.IndustryMappings on row1.ID equals row2.ComplianceId
                                join row3 in entities.CustomerBranchIndustryMappings on row2.IndustryID equals row3.IndustryID
                                join row4 in entities.CustomerBranches on row3.CustomerBranchID equals row4.ID
                                where row.ParentEventID == ParentEventID && row.IntermediateEventID == IntermediateEventID
                                && row.SubEventID == SubEventID && row1.ComplinceVisible == true
                                && row4.ID == CustomerBranchID
                                select row).Distinct().FirstOrDefault();

                    if (data == null)
                    {
                        result = false;
                    }
                    else
                    {
                        result = true;
                    }

                }
                else if (EventClassificationID == 2)
                {
                    var data = (from row in entities.EventCompAssignDays
                                join row1 in entities.Compliances on row.ComplianceID equals row1.ID
                                join row6 in entities.Acts on row1.ActID equals row6.ID
                                join row2 in entities.IndustryMappings on row1.ID equals row2.ComplianceId
                                join row3 in entities.CustomerBranchIndustryMappings on row2.IndustryID equals row3.IndustryID
                                join row4 in entities.CustomerBranches on row3.CustomerBranchID equals row4.ID
                                where row.ParentEventID == ParentEventID && row.IntermediateEventID == IntermediateEventID
                                && row.SubEventID == SubEventID && row1.ComplinceVisible == true
                                && (row6.StateID == row4.StateID || row6.ComplianceTypeId == 2)
                                && row4.ID == CustomerBranchID
                                select row).Distinct().FirstOrDefault();

                    if (data == null)
                    {
                        result = false;
                    }
                    else
                    {
                        result = true;
                    }

                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }
     
        protected bool visibleIntermediateSubEventUpdateButton(int ParentEventID, int IntermediateEventID, int SubEventID, int EventScheduledOnId)
        {
            try
            {
                bool result = false;
                ComplianceDBEntities entities = new ComplianceDBEntities();
                //  int CustomerBranchID = Convert.ToInt32(Session["Rbranch"]);
                //  int EventClassificationID = Convert.ToInt32(Session["EventClassificationID"]);
                int CustomerBranchID = Convert.ToInt32(Request.QueryString["CustomerBranchID"]);
                int EventClassificationID = Convert.ToInt32(Request.QueryString["EventClassificationID"]);

                if (EventClassificationID == 1)
                {
                    var data = (from row in entities.EventCompAssignDays
                                join row1 in entities.Compliances on row.ComplianceID equals row1.ID
                                join row2 in entities.IndustryMappings on row1.ID equals row2.ComplianceId
                                join row3 in entities.CustomerBranchIndustryMappings on row2.IndustryID equals row3.IndustryID
                                join row4 in entities.CustomerBranches on row3.CustomerBranchID equals row4.ID
                                where row.ParentEventID == ParentEventID && row.IntermediateEventID == IntermediateEventID
                                && row.SubEventID == SubEventID && row1.ComplinceVisible == true
                                && row4.ID == CustomerBranchID
                                select row).Distinct().FirstOrDefault();

                    if (data == null)
                    {
                        result = false;
                    }
                    else
                    {
                        var data1 = (from row in entities.ComplianceScheduleOns
                                     join row1 in entities.ComplianceTransactions
                                     on row.ID equals row1.ComplianceScheduleOnID
                                     where row.ParentEventD == ParentEventID && row.IntermediateEventID == IntermediateEventID
                                     && row.SubEventID == SubEventID && row.EventScheduledOnID == EventScheduledOnId
                                     && row1.StatusId != 1
                                      && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                     select row).FirstOrDefault();

                        if (data1 == null)
                        {
                            result = EventManagement.CheckIntermediateSubEventDateAssigned(ParentEventID, IntermediateEventID, SubEventID, EventScheduledOnId);

                            if (result == true)
                            {
                                result = false;
                            }
                            else
                            {
                                result = true;
                            }
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
                else if (EventClassificationID == 2)
                {
                    var data = (from row in entities.EventCompAssignDays
                                join row1 in entities.Compliances on row.ComplianceID equals row1.ID
                                join row6 in entities.Acts on row1.ActID equals row6.ID
                                join row2 in entities.IndustryMappings on row1.ID equals row2.ComplianceId
                                join row3 in entities.CustomerBranchIndustryMappings on row2.IndustryID equals row3.IndustryID
                                join row4 in entities.CustomerBranches on row3.CustomerBranchID equals row4.ID
                                where row.ParentEventID == ParentEventID && row.IntermediateEventID == IntermediateEventID
                                && row.SubEventID == SubEventID && row1.ComplinceVisible == true
                                 && (row6.StateID == row4.StateID || row6.ComplianceTypeId == 2)
                                && row4.ID == CustomerBranchID
                                select row).Distinct().FirstOrDefault();

                    if (data == null)
                    {
                        result = false;
                    }
                    else
                    {
                        var data1 = (from row in entities.ComplianceScheduleOns
                                     join row1 in entities.ComplianceTransactions
                                     on row.ID equals row1.ComplianceScheduleOnID
                                     where row.ParentEventD == ParentEventID && row.IntermediateEventID == IntermediateEventID
                                     && row.SubEventID == SubEventID && row.EventScheduledOnID == EventScheduledOnId
                                     && row1.StatusId != 1
                                      && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                     select row).FirstOrDefault();

                        if (data1 == null)
                        {
                            result = EventManagement.CheckIntermediateSubEventDateAssigned(ParentEventID, IntermediateEventID, SubEventID, EventScheduledOnId);

                            if (result == true)
                            {
                                result = false;
                            }
                            else
                            {
                                result = true;
                            }
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }
        protected bool visibleIntermediateSubEventAddButton(int ParentEventID, int IntermediateEventID, int SubEventID, int EventScheduledOnId)
        {
            try
            {
                bool result = false;

                ComplianceDBEntities entities = new ComplianceDBEntities();
                //int CustomerBranchID = Convert.ToInt32(Session["Rbranch"]);

                //int EventClassificationID = Convert.ToInt32(Session["EventClassificationID"]);
                int CustomerBranchID = Convert.ToInt32(Request.QueryString["CustomerBranchID"]);
                int EventClassificationID = Convert.ToInt32(Request.QueryString["EventClassificationID"]);
                if (EventClassificationID == 1)
                {
                    var data = (from row in entities.EventCompAssignDays
                                join row1 in entities.Compliances on row.ComplianceID equals row1.ID
                                join row2 in entities.IndustryMappings on row1.ID equals row2.ComplianceId
                                join row3 in entities.CustomerBranchIndustryMappings on row2.IndustryID equals row3.IndustryID
                                join row4 in entities.CustomerBranches on row3.CustomerBranchID equals row4.ID
                                where row.ParentEventID == ParentEventID && row.IntermediateEventID == IntermediateEventID
                                && row.SubEventID == SubEventID && row1.ComplinceVisible == true
                                && row4.ID == CustomerBranchID
                                select row).Distinct().FirstOrDefault();

                    if (data == null)
                    {
                        result = false;
                    }
                    else
                    {
                        var data1 = (from row in entities.ComplianceScheduleOns
                                     join row1 in entities.ComplianceTransactions
                                     on row.ID equals row1.ComplianceScheduleOnID
                                     where row.ParentEventD == ParentEventID && row.IntermediateEventID == IntermediateEventID
                                     && row.SubEventID == SubEventID && row.EventScheduledOnID == EventScheduledOnId
                                     && row1.StatusId != 1
                                     && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                     select row).FirstOrDefault();

                        if (data1 == null)
                        {
                            result = EventManagement.CheckIntermediateSubEventDateAssigned(ParentEventID, IntermediateEventID, SubEventID, EventScheduledOnId);
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
                else if (EventClassificationID == 2)
                {
                    var data = (from row in entities.EventCompAssignDays
                                join row1 in entities.Compliances on row.ComplianceID equals row1.ID
                                join row6 in entities.Acts on row1.ActID equals row6.ID
                                join row2 in entities.IndustryMappings on row1.ID equals row2.ComplianceId
                                join row3 in entities.CustomerBranchIndustryMappings on row2.IndustryID equals row3.IndustryID
                                join row4 in entities.CustomerBranches on row3.CustomerBranchID equals row4.ID
                                where row.ParentEventID == ParentEventID && row.IntermediateEventID == IntermediateEventID
                                && row.SubEventID == SubEventID && row1.ComplinceVisible == true
                                && (row6.StateID == row4.StateID || row6.ComplianceTypeId == 2)
                                && row4.ID == CustomerBranchID
                                select row).Distinct().FirstOrDefault();

                    if (data == null)
                    {
                        result = false;
                    }
                    else
                    {
                        var data1 = (from row in entities.ComplianceScheduleOns
                                     join row1 in entities.ComplianceTransactions
                                     on row.ID equals row1.ComplianceScheduleOnID
                                     where row.ParentEventD == ParentEventID && row.IntermediateEventID == IntermediateEventID
                                     && row.SubEventID == SubEventID && row.EventScheduledOnID == EventScheduledOnId
                                     && row1.StatusId != 1
                                     && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                     select row).FirstOrDefault();

                        if (data1 == null)
                        {
                            result = EventManagement.CheckIntermediateSubEventDateAssigned(ParentEventID, IntermediateEventID, SubEventID, EventScheduledOnId);
                        }
                        else
                        {
                            result = false;
                        }
                    }

                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }
        protected bool enableIntermediateSubEventUpdateButton(int ParentEventID, int IntermediateEventID, int SubEventID, int EventScheduledOnId)
        {
            try
            {
                bool result = false;
                ComplianceDBEntities entities = new ComplianceDBEntities();

                var data = (from row in entities.ComplianceScheduleOns
                            join row1 in entities.ComplianceTransactions
                            on row.ID equals row1.ComplianceScheduleOnID
                            where row.ParentEventD == ParentEventID && row.IntermediateEventID == IntermediateEventID
                            && row.SubEventID == SubEventID && row.EventScheduledOnID == EventScheduledOnId
                            && row1.StatusId != 1
                             && row.IsActive == true && row.IsUpcomingNotDeleted == true
                            select row).FirstOrDefault();

                if (data == null)
                {
                    result = EventManagement.CheckIntermediateSubEventDateAssigned(ParentEventID, IntermediateEventID, SubEventID, EventScheduledOnId);
                    if (result == false)
                    {
                        result = true;
                    }
                }
                else
                {
                    result = false;
                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }
        protected void AddInstancesSortImage(int columnIndex, GridViewRow headerRow)
        {
            try
            {
                System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
                sortImage.ImageAlign = ImageAlign.AbsMiddle;

                if (direction == SortDirection.Ascending)
                {
                    sortImage.ImageUrl = "../Images/SortAsc.gif";
                    sortImage.AlternateText = "Ascending Order";
                }
                else
                {
                    sortImage.ImageUrl = "../Images/SortDesc.gif";
                    sortImage.AlternateText = "Descending Order";
                }
                headerRow.Cells[columnIndex].Controls.Add(sortImage);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp)
        {
            try
            {
                foreach (var item in nvp.Children)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    BindBranchesHierarchy(node, item);
                    parent.ChildNodes.Add(node);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected bool CheckIntermediateEventTransactionComplete(int ParentEventID, int IntermediateID, int SubEventID, int EventScheduledOnId)
        {
            try
            {
                bool result = false;
                ComplianceDBEntities entities = new ComplianceDBEntities();
                //int CustomerBranchID = Convert.ToInt32(Session["Rbranch"]);
                //int EventClassificationID = Convert.ToInt32(Session["EventClassificationID"]);
                int CustomerBranchID = Convert.ToInt32(Request.QueryString["CustomerBranchID"]);
                int EventClassificationID = Convert.ToInt32(Request.QueryString["EventClassificationID"]);

                if (EventClassificationID == 1)
                {
                    System.Data.Entity.Core.Objects.ObjectParameter returnFlag = new ObjectParameter("returnFlag", typeof(bool));
                    entities.SP_CheckSubEventTransactionComplete(EventClassificationID, ParentEventID, IntermediateID, SubEventID, EventScheduledOnId, CustomerBranchID, returnFlag);
                    result = Convert.ToBoolean(returnFlag.Value);

                    //var ComplainceTransCompleteList = (from row in entities.ComplianceScheduleOns
                    //                                   join row1 in entities.ComplianceTransactions
                    //                                   on row.ID equals row1.ComplianceScheduleOnID
                    //                                   where row.ParentEventD == ParentEventID && row.IntermediateEventID == IntermediateID
                    //                                   && row.SubEventID == SubEventID && row.EventScheduledOnID == EventScheduledOnId
                    //                                   && (row1.StatusId == 4 || row1.StatusId == 5)
                    //                                   && row.IsActive == true && row.IsUpcomingNotDeleted == true
                    //                                   select row).ToList();

                    //var SubeventComplainceList = (from row in entities.EventCompAssignDays
                    //                              join row1 in entities.Compliances on row.ComplianceID equals row1.ID
                    //                              join row2 in entities.IndustryMappings on row1.ID equals row2.ComplianceId
                    //                              join row3 in entities.CustomerBranchIndustryMappings on row2.IndustryID equals row3.IndustryID
                    //                              join row4 in entities.CustomerBranches on row3.CustomerBranchID equals row4.ID
                    //                              where row.ParentEventID == ParentEventID && row.IntermediateEventID == IntermediateID
                    //                              && row.SubEventID == SubEventID && row1.ComplinceVisible == true
                    //                              && row4.ID == CustomerBranchID
                    //                              select row).Distinct().ToList();

                    //if (ComplainceTransCompleteList.Count >= SubeventComplainceList.Count)
                    //{
                    //    result = false;
                    //}
                    //else
                    //{
                    //    result = true;
                    //}
                }
                else if (EventClassificationID == 2)
                {

                    System.Data.Entity.Core.Objects.ObjectParameter returnFlag = new ObjectParameter("returnFlag", typeof(bool));
                    entities.SP_CheckSubEventTransactionComplete(EventClassificationID, ParentEventID, IntermediateID, SubEventID, EventScheduledOnId, CustomerBranchID, returnFlag);
                    result = Convert.ToBoolean(returnFlag.Value);

                    //var ComplainceTransCompleteList = (from row in entities.ComplianceScheduleOns
                    //                                   join row1 in entities.ComplianceTransactions
                    //                                   on row.ID equals row1.ComplianceScheduleOnID
                    //                                   where row.ParentEventD == ParentEventID && row.IntermediateEventID == IntermediateID
                    //                                   && row.SubEventID == SubEventID && row.EventScheduledOnID == EventScheduledOnId
                    //                                   && (row1.StatusId == 4 || row1.StatusId == 5)
                    //                                    && row.IsActive == true && row.IsUpcomingNotDeleted == true
                    //                                   select row).ToList();

                    //var SubeventComplainceList = (from row in entities.EventCompAssignDays
                    //                              join row1 in entities.Compliances on row.ComplianceID equals row1.ID
                    //                              join row6 in entities.Acts on row1.ActID equals row6.ID
                    //                              join row2 in entities.IndustryMappings on row1.ID equals row2.ComplianceId
                    //                              join row3 in entities.CustomerBranchIndustryMappings on row2.IndustryID equals row3.IndustryID
                    //                              join row4 in entities.CustomerBranches on row3.CustomerBranchID equals row4.ID
                    //                              where row.ParentEventID == ParentEventID && row.IntermediateEventID == IntermediateID
                    //                              && row.SubEventID == SubEventID && row1.ComplinceVisible == true
                    //                              && (row6.StateID == row4.StateID || row6.ComplianceTypeId == 2)
                    //                              && row4.ID == CustomerBranchID
                    //                              select row).Distinct().ToList();


                    //if (ComplainceTransCompleteList.Count >= SubeventComplainceList.Count)
                    //{
                    //    result = false;
                    //}
                    //else
                    //{
                    //    result = true;
                    //}
                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }
        protected bool visibleIntermediateEventInternalComplianceAddButton(int ParentEventID, int IntermediateID, int SubEventID, int EventScheduledOnId)
        {
            try
            {
                bool result = false;
                ComplianceDBEntities entities = new ComplianceDBEntities();
                //int CustomerBranchID = Convert.ToInt32(Session["Rbranch"]);
                //int EventClassificationID = Convert.ToInt32(Session["EventClassificationID"]);
                int CustomerBranchID = Convert.ToInt32(Request.QueryString["CustomerBranchID"]);
                int EventClassificationID = Convert.ToInt32(Request.QueryString["EventClassificationID"]);

                if (EventClassificationID == 1)
                {

                    var data1 = (from row in entities.ComplianceScheduleOns
                                 join row1 in entities.ComplianceTransactions
                                 on row.ID equals row1.ComplianceScheduleOnID
                                 where row.ParentEventD == ParentEventID && row.IntermediateEventID == IntermediateID
                                 && row.SubEventID == SubEventID && row.EventScheduledOnID == EventScheduledOnId
                                 && row1.StatusId != 1
                                  && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                 select row).FirstOrDefault();

                    if (data1 == null)
                    {
                        result = true;
                    }
                    else
                    {
                        result = false;
                    }

                }
                else if (EventClassificationID == 2)
                {
                    var data1 = (from row in entities.ComplianceScheduleOns
                                 join row1 in entities.ComplianceTransactions
                                 on row.ID equals row1.ComplianceScheduleOnID
                                 where row.ParentEventD == ParentEventID && row.IntermediateEventID == IntermediateID
                                 && row.SubEventID == SubEventID && row.EventScheduledOnID == EventScheduledOnId
                                 && row1.StatusId != 1
                                  && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                 select row).FirstOrDefault();

                    if (data1 == null)
                    {
                        result = true;
                    }
                    else
                    {
                        result = false;
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        protected void gvIntermediateComplainceGrid_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int CompliaceID = Convert.ToInt32(e.Row.Cells[0].Text);
                int IsIntermediateInternal = Convert.ToInt32(e.Row.Cells[3].Text);

                if (IsIntermediateInternal == 0)
                {
                    string type = Business.EventManagement.CheckEventComplianceType(CompliaceID);

                    Label lblIntermediateStatus = (e.Row.FindControl("lblIntermediateStatus") as Label);
                    Button btngvIntermediateComplainceGridClosure = (e.Row.FindControl("btngvIntermediateComplainceGridClosure") as Button);
                    if (type == "Informational")
                    {
                        e.Row.ForeColor = System.Drawing.Color.Chocolate;
                        e.Row.CssClass = "Inforamative";
                        lblIntermediateStatus.Visible = false;
                        btngvIntermediateComplainceGridClosure.Visible = false;
                    }
                    else if (type == "Actionable")
                    {
                        e.Row.ForeColor = System.Drawing.Color.SlateBlue;
                        e.Row.CssClass = "Actionable";
                        lblIntermediateStatus.Visible = true;
                        Boolean transcomplete = CheckCompleteTransactionComplete(glParentID, glIntermediateID, glsubEventID, CompliaceID, Convert.ToInt32(Request.QueryString["EventScheduledOnID"]));
                        if (transcomplete == true)
                        {
                            lblIntermediateStatus.Visible = true;
                            btngvIntermediateComplainceGridClosure.Visible = false;

                        }
                        else
                        {
                            var isvisibleClosure = visibleClosure(CompliaceID);
                            if (isvisibleClosure == true)
                            {
                                var isClosure = GetEventComplianceClosure(CompliaceID, Convert.ToInt64(Request.QueryString["EventScheduledOnID"]), glParentID, glIntermediateID, glsubEventID);
                                if (isClosure == null)
                                {
                                    btngvIntermediateComplainceGridClosure.Visible = true;
                                    lblIntermediateStatus.Visible = false;
                                }
                                else
                                {
                                    var isTransaction = EventManagement.CheckComplianceTrasactionClosure(CompliaceID, Convert.ToInt64(Request.QueryString["EventScheduledOnID"]), isClosure.FromDate, glParentID, 0, glsubEventID);

                                    if (isTransaction == false)
                                    {
                                        btngvIntermediateComplainceGridClosure.Visible = true;
                                        lblIntermediateStatus.Visible = false;
                                    }
                                    else
                                    {
                                        btngvIntermediateComplainceGridClosure.Visible = false;
                                        lblIntermediateStatus.Visible = true;
                                    }
                                }
                            }
                            else
                            {
                                btngvIntermediateComplainceGridClosure.Visible = false;
                                lblIntermediateStatus.Visible = false;
                            }
                        }
                    }
                    else if (type == "Statutory")
                    {
                        lblIntermediateStatus.Visible = true;
                        Boolean transcomplete = CheckCompleteTransactionComplete(glParentID, glIntermediateID, glsubEventID, CompliaceID, Convert.ToInt32(Request.QueryString["EventScheduledOnID"]));
                        if (transcomplete == true)
                        {
                            lblIntermediateStatus.Visible = true;
                            btngvIntermediateComplainceGridClosure.Visible = false;

                        }
                        else
                        {
                            var isvisibleClosure = visibleClosure(CompliaceID);
                            if (isvisibleClosure == true)
                            {
                                var isClosure = GetEventComplianceClosure(CompliaceID, Convert.ToInt64(Request.QueryString["EventScheduledOnID"]), glParentID, glIntermediateID, glsubEventID);
                                if (isClosure == null)
                                {
                                    btngvIntermediateComplainceGridClosure.Visible = true;
                                    lblIntermediateStatus.Visible = false;
                                }
                                else
                                {
                                    var isTransaction = EventManagement.CheckComplianceTrasactionClosure(CompliaceID, Convert.ToInt64(Request.QueryString["EventScheduledOnID"]), isClosure.FromDate, glParentID, 0, glsubEventID);

                                    if (isTransaction == false)
                                    {
                                        btngvIntermediateComplainceGridClosure.Visible = true;
                                        lblIntermediateStatus.Visible = false;
                                    }
                                    else
                                    {
                                        btngvIntermediateComplainceGridClosure.Visible = false;
                                        lblIntermediateStatus.Visible = true;
                                    }
                                }
                            }
                            else
                            {
                                btngvIntermediateComplainceGridClosure.Visible = false;
                                lblIntermediateStatus.Visible = false;
                            }
                        }
                    }
                    else
                    {
                        Boolean transcomplete = CheckCompleteTransactionComplete(glParentID, glIntermediateID, glsubEventID, CompliaceID, Convert.ToInt32(Request.QueryString["EventScheduledOnID"]));
                        if (transcomplete == true)
                        {
                            lblIntermediateStatus.Visible = true;
                        }
                        else
                        {
                            lblIntermediateStatus.Visible = false;
                        }
                    }
                }
                else
                {
                    Label lblIntermediateStatus = (e.Row.FindControl("lblIntermediateStatus") as Label);

                    Boolean transcomplete = CheckInternalCompleteTransactionComplete(glParentID, glIntermediateID, glsubEventID, CompliaceID, Convert.ToInt32(Request.QueryString["EventScheduledOnID"]));
                    if (transcomplete == true)
                    {
                        lblIntermediateStatus.Visible = true;
                    }
                    else
                    {
                        lblIntermediateStatus.Visible = false;
                    }
                }
            }
        }
        protected EventComplianceClosure GetEventComplianceClosure(long ComplianceID, long EventScheduleOnID, long ParentEventID, long IntermediateEventID, long SubEventID)
        {
            try
            {
                ComplianceDBEntities entities = new ComplianceDBEntities();
                var data = (from row in entities.EventComplianceClosures
                            where row.ComplianceID == ComplianceID
                            && row.EventScheduleOnID == EventScheduleOnID
                            && row.ParentEventD == ParentEventID && row.IntermediateEventID == IntermediateEventID
                            && row.SubEventID == SubEventID
                            select row).FirstOrDefault();

                return data;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return null;
        }

        protected bool visibleClosure(int ComplianceID)
        {
            try
            {
                ComplianceDBEntities entities = new ComplianceDBEntities();
                bool result = false;
                var data = (from row in entities.Compliances
                            where row.ID == ComplianceID
                            select row).Distinct().FirstOrDefault();

                if (data.IsFrequencyBased == true || data.ComplianceType == 2)
                {
                    if (data.IsForcefulClosure == true)
                    {
                        result = true;
                    }
                    else
                    {
                        result = false;
                    }
                }
                else
                {
                    result = false;
                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }
        protected bool CheckInternalCompleteTransactionComplete(int ParentEventID, int IntermediateEventID, int SubEventID, int ComplianceID, int EventScheduledOnId)
        {
            try
            {
                bool result = false;
                ComplianceDBEntities entities = new ComplianceDBEntities();

                var ComplainceTransCompleteList = (from row in entities.InternalComplianceScheduledOns
                                                   join row1 in entities.InternalComplianceTransactions
                                                   on row.ID equals row1.InternalComplianceScheduledOnID
                                                   join row2 in entities.ComplianceInstances
                                                   on row1.InternalComplianceInstanceID equals row2.ID
                                                   where row.ParentEventD == ParentEventID && row.IntermediateEventID == IntermediateEventID
                                                   && row.SubEventID == SubEventID && row2.ComplianceId == ComplianceID && row.EventScheduledOnID == EventScheduledOnId
                                                   && (row1.StatusId == 4 || row1.StatusId == 5)
                                                   && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                                   select row).ToList();

                if (ComplainceTransCompleteList.Count >= 1)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }
        protected void gvIntermediateComplainceGrid_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                GridView IntermediateComplainceGrid = (GridView)sender;

                //GridViewRow masterrow = (GridViewRow)(sender as Control).Parent.Parent;
                //int interIndex = masterrow.RowIndex;

                //GridView intermediateGrid = (GridView)gvParentGrid.Rows[0].Cells[0].FindControl("gvChildGrid");
                //GridView childGrid = (GridView)intermediateGrid.Rows[interIndex].Cells[0].FindControl("gvIntermediateSubEventGrid");
                //GridView complianceGrid = (GridView)childGrid.Rows[e.RowIndex].Cells[0].FindControl("gvIntermediateComplainceGrid");
                int complianceID = Convert.ToInt32(IntermediateComplainceGrid.Rows[e.RowIndex].Cells[0].Text);

                int ParentEventID = Convert.ToInt32((IntermediateComplainceGrid.Rows[e.RowIndex].FindControl("lblgvIIntermediateParentEventID") as Label).Text);
                int IntermediateEventID = Convert.ToInt32((IntermediateComplainceGrid.Rows[e.RowIndex].FindControl("lblgvIIntermediateEventID") as Label).Text);
                int SubEventID = Convert.ToInt32((IntermediateComplainceGrid.Rows[e.RowIndex].FindControl("lblgvIIntermediateSubEventID") as Label).Text);


                //GridViewRow masterrow = (GridViewRow)(sender as Control).Parent.Parent;
                //int interIndex = masterrow.RowIndex;
                //GridView childGrid = (GridView)gvParentGrid.Rows[0].Cells[0].FindControl("gvChildGrid");

                //GridView complianceGrid = (GridView)childGrid.Rows[interIndex].Cells[0].FindControl("gvComplianceGrid");
                //int complianceID = Convert.ToInt32(complianceGrid.Rows[e.RowIndex].Cells[0].Text);

                int ComplianceInstanceID = Convert.ToInt32(EventManagement.GetComplianceInstance(complianceID, Convert.ToInt32(Request.QueryString["CustomerBranchID"])).ID);
                int EventScheduledOnID = Convert.ToInt32(Request.QueryString["EventScheduledOnID"]);

                var SchdeuleDates = EventManagement.GetComplianceSchdeuleDates(ComplianceInstanceID, EventScheduledOnID, ParentEventID, IntermediateEventID, SubEventID);

                ddlScheduleDates.DataSource = SchdeuleDates;
                ddlScheduleDates.DataBind();

                ddlScheduleDates.Items.Insert(0, new ListItem("< Select Date >", "-1"));

                UpdatePanel3.Update();
                ViewState["ClosurecomplianceID"] = complianceID;
                ViewState["ClosureComplianceInstanceID"] = ComplianceInstanceID;

                ViewState["ClosureParentEventID"] = ParentEventID;
                ViewState["ClosureIntermediateEventID"] = IntermediateEventID;
                ViewState["ClosureSubEventID"] = SubEventID;

                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#modalClosure').modal();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void gvComplianceGrid_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int CompliaceID = Convert.ToInt32(e.Row.Cells[0].Text);

                int IsInternal = Convert.ToInt32(e.Row.Cells[3].Text);

                if (IsInternal == 0)
                {
                    string type = Business.EventManagement.CheckEventComplianceType(CompliaceID);

                    Label lblStatus = (e.Row.FindControl("lblStatus") as Label);
                    Button btngvComplianceGridClosure = (e.Row.FindControl("btngvComplianceGridClosure") as Button);

                    if (type == "Informational")
                    {
                        e.Row.ForeColor = System.Drawing.Color.Chocolate;
                        e.Row.CssClass = "Inforamative";
                        lblStatus.Visible = false;
                        btngvComplianceGridClosure.Visible = false;
                    }
                    else if (type == "Actionable")
                    {
                        e.Row.ForeColor = System.Drawing.Color.SlateBlue;
                        e.Row.CssClass = "Actionable";
                        lblStatus.Visible = false;
                        Boolean transcomplete = CheckCompleteTransactionComplete(glParentID, 0, glsubEventID, CompliaceID, Convert.ToInt32(Request.QueryString["EventScheduledOnID"]));
                        if (transcomplete == true)
                        {
                            lblStatus.Visible = true;
                            btngvComplianceGridClosure.Visible = false;
                        }
                        else
                        {
                            var isVisibleClosure = visibleClosure(CompliaceID);
                            if (isVisibleClosure == true)
                            {
                                var isClosure = GetEventComplianceClosure(CompliaceID, Convert.ToInt64(Request.QueryString["EventScheduledOnID"]), glParentID, 0, glsubEventID);
                                if (isClosure == null)
                                {
                                    btngvComplianceGridClosure.Visible = true;
                                    lblStatus.Visible = false;
                                }
                                else
                                {
                                    var isTransaction = EventManagement.CheckComplianceTrasactionClosure(CompliaceID, Convert.ToInt64(Request.QueryString["EventScheduledOnID"]), isClosure.FromDate, glParentID, 0, glsubEventID);

                                    if (isTransaction == false)
                                    {
                                        btngvComplianceGridClosure.Visible = true;
                                        lblStatus.Visible = false;
                                    }
                                    else
                                    {
                                        btngvComplianceGridClosure.Visible = false;
                                        lblStatus.Visible = true;
                                    }
                                }
                            }
                            else
                            {
                                btngvComplianceGridClosure.Visible = false;
                                lblStatus.Visible = false;
                            }
                        }
                    }
                    else if (type == "Statutory")
                    {
                        lblStatus.Visible = false;
                        Boolean transcomplete = CheckCompleteTransactionComplete(glParentID, 0, glsubEventID, CompliaceID, Convert.ToInt32(Request.QueryString["EventScheduledOnID"]));
                        if (transcomplete == true)
                        {
                            lblStatus.Visible = true;
                            btngvComplianceGridClosure.Visible = false;
                        }
                        else
                        {
                            var isVisibleClosure = visibleClosure(CompliaceID);
                            if (isVisibleClosure == true)
                            {
                                var isClosure = GetEventComplianceClosure(CompliaceID, Convert.ToInt64(Request.QueryString["EventScheduledOnID"]), glParentID, 0, glsubEventID);
                                if (isClosure == null)
                                {
                                    btngvComplianceGridClosure.Visible = true;
                                    lblStatus.Visible = false;
                                }
                                else
                                {
                                    var isTransaction = EventManagement.CheckComplianceTrasactionClosure(CompliaceID, Convert.ToInt64(Request.QueryString["EventScheduledOnID"]), isClosure.FromDate, glParentID, 0, glsubEventID);

                                    if (isTransaction == false)
                                    {
                                        btngvComplianceGridClosure.Visible = true;
                                        lblStatus.Visible = false;
                                    }
                                    else
                                    {
                                        btngvComplianceGridClosure.Visible = false;
                                        lblStatus.Visible = true;
                                    }
                                }
                            }
                            else
                            {
                                btngvComplianceGridClosure.Visible = false;
                                lblStatus.Visible = false;
                            }
                        }
                    }
                    else
                    {
                        Boolean transcomplete = CheckCompleteTransactionComplete(glParentID, 0, glsubEventID, CompliaceID, Convert.ToInt32(Request.QueryString["EventScheduledOnID"]));
                        if (transcomplete == true)
                        {
                            lblStatus.Visible = true;
                        }
                        else
                        {
                            lblStatus.Visible = false;
                        }
                    }

                }
                else //Internal
                {
                    Label lblStatus = (e.Row.FindControl("lblStatus") as Label);

                    Boolean transcomplete = CheckInternalCompleteTransactionComplete(glParentID, 0, glsubEventID, CompliaceID, Convert.ToInt32(Request.QueryString["EventScheduledOnID"]));
                    if (transcomplete == true)
                    {
                        lblStatus.Visible = true;
                    }
                    else
                    {
                        lblStatus.Visible = false;
                    }
                }
            }
        }
        protected void gvComplianceGrid_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                //GridViewRow masterrow = (GridViewRow)(sender as Control).Parent.Parent;
                //int interIndex = masterrow.RowIndex;
                //GridView childGrid = (GridView)gvParentGrid.Rows[0].Cells[0].FindControl("gvChildGrid");

                //GridView complianceGrid = (GridView)childGrid.Rows[interIndex].Cells[0].FindControl("gvComplianceGrid");
                //int complianceID = Convert.ToInt32(complianceGrid.Rows[e.RowIndex].Cells[0].Text);

                GridView gvComplianceGrid = (GridView)sender;
                int complianceID = Convert.ToInt32(gvComplianceGrid.Rows[e.RowIndex].Cells[0].Text);

                int ParentEventID = Convert.ToInt32((gvComplianceGrid.Rows[e.RowIndex].FindControl("lblgvSParentEventID") as Label).Text);
                int IntermediateEventID = Convert.ToInt32((gvComplianceGrid.Rows[e.RowIndex].FindControl("lblgvSIntermediateEventID") as Label).Text);
                int SubEventID = Convert.ToInt32((gvComplianceGrid.Rows[e.RowIndex].FindControl("lblgvSSubeventID") as Label).Text);

                int ComplianceInstanceID = Convert.ToInt32(EventManagement.GetComplianceInstance(complianceID, Convert.ToInt32(Request.QueryString["CustomerBranchID"])).ID);
                //   int EventScheduledOnID = Convert.ToInt32(Session["EventScheduledOnID"]);
                int EventScheduledOnID = Convert.ToInt32(Request.QueryString["EventScheduledOnID"]);

                var SchdeuleDates = EventManagement.GetComplianceSchdeuleDates(ComplianceInstanceID, EventScheduledOnID, ParentEventID, IntermediateEventID, SubEventID);
                var q = SchdeuleDates.Select(t => t.Date).ToList();
                ddlScheduleDates.DataSource = q;
                ddlScheduleDates.DataBind();
                ddlScheduleDates.Items.Insert(0, new ListItem("< Select >", "-1"));
                UpdatePanel3.Update();
                ViewState["ClosurecomplianceID"] = complianceID;
                ViewState["ClosureComplianceInstanceID"] = ComplianceInstanceID;

                ViewState["ClosureParentEventID"] = ParentEventID;
                ViewState["ClosureIntermediateEventID"] = IntermediateEventID;
                ViewState["ClosureSubEventID"] = SubEventID;

                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#modalClosure').modal();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void btnSaveShortNotice_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtShortNoticeDays.Text == "" && rbtnShortnotice.SelectedValue.Equals("1"))
                {
                    lblMessage.Text = "Please enter short notice days";
                }
                else
                {
                    lblMessage.Text = "";
                    SaveShortNotice();
                   // BindParentEventData(Convert.ToInt32(ParentID));
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#modalShortNotice').modal('hide');", true);
                    upshortnotice.Update();
                    //int ParentID = Convert.ToInt32(Request.QueryString["ID"]);
                    //BindParentEventData(ParentID);
                    //BindInternalCompliance(CustomerId);
                    //DateTime date = DateTime.Now;

                    //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                   // upOptionalCompliances.Update();
                }
            }
            catch (Exception ex)
            {                                                     
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void SaveShortNotice()
        {
            try
            {
                int interIndex = Convert.ToInt32(ViewState["interIndex"]);
                int RowIndex = Convert.ToInt32(ViewState["RowIndex"]);
                int ParentEventID = Convert.ToInt32(ViewState["ParentEventID"]);
                int IntermediateEventID = Convert.ToInt32(ViewState["IntermediateEventID"]);
                int SubEventID = Convert.ToInt32(ViewState["SubEventID"]);
                int EventScheduledOnID = Convert.ToInt32(ViewState["EventScheduledOnID"]);

                GridView complianceGrid = new GridView();
                GridView childGrid = new GridView();
                Button btnSave = new Button();
                Button btnUpdate = new Button();
                var textbox2 = new TextBox();
                Boolean UpdateFlag = false;


                if (ParentEventID != 0 && IntermediateEventID == 0 && SubEventID == 0) //Parent
                {
                    complianceGrid = (GridView)childGrid.Rows[RowIndex].Cells[0].FindControl("gvParentToComplianceGrid");

                    btnSave = (Button)childGrid.Rows[RowIndex].Cells[0].FindControl("BtnAddgvParentGrid");
                    btnUpdate = (Button)childGrid.Rows[RowIndex].Cells[0].FindControl("BtnUpdategvParentGrid");

                    if (btnSave.Visible == true)
                    {
                        UpdateFlag = false;
                    }
                    else if (btnUpdate.Visible == true)
                    {
                        UpdateFlag = true;
                    }

                    textbox2 = childGrid.Rows[RowIndex].FindControl("txtgvParentGridDate") as TextBox;
                }
                else if (ParentEventID != 0 && IntermediateEventID == 0 && SubEventID != 0) //ParentToSub
                {
                    childGrid = (GridView)gvParentGrid.Rows[0].Cells[0].FindControl("gvChildGrid");

                    textbox2 = childGrid.Rows[RowIndex].FindControl("txtgvChildGridDate") as TextBox;

                    btnSave = (Button)childGrid.Rows[RowIndex].Cells[0].FindControl("BtnAddgvChildGrid");
                    btnUpdate = (Button)childGrid.Rows[RowIndex].Cells[0].FindControl("BtnUpdategvChildGrid");

                    if (btnSave.Visible == true)
                    {
                        UpdateFlag = false;
                    }
                    else if (btnUpdate.Visible == true)
                    {
                        UpdateFlag = true;
                    }

                    complianceGrid = (GridView)childGrid.Rows[RowIndex].Cells[0].FindControl("gvComplianceGrid");

                }
                else if (ParentEventID != 0 && IntermediateEventID != 0 && SubEventID != 0) //ParentToIntermediateToSub
                {
                    GridView intermediateGrid = (GridView)gvParentGrid.Rows[0].Cells[0].FindControl("gvChildGrid");

                    childGrid = (GridView)intermediateGrid.Rows[interIndex].Cells[0].FindControl("gvIntermediateSubEventGrid");

                    complianceGrid = (GridView)childGrid.Rows[RowIndex].Cells[0].FindControl("gvIntermediateComplainceGrid");

                    btnSave = (Button)childGrid.Rows[RowIndex].Cells[0].FindControl("BtnAddIntermediateSubEventGrid");
                    btnUpdate = (Button)childGrid.Rows[RowIndex].Cells[0].FindControl("BtnUpdateIntermediateSubEventGrid");

                    if (btnSave.Visible == true)
                    {
                        UpdateFlag = false;
                    }
                    else if (btnUpdate.Visible == true)
                    {
                        UpdateFlag = true;
                    }

                    textbox2 = childGrid.Rows[RowIndex].FindControl("txtgvIntermediateSubEventGridDate") as TextBox;
                }


                DateTime Date = DateTime.ParseExact(textbox2.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                EventAssignDate eventAssignDate = new EventAssignDate()
                {
                    ParentEventID = ParentEventID,
                    EventScheduleOnID = EventScheduledOnID,
                    IntermediateEventID = IntermediateEventID,
                    SubEventID = SubEventID,
                    Date = Date,
                    IsActive = true,
                    CreatedDate = DateTime.Now,
                    CreatedBy = Convert.ToInt32(Session["userID"]),
                    Type = Type,
                };
                Business.EventManagement.UpdateEventAssignDates(eventAssignDate);

                // Child Compliance 

                foreach (GridViewRow EvenToCompliancerow in complianceGrid.Rows)
                {
                    int ComplinaceID = Convert.ToInt32(EvenToCompliancerow.Cells[0].Text);

                    int IsInternal = Convert.ToInt32(EvenToCompliancerow.Cells[3].Text);

                    if (IsInternal == 0)  //Statutory
                    {
                        Boolean VisibleFlag = Business.EventManagement.CheckComplianceVisible(ComplinaceID);
                        if (VisibleFlag == true)  //If complaince visible true then Schudule generated
                        {
                            int ComplianceInstanceID = Convert.ToInt32(EventManagement.GetComplianceInstance(ComplinaceID, Convert.ToInt32(Request.QueryString["CustomerBranchID"])).ID);
                            Boolean IsComplianceChecklistStatutory = EventManagement.CheckComplianceStatutoryChecklist(ComplinaceID);

                            if (IsComplianceChecklistStatutory == true)
                            {
                                //Change Generate flag Schedule change
                                EventManagement.UpdateComplianceInstanceScheduleFlag(ComplinaceID, ComplianceInstanceID);
                            }
                            string days = Convert.ToString(EvenToCompliancerow.Cells[2].Text);
                            Boolean FlgCheck = false;

                            if (rbtnShortnotice.SelectedValue.Equals("1")) //Short Notice Selected 
                            {
                                FlgCheck = EventManagement.CheckShortnoticeCompliance(ComplinaceID);
                                if (FlgCheck == false)
                                {
                                    // Normal Schedule Save
                                    Boolean flgSheduelePresetnt = false;
                                    flgSheduelePresetnt = EventManagement.CheckEventComplianceAssigned(ParentEventID, IntermediateEventID, SubEventID, Type, ComplinaceID, EventScheduledOnID, ComplianceInstanceID);

                                    if (flgSheduelePresetnt == false) //Save
                                    {
                                        EventManagement.GenerateEventComplianceScheduele(ParentEventID, IntermediateEventID, SubEventID, EventScheduledOnID, Date, ComplinaceID, ComplianceInstanceID, days);
                                    }
                                    else //Update
                                    {
                                        int IncludeDays = 0;
                                        var compdetails = EventManagement.GetCompliance(ComplinaceID);
                                        if ((compdetails.ComplianceType == 0 && (compdetails.IsFrequencyBased == false || compdetails.IsFrequencyBased == null)) || compdetails.ComplianceType == 1)
                                        {
                                            int Days = Convert.ToInt32(days);
                                            if (Days == 0)
                                            {
                                                IncludeDays = 0;
                                            }
                                            else
                                            {
                                                if (Days > 0)
                                                {
                                                    IncludeDays = Days - 1;
                                                }
                                                else
                                                {
                                                    IncludeDays = Days + 1;
                                                }
                                            }
                                            DateTime UpdatedDate = Date.AddDays(IncludeDays);
                                            EventManagement.UpdateReminderSheduleOnDates(EventScheduledOnID, ComplinaceID, UpdatedDate, ComplianceInstanceID, Date);
                                            EventManagement.UpdateComplianceSheduleOnDates(ParentEventID, IntermediateEventID, SubEventID, EventScheduledOnID, ComplinaceID, ComplianceInstanceID, UpdatedDate);
                                        }
                                        else
                                        {
                                            EventManagement.RemoveComplianceScheduleEventDateUpdation(ComplinaceID, ParentEventID, IntermediateEventID, SubEventID, ComplianceInstanceID, EventScheduledOnID);
                                            EventManagement.GenerateEventComplianceScheduele(ParentEventID, IntermediateEventID, SubEventID, EventScheduledOnID, Date, ComplinaceID, ComplianceInstanceID, days);
                                        }
                                    }
                                }
                                else
                                {
                                    // Short Notice Schedule 
                                    Boolean flgShortNoticeSheduelePresetnt = false;
                                    flgShortNoticeSheduelePresetnt = EventManagement.CheckShortnoticePresent(ParentEventID, IntermediateEventID, SubEventID, ComplinaceID, EventScheduledOnID, ComplianceInstanceID);
                                    int shortnoticedays = Convert.ToInt32(txtShortNoticeDays.Text);
                                    int shortdays = shortnoticedays * -1;
                                    if (flgShortNoticeSheduelePresetnt == false) //SaveShortNotice 
                                    {
                                        EventManagement.SaveShortNoticeDays(ParentEventID, IntermediateEventID, SubEventID, ComplinaceID, EventScheduledOnID, ComplianceInstanceID, shortdays);
                                        if (UpdateFlag == false) //SaveSchedule
                                        {
                                            EventManagement.GenerateEventComplianceSchedueleForShortNotice(ParentEventID, IntermediateEventID, SubEventID, EventScheduledOnID, Date, ComplinaceID, ComplianceInstanceID, shortdays);
                                        }
                                        else if (UpdateFlag == true) //Update Schedule
                                        {
                                            int IncludeDays = 0;
                                            if (shortdays == 0 || shortdays == -0)
                                            {
                                                IncludeDays = 0;
                                            }
                                            else
                                            {
                                                int Days = Convert.ToInt32(days);
                                                if (Days > 0)
                                                {
                                                    IncludeDays = shortdays - 1;
                                                }
                                                else
                                                {
                                                    IncludeDays = shortdays + 1;
                                                }
                                            }
                                            DateTime UpdatedDate = Date.AddDays(IncludeDays);
                                            EventManagement.UpdateReminderSheduleOnDates(EventScheduledOnID, ComplinaceID, UpdatedDate, ComplianceInstanceID, Date);
                                            EventManagement.UpdateComplianceSheduleOnDates(ParentEventID, IntermediateEventID, SubEventID, EventScheduledOnID, ComplinaceID, ComplianceInstanceID, UpdatedDate);

                                        }
                                    }
                                    else //Update
                                    {
                                        int IncludeDays = 0;
                                        if (shortdays == 0 || shortdays == -0)
                                        {
                                            IncludeDays = 0;
                                        }
                                        else
                                        {
                                            int Days = Convert.ToInt32(days);
                                            if (Days > 0)
                                            {
                                                IncludeDays = shortdays - 1;
                                            }
                                            else
                                            {
                                                IncludeDays = shortdays + 1;
                                            }
                                        }
                                        DateTime UpdatedDate = Date.AddDays(IncludeDays);
                                        EventManagement.UpdateShortNotice(ParentEventID, IntermediateEventID, SubEventID, ComplinaceID, EventScheduledOnID, ComplianceInstanceID, shortdays);
                                        EventManagement.UpdateReminderSheduleOnDates(EventScheduledOnID, ComplinaceID, UpdatedDate, ComplianceInstanceID, Date);
                                        EventManagement.UpdateComplianceSheduleOnDates(ParentEventID, IntermediateEventID, SubEventID, EventScheduledOnID, ComplinaceID, ComplianceInstanceID, UpdatedDate);
                                    }

                                }
                            }
                            else //Short Notice Not selected 
                            {
                                // Normal Schedule Save
                                Boolean flgSheduelePresetnt = false;
                                flgSheduelePresetnt = EventManagement.CheckEventComplianceAssigned(ParentEventID, IntermediateEventID, SubEventID, Type, ComplinaceID, EventScheduledOnID, ComplianceInstanceID);

                                if (flgSheduelePresetnt == false) //Save
                                {

                                    EventManagement.GenerateEventComplianceScheduele(ParentEventID, IntermediateEventID, SubEventID, EventScheduledOnID, Date, ComplinaceID, ComplianceInstanceID, days);
                                }
                                else //Update
                                {
                                    int IncludeDays = 0;
                                    DateTime scheduledOnDate = DateTime.Now;
                                    var compdetails = EventManagement.GetCompliance(ComplinaceID);
                                    if (compdetails.ComplianceType == 0 && (compdetails.IsFrequencyBased == false || compdetails.IsFrequencyBased == null))
                                    {
                                        int Days = Convert.ToInt32(days);
                                        if (Days == 0)
                                        {
                                            IncludeDays = 0;
                                        }
                                        else
                                        {
                                            if (Days > 0)
                                            {
                                                IncludeDays = Days - 1;
                                            }
                                            else
                                            {
                                                IncludeDays = Days + 1;
                                            }
                                        }
                                        DateTime UpdatedDate = Date.AddDays(IncludeDays);
                                        EventManagement.UpdateReminderSheduleOnDates(EventScheduledOnID, ComplinaceID, UpdatedDate, ComplianceInstanceID, Date);
                                        EventManagement.UpdateComplianceSheduleOnDates(ParentEventID, IntermediateEventID, SubEventID, EventScheduledOnID, ComplinaceID, ComplianceInstanceID, UpdatedDate);
                                    }
                                }
                            }
                        }
                    }
                    else if (IsInternal == 1)  //Internal
                    {
                        int ComplianceInstanceID = Convert.ToInt32(EventManagement.GetInternalComplianceInstance(ComplinaceID, Convert.ToInt32(Request.QueryString["CustomerBranchID"])).ID);
                        int days = Convert.ToInt32(EvenToCompliancerow.Cells[2].Text);
                        // Internal Normal Schedule Save
                        Boolean flgSheduelePresetnt = false;
                        flgSheduelePresetnt = EventManagement.CheckEventInternalComplianceAssigned(ParentEventID, IntermediateEventID, SubEventID, ComplinaceID, EventScheduledOnID, ComplianceInstanceID);

                        if (flgSheduelePresetnt == false) //Save
                        {
                            EventManagement.GenerateEventInternalComplianceScheduele(ParentEventID, IntermediateEventID, SubEventID, EventScheduledOnID, Date, ComplinaceID, ComplianceInstanceID, days);
                        }
                        else //Update
                        {
                            int IncludeDays = 0;
                            if (days == 0)
                            {
                                IncludeDays = 0;
                            }
                            else
                            {
                                if (days > 0)
                                {
                                    IncludeDays = days - 1;
                                }
                                else
                                {
                                    IncludeDays = days + 1;
                                }
                            }
                            DateTime UpdatedDate = Date.AddDays(IncludeDays);
                            EventManagement.UpdateInternalReminderSheduleOnDates(EventScheduledOnID, ComplinaceID, UpdatedDate, ComplianceInstanceID, Date);
                            EventManagement.UpdateInternalComplianceSheduleOnDates(ParentEventID, IntermediateEventID, SubEventID, EventScheduledOnID, ComplinaceID, ComplianceInstanceID, UpdatedDate);
                        }
                    }
                }
                txtShortNoticeDays.Text = "";
                // BindParentEventData(Convert.ToInt32(ParentEventID), Convert.ToInt32(EventScheduledOnID), Convert.ToInt32(Type));
                BindParentEventData (Convert.ToInt32(ParentEventID));
               
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Saved Successfully.";
                upOptionalCompliances.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void btnClosure_Click(object sender, EventArgs e)
        {
            try
            {
                long complianceID = Convert.ToInt64(ViewState["ClosurecomplianceID"]);
                long ClosureComplianceInstanceID = Convert.ToInt64(ViewState["ClosureComplianceInstanceID"]);
                int EventScheduledOnID = Convert.ToInt32(Request.QueryString["EventScheduledOnID"]);

                long ParentEventID = Convert.ToInt64(ViewState["ClosureParentEventID"]);
                long IntermediateEventID = Convert.ToInt64(ViewState["ClosureIntermediateEventID"]);
                long SubEventID = Convert.ToInt64(ViewState["ClosureSubEventID"]);

                var FlagExist = EventManagement.CheckEventComplianceClosureExist(complianceID, ClosureComplianceInstanceID, EventScheduledOnID, ParentEventID, IntermediateEventID, SubEventID);

                DateTime dtScheduleDate = Convert.ToDateTime(ddlScheduleDates.SelectedValue);
                EventComplianceClosure data = new EventComplianceClosure()
                {
                    EventScheduleOnID = EventScheduledOnID,
                    ComplianceID = complianceID,
                    ComplianceInstanceID = ClosureComplianceInstanceID,
                    Remark = txtClosureRemark.Text,
                    FromDate = dtScheduleDate,
                    CreatedOn = DateTime.Now,
                    CreateBy = AuthenticationHelper.UserID,
                    ParentEventD = ParentEventID,
                    IntermediateEventID = IntermediateEventID,
                    SubEventID = SubEventID
                };

                if (FlagExist == false)
                {
                    Business.EventManagement.CreateEventComplianceClosure(data);
                    lblImsg1.Text = "Compliance schedule closed entry save successfully";
                }
                else
                {
                    EventManagement.UpdateEventComplianceClosure(data);
                    lblImsg1.Text = "Compliance schedule closed entry update successfully";
                }

                EventManagement.RemoveComplianceScheduleFromClosure(complianceID, EventScheduledOnID, dtScheduleDate, ParentEventID, IntermediateEventID, SubEventID);
                BindParentEventData(Convert.ToInt32(ParentEventID));
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#modalClosure').hide();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void rbtnShortnotice_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                lblMessage.Text = "";
                if (rbtnShortnotice.SelectedValue.Equals("1"))
                {
                    txtShortNoticeDays.Text = "";
                    divDays.Visible = true;
                }
                else
                {
                    txtShortNoticeDays.Text = "";
                    divDays.Visible = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindInternalCompliance(int CustomerbranchID)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                ddlCompliance.Items.Clear();

                ddlCompliance.DataTextField = "IShortDescription";
                ddlCompliance.DataValueField = "ID";

                ddlCompliance.DataSource = EventManagement.GetInternalEventCompliance(customerID, CustomerbranchID);
                ddlCompliance.DataBind();

                ddlCompliance.Items.Insert(0, new ListItem("< Select Compliance >", "-1"));

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void btnInternalComplianceAdd_Click(object sender, EventArgs e)
        {
            try
            {
                int ParentEventID = Convert.ToInt32(ViewState["IParentEventID"]);
                int IntermediateEventID = Convert.ToInt32(ViewState["IIntermediateEventID"]);
                int SubEventID = Convert.ToInt32(ViewState["ISubEventID"]);
                int EventScheduledOnID = Convert.ToInt32(ViewState["IEventScheduledOnID"]);
                int ComplinaceID = Convert.ToInt32(ddlCompliance.SelectedValue);
                var FlagExist = EventManagement.CheckEventInternalComplianceMappingExist(ComplinaceID, ParentEventID, IntermediateEventID, SubEventID);

                bool flagOccurance;
                if (rbtInternalOccurance.SelectedValue.Equals("1")) //One time
                    flagOccurance = true;
                else
                    flagOccurance = false;

                EventInternalComplianceMapping data = new EventInternalComplianceMapping()
                {
                    EventScheduleOnID = EventScheduledOnID,
                    ParentEventID = ParentEventID,
                    IntermediateEventID = IntermediateEventID,
                    SubEventID = SubEventID,
                    InternalComplianceID = ComplinaceID,
                    Days = Convert.ToInt32(txtDays.Text),
                    IsOneTime = flagOccurance,
                    IsActive = true,
                    CreatedDate = DateTime.Now,
                    CreatedBy = AuthenticationHelper.UserID,
                };

                if (FlagExist == false)
                {
                    Business.EventManagement.CreateEventInternalComplianceMapping(data);
                    lblImsg.Text = "Internal Compliance Added successfully";
                }
                else
                {
                    DailyUpdateManagment.UpdateEventInternalComplianceMapping(data);
                    lblImsg.Text = "Internal Compliance updated successfully";
                }
                //   BindParentEventData(ParentEventID, EventScheduledOnID, Type);
               // BindParentEventData(Convert.ToInt32(ParentEventID));

                DateTime Date = Convert.ToDateTime(ViewState["IDate"]);


                if (Date.ToString() != "1/1/0001 12:00:00 AM")
                {
                    //Add Internal Scheduele
                    int ComplianceInstanceID = Convert.ToInt32(EventManagement.GetInternalComplianceInstance(ComplinaceID, Convert.ToInt32(Request.QueryString["CustomerBranchID"])).ID);
                    int days = Convert.ToInt32(txtDays.Text);
                    Boolean flgSheduelePresetnt = false;
                    flgSheduelePresetnt = EventManagement.CheckEventInternalComplianceAssigned(ParentEventID, IntermediateEventID, SubEventID, ComplinaceID, EventScheduledOnID, ComplianceInstanceID);

                    if (flgSheduelePresetnt == false) //Save
                    {
                        EventManagement.GenerateEventInternalComplianceScheduele(ParentEventID, IntermediateEventID, SubEventID, EventScheduledOnID, Date, ComplinaceID, ComplianceInstanceID, days);
                    }
                    else //Update
                    {
                        int IncludeDays = 0;
                        if (days == 0)
                        {
                            IncludeDays = 0;
                        }
                        else
                        {
                            if (days > 0)
                            {
                                IncludeDays = days - 1;
                            }
                            else
                            {
                                IncludeDays = days + 1;
                            }
                        }
                        DateTime UpdatedDate = Date.AddDays(IncludeDays);
                        EventManagement.UpdateInternalReminderSheduleOnDates(EventScheduledOnID, ComplinaceID, UpdatedDate, ComplianceInstanceID, Date);
                        EventManagement.UpdateInternalComplianceSheduleOnDates(ParentEventID, IntermediateEventID, SubEventID, EventScheduledOnID, ComplinaceID, ComplianceInstanceID, UpdatedDate);
                    }
                }
                BindParentEventData(Convert.ToInt32(ParentEventID));
                // BindParentEventData(Convert.ToInt32(ParentEventID), Convert.ToInt32(EventScheduledOnID), Convert.ToInt32(Type));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
    }
}
