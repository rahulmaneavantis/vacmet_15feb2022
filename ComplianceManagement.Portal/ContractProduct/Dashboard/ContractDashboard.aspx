﻿<%@ Page Title="My Dashboard" Language="C#" MasterPageFile="~/ContractProduct.Master" AutoEventWireup="true" CodeBehind="ContractDashboard.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.Dashboard.ContractDashboard" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            setactivemenu('leftdashboardmenu');
            fhead('My Dashboard');
        });

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }
          function CloseDocTypePopup() {
              $('#divShowDialog').modal('hide');
          
                  document.getElementById('<%= lnkBtnBindGrid1.ClientID %>').click();
        }
        function OpenDocTypePopup(MilestoneId, ContractID) {

            var modalHeight = screen.height - 150;

            if (modalHeight < 0)
                modalHeight = 200;

            $('#divShowDialog').modal('show');
            $('.modal-dialog').css('width', '95%');
            $('#showdetails').attr('width', '100%');
            $('#showdetails').attr('height', modalHeight + "px");
            $('#showdetails').attr('src', "/ContractProduct/aspxPages/MileStoneUpdate.aspx?AccessID=" + ContractID + "&MilestoneID=" + MilestoneId);
           
        }

        $(document).on("click", function (event) {
            if (event.target.id == "") {
                var idvid = $(event.target).closest('div');
                if ($(idvid).attr('id') != undefined && $(idvid).attr('id') != '' && $(idvid).attr('id').indexOf('<%= tvFilterLocation.ClientID %>') > -1) {
                    $("#divFilterLocation").show();
                } else {
                    $("#divFilterLocation").hide();
                }
            } else if (event.target.id != '<%= tbxFilterLocation.ClientID %>') {
                $("#divFilterLocation").hide();
            } else if (event.target.id != "" && event.target.id.indexOf('<%= tvFilterLocation.ClientID %>') > -1) {
                $("#divFilterLocation").show();
            } else if (event.target.id == '<%= tbxFilterLocation.ClientID %>') {
                $('<%= tbxFilterLocation.ClientID %>').unbind('click');

                $('<%= tbxFilterLocation.ClientID %>').click(function () {
                    $("#divFilterLocation").toggle("blind", null, 500, function () { });
                });
            }
        });

         function ShowContractDialog(ContractInstanceID) {
             var modalHeight = screen.height - 150;

             if (modalHeight < 0)
                 modalHeight = 200;

             $('#divShowDialog').modal('show');
             $('.modal-dialog').css('width', '95%');
             $('#showdetails').attr('width', '100%');
             $('#showdetails').attr('height', modalHeight + "px");
             $('#showdetails').attr('src', "/ContractProduct/aspxPages/ContractDetailsPage.aspx?AccessID=" + ContractInstanceID);
         };

         function ShowContractCommentDialog(ContractInstanceID, RoleID) {
             var modalHeight = screen.height - 150;

             if (modalHeight < 0)
                 modalHeight = 200;

             $('#divShowDialog').modal('show');
             $('.modal-dialog').css('width', '95%');
             $('#showdetails').attr('width', '100%');
             $('#showdetails').attr('height', modalHeight + "px");
             if (RoleID == 0) {
           
                 $('#showdetails').attr('src', "/ContractProduct/aspxPages/ContractDetailsPage.aspx?AccessID=" + ContractInstanceID);
             }
             else
             {
              
                 $('#showdetails').attr('src', "/ContractProduct/aspxPages/ContractDetailsPage.aspx?AccessID=" + ContractInstanceID + "&AccessRoleID=" + RoleID);
             }               
         };

         function ShowTaskDialog(contractID, taskID, userID, roleID, checkSum) {

             var modalHeight = screen.height - 200;

             if (modalHeight < 0)
                 modalHeight = screen.height;

             //alert("Screen Height-" + screen.height + "\n Modal Height-" + modalHeight);

             $('#divShowDialog').modal('show');
             $('.modal-dialog').css('width', '95%');
             $('#showdetails').attr('width', '100%');
             $('#showdetails').attr('height', modalHeight + "px");
             $('#showdetails').attr('src', "/ContractProduct/aspxPages/ContractTaskDetailsPage.aspx?A=" + contractID + "&B=" + taskID + "&C=" + userID + "&D=" + roleID + "&Checksum=" + checkSum);
         };
    </script>
    <style type="text/css">
      .seven-cols .col-md-1, .seven-cols .col-sm-1, .seven-cols .col-lg-1 
          {
          width: 12.5%;
          }
         #collapseDivFilters > div > div > div > div > div > div > a.bx-prev {
            left: 0%;
        }

        #collapseDivFilters > div > div > div > div > div > div > a.bx-prev {
            left: 0%;
        }
        @media screen and (max-width: 750px) {
            iframe {
                max-width: 100% !important;
                width: auto !important;
                height: auto !important;
            }
        }

        .mang-dashboard-white-widget {
            background: #fff;
            padding: 5px 0px 5px 0px;
            margin-bottom: 10px;
            border-radius: 10px;
        }

        .panel {
            margin-bottom: 10px;
        }

            .panel .panel-heading {
                border: none;
                background: #fff;
                padding: 0;
            }

                .panel .panel-heading h2 {
                    font-size: 20px;
                }


            .panel .panel-heading-dashboard {
                border: none;
                background: #fff;
                padding: 0;
            }

        .info-box {
            min-height: 110px;
            margin-bottom: 10px;
            /*border: 1px solid #ADD8E6;*/
        }

            .info-box:hover {
                color: #FF7473;
                font-weight: 500;
                -webkit-transform: scale(1.1);
                -ms-transform: scale(1.1);
                transform: scale(1.1);
                z-index: 5;
            }

        .info-box-task {
            min-height: 140px;
            margin-bottom: 18px;
            width: 95%;
            border-radius: 10px;
            padding: 10px 10px 0px 10px;
            color: #666;
            -webkit-box-shadow: inset 0 0 1px 1px rgba(255,255,255,.35), 0 1px 1px -1px rgba(0,0,0,.1);
            -moz-box-shadow: inset 0 0 1px 1px rgba(255,255,255,.35),0 1px 1px -1px rgba(0,0,0,.1);
            box-shadow: inset 0 0 1px 1px rgba(255,255,255,.35), 0 1px 1px -1px rgba(0,0,0,.1);
        }

        .info-box-task {
            min-height: 75px;
            margin-bottom: 10px;
            /*border: 1px solid #ADD8E6;*/
        }

            .info-box-task .count {
                font-size: 23px;
                color: #1fd9e1;
                text-align: center;
            }

            .info-box-task .desc {
                font-size: 14px;
                text-align: center;
            }

            .info-box-task .count:hover {
                color: #FF7473;
                font-weight: 500;
                -webkit-transform: scale(1.1);
                -ms-transform: scale(1.1);
                transform: scale(1.1);
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

       <div style="float: right; margin-right: 6px;">            
                            <asp:Label ID="Label1" runat="server" Text="" Style="color:#666;margin-right: 8px;"  ></asp:Label>
                            <asp:LinkButton ID="btnRefresh1" runat="server" OnClick="btnRefresh_Click" ToolTip="Refresh Now" data-toggle="tooltip">
                            <label style="font-weight: bold;color: blue; cursor: pointer;">Refresh Now</label>              
                            </asp:LinkButton>           
                        </div>
    <br />
     <div class="dashboard">
        <div class="col-md-12 colpadding0">
            <asp:ValidationSummary ID="vsContractDashboard" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                ValidationGroup="ContractDashboardValidationGroup" />
            <asp:CustomValidator ID="cvContractDashboard" runat="server" EnableClientScript="False"
                ValidationGroup="ContractDashboardValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
        </div>

        <!-- Top Count start -->
         <div id="countSummary" class="col-lg-12 col-md-12 mb10 plr0">
             <div class="panel panel-default" style="background: none;">
                 <div class="panel-heading" style="background: none;">
                     <h2>Contract Owner Summary</h2>
                 </div>
             </div>
         </div>
         
        <div id="divTabs" runat="server" class="row seven-cols" style="width:990px">
             <div id="draftCount" runat="server" class="col-lg-1 col-md-1 col-sm-1 TMB plr5">
                 <div class="info-box indigo-bg w100per">
                     <div class="div-location" style="cursor: pointer;">
                         <a href="/ContractProduct/aspxPages/ContractList.aspx?Status=Draft">
                             <div class="col-md-5 colpadding0 mt10">
                                 <img src="/img/Draft.png" height="55" />
                             </div>
                             <div class="col-md-7 colpadding0">
                                 <div class="titleMD">Draft</div>
                                 <div id="divDraftCount" runat="server" class="countMD">0</div>
                             </div>
                         </a>
                     </div>
                 </div>
             </div>

             <div id="pendingReviewCount" runat="server" class="col-lg-1 col-md-1 col-sm-1 TMB plr5">
                 <div class="info-box seablue-bg w100per">
                     <div class="div-location" style="cursor: pointer">
                         <a href="/ContractProduct/aspxPages/ContractList.aspx?Status=Pending Review">
                             <div class="col-md-5 colpadding0 mt10" style="margin-left:-7px">
                                 <img src="/img/Pending-Review.png" height="55" />
                             </div>
                             <div class="col-md-7 colpadding0">
                                 <div class="titleMD" style="margin-left:-4px">Pending Review</div>
                                 <div id="divPendingReviewCount" runat="server" class="countMD">0</div>
                             </div>
                         </a>
                     </div>
                 </div>
             </div>

             <div id="reviewedCount" runat="server" class="col-lg-1 col-md-1 col-sm-1 TMB plr5">
                 <div class="info-box bluenew-bg w100per">
                     <div class="div-location" style="cursor: pointer">
                         <a href="/ContractProduct/aspxPages/ContractList.aspx?Status=Review Completed">
                             <div class="col-md-5 colpadding0 mt10" style="margin-left:-7px">
                                 <img src="/img/Reviewed.png" height="55" />
                             </div>
                             <div class="col-md-7 colpadding0">
                                 <div class="titleMD" style="margin-left: -9px;">Reviewed</div>
                                 <div id="divReviewedCount" runat="server" class="countMD">0</div>
                             </div>
                         </a>
                     </div>
                 </div>
             </div>

             <div id="pendingApprovalCount" runat="server" class="col-lg-1 col-md-1 col-sm-1 TMB plr5">
                 <div class="info-box rednew-bg w100per">
                     <div class="div-location" style="cursor: pointer">
                         <a href="/ContractProduct/aspxPages/ContractList.aspx?Status=Pending Approval">
                             <div class="col-md-5 colpadding0 mt10"style="margin-left: -10px;">
                                 <img src="/img/Pending-approval.png" height="55" />
                             </div>
                             <div class="col-md-7 colpadding0">
                                 <div class="titleMD">Pending Approval</div>
                                 <div id="divPendingApprovalCount" runat="server" class="countMD">0</div>
                             </div>
                         </a>
                     </div>
                 </div>
             </div>

             <div id="approvedcount" runat="server" class="col-lg-1 col-md-1 col-sm-1 TMB plr5">
                 <div class="info-box greennew-bg w100per">
                     <div class="div-location" style="cursor: pointer;">
                         <a href="/ContractProduct/aspxPages/ContractList.aspx?Status=Approval Completed">
                             <div class="col-md-5 colpadding0 mt10" style="margin-left: -8px;">
                                 <img src="/img/Reviewed.png" height="55" />
                             </div>
                             <div class="col-md-7 colpadding0">
                                 <div class="titleMD" style="margin-left: -9px;">Approved</div>
                                 <div id="divApprovedcount" runat="server" class="countMD">0</div>
                             </div>
                         </a>
                     </div>
                 </div>
             </div>

            <div id="activeCount" runat="server" class="col-lg-1 col-md-1 col-sm-1 TMB plr5">
                <div class="info-box amber-bg w100per">
                    <div class="div-location" style="cursor: pointer;">
                        <a href="/ContractProduct/aspxPages/ContractList.aspx?Status=Active">
                            <div class="col-md-5 colpadding0 mt10">
                                <img src="/img/Active.png" height="55" />
                            </div>
                            <div class="col-md-7 colpadding0">
                                <div class="titleMD">Active</div>
                                <div id="divActiveCount" runat="server" class="countMD">0</div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>

              <div id="expiredCount" runat="server" class="col-lg-1 col-md-1 col-sm-1 TMB plr5">
                  <div class="info-box seablue-bg w100per">
                      <div class="div-location" style="cursor: pointer">
                          <a href="/ContractProduct/aspxPages/ContractList.aspx?Status=Expired">
                              <div class="col-md-5 colpadding0 mt10">
                                  <img src="/img/Expired.png" height="60" />
                              </div>
                              <div class="col-md-7 colpadding0">
                                  <div class="titleMD">Expired</div>
                                  <div id="divExpiredCount" runat="server" class="countMD">0</div>
                              </div>
                          </a>
                      </div>
                  </div>
             </div>
               <div id="Div1" runat="server" class="col-lg-1 col-md-1 col-sm-1 TMB plr5">
                  <div class="info-box seablue-bg w100per">
                      <div class="div-location" style="cursor: pointer">
                          <a href="/ContractProduct/aspxPages/ContractTemplateList.aspx">
                              <div class="col-md-5 colpadding0 mt10">
                                  <img src="/img/Expired.png" height="60" />
                              </div>
                              <div class="col-md-7 colpadding0" style="margin-left: -7px;">
                                  <div class="titleMD">Reviews</div>
                                  <div id="divReviwesCount" runat="server" class="countMD">0</div>
                              </div>
                          </a>
                      </div>
                  </div>
             </div>
                <div id="divrejected" runat="server" class="col-lg-1 col-md-1 col-sm-1 TMB plr5">
                  <div class="info-box seablue-bg w100per">
                      <div class="div-location" style="cursor: pointer">
                          <a href="/ContractProduct/aspxPages/ContractList.aspx?Status=Rejected">
                              <div class="col-md-5 colpadding0 mt10">
                                  <img src="/img/Expired.png" height="60" />
                              </div>
                              <div class="col-md-7 colpadding0">
                                  <div class="titleMD">Rejected</div>
                                  <div id="divcontractRejectedCount" runat="server" class="countMD">0</div>
                              </div>
                          </a>
                      </div>
                  </div>
             </div>
                <div id="onlyreviewDiv" runat="server" class="col-lg-1 col-md-1 col-sm-1 TMB plr5">
                  <div class="info-box seablue-bg w100per">
                      <div class="div-location" style="cursor: pointer">
                          <a href="/ContractProduct/aspxPages/ContractReview.aspx?Status=0">
                              <div class="col-md-5 colpadding0 mt10">
                                  <img src="/img/Expired.png" height="60" />
                              </div>
                              <div class="col-md-7 colpadding0" style="margin-left:-28px">
                                  <div class="titleMD">Reviews</div>
                                  <div id="divcontractReviwesCount" runat="server" class="countMD">0</div>
                              </div>
                          </a>
                      </div>
                  </div>
             </div>           
             <div id="divcomments" runat="server" class="col-lg-1 col-md-1 col-sm-1 TMB plr5">
                  <div class="info-box seablue-bg w100per">
                      <div class="div-location" style="cursor: pointer">
                          <a href="/ContractProduct/aspxPages/ContractCommentList.aspx?Status=0">
                              <div class="col-md-5 colpadding0 mt10">
                                  <img src="/img/Expired.png" height="60" />
                              </div>
                              <div class="col-md-7 colpadding0">
                                  <div class="titleMD" style="margin-left: -12px;">Comments</div>
                                  <div id="divcontractCommentsCount" runat="server" class="countMD">0</div>
                              </div>
                          </a>
                      </div>
                  </div>
             </div>
          
                <div id="TNotRenewed" runat="server" class="col-lg-1 col-md-1 col-sm-1 TMB plr5" style="margin-left: 986px;margin-top: -120px;width: 120px;">
                  <div class="info-box seablue-bg w100per">
                      <div class="div-location" style="cursor: pointer;margin-top: -10px;">
                          <a href="/ContractProduct/aspxPages/ContractList.aspx?Status=Terminated">
                          <div class="col-md-12 colpadding0 mt12" style="margin-left: 0px;font-size: 14px;color: #fff;margin-top: -1px;"></div>
                          <%--    <div class="col-md-5 colpadding0 mt10">
                                  <img src="/img/Expired.png" height="60" style="margin-top:-10px"/>
                              </div>--%>
                          <%--    <div class="col-md-7 colpadding0"  style="padding-top:17px;margin-top:-10px;">
                                 <%-- <div class="titleMD">Expired</div>--%>
                                <%--  <div id="divTNR" runat="server" class="countMD">0</div>
                              </div>--%>
                                   <div class="col-md-7 colpadding0" style="margin-left: -7px;">
                                  <div class="titleMD">Terminated & Not Renewed</div>
                                  <div id="divTNR" runat="server" class="countMD" style="margin-top: -10px;margin-left: 36px;">0</div>
                              </div>
                          </a>
                      </div>
                  </div>
             </div>
         </div>       
        <!-- Top Count End -->
         
        <!-- Filters-->
        <div class="row d-none">
             <div id="DivFilters" class="row mang-dashboard-white-widget">
                <div class="col-lg-12 col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading" style="margin-left: 5px;">
                            <h2>Filters</h2>
                            <div class="panel-actions">
                                <%--onclick="btnminimize(this)"--%>
                                <a class="btn-minimize"  data-toggle="collapse" data-parent="#accordion" href="#collapseDivFilters">
                                    <i class="fa fa-chevron-down"></i></a>
                            </div>
                        </div>
                        <div id="collapseDivFilters" class="panel-collapse collapse" runat="server">
                            <div class="panel-body">
                                <div class="col-md-12 colpadding0 form-group">

                                    <div class="col-md-3 colpadding0 w20per">
                                        <asp:UpdatePanel ID="upDivLocation" runat="server" UpdateMode="Conditional" OnLoad="upDivLocation_Load">
                                            <ContentTemplate>
                                                <label for="tbxFilterLocation" class="filter-label">Entity/Branch/Location</label>
                                                <asp:TextBox runat="server" ID="tbxFilterLocation" PlaceHolder="Click to Select" autocomplete="off" CssClass="form-control" Width="95%" />
                                                <div style="margin-left: 1px; position: absolute; z-index: 10; overflow-y: auto; height: 200px;" id="divFilterLocation">
                                                    <asp:TreeView runat="server" ID="tvFilterLocation" SelectedNodeStyle-Font-Bold="true" Width="100%" NodeStyle-ForeColor="#8e8e93"
                                                        Style="overflow: auto; border-left: 1px solid #c7c7cc; border-right: 1px solid #c7c7cc; border-bottom: 1px solid #c7c7cc; background-color: #ffffff; color: #8e8e93 !important;" ShowLines="true"
                                                        OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged">
                                                    </asp:TreeView>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>

                                    <div class="col-md-3 colpadding0 w20per">
                                        <label for="ddlDeptPage" class="filter-label">Department</label>
                                        <asp:DropDownListChosen runat="server" ID="ddlDeptPage" AllowSingleDeselect="false" DisableSearchThreshold="3"
                                            DataPlaceHolder="Select Department" class="form-control" Width="95%" />
                                    </div>

                                    <div class="col-md-3 colpadding0 w20per">
                                        <label for="ddlVendorPage" class="filter-label">Vendor</label>
                                        <asp:DropDownListChosen runat="server" ID="ddlVendorPage" AllowSingleDeselect="false" DisableSearchThreshold="3"
                                            DataPlaceHolder="Select Vendor" class="form-control" Width="95%" />
                                    </div>

                                    <div class="col-md-3 colpadding0 w20per">
                                        <label for="ddlStatus" class="filter-label">Status</label>
                                        <asp:DropDownListChosen runat="server" ID="ddlContractStatus" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                            DataPlaceHolder="Select Status" class="form-control" Width="95%">
                                        </asp:DropDownListChosen>
                                    </div>

                                    <div class="col-md-3 colpadding0 w20per">
                                       <label for="ddlContractType" class="filter-label">Contract Type</label>
                                        <asp:DropDownListChosen runat="server" ID="ddlContractType" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                            DataPlaceHolder="Select Type" class="form-control" Width="95%">
                                        </asp:DropDownListChosen>
                                    </div>
                                </div>

                                <div class="clearfix"></div>

                                <div class="col-md-12 colpadding0">
                                    <div class="col-md-3 colpadding0 w20per"></div>
                                    <div class="col-md-3 colpadding0 w20per"></div>
                                    <div class="col-md-3 colpadding0 w20per"></div>
                                    <div class="col-md-3 colpadding0 w20per"></div>
                                    <div class="col-md-3 colpadding0 w20per text-right">
                                        <label for="btnFilter" class="hidden-label">Filter</label>
                                        <div style="float: left">
                                            <asp:Button ID="btnFilter" class="btn btn-primary" runat="server" Text="Apply" OnClick="btnApplyFilter_Click" />
                                        </div>
                                        <div style="float: right;">
                                            <asp:Button ID="btnClearFilter" class="btn btn-primary" runat="server" Text="Clear" OnClick="btnClearFilter_Click" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Filters End-->

          <!-- Comment List-->
          <div class="row">
            <div id="divgrdComments" class="row mang-dashboard-white-widget" runat="server">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12">

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h2>Comment(s)</h2>
                                <div class="panel-actions">
                                    <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapseComments">
                                        <i class="fa fa fa-chevron-up"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div id="collapseComments" class="panel-collapse collapse in">
                                <div class="panel-body" style="max-height: 300px; overflow: auto;">
                                    <div id="divgrddeatilscomments" class="col-md-12 plr0">
                                        <asp:GridView runat="server" ID="grdcomments" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                            GridLines="None" PageSize="5" AutoPostBack="true" CssClass="table" Width="100%" AllowPaging="true" ShowFooter="false">
                                            <Columns>
                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr.No." ItemStyle-Width="5%">
                                                    <ItemTemplate>
                                                        <%#Container.DataItemIndex+1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Contract No" ItemStyle-Width="20%" SortExpression="ContractNo">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px">
                                                            <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ContractNo") %>' ToolTip='<%# Eval("ContractNo") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Contract Title" ItemStyle-Width="20%" SortExpression="ContractTitle">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px">
                                                            <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ContractTitle") %>' ToolTip='<%# Eval("ContractTitle") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Department" ItemStyle-Width="10%" SortExpression="DeptName">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                            <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("DeptName") %>' ToolTip='<%# Eval("DeptName") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Vendor" ItemStyle-Width="15%" SortExpression="VendorNames">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px">
                                                            <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("VendorNames") %>' ToolTip='<%# Eval("VendorNames") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="End Date" ItemStyle-Width="15%" ItemStyle-CssClass="text-center" SortExpression="ExpirationDate">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px">
                                                            <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ExpirationDate") != null ? Convert.ToDateTime(Eval("ExpirationDate")).ToString("dd-MM-yyyy") : "null" %>'
                                                                ToolTip='<%# Eval("ExpirationDate") != null ? Convert.ToDateTime(Eval("ExpirationDate")).ToString("dd-MM-yyyy") : "null" %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Entity" ItemStyle-Width="17%" Visible="false">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                            <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("BranchName") %>' ToolTip='<%# Eval("BranchName") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Department" ItemStyle-Width="10%" Visible="false">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                            <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("DeptName") %>' ToolTip='<%# Eval("DeptName") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Status" ItemStyle-Width="10%" SortExpression="StatusName">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                            <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("StatusName") %>' ToolTip='<%# Eval("StatusName") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="Action" HeaderStyle-Width="5%" ItemStyle-Width="5%" HeaderStyle-CssClass="text-right" ItemStyle-CssClass="text-right">
                                                    <ItemTemplate>                                                        
                                                            <asp:LinkButton ID="lnkBtnViewContractComment" runat="server" OnClick="lnkBtnViewContractComment_Click" CommandArgument='<%# Eval("ID") %>'
                                                               data-toggle="tooltip" data-placement="left" ToolTip="View Comment Detail(s)">
                                                                <img src='<%# ResolveUrl("~/Images/View-icon-new.png")%>' alt="View" />
                                                            </asp:LinkButton>                                                        
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                            </Columns>
                                            <RowStyle CssClass="clsROWgrid" />
                                            <HeaderStyle CssClass="clsheadergrid" />
                                            <PagerTemplate></PagerTemplate>
                                            <EmptyDataTemplate>
                                                No Comments
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                    </div>
                                </div>

                                <div class="col-md-12 text-right">
                                    <asp:LinkButton runat="server" ID="LinkButton2" Text="..Show More"
                                    style="margin-left: -5px;color: blue;font-style: italic;border: none;background: white;"
                                        PostBackUrl="~/ContractProduct/aspxPages/ContractCommentList.aspx"></asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
          <!-- Comment List END-->

        <!-- Contract Expiring-->
        <div class="row">
            <div id="divOuterContractExpiring" class="row mang-dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12">

                        <div class="panel panel-default">
                            <div class="panel-heading"> <%--data-toggle="collapse" data-parent="#accordion" href="#collapseContractExpiring"--%>
                                <div class="float-left">
                                    <h2>Contracts Expiring in</h2>
                                </div>
                                <div class="float-left ml10 mt5">
                                    <asp:DropDownListChosen runat="server" ID="ddlContractExpiry" AllowSingleDeselect="false" DisableSearchThreshold="5" AutoPostBack="true"
                                        DataPlaceHolder="Select" class="form-control" OnSelectedIndexChanged="ddlContractExpiry_SelectedIndexChanged">
                                        <asp:ListItem Text="< 30 Days" Value="30" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="< 60 Days" Value="60"></asp:ListItem>
                                        <asp:ListItem Text="< 90 Days" Value="90"></asp:ListItem>
                                        <asp:ListItem Text="> 90 Days" Value="91"></asp:ListItem>
                                    </asp:DropDownListChosen>
                                </div>
                                <div class="panel-actions">
                                    <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapseContractExpiring">
                                        <i class="fa fa-chevron-up"></i>
                                    </a>
                                </div>
                            </div>

                            <div class="clearfix"></div>

                            <div id="collapseContractExpiring" class="panel-collapse collapse in">
                                <div class="panel-body" style="max-height: 300px; overflow: auto;">  
                                    <div id="divContractExpiring" class="col-md-12 plr0">
                                        <asp:GridView runat="server" ID="grdContractExpiring" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                            GridLines="None" PageSize="5" AutoPostBack="true" CssClass="table" Width="100%" AllowPaging="true" ShowFooter="false">
                                           <%--  PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Right"--%>
                                            <Columns>

                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr.No." ItemStyle-Width="5%" HeaderStyle-Width="5%">
                                                    <ItemTemplate>
                                                        <%#Container.DataItemIndex+1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Contract No." HeaderStyle-Width="20%" ItemStyle-Width="20%">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px">
                                                            <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ContractNo") %>' ToolTip='<%# Eval("ContractNo") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Title" HeaderStyle-Width="30%" ItemStyle-Width="30%">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                                            <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ContractTitle") %>' ToolTip='<%# Eval("ContractTitle") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Vendor" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px">
                                                            <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("VendorNames") %>' ToolTip='<%# Eval("VendorNames") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Expiration Date" HeaderStyle-Width="20%" ItemStyle-Width="20%" ItemStyle-CssClass="text-center">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px">
                                                            <asp:Label runat="server" data-toggle="tooltip" Width="100%" data-placement="bottom" Text='<%# Eval("ExpirationDate") != DBNull.Value ? Convert.ToDateTime(Eval("ExpirationDate")).ToString("dd-MM-yyyy") : "" %>'
                                                                ToolTip='<%# Eval("ExpirationDate") != DBNull.Value ? Convert.ToDateTime(Eval("ExpirationDate")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Status" HeaderStyle-Width="10%" ItemStyle-Width="10%">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                            <asp:Label runat="server" data-toggle="tooltip" Width="100%" data-placement="bottom" Text='<%# Eval("StatusName") %>' ToolTip='<%# Eval("StatusName") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                 <asp:TemplateField HeaderText="Action" HeaderStyle-Width="5%" ItemStyle-Width="5%" HeaderStyle-CssClass="text-right" ItemStyle-CssClass="text-right">
                                                    <ItemTemplate>                                                        
                                                            <asp:LinkButton ID="lnkBtnViewContract" runat="server" OnClick="lnkEditContract_Click" CommandArgument='<%# Eval("ID") %>'
                                                               data-toggle="tooltip" data-placement="left" ToolTip="View Contract Detail(s)">
                                                                <img src='<%# ResolveUrl("~/Images/View-icon-new.png")%>' alt="View" />
                                                            </asp:LinkButton>                                                        
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                            </Columns>
                                            <RowStyle CssClass="clsROWgrid" />
                                            <HeaderStyle CssClass="clsheadergrid" />
                                            <EmptyDataTemplate>
                                                No Records Found.
                                            </EmptyDataTemplate>
                                            <PagerSettings Visible="false" />
                                        </asp:GridView>
                                    </div>
                                </div>
                                <div class="col-md-12 text-right">
                                    <asp:LinkButton runat="server" ID="lnkShowDetailContract" Text="..Show More"
                                    style="margin-left: -5px;color: blue;font-style: italic;border: none;background: white;"
                                        PostBackUrl="~/ContractProduct/aspxPages/ContractList.aspx?Status=0" Visible="false"></asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Contract Expiring END-->      

          <!-- Task Status Count start -->
         <div id="taskStatusCountSummary" class="col-lg-12 col-md-12 mb10 plr0">
             <div class="panel panel-default" style="background: none;">
                 <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseTaskStatusCountSummary" style="background: none;">
                     <h2>Task Summary</h2>
                 </div>
             </div>
         </div>

         <div id="collapseTaskStatusCountSummary" class="panel-collapse collapse in">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 colpadding0">
                        <div class="info-box-task white-bg w100per">
                            <div class="title d-none">Task</div>

                            <div class="col-md-2 borderright">
                                <a href="/ContractProduct/aspxPages/ContractTaskList.aspx?Status=Upcoming">
                                    <div class="count" runat="server" id="divUpcomingTaskCount">0</div>
                                </a>
                                <div class="desc">Upcoming</div>                                
                            </div>

                            <div class="col-md-2 borderright">
                                <a href="/ContractProduct/aspxPages/ContractTaskList.aspx?Status=Overdue">
                                    <div class="count" runat="server" id="divOverdueTaskCount">0</div>
                                </a>
                                <div class="desc">Overdue</div>                                
                            </div>

                            <div class="col-md-2 borderright">
                                <a href="/ContractProduct/aspxPages/ContractTaskList.aspx?Status=Submitted for Review">
                                    <div class="count" runat="server" id="divSubmittedReviewTaskCount">0</div>
                                </a>
                                <div class="desc">Submitted for Review</div>                                
                            </div>

                            <div class="col-md-2 borderright">
                                <a href="/ContractProduct/aspxPages/ContractTaskList.aspx?Status=Submitted for Approval">
                                    <div class="count" runat="server" id="divSubmittedApprovalTaskCount">0</div>
                                </a>
                                <div class="desc">Submitted for Approval</div>                                
                            </div>

                            <div class="col-md-2 borderright">
                                <a href="/ContractProduct/aspxPages/ContractTaskList.aspx?Status=Reviewed/Approved">
                                    <div class="count" runat="server" id="divReviewedApprovedTaskCount">0</div>
                                </a>
                                <div class="desc">Reviewed/Approved</div>                                
                            </div>

                            <div class="col-md-2">
                                <a href="/ContractProduct/aspxPages/ContractTaskList.aspx?Status=Closed">
                                    <div class="count" runat="server" id="divClosedTaskCount">0</div>
                                </a>
                                <div class="desc">Closed</div>                                
                            </div>

                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
          <!-- Task Status Count End -->

         <!-- Upcoming Task List-->
        <div class="row">
            <div id="divOuterUpcomingTask" class="row mang-dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12">

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h2>Task(s)</h2>
                                <div class="panel-actions">
                                    <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapseUpcomingTask">
                                        <i class="fa fa fa-chevron-up"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div id="collapseUpcomingTask" class="panel-collapse collapse in">
                                <div class="panel-body" style="max-height: 300px; overflow: auto;">
                                    <div id="divUpcomingTask" class="col-md-12 plr0">
                                        <asp:GridView runat="server" ID="grdTaskActivity" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                            GridLines="None" PageSize="5" AutoPostBack="true" CssClass="table" Width="100%" AllowPaging="true" ShowFooter="false">
                                            <Columns>
                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr.No." ItemStyle-Width="5%">
                                                    <ItemTemplate>
                                                        <%#Container.DataItemIndex+1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Type" ItemStyle-Width="10%">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px;">
                                                            <asp:Label ID="lblTaskType" runat="server" Width="100%" Text='<%# com.VirtuosoITech.ComplianceManagement.Business.Contract.ContractTaskManagement.ShowTaskType((int)Eval("TaskType")) %>'
                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# com.VirtuosoITech.ComplianceManagement.Business.Contract.ContractTaskManagement.ShowTaskType((int)Eval("TaskType")) %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Contract" ItemStyle-Width="20%">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px">
                                                            <asp:Label ID="lblContractTitle" Width="100%" runat="server" Text='<%# Eval("ContractTitle") %>'
                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("ContractTitle") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Task" ItemStyle-Width="20%">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px">
                                                            <asp:Label ID="lblTask" runat="server" Text='<%# Eval("TaskTitle") %>' Width="100%"
                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("TaskTitle") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Priority" ItemStyle-Width="10%">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblTaskPriority" runat="server" Width="100%" Text='<%# Eval("Priority") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Assign On" ItemStyle-Width="10%">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px;">
                                                            <asp:Label ID="lblAssignOn" runat="server" Width="100%" Text='<%# Eval("AssignOn") != null ? Convert.ToDateTime(Eval("AssignOn")).ToString("dd-MM-yyyy") : "" %>'
                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("AssignOn") != null ? Convert.ToDateTime(Eval("AssignOn")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Due Date" ItemStyle-Width="10%">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px;">
                                                            <asp:Label ID="lblDueOn" runat="server" Width="100%" Text='<%# Eval("DueDate") != null ? Convert.ToDateTime(Eval("DueDate")).ToString("dd-MM-yyyy") : "" %>'
                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("DueDate") != null ? Convert.ToDateTime(Eval("DueDate")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Status" ItemStyle-Width="10%">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblTaskStatus" runat="server" Width="100%" Text='<%# Eval("StatusName") %>' ToolTip='<%# Eval("StatusName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Action" HeaderStyle-CssClass="text-right" ItemStyle-CssClass="text-right" ItemStyle-Width="5%">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 50px;">
                                                            <asp:LinkButton CommandArgument='<%# Eval("TaskID")+","+Eval("ContractID")+","+Eval("RoleID")%>'
                                                                ID="lnkBtnTaskResponse" runat="server" AutoPostBack="true" OnClick="lnkBtnTaskResponse_Click"
                                                                 data-placement="left" ToolTip="View Task Detail(s)/Submit Response" data-toggle="tooltip">
                                                                <img src='<%# ResolveUrl("~/Images/edit_icon_new.png")%>' alt="View"/>  <%----%>
                                                            </asp:LinkButton>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                            </Columns>
                                            <RowStyle CssClass="clsROWgrid" />
                                            <HeaderStyle CssClass="clsheadergrid" />
                                            <PagerTemplate></PagerTemplate>
                                            <EmptyDataTemplate>
                                                No Upcoming(i.e. in next 30 days) Task Found.
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                    </div>
                                </div>

                                <div class="col-md-12 text-right">
                                    <asp:LinkButton runat="server" ID="lnkShowDetailTask" Text="..Show More"
                                    style="margin-left: -5px;color: blue;font-style: italic;border: none;background: white;"
                                        PostBackUrl="~/ContractProduct/aspxPages/ContractTaskList.aspx"></asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Upcoming Task List END-->

           <!-- Upcoming Milestone List-->
        <div class="row">
            <div id="divOuterUpcomingMilestone" class="row mang-dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12">

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h2>Milestone(s)</h2>
                                <div class="panel-actions">
                                    <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapseUpcomingTask">
                                        <i class="fa fa fa-chevron-up"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div id="collapseUpcomingMilestone" class="panel-collapse collapse in">
                                <div class="panel-body" style="max-height: 300px; overflow: auto;">
                                    <div id="divUpcomingMilestone" class="col-md-12 plr0">
                                        <asp:GridView runat="server" ID="GrdMilestone" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                            GridLines="None" PageSize="5" AutoPostBack="true" CssClass="table" Width="100%" AllowPaging="true" ShowFooter="false">
                                            <Columns>
                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr.No." ItemStyle-Width="5%">
                                                    <ItemTemplate>
                                                        <%#Container.DataItemIndex+1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Title" ItemStyle-Width="10%">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblMilestonetitle" runat="server" Width="100%" Text='<%# Eval("Title") %>' ToolTip='<%# Eval("Title") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Description" ItemStyle-Width="10%">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblMilestonedescription" runat="server" Width="100%" Text='<%# Eval("Description") %>' ToolTip='<%# Eval("Description") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Status" ItemStyle-Width="10%">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblMilestoneStatus" runat="server" Width="100%" Text='<%# Eval("Status") %>' ToolTip='<%# Eval("Status") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Department" ItemStyle-Wrap="true" ItemStyle-Width="15%">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                            <asp:Label ID="lbl2" runat="server" Text='<%# Eval("DeptName") %>'
                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("DeptName") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Due Date" ItemStyle-Width="10%">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px;">
                                                            <asp:Label ID="lblDueDate" runat="server" Text='<%# Eval("DueDate") != null ? Convert.ToDateTime(Eval("DueDate")).ToString("dd-MM-yyyy") : "" %>'
                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("DueDate") != null ? Convert.ToDateTime(Eval("DueDate")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Action" HeaderStyle-CssClass="text-right" ItemStyle-CssClass="text-right" ItemStyle-Width="5%">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 50px;">
                                                            <asp:LinkButton CommandArgument='<%# Eval("ID")+","+ Eval("ContractTemplateID")%>'
                                                                ID="lnkBtnMilestoneResponse" runat="server" AutoPostBack="true" OnClick="lnkBtnMilestoneResponse_Click"
                                                                 data-placement="left" ToolTip="View Milestone Detail(s)/Submit Response" data-toggle="tooltip">
                                                                <img src='<%# ResolveUrl("~/Images/edit_icon_new.png")%>' alt="View"/>  <%----%>
                                                            </asp:LinkButton>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                            </Columns>
                                            <RowStyle CssClass="clsROWgrid" />
                                            <HeaderStyle CssClass="clsheadergrid" />
                                            <PagerTemplate></PagerTemplate>
                                            <EmptyDataTemplate>
                                                No Upcoming(i.e. in next 30 days) Task Found.
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                    </div>
                                </div>

                                <div class="col-md-12 text-right">
                                <asp:LinkButton ID="lnkBtnBindGrid1" OnClick="lnkBtnBindGrid1_Click" Style="float: right; display: none;" Width="100%" runat="server">
                </asp:LinkButton>
                                    <asp:LinkButton runat="server" ID="LinkButton1" Text="..Show More"
                                    style="margin-left: -5px;color: blue;font-style: italic;border: none;background: white;"
                                        PostBackUrl="~/ContractProduct/aspxPages/ContractMilestoneListNew.aspx"></asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Upcoming Milestone List END-->

    </div>

      <div class="modal fade" id="divShowDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="background-color: #f7f7f7; height: 30px;">
                    <button type="button" class="close" onclick="javascript:reloadTaskList();" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>

                <div class="modal-body" style="background-color: #f7f7f7;">
                    <iframe id="showdetails" src="about:blank" width="95%" height="75%" frameborder="0"></iframe>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            //$('.btn-minimizes').click(function () {
            //    
            //    var s1 = $(this).find('i');
            //    if ($(this).hasClass('collapsed')) {
            //        $(s1).removeClass('fa-chevron-down');
            //        $(s1).addClass('fa-chevron-up');
            //    } else {
            //        $(s1).removeClass('fa-chevron-up');
            //        $(s1).addClass('fa-chevron-down');
            //    }
            //});

            //function btnminimize(obj) {
            //    var s1 = $(obj).find('i');
            //    if ($(obj).hasClass('collapsed')) {

            //        $(s1).removeClass('fa-chevron-up');
            //        $(s1).addClass('fa-chevron-down');
            //    } else {
            //        $(s1).removeClass('fa-chevron-down');
            //        $(s1).addClass('fa-chevron-up');
            //    }
            //}
        });
        </script>
</asp:Content>
