﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataLitigation;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.Dashboard
{
    public partial class ContractDashboard : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                HiddenField home = (HiddenField)Master.FindControl("Ishome");
                home.Value = "true";

                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                BindVendors();

                //Bind Tree Views
                var branchList = CustomerBranchManagement.GetAllHierarchyManagementSatutory(Convert.ToInt32(customerID));
                //var branchList = CustomerBranchManagement.GetAllHierarchy(customerID);

                BindCustomerBranches(tvFilterLocation, tbxFilterLocation, branchList);

                BindDepartment();
                BindContractStatusList(false, true);

                BindContractCategoryType();

                BindDashboard();
                BindComments();
                bool IsOnlycontract = ContractManagement.getcontractchecklist(Convert.ToInt32(AuthenticationHelper.CustomerID));
                if (IsOnlycontract)
                {
                    reviewedCount.Visible = false;
                    approvedcount.Visible = true;
                    expiredCount.Visible = false;
                    Div1.Visible = false;
                    onlyreviewDiv.Visible = true;
                    divrejected.Visible = true;
                    divcomments.Visible = true;
                    divgrdComments.Visible = true;
                    activeCount.Visible = true;
                }
                else
                {
                    activeCount.Visible = true;
                    reviewedCount.Visible = true;
                    approvedcount.Visible = true;
                    expiredCount.Visible = true;
                    Div1.Visible = true;
                    onlyreviewDiv.Visible = false;
                    divrejected.Visible = false;
                    divcomments.Visible = false;
                    divgrdComments.Visible = false;
                }
                


                //if(grdContractExpiring.co)
                //{
                //    lnkShowDetailContract.Visible = false;
                //}
                //else
                //{
                //    lnkShowDetailContract.Visible = true;
                //}

            }
        }
        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    List<NameValueHierarchy> bracnhes;
                    string key = "LocationHierarchy" + 29;
                    if (CacheHelper.Exists(key))
                    {
                        CacheHelper.Remove(key);
                    }

                    // bracnhes = CustomerBranchManagement.GetAllHierarchyManagementSatutory(2);
                    bracnhes = CustomerBranchManagement.GetAllContractEntitiesAssignedHierarchyManagementSatutory(Convert.ToInt32(AuthenticationHelper.CustomerID), Convert.ToInt32(AuthenticationHelper.UserID), "MGMT");
                    CacheHelper.Set<List<NameValueHierarchy>>(key, bracnhes);
                }
            }
            catch (Exception ex)
            {

            }
        }
        private void BindCustomerBranches(TreeView treetoBind, TextBox treeTxtBox, List<NameValueHierarchy> branchList)
        {
            try
            {
                treetoBind.Nodes.Clear();

                NameValueHierarchy branch = null;

                if (branchList.Count > 0)
                {
                    branch = branchList[0];
                }

                //treeTxtBox.Text = "Select Entity/Branch/Location";
                treeTxtBox.Text = "Click to Select";

                List<TreeNode> nodes = new List<TreeNode>();

                BindBranchesHierarchy(null, branch, nodes);

                foreach (TreeNode item in nodes)
                {
                    treetoBind.Nodes.Add(item);
                }

                treetoBind.CollapseAll();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvContractDashboard.IsValid = false;
                cvContractDashboard.ErrorMessage = "Something went wrong, Please try again.";
            }
        }

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp, List<TreeNode> nodes)
        {
            try
            {
                if (nvp != null)
                {
                    foreach (var item in nvp.Children)
                    {
                        TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                        BindBranchesHierarchy(node, item, nodes);
                        if (parent == null)
                        {
                            nodes.Add(node);
                        }
                        else
                        {
                            parent.ChildNodes.Add(node);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvContractDashboard.IsValid = false;
                cvContractDashboard.ErrorMessage = "Something went wrong, Please try again.";
            }
        }

        public void BindDepartment()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            var obj = CompDeptManagement.GetAllDepartmentMasterList(customerID);

            //Page DropDown
            ddlDeptPage.DataTextField = "Name";
            ddlDeptPage.DataValueField = "ID";

            ddlDeptPage.DataSource = obj;
            ddlDeptPage.DataBind();

            ddlDeptPage.Items.Insert(0, new ListItem("All", "-1"));
        }

        public void BindVendors()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            var lstVendors = VendorDetails.GetVendors_All(customerID);

            ddlVendorPage.DataTextField = "VendorName";
            ddlVendorPage.DataValueField = "ID";

            ddlVendorPage.DataSource = lstVendors;
            ddlVendorPage.DataBind();

            ddlVendorPage.Items.Insert(0, new ListItem("All", "-1"));
        }

        private void BindContractStatusList(bool isForTask, bool isVisibleToUser)
        {
            try
            {
                ddlContractStatus.DataSource = null;
                ddlContractStatus.DataBind();
                ddlContractStatus.ClearSelection();

                ddlContractStatus.DataTextField = "StatusName";
                ddlContractStatus.DataValueField = "ID";

                var statusList = ContractTaskManagement.GetStatusList_All(isForTask, isVisibleToUser);

                ddlContractStatus.DataSource = statusList;
                ddlContractStatus.DataBind();

                ddlContractStatus.Items.Insert(0, new ListItem("All", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindContractCategoryType()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                var lstContractTypes = ContractTypeMasterManagement.GetContractTypes_All(customerID);

                if (lstContractTypes.Count > 0)
                    lstContractTypes = lstContractTypes.OrderBy(row => row.TypeName).ToList();

                ddlContractType.DataTextField = "TypeName";
                ddlContractType.DataValueField = "ID";

                ddlContractType.DataSource = lstContractTypes;
                ddlContractType.DataBind();

                ddlContractType.Items.Add(new ListItem("All", "0"));

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnApplyFilter_Click(object sender, EventArgs e)
        {
            try
            {
                BindDashboard();

                collapseDivFilters.Attributes.Remove("class");
                collapseDivFilters.Attributes.Add("class", "panel-collapse in");
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnClearFilter_Click(object sender, EventArgs e)
        {
            try
            {
                ClearTreeViewSelection(tvFilterLocation);
                tbxFilterLocation.Text = "Select Entity/Location";

                ddlDeptPage.ClearSelection();
                ddlVendorPage.ClearSelection();
                ddlContractStatus.ClearSelection();
                ddlContractType.ClearSelection();

                btnApplyFilter_Click(sender, e);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lnkEditContract_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton)(sender);
                if (btn != null)
                {
                    long contractInstanceID = Convert.ToInt64(btn.CommandArgument);

                    if (contractInstanceID != 0)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowContractDialog(" + contractInstanceID + ");", true);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvContractDashboard.IsValid = false;
                cvContractDashboard.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void ddlContractExpiry_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindDashboard();
        }

        public static void ClearTreeViewSelection(TreeView tree)
        {
            if (tree.SelectedNode != null)
            {
                tree.SelectedNode.Selected = false;
            }
        }

        public void BindDashboard()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var lstContracts = (from row in entities.Cont_SP_GetAssignedContracts_All(customerID)
                                 select row).ToList();

                    if (lstContracts != null)
                    {
                        if (lstContracts.Count > 0)
                        {
                            var filteredContracts = GetFilteredContracts(lstContracts, AuthenticationHelper.UserID, AuthenticationHelper.Role, 3); //RoleID-3-Contract Owner

                            if (filteredContracts != null)
                            {
                                BindContractStatusCounts(filteredContracts);
                                BindContractExpiring(filteredContracts);                                
                            }
                        }
                        //if(lstContracts.Count > 0)
                        //{
                        //    if (filteredRecords.Count > 5)
                        //        lnkShowDetailContract.Visible = true;
                        //    else
                        //        lnkShowDetailContract.Visible = fals
                        //}
                    }

                    //Task Status Count
                    int priorityID = 0;                    
                    string taskStatus = string.Empty;                   

                    var lstTaskDetails = ContractTaskManagement.GetAssignedTaskList(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, AuthenticationHelper.Role, 3, priorityID, taskStatus);

                    if(lstTaskDetails != null)
                    {
                        BindTaskStatusCounts(lstTaskDetails);
                        BindUpcomingTasks(lstTaskDetails);
                       
                    }
                    BindUpcomingMileStones();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public List<Cont_SP_GetAssignedContracts_All_Result> GetFilteredContracts(List<Cont_SP_GetAssignedContracts_All_Result> lstMasterRecords, int loggedInUserID, string loggedInUserRole, int roleID)
        {
            try
            {
                int branchID = -1;
                long vendorID = -1;
                int deptID = -1;
                long typeID = -1;
                long contractStatusID = -1;

                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                //Filters
                if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1")
                {
                    branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                }

                var branchList = LitigationManagement.GetAllHierarchy(Convert.ToInt32(customerID), branchID);
                ClientCustomization isexist = CustomerManagement.GetAssignedEntity("AssignedEntity", Convert.ToInt32(customerID));
                if (!string.IsNullOrEmpty(ddlVendorPage.SelectedValue))
                {
                    vendorID = Convert.ToInt32(ddlVendorPage.SelectedValue);
                }

                if (!string.IsNullOrEmpty(ddlDeptPage.SelectedValue))
                {
                    deptID = Convert.ToInt32(ddlDeptPage.SelectedValue);
                }

                if (!string.IsNullOrEmpty(ddlContractStatus.SelectedValue))
                {
                    contractStatusID = Convert.ToInt64(ddlContractStatus.SelectedValue);
                }

                if (!string.IsNullOrEmpty(ddlContractType.SelectedValue))
                {
                    typeID = Convert.ToInt64(ddlContractType.SelectedValue);
                }
                if (isexist != null)
                {
                    var assignedcontractList = CustomerBranchManagement.GetAllAssignedContractListbyuser(Convert.ToInt32(customerID), loggedInUserID, loggedInUserRole);
                    lstMasterRecords = lstMasterRecords.Where(entry => assignedcontractList.Contains(entry.ContractNo)).ToList();
                }
               else if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
                {
                    lstMasterRecords = lstMasterRecords.Where(entry => (entry.UserID == loggedInUserID && entry.RoleID == roleID)|| (entry.CreatedBy == loggedInUserID)).ToList();
                }
               
                if (branchList.Count > 0)
                    lstMasterRecords = lstMasterRecords.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();

                if (vendorID != -1)
                    lstMasterRecords = lstMasterRecords.Where(entry => entry.VendorIDs.Split(',').ToList().Contains(vendorID.ToString())).ToList();

                if (deptID != -1)
                    lstMasterRecords = lstMasterRecords.Where(entry => entry.DepartmentID == deptID).ToList();

                if (contractStatusID != 0 && contractStatusID != -1)
                {
                    lstMasterRecords = lstMasterRecords.Where(entry => entry.ContractStatusID == contractStatusID).ToList();
                }

                if (lstMasterRecords.Count > 0)
                {
                    lstMasterRecords = (from g in lstMasterRecords
                                        group g by new
                                        {
                                            g.ID, //ContractID
                                            g.CustomerID,
                                            g.ContractNo,
                                            g.ContractTitle,
                                            g.VendorIDs,
                                            g.VendorNames,
                                            g.CustomerBranchID,
                                            g.BranchName,
                                            g.DepartmentID,
                                            g.DeptName,
                                            g.ContractTypeID,
                                            g.TypeName,
                                            g.ContractSubTypeID,
                                            g.SubTypeName,
                                            g.EffectiveDate,
                                            g.ExpirationDate,
                                            g.CreatedOn,
                                            g.UpdatedOn,
                                            g.StatusName
                                        } into GCS
                                        select new Cont_SP_GetAssignedContracts_All_Result()
                                        {
                                            ID = GCS.Key.ID, //ContractID
                                            CustomerID = GCS.Key.CustomerID,
                                            ContractNo = GCS.Key.ContractNo,
                                            ContractTitle = GCS.Key.ContractTitle,
                                            VendorIDs = GCS.Key.VendorIDs,
                                            VendorNames = GCS.Key.VendorNames,
                                            CustomerBranchID = GCS.Key.CustomerBranchID,
                                            BranchName = GCS.Key.BranchName,
                                            DepartmentID = GCS.Key.DepartmentID,
                                            DeptName = GCS.Key.DeptName,
                                            ContractTypeID = GCS.Key.ContractTypeID,
                                            TypeName = GCS.Key.TypeName,
                                            ContractSubTypeID = GCS.Key.ContractSubTypeID,
                                            SubTypeName = GCS.Key.SubTypeName,
                                            EffectiveDate = GCS.Key.EffectiveDate,
                                            ExpirationDate = GCS.Key.ExpirationDate,
                                            CreatedOn = GCS.Key.CreatedOn,
                                            UpdatedOn = GCS.Key.UpdatedOn,
                                            StatusName = GCS.Key.StatusName
                                        }).ToList();
                }

                if (lstMasterRecords.Count > 0)
                {
                    lstMasterRecords = lstMasterRecords.OrderBy(entry => entry.ExpirationDate).ToList();
                }

                return lstMasterRecords; // filteredRecords;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public void BindContractStatusCounts(List<Cont_SP_GetAssignedContracts_All_Result> lstContractRecords_All)
        {
            try
            {
                divDraftCount.InnerText = lstContractRecords_All.Where(row => row.StatusName == "Draft").Count().ToString();
                divPendingReviewCount.InnerText = lstContractRecords_All.Where(row => row.StatusName == "Pending Review").Count().ToString();
                divReviewedCount.InnerText = lstContractRecords_All.Where(row => row.StatusName == "Review Completed").Count().ToString();
                divPendingApprovalCount.InnerText = lstContractRecords_All.Where(row => row.StatusName == "Pending Approval").Count().ToString();
                //divApprovedcount.InnerText = lstContractRecords_All.Where(row => row.StatusName == "Approval Completed").Count().ToString();
                //divActiveCount.InnerText = lstContractRecords_All.Where(row => row.StatusName == "Active").Count().ToString();
                divExpiredCount.InnerText = lstContractRecords_All.Where(row => row.StatusName == "Expired").Count().ToString();
                divTNR.InnerText = lstContractRecords_All.Where(row => row.StatusName == "Terminated" || row.StatusName == "Not Renewed" ).Count().ToString();
                divReviwesCount.InnerText = GetCountMyReviews().ToString();
                divcontractReviwesCount.InnerText = GetCountContractMyReviews().ToString();
                divcontractCommentsCount.InnerText = GetCountContractMyComments().ToString();
                bool IsOnlycontract = ContractManagement.getcontractchecklist(Convert.ToInt32(AuthenticationHelper.CustomerID));
                if (IsOnlycontract)
                {
                    int rejcted = GetCountContractRejected();
                    int ownrejected = lstContractRecords_All.Where(row => row.StatusName == "Rejected").Count();
                    divcontractRejectedCount.InnerText = (rejcted + ownrejected).ToString();

                    int Approve = GetCountContractApprove();
                    int ownApproved = lstContractRecords_All.Where(row => row.StatusName == "Approval Completed").Count();
                    divApprovedcount.InnerText = (Approve + ownApproved).ToString();

                    int Active = GetCountContractActive();
                    int ownActive = lstContractRecords_All.Where(row => row.StatusName == "Active").Count();
                    divActiveCount.InnerText = (ownActive + Active).ToString();                   
                }
                else
                {
                    divActiveCount.InnerText = lstContractRecords_All.Where(row => row.StatusName == "Active").Count().ToString();
                    divApprovedcount.InnerText = lstContractRecords_All.Where(row => row.StatusName == "Approval Completed").Count().ToString();
                    divcontractRejectedCount.InnerText = lstContractRecords_All.Where(row => row.StatusName == "Rejected").Count().ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public static int GetCountContractMyComments()
        {
            int Count = 0;
            try
            {
                long customerID = -1;
                customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;

                int branchID = -1;
                int vendorID = -1;
                int deptID = -1;
                long contractStatusID = -1;
                long contractTypeID = -1;

                var branchList = LitigationManagement.GetAllHierarchy(Convert.ToInt32(customerID), branchID);

                var lstAssignedContracts = ContractManagement.GetCommentedContractsonlyrevieweList(Convert.ToInt32(customerID), AuthenticationHelper.UserID, AuthenticationHelper.Role,
                    3, branchList, vendorID, deptID, contractStatusID, contractTypeID);

                Count = lstAssignedContracts.Count;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return Count;
        }
        public static int GetCountContractActive()
        {
            int Count = 0;
            try
            {
                long customerID = -1;
                customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;

                int branchID = -1;
                int vendorID = -1;
                int deptID = -1;
                long contractStatusID = 7;
                long contractTypeID = -1;

                var branchList = LitigationManagement.GetAllHierarchy(Convert.ToInt32(customerID), branchID);

                var lstAssignedContractsrejcted = ContractManagement.GetAssignedContractsonlyrejectedList(Convert.ToInt32(customerID), AuthenticationHelper.UserID, AuthenticationHelper.Role,
                    4, branchList, vendorID, deptID, contractStatusID, contractTypeID);

                var lstAssignedApproverContractsrejcted = ContractManagement.GetAssignedContractsonlyrejectedList(Convert.ToInt32(customerID), AuthenticationHelper.UserID, AuthenticationHelper.Role,
                    6, branchList, vendorID, deptID, contractStatusID, contractTypeID);

                Count = lstAssignedContractsrejcted.Count + lstAssignedApproverContractsrejcted.Count;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return Count;
        }
        public static int GetCountContractApprove()
        {
            int Count = 0;
            try
            {
                long customerID = -1;
                customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;

                int branchID = -1;
                int vendorID = -1;
                int deptID = -1;
                long contractStatusID = 5;
                long contractTypeID = -1;

                var branchList = LitigationManagement.GetAllHierarchy(Convert.ToInt32(customerID), branchID);

                var lstAssignedContractsrejcted = ContractManagement.GetAssignedContractsonlyrejectedList(Convert.ToInt32(customerID), AuthenticationHelper.UserID, AuthenticationHelper.Role,
                    4, branchList, vendorID, deptID, contractStatusID, contractTypeID);

                var lstAssignedApproverContractsrejcted = ContractManagement.GetAssignedContractsonlyrejectedList(Convert.ToInt32(customerID), AuthenticationHelper.UserID, AuthenticationHelper.Role,
                    6, branchList, vendorID, deptID, contractStatusID, contractTypeID);

                Count = lstAssignedContractsrejcted.Count + lstAssignedApproverContractsrejcted.Count;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return Count;
        }
        public static int GetCountContractRejected()
        {
            int Count = 0;
            try
            {
                long customerID = -1;
                customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;

                int branchID = -1;
                int vendorID = -1;
                int deptID = -1;
                long contractStatusID = 15;
                long contractTypeID = -1;

                var branchList = LitigationManagement.GetAllHierarchy(Convert.ToInt32(customerID), branchID);

                var lstAssignedContractsrejcted = ContractManagement.GetAssignedContractsonlyrejectedList(Convert.ToInt32(customerID), AuthenticationHelper.UserID, AuthenticationHelper.Role,
                    4, branchList, vendorID, deptID, contractStatusID, contractTypeID);

                var lstAssignedApproverContractsrejcted = ContractManagement.GetAssignedContractsonlyrejectedList(Convert.ToInt32(customerID), AuthenticationHelper.UserID, AuthenticationHelper.Role,
                    6, branchList, vendorID, deptID, contractStatusID, contractTypeID);

                Count = lstAssignedContractsrejcted.Count + lstAssignedApproverContractsrejcted.Count;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return Count;
        }
        public static int GetCountContractMyReviews()
        {
            int Count = 0;
            try
            {
                long customerID = -1;
                customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;

                int branchID = -1;
                int vendorID = -1;
                int deptID = -1;
                long contractStatusID = -1;
                long contractTypeID = -1;

                var branchList = LitigationManagement.GetAllHierarchy(Convert.ToInt32(customerID), branchID);
                
                var lstAssignedContracts = ContractManagement.GetAssignedContractsonlyrevieweList(Convert.ToInt32(customerID), AuthenticationHelper.UserID, AuthenticationHelper.Role,
                    4, branchList, vendorID, deptID, contractStatusID, contractTypeID);

                var lstAssignedApproverContracts = ContractManagement.GetAssignedContractsonlyrevieweList(Convert.ToInt32(customerID), AuthenticationHelper.UserID, AuthenticationHelper.Role,
                    6, branchList, vendorID, deptID, contractStatusID, contractTypeID);

                Count = lstAssignedContracts.Count + lstAssignedApproverContracts.Count;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return Count;
        }
        public static int GetCountMyReviews()
        {
            int Count = 0;
            try
            {   
                long customerID = -1;
                customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;

                int branchID = -1;
                int vendorID = -1;
                int deptID = -1;
                long contractStatusID = -1;
                long contractTypeID = -1;
                
                var branchList = LitigationManagement.GetAllHierarchy(Convert.ToInt32(customerID), branchID);

                var lstAssignedContracts = ContractManagement.GetAssignedContractsTemplateList(Convert.ToInt32(customerID), AuthenticationHelper.UserID, AuthenticationHelper.Role,
                    4, branchList, vendorID, deptID, contractStatusID, contractTypeID);

                var lstAssignedApproverContracts = ContractManagement.GetAssignedContractsTemplateList(Convert.ToInt32(customerID), AuthenticationHelper.UserID, AuthenticationHelper.Role,
                    6, branchList, vendorID, deptID, contractStatusID, contractTypeID);

                var lstAssignedPrimaryApproverContracts = ContractManagement.GetAssignedContractsTemplateList(Convert.ToInt32(customerID), AuthenticationHelper.UserID, AuthenticationHelper.Role,
                   29, branchList, vendorID, deptID, contractStatusID, contractTypeID);

                Count = lstAssignedContracts.Count + lstAssignedApproverContracts.Count + lstAssignedPrimaryApproverContracts.Count;               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return Count;
        }
        public void BindContractExpiring(List<Cont_SP_GetAssignedContracts_All_Result> filteredRecords)
        {
            try
            {
                int upcomingDays = -1;

                if (!string.IsNullOrEmpty(ddlContractExpiry.SelectedValue))
                {
                    upcomingDays = Convert.ToInt32(ddlContractExpiry.SelectedValue);
                }

                if (upcomingDays != -1)
                {
                    if (upcomingDays != 91)
                        filteredRecords = filteredRecords.Where(row => row.ExpirationDate <= DateTime.Now.AddDays(upcomingDays).Date).ToList();
                    else
                        filteredRecords = filteredRecords.Where(row => row.ExpirationDate >= DateTime.Now.AddDays(upcomingDays).Date).ToList();
                }

                grdContractExpiring.DataSource = filteredRecords;
                grdContractExpiring.DataBind();

                if (filteredRecords.Count > 5)
                    lnkShowDetailContract.Visible = true;
                else
                    lnkShowDetailContract.Visible = false;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void GetContractExpiring(List<View_LitigationCaseResponse> viewRecords, int loggedInUserID, string loggedInUserRole, int loggedInUserRoleID, string noticeCase, string noticeCaseType, List<int> branchList, int partyID, int deptID, int catID, int riskID, int stageID, int noticeCaseStatus)
        {
            try
            {
                if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
                    viewRecords = viewRecords.Where(entry => entry.UserID == loggedInUserID && entry.RoleID == loggedInUserRoleID).ToList();
                else
                    viewRecords = viewRecords.Where(entry => entry.RoleID == 3).ToList(); // In case of MGMT or CADMN 

                //if (branchID != -1)
                //    query = query.Where(entry => entry.CustomerBranchID == branchID).ToList();

                if (branchList.Count > 0)
                    viewRecords = viewRecords.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();

                //if (partyID != -1)
                //    viewRecords = viewRecords.Where(entry => entry.PartyID == partyID).ToList();

                if (deptID != -1)
                    viewRecords = viewRecords.Where(entry => entry.DepartmentID == deptID).ToList();

                if (riskID != -1 && riskID != 0)
                    viewRecords = viewRecords.Where(entry => entry.CaseRiskID == riskID).ToList();

                if (noticeCaseType != "" && noticeCaseType != "B") //B--Both --Inward(I) and Outward(O)
                    viewRecords = viewRecords.Where(entry => entry.CaseType == noticeCaseType).ToList();

                if (noticeCaseStatus != 0 && noticeCaseStatus != -1)
                {
                    if (noticeCaseStatus == 3) //3--Closed otherwise Open
                        viewRecords = viewRecords.Where(entry => entry.TxnStatusID == noticeCaseStatus).ToList();
                    else
                        viewRecords = viewRecords.Where(entry => entry.TxnStatusID < 3).ToList();
                }

                if (viewRecords.Count > 0)
                {
                    viewRecords = viewRecords.OrderBy(entry => entry.ReminderDate).ToList();
                }

                grdContractExpiring.DataSource = viewRecords;
                grdContractExpiring.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void upDivLocation_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindTaskStatusCounts(List<Cont_SP_GetAssignedTasks_All_Result> lstTaskRecords_Assigned)
        {
            try
            {
                divUpcomingTaskCount.InnerText = ContractTaskManagement.GetTasksStatusWise(lstTaskRecords_Assigned, "Upcoming").Count().ToString();                

                divSubmittedReviewTaskCount.InnerText = ContractTaskManagement.GetTasksStatusWise(lstTaskRecords_Assigned, "Submitted for Review").Count().ToString();

                divSubmittedApprovalTaskCount.InnerText = ContractTaskManagement.GetTasksStatusWise(lstTaskRecords_Assigned, "Submitted for Approval").Count().ToString();

                divReviewedApprovedTaskCount.InnerText = ContractTaskManagement.GetTasksStatusWise(lstTaskRecords_Assigned, "Reviewed/Approved").Count().ToString();

                divOverdueTaskCount.InnerText = ContractTaskManagement.GetTasksStatusWise(lstTaskRecords_Assigned, "Overdue").Count().ToString();

                divClosedTaskCount.InnerText = ContractTaskManagement.GetTasksStatusWise(lstTaskRecords_Assigned, "Closed").Count().ToString();

                //divSubmittedReviewTaskCount.InnerText = lstTaskRecords_Assigned.Where(row => (row.StatusName == "Open" || row.StatusName == "In-Progress")
                //                                                                          && row.AssignOn >= DateTime.Now
                //                                                                          && row.TaskType == 4
                //                                                                          && !(row.DueDate > DateTime.Now)).Count().ToString();

                //divSubmittedApprovalTaskCount.InnerText = lstTaskRecords_Assigned.Where(row => (row.StatusName == "Open" || row.StatusName == "In-Progress")
                //                                                                            && row.AssignOn >= DateTime.Now
                //                                                                            && row.TaskType == 6
                //                                                                            && !(row.DueDate > DateTime.Now)).Count().ToString();

                //divReviewedApprovedTaskCount.InnerText = lstTaskRecords_Assigned.Where(row => row.StatusName.Contains("Submitted")
                //                                                                           //&& row.AssignOn >= DateTime.Now                                                                                        
                //                                                                           //&& !(row.DueDate > DateTime.Now)
                //                                                                           ).Count().ToString();

                //divOverdueTaskCount.InnerText = lstTaskRecords_Assigned.Where(row => (row.StatusName == "Open" || row.StatusName == "In-Progress")
                //                                                                  && row.DueDate > DateTime.Now).Count().ToString();

                //divClosedTaskCount.InnerText = lstTaskRecords_Assigned.Where(row => row.StatusName == "Closed").Count().ToString(); 
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindUpcomingTasks(List<Cont_SP_GetAssignedTasks_All_Result> lstTaskRecords_Assigned)
        {
            try
            {
                var upcomingTasksRecords = ContractTaskManagement.GetTasksStatusWise(lstTaskRecords_Assigned, "All"); //"Upcoming"

                grdTaskActivity.DataSource = upcomingTasksRecords;
                grdTaskActivity.DataBind();
                
                if (upcomingTasksRecords.Count > 5)
                    lnkShowDetailTask.Visible = true;
                else
                    lnkShowDetailTask.Visible = false;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindUpcomingMileStones()
        {
            try
            {
                long CustomerID = AuthenticationHelper.CustomerID;
                long UserID = AuthenticationHelper.UserID;
                var MilestoneList = ContractMastersManagement.GetContractMilestoneDetail(CustomerID, UserID);
                //var upcomingTasksRecords = ContractTaskManagement.GetTasksStatusWise(lstTaskRecords_Assigned, "All"); //"Upcoming"
                MilestoneList = MilestoneList.Where(x => x.StatusID != 3).ToList();
                GrdMilestone.DataSource = MilestoneList;
                GrdMilestone.DataBind();

                if (MilestoneList.Count > 5)
                    LinkButton1.Visible = true;
                else
                    LinkButton1.Visible = false;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindComments()
        {
            try
            {
                long CustomerID = AuthenticationHelper.CustomerID;
                long UserID = AuthenticationHelper.UserID;
                var commentsList = ContractMastersManagement.GetContractComments(CustomerID, UserID);
                grdcomments.DataSource = commentsList;
                grdcomments.DataBind();

                if (commentsList.Count > 5)
                    LinkButton2.Visible = true;
                else
                    LinkButton2.Visible = false;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lnkBtnTaskResponse_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton)(sender);
                if (btn != null)
                {
                    string[] commandArgs = btn.CommandArgument.ToString().Split(new char[] { ',' });

                    long taskID = Convert.ToInt64(commandArgs[0]);
                    long contractID = Convert.ToInt64(commandArgs[1]);
                    int roleID = Convert.ToInt32(commandArgs[2]);

                    if (taskID != 0 && contractID != 0)
                    {
                        var strContractID = CryptographyManagement.Encrypt(contractID.ToString());
                        var strTaskID = CryptographyManagement.Encrypt(taskID.ToString());
                        var strUserID = CryptographyManagement.Encrypt(AuthenticationHelper.UserID.ToString());
                        var strRoleID = CryptographyManagement.Encrypt(roleID.ToString());

                        string checkSum = Util.CalculateMD5Hash(contractID.ToString() + taskID.ToString() + AuthenticationHelper.UserID.ToString() + roleID.ToString());

                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowTaskDialog('" + strContractID + "','" + strTaskID + "','" + strUserID + "','" + strRoleID + "','" + checkSum + "');", true);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvContractDashboard.IsValid = false;
                cvContractDashboard.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void lnkBtnMilestoneResponse_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton)(sender);
                if (btn != null)
                {
                    string[] commandArgs = btn.CommandArgument.ToString().Split(new char[] { ',' });

                    long MilestoneID = Convert.ToInt64(commandArgs[0]);
                    long contractID = Convert.ToInt64(commandArgs[1]);
                    //int roleID = Convert.ToInt32(commandArgs[2]);

                    if (MilestoneID != 0 && contractID != 0)
                    {
                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenDocTypePopup('" + MilestoneID + "','" + contractID + "');", true);                        
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvContractDashboard.IsValid = false;
                cvContractDashboard.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void lnkBtnBindGrid1_Click(object sender, EventArgs e)
        {
            BindUpcomingMileStones();
        }

        protected void lnkBtnViewContractComment_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton)(sender);
                if (btn != null)
                {
                    long contractInstanceID = Convert.ToInt64(btn.CommandArgument);
                    long UserID = Convert.ToInt64(AuthenticationHelper.UserID);
                    if (contractInstanceID != 0)
                    {
                        long RoleId = 0;
                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        {
                            var userRoles = (from row in entities.ContractAssignments
                                             where row.ContractInstanceID == contractInstanceID
                                             && row.IsActive == true
                                             && row.UserID == UserID
                                             select row).FirstOrDefault();

                            if (userRoles != null)
                            {
                                RoleId = userRoles.RoleID;
                            }
                        }
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowContractCommentDialog(" + contractInstanceID + "," + RoleId + ");", true);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvContractDashboard.IsValid = false;
                cvContractDashboard.ErrorMessage = "Something went wrong, Please try again";
            }
        }
    }
}