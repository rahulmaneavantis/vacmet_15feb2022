﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataLitigation;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.Dashboard
{
    public partial class DashboardGraphDetail : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                BindVendors();

                //Bind Tree Views
                var branchList = CustomerBranchManagement.GetAllHierarchyManagementSatutory(Convert.ToInt32(customerID));
                //var branchList = CustomerBranchManagement.GetAllHierarchy(customerID);

                BindCustomerBranches(tvFilterLocation, tbxFilterLocation, branchList);

                BindDepartment();
                BindContractStatusList(false, true);
                BindContractCategoryType();

                if (!string.IsNullOrEmpty(Request.QueryString["BID"]))
                {
                    int BId = Convert.ToInt32(Request.QueryString["BID"]);
                    foreach (TreeNode node in tvFilterLocation.Nodes)
                    {
                        if (node.Value == BId.ToString())
                        {
                            node.Selected = true;
                        }
                        foreach (TreeNode item1 in node.ChildNodes)
                        {
                            if (item1.Value == BId.ToString())
                                item1.Selected = true;
                        }
                    }
                    tvFilterLocation_SelectedNodeChanged(null, null);
                    //tvFilterLocation.SelectedNode.Value = Request.QueryString["BID"];
                    //tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
                }

                if (!string.IsNullOrEmpty(Request.QueryString["DID"]))
                {
                    if (ddlDeptPage.Items.FindByValue(Request.QueryString["DID"]) != null)
                    {
                        ddlDeptPage.ClearSelection();
                        ddlDeptPage.Items.FindByValue(Request.QueryString["DID"]).Selected = true;
                    }
                }

                if (!string.IsNullOrEmpty(Request.QueryString["VID"]))
                {
                    if (ddlVendorPage.Items.FindByValue(Request.QueryString["VID"]) != null)
                    {
                        ddlVendorPage.ClearSelection();
                        ddlVendorPage.Items.FindByValue(Request.QueryString["VID"]).Selected = true;
                    }                   
                }

                if (!string.IsNullOrEmpty(Request.QueryString["SID"]))
                {
                    if (ddlContractStatus.Items.FindByValue(Request.QueryString["SID"]) != null)
                    {
                        ddlContractStatus.ClearSelection();
                        ddlContractStatus.Items.FindByValue(Request.QueryString["SID"]).Selected = true;
                    }                    
                }

                if (!string.IsNullOrEmpty(Request.QueryString["TID"]))
                {
                    if (ddlContractType.Items.FindByValue(Request.QueryString["TID"]) != null)
                    {
                        ddlContractType.ClearSelection();
                        ddlContractType.Items.FindByValue(Request.QueryString["TID"]).Selected = true;
                    }                    
                }

                BindGrid();
                bindPageNumber();
                ShowGridDetail();
                upDivLocation_Load(sender, e);
            }
        }

        protected void lnkEditContract_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton)(sender);
                if (btn != null)
                {
                    long contractInstanceID = Convert.ToInt64(btn.CommandArgument);

                    if (contractInstanceID != 0)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowContractDialog(" + contractInstanceID + ");", true);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvGraphDetail.IsValid = false;
                cvGraphDetail.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        private void BindCustomerBranches(TreeView treetoBind, TextBox treeTxtBox, List<NameValueHierarchy> branchList)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                int userId = -1;
                userId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                int IsForBranch = UserManagement.ExistBranchAssignment(customerID);
                if (IsForBranch == 1)
                {
                    tvFilterLocation.Nodes.Clear();
                    NameValueHierarchy branch = null;

                    List<NameValueHierarchy> branches;
                    string key = "LocationHierarchy" + AuthenticationHelper.CustomerID;
                    if (CacheHelper.Exists(key))
                    {
                        CacheHelper.Get<List<NameValueHierarchy>>(key, out branches);
                    }
                    else
                    {
                        branches = CustomerBranchManagement.GetAllHierarchyManagementSatutoryNew(customerID);
                        CacheHelper.Set<List<NameValueHierarchy>>(key, branches);
                    }
                    if (branches.Count > 0)
                    {
                        branch = branches[0];
                    }

                    tbxFilterLocation.Text = "Select Entity/Location";

                    TreeNode node = new TreeNode("Entity/Sub-Entity/Location", "-1");
                    node.Selected = true;
                    tvFilterLocation.Nodes.Add(node);

                    List<TreeNode> nodes = new List<TreeNode>();
                    using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                    {
                        var BranchList = (from row in entities.LitigationEntitiesAssignments
                                          where row.UserID == userId
                                          select (int)row.BranchID).ToList();

                        List<int> LegalCasebranchlist = (from row in entities.tbl_LegalCaseAssignment
                                                         join row1 in entities.tbl_LegalCaseInstance
                                                         on row.CaseInstanceID equals row1.ID
                                                         where row1.CustomerID == (int)customerID
                                                         && (row.UserID == userId || row1.OwnerID == userId || row1.CreatedBy == userId || BranchList.Contains(row1.CustomerBranchID))
                                                         select (int)row1.CustomerBranchID).ToList();

                        List<int> LegalNoticebranchlist = (from row in entities.tbl_LegalNoticeAssignment
                                                           join row1 in entities.tbl_LegalNoticeInstance
                                                           on row.NoticeInstanceID equals row1.ID
                                                           where row1.CustomerID == (int)customerID
                                                           && (row.UserID == userId || row1.OwnerID == userId || row1.CreatedBy == userId || BranchList.Contains(row1.CustomerBranchID))
                                                           select (int)row1.CustomerBranchID).ToList();

                        List<int> LegalCasbranchlist = LegalCasebranchlist.Union(LegalNoticebranchlist).ToList();
                        var LocationList = LegalCasbranchlist.Select(a => a).ToList();

                        BindBranchesHierarchyNew(null, branch, nodes, LocationList);
                    }

                    foreach (TreeNode item in nodes)
                    {
                        tvFilterLocation.Nodes.Add(item);
                    }

                    tvFilterLocation.CollapseAll();
                }
                else
                {
                    treetoBind.Nodes.Clear();

                    NameValueHierarchy branch = null;

                    if (branchList.Count > 0)
                    {
                        branch = branchList[0];
                    }

                    tbxFilterLocation.Text = "Select Entity/Location";

                    TreeNode node = new TreeNode("All", "-1");
                    node.Selected = true;
                    tvFilterLocation.Nodes.Add(node);

                    List<TreeNode> nodes = new List<TreeNode>();

                    BindBranchesHierarchy(null, branch, nodes);

                    foreach (TreeNode item in nodes)
                    {
                        treetoBind.Nodes.Add(item);
                    }

                    treetoBind.CollapseAll();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvGraphDetail.IsValid = false;
                cvGraphDetail.ErrorMessage = "Something went wrong, Please try again.";
            }
        }
        private void BindBranchesHierarchyNew(TreeNode parent, NameValueHierarchy nvp, List<TreeNode> nodes, List<int> branchlst)
        {
            try
            {
                if (nvp != null)
                {
                    foreach (var item in nvp.Children)
                    {
                        TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                        BindBranchesHierarchyNew(node, item, nodes, branchlst);
                        if (branchlst.Contains(item.ID) == true)
                        {
                            nodes.Add(node);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp, List<TreeNode> nodes)
        {
            try
            {
                if (nvp != null)
                {
                    foreach (var item in nvp.Children)
                    {
                        TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                        BindBranchesHierarchy(node, item, nodes);
                        if (parent == null)
                        {
                            nodes.Add(node);
                        }
                        else
                        {
                            parent.ChildNodes.Add(node);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvGraphDetail.IsValid = false;
                cvGraphDetail.ErrorMessage = "Something went wrong, Please try again.";
            }
        }

        public void BindDepartment()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            var obj = CompDeptManagement.GetAllDepartmentMasterList(customerID);

            //Page DropDown
            ddlDeptPage.DataTextField = "Name";
            ddlDeptPage.DataValueField = "ID";

            ddlDeptPage.DataSource = obj;
            ddlDeptPage.DataBind();

            ddlDeptPage.Items.Insert(0, new ListItem("All", "-1"));
        }

        public void BindVendors()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            var lstVendors = VendorDetails.GetVendors_All(customerID);

            ddlVendorPage.DataTextField = "VendorName";
            ddlVendorPage.DataValueField = "ID";

            ddlVendorPage.DataSource = lstVendors;
            ddlVendorPage.DataBind();

            ddlVendorPage.Items.Insert(0, new ListItem("All", "-1"));
        }

        private void BindContractStatusList(bool isForTask, bool isVisibleToUser)
        {
            try
            {
                ddlContractStatus.DataSource = null;
                ddlContractStatus.DataBind();
                ddlContractStatus.ClearSelection();

                ddlContractStatus.DataTextField = "StatusName";
                ddlContractStatus.DataValueField = "ID";

                var statusList = ContractTaskManagement.GetStatusList_All(isForTask, isVisibleToUser);

                ddlContractStatus.DataSource = statusList;
                ddlContractStatus.DataBind();

                ddlContractStatus.Items.Insert(0, new ListItem("All", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindContractCategoryType()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                var lstContractTypes = ContractTypeMasterManagement.GetContractTypes_All(customerID);

                if (lstContractTypes.Count > 0)
                    lstContractTypes = lstContractTypes.OrderBy(row => row.TypeName).ToList();

                ddlContractType.DataTextField = "TypeName";
                ddlContractType.DataValueField = "ID";

                ddlContractType.DataSource = lstContractTypes;
                ddlContractType.DataBind();

                ddlContractType.Items.Insert(0, new ListItem("All", "-1"));               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxFilterLocation.Text = tvFilterLocation.SelectedNode != null ? tvFilterLocation.SelectedNode.Text : "All";
                //tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;

                //tbxBranch.Text = tvBranches.SelectedNode != null ? tvBranches.SelectedNode.Text : "Select Entity/Location";
                //setContractNumber();
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "hideDivTree", "hideDivBranch();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnApplyFilter_Click(object sender, EventArgs e)
        {
            try
            {
                BindGrid();
                bindPageNumber();
                ShowGridDetail();
                upDivLocation_Load(sender, e);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnClearFilter_Click(object sender, EventArgs e)
        {
            try
            {
                ClearTreeViewSelection(tvFilterLocation);
                tbxFilterLocation.Text = "All";

                ddlDeptPage.ClearSelection();
                ddlVendorPage.ClearSelection();
                ddlContractStatus.ClearSelection();
                ddlContractType.ClearSelection();

                btnApplyFilter_Click(sender, e);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public static void ClearTreeViewSelection(TreeView tree)
        {
            if (tree.SelectedNode != null)
            {
                tree.SelectedNode.Selected = false;
            }
        }

        protected void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {                
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    if (Session["grdDetailData"] != null)
                    {
                        String FileName = String.Empty;

                        FileName = "ContractReport";

                        ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("Report");
                        DataTable ExcelData = null;

                        DataView view = new System.Data.DataView((DataTable)Session["grdDetailData"]);
                        ExcelData = view.ToTable("Selected", false, "ContractNo", "StatusName", "ContractTitle", "ContractDetailDesc", "BranchName", "DeptName", "TypeName", "SubTypeName", "VendorNames", "ProposalDate", "AgreementDate", "EffectiveDate", "ReviewDate", "ExpirationDate");

                        if (ExcelData.Rows.Count > 0)
                        {
                            ExcelData.Columns.Add("SNo", typeof(int)).SetOrdinal(0);

                            int rowCount = 0;                            
                            foreach (DataRow item in ExcelData.Rows)
                            {
                                item["SNo"] = ++rowCount;

                                if (item["ProposalDate"] != null)
                                {
                                    if (!string.IsNullOrEmpty(Convert.ToString(item["ProposalDate"])))
                                    {
                                        item["ProposalDate"] = Convert.ToDateTime(item["ProposalDate"]).ToString("dd-MMM-yyyy");
                                    }
                                }

                                if (item["AgreementDate"] != null)
                                {
                                    if (!string.IsNullOrEmpty(Convert.ToString(item["AgreementDate"])))
                                    {
                                        item["AgreementDate"] = Convert.ToDateTime(item["AgreementDate"]).ToString("dd-MMM-yyyy");
                                    }
                                }

                                if (item["EffectiveDate"] != null)
                                {
                                    if (!string.IsNullOrEmpty(Convert.ToString(item["EffectiveDate"])))
                                    {
                                        item["EffectiveDate"] = Convert.ToDateTime(item["EffectiveDate"]).ToString("dd-MMM-yyyy");
                                    }
                                }

                                if (item["ReviewDate"] != null)
                                {
                                    if (!string.IsNullOrEmpty(Convert.ToString(item["ReviewDate"])))
                                    {
                                        item["ReviewDate"] = Convert.ToDateTime(item["ReviewDate"]).ToString("dd-MMM-yyyy");
                                    }
                                }

                                if (item["ExpirationDate"] != null)
                                {
                                    if (!string.IsNullOrEmpty(Convert.ToString(item["ExpirationDate"])))
                                    {
                                        item["ExpirationDate"] = Convert.ToDateTime(item["ExpirationDate"]).ToString("dd-MMM-yyyy");
                                    }
                                }
                            }

                            var customer = UserManagementRisk.GetCustomer(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);

                            exWorkSheet.Cells["A1:B1"].Merge = true;
                            exWorkSheet.Cells["A1"].Value = "Report Generated On:";
                            exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A1"].Style.Font.Size = 12;

                            exWorkSheet.Cells["C1"].Value = DateTime.Now.ToString("dd-MMM-yyyy");
                            exWorkSheet.Cells["C1"].Style.Font.Size = 12;

                            exWorkSheet.Cells["A2:C2"].Merge = true;
                            exWorkSheet.Cells["A2"].Value = customer.Name;
                            exWorkSheet.Cells["A2"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A2"].Style.Font.Size = 12;
                            
                            exWorkSheet.Cells["A3:C3"].Merge = true;
                            exWorkSheet.Cells["A3"].Value = "Contract Report";
                            exWorkSheet.Cells["A3"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A3"].Style.Font.Size = 12;
                            exWorkSheet.Cells["A3"].AutoFitColumns(15);

                            exWorkSheet.Cells["A5"].LoadFromDataTable(ExcelData, true);

                            exWorkSheet.Cells["A5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["A5"].Value = "S.No.";
                            exWorkSheet.Cells["A5"].AutoFitColumns(5);

                            exWorkSheet.Cells["B5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["B5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["B5"].Value = "Contract No.";
                            exWorkSheet.Cells["B5"].AutoFitColumns(20);

                            exWorkSheet.Cells["C5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["C5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["C5"].Value = "Status";
                            exWorkSheet.Cells["C5"].AutoFitColumns(20);

                            exWorkSheet.Cells["D5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["D5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["D5"].Value = "Contract Title";
                            exWorkSheet.Cells["D5"].AutoFitColumns(50);

                            exWorkSheet.Cells["E5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["E5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["E5"].Value = "Description";
                            exWorkSheet.Cells["E5"].AutoFitColumns(20);

                            exWorkSheet.Cells["F5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["F5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["F5"].Value = "Entity/Branch/Location";
                            exWorkSheet.Cells["F5"].AutoFitColumns(25);

                            exWorkSheet.Cells["G5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["G5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["G5"].Value = "Department";
                            exWorkSheet.Cells["G5"].AutoFitColumns(25);

                            exWorkSheet.Cells["H5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["H5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["H5"].Value = "Contract Type";
                            exWorkSheet.Cells["H5"].AutoFitColumns(25);

                            exWorkSheet.Cells["I5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["I5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["I5"].Value = "Contract Sub-Type";
                            exWorkSheet.Cells["I5"].AutoFitColumns(25);

                            exWorkSheet.Cells["J5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["J5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["J5"].Value = "Vendor";
                            exWorkSheet.Cells["J5"].AutoFitColumns(25);

                            exWorkSheet.Cells["K5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["K5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["K5"].Value = "Proposal Date";
                            exWorkSheet.Cells["K5"].AutoFitColumns(15);

                            exWorkSheet.Cells["L5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["L5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["L5"].Value = "Agreement Date";
                            exWorkSheet.Cells["L5"].AutoFitColumns(15);

                            exWorkSheet.Cells["M5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["M5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["M5"].Value = "Start Date";
                            exWorkSheet.Cells["M5"].AutoFitColumns(15);

                            exWorkSheet.Cells["N5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["N5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["N5"].Value = "Review Date";
                            exWorkSheet.Cells["N5"].AutoFitColumns(15);

                            exWorkSheet.Cells["O5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["O5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["O5"].Value = "End Date";
                            exWorkSheet.Cells["O5"].AutoFitColumns(15);                           

                            //Assign borders
                            using (ExcelRange col = exWorkSheet.Cells[5, 1, 5 + ExcelData.Rows.Count, 15])
                            {
                                col.Style.Numberformat.Format = "dd/MM/yyyy";
                                col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;
                                col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;

                                col.Style.WrapText = true;

                                col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            }

                            Byte[] fileBytes = exportPackge.GetAsByteArray();
                            Response.ClearContent();
                            Response.Buffer = true;
                            Response.AddHeader("content-disposition", "attachment;filename=" + FileName + "-" + DateTime.Now.ToString("ddMMyyyy") + ".xlsx");
                            Response.Charset = "";
                            Response.ContentType = "application/vnd.ms-excel";
                            StringWriter sw = new StringWriter();
                            Response.BinaryWrite(fileBytes);
                            //Response.End();
                            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                            
                        }
                        else
                        {
                            cvGraphDetail.IsValid = false;
                            cvGraphDetail.ErrorMessage = "No data available to export for current selection(s)";
                            cvGraphDetail.CssClass = "alert alert-danger;";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void upDivLocation_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindGrid()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                int branchID = -1;
                int deptID = -1;
                int vendorID = -1;               
                long contractStatusID = -1;
                long contractTypeID = -1;
                long OwnerID = -1;
                if (!string.IsNullOrEmpty(Request.QueryString["OID"]))
                {
                    OwnerID = Convert.ToInt64(Request.QueryString["OID"]);
                }
                if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1")
                {
                    branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                }

                var branchList = LitigationManagement.GetAllHierarchy(Convert.ToInt32(customerID), branchID);

                if (!string.IsNullOrEmpty(ddlDeptPage.SelectedValue))
                {
                    deptID = Convert.ToInt32(ddlDeptPage.SelectedValue);
                }

                if (!string.IsNullOrEmpty(ddlVendorPage.SelectedValue))
                {
                    vendorID = Convert.ToInt32(ddlVendorPage.SelectedValue);
                }                

                if (!string.IsNullOrEmpty(ddlContractStatus.SelectedValue))
                {
                    contractStatusID = Convert.ToInt64(ddlContractStatus.SelectedValue);
                }

                if (!string.IsNullOrEmpty(ddlContractType.SelectedValue))
                {
                    contractTypeID = Convert.ToInt64(ddlContractType.SelectedValue);
                }

                var lstAssignedContracts = ContractManagement.GetAssignedContractsList(Convert.ToInt32(customerID), AuthenticationHelper.UserID, AuthenticationHelper.Role,
                    3, branchList, vendorID, deptID, contractStatusID, contractTypeID);
                if (OwnerID != -1)
                {
                    lstAssignedContracts = lstAssignedContracts.Where(entry => entry.OwnerIDs.Trim().Split(',').ToList().Contains(OwnerID.ToString())).ToList();
                }
                grdContractList.DataSource = lstAssignedContracts;
                grdContractList.DataBind();

                Session["TotalRows"] = lstAssignedContracts.Count;
                Session["grdDetailData"] = (grdContractList.DataSource as List<Cont_SP_GetAssignedContracts_All_Result>).ToDataTable();

                if (lstAssignedContracts.Count > 0)
                    btnExportExcel.Enabled = true;
                else
                    btnExportExcel.Enabled = false;

                lstAssignedContracts.Clear();
                lstAssignedContracts = null;

                // bindPageNumber();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvGraphDetail.IsValid = false;
                cvGraphDetail.ErrorMessage = "Something went wrong, Please try again.";
                cvGraphDetail.CssClass="alert alert-danger";
            }
        }

        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }

                //var StartRecord = DropDownListPageNo.SelectedItem.Text;
                //var TotalRecord = Session["TotalRows"].ToString();
                //lblStartRecord.Text = StartRecord.ToString();
                //lblEndRecord.Text = Convert.ToString(count) + " ";
                //lblTotalRecord.Text = TotalRecord.ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void ShowGridDetail()
        {
            if (Session["TotalRows"] != null)
            {
                var PageSize = Convert.ToInt32(grdContractList.PageSize);
                var PageNumber = Convert.ToInt32(DropDownListPageNo.SelectedValue);
                var EndRecord = 0;
                var TotalRecord = 0;
                var TotalValue = PageSize * PageNumber;

                TotalRecord = Convert.ToInt32(Session["TotalRows"]);
                if (TotalRecord < TotalValue)
                {
                    EndRecord = TotalRecord;
                }
                else
                {
                    EndRecord = TotalValue;
                }

                if (TotalRecord != 0)
                    lblStartRecord.Text = Convert.ToString(PageSize * PageNumber - PageSize + 1);
                else
                    lblStartRecord.Text = "0";

                lblEndRecord.Text = Convert.ToString(EndRecord) + " ";
                lblTotalRecord.Text = TotalRecord.ToString();
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdContractList.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);

                BindGrid();
                bindPageNumber();

                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdContractList.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }

                ShowGridDetail();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdContractList.PageIndex = chkSelectedPage - 1;

            grdContractList.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);

            BindGrid();
            ShowGridDetail();
        }
    }
}