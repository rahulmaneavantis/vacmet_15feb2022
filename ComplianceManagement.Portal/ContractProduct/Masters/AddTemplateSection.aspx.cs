﻿using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;

namespace com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.Masters
{
    public partial class AddTemplateSection : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    theEditor.TrackChangesSettings.CanAcceptTrackChanges = false;
                    theEditor.EnableTrackChanges = false;
                    theEditor.Content = "";

                    BindDepartment();
                    if (!string.IsNullOrEmpty(Request.QueryString["accessID"]))
                    {
                        int customerID = -1;
                        customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                        long sectionID = Convert.ToInt64(Request.QueryString["accessID"]);

                        var objRecord = ContractTemplateManagement.GetSectionDetailsByID(sectionID, customerID);

                        if (objRecord != null)
                        {
                            tbxHeader.Text = objRecord.Header;
                            //tbxContent.Text = objRecord.BodyContent;
                            theEditor.Content = objRecord.BodyContent;
                            if (objRecord.DeptID != null)
                            {
                                if (objRecord.DeptID > 0)
                                {
                                    ddlDeptPage.SelectedValue = Convert.ToString(objRecord.DeptID);
                                }
                            }
                            if (objRecord.Headervisibilty != null)
                            {
                                if (objRecord.Headervisibilty > 0)
                                {
                                    ddlVisibilitySectionHeader.SelectedValue = Convert.ToString(objRecord.Headervisibilty);
                                }
                            }
                           
                            ViewState["Mode"] = 1;
                            ViewState["SectionID"] = sectionID;
                        }
                    }
                    else
                    {
                        ViewState["Mode"] = 0;
                    }

                    if (!string.IsNullOrEmpty(Request.QueryString["ViewOnly"]))
                    {
                        if (Convert.ToInt32(Request.QueryString["ViewOnly"]) == 0)
                            enableDisableControls(true);
                        else
                            enableDisableControls(false);
                    }
                    else
                        enableDisableControls(true);
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvTemplateSection.IsValid = false;
                    cvTemplateSection.ErrorMessage = "Something went wrong, Please try again";
                }
            }
        }
        public void BindDepartment()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            var obj = CompDeptManagement.GetAllDepartmentMasterList(customerID);

            //Page DropDown
            ddlDeptPage.DataTextField = "Name";
            ddlDeptPage.DataValueField = "ID";

            ddlDeptPage.DataSource = obj;
            ddlDeptPage.DataBind();

            ddlDeptPage.Items.Insert(0, new ListItem("Select Department", "-1"));
        }

        //protected void btnSave_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        int customerID = -1;
        //        customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

        //        bool validateSuccess = false;
        //        List<string> lstErrorMsg = new List<string>();

        //        #region Validation

        //        if (!string.IsNullOrEmpty(tbxHeader.Text))
        //            validateSuccess = true;
        //        else
        //            lstErrorMsg.Add("Required Header");

        //        if (!string.IsNullOrEmpty(tbxContent.Text))
        //            validateSuccess = true;
        //        else
        //            lstErrorMsg.Add("Required Content");

        //        List<long> lstDeptMapping = new List<long>();
        //        if (lstBoxDepartment.Items.Count > 0)
        //        {
        //            foreach (ListItem eachdept in lstBoxDepartment.Items)
        //            {
        //                if (eachdept.Selected)
        //                {
        //                    if (Convert.ToInt64(eachdept.Value) != 0)
        //                        lstDeptMapping.Add(Convert.ToInt64(eachdept.Value));
        //                }
        //            }
        //        }
        //        if (lstDeptMapping.Count == 0)
        //        {
        //            validateSuccess = false;
        //            lstErrorMsg.Add("Required Department");
        //        }

        //        if (lstErrorMsg.Count > 0)
        //        {
        //            validateSuccess = false;
        //            showErrorMessages(lstErrorMsg, cvTemplateSection);
        //        }
        //        #endregion

        //        if (validateSuccess)
        //        {
        //            if ((int)ViewState["Mode"] == 0)
        //            {
        //                if (lstDeptMapping.Count > 0)
        //                {
        //                    Cont_tbl_SectionMaster _objSection = new Cont_tbl_SectionMaster()
        //                    {
        //                        Header = tbxHeader.Text,
        //                        BodyContent = tbxContent.Text,
        //                        CustomerID = customerID,
        //                        CreatedBy = AuthenticationHelper.UserID,
        //                        CreatedOn = DateTime.Now,
        //                        IsActive = true,
        //                    };
        //                    if (ddlVisibilitySectionHeader.SelectedValue != null && ddlVisibilitySectionHeader.SelectedValue != "-1")
        //                    {
        //                        _objSection.Headervisibilty = Convert.ToInt32(ddlVisibilitySectionHeader.SelectedValue);
        //                    }

        //                    if (ContractTemplateManagement.ExistsSection(_objSection, customerID, 0))
        //                    {
        //                        cvTemplateSection.IsValid = false;
        //                        cvTemplateSection.ErrorMessage = "Section with Same Header and Version already exists";
        //                    }
        //                    else
        //                    {
        //                        long newSectionID = 0;

        //                        newSectionID = ContractTemplateManagement.CreateSection(_objSection);

        //                        if (newSectionID > 0)
        //                        {
        //                            List<Cont_tbl_SectionDeptMapping> lstDeptMapping_ToSave = new List<Cont_tbl_SectionDeptMapping>();

        //                            lstDeptMapping.ForEach(EachDept =>
        //                            {
        //                                Cont_tbl_SectionDeptMapping mappingdept = new Cont_tbl_SectionDeptMapping()
        //                                {
        //                                    SectionID = newSectionID,
        //                                    CustomerID = customerID,
        //                                    DeptID = EachDept,
        //                                    IsActive = true,
        //                                    CreatedBy = AuthenticationHelper.UserID,
        //                                    CreatedOn = DateTime.Now,
        //                                };
        //                                lstDeptMapping_ToSave.Add(mappingdept);
        //                            });
        //                            if (lstDeptMapping_ToSave.Count > 0)
        //                            {
        //                                bool deactivatedetails = ContractManagement.DeactivateDeptSectionMapping(Convert.ToInt32(newSectionID), Convert.ToInt32(AuthenticationHelper.CustomerID));
        //                                bool saveDeptSuccess = ContractManagement.CreateUpdateSectionDeptMapping(lstDeptMapping_ToSave);
        //                                if (saveDeptSuccess)
        //                                {
        //                                    ContractManagement.CreateAuditLog("C", newSectionID, "Cont_tbl_SectionDeptMapping", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Section Department Mapping Created", true);
        //                                }
        //                            }
        //                            if (cvTemplateSection.IsValid)
        //                            {
        //                                divsuccessmsgaCTemSec.Visible = true;
        //                                successmsgaCTemSec.Text = "Section Save Successfully.";
        //                                ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "script", "window.setTimeout(function() { document.getElementById('" + divsuccessmsgaCTemSec.ClientID + "').style.display = 'none' },2000);", true);
        //                            }

        //                            clearControls();

        //                            Session["SectionID"] = newSectionID;
        //                        }
        //                        else
        //                        {
        //                            cvTemplateSection.IsValid = false;
        //                            cvTemplateSection.ErrorMessage = "Something went wrong, Please try again";
        //                        }
        //                    }
        //                }
        //            }
        //            else if ((int)ViewState["Mode"] == 1)
        //            {
        //                Cont_tbl_SectionMaster _objSection = new Cont_tbl_SectionMaster()
        //                {
        //                    Header = tbxHeader.Text,
        //                    BodyContent = tbxContent.Text,
        //                    CustomerID = customerID,
        //                    CreatedBy = AuthenticationHelper.UserID,
        //                    CreatedOn = DateTime.Now,
        //                    IsActive = true,
        //                };
        //                if (ddlVisibilitySectionHeader.SelectedValue != null && ddlVisibilitySectionHeader.SelectedValue != "-1")
        //                {
        //                    _objSection.Headervisibilty = Convert.ToInt32(ddlVisibilitySectionHeader.SelectedValue);
        //                }
        //                //if (lstBoxDepartment.SelectedValue != null && lstBoxDepartment.SelectedValue != "-1")
        //                //{
        //                //    _objSection.DeptID = Convert.ToInt32(lstBoxDepartment.SelectedValue);
        //                //}

        //                _objSection.ID = Convert.ToInt64(ViewState["SectionID"]);

        //                if (ContractTemplateManagement.ExistsSection(_objSection, customerID, _objSection.ID))
        //                {
        //                    cvTemplateSection.IsValid = false;
        //                    cvTemplateSection.ErrorMessage = "Section with Same Header and Version already exists";
        //                }
        //                else
        //                {
        //                    _objSection.UpdatedBy = AuthenticationHelper.UserID;
        //                    _objSection.UpdatedOn = DateTime.Now;

        //                    bool saveSuccess = ContractTemplateManagement.UpdateSection(_objSection);

        //                    if (saveSuccess)
        //                    {
        //                        List<Cont_tbl_SectionDeptMapping> lstDeptMapping_ToSave = new List<Cont_tbl_SectionDeptMapping>();

        //                        lstDeptMapping.ForEach(EachDept =>
        //                        {
        //                            Cont_tbl_SectionDeptMapping mappingdept = new Cont_tbl_SectionDeptMapping()
        //                            {
        //                                SectionID = _objSection.ID,
        //                                CustomerID = customerID,
        //                                DeptID = EachDept,
        //                                IsActive = true,
        //                                CreatedBy = AuthenticationHelper.UserID,
        //                                CreatedOn = DateTime.Now,
        //                            };
        //                            lstDeptMapping_ToSave.Add(mappingdept);
        //                        });
        //                        if (lstDeptMapping_ToSave.Count > 0)
        //                        {
        //                            bool deactivatedetails = ContractManagement.DeactivateDeptSectionMapping(Convert.ToInt32(_objSection.ID), Convert.ToInt32(AuthenticationHelper.CustomerID));
        //                            bool saveDeptSuccess = ContractManagement.CreateUpdateSectionDeptMapping(lstDeptMapping_ToSave);
        //                            if (saveDeptSuccess)
        //                            {
        //                                ContractManagement.CreateAuditLog("C", _objSection.ID, "Cont_tbl_SectionDeptMapping", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Section Department Mapping Updated", true);
        //                            }
        //                        }

        //                        if (cvTemplateSection.IsValid)
        //                        {
        //                            divsuccessmsgaCTemSec.Visible = true;
        //                            successmsgaCTemSec.Text = "Section Updated Successfully";
        //                            ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "script", "window.setTimeout(function() { document.getElementById('" + divsuccessmsgaCTemSec.ClientID + "').style.display = 'none' },2000);", true);

        //                        }
        //                    }
        //                    else
        //                    {
        //                        cvTemplateSection.IsValid = false;
        //                        cvTemplateSection.ErrorMessage = "Something went wrong, Please try again";
        //                    }
        //                }
        //            }
        //        }

        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "bindTinyMCE", "bind_tinyMCE();", true);

        //        if (!cvTemplateSection.IsValid)
        //            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "scrollUpPageScript", "scrollUp();", true);

        //        //upTemplateSection.Update();
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvTemplateSection.IsValid = false;
        //        cvTemplateSection.ErrorMessage = "Something went wrong, Please try again";
        //    }
        //}


        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                bool validateSuccess = false;
                List<string> lstErrorMsg = new List<string>();

                #region Validation

                if (!string.IsNullOrEmpty(tbxHeader.Text))
                    validateSuccess = true;
                else
                    lstErrorMsg.Add("Please Enter Header.");

                if (!string.IsNullOrEmpty(theEditor.Content))
                    validateSuccess = true;
                else
                    lstErrorMsg.Add("Please Enter Content.");

                //if (!string.IsNullOrEmpty(tbxContent.Text))
                //    validateSuccess = true;
                //else
                //    lstErrorMsg.Add("Please Enter Content.");

                //if (ddlDeptPage.SelectedValue != null && ddlDeptPage.SelectedValue != "" && ddlDeptPage.SelectedValue != "-1")
                //{
                //    validateSuccess = true;
                //}
                //else
                //{
                //    lstErrorMsg.Add("Please Select Department.");
                //}

                if (lstErrorMsg.Count > 0)
                {
                    validateSuccess = false;
                    showErrorMessages(lstErrorMsg, cvTemplateSection);
                }
                #endregion

                if (validateSuccess)
                {
                    Cont_tbl_SectionMaster _objSection = new Cont_tbl_SectionMaster()
                    {
                        Header = tbxHeader.Text,
                        //BodyContent = tbxContent.Text,
                        BodyContent = theEditor.Content,
                        CustomerID = customerID,
                        CreatedBy = AuthenticationHelper.UserID,
                        CreatedOn = DateTime.Now,
                        IsActive = true,
                    };
                    if (ddlVisibilitySectionHeader.SelectedValue != null && ddlVisibilitySectionHeader.SelectedValue != "-1")
                    {
                        _objSection.Headervisibilty = Convert.ToInt32(ddlVisibilitySectionHeader.SelectedValue);
                    }

                    if (ddlDeptPage.SelectedValue != null && ddlDeptPage.SelectedValue != "" && ddlDeptPage.SelectedValue != "-1")
                    {
                        _objSection.DeptID = Convert.ToInt32(ddlDeptPage.SelectedValue);
                    }

                    if ((int)ViewState["Mode"] == 0)
                    {
                        if (ContractTemplateManagement.ExistsSection(_objSection, customerID, 0))
                        {
                            cvTemplateSection.IsValid = false;
                            cvTemplateSection.ErrorMessage = "Section with same Header name already exist.";
                        }
                        else
                        {
                            long newSectionID = 0;

                            newSectionID = ContractTemplateManagement.CreateSection(_objSection);

                            if (newSectionID > 0)
                            {
                                if (cvTemplateSection.IsValid)
                                {
                                    divsuccessmsgaCTemSec.Visible = true;
                                    successmsgaCTemSec.Text = "Section Saved Successfully.";
                                    //ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "script", "window.setTimeout(function() { document.getElementById('" + divsuccessmsgaCTemSec.ClientID + "').style.display = 'none' },2000);", true);
                                }

                                clearControls();

                                Session["SectionID"] = newSectionID;
                            }
                            else
                            {
                                cvTemplateSection.IsValid = false;
                                cvTemplateSection.ErrorMessage = "Something went wrong, Please try again";
                            }
                        }
                    }
                    else if ((int)ViewState["Mode"] == 1)
                    {
                        _objSection.ID = Convert.ToInt64(ViewState["SectionID"]);

                        if (ContractTemplateManagement.ExistsSection(_objSection, customerID, _objSection.ID))
                        {
                            cvTemplateSection.IsValid = false;
                            cvTemplateSection.ErrorMessage = "Section with same Header name already exist.";
                        }
                        else
                        {
                            _objSection.UpdatedBy = AuthenticationHelper.UserID;
                            _objSection.UpdatedOn = DateTime.Now;

                            bool saveSuccess = ContractTemplateManagement.UpdateSection(_objSection);

                            if (saveSuccess)
                            {
                                if (cvTemplateSection.IsValid)
                                {
                                    divsuccessmsgaCTemSec.Visible = true;
                                    successmsgaCTemSec.Text = "Section Updated Successfully";
                                    ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "script", "window.setTimeout(function() { document.getElementById('" + divsuccessmsgaCTemSec.ClientID + "').style.display = 'none' },2000);", true);

                                }
                            }
                            else
                            {
                                cvTemplateSection.IsValid = false;
                                cvTemplateSection.ErrorMessage = "Something went wrong, Please try again";
                            }
                        }
                    }
                }

                //ScriptManager.RegisterStartupScript(this, this.GetType(), "bindTinyMCE", "bind_tinyMCE();", true);

                //if (!cvTemplateSection.IsValid)
                //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "scrollUpPageScript", "scrollUp();", true);

               //upTemplateSection.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvTemplateSection.IsValid = false;
                cvTemplateSection.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        private void clearControls()
        {
            tbxHeader.Text = string.Empty;     
            //tbxContent.Text = string.Empty;
            theEditor.Content = string.Empty;
            ddlDeptPage.SelectedValue = "-1";
        }

        private void enableDisableControls(bool flag)
        {
            tbxHeader.Enabled = flag;            
            //tbxContent.Enabled = flag;
            theEditor.Enabled = flag;
            ddlDeptPage.Enabled = flag;
            ddlVisibilitySectionHeader.Enabled = flag;
            btnSave.Enabled = flag;
        }

        public void showErrorMessages(List<string> lstErrMsgs, CustomValidator cvtoShowErrorMsg)
        {
          
            string finalErrMsg = string.Empty;

            finalErrMsg += "<ol type='1'>";

            if (lstErrMsgs.Count > 0)
            {
                lstErrMsgs.ForEach(eachErrMsg =>
                {
                    finalErrMsg += "<li>" + eachErrMsg + "</li>";
                });

                finalErrMsg += "</ol>";
            }

            cvtoShowErrorMsg.IsValid = false;
            cvtoShowErrorMsg.ErrorMessage = finalErrMsg;
        }
    }
}