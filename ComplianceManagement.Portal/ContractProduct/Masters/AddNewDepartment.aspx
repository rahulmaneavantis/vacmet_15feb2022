﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddNewDepartment.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.Masters.AddNewDepartment" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <title></title>
    <!-- Bootstrap CSS -->
    <link href="../../NewCSS/bootstrap.min.css" rel="stylesheet" />
    <!-- bootstrap theme -->
    <link href="../../NewCSS/bootstrap-theme.css" rel="stylesheet" />
    <!--external css-->
    <!-- font icon -->
    <link href="../../NewCSS/elegant-icons-style.css" rel="stylesheet" />
    <link href="../../NewCSS/font-awesome.css" rel="stylesheet" />
    <!-- Custom styles -->
    <link href="../../NewCSS/style.css" rel="stylesheet" />
    <link href="../../NewCSS/style-responsive.css" rel="stylesheet" />
    <script type="text/javascript" src="../../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery-ui-1.9.2.custom.min.js"></script>
     <link href="~/NewCSS/contract_custom_style.css" rel="stylesheet" />
    <script>
        function CloseMe() {
            window.parent.ClosePopDepartment();
        }
        function getColor()
        {
            document.getElementById('<%=Txtboxcolor.ClientID %>').value = document.getElementById("favcolor").value;

            return true;
        }
    </script>
</head>
<body style="background-color: white">
    <form id="form1" runat="server">
        <div>
            <asp:ScriptManager ID="smAddNewDept" runat="server"></asp:ScriptManager>
            <asp:UpdatePanel ID="upDepartment" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div>
                        <div class="row form-group">
                            <asp:ValidationSummary ID="vsAddEditDept" runat="server" Display="none" class="alert alert-block alert-danger fade in" ValidationGroup="DepartmentPopupValidationGroup" />
                            <asp:CustomValidator ID="cvDeptPopup" runat="server" EnableClientScript="False"
                                ValidationGroup="DepartmentPopupValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                        </div>

                        <div class="row form-group">
                            <div class="form-group required col-md-4">
                                <label for="tbxSubContType" class="control-label">Department Name</label>
                                <asp:TextBox runat="server" ID="txtFName" CssClass="form-control" />
                                <%--<asp:RequiredFieldValidator ID="rfvDeptName" ErrorMessage="Please Enter Department Name." ControlToValidate="txtFName"
                                    runat="server" ValidationGroup="DepartmentPopupValidationGroup" Display="None" />--%>
                            </div>
                        </div>

                        
                        <div class="row form-group">
                            <div class="form-group required col-md-4">
                                <label for="tbxSubContType" class="control-label">Select Department Name Colour</label>
                                    <input type="color" id="favcolor" runat="server" style="margin-left: 36px;" name="favcolor" value="#ff0000"><br> 
                                    <asp:TextBox runat="server" Style="display:none;" ID="Txtboxcolor" CssClass="form-control" />            
                            </div>
                        </div>

                        <div class="row form-group text-center">
                            <div class="col-md-12">
                                <asp:Button Text="Save" runat="server" ID="btnSave" OnClientClick="return getColor();" OnClick="btnSave_Click" CssClass="btn btn-primary"
                                    ValidationGroup="DepartmentPopupValidationGroup" />
                                <asp:Button Text="Close" runat="server" ID="btnCloseDeptPopUp" CssClass="btn btn-primary"
                                    data-dismiss="modal" OnClientClick="CloseMe()" />
                            </div>
                        </div>                      
                       
                       <div class="row">
                            <div class="col-md-12">
                                <p style="color: red;"><strong>Note:</strong> (*) fields are Compulsory</p>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </form>
    <%--<script src="../Scripts/binddropdownusingajax.js"></script>--%>
</body>
</html>
