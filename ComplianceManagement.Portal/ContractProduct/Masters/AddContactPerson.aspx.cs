﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
namespace com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.Masters
{
    public partial class AddContactPerson : System.Web.UI.Page
    {
        public static string ContID = string.Empty;
        public bool DisplayOnlyNonAdmin;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["Mode"] = 0;
                DisplayOnlyNonAdmin = CaseManagement.CheckForClient(Convert.ToInt32(AuthenticationHelper.CustomerID), "DisplayOnlyNonAdmin");
                BindRoles();
            }

        }

        private void BindRoles()
        {
            try
            {
                ddlContractRole.DataTextField = "Name";
                ddlContractRole.DataValueField = "ID";

                var roles = RoleManagement.GetLitigationRoles(true);

                if (DisplayOnlyNonAdmin) {
                    roles = roles.Where(x => x.Code == "EXCT").ToList();
                }
                else
                {
                    if (AuthenticationHelper.Role == "CADMN" || AuthenticationHelper.Role == "MGMT")
                    {
                        roles = roles.Where(entry => !entry.Code.Equals("SADMN") && !entry.Code.Equals("IMPT")).ToList();
                    }

                    if (AuthenticationHelper.Role == "SADMN" || AuthenticationHelper.Role == "IMPT")
                    {
                        roles = roles.Where(entry => !entry.Code.Equals("IMPT")).ToList();
                    }
                }

                ddlContractRole.DataSource = roles.OrderBy(entry => entry.Name);
                ddlContractRole.DataBind();
                ddlContractRole.Items.Insert(0, new ListItem("Select User Role", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        //protected void ddlContractRole_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        string roleCode = string.Empty;
        //        var role = RoleManagement.GetByID(Convert.ToInt32(ddlContractRole.SelectedValue));
        //        if (role != null)
        //        {
        //            roleCode = role.Code;
        //        }

               
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvDuplicateEntry.IsValid = false;
        //        cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
        //    }
        //}

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                long ContactNo = 0;
                bool MailValid = false;
                string MailID = string.Empty;

                int customerID = -1;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                if (!string.IsNullOrEmpty(tbxContactNo.Text))
                {
                    ContactNo = Convert.ToInt64(tbxContactNo.Text);
                }
                if (!string.IsNullOrEmpty(tbxEmail.Text))
                {
                    MailID = tbxEmail.Text;
                }

                if (ZohoCRMAPI.IsValidEmail(MailID))
                {
                    MailValid = true;
                }

                #region Contract User
                User user = new User()
                {
                    IsActive = true,
                    IsExternal = true,
                    CustomerID = customerID,
                    IsDeleted = false,
                    FirstName = tbxFirstName.Text.Trim(),
                    LastName = tbxLastName.Text.Trim(),
                    Email = tbxEmail.Text.Trim(),
                    ContactNumber = tbxContactNo.Text.Trim(),
                    CreatedBy = AuthenticationHelper.UserID,
                    CreatedByText = AuthenticationHelper.User,
                    RoleID = 7,
                    ContractRoleID = Convert.ToInt32(ddlContractRole.SelectedValue),
                };
                #endregion

                #region Audit User

                com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_User mstUser = new com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_User()
                {
                    IsActive = true,
                    IsExternal = true,
                    CustomerID = customerID,
                    IsDeleted = false,
                    FirstName = tbxFirstName.Text.Trim(),
                    LastName = tbxLastName.Text.Trim(),
                    Email = tbxEmail.Text.Trim(),
                    ContactNumber = tbxContactNo.Text.Trim(),

                    CreatedBy = AuthenticationHelper.UserID,
                    CreatedByText = AuthenticationHelper.User,

                    RoleID = 7,
                    ContractRoleID = Convert.ToInt32(ddlContractRole.SelectedValue),
                };

                #endregion

                bool emailExists;

                UserManagement.Exists(user, out emailExists);
                if (emailExists)
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "User with Same Email already Exists";
                    cvDuplicateEntry.CssClass = "alert alert-danger";
                    return;
                }

                UserManagementRisk.Exists(mstUser, out emailExists);
                if (emailExists)
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "User with Same Email already Exists";
                    cvDuplicateEntry.CssClass = "alert alert-danger";
                    return;
                }

                bool result = false;
                int newUserID = 0;

                if ((int)ViewState["Mode"] == 0)
                {
                    string passwordText = Util.CreateRandomPassword(10);

                    user.Password = Util.CalculateAESHash(passwordText);
                    mstUser.Password = Util.CalculateAESHash(passwordText);

                    if (Convert.ToString(ContactNo).Length == 10 && (Convert.ToString(ContactNo).StartsWith("9") || Convert.ToString(ContactNo).StartsWith("8") || Convert.ToString(ContactNo).StartsWith("7")))
                    {
                        if (MailValid)
                        {
                            newUserID = ContractUserManagement.CreateNewUserCompliance(user);
                            if (newUserID > 0)
                            {
                                result = ContractUserManagement.CreateNewUserAudit(mstUser);

                                if (!result)
                                {

                                    UserManagement.deleteUser(newUserID);
                                }
                                else
                                {
                                    ContID = newUserID.ToString();
                                    string message = getEmailMessage(user, passwordText);
                                    EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(new String[] { user.Email }), null, null, "AVACOM Account Created.", message);
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "User Created Successfully.";
                                    vsAddUserContract.CssClass = "alert alert-success";
                                }
                            }
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Invalid Email, Please enter Email in valid format.";
                            cvDuplicateEntry.CssClass = "alert alert-danger";
                        }
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Invalid Contact Number or Contact Number should start with 9, 8 or 7.";
                        cvDuplicateEntry.CssClass = "alert alert-danger";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        private string getEmailMessage(User user, string passwordText)
        {
            try
            {
                int customerID = -1;
                string ReplyEmailAddressName = "Teamlease";
               
                string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                string accessURL = string.Empty;
                URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(AuthenticationHelper.CustomerID));
                if (Urloutput != null)
                {
                    accessURL = Urloutput.URL;
                }
                else
                {
                    accessURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                }
                string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_UserRegistration
                                        .Replace("@Username", user.Email)
                                        .Replace("@User", username)
                                        .Replace("@PortalURL", Convert.ToString(accessURL))
                                        .Replace("@Password", passwordText)
                                        .Replace("@From", ReplyEmailAddressName)
                                        .Replace("@URL", Convert.ToString(accessURL));
                return message;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.Visible = false;
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
                return null;
            }
        }

        [WebMethod]
        public static string getContUserID()
        {

            try
            {
                string CUserID = string.Empty;
                if (ContID != null)
                {
                    CUserID = ContID;
                }
                return CUserID;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "null";
            }
        }
    }
}