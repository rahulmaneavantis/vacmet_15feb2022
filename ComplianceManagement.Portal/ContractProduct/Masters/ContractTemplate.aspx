﻿<%@ Page Title="Template Master:: Contract" Language="C#" MasterPageFile="~/ContractProduct.Master" AutoEventWireup="true" CodeBehind="ContractTemplate.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.Masters.ContractTemplate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
          $(function () {
          
            <% if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.Role == "EXCT")
                            {%>         
            document.getElementById('<%=LnkbtnPrevious.ClientID%>').style.visibility = 'hidden';
            document.getElementById('<%=btnSection.ClientID%>').style.visibility = 'hidden';           
               <%}%>
          });

        function OpenContractModal(templateID) {
            $('#ContractTemplatePopup').modal('show');
            $('#<%# IframeContractTemplate.ClientID%>').attr('src', "/ContractProduct/Masters/AddContractTemplate.aspx?accessID=" + templateID);
        }
        
        function CloseContractModal() {
            $('#ContractTemplatePopup').modal('hide');
            document.getElementById('<%= lnkBtn_RebindGrid.ClientID %>').click();
            //window.location.href = window.location.href;
        }

        
        
        function OpenContractLogModal(templateID) {
            $('#CTLOGPopup').modal('show');
            $('#<%# IframeCTlog.ClientID%>').attr('src', "/ContractProduct/Masters/AddContractTemplateLog.aspx?accessID=" + templateID);
        }

        function CloseContractLogModal(templateID) {
            $('#CTLOGPopup').modal('hide');
            document.getElementById('<%= lnkBtn_RebindGrid.ClientID %>').click();
         }
        function RefreshParent() {
            document.getElementById('<%= lnkBtn_RebindGrid.ClientID %>').click();
            //window.location.href = window.location.href;
        }
    </script>


    <script type="text/javascript">
        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);
        window.onunload = function () { null };

        $(document).ready(function () {
            fhead('Masters/ Contract Template(s)');
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row Dashboard-white-widget">
        <div class="dashboard">
            <div class="row">
                 <div class="col-md-12 alert alert-success" runat="server" visible="false" id="divMsuccessmsgaCTem">
                            <asp:Label runat="server" ID="MsuccessmsgaCTem" ></asp:Label>
                        </div>
                <asp:ValidationSummary ID="vsContractTemplatePage" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                    ValidationGroup="ContractTemplatePageValidationGroup" />
                <asp:CustomValidator ID="cvContractTemplatePage" runat="server" EnableClientScript="False"
                    ValidationGroup="ContractTemplatePageValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-6 colpadding0">
                        <asp:TextBox runat="server" ID="tbxFilter" CssClass="form-control" AutoComplete="off" PlaceHolder="Type Template Name to Search" AutoPostBack="true"
                            OnTextChanged="tbxFilter_TextChanged" Width="70%" />
                    </div>

                    <div class="col-md-4 colpadding0 text-right">
                        <asp:LinkButton ID="lnkBtn_RebindGrid" OnClick="lnkBtn_RebindGrid_Click" Style="display: none;" runat="server"></asp:LinkButton>
                    </div>

                    <div class="col-md-2 colpadding0 text-right">
                        <asp:LinkButton runat="server" ID="btnSection" OnClick="btnSection_Click" Height="25px"
                            data-toggle="tooltip" data-placement="bottom" title="Add/Edit Section(s)">
                            <img src='/Images/edit_icon_new.png' alt="Section" style="margin-top:5px;"/>
                        </asp:LinkButton>
                        <asp:LinkButton runat="server" ID="btnAddNew" OnClick="btnAddNew_Click"
                            data-toggle="tooltip" data-placement="bottom" title="Add New Template" CssClass="btn btn-primary">
                            <span class="AddNewspan1"><i class='fa fa-plus'></i></span>&nbsp;New</asp:LinkButton>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12">
                    <asp:GridView runat="server" ID="grdContractTemplates" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true"
                        AllowSorting="true" GridLines="none" Width="100%" PageSize="10" AllowPaging="true"
                        OnRowDataBound="grdContractTemplates_RowDataBound" AutoPostBack="true" CssClass="table" DataKeyNames="ID"
                        OnRowCommand="grdContractTemplates_RowCommand" OnSorting="grdContractTemplates_Sorting" OnRowCreated="grdContractTemplates_RowCreated">
                        <%--OnRowCreated="grdContractType_RowCreated" OnSorting="grdContractType_Sorting" OnRowDataBound="grdContractType_RowDataBound" OnRowCommand="grdContractType_RowCommand"--%>
                        <Columns>
                            <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr.No." ItemStyle-Width="5%">
                                <ItemTemplate>
                                    <%#Container.DataItemIndex+1 %>
                                    <%--<asp:Label ID="lblRowID" runat="server" Text='<%# Eval("RowID") %>'></asp:Label>--%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Template" SortExpression="ContactNumber" ItemStyle-Width="35%">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%;">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("TemplateName") %>' ToolTip='<%# Eval("TemplateName") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Status" SortExpression="Status" ItemStyle-Width="25%">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%;">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Status") %>' ToolTip='<%# Eval("Status") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                            <%--<asp:BoundField DataField="TemplateName" HeaderText="Template" ItemStyle-Width="35%" SortExpression="TemplateName" />
                            <asp:BoundField DataField="Status" HeaderText="Status" ItemStyle-Width="25%" SortExpression="Status" />--%>
                            <%--<asp:BoundField DataField="Version" HeaderText="Version" ItemStyle-Width="15%" SortExpression="Version" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center" />--%>

                            <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="right" ItemStyle-Width="40%"
                                HeaderStyle-CssClass="text-right" ItemStyle-CssClass="text-right">
                                <ItemTemplate>

                                        <asp:LinkButton ID="lnkEditApprovalLevel" runat="server" CommandName="EDIT_ApprovalLevel"
                                        title="Edit Approval Level" data-toggle="tooltip"     
                                             Visible='<%# CanChangeDisplayApprover((int)Eval("CreatedBy"),(string)Eval("Status")) %>'
                                        CommandArgument='<%# Eval("ID") %>'>                                   
                                    <img src='<%# ResolveUrl("/Images/EditICON.png")%>' alt="Edit Approval Level" />
                                    </asp:LinkButton>

                                    <asp:LinkButton ID="lnkEditTemplate" runat="server" CommandName="EDIT_Template"
                                        title="Edit Template" data-toggle="tooltip"                                         
                                        CommandArgument='<%# Eval("ID") %>'>                                   
                                    <img src='<%# ResolveUrl("/Images/edit_icon_new.png")%>' alt="Edit" />
                                    </asp:LinkButton>

                                    <asp:LinkButton ID="lnkDeleteTemplate" runat="server" CommandName="DELETE_Template"
                                      Visible='<%# CanChangeStatusNewDelete((int)Eval("CreatedBy")) %>'
                                     ToolTip="Delete Template" data-toggle="tooltip"
                                        CommandArgument='<%# Eval("ID") %>' OnClientClick="return confirm('Are you certain you want to delete this Contract Template?');">
                                    <img src='<%# ResolveUrl("/Images/delete_icon_new.png")%>' alt="Delete" />
                                    </asp:LinkButton>

                                    <asp:LinkButton ID="lnkExportTemplate" runat="server" CommandName="EXPORT_Template" ToolTip="Export to Word" data-toggle="tooltip"
                                     CommandArgument='<%# Eval("ID")%>'>
                                    <img src='<%# ResolveUrl("/Images/download_icon_new.png")%>' alt="Export" />
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <RowStyle CssClass="clsROWgrid" />
                        <HeaderStyle CssClass="clsheadergrid" />
                        <PagerTemplate>
                            <table style="display: none">
                                <tr>
                                    <td>
                                        <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                    </td>
                                </tr>
                            </table>
                        </PagerTemplate>
                        <EmptyDataTemplate>
                            No Record Found
                        </EmptyDataTemplate>
                    </asp:GridView>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-10 colpadding0">
                        <div runat="server" id="DivRecordsScrum" style="float: left; margin-top: 5px; color: #999">
                            <p style="padding-right: 0px !Important;">
                                <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>- 
                                <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                                <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                            </p>
                        </div>
                    </div>

                    <div class="col-md-1 colpadding0">
                        <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control" Style="width: 100%; float: right; margin-right: 6%;"
                            AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                            <asp:ListItem Text="5" />
                            <asp:ListItem Text="10" Selected="True" />
                            <asp:ListItem Text="20" />
                            <asp:ListItem Text="50" />
                        </asp:DropDownList>
                    </div>

                    <div class="col-md-1 colpadding0" style="float: right;">
                        <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true" DataPlaceHolder="No" AllowSingleDeselect="false"
                            OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged" class="form-control" Width="100%" Height="30px">
                        </asp:DropDownListChosen>
                    </div>
                </div>
                <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
            </div>
            <div class="row">
                <div class="col-md-12" style="margin-top: 1%;">
                    <div class="col-md-6 colpadding0">
                        <div class="col-md-2 colpadding0">
                            <asp:LinkButton Text="Previous" CssClass="btn btn-primary" ToolTip="Go to Previous Master (Custom Field)" data-toggle="tooltip"
                                runat="server" ID="LnkbtnPrevious" Width="100%" OnClick="lnkPreviousSwitch_Click" />
                        </div>
                        <div class="col-md-10 colpadding0"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <%--<div class="modal fade" id="ContractTemplatePopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
         <div class="modal-dialog w50per" style="float: left;">
            <div class="modal-content" style="width:1323px;margin-left: 17%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="RefreshParent()">&times;</button>
                </div>
                <div class="modal-body w100per">
                    <iframe src="about:blank" id="IframeContractTemplate" runat="server" frameborder="0"  width="100%" height="648px"></iframe>
                </div>
            </div>
        </div>
    </div>--%>
         <div class="modal fade" id="ContractTemplatePopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow: hidden">
            <div class="modal-dialog" style="width: 98%">

                <div class="modal-content">
                    <div class="modal-header" style="background-color: #f7f7f7; height: 36px;">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                    </div>
                    <div class="modal-body" style="background-color: #f7f7f7; width: 100%; max-height: 648px; overflow-y: hidden;">
                        <iframe id="IframeContractTemplate" runat="server"  src="about:blank" width="100%" height="648px" frameborder="0"></iframe>
                    </div>
                </div>
            </div>
        </div>
      <div class="modal fade" id="CTLOGPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow: hidden">
            <div class="modal-dialog" style="width: 98%">

                <div class="modal-content">
                    <div class="modal-header" style="background-color: #f7f7f7; height: 36px;">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                    </div>
                    <div class="modal-body" style="background-color: #f7f7f7; width: 100%; max-height: 648px; overflow-y: hidden;">
                        <iframe id="IframeCTlog" runat="server"  src="about:blank" width="100%" height="648px" frameborder="0"></iframe>
                    </div>
                </div>
            </div>
        </div>
 <%-- <div class="modal fade" id="CTLOGPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
         <div class="modal-dialog w50per" style="float: left;">
            <div class="modal-content" style="width:1323px;margin-left: 17%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="RefreshParent()">&times;</button>
                </div>
                <div class="modal-body w100per">
                    <iframe src="about:blank" id="IframeCTlog" runat="server" frameborder="0"  width="100%" height="648px"></iframe>
                </div>
            </div>
        </div>
    </div>--%>
      <%--<div class="modal fade" id="CTLOGPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
         <div class="modal-dialog w50per" style="float: left;">
            <div class="modal-content" style="width:1323px;margin-left: 17%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="RefreshParent()">&times;</button>
                </div>
                <div class="modal-body w100per">
                    <iframe src="about:blank" id="IframeCTlog" runat="server" frameborder="0"  width="100%" height="648px"></iframe>
                </div>
            </div>
        </div>
    </div>--%>
</asp:Content>
