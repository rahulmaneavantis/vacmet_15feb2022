﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddContractTemplateLog.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.Masters.AddContractTemplateLog" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <!-- font icon -->
    <link href="../../NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!--external css-->
    <link href="../../NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="../../NewCSS/contract_custom_style.css" rel="stylesheet" />
    <link href="../../NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="../../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>

    <!-- Bootstrap CSS -->
    <link href="../../NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap theme -->
    <link href="../../NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <link href="../../NewCSS/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
    <script src="../../Newjs/bootstrap-multiselect.js" type="text/javascript"></script>
    <script src="../../Newjs/bootstrap-tagsinput.js"></script>
    <link href="../../NewCSS/bootstrap-tagsinput.css" rel="stylesheet" />
    <link href="../../NewCSS/timeline.css" rel="stylesheet" />
    <!-- nice scroll -->
    <script type="text/javascript" src="../../Newjs/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery.nicescroll.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <link href="~/NewCSS/contract_custom_style.css" rel="stylesheet" />
    <script type="text/javascript">

        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }

        function CloseMe() {
            window.parent.CloseContractModal();
        }

    </script>
    <style type="text/css">
        ul.multiselect-container.dropdown-menu {
            width: 100%;
            max-height: 218px;
            overflow-x: hidden;
        }

        .input-group-btn {
            position: relative;
            white-space: nowrap;
            display: none;
        }

        .glyphicon-remove-circle {
            top: 1px;
            display: none;
            font-style: normal;
            font-weight: 400;
            line-height: 1;
            -webkit-font-smoothing: antialiased;
        }

        .multiselect-container > li {
            padding: 0;
            margin-right: 10px;
        }

        #upAddEditTemplate {
            width: 100%;
        }

        ul.multiselect-container.dropdown-menu {
            height: auto;
        }
    </style>

</head>
<body class="bgColor-white">
    <form id="form1" runat="server">
        <div>
            <asp:ScriptManager ID="smAddEditTemplate" runat="server"></asp:ScriptManager>
            <asp:UpdatePanel ID="upAddEditTemplate" runat="server" UpdateMode="Conditional">
                <ContentTemplate>

                    <div class="row form-group">
                        <div class="col-md-12 alert alert-success" runat="server" visible="false" id="divsuccessmsgaCTem">
                            <asp:Label runat="server" ID="successmsgaCTem"></asp:Label>
                        </div>
                        <asp:ValidationSummary ID="vsAddEditTemplate" runat="server"
                            Style="padding-left: 41px;" Display="none" class="alert alert-block alert-danger fade in"
                            ValidationGroup="ContractTemplateValidationGroup" />
                        <asp:CustomValidator ID="cvAddEditTemplate" runat="server" EnableClientScript="False"
                            ValidationGroup="ContractTemplateValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                    </div>
                    <div>

                        <div class="row form-group">
                            <div class="form-group required col-md-4">
                                <label for="tbxApprover" class="control-label">Approver Level</label>
                                <asp:TextBox runat="server" ID="tbxApprover" CssClass="form-control" onkeypress="return isNumber(event)" autocomplete="off" />

                                <asp:RequiredFieldValidator ID="RequiredFieldValidator61" ErrorMessage="Please enter valid Approver Level."
                                    ControlToValidate="tbxApprover" runat="server" ValidationGroup="ContractTemplateValidationGroup"
                                    Display="None" />

                            </div>
                            <div class="form-group required col-md-4">
                                <label for="tbxApprover1" style="margin-top: 33px;">&nbsp;</label>
                                <asp:Button Text="Save" runat="server" ID="btnSaveApproverCount" CssClass="btn btn-primary"
                                    ValidationGroup="ContractTemplateValidationGroup" OnClick="btnSaveApproverCount_Click" />

                                <asp:Button Text="Close" runat="server" ID="Button2"
                                    CssClass="btn btn-primary" data-dismiss="modal" OnClientClick="CloseMe();" />

                            </div>
                        </div>


                        <div id="divOuterGridTemplateSection" class="row col-md-12" runat="server">
                            <fieldset>
                                <legend>Tagged Contracts</legend>
                                <div class="clearfix"></div>

                                <div class="col-md-12 mt5 mb5" style="min-height: 50px; max-height: 300px; overflow-y: auto; overflow-x: hidden">
                                    <asp:GridView runat="server" ID="grdTemplateSection" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true"
                                        AllowSorting="true" GridLines="none" Width="100%" AllowPaging="false" AutoPostBack="true" CssClass="table"
                                        DataKeyNames="TemplateID">
                                        <Columns>


                                            <asp:TemplateField HeaderText="ContractNo" ItemStyle-Width="25%" HeaderStyle-CssClass="text-left"
                                                ItemStyle-CssClass="text-left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblContractID" runat="server" Text='<%# Eval("ContractID") %>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lblTemplateID" runat="server" Text='<%# Eval("TemplateID") %>' Visible="false"></asp:Label>

                                                    <asp:Label ID="lblContractNo" runat="server" Text='<%# Eval("ContractNo") %>'></asp:Label>

                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="ContractTitle" HeaderText="Contract Title" ItemStyle-Width="25%" />

                                            <asp:TemplateField HeaderText="Status" ItemStyle-Width="15%" HeaderStyle-CssClass="text-left" ItemStyle-CssClass="text-left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblContractStatusID" runat="server" Text='<%# Eval("ContractStatusID") %>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lblStatus" runat="server" Text='<%# Eval("Status") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Approver Level" ItemStyle-Width="15%" HeaderStyle-CssClass="text-left" ItemStyle-CssClass="text-left">
                                                <ItemTemplate>

                                                    <asp:Label ID="lblApproverCount" runat="server" Text='<%# Eval("ApproverCount") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <RowStyle CssClass="clsROWgrid" />
                                        <HeaderStyle CssClass="clsheadergrid" />
                                        <PagerTemplate>
                                            <table style="display: none">
                                                <tr>
                                                    <td>
                                                        <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                    </td>
                                                </tr>
                                            </table>
                                        </PagerTemplate>
                                        <EmptyDataTemplate>
                                            No Record Found
                                        </EmptyDataTemplate>
                                    </asp:GridView>

                                    <asp:LinkButton ID="lnkBtnRebind_Sections" OnClick="lnkBtnRebind_Sections_Click"
                                        Style="float: right; display: none;" Width="100%" runat="server"> 
                                    </asp:LinkButton>
                                </div>
                            </fieldset>


                        </div>



                        <div id="div1" class="row col-md-12" runat="server">
                            <fieldset>
                                <legend>Audit Log</legend>
                                <div class="clearfix"></div>

                                <div class="col-md-12 mt5 mb5" style="min-height: 50px; max-height: 300px; overflow-y: auto; overflow-x: hidden">
                                    <asp:GridView runat="server" ID="grdLog" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true"
                                        AllowSorting="true" GridLines="none" Width="100%" AllowPaging="false" AutoPostBack="true" CssClass="table"
                                        DataKeyNames="TemplateID">
                                        <Columns>


                                            <asp:TemplateField HeaderText="TemplateName" ItemStyle-Width="25%" HeaderStyle-CssClass="text-left"
                                                ItemStyle-CssClass="text-left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblTemplateID" runat="server" Text='<%# Eval("TemplateID") %>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lblTemplateName" runat="server" Text='<%# Eval("TemplateName") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="CreatedByText" HeaderText="Created By" ItemStyle-Width="25%" />
                                            <asp:TemplateField HeaderText="Created On">
                                                <ItemTemplate>
                                                    <%# Eval("CreatedOn") != DBNull.Value ? Convert.ToDateTime(Eval("CreatedOn")).ToString("dd-MMM-yyyy hh:mm:ss:tt") : ""%>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Approver Level" ItemStyle-Width="15%" HeaderStyle-CssClass="text-left" ItemStyle-CssClass="text-left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblApproverCount" runat="server" Text='<%# Eval("ApproverCount") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <RowStyle CssClass="clsROWgrid" />
                                        <HeaderStyle CssClass="clsheadergrid" />
                                        <PagerTemplate>
                                            <table style="display: none">
                                                <tr>
                                                    <td>
                                                        <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                    </td>
                                                </tr>
                                            </table>
                                        </PagerTemplate>
                                        <EmptyDataTemplate>
                                            No Record Found
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </ContentTemplate>
                <%--   <Triggers>
                    <asp:PostBackTrigger ControlID="btnExportWord1" />
                    <asp:PostBackTrigger ControlID="btnExportWord2" />
                </Triggers>--%>
            </asp:UpdatePanel>
        </div>
    </form>
</body>
</html>
