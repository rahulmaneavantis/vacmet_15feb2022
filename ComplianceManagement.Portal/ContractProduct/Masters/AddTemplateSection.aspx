﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddTemplateSection.aspx.cs" ValidateRequest="false" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.Masters.AddTemplateSection" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <!-- Bootstrap CSS -->

    <link href="../../NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!--external css-->
    <link href="../../NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="../../NewCSS/contract_custom_style.css" rel="stylesheet" />
    <link href="../../NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="../../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>

    <!-- Bootstrap CSS -->
    <link href="../../NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap theme -->
    <link href="../../NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <link href="../../NewCSS/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
    <script src="../../Newjs/bootstrap-multiselect.js" type="text/javascript"></script>
    <script src="../../Newjs/bootstrap-tagsinput.js"></script>
    <link href="../../NewCSS/bootstrap-tagsinput.css" rel="stylesheet" />
    <link href="../../NewCSS/timeline.css" rel="stylesheet" />
    <!-- nice scroll -->
    <script type="text/javascript" src="../../Newjs/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery.nicescroll.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <link href="~/NewCSS/contract_custom_style.css" rel="stylesheet" />



    <!-- font icon -->
    <link href="../../NewCSS/elegant-icons-style.css" rel="stylesheet" />
    <link href="../../NewCSS/font-awesome.css" rel="stylesheet" />
    <!-- Custom styles -->
    <link href="../../NewCSS/style-responsive.css" rel="stylesheet" />


    <%--<script type="text/javascript" src="//cdn.tinymce.com/4/tinymce.min.js"></script>--%>
      <link href="../../Style/css/StyleSheetTextEditor.css" rel="stylesheet" />
    <script type="text/javascript">

        var editor, range;
        function OnClientLoad(sender, args) {
            editor = sender;
        }

        function OnClientSelectionChange(sender, args) {
            range = editor.getSelection().getRange(true);
        }

        function Button_OnClick()
        {
            debugger;
            alert(1);
            //if (range) {
            //    editor.getSelection().selectRange(range);
            //}
            //var e = document.getElementById("ddlContractTemplate");
            //var selectedLocation = e.options[e.selectedIndex].value;
            //editor.pasteHtml(selectedLocation);
        }

        bind_tinyMCE();

        function bind_tinyMCE() {

            tinymce.remove();
            tinymce.init({
                mode: "specific_textareas",
                editor_selector: "myTextEditor",
                width: '100%',
                height: 400,
                selector: 'textarea',  // change this value according to your HTML
                plugins: 'table',
                table_toolbar: 'tableprops tabledelete | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol',
                toolbar: ' undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | fontselect | fontsizeselect',
                font_formats: 'Andale Mono=andale mono,times;Arial=arial,helvetica,sans-serif;Arial Black=arial black,avant garde;Book Antiqua=book antiqua,palatino;Comic Sans MS=comic sans ms,sans-serif;Courier New=courier new,courier;Georgia=georgia,palatino;Helvetica=helvetica;Impact=impact,chicago;Symbol=symbol;Tahoma=tahoma,arial,helvetica,sans-serif;Terminal=terminal,monaco;Times New Roman=times new roman,times;Trebuchet MS=trebuchet ms,geneva;Verdana=verdana,geneva;Webdings=webdings;Wingdings=wingdings,zapf dingbats',
                fontsize_formats: '8pt 10pt 11pt 12pt 13pt 14pt 15pt 18pt 24pt 36pt',
                statusbar: false,
                setup: function (editor) {
                    editor.on('change', function () {
                        tinymce.triggerSave();
                    });
                }
            });

            BindControls();
        }

        function CloseMe() {
            window.parent.CloseSectionModal();
        }

        function RefreshParent() {
            window.parent.location.href = window.parent.location.href;
        }

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 46 && charCode > 31
                && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }

        function scrollUp() {

            $('html, body').animate({ scrollTop: '0px' }, 800);
        }
    </script>


</head>
<body class="bgColor-white">
    <form id="form1" runat="server">
        <div>
          <%--  <asp:ScriptManager ID="smTemplateSection" runat="server"></asp:ScriptManager>--%>
            <asp:UpdatePanel ID="upTemplateSection" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div>
                        <div class="row form-group">
                            <div class="col-md-12 alert alert-block alert-success fade in" runat="server" visible="false" id="divsuccessmsgaCTemSec">
                                <asp:Label runat="server" ID="successmsgaCTemSec"></asp:Label>
                            </div>
                            <asp:ValidationSummary ID="vsTemplateSection" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                ValidationGroup="TemplateSectionValidationGroup" />
                            <asp:CustomValidator ID="cvTemplateSection" runat="server" EnableClientScript="False"
                                ValidationGroup="TemplateSectionValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                        </div>

                        <div class="row col-md-12">
                            <div class="form-group required col-md-4">
                                <label for="tbxTemplateName" class="control-label">Section Header</label>
                                <asp:TextBox runat="server" ID="tbxHeader" CssClass="form-control" autocomplete="off" />
                                <asp:RequiredFieldValidator ID="rfvHeader" ErrorMessage="Please Enter Header." ControlToValidate="tbxHeader"
                                    runat="server" ValidationGroup="TemplateSectionValidationGroup" Display="None" />
                            </div>
                          <%--  required--%>
                            <div class="form-group col-md-4">
                                <label for="ddlDeptPage" class="control-label">Department</label>
                               

                                <asp:DropDownListChosen runat="server" ID="ddlDeptPage" AllowSingleDeselect="false"
                                 DisableSearchThreshold="3" style="padding: 3px 0 0 8px;height: 33px;"
                                    DataPlaceHolder="Select Department" class="form-control" Width="95%" Height="34px"/>
               
                            </div>
                            <div class="form-group required col-md-4">
                                <label for="ddlVisibilitySectionHeader" class="control-label">Header Visibility</label>
                                <asp:DropDownList runat="server" ID="ddlVisibilitySectionHeader" class="form-control"
                                    Style="padding: 3px 0 0 8px; height: 33px;" Width="95%" Height="34px">
                                    <asp:ListItem Text="Show" Value="1" />
                                    <asp:ListItem Text="Hide" Value="0" Selected="True" />
                                </asp:DropDownList>
                            </div>
                        </div>

                         <div class="row col-md-12">
                              <div class="form-group required col-md-12">
                                <label for="tbxVersion" class="control-label">Section Content</label>
                        <telerik:RadScriptManager runat="server" ID="RadScriptManager1" />
                        <telerik:RadSkinManager ID="RadSkinManager1" runat="server" ShowChooser="true" Visible="false" />
                        <div class="demo-containers" style="padding-top: 6px;">
                            <div class="demo-container">
                                <telerik:RadEditor RenderMode="Lightweight" ID="theEditor" StripFormattingOptions="MSWord" EnableTrackChanges="true" runat="server"
                                    Width="1045px" OnClientLoad="OnClientLoad" OnClientSelectionChange="OnClientSelectionChange"
                                    Height="420px" ToolsFile="../../ToolsFile.xml"
                                    ContentFilters="DefaultFilters, PdfExportFilter"
                                    SkinID="WordLikeExperience" EditModes="Design,Preview">
                                    <ExportSettings>
                                        <Docx DefaultFontName="Arial" DefaultFontSizeInPoints="12" HeaderFontSizeInPoints="8"
                                            PageHeader="Some header text for DOCX documents" />
                                        <Rtf DefaultFontName="Times New Roman" DefaultFontSizeInPoints="13"
                                            HeaderFontSizeInPoints="9" PageHeader="Some header text for RTF documents" />
                                    </ExportSettings>
                                    <Tools>
                                        <telerik:EditorToolGroup>
                                            <telerik:EditorTool Name="AcceptAllTrackChanges" />
                                            <telerik:EditorTool Name="RejectAllTrackChanges" />
                                            <telerik:EditorTool Name="RealFontSize" shortcut="CTRL+SHIFT+P / CMD+SHIFT+P" />
                                            <telerik:EditorTool name="FontName" />
                                            <telerik:EditorTool name="FindAndReplace" shortcut="CTRL+F / CMD+F" />
                                            <telerik:EditorTool name="Print" shortcut="CTRL+P / CMD+P"/>
                                            <telerik:EditorTool name="AjaxSpellCheck"/>
                                            <telerik:EditorTool name="Cut" shortcut="CTRL+X / CMD+X"/>
                                            <telerik:EditorTool name="Copy" shortcut="CTRL+C / CMD+X"/>
                                           <telerik:EditorTool name="Paste" shortcut="CTRL+V / CMD+V"/>
                                            <telerik:EditorTool name="InsertDate" />
                                            <telerik:EditorTool name="InsertTime" />
                                            <telerik:EditorTool name="Help" shortcut="F1"/>
                                            <telerik:EditorTool name="InsertSymbol" />
                                        </telerik:EditorToolGroup>
                                    </Tools>
                                    <TrackChangesSettings Author="RadEditorUser" CanAcceptTrackChanges="true"
                                        UserCssId="reU0"></TrackChangesSettings>
                                    <Content>                  
                                    </Content>
                                </telerik:RadEditor>
                            </div>
                        </div>
                        <telerik:RadAjaxManager runat="server" ID="RadAjaxManager1">
                            <AjaxSettings>
                                <telerik:AjaxSetting AjaxControlID="ConfiguratorPanel1">
                                    <UpdatedControls>
                                        <telerik:AjaxUpdatedControl ControlID="theEditor" LoadingPanelID="RadAjaxLoadingPanel1" />
                                        <telerik:AjaxUpdatedControl ControlID="ConfiguratorPanel1" />
                                    </UpdatedControls>
                                </telerik:AjaxSetting>
                            </AjaxSettings>
                        </telerik:RadAjaxManager>
                        <telerik:RadAjaxLoadingPanel runat="server" ID="RadAjaxLoadingPanel1">
                        </telerik:RadAjaxLoadingPanel>
                        <div runat="server" id="divHtml"></div>
                    </div>
                       <%-- <div class="row col-md-12">
                            <div class="form-group required col-md-12">
                                <label for="tbxVersion" class="control-label">Section Content</label>
                                <asp:TextBox runat="server" ID="tbxContent" CssClass="form-control myTextEditor" TextMode="MultiLine" Rows="3" Width="100%" />
                                <asp:RequiredFieldValidator ID="rfvContent" ErrorMessage="Please Enter Content." ControlToValidate="tbxContent"
                                    runat="server" ValidationGroup="TemplateSectionValidationGroup" Display="None" />
                            </div>
                        </div>--%>

                        <div class="row form-group text-center">
                            <div class="col-md-12">
                                <asp:Button Text="Save" runat="server" ID="btnSave" CssClass="btn btn-primary"
                                    ValidationGroup="TemplateSectionValidationGroup" OnClick="btnSave_Click"
                                    />
                                <asp:Button Text="Close" runat="server" ID="btnCancel"
                                    CssClass="btn btn-primary" data-dismiss="modal" OnClientClick="CloseMe();" />
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <p style="color: red;"><strong>Note:</strong> (*) fields are Compulsory</p>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnSave" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </form>
</body>
</html>
