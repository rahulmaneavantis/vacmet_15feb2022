﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Globalization;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using System.Web.Services;
using com.VirtuosoITech.ComplianceManagement.Business.DataContract;

namespace com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.Masters
{
    public partial class AddVendor : System.Web.UI.Page
    {
        private long CustomerID = AuthenticationHelper.CustomerID;
        public static string ContvendorID;
        public static string ContCityID;
        protected void Page_Load(object sender, EventArgs e)
        {
            divsuccessmsgaCvndr.Visible = false;
            if (!IsPostBack)
            {
                BindCountry();

                if (!string.IsNullOrEmpty(Request.QueryString["VendorId"]))
                {
                    long VendorID = Convert.ToInt64(Request.QueryString["VendorId"]);

                    Cont_tbl_VendorMaster VendorData = VendorDetails.GetVendorDetailsByID(VendorID);

                    if (VendorData != null)
                    {
                        tbxName.Text = VendorData.VendorName;
                        ddlCountry.SelectedValue = (VendorData.CountryID).ToString();
                        ddlCountry_SelectedIndexChanged(sender, e);

                        ddlState.SelectedValue = (VendorData.StateID).ToString();
                        ddlState_SelectedIndexChanged(sender, e);

                        ddlCity.SelectedValue = (VendorData.CityID).ToString();


                        if (VendorData.isMSME == true)
                        {
                            chkMSME.Checked = true;

                        }
                        else
                        {
                            chkMSME.Checked = false;

                        }

                        tbxEmail.Text = VendorData.Email;
                        tbxContactNo.Text = VendorData.ContactNumber;
                        tbxAddress.Text = VendorData.Address;
                        tbxContactPerson.Text = VendorData.ContactPerson;
                        tbxVAT.Text = VendorData.VAT;
                        tbxTIN.Text = VendorData.TIN;
                        tbxGSTIN.Text = VendorData.GSTIN;
                        tbxPAN.Text = VendorData.PAN;

                        if (VendorData.Type > 0)
                        {
                            rbPartyType.SelectedValue = null;
                            if (VendorData.Type == 1)
                            {
                                rbPartyType.SelectedValue = "1";
                                rbPartyType_SelectedIndexChanged(sender, e);
                            }
                            else if (VendorData.Type == 2)
                            {
                                rbPartyType.SelectedValue = "2";
                                rbPartyType_SelectedIndexChanged(sender, e);
                                ddlTypesofCorporate.SelectedValue = (VendorData.TOC).ToString();
                            }
                        }

                        ViewState["Mode"] = 1;
                        btnSave.Text = "Update";
                        ViewState["VendorID"] = VendorID;
                    }
                }
                else
                {
                    chkMSME.Checked = false;
                    btnSave.Text = "Save";
                    ViewState["Mode"] = 0;
                    rbPartyType_SelectedIndexChanged(sender, e);
                }
            }
        }

        public void BindCountry()
        {
            var CountryList = CityStateCountryManagement.GetAllContryData();

            ddlCountry.DataTextField = "Name";
            ddlCountry.DataValueField = "ID";

            ddlCountry.DataSource = CountryList;
            ddlCountry.DataBind();
            ddlCountry.Items.Insert(0, new ListItem("Select Country", "-1"));
        }

        public void BindState(int CountryID)
        {
            var StateListcountrywise = CityStateCountryManagement.GetAllStateCountryWise(CountryID);

            ddlState.DataTextField = "Name";
            ddlState.DataValueField = "ID";

            ddlState.DataSource = StateListcountrywise;
            ddlState.DataBind();

            ddlState.Items.Insert(0, new ListItem("Select State", "-1"));
        }

        public void BindCity(int a)
        {
            if (ddlState.SelectedValue != "-1" && ddlState.SelectedValue != null)
            {
                var CountryList = StateCityManagement.GetAllCitysByState(Convert.ToInt32(ddlState.SelectedValue));

                ddlCity.DataTextField = "Name";
                ddlCity.DataValueField = "ID";

                ddlCity.DataSource = CountryList;
                ddlCity.DataBind();

                ddlCity.Items.Add(new System.Web.UI.WebControls.ListItem("Add New", "0"));
                ddlCity.Items.Insert(0, new ListItem("Select City", "-1"));
            }
        }

        protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlCountry.SelectedValue))
            {
                if (ddlCountry.SelectedValue != "-1")
                {
                    BindState(Convert.ToInt32(ddlCountry.SelectedValue));
                }
            }
        }

        protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(ddlState.SelectedValue))
            {
                if (ddlState.SelectedValue != "-1")
                {
                    BindCity(Convert.ToInt32(ddlState.SelectedValue));
                }
            }
        }
        protected void lnkBtnCity_Click(object sender, EventArgs e)
        {
            try
            {
                if (tbxCity.Text != "")
                {
                    bool success = true;
                    int stateid = -1;
                    if (!string.IsNullOrEmpty(ddlState.SelectedValue))
                    {
                        stateid = Convert.ToInt32(ddlState.SelectedValue);
                    }
                    int newCityID = 0;
                    City newCity = new City()
                    {
                        Name = tbxCity.Text.Trim(),
                        StateId = stateid,
                        IsDeleted = false,
                    };

                    /*  if (!StateCityManagement.ExitsCity(newCity))
                      {
                          CityValidator.IsValid = false;

                          CityValidator.ErrorMessage = "City with same name already exists.";
                          success = false;
                          return;
                      }*/

                    if (!StateCityManagement.ExitsCity(newCity))
                    {
                        divsuccessmsgaCcity.Visible = false;
                        CityValidator.IsValid = false;
                        CityValidator.ErrorMessage = "City with same name already exists.";
                        success = false;
                        return;
                    }

                    if (success)
                    {
                       // BindCity(Convert.ToInt32(ddlState.SelectedValue));
                        newCityID = StateCityManagement.CreateCity(newCity);
                        ContCityID = newCityID.ToString();
                        if (CityValidator.IsValid)
                        {
                            CityValidator.IsValid = false;
                            CityValidator.ErrorMessage = "City Save Successfully.";
                           
                           ScriptManager.RegisterStartupScript(this, this.GetType(), "rebindCity", "restoreSelectedCity();", true);
                            // ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "script", "window.setTimeout(function() { document.getElementById('" + divsuccessmsgaCcity.ClientID + "').style.display = 'none' },2000);", true);
                            BindCity(Convert.ToInt32(ddlState.SelectedValue));
                            upDepartment.Update();
                        }
                    }
                }

                tbxCity.Text = " ";
            }

            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CityValidator.IsValid = false;
                CityValidator.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                bool dataValidationSuccess = false;
                bool saveSuccess = false;

                #region Validation
                if (!string.IsNullOrEmpty(tbxName.Text))
                {
                    dataValidationSuccess = true;
                }
                else
                {
                    divsuccessmsgaCvndr.Visible = false;
                    cvAddVendor.IsValid = false;
                    cvAddVendor.ErrorMessage = "Please Enter Vendor Name";
                }
                #endregion

                if (dataValidationSuccess)
                {
                    Cont_tbl_VendorMaster objVendor = new Cont_tbl_VendorMaster()
                    {
                        CustomerID = Convert.ToInt32(CustomerID),
                        Type = Convert.ToInt32(rbPartyType.SelectedValue),
                        VendorName = tbxName.Text,
                        ContactPerson = tbxContactPerson.Text,
                        Email = tbxEmail.Text,
                        ContactNumber = tbxContactNo.Text,
                        Address = tbxAddress.Text,
                        VAT = tbxVAT.Text,
                        TIN = tbxTIN.Text,
                        GSTIN = tbxGSTIN.Text,
                        PAN = tbxPAN.Text,
                        IsDeleted = false,
                        CreatedOn = DateTime.Now,
                        CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                        UpdatedOn = DateTime.Now,
                        UpdatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                    };

                    if (chkMSME.Checked == true)
                    {                       
                        objVendor.isMSME = true;
                    }
                    else
                    {
                        objVendor.isMSME = false;
                    }
                    if (ddlTypesofCorporate.SelectedValue != "")
                        objVendor.TOC = Convert.ToInt32(ddlTypesofCorporate.SelectedValue);

                    if (ddlCountry.SelectedValue != "")
                        objVendor.CountryID = Convert.ToInt32(ddlCountry.SelectedValue);

                    if (ddlState.SelectedValue != "")
                        objVendor.StateID = Convert.ToInt32(ddlState.SelectedValue);

                    if (ddlCity.SelectedValue != "")
                        objVendor.CityID = Convert.ToInt32(ddlCity.SelectedValue);

                    if ((int)ViewState["Mode"] == 0)
                    {
                        if (VendorDetails.ExistsVendorName(objVendor, 0))
                        {
                            divsuccessmsgaCvndr.Visible = false;
                            cvAddVendor.IsValid = false;
                            cvAddVendor.ErrorMessage = "Vendor with same name already exists.";
                        }
                        else
                        {
                            long newVendorID = 0;
                            saveSuccess = VendorDetails.CreateVendorData(objVendor);
                            newVendorID = objVendor.ID;
                            if (saveSuccess)
                            {
                                ContvendorID = newVendorID.ToString();                              
                                if (cvAddVendor.IsValid)
                                {
                                    divsuccessmsgaCvndr.Visible = true;
                                    successmsgaCvndr.Text = "Vendor Detail Saved Successfully.";
                                    ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "script", "window.setTimeout(function() { document.getElementById('" + divsuccessmsgaCvndr.ClientID + "').style.display = 'none' },2000);", true);
                                }
                                rbPartyType.SelectedValue = "1";
                                tbxName.Text = string.Empty;
                                tbxContactPerson.Text = string.Empty;
                                tbxEmail.Text = string.Empty;
                                tbxContactNo.Text = string.Empty;
                                ddlState.SelectedValue = "-1";
                                ddlCity.SelectedValue = "-1";
                                ddlCountry.SelectedValue = "-1";
                                tbxAddress.Text = string.Empty;
                                tbxVAT.Text = string.Empty;
                                tbxTIN.Text = string.Empty;
                                tbxGSTIN.Text = string.Empty;
                            }
                            else
                            {
                                divsuccessmsgaCvndr.Visible = false;
                                cvAddVendor.IsValid = false;
                                cvAddVendor.ErrorMessage = "Something went wrong, Please try again";
                            }
                        }
                    }
                    else if ((int)ViewState["Mode"] == 1)
                    {
                        objVendor.ID = Convert.ToInt32(ViewState["VendorID"]);

                        if (!VendorDetails.ExistsVendorName(objVendor, Convert.ToInt64(objVendor.ID)))
                        {
                            VendorDetails.UpdateVendorData(objVendor);
                            if (cvAddVendor.IsValid)
                            {
                                divsuccessmsgaCvndr.Visible = true;
                                successmsgaCvndr.Text = "Vendor Detail Updated Successfully.";
                                ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "script", "window.setTimeout(function() { document.getElementById('" + divsuccessmsgaCvndr.ClientID + "').style.display = 'none' },2000);", true);
                            }
                        }
                        else
                        {
                            divsuccessmsgaCvndr.Visible = false;
                            cvAddVendor.IsValid = false;
                            cvAddVendor.ErrorMessage = "Vendor with same name already exists.";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                divsuccessmsgaCvndr.Visible = false;
                cvAddVendor.IsValid = false;
                cvAddVendor.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        [WebMethod]
        public static string getvendorID()
        {

            try
            {
                string CvendorID = string.Empty;
                if (ContvendorID != null)
                {
                     CvendorID = ContvendorID;
                }
                return CvendorID;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "null";
            }
        }


        protected void btnRebindddlCity_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(ddlState.SelectedValue))
            {
                if (ddlState.SelectedValue != "-1")
                {
                    BindCity(Convert.ToInt32(ddlState.SelectedValue));
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "rebindCity", "restoreSelectedCity();", true);
                    upDepartment.Update();

                }
            }
        }

        protected void rbPartyType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(rbPartyType.SelectedValue))
            {    
                if (rbPartyType.SelectedValue == "1")
                {
                    typeofcor.Visible = false;
                    ddlTypesofCorporate.SelectedValue = "-1";
                }                    
                else if (rbPartyType.SelectedValue == "2")
                {
                    typeofcor.Visible = true;
                    ddlTypesofCorporate.SelectedValue = "-1";
                }               
            }
        }
    }
}