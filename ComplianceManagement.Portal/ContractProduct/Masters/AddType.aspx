﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddType.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Contract.Masters.AddType" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <!-- Bootstrap CSS -->
    <link href="/NewCSS/bootstrap.min.css" rel="stylesheet" />
    <!-- bootstrap theme -->
    <link href="/NewCSS/bootstrap-theme.css" rel="stylesheet" />
    <!--external css-->
    <!-- font icon -->
    <link href="/NewCSS/elegant-icons-style.css" rel="stylesheet" />
    <link href="/NewCSS/font-awesome.css" rel="stylesheet" />
    <!-- Custom styles -->
    <link href="/NewCSS/style.css" rel="stylesheet" />
    <link href="/NewCSS/style-responsive.css" rel="stylesheet" />
    <script type="text/javascript" src="/Newjs/jquery.js"></script>
    <script type="text/javascript" src="/Newjs/jquery-ui-1.9.2.custom.min.js"></script>
     <link href="~/NewCSS/contract_custom_style.css" rel="stylesheet" />
    <script type="text/javascript">
        function CloseMe() {
            //alert("called");
            window.parent.CloseContractTypePopUp();
        }

        function RefreshParent() {
            window.parent.location.href = window.parent.location.href;
        }
    </script>
</head>
<body style="background-color: white">
    <form id="form1" runat="server">
        <div>
            <asp:ScriptManager ID="smAddEditContractType" runat="server"></asp:ScriptManager>
            <asp:UpdatePanel ID="upAddEditContractType" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div>
                        <div class="row form-group">
                               <div class="col-md-12 alert alert-block alert-success fade in" runat="server" visible="false" id="divsuccessmsgaCType">
                            <asp:Label runat="server" ID="successmsgaCType" ></asp:Label>
                        </div>
                            <asp:ValidationSummary ID="vsAddEditContractType" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                 ValidationGroup="ContractTypeValidationGroup" />
                            <asp:CustomValidator ID="cvAddEditContractType" runat="server" EnableClientScript="False"
                                ValidationGroup="ContractTypeValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />                           
                        </div>

                        <div class="row form-group">
                            <div class="form-group required col-md-4">
                                <label for="tbxContType" class="control-label">Contract Type</label>
                                 <asp:TextBox runat="server" ID="tbxContType" CssClass="form-control" autocomplete="off" />
                                <asp:RequiredFieldValidator ID="rfvTypeName" ErrorMessage="Please Enter Contract Type Name" ControlToValidate="tbxContType"
                                    runat="server" Display="None" />
                            </div>
                        </div>

                        <div class="row form-group text-center">
                            <div class="col-md-12">
                                <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="btn btn-primary"
                                    ValidationGroup="ContractTypeValidationGroup" />
                                <asp:Button Text="Close" runat="server" ID="btnCancel"
                                    CssClass="btn btn-primary" data-dismiss="modal" OnClientClick="CloseMe();" />
                                <%--RefreshParent();--%>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <p style="color: red;"><strong>Note:</strong> (*) fields are Compulsory</p>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </form>
</body>
</html>