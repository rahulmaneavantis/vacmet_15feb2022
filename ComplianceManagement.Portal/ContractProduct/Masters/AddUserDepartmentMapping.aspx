﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddUserDepartmentMapping.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.Masters.AddUserDepartmentMapping" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <!-- Bootstrap CSS -->
    <!-- font icon -->
    <link href="../../NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!--external css-->
    <link href="../../NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="../../NewCSS/contract_custom_style.css" rel="stylesheet" />
    <link href="../../NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    
    <script type="text/javascript" src="../../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>
    
    <link href="../../NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap theme -->
    <link href="../../NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <link href="../../NewCSS/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
    <script src="../../Newjs/bootstrap-multiselect.js" type="text/javascript"></script>
    <script src="../../Newjs/bootstrap-tagsinput.js"></script>
    <link href="../../NewCSS/bootstrap-tagsinput.css" rel="stylesheet" />
    <link href="../../NewCSS/timeline.css" rel="stylesheet" />
    <!-- nice scroll -->
    <script type="text/javascript" src="../../Newjs/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery.nicescroll.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
      <link href="~/NewCSS/contract_custom_style.css" rel="stylesheet" />
    <script type="text/javascript">

 function checkUncheckRowField() {

            var selectedRowCount = 0;
            //Get the reference of GridView
            var grid = document.getElementById("<%=grddept.ClientID %>");

            //Get all input elements in Gridview
            var inputList = grid.getElementsByTagName("input");

            //The First element is the Header Checkbox
            var headerCheckBox = inputList[0];
            var checked = true;
            for (var i = 0; i < inputList.length; i++) {
                //Based on all or none checkboxes are checked check/uncheck Header Checkbox                
                if (inputList[i].type == "checkbox" && inputList[i] != headerCheckBox) {
                    if (!inputList[i].checked) {
                        checked = false;
                    }
                    if (inputList[i].checked) {
                        selectedRowCount++;
                    }
                }
            }

            headerCheckBox.checked = checked;

        }

  function checkAllField(chkHeader) {
            var selectedRowCount = 0;
            var grid = document.getElementById("<%=grddept.ClientID %>");
            if (grid != null) {
                if (chkHeader != null) {
                    //Get all input elements in Gridview
                    var inputList = grid.getElementsByTagName("input");

                    for (var i = 1; i < inputList.length; i++) {
                        if (inputList[i].type == "checkbox") {
                            if (chkHeader.checked) {
                                inputList[i].checked = true;
                                selectedRowCount++;
                            }
                            else if (!chkHeader.checked)
                                inputList[i].checked = false;
                        }
                    }
                }
            }
        }

        function CloseMe() {
            window.parent.CloseDocTypePopup();
        }
    </script>
    <style>
        .MainWrapper {
            /* width: 21.5%; */
            display: block;
            float: left;
            font-size: 13px;
            color: #666;
            margin-left: -2%;
            font-weight: 500;
        }
    </style>
</head>
<body style="background-color: white">
    <form id="form1" runat="server">
        <div>
            <asp:ScriptManager ID="ContractAddDocType" runat="server"></asp:ScriptManager>
            <asp:UpdatePanel ID="updocumenttype" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div>
                        <div class="row form-group">
                             <div class="col-md-12 alert alert-block alert-danger fade in" runat="server" visible="false" id="divsuccessmsgaCDoctype">
                            <asp:Label runat="server" ID="successmsgaCDoctype" ></asp:Label>
                        </div>
                            <asp:ValidationSummary ID="vsAddDocType" runat="server" Display="none" class="alert alert-block alert-danger fade in" ValidationGroup="PromotorValidationGroup" />
                            <asp:CustomValidator ID="cvDuplicateLocation" runat="server" EnableClientScript="False"
                                ValidationGroup="PromotorValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                        </div>
                        <div class="col-md-12" style="margin-top: 8px;margin-bottom:48px">
                         <div class="col-md-2">
                                      <label class="MainWrapper">User<span style="color: red;">*</span></label>
                               </div> 

                              <div  class="col-md-2" style="width: 46%;float:left">
                                 <asp:DropDownListChosen runat="server" ID="ddlUser" AutoPostBack="true"
                                        class="form-control" Width="100%" DataPlaceHolder="Select User"
                                        OnSelectedIndexChanged="ddlUser_SelectedIndexChanged">
                                    </asp:DropDownListChosen>
                                     <asp:CompareValidator ID="CompareValidator1" ErrorMessage="Please Select User Role." ControlToValidate="ddlUser"
                                                runat="server" ValueToCompare="" Operator="NotEqual" ValidationGroup="PromotorValidationGroup"
                                                Display="None" />
                              </div>
                                    </div>

                        <div class="col-md-12" style="height: 400px; overflow-y: scroll">
                            <asp:GridView runat="server" ID="grddept" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true"
                                AllowSorting="true" GridLines="none" Width="100%" AllowPaging="false" AutoPostBack="true" CssClass="table" DataKeyNames="ID">
                                <Columns>
                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="5%">
                                            <HeaderTemplate>
                                             
                                                <asp:CheckBox ID="chkHeader" runat="server" onclick="javascript:checkAllField(this)" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                            <asp:Label ID="lblDeptID" runat="server" Text='<%# Eval("ID") %>' Visible="false"></asp:Label>
                                                <asp:CheckBox ID="chkRow" runat="server" onclick="javascript:checkUncheckRowField(this)" />
                                            </ItemTemplate>
                                        </asp:TemplateField>                         
                                    <asp:BoundField DataField="Name" HeaderText="Department" ItemStyle-Width="23%" />
                                         <asp:TemplateField HeaderText="Is Allow for Contract Add" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="15%">
                                            <%--<HeaderTemplate>
                                                <asp:CheckBox ID="chkHeader" runat="server" onclick="javascript:checkAllField(this)" />
                                            </HeaderTemplate>--%>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkRowisAllow" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>  
                                </Columns>
                                <RowStyle CssClass="clsROWgrid" />
                                <HeaderStyle CssClass="clsheadergrid" />
                                <PagerTemplate>
                                    <table style="display: none">
                                        <tr>
                                            <td>
                                                <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                            </td>
                                        </tr>
                                    </table>
                                </PagerTemplate>
                                <EmptyDataTemplate>
                                    No Record Found
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>

                        <div class="row form-group text-center">
                            <div class="col-md-12">
                                <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="btn btn-primary"
                                ValidationGroup="PromotorValidationGroup" />
                                <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="btn btn-primary" data-dismiss="modal" 
                                    OnClientClick="CloseMe();" />
                            </div>
                        </div>                       
                       <div class="row">
                            <div class="col-md-12">
                                <p style="color: red;"><strong>Note:</strong> (*) fields are Compulsory</p>
                            </div>
                        </div>                     
                    </div>
                </ContentTemplate>                
            </asp:UpdatePanel>
        </div>
    </form>
</body>
</html>
