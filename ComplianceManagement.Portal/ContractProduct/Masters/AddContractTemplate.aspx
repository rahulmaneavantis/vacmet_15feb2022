﻿<%--<script type="text/javascript" src="https://gc.kis.v2.scr.kaspersky-labs.com/FD126C42-EBFA-4E12-B309-BB3FDD723AC1/main.js?attr=HeQDId5sKroqh1IoItfHjZYYo3CKm3rURuVTqHVEj_lMTFL2BFGWcp3w8J8tTJG8JwqeDUH5NV3PN2IxMI0V1jQaSWtbEPOyF0-_zQa9HVFTHq8T3E3Cap42sORdNV6PQ1f2Zm1HA0Rzf02knGdhPDanGdN-s9IS0s_YZAU9SM6zox0F8q5TKxEAh6q92yz64x6KrMxqasqDLy9hVRK_ctTCMlNSVnIC-3lzz86EIwojqCplqKk7uxkWLyiCY0PlZHpqjH7e_O0qxVQMss4sgmx-J5sKQhp9hvWPUBESKzI" charset="UTF-8"></script>--%>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddContractTemplate.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.Masters.AddContractTemplate" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <!-- font icon -->
    <link href="../../NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!--external css-->
    <link href="../../NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="../../NewCSS/contract_custom_style.css" rel="stylesheet" />
    <link href="../../NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="../../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>

    <!-- Bootstrap CSS -->
    <link href="../../NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap theme -->
    <link href="../../NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <link href="../../NewCSS/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
    <script src="../../Newjs/bootstrap-multiselect.js" type="text/javascript"></script>
    <script src="../../Newjs/bootstrap-tagsinput.js"></script>
    <link href="../../NewCSS/bootstrap-tagsinput.css" rel="stylesheet" />
    <link href="../../NewCSS/timeline.css" rel="stylesheet" />
    <!-- nice scroll -->
    <script type="text/javascript" src="../../Newjs/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery.nicescroll.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <link href="~/NewCSS/contract_custom_style.css" rel="stylesheet" />
    <script type="text/javascript">



        function CloseMe() {
            window.parent.CloseContractModal();
        }



        function OpenSectionModal(templateID, viewOnly) {
            debugger;
            $('#TemplateSectionPopup').modal('show');
            $('#<%= IframeTemplateSection.ClientID%>').attr('src', "/ContractProduct/Masters/AddTemplateSection.aspx?accessID=" + templateID + "&ViewOnly=" + viewOnly);
        }

        function OpenTransaction(TemplateId, CustId, UId, RoleId) {

            $('#<%= showdetails.ClientID%>').attr('src', "../Common/TemplateTransactionDetail.aspx?UID=" + UId + "&RID=" + RoleId + "&TID=" + TemplateId + "&CID=" + CustId);
        }

        function OpenTransactionHistory(TemplateId, CustId, UId, RoleId) {
            <%--<iframe id="showdetails1" src="../Common/TemplateTransactionHistry.aspx?UID=<% =UId%>&RID=<% =RoleId%>&TID=<% =TemplateId%>&CID=<% =CustId%>" width="100%" height="525px;" frameborder="0"></iframe>--%>
            $('#<%= showdetails1.ClientID%>').attr('src', "../Common/TemplateTransactionHistry.aspx?UID=" + UId + "&RID=" + RoleId + "&TID=" + TemplateId + "&CID=" + CustId);
        }

        function CloseSectionModal() {
            $('#TemplateSectionPopup').modal('hide');

        }

        function EnableLinkButton(ID, flag) {
            document.getElementById(ID).onclick = function () { return flag; };
            if (!flag) {
                document.getElementById(ID).setAttribute("disabled", "disabled");
                document.getElementById(ID).className = "LnkDisabled";
            }
            else {
                document.getElementById(ID).setAttribute("disabled", "");
                document.getElementById(ID).className = "LnkEnabled";
            }
        }
        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }
        function CheckNumber(event) {

            if (event.keyCode == 46 || event.keyCode == 8) {
            } else {
                if (event.keyCode < 95) {
                    if (event.keyCode < 48 || event.keyCode > 57) {
                        event.preventDefault();
                    }
                } else {
                    if (event.keyCode < 96 || event.keyCode > 105) {
                        event.preventDefault();
                    }
                }
            }
        }
        function pageLoad() {
            BindControls();
            $('[data-toggle="tooltip"]').tooltip()
        };
    </script>
    <style type="text/css">
        ul.multiselect-container.dropdown-menu {
            width: 100%;
            max-height: 218px;
            overflow-x: hidden;
        }

        .input-group-btn {
            position: relative;
            white-space: nowrap;
            display: none;
        }

        .glyphicon-remove-circle {
            top: 1px;
            display: none;
            font-style: normal;
            font-weight: 400;
            line-height: 1;
            -webkit-font-smoothing: antialiased;
        }

        .multiselect-container > li {
            padding: 0;
            margin-right: 10px;
        }

        #upAddEditTemplate {
            width: 100%;
        }

        ul.multiselect-container.dropdown-menu {
            height: auto;
        }
    </style>
    <script type="text/javascript">

        BindControls();
        function BindControls() {
            $(function () {
                $('i.glyphicon.glyphicon-search').removeClass('glyphicon glyphicon-search').addClass('fa fa-search color-black');

                $('[id*=lstBoxDepartment]').multiselect({
                    includeSelectAllOption: true,
                    numberDisplayed: 2,
                    buttonWidth: '100%',
                    enableCaseInsensitiveFiltering: true,
                    filterPlaceholder: 'Type to Search for Department..',
                    nSelectedText: ' - Department(s) selected',
                    enableFiltering: true,
                });
            });
        }

        function checkAll(chkHeader) {
            var selectedRowCount = 0;
            var grid = document.getElementById("<%=grdTemplateSection.ClientID %>");
            if (grid != null) {
                if (chkHeader != null) {
                    //Get all input elements in Gridview
                    var inputList = grid.getElementsByTagName("input");

                    for (var i = 1; i < inputList.length; i++) {
                        if (inputList[i].type == "checkbox") {
                            if (chkHeader.checked) {
                                inputList[i].checked = true;
                                selectedRowCount++;
                            }
                            else if (!chkHeader.checked)
                                inputList[i].checked = false;
                        }
                    }
                }

                var btnSave = document.getElementById("<%=btnSaveSections.ClientID %>");
                var lblTotalSectionsSelected = document.getElementById("<%=lblTotalSectionsSelected.ClientID %>");
                if ((btnSave != null || btnSave != undefined) && (lblTotalSectionsSelected != null || lblTotalSectionsSelected != undefined)) {
                    if (selectedRowCount > 0) {
                        lblTotalSectionsSelected.innerHTML = selectedRowCount + " Selected";
                        divCountSelectedSections.style.display = "block";
                    }
                    else {
                        lblTotalSectionsSelected.innerHTML = "";;
                        divCountSelectedSections.style.display = "none";
                    }
                }
            }
        }

        function checkUncheckRow() {

            var selectedRowCount = 0;
            //Get the reference of GridView
            var grid = document.getElementById("<%=grdTemplateSection.ClientID %>");

            if (grid != null) {

                //Get all input elements in Gridview
                var inputList = grid.getElementsByTagName("input");

                //The First element is the Header Checkbox
                var headerCheckBox = inputList[0];
                var checked = true;
                for (var i = 0; i < inputList.length; i++) {
                    //Based on all or none checkboxes are checked check/uncheck Header Checkbox                
                    if (inputList[i].type == "checkbox" && inputList[i] != headerCheckBox) {
                        if (!inputList[i].checked) {
                            checked = false;
                        }
                        if (inputList[i].checked) {
                            selectedRowCount++;
                        }
                    }
                }

                headerCheckBox.checked = checked;

                var btnSave = document.getElementById("<%=btnSaveSections.ClientID %>");
                var lblTotalSectionsSelected = document.getElementById("<%=lblTotalSectionsSelected.ClientID %>");
                if ((btnSave != null || btnSave != undefined) && (lblTotalSectionsSelected != null || lblTotalSectionsSelected != undefined)) {
                    if (selectedRowCount > 0) {
                        lblTotalSectionsSelected.innerHTML = selectedRowCount + " Selected";
                        divCountSelectedSections.style.display = "block";
                    }
                    else {
                        lblTotalSectionsSelected.innerHTML = "";;
                        divCountSelectedSections.style.display = "none";
                    }
                }
            }
        }

        function unCheckAll(gridName) {
            var grid = document.getElementById(gridName);
            if (grid != null) {
                //Get all input elements in Gridview
                var inputList = grid.getElementsByTagName("input");

                for (var i = 1; i < inputList.length; i++) {
                    if (inputList[i].type == "checkbox") {
                        inputList[i].checked = false;
                    }
                }
            }
        }

        function ClosedSectionTemplate() {
            document.getElementById('<%= lnkBtnImportSection.ClientID %>').click();
        }

    </script>
</head>
<body class="bgColor-white">
    <form id="form1" runat="server">
        <div>
            <asp:ScriptManager ID="smAddEditTemplate" runat="server"></asp:ScriptManager>
            <asp:UpdatePanel ID="upAddEditTemplate" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="row">
                        <ul class="nav nav-tabs">
                            <li class="active" id="liTemplate" runat="server">
                                <asp:LinkButton ID="lnkBtnTemplate" OnClick="lnkBtnTemplate_Click" runat="server">Step 1-Import Section</asp:LinkButton>
                            </li>
                            <li class="" id="liImportSection" runat="server">
                                <asp:LinkButton ID="lnkBtnImportSection" OnClick="lnkBtnImportSection_Click" runat="server">Step 2-Template</asp:LinkButton>
                            </li>
                            <li class="" id="liTransactionTemplate" runat="server">
                                <asp:LinkButton ID="lnkBtnTransactionHistry" OnClick="lnkBtnTransactionHistry_Click" runat="server">History</asp:LinkButton>
                            </li>
                        </ul>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-12 alert alert-success" runat="server" visible="false" id="divsuccessmsgaCTem">
                            <asp:Label runat="server" ID="successmsgaCTem"></asp:Label>
                        </div>
                        <asp:ValidationSummary ID="vsAddEditTemplate" runat="server"
                            Style="padding-left: 41px;" Display="none" class="alert alert-block alert-danger fade in"
                            ValidationGroup="ContractTemplateValidationGroup" />
                        <asp:CustomValidator ID="cvAddEditTemplate" runat="server" EnableClientScript="False"
                            ValidationGroup="ContractTemplateValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                    </div>
                    <div>
                        <asp:MultiView ID="MainView" runat="server">
                            <asp:View ID="TemplateView" runat="server">
                                <div class="row form-group">
                                    <div class="form-group required col-md-4">
                                        <label for="tbxTemplateName" class="control-label">Template Name</label>
                                        <asp:TextBox runat="server" ID="tbxTemplateName" CssClass="form-control" autocomplete="off" />
                                        <asp:RequiredFieldValidator ID="rfvTemplateName" ErrorMessage="Please Enter Template Name." ControlToValidate="tbxTemplateName"
                                            runat="server" ValidationGroup="ContractTemplateValidationGroup" Display="None" />
                                    </div>
                                    <div class="form-group required col-md-4">
                                        <label for="tbxVersion" class="control-label">Department</label>
                                        <asp:ListBox ID="lstBoxDepartment" CssClass="form-control" runat="server" SelectionMode="Multiple" Width="100%"></asp:ListBox>
                                        <asp:RequiredFieldValidator ID="rfvdept" ErrorMessage="Please Select Department."
                                            ControlToValidate="lstBoxDepartment" runat="server" ValidationGroup="ContractTemplateValidationGroup" Display="None" />
                                    </div>
                                    <div class="form-group required col-md-4">
                                        <label for="tbxApprover" class="control-label">Approver Level</label>
                                        <asp:TextBox runat="server" ID="tbxApprover" CssClass="form-control" onkeypress="return isNumber(event)" autocomplete="off" />

                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator61" ErrorMessage="Please enter valid Approver Level."
                                            ControlToValidate="tbxApprover" runat="server" ValidationGroup="ContractTemplateValidationGroup"
                                            Display="None" />

                                    </div>
                                </div>
                                <div class="row form-group text-right" style="display: none;">
                                    <div class="col-md-12">
                                        <asp:Button Text="Save" runat="server" ID="btnSaveTemplate" CssClass="btn btn-primary"
                                            ValidationGroup="ContractTemplateValidationGroup" OnClick="btnSaveTemplate_Click" />
                                        <asp:Button Text="Clear" runat="server" ID="btnClear" CssClass="btn btn-primary"
                                            OnClick="btnClearTemplate_Click" Visible="false" />
                                        <asp:Button Text="Export as Word" runat="server" ID="btnExportWord1" CssClass="btn btn-primary"
                                            OnClick="btnExportWord_Click" />
                                        <asp:Button Text="Save & Next" runat="server" ID="btnSaveNext" CssClass="btn btn-primary"
                                            ValidationGroup="ContractTemplateValidationGroup" OnClick="btnSaveNext_Click" />
                                    </div>
                                </div>



                                <div id="divOuterGridTemplateSection" class="row col-md-12" runat="server">
                                    <div class="w100per">
                                        <div class="float-left col-md-11 colpadding0 w95per">
                                            <label for="grdTemplateSection" class="control-label">Section Browser</label>
                                            <i style="color: #c1c1c1;">(Select (<i class="fa fa-check" aria-hidden="true"></i>) one or more section(s) to add in the template)</i>
                                        </div>
                                        <div class="float-left col-md-1 colpadding0 w5per text-right">
                                        </div>
                                    </div>
                                    <div class="row col-md-12">
                                        <div class="row col-md-3" style="margin-left: -28px;">
                                            <asp:TextBox runat="server" ID="tbxFilter" CssClass="form-control" PlaceHolder="Type Section Name to Search" AutoPostBack="true"
                                                OnTextChanged="tbxFilter_TextChanged" Width="100%" AutoComplete="off" />
                                        </div>
                                        <div class="row col-md-2">
                                            <asp:LinkButton runat="server" ID="btnAddNew" OnClick="btnAddNew_Click"
                                                data-toggle="tooltip" data-placement="top" title="Add New Section">
                                                <img src='/Images/add_icon_new.png' alt="Section" />
                                            </asp:LinkButton>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>

                                    <div class="col-md-12 mt5 mb5" style="min-height: 50px; max-height: 300px; overflow-y: auto; overflow-x: hidden">
                                        <asp:GridView runat="server" ID="grdTemplateSection" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true"
                                            AllowSorting="true" GridLines="none" Width="100%" AllowPaging="false" AutoPostBack="true" CssClass="table" DataKeyNames="ID"
                                            OnRowCommand="grdTemplateSections_RowCommand"
                                            OnRowDataBound="grdTemplateSection_RowDataBound">
                                            <Columns>
                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="5%">
                                                    <HeaderTemplate>
                                                        <asp:CheckBox ID="chkHeader" runat="server" onclick="javascript:checkAll(this)" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkRow" runat="server" onclick="javascript:checkUncheckRow(this)" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Order" ItemStyle-Width="7%" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSectionID" runat="server" Text='<%# Eval("ID") %>' Visible="false"></asp:Label>
                                                        <asp:Label ID="lblSectionvisibility" runat="server" Text='<%# Eval("Headervisibilty") %>' Visible="false"></asp:Label>
                                                        <asp:TextBox ID="txtOrder" runat="server" CssClass="form-control text-center" autocomplete="off" onkeypress="return isNumber(event)"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Header" HeaderText="Header" ItemStyle-Width="45%" />

                                                 <asp:TemplateField HeaderText="Department" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-left">
                                                    <ItemTemplate>
                                                     <asp:DropDownListChosen runat="server" ID="ddldpetsection" class="form-control" Width="100%" AutoPostBack="true"
                                                            AllowSingleDeselect="false" OnSelectedIndexChanged="ddldpetsection_SelectedIndexChanged" DataPlaceHolder="Select Department" DisableSearchThreshold="5">
                                                        </asp:DropDownListChosen>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Reviewer" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-left">
                                                    <ItemTemplate>
                                                    <asp:Label ID="lblDeptID" runat="server" Text='<%# Eval("DeptID") %>' Visible="false"></asp:Label>
                                                        <asp:DropDownListChosen runat="server" ID="ddlReviewer" class="form-control" Width="100%"
                                                            AllowSingleDeselect="false" DataPlaceHolder="Select Reviewer" DisableSearchThreshold="5">
                                                        </asp:DropDownListChosen>
                                                    </ItemTemplate>
                                                </asp:TemplateField>



                                                <asp:BoundField DataField="DeptName" HeaderText="Department" ItemStyle-Width="10%"
                                                    HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center" />
                                                <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="10%"
                                                    HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkViewSection" runat="server" CommandName="View_Section"
                                                            ToolTip="View" data-toggle="tooltip" CommandArgument='<%# Eval("ID") %>'>
                                                            <img src='<%# ResolveUrl("/Images/Eye.png")%>' alt="View" />
                                                        </asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <RowStyle CssClass="clsROWgrid" />
                                            <HeaderStyle CssClass="clsheadergrid" />
                                            <PagerTemplate>
                                                <table style="display: none">
                                                    <tr>
                                                        <td>
                                                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </PagerTemplate>
                                            <EmptyDataTemplate>
                                                No Record Found
                                            </EmptyDataTemplate>
                                        </asp:GridView>

                                        <asp:LinkButton ID="lnkBtnRebind_Sections" OnClick="lnkBtnRebind_Sections_Click"
                                            Style="float: right; display: none;" Width="100%" runat="server">
                                        </asp:LinkButton>
                                    </div>
                                </div>

                                <div id="divCountSelectedSections" class="row col-md-12 pl0" style="display: none;">
                                    <div class="col-md-12 text-left">
                                        <asp:Label runat="server" ID="lblTotalSectionsSelected" Text="" CssClass="control-label"></asp:Label>
                                    </div>
                                </div>

                                <div class="row form-group text-right">
                                    <div class="col-md-12">
                                        <asp:Button Text="Save & Next" runat="server" ID="btnSaveSections" CssClass="btn btn-primary"
                                            ValidationGroup="ContractTemplateValidationGroup" OnClick="btnSaveSections_Click" />
                                        <asp:Button Text="Export as Word" runat="server" ID="btnExportWord2" CssClass="btn btn-primary"
                                            OnClick="btnExportWord_Click" Style="display: none;" />
                                        <asp:Button Text="Clear" runat="server" Visible="false" ID="btnClearSections"
                                            CssClass="btn btn-primary" data-dismiss="modal" OnClick="btnClearSections_Click" />
                                        <asp:Button Text="Close" runat="server" ID="Button2"
                                            CssClass="btn btn-primary" data-dismiss="modal" OnClientClick="CloseMe();" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <p style="color: red;"><strong>Note:</strong> (*) fields are Compulsory</p>
                                    </div>
                                </div>
                            </asp:View>

                            <asp:View ID="SectionBrowserView" runat="server">
                                <div>
                                    <iframe src="about:blank" id="showdetails" runat="server" width="100%" height="525px;" frameborder="0"></iframe>

                                </div>
                            </asp:View>
                            <asp:View ID="TransactionHistryView" runat="server">
                                <iframe src="about:blank" id="showdetails1" runat="server" width="100%" height="525px;" frameborder="0"></iframe>

                            </asp:View>
                        </asp:MultiView>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnExportWord1" />
                    <asp:PostBackTrigger ControlID="btnExportWord2" />
                </Triggers>
            </asp:UpdatePanel>
            <div class="modal fade" id="TemplateSectionPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header" style="margin-bottom: 10px; border-bottom: none;">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="CloseSectionModal()">&times;</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                            <iframe src="about:blank" id="IframeTemplateSection" runat="server" frameborder="0" width="100%" height="450px"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>

</html>
