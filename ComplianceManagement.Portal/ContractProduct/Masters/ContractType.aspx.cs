﻿using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Contract.Masters
{
    public partial class ContractType : System.Web.UI.Page
    {
        private long CutomerID = AuthenticationHelper.CustomerID;
        protected bool flag;
        protected Cont_tbl_PageAuthorizationMaster authpage;
        protected int pageid = 4;
        //protected int User_cont_id;

        protected void Page_Load(object sender, EventArgs e)
        {
            

            if (!IsPostBack)
            {
                String key = "ContractAuthenticate" + AuthenticationHelper.UserID;
                if (Cache.Get(key) != null)
                {
                    var Records = (List<Cont_tbl_PageAuthorizationMaster>)HttpContext.Current.Cache[key];
                    if (Records.Count > 0)
                    {
                        var query = (from row in Records
                                     where row.PageID == pageid
                                     select row).FirstOrDefault();
                        authpage = query;
                    }
                }
                if (authpage != null)
                {
                    btnAddNew.Visible = false;
                    if (authpage.Addval == true)
                    {
                        btnAddNew.Visible = true;
                    }
                }
                else
                    btnAddNew.Visible = true;

                flag = false;
                BindContractTypes();
                bindPageNumber();
            }
        }

        protected void btnAddNew_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["Mode"] = 0;
                string ID = null;
                lblName.InnerText = "Add New Contract Type";

                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenCaseTypePopup('" + ID + "');", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvContractTypePage.IsValid = false;
                cvContractTypePage.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        private void BindContractTypes()
        {
            try
            {
                var TypeList = ContractTypeMasterManagement.GetContractTypes_Paging(Convert.ToInt32(CutomerID), tbxFilter.Text.Trim().ToString());
             
                string SortExpr = string.Empty;
                string CheckDirection = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(ViewState["Direction"])))
                    {
                        CheckDirection = Convert.ToString(ViewState["Direction"]);

                        SortExpr = Convert.ToString(ViewState["SortExpression"]);
                        if (CheckDirection == "Ascending")
                        {
                            TypeList = TypeList.OrderBy(entry => entry.GetType().GetProperty(SortExpr).GetValue(entry, null)).ToList();
                        }
                        else
                        {
                            CheckDirection = "Descending";
                            TypeList = TypeList.OrderByDescending(entry => entry.GetType().GetProperty(SortExpr).GetValue(entry, null)).ToList();
                        }
                    }
                }
                flag = true;
                Session["TotalRows"] = null;
                if (TypeList.Count > 0)
                {
                    grdContractType.DataSource = TypeList;
                    Session["TotalRows"] = TypeList.Count;
                    grdContractType.DataBind();
                }
                else
                {
                    grdContractType.DataSource = TypeList;
                    grdContractType.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvContractTypePage.IsValid = false;
                cvContractTypePage.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                grdContractType.PageIndex = 0;
                BindContractTypes();
                bindPageNumber();
                SetShowingRecords();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvContractTypePage.IsValid = false;
                cvContractTypePage.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdContractType.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                BindContractTypes();

                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdContractType.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();

                }

                SetShowingRecords();

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                throw ex;
            }
        }

        private void SetShowingRecords()
        {
            if (Session["TotalRows"] != null)
            {
                int PageSize = 0;
                int PageNumber = 0;

                if (!string.IsNullOrEmpty(ddlPageSize.SelectedItem.Text))
                    PageSize = Convert.ToInt32(ddlPageSize.SelectedItem.Text);

                if (!string.IsNullOrEmpty(DropDownListPageNo.SelectedValue))
                    PageNumber = Convert.ToInt32(DropDownListPageNo.SelectedValue);

                var EndRecord = 0;
                var TotalRecord = 0;
                var TotalValue = PageSize * PageNumber;

                TotalRecord = Convert.ToInt32(Session["TotalRows"]);
                if (TotalRecord < TotalValue)
                {
                    EndRecord = TotalRecord;
                }
                else
                {
                    EndRecord = TotalValue;
                }

                if (TotalRecord != 0)
                    lblStartRecord.Text = Convert.ToString(PageSize * PageNumber - PageSize + 1);
                else
                    lblStartRecord.Text = "0";

                lblEndRecord.Text = Convert.ToString(EndRecord) + " ";
                lblTotalRecord.Text = TotalRecord.ToString();
            }
            else
            {
                lblStartRecord.Text = "0 ";
                lblEndRecord.Text = "0 ";
                lblTotalRecord.Text = "0";
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                long CustomerID = AuthenticationHelper.CustomerID;
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displayed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);
               
                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        protected void lnkBtn_RebindGrid_Click(object sender, EventArgs e)
        {
            BindContractTypes(); bindPageNumber();
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdContractType.PageIndex = chkSelectedPage - 1;
            grdContractType.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            BindContractTypes();SetShowingRecords();
        }

        protected void grdContractType_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (ContractTypeMasterManagement.getContractRole(Convert.ToInt64(AuthenticationHelper.UserID), Convert.ToInt64(AuthenticationHelper.CustomerID)))
                {
                    LinkButton LinkButton2 = (LinkButton)e.Row.FindControl("lnkEditContractType");
                
                    LinkButton LinkButton1 = (LinkButton)e.Row.FindControl("lnkDeleteContractType");
                 
                    LinkButton2.Visible = true;
                    LinkButton1.Visible = true;
                }
                else if (authpage != null)
                {
                    LinkButton LinkButton2 = (LinkButton)e.Row.FindControl("lnkEditContractType");
                    LinkButton2.Visible = false;

                    LinkButton LinkButton1 = (LinkButton)e.Row.FindControl("lnkDeleteContractType");
                    LinkButton1.Visible = false;

                    if (authpage.Modify == true)
                    {
                        LinkButton2.Visible = true;
                    }
                    if (authpage.Deleteval == true)
                    {
                        LinkButton1.Visible = true;
                    }
                }
            }
        }
               
        protected void grdContractType_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {               
                long CustomerID = AuthenticationHelper.CustomerID;
                lblName.InnerText = "Edit Contract Type Details";

                if (e.CommandName.Equals("EDIT_ContType"))
                {
                    long ID = Convert.ToInt32(e.CommandArgument);

                    ViewState["Mode"] = 1;
                    ViewState["ContTypeID"] = ID;

                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenCaseTypePopup('" + ID + "');", true);
                    
                }
                else if (e.CommandName.Equals("DELETE_ContType"))
                {
                    long typeID = Convert.ToInt64(e.CommandArgument);

                    bool deleteSuccess = ContractTypeMasterManagement.DeleteContractType(typeID, Convert.ToInt32(CustomerID));

                    if (deleteSuccess)
                    {
                        BindContractTypes(); SetShowingRecords();
                    }
                }
                else if (e.CommandName.Equals("VIEW_ContractType"))
                {
                    long typeID = Convert.ToInt64(e.CommandArgument);
                    GridViewRow gvr = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                    int RowIndex = gvr.RowIndex;
                    
                    Session["ContractTypeID"] = typeID;
                   
                    Session["ParentID"] = null;
                    Response.Redirect("~/ContractProduct/Masters/ContractSubType.aspx", false);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvContractTypePage.IsValid = false;
                cvContractTypePage.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        
        protected void grdContractType_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                int customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);

                var detailView = ContractTypeMasterManagement.GetContractTypes_Paging(Convert.ToInt32(CutomerID), tbxFilter.Text.Trim().ToString());
                // var detailView = ContractTypeMasterManagement.GetAllContractTypes(CutomerID);
                int TotalCount = 0;
                List<object> dataSource = new List<object>();
                foreach (var contInfo in detailView)
                {
                    dataSource.Add(new
                    {
                        contInfo.TypeName,
                        contInfo.ID
                    });
                }

               string SortExpr = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                {
                    SortExpr = Convert.ToString(ViewState["SortExpression"]);
                    if (SortExpr == e.SortExpression)
                    {
                        if (direction == SortDirection.Ascending)
                        {
                            direction = SortDirection.Descending;
                        }
                        else
                        {
                            direction = SortDirection.Ascending;
                        }
                    }
                    else
                    {
                        direction = SortDirection.Ascending;
                    }
                }

                if (direction == SortDirection.Ascending)
                {
                    ViewState["Direction"] = "Ascending";
                    dataSource = dataSource.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                }
                else
                {
                    ViewState["Direction"] = "Descending";
                    dataSource = dataSource.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                }

                ViewState["SortExpression"] = e.SortExpression;
                foreach (DataControlField field in grdContractType.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdContractType.Columns.IndexOf(field);
                    }
                }
                flag = true;
                grdContractType.DataSource = dataSource;
                grdContractType.DataBind();
                Session["TotalRows"] = TotalCount;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void grdContractType_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
            {
                System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
                sortImage.ImageAlign = ImageAlign.AbsMiddle;

                if (flag == true)
                {
                    if (direction == SortDirection.Ascending)
                    {
                        sortImage.ImageUrl = "../../Images/down_arrow1.png";
                        sortImage.AlternateText = "Ascending Order";
                    }
                    else
                    {
                        sortImage.ImageUrl = "../../Images/up_arrow1.png";
                        sortImage.AlternateText = "Descending Order";
                    }
                    headerRow.Cells[columnIndex].Controls.Add(sortImage);
                }
            }
        }

        protected void lnkPreviousSwitch_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/ContractProduct/Masters/VendorMaster.aspx", false);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lnkNextSwitch_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/ContractProduct/Masters/ContDocumentType.aspx", false);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lnkApply_Click(object sender, EventArgs e)
        {
            BindContractTypes(); bindPageNumber();
        }

    }
}