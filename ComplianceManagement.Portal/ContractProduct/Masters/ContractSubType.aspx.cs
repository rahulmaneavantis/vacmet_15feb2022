﻿using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.Masters
{
    public partial class ContractSubType : System.Web.UI.Page
    {
        private long CutomerID = AuthenticationHelper.CustomerID;
        protected bool flag;
        protected Cont_tbl_PageAuthorizationMaster authpage;
        protected int pageid = 9;
        protected void Page_Load(object sender, EventArgs e)
        {
            

            if (!IsPostBack)
            {
                String key = "ContractAuthenticate" + AuthenticationHelper.UserID;
                if (Cache.Get(key) != null)
                {
                    var Records = (List<Cont_tbl_PageAuthorizationMaster>)HttpContext.Current.Cache[key];
                    if (Records.Count > 0)
                    {
                        var query = (from row in Records
                                     where row.PageID == pageid
                                     select row).FirstOrDefault();
                        authpage = query;
                    }
                }
                if (authpage != null)
                {
                    btnAddSubType.Visible = false;
                    if (authpage.Addval == true)
                    {
                        btnAddSubType.Visible = true;
                    }
                }
                else
                    btnAddSubType.Visible = true;

                try
                {
                    flag = false;
                    if (Session["ContractTypeID"] != null)
                    {
                        ViewState["ContractTypeID"] = Session["ContractTypeID"];

                        BindSubContType();
                        bindPageNumber();
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
            }
        }

        private void BindSubContType()
        {
            try
            {
                int customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                long contractTypeID = -1;

                if (ViewState["ContractTypeID"] != null)
                {
                    contractTypeID = Convert.ToInt64(ViewState["ContractTypeID"]);

                    var lstContractSubTypes = ContractTypeMasterManagement.GetContractSubTypes_Paging(customerID, contractTypeID, tbxtypeTofilter.Text.Trim().ToString());

                    if (lstContractSubTypes.Count > 0)
                    {
                        lstContractSubTypes = lstContractSubTypes.OrderBy(entry => entry.SubTypeName).ToList();
                    }

                    string SortExpr = string.Empty;
                    string CheckDirection = string.Empty;
                    if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(ViewState["Direction"])))
                        {
                            CheckDirection = Convert.ToString(ViewState["Direction"]);

                            SortExpr = Convert.ToString(ViewState["SortExpression"]);
                            if (CheckDirection == "Ascending")
                            {
                                lstContractSubTypes = lstContractSubTypes.OrderBy(entry => entry.GetType().GetProperty(SortExpr).GetValue(entry, null)).ToList();
                            }
                            else
                            {
                                CheckDirection = "Descending";
                                lstContractSubTypes = lstContractSubTypes.OrderByDescending(entry => entry.GetType().GetProperty(SortExpr).GetValue(entry, null)).ToList();
                            }
                        }
                    }

                    flag = true;
                    Session["TotalRows"] = null;
                    if (lstContractSubTypes.Count > 0)
                    {
                        grdSubConType.DataSource = lstContractSubTypes;
                        Session["TotalRows"] = lstContractSubTypes.Count;
                        grdSubConType.DataBind();
                    }
                    else
                    {
                        grdSubConType.DataSource = lstContractSubTypes;
                        grdSubConType.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                grdSubConType.PageIndex = 0;
                BindSubContType();
                bindPageNumber();
                SetShowingRecords();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvSubTypePage.IsValid = false;
                cvSubTypePage.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void grdSubConType_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                if (e.CommandName.Equals("EDIT_SubContype"))
                {
                    long subTypeID = Convert.ToInt64(e.CommandArgument);
                    ViewState["Mode"] = 1;
                    ViewState["SubTypeID"] = subTypeID;

                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenSubConTypePopup('" + subTypeID + "');", true);
                }
                else if (e.CommandName.Equals("DELETE_SubContype"))
                {
                    long subTypeID = Convert.ToInt64(e.CommandArgument);
                    bool deleteSuccess = ContractTypeMasterManagement.DeleteContractSubType(subTypeID, CustomerID);

                    if (deleteSuccess)
                    {
                        BindSubContType();SetShowingRecords();
                        cvSubTypePage.IsValid = false;
                        cvSubTypePage.ErrorMessage = "Contract Sub-Type Deleted Successfully";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvSubTypePage.IsValid = false;
                cvSubTypePage.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void btnAddSubType_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["Mode"] = 0;
                string ID = null;
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenSubConTypePopup('" + ID + "');", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvSubTypePage.IsValid = false;
                cvSubTypePage.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void lnkBackToContractType_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/ContractProduct/Masters/ContractType.aspx", false);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvSubTypePage.IsValid = false;
                cvSubTypePage.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdSubConType.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                BindSubContType();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdSubConType.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvSubTypePage.IsValid = false;
                cvSubTypePage.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }

                DropDownListPageNo.DataTextField = "ContractSubTypeID";
                DropDownListPageNo.DataValueField = "ContractSubTypeID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }

                SetShowingRecords();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                throw ex;
            }
        }

        private void SetShowingRecords()
        {
            if (Session["TotalRows"] != null)
            {
                int PageSize = 0;
                int PageNumber = 0;

                if (!string.IsNullOrEmpty(ddlPageSize.SelectedItem.Text))
                    PageSize = Convert.ToInt32(ddlPageSize.SelectedItem.Text);

                if (!string.IsNullOrEmpty(DropDownListPageNo.SelectedValue))
                    PageNumber = Convert.ToInt32(DropDownListPageNo.SelectedValue);
                var EndRecord = 0;
                var TotalRecord = 0;
                var TotalValue = PageSize * PageNumber;

                TotalRecord = Convert.ToInt32(Session["TotalRows"]);
                if (TotalRecord < TotalValue)
                {
                    EndRecord = TotalRecord;
                }
                else
                {
                    EndRecord = TotalValue;
                }

                if (TotalRecord != 0)
                    lblStartRecord.Text = Convert.ToString(PageSize * PageNumber - PageSize + 1);
                else
                    lblStartRecord.Text = "0";

                lblEndRecord.Text = Convert.ToString(EndRecord) + " ";
                lblTotalRecord.Text = TotalRecord.ToString();
            }
            else
            {
                lblStartRecord.Text = "0 ";
                lblEndRecord.Text = "0 ";
                lblTotalRecord.Text = "0";
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                long CustomerID = AuthenticationHelper.CustomerID;
                int totalPages = 0;
                if (Session["TotalRows"] != null)
                {
                    TotalRows.Value = Session["TotalRows"].ToString();

                    totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                    // total page item to be displayed
                    int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                    // remaing no of pages
                    if (pageItemRemain > 0)// set total No of pages
                    {
                        totalPages = totalPages + 1;
                    }
                    else
                    {
                        totalPages = totalPages + 0;
                    }
                    
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        protected void lnkBtn_RebindGrid_Click(object sender, EventArgs e)
        {
            BindSubContType(); bindPageNumber();
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdSubConType.PageIndex = chkSelectedPage - 1;
            grdSubConType.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            BindSubContType(); SetShowingRecords();
        }

        protected void grdSubConType_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                if (ViewState["ContractTypeID"] != null)
                {
                    int totalrowcount = 0;
                    long contractTypeID = Convert.ToInt64(ViewState["ContractTypeID"]);
                    var detailView = ContractTypeMasterManagement.GetContractSubTypes_Paging(Convert.ToInt32(AuthenticationHelper.CustomerID), contractTypeID, tbxtypeTofilter.Text.Trim().ToString());

                    List<object> dataSource = new List<object>();
                    foreach (var contInfo in detailView)
                    {
                        dataSource.Add(new
                        {
                            contInfo.RowID,
                            contInfo.ContractSubTypeID,
                            contInfo.TypeName,
                            contInfo.SubTypeName
                        });
                    }

                    string SortExpr = string.Empty;
                    if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                    {
                        SortExpr = Convert.ToString(ViewState["SortExpression"]);
                        if (SortExpr == e.SortExpression)
                        {
                            if (direction == SortDirection.Ascending)
                            {
                                direction = SortDirection.Descending;
                            }
                            else
                            {
                                direction = SortDirection.Ascending;
                            }
                        }
                        else
                        {
                            direction = SortDirection.Ascending;
                        }
                    }

                    if (direction == SortDirection.Ascending)
                    {
                        ViewState["Direction"] = "Ascending";
                        dataSource = dataSource.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    }
                    else
                    {
                        ViewState["Direction"] = "Descending";
                        dataSource = dataSource.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    }

                    ViewState["SortExpression"] = e.SortExpression;
                    foreach (DataControlField field in grdSubConType.Columns)
                    {
                        if (field.SortExpression == e.SortExpression)
                        {
                            ViewState["SortIndex"] = grdSubConType.Columns.IndexOf(field);
                        }
                    }
                    flag = true;
                    grdSubConType.DataSource = dataSource;
                    grdSubConType.DataBind();
                    Session["TotalRows"] = totalrowcount;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdSubConType_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }
        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
            {
                System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
                sortImage.ImageAlign = ImageAlign.AbsMiddle;

                if (flag == true)
                {
                    if (direction == SortDirection.Ascending)
                    {
                        sortImage.ImageUrl = "../../Images/down_arrow1.png";
                        sortImage.AlternateText = "Ascending Order";
                    }
                    else
                    {
                        sortImage.ImageUrl = "../../Images/up_arrow1.png";
                        sortImage.AlternateText = "Descending Order";
                    }
                    headerRow.Cells[columnIndex].Controls.Add(sortImage);
                }
            }
        }
        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection) ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void lnkBtnApplyFilter_Click(object sender, EventArgs e)
        {
            try
            {
                BindSubContType();
                bindPageNumber();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvSubTypePage.IsValid = false;
                cvSubTypePage.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void grdSubConType_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (ContractTypeMasterManagement.getContractRole(Convert.ToInt64(AuthenticationHelper.UserID), Convert.ToInt64(AuthenticationHelper.CustomerID)))
                {
                    LinkButton LinkButton2 = (LinkButton)e.Row.FindControl("LinkButton1");

                    LinkButton LinkButton1 = (LinkButton)e.Row.FindControl("LinkButton2");

                    LinkButton2.Visible = true;

                    LinkButton1.Visible = true;
                }
                else if (authpage != null)
                {
                    LinkButton LinkButton2 = (LinkButton)e.Row.FindControl("LinkButton1");
                    LinkButton2.Visible = false;

                    LinkButton LinkButton1 = (LinkButton)e.Row.FindControl("LinkButton2");
                    LinkButton1.Visible = false;

                    if (authpage.Modify == true)
                    {
                        LinkButton2.Visible = true;
                    }
                    if (authpage.Deleteval == true)
                    {
                        LinkButton1.Visible = true;
                    }
                }
            }
        }
    }
}