﻿using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.Masters
{
    public partial class AddCustomFieldValue : System.Web.UI.Page
    {
        private long CustomerID = AuthenticationHelper.CustomerID;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                divsuccessmsgaCcustomtype.Visible = false;
                if (!IsPostBack)
                {
                    BindContractTypes();

                    if (!string.IsNullOrEmpty(Request.QueryString["lblValue"]) && !string.IsNullOrEmpty(Request.QueryString["TypeID"]) && !string.IsNullOrEmpty(Request.QueryString["CustomFieldId"]))
                    {
                        string lblValue = Request.QueryString["lblValue"];

                        long TypeID = Convert.ToInt64(Request.QueryString["TypeID"]);

                        ViewState["Mode"] = "1";
                        btnSave.Text = "Update";
                        BindData(lblValue, TypeID);
                    }
                    else if (!string.IsNullOrEmpty(Request.QueryString["TypeID"]))
                    {
                        long TypeID = Convert.ToInt64(Request.QueryString["TypeID"]);

                        ViewState["Mode"] = "0";
                        btnSave.Text = "Save";
                        BindDatabytype(TypeID);
                    }
                    else
                    {
                        ViewState["Mode"] = "0";
                        btnSave.Text = "Save";
                    }
                }

                ScriptManager.RegisterStartupScript(this, GetType(), "myPostBackScript", "bindMultiSelect();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindDatabytype(long typeID)
        {
            try
            {
                if (ddlContractType.Items.Count > 0)
                {
                    ddlContractType.ClearSelection();

                    if (ddlContractType.Items.FindByValue(typeID.ToString()) != null)
                        ddlContractType.Items.FindByValue(typeID.ToString()).Selected = true;

                    tbxLableName.Text = string.Empty;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindData(string lblValue, long TypeID)
        {
            try
            {
                if (ddlContractType.Items.Count > 0)
                {
                    ddlContractType.ClearSelection();

                    if (ddlContractType.Items.FindByValue(TypeID.ToString()) != null)
                        ddlContractType.Items.FindByValue(TypeID.ToString()).Selected = true;

                    tbxLableName.Text = lblValue;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindContractTypes()
        {
            try
            {
                var lstContractType = ContractTypeMasterManagement.GetContractTypes_All(Convert.ToInt32(CustomerID));

                ddlContractType.DataTextField = "TypeName";
                ddlContractType.DataValueField = "ID";

                ddlContractType.DataSource = lstContractType;
                ddlContractType.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string CheckStatus = string.Empty;
                List<long> lstSelectContractTypes = new List<long>();

                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["Mode"])))
                {
                    bool validationSuccess = false;
                    bool saveSuccess = false;

                    #region Validation
                    if (tbxLableName.Text != "")
                    {
                        lstSelectContractTypes.Clear();

                        long fieldID = 0;
                        long contractTypeID = 0;

                        if (Convert.ToString(ViewState["Mode"]) == "1")
                            fieldID = Convert.ToInt64(Request.QueryString["CustomFieldId"]);

                        for (int i = 0; i < ddlContractType.Items.Count; i++)
                        {
                            if (ddlContractType.Items[i].Selected)
                            {
                                contractTypeID = Convert.ToInt64(ddlContractType.Items[i].Value);

                                if (ContractMastersManagement.ExistsCustomField(Convert.ToInt32(CustomerID), contractTypeID, tbxLableName.Text.Trim(), fieldID))
                                {
                                    divsuccessmsgaCcustomtype.Visible = false;
                                    cvCustomField.IsValid = false;
                                    cvCustomField.ErrorMessage = "Field with Same Name Already exists for Contract Type - " + ddlContractType.Items[i].Text;

                                    i = ddlContractType.Items.Count;
                                    return;
                                }
                                else
                                    lstSelectContractTypes.Add(contractTypeID);
                            }
                        }

                        if (lstSelectContractTypes.Count > 0)
                            validationSuccess = true;
                    }
                    else
                    {
                        divsuccessmsgaCcustomtype.Visible = false;
                        cvCustomField.IsValid = false;
                        cvCustomField.ErrorMessage = "Please Enter Label Name";
                    }
                    #endregion

                    #region SAVE

                    if (validationSuccess)
                    {
                        if (lstSelectContractTypes.Count > 0)
                        {
                            Cont_tbl_CustomField NewFieldAdd = new Cont_tbl_CustomField()
                            {
                                Label = tbxLableName.Text,
                                IsActive = true,
                                CreatedBy = AuthenticationHelper.UserID,
                                CreatedOn = DateTime.Now,
                                CustomerID = Convert.ToInt32(CustomerID),
                            };

                            foreach (var eachTypeID in lstSelectContractTypes)
                            {
                                NewFieldAdd.TypeID = eachTypeID;

                                if (Convert.ToString(ViewState["Mode"]) == "0")
                                {
                                    long newCustomFieldID = 0;
                                    newCustomFieldID = ContractMastersManagement.CreateCustomFieldDetails(NewFieldAdd);

                                    if (newCustomFieldID > 0)
                                    {
                                        //cvCustomField.IsValid = false;
                                        //cvCustomField.ErrorMessage = "Custom Field Save Successfully";
                                        //vsCustomField.CssClass = "alert alert-success";
                                        if (cvCustomField.IsValid)
                                        {
                                            divsuccessmsgaCcustomtype.Visible = true;
                                            successmsgaCcustomtype.Text = "Custom Field Saved Successfully.";
                                            ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "script", "window.setTimeout(function() { document.getElementById('" + divsuccessmsgaCcustomtype.ClientID + "').style.display = 'none' },2000);", true);
                                        }
                                        saveSuccess = true;
                                        ViewState["Mode"] = "0";
                                        Session["custFieldID"] = newCustomFieldID;
                                    }
                                }
                                else if (Convert.ToString(ViewState["Mode"]) == "1")
                                {
                                    if (!string.IsNullOrEmpty(Request.QueryString["CustomFieldId"]))
                                    {
                                        NewFieldAdd.ID = Convert.ToInt64(Request.QueryString["CustomFieldId"]);
                                        NewFieldAdd.UpdatedBy = AuthenticationHelper.UserID;

                                        saveSuccess = ContractMastersManagement.UpdateCustomFieldDetails(NewFieldAdd);

                                        if (saveSuccess)
                                        {
                                            //cvCustomField.IsValid = false;
                                            //cvCustomField.ErrorMessage = "Custom Field Updated Successfully.";
                                            //vsCustomField.CssClass = "alert alert-success";
                                            if (cvCustomField.IsValid)
                                            {
                                                divsuccessmsgaCcustomtype.Visible = true;
                                                successmsgaCcustomtype.Text = "Custom Field Updated Successfully.";

                                                ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "script", "window.setTimeout(function() { document.getElementById('" + divsuccessmsgaCcustomtype.ClientID + "').style.display = 'none' },2000);", true);
                                            }
                                            ViewState["Mode"] = "0";
                                        }
                                    }
                                }
                            }
                            if (saveSuccess)
                            {
                                for (int i = 0; i < ddlContractType.Items.Count; i++)
                                {
                                    if (ddlContractType.Items[i].Selected)
                                    {
                                        ddlContractType.Items[i].Selected = false;
                                    }
                                }
                                tbxLableName.Text = string.Empty;
                            }
                        }
                    }

                    #endregion
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                divsuccessmsgaCcustomtype.Visible = false;
                cvCustomField.IsValid = false;
                cvCustomField.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        [WebMethod]
        public static string getCustomFieldID()
        {
            string subtypeID = string.Empty;
            try
            {
                subtypeID = ContractMastersManagement.GetCustomFieldID();
                return subtypeID;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return subtypeID;
            }
        }

    }
}