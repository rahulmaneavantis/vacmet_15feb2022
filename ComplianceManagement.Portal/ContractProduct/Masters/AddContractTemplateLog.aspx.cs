﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataContract;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;

namespace com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.Masters
{
    public partial class AddContractTemplateLog : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                divsuccessmsgaCTem.Visible = false;
                if (!IsPostBack)
                {
                    int customerID = -1;
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                    long templateID = 0;

                    if (!string.IsNullOrEmpty(Request.QueryString["accessID"]))
                    {
                        templateID = Convert.ToInt64(Request.QueryString["accessID"]);
                        BindGrid(templateID);
                        BindGridLog(templateID);
                    }
                    else
                    {
                        ViewState["Mode"] = 0;
                        templateID = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvAddEditTemplate.IsValid = false;
                cvAddEditTemplate.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        public static bool CreateUpdate_LogMapping(Cont_tbl_TemplateMaster_log logMapping)
        {
            using (ContractMgmtEntities entities = new ContractMgmtEntities())
            {
                bool saveSuccess = false;
                try
                {
                    var contdetails = (from row in entities.Cont_tbl_TemplateMaster_log
                                       where row.TemplateID == logMapping.TemplateID
                                       && row.ApproverCount == logMapping.ApproverCount
                                       && row.CustomerID == AuthenticationHelper.CustomerID
                                       select row).FirstOrDefault();

                    if (contdetails == null)
                    {
                        entities.Cont_tbl_TemplateMaster_log.Add(logMapping);
                        saveSuccess = true;
                        entities.SaveChanges();
                    }
                    return saveSuccess;
                }
                catch (Exception ex)
                {
                    ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    return false;
                }
            }
        }
        protected void btnSaveApproverCount_Click(object sender, EventArgs e)
        {
            try
            {
                long templateID = 0;
                if (!string.IsNullOrEmpty(Request.QueryString["accessID"]))
                {
                    templateID = Convert.ToInt64(Request.QueryString["accessID"]);
                }
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    if (grdTemplateSection != null)
                    {
                        var Approvarlevelcount = Convert.ToInt32(tbxApprover.Text);
                        bool sucess = false;
                        try
                        {                                                    
                            var tempdetails = (from row in entities.Cont_tbl_TemplateMaster
                                               where row.ID == templateID
                                               && row.CustomerID == AuthenticationHelper.CustomerID
                                               select row).FirstOrDefault();

                            if (tempdetails != null)
                            {
                                Cont_tbl_TemplateMaster_log _objlog = new Cont_tbl_TemplateMaster_log()
                                {

                                    CustomerID = (int)AuthenticationHelper.CustomerID,
                                    TemplateID = (int)templateID,
                                    Version = tempdetails.Version,
                                    CreatedBy = AuthenticationHelper.UserID,
                                    CreatedOn = DateTime.Now,
                                    UpdatedBy = tempdetails.UpdatedBy,
                                    UpdatedOn = tempdetails.UpdatedOn,
                                    ApproverCount = tempdetails.ApproverCount,
                                };

                                CreateUpdate_LogMapping(_objlog);
                                tempdetails.ApproverCount = Approvarlevelcount;
                                entities.SaveChanges();
                            }
                            sucess = true;
                        }
                        catch (Exception ex)
                        {
                            cvAddEditTemplate.IsValid = false;
                            cvAddEditTemplate.ErrorMessage = "Something went wrong, Please try again";
                            LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                        }


                        try
                        {
                            for (int i = 0; i < grdTemplateSection.Rows.Count; i++)
                            {
                                if (grdTemplateSection.Rows[i].RowType == DataControlRowType.DataRow)
                                {
                                    Label lblTemplateID = (Label)grdTemplateSection.Rows[i].FindControl("lblTemplateID");
                                    Label lblContractID = (Label)grdTemplateSection.Rows[i].FindControl("lblContractID");
                                    Label lblContractStatusID = (Label)grdTemplateSection.Rows[i].FindControl("lblContractStatusID");
                                    List<long> ApprovedStatus = new List<long>();
                                    ApprovedStatus.Add(4);//pending Approval
                                    ApprovedStatus.Add(5);//Approval Completed
                                    ApprovedStatus.Add(7);//Active
                                    ApprovedStatus.Add(15);//Rejected
                                    if (lblContractStatusID != null && lblTemplateID != null && lblContractID != null)
                                    {
                                        var contratcstatusid = Convert.ToInt32(lblContractStatusID.Text);
                                        var templateid = Convert.ToInt32(lblTemplateID.Text);
                                        var ContractID = Convert.ToInt32(lblContractID.Text);

                                        if (!ApprovedStatus.Contains(contratcstatusid))
                                        {

                                            var contdetails = (from row in entities.Cont_tbl_ContractInstance
                                                               where row.ID == ContractID
                                                               && row.CustomerID == AuthenticationHelper.CustomerID
                                                               select row).FirstOrDefault();

                                            if (contdetails != null)
                                            {
                                                contdetails.ApproverCount = Approvarlevelcount;
                                                entities.SaveChanges();
                                                sucess = true;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            cvAddEditTemplate.IsValid = false;
                            cvAddEditTemplate.ErrorMessage = "Something went wrong, Please try again";
                            LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                        }
                       

                        if (sucess==true)
                        {
                            cvAddEditTemplate.IsValid = false;
                            cvAddEditTemplate.ErrorMessage = "Saved Sucessfully";
                        }
                        BindGridLog(templateID);
                    }
                }
            }
            catch (Exception ex)
            {
                cvAddEditTemplate.IsValid = false;
                cvAddEditTemplate.ErrorMessage = "Something went wrong, Please try again";
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void lnkBtnRebind_Sections_Click(object sender, EventArgs e)
        {
          
        }
        public static List<Cont_SP_TemplateLog_Result> GetDetailsLog_All(long templateID)
        {
            using (ContractMgmtEntities entities = new ContractMgmtEntities())
            {
                var lstSections = entities.Cont_SP_TemplateLog(templateID).ToList();
                return lstSections;
            }
        }
        public static List<Cont_SP_TemplateMappedContract_Result> GetDetails_All(long templateID)
        {
            using (ContractMgmtEntities entities = new ContractMgmtEntities())
            {
                var lstSections = entities.Cont_SP_TemplateMappedContract(templateID).ToList();
                return lstSections;
            }
        }
        public void BindGridLog(long templateID)
        {
            try
            {             
                grdLog.DataSource = GetDetailsLog_All(templateID);
                grdLog.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void BindGrid(long templateID)
        {
            try
            {                         
                grdTemplateSection.DataSource = GetDetails_All(templateID);
                grdTemplateSection.DataBind();              
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}