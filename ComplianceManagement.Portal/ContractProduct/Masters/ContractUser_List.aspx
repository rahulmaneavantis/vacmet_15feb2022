﻿<%@ Page Title="User Master:: Contract" Language="C#" MasterPageFile="~/ContractProduct.Master" AutoEventWireup="true" CodeBehind="ContractUser_List.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.Masters.ContractUser_List" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>

<asp:Content ID="Content3" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    </style>

    <script type="text/javascript">

        $(document).ready(function () {
            $("#ContentPlaceHolder1_udcInputForm_tbxBranch").unbind('click');
            $("#ContentPlaceHolder1_udcInputForm_tbxBranch").click(function () {
                $("#divBranches").toggle("blind", null, 500, function () { });
            });
        });

        function txtclick() {
            $("#divBranches").toggle("blind", null, 500, function () { });
        }
        $('#tbxFirstName').mouseenter(function () {

            alert('empty');
            //process further

        });

        $(document).ready(function () {
            setactivemenu('leftmastermenu');
            fhead('Masters / User');
        });

        function openModalControl(e) {
            
            if (e == 1) {
                document.getElementById('ContentPlaceHolder1_lblName1').innerHTML = "Add New User";
            }
            else {
                document.getElementById('ContentPlaceHolder1_lblName1').innerHTML = "Edit User Detail(s)";
            }
            $('#divUserDialog').modal('show');
            return true;
        }

        function ClosePopUser() {
            $('#divUserDialog').modal('hide');
            document.getElementById('<%= lnkBtn_RebindGrid.ClientID %>').click();
        }

    </script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row Dashboard-white-widget">
        <div class="dashboard">
            <div class="col-lg-12 col-md-12">
                <div style="margin: 5px">

                    <asp:UpdatePanel ID="upUserList" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div style="margin-bottom: 4px">
                                <asp:ValidationSummary ID="ValidationSummary2" runat="server" Display="none"
                                    class="alert alert-block alert-danger fade in" ValidationGroup="UserPageValidationGroup" />
                                <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                    ValidationGroup="UserPageValidationGroup" Display="None" />
                            </div>

                            <div class="col-md-12 colpadding0">
                                <div class="col-md-6 colpadding0">
                                    <asp:TextBox runat="server" ID="tbxFilter" AutoComplete="off" CssClass="form-control" MaxLength="50" Width="70%"
                                        PlaceHolder="Type to Search User(e.g. Name/ Email/ Contact No" AutoPostBack="true" OnTextChanged="tbxFilter_TextChanged" />
                                </div>

                                <div class="col-md-4 colpadding0">
                                    <asp:LinkButton ID="lnkBtn_RebindGrid" OnClick="lnkBtn_RebindGrid_Click" Style="display: none;" runat="server"></asp:LinkButton>
                                    <asp:DropDownList runat="server" ID="ddlCustomerPage" class="form-control m-bot15" Width="90%"
                                        AutoPostBack="true" OnSelectedIndexChanged="ddlCustomerPage_SelectedIndexChanged" Visible="false" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ErrorMessage="Please Select Customer."
                                        ControlToValidate="ddlCustomerPage" runat="server" ValidationGroup="UserPageValidationGroup"
                                        Display="None" />
                                </div>

                                <div class="col-md-2 colpadding0 text-right">
                                    <asp:LinkButton Text="Apply" CssClass="btn btn-primary" OnClick="lnkApply_Click" Style="margin-left: 3%;"
                                        runat="server" ID="lnkApply" Visible="false" />

                                    <asp:LinkButton runat="server" ID="btnAddUser" OnClientClick="openModalControl(1);" OnClick="btnAddUser_Click"
                                        data-toggle="tooltip" data-placement="bottom" ToolTip="Add New User" CssClass="btn btn-primary">
                                        <span class="AddNewspan1"><i class='fa fa-plus'></i></span>&nbsp;New</asp:LinkButton>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <div class="row">
                                <div class="panel col-md-12 colpadding0" style="overflow: hidden;">
                                    <asp:GridView runat="server" ID="grdUser" AutoGenerateColumns="false" AllowPaging="true" AutoPostBack="true" AllowSorting="true"
                                        CssClass="table" GridLines="none" PageSize="10" OnSorting="grdUser_Sorting"
                                        OnRowDataBound="grdUser_RowDataBound" Width="100%" ShowHeaderWhenEmpty="true"
                                        DataKeyNames="ID" OnRowCommand="grdUser_RowCommand" OnRowCreated="grdUser_RowCreated">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Sr.No." HeaderStyle-Width="5%" ItemStyle-Width="5%">
                                                <ItemTemplate>
                                                    <%#Container.DataItemIndex+1 %>
                                                    <%--<asp:Label ID="lblRowID" runat="server" Text='<%# Eval("RowID") %>'></asp:Label>--%>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                           <%-- <asp:BoundField DataField="FirstName" SortExpression="FirstName" HeaderText="First Name" ItemStyle-Height="20px" HeaderStyle-Height="20px" HeaderStyle-Width="15%"
                                                ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" />--%>

                                            <asp:TemplateField HeaderText="First Name" SortExpression="FirstName" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                        <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("FirstName") %>' ToolTip='<%# Eval("FirstName") %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Last Name" SortExpression="LastName" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                        <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("LastName") %>' ToolTip='<%# Eval("LastName") %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                           <%-- <asp:BoundField DataField="LastName" SortExpression="LastName" HeaderText="Last Name" HeaderStyle-Width="15%" ItemStyle-Width="15%" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Wrap="false"
                                                ItemStyle-HorizontalAlign="Left" ItemStyle-Wrap="false" />--%>
                                            <%--SortExpression="LastName"--%>

                                            <asp:TemplateField HeaderText="Email" SortExpression="Email" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                        <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Email") %>' ToolTip='<%# Eval("Email") %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                              <asp:TemplateField HeaderText="Contact No" SortExpression="ContactNumber" HeaderStyle-Width="10%" ItemStyle-Width="10%">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                        <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ContactNumber") %>' ToolTip='<%# Eval("ContactNumber") %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                             <asp:TemplateField HeaderText="Contact Role" SortExpression="Role" HeaderStyle-Width="10%" ItemStyle-Width="10%">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                        <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Role") %>' ToolTip='<%# Eval("Role") %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%--<asp:BoundField DataField="ContactNumber" SortExpression="ContactNumber" HeaderText="Contact" HeaderStyle-Width="10%" ItemStyle-Width="10%" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" />--%>

                                            <%--<asp:BoundField DataField="Role" SortExpression="Role" HeaderText="Contract Role" HeaderStyle-Width="10%" ItemStyle-Width="10%" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" />--%>

                                            <asp:TemplateField HeaderText="Status" SortExpression="IsActive" HeaderStyle-Width="10%" ItemStyle-Width="10%" HeaderStyle-CssClass="text-right" ItemStyle-CssClass="text-right">
                                                <ItemTemplate>
                                                    <asp:LinkButton runat="server" CommandName="CHANGE_STATUS" CommandArgument='<%# Eval("ID") %>' ID="lbtnChangeStatus"
                                                        ToolTip="Toggle User Status" data-toggle="tooltip" data-placement="bottom" OnClientClick="return confirm('Are you certain you want to change the status of this User?');"><%# (Convert.ToBoolean(Eval("IsActive"))) ? "Active" : "Disabled" %></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Action" HeaderStyle-Width="5%" ItemStyle-Width="5%" HeaderStyle-CssClass="text-right" ItemStyle-CssClass="text-right">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                        <asp:LinkButton runat="server" ToolTip="Edit" data-toggle="tooltip" data-placement="bottom"
                                                            CommandName="EDIT_USER" OnClientClick="openModalControl(0)" ID="lbtnEdit" CommandArgument='<%# Eval("ID") %>'>
                                                        <img src='<%# ResolveUrl("~/Images/edit_icon_new.png")%>' alt="Edit User" />
                                                        </asp:LinkButton>

                                                        <asp:LinkButton runat="server" CommandName="DELETE_USER" ToolTip="Delete" data-toggle="tooltip" data-placement="bottom"
                                                            CommandArgument='<%# Eval("ID") %>' ID="lbtnDelete" OnClientClick="return confirm('Are you certain you want to delete this User?');" Visible="false">
                                                        <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="Delete User" />
                                                        </asp:LinkButton>

                                                        <asp:LinkButton runat="server" CommandName="RESET_PASSWORD" ID="lbtnReset" CommandArgument='<%# Eval("ID") %>'
                                                            OnClientClick="return confirm('Are you certain you want to reset password for this user?');"
                                                            ToolTip="Reset Password" data-toggle="tooltip" data-placement="bottom">
                                                        <img src='<%# ResolveUrl("~/Images/reset_password_new.png")%>' alt="Reset" />
                                                        </asp:LinkButton>
                                                        <asp:LinkButton runat="server" CommandName="UNLOCK_USER" ToolTip="Unlock User Account" data-toggle="tooltip" data-placement="bottom"
                                                            CommandArgument='<%# Eval("ID") %>' Visible='<%# IsLocked((string)Eval("Email")) %>'>
                                                        <img src='<%# ResolveUrl("~/Images/permissions_icon.png")%>' alt="Unblock" />
                                                        </asp:LinkButton>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                        </Columns>
                                        <RowStyle CssClass="clsROWgrid" />
                                        <HeaderStyle CssClass="clsheadergrid" />
                                        <PagerSettings Visible="false" />
                                        <EmptyDataTemplate>
                                            No Record Found
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 colpadding0">
                                    <div class="col-md-10 colpadding0">
                                        <div runat="server" id="DivRecordsScrum" style="float: left; margin-top: 5px; color: #999">
                                            <p style="padding-right: 0px !Important;">
                                                <%--<asp:Label ID="Label4" runat="server" Text="Showing "></asp:Label>--%>
                                                <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>- 
                                    <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                                    <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-md-1 colpadding0">
                                        <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 100%; float: right;margin-right: 6%;"
                                            AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                                            <asp:ListItem Text="5" />
                                            <asp:ListItem Text="10" Selected="True" />
                                            <asp:ListItem Text="20" />
                                            <asp:ListItem Text="50" />
                                        </asp:DropDownList>
                                    </div>

                                    <div class="col-md-1 colpadding0" style="float: right;">
                                        <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true" DataPlaceHolder="No" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                            OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged" class="form-control m-bot15" Width="100%" Height="35px">
                                        </asp:DropDownListChosen>

                                    </div>
                                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                </div>
                            </div>
                            <div class="col-md-12 colpadding0">
                                <div class="col-md-6 colpadding0">
                                    <div class="col-md-2 colpadding0">
                                        <asp:LinkButton Text="Previous" CssClass="btn btn-primary" Visible="false" Width="100%" OnClick="lnkPreviousSwitch_Click" ToolTip="Go to Previous Master (Legal Entity)" data-toggle="tooltip" runat="server" ID="LnkbtnPrevious" />
                                    </div>
                                    <div class="col-md-10 colpadding0">
                                    </div>
                                </div>
                                <div class="col-md-6 colpadding0">
                                    <div class="col-md-10 colpadding0">
                                    </div>
                                    <div class="col-md-2 colpadding0">
                                        <asp:LinkButton Text="Next" CssClass="btn btn-primary" runat="server" OnClick="lnkNextSwitch_Click" ToolTip="Go to Next Master (Vendor)" data-toggle="tooltip" ID="LnkbtnNext" Style="float: right; width: 100%;" />
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                </div>
            </div>
        </div>
    </div>

    <%-- Modal Pop-Up  --%>
    <div class="modal fade" id="divUserDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin-left: 14%;">
        <div class="modal-dialog" style="width: 100%;">
            <div class="modal-content" style="width: 85%; height: auto;">
                <div class="modal-header">
                    &nbsp;
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label runat="server" id="lblName1" style="width: 180px; display: block; float: left; font-size: 20px; color: #1fd9e1;">
                        </label>  <%--Add/Edit User--%>
                       <%--<label id="lblname" runat="server" class="modal-header-custom col-md-6 plr0"> </label>--%>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <%--<vit:audituserdetailscontrol runat="server" id="udcInputForm" />--%>
                    <asp:UpdatePanel ID="upUsersPopup" runat="server" UpdateMode="Conditional" OnLoad="upUsersPopup_Load">
                        <ContentTemplate>
                            <div>
                                <div class="row col-md-12">
                                    <div class="col-md-12 alert alert-block alert-success fade in" runat="server" visible="false" id="divsuccessmsgCusrlst">
                                        <asp:Label runat="server" ID="successmsgCusrlst"></asp:Label>
                                    </div>
                                    <asp:ValidationSummary ID="vsUserPopup" runat="server" class="alert alert-block alert-danger fade in" ValidationGroup="UserPopupValidationGroup" />

                                    <asp:CustomValidator ID="cvUserPopup" runat="server" EnableClientScript="False"
                                        ValidationGroup="UserPopupValidationGroup" Display="None" />

                                </div>

                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 19%; display: block; float: left; font-size: 13px; color: #333;">
                                            First Name</label>
                                        <asp:TextBox runat="server" ID="tbxFirstName" Style="width: 70%;" CssClass="form-control" MaxLength="100" autocomplete="off" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Please Enter First Name."
                                            ControlToValidate="tbxFirstName" runat="server" ValidationGroup="UserPopupValidationGroup"
                                            Display="None" />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="None" runat="server"
                                            ValidationGroup="UserPopupValidationGroup" ErrorMessage="Please enter a valid first name."
                                            ControlToValidate="tbxFirstName" ValidationExpression="^[a-zA-Z_]+[a-zA-Z0-9_ .]*$"></asp:RegularExpressionValidator>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 19%; display: block; float: left; font-size: 13px; color: #333;">
                                            Last Name</label>
                                        <asp:TextBox runat="server" ID="tbxLastName" Style="width: 70%;" CssClass="form-control" MaxLength="100" autocomplete="off" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Please Enter Last Name."
                                            ControlToValidate="tbxLastName" runat="server" ValidationGroup="UserPopupValidationGroup"
                                            Display="None" />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" Display="None" runat="server"
                                            ValidationGroup="UserPopupValidationGroup" ErrorMessage="Please enter a valid last name."
                                            ControlToValidate="tbxLastName" ValidationExpression="^[a-zA-Z_]+[a-zA-Z0-9_ .]*$"></asp:RegularExpressionValidator>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 19%; display: block; float: left; font-size: 13px; color: #333;">
                                            Designation</label>
                                        <asp:TextBox runat="server" ID="tbxDesignation" Style="width: 70%;" CssClass="form-control" MaxLength="50" autocomplete="off" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="Please Enter Designation."
                                            ControlToValidate="tbxDesignation" runat="server" ValidationGroup="UserPopupValidationGroup"
                                            Display="None" />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" Display="None" runat="server"
                                            ValidationGroup="UserPopupValidationGroup" ErrorMessage="Please enter a valid Designation."
                                            ControlToValidate="tbxDesignation" ValidationExpression="^[a-zA-Z_]+[a-zA-Z0-9_ .]*$"></asp:RegularExpressionValidator>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 19%; display: block; float: left; font-size: 13px; color: #333;">
                                            Email</label>
                                        <asp:TextBox runat="server" ID="tbxEmail" Style="width: 70%;" MaxLength="200" CssClass="form-control" autocomplete="off" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ErrorMessage="Please Enter Email-ID."
                                            ControlToValidate="tbxEmail" runat="server" ValidationGroup="UserPopupValidationGroup"
                                            Display="None" />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" Display="None" runat="server"
                                            ValidationGroup="UserPopupValidationGroup" ErrorMessage="Please enter a valid email-ID."
                                            ControlToValidate="tbxEmail" ValidationExpression="^([\w-_]+\.)*[\w-_]+@([\w-_]+\.)*[\w-_]+\.[\w-_]+$"></asp:RegularExpressionValidator>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 19%; display: block; float: left; font-size: 13px; color: #333;">
                                            Contact No</label>
                                        <asp:TextBox runat="server" ID="tbxContactNo" Style="width: 70%;" CssClass="form-control" MaxLength="10" autocomplete="off" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ErrorMessage="Please Enter Contact Number."
                                            ControlToValidate="tbxContactNo" runat="server" ValidationGroup="UserPopupValidationGroup"
                                            Display="None" />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator5" Display="None" runat="server"
                                            ValidationGroup="UserPopupValidationGroup" ErrorMessage="Please Enter valid contact number, Use number only"
                                            ControlToValidate="tbxContactNo" ValidationExpression="^[\+\d]+(?:[\d-.\s()]*)$"></asp:RegularExpressionValidator>
                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers" TargetControlID="tbxContactNo" />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator6" Display="None" runat="server"
                                            ValidationGroup="UserPopupValidationGroup" ErrorMessage="Please Enter valid  Contact-No, can be of 10 digits only"
                                            ControlToValidate="tbxContactNo" ValidationExpression="[0-9]{10}"></asp:RegularExpressionValidator>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <div runat="server" id="divContractRole">
                                            <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                            <label style="width: 19%; display: block; float: left; font-size: 13px; color: #333;">
                                                Role (Contract)</label>
                                            <asp:DropDownList runat="server" ID="ddlContractRole" Style="padding: 0px; margin: 0px; width: 70%;"
                                                CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlContractRole_SelectedIndexChanged" />
                                            <asp:CompareValidator ID="CompareValidator1" ErrorMessage="Please Select User Role." ControlToValidate="ddlContractRole"
                                                runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="UserPopupValidationGroup"
                                                Display="None" />
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 19%; display: block; float: left; font-size: 13px; color: #333;">
                                            Department</label>
                                        <asp:DropDownList runat="server" ID="ddlDepartment" Width="100%" Style="padding: 0px; margin: 0px; width: 70%;" CssClass="form-control m-bot15" />
                                        <asp:CompareValidator ID="CVDept" ErrorMessage="Please Select Department." ControlToValidate="ddlDepartment"
                                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="UserPopupValidationGroup" Display="None" />
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 19%; display: block; float: left; font-size: 13px; color: #333;">
                                            Address</label>
                                        <asp:TextBox runat="server" ID="tbxAddress" Style="width: 70%;" CssClass="form-control m-bot15" MaxLength="500"
                                            TextMode="MultiLine" autocomplete="off" />
                                    </div>
                                </div>

                                <div id="div1" style="margin-bottom: 7px; display: none;">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                        User Type</label>
                                    <asp:RadioButtonList ID="rbCheckType" runat="server" RepeatDirection="Horizontal"
                                        Style="padding: 0px; margin: 0px; width: 390px;">
                                        <asp:ListItem class="radio-inline" Text="Internal" Value="Internal" Selected="True"></asp:ListItem>
                                        <asp:ListItem class="radio-inline" Text="External" Value="External"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>

                                <div style="display: none;">
                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                    <label style="width: 19%; display: block; float: left; font-size: 13px; color: #333;">
                                        Department Head</label>
                                    <asp:CheckBox runat="server" ID="chkHead" Style="padding: 0px; margin: 0px; height: 30px; width: 70%; color: #333;" />
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 19%; display: block; float: left; font-size: 13px; color: #333;">
                                            Profile Picture</label>
                                        <asp:FileUpload ID="UserImageUpload" runat="server" ForeColor="#333" />
                                        <asp:Button ID="btnUpload" runat="server" Text="Upload" OnClick="Upload" Visible="false" />
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 19%; display: block; float: left; font-size: 13px; color: #333;">
                                            &nbsp;
                                        </label>
                                        <asp:Image ID="ImageShow" runat="server" Height="100" Width="100" ImageUrl="~/UserPhotos/DefaultImage.png" Visible="false" />
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <div runat="server" id="divCustomer">
                                            <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                            <label style="width: 19%; display: block; float: left; font-size: 13px; color: #333;">
                                                Customer</label>
                                            <asp:DropDownList runat="server" ID="ddlCustomerPopup" Style="padding: 0px; margin: 0px; width: 70%;"
                                                CssClass="form-control m-bot15" AutoPostBack="true" OnSelectedIndexChanged="ddlCustomerPopup_SelectedIndexChanged" />
                                            <asp:CompareValidator ID="CompareValidator2" ErrorMessage="Please Select Customer."
                                                ControlToValidate="ddlCustomerPopup" runat="server" ValueToCompare="-1" Operator="NotEqual"
                                                ValidationGroup="UserPopupValidationGroup" Display="None" />
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <div runat="server" id="divReportingTo">
                                            <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                            <label style="width: 19%; display: block; float: left; font-size: 13px; color: #333;">
                                                Reporting to</label>
                                            <asp:DropDownList runat="server" ID="ddlReportingTo" Style="padding: 0px; margin: 0px; width: 70%;"
                                                CssClass="form-control m-bot15" />
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <div runat="server" id="divCustomerBranch">
                                            <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                            <label style="width: 19%; display: block; float: left; font-size: 13px; color: #333;">
                                                Location</label>
                                            <asp:TextBox runat="server" ID="tbxBranch" onclick="txtclick()" Style="padding: 5px; margin: 0px; width: 70%;" CausesValidation="true"
                                                CssClass="form-control" autocomplete="off" />
                                            <div style="margin-left: 17%; position: absolute; z-index: 10" id="divBranches">
                                                <asp:TreeView runat="server" ID="tvBranches" BackColor="White" BorderColor="Black"
                                                    BorderWidth="1" SelectedNodeStyle-Font-Bold="true" Height="100px" Width="100%"
                                                    Style="overflow: auto; margin-left: 11%;" ShowLines="true" OnSelectedNodeChanged="tvBranches_SelectedNodeChanged">
                                                </asp:TreeView>
                                            </div>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ErrorMessage="Please Select Location."
                                                ControlToValidate="tbxBranch" runat="server" ValidationGroup="UserPopupValidationGroup" InitialValue="Select Location"
                                                Display="None" />
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                    </div>
                                </div>

                                <asp:Repeater runat="server" ID="repParameters">
                                    <ItemTemplate>
                                        <div style="margin-bottom: 7px">
                                            <asp:Label runat="server" ID="lblName" Style="width: 150px; display: block; float: left; font-size: 13px; color: #333;"
                                                Text='<%# Eval("Name")  + ":"%>' />
                                            <asp:TextBox runat="server" ID="tbxValue" Style="height: 20px; width: 390px;" Text='<%# Eval("Value") %>'
                                                MaxLength='<%# Eval ("Length") %>' />
                                            <asp:HiddenField runat="server" ID="hdnID" Value='<%# Eval("ValueID") %>' />
                                            <asp:HiddenField runat="server" ID="hdnEntityParameterID" Value='<%# Eval("ParameterID") %>' />
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>

                                <div class="clearfix"></div>

                                <div style="margin-bottom: 7px; margin-top: 10px; text-align: center;">
                                    <%--float: right; margin-right: 257px; --%>;
                                    <asp:Button runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="btn btn-primary"
                                        ValidationGroup="UserPopupValidationGroup" />
                                    <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="btn btn-primary" data-dismiss="modal" OnClientClick="ClosePopUser();" />
                                </div>

                                <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 25px;">
                                    <p style="color: red;"><strong>Note:</strong> (*) fields are compulsory</p>
                                </div>
                                <div class="clearfix" style="height: 50px"></div>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <%--<asp:PostBackTrigger ControlID="btnSave" />--%>
                            <%--<asp:AsyncPostBackTrigger ControlID="btnUpload" EventName="Click" />--%>
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
