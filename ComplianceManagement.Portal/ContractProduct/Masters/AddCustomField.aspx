﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddCustomField.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.Masters.AddCustomFieldValue" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="asp" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
  
    <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />

    <!-- bootstrap theme -->
    <link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />

    <!-- font icon -->
    <link href="~/NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <!--external css-->
    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <%--<link href="~/NewCSS/jquery-ui.css" rel="stylesheet" type="text/css" />--%>

    <%--<script src="https://code.jquery.com/jquery-1.11.3.js"></script>--%>
    <script type="text/javascript" src="/Newjs/jquery.js"></script>
    <script type="text/javascript" src="/Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="/Newjs/bootstrap.min.js"></script>
    <%--<script type="text/javascript" src="../../Newjs/jquery-1.8.3.min.js"></script>--%>

    <!-- nice scroll -->
    <script type="text/javascript" src="/Newjs/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="/Newjs/jquery.nicescroll.js"></script>

    <link href="~/NewCSS/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="/Newjs/bootstrap-multiselect.js" ></script>
    <link href="~/NewCSS/contract_custom_style.css" rel="stylesheet" />

    <script type="text/javascript">

        $(document).ready(function () {
            bindMultiSelect();
        });

        function bindMultiSelect() {
            $('[id*=ddlContractType]').multiselect({
                includeSelectAllOption: true,
                numberDisplayed: 1,
                buttonWidth: '100%'
            });
        }

        function CloseMe() {
            window.parent.CloseCustomFieldPopup();
        }

        function RefreshParent() {
            window.parent.location.href = window.parent.location.href;
        }     
    </script>
</head>
<body style="background-color: white">
    <form id="form1" runat="server">
        <div>
            <asp:ScriptManager ID="ContAddCustomField" runat="server"></asp:ScriptManager>

            <asp:UpdatePanel ID="upContCustomField" style="margin-top: 10px;" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div>
                        <div class="row">
                             <div class="col-md-12 alert alert-block alert-success fade in" runat="server" visible="false" id="divsuccessmsgaCcustomtype">
                            <asp:Label runat="server" ID="successmsgaCcustomtype" ></asp:Label>
                        </div>
                            <asp:ValidationSummary ID="vsCustomField" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                ValidationGroup="CustomFieldValidationGroup" />
                            <asp:CustomValidator ID="cvCustomField" runat="server" EnableClientScript="False"
                                ValidationGroup="CustomFieldValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                        </div>

                        <div class="row">
                            <div class="form-group required col-md-12">                                
                                    <label for="ddlContractType" class="control-label">Contract Type</label>
                                    <asp:ListBox ID="ddlContractType" CssClass="form-control" runat="server" SelectionMode="Multiple" Width="100%"></asp:ListBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Please Select Contract Type" ControlToValidate="ddlContractType"
                                        runat="server"  Display="None" ValidationGroup="CustomFieldValidationGroup"/>                               
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group required col-md-12">
                                <label for="tbxLableName" class="control-label">Field (Display Name)</label>
                                <asp:TextBox runat="server" ID="tbxLableName" CssClass="form-control" autocomplete="off" />
                                <asp:RequiredFieldValidator ID="rfvLableName" ErrorMessage="Please Enter Field Label Name" ControlToValidate="tbxLableName"
                                    runat="server"  Display="None" ValidationGroup="CustomFieldValidationGroup" />
                            </div>
                        </div>

                        <div class="row form-group text-center">
                            <div class="col-md-12">
                                <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="btn btn-primary"
                                    ValidationGroup="CustomFieldValidationGroup" />
                                <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="btn btn-primary" data-dismiss="modal" OnClientClick="CloseMe();" />
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <p style="color: red;"><strong>Note:</strong> (*) fields are Compulsory</p>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </form>
</body>
</html>