﻿<%@ Page Title="My Documents" Language="C#" MasterPageFile="~/ContractProduct.Master" AutoEventWireup="true" CodeBehind="MyDocumentContract.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.Documents.MyDocumentContract" %>

<asp:Content ID="Content3" ContentPlaceHolderID="head" runat="server">
    <script src="/Newjs/tag-scrolling.js" type="text/javascript"></script>
    <link href="/NewCSS/tag-scrolling.css" rel="stylesheet" />
     
     <style type="text/css">
         .chosen-container-single .chosen-single div b {
             margin-top: 3px;
             display: block;
             width: 100%;
             height: 100%;
             background: url(WebResource.axd?d=vcanw1OGyEXjITnW_Lw6o1Ipyyl_7xqXXsF4l85T_LM075DfB4Sa1X81JR-ERGJWebwjBZ50cFK6Pp0E393ZYqyZGOYHT_-VfdsqCmyDCfaRmJRJ_t0w8jjfIDakqF4U1oofqFWh029b9t4iZjam4LmYmixIQZtujQLO7ryIDAI1&t=637298087674161687) no-repeat 0px 6px;
         }
     </style>
    <script type="text/javascript">
        $(document).on("click", function (event) {
            if (event.target.id == "") {
                var idvid = $(event.target).closest('div');
                if ($(idvid).attr('id') != undefined && $(idvid).attr('id') != '' && $(idvid).attr('id').indexOf('<%= tvFilterLocation.ClientID %>') > -1) {
                    $("#divFilterLocation").show();
                } else {
                    $("#divFilterLocation").hide();
                }<%-- event.target.id != '<%= tbxFilterLocation.ClientID %>'--%>
            } else if (event.target.id != "" && event.target.id.indexOf('<%= tvFilterLocation.ClientID %>') > -1) {
                $("#divFilterLocation").show();
            } else if (event.target.id != '<%= tbxFilterLocation.ClientID %>') {
                $("#divFilterLocation").hide();
            } else if (event.target.id == '<%= tbxFilterLocation.ClientID %>') {
                $('<%= tbxFilterLocation.ClientID %>').unbind('click');

                $('<%= tbxFilterLocation.ClientID %>').click(function () {
                    $("#divFilterLocation").toggle("blind", null, 500, function () { });
                });
            }
         });
        $(document).ready(function () {
            setactivemenu('leftdocumentmenu');
            fhead('My Documents');

            $('input[id*=lstBoxFileTags]').hide();

            //$(".nicescroll-box").niceScroll(".wrap", { cursorcolor: "aquamarine" });
            //$('#divlstFileTags').getNiceScroll();

            $('#AdvanceSearch').on('shown.bs.modal', function () {
                $('#divBranches').hide("blind", null, 100, function () { });
                document.getElementById('<%= tbxFilterLocation.ClientID %>').click();
            });
        });
        function OpenDocSharePopup(FID, CID)
        {
            $('#divDocumentsharePopup').modal('show');
            $('#ContentPlaceHolder1_Iframe_Docshare').attr('src', "../aspxPages/ShareDocInfo.aspx?AccessID=" + FID + "&CID=" + CID);
        }
        function CloseUploadShareDocumentPopup() {
            $('#divDocumentsharePopup').modal('hide');
        }
        function fopendocfileContract(file) {
            $('#divViewDocument').modal('show');
            $('#ContentPlaceHolder1_docViewerLitigation').attr('src', "/docviewer.aspx?docurl=" + file);
        }
        function OpenNoDocSharePopup() {
            alert("Please select at least one Document");
        }
        function ShowViewDocument() {
            $('#divViewDocument').modal('show');
            return true;
        };        
    </script>

    <style>
        .tag .label {
            font-size: 100%;
        }

        /*span input[type=checkbox]:checked {
            color: #007aff;
            border: 1px solid;
            border-color: #007aff;
            background: none;
        }*/

        .label-info, .label-info-selected {
            font-size: 100%;
        }

            .label-info:active, .label-info:focus, .label-info:hover {
                color: #007aff;
                border: 1px solid;
                border-color: #007aff;
                background: 0 0;
            }

             .label-info-selected {
                color: #007aff;
                border: 1px solid;
                border-color: #007aff;
                background: 0 0;
            }
    </style>

  <%--  <style>
        .menu-wrapper {
            position: relative;
            /*max-width: 500px;*/
            height: 100px; /*// hide the scroll bar*/
            margin: 1em auto;
            border: 1px solid black;
            overflow-x: hidden;
            overflow-y: hidden;
        }

        .menu {
            height: 120px;
            /*// hide the scroll bar background: #f3f3f3;*/
            box-sizing: border-box;
            white-space: nowrap;
            overflow-x: auto;
            overflow-y: hidden;
            -webkit-overflow-scrolling: touch;
        }  
        .paddles {
        }

        .paddle {
            position: absolute;
            top: 0;
            bottom: 0;
            width: 3em;
        }

        .left-paddle {
            left: 0;
        }

        .right-paddle {
            right: 0;
        }

        .hidden {
            display: none;
        }
    </style>--%>

   <%-- <script type="text/javascript">
        
        // duration of scroll animation
        var scrollDuration = 300;
        // paddles
        var leftPaddle = document.getElementsByClassName('left-paddle');
        var rightPaddle = document.getElementsByClassName('right-paddle');
        // get items dimensions
        var itemsLength = $('.item').length;
        var itemSize = $('.item').outerWidth(true);
        // get some relevant size for the paddle triggering point
        var paddleMargin = 20;

        // get wrapper width
        var getMenuWrapperSize = function() {
            return $('.menu-wrapper').outerWidth();
        }
        var menuWrapperSize = getMenuWrapperSize();
        // the wrapper is responsive
        $(window).on('resize', function() {
            menuWrapperSize = getMenuWrapperSize();
        });
        // size of the visible part of the menu is equal as the wrapper size 
        var menuVisibleSize = menuWrapperSize;

        // get total width of all menu items
        var getMenuSize = function() {
            return itemsLength * itemSize;
        };
        var menuSize = getMenuSize();
        // get how much of menu is invisible
        var menuInvisibleSize = menuSize - menuWrapperSize;

        // get how much have we scrolled to the left
        var getMenuPosition = function() {
            return $('.menu').scrollLeft();
        };

        // finally, what happens when we are actually scrolling the menu
        $('.menu').on('scroll', function() {

            // get how much of menu is invisible
            menuInvisibleSize = menuSize - menuWrapperSize;
            // get how much have we scrolled so far
            var menuPosition = getMenuPosition();

            var menuEndOffset = menuInvisibleSize - paddleMargin;

            // show & hide the paddles 
            // depending on scroll position
            if (menuPosition <= paddleMargin) {
                $(leftPaddle).addClass('hidden');
                $(rightPaddle).removeClass('hidden');
            } else if (menuPosition < menuEndOffset) {
                // show both paddles in the middle
                $(leftPaddle).removeClass('hidden');
                $(rightPaddle).removeClass('hidden');
            } else if (menuPosition >= menuEndOffset) {
                $(leftPaddle).removeClass('hidden');
                $(rightPaddle).addClass('hidden');
            }
        });

        // scroll to left
        $(rightPaddle).on('click', function () {
            $('.menu').animate({ scrollLeft: menuInvisibleSize }, scrollDuration);
        });

        // scroll to right
        $(leftPaddle).on('click', function () {
            $('.menu').animate({ scrollLeft: '0' }, scrollDuration);
        });
    </script>--%>
    
 <%-- <script>
      $(document).ready(function () {
          var view = $("#tslshow");
          var move = "100px";
          var sliderLimit = -1000

          $("#rightArrow").click(function () {
              
              //var currentPosition = parseInt(view.css("left"));
              ////if (currentPosition >= sliderLimit)
              //view.stop(false, true).animate({ left: "-=" + move }, { duration: 400 });
             
              $('div.divFileTags').animate({ scrollLeft: '+=1000' }, 1500);
              var currentPosition = $('div.divFileTags').scrollLeft();
          });

          $("#leftArrow").click(function () {
              
              //var currentPosition = parseInt(view.css("left"));
              ////if (currentPosition < 0)
              //view.stop(false, true).animate({ left: "+=" + move }, { duration: 400 });

              $('div.divFileTags').animate({ scrollLeft: '-=1000' }, 1500);
              var currentPosition = $('div.divFileTags').scrollLeft();
          });
      });
  </script>--%>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row Dashboard-white-widget">
        <div class="dashboard">

            <div class="col-md-12 colpadding0">
                <asp:ValidationSummary ID="vsContractListPage" runat="server" Display="none" class="alert alert-block alert-danger fade in" style="padding-left:5%"
                    ValidationGroup="ContractListPageValidationGroup" />
                <asp:CustomValidator ID="cvErrorContractListPage" runat="server" EnableClientScript="False"
                    ValidationGroup="ContractListPageValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
            </div>

            <div class="row col-md-12 colpadding0">
                <div class="col-md-10 colpadding0">
                <div id="outerDivFileTags" runat="server" >
                    <div id="leftArrow" class="scroller scroller-left mt5 mb5 col-md-1 colpadding0" style="width: 4%">
                        <span class="arrow-button arrow-button-right">
                            <i class="fa fa-chevron-left color-black"></i>
                        </span>
                    </div>
                    <div id="rightArrow" class="scroller scroller-right mt5 mb5 col-md-1 colpadding0" style="width: 4%">
                        <span runat="server" class="arrow-button arrow-button-left">
                            <i class="fa fa-chevron-right color-black"></i>
                        </span>
                    </div>
                    
                    <div class="divFileTags col-md-10 colpadding0" style="overflow-x: hidden; overflow-y: hidden; width: 92%;">
                        <asp:CheckBoxList ID="lstBoxFileTags" runat="server" RepeatDirection="Horizontal" TextAlign="Left"
                            CssClass="list mt5 mb5" OnSelectedIndexChanged="lstBoxFileTags_SelectedIndexChanged" AutoPostBack="true">                           
                        </asp:CheckBoxList>
                    </div>                    
                </div>
                </div>
                <%--<div class="col-md-2 colpadding0 text-right">
                    <a class="btn btn-advanceSearch" data-toggle="modal" href="#AdvanceSearch">Advance Search</a>
                </div>--%>
            </div>

            <div class="clearfix"></div>

            <div class="col-md-12 colpadding0">
                <div class="col-md-3 colpadding0" style="margin-top: 5px; width: 10%;">
                    <asp:LinkButton ID="lnkBtnBindGrid" OnClick="lnkBtnBindGrid_Click" Visible="false" runat="server">
                    </asp:LinkButton>
                </div>
            </div>

            <div class="clearfix"></div>

          
            <!--advance search starts-->
    
   
              <div class="col-md-12 colpadding0">

             <asp:UpdatePanel ID="upDivLocation" runat="server" UpdateMode="Conditional" OnLoad="upDivLocation_Load">
                 <ContentTemplate>
                     <div class="col-md-4 colpadding0 entrycount" style="margin-top: 5px; width:25%; margin-right:10px;">
                         <asp:TextBox runat="server" ID="tbxFilterLocation" PlaceHolder="Click to Select" autocomplete="off" CssClass="form-control" Width="100%" />
                         <div style="margin-left: 1px; position: absolute; z-index: 10; overflow-y: auto; height: 200px; width: 100%" id="divFilterLocation">
                             <asp:TreeView runat="server" ID="tvFilterLocation" SelectedNodeStyle-Font-Bold="true" NodeStyle-ForeColor="#8e8e93"
                                 Style="margin-top: -20px;height: 199px; overflow: auto;border-top: 1px solid #c7c7cc; border-left: 1px solid #c7c7cc; border-right: 1px solid #c7c7cc; border-bottom: 1px solid #c7c7cc; background-color: #ffffff; color: #8e8e93 !important;"
                                 ShowLines="true"
                                 OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged">
                             </asp:TreeView>
                         </div>
                     </div>
                 </ContentTemplate>
             </asp:UpdatePanel>

                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 25%; margin-right:10px;">
                 
                    <asp:DropDownListChosen runat="server" ID="ddlDeptPage" AllowSingleDeselect="false" DisableSearchThreshold="3"
                          DataPlaceHolder="Select Department"   class="form-control" Width="100%" />
                 </div>

             <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 25%; margin-right: 10px;">

                 <asp:DropDownListChosen runat="server" ID="ddlVendorPage" AllowSingleDeselect="false" DisableSearchThreshold="3"
                     DataPlaceHolder="Select Vendor" class="form-control" Width="100%" />
             </div>

           
                

            </div>
            <div class="clearfix"></div>
          
          
            <div class="col-md-12 colpadding0">
                 <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 25%; margin-right: 10px;">
                 <asp:DropDownListChosen runat="server" ID="ddlContractStatus" AllowSingleDeselect="false" DisableSearchThreshold="5"
                  DataPlaceHolder="Select Status" class="form-control" Width="100%">
                 </asp:DropDownListChosen>
             </div>
            
                <div class="col-md-3 colpadding0" style="margin-top: 5px; width: 25%; margin-right: 5px;">
                    <asp:TextBox runat="server" OnTextChanged="tbxFilter_TextChanged" ID="tbxFilter"  Width="100%" AutoComplete="off" placeholder="Type to Search" CssClass="form-control" />
                </div>
                
                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 25%; margin-right: 5px">
                    <label for="ddlStatus" class="hidden-label">Filter</label>
                    <asp:LinkButton Text="Apply" CssClass="btn btn-primary" runat="server" ID="LinkButton1"
                        OnClick="lnkBtnApplyFilter_Click" OnClientClick="javascript:$('#updateProgress').show()" />


                    <label for="ddlStatus" class="hidden-label">Filter</label>
                    <asp:LinkButton Text="Clear" CssClass="btn btn-primary" runat="server" ID="LinkButton2"
                        OnClick="lnkBtnClearFilter_Click" OnClientClick="javascript:$('#updateProgress').show()" />


                </div>

            </div>

             <!--advance search ends-->

             <div class="clearfix"></div>

            <div class="col-md-12 colpadding0">
                <asp:GridView runat="server" ID="grdContractList" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                    PageSize="10" AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%" DataKeyNames="ContractID"
                    OnRowCommand="grdContractList_RowCommand" OnRowDataBound="grdContractList_RowDataBound" OnSorting="grdContractList_Sorting" OnRowCreated="grdContractList_RowCreated">
                    <Columns>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr.No." ItemStyle-Width="3%">
                            <ItemTemplate>
                                <%#Container.DataItemIndex+1 %>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Contract No" ItemStyle-Width="20%" SortExpression="ContractNo">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ContractNo") %>' ToolTip='<%# Eval("ContractNo") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Contract Title" ItemStyle-Width="30%" SortExpression="ContractTitle">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ContractTitle") %>' ToolTip='<%# Eval("ContractTitle") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Vendor" ItemStyle-Width="20%" SortExpression="VendorNames">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("VendorNames") %>' ToolTip='<%# Eval("VendorNames") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField ItemStyle-Width="20%" HeaderText="Select Document Type" HeaderStyle-HorizontalAlign="center">
                            <ItemTemplate>
                                <asp:UpdatePanel runat="server" ID="aa1a" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:DropDownListChosen runat="server" ID="ddlDocType" class="form-control" Width="100%"
                                            AllowSingleDeselect="false" DisableSearchThreshold="5">
                                        </asp:DropDownListChosen>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:PostBackTrigger ControlID="ddlDocType" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </ItemTemplate>
                        </asp:TemplateField>

                      <%-- <asp:TemplateField HeaderText="Status" ItemStyle-Width="10%">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>--%>

                        <asp:TemplateField HeaderText="Action" HeaderStyle-CssClass="text-right" ItemStyle-CssClass="text-right" ItemStyle-Width="10%">
                            <ItemTemplate>
                                <asp:UpdatePanel ID="upDownloadFile" runat="server">
                                    <ContentTemplate>
                                        <asp:ImageButton ID="lblDownLoadfile" runat="server" ImageUrl="~/img/icon-download.png" 
                                            CommandArgument='<%# Eval("ContractID") %>' CommandName="Download"
                                            data-toggle="tooltip" data-placement="bottom" ToolTip="Download Document(s)"></asp:ImageButton>
                                        <asp:ImageButton ID="lblViewfile" runat="server" ImageUrl="~/img/view-doc.png" 
                                            CommandArgument='<%# Eval("ContractID") %>' CommandName="View"
                                            data-toggle="tooltip" data-placement="bottom" ToolTip="View Document(s)"></asp:ImageButton>
                                        
                                        <asp:ImageButton ID="lblSharefile" runat="server" ImageUrl="~/Images/share-icons.png" 
                                            CommandArgument='<%# Eval("ContractID") %>' CommandName="Share"
                                            data-toggle="tooltip" data-placement="bottom" ToolTip="Share Document(s)"></asp:ImageButton>

                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:PostBackTrigger ControlID="lblDownLoadfile" />
                                        <asp:PostBackTrigger ControlID="lblViewfile" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="clsROWgrid" />
                    <HeaderStyle CssClass="clsheadergrid" />
                    <PagerSettings Visible="false" />
                    <PagerTemplate>
                    </PagerTemplate>
                    <EmptyDataTemplate>
                        No Record Found
                    </EmptyDataTemplate>
                </asp:GridView>
            </div>

            <div class="col-md-12 colpadding0">
                <div class="col-md-10 colpadding0">
                    <div runat="server" id="DivRecordsScrum" style="float: left; margin-top: 5px; color: #999">
                        <p style="padding-right: 0px !Important;">
                            <%--<asp:Label ID="Label4" runat="server" Text="Showing "></asp:Label>--%>
                            <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>- 
                        <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                        <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                        </p>
                    </div>
                </div>
                <div class="col-md-1 colpadding0">
                    <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width:100%; float: right; height: 32px !important; margin-right:6%"
                        AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                        <asp:ListItem Text="5"/>
                        <asp:ListItem Text="10" Selected="True"/>
                        <asp:ListItem Text="20" />
                        <asp:ListItem Text="50" />
                    </asp:DropDownList>
                </div>
               
                   
                    <div class="col-md-1 colpadding0"> <%--<div style="float: left; width: 40%">--%>                    
                        <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true" DataPlaceHolder="No" AllowSingleDeselect="false"
                            OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged" class="form-control m-bot15" Width="100%" Height="32px">
                        </asp:DropDownListChosen>
                    </div>
                </div>
                <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
            </div>
        </div>
    

    

     <%--Viewer--%>
    <div>
        <div class="modal fade" id="divViewDocument" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
            <div class="modal-dialog" style="width: 90%;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body" style="height: 570px;">
                        <div class="col-md-12 colpadding0">
                            <asp:Label runat="server" ID="lblMessage" Style="color: red;"></asp:Label>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12 colpadding0">
                            <div class="col-md-2 colpadding0">
                                <table class="col-md-12">
                                    <thead>
                                        <tr>
                                            <td valign="top">
                                                <asp:UpdatePanel ID="upLitigationDetails" runat="server" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <asp:Repeater ID="rptLitigationVersionView" runat="server" OnItemCommand="rptLitigationVersionView_ItemCommand"
                                                            OnItemDataBound="rptLitigationVersionView_ItemDataBound">
                                                            <HeaderTemplate>
                                                                <table id="tblComplianceDocumnets">
                                                                    <thead>
                                                                        <th><b>File Name</b></th>
                                                                    </thead>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td>
                                                                        <asp:LinkButton CommandName="View" CommandArgument='<%# Eval("ContractID") + ","+ Eval("FileName") %>'
                                                                            ID="lblDocumentVersionView" runat="server" data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("FileName")%>'
                                                                            Text='<%# Eval("FileName").ToString().Substring(0,10) +"..." %>'></asp:LinkButton>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                </table>
                                                            </FooterTemplate>
                                                        </asp:Repeater>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="rptLitigationVersionView" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                            <div class="col-md-10 colpadding0">
                                
                                <fieldset style="height: 550px; width: 100%;">
                                    <iframe src="about:blank" id="docViewerLitigation" runat="server" width="100%" height="530px"></iframe>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="divDocumentsharePopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog p5" style="width: 82%;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label class="modal-header-custom">
                                Add Sharing Details</label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                            <iframe src="about:blank" id="Iframe_Docshare" frameborder="0" runat="server" width="100%" height="600px"></iframe>
                        </div>
                    </div>
                </div>
            </div>

</asp:Content>
