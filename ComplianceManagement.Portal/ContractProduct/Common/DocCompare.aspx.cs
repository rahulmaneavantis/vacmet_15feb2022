﻿using Spire.Doc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.Common
{
    public partial class DocCompare : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        public static void Contract_SaveDocFiles(List<KeyValuePair<string, Byte[]>> filesList)
        {
            if (filesList != null)
            {
                foreach (var file in filesList)
                {
                    using (BinaryWriter Writer = new BinaryWriter(File.OpenWrite(file.Key)))
                    {
                        Writer.Write(file.Value);
                        Writer.Flush();
                        Writer.Close();
                    }
                }
            }
        }

        protected void lnkDocumentUpload_Click(object sender, EventArgs e)
        {
            int UID = Convert.ToInt32(Request.QueryString["UID"]);
            int contractID = Convert.ToInt32(Request.QueryString["TID"]);
            int customerID  = Convert.ToInt32(Request.QueryString["CID"]);
           
            string datetime = DateTime.Now.ToString("yyyyMMddHHmmss");
            string directoryPath = string.Empty;
            string finalPath1 = string.Empty;
            string finalPath2 = string.Empty;
            if (ContractFileUpload.HasFile)
            {
                try
                {
                    directoryPath = Server.MapPath("~/ContractDocuments/" + customerID + "/" + Convert.ToInt32(contractID) + "/" + datetime + "/");

                    if (!Directory.Exists(directoryPath))
                        Directory.CreateDirectory(directoryPath);

                    Guid fileKey1 = Guid.NewGuid();
                    finalPath1 = Path.Combine(directoryPath, fileKey1 + Path.GetExtension(ContractFileUpload.FileName));
                    Stream fs = ContractFileUpload.PostedFile.InputStream;
                    BinaryReader br = new BinaryReader(fs);
                    Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                    List<KeyValuePair<string, Byte[]>> fileList = new List<KeyValuePair<string, Byte[]>>();
                    fileList.Add(new KeyValuePair<string, Byte[]>(finalPath1, bytes));

                    Contract_SaveDocFiles(fileList);
                }
                catch (Exception ex)
                {
                    finalPath1 = string.Empty;
                }
            }

            if (FileUpload1.HasFile)
            {
                try
                {
                    directoryPath = Server.MapPath("~/ContractDocuments/" + customerID + "/" + Convert.ToInt32(contractID) + "/" + datetime + "/");

                    if (!Directory.Exists(directoryPath))
                        Directory.CreateDirectory(directoryPath);

                    Guid fileKey12 = Guid.NewGuid();
                    finalPath2 = Path.Combine(directoryPath, fileKey12 + Path.GetExtension(FileUpload1.FileName));
                    Stream fs = FileUpload1.PostedFile.InputStream;
                    BinaryReader br = new BinaryReader(fs);
                    Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                    List<KeyValuePair<string, Byte[]>> fileList = new List<KeyValuePair<string, Byte[]>>();
                    fileList.Add(new KeyValuePair<string, Byte[]>(finalPath2, bytes));

                    Contract_SaveDocFiles(fileList);
                }
                catch (Exception ex)
                {
                    finalPath2 = string.Empty;
                }
            }

            if (!string.IsNullOrEmpty(finalPath1) && !string.IsNullOrEmpty(finalPath2))
            {
                //Create a Document instance

                Document doc1 = new Document();

                //Load the first Word document
                doc1.LoadFromFile(finalPath1);

                //Create a Document instance

                Document doc2 = new Document();

                //Load the second Word document

                doc2.LoadFromFile(finalPath2);

                //Compare the two Word documents

                doc1.Compare(doc2, "shreee");

                //Save the result to file

                doc1.SaveToFile(directoryPath + "Result.docx");
                //doc1.SaveToFile("C://Users/amol.patil/Desktop/25 nov 2021/Result.docx");

                doc1.Dispose();

                byte[] Content = File.ReadAllBytes(directoryPath + "Result.docx"); //missing ;
                Response.ContentType = "text/csv";
                Response.AddHeader("content-disposition", "attachment; filename=" + "Result.docx");
                Response.BufferOutput = true;
                Response.OutputStream.Write(Content, 0, Content.Length);
                Response.End();
            }
        }

        protected void btnUploadFile_Click(object sender, EventArgs e)
        {
            int UID = Convert.ToInt32(Request.QueryString["UID"]);
            int contractID = Convert.ToInt32(Request.QueryString["TID"]);
            int customerID = Convert.ToInt32(Request.QueryString["CID"]);

            string datetime = DateTime.Now.ToString("yyyyMMddHHmmss");
            string directoryPath = string.Empty;
            string finalPath1 = string.Empty;
            string finalPath2 = string.Empty;
            if (ContractFileUpload.HasFile)
            {
                try
                {
                    directoryPath = Server.MapPath("~/ContractDocuments/" + customerID + "/" + Convert.ToInt32(contractID) + "/" + datetime + "/");

                    if (!Directory.Exists(directoryPath))
                        Directory.CreateDirectory(directoryPath);

                    Guid fileKey1 = Guid.NewGuid();
                    finalPath1 = Path.Combine(directoryPath, fileKey1 + Path.GetExtension(ContractFileUpload.FileName));
                    Stream fs = ContractFileUpload.PostedFile.InputStream;
                    BinaryReader br = new BinaryReader(fs);
                    Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                    List<KeyValuePair<string, Byte[]>> fileList = new List<KeyValuePair<string, Byte[]>>();
                    fileList.Add(new KeyValuePair<string, Byte[]>(finalPath1, bytes));

                    Contract_SaveDocFiles(fileList);
                }
                catch (Exception ex)
                {
                    finalPath1 = string.Empty;
                }
            }

            if (FileUpload1.HasFile)
            {
                try
                {
                    directoryPath = Server.MapPath("~/ContractDocuments/" + customerID + "/" + Convert.ToInt32(contractID) + "/" + datetime + "/");

                    if (!Directory.Exists(directoryPath))
                        Directory.CreateDirectory(directoryPath);

                    Guid fileKey12 = Guid.NewGuid();
                    finalPath2 = Path.Combine(directoryPath, fileKey12 + Path.GetExtension(FileUpload1.FileName));
                    Stream fs = FileUpload1.PostedFile.InputStream;
                    BinaryReader br = new BinaryReader(fs);
                    Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                    List<KeyValuePair<string, Byte[]>> fileList = new List<KeyValuePair<string, Byte[]>>();
                    fileList.Add(new KeyValuePair<string, Byte[]>(finalPath2, bytes));

                    Contract_SaveDocFiles(fileList);
                }
                catch (Exception ex)
                {
                    finalPath2 = string.Empty;
                }
            }

            if (!string.IsNullOrEmpty(finalPath1) && !string.IsNullOrEmpty(finalPath2))
            {
                //Create a Document instance

                Document doc1 = new Document();

                //Load the first Word document
                doc1.LoadFromFile(finalPath1);

                //Create a Document instance

                Document doc2 = new Document();

                //Load the second Word document

                doc2.LoadFromFile(finalPath2);

                //Compare the two Word documents
                
                doc1.Compare(doc2, "shreee");

                //Save the result to file

                doc1.SaveToFile(directoryPath + "Result.docx");
                //doc1.SaveToFile("C://Users/amol.patil/Desktop/25 nov 2021/Result.docx");

                doc1.Dispose();

                byte[] Content = File.ReadAllBytes(directoryPath + "Result.docx"); //missing ;
                Response.ContentType = "text/csv";
                Response.AddHeader("content-disposition", "attachment; filename=" + "Result.docx");
                Response.BufferOutput = true;
                Response.OutputStream.Write(Content, 0, Content.Length);
                Response.End();
            }
        }
    }
}