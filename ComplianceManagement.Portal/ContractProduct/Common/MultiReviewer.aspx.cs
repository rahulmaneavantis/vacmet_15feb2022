﻿using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataContract;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.Common
{
    public partial class MultiReviewer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                int UserID = Convert.ToInt32(Request.QueryString["UID"]);
                int RoleID = Convert.ToInt32(Request.QueryString["RID"]);
                int ContractTemplateID = Convert.ToInt32(Request.QueryString["TID"]);
                int SectionID = Convert.ToInt32(Request.QueryString["SID"]);

                BindUser(SectionID, ContractTemplateID, RoleID);
                BindUserlist();
            }
        }

        private void BindUserlist()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            int ContractTemplateID = Convert.ToInt32(Request.QueryString["TID"]);

            List<UserAssignDetail> ShareList = new List<UserAssignDetail>();
            using (ContractMgmtEntities entities = new ContractMgmtEntities())
            {
                ShareList = (from row in entities.Cont_SP_GetuserassignDetails(customerID)
                             where row.IsReviewerflag.Equals("R")
                             && row.ContractID == ContractTemplateID
                             select new UserAssignDetail
                             {
                                 UserId = row.userID,
                                 UserName = row.Name,
                                 ID = row.ID
                             }).ToList();

                myRepeater.DataSource = ShareList;
                myRepeater.DataBind();
            }
        }
        protected void myRepeater_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "RemoveShare")
            {
                string Type = string.Empty;
                string[] str = e.CommandArgument.ToString().Split(',');
                int DeleteID = Convert.ToInt32(str[0]);
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                using (ContractMgmtEntities entities = new ContractMgmtEntities())
                {
                    Cont_MappingRevAppr obj = (from row in entities.Cont_MappingRevAppr
                                               where row.IsReviewerflag.Equals("R")
                                               && row.ID == DeleteID
                                               && row.CustID == customerID
                                               select row).FirstOrDefault();
                    if (obj != null)
                    {
                        obj.IsDelete = true;
                        entities.SaveChanges();
                        BindUserlist();
                    }
                }
            }
        }
        public class UserAssignDetail
        {
            public int UserId { get; set; }
            public string UserName { get; set; }
            public int ID { get; set; }
        }

        public static List<User> GetAllUsers_ContractNew(int customerID,int UID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Users
                             where row.IsDeleted == false
                             && row.IsActive == true
                             && row.CustomerID == customerID
                             && row.ContractRoleID != null
                             && row.ID != UID
                             select row).ToList();

                return query.ToList();
            }
        }

        public void BindUser(int SectionID, int ContractTemplateID, int RoleID)
        {
            int userID = -1;
            userID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
            
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            var lstAllUsers = GetAllUsers_ContractNew(customerID, userID);
            if (lstAllUsers.Count > 0)
                lstAllUsers = lstAllUsers.Where(entry => entry.ContractRoleID != null).ToList();
            
            var internalUsers = ContractUserManagement.GetRequiredUsers(lstAllUsers, 1); //1--Internal---

            ddlUser.DataValueField = "ID";
            ddlUser.DataTextField = "Name";
            ddlUser.DataSource = internalUsers;
            ddlUser.DataBind();
            ddlUser.Items.Insert(0, new ListItem("Select Reviewer", "-1"));            
        }
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            int UID = -1;
            UID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);

            int ContractTemplateID = Convert.ToInt32(Request.QueryString["TID"]);
            int SectionID = Convert.ToInt32(Request.QueryString["SID"]);


            using (ContractMgmtEntities entities = new ContractMgmtEntities())
            {
                int selecteduserID = Convert.ToInt32(ddlUser.SelectedValue);

                Cont_MappingRevAppr obj = (from row in entities.Cont_MappingRevAppr
                                           where row.IsReviewerflag.Equals("R")
                                           && row.UserID == selecteduserID
                                           && row.ContractID == ContractTemplateID
                                           select row).FirstOrDefault();
                if (obj == null)
                {
                    Cont_MappingRevAppr obj1 = new Cont_MappingRevAppr();
                    obj1.ContractID = ContractTemplateID;
                    obj1.UserID = selecteduserID;
                    obj1.CustID = customerID;
                    obj1.SectionID = SectionID;
                    obj1.TemplateID = ContractTemplateID;
                    obj1.IsReviewerflag = "R";
                    obj1.IsDelete = false;
                    obj1.CreatedOn = DateTime.Now;
                    obj1.CreatedBy = UID;
                    obj1.UpdatedBy = UID;
                    obj1.UpdatedOn = DateTime.Now;

                    entities.Cont_MappingRevAppr.Add(obj1);
                    entities.SaveChanges();
                    BindUserlist();
                }
                else
                {
                    if (obj.IsDelete == true)
                    {
                        entities.Cont_MappingRevAppr.Remove(obj);
                        entities.SaveChanges();

                        Cont_MappingRevAppr obj1 = new Cont_MappingRevAppr();
                        obj1.ContractID = ContractTemplateID;
                        obj1.UserID = selecteduserID;
                        obj1.CustID = customerID;
                        obj1.SectionID = SectionID;
                        obj1.TemplateID = ContractTemplateID;
                        obj1.IsReviewerflag = "R";
                        obj1.IsDelete = false;
                        obj1.CreatedOn = DateTime.Now;
                        obj1.CreatedBy = UID;
                        obj1.UpdatedBy = UID;
                        obj1.UpdatedOn = DateTime.Now;

                        entities.Cont_MappingRevAppr.Add(obj1);
                        entities.SaveChanges();
                        BindUserlist();
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "alertmsgduplicate();", true);
                    }
                }
            }
        }

        public class getTemplateTransactionDetail
        {
            public long TemplateTransactionID { get; set; }
            public long TemplateInstanceID { get; set; }
            public long ComplianceStatusID { get; set; }
            public System.DateTime CreatedOn { get; set; }
            public string Status { get; set; }
            public Nullable<System.DateTime> StatusChangedOn { get; set; }
            public string TemplateContent { get; set; }
            public long RoleID { get; set; }
            public long UserID { get; set; }
            public int sectionID { get; set; }
            public int orderID { get; set; }
            public int visibilty { get; set; }
            public string sectionHeader { get; set; }
        }

        protected void btnsubmitforrev_Click(object sender, EventArgs e)
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            int UserID = Convert.ToInt32(Request.QueryString["UID"]);
            int RoleID = Convert.ToInt32(Request.QueryString["RID"]);
            int TemplateID = Convert.ToInt32(Request.QueryString["TID"]);
            int SectionID = Convert.ToInt32(Request.QueryString["SID"]);
            //int ContractID = Convert.ToInt32(Request.QueryString["TID"]);
            
            List<int> ShareList = new List<int>();
            using (ContractMgmtEntities entities1 = new ContractMgmtEntities())
            {
                ShareList = (from row in entities1.Cont_SP_GetuserassignDetails(customerID)
                             where row.IsReviewerflag.Equals("R")
                             && row.ContractID == TemplateID
                             select row.userID).Distinct().ToList();
            }
            if (ShareList.Count > 0)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    string ContractVersion = ContractTemplateManagement.getVersion(Convert.ToInt32(TemplateID), customerID);

                    if (RoleID == 3)
                    {
                        List<ContractTemplateAssignment> objlst = (from row in entities.ContractTemplateAssignments
                                                                   where row.ContractTemplateInstanceID == TemplateID
                                                                   && row.RoleID == 4
                                                                   select row).ToList();

                        foreach (var item1 in objlst)
                        {
                            ContractTemplateAssignment objlst1 = (from row in entities.ContractTemplateAssignments
                                                                  where row.ContractTemplateInstanceID == TemplateID
                                                                  && row.ID == item1.ID
                                                                  && row.RoleID == 4
                                                                  select row).FirstOrDefault();
                            if (objlst1 != null)
                            {
                                entities.ContractTemplateAssignments.Remove(objlst1);
                                entities.SaveChanges();

                            }
                        }
                        

                        int approverordercount = 1;
                        int Isactioncount = 1;
                        //int selecteduserID = Convert.ToInt32(ddlUser.SelectedValue);
                        foreach (var selecteduserID in ShareList)
                        {
                            ContractTemplateAssignment ContracttemplateAssignment = new ContractTemplateAssignment()
                            {
                                ContractTemplateInstanceID = Convert.ToInt32(TemplateID),
                                RoleID = 4,
                                UserID = selecteduserID,
                                IsActive = true,
                                SectionID = Convert.ToInt32(SectionID),
                                Visibility = 1,
                                ApproveOrder = approverordercount,
                                IsAction = Isactioncount
                            };
                            ContractTemplateManagement.CreateContractTemplateAssignmentNew(ContracttemplateAssignment);
                            approverordercount = approverordercount + 1;
                            Isactioncount = 0;
                        }
                    }

                    var data12 = (from row in entities.Sp_GetContractTemplateDetailTransaction()
                                  where row.UserID == UserID
                                  && row.ContractTemplateInstanceID == TemplateID
                                  && row.RoleID == RoleID
                                  select row).ToList();

                    if (data12.Count > 0)
                    {
                        if (data12.Count > 0)
                        {
                            var data11 = (from row in data12
                                          join row1 in entities.Cont_tbl_ContractTemplateSectionMapping
                                          on row.sectionID equals row1.SectionID
                                          where row1.ContractTemplateInstanceID == TemplateID
                                          select new getTemplateTransactionDetail
                                          {
                                              TemplateTransactionID = row.TemplateTransactionID,
                                              TemplateInstanceID = row.ContractTemplateInstanceID,
                                              ComplianceStatusID = row.ComplianceStatusID,
                                              CreatedOn = row.CreatedOn,
                                              Status = row.Status,
                                              StatusChangedOn = row.StatusChangedOn,
                                              TemplateContent = row.ContractTemplateContent,
                                              RoleID = row.RoleID,
                                              UserID = row.UserID,
                                              sectionID = Convert.ToInt32(row.sectionID),
                                              orderID = Convert.ToInt32(row1.SectionOrder),
                                              visibilty = Convert.ToInt32(row1.Headervisibilty),
                                              sectionHeader = row.SectionHeader
                                          }).OrderBy(x => x.orderID).ToList();

                            System.Text.StringBuilder strExporttoWord1 = new System.Text.StringBuilder();

                            data11.ForEach(eachSection =>
                            {
                                //if (eachSection.visibilty == 1)
                                //{
                                //    strExporttoWord.Append(@"<div>");
                                //    strExporttoWord.Append(@"<p><B>" + eachSection.sectionHeader + "</B></p>");
                                //    strExporttoWord.Append(@"</div>");
                                //}

                                strExporttoWord1.Append(@"<div>");
                                strExporttoWord1.Append(@"<p>" + eachSection.TemplateContent + "</p>");
                                strExporttoWord1.Append(@"</div>");
                            });

                            ContractTemplateTransaction obj = new ContractTemplateTransaction();
                            obj.ContractTemplateInstanceID = TemplateID;
                            obj.CreatedOn = DateTime.Now;
                            obj.CreatedBy = UserID;
                            obj.ContractTemplateContent = strExporttoWord1.ToString();
                            //obj.ContractTemplateContent = theEditor.Content;
                            obj.SectionID = SectionID;
                            obj.Version = ContractVersion;
                            obj.CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User;
                            obj.Status = 2;
                            obj.OrderID = 2;
                            //if (Convert.ToInt32(ViewState["TransactionOrderID"]) > 0)
                            //{
                            //    obj.OrderID = Convert.ToInt32(ViewState["TransactionOrderID"]) + 1;
                            //}
                            entities.ContractTemplateTransactions.Add(obj);
                            entities.SaveChanges();
                            int TransactionID = Convert.ToInt32(obj.Id);
                            if (TransactionID > 0)
                            {
                                #region set transaction history

                                var entitiesData = (from row in entities.Cont_SP_GetContractSectionsDetail(customerID, Convert.ToInt32(TemplateID))
                                                    select row).ToList();


                                var data1 = (from row in entities.Sp_GetContractTemplateDetailTransaction()
                                             where row.UserID == UserID
                                             && row.ContractTemplateInstanceID == TemplateID
                                             && row.RoleID == RoleID
                                             select row).ToList();
                                if (data1.Count > 0)
                                {
                                    var data = (from row in data1
                                                join row1 in entities.Cont_tbl_ContractTemplateSectionMapping
                                                on row.sectionID equals row1.SectionID
                                                where row1.ContractTemplateInstanceID == TemplateID
                                                select new getTemplateTransactionDetail
                                                {
                                                    TemplateTransactionID = row.TemplateTransactionID,
                                                    TemplateInstanceID = row.ContractTemplateInstanceID,
                                                    ComplianceStatusID = row.ComplianceStatusID,
                                                    CreatedOn = row.CreatedOn,
                                                    Status = row.Status,
                                                    StatusChangedOn = row.StatusChangedOn,
                                                    TemplateContent = row.ContractTemplateContent,
                                                    RoleID = row.RoleID,
                                                    UserID = row.UserID,
                                                    sectionID = Convert.ToInt32(row.sectionID),
                                                    orderID = Convert.ToInt32(row1.SectionOrder),
                                                    visibilty = Convert.ToInt32(row1.Headervisibilty),
                                                    sectionHeader = row.SectionHeader
                                                }).OrderBy(x => x.orderID).ToList();

                                    System.Text.StringBuilder strExporttoWord = new System.Text.StringBuilder();

                                    data.ForEach(eachSection =>
                                    {
                                        if (eachSection.visibilty == 1)
                                        {
                                            strExporttoWord.Append(@"<div>");
                                            strExporttoWord.Append(@"<p><B>" + eachSection.sectionHeader + "</B></p>");
                                            strExporttoWord.Append(@"</div>");
                                        }

                                        strExporttoWord.Append(@"<div>");
                                        strExporttoWord.Append(@"<p>" + eachSection.TemplateContent + "</p>");
                                        strExporttoWord.Append(@"</div>");
                                    });

                                    ContractTemplateTransactionHistory obj1 = new ContractTemplateTransactionHistory();
                                    obj1.ContractTemplateID = TemplateID;
                                    obj1.CreatedOn = DateTime.Now;
                                    obj1.CreatedBy = AuthenticationHelper.UserID;
                                    obj1.TemplateContent = strExporttoWord.ToString();
                                    obj1.CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User;
                                    obj1.Status = 2;
                                    obj1.OrderID = 2;
                                    entities.ContractTemplateTransactionHistories.Add(obj1);
                                    entities.SaveChanges();
                                }
                                #endregion
                            }
                            if (RoleID == 3)
                            {
                                var queryResult1 = (from row in entities.Cont_tbl_ContractInstance
                                                    where row.ID == TemplateID
                                                   && row.IsDeleted == false
                                                    select row).FirstOrDefault();
                                if (queryResult1 != null)
                                {
                                    Cont_tbl_ContractStatusTransaction newStatusRecord = new Cont_tbl_ContractStatusTransaction()
                                    {
                                        CustomerID = customerID,
                                        ContractID = Convert.ToInt32(TemplateID),
                                        StatusID = 2,
                                        StatusChangeOn = DateTime.Now,
                                        IsActive = true,
                                        CreatedBy = UserID,
                                        UpdatedBy = UserID,
                                    };
                                    ContractManagement.CreateContractStatusTransaction(newStatusRecord);
                                }
                            }
                        }

                        //if (ShareList.Count > 0)
                        //{
                        //    var queryResult1 = (from row in entities.Cont_tbl_ContractInstance
                        //                        where row.ID == ContractID
                        //                       && row.IsDeleted == false
                        //                        select row).FirstOrDefault();
                        //    if (queryResult1 != null)
                        //    {
                        //        var queryResult12 = (from row in entities.ContractTemplateAssignments
                        //                             where row.ContractTemplateInstanceID == ContractID
                        //                           && row.IsActive == true
                        //                           && row.RoleID == 3
                        //                             select row).FirstOrDefault();
                        //        if (queryResult12 != null)
                        //        {
                        //            long sectionid = queryResult12.SectionID;

                        //            int approverordercount = 1;
                        //            int Isactioncount = 1;
                        //            foreach (var selecteduserID in ShareList)
                        //            {
                        //                ContractTemplateAssignment ContracttemplateAssignment = new ContractTemplateAssignment()
                        //                {
                        //                    ContractTemplateInstanceID = Convert.ToInt32(ContractID),
                        //                    RoleID = 4,
                        //                    UserID = selecteduserID,
                        //                    IsActive = true,
                        //                    SectionID = Convert.ToInt32(sectionid),
                        //                    Visibility = 1,
                        //                    ApproveOrder = approverordercount,
                        //                    IsAction = Isactioncount
                        //                };
                        //                ContractTemplateManagement.CreateContractTemplateAssignment(ContracttemplateAssignment);

                        //                approverordercount = approverordercount + 1;
                        //                Isactioncount = 0;
                        //            }
                        //        }
                        //    }
                        //}
                    }
                }
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "closepopup();", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "alertmsg();", true);
            }
        }
    }
}