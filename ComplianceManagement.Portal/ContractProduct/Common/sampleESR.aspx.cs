﻿using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.Common
{
    public partial class sampleESR : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //select the Home tab by default
            RadEditor1.RibbonBar.SelectedTabIndex = 1;
            //handle postbacks from the export commands
            try
            {
                string evtArg = Request["__EVENTARGUMENT"];
                switch (evtArg)
                {
                    case "SaveAsDocx":
                        RadEditor1.ExportToDocx();
                        break;
                    case "SaveAsRtf":
                        RadEditor1.ExportToRtf();
                        break;
                    case "SaveAsPDF":
                        RadEditor1.ExportToPdf();
                        break;
                    case "SaveAsMarkdown":
                        RadEditor1.ExportToMarkdown();
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                RadNotification1.Show("There was an error during the export operation. Try simplifying the content and removing images/lists.");
            }
        }

        protected void RadAsyncUpload1_FileUploaded(object sender, FileUploadedEventArgs e)
        {
            //the maximum allowed file inputs is one, so there should be no multiple files uploaded

            //see what is the uploaded file extension and attempt to import it accordingly
            try
            {
                string fileExt = e.File.GetExtension();
                switch (fileExt)
                {
                    case ".doc":
                    case ".docx":
                        RadEditor1.LoadDocxContent(e.File.InputStream);
                        break;
                    case ".rtf":
                        RadEditor1.LoadRtfContent(e.File.InputStream);
                        break;
                    case ".txt":
                    case ".html":
                    case ".htm":
                        using (StreamReader sr = new StreamReader(e.File.InputStream))
                        {
                            RadEditor1.Content = sr.ReadToEnd();
                        }
                        break;
                    case ".md":
                        using (StreamReader sr = new StreamReader(e.File.InputStream))
                        {
                            RadEditor1.Content = sr.ReadToEnd();
                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "importMarkdownScript", "TelerikDemo.setMarkdownContent();", true);
                        }
                        break;
                    default:
                        RadNotification1.Show("The selected file is invalid. Please upload an MS Word document with an extension .doc, .docx or .rtf, or a .txt/.html file with HTML content!");
                        break;
                }
            }
            catch (Exception ex)
            {
                RadNotification1.Show("There was an error during the import operation. Try simplifying the content.");
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (RadEditor1.Content != "" && RadEditor1.Content != "null")
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        if (Request.QueryString["ContId"] != null && Request.QueryString["ContId"] != "")
                        {
                            int newContractID = Convert.ToInt32(Request.QueryString["ContId"]);


                            Cont_tbl_ContractTemplateSectionMapping obj12Mappingobj = (from row in entities.Cont_tbl_ContractTemplateSectionMapping
                                                                                    where row.ContractTemplateInstanceID == newContractID
                                                                                    select row).FirstOrDefault();

                            if (obj12Mappingobj != null)
                            {
                                ContractTemplateTransaction obj = new ContractTemplateTransaction();
                                obj.ContractTemplateInstanceID = newContractID;
                                obj.CreatedOn = DateTime.Now;
                                obj.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                obj.ContractTemplateContent = RadEditor1.Content;
                                obj.SectionID = obj12Mappingobj.SectionID;
                                obj.Version = "1.0";
                                obj.CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User;
                                obj.Status = 1;
                                obj.OrderID = 1;

                                entities.ContractTemplateTransactions.Add(obj);
                                entities.SaveChanges();
                            }
                            else
                            {
                                Cont_tbl_TemplateMaster obj1 = new Cont_tbl_TemplateMaster();
                                obj1.CustomerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                                obj1.TemplateName = "";
                                obj1.Version = "1.0";
                                obj1.CreatedOn = DateTime.Now;
                                obj1.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                obj1.ApproverCount = 1;
                                obj1.UploadType = 1;
                                obj1.IsDeleted = false;
                                entities.Cont_tbl_TemplateMaster.Add(obj1);
                                entities.SaveChanges();

                                long gettemplateId = obj1.ID;

                                Cont_tbl_SectionMaster obj12 = new Cont_tbl_SectionMaster();
                                obj12.CustomerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                                obj12.IsActive = true;
                                obj12.Header = "";
                                obj12.BodyContent = "";
                                obj12.CreatedOn = DateTime.Now;
                                obj12.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                obj12.Headervisibilty = 0;
                                obj12.UploadType = 1;
                                entities.Cont_tbl_SectionMaster.Add(obj12);
                                entities.SaveChanges();

                                long getsectionId = obj12.ID;

                                Cont_tbl_ContractTemplateSectionMapping obj12Mapping = (from row in entities.Cont_tbl_ContractTemplateSectionMapping
                                                                                        where row.ContractTemplateInstanceID == newContractID
                                                                                        select row).FirstOrDefault();
                                if (obj12Mapping == null)
                                {
                                    Cont_tbl_ContractTemplateSectionMapping obj11 = new Cont_tbl_ContractTemplateSectionMapping();
                                    obj11.ContractTemplateInstanceID = newContractID;
                                    obj11.SectionID = getsectionId;
                                    obj11.SectionOrder = 1;
                                    obj11.IsActive = true;
                                    obj11.CustomerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                                    obj11.CreatedOn = DateTime.Now;
                                    obj11.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                    obj11.UpdatedOn = DateTime.Now;
                                    obj11.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                    obj11.Headervisibilty = 0;
                                    entities.Cont_tbl_ContractTemplateSectionMapping.Add(obj11);
                                    entities.SaveChanges();

                                    ContractTemplateAssignment obj12ass = (from row in entities.ContractTemplateAssignments
                                                                           where row.ContractTemplateInstanceID == newContractID
                                                                           && row.RoleID == 3
                                                                           select row).FirstOrDefault();

                                    if (obj12ass == null)
                                    {
                                        ContractTemplateAssignment ContracttemplateAssignment = new ContractTemplateAssignment()
                                        {
                                            ContractTemplateInstanceID = Convert.ToInt32(newContractID),
                                            RoleID = 3,
                                            UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                            IsActive = true,
                                            SectionID = getsectionId,
                                            Visibility = 1
                                        };
                                        ContractTemplateManagement.CreateContractTemplateAssignment(ContracttemplateAssignment);

                                        ContractTemplateTransaction obj = new ContractTemplateTransaction();
                                        obj.ContractTemplateInstanceID = newContractID;
                                        obj.CreatedOn = DateTime.Now;
                                        obj.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                        obj.ContractTemplateContent = RadEditor1.Content;
                                        obj.SectionID = getsectionId;
                                        obj.Version = "1.0";
                                        obj.CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User;
                                        obj.Status = 1;
                                        obj.OrderID = 1;

                                        entities.ContractTemplateTransactions.Add(obj);
                                        entities.SaveChanges();

                                        Cont_tbl_ContractInstance data1 = (from row in entities.Cont_tbl_ContractInstance
                                                                           where row.ID == newContractID
                                                                           && row.IsDeleted == false
                                                                           select row).FirstOrDefault();

                                        if (data1 != null)
                                        {
                                            data1.ApproverCount = 1;
                                            entities.SaveChanges();
                                        }
                                    }
                                }
                            }
                        }
                    }
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "ColsedSaveuploadModal();", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "ColsedSaveuploadModal1();", true);
                }
                
            }
            catch (Exception ex)
            {

            }
        }
    }
}