﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MultiReviewer.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.Common.MultiReviewer" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <!-- Bootstrap CSS -->
  <link href="../../NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
            <!-- bootstrap theme -->
    <link href="../../NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />  
    <link href="../../NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet"  type="text/css" />
    <link href="../../Style/css/StyleSheetTextEditor.css" rel="stylesheet" />

     <style type="text/css">
        .dropdownclass {
            color: rgb(142, 142, 147);
            background-color: rgb(255, 255, 255);
            padding: 6px 12px;
            border-width: 1px;
            border-style: solid;
            border-color: rgb(199, 199, 204);
            width: 24%;
            height: 32px !important;
            margin-right: 6%;
            border-image: initial;
            border-radius: 4px;
            transition: border-color 0.15s ease-in-out 0s, box-shadow 0.15s ease-in-out 0s;
        }


    </style>
    <script type="text/javascript">
        function closepopup()
        {
            window.parent.CloseSectionModal();
        }
        function alertmsg() {
            alert("Please select at least one User.");
            return false;
        }
        function alertmsgduplicate() {
            alert("Please select different User.");
            return false;
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div class="row">
            <div class="col-md-12 colpadding0 ">
                  <div class="col-md-2">
                      <asp:Label Text="Reviewer Name" style="font-weight: bold;color: black;">Reviewer Name</asp:Label>
                      </div>
                <div class="col-md-2">
                    <asp:DropDownList runat="server" ID="ddlUser" style="width: 70%;" CssClass="dropdownclass">
                    </asp:DropDownList>
               <%-- </div>
                <div class="col-md-2">--%>
                    <div style="float: right; margin-right: 0.3%;">
                        <asp:Button ID="btnAdd" runat="server" Text="ADD" CssClass="btn btn-primary" OnClick="btnAdd_Click" />
                    </div>
                </div>
            </div>
        </div>
        <div>
            <div class="row">
                        <div class="form-group required col-md-12"  style="overflow-x: auto;min-height: 89px;"><%--max-height: 212px;--%>
                            <%--<asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                <ContentTemplate>--%>
                                    <asp:Repeater ID="myRepeater" runat="server" OnItemCommand="myRepeater_ItemCommand">
                                        <HeaderTemplate>
                                            <div style="font-weight: bold; color: black"></div> <%--Reviewers List--%>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <div class="row">
                                                <div class="form-group required col-md-12" style="padding-left: 0px;">
                                                    <div class="form-group required col-md-4" style="padding-left: 0px;margin: 10px 0 8% 0px;">
                                                        <asp:Label ID="myLabel" runat="server" Style="color: black; float: left;width: 50%;" Text='<%# Eval("UserName") %>' />
                                                    <%--</div>
                                                    <div class="form-group required col-md-1" style="padding-left: 0px;">--%>
                                                        <asp:ImageButton ID="LnkDeletShare" runat="server" CommandName="RemoveShare" CommandArgument='<%# Eval("ID") +","+ Eval("UserId") %>' Style="float: left; padding: 0px 2px 0px 0px;" ImageUrl="~/Images/delete_icon_new.png" data-toggle="tooltip" data-placement="bottom" ToolTip="Click to UnShare File"></asp:ImageButton>                                                         
                                                    </div>
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:Repeater>
                               <%-- </ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="myRepeater" />
                                </Triggers>
                            </asp:UpdatePanel>--%>
                        </div>
                    </div>
            <div class="row">
                <div class="col-md-12 colpadding0 ">
                    <div style="margin-left: 38%;">
                        <asp:Button ID="btnsubmitforrev" runat="server" Text="Submit for Review" CssClass="btn btn-primary" OnClick="btnsubmitforrev_Click" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
