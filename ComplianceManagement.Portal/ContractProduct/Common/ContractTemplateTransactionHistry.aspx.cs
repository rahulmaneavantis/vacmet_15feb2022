﻿using System;
using System.Linq;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Telerik.Web.UI;
using Spire.Doc;
using com.VirtuosoITech.ComplianceManagement.Business.Contract;

namespace com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.Common
{
    public partial class ContractTemplateTransactionHistry : System.Web.UI.Page
    {
        protected static int roleid;
        protected static int ContId;
        protected static int CustId;
        protected static int UId;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                int UserID = Convert.ToInt32(Request.QueryString["UID"]);
                int RoleID = Convert.ToInt32(Request.QueryString["RID"]);
                int ContractTemplateID = Convert.ToInt32(Request.QueryString["TID"]);
                BindTransactionHistory(ContractTemplateID);
            }
        }

        public void BindTransactionHistory(long TemplateInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var entitiesData = (from row in entities.ContractTemplateTransactionHistories
                                    where row.ContractTemplateID == TemplateInstanceID
                                    select row).ToList();

                rptComplianceVersionView.DataSource = entitiesData.OrderByDescending(entry => entry.CreatedOn);
                rptComplianceVersionView.DataBind();
            }
        }
        protected void rptComplianceVersionView_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                if (e.CommandName.Equals("View"))
                {
                    theEditor.Content = "";
                    string[] commandArg = e.CommandArgument.ToString().Split(',');
                    int TransactionID = Convert.ToInt32(commandArg[0]);
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        int TemplateID = Convert.ToInt32(Request.QueryString["TID"]);
                        
                        var entitiesData = (from row in entities.ContractTemplateTransactionHistories
                                            where row.ContractTemplateID == TemplateID
                                            && row.ID == TransactionID
                                            select row).FirstOrDefault();
                        if (entitiesData != null)
                        {
                            theEditor.Content = entitiesData.TemplateContent;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void BtnExportDocument_Click(object sender, EventArgs e)
        {
            try
            {
                theEditor.ExportToDocx();
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

            if (true)
            {
                ////#region doc comparision
                //////Create a Document instance

                ////Document doc1 = new Document();

                //////Load the first Word document

                ////doc1.LoadFromFile("C://Users/amol.patil/Desktop/25 nov 2021/sample3.docx");
                //////doc1.LoadFromFile("Doc1.docx");      

                //////Create a Document instance

                ////Document doc2 = new Document();

                //////Load the second Word document

                ////doc2.LoadFromFile("C://Users/amol.patil/Desktop/25 nov 2021/sample1.docx");

                //////Compare the two Word documents

                ////doc1.Compare(doc2, "Shawn");

                //////Save the result to file
                ////doc1.SaveToFile("C://Users/amol.patil/Desktop/25 nov 2021/Result.docx");

                ////doc1.Dispose();
                ////#endregion


                //System.Text.StringBuilder strExporttoWord = new System.Text.StringBuilder();

                //string fileName = "Sampledocument";
                ////string templateName = lstSections.TemplateName;

                //strExporttoWord.Append(@"
                //<html 
                //xmlns:o='urn:schemas-microsoft-com:office:office' 
                //xmlns:w='urn:schemas-microsoft-com:office:word'
                //xmlns='http://www.w3.org/TR/REC-html40'>
                //<head><title></title>

                //<!--[if gte mso 9]>
                //<xml>
                //<w:WordDocument>
                //<w:View>Print</w:View>
                //<w:Zoom>90</w:Zoom>
                //<w:DoNotOptimizeForBrowser/>
                //</w:WordDocument>
                //</xml>
                //<![endif]-->

                //<style>
                //p.MsoFooter, li.MsoFooter, div.MsoFooter
                //{
                //margin:0in;
                //margin-bottom:.0001pt;
                //mso-pagination:widow-orphan;
                //tab-stops:center 3.0in right 6.0in;
                //font-size:12.0pt;
                //}
                //<style>

                //<!-- /* Style Definitions */

                //@page Section1
                //{
                //size:8.5in 11.0in; 
                //margin:1.0in 1.25in 1.0in 1.25in ;
                //mso-header-margin:.5in;
                //mso-header: h1;
                //mso-footer: f1; 
                //mso-footer-margin:.5in;
                // font-family:Arial;
                //}

                //div.Section1
                //{
                //page:Section1;
                //}
                //table#hrdftrtbl
                //{
                //    margin:0in 0in 0in 9in;
                //}

                //.break { page-break-before: always; }
                //-->
                //</style></head>");

                //strExporttoWord.Append(@"<body lang=EN-US style='tab-interval:.5in'> <div class=Section1>");
                ////data.ForEach(eachSection =>
                ////{
                //    //if (eachSection.visibilty == 1)
                //    //{
                //    //    strExporttoWord.Append(@"<div>");
                //    //    strExporttoWord.Append(@"<p><B>" + eachSection.sectionHeader + "</B></p>");
                //    //    strExporttoWord.Append(@"</div>");
                //    //}

                //    strExporttoWord.Append(@"<div>");
                //    strExporttoWord.Append(@"<p>" + theEditor.Content + "</p>");
                //    strExporttoWord.Append(@"</div>");
                ////});

                //strExporttoWord.Append(@"<table id='hrdftrtbl' border='1' cellspacing='0' cellpadding='0'>
                //<tr>
                //    <td>
                //        <div style='mso-element:header' id=h1 >
                //            <p class=MsoHeader style='text-align:left'>
                //        </div>
                //    </td>
                //    <td>
                //        <div style='mso-element:footer' id=f1>
                //            <p class=MsoFooter>
                //            <span style=mso-tab-count:2'></span>
                //            <span style='mso-field-code:"" PAGE ""'></span>
                //            of <span style='mso-field-code:"" NUMPAGES ""'></span>
                //            </p>
                //        </div>
                //    </td>
                //</tr>
                //</table>
                //</body>
                //</html>");

                ////fileName = fileName + "-" + DateTime.Now.ToString("ddMMyyyy");

                ////string out1 = fileName + ".doc";
                ////out1 = fileName + ".docx";

                ////Response.AppendHeader("Content-Type", "application/msword");
                ////Response.AppendHeader("Content-disposition", "attachment; filename=" + out1);
                ////Response.Write(strExporttoWord);


                //Response.Clear();
                //Response.ContentType = "Application/msword";
                //Response.AddHeader("Content-Disposition", "attachment; filename=myfile.docx");
                //Response.Write(strExporttoWord); //works too
                ////Response.BinaryWrite(strExporttoWord.ToArray());
                //Response.Flush();
                //Response.Close();
                //Response.End();
                //theEditor.ExportToDocx();
            }
        }

        protected void btndocumentcompare_Click(object sender, EventArgs e)
        {
            CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            UId = Convert.ToInt32(Request.QueryString["UID"]);
            roleid = Convert.ToInt32(Request.QueryString["RID"]);
            ContId = Convert.ToInt32(Request.QueryString["ContractTID"]);
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenHistorySectionModal();", true);
        }
    }
}