﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataContract;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Telerik.Web.UI;
using Telerik.Web.UI.Editor;
using Telerik.Web.UI.Editor.Export;
using Telerik.Windows.Documents.Flow.FormatProviders.Docx;
using Telerik.Windows.Documents.Flow.Model;
using Telerik.Windows.Documents.Flow.Model.Styles;

namespace com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.Common
{
    public partial class SectionContractReviewDetail : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                int UserID = Convert.ToInt32(Request.QueryString["UID"]);
                int CustomerID = Convert.ToInt32(Request.QueryString["CID"]);
                int RoleID = Convert.ToInt32(Request.QueryString["RID"]);
                int ContractTemplateID = Convert.ToInt32(Request.QueryString["TID"]);
                int SectionID = Convert.ToInt32(Request.QueryString["SID"]);
                BindUser(SectionID, ContractTemplateID, RoleID, CustomerID);
                SetDefaultConfiguratorValues(UserID, RoleID, ContractTemplateID, SectionID);
            }
        }
        
        public void BindUser(int SectionID, int ContractTemplateID, int RoleID,int customerID)
        {            
            var lstAllUsers = ContractUserManagement.GetAllUsers_Contract(customerID);
            if (lstAllUsers.Count > 0)
                lstAllUsers = lstAllUsers.Where(entry => entry.ContractRoleID != null).ToList();


            var internalUsers = ContractUserManagement.GetRequiredUsers(lstAllUsers, 1); //1--Internal---

            ddlUser.DataValueField = "ID";
            ddlUser.DataTextField = "Name";
            ddlUser.DataSource = internalUsers;
            ddlUser.DataBind();
            ddlUser.Items.Insert(0, new ListItem("Select Reviewer", "-1"));

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var getdetails = (from row in entities.ContractTemplateAssignments
                                  where row.ContractTemplateInstanceID == ContractTemplateID
                                  && row.SectionID == SectionID
                                  && row.IsActive == true
                                  && row.RoleID == RoleID
                                  select row).FirstOrDefault();

                if (getdetails != null)
                {
                    if (getdetails.UserID != null)
                        ddlUser.SelectedValue = Convert.ToString(getdetails.UserID);
                }
            }

        }
        public int getRID(string roleCode)
        {
            int RoleID = -1;
            try
            {

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Role data = (from row in entities.Roles
                                 where row.Code == roleCode
                                 select row).FirstOrDefault();
                    if (data != null)
                    {
                        RoleID = Convert.ToInt32(data.ID);
                    }
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return RoleID;
        }

        private void SetDefaultConfiguratorValues(int UserID, int RoleID, int TemplateID, int SectionID)
        {
            int RolegetID = getRID("VIR");//Vendor Internal Reviewer
            int RolegetVRID = getRID("VR");//Vendor Reviewer
            btnshareforinternalrevvendor.Visible = false;
            btnSubmit.Visible = false;
            btnReject.Visible = false;
            ddlUser.Visible = false;
            
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                //string UserName = "";
                int CustomerID = Convert.ToInt32(Request.QueryString["CID"]);
                string UserName = getVendorName(UserID, CustomerID, RoleID, RolegetID, RolegetVRID);
                //string UserName = getVendorName(UserID, CustomerID, RoleID);

                var data = (from row in entities.Sp_GetContractTemplateDetailTransaction()
                            where row.UserID == UserID
                            && row.ContractTemplateInstanceID == TemplateID
                            && row.RoleID == RoleID
                            && row.sectionID == SectionID
                            select row).FirstOrDefault();

                if (data != null)
                {
                    theEditor.TrackChangesSettings.CanAcceptTrackChanges = true;
                    theEditor.EnableTrackChanges = true;
                    if (RoleID == RolegetVRID)
                    {
                        if (data.ComplianceStatusID == 16 || data.ComplianceStatusID == 21 || data.ComplianceStatusID == 22)
                        {
                            theEditor.Content = data.ContractTemplateContent;
                            theEditor.TrackChangesSettings.Author = UserName;
                            lblUser.Text = UserName;
                            theEditor.TrackChangesSettings.UserCssId = "reU0";
                            btnSubmit.Visible = true;
                            btnReject.Visible = true;
                            btnshareforinternalrevvendor.Visible = true;
                        }
                        else
                        {
                            btnSubmit.Visible = false;
                            btnReject.Visible = false;
                        }
                    }
                    if (RoleID == RolegetID)
                    {
                        if (data.ComplianceStatusID == 20)
                        {
                            theEditor.Content = data.ContractTemplateContent;
                            theEditor.TrackChangesSettings.Author = UserName;
                            lblUser.Text = UserName;
                            theEditor.TrackChangesSettings.UserCssId = "reU0";
                            btnSubmit.Visible = true;
                            btnReject.Visible = true;
                        }
                        else
                        {
                            btnSubmit.Visible = false;
                            btnReject.Visible = false;
                        }
                    }
                    //if (RoleID == 3)
                    //{
                    //    if (data.ComplianceStatusID == 16 || data.ComplianceStatusID == 21)
                    //    {
                    //        theEditor.Content = data.ContractTemplateContent;
                    //        theEditor.TrackChangesSettings.Author = UserName;
                    //        lblUser.Text = UserName;
                    //        theEditor.TrackChangesSettings.UserCssId = "reU0";
                    //        btnSubmit.Visible = true;
                    //        btnReject.Visible = true;
                    //    }
                    //    else
                    //    {
                    //        btnSubmit.Visible = false;
                    //        btnReject.Visible = false;
                    //    }
                    //}
                    
                }
                else
                {
                    btnSubmit.Visible = true;
                    btnReject.Visible = true;
                    theEditor.TrackChangesSettings.Author = UserName;
                    lblUser.Text = UserName;
                    theEditor.TrackChangesSettings.UserCssId = "reU9";
                    theEditor.TrackChangesSettings.CanAcceptTrackChanges = false;
                    theEditor.EnableTrackChanges = false;
                }
            }
        }

        public static string getVendorName(long UserID, long CustomerID,long RoleID,int RolegetVIRID, int RolegetVRID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                string VendorName = string.Empty;
                if (RoleID == RolegetVRID)
                {
                    //Cont_tbl_VendorMaster vendorObj = (from row in entities.Cont_tbl_VendorMaster
                    //                                   where row.ID == UserID
                    //                                        && row.CustomerID == CustomerID
                    //                                        && row.IsDeleted==false
                    //                                   select row).FirstOrDefault();
                    Cont_tbl_VendorMaster vendorObj = VendorDetails.GetVendorMasterDetails(UserID, CustomerID);


                    if (vendorObj != null)
                    {
                        VendorName = vendorObj.VendorName;
                    }
                }
                else if (RoleID == RolegetVIRID)
                {
                    Cont_tbl_SubVendorMaster vendorObj = (from row in entities.Cont_tbl_SubVendorMaster
                                                          where row.ID == UserID
                                                               && row.CustomerID == CustomerID
                                                               && row.IsDeleted == false
                                                          select row).FirstOrDefault();
                    if (vendorObj != null)
                    {
                        VendorName = vendorObj.VendorName;
                    }
                }
                return VendorName;
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            int RolegetID = getRID("VIR");//Vendor Internal Reviewer
            int RolegetVRID = getRID("VR");//Vendor Reviewer
            int UserID = Convert.ToInt32(Request.QueryString["UID"]);
            int RoleID = Convert.ToInt32(Request.QueryString["RID"]);
            int TemplateID = Convert.ToInt32(Request.QueryString["TID"]);
            int SectionID = Convert.ToInt32(Request.QueryString["SID"]);
            int CustomerID = Convert.ToInt32(Request.QueryString["CID"]);
            string ContractVersion = ContractTemplateManagement.getVersion(Convert.ToInt32(TemplateID), CustomerID);
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                string VendorName = getVendorName(UserID, CustomerID, RoleID, RolegetID, RolegetVRID);
                //string VendorName = getVendorName(UserID, CustomerID, RoleID);
                //string VendorName = string.Empty;
                //Cont_tbl_VendorMaster vendorObj = (from row in entities.Cont_tbl_VendorMaster
                //                                   where row.ID == UserID
                //                                        && row.CustomerID == CustomerID
                //                                   select row).FirstOrDefault();
                //if (vendorObj != null)
                //{
                //    VendorName = vendorObj.VendorName;
                //}

                ContractTemplateTransaction obj = new ContractTemplateTransaction();
                obj.ContractTemplateInstanceID = TemplateID;
                obj.CreatedOn = DateTime.Now;
                obj.CreatedBy = UserID;
                obj.ContractTemplateContent = theEditor.Content;
                obj.SectionID = SectionID;
                obj.CreatedByText = VendorName;
                if (RoleID == RolegetVRID)
                    obj.Status = 17;
                else if (RoleID == RolegetID)
                    obj.Status = 21;

                obj.Version = ContractVersion;
                obj.OrderID = 1;
                entities.ContractTemplateTransactions.Add(obj);
                entities.SaveChanges();
                int TransactionID = Convert.ToInt32(obj.Id);
                if (TransactionID > 0)
                {
                    if (!string.IsNullOrEmpty(getHistryInsert.Text))
                    {
                        UpdateTransactionHistory(TemplateID, TransactionID, getHistryInsert.Text, "I", UserID);
                    }
                    if (!string.IsNullOrEmpty(getHistryDel.Text))
                    {
                        UpdateTransactionHistory(TemplateID, TransactionID, getHistryDel.Text, "D", UserID);
                    }
                    #region set transaction history
                    
                    var entitiesData = (from row in entities.Cont_SP_GetContractSectionsDetail(CustomerID, Convert.ToInt32(TemplateID))
                                        select row).ToList();


                    var data1 = (from row in entities.Sp_GetContractTemplateDetailTransaction()
                                 where row.UserID == UserID
                                 && row.ContractTemplateInstanceID == TemplateID
                                 && row.RoleID == RoleID
                                 select row).ToList();
                    if (data1.Count > 0)
                    {
                        var data = (from row in data1
                                    join row1 in entities.Cont_tbl_ContractTemplateSectionMapping
                                    on row.sectionID equals row1.SectionID
                                    where row1.ContractTemplateInstanceID == TemplateID
                                    select new getTemplateTransactionDetail
                                    {
                                        TemplateTransactionID = row.TemplateTransactionID,
                                        TemplateInstanceID = row.ContractTemplateInstanceID,
                                        ComplianceStatusID = row.ComplianceStatusID,
                                        CreatedOn = row.CreatedOn,
                                        Status = row.Status,
                                        StatusChangedOn = row.StatusChangedOn,
                                        TemplateContent = row.ContractTemplateContent,
                                        RoleID = row.RoleID,
                                        UserID = row.UserID,
                                        sectionID = Convert.ToInt32(row.sectionID),
                                        orderID = Convert.ToInt32(row1.SectionOrder),
                                        visibilty = Convert.ToInt32(row1.Headervisibilty),
                                        sectionHeader = row.SectionHeader
                                    }).OrderBy(x => x.orderID).ToList();

                        System.Text.StringBuilder strExporttoWord = new System.Text.StringBuilder();

                        data.ForEach(eachSection =>
                        {
                            if (eachSection.visibilty == 1)
                            {
                                strExporttoWord.Append(@"<div>");
                                strExporttoWord.Append(@"<p><B>" + eachSection.sectionHeader + "</B></p>");
                                strExporttoWord.Append(@"</div>");
                            }

                            strExporttoWord.Append(@"<div>");
                            strExporttoWord.Append(@"<p>" + eachSection.TemplateContent + "</p>");
                            strExporttoWord.Append(@"</div>");
                        });

                        ContractTemplateTransactionHistory obj1 = new ContractTemplateTransactionHistory();
                        obj1.ContractTemplateID = TemplateID;
                        obj1.CreatedOn = DateTime.Now;
                        obj1.CreatedBy = UserID;
                        obj1.TemplateContent = strExporttoWord.ToString();
                        obj1.CreatedByText = VendorName;
                        obj1.Status = 17;
                        obj1.OrderID = 1;
                        entities.ContractTemplateTransactionHistories.Add(obj1);
                        entities.SaveChanges();
                    }
                    #endregion
                }
            }
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "CloseModel();", true);
        }

        public class getTemplateTransactionDetail
        {
            public long TemplateTransactionID { get; set; }
            public long TemplateInstanceID { get; set; }
            public long ComplianceStatusID { get; set; }
            public System.DateTime CreatedOn { get; set; }
            public string Status { get; set; }
            public Nullable<System.DateTime> StatusChangedOn { get; set; }
            public string TemplateContent { get; set; }
            public long RoleID { get; set; }
            public long UserID { get; set; }
            public int sectionID { get; set; }
            public int orderID { get; set; }
            public int visibilty { get; set; }
            public string sectionHeader { get; set; }
        }
        
        public bool UpdateTransactionHistory(int TemplateId, int TransactionID, string getInput, string TransactionType,int UserID)
        {
            bool result = false;

            if (!string.IsNullOrEmpty(getInput))
            {
                var getdata = getInput;
                string[] stringSeparators = new string[] { "endend" };
                var resultdata = getdata.Split(stringSeparators, StringSplitOptions.None);
                foreach (var item in resultdata)
                {
                    if (!string.IsNullOrEmpty(item))
                    {
                        string[] stringSeparators1 = new string[] { "andand" };
                        var result1 = item.Split(stringSeparators1, StringSplitOptions.None);
                        string inputtext = result1[0];
                        string author = result1[1];
                        string title = result1[2];
                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        {
                            cont_ContractTemplateTransactionHistoryMapping obj = new cont_ContractTemplateTransactionHistoryMapping
                            {
                                Author = result1[1],
                                Inputtext = result1[0],
                                Title = result1[2],
                                CreatedBy = Convert.ToInt64(UserID),
                                ContractTemplateInstanceID = TemplateId,
                                TemplateTransactionID = TransactionID,
                                TransactionType = TransactionType,
                                CreatedOn = DateTime.Now,
                                IsActive = true
                            };
                            entities.cont_ContractTemplateTransactionHistoryMapping.Add(obj);
                            entities.SaveChanges();
                        }
                    }
                }
            }
            return result;
        }

        protected void RadEditor1_ExportContent(object sender, EditorExportingArgs e)
        {
            ExportType exportType = e.ExportType;

            if (exportType == ExportType.Word)
            {
                string exportedOutput = e.ExportOutput;

                Byte[] output = Encoding.Default.GetBytes(exportedOutput);

                DocxFormatProvider docxProvider = new DocxFormatProvider();
                RadFlowDocument document = docxProvider.Import(output);

                Header defaultHeader = document.Sections.First().Headers.Add();
                Paragraph defaultHeaderParagraph = defaultHeader.Blocks.AddParagraph();
                defaultHeaderParagraph.TextAlignment = Alignment.Right;
                defaultHeaderParagraph.Inlines.AddRun("This is a sample header.");

                Footer defaultFooter = document.Sections.First().Footers.Add();
                Paragraph defaultFooterParagraph = defaultFooter.Blocks.AddParagraph();
                defaultFooterParagraph.TextAlignment = Alignment.Right;
                defaultFooterParagraph.Inlines.AddRun("This is a sample footer.");

                Byte[] modifiedOutput = docxProvider.Export(document);
                string finalOutput = Encoding.Default.GetString(modifiedOutput, 0, modifiedOutput.Length);

                e.ExportOutput = finalOutput;
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            theEditor.ExportToDocx();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            divHtml.InnerHtml = theEditor.Content;
        }

        protected void btnReject_Click(object sender, EventArgs e)
        {
            int RolegetID = getRID("VIR");//Vendor Internal Reviewer
            int RolegetVRID = getRID("VR");//Vendor Reviewer
            int UserID = Convert.ToInt32(Request.QueryString["UID"]);
            int RoleID = Convert.ToInt32(Request.QueryString["RID"]);
            int TemplateID = Convert.ToInt32(Request.QueryString["TID"]);
            int SectionID = Convert.ToInt32(Request.QueryString["SID"]);
            int CustomerID = Convert.ToInt32(Request.QueryString["CID"]);
            string ContractVersion = ContractTemplateManagement.getVersion(Convert.ToInt32(TemplateID), CustomerID);
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                string VendorName = getVendorName(UserID, CustomerID, RoleID, RolegetID, RolegetVRID);

                //string VendorName = string.Empty;
                //string VendorName = getVendorName(UserID, CustomerID, RoleID);
                //Cont_tbl_VendorMaster vendorObj = (from row in entities.Cont_tbl_VendorMaster
                //                                   where row.ID == UserID
                //                                        && row.CustomerID == CustomerID
                //                                   select row).FirstOrDefault();
                //if (vendorObj != null)
                //{
                //    VendorName = vendorObj.VendorName;
                //}

                ContractTemplateTransaction obj = new ContractTemplateTransaction();
                obj.ContractTemplateInstanceID = TemplateID;
                obj.CreatedOn = DateTime.Now;
                obj.CreatedBy = UserID;
                obj.ContractTemplateContent = theEditor.Content;
                obj.SectionID = SectionID;
                obj.CreatedByText = VendorName;
                //obj.Status = 15;
                if (RoleID == RolegetVRID)
                    obj.Status = 15;
                else if (RoleID == RolegetID)
                    obj.Status = 22;

                obj.Version = ContractVersion;
                obj.OrderID = 1;
                entities.ContractTemplateTransactions.Add(obj);
                entities.SaveChanges();
                int TransactionID = Convert.ToInt32(obj.Id);
                if (TransactionID > 0)
                {
                    if (!string.IsNullOrEmpty(getHistryInsert.Text))
                    {
                        UpdateTransactionHistory(TemplateID, TransactionID, getHistryInsert.Text, "I", UserID);
                    }
                    if (!string.IsNullOrEmpty(getHistryDel.Text))
                    {
                        UpdateTransactionHistory(TemplateID, TransactionID, getHistryDel.Text, "D", UserID);
                    }
                    #region set transaction history

                    var entitiesData = (from row in entities.Cont_SP_GetContractSectionsDetail(CustomerID, Convert.ToInt32(TemplateID))
                                        select row).ToList();


                    var data1 = (from row in entities.Sp_GetContractTemplateDetailTransaction()
                                 where row.UserID == UserID
                                 && row.ContractTemplateInstanceID == TemplateID
                                 && row.RoleID == RoleID
                                 select row).ToList();
                    if (data1.Count > 0)
                    {
                        var data = (from row in data1
                                    join row1 in entities.Cont_tbl_ContractTemplateSectionMapping
                                    on row.sectionID equals row1.SectionID
                                    where row1.ContractTemplateInstanceID == TemplateID
                                    select new getTemplateTransactionDetail
                                    {
                                        TemplateTransactionID = row.TemplateTransactionID,
                                        TemplateInstanceID = row.ContractTemplateInstanceID,
                                        ComplianceStatusID = row.ComplianceStatusID,
                                        CreatedOn = row.CreatedOn,
                                        Status = row.Status,
                                        StatusChangedOn = row.StatusChangedOn,
                                        TemplateContent = row.ContractTemplateContent,
                                        RoleID = row.RoleID,
                                        UserID = row.UserID,
                                        sectionID = Convert.ToInt32(row.sectionID),
                                        orderID = Convert.ToInt32(row1.SectionOrder),
                                        visibilty = Convert.ToInt32(row1.Headervisibilty),
                                        sectionHeader = row.SectionHeader
                                    }).OrderBy(x => x.orderID).ToList();

                        System.Text.StringBuilder strExporttoWord = new System.Text.StringBuilder();

                        data.ForEach(eachSection =>
                        {
                            if (eachSection.visibilty == 1)
                            {
                                strExporttoWord.Append(@"<div>");
                                strExporttoWord.Append(@"<p><B>" + eachSection.sectionHeader + "</B></p>");
                                strExporttoWord.Append(@"</div>");
                            }

                            strExporttoWord.Append(@"<div>");
                            strExporttoWord.Append(@"<p>" + eachSection.TemplateContent + "</p>");
                            strExporttoWord.Append(@"</div>");
                        });

                        ContractTemplateTransactionHistory obj1 = new ContractTemplateTransactionHistory();
                        obj1.ContractTemplateID = TemplateID;
                        obj1.CreatedOn = DateTime.Now;
                        obj1.CreatedBy = UserID;
                        obj1.TemplateContent = strExporttoWord.ToString();
                        obj1.CreatedByText = VendorName;
                        obj1.Status = 15;
                        obj1.OrderID = 1;
                        entities.ContractTemplateTransactionHistories.Add(obj1);
                        entities.SaveChanges();
                    }
                    #endregion
                }
            }
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "CloseModel();", true);
        }
          public static int UpdateVendorRev(string Email,string ContactNo,int CID, int UID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Cont_tbl_SubVendorMaster data = (from row in entities.Cont_tbl_SubVendorMaster
                                                  where row.Email == Email
                                                       && row.ContactNumber == ContactNo
                                                       && row.CustomerID == CID
                                                  select row).FirstOrDefault();
                    if (data != null)
                    {
                        data.IsDeleted = false;
                        entities.SaveChanges();
                        return Convert.ToInt32(data.ID);
                    }
                    else
                    {
                        Cont_tbl_SubVendorMaster obj = new Cont_tbl_SubVendorMaster
                        {
                            CustomerID = CID,
                            IsDeleted = false,                           
                            VendorName = Email,                            
                            ContactPerson = "",
                            ContactNumber = ContactNo,
                            Email = Email,
                            CreatedBy = UID,
                            CreatedOn = DateTime.Now
                        };
                        entities.Cont_tbl_SubVendorMaster.Add(obj);
                        entities.SaveChanges();
                        long getVID= obj.ID;
                        return Convert.ToInt32(getVID);
                    }
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
           
        }
        protected void btnvendr_Click(object sender, EventArgs e)
        {
            int RolegetID = getRID("VIR");//Vendor Internal Reviewer
            int UserID = Convert.ToInt32(Request.QueryString["UID"]);
            int customerID = Convert.ToInt32(Request.QueryString["CID"]);
            int RoleID = Convert.ToInt32(Request.QueryString["RID"]);
            int ContractTemplateID = Convert.ToInt32(Request.QueryString["TID"]);

            List<int> vendorlist = new List<int>();
            string ContractVersion = ContractTemplateManagement.getVersion(Convert.ToInt32(ContractTemplateID), customerID);
            int vID = -1;
            if (VendorReviewerEmail.Text != "" && txtcontactnum.Text != "")
            {
                vID = UpdateVendorRev(VendorReviewerEmail.Text, txtcontactnum.Text, customerID, UserID);
            }
            long SectionID = Convert.ToInt32(Request.QueryString["SID"]);
            
            if (SectionID != -1 && vID != -1)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    string ContractTemplate = string.Empty;
                    var data12 = (from row in entities.Sp_GetContractTemplateDetailTransaction()
                                  where row.UserID == UserID
                                  && row.ContractTemplateInstanceID == ContractTemplateID
                                  && row.RoleID == RoleID
                                  && row.sectionID == SectionID
                                  select row).FirstOrDefault();
                    if (data12 != null)
                    {
                        ContractTemplate = data12.ContractTemplateContent;
                    }

                    ContractTemplateTransaction obj = new ContractTemplateTransaction();
                    obj.ContractTemplateInstanceID = Convert.ToInt32(ContractTemplateID);
                    obj.CreatedOn = DateTime.Now;
                    obj.CreatedBy = UserID;
                    obj.ContractTemplateContent = ContractTemplate;
                    obj.SectionID = Convert.ToInt32(SectionID);
                    obj.CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User;
                    obj.Status = 20;
                    obj.Version = ContractVersion;
                    obj.OrderID = 1;
                    entities.ContractTemplateTransactions.Add(obj);
                    entities.SaveChanges();
                }

                ContractTemplateAssignment ContracttemplateAssignment = new ContractTemplateAssignment()
                {
                    ContractTemplateInstanceID = Convert.ToInt32(ContractTemplateID),
                    RoleID = RolegetID,
                    UserID = vID,
                    IsActive = true,
                    SectionID = Convert.ToInt32(SectionID),
                    Visibility = 1
                };
                ContractTemplateManagement.CreateContractTemplateAssignment(ContracttemplateAssignment);

                vendorlist.Add(vID);
            }
            if (ContractTemplateID > 0 && vendorlist.Count > 0)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var contractdata = (from row in entities.Cont_tbl_ContractInstance
                                        where row.ID == ContractTemplateID
                                        && row.IsDeleted == false
                                        select row).FirstOrDefault();

                    if (contractdata != null)
                    {
                        vendorlist = vendorlist.Distinct().ToList();
                        foreach (var item in vendorlist)
                        {
                            try
                            {
                                Cont_tbl_SubVendorMaster data = (from row in entities.Cont_tbl_SubVendorMaster
                                                                 where row.ID == item
                                                                 && row.IsDeleted == false
                                                                 select row).FirstOrDefault();
                                if (data != null)
                                {
                                    int vendorID = item;
                                    int assignedRoleID = RolegetID;
                                    string checkSum = Util.CalculateMD5Hash(vendorID.ToString() + ContractTemplateID.ToString() + assignedRoleID.ToString());

                                    string vendorname = data.VendorName;
                                    string contname = contractdata.ContractTitle;
                                    string contnum = contractdata.ContractNo;
                                    string accessURL = string.Empty;
                                    URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(AuthenticationHelper.CustomerID));
                                    if (Urloutput != null)
                                    {
                                        accessURL = Urloutput.URL;
                                    }
                                    else
                                    {
                                        accessURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                                    }
                                    string AccessURL = Convert.ToString(accessURL) +
                                                               "/ContractVerifyOtp.aspx?" +
                                                               "UID=" + vendorID.ToString() +
                                                                "&RID=" + RolegetID.ToString() +
                                                                 "&CustID=" + customerID +
                                                                   "&ContractID=" + ContractTemplateID.ToString();

                                    //string AccessURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]) +
                                    //                            "/ContractVerifyOtp.aspx?" +
                                    //                            "UID=" + vendorID.ToString() +
                                    //                             "&RID=" + RolegetID.ToString() +
                                    //                              "&CustID=" + customerID +
                                    //                                "&ContractID=" + ContractTemplateID.ToString();

                                    string header = "Dear " + vendorname + "<br>" + "<br>" + "Contract has been shared with you for review." + "<br>" + "<br>";

                                    string ContractName = "Contract Name - " + contname + "<br>" + "<br>";

                                    string ContractNo = "Contract no. - " + contnum + "<br>" + "<br>";

                                    string UrlAccess = "Url to access Contract - " + AccessURL;

                                    string FinalMail = header + ContractName + ContractNo + UrlAccess;

                                    string SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"].ToString();

                                    string vendorEmail = data.Email;

                                    EmailManager.SendMail(SenderEmailAddress, new List<String>(new String[] { vendorEmail }), null, null, "You have received a contract for review", FinalMail);
                                }
                            }
                            catch (Exception ex)
                            {
                                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                            }
                        }
                    }
                }
            }
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "CloseModel();", true);
        }
    }
}