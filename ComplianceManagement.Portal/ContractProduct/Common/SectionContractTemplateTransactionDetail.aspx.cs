﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Telerik.Web.UI;
using Telerik.Web.UI.Editor;
using Telerik.Web.UI.Editor.Export;
using Telerik.Windows.Documents.Flow.FormatProviders.Docx;
using Telerik.Windows.Documents.Flow.Model;
using Telerik.Windows.Documents.Flow.Model.Styles;

namespace com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.Common
{
    public partial class SectionContractTemplateTransactionDetail : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                int UserID = Convert.ToInt32(Request.QueryString["UID"]);
                int RoleID = Convert.ToInt32(Request.QueryString["RID"]);
                int ContractTemplateID = Convert.ToInt32(Request.QueryString["TID"]);
                int SectionID = Convert.ToInt32(Request.QueryString["SID"]);
                                
                BindUser(SectionID, ContractTemplateID,RoleID);               
                SetDefaultConfiguratorValues(UserID, RoleID, ContractTemplateID, SectionID);
            }
        }
        
        public void BindUser(int SectionID,int ContractTemplateID,int RoleID)
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            var lstAllUsers = ContractUserManagement.GetAllUsers_Contract(customerID);
            if (lstAllUsers.Count > 0)
                lstAllUsers = lstAllUsers.Where(entry => entry.ContractRoleID != null).ToList();


            var internalUsers = ContractUserManagement.GetRequiredUsers(lstAllUsers, 1); //1--Internal---

            ddlUser.DataValueField = "ID";
            ddlUser.DataTextField = "Name";
            ddlUser.DataSource = internalUsers;
            ddlUser.DataBind();
            ddlUser.Items.Insert(0, new ListItem("Select Reviewer", "-1"));
            
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var getdetails = (from row in entities.ContractTemplateAssignments
                                  where row.ContractTemplateInstanceID == ContractTemplateID
                                  && row.SectionID == SectionID
                                  && row.IsActive == true
                                  && row.RoleID == 4
                                  select row).FirstOrDefault();

                if (getdetails != null)
                {
                    if (getdetails.UserID != null)
                        ddlUser.SelectedValue = Convert.ToString(getdetails.UserID);
                }
            }

        }

        private void SetDefaultConfiguratorValues(int UserID, int RoleID, int TemplateID, int SectionID)
        {
            ViewState["TransactionOrderID"] = 0;
            ViewState["TransactionID"] = 0;
            btnSubmit.Visible = true;
            btnSubmitr.Visible = true;
            ddlUser.Visible = false;
            btnforClosed.Visible = false;
            
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                string UserName = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User;

                var data = (from row in entities.Sp_GetContractTemplateDetailTransaction()
                            where row.UserID == UserID
                            && row.ContractTemplateInstanceID == TemplateID
                            && row.RoleID == RoleID
                            && row.sectionID == SectionID
                            select row).FirstOrDefault();

                if (data != null)
                {
                    ViewState["TransactionOrderID"] = data.OrderID;
                    ViewState["TransactionID"] = data.TemplateTransactionID;
                    theEditor.TrackChangesSettings.CanAcceptTrackChanges = true;
                    theEditor.EnableTrackChanges = true;

                    if (RoleID == 3)
                    {
                        btnSubmit.Text = "Review";
                        if (data.ComplianceStatusID == 1)
                        {
                            theEditor.Content = data.ContractTemplateContent;
                            btnSubmit.Visible = true;
                            btnSubmitr.Visible = false;
                            btnforApproval.Visible = true;
                            ddlUser.Visible = true;
                            Button3.Visible = false;
                            btnPrimaryApprover.Visible = false;
                            btnPrimaryRejetced.Visible = false;
                            Button4.Visible = false;
                            btnforClosed.Visible = true;
                            theEditor.TrackChangesSettings.Author = UserName;
                            lblUser.Text = UserName;
                            theEditor.TrackChangesSettings.UserCssId = "reU9";
                            //theEditor.TrackChangesSettings.CanAcceptTrackChanges = false;
                            //theEditor.EnableTrackChanges = false;
                            theEditor.TrackChangesSettings.CanAcceptTrackChanges = true;
                            theEditor.EnableTrackChanges = true;

                        }
                        else if (data.ComplianceStatusID == 3 || data.ComplianceStatusID == 14)
                        {
                            theEditor.Content = data.ContractTemplateContent;
                            btnSubmit.Visible = true;
                            btnSubmitr.Visible = false;
                            ddlUser.Visible = true;
                            btnforApproval.Visible = true;
                            Button3.Visible = false;
                            Button4.Visible = false;
                            btnforClosed.Visible = true;
                            btnPrimaryApprover.Visible = false;
                            btnPrimaryRejetced.Visible = false;
                            theEditor.TrackChangesSettings.Author = UserName;
                            lblUser.Text = UserName;
                            theEditor.TrackChangesSettings.UserCssId = "reU9";
                            theEditor.TrackChangesSettings.CanAcceptTrackChanges = true;
                            theEditor.EnableTrackChanges = true;
                        }
                        else if (data.ComplianceStatusID == 15)
                        {
                            theEditor.Content = data.ContractTemplateContent;
                            btnSubmit.Visible = true;
                            btnSubmitr.Visible = false;
                            ddlUser.Visible = false;
                            btnforApproval.Visible = false;
                            Button3.Visible = false;
                            Button4.Visible = false;
                            btnforClosed.Visible = true;
                            btnPrimaryApprover.Visible = false;
                            btnPrimaryRejetced.Visible = false;
                            theEditor.TrackChangesSettings.Author = UserName;
                            lblUser.Text = UserName;
                            theEditor.TrackChangesSettings.UserCssId = "reU9";
                            theEditor.TrackChangesSettings.CanAcceptTrackChanges = true;
                            theEditor.EnableTrackChanges = true;
                        }
                        else if (data.ComplianceStatusID == 17)
                        {
                            theEditor.Content = data.ContractTemplateContent;
                            btnSubmit.Visible = false;
                            btnSubmitr.Visible = false;
                            ddlUser.Visible = false;
                            btnforApproval.Visible = false;
                            Button3.Visible = false;
                            Button4.Visible = false;
                            btnforClosed.Visible = true;
                            btnPrimaryApprover.Visible = false;
                            btnPrimaryRejetced.Visible = false;
                            theEditor.TrackChangesSettings.Author = UserName;
                            lblUser.Text = UserName;
                            theEditor.TrackChangesSettings.UserCssId = "reU9";
                            theEditor.TrackChangesSettings.CanAcceptTrackChanges = true;
                            theEditor.EnableTrackChanges = true;
                        }
                        else
                        {
                            btnSubmit.Visible = false;
                            btnSubmitr.Visible = false;
                            btnforApproval.Visible = false;
                            btnforClosed.Visible = false;
                            Button3.Visible = false;
                            Button4.Visible = false;
                            btnPrimaryApprover.Visible = false;
                            btnPrimaryRejetced.Visible = false;
                        }
                    }
                    if (RoleID == 4)
                    {
                        if (data.ComplianceStatusID == 2)
                        {
                            theEditor.Content = data.ContractTemplateContent;
                            theEditor.TrackChangesSettings.Author = UserName;
                            lblUser.Text = UserName;
                            theEditor.TrackChangesSettings.UserCssId = "reU0";
                            btnSubmit.Visible = true;
                            btnSubmitr.Visible = true;
                            btnforApproval.Visible = false;
                            btnforClosed.Visible = false;
                            Button3.Visible = false;
                            Button4.Visible = false;
                            btnPrimaryApprover.Visible = false;
                            btnPrimaryRejetced.Visible = false;
                        }
                        else
                        {
                            btnSubmit.Visible = false;
                            btnSubmitr.Visible = false;
                            btnforApproval.Visible = false;
                            btnforClosed.Visible = false;
                            Button3.Visible = false;
                            btnPrimaryApprover.Visible = false;
                            btnPrimaryRejetced.Visible = false;
                            Button4.Visible = false;
                        }
                    }
                    if (RoleID == 6)
                    {
                        if (data.ComplianceStatusID == 4)
                        {
                            theEditor.Content = data.ContractTemplateContent;

                            btnSubmit.Visible = false;
                            btnSubmitr.Visible = false;
                            btnforApproval.Visible = false;
                            btnforClosed.Visible = false;
                            Button3.Visible = true;
                            Button4.Visible = true;
                            btnPrimaryApprover.Visible = false;
                            btnPrimaryRejetced.Visible = false;
                            theEditor.TrackChangesSettings.Author = UserName;
                            lblUser.Text = UserName;
                            theEditor.TrackChangesSettings.UserCssId = "reU9";
                        }
                        else
                        {
                            btnSubmit.Visible = false;
                            btnSubmitr.Visible = false;
                            btnforClosed.Visible = false;
                            btnforApproval.Visible = false;
                            Button3.Visible = false;
                            Button4.Visible = false;
                            btnPrimaryApprover.Visible = false;
                            btnPrimaryRejetced.Visible = false;
                        }
                    }
                    if (RoleID == 29)
                    {
                        if (data.ComplianceStatusID == 18)
                        {
                            theEditor.Content = data.ContractTemplateContent;

                            btnSubmit.Visible = false;
                            btnforApproval.Visible = false;
                            btnforClosed.Visible = false;
                            Button3.Visible = false;
                            Button4.Visible = false;
                            btnPrimaryApprover.Visible = true;
                            btnPrimaryRejetced.Visible = true;
                            btnSubmitr.Visible = false;
                            theEditor.TrackChangesSettings.Author = UserName;
                            lblUser.Text = UserName;
                            theEditor.TrackChangesSettings.UserCssId = "reU9";
                        }
                        else
                        {
                            btnSubmit.Visible = false;
                            btnforClosed.Visible = false;
                            btnforApproval.Visible = false;
                            Button3.Visible = false;
                            Button4.Visible = false;
                            btnSubmitr.Visible = false;
                            btnPrimaryApprover.Visible = false;
                            btnPrimaryRejetced.Visible = false;
                        }
                    }
                }
                else
                {
                    btnSubmit.Visible = true;
                    btnforApproval.Visible = true;
                    btnforClosed.Visible = true;
                    Button3.Visible = false;
                    Button4.Visible = false;
                    btnSubmitr.Visible = false;
                    btnPrimaryApprover.Visible = false;
                    btnPrimaryRejetced.Visible = false;
                    theEditor.TrackChangesSettings.Author = UserName;
                    lblUser.Text = UserName;
                    theEditor.TrackChangesSettings.UserCssId = "reU9";
                    theEditor.TrackChangesSettings.CanAcceptTrackChanges = false;
                    theEditor.EnableTrackChanges = false;
                }
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            int UserID = Convert.ToInt32(Request.QueryString["UID"]);
            int RoleID = Convert.ToInt32(Request.QueryString["RID"]);
            if (RoleID == 4)
            {
                int TemplateID = Convert.ToInt32(Request.QueryString["TID"]);
                int SectionID = Convert.ToInt32(Request.QueryString["SID"]);
                int StatusID = -1;

                if (Request.QueryString["StatusID"] != null)
                {
                    StatusID = Convert.ToInt32(Request.QueryString["StatusID"]);
                }

                bool validation = true;
                if (RoleID == 3)
                {
                    if (StatusID == 1)
                    {
                        if (ddlUser.SelectedValue == "" || ddlUser.SelectedValue == "-1")
                        {
                            validation = false;
                        }
                    }
                }

                if (validation)
                {
                    string ContractVersion = ContractTemplateManagement.getVersion(Convert.ToInt32(TemplateID), customerID);
                    bool flagcheck = false;
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        #region assiggnment next


                        bool updatestatus = false;
                        int nextorderId = 0;
                        var objRecord = (from row in entities.ContractTemplateAssignments
                                         where row.ContractTemplateInstanceID == TemplateID
                                         && row.RoleID == RoleID
                                         && row.SectionID == SectionID
                                         select row).OrderBy(x => x.ApproveOrder).ToList();
                        if (objRecord.Count > 1)
                        {
                            ContractTemplateAssignment data = (from row in entities.ContractTemplateAssignments
                                                               where row.ContractTemplateInstanceID == TemplateID
                                                               && row.RoleID == RoleID
                                                               && row.UserID == UserID
                                                               && row.IsActive == true
                                                               && row.SectionID == SectionID
                                                               select row).FirstOrDefault();
                            if (data != null)
                            {
                                int orderID = Convert.ToInt32(data.ApproveOrder);
                                nextorderId = orderID + 1;
                                var getdetails = objRecord.Where(x => x.ApproveOrder == nextorderId && x.IsActive == true && x.IsAction == 0).FirstOrDefault();
                                if (getdetails != null)
                                {
                                    updatestatus = true;
                                    data.IsAction = 0;
                                    entities.SaveChanges();
                                }
                            }
                        }



                        int cnt = 0;
                        var d1 = (from row in entities.ContractTemplateTransactions
                                  where row.ContractTemplateInstanceID == TemplateID
                                     && row.SectionID == SectionID
                                  select row).ToList();
                        if (d1.Count > 0)
                        {
                            cnt = d1.Count + 1;
                        }
                        ContractTemplateTransaction obj = new ContractTemplateTransaction();
                        obj.ContractTemplateInstanceID = TemplateID;
                        obj.CreatedOn = DateTime.Now;
                        obj.CreatedBy = UserID;
                        obj.ContractTemplateContent = theEditor.Content;
                        obj.SectionID = SectionID;
                        obj.CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User;

                        if (updatestatus)
                        {
                            obj.Status = 2;
                            ContractTemplateAssignment data12 = (from row in entities.ContractTemplateAssignments
                                                                 where row.ContractTemplateInstanceID == TemplateID
                                                                 && row.RoleID == RoleID
                                                                 && row.ApproveOrder == nextorderId
                                                                 && row.IsActive == true
                                                                 && row.SectionID == SectionID
                                                                 select row).FirstOrDefault();
                            if (data12 != null)
                            {
                                flagcheck = true;
                                data12.IsAction = 1;
                                entities.SaveChanges();
                            }
                        }
                        else
                            obj.Status = 3;

                        obj.OrderID = cnt; //1//change by amol
                        obj.Version = ContractVersion;
                        entities.ContractTemplateTransactions.Add(obj);
                        entities.SaveChanges();
                        #endregion

                        //ContractTemplateTransaction obj = new ContractTemplateTransaction();
                        //obj.ContractTemplateInstanceID = TemplateID;
                        //obj.CreatedOn = DateTime.Now;
                        //obj.CreatedBy = UserID;
                        //obj.ContractTemplateContent = theEditor.Content;
                        //obj.SectionID = SectionID;
                        //obj.Version = ContractVersion;
                        //obj.CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User;
                        //if (RoleID == 4)
                        //{
                        //    obj.Status = 3;
                        //}
                        //else if (RoleID == 3)// && StatusID != 15
                        //{
                        //    obj.Status = 2;
                        //}
                        //if (Convert.ToInt32(ViewState["TransactionOrderID"]) > 0)
                        //{
                        //    obj.OrderID = Convert.ToInt32(ViewState["TransactionOrderID"]) + 1;
                        //}
                        //entities.ContractTemplateTransactions.Add(obj);
                        //entities.SaveChanges();

                        int TransactionID = Convert.ToInt32(obj.Id);
                        if (TransactionID > 0)
                        {
                            if (!string.IsNullOrEmpty(getHistryInsert.Text))
                            {
                                UpdateTransactionHistory(TemplateID, TransactionID, getHistryInsert.Text, "I");
                            }
                            if (!string.IsNullOrEmpty(getHistryDel.Text))
                            {
                                UpdateTransactionHistory(TemplateID, TransactionID, getHistryDel.Text, "D");
                            }
                            #region set transaction history

                            var entitiesData = (from row in entities.Cont_SP_GetContractSectionsDetail(customerID, Convert.ToInt32(TemplateID))
                                                select row).ToList();


                            var data1 = (from row in entities.Sp_GetContractTemplateDetailTransaction()
                                         where row.UserID == UserID
                                         && row.ContractTemplateInstanceID == TemplateID
                                         && row.RoleID == RoleID
                                         select row).ToList();
                            if (data1.Count > 0)
                            {
                                var data = (from row in data1
                                            join row1 in entities.Cont_tbl_ContractTemplateSectionMapping
                                            on row.sectionID equals row1.SectionID
                                            where row1.ContractTemplateInstanceID == TemplateID
                                            select new getTemplateTransactionDetail
                                            {
                                                TemplateTransactionID = row.TemplateTransactionID,
                                                TemplateInstanceID = row.ContractTemplateInstanceID,
                                                ComplianceStatusID = row.ComplianceStatusID,
                                                CreatedOn = row.CreatedOn,
                                                Status = row.Status,
                                                StatusChangedOn = row.StatusChangedOn,
                                                TemplateContent = row.ContractTemplateContent,
                                                RoleID = row.RoleID,
                                                UserID = row.UserID,
                                                sectionID = Convert.ToInt32(row.sectionID),
                                                orderID = Convert.ToInt32(row1.SectionOrder),
                                                visibilty = Convert.ToInt32(row1.Headervisibilty),
                                                sectionHeader = row.SectionHeader
                                            }).OrderBy(x => x.orderID).ToList();

                                System.Text.StringBuilder strExporttoWord = new System.Text.StringBuilder();

                                data.ForEach(eachSection =>
                                {
                                    if (eachSection.visibilty == 1)
                                    {
                                        strExporttoWord.Append(@"<div>");
                                        strExporttoWord.Append(@"<p><B>" + eachSection.sectionHeader + "</B></p>");
                                        strExporttoWord.Append(@"</div>");
                                    }

                                    strExporttoWord.Append(@"<div>");
                                    strExporttoWord.Append(@"<p>" + eachSection.TemplateContent + "</p>");
                                    strExporttoWord.Append(@"</div>");
                                });

                                ContractTemplateTransactionHistory obj1 = new ContractTemplateTransactionHistory();
                                obj1.ContractTemplateID = TemplateID;
                                obj1.CreatedOn = DateTime.Now;
                                obj1.CreatedBy = AuthenticationHelper.UserID;
                                obj1.TemplateContent = strExporttoWord.ToString();
                                obj1.CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User;

                                if (updatestatus)
                                    obj1.Status = 2;
                                else
                                    obj1.Status = 3;

                                obj1.OrderID = cnt; // 1;
                                //obj1.Status = 1;
                                //obj1.OrderID = 1;
                                entities.ContractTemplateTransactionHistories.Add(obj1);
                                entities.SaveChanges();
                            }
                            #endregion
                        }
                        if (RoleID == 4)
                        {
                            var queryResult1 = (from row in entities.Cont_tbl_ContractInstance
                                                where row.ID == TemplateID
                                               && row.IsDeleted == false
                                                select row).FirstOrDefault();
                            if (queryResult1 != null)
                            {
                                if (queryResult1.TemplateID != null && queryResult1.TemplateID != -1)
                                {

                                }
                                else
                                {
                                    if (flagcheck == false)
                                    {
                                        Cont_tbl_ContractStatusTransaction newStatusRecord = new Cont_tbl_ContractStatusTransaction()
                                        {
                                            CustomerID = customerID,
                                            ContractID = Convert.ToInt32(TemplateID),
                                            StatusID = 3,
                                            StatusChangeOn = DateTime.Now,
                                            IsActive = true,
                                            CreatedBy = UserID,
                                            UpdatedBy = UserID,
                                        };
                                        ContractManagement.CreateContractStatusTransaction(newStatusRecord);
                                    }
                                }
                            }
                        }
                    }
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "CloseModel();", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "validationmsg();", true);
                }
            }
            else
            {
                int TemplateID = Convert.ToInt32(Request.QueryString["TID"]);
                int SectionID = Convert.ToInt32(Request.QueryString["SID"]);
                int StatusID = -1;

                if (Request.QueryString["StatusID"] != null)
                {
                    StatusID = Convert.ToInt32(Request.QueryString["StatusID"]);
                }

                bool validation = true;
                if (RoleID == 3)
                {
                    if (StatusID == 1)
                    {
                        if (ddlUser.SelectedValue == "" || ddlUser.SelectedValue == "-1")
                        {
                            validation = false;
                        }
                    }
                }
                if (validation)
                {
                    string ContractVersion = ContractTemplateManagement.getVersion(Convert.ToInt32(TemplateID), customerID);

                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        if (RoleID == 3)//&& StatusID != 15
                        {
                            int selecteduserID = Convert.ToInt32(ddlUser.SelectedValue);
                            ContractTemplateAssignment ContracttemplateAssignment = new ContractTemplateAssignment()
                            {
                                ContractTemplateInstanceID = Convert.ToInt32(TemplateID),
                                RoleID = 4,
                                UserID = selecteduserID,
                                IsActive = true,
                                SectionID = Convert.ToInt32(SectionID),
                                Visibility = 1
                            };
                            ContractTemplateManagement.CreateContractTemplateAssignment(ContracttemplateAssignment);
                        }

                        ContractTemplateTransaction obj = new ContractTemplateTransaction();
                        obj.ContractTemplateInstanceID = TemplateID;
                        obj.CreatedOn = DateTime.Now;
                        obj.CreatedBy = UserID;
                        obj.ContractTemplateContent = theEditor.Content;
                        obj.SectionID = SectionID;
                        obj.Version = ContractVersion;
                        obj.CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User;
                        if (RoleID == 4)
                        {
                            obj.Status = 3;
                        }
                        else if (RoleID == 3)// && StatusID != 15
                        {
                            obj.Status = 2;
                        }
                        if (Convert.ToInt32(ViewState["TransactionOrderID"]) > 0)
                        {
                            obj.OrderID = Convert.ToInt32(ViewState["TransactionOrderID"]) + 1;
                        }
                        entities.ContractTemplateTransactions.Add(obj);
                        entities.SaveChanges();
                        int TransactionID = Convert.ToInt32(obj.Id);
                        if (TransactionID > 0)
                        {
                            if (!string.IsNullOrEmpty(getHistryInsert.Text))
                            {
                                UpdateTransactionHistory(TemplateID, TransactionID, getHistryInsert.Text, "I");
                            }
                            if (!string.IsNullOrEmpty(getHistryDel.Text))
                            {
                                UpdateTransactionHistory(TemplateID, TransactionID, getHistryDel.Text, "D");
                            }
                            #region set transaction history

                            var entitiesData = (from row in entities.Cont_SP_GetContractSectionsDetail(customerID, Convert.ToInt32(TemplateID))
                                                select row).ToList();


                            var data1 = (from row in entities.Sp_GetContractTemplateDetailTransaction()
                                         where row.UserID == UserID
                                         && row.ContractTemplateInstanceID == TemplateID
                                         && row.RoleID == RoleID
                                         select row).ToList();
                            if (data1.Count > 0)
                            {
                                var data = (from row in data1
                                            join row1 in entities.Cont_tbl_ContractTemplateSectionMapping
                                            on row.sectionID equals row1.SectionID
                                            where row1.ContractTemplateInstanceID == TemplateID
                                            select new getTemplateTransactionDetail
                                            {
                                                TemplateTransactionID = row.TemplateTransactionID,
                                                TemplateInstanceID = row.ContractTemplateInstanceID,
                                                ComplianceStatusID = row.ComplianceStatusID,
                                                CreatedOn = row.CreatedOn,
                                                Status = row.Status,
                                                StatusChangedOn = row.StatusChangedOn,
                                                TemplateContent = row.ContractTemplateContent,
                                                RoleID = row.RoleID,
                                                UserID = row.UserID,
                                                sectionID = Convert.ToInt32(row.sectionID),
                                                orderID = Convert.ToInt32(row1.SectionOrder),
                                                visibilty = Convert.ToInt32(row1.Headervisibilty),
                                                sectionHeader = row.SectionHeader
                                            }).OrderBy(x => x.orderID).ToList();

                                System.Text.StringBuilder strExporttoWord = new System.Text.StringBuilder();

                                data.ForEach(eachSection =>
                                {
                                    if (eachSection.visibilty == 1)
                                    {
                                        strExporttoWord.Append(@"<div>");
                                        strExporttoWord.Append(@"<p><B>" + eachSection.sectionHeader + "</B></p>");
                                        strExporttoWord.Append(@"</div>");
                                    }

                                    strExporttoWord.Append(@"<div>");
                                    strExporttoWord.Append(@"<p>" + eachSection.TemplateContent + "</p>");
                                    strExporttoWord.Append(@"</div>");
                                });

                                ContractTemplateTransactionHistory obj1 = new ContractTemplateTransactionHistory();
                                obj1.ContractTemplateID = TemplateID;
                                obj1.CreatedOn = DateTime.Now;
                                obj1.CreatedBy = AuthenticationHelper.UserID;
                                obj1.TemplateContent = strExporttoWord.ToString();
                                obj1.CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User;
                                obj1.Status = 1;
                                obj1.OrderID = 1;
                                entities.ContractTemplateTransactionHistories.Add(obj1);
                                entities.SaveChanges();
                            }
                            #endregion
                        }
                        if (RoleID == 4)
                        {
                            var queryResult1 = (from row in entities.Cont_tbl_ContractInstance
                                                where row.ID == TemplateID
                                               && row.IsDeleted == false
                                                select row).FirstOrDefault();
                            if (queryResult1 != null)
                            {
                                if (queryResult1.TemplateID != null && queryResult1.TemplateID != -1)
                                {

                                }
                                else
                                {
                                    Cont_tbl_ContractStatusTransaction newStatusRecord = new Cont_tbl_ContractStatusTransaction()
                                    {
                                        CustomerID = customerID,
                                        ContractID = Convert.ToInt32(TemplateID),
                                        StatusID = 3,
                                        StatusChangeOn = DateTime.Now,
                                        IsActive = true,
                                        CreatedBy = UserID,
                                        UpdatedBy = UserID,
                                    };
                                    ContractManagement.CreateContractStatusTransaction(newStatusRecord);
                                }
                            }
                        }
                        if (RoleID == 3)
                        {

                            var queryResult1 = (from row in entities.Cont_tbl_ContractInstance
                                                where row.ID == TemplateID
                                               && row.IsDeleted == false
                                                select row).FirstOrDefault();
                            if (queryResult1 != null)
                            {
                                if (queryResult1.TemplateID != null && queryResult1.TemplateID != -1)
                                {
                                    int SectionTemplateID = getContractStatusID(Convert.ToInt32(TemplateID));
                                    if (SectionTemplateID == 2)
                                    {
                                        Cont_tbl_ContractStatusTransaction newStatusRecord = new Cont_tbl_ContractStatusTransaction()
                                        {
                                            CustomerID = customerID,
                                            ContractID = Convert.ToInt32(TemplateID),
                                            StatusID = 2,
                                            StatusChangeOn = DateTime.Now,
                                            IsActive = true,
                                            CreatedBy = UserID,
                                            UpdatedBy = UserID,
                                        };
                                        ContractManagement.CreateContractStatusTransaction(newStatusRecord);
                                    }
                                    if (SectionTemplateID == 3)
                                    {
                                        Cont_tbl_ContractStatusTransaction newStatusRecord = new Cont_tbl_ContractStatusTransaction()
                                        {
                                            CustomerID = customerID,
                                            ContractID = Convert.ToInt32(TemplateID),
                                            StatusID = 3,
                                            StatusChangeOn = DateTime.Now,
                                            IsActive = true,
                                            CreatedBy = UserID,
                                            UpdatedBy = UserID,
                                        };
                                        ContractManagement.CreateContractStatusTransaction(newStatusRecord);
                                    }
                                }
                                else
                                {
                                    Cont_tbl_ContractStatusTransaction newStatusRecord = new Cont_tbl_ContractStatusTransaction()
                                    {
                                        CustomerID = customerID,
                                        ContractID = Convert.ToInt32(TemplateID),
                                        StatusID = 2,
                                        StatusChangeOn = DateTime.Now,
                                        IsActive = true,
                                        CreatedBy = UserID,
                                        UpdatedBy = UserID,
                                    };
                                    ContractManagement.CreateContractStatusTransaction(newStatusRecord);
                                }
                            }
                        }
                    }
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "CloseModel();", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "validationmsg();", true);
                }
            }
        }
        //protected void btnSubmit_Click(object sender, EventArgs e)
        //{
        //    int customerID = -1;
        //    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

        //    int UserID = Convert.ToInt32(Request.QueryString["UID"]);
        //    int RoleID = Convert.ToInt32(Request.QueryString["RID"]);
        //    int TemplateID = Convert.ToInt32(Request.QueryString["TID"]);
        //    int SectionID = Convert.ToInt32(Request.QueryString["SID"]);
        //    int StatusID = -1;

        //    if (Request.QueryString["StatusID"] != null)
        //    {
        //        StatusID = Convert.ToInt32(Request.QueryString["StatusID"]);
        //    }

        //    bool validation = true;
        //    if (RoleID == 3)
        //    {
        //        if (StatusID == 1)
        //        {
        //            if (ddlUser.SelectedValue == "" || ddlUser.SelectedValue == "-1")
        //            {
        //                validation = false;
        //            }
        //        }
        //    }
        //    if (validation)
        //    {
        //        string ContractVersion = ContractTemplateManagement.getVersion(Convert.ToInt32(TemplateID), customerID);

        //        using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //        {
        //            if (RoleID == 3)//&& StatusID != 15
        //            {
        //                int selecteduserID = Convert.ToInt32(ddlUser.SelectedValue);
        //                ContractTemplateAssignment ContracttemplateAssignment = new ContractTemplateAssignment()
        //                {
        //                    ContractTemplateInstanceID = Convert.ToInt32(TemplateID),
        //                    RoleID = 4,
        //                    UserID = selecteduserID,
        //                    IsActive = true,
        //                    SectionID = Convert.ToInt32(SectionID),
        //                    Visibility = 1
        //                };
        //                ContractTemplateManagement.CreateContractTemplateAssignment(ContracttemplateAssignment);
        //            }

        //            ContractTemplateTransaction obj = new ContractTemplateTransaction();
        //            obj.ContractTemplateInstanceID = TemplateID;
        //            obj.CreatedOn = DateTime.Now;
        //            obj.CreatedBy = UserID;
        //            obj.ContractTemplateContent = theEditor.Content;
        //            obj.SectionID = SectionID;
        //            obj.Version = ContractVersion;
        //            obj.CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User;
        //            if (RoleID == 4)
        //            {
        //                obj.Status = 3;
        //            }
        //            else if (RoleID == 3)// && StatusID != 15
        //            {
        //                obj.Status = 2;
        //            }
        //            if (Convert.ToInt32(ViewState["TransactionOrderID"]) > 0)
        //            {
        //                obj.OrderID = Convert.ToInt32(ViewState["TransactionOrderID"]) + 1;
        //            }
        //            entities.ContractTemplateTransactions.Add(obj);
        //            entities.SaveChanges();
        //            int TransactionID = Convert.ToInt32(obj.Id);
        //            if (TransactionID > 0)
        //            {
        //                if (!string.IsNullOrEmpty(getHistryInsert.Text))
        //                {
        //                    UpdateTransactionHistory(TemplateID, TransactionID, getHistryInsert.Text, "I");
        //                }
        //                if (!string.IsNullOrEmpty(getHistryDel.Text))
        //                {
        //                    UpdateTransactionHistory(TemplateID, TransactionID, getHistryDel.Text, "D");
        //                }
        //                #region set transaction history

        //                var entitiesData = (from row in entities.Cont_SP_GetContractSectionsDetail(customerID, Convert.ToInt32(TemplateID))
        //                                    select row).ToList();


        //                var data1 = (from row in entities.Sp_GetContractTemplateDetailTransaction()
        //                             where row.UserID == UserID
        //                             && row.ContractTemplateInstanceID == TemplateID
        //                             && row.RoleID == RoleID
        //                             select row).ToList();
        //                if (data1.Count > 0)
        //                {
        //                    var data = (from row in data1
        //                                join row1 in entities.Cont_tbl_ContractTemplateSectionMapping
        //                                on row.sectionID equals row1.SectionID
        //                                where row1.ContractTemplateInstanceID == TemplateID
        //                                select new getTemplateTransactionDetail
        //                                {
        //                                    TemplateTransactionID = row.TemplateTransactionID,
        //                                    TemplateInstanceID = row.ContractTemplateInstanceID,
        //                                    ComplianceStatusID = row.ComplianceStatusID,
        //                                    CreatedOn = row.CreatedOn,
        //                                    Status = row.Status,
        //                                    StatusChangedOn = row.StatusChangedOn,
        //                                    TemplateContent = row.ContractTemplateContent,
        //                                    RoleID = row.RoleID,
        //                                    UserID = row.UserID,
        //                                    sectionID = Convert.ToInt32(row.sectionID),
        //                                    orderID = Convert.ToInt32(row1.SectionOrder),
        //                                    visibilty = Convert.ToInt32(row1.Headervisibilty),
        //                                    sectionHeader = row.SectionHeader
        //                                }).OrderBy(x => x.orderID).ToList();

        //                    System.Text.StringBuilder strExporttoWord = new System.Text.StringBuilder();

        //                    data.ForEach(eachSection =>
        //                    {
        //                        if (eachSection.visibilty == 1)
        //                        {
        //                            strExporttoWord.Append(@"<div>");
        //                            strExporttoWord.Append(@"<p><B>" + eachSection.sectionHeader + "</B></p>");
        //                            strExporttoWord.Append(@"</div>");
        //                        }

        //                        strExporttoWord.Append(@"<div>");
        //                        strExporttoWord.Append(@"<p>" + eachSection.TemplateContent + "</p>");
        //                        strExporttoWord.Append(@"</div>");
        //                    });

        //                    ContractTemplateTransactionHistory obj1 = new ContractTemplateTransactionHistory();
        //                    obj1.ContractTemplateID = TemplateID;
        //                    obj1.CreatedOn = DateTime.Now;
        //                    obj1.CreatedBy = AuthenticationHelper.UserID;
        //                    obj1.TemplateContent = strExporttoWord.ToString();
        //                    obj1.CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User;
        //                    obj1.Status = 1;
        //                    obj1.OrderID = 1;
        //                    entities.ContractTemplateTransactionHistories.Add(obj1);
        //                    entities.SaveChanges();
        //                }
        //                #endregion
        //            }
        //            if (RoleID == 4)
        //            {
        //                var queryResult1 = (from row in entities.Cont_tbl_ContractInstance
        //                                    where row.ID == TemplateID
        //                                   && row.IsDeleted == false
        //                                    select row).FirstOrDefault();
        //                if (queryResult1 != null)
        //                {
        //                    if (queryResult1.TemplateID != null && queryResult1.TemplateID != -1)
        //                    {

        //                    }
        //                    else
        //                    {
        //                        Cont_tbl_ContractStatusTransaction newStatusRecord = new Cont_tbl_ContractStatusTransaction()
        //                        {
        //                            CustomerID = customerID,
        //                            ContractID = Convert.ToInt32(TemplateID),
        //                            StatusID = 3,
        //                            StatusChangeOn = DateTime.Now,
        //                            IsActive = true,
        //                            CreatedBy = UserID,
        //                            UpdatedBy = UserID,
        //                        };
        //                        ContractManagement.CreateContractStatusTransaction(newStatusRecord);
        //                    }
        //                }
        //            }
        //            if (RoleID == 3)
        //            {

        //                var queryResult1 = (from row in entities.Cont_tbl_ContractInstance
        //                                    where row.ID == TemplateID
        //                                   && row.IsDeleted == false
        //                                    select row).FirstOrDefault();
        //                if (queryResult1 != null)
        //                {
        //                    if (queryResult1.TemplateID != null && queryResult1.TemplateID != -1)
        //                    {
        //                        int SectionTemplateID = getContractStatusID(Convert.ToInt32(TemplateID));
        //                        if (SectionTemplateID == 2)
        //                        {
        //                            Cont_tbl_ContractStatusTransaction newStatusRecord = new Cont_tbl_ContractStatusTransaction()
        //                            {
        //                                CustomerID = customerID,
        //                                ContractID = Convert.ToInt32(TemplateID),
        //                                StatusID = 2,
        //                                StatusChangeOn = DateTime.Now,
        //                                IsActive = true,
        //                                CreatedBy = UserID,
        //                                UpdatedBy = UserID,
        //                            };
        //                            ContractManagement.CreateContractStatusTransaction(newStatusRecord);
        //                        }
        //                        if (SectionTemplateID == 3)
        //                        {
        //                            Cont_tbl_ContractStatusTransaction newStatusRecord = new Cont_tbl_ContractStatusTransaction()
        //                            {
        //                                CustomerID = customerID,
        //                                ContractID = Convert.ToInt32(TemplateID),
        //                                StatusID = 3,
        //                                StatusChangeOn = DateTime.Now,
        //                                IsActive = true,
        //                                CreatedBy = UserID,
        //                                UpdatedBy = UserID,
        //                            };
        //                            ContractManagement.CreateContractStatusTransaction(newStatusRecord);
        //                        }
        //                    }
        //                    else
        //                    {
        //                        Cont_tbl_ContractStatusTransaction newStatusRecord = new Cont_tbl_ContractStatusTransaction()
        //                        {
        //                            CustomerID = customerID,
        //                            ContractID = Convert.ToInt32(TemplateID),
        //                            StatusID = 2,
        //                            StatusChangeOn = DateTime.Now,
        //                            IsActive = true,
        //                            CreatedBy = UserID,
        //                            UpdatedBy = UserID,
        //                        };
        //                        ContractManagement.CreateContractStatusTransaction(newStatusRecord);
        //                    }
        //                }
        //            }
        //        }
        //        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "CloseModel();", true);
        //    }
        //    else
        //    {
        //        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "validationmsg();", true);
        //    }
        //}

        public int getContractStatusID(int ContractTemplateId)
        {
            int StatusID = -1;
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.Cont_View_RecentContractStatusTransaction
                            join row1 in entities.Cont_tbl_ContractInstance
                            on row.ContractID equals row1.ID
                            where row1.ID == ContractTemplateId
                                                   && row1.IsDeleted == false
                                                   && row.CustomerID == customerID
                            select row).FirstOrDefault();
                if (data != null)
                {
                    if (data.ContractStatusID != null)
                        StatusID = Convert.ToInt32(data.ContractStatusID);
                }
            }
            return StatusID;
        }


        public class getTemplateTransactionDetail
        {
            public long TemplateTransactionID { get; set; }
            public long TemplateInstanceID { get; set; }
            public long ComplianceStatusID { get; set; }
            public System.DateTime CreatedOn { get; set; }
            public string Status { get; set; }
            public Nullable<System.DateTime> StatusChangedOn { get; set; }
            public string TemplateContent { get; set; }
            public long RoleID { get; set; }
            public long UserID { get; set; }
            public int sectionID { get; set; }
            public int orderID { get; set; }
            public int visibilty { get; set; }
            public string sectionHeader { get; set; }
        }


        public bool UpdateTransactionHistory(int TemplateId, int TransactionID, string getInput, string TransactionType)
        {
            bool result = false;

            if (!string.IsNullOrEmpty(getInput))
            {
                var getdata = getInput;
                string[] stringSeparators = new string[] { "endend" };
                var resultdata = getdata.Split(stringSeparators, StringSplitOptions.None);
                foreach (var item in resultdata)
                {
                    if (!string.IsNullOrEmpty(item))
                    {
                        string[] stringSeparators1 = new string[] { "andand" };
                        var result1 = item.Split(stringSeparators1, StringSplitOptions.None);
                        string inputtext = result1[0];
                        string author = result1[1];
                        string title = result1[2];
                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        {
                            cont_ContractTemplateTransactionHistoryMapping obj = new cont_ContractTemplateTransactionHistoryMapping
                            {
                                Author = result1[1],
                                Inputtext = result1[0],
                                Title = result1[2],
                                CreatedBy = Convert.ToInt64(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID),
                                ContractTemplateInstanceID = TemplateId,
                                TemplateTransactionID = TransactionID,
                                TransactionType = TransactionType,
                                CreatedOn = DateTime.Now,
                                IsActive = true
                            };
                            entities.cont_ContractTemplateTransactionHistoryMapping.Add(obj);
                            entities.SaveChanges();
                        }
                    }
                }
            }
            return result;
        }

        protected void RadEditor1_ExportContent(object sender, EditorExportingArgs e)
        {
            ExportType exportType = e.ExportType;

            if (exportType == ExportType.Word)
            {
                string exportedOutput = e.ExportOutput;

                Byte[] output = Encoding.Default.GetBytes(exportedOutput);

                DocxFormatProvider docxProvider = new DocxFormatProvider();
                RadFlowDocument document = docxProvider.Import(output);

                Header defaultHeader = document.Sections.First().Headers.Add();
                Paragraph defaultHeaderParagraph = defaultHeader.Blocks.AddParagraph();
                defaultHeaderParagraph.TextAlignment = Alignment.Right;
                defaultHeaderParagraph.Inlines.AddRun("This is a sample header.");

                Footer defaultFooter = document.Sections.First().Footers.Add();
                Paragraph defaultFooterParagraph = defaultFooter.Blocks.AddParagraph();
                defaultFooterParagraph.TextAlignment = Alignment.Right;
                defaultFooterParagraph.Inlines.AddRun("This is a sample footer.");

                Byte[] modifiedOutput = docxProvider.Export(document);
                string finalOutput = Encoding.Default.GetString(modifiedOutput, 0, modifiedOutput.Length);

                e.ExportOutput = finalOutput;
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            theEditor.ExportToDocx();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            divHtml.InnerHtml = theEditor.Content;
        }

        public static TemplateTransaction GetDetail(int TID, int RoleID, int UserID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.TemplateTransactions
                            where row.Id == TID
                            select row).OrderByDescending(x => x.CreatedOn).FirstOrDefault();

                return data;
            }
        }

        protected void btnforApproval_Click(object sender, EventArgs e)
        {
            int UserID = Convert.ToInt32(Request.QueryString["UID"]);
            int RoleID = Convert.ToInt32(Request.QueryString["RID"]);
            int TemplateID = Convert.ToInt32(Request.QueryString["TID"]);
            if (RoleID == 3)
            {
                if (ddlUser.SelectedValue != "")
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        var Approvaldata = (from row in entities.TemplateAssignments
                                            where row.TemplateInstanceID == TemplateID
                                            && row.RoleID == 6
                                            select row).ToList();
                        if (Approvaldata.Count > 0)
                        {
                            foreach (var item in Approvaldata)
                            {
                                TemplateAssignment checkdata = (from row in entities.TemplateAssignments
                                                                where row.TemplateInstanceID == TemplateID
                                                                && row.RoleID == 6
                                                                && row.ID == item.ID
                                                                select row).FirstOrDefault();
                                if (checkdata != null)
                                {
                                    checkdata.IsActive = false;
                                    entities.SaveChanges();
                                }
                            }
                        }

                        int selectedUserID = Convert.ToInt32(ddlUser.SelectedValue);

                        TemplateAssignment data = (from row in entities.TemplateAssignments
                                                   where row.TemplateInstanceID == TemplateID
                                                   && row.RoleID == 6
                                                   && row.UserID == selectedUserID
                                                   select row).FirstOrDefault();
                        if (data != null)
                        {
                            data.IsActive = true;
                            entities.SaveChanges();
                        }
                        else
                        {
                            TemplateAssignment templateAssignment = new TemplateAssignment()
                            {
                                TemplateInstanceID = TemplateID,
                                RoleID = 6,
                                UserID = Convert.ToInt32(ddlUser.SelectedValue),
                                IsActive = true
                            };
                            ContractTemplateManagement.CreateTemplateAssignment(templateAssignment);
                        }
                    }
                }
            }
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                TemplateTransaction obj = new TemplateTransaction();
                obj.TemplateInstanceID = TemplateID;
                obj.CreatedOn = DateTime.Now;
                obj.CreatedBy = UserID;
                obj.TemplateContent = theEditor.Content;
                obj.Status = 4;
                obj.CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User;
                if (Convert.ToInt32(ViewState["TransactionOrderID"]) > 0)
                {
                    obj.OrderID = Convert.ToInt32(ViewState["TransactionOrderID"]) + 1;
                }
                entities.TemplateTransactions.Add(obj);
                entities.SaveChanges();
                int TransactionID = Convert.ToInt32(obj.Id);
                if (TransactionID > 0)
                {
                    if (!string.IsNullOrEmpty(getHistryInsert.Text))
                    {
                        UpdateTransactionHistory(TemplateID, TransactionID, getHistryInsert.Text, "I");
                    }
                    if (!string.IsNullOrEmpty(getHistryDel.Text))
                    {
                        UpdateTransactionHistory(TemplateID, TransactionID, getHistryDel.Text, "D");
                    }
                }
            }
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "CloseModel();", true);
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            int UserID = Convert.ToInt32(Request.QueryString["UID"]);
            int RoleID = Convert.ToInt32(Request.QueryString["RID"]);
            int TemplateID = Convert.ToInt32(Request.QueryString["TID"]);        
            int SectionID = Convert.ToInt32(Request.QueryString["SID"]);
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            string ContractVersion = ContractTemplateManagement.getVersion(Convert.ToInt32(TemplateID), customerID);
            
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                ContractTemplateTransaction obj = new ContractTemplateTransaction();
                obj.ContractTemplateInstanceID = TemplateID;
                obj.CreatedOn = DateTime.Now;
                obj.CreatedBy = UserID;
                obj.ContractTemplateContent = theEditor.Content;
                obj.Status = 15;
                obj.SectionID = SectionID;
                obj.Version = ContractVersion;
                obj.CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User;
                if (Convert.ToInt32(ViewState["TransactionOrderID"]) > 0)
                {
                    obj.OrderID = Convert.ToInt32(ViewState["TransactionOrderID"]) + 1;
                }
                entities.ContractTemplateTransactions.Add(obj);
                entities.SaveChanges();
                int TransactionID = Convert.ToInt32(obj.Id);
                if (TransactionID > 0)
                {
                    if (!string.IsNullOrEmpty(getHistryInsert.Text))
                    {
                        UpdateTransactionHistory(TemplateID, TransactionID, getHistryInsert.Text, "I");
                    }
                    if (!string.IsNullOrEmpty(getHistryDel.Text))
                    {
                        UpdateTransactionHistory(TemplateID, TransactionID, getHistryDel.Text, "D");
                    }

                    #region set transaction history
                    
                    var entitiesData = (from row in entities.Cont_SP_GetContractSectionsDetail(customerID, Convert.ToInt32(TemplateID))
                                        select row).ToList();


                    var data1 = (from row in entities.Sp_GetContractTemplateDetailTransaction()
                                 where row.UserID == UserID
                                 && row.ContractTemplateInstanceID == TemplateID
                                 && row.RoleID == RoleID
                                 select row).ToList();
                    if (data1.Count > 0)
                    {
                        var data = (from row in data1
                                    join row1 in entities.Cont_tbl_ContractTemplateSectionMapping
                                    on row.sectionID equals row1.SectionID
                                    where row1.ContractTemplateInstanceID == TemplateID
                                    select new getTemplateTransactionDetail
                                    {
                                        TemplateTransactionID = row.TemplateTransactionID,
                                        TemplateInstanceID = row.ContractTemplateInstanceID,
                                        ComplianceStatusID = row.ComplianceStatusID,
                                        CreatedOn = row.CreatedOn,
                                        Status = row.Status,
                                        StatusChangedOn = row.StatusChangedOn,
                                        TemplateContent = row.ContractTemplateContent,
                                        RoleID = row.RoleID,
                                        UserID = row.UserID,
                                        sectionID = Convert.ToInt32(row.sectionID),
                                        orderID = Convert.ToInt32(row1.SectionOrder),
                                        visibilty = Convert.ToInt32(row1.Headervisibilty),
                                        sectionHeader = row.SectionHeader
                                    }).OrderBy(x => x.orderID).ToList();

                        System.Text.StringBuilder strExporttoWord = new System.Text.StringBuilder();

                        data.ForEach(eachSection =>
                        {
                            if (eachSection.visibilty == 1)
                            {
                                strExporttoWord.Append(@"<div>");
                                strExporttoWord.Append(@"<p><B>" + eachSection.sectionHeader + "</B></p>");
                                strExporttoWord.Append(@"</div>");
                            }
                            strExporttoWord.Append(@"<div>");
                            strExporttoWord.Append(@"<p>" + eachSection.TemplateContent + "</p>");
                            strExporttoWord.Append(@"</div>");
                        });

                        ContractTemplateTransactionHistory obj1 = new ContractTemplateTransactionHistory();
                        obj1.ContractTemplateID = TemplateID;
                        obj1.CreatedOn = DateTime.Now;
                        obj1.CreatedBy = AuthenticationHelper.UserID;
                        obj1.TemplateContent = strExporttoWord.ToString();
                        obj1.CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User;
                        obj1.Status = 1;
                        obj1.OrderID = 1;
                        entities.ContractTemplateTransactionHistories.Add(obj1);
                        entities.SaveChanges();
                    }
                    #endregion
                }
            }

            if (RoleID == 6)
            {
                int SectionTemplateID = getContractStatusID(Convert.ToInt32(TemplateID));
                if (SectionTemplateID == 15)
                {                   
                    Cont_tbl_ContractStatusTransaction newStatusRecord = new Cont_tbl_ContractStatusTransaction()
                    {
                        CustomerID = customerID,
                        ContractID = Convert.ToInt32(TemplateID),
                        StatusID = 15,
                        StatusChangeOn = DateTime.Now,
                        IsActive = true,
                        CreatedBy = UserID,
                        UpdatedBy = UserID,
                    };
                    ContractManagement.CreateContractStatusTransaction(newStatusRecord);
                }
            }

            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "CloseModel();", true);
        }

        protected void Button4_Click(object sender, EventArgs e)
        {
            int UserID = Convert.ToInt32(Request.QueryString["UID"]);
            int RoleID = Convert.ToInt32(Request.QueryString["RID"]);
            int TemplateID = Convert.ToInt32(Request.QueryString["TID"]);
            int SectionID = Convert.ToInt32(Request.QueryString["SID"]);
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            string ContractVersion = ContractTemplateManagement.getVersion(Convert.ToInt32(TemplateID), customerID);

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                bool updatestatus = false;
                int nextorderId = 0;
                var objRecord = (from row in entities.ContractTemplateAssignments
                                 where row.ContractTemplateInstanceID == TemplateID
                                 && row.SectionID == SectionID
                                 && row.RoleID == RoleID
                                 select row).OrderBy(x => x.ApproveOrder).ToList();

                if (objRecord.Count > 1)
                {
                    ContractTemplateAssignment data = (from row in entities.ContractTemplateAssignments
                                                       where row.ContractTemplateInstanceID == TemplateID
                                                       && row.RoleID == RoleID
                                                       && row.UserID == UserID
                                                       && row.IsActive == true
                                                         && row.SectionID == SectionID
                                                       select row).FirstOrDefault();
                    if (data != null)
                    {
                        int orderID = Convert.ToInt32(data.ApproveOrder);
                        nextorderId = orderID + 1;
                        var getdetails = objRecord.Where(x => x.ApproveOrder == nextorderId && x.IsActive == false && x.IsAction == 0).FirstOrDefault();
                        if (getdetails != null)
                        {
                            updatestatus = true;
                            data.IsAction = 1;
                        }
                    }
                }

                ContractTemplateTransaction obj = new ContractTemplateTransaction();
                obj.ContractTemplateInstanceID = TemplateID;
                obj.CreatedOn = DateTime.Now;
                obj.CreatedBy = UserID;
                obj.ContractTemplateContent = theEditor.Content;
                //obj.Status = 5;
                if (updatestatus)
                {
                    obj.Status = 4;
                    ContractTemplateAssignment data = (from row in entities.ContractTemplateAssignments
                                                       where row.ContractTemplateInstanceID == TemplateID
                                                       && row.RoleID == RoleID
                                                       && row.ApproveOrder == nextorderId
                                                       && row.IsActive == false
                                                         && row.SectionID == SectionID
                                                       select row).FirstOrDefault();
                    if (data != null)
                    {
                        data.IsActive = true;
                    }
                }
                else
                    obj.Status = 5;


                obj.SectionID = SectionID;
                obj.Version = ContractVersion;
                obj.CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User;
                if (Convert.ToInt32(ViewState["TransactionOrderID"]) > 0)
                {
                    obj.OrderID = Convert.ToInt32(ViewState["TransactionOrderID"]) + 1;
                }
                entities.ContractTemplateTransactions.Add(obj);
                entities.SaveChanges();
                int TransactionID = Convert.ToInt32(obj.Id);
                if (TransactionID > 0)
                {
                    if (!string.IsNullOrEmpty(getHistryInsert.Text))
                    {
                        UpdateTransactionHistory(TemplateID, TransactionID, getHistryInsert.Text, "I");
                    }
                    if (!string.IsNullOrEmpty(getHistryDel.Text))
                    {
                        UpdateTransactionHistory(TemplateID, TransactionID, getHistryDel.Text, "D");
                    }
                }

                var queryResult1 = (from row in entities.Cont_tbl_ContractInstance
                                    where row.ID == TemplateID
                                   && row.IsDeleted == false
                                    select row).FirstOrDefault();
                if (queryResult1 != null)
                {
                    if (queryResult1.TemplateID != null && queryResult1.TemplateID != -1)
                    {

                    }
                    else
                    {
                        Cont_tbl_ContractStatusTransaction newStatusRecord = new Cont_tbl_ContractStatusTransaction()
                        {
                            CustomerID = customerID,
                            ContractID = Convert.ToInt32(TemplateID),
                            StatusID = 5,
                            StatusChangeOn = DateTime.Now,
                            IsActive = true,
                            CreatedBy = UserID,
                            UpdatedBy = UserID,
                        };
                        ContractManagement.CreateContractStatusTransaction(newStatusRecord);
                    }
                }                
            }

            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "CloseModel();", true);
        }
        
        protected void btnforClosed_Click(object sender, EventArgs e)
        {
            int UserID = Convert.ToInt32(Request.QueryString["UID"]);
            int RoleID = Convert.ToInt32(Request.QueryString["RID"]);
            int TemplateID = Convert.ToInt32(Request.QueryString["TID"]);

            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            string ContractVersion = ContractTemplateManagement.getVersion(Convert.ToInt32(TemplateID), customerID);
            
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int StatusID = -1;
                if (Request.QueryString["StatusID"] != null)
                {
                    StatusID = Convert.ToInt32(Request.QueryString["StatusID"]);
                }
                int SectionID = Convert.ToInt32(Request.QueryString["SID"]);
                ContractTemplateTransaction obj = new ContractTemplateTransaction();
                obj.ContractTemplateInstanceID = TemplateID;
                obj.CreatedOn = DateTime.Now;
                obj.CreatedBy = UserID;
                obj.Version = ContractVersion;
                obj.ContractTemplateContent = theEditor.Content;
                obj.SectionID = SectionID;
                obj.CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User;

                if (RoleID == 3 && StatusID != 15 && StatusID != 17)
                    obj.Status = StatusID;
                else
                    obj.Status = StatusID;
                //obj.Status = 14;
                if (Convert.ToInt32(ViewState["TransactionOrderID"]) > 0)
                {
                    obj.OrderID = Convert.ToInt32(ViewState["TransactionOrderID"]) + 1;
                }
                entities.ContractTemplateTransactions.Add(obj);
                entities.SaveChanges();
                int TransactionID = Convert.ToInt32(obj.Id);
                if (TransactionID > 0)
                {
                    if (!string.IsNullOrEmpty(getHistryInsert.Text))
                    {
                        UpdateTransactionHistory(TemplateID, TransactionID, getHistryInsert.Text, "I");
                    }
                    if (!string.IsNullOrEmpty(getHistryDel.Text))
                    {
                        UpdateTransactionHistory(TemplateID, TransactionID, getHistryDel.Text, "D");
                    }

                    #region set transaction history
                  
                    var entitiesData = (from row in entities.Cont_SP_GetContractSectionsDetail(customerID, Convert.ToInt32(TemplateID))
                                        select row).ToList();


                    var data1 = (from row in entities.Sp_GetContractTemplateDetailTransaction()
                                 where row.UserID == UserID
                                 && row.ContractTemplateInstanceID == TemplateID
                                 && row.RoleID == RoleID
                                 select row).ToList();
                    if (data1.Count > 0)
                    {
                        var data = (from row in data1
                                    join row1 in entities.Cont_tbl_ContractTemplateSectionMapping
                                    on row.sectionID equals row1.SectionID
                                    where row1.ContractTemplateInstanceID == TemplateID
                                    select new getTemplateTransactionDetail
                                    {
                                        TemplateTransactionID = row.TemplateTransactionID,
                                        TemplateInstanceID = row.ContractTemplateInstanceID,
                                        ComplianceStatusID = row.ComplianceStatusID,
                                        CreatedOn = row.CreatedOn,
                                        Status = row.Status,
                                        StatusChangedOn = row.StatusChangedOn,
                                        TemplateContent = row.ContractTemplateContent,
                                        RoleID = row.RoleID,
                                        UserID = row.UserID,
                                        sectionID = Convert.ToInt32(row.sectionID),
                                        orderID = Convert.ToInt32(row1.SectionOrder),
                                        visibilty = Convert.ToInt32(row1.Headervisibilty),
                                        sectionHeader = row.SectionHeader
                                    }).OrderBy(x => x.orderID).ToList();

                        System.Text.StringBuilder strExporttoWord = new System.Text.StringBuilder();

                        data.ForEach(eachSection =>
                        {
                            if (eachSection.visibilty == 1)
                            {
                                strExporttoWord.Append(@"<div>");
                                strExporttoWord.Append(@"<p><B>" + eachSection.sectionHeader + "</B></p>");
                                strExporttoWord.Append(@"</div>");
                            }

                            strExporttoWord.Append(@"<div>");
                            strExporttoWord.Append(@"<p>" + eachSection.TemplateContent + "</p>");
                            strExporttoWord.Append(@"</div>");
                        });

                        ContractTemplateTransactionHistory obj1 = new ContractTemplateTransactionHistory();
                        obj1.ContractTemplateID = TemplateID;
                        obj1.CreatedOn = DateTime.Now;
                        obj1.CreatedBy = AuthenticationHelper.UserID;
                        obj1.TemplateContent = strExporttoWord.ToString();
                        obj1.CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User;
                        obj1.Status = 1;
                        obj1.OrderID = 1;
                        entities.ContractTemplateTransactionHistories.Add(obj1);
                        entities.SaveChanges();
                    }
                    #endregion
                }
            }
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "CloseModel();", true);
        }

        protected void btnPrimaryApprover_Click(object sender, EventArgs e)
        {
            int UserID = Convert.ToInt32(Request.QueryString["UID"]);
            int RoleID = Convert.ToInt32(Request.QueryString["RID"]);
            int TemplateID = Convert.ToInt32(Request.QueryString["TID"]);
            int SectionID = Convert.ToInt32(Request.QueryString["SID"]);
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            string ContractVersion = ContractTemplateManagement.getVersion(Convert.ToInt32(TemplateID), customerID);

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                ContractTemplateTransaction obj = new ContractTemplateTransaction();
                obj.ContractTemplateInstanceID = TemplateID;
                obj.CreatedOn = DateTime.Now;
                obj.CreatedBy = UserID;
                obj.Version = ContractVersion;
                obj.ContractTemplateContent = theEditor.Content;
                obj.Status = 4;
                obj.SectionID = SectionID;
                obj.CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User;
                if (Convert.ToInt32(ViewState["TransactionOrderID"]) > 0)
                {
                    obj.OrderID = Convert.ToInt32(ViewState["TransactionOrderID"]) + 1;
                }
                entities.ContractTemplateTransactions.Add(obj);
                entities.SaveChanges();
                int TransactionID = Convert.ToInt32(obj.Id);
                if (TransactionID > 0)
                {
                    if (!string.IsNullOrEmpty(getHistryInsert.Text))
                    {
                        UpdateTransactionHistory(TemplateID, TransactionID, getHistryInsert.Text, "I");
                    }
                    if (!string.IsNullOrEmpty(getHistryDel.Text))
                    {
                        UpdateTransactionHistory(TemplateID, TransactionID, getHistryDel.Text, "D");
                    }
                }
            }

            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "CloseModel();", true);
        }

        protected void btnPrimaryRejetced_Click(object sender, EventArgs e)
        {
            int UserID = Convert.ToInt32(Request.QueryString["UID"]);
            int RoleID = Convert.ToInt32(Request.QueryString["RID"]);
            int TemplateID = Convert.ToInt32(Request.QueryString["TID"]);
            int SectionID = Convert.ToInt32(Request.QueryString["SID"]);
            int customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            string ContractVersion = ContractTemplateManagement.getVersion(Convert.ToInt32(TemplateID), customerID);

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                ContractTemplateTransaction obj = new ContractTemplateTransaction();
                obj.ContractTemplateInstanceID = TemplateID;
                obj.CreatedOn = DateTime.Now;
                obj.CreatedBy = UserID;
                obj.Version = ContractVersion;
                obj.ContractTemplateContent = theEditor.Content;
                obj.Status = 15;
                obj.SectionID = SectionID;
                obj.CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User;
                if (Convert.ToInt32(ViewState["TransactionOrderID"]) > 0)
                {
                    obj.OrderID = Convert.ToInt32(ViewState["TransactionOrderID"]) + 1;
                }
                entities.ContractTemplateTransactions.Add(obj);
                entities.SaveChanges();
                int TransactionID = Convert.ToInt32(obj.Id);
                if (TransactionID > 0)
                {
                    if (!string.IsNullOrEmpty(getHistryInsert.Text))
                    {
                        UpdateTransactionHistory(TemplateID, TransactionID, getHistryInsert.Text, "I");
                    }
                    if (!string.IsNullOrEmpty(getHistryDel.Text))
                    {
                        UpdateTransactionHistory(TemplateID, TransactionID, getHistryDel.Text, "D");
                    }

                    #region set transaction history
                   
                    var entitiesData = (from row in entities.Cont_SP_GetContractSectionsDetail(customerID, Convert.ToInt32(TemplateID))
                                        select row).ToList();
                    
                    var data1 = (from row in entities.Sp_GetContractTemplateDetailTransaction()
                                 where row.UserID == UserID
                                 && row.ContractTemplateInstanceID == TemplateID
                                 && row.RoleID == RoleID
                                 select row).ToList();
                    if (data1.Count > 0)
                    {
                        var data = (from row in data1
                                    join row1 in entities.Cont_tbl_ContractTemplateSectionMapping
                                    on row.sectionID equals row1.SectionID
                                    where row1.ContractTemplateInstanceID == TemplateID
                                    select new getTemplateTransactionDetail
                                    {
                                        TemplateTransactionID = row.TemplateTransactionID,
                                        TemplateInstanceID = row.ContractTemplateInstanceID,
                                        ComplianceStatusID = row.ComplianceStatusID,
                                        CreatedOn = row.CreatedOn,
                                        Status = row.Status,
                                        StatusChangedOn = row.StatusChangedOn,
                                        TemplateContent = row.ContractTemplateContent,
                                        RoleID = row.RoleID,
                                        UserID = row.UserID,
                                        sectionID = Convert.ToInt32(row.sectionID),
                                        orderID = Convert.ToInt32(row1.SectionOrder),
                                        visibilty = Convert.ToInt32(row1.Headervisibilty),
                                        sectionHeader = row.SectionHeader
                                    }).OrderBy(x => x.orderID).ToList();

                        System.Text.StringBuilder strExporttoWord = new System.Text.StringBuilder();

                        data.ForEach(eachSection =>
                        {
                            if (eachSection.visibilty == 1)
                            {
                                strExporttoWord.Append(@"<div>");
                                strExporttoWord.Append(@"<p><B>" + eachSection.sectionHeader + "</B></p>");
                                strExporttoWord.Append(@"</div>");
                            }

                            strExporttoWord.Append(@"<div>");
                            strExporttoWord.Append(@"<p>" + eachSection.TemplateContent + "</p>");
                            strExporttoWord.Append(@"</div>");
                        });

                        ContractTemplateTransactionHistory obj1 = new ContractTemplateTransactionHistory();
                        obj1.ContractTemplateID = TemplateID;
                        obj1.CreatedOn = DateTime.Now;
                        obj1.CreatedBy = AuthenticationHelper.UserID;
                        obj1.TemplateContent = strExporttoWord.ToString();
                        obj1.CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User;
                        obj1.Status = 15;
                        obj1.OrderID = 1;
                        entities.ContractTemplateTransactionHistories.Add(obj1);
                        entities.SaveChanges();
                    }
                    #endregion
                }
            }            
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "CloseModel();", true);
        }

        protected void btnSubmitr_Click(object sender, EventArgs e)
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            int UserID = Convert.ToInt32(Request.QueryString["UID"]);
            int RoleID = Convert.ToInt32(Request.QueryString["RID"]);
            int TemplateID = Convert.ToInt32(Request.QueryString["TID"]);
            int SectionID = Convert.ToInt32(Request.QueryString["SID"]);
            int StatusID = -1;

            string ContractVersion = ContractTemplateManagement.getVersion(Convert.ToInt32(TemplateID), customerID);

            if (Request.QueryString["StatusID"] != null)
            {
                StatusID = Convert.ToInt32(Request.QueryString["StatusID"]);
            }

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (RoleID == 3)//&& StatusID != 15
                {
                    int selecteduserID = Convert.ToInt32(ddlUser.SelectedValue);
                    ContractTemplateAssignment ContracttemplateAssignment = new ContractTemplateAssignment()
                    {
                        ContractTemplateInstanceID = Convert.ToInt32(TemplateID),
                        RoleID = 4,
                        UserID = selecteduserID,
                        IsActive = true,
                        SectionID = Convert.ToInt32(SectionID),
                        Visibility = 1
                    };
                    ContractTemplateManagement.CreateContractTemplateAssignment(ContracttemplateAssignment);
                }

                ContractTemplateTransaction obj = new ContractTemplateTransaction();
                obj.ContractTemplateInstanceID = TemplateID;
                obj.CreatedOn = DateTime.Now;
                obj.CreatedBy = UserID;
                obj.ContractTemplateContent = theEditor.Content;
                obj.SectionID = SectionID;
                obj.Version = ContractVersion;
                obj.CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User;
                obj.Status = 15;

                if (Convert.ToInt32(ViewState["TransactionOrderID"]) > 0)
                {
                    obj.OrderID = Convert.ToInt32(ViewState["TransactionOrderID"]) + 1;
                }
                entities.ContractTemplateTransactions.Add(obj);
                entities.SaveChanges();
                int TransactionID = Convert.ToInt32(obj.Id);
                if (TransactionID > 0)
                {
                    if (!string.IsNullOrEmpty(getHistryInsert.Text))
                    {
                        UpdateTransactionHistory(TemplateID, TransactionID, getHistryInsert.Text, "I");
                    }
                    if (!string.IsNullOrEmpty(getHistryDel.Text))
                    {
                        UpdateTransactionHistory(TemplateID, TransactionID, getHistryDel.Text, "D");
                    }
                    #region set transaction history

                    var entitiesData = (from row in entities.Cont_SP_GetContractSectionsDetail(customerID, Convert.ToInt32(TemplateID))
                                        select row).ToList();


                    var data1 = (from row in entities.Sp_GetContractTemplateDetailTransaction()
                                 where row.UserID == UserID
                                 && row.ContractTemplateInstanceID == TemplateID
                                 && row.RoleID == RoleID
                                 select row).ToList();
                    if (data1.Count > 0)
                    {
                        var data = (from row in data1
                                    join row1 in entities.Cont_tbl_ContractTemplateSectionMapping
                                    on row.sectionID equals row1.SectionID
                                    where row1.ContractTemplateInstanceID == TemplateID
                                    select new getTemplateTransactionDetail
                                    {
                                        TemplateTransactionID = row.TemplateTransactionID,
                                        TemplateInstanceID = row.ContractTemplateInstanceID,
                                        ComplianceStatusID = row.ComplianceStatusID,
                                        CreatedOn = row.CreatedOn,
                                        Status = row.Status,
                                        StatusChangedOn = row.StatusChangedOn,
                                        TemplateContent = row.ContractTemplateContent,
                                        RoleID = row.RoleID,
                                        UserID = row.UserID,
                                        sectionID = Convert.ToInt32(row.sectionID),
                                        orderID = Convert.ToInt32(row1.SectionOrder),
                                        visibilty = Convert.ToInt32(row1.Headervisibilty),
                                        sectionHeader = row.SectionHeader
                                    }).OrderBy(x => x.orderID).ToList();

                        System.Text.StringBuilder strExporttoWord = new System.Text.StringBuilder();

                        data.ForEach(eachSection =>
                        {
                            if (eachSection.visibilty == 1)
                            {
                                strExporttoWord.Append(@"<div>");
                                strExporttoWord.Append(@"<p><B>" + eachSection.sectionHeader + "</B></p>");
                                strExporttoWord.Append(@"</div>");
                            }

                            strExporttoWord.Append(@"<div>");
                            strExporttoWord.Append(@"<p>" + eachSection.TemplateContent + "</p>");
                            strExporttoWord.Append(@"</div>");
                        });

                        ContractTemplateTransactionHistory obj1 = new ContractTemplateTransactionHistory();
                        obj1.ContractTemplateID = TemplateID;
                        obj1.CreatedOn = DateTime.Now;
                        obj1.CreatedBy = AuthenticationHelper.UserID;
                        obj1.TemplateContent = strExporttoWord.ToString();
                        obj1.CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User;
                        obj1.Status = 15;
                        obj1.OrderID = 1;
                        entities.ContractTemplateTransactionHistories.Add(obj1);
                        entities.SaveChanges();
                    }
                    #endregion
                }
                
            }
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "CloseModel();", true);
        }
    }
}