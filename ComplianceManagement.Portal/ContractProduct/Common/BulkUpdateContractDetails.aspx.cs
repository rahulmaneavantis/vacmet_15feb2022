﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataContract;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.Common
{
    public partial class BulkUpdateContractDetails : System.Web.UI.Page
    {
        protected static int CustomerID;
        protected void Page_Load(object sender, EventArgs e)
        {
            CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

        }

        protected void btn_Download_Click(object sender, EventArgs e)
        {
            try
            {
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    long customerID = -1;
                    customerID = AuthenticationHelper.CustomerID;
                    var lstAssignedContracts = ContractManagement.GetAllAssignedContractsList(Convert.ToInt32(customerID));


                    Session["grdDetailData"] = (lstAssignedContracts).ToDataTable();

                    String FileName = String.Empty;

                    FileName = "Contract";
            //        ExcelData = view.ToTable("Selected", false, "ContractNo", "TypeName", "ContractTitle", "SubTypeName", "BranchName",
            //"StatusName", "ContractDetailDesc", "VendorNames", "DeptName", "PaymentTerm", "ContactPersonOfDepartment", "ProposalDate", "AgreementDate", "StartDate", "ReviewDate", "EndDate", "Owner", "ContractAmt", "Taxex", "PaymentType", "PaymentTerm", "ProductItems", "LockInPeriodDate", "AddNewClause", "Taxes");

                    ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("ContractDetails");
                    DataTable ExcelData = null;

                    DataView view = new System.Data.DataView((DataTable)Session["grdDetailData"]);
                    // ExcelData = view.ToTable("Selected", false, "ContractNo", "StatusName", "ContractTitle", "ContractDetailDesc", "BranchName", "ContractAmt", "PaymentType", "DeptName", "ContactPersonOfDepartment", "TypeName", "SubTypeName", "VendorNames", "ProposalDate", "AgreementDate", "EffectiveDate", "ReviewDate", "ExpirationDate", "AddNewClause", "Owner", "ProductItems", "Product_Specification", "Rate_per_product", "GST", "PaymentTerm", "Delivery_Installation_Period", "Penalty", "Product_Warranty");

                    ExcelData = view.ToTable("Selected", false, "ContractNo", "StatusName", "ContractTitle", "ContractDetailDesc", "BranchName",
                         "ContractAmt", "PaymentType", "DeptName", "ContactPersonOfDepartment", "TypeName", "SubTypeName", "VendorNames", "ProposalDate", "AgreementDate", "EffectiveDate", "ReviewDate", "ExpirationDate", "AddNewClause", "Owner", "ProductItems","PaymentTerm","Lock-in-period","Taxes","Remark");

                    if (ExcelData.Rows.Count > 0)
                    {
                        ExcelData.Columns.Add("SNo", typeof(int)).SetOrdinal(0);

                        int rowCount = 0;
                        foreach (DataRow item in ExcelData.Rows)
                        {
                            item["SNo"] = ++rowCount;

                            if (item["ProposalDate"] != null)
                            {
                                if (!string.IsNullOrEmpty(Convert.ToString(item["ProposalDate"])))
                                {
                                    item["ProposalDate"] = Convert.ToDateTime(item["ProposalDate"]).ToString("dd-MMM-yyyy");
                                }
                            }

                            if (item["AgreementDate"] != null)
                            {
                                if (!string.IsNullOrEmpty(Convert.ToString(item["AgreementDate"])))
                                {
                                    item["AgreementDate"] = Convert.ToDateTime(item["AgreementDate"]).ToString("dd-MMM-yyyy");
                                }
                            }

                            if (item["EffectiveDate"] != null)
                            {
                                if (!string.IsNullOrEmpty(Convert.ToString(item["EffectiveDate"])))
                                {
                                    item["EffectiveDate"] = Convert.ToDateTime(item["EffectiveDate"]).ToString("dd-MMM-yyyy");
                                }
                            }

                            if (item["ReviewDate"] != null)
                            {
                                if (!string.IsNullOrEmpty(Convert.ToString(item["ReviewDate"])))
                                {
                                    item["ReviewDate"] = Convert.ToDateTime(item["ReviewDate"]).ToString("dd-MMM-yyyy");
                                }
                            }

                            if (item["ExpirationDate"] != null)
                            {
                                if (!string.IsNullOrEmpty(Convert.ToString(item["ExpirationDate"])))
                                {
                                    item["ExpirationDate"] = Convert.ToDateTime(item["ExpirationDate"]).ToString("dd-MMM-yyyy");
                                }
                            }
                        }

                        var customer = UserManagementRisk.GetCustomer(AuthenticationHelper.UserID);



                        exWorkSheet.Cells["A1"].LoadFromDataTable(ExcelData, true);

                        exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["A1"].Value = "S.No.";
                        exWorkSheet.Cells["A1"].AutoFitColumns(5);

                        exWorkSheet.Cells["B1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["B1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["B1"].Value = "Contract No.";
                        exWorkSheet.Cells["B1"].AutoFitColumns(20);

                        exWorkSheet.Cells["C1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["C1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["C1"].Value = "Status";
                        exWorkSheet.Cells["C1"].AutoFitColumns(20);

                        exWorkSheet.Cells["D1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["D1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["D1"].Value = "Contract Title";
                        exWorkSheet.Cells["D1"].AutoFitColumns(10);

                        exWorkSheet.Cells["E1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["E1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["E1"].Value = "Description";
                        exWorkSheet.Cells["E5"].AutoFitColumns(50);

                        exWorkSheet.Cells["F1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["F1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["F1"].Value = "Entity/Branch/Location";
                        exWorkSheet.Cells["F1"].AutoFitColumns(25);

                        exWorkSheet.Cells["G1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["G1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["G1"].Value = "Contract Amount/Value";
                        exWorkSheet.Cells["G1"].AutoFitColumns(25);

                        exWorkSheet.Cells["H1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["H1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["H1"].Value = "Payment Type";
                        exWorkSheet.Cells["H1"].AutoFitColumns(25);

                        exWorkSheet.Cells["I1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["I1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["I1"].Value = "Department";
                        exWorkSheet.Cells["I1"].AutoFitColumns(25);

                        exWorkSheet.Cells["J1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["J1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["J1"].Value = "Contact Person Of Deapartment";
                        exWorkSheet.Cells["J1"].AutoFitColumns(25);

                        exWorkSheet.Cells["K1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["K1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["K1"].Value = "Contract Type";
                        exWorkSheet.Cells["K1"].AutoFitColumns(25);

                        exWorkSheet.Cells["L1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["L1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["L1"].Value = "Contract Sub-Type";
                        exWorkSheet.Cells["L1"].AutoFitColumns(25);

                        exWorkSheet.Cells["M1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["M1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["M1"].Value = "Vendor";
                        exWorkSheet.Cells["M1"].AutoFitColumns(25);

                        exWorkSheet.Cells["N1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["N1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["N1"].Value = "Proposal Date";
                        exWorkSheet.Cells["N1"].AutoFitColumns(15);

                        exWorkSheet.Cells["O1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["O1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["O1"].Value = "Agreement Date";
                        exWorkSheet.Cells["O1"].AutoFitColumns(15);

                        exWorkSheet.Cells["P1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["P1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["P1"].Value = "Start Date";
                        exWorkSheet.Cells["P1"].AutoFitColumns(15);

                        exWorkSheet.Cells["Q1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["Q1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["Q1"].Value = "Review Date";
                        exWorkSheet.Cells["Q1"].AutoFitColumns(15);

                        exWorkSheet.Cells["R1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["R1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["R1"].Value = "End Date";
                        exWorkSheet.Cells["R1"].AutoFitColumns(15);

                        exWorkSheet.Cells["S1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["S1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["S1"].Value = "Add New Clause";
                        exWorkSheet.Cells["S1"].AutoFitColumns(15);

                        exWorkSheet.Cells["T1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["T1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["T1"].Value = "Owner(s)";
                        exWorkSheet.Cells["T1"].AutoFitColumns(15);

                        exWorkSheet.Cells["U1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["U1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["U1"].Value = "Payment terms";
                        exWorkSheet.Cells["U1"].AutoFitColumns(15);

                        exWorkSheet.Cells["V1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["V1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["V1"].Value = "Lock-in-period";
                        exWorkSheet.Cells["V1"].AutoFitColumns(15);

                        exWorkSheet.Cells["W1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["W1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["W1"].Value = "Notice Term";
                        exWorkSheet.Cells["W1"].AutoFitColumns(15);

                        exWorkSheet.Cells["X1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["X1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["X1"].Value = "Remark";
                        exWorkSheet.Cells["X1"].AutoFitColumns(15);

                        exWorkSheet.Cells["Y1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["Y1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["Y1"].Value = "Payment Type";
                        exWorkSheet.Cells["Y1"].AutoFitColumns(15);


                        //Assign borders

                        using (ExcelRange col = exWorkSheet.Cells[1, 1, 1 + ExcelData.Rows.Count, 28])
                        {
                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;
                            col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;

                            col.Style.WrapText = true;

                            col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        }

                        using (ExcelRange col = exWorkSheet.Cells[1, 8, 1 + ExcelData.Rows.Count, 28])
                        {
                            col.Style.Numberformat.Format = "dd/MM/yyyy";
                        }

                        Byte[] fileBytes = exportPackge.GetAsByteArray();
                        Response.ClearContent();
                        Response.Buffer = true;
                        Response.AddHeader("content-disposition", "attachment;filename=" + FileName + ".xlsx");
                        Response.Charset = "";
                        Response.ContentType = "application/vnd.ms-excel";
                        StringWriter sw = new StringWriter();
                        Response.BinaryWrite(fileBytes);
                        //Response.End();
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                    }
                    else
                    {
                        cvUploadUtility.IsValid = false;
                        cvUploadUtility.ErrorMessage = "No data available to export for current selection(s)";
                    }

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnUploadFile_Click(object sender, EventArgs e)
        {
            if (MasterFileUpload.HasFile)
            {
                try
                {
                    string filename = Path.GetFileName(MasterFileUpload.FileName);
                    MasterFileUpload.SaveAs(Server.MapPath("~/ContractProduct/Uploaded/") + filename.Trim());
                    FileInfo excelfile = new FileInfo(Server.MapPath("~/ContractProduct/Uploaded/") + filename.Trim());

                    if (excelfile != null)
                    {
                        using (ExcelPackage xlWorkbook = new ExcelPackage(excelfile))
                        {

                            bool matchSuccess = ContractCommonMethods.checkSheetExist(xlWorkbook, "ContractDetails");
                            if (matchSuccess)
                            {
                                ProcessContractData(xlWorkbook);
                            }
                            else
                            {
                                cvUploadUtility.IsValid = false;
                                cvUploadUtility.ErrorMessage = "No Data Found in Excel Document or Sheet Name must be 'ContractDetails'.";
                            }
                        }
                    }
                    else
                    {
                        cvUploadUtility.IsValid = false;
                        cvUploadUtility.ErrorMessage = "Error Uploading Excel Document. Please try again.";
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvUploadUtility.IsValid = false;
                    cvUploadUtility.ErrorMessage = "Something went wrong, Please try again";
                }
            }
        }
              public bool checkDuplicateMultipleDataExistExcelSheet(ExcelWorksheet xlWorksheet, int currentRowNumber, int colNum, string providedText, string MultipleProviderText)
        {
            bool matchSuccess = false;
            try
            {
                if (xlWorksheet != null)
                {
                    string TextToCompare = string.Empty;
                    int lastRow = xlWorksheet.Dimension.End.Row;

                    for (int i = 2; i <= lastRow; i++)
                    {
                        if (i != currentRowNumber)
                        {
                            string TypeTextToCompare = xlWorksheet.Cells[i, 1].Text.ToString().Trim();

                            if (MultipleProviderText.Equals(TypeTextToCompare))
                            {
                                TextToCompare = xlWorksheet.Cells[i, colNum].Text.ToString().Trim();
                                if (String.Equals(providedText, TextToCompare, StringComparison.OrdinalIgnoreCase))
                                {
                                    matchSuccess = true;
                                    i = lastRow; //exit from for Loop
                                }
                            }
                        }
                    }
                }

                return matchSuccess;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return matchSuccess;
            }
        }

        
        private void ProcessContractData(ExcelPackage xlWorkbook)
        {
            try
            {
                int uploadedVendorCount = 0;
                bool saveSuccessresult = false;
                ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["ContractDetails"];


                if (xlWorksheet != null)
                {
                    List<string> errorMessage = new List<string>();
                   

                    int xlrow2 = xlWorksheet.Dimension.End.Row;
                    int xlCell = xlWorksheet.Dimension.End.Column;

                    string ContractID = string.Empty;
                    string ColoumnValue = string.Empty;


                    for (int a = 2; a <= xlrow2; a++)
                    {
                        for (int i = 2; i <= xlCell; i++)
                        {
                            string Column = Convert.ToString(xlWorksheet.Cells[1, i].Text).Trim();
                            string ColoumnName = "Cont_tbl_ContractInstance." + Column;

                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                            {
                                ContractID = Convert.ToString(xlWorksheet.Cells[a, 1].Text).Trim();
                                ColoumnValue = Convert.ToString(xlWorksheet.Cells[a, i].Text).Trim();

                                Cont_tbl_ContractInstance updateDetails = (from row in entities.Cont_tbl_ContractInstance
                                                                           where row.ContractNo == ContractID
                                                                           select row).FirstOrDefault();
                                if (updateDetails != null)
                                {
                                    string ContractTitle = "Cont_tbl_ContractInstance.ContractTitle";
                                    if (ContractTitle == ColoumnName)
                                    {
                                        if (!string.IsNullOrEmpty(ColoumnValue))
                                        {
                                            updateDetails.ContractTitle = ColoumnValue;
                                        }
                                        else
                                        {
                                            errorMessage.Add("Contract Title should not be empty at row-" + a);
                                        }

                                    } 
                                    string ContractDetailDesc = "Cont_tbl_ContractInstance.ContractDetailDesc";
                                    if (ContractDetailDesc == ColoumnName)
                                    {
                                        if (!string.IsNullOrEmpty(ColoumnValue))
                                        {
                                            updateDetails.ContractDetailDesc = ColoumnValue;
                                        }
                                        else
                                        {
                                            errorMessage.Add("Contract Description should not be empty at row-" + a);
                                        }

                                    }

                                    string ProductItems = "Cont_tbl_ContractInstance.ProductItems";
                                    if (ProductItems == ColoumnName)
                                    {

                                        if (!string.IsNullOrEmpty(ColoumnValue))
                                        {
                                            updateDetails.ProductItems = ColoumnValue;
                                        }
                                        else
                                        {
                                            updateDetails.ProductItems = null;
                                        }
                                    }    
                                    string Remark = "Cont_tbl_ContractInstance.Remark";
                                    if (Remark == ColoumnName)
                                    {
                                        if (!string.IsNullOrEmpty(ColoumnValue))
                                        {
                                            updateDetails.AddNewClause = ColoumnValue;
                                        }
                                        else
                                        {
                                            updateDetails.AddNewClause = null;
                                        }


                                    }
                                    string Taxes = "Cont_tbl_ContractInstance.Taxes";
                                    if (Taxes == ColoumnName)
                                    {
                                        if (!string.IsNullOrEmpty(ColoumnValue))
                                        {
                                            updateDetails.Taxes = ColoumnValue;
                                        }
                                        else
                                        {
                                            updateDetails.Taxes = null;
                                        }
                                    }

                                    string BranchName = "Cont_tbl_ContractInstance.BranchName";
                                    if (BranchName == ColoumnName)
                                    {
                                        var CBID = CustomerBranchManagement.GetByName(ColoumnValue, CustomerID);
                                        if(CBID != null)
                                        {
                                            if (CBID.ID == 0 || CBID.ID == -1)
                                            {
                                                errorMessage.Add("Please Correct the (Entity/ Location/ Branch) or (Entity/ Location/ Branch) not defined in the Legal Entity Master at row-"+a);
                                            }
                                            else
                                            {
                                                updateDetails.CustomerBranchID = Convert.ToInt32(CBID.ID);
                                            }
                                        }
                                        else
                                        {
                                            errorMessage.Add("Please Correct the (Entity/ Location/ Branch) or (Entity/ Location/ Branch) not defined in the Legal Entity Master at row-"+a);

                                        }

                                    }

                                    string VendorNames = "Cont_tbl_ContractInstance.VendorNames";
                                    if (VendorNames == ColoumnName)
                                    {
                                       
                                        List<long> lstVendorIDs = new List<long>();
                                        string ContractVendor = string.Empty;
                                        long vendorID = -1;
                                        bool matchSuccess = false;
                                        bool saveSuccess = false;
                                        List<Cont_tbl_VendorMapping> lstVendorMapping_ToSave = new List<Cont_tbl_VendorMapping>();
                                        var masterlstVendors = VendorDetails.GetVendorDetails(CustomerID);
                                        //var masterlstVendors = entities.Cont_tbl_VendorMaster.Where(row => row.CustomerID == CustomerID).ToList();
                                        var CID = (from row in entities.Cont_tbl_ContractInstance
                                                   where row.ContractNo == ContractID
                                                   select row.ID).FirstOrDefault();

                                        if (lstVendorIDs.Count > 0)
                                            lstVendorIDs.Clear();

                                        ContractVendor = ColoumnValue;
                                        string[] split = ContractVendor.Split(',');
                                        if (split.Length > 0)
                                        {
                                            for (int rs = 0; rs < split.Length; rs++)
                                            {
                                                vendorID = VendorDetails.GetVendorIDByName(masterlstVendors, split[rs].ToString().Trim(), CustomerID);

                                                if (vendorID <= 0)
                                                {
                                                    errorMessage.Add("Please Correct the Vendor Name or Vendor (" + split[rs].ToString().Trim() + ") not defined in the Vendor Master at row-" + a);
                                                }
                                                else
                                                {
                                                    lstVendorIDs.Add(vendorID);
                                                }
                                            }
                                        }

                                        if (lstVendorIDs.Count > 0)
                                        {

                                            lstVendorIDs.ForEach(EachVendor =>
                                            {
                                                Cont_tbl_VendorMapping _vendorMappingRecord = new Cont_tbl_VendorMapping()
                                                {
                                                    ContractID = CID,
                                                    VendorID = EachVendor,
                                                    IsActive = true,
                                                    CreatedBy = AuthenticationHelper.UserID,
                                                    CreatedOn = DateTime.Now,
                                                };

                                                lstVendorMapping_ToSave.Add(_vendorMappingRecord);
                                            });

                                            VendorDetails.CreateUpdate_VendorMapping(lstVendorMapping_ToSave);

                                            var existingVendorMapping = VendorDetails.GetVendorMapping(CID);
                                            var assignedVendorIDs = lstVendorMapping_ToSave.Select(row => row.VendorID).ToList();

                                            if (existingVendorMapping.Count != assignedVendorIDs.Count)
                                            {
                                                matchSuccess = false;
                                            }
                                            else
                                            {
                                                matchSuccess = existingVendorMapping.Except(assignedVendorIDs).ToList().Count > 0 ? false : true;
                                            }

                                            if (!matchSuccess)
                                            {
                                                VendorDetails.DeActiveExistingVendorMapping(CID);

                                                saveSuccess = VendorDetails.CreateUpdate_VendorMapping(lstVendorMapping_ToSave);
                                                if (saveSuccess)
                                                {
                                                    ContractManagement.CreateAuditLog("C", CID, "Cont_tbl_VendorMapping", "Update", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Contract Vendor Updated", true);
                                                }
                                            }
                                            else
                                                saveSuccess = false;

                                            //Refresh List
                                            lstVendorMapping_ToSave.Clear();
                                            lstVendorMapping_ToSave = null;

                                        }                                     
                                    }

                                    string DeptName = "Cont_tbl_ContractInstance.DeptName";
                                    if (DeptName == ColoumnName)
                                    {
                                        var DepartmentID = CompDeptManagement.GetDepartmentIDByName1(ColoumnValue, CustomerID);
                                        if (DepartmentID == 0 || DepartmentID == -1)
                                        {
                                            errorMessage.Add("Please Correct the Department or Department not Defined in the Masters at row-"+a);
                                        }
                                        else
                                        {
                                            updateDetails.DepartmentID = DepartmentID;
                                        }
                                    }

                                    string ContactPersonOfDepartment = "Cont_tbl_ContractInstance.ContactPersonOfDepartment";
                                    if (ContactPersonOfDepartment == ColoumnName)
                                    {
                                       var ContactPersonofDeptID = CompDeptManagement.GetContactPersonIDByEmail(ColoumnValue, CustomerID);
                                       // var allUsers = ContractUserManagement.GetRequiredUsers(lstAllUsers, 4);
                                        if (ContactPersonofDeptID == 0 || ContactPersonofDeptID == -1)
                                        {
                                            errorMessage.Add("Contact Person Name of Department not valid or not available in User Master at row-"+a);
                                        }
                                        else
                                        {
                                            updateDetails.ContactPersonOfDepartment = ContactPersonofDeptID;
                                        }
                                    }

                                    string PaymentType = "Cont_tbl_ContractInstance.PaymentType(Payee/Receipt)";
                                    if (PaymentType == ColoumnName)
                                    {
                                        if (!String.IsNullOrEmpty(ColoumnValue))
                                        {
                                            //  var PaymentTypeID = CompDeptManagement.GetPaymentTypeByID(ColoumnValue, CustomerID);
                                            if (ColoumnValue == "Payee" || ColoumnValue == "payee")
                                            {
                                                updateDetails.PaymentType = 0;
                                            }
                                            else if (ColoumnValue == "Receipt" || ColoumnValue == "receipt")
                                            {
                                                updateDetails.PaymentType = 1;
                                            }
                                        }
                                        else
                                        {
                                            updateDetails.PaymentType = -1;
                                        }
                                       
                                    }

                                    string ProposalDate = "Cont_tbl_ContractInstance.ProposalDate(dd/MM/yyyy)";
                                    if (ProposalDate == ColoumnName)
                                    {
                                        if (!String.IsNullOrEmpty(ColoumnValue))
                                        {
                                         // bool checkDate = ContractCommonMethods.CheckValidDate(ColoumnValue);
                                            //   bool checkDate = ContractCommonMethods.CheckDate(ColoumnValue);
                                            //     updateDetails.ProposalDate = DateTime.ParseExact(ProposalDate, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);

                                            //    ParseExact(ProposalDate, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                                            bool check = CheckDate(ColoumnValue);
                                            if (!check)
                                            { 
                                                errorMessage.Add("Invalid Proposal Date or Please provide Proposal Date in Correct format at row-"+a);
                                                //updateDetails.ProposalDate = null;
                                            }
                                            else
                                            {
                                                updateDetails.ProposalDate = Convert.ToDateTime(ColoumnValue);
                                            }
                                        }
                                        else
                                        {
                                            updateDetails.ProposalDate = null;
                                        }
                                    }

                                    string AgreementDate = "Cont_tbl_ContractInstance.AgreementDate(dd/MM/yyyy)";
                                    if (AgreementDate == ColoumnName)
                                    {
                                        if (!String.IsNullOrEmpty(ColoumnValue))
                                        {
                                            // bool checkDate = ContractCommonMethods.CheckValidDate(ColoumnValue);
                                            bool checkDate = CheckDate(ColoumnValue);
                                            if (!checkDate)
                                            {
                                                errorMessage.Add("Invalid Agreement Date or Please provide Agreement Date in Correct format at row-"+a);
                                                updateDetails.AgreementDate = null;
                                            }
                                            else
                                            {
                                                //  updateDetails.AgreementDate = DateTime.ParseExact(ProposalDate, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);

                                                updateDetails.AgreementDate = Convert.ToDateTime(ColoumnValue);
                                            }
                                        }
                                        else
                                        {
                                            updateDetails.AgreementDate = null;
                                        }
                                    }

                                    string EffectiveDate = "Cont_tbl_ContractInstance.EffectiveDate(dd/MM/yyyy)";
                                    if (EffectiveDate == ColoumnName)
                                    {
                                        if (!String.IsNullOrEmpty(ColoumnValue))
                                        {
                                            bool checkDate = CheckDate(ColoumnValue);
                                            if (!checkDate)
                                            {
                                                errorMessage.Add("Invalid Effective Date or Please provide Effective Date in Correct format at row-"+a);
                                                //updateDetails.EffectiveDate = null;
                                            }
                                            else
                                            {
                                                updateDetails.EffectiveDate = Convert.ToDateTime(ColoumnValue);
                                            }
                                        }
                                        else
                                        {
                                            updateDetails.EffectiveDate = null;
                                        }
                                    }

                                    string ReviewDate = "Cont_tbl_ContractInstance.ReviewDate(dd/MM/yyyy)";
                                    if (ReviewDate == ColoumnName)
                                    {
                                        if (!String.IsNullOrEmpty(ColoumnValue))
                                        {
                                            bool checkDate = CheckDate(ColoumnValue);
                                            if (!checkDate)
                                            {
                                                errorMessage.Add("Invalid Review Date or Please provide Review Date in Correct format at row-"+a);
                                                //updateDetails.ReviewDate = null;
                                            }
                                            else
                                            {
                                                updateDetails.ReviewDate = Convert.ToDateTime(ColoumnValue);
                                            }
                                        }
                                        else
                                        {
                                            updateDetails.ReviewDate = null;
                                        }
                                    }

                                    string ExpirationDate = "Cont_tbl_ContractInstance.ExpirationDate(dd/MM/yyyy)";
                                    if (ExpirationDate == ColoumnName)
                                    {
                                        if (!String.IsNullOrEmpty(ColoumnValue))
                                        {
                                            bool checkDate = CheckDate(ColoumnValue);
                                            if (!checkDate)
                                            {
                                                errorMessage.Add("Invalid Expiration Date or Please provide Expiration Date in Correct format at row-"+a);
                                                //updateDetails.ExpirationDate = null;
                                            }
                                            else
                                            {
                                                updateDetails.ExpirationDate = Convert.ToDateTime(ColoumnValue);
                                            }
                                        }
                                        else
                                        {
                                            updateDetails.ExpirationDate = null;
                                        }
                                    }

                                    string LockInperiod = "Cont_tbl_ContractInstance.Lock-in-period";
                                    if (LockInperiod == ColoumnName)
                                    {
                                        if (!String.IsNullOrEmpty(ColoumnValue))
                                        {
                                            bool checkDate = CheckDate(ColoumnValue);
                                            if (!checkDate)
                                            {
                                                errorMessage.Add("Invalid Lock In Period Date or Please provide Lock-in-period Date in Correct format at row-"+a);
                                                //updateDetails.ProposalDate = null;
                                            }
                                            else
                                            {
                                                updateDetails.LockInPeriodDate = Convert.ToDateTime(ColoumnValue);
                                            }
                                        }
                                        else
                                        {
                                            updateDetails.LockInPeriodDate = null;
                                        }
                                    }
                                    string NoticeTerm = "Cont_tbl_ContractInstance.NoticeTerm(Days)";
                                    if (NoticeTerm == ColoumnName)
                                    {
                                        if (!string.IsNullOrEmpty(ColoumnValue))
                                        {
                                            updateDetails.NoticeTermNumber =Convert.ToInt32(ColoumnValue);
                                        }
                                        else
                                        {
                                            updateDetails.NoticeTermNumber = null;
                                        }
                                    }   
                                    string TypeName = "Cont_tbl_ContractInstance.TypeName";
                                    if (TypeName == ColoumnName)
                                    {
                                        var ContractTypesID = ContractTypeMasterManagement.GetContractTypeID(ColoumnValue, CustomerID);
                                        if (ContractTypesID == 0 || ContractTypesID == -1)
                                        {
                                            errorMessage.Add("Please Correct the Contract Type or Contract Type not defined in the Contract Type Maters at row-"+a);
                                        }
                                        else
                                        {
                                            updateDetails.ContractTypeID = ContractTypesID;
                                        }
                                       
                                    }
                                    string SubTypeName = "Cont_tbl_ContractInstance.SubTypeName";
                                    if (SubTypeName == ColoumnName)
                                    {
                                        if (!String.IsNullOrEmpty(ColoumnValue))
                                        {
                                            var ContractSubTypeID = ContractTypeMasterManagement.GetContractSubTypeIDByName1(ColoumnValue, CustomerID);
                                            if (ContractSubTypeID == 0 || ContractSubTypeID == -1)
                                            {
                                                errorMessage.Add("Please Correct the Contract Sub-Type or Contract Sub Type not defined in the Contract Sub-Type Maters at row-"+a);
                                                //updateDetails.ContractSubTypeID = null;
                                            }
                                            else
                                            {
                                                updateDetails.ContractSubTypeID = ContractSubTypeID;
                                            }
                                        }
                                        else
                                        {
                                            updateDetails.ContractSubTypeID = null;
                                        }
                                       
                                    }


                                    string Owner = "Cont_tbl_ContractInstance.Owner";
                                    if (Owner == ColoumnName)
                                    {

                                        List<int> lstContractOwnerIDs = new List<int>();
                                        int OwnerID = -1;
                                        int uploadedContractCount = 0;
                                        bool saveSuccess = false;
                                        var masterlstUsers = entities.Users.Where(row => row.CustomerID == CustomerID).ToList();
                                        var CID = (from row in entities.Cont_tbl_ContractInstance
                                                   where row.ContractNo == ContractID
                                                   select row.ID).FirstOrDefault();
                                        string ContractOwner = string.Empty;
                                        ContractOwner = ColoumnValue;

                                        string[] split = ContractOwner.Split(',');
                                        if (split.Length > 0)
                                        {
                                            for (int rs = 0; rs < split.Length; rs++)
                                            {
                                                OwnerID = ContractUserManagement.GetContractOwnerUserIDByEmail(masterlstUsers, CustomerID, split[rs].ToString().Trim());
                                                if (OwnerID == 0 || OwnerID == -1)
                                                {
                                                    errorMessage.Add("Please Correct the Owner(Email) or Owner (" + split[rs].ToString().Trim() + ") not defined in the User Master at row-"+a);
                                                }
                                                else
                                                {
                                                    lstContractOwnerIDs.Add(OwnerID);
                                                }
                                            }
                                        }

                                        ContractManagement.DeActiveContractUserAssignments(CID, AuthenticationHelper.UserID);
                                        List<Cont_tbl_UserAssignment> lstUserAssignmentRecord_ToSave = new List<Cont_tbl_UserAssignment>();


                                        if (lstContractOwnerIDs.Count > 0)
                                        {
                                            lstUserAssignmentRecord_ToSave.Clear();

                                            lstContractOwnerIDs.ForEach(eachOwner =>
                                            {
                                                Cont_tbl_UserAssignment newAssignment = new Cont_tbl_UserAssignment()
                                                {
                                                    AssignmentType = 1,
                                                    ContractID = CID,
                                                    UserID = eachOwner,
                                                    RoleID = 3,
                                                    IsActive = true,
                                                    CreatedBy = AuthenticationHelper.UserID,
                                                    UpdatedBy = AuthenticationHelper.UserID,
                                                };

                                                lstUserAssignmentRecord_ToSave.Add(newAssignment);
                                            });

                                            if (lstUserAssignmentRecord_ToSave.Count > 0)
                                                saveSuccess = ContractManagement.CreateUpdate_ContractUserAssignments(lstUserAssignmentRecord_ToSave);

                                            if (saveSuccess)
                                            {
                                                uploadedContractCount++;
                                                ContractManagement.CreateAuditLog("C", CID, "Cont_tbl_UserAssignment", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Contract Owner Mapping Created", true);
                                            }

                                            lstContractOwnerIDs.Clear();
                                            lstUserAssignmentRecord_ToSave.Clear();
                                        }
                                    }

                                    string PaymentTerm = "Cont_tbl_ContractInstance.PaymentTerm";
                                    if (PaymentTerm == ColoumnName)
                                    {
                                        //if (!String.IsNullOrEmpty(ColoumnValue))
                                        //{
                                            List<int> lstContractPaymentTermIDs = new List<int>();
                                            //  int PaymentTermID = -1;
                                            int uploadedContractCount = 0;
                                            bool saveSuccess = false;
                                            string PaymentTerms = string.Empty;
                                            PaymentTerms = ColoumnValue;
                                            var masterlstUsers = entities.Users.Where(row => row.CustomerID == CustomerID).ToList();
                                            var CID = (from row in entities.Cont_tbl_ContractInstance
                                                       where row.ContractNo == ContractID
                                                       //&& row.PaymentTermID == PaymentTerms
                                                       select row.ID).FirstOrDefault();


                                            string[] split = PaymentTerms.Split(',');
                                            if (split.Length > 0)
                                            {
                                                for (int rs = 0; rs < split.Length; rs++)
                                                {
                                                    var PaymentTermID = Convert.ToInt32(ContractUserManagement.GetPaymentTermMapping(split[rs].ToString().Trim()));

                                                    if (PaymentTermID == -1)
                                                    {
                                                        errorMessage.Add("Please Correct the Payment Term or Payment Term not defined in the Payment Master at row-" + a);
                                                    }
                                                    else
                                                    {
                                                        lstContractPaymentTermIDs.Add(PaymentTermID);
                                                    }
                                                }
                                            }
                                            ContractManagement.DeActivePaymentTermAssignments(CID, AuthenticationHelper.UserID);
                                            List<Cont_tbl_PaymentTermMapping> lstUserAssignmentRecord_ToSave = new List<Cont_tbl_PaymentTermMapping>();
                                            if (lstContractPaymentTermIDs.Count > 0)
                                            {
                                                lstUserAssignmentRecord_ToSave.Clear();
                                                lstContractPaymentTermIDs.ForEach(eachOwner =>
                                                {
                                                    Cont_tbl_PaymentTermMapping newAssignment = new Cont_tbl_PaymentTermMapping()
                                                    {
                                                        PaymentTermID = eachOwner,
                                                        ContractID = CID,
                                                        CustomerID = AuthenticationHelper.CustomerID,

                                                    };
                                                    lstUserAssignmentRecord_ToSave.Add(newAssignment);
                                                });
                                                if (lstUserAssignmentRecord_ToSave.Count > 0)
                                                    saveSuccess = ContractManagement.CreateUpdate_PayementTermAssignments(lstUserAssignmentRecord_ToSave);

                                                if (saveSuccess)
                                                {
                                                    uploadedContractCount++;
                                                    ContractManagement.CreateAuditLog("C", CID, "Cont_tbl_PaymentTerm", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Contract Owner Mapping Created", true);
                                                }
                                                lstContractPaymentTermIDs.Clear();
                                                lstUserAssignmentRecord_ToSave.Clear();
                                            }
                                        //}
                                        //else
                                        //{

                                        //}
                                    }


                                    string ContractAmt = "Cont_tbl_ContractInstance.ContractAmt";
                                    if (ContractAmt == ColoumnName)
                                    {
                                        if (!String.IsNullOrEmpty(ColoumnValue))
                                        {
                                            bool checkNumber = ContractCommonMethods.IsValidNumber(ColoumnValue);
                                            if (!checkNumber)
                                            {
                                                errorMessage.Add("Please Enter only numbers in Contract Amount at row-"+a);
                                            }
                                            else
                                            {
                                                updateDetails.ContractAmt = Convert.ToDecimal(ColoumnValue);
                                            }
                                        }
                                        else
                                        {
                                            updateDetails.ContractAmt = null;
                                        }
                                    }
                                           
                                  
                                    if (errorMessage.Count > 0)
                                    {
                                        ErrorMessages(errorMessage);
                                    }
                                    else
                                    {
                                        entities.SaveChanges();
                                        uploadedVendorCount++;
                                        saveSuccessresult = true;
                                    }  
                                }
                                else
                                {
                                    cvUploadUtility.IsValid = false;
                                    cvUploadUtility.ErrorMessage = "Contract No. not found in our Database, Please add new contract";
                                }
                            }
                        }

                    }

                    if (saveSuccessresult)
                    {
                        if (errorMessage.Count <= 0)
                        {
                            if (uploadedVendorCount > 0)
                            {
                                cvUploadUtilityPageSuccess.IsValid = false;
                                cvUploadUtilityPageSuccess.ErrorMessage = " Contract(s) Details Updated Successfully";
                                cvUploadUtilityPageSuccess.CssClass = "alert alert-success";
                            }
                        }
                    }

                }
            
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUploadUtility.IsValid = false;
                cvUploadUtility.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }


        public class ContractDetails
        {
            
            public string ContractorID { get; set; }
            public string ContractorName { get; set; }
            public string ContractTitle { get; set; }
            public string ContractDescription { get; set; }
           

        }

        public void ErrorMessages(List<string> emsg)
        {
            //string finalErrMsg = string.Join("<br/>", emsg.ToArray());

            string finalErrMsg = string.Empty;

            finalErrMsg += "<ol type='1'>";

            if (emsg.Count > 0)
            {
                emsg.ForEach(eachErrMsg =>
                {
                    finalErrMsg += "<li>" + eachErrMsg + "</li>";
                });

                finalErrMsg += "</ol>";
            }

            cvUploadUtility.IsValid = false;
            cvUploadUtility.ErrorMessage = finalErrMsg;
        }

        protected void btnOpenUploaddialog_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowDialog(" + 0 + ");", true);
        }

        protected void btnOpenkendodialog_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowDialog1(" + 0 + ");", true);
        }
        protected bool CheckDate(String date)
        {
            try
            {
                DateTime dt = DateTime.Parse(date);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}