﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataContract;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Microsoft.IdentityModel.Protocols;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.Common
{
    public partial class MultiApprover : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                int UserID = Convert.ToInt32(Request.QueryString["UID"]);
                int RoleID = Convert.ToInt32(Request.QueryString["RID"]);
                int ContractTemplateID = Convert.ToInt32(Request.QueryString["TID"]);
                int SectionID = Convert.ToInt32(Request.QueryString["SID"]);

                BindUser(SectionID, ContractTemplateID, RoleID);
                BindUserlist();
            }
        }

        public static List<User> GetAllUsers_ContractNew(int customerID, int UID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Users
                             where row.IsDeleted == false
                             && row.IsActive == true
                             && row.CustomerID == customerID
                             && row.ContractRoleID != null
                             && row.ID != UID
                             select row).ToList();

                return query.ToList();
            }
        }

        private void BindUserlist()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            int ContractTemplateID = Convert.ToInt32(Request.QueryString["TID"]);

            List<UserAssignDetail> ShareList = new List<UserAssignDetail>();
            using (ContractMgmtEntities entities = new ContractMgmtEntities())
            {
                ShareList = (from row in entities.Cont_SP_GetuserassignDetails(customerID)
                             where row.IsReviewerflag.Equals("A")
                             && row.ContractID == ContractTemplateID
                             select new UserAssignDetail
                             {
                                 UserId = row.userID,
                                 UserName = row.Name,
                                 ID = row.ID
                             }).ToList();

                myRepeater.DataSource = ShareList;
                myRepeater.DataBind();
            }
        }
        protected void myRepeater_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "RemoveShare")
            {
                string Type = string.Empty;
                string[] str = e.CommandArgument.ToString().Split(',');
                int DeleteID = Convert.ToInt32(str[0]);
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                using (ContractMgmtEntities entities = new ContractMgmtEntities())
                {
                    Cont_MappingRevAppr obj = (from row in entities.Cont_MappingRevAppr
                                               where row.IsReviewerflag.Equals("A")
                                               && row.ID == DeleteID
                                               && row.CustID == customerID
                                               select row).FirstOrDefault();
                    if (obj != null)
                    {
                        obj.IsDelete = true;
                        entities.SaveChanges();
                        BindUserlist();
                    }
                }
            }
        }
        public class UserAssignDetail
        {
            public int UserId { get; set; }
            public string UserName { get; set; }
            public int ID { get; set; }
        }

        public void BindUser(int SectionID, int ContractTemplateID, int RoleID)
        {
            int userID = -1;
            userID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);

            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            var lstAllUsers = GetAllUsers_ContractNew(customerID, userID);
            if (lstAllUsers.Count > 0)
                lstAllUsers = lstAllUsers.Where(entry => entry.ContractRoleID != null).ToList();

            var internalUsers = ContractUserManagement.GetRequiredUsers(lstAllUsers, 1); //1--Internal---

            ddlUser.DataValueField = "ID";
            ddlUser.DataTextField = "Name";
            ddlUser.DataSource = internalUsers;
            ddlUser.DataBind();
            ddlUser.Items.Insert(0, new ListItem("Select Approver", "-1"));
        }
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            int UID = -1;
            UID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);

            int ContractTemplateID = Convert.ToInt32(Request.QueryString["TID"]);
            //int SectionID = Convert.ToInt32(Request.QueryString["SID"]);


            using (ContractMgmtEntities entities = new ContractMgmtEntities())
            {
                int selecteduserID = Convert.ToInt32(ddlUser.SelectedValue);

                Cont_MappingRevAppr obj = (from row in entities.Cont_MappingRevAppr
                                           where row.IsReviewerflag.Equals("A")
                                           && row.UserID == selecteduserID
                                           && row.ContractID == ContractTemplateID
                                           select row).FirstOrDefault();
                if (obj == null)
                {

                    Cont_MappingRevAppr obj1 = new Cont_MappingRevAppr();
                    obj1.ContractID = ContractTemplateID;
                    obj1.UserID = selecteduserID;
                    obj1.CustID = customerID;
                    obj1.SectionID = ContractTemplateID;
                    obj1.TemplateID = ContractTemplateID;
                    obj1.IsReviewerflag = "A";
                    obj1.IsDelete = false;
                    obj1.CreatedOn = DateTime.Now;
                    obj1.CreatedBy = UID;
                    obj1.UpdatedBy = UID;
                    obj1.UpdatedOn = DateTime.Now;

                    entities.Cont_MappingRevAppr.Add(obj1);
                    entities.SaveChanges();
                    BindUserlist();
                }
                else
                {
                    if (obj.IsDelete == true)
                    {
                        entities.Cont_MappingRevAppr.Remove(obj);
                        entities.SaveChanges();

                        Cont_MappingRevAppr obj1 = new Cont_MappingRevAppr();
                        obj1.ContractID = ContractTemplateID;
                        obj1.UserID = selecteduserID;
                        obj1.CustID = customerID;
                        obj1.SectionID = ContractTemplateID;
                        obj1.TemplateID = ContractTemplateID;
                        obj1.IsReviewerflag = "A";
                        obj1.IsDelete = false;
                        obj1.CreatedOn = DateTime.Now;
                        obj1.CreatedBy = UID;
                        obj1.UpdatedBy = UID;
                        obj1.UpdatedOn = DateTime.Now;

                        entities.Cont_MappingRevAppr.Add(obj1);
                        entities.SaveChanges();
                        BindUserlist();
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "alertmsgduplicate();", true);
                    }
                }
                //else
                //{
                //    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "alertmsgduplicate();", true);
                //}
            }
        }

        public class getTemplateTransactionDetail
        {
            public long TemplateTransactionID { get; set; }
            public long TemplateInstanceID { get; set; }
            public long ComplianceStatusID { get; set; }
            public System.DateTime CreatedOn { get; set; }
            public string Status { get; set; }
            public Nullable<System.DateTime> StatusChangedOn { get; set; }
            public string TemplateContent { get; set; }
            public long RoleID { get; set; }
            public long UserID { get; set; }
            public int sectionID { get; set; }
            public int orderID { get; set; }
            public int visibilty { get; set; }
            public string sectionHeader { get; set; }
        }
        public class ApproverSelectionDetail
        {
            public int ID { get; set; }
            public int UserID { get; set; }
        }
        public void showErrorMessages(List<string> lstErrMsgs, CustomValidator cvtoShowErrorMsg)
        {
            //string finalErrMsg = string.Join("<br/>", emsg.ToArray());

            string finalErrMsg = string.Empty;

            finalErrMsg += "<ol type='1'>";

            if (lstErrMsgs.Count > 0)
            {
                lstErrMsgs.ForEach(eachErrMsg =>
                {
                    finalErrMsg += "<li>" + eachErrMsg + "</li>";
                });

                finalErrMsg += "</ol>";
            }

            cvtoShowErrorMsg.IsValid = false;
            cvtoShowErrorMsg.ErrorMessage = finalErrMsg;
        }

        protected void btnsubmitforrev_Click(object sender, EventArgs e)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                int UserID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                int RoleID = Convert.ToInt32(Request.QueryString["RID"]);
                int ContractTemplateID = Convert.ToInt32(Request.QueryString["TID"]);

                List<int> ShareList = new List<int>();
                using (ContractMgmtEntities entities1 = new ContractMgmtEntities())
                {
                    ShareList = (from row in entities1.Cont_SP_GetuserassignDetails(customerID)
                                 where row.IsReviewerflag.Equals("A")
                                 && row.ContractID == ContractTemplateID
                                 select row.userID).Distinct().ToList();
                }
                if (ShareList.Count > 0)
                {
                    List<string> lstErrorMsg = new List<string>();
                    int Count = ShareList.Count;
                    int flagvalidation = 0;
                    bool flag = false;
                    List<ApproverSelectionDetail> Apprpverlist = new List<ApproverSelectionDetail>();

                    int cnt = 0;
                    foreach (var item in ShareList)
                    {
                        cnt = cnt + 1;
                        int Countcheck = Apprpverlist.Where(x => x.UserID == Convert.ToInt32(item)).ToList().Count;
                        if (Countcheck > 0)
                        {
                            flag = true;
                        }
                        else
                        {
                            Apprpverlist.Add(new ApproverSelectionDetail { ID = Convert.ToInt32(cnt), UserID = Convert.ToInt32(item) });
                        }
                    }
                    if (flag)
                    {
                        lstErrorMsg.Add("Please Select Different Approver");

                        if (lstErrorMsg.Count > 0)
                        {
                            //showErrorMessages(lstErrorMsg, CustomValidator1);
                        }
                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenApprovarSectionModal();", true);
                    }
                    else
                    {
                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        {

                            List<ContractTemplateAssignment> objlst = (from row in entities.ContractTemplateAssignments
                                                                       where row.ContractTemplateInstanceID == ContractTemplateID
                                                                       && row.RoleID == 6
                                                                       select row).ToList();

                            foreach (var item1 in objlst)
                            {
                                ContractTemplateAssignment objlst1 = (from row in entities.ContractTemplateAssignments
                                                                      where row.ContractTemplateInstanceID == ContractTemplateID
                                                                      && row.ID == item1.ID
                                                                      && row.RoleID == 6
                                                                      select row).FirstOrDefault();
                                if (objlst1 != null)
                                {
                                    entities.ContractTemplateAssignments.Remove(objlst1);
                                    entities.SaveChanges();

                                }
                            }


                            string ContractVersion = ContractTemplateManagement.getVersion(Convert.ToInt32(ContractTemplateID), customerID);
                            var data1 = (from row in entities.Sp_GetContractTemplateDetailTransaction()
                                         where row.UserID == UserID
                                         && row.ContractTemplateInstanceID == ContractTemplateID
                                         && row.RoleID == RoleID
                                         select row).ToList();

                            var gettemplatesection = (from row in data1
                                                      join row1 in entities.Cont_tbl_ContractTemplateSectionMapping
                                                      on row.sectionID equals row1.SectionID
                                                      where row1.ContractTemplateInstanceID == ContractTemplateID
                                                      select new getTemplateTransactionDetail
                                                      {
                                                          TemplateTransactionID = row.TemplateTransactionID,
                                                          TemplateInstanceID = row.ContractTemplateInstanceID,
                                                          ComplianceStatusID = row.ComplianceStatusID,
                                                          CreatedOn = row.CreatedOn,
                                                          Status = row.Status,
                                                          StatusChangedOn = row.StatusChangedOn,
                                                          TemplateContent = row.ContractTemplateContent,
                                                          RoleID = row.RoleID,
                                                          UserID = row.UserID,
                                                          sectionID = Convert.ToInt32(row.sectionID),
                                                          orderID = Convert.ToInt32(row1.SectionOrder)
                                                      }).OrderBy(x => x.orderID).ToList();

                            if (gettemplatesection.Count > 0)
                            {
                                ContractTemplateManagement.DeleteContractTemplateAssignmentApprover(Convert.ToInt32(ContractTemplateID));
                                foreach (var item in gettemplatesection)
                                {
                                    if (item.ComplianceStatusID == 17)
                                    {
                                        foreach (var detail in Apprpverlist)
                                        {
                                            int ApproverID = detail.UserID;
                                            ContractTemplateAssignment ContracttemplateAssignment = new ContractTemplateAssignment()
                                            {
                                                ContractTemplateInstanceID = Convert.ToInt32(ContractTemplateID),
                                                RoleID = 6,
                                                UserID = ApproverID,
                                                //IsActive = true,
                                                SectionID = item.sectionID,
                                                Visibility = 1,
                                                IsAction = 0,
                                                ApproveOrder = detail.ID
                                            };
                                            if (detail.ID == 1)
                                            {
                                                ContracttemplateAssignment.IsActive = true;
                                            }
                                            else
                                            {
                                                ContracttemplateAssignment.IsActive = false;
                                            }
                                            ContractTemplateManagement.CreateContractTemplateAssignmentApprover(ContracttemplateAssignment);
                                        }
                                        string templateSectionContent = item.TemplateContent;

                                        ContractTemplateTransaction obj1 = new ContractTemplateTransaction();
                                        obj1.ContractTemplateInstanceID = Convert.ToInt32(ContractTemplateID);
                                        obj1.CreatedOn = DateTime.Now;
                                        obj1.CreatedBy = UserID;
                                        obj1.ContractTemplateContent = templateSectionContent;
                                        obj1.CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User;
                                        obj1.Status = 4;
                                        obj1.OrderID = 1;
                                        obj1.Version = ContractVersion;
                                        obj1.SectionID = item.sectionID;
                                        entities.ContractTemplateTransactions.Add(obj1);
                                        entities.SaveChanges();
                                    }
                                }
                                Cont_tbl_ContractStatusTransaction newStatusRecord = new Cont_tbl_ContractStatusTransaction()
                                {
                                    CustomerID = customerID,
                                    ContractID = Convert.ToInt32(ContractTemplateID),
                                    StatusID = 4,
                                    StatusChangeOn = DateTime.Now,
                                    IsActive = true,
                                    CreatedBy = UserID,
                                    UpdatedBy = UserID,
                                };
                                ContractManagement.CreateContractStatusTransaction(newStatusRecord);
                                if (Apprpverlist.Count > 0)
                                {
                                    List<int> revuser = new List<int>();
                                    revuser = Apprpverlist.Select(x => x.UserID).ToList();
                                    revuser = revuser.Distinct().ToList();
                                    SendReviewers(revuser, ContractTemplateID, Convert.ToInt32(AuthenticationHelper.CustomerID));
                                }
                            }
                        }
                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "closepopup();", true);
                        //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "closedRefreshModel();", true);
                    }
                    //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "closepopup();", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "alertmsg();", true);
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void SendReviewers(List<int> revid, int ContractTemplateID, int cid)
        {
            if (revid.Count > 0)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var contractdata = (from row in entities.Cont_tbl_ContractInstance
                                        where row.ID == ContractTemplateID
                                        && row.IsDeleted == false
                                        select row).FirstOrDefault();

                    if (contractdata != null)
                    {
                        foreach (var item in revid)
                        {
                            try
                            {
                                User data = (from row in entities.Users
                                             where row.ID == item
                                             && row.CustomerID == cid
                                             && row.IsDeleted == false
                                             select row).FirstOrDefault();
                                if (data != null)
                                {
                                    string reviewername = data.FirstName + " " + data.LastName;
                                    string contname = contractdata.ContractTitle;
                                    string contnum = contractdata.ContractNo;

                                    string header = "Dear " + reviewername + "<br>" + "<br>" + "Contract has been shared with you for review." + "<br>" + "<br>";

                                    string ContractName = "Contract Name - " + contname + "<br>" + "<br>";

                                    string ContractNo = "Contract no. - " + contnum + "<br>" + "<br>";

                                    string FinalMail = header + ContractName + ContractNo;

                                    string SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"].ToString();

                                    string ReviewerEmail = data.Email;

                                    EmailManager.SendMail(SenderEmailAddress, new List<String>(new String[] { ReviewerEmail }), null, null, "You have received a contract for review", FinalMail);
                                }
                            }
                            catch (Exception ex)
                            {
                                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                            }
                        }
                    }
                }
            }
        }
    }
}