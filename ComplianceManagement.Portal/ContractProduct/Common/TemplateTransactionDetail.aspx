﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TemplateTransactionDetail.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.Common.TemplateTransactionDetail" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns='http://www.w3.org/1999/xhtml'>
<head runat="server">
    <title>Telerik ASP.NET Example</title>
     <link href="../../NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />   
    <link href="../../NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="../../NewCSS/contract_custom_style.css" rel="stylesheet" />
    <link href="../../NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>
    <link href="../../NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />    
    <link href="../../NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <link href="../../NewCSS/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
    <script src="../../Newjs/bootstrap-multiselect.js" type="text/javascript"></script>
    <script src="../../Newjs/bootstrap-tagsinput.js"></script>
    <link href="../../NewCSS/bootstrap-tagsinput.css" rel="stylesheet" />
    <link href="../../NewCSS/timeline.css" rel="stylesheet" />    
    <script type="text/javascript" src="../../Newjs/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery.nicescroll.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
      <link href="~/NewCSS/contract_custom_style.css" rel="stylesheet" />
    <link href="../../Style/css/StyleSheetTextEditor.css" rel="stylesheet" />
    <script>
        
            var editor, range;
            function OnClientLoad(sender, args) {
                editor = sender;
            }

            function OnClientSelectionChange(sender, args) {
                range = editor.getSelection().getRange(true);
            }
            
                        
            function CloseModel() {
                window.parent.parent.CloseContractModal();
            }
            //function GetAllTransaction()
            //{
            //    //document.getElementById("getHistryInsert").value = "";
            //    //document.getElementById("getHistryDel").value = "";
            //    //if (editor._contentArea.getElementsByTagName("ins").length > 0)
            //    //{                    
            //    //    var getdetails = "";
            //    //    for (var i = 0; i < editor._contentArea.getElementsByTagName("ins").length; i++)
            //    //    {
            //    //        if (document.getElementById("lblUser").innerText == editor._contentArea.getElementsByTagName("ins")[i].getAttribute("author"))
            //    //        {
            //    //            var text = editor._contentArea.getElementsByTagName("ins")[i].innerText + "andand";
            //    //            var author = editor._contentArea.getElementsByTagName("ins")[i].getAttribute("author") + "andand";
            //    //            var title = editor._contentArea.getElementsByTagName("ins")[i]["title"] + "andand";

            //    //            var res = text.concat(author, title);
            //    //            getdetails = getdetails.concat(res + "endend");
            //    //        }
            //    //    }
            //    //    document.getElementById("getHistryInsert").value = getdetails;
            //    //}


            //    //if (editor._contentArea.getElementsByTagName("del").length > 0) {
            //    //    var getdeletedetails = "";
            //    //    for (var i = 0; i < editor._contentArea.getElementsByTagName("del").length; i++) {
            //    //        if (document.getElementById("lblUser").innerText == editor._contentArea.getElementsByTagName("del")[i].getAttribute("author")) {
            //    //            var text1 = editor._contentArea.getElementsByTagName("del")[i].innerText + "andand";
            //    //            var author1 = editor._contentArea.getElementsByTagName("del")[i].getAttribute("author") + "andand";
            //    //            var title1 = editor._contentArea.getElementsByTagName("del")[i]["title"] + "andand";

            //    //            var res1 = text1.concat(author1, title1);
            //    //            getdeletedetails = getdeletedetails.concat(res1 + "endend");
            //    //        }
            //    //    }
            //    //    document.getElementById("getHistryDel").value = getdeletedetails;
            //    //}
            //    return true;
            //}

            function OpenSectionModal(UId, RoleId, templateID, custID, sectionID)
            {
                $('#TemplateSectionPopup').modal('show');                
                return false;
            }
            
            function CloseSectionModal()
            {
                $('#TemplateSectionPopup').modal('hide');                
                document.getElementById('btnsectionbind').click();           
            }
            function closedpublish() {
                window.parent.parent.CloseContractModal();
            }

            function closedTemplate()
            {
                window.parent.parent.CloseContractModal();
            }
        // Specify the normal table row background color
        //   and the background color for when the mouse 
        //   hovers over the table row.

        var TableBackgroundNormalColor = "#ffffff";
        var TableBackgroundMouseoverColor = "#D3D3D3";

        // These two functions need no customization.
        function ChangeBackgroundColor(row) { row.style.backgroundColor = TableBackgroundMouseoverColor; }
        function RestoreBackgroundColor(row) { row.style.backgroundColor = TableBackgroundNormalColor; }

    </script> 
</head>
 
<body>
    <form id="form1" runat="server">

     <div class="row">
            <div class="col-md-12 colpadding0">
             <div class="col-md-2 colpadding0" style="overflow-y: auto;overflow-x: hidden;max-height: 457px;">  
              <div class="col-md-12 colpadding0">
                <asp:Repeater ID="rptComplianceVersionView"
                            OnItemCommand="rptComplianceVersionView_ItemCommand" runat="server">
                            <HeaderTemplate>
                            </HeaderTemplate>
                            <ItemTemplate>
                            <asp:LinkButton CommandName="View" CommandArgument='<%# Eval("SectionID")+","+Eval("SectionStatusID") %>' ID="lblDocumentVersionView"
                                              style="" Font-Underline="false"  runat="server">
                                <table  onmouseover="ChangeBackgroundColor(this)" onmouseout="RestoreBackgroundColor(this)" 
                                style="border-collapse: separate; border: 1px solid rgb(229, 229, 229); border-radius: 3px; padding: 6px; margin-bottom: 7px; width: 95%;">
                                    <tr>
                                        <td>  <%#Eval("Header")%></td>
                                    </tr>
                                    <tr>                                         
                                         <td><%#Eval("SectionStatus")%></td>
                                    </tr>
                                     <tr>                                         
                                         <td style='color:<%#Eval("DeptColor")%>;font-size: 16px;'><%#Eval("DeptName")%></td>
                                    </tr>
                                     <tr>                                         
                                         <td><%#Eval("SectionReviwerName")%></td>
                                    </tr>
                                    <tr> 
                                        <td><%# Eval("CreatedOn") != null ? ((DateTime)Eval("CreatedOn")).ToString("dd-MM-yyyy  h:mm tt") : "" %></td>                                        
                                    </tr>
                                </table>
                                </asp:LinkButton>
                            </ItemTemplate>
                            <FooterTemplate>
                            </FooterTemplate>
                        </asp:Repeater>
              </div>
            </div>
           <div class="col-md-8 colpadding0" style="margin-left: 7px;">
                <div class="row">
                    <div class="col-md-12 colpadding0">     
                        <div class="col-md-1 colpadding0" style="display: none;">
                            User :
                            <asp:Label ID="lblUser" runat="server" Text="P"></asp:Label>
                            <asp:Button ID="Button1" runat="server" CssClass="btn btn-primary" Text="Save Version" OnClick="Button1_Click" />
                        </div>
                        <div class="col-md-1 colpadding0" style="display: none;">
                            <asp:Button ID="btnSave" runat="server" CssClass="btn btn-primary" Text="Html" OnClick="btnSave_Click" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 colpadding0">
                        <div class="col-md-1 colpadding0">
                            <asp:TextBox ID="getHistryInsert" runat="server" Style="display: none;"></asp:TextBox>
                            <asp:TextBox ID="getHistryDel" runat="server" Style="display: none;"></asp:TextBox>
                        </div>
                    </div>
                </div>
                 <div class="row" style="padding-top: 6px;">
                <div class="col-md-12 colpadding0" style="width: 1045px;">
                        <div class="col-md-5 colpadding0">
                        </div>
                        <div class="col-md-2 colpadding0" style="text-align:center;">
                            <asp:Button ID="btnsectionbind" runat="server" style="float: right;display:none;" OnClick="btnsectionbind_Click" CssClass="btn btn-primary" Text="Bind section with template" />
                        </div>
                        <div class="col-md-5 colpadding0">
                        <asp:Button ID="BtnExportDocument" runat="server" style="float: right;" OnClick="BtnExportDocument_Click" CssClass="btn btn-primary" Text="Export Document" />
                                                
                        <asp:Button ID="BtnPublish" runat="server" style="float: right;margin-right: 9px;" OnClick="BtnPublish_Click" CssClass="btn btn-primary" Text="Publish" />
                        </div>
                        </div>
                </div> 
                <div class="row">
                    <div class="col-md-12 colpadding0" style="margin-left: -2px;">
                        <telerik:RadScriptManager runat="server" ID="RadScriptManager1" />
                        <telerik:RadSkinManager ID="RadSkinManager1" runat="server" ShowChooser="true" Visible="false" />
                        <div class="demo-containers" style="padding-top: 6px;">
                            <div class="demo-container">
                                <telerik:RadEditor RenderMode="Lightweight" ID="theEditor" StripFormattingOptions="MSWord" EnableTrackChanges="true" runat="server"
                                    Width="1055px" OnClientLoad="OnClientLoad" OnClientSelectionChange="OnClientSelectionChange"
                                    Height="444px" ToolsFile="../../ToolsFile.xml" OnExportContent="RadEditor1_ExportContent"
                                    ContentFilters="DefaultFilters, PdfExportFilter"
                                    SkinID="WordLikeExperience" EditModes="Preview">
                                    <RealFontSizes>
                                        <telerik:EditorRealFontSize Value="12pt" />
                                        <telerik:EditorRealFontSize Value="18pt" />
                                        <telerik:EditorRealFontSize Value="22px" />
                                    </RealFontSizes>
                                    <ExportSettings>
                                        <Docx DefaultFontName="Arial" DefaultFontSizeInPoints="12" HeaderFontSizeInPoints="8"
                                            PageHeader="Some header text for DOCX documents" />
                                        <Rtf DefaultFontName="Times New Roman" DefaultFontSizeInPoints="13"
                                            HeaderFontSizeInPoints="9" PageHeader="Some header text for RTF documents" />
                                    </ExportSettings>
                                    <TrackChangesSettings Author="RadEditorUser" CanAcceptTrackChanges="true"
                                        UserCssId="reU0"></TrackChangesSettings>
                                    <Content>                  
                                    </Content>
                                </telerik:RadEditor>
                            </div>
                        </div>
                        <telerik:RadAjaxManager runat="server" ID="RadAjaxManager1">
                            <AjaxSettings>
                                <telerik:AjaxSetting AjaxControlID="ConfiguratorPanel1">
                                    <UpdatedControls>
                                        <telerik:AjaxUpdatedControl ControlID="theEditor" LoadingPanelID="RadAjaxLoadingPanel1" />
                                        <telerik:AjaxUpdatedControl ControlID="ConfiguratorPanel1" />
                                    </UpdatedControls>
                                </telerik:AjaxSetting>
                            </AjaxSettings>
                        </telerik:RadAjaxManager>
                        <telerik:RadAjaxLoadingPanel runat="server" ID="RadAjaxLoadingPanel1">
                        </telerik:RadAjaxLoadingPanel>
                        <div runat="server" id="divHtml"></div>
                    </div>
                </div>
                            
        </div>
         </div>
        </div>
         <div class="modal fade" id="TemplateSectionPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content" style="width: 1018px;margin-left: -203px;">
                        <div class="modal-header" style="margin-bottom: 0px;border-bottom: 0px;">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="CloseSectionModal()">&times;</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                            <iframe id="showdetails" src="../Common/SectionTemplateTransactionDetail.aspx?UID=<% =UId%>&RID=<% =RoleId%>&TID=<% =TemplateId%>&CID=<% =CustId%>&SID=<% =SectionId%>" width="100%" height="525px;" frameborder="0"></iframe>                            
                        </div>
                    </div>
                </div>
            </div>
    </form>
</body>
</html>
