﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Telerik.Web.UI;
using Telerik.Web.UI.Editor;
using Telerik.Web.UI.Editor.Export;
using Telerik.Windows.Documents.Flow.FormatProviders.Docx;
using Telerik.Windows.Documents.Flow.Model;
using Telerik.Windows.Documents.Flow.Model.Styles;


namespace com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.Common
{
    public partial class TemplateTransactionHistry : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                int UserID = Convert.ToInt32(Request.QueryString["UID"]);
                int RoleID = Convert.ToInt32(Request.QueryString["RID"]);
                int TemplateID = Convert.ToInt32(Request.QueryString["TID"]);

                BindTransactionHistory(TemplateID, UserID, RoleID);
                //theEditor.Content = data.TemplateContent;
            }
        }

        public void BindTransactionHistory(long TemplateInstanceID,long UserID, long RoleID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                //var entitiesData = (from row in entities.TemplateTransactions
                //                    where row.TemplateInstanceID == TemplateInstanceID
                //                    select row).ToList();

                var entitiesData = (from row in entities.TemplateTransactionHistories
                                    where row.TemplateID == TemplateInstanceID
                                    select row).ToList();

                if (RoleID == 3)
                {
                    rptComplianceVersionView.DataSource = entitiesData.OrderByDescending(entry => entry.CreatedOn);
                    rptComplianceVersionView.DataBind();
                }
                else
                {
                    var PerformerUserID = (from row in entities.TemplateAssignments
                                           where row.TemplateInstanceID == TemplateInstanceID
                                           && row.IsActive == true
                                           select row.UserID).FirstOrDefault();
                    if (PerformerUserID != null)
                    {
                        List<long> obj = new List<long>();
                        obj.Add(PerformerUserID);
                        obj.Add(UserID);

                        entitiesData = entitiesData.Where(x => obj.Contains(x.CreatedBy)).ToList();

                        rptComplianceVersionView.DataSource = entitiesData.OrderByDescending(entry => entry.CreatedOn);
                        rptComplianceVersionView.DataBind();
                    }
                }
            }
        }
        protected void rptComplianceVersionView_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                if (e.CommandName.Equals("View"))
                {
                    theEditor.Content = "";
                    string[] commandArg = e.CommandArgument.ToString().Split(',');
                    int TransactionID = Convert.ToInt32(commandArg[0]);
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        int TemplateID = Convert.ToInt32(Request.QueryString["TID"]);

                        var entitiesData = (from row in entities.TemplateTransactionHistories
                                            where row.TemplateID == TemplateID
                                            && row.ID == TransactionID
                                            select row).FirstOrDefault();
                        if (entitiesData != null)
                        {
                            theEditor.Content = entitiesData.TemplateContent;
                        }                        
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

    }
}