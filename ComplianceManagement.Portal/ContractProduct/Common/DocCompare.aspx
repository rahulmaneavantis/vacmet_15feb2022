﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DocCompare.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.Common.DocCompare" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
      <link href="../../NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
            <!-- bootstrap theme -->
    <link href="../../NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />  
    <link href="../../NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet"  type="text/css" />
    <link href="../../Style/css/StyleSheetTextEditor.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
    <div class="row" style="margin-left: 24%;">
        <div class="col-md-12 colpadding0" style="padding: 5%;">
            <div class="col-md-2 colpadding0">
                <asp:Label>Document 1</asp:Label>
            </div>
            <div class="col-md-10 colpadding0">
                 <asp:FileUpload ID="ContractFileUpload" runat="server" AllowMultiple="false" />
            </div>
        </div>
         <div class="col-md-12 colpadding0" style="padding: 5%;">
            <div class="col-md-2 colpadding0">
                <asp:Label>Document 2</asp:Label>
            </div>
            <div class="col-md-10 colpadding0">
                  <asp:FileUpload ID="FileUpload1" runat="server" AllowMultiple="false" />
            </div>
        </div>
   
       
    </div>
        <div style="margin-left: 30%;">
             <asp:Button ID="btnUploadFile" runat="server" Text="Upload" 
                                        class="btn btn-primary" OnClick="btnUploadFile_Click" />

             <%--<asp:LinkButton ID="lnkDocumentUpload" runat="server" CssClass="btn btn-primary"
                                Text="<i class='fa fa-upload' aria-hidden='true'></i> Upload" OnClick="lnkDocumentUpload_Click"
                                ToolTip="Upload Selected Document(s)" data-toggle="tooltip">
                            </asp:LinkButton>--%>
        </div>
    </form>
</body>
</html>

