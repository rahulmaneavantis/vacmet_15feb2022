﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ContractKendoGrid.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.Common.ContractKendoGrid" %>

<!DOCTYPE html>


<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jszip.min.js"></script>
    <link href="../../NewCSS/bootstrap.min.css" rel="stylesheet" />
    <script src="../../Newjs/bootstrap.min.js"></script>
    <script type="text/javascript" src="../Newjs/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.14.1/moment.min.js"></script>

    
    <style type="text/css">

        .k-grid-header-locked + .k-grid-header-wrap {
            overflow: hidden;
        }

        .media, .media-body, .modal-open, .progress {
            overflow: scroll;
        }

        .modal {
            display: none;
            overflow: hidden;
            overflow-y: hidden;
            position: relative;
            z-index: 1040;
        }


        .Dashboard-white-widget {
            background: #fff;
            padding: 20px 10px 10px;
            margin-bottom: 20px;
            border-radius: 10px;
        }

        .k-grid-content {
            min-height: 380px !important;
            overflow: hidden;
        }

        .k-auto-scrollable {
            overflow: scroll;
        }



        input[type=checkbox], input[type=radio] {
            margin: 4px 4px 0;
            line-height: normal;
        }

        .div.k-grid-footer, div.k-grid-header {
            border-top-width: 1px;
            margin-right: -2px;
        }

        .k-grid-footer-wrap, .k-grid-header-wrap {
            position: relative;
            width: 100%;
            overflow: hidden;
            border-style: solid;
            border-width: 0 1px 0 0;
            zoom: 1;
        }

        .k-grid-content {
            min-height: 380px !important;
        }

        #grid1 .k-grid-content {
            min-height: 320px !important;
        }

        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        #grid .k-grouping-header {
            font-style: italic;
            margin-top: 0px;
            background-color: white;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 2.0em;
            border-bottom-width: 1px;
            background-color: white;
            border-width: 0 1px 1px 0px;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
            background-color: white;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: white;
        }

        #grid .k-grid-toolbar {
            background: white;
        }

        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 0px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: white;
        }

        html .k-grid tr.k-alt:hover {
            background: white;
        }

        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            background: white;
            border: none;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow-y: scroll;
        }

        .k-grid-header {
            padding-right: 0px !important;
            margin-right: 2px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: white;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 0px 1px 0px;
            text-align: center;
        }

        .k-grid-pager {
            margin-top: -1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-i-arrow-60-down, .k-i-arrow-60-left, .k-i-arrow-60-right, .k-i-arrow-60-up {
            cursor: pointer;
            margin-top: 6px;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            border-width: 0px 0px 1px 0px;
            background: #E9EAEA;
            font-weight: bold;
            margin-right: 18px;
            font-family: 'Roboto',sans-serif;
            font-size: 15px;
            color: rgba(0, 0, 0, 0.5);
            height: 20px;
            vertical-align: middle;
        }

        .k-dropdown-wrap.k-state-default {
            background-color: white;
        }

        .k-popup.k-calendar-container, .k-popup.k-list-container {
            background-color: white;
        }

        .k-grid-header th.k-state-focused, .k-list > .k-state-focused, .k-listview > .k-state-focused, .k-state-focused, td.k-state-focused {
            -webkit-box-shadow: inset 0 0 3px 1px white;
            box-shadow: inset 0 0 3px 1px white;
        }

        label.k-label {
            font-family: roboto,sans-serif !important;
            color: #515967;
            /* font-stretch: 100%; */
            font-style: normal;
            font-weight: 400;
            min-width: 362px;
            white-space: pre-wrap;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: flex;
            margin-bottom: 0px;
        }

        .k-state-default > .k-select {
            border-color: #ceced2;
            margin-top: 0px;
        }

        .k-grid-norecords {
            width: 100%;
            height: 100%;
            text-align: center;
            margin-top: 2%;
        }

        /*#grid .k-grouping-header {
            font-style: italic;
            margin-top: -4px;
            background-color: white;
        }*/
        .k-grouping-header {
            font-style: italic;
            background-color: white;
        }

        .k-grid-toolbar {
            background: white;
            border: none;
        }

        .k-grid table {
            width: 100.5%;
        }

        .k-active-filter, .k-state-active, .k-state-active:hover {
            background-color: #E9EAEA;
            border-color: #a6a6ad;
            color: #535b6a;
        }

        .k-multiselect-wrap .k-input {
            /*padding-top:6px;*/
            display: inherit !important;
        }
    </style>

    <script type="text/javascript">
        $(document).ready(function () {

            // $("#grid").data("kendoGrid").wrapper.find(".k-grid-header-wrap").off("scroll.kendoGrid");
            Bindgrid();

        });

        var record = 0;

        function Bindgrid() {
            var grid = $('#grid').data("kendoGrid");
            if (grid != undefined || grid != null)
                $('#grid').empty();

            var grid = $("#grid").kendoGrid({
                dataSource: {
                    transport: {

                        read: '../../Setup/getAllContractdetails'
                    },
                    pageSize: 10
                },
                toolbar: ["excel"],
                excel: {
                    allPages: true
                },
                excelExport: function (e) {
                    var sheet = e.workbook.sheets[0];
                    e.workbook.sheets[0].title = "ContractDetails";

                    var grid = e.sender;
                    var fields = grid.dataSource.options.fields;
                    var fieldsModels = grid.dataSource.options.schema.model.fields;
                    var columns = grid.columns;
                    var dateCells = [];
                    e.workbook.fileName = " ContractUpdate" + kendo.toString(new Date, "d");
                    //  e.workbook.fileName = "ContractUpdate.xlsx" + kendo.toString(new Date, "dd/MM/yyyy HH:mm");

                    debugger;
                    for (var rowIndex = 1; rowIndex < sheet.rows.length; rowIndex++) {
                        debugger;

                        var row = sheet.rows[rowIndex];
                        for (var cellIndex = 0; cellIndex < row.cells.length; cellIndex++) {
                            if (cellIndex > 0)
                            {
                                debugger;
                                //alert(row.cells[cellIndex].value);
                                //alert(sheet.rows[0].cells[cellIndex].value);

                                if (sheet.rows[0].cells[cellIndex].value == "ProposalDate(dd/MM/yyyy)")
                                {
                                    debugger;
                                    var ProposalDate = sheet.rows[rowIndex].cells[cellIndex].value;
                                    if (ProposalDate != null && ProposalDate != undefined) {
                                        var ProposalDateValue = new Date(ProposalDate.getFullYear(), ProposalDate.getMonth(), ProposalDate.getDay());
                                        sheet.rows[rowIndex].cells[cellIndex].value = ProposalDateValue;
                                        sheet.rows[rowIndex].cells[cellIndex].format = "dd-MMM-yyyy";
                                    }

                                }


                                if (sheet.rows[0].cells[cellIndex].value == "AgreementDate(dd/MM/yyyy)") {
                                    var AgreementDate = sheet.rows[rowIndex].cells[cellIndex].value;
                                    if (AgreementDate != null && AgreementDate != undefined) {
                                        var AgreementDateValue = new Date(AgreementDate.getFullYear(), AgreementDate.getMonth(), AgreementDate.getDay());
                                        sheet.rows[rowIndex].cells[cellIndex].value = AgreementDateValue;
                                        sheet.rows[rowIndex].cells[cellIndex].format = "dd-MMM-yyyy";
                                    }
                                }

                                if (sheet.rows[0].cells[cellIndex].value == "EffectiveDate(dd/MM/yyyy)") {
                                    var EffectiveDate = sheet.rows[rowIndex].cells[cellIndex].value;
                                    if (EffectiveDate != null && EffectiveDate != undefined) {
                                        var EffectiveDateValue = new Date(EffectiveDate.getFullYear(), EffectiveDate.getMonth(), EffectiveDate.getDay());
                                        sheet.rows[rowIndex].cells[cellIndex].value = EffectiveDateValue;
                                        sheet.rows[rowIndex].cells[cellIndex].format = "dd-MMM-yyyy";
                                    }
                                }

                                if (sheet.rows[0].cells[cellIndex].value == "ReviewDate(dd/MM/yyyy)") {
                                    var ReviewDate = sheet.rows[rowIndex].cells[cellIndex].value;
                                    if (ReviewDate != null && ReviewDate != undefined) {
                                        var ReviewDateValue = new Date(ReviewDate.getFullYear(), ReviewDate.getMonth(), ReviewDate.getDay());
                                        sheet.rows[rowIndex].cells[cellIndex].value = ReviewDateValue;
                                        sheet.rows[rowIndex].cells[cellIndex].format = "dd-MMM-yyyy";
                                    }
                                }

                         
                                if (sheet.rows[0].cells[cellIndex].value == "ExpirationDate(dd/MM/yyyy)") {
                                    var ExpirationDate = sheet.rows[rowIndex].cells[cellIndex].value;
                                    if (ExpirationDate != null && ExpirationDate != undefined) {
                                        var ExpirationDateValue = new Date(ExpirationDate.getFullYear(), ExpirationDate.getMonth(), ExpirationDate.getDay());
                                        sheet.rows[rowIndex].cells[cellIndex].value = ExpirationDateValue;
                                        sheet.rows[rowIndex].cells[cellIndex].format = "dd-MMM-yyyy";
                                    }
                                }
                                if (sheet.rows[0].cells[cellIndex].value == "Lock-in-period") {
                                    var LockInPeriod = sheet.rows[rowIndex].cells[cellIndex].value;
                                    if (LockInPeriod != null && LockInPeriod != undefined) {
                                        var LockInPeriodValue = new Date(LockInPeriod.getFullYear(), LockInPeriod.getMonth(), LockInPeriod.getDay());
                                        sheet.rows[rowIndex].cells[cellIndex].value = LockInPeriodValue;
                                        sheet.rows[rowIndex].cells[cellIndex].format = "dd-MMM-yyyy";
                                    }
                                }
                              

                            }


                            row.cells[cellIndex].borderLeft = "#000000";
                            row.cells[cellIndex].borderBottom = "#000000";
                            row.cells[cellIndex].borderTop = "#000000";
                            row.cells[cellIndex].borderRight = "#000000";


                            if (row.cells[cellIndex].value == "ContractNo") {
                                sheet.rows[0].cells[cellIndex].background = "#ff0000"
                            }

                            if (row.cells[cellIndex].value == "ContractTitle") {
                                sheet.rows[0].cells[cellIndex].background = "#ff0000"
                            }

                            if (row.cells[cellIndex].value == "ContractDetailDesc") {
                                sheet.rows[0].cells[cellIndex].background = "#ff0000"
                            }

                            if (row.cells[cellIndex].value == "BranchName") {
                                sheet.rows[0].cells[cellIndex].background = "#ff0000"
                            }

                            if (row.cells[cellIndex].value == "VendorNames") {
                                sheet.rows[0].cells[cellIndex].background = "#ff0000"
                            }

                            if (row.cells[cellIndex].value == "DeptName") {
                                sheet.rows[0].cells[cellIndex].background = "#ff0000"
                            }

                            if (row.cells[cellIndex].value == "TypeName") {
                                sheet.rows[0].cells[cellIndex].background = "#ff0000"
                            }

                            if (row.cells[cellIndex].value == "Owner") {
                                sheet.rows[0].cells[cellIndex].background = "#ff0000"
                            }
                        }
                    }


                },


                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                sortable: true,
                filterable: true,
                columnMenu: true,
                pageable: true,
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,

                groupable: true,
                scrollable: true,

               // allpages:true,

                dataBinding: function () {
                    record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                },
                columns: [

                {
                    width: "150px",
                    field: "ContractNo", title: 'ContractNo',
                    locked: true,
                    lockable: false,
                    attributes: {
                        style: 'white-space: nowrap;'

                    }, filterable: {
                        multi: true,
                        extra: false,
                        search: true,
                        operators: {
                            string: {
                                eq: "Is equal to",
                                neq: "Is not equal to",
                                contains: "Contains"
                            }
                        }
                    }
                },
                    {
                        lockable: false,
                        width: "200px",
                        field: "ContractTitle", title: 'ContractTitle',
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        lockable: false,
                        width: "200px",
                        field: "ContractDetailDesc", title: 'ContractDetailDesc',
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },

                   {
                       lockable: false,
                       width: "150px",
                       field: "BranchName", title: 'BranchName',
                       attributes: {
                           style: 'white-space: nowrap;'

                       }, filterable: {
                           multi: true,
                           extra: false,
                           search: true,
                           operators: {
                               string: {
                                   eq: "Is equal to",
                                   neq: "Is not equal to",
                                   contains: "Contains"
                               }
                           }
                       }
                   },
                   {
                       lockable: false,
                       width: "150px",
                       field: "VendorNames", title: 'VendorNames',
                       attributes: {
                           style: 'white-space: nowrap;'

                       }, filterable: {
                           multi: true,
                           extra: false,
                           search: true,
                           operators: {
                               string: {
                                   eq: "Is equal to",
                                   neq: "Is not equal to",
                                   contains: "Contains"
                               }
                           }
                       }
                   },
                   {
                       lockable: false,
                       width: "100px",
                       field: "DeptName", title: 'DeptName',
                       attributes: {
                           style: 'white-space: nowrap;'

                       }, filterable: {
                           multi: true,
                           extra: false,
                           search: true,
                           operators: {
                               string: {
                                   eq: "Is equal to",
                                   neq: "Is not equal to",
                                   contains: "Contains"
                               }
                           }
                       }
                   },
                   {
                       lockable: false,
                       width: "100px",
                       field: "TypeName", title: 'TypeName',
                       attributes: {
                           style: 'white-space: nowrap;'

                       }, filterable: {
                           multi: true,
                           extra: false,
                           search: true,
                           operators: {
                               string: {
                                   eq: "Is equal to",
                                   neq: "Is not equal to",
                                   contains: "Contains"
                               }
                           }
                       }
                   },
                   {
                       lockable: false,
                       width: "100px",
                       field: "SubTypeName", title: 'SubTypeName',
                       attributes: {
                           style: 'white-space: nowrap;'

                       }, filterable: {
                           multi: true,
                           extra: false,
                           search: true,
                           operators: {
                               string: {
                                   eq: "Is equal to",
                                   neq: "Is not equal to",
                                   contains: "Contains"
                               }
                           }
                       }
                   },

                     {
                         lockable: false,
                         width: "100px",
                         field: "ContactPersonOfDepartment", title: 'ContactPersonOfDepartment',
                         attributes: {
                             style: 'white-space: nowrap;'

                         }, filterable: {
                             multi: true,
                             extra: false,
                             search: true,
                             operators: {
                                 string: {
                                     eq: "Is equal to",
                                     neq: "Is not equal to",
                                     contains: "Contains"
                                 }
                             }
                         }
                     },
                    {
                        lockable: false,
                        width: "150px",
                        field: "ProposalDate", title: 'ProposalDate(dd/MM/yyyy)',
                        type: "date",
                        format: "{0:dd-MMM-yyyy}",
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    type: "date",
                                    format: "{0:dd-MMM-yyyy}",
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            },
                        }

                    },
                    {
                        lockable: false,
                        width: "150px",
                        field: "AgreementDate", title: 'AgreementDate(dd/MM/yyyy)',
                        type: "date",
                        format: "{0:dd-MMM-yyyy}",
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    type: "date",
                                    format: "{0:dd-MMM-yyyy}",
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            },
                        }
                    },
                    {
                        lockable: false,
                        width: "150px",
                        field: "EffectiveDate", title: 'EffectiveDate(dd/MM/yyyy)',
                        type: "date",
                        format: "{0:dd-MMM-yyyy}",
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    type: "date",
                                    format: "{0:dd-MMM-yyyy}",
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            },
                        },
                    },
                    {
                        lockable: false,
                        width: "150px",
                        field: "ReviewDate", title: 'ReviewDate(dd/MM/yyyy)',
                        type: "date",
                        format: "{0:dd-MMM-yyyy}",
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    type: "date",
                                    format: "{0:dd-MMM-yyyy}",
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            },
                        },
                    },
                    {
                        lockable: false,
                        width: "150px",
                        field: "ExpirationDate", title: 'ExpirationDate(dd/MM/yyyy)',
                        type: "date",
                        format: "{0:dd-MMM-yyyy}",
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    type: "date",
                                    format: "{0:dd-MMM-yyyy}",
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            },
                        },
                    },
                        {
                            lockable: false,
                            width: "120px",
                            field: "ProductItems", title: 'ProductItems',
                            attributes: {
                                style: 'white-space: nowrap;'

                            }, filterable: {
                                multi: true,
                                extra: false,
                                search: true,
                                operators: {
                                    string: {
                                        eq: "Is equal to",
                                        neq: "Is not equal to",
                                        contains: "Contains"
                                    }
                                }
                            }
                        },
                         {
                             lockable: false,
                             width: "120px",
                             field: "PaymentType", title: 'PaymentType(Payee/Receipt)',
                             attributes: {
                                 style: 'white-space: nowrap;'

                             }, filterable: {
                                 multi: true,
                                 extra: false,
                                 search: true,
                                 operators: {
                                     string: {
                                         eq: "Is equal to",
                                         neq: "Is not equal to",
                                         contains: "Contains"
                                     }
                                 }
                             }
                         },
                             {
                                 lockable: false,
                                 width: "120px",
                                 field: "PaymentFrequency", title: 'PaymentTerm',
                                 attributes: {
                                     style: 'white-space: nowrap;'
                                 }, filterable: {
                                     multi: true,
                                     extra: false,
                                     search: true,
                                     operators: {
                                         string: {
                                             eq: "Is equal to",
                                             neq: "Is not equal to",
                                             contains: "Contains"
                                         }
                                     }
                                 }
                             },
                     {
                         lockable: false,
                         width: "200px",
                         field: "Owner", title: 'Owner',
                         attributes: {
                             style: 'white-space: nowrap;'

                         }, filterable: {
                             multi: true,
                             extra: false,
                             search: true,
                             operators: {
                                 string: {
                                     eq: "Is equal to",
                                     neq: "Is not equal to",
                                     contains: "Contains"
                                 }
                             }
                         }
                     },
                      {
                          lockable: false,
                          width: "120px",
                          field: "ContractAmt", title: 'ContractAmt',
                          attributes: {
                              style: 'white-space: nowrap;'

                          }, filterable: {
                              multi: true,
                              extra: false,
                              search: true,
                              operators: {
                                  string: {
                                      eq: "Is equal to",
                                      neq: "Is not equal to",
                                      contains: "Contains"
                                  }
                              }
                          }
                      },
                          {
                              lockable: false,
                              width: "120px",
                              field: "NoticeTermNumber", title: 'NoticeTerm(Days)',
                              attributes: {
                                  style: 'white-space: nowrap;'

                              }, filterable: {
                                  multi: true,
                                  extra: false,
                                  search: true,
                                  operators: {
                                      string: {
                                          eq: "Is equal to",
                                          neq: "Is not equal to",
                                          contains: "Contains"
                                      }
                                  }
                              }
                          },
                              {
                                  lockable: false,
                                  width: "150px",
                                  field: "LockInPeriodDate", title: 'Lock-in-period',
                                  type: "date",
                                  format: "{0:dd-MMM-yyyy}",
                                  filterable: {
                                      multi: true,
                                      extra: false,
                                      search: true,
                                      operators: {
                                          string: {
                                              type: "date",
                                              format: "{0:dd-MMM-yyyy}",
                                              eq: "Is equal to",
                                              neq: "Is not equal to",
                                              contains: "Contains"
                                          }
                                      },
                                  },
                              },

                                {
                                    lockable: false,
                                    width: "120px",
                                    field: "Taxes", title: 'Taxes',
                                    attributes: {
                                        style: 'white-space: nowrap;'

                                    }, filterable: {
                                        multi: true,
                                        extra: false,
                                        search: true,
                                        operators: {
                                            string: {
                                                eq: "Is equal to",
                                                neq: "Is not equal to",
                                                contains: "Contains"
                                            }
                                        }
                                    }
                                },

                                      {
                                          lockable: false,
                                          width: "120px",
                                          field: "AddNewClause", title: 'Remark',
                                          attributes: {
                                              style: 'white-space: nowrap;'

                                          }, filterable: {
                                              multi: true,
                                              extra: false,
                                              search: true,
                                              operators: {
                                                  string: {
                                                      eq: "Is equal to",
                                                      neq: "Is not equal to",
                                                      contains: "Contains"
                                                  }
                                              }
                                          }
                                      },


                ]
            });

            $('#grid').kendoTooltip({
                filter: "td[role=gridcell]",  //what element
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                },
                beforeShow: function (e) {
                    var target = e.target;

                    var isTextBiggerThanElement = findOutSize(target);
                    if (!isTextBiggerThanElement) {
                        e.preventDefault();
                    }
                },
                position: 'down'
            }).data("kendoTooltip");

            $('#grid').kendoTooltip({
                filter: "th[role=columnheader]",  //what element
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                },
                beforeShow: function (e) {
                    var target = e.target;

                    var isTextBiggerThanElement = findOutSize(target);
                    if (!isTextBiggerThanElement) {
                        e.preventDefault();
                    }
                },
                position: 'down'
            }).data("kendoTooltip");

            $('#grid').kendoTooltip({
                filter: "td[colspan=7]",  //what element
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                },
                position: 'down'
            }).data("kendoTooltip");

            $('#grid').kendoTooltip({
                filter: "td[colspan=8]",  //what element
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                },
                position: 'down'
            }).data("kendoTooltip");

            $('#grid').kendoTooltip({
                filter: "td[colspan=9]",  //what element
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                },
                position: 'down'
            }).data("kendoTooltip");

            $('#grid').kendoTooltip({
                filter: "td[colspan=10]",  //what element
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                },
                position: 'down'
            }).data("kendoTooltip");

            $('#grid').kendoTooltip({
                filter: "td[colspan=11]",  //what element
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                },
                position: 'down'
            }).data("kendoTooltip");

            $('#grid').kendoTooltip({
                filter: "td[colspan=12]",  //what element
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                },
                position: 'down'
            }).data("kendoTooltip");

            $("#grid").data("kendoGrid").wrapper.find(".k-grid-header-wrap").off("scroll.kendoGrid");

        }






    </script>

</head>

<body>
    <form id="form1" runat="server">
        <div>
            <div class="row" style="padding-bottom: 8px;">
            </div>
            <div class="row" style="padding-top: 12px;">
            </div>
            <div id="grid" style="margin-left: 30px; margin-right: -9px; margin-top: 13px"></div>

        </div>
    </form>
</body>
</html>
