﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ContractProduct.Master" AutoEventWireup="true" CodeBehind="SharedContDocument.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.Common.SharedContDocument" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function fopenpopup() {
            $('#divContDocTypeDialog').modal('show');
        }
        function OpenDocSharePopup(FID, CID,ShareID) {
            $('#divDocumentsharePopup').modal('show');
            $('#ContentPlaceHolder1_Iframe_Docshare').attr('src', "/ContractProduct/aspxPages/ShareDocInfo.aspx?AccessID=" + FID + "&CID=" + CID + "&EditFileID=" + ShareID);
        }
        function CloseUploadShareDocumentPopup() {
            $('#divDocumentsharePopup').modal('hide');
            document.getElementById('<%= lnkBtn_RebindGrid.ClientID %>').click();
        }

        function RefreshParent() {
            window.location.href = window.location.href;
        }
    </script>
    <script type="text/javascript">
        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);
        window.onunload = function () { null };

        $(document).ready(function () {
            fhead('Shared Document Details');
        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row Dashboard-white-widget">
        <div class="dashboard">
            <div class="row">
                <asp:ValidationSummary ID="vsPageDocType" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                    ValidationGroup="PageDocTypeValidationGroup" />
                <asp:CustomValidator ID="cvPageDocType" runat="server" EnableClientScript="False"
                    ValidationGroup="PageDocTypeValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
            </div>

            <div class="row">
                <div class="col-md-12 colpadding0">
                    <div class="col-md-6 colpadding0">
                        <asp:TextBox runat="server" ID="tbxFilter" AutoComplete="off" CssClass="form-control" MaxLength="50"
                            PlaceHolder="Type Name to Search" AutoPostBack="true" OnTextChanged="tbxFilter_TextChanged" Width="70%" />
                    </div>
                    <div class="col-md-4 colpadding0">
                        <asp:LinkButton ID="lnkBtn_RebindGrid" OnClick="lnkBtn_RebindGrid_Click" Style="display: none;" runat="server"></asp:LinkButton>
                    </div>
                </div>
            </div>

            <div class="row">
                <asp:GridView runat="server" ID="grdContDoctType" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true" AllowSorting="true" GridLines="none" Width="100%"
                    PageSize="10" AllowPaging="true" AutoPostBack="true" CssClass="table" DataKeyNames="ID"
                    OnRowCreated="grdContDoctType_RowCreated" OnSorting="grdContDoctType_Sorting"
                    OnRowDataBound="grdContDoctType_RowDataBound"
                    OnRowCommand="grdContDoctType_RowCommand">
                    <Columns>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr.No." ItemStyle-Width="5%">
                            <ItemTemplate>
                                <%#Container.DataItemIndex+1 %>
                                <%--<asp:Label ID="lblRowID" runat="server" Text='<%# Eval("RowID") %>'></asp:Label>--%>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Vendor Name" ItemStyle-Width="15%">
                            <ItemTemplate>
                                <asp:Label ID="lblVendorName" data-toggle="tooltip" ToolTip='<%# Eval("VendorName") %>' runat="server" Text='<%# Eval("VendorName") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                          <asp:TemplateField HeaderText="Contract No" ItemStyle-Width="15%">
                            <ItemTemplate>
                                <asp:Label ID="lblContractNo" data-toggle="tooltip" ToolTip='<%# Eval("ContractNo") %>' runat="server" Text='<%# Eval("ContractNo") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Contract Title" ItemStyle-Width="15%">
                            <ItemTemplate>
                                <asp:Label ID="lblContractTitle" data-toggle="tooltip" ToolTip='<%# Eval("ContractTitle") %>' runat="server" Text='<%# Eval("ContractTitle") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Period" ItemStyle-Width="15%">
                            <ItemTemplate>
                                <asp:Label ID="lblPeriodDetail" data-toggle="tooltip" ToolTip='<%# Eval("PeriodDetail") %>' runat="server" Text='<%# Eval("PeriodDetail") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="File Name" ItemStyle-Width="15%">
                            <ItemTemplate>
                                <asp:Label ID="lblFileName" data-toggle="tooltip" ToolTip='<%# Eval("FileName") %>' runat="server" Text='<%# Eval("FileName") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Start Date" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="10%">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                    <asp:Label ID="lblStartDate" runat="server" Text='<%# Eval("StartDate") != DBNull.Value ? Convert.ToDateTime(Eval("StartDate")).ToString("dd-MM-yyyy") : "" %>'
                                        data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("StartDate") != DBNull.Value ? Convert.ToDateTime(Eval("StartDate")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="End Date" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="10%">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                    <asp:Label ID="lblEndDate" runat="server" Text='<%# Eval("EndDate") != null ? Convert.ToDateTime(Eval("EndDate")).ToString("dd-MM-yyyy") : "" %>'
                                        data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("EndDate") != null ? Convert.ToDateTime(Eval("EndDate")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="10%" ItemStyle-CssClass="text-right" HeaderStyle-CssClass="text-right">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkEditContractDocType" runat="server" CommandName="EDIT_ContDocShare" ToolTip="Edit" data-toggle="tooltip"
                                    CommandArgument='<%# Eval("ID") %>'>
                                     <img src='<%# ResolveUrl("/Images/edit_icon_new.png")%>' alt="Edit" />
                                </asp:LinkButton>
                                <asp:LinkButton ID="lnkDeleteContractDocType" runat="server" CommandName="DELETE_ContDocShare" ToolTip="Unshare" data-toggle="tooltip"
                                    CommandArgument='<%# Eval("ID") %>' OnClientClick="return confirm('Are you certain you want to Unshare this Document?');">
                            <img src='<%# ResolveUrl("/Images/delete_icon_new.png")%>' alt="Delete" />
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="clsROWgrid" />
                    <HeaderStyle CssClass="clsheadergrid" />
                    <PagerTemplate>
                        <table style="display: none">
                            <tr>
                                <td>
                                    <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                </td>
                            </tr>
                        </table>
                    </PagerTemplate>
                    <EmptyDataTemplate>
                        No Record Found
                    </EmptyDataTemplate>
                </asp:GridView>
            </div>

            <div class="row">
                <div class="col-md-12 colpadding0">
                    <div class="col-md-10 colpadding0">
                        <div runat="server" id="DivRecordsScrum" style="float: left; margin-top: 5px; color: #999">
                            <p style="padding-right: 0px !Important;">
                                <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>- 
                                <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                                <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                            </p>
                        </div>
                    </div>
                    <div class="col-md-1 colpadding0">
                        <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control" Style="width: 100%; float: right; margin-right: 6%;"
                            AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                            <asp:ListItem Text="5" />
                            <asp:ListItem Text="10" Selected="True" />
                            <asp:ListItem Text="20" />
                            <asp:ListItem Text="50" />
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-1 colpadding0" style="float: right;">
                        <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true" DataPlaceHolder="No" AllowSingleDeselect="false"
                            OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged" class="form-control" Width="100%" Height="30px">
                        </asp:DropDownListChosen>
                    </div>
                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                </div>
            </div>
        </div>
    </div>
       <div class="modal fade" id="divDocumentsharePopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog p5" style="width: 82%;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label class="modal-header-custom">
                                Add Sharing Details</label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                            <iframe src="about:blank" id="Iframe_Docshare" frameborder="0" runat="server" width="100%" height="600px"></iframe>
                        </div>
                    </div>
                </div>
            </div>
   <%-- <div class="modal fade" id="divDocumentsharePopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 82%;">
            <div class="modal-content">
                <div class="modal-header">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label class="modal-header-custom">
                                Add Sharing Details</label>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="RefreshParent()">&times;</button>
                </div>
                <div class="modal-body" style="width: 100%;">
                    <iframe src="about:blank" id="Iframe_Docshare" runat="server" frameborder="0" width="100%" height="600px"></iframe>
                </div>
            </div>
        </div>
    </div>--%>
</asp:Content>

