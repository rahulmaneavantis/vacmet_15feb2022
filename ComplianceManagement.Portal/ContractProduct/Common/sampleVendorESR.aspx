﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="sampleVendorESR.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.Common.sampleVendorESR" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<!DOCTYPE html>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head runat="server">
    <title>Telerik ASP.NET Example</title>

      <link href="../../NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!--external css-->
    <link href="../../NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="../../NewCSS/contract_custom_style.css" rel="stylesheet" />
    <link href="../../NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>
    <link href="../../NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap theme -->
    <link href="../../NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <link href="../../NewCSS/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
    <script src="../../Newjs/bootstrap-multiselect.js" type="text/javascript"></script>
    <script src="../../Newjs/bootstrap-tagsinput.js"></script>
    <link href="../../NewCSS/bootstrap-tagsinput.css" rel="stylesheet" />
    <link href="../../NewCSS/timeline.css" rel="stylesheet" />
    <!-- nice scroll -->
    <script type="text/javascript" src="../../Newjs/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery.nicescroll.js"></script>
      <link href="~/NewCSS/contract_custom_style.css" rel="stylesheet" />
    <link href="../../Style/css/StyleSheetTextEditor.css" rel="stylesheet" />

    <style>
        /* custom save tools icons */
        /*Active state*/
        div.RadEditor .reToolBarWrapper .RadRibbonBar .reSaveAsDoc:before,
        div.RadEditor .reToolBarWrapper .RadRibbonBar .reSaveAsDocx:before,
        div.RadEditor .reToolBarWrapper .RadRibbonBar .reSaveAsRtf:before,
        div.RadEditor .reToolBarWrapper .RadRibbonBar .reSaveAsMarkdown:before,
        div.RadEditor .reToolBarWrapper .RadRibbonBar .reSaveAs:before,
        div.RadEditor .reToolBarWrapper .RadRibbonBar .reSaveAsPDF:before {
            font-family: TelerikWebUI;
            content: "\e0BD";
        }

        /*Hovered state*/
        div.RadEditor .reToolBarWrapper .RadRibbonBar .reSaveAsDoc:hover:before,
        div.RadEditor .reToolBarWrapper .RadRibbonBar .reSaveAsDocx:hover:before,
        div.RadEditor .reToolBarWrapper .RadRibbonBar .reSaveAsRtf:hover:before,
        div.RadEditor .reToolBarWrapper .RadRibbonBar .reSaveAsMarkdown:hover:before,
        div.RadEditor .reToolBarWrapper .RadRibbonBar .reSaveAs:hover:before,
        div.RadEditor .reToolBarWrapper .RadRibbonBar .reSSaveAsPDF:hover:before {
            font-family: TelerikWebUI;
            content: "\e0BD";
        }

        /*Selected state*/
        div.RadEditor .reToolBarWrapper .RadRibbonBar .reSaveAsDoc.reToolSelected:before,
        div.RadEditor .reToolBarWrapper .RadRibbonBar .reSaveAsDocx.reToolSelected:before,
        div.RadEditor .reToolBarWrapper .RadRibbonBar .reSaveAsMarkdown.reToolSelected:before,
        div.RadEditor .reToolBarWrapper .RadRibbonBar .reSaveAs.reToolSelected:before,
        div.RadEditor .reToolBarWrapper .RadRibbonBar .reSaveAsPDF.reToolSelected:before {
            font-family: TelerikWebUI;
            content: "\e0BD";
        }


        /* custom open tool icon */
        /*Active state*/
        div.RadEditor .reToolBarWrapper .RadRibbonBar .reOpen:before {
            font-family: TelerikWebUI;
            content: "\e0A2";
        }

        /*Hovered state*/
        div.RadEditor .reToolBarWrapper .RadRibbonBar .reOpen:hover:before {
            font-family: TelerikWebUI;
            content: "\e0A2";
        }

        /*Selected state*/
        div.RadEditor .reToolBarWrapper .RadRibbonBar .reOpen.reToolSelected:before {
            font-family: TelerikWebUI;
            content: "\e0A2";
        }


        /* demo container */
        .demo-container.no-bg {
            margin: 0 auto;
            width: 900px;
        }
    </style>
   
</head>
<body style="background-color: #fff;">
    <form id="form1" runat="server" style="background-color: #fff">
    <telerik:RadScriptManager runat="server" ID="RadScriptManager1" />
    <%--<telerik:RadSkinManager ID="RadSkinManager1" runat="server" ShowChooser="true" />--%>
    <script type="text/javascript" src="scripts.js"></script>
    <script type="text/javascript">

        function ColsedSaveuploadModal()
        {
            alert("Document Upload ");        
            //$("#upload-loader").css("visibility", "hidden");
            window.parent.CloseSectionModal();
        }

        function ColsedSaveuploadModal1() {
            alert("Please Upload Document.");
            $("#upload-loader").css("visibility", "hidden");
        }

        function loaderadd1()
        {
            debugger;
            $("#upload-loader").css("visibility", "visible");
        }

        TelerikDemo.rwUploadId = "<%=RadWindow1.ClientID%>";
        TelerikDemo.rnMessagesId = "<%=RadNotification1.ClientID%>";
        TelerikDemo.reId = "<%=RadEditor1.ClientID%>";

       
    </script>
        <div class="col-md-1 col-xs-1">
            <img id="upload-loader" style="visibility: hidden; height: 50px; width: 50px" src="../../assets/loader.gif" />
        </div>
        <div class="row" style="padding-top:6px;">
         <asp:Button ID="btnSubmit" style="float: right;margin-right: 0.3%;" runat="server" Text="Save" CssClass="btn btn-primary" OnClientClick="loaderadd1()" OnClick="btnSubmit_Click" />                                                 
            </div>
    <div class="demo-container no-bg" style="padding-top: 5px;">
        <telerik:RadEditor RenderMode="Lightweight" runat="server" ID="RadEditor1" ToolbarMode="RibbonBar" Width="1326px" Height="600px" style="margin-left: -213px;"
            EnableTrackChanges="true" EnableComments="true" ToolsFile="word-like-tools.xml"
            ContentFilters="DefaultFilters, PdfExportFilter" SkinID="WordLikeExperience"
            OnClientLoad="TelerikDemo.toggleTrackChanges" EditModes="Design, Preview">
            <TrackChangesSettings CanAcceptTrackChanges="true" Author="John Smith" />
            <ExportSettings OpenInNewWindow="true" FileName="RadEditor-Export"></ExportSettings>
            <ImageManager
                ViewPaths="~/Editor/images/UserDir/Marketing,~/Editor/images/UserDir/PublicRelations"
                UploadPaths="~/Editor/images/UserDir/Marketing,~/Editor/images/UserDir/PublicRelations"
                DeletePaths="~/Editor/images/UserDir/Marketing,~/Editor/images/UserDir/PublicRelations" />
        </telerik:RadEditor>
    </div>
    <telerik:RadWindow RenderMode="Lightweight" ID="RadWindow1" runat="server" Title="Upload File" Behaviors="Close, Move" VisibleStatusbar="false" AutoSize="true">
        <ContentTemplate>
            <div style="width: 500px">               
                Upload a DOCX file to load its content for editing.<br />
                <%--Upload a DOCX, DOC or RTF file to load its content for editing. You can also use plain text (.txt) or HTML (.htm, .html) files and content.<br />--%>
                <telerik:RadAsyncUpload RenderMode="Lightweight" runat="server" ID="RadAsyncUpload1" AllowedFileExtensions=".doc,docx,rtf,txt,htm,html,md" MaxFileInputsCount="1"
                                        OnClientValidationFailed="TelerikDemo.OnClientValidationFailed" OnClientFileUploaded="TelerikDemo.uploadFile"
                                        OnFileUploaded="RadAsyncUpload1_FileUploaded"
                    Width="100%">
                </telerik:RadAsyncUpload>
                 <img id="upload-loader1" style="visibility: hidden; height: 50px; width: 50px" src="../../assets/loader.gif" />
            </div>
        </ContentTemplate>
    </telerik:RadWindow>
    <telerik:RadNotification RenderMode="Lightweight" ID="RadNotification1" runat="server" Width="350px" Height="150px" Title="An error occured" TitleIcon="warning"
                            ContentIcon="info" Position="Center" AutoCloseDelay="5" EnableRoundedCorners="true" EnableShadow="true">
    </telerik:RadNotification>
    </form>
</body>
     <script>
        (function (global, undefined) {
         
            debugger;
            global.TelerikDemo = {
                rwUploadId: null,
                rnMessagesId: null,
                reId: null,
                uploadFile: function () {
                    //alert(1);
                    debugger;
                    $("#upload-loader1").css("visibility", "visible");
                    //invoke a postback so the file actually uploads so it can be handled, without the user having to click an additional Upload button
                    __doPostBack("", "");                  
                },
                openUploadDialog: function () {
                    //alert(12);
                       TelerikDemo.rwUploadId = "<%=RadWindow1.ClientID%>";
                    $find(TelerikDemo.rwUploadId).show();                   
                },
                OnClientValidationFailed: function (sender, args) {
                    //alert(123);
                    var errMessage = "The selected file is invalid. Please upload an MS Word document with an extension .doc, .docx or .rtf, or a .txt/.html/.htm file with HTML content!";
                    var notification = $find(TelerikDemo.rnMessagesId);
                    notification.set_text(errMessage);
                    notification.show();

                    //clear the invalid file so the user can try again
                    sender.deleteAllFileInputs();
                },
                setMarkdownContent: function () {
                    //alert(1234);
                    //this function gets called from the server because this is where the markdown file is,
                    //but the Markdown import is client-side functionality in RadEditor so the content has to be processed
                    //so, to ensure the scripts execute when the controls are available, the Sys.Application.Load event is used
                    Sys.Application.add_load(function () {
                        try {
                            var converter = new Telerik.Web.UI.Editor.Markdown.Converter();
                            var editor = $find(TelerikDemo.reId);
                            var content = editor.get_text();
                            editor.set_html(converter.makeHtml(content));
                        }
                        catch (ex) {
                            editor.set_html("");
                            var notification = $find(TelerikDemo.rnMessagesId);
                            var errMessage = "There was an error during the import operation. Try simplifying the content.";
                            notification.set_text(errMessage);
                            notification.show();
                        }
                    });
                },
                toggleTrackChanges: function (sender, args) {
                    //alert(12345);
                    //$("#upload-loader").css("visibility", "hidden");
                    //disable track changes for initial typing, but let the user enable them if they want to
                    if (sender.get_enableTrackChanges()) {
                        sender.fire("EnableTrackChangesOverride");
                    }
                }
            };
          
        })(window);

        Telerik.Web.UI.Editor.CommandList["SaveAsDoc"] =
        Telerik.Web.UI.Editor.CommandList["SaveAsDocx"] =
        Telerik.Web.UI.Editor.CommandList["SaveAsRtf"] =
        Telerik.Web.UI.Editor.CommandList["SaveAsMarkdown"] =
        Telerik.Web.UI.Editor.CommandList["SaveAsPDF"] = function (commandName, editor, args) {
            __doPostBack("", commandName);
        };
        Telerik.Web.UI.Editor.CommandList["Open"] = function (commandName, editor, args)
        {
            TelerikDemo.openUploadDialog();
        };

    </script>
</html>