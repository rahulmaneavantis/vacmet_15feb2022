﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Telerik.Web.UI;
using Telerik.Web.UI.Editor;
using Telerik.Web.UI.Editor.Export;
using Telerik.Windows.Documents.Flow.FormatProviders.Docx;
using Telerik.Windows.Documents.Flow.Model;
using Telerik.Windows.Documents.Flow.Model.Styles;


namespace com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.Common
{
    public partial class TemplateTransactionDetail : System.Web.UI.Page
    {
        protected static int TemplateId;
        protected static int CustId;
        protected static int UId;
        protected static int RoleId;
        protected static int SectionId;
        //protected static string User;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                int UserID = Convert.ToInt32(Request.QueryString["UID"]);
                int RoleID = Convert.ToInt32(Request.QueryString["RID"]);
                int TemplateID = Convert.ToInt32(Request.QueryString["TID"]);
                SetDefaultConfiguratorValues(UserID, RoleID, TemplateID);
                BindTransactionHistory(TemplateID);
                //BindControls(TemplateID);                
            }
        }
        public void BindControls(long TemplateInstanceID)
        {
            BtnPublish.Visible = false;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                int UserID = -1;
                UserID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);

                var entitiesData = (from row in entities.Cont_tbl_TemplateMaster
                                    where row.ID == TemplateInstanceID
                                    && row.IsDeleted == false
                                    && row.CreatedBy == UserID
                                    && row.CustomerID == customerID
                                    select row).FirstOrDefault();
                if (entitiesData!=null)
                {
                    BtnPublish.Visible = true;
                }
            }
        }

        public int getRoleID(int TemplateId, int UserID,int sectionID)
        {
            int RoleID = -1;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                TemplateAssignment data = (from row in entities.TemplateAssignments
                                           where row.TemplateInstanceID == TemplateId
                                           && row.UserID == UserID
                                           && row.SectionID == sectionID
                                           select row).FirstOrDefault();
                if (data != null)
                {
                    RoleID = Convert.ToInt32(data.RoleID);
                }
            }
            return RoleID;
        }
        protected void rptComplianceVersionView_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                if (e.CommandName.Equals("View"))
                {
                    SectionId = Convert.ToInt32(commandArgs[0]);
                    int StatusId = Convert.ToInt32(commandArgs[1]);
                    RoleId = getRoleID(Convert.ToInt32(Request.QueryString["TID"]), AuthenticationHelper.UserID, SectionId);                 
                    TemplateId = Convert.ToInt32(Request.QueryString["TID"]);
                    UId = Convert.ToInt32(AuthenticationHelper.UserID);
                    CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    //SectionId = 1;
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        int TemplateID = Convert.ToInt32(Request.QueryString["TID"]);

                        var entitiesData = (from row in entities.TemplateAssignments
                                            where row.TemplateInstanceID == TemplateID
                                            && row.UserID == UId
                                            && row.IsActive == true
                                            && row.SectionID == SectionId
                                            select row).FirstOrDefault();
                        if (entitiesData != null)
                        {
                            int RID = Convert.ToInt32(entitiesData.RoleID);
                            if (RID == 3)
                            {
                                if (StatusId == 1 || StatusId == 3 || StatusId == 5 || StatusId == 14 || StatusId == 15)
                                {
                                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenSectionModal();", true);
                                }
                            }
                            if (RID == 4)
                            {
                                if (StatusId == 2)
                                {
                                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenSectionModal();", true);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void BindTransactionHistory(long TemplateInstanceID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int RoleID = Convert.ToInt32(Request.QueryString["RID"]);
                    BtnPublish.Visible = false;
                    int customerID = -1;
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                    var entitiesData = (from row in entities.Cont_SP_GetSectionsDetail(customerID, Convert.ToInt32(TemplateInstanceID))
                                        select row).ToList();
                    if (entitiesData.Count > 0)
                    {
                        int ClosedCount = entitiesData.Where(x => x.SectionStatusID == "5").ToList().Count;
                        if (ClosedCount == entitiesData.Count)
                        {
                            if (RoleID == 3)
                            {
                                BtnPublish.Visible = true;
                            }
                        }
                    }
                    rptComplianceVersionView.DataSource = entitiesData.OrderBy(entry => entry.SectionOrder).ToList();
                    rptComplianceVersionView.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        
        public class getTemplateTransactionDetail
        {
            public long TemplateTransactionID { get; set; }
            public long TemplateInstanceID { get; set; }
            public long ComplianceStatusID { get; set; }
            public System.DateTime CreatedOn { get; set; }
            public string Status { get; set; }
            public Nullable<System.DateTime> StatusChangedOn { get; set; }
            public string TemplateContent { get; set; }
            public long RoleID { get; set; }
            public long UserID { get; set; }
            public int sectionID { get; set; }
            public int orderID { get; set; }
            public int visibilty { get; set; }
            public string sectionHeader { get; set; }
        }

        private void SetDefaultConfiguratorValues(int UserID,int RoleID,int TemplateID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    string UserName = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User;

                    var data1 = (from row in entities.Sp_GetTemplateTransaction()
                                 where row.UserID == UserID
                                 && row.TemplateInstanceID == TemplateID
                                 && row.RoleID == RoleID
                                 select row).ToList();

                    if (data1.Count > 0)
                    {
                        var data = (from row in data1
                                    join row1 in entities.Cont_tbl_TemplateSectionMapping
                                    on row.sectionID equals row1.SectionID
                                    where row1.TemplateID == TemplateID
                                    select new getTemplateTransactionDetail
                                    {
                                        TemplateTransactionID = row.TemplateTransactionID,
                                        TemplateInstanceID = row.TemplateInstanceID,
                                        ComplianceStatusID = row.ComplianceStatusID,
                                        CreatedOn = row.CreatedOn,
                                        Status = row.Status,
                                        StatusChangedOn = row.StatusChangedOn,
                                        TemplateContent = row.TemplateContent,
                                        RoleID = row.RoleID,
                                        UserID = row.UserID,
                                        sectionID = row.sectionID,
                                        orderID = Convert.ToInt32(row1.SectionOrder),
                                        visibilty = Convert.ToInt32(row1.Headervisibilty),
                                        sectionHeader = row.SectionHeader
                                    }).OrderBy(x => x.orderID).ToList();

                        //theEditor.TrackChangesSettings.CanAcceptTrackChanges = true;
                        //theEditor.EnableTrackChanges = true;

                        System.Text.StringBuilder strExporttoWord = new System.Text.StringBuilder();

                        data.ForEach(eachSection =>
                        {
                            if (eachSection.visibilty == 1)
                            {
                                strExporttoWord.Append(@"<div>");
                                strExporttoWord.Append(@"<p><B>" + eachSection.sectionHeader + "</B></p>");
                                strExporttoWord.Append(@"</div>");
                            }

                            strExporttoWord.Append(@"<div>");
                            strExporttoWord.Append(@"<p>" + eachSection.TemplateContent + "</p>");
                            strExporttoWord.Append(@"</div>");
                        });

                        theEditor.Content = strExporttoWord.ToString();
                        theEditor.TrackChangesSettings.Author = UserName;
                        lblUser.Text = UserName;
                        //theEditor.TrackChangesSettings.Author = "Performer";                            
                        //lblUser.Text = "Performer";
                        theEditor.TrackChangesSettings.UserCssId = "reU9";
                        theEditor.TrackChangesSettings.CanAcceptTrackChanges = false;
                        theEditor.EnableTrackChanges = false;
                    }
                    else
                    {
                        theEditor.TrackChangesSettings.Author = UserName;
                        lblUser.Text = UserName;
                        theEditor.TrackChangesSettings.UserCssId = "reU9";
                        theEditor.TrackChangesSettings.CanAcceptTrackChanges = false;
                        theEditor.EnableTrackChanges = false;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        
        protected void RadEditor1_ExportContent(object sender, EditorExportingArgs e)
        {
            ExportType exportType = e.ExportType;

            if (exportType == ExportType.Word)
            {
                string exportedOutput = e.ExportOutput;

                Byte[] output = Encoding.Default.GetBytes(exportedOutput);

                DocxFormatProvider docxProvider = new DocxFormatProvider();
                RadFlowDocument document = docxProvider.Import(output);

                Header defaultHeader = document.Sections.First().Headers.Add();
                Paragraph defaultHeaderParagraph = defaultHeader.Blocks.AddParagraph();
                defaultHeaderParagraph.TextAlignment = Alignment.Right;
                defaultHeaderParagraph.Inlines.AddRun("This is a sample header.");

                Footer defaultFooter = document.Sections.First().Footers.Add();
                Paragraph defaultFooterParagraph = defaultFooter.Blocks.AddParagraph();
                defaultFooterParagraph.TextAlignment = Alignment.Right;
                defaultFooterParagraph.Inlines.AddRun("This is a sample footer.");

                Byte[] modifiedOutput = docxProvider.Export(document);
                string finalOutput = Encoding.Default.GetString(modifiedOutput, 0, modifiedOutput.Length);

                e.ExportOutput = finalOutput;
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            theEditor.ExportToDocx();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            divHtml.InnerHtml = theEditor.Content;
        }

        public static TemplateTransaction GetDetail(int TID, int RoleID, int UserID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.TemplateTransactions
                            where row.Id == TID
                            select row).OrderByDescending(x => x.CreatedOn).FirstOrDefault();

                return data;
            }
        }

        protected void BtnPublish_Click(object sender, EventArgs e)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int UserID = Convert.ToInt32(Request.QueryString["UID"]);
                    int RoleID = Convert.ToInt32(Request.QueryString["RID"]);
                    int TemplateID = Convert.ToInt32(Request.QueryString["TID"]);

                    string UserName = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User;

                    var data1 = (from row in entities.Sp_GetTemplateTransaction()
                                 where row.UserID == UserID
                                 && row.TemplateInstanceID == TemplateID
                                 && row.RoleID == RoleID
                                 select row).ToList();

                    if (data1.Count > 0)
                    {
                        var data = (from row in data1
                                    join row1 in entities.Cont_tbl_TemplateSectionMapping
                                    on row.sectionID equals row1.SectionID
                                    where row1.TemplateID == TemplateID
                                    select new getTemplateTransactionDetail
                                    {
                                        TemplateTransactionID = row.TemplateTransactionID,
                                        TemplateInstanceID = row.TemplateInstanceID,
                                        ComplianceStatusID = row.ComplianceStatusID,
                                        CreatedOn = row.CreatedOn,
                                        Status = row.Status,
                                        StatusChangedOn = row.StatusChangedOn,
                                        TemplateContent = row.TemplateContent,
                                        RoleID = row.RoleID,
                                        UserID = row.UserID,
                                        sectionID = row.sectionID,
                                        orderID = Convert.ToInt32(row1.SectionOrder)
                                    }).OrderBy(x => x.orderID).ToList();

                        System.Text.StringBuilder strExporttoWord = new System.Text.StringBuilder();

                        data.ForEach(eachSection =>
                        {
                            strExporttoWord.Append(@"<div>");
                            strExporttoWord.Append(@"<p>" + eachSection.TemplateContent + "</p>");
                            strExporttoWord.Append(@"</div>");
                        });

                        int customerID = -1;
                        customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                        var entitiesData = (from row in entities.Cont_tbl_TemplateStatusMapping
                                            where row.TemplateID == TemplateID
                                            && row.CustomerID == customerID
                                            select row).FirstOrDefault();
                        if (entitiesData != null)
                        {
                            entitiesData.UpdatedBy = UserID;
                            entitiesData.UpdatedOn = DateTime.Now;
                            entitiesData.IsActive = true;
                            entitiesData.TemplateContent = strExporttoWord.ToString();
                            entitiesData.Status = true;
                            entities.SaveChanges();
                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "closedTemplate();", true);
                        }
                        else
                        {
                            Cont_tbl_TemplateStatusMapping obj1 = new Cont_tbl_TemplateStatusMapping();
                            obj1.TemplateID = Convert.ToInt32(TemplateID);
                            obj1.CustomerID = Convert.ToInt32(customerID);
                            obj1.TemplateContent = strExporttoWord.ToString();
                            obj1.CreatedOn = DateTime.Now;
                            obj1.CreatedBy = AuthenticationHelper.UserID;
                            obj1.Status = true;
                            obj1.IsActive = true;
                            entities.Cont_tbl_TemplateStatusMapping.Add(obj1);
                            entities.SaveChanges();
                        }
                    }
                }
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "closedpublish();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public bool ExporttoWordNew(int userID, int CustomerID, int TemplateID,int RoleID)
        {
            bool resultdata = false;

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data1 = (from row in entities.Sp_GetTemplateTransaction()
                             where row.UserID == userID
                             && row.TemplateInstanceID == TemplateID
                             && row.RoleID == RoleID
                             select row).ToList();

                //    System.Text.StringBuilder strExporttoWord1 = new System.Text.StringBuilder();

                var data = (from row in data1
                            join row1 in entities.Cont_tbl_TemplateSectionMapping
                            on row.sectionID equals row1.SectionID
                            where row1.TemplateID == TemplateID
                            select new getTemplateTransactionDetail
                            {
                                TemplateTransactionID = row.TemplateTransactionID,
                                TemplateInstanceID = row.TemplateInstanceID,
                                ComplianceStatusID = row.ComplianceStatusID,
                                CreatedOn = row.CreatedOn,
                                Status = row.Status,
                                StatusChangedOn = row.StatusChangedOn,
                                TemplateContent = row.TemplateContent,
                                RoleID = row.RoleID,
                                UserID = row.UserID,
                                sectionID = row.sectionID,
                                orderID = Convert.ToInt32(row1.SectionOrder),
                                visibilty = Convert.ToInt32(row1.Headervisibilty),
                                sectionHeader = row.SectionHeader
                            }).OrderBy(x => x.orderID).ToList();

                var lstSections = (from row in entities.Cont_SP_GetTemplates_Excel(CustomerID, userID, TemplateID)
                                   select row).FirstOrDefault();
                if (lstSections != null)
                {
                    resultdata = true;
                    System.Text.StringBuilder strExporttoWord = new System.Text.StringBuilder();

                    string fileName = lstSections.TemplateName + "-" + lstSections.Version;
                    string templateName = lstSections.TemplateName;

                    strExporttoWord.Append(@"
                <html 
                xmlns:o='urn:schemas-microsoft-com:office:office' 
                xmlns:w='urn:schemas-microsoft-com:office:word'
                xmlns='http://www.w3.org/TR/REC-html40'>
                <head><title></title>

                <!--[if gte mso 9]>
                <xml>
                <w:WordDocument>
                <w:View>Print</w:View>
                <w:Zoom>90</w:Zoom>
                <w:DoNotOptimizeForBrowser/>
                </w:WordDocument>
                </xml>
                <![endif]-->

                <style>
                p.MsoFooter, li.MsoFooter, div.MsoFooter
                {
                margin:0in;
                margin-bottom:.0001pt;
                mso-pagination:widow-orphan;
                tab-stops:center 3.0in right 6.0in;
                font-size:12.0pt;
                }
                <style>

                <!-- /* Style Definitions */

                @page Section1
                {
                size:8.5in 11.0in; 
                margin:1.0in 1.25in 1.0in 1.25in ;
                mso-header-margin:.5in;
                mso-header: h1;
                mso-footer: f1; 
                mso-footer-margin:.5in;
                 font-family:Arial;
                }

                div.Section1
                {
                page:Section1;
                }
                table#hrdftrtbl
                {
                    margin:0in 0in 0in 9in;
                }

                .break { page-break-before: always; }
                -->
                </style></head>");

                    strExporttoWord.Append(@"<body lang=EN-US style='tab-interval:.5in'> <div class=Section1>");
                    data.ForEach(eachSection =>
                    {
                        if (eachSection.visibilty == 1)
                        {
                            strExporttoWord.Append(@"<div>");
                            strExporttoWord.Append(@"<p><B>" + eachSection.sectionHeader + "</B></p>");
                            strExporttoWord.Append(@"</div>");
                        }

                        strExporttoWord.Append(@"<div>");
                        strExporttoWord.Append(@"<p>" + eachSection.TemplateContent + "</p>");
                        strExporttoWord.Append(@"</div>");
                    });

                    strExporttoWord.Append(@"<table id='hrdftrtbl' border='1' cellspacing='0' cellpadding='0'>
                <tr>
                    <td>
                        <div style='mso-element:header' id=h1 >
                            <p class=MsoHeader style='text-align:left'>
                        </div>
                    </td>
                    <td>
                        <div style='mso-element:footer' id=f1>
                            <p class=MsoFooter>
                            <span style=mso-tab-count:2'></span>
                            <span style='mso-field-code:"" PAGE ""'></span>
                            of <span style='mso-field-code:"" NUMPAGES ""'></span>
                            </p>
                        </div>
                    </td>
                </tr>
                </table>
                </body>
                </html>");

                    fileName = fileName + "-" + DateTime.Now.ToString("ddMMyyyy");

                    Response.AppendHeader("Content-Type", "application/msword");
                    Response.AppendHeader("Content-disposition", "attachment; filename=" + fileName + ".doc");
                    Response.Write(strExporttoWord);
                }
            }
            return resultdata;
        }
        
        protected void BtnExportDocument_Click(object sender, EventArgs e)
        {
            int UserID = Convert.ToInt32(Request.QueryString["UID"]);
            int RoleID = Convert.ToInt32(Request.QueryString["RID"]);
            int templateID = Convert.ToInt32(Request.QueryString["TID"]);

            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            if (templateID != 0)
            {
                bool result = ExporttoWordNew(UserID, customerID, Convert.ToInt32(templateID), RoleID);
                if (!result)
                {

                }
            }
        }

        protected void btnsectionbind_Click(object sender, EventArgs e)
        {
            int UserID = Convert.ToInt32(Request.QueryString["UID"]);
            int RoleID = Convert.ToInt32(Request.QueryString["RID"]);
            int TemplateID = Convert.ToInt32(Request.QueryString["TID"]);
            SetDefaultConfiguratorValues(UserID, RoleID, TemplateID);
            BindTransactionHistory(TemplateID);
        }
    }
}