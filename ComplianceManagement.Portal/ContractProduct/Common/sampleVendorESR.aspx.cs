﻿using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataContract;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.Common
{
    public partial class sampleVendorESR : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //select the Home tab by default
            RadEditor1.RibbonBar.SelectedTabIndex = 1;
            //handle postbacks from the export commands
            try
            {
                string evtArg = Request["__EVENTARGUMENT"];
                switch (evtArg)
                {
                    case "SaveAsDocx":
                        RadEditor1.ExportToDocx();
                        break;
                    case "SaveAsRtf":
                        RadEditor1.ExportToRtf();
                        break;
                    case "SaveAsPDF":
                        RadEditor1.ExportToPdf();
                        break;
                    case "SaveAsMarkdown":
                        RadEditor1.ExportToMarkdown();
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                RadNotification1.Show("There was an error during the export operation. Try simplifying the content and removing images/lists.");
            }
        }

        protected void RadAsyncUpload1_FileUploaded(object sender, FileUploadedEventArgs e)
        {
            //the maximum allowed file inputs is one, so there should be no multiple files uploaded

            //see what is the uploaded file extension and attempt to import it accordingly
            try
            {
                //string path = "path to your Word document";
                //string path = "C://Users//amol.patil//Desktop//3 sept 2021//ATL testing-1.0-16062021.doc";
                //using (var fs = File.OpenRead(path))
                //{
                //    var ms = new MemoryStream();
                //    ms.SetLength(fs.Length);
                //    fs.Read(ms.GetBuffer(), 0, (int)fs.Length);
                //    RadEditor1.LoadDocxContent(ms);
                //}

                string fileExt = e.File.GetExtension();
                switch (fileExt)
                {
                    case ".doc":
                        RadEditor1.LoadDocxContent(e.File.InputStream);
                        break;
                    case ".docx":
                        RadEditor1.LoadDocxContent(e.File.InputStream);
                        break;
                    case ".rtf":
                        RadEditor1.LoadRtfContent(e.File.InputStream);
                        break;
                    case ".txt":
                    case ".html":
                    case ".htm":
                        using (StreamReader sr = new StreamReader(e.File.InputStream))
                        {
                            RadEditor1.Content = sr.ReadToEnd();
                        }
                        break;
                    case ".md":
                        using (StreamReader sr = new StreamReader(e.File.InputStream))
                        {
                            RadEditor1.Content = sr.ReadToEnd();
                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "importMarkdownScript", "TelerikDemo.setMarkdownContent();", true);
                        }
                        break;
                    default:
                        RadNotification1.Show("The selected file is invalid. Please upload an MS Word document with an extension .doc, .docx or .rtf, or a .txt/.html file with HTML content!");
                        break;
                }
            }
            catch (Exception ex)
            {
                RadNotification1.Show("There was an error during the import operation. Try simplifying the content.");
            }
        }
        public static string getVendorName(long UserID, long CustomerID, long RoleID, int RolegetVIRID, int RolegetVRID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                string VendorName = string.Empty;
                if (RoleID == RolegetVRID)
                {
                    Cont_tbl_VendorMaster vendorObj = VendorDetails.GetVendorMasterDetails(UserID, CustomerID);
                    if (vendorObj != null)
                    {
                        VendorName = vendorObj.VendorName;
                    }
                }
                else if (RoleID == RolegetVIRID)
                {
                    Cont_tbl_SubVendorMaster vendorObj = (from row in entities.Cont_tbl_SubVendorMaster
                                                          where row.ID == UserID
                                                               && row.CustomerID == CustomerID
                                                               && row.IsDeleted == false
                                                          select row).FirstOrDefault();
                    if (vendorObj != null)
                    {
                        VendorName = vendorObj.VendorName;
                    }
                }
                return VendorName;
            }
        }

        public int getRID(string roleCode)
        {
            int RoleID = -1;
            try
            {

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Role data = (from row in entities.Roles
                                 where row.Code == roleCode
                                 select row).FirstOrDefault();
                    if (data != null)
                    {
                        RoleID = Convert.ToInt32(data.ID);
                    }
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return RoleID;
        }

        public class getTemplateTransactionDetail
        {
            public long TemplateTransactionID { get; set; }
            public long TemplateInstanceID { get; set; }
            public long ComplianceStatusID { get; set; }
            public System.DateTime CreatedOn { get; set; }
            public string Status { get; set; }
            public Nullable<System.DateTime> StatusChangedOn { get; set; }
            public string TemplateContent { get; set; }
            public long RoleID { get; set; }
            public long UserID { get; set; }
            public int sectionID { get; set; }
            public int orderID { get; set; }
            public int visibilty { get; set; }
            public string sectionHeader { get; set; }
        }


        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (RadEditor1.Content != "" && RadEditor1.Content != "null")
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {

                        if (Request.QueryString["ContId"] != null && Request.QueryString["ContId"] != ""
                            && Request.QueryString["CusttID"] != null && Request.QueryString["CusttID"] != ""
                            && Request.QueryString["UID"] != null && Request.QueryString["UID"] != ""
                            && Request.QueryString["RID"] != null && Request.QueryString["RID"] != "")
                        {
                            int CustomerID = Convert.ToInt32(Request.QueryString["CusttID"]);
                            int UserID = Convert.ToInt32(Request.QueryString["UID"]);
                            int RoleID = Convert.ToInt32(Request.QueryString["RID"]);
                            int RolegetVIRID = getRID("VIR");//Vendor Internal Reviewer
                            int RolegetVRID = getRID("VR");//Vendor Reviewer
                            int newContractID = Convert.ToInt32(Request.QueryString["ContId"]);
                            string ContractVersion = ContractTemplateManagement.getVersion(Convert.ToInt32(newContractID), CustomerID);
                            string UserName = getVendorName(UserID, CustomerID, RoleID, RolegetVIRID, RolegetVRID);

                            #region old

                            Cont_tbl_ContractTemplateSectionMapping obj12Mappingobj = (from row in entities.Cont_tbl_ContractTemplateSectionMapping
                                                                                       where row.ContractTemplateInstanceID == newContractID
                                                                                       select row).FirstOrDefault();

                            if (obj12Mappingobj != null)
                            {

                                ContractTemplateTransaction obj = new ContractTemplateTransaction();
                                obj.ContractTemplateInstanceID = newContractID;
                                obj.CreatedOn = DateTime.Now;
                                obj.CreatedBy = UserID;
                                obj.ContractTemplateContent = RadEditor1.Content;
                                obj.SectionID = obj12Mappingobj.SectionID;                             
                                obj.CreatedByText = UserName;
                                obj.Status = 17;
                                obj.OrderID = 1;
                                obj.Version = ContractVersion;

                                entities.ContractTemplateTransactions.Add(obj);
                                entities.SaveChanges();

                                int TransactionID = Convert.ToInt32(obj.Id);
                                if (TransactionID > 0)
                                {
                                    #region set transaction history

                                    var entitiesData1 = (from row in entities.Cont_SP_GetContractSectionsDetail(CustomerID, Convert.ToInt32(newContractID))
                                                         select row).ToList();

                                    var data1 = (from row in entities.Sp_GetContractTemplateDetailTransaction()
                                                 where row.UserID == UserID
                                                 && row.ContractTemplateInstanceID == newContractID
                                                 && row.RoleID == RoleID
                                                 select row).ToList();
                                    if (data1.Count > 0)
                                    {
                                        var data = (from row in data1
                                                    join row1 in entities.Cont_tbl_ContractTemplateSectionMapping
                                                    on row.sectionID equals row1.SectionID
                                                    where row1.ContractTemplateInstanceID == newContractID
                                                    select new getTemplateTransactionDetail
                                                    {
                                                        TemplateTransactionID = row.TemplateTransactionID,
                                                        TemplateInstanceID = row.ContractTemplateInstanceID,
                                                        ComplianceStatusID = row.ComplianceStatusID,
                                                        CreatedOn = row.CreatedOn,
                                                        Status = row.Status,
                                                        StatusChangedOn = row.StatusChangedOn,
                                                        TemplateContent = row.ContractTemplateContent,
                                                        RoleID = row.RoleID,
                                                        UserID = row.UserID,
                                                        sectionID = Convert.ToInt32(row.sectionID),
                                                        orderID = Convert.ToInt32(row1.SectionOrder),
                                                        visibilty = Convert.ToInt32(row1.Headervisibilty),
                                                        sectionHeader = row.SectionHeader
                                                    }).OrderBy(x => x.orderID).ToList();

                                        System.Text.StringBuilder strExporttoWord = new System.Text.StringBuilder();

                                        data.ForEach(eachSection =>
                                        {
                                            if (eachSection.visibilty == 1)
                                            {
                                                strExporttoWord.Append(@"<div>");
                                                strExporttoWord.Append(@"<p><B>" + eachSection.sectionHeader + "</B></p>");
                                                strExporttoWord.Append(@"</div>");
                                            }

                                            strExporttoWord.Append(@"<div>");
                                            strExporttoWord.Append(@"<p>" + eachSection.TemplateContent + "</p>");
                                            strExporttoWord.Append(@"</div>");
                                        });

                                        ContractTemplateTransactionHistory obj1 = new ContractTemplateTransactionHistory();
                                        obj1.ContractTemplateID = newContractID;
                                        obj1.CreatedOn = DateTime.Now;
                                        obj1.CreatedBy = UserID;
                                        obj1.TemplateContent = strExporttoWord.ToString();
                                        obj1.CreatedByText = UserName;
                                        obj1.Status = 17;
                                        obj1.OrderID = 1;
                                        entities.ContractTemplateTransactionHistories.Add(obj1);
                                        entities.SaveChanges();
                                    }
                                    #endregion
                                }
                            }
                            Cont_tbl_ContractStatusTransaction newStatusRecord = new Cont_tbl_ContractStatusTransaction()
                            {
                                CustomerID = CustomerID,
                                ContractID = Convert.ToInt32(newContractID),
                                StatusID = 17,
                                StatusChangeOn = DateTime.Now,
                                IsActive = true,
                                CreatedBy = UserID,
                                UpdatedBy = UserID,
                            };
                            ContractManagement.CreateContractStatusTransaction(newStatusRecord);
                            #endregion
                        }
                    }
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "ColsedSaveuploadModal();", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "ColsedSaveuploadModal1();", true);
                }

            }
            catch (Exception ex)
            {

            }
        }
    }
}