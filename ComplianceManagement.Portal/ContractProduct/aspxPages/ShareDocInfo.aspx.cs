﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataContract;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Microsoft.IdentityModel.Protocols;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.aspxPages
{
    public partial class ShareDocInfo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["AccessID"]))
                    {
                        ViewState["statusFileEditID"] = 0;
                        BindVendor();
                        BindContractDocSharing();
                        divfrmdt.Visible = false;
                        divtodt.Visible = false;

                        if (!string.IsNullOrEmpty(Request.QueryString["EditFileID"]))
                        {
                            int customerID = -1;
                            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                            int ID = Convert.ToInt32(Request.QueryString["EditFileID"]);

                            var fileRecord = VendorDetails.GetDocSharing_AllByID(customerID, ID);
                            if (fileRecord != null)
                            {
                                ddlVendor.SelectedValue = Convert.ToString(fileRecord.VendorID);
                                ddlPeriod.SelectedValue = Convert.ToString(fileRecord.Period);
                                if (fileRecord.Period == 6)
                                {
                                    divfrmdt.Visible = true;
                                    divtodt.Visible = true;
                                    
                                    txtfromdt.Text = Convert.ToDateTime(fileRecord.StartDate).ToString("dd-MM-yyyy");
                                    txttodate.Text = Convert.ToDateTime(fileRecord.EndDate).ToString("dd-MM-yyyy");

                                    //txtfromdt.Text = Convert.ToString(fileRecord.StartDate);
                                    //txttodate.Text = Convert.ToString(fileRecord.EndDate);                                    
                                }
                                ViewState["statusFileEditID"] = 1;

                                using (ContractMgmtEntities entities = new ContractMgmtEntities())
                                {
                                    Cont_tbl_VendorMaster data = (from row in entities.Cont_tbl_VendorMaster
                                                                  where row.ID == fileRecord.VendorID
                                                                       && row.CustomerID == customerID
                                                                  select row).FirstOrDefault();
                                    if (data != null)
                                    {
                                        txtEmail.Text = data.Email;
                                        txtcontactNum.Text = data.ContactNumber;
                                    }
                                }
                            }
                        }
                    }

                    if (!string.IsNullOrEmpty(Request.QueryString["CID"]))
                    {
                        ViewState["ContractID"] = Convert.ToInt64(Request.QueryString["CID"]);
                    }
                }

                ScriptManager.RegisterStartupScript(this, this.GetType(), "showDocInfo", "BindControls();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindVendor()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
              
                var lstVendors = VendorDetails.GetVendors_All(customerID);

                if (lstVendors.Count > 0)
                    lstVendors = lstVendors.OrderBy(row => row.VendorName).ToList();

                ddlVendor.DataTextField = "VendorName";
                ddlVendor.DataValueField = "ID";

                ddlVendor.DataSource = lstVendors;
                ddlVendor.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindContractDocSharing()
        {
            List<Cont_SP_GetSharingDocDetail_Result> lstContDocs = new List<Cont_SP_GetSharingDocDetail_Result>();

            var selectedFileids = Request.QueryString["AccessID"];
            List<int> fileIDs = selectedFileids.Split(',').Select(int.Parse).ToList();
            //long fileID = Convert.ToInt64(Request.QueryString["AccessID"]);
            long ContID = Convert.ToInt64(Request.QueryString["CID"]);

            long CustID = Convert.ToInt64(AuthenticationHelper.CustomerID);

            lstContDocs = VendorDetails.GetDocSharing_All(CustID, string.Empty);
            lstContDocs = lstContDocs.Where(x => fileIDs.Contains((int)x.FileID)).ToList();
            lstContDocs = lstContDocs.Where(x => x.ContractID == ContID).ToList();

            if (!string.IsNullOrEmpty(Request.QueryString["EditFileID"]))
            {
                int ID = Convert.ToInt32(Request.QueryString["EditFileID"]);
                lstContDocs = lstContDocs.Where(x => x.Id == ID).ToList();
            }
            
            grdContractDocuments.DataSource = lstContDocs;
            grdContractDocuments.DataBind();
        }
        

        protected void btnUpdateDocInfo_Click(object sender, EventArgs e)
        {
            try
            {
                bool validation = true;
                if (ddlVendor.SelectedValue == "" || ddlVendor.SelectedValue == "-1" || ddlVendor.SelectedValue == "0")
                {
                    validation = false;
                    cvDocInfo.IsValid = false;
                    cvDocInfo.ErrorMessage = "Please Select Vendor.";
                    return;
                }
                if (ddlPeriod.SelectedValue == "" || ddlPeriod.SelectedValue == "-1" || ddlPeriod.SelectedValue == "0")
                {
                    validation = false;
                    cvDocInfo.IsValid = false;
                    cvDocInfo.ErrorMessage = "Please Select Period.";
                    return;
                }
                if (string.IsNullOrEmpty(txtEmail.Text))
                {
                    validation = false;
                    cvDocInfo.IsValid = false;
                    cvDocInfo.ErrorMessage = "Please Enter Email.";
                    return;
                }
                if (string.IsNullOrEmpty(txtcontactNum.Text))
                {
                    validation = false;
                    cvDocInfo.IsValid = false;
                    cvDocInfo.ErrorMessage = "Please Enter Contact Number";
                    return;
                }
                if (ddlPeriod.SelectedValue == "6")
                {
                    if (string.IsNullOrEmpty(txtfromdt.Text))
                    {
                        validation = false;
                        cvDocInfo.IsValid = false;
                        cvDocInfo.ErrorMessage = "Please Enter From Date";
                        return;
                    }
                    if (string.IsNullOrEmpty(txttodate.Text))
                    {
                        validation = false;
                        cvDocInfo.IsValid = false;
                        cvDocInfo.ErrorMessage = "Please Enter To Date";
                        return;
                    }
                    if (!string.IsNullOrEmpty(txtfromdt.Text) && !string.IsNullOrEmpty(txttodate.Text))
                    {
                        DateTime d1 = Convert.ToDateTime(txtfromdt.Text);
                        DateTime d2 = Convert.ToDateTime(txttodate.Text);
                        if (d2 < d1)
                        {
                            validation = false;
                            cvDocInfo.IsValid = false;
                            cvDocInfo.ErrorMessage = "End Date should be grater than Start Date";
                            return;
                        }
                    }
                }

                if (validation)
                {
                    using (ContractMgmtEntities entities = new ContractMgmtEntities())
                    {
                        if (Convert.ToInt32(ViewState["statusFileEditID"]) == 1)
                        {
                            int ID = Convert.ToInt32(Request.QueryString["EditFileID"]);
                            var selectedFileids = Request.QueryString["AccessID"];
                            List<int> fileIDs = selectedFileids.Split(',').Select(int.Parse).ToList();
                            int Permission = 0;

                            long fileID = Convert.ToInt64(fileIDs[0]);
                            long ContID = Convert.ToInt64(Request.QueryString["CID"]);

                            long CustID = Convert.ToInt64(AuthenticationHelper.CustomerID);
                            long VendorID = Convert.ToInt64(ddlVendor.SelectedValue);
                            long Period = Convert.ToInt64(ddlPeriod.SelectedValue);
                            if (ddlPermission1.SelectedValue != "" && ddlPermission1.SelectedValue != "-1" && ddlPermission1.SelectedValue != "0")
                            {
                                Permission = Convert.ToInt32(ddlPermission1.SelectedValue);
                            }

                            Cont_tbl_SharingDoc vendorMappingExists = (from row in entities.Cont_tbl_SharingDoc
                                                                       where row.ContractID == ContID
                                                                       && row.Id == ID
                                                                       && row.FileID == fileID
                                                                       select row).FirstOrDefault();
                            if (vendorMappingExists != null)
                            {
                                vendorMappingExists.FileID = fileID;
                                vendorMappingExists.ContractID = ContID;
                                vendorMappingExists.VendorID = VendorID;
                                vendorMappingExists.Period = Period;
                                vendorMappingExists.UpdatedBy = AuthenticationHelper.UserID;
                                vendorMappingExists.UpdatedOn = DateTime.Now;
                                vendorMappingExists.IsDeleted = false;
                                vendorMappingExists.CustomerID = CustID;
                                vendorMappingExists.Permission = Permission;
                            
                                DateTime SD = DateTime.Now;
                                DateTime EndDt = DateTime.Now;

                                vendorMappingExists.StartDate = SD;
                                if (Period == 1)
                                {
                                    DateTime ED = SD.AddDays(30);
                                    vendorMappingExists.EndDate = ED;
                                    EndDt = ED;
                                }
                                if (Period == 2)
                                {
                                    DateTime ED = SD.AddDays(90);
                                    vendorMappingExists.EndDate = ED;
                                    EndDt = ED;
                                }
                                if (Period == 3)
                                {
                                    DateTime ED = SD.AddDays(180);
                                    vendorMappingExists.EndDate = ED;
                                    EndDt = ED;
                                }
                                if (Period == 4)
                                {
                                    DateTime ED = SD.AddDays(365);
                                    vendorMappingExists.EndDate = ED;
                                    EndDt = ED;
                                }
                                if (Period == 6)
                                {
                                    vendorMappingExists.StartDate = Convert.ToDateTime(txtfromdt.Text);
                                    vendorMappingExists.EndDate = Convert.ToDateTime(txttodate.Text);
                                    EndDt = Convert.ToDateTime(txttodate.Text);
                                }

                                if (DateTime.Now.Date <= EndDt.Date)
                                    vendorMappingExists.docexpired = false;
                                else
                                    vendorMappingExists.docexpired = true;

                                entities.SaveChanges();
                            }

                            ScriptManager.RegisterStartupScript(this, this.GetType(), "showDocInfo", "CloseMe();", true);
                        }
                        else
                        {
                            bool savesuccess = false;
                            long ContID = Convert.ToInt64(Request.QueryString["CID"]);
                            long CustID = Convert.ToInt64(AuthenticationHelper.CustomerID);
                            var selectedFileids = Request.QueryString["AccessID"];
                            int Permission = 0;
                            List<int> fileIDs = selectedFileids.Split(',').Select(int.Parse).ToList();
                            DateTime StartDt = DateTime.Now;
                            DateTime EndDt = DateTime.Now;
                            foreach (var item in fileIDs)
                            {
                                long fileID = Convert.ToInt64(item);
                               
                                long VendorID = Convert.ToInt64(ddlVendor.SelectedValue);
                                long Period = Convert.ToInt64(ddlPeriod.SelectedValue);
                                if (ddlPermission1.SelectedValue != "" && ddlPermission1.SelectedValue != "-1" && ddlPermission1.SelectedValue != "0")
                                {
                                    Permission = Convert.ToInt32(ddlPermission1.SelectedValue);
                                }

                                Cont_tbl_SharingDoc _objFileRecord = new Cont_tbl_SharingDoc()
                                {
                                    FileID = fileID,
                                    ContractID = ContID,
                                    VendorID = VendorID,
                                    Period = Period,
                                    Permission=Permission,
                                    CreatedBy = AuthenticationHelper.UserID,
                                    CreatedOn = DateTime.Now,
                                    IsDeleted = false,
                                    CustomerID = CustID
                                };

                                DateTime SD = DateTime.Now;
                                _objFileRecord.StartDate = SD;
                                if (Period == 1)
                                {
                                    DateTime ED = SD.AddDays(30);
                                    _objFileRecord.EndDate = ED;
                                    EndDt = ED;
                                }
                                if (Period == 2)
                                {
                                    DateTime ED = SD.AddDays(90);
                                    _objFileRecord.EndDate = ED;
                                    EndDt = ED;
                                }
                                if (Period == 3)
                                {
                                    DateTime ED = SD.AddDays(180);
                                    _objFileRecord.EndDate = ED;
                                    EndDt = ED;
                                }
                                if (Period == 4)
                                {
                                    DateTime ED = SD.AddDays(365);
                                    _objFileRecord.EndDate = ED;
                                    EndDt = ED;
                                }
                                if (Period == 6)
                                {
                                    _objFileRecord.StartDate = Convert.ToDateTime(txtfromdt.Text);
                                    _objFileRecord.EndDate = Convert.ToDateTime(txttodate.Text);
                                    EndDt = Convert.ToDateTime(txttodate.Text);
                                }

                                if (DateTime.Now.Date <= EndDt.Date)                                
                                    _objFileRecord.docexpired = false;                                
                                else
                                    _objFileRecord.docexpired = true;

                                bool saveSuccess = VendorDetails.CrontactDocSharingMapping(_objFileRecord);
                                if (saveSuccess)
                                {
                                    savesuccess = true;
                                    ContractManagement.CreateAuditLog("C", ContID, "Cont_tbl_SharingDoc", "Create", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Contract document sharing Updated", true, Convert.ToInt32(ContID));

                                    Cont_tbl_VendorMaster data = (from row in entities.Cont_tbl_VendorMaster
                                                                  where row.ID == VendorID
                                                                       && row.CustomerID == CustID
                                                                  select row).FirstOrDefault();
                                    if (data != null)
                                    {
                                        data.Email = txtEmail.Text;
                                        data.ContactNumber = txtcontactNum.Text;
                                        entities.SaveChanges();
                                    }
                                }
                            }
                            #region Mail send to Vendor for share document                          
                            if (savesuccess)
                            {
                                string vendorname = ddlVendor.SelectedItem.Text;
                                var obj = ContractManagement.getContractdetail(Convert.ToInt32(CustID), Convert.ToInt32(ContID));
                                if (obj != null)
                                {
                                    string contname = obj.ContractTitle;
                                    string contnum = obj.ContractNo;
                                    string portalURL = string.Empty;
                                    URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(AuthenticationHelper.CustomerID));
                                    if (Urloutput != null)
                                    {
                                        portalURL = Urloutput.URL;
                                    }
                                    else
                                    {
                                        portalURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                                    }

                                    string AccessURL = Convert.ToString(portalURL) +
                                                                "/ContractDocVerifyOtp.aspx?" +
                                                                "UID=" + ddlVendor.SelectedValue.ToString() +
                                                                  "&CustID=" + CustID +
                                                                    "&ContractID=" + ContID.ToString();

                                    string header = "Dear " + vendorname + "<br><br>";

                                    string data = "Contract Document has been shared with you for a specific period  " + StartDt.ToString("dd-MM-yyyy") + " to " + EndDt.ToString("dd-MM-yyyy") + "<br><br>";

                                    string ContractName = "Contract Name - " + contname + "<br>" + "<br>";

                                    string ContractNo = "Contract no. - " + contnum + "<br>" + "<br>";

                                    string UrlAccess = "Url to access document - " + AccessURL;

                                    string FinalMail = header + data + ContractName + ContractNo + UrlAccess;

                                    string SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"].ToString();

                                    string vendorEmail = txtEmail.Text;

                                    try
                                    {
                                        EmailManager.SendMail(SenderEmailAddress, new List<String>(new String[] { vendorEmail }), null, null, "Shared contract document", FinalMail);
                                    }
                                    catch (Exception ex)
                                    {

                                    }
                                }
                            }
                            #endregion
                            txtEmail.Text = "";
                            txtcontactNum.Text = "";
                            BindContractDocSharing();
                        }
                    }                   
                }               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDocInfo.IsValid = false;
                cvDocInfo.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void ddlVendor_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (ContractMgmtEntities entities = new ContractMgmtEntities())
            {
                int vendorID = Convert.ToInt32(ddlVendor.SelectedValue);

                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                int userID = -1;
                userID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);

                Cont_tbl_VendorMaster data = (from row in entities.Cont_tbl_VendorMaster
                                              where row.ID == vendorID
                                                   && row.CustomerID == customerID
                                              select row).FirstOrDefault();
                if (data != null)
                {
                    txtEmail.Text = data.Email;
                    txtcontactNum.Text = data.ContactNumber;
                }
            }
        }

        protected void ddlPeriod_SelectedIndexChanged(object sender, EventArgs e)
        {
            divfrmdt.Visible = false;
            divtodt.Visible = false;

            if (Convert.ToInt32(ddlPeriod.SelectedValue) == 6)
            {
                divfrmdt.Visible = true;
                divtodt.Visible = true;
            }
        }

        protected void grdContractDocuments_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdContractDocuments.PageIndex = e.NewPageIndex;
                //BindContractDocSharing();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdContractDocuments_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandArgument != null)
                {
                    GridView senderGridView = sender as GridView;

                    if (senderGridView != null)
                    {
                        if (e.CommandName.Equals("DeleteContDoc"))
                        {
                            int RecordId = Convert.ToInt32(e.CommandArgument);
                            int FileID = -1;
                            //int FileID = Convert.ToInt32(Request.QueryString["AccessID"]);
                            int ContractId = Convert.ToInt32(Request.QueryString["CID"]);

                            int customerID = -1;
                            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                            int userID = -1;
                            userID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                            
                            bool saveSuccess = VendorDetails.CrontactDocUnSharingMapping(RecordId, customerID, ContractId, FileID, userID);
                            if (saveSuccess)
                            {
                                cvDocInfo.IsValid = false;
                                cvDocInfo.ErrorMessage = "Document Unshare.";
                                BindContractDocSharing();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lnkBtnRebind_Vendor_Click(object sender, EventArgs e)
        {
            BindVendor();
        }
    }
}