﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Ionic.Zip;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using System.Web.Services;
using System.Threading.Tasks;
using System.Text;
using System.Globalization;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using com.VirtuosoITech.ComplianceManagement.Business.DataContract;

namespace com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.aspxPages
{
    public partial class ContractDetailsPage : System.Web.UI.Page
    {
        public static string DocumentPath = "";

        protected static String[] items;

        public static List<long> Branchlist = new List<long>();
        protected static int TemplateId;
        protected static int CustId;
        protected static int UId;
        protected static int RoleId;
        protected static int ContractTemplateInstanceId;
        protected static string IsCustID;
        protected static string KendoPath;
        protected static int contID;
        protected static bool IsOnlycontract;
        protected static bool RenameNoticeTerm;
        protected static bool ActiveContractsApprovals;
        bool IscontractEditbuttonShowCustID = false;
        protected static int customizedid;
        protected static int subtype;
        public bool ShowContractInitiator;
        public bool ShowDownLoadIcon;
        protected static string FlagIsApp;
        protected static string DocViewResult;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                RenameNoticeTerm = CaseManagement.CheckForClientNew(Convert.ToInt32(AuthenticationHelper.CustomerID), "RenameNoticeTerm");
                ActiveContractsApprovals = CaseManagement.CheckForClientNew(Convert.ToInt32(AuthenticationHelper.CustomerID), "ActiveContractsApprovals");
                ShowDownLoadIcon = CaseManagement.CheckForClient(Convert.ToInt32(AuthenticationHelper.CustomerID), "PermissionToDowloadAdama");
                VSContractPopup.CssClass = "alert alert-danger";
                vsTaskTab.CssClass = "alert alert-danger";
                items = new String[] { "Not Started", "In Progress", "Under Review", "Closed" };
                FlagIsApp = AuthenticationHelper.Role;
                IsCustID = Convert.ToString(ConfigurationManager.AppSettings["IsViewToUser"]);
                if (!IsPostBack)
                {
                    subtype = 0;
                    DocViewResult = "0";
                    customizedid = CustomerManagement.GetCustomizedCustomerid(AuthenticationHelper.CustomerID, "ContractSubTypeWiseContractNo");
                     ShowContractInitiator = CaseManagement.CheckForClientNew(Convert.ToInt32(AuthenticationHelper.CustomerID), "ShowContractInitiator");

                    if (customizedid == AuthenticationHelper.CustomerID)
                    {
                        subtype = 1;
                    }
                   
                    KendoPath = ConfigurationManager.AppSettings["KendoPathApp"];
                    BindVendors();
                    BindPaymentTerms();
                    BindCustomerBranches();
                    BindDepartments();
                    BindUsers();
                    DocumentManagement.DeleteAllTempContractDocumentFile(Convert.ToInt64(AuthenticationHelper.UserID), Convert.ToInt64(AuthenticationHelper.CustomerID));
                    btnsavedraft.Visible = false;
                    IsApproverReviewer.Visible = false;
                    CommentContract.Visible = false;
                    statusandcommentsection.Visible = false;
                    uploaddocsection.Visible = false;
                    lblddlCPDepartment.Visible = true;                                    
                    BindContractCategoryType();
                    BindContractStatusList(false, true);

                    ViewState["PageLink"] = "ContractLink";
                    lnkexecutivesummary.Visible = false;
                    var ConIniID = Request.QueryString["ConAccessID"];
                    int coninid = Convert.ToInt32(ConIniID);
                    //if (ShowContractInitiator == true)
                    //{
                    //    string contractnum = GetcontractNum(coninid);
                    //    txtContractNo.Text = contractnum;
                    //}
                    if (!string.IsNullOrEmpty(Request.QueryString["AccessID"]))
                    {
                        var contractno = Request.QueryString["ContractNo"];
                        var contractInstanceID = Request.QueryString["AccessID"];
                        
                        if (contractInstanceID != "")
                        {
                            contID = Convert.ToInt32(contractInstanceID);
                            ViewState["ContractInstanceID"] = contractInstanceID;
                            Session["ContractInstanceID"] = contractInstanceID;
                            BindTransactions();

                            if (!string.IsNullOrEmpty(Request.QueryString["HistoryFlag"]))
                            {
                                ViewState["FlagHistory"] = "1";
                            }
                            else
                            {
                                ViewState["FlagHistory"] = null;
                            }
                            
                            if (Convert.ToInt64(contractInstanceID) == 0)
                            {
                                //if (IsOnlycontract)
                                //    btnsavedraft.Visible = true;

                                drdTemplateID.Enabled = true;
                                BindContractStatusList(true);
                                btnAddContract_Click(sender, e);  //Add New Detail
                                BindTempDocumentData(Convert.ToInt64(AuthenticationHelper.UserID), Convert.ToInt64(AuthenticationHelper.CustomerID));                              
                            }
                            else
                            {
                                btnsavedraft.Visible = false;

                                if (!string.IsNullOrEmpty(Request.QueryString["AccessRoleID"]))
                                    BindStatus(Convert.ToString(Request.QueryString["AccessRoleID"]));
                                else
                                    BindStatus("3");

                                BindShareUsers(Convert.ToInt32(ViewState["ContractInstanceID"]));
                                BindContractStatusList(false);
                                btnEditContract_Click(sender, e); //Edit Detail 
                                RoleId = getRoleID(Convert.ToInt32(contractInstanceID), AuthenticationHelper.UserID);
                                TemplateId = Convert.ToInt32(contractInstanceID);
                                UId = Convert.ToInt32(AuthenticationHelper.UserID);
                                CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                            }
                        }
                    }
                    else
                    {
                        drdTemplateID.Enabled = true;
                        BindContractStatusList(true);
                    }

                    ViewState["CustomefieldCount"] = null;

                    //Show Hide Grid Control - Enable/Disable Form Controls
                    if (ViewState["ContractStatusID"] != null)
                    {
                        showHideContractSummaryTabTopButtons(true);
                        enableDisableContractPopUpControls(true);

                        string customer = ConfigurationManager.AppSettings["ContractActionDisplay"].ToString();
                        int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                        List<string> PenaltyNotDisplayCustomerList = customer.Split(',').ToList();
                        if (PenaltyNotDisplayCustomerList.Count > 0)
                        {
                            foreach (string PList in PenaltyNotDisplayCustomerList)
                            {
                                if (PList == customerID.ToString())
                                {
                                    IscontractEditbuttonShowCustID = true;
                                }
                            }
                        }

                        if (IscontractEditbuttonShowCustID == true)
                        {
                            long contractInstanceID = 0;
                            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["ContractInstanceID"])))
                            {
                                contractInstanceID = Convert.ToInt32(ViewState["ContractInstanceID"]);

                                if (contractInstanceID != 0)
                                {
                                    var lstContractUserAssignment = ContractManagement.GetContractUserAssignment_All(contractInstanceID);
                                    if (lstContractUserAssignment.Count > 0)
                                    {
                                        int UserID = Convert.ToInt32(AuthenticationHelper.UserID);
                                        var query = lstContractUserAssignment.Where(x => x.UserID == UserID).ToList();
                                        if (query.Count <= 0)
                                        {
                                            btnEditContractDetail.Visible = false;
                                        }
                                        else
                                        {
                                            btnEditContractDetail.Visible = true;
                                        }
                                    }

                                }
                            }
                        }
                    }
                    else
                    {
                        enableDisableContractPopUpControls(true);
                    }

                    if (!string.IsNullOrEmpty(Request.QueryString["HistoryFlag"]))
                    {
                        ViewState["FlagHistory"] = "1";

                        enableDisableContractPopUpControls(false);
                        showHideContractSummaryTabTopButtons(false);
                        ShowHide_AddNewButtons(false);
                    }
                    else
                    {
                        ViewState["FlagHistory"] = null;
                        ShowHide_AddNewButtons(true);
                    }

                    if (ShowDownLoadIcon == true)
                    {
                        if (FlagIsApp == "MGMT" || FlagIsApp == "CADMN")
                        {
                            divShowPermission.Visible = true;
                            DocViewResult = "1";
                        }
                        if (lstBoxOwner.Items.FindByValue(AuthenticationHelper.UserID.ToString()) != null)
                        {
                            if (lstBoxOwner.Items.FindByValue(AuthenticationHelper.UserID.ToString()).Selected == true)
                            {
                                divShowPermission.Visible = true;
                            }
                        }
                    }
                    else
                    {
                        divShowPermission.Visible = false;
                        DocViewResult = "1";
                    }
                }

                applyCSStoFileTag_ListItems();
                if (IsCustID == Convert.ToString(AuthenticationHelper.CustomerID))
                {
                    int contractInstanceID = Convert.ToInt32(ViewState["ContractInstanceID"]);
                    IsViewUser();
                }
                else
                {
                    lnksharesummary.Visible = false;
                }

                CommentContract.Visible = false;
                statusandcommentsection.Visible = false;
                uploaddocsection.Visible = false;
                btnsavedraft.Visible = false;

                bool IsPerformercontract = ContractManagement.getcontractchecklist(Convert.ToInt32(AuthenticationHelper.CustomerID));
                if (IsPerformercontract)
                {
                    lblddlCPDepartment.Visible = false;
                    CommentContract.Visible = false;
                    divtemplateID.Visible = false;
                    IsApproverReviewer.Visible = true;
                    lblBoxOwner.InnerText = "Legal User(s)";
                    ddlContractStatus.Enabled = false;

                    if (ViewState["ContractInstanceID"] != null)
                    {
                        long contractInstanceID = Convert.ToInt64(ViewState["ContractInstanceID"]);
                        if (contractInstanceID > 0)
                        {                            
                            BindContractDocuments_All(grdMailDocumentList);
                            if (true)
                            {
                                statusandcommentsection.Visible = false;
                                uploaddocsection.Visible = false;

                                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                {
                                    var getrecord = (from row in entities.Cont_tbl_ContractInstance
                                                     where row.ID == contractInstanceID
                                                     && row.IsDeleted == false
                                                     && row.IsReviewedContract == 0
                                                     select row).FirstOrDefault();
                                    if (getrecord != null)
                                    {
                                        lblCustomField.Visible = false;
                                        grdCustomField.Visible = false;
                                        liContractDetails.Visible = false;
                                        liDocument.Visible = false;
                                        liTask.Visible = false;
                                        liAuditLog.Visible = false;
                                        liContractTransaction.Visible = true;
                                        //liContractTransaction.Visible = false;
                                        liMilestone.Visible = false;

                                        BindTempDocumentData(Convert.ToInt64(AuthenticationHelper.UserID), Convert.ToInt64(AuthenticationHelper.CustomerID));

                                        var userRoles = (from row in entities.ContractAssignments
                                                         where row.ContractInstanceID == contractInstanceID
                                                         && row.IsActive == true
                                                         select row).ToList();
                                        if (userRoles.Count > 0)
                                        {
                                            long RID = userRoles.Where(x => x.RoleID == 4).Select(x => x.UserID).FirstOrDefault();
                                            long AID = userRoles.Where(x => x.RoleID == 6).Select(x => x.UserID).FirstOrDefault();

                                            if (RID > 0)
                                                ddlReviewer.SelectedValue = RID.ToString();

                                            if (AID > 0)
                                                ddlApprover.SelectedValue = AID.ToString();

                                            ddlReviewer.Enabled = false;
                                            ddlApprover.Enabled = false;
                                            btnEditContractDetail.Visible = false;
                                            lnksharesummary.Visible = false;
                                            lnkSendMailWithDoc.Visible = false;
                                            lnkLinkContract.Visible = false;
                                        }
                                        if (!string.IsNullOrEmpty(Request.QueryString["AccessRoleID"]))
                                        {
                                            CommentContract.Visible = true;
                                            var ContractCurrentStatus = (from row in entities.Cont_View_RecentContractReviweStatusTransaction
                                                                         where row.ContractID == contractInstanceID
                                                                         select row).FirstOrDefault();
                                            if (ContractCurrentStatus != null)
                                            {
                                                if ((Convert.ToInt32(Request.QueryString["AccessRoleID"]) == 4 && ContractCurrentStatus.ContractStatusID == 2)
                                                    || (Convert.ToInt32(Request.QueryString["AccessRoleID"]) == 6 && ContractCurrentStatus.ContractStatusID == 4))
                                                {
                                                    btnEditContractDetail.Visible = true;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            btnEditContractDetail.Visible = true;
                                            var ContractCurrentStatus = (from row in entities.Cont_View_RecentContractReviweStatusTransaction
                                                                         where row.ContractID == contractInstanceID
                                                                         select row).FirstOrDefault();
                                            if (ContractCurrentStatus.ContractStatusID == 2 || ContractCurrentStatus.ContractStatusID == 15 || ContractCurrentStatus.ContractStatusID == 5 || ContractCurrentStatus.ContractStatusID == 4 || ContractCurrentStatus.ContractStatusID == 7)
                                            {
                                                CommentContract.Visible = true;
                                                btnEditContractDetail.Visible = false;
                                                statusandcommentsection.Visible = false;//true;
                                                uploaddocsection.Visible = true;

                                                ddlReviewer.Enabled = false;
                                                ddlApprover.Enabled = false;
                                                lnksharesummary.Visible = false;
                                                lnkSendMailWithDoc.Visible = false;
                                                lnkLinkContract.Visible = false;
                                                lnklinktotermsheet.Visible = false;
                                            }
                                            else if (ContractCurrentStatus.ContractStatusID == 1)
                                            {
                                                btnEditContractDetail.Visible = true;
                                                statusandcommentsection.Visible = true;
                                                uploaddocsection.Visible = true;
                                                btnsavedraft.Visible = true;
                                                ddlReviewer.Enabled = true;
                                                ddlApprover.Enabled = true;
                                                lnksharesummary.Visible = false;
                                                lnkSendMailWithDoc.Visible = false;
                                                lnkLinkContract.Visible = false;
                                                lnklinktotermsheet.Visible = false;
                                            }                                           
                                        }
                                        if (!string.IsNullOrEmpty(Request.QueryString["AccessRoleID"]))
                                        {
                                            if (Convert.ToInt32(Request.QueryString["AccessRoleID"]) == 4 || Convert.ToInt32(Request.QueryString["AccessRoleID"]) == 6)
                                            {
                                                statusandcommentsection.Visible = true;
                                                uploaddocsection.Visible = true;
                                            }
                                            if (Convert.ToInt32(Request.QueryString["AccessRoleID"]) == 3)
                                            {
                                                statusandcommentsection.Visible = true;
                                            }
                                            if (Convert.ToInt32(Request.QueryString["AccessRoleID"]) == 38)
                                            {
                                                var ContractCurrentStatus = (from row in entities.Cont_View_RecentContractReviweStatusTransaction
                                                                             where row.ContractID == contractInstanceID
                                                                             select row).FirstOrDefault();
                                                if (ContractCurrentStatus.ContractStatusID == 5)
                                                {
                                                    statusandcommentsection.Visible = true;
                                                    btnSaveContract.Visible = true;
                                                    btnSaveContract.Attributes.Remove("disabled");
                                                }
                                            }
                                        }                                        
                                    }
                                    else
                                    {
                                        lblCustomField.Visible = false;
                                        grdCustomField.Visible = false;
                                    }
                                }
                            }
                        }
                        else
                        {
                            btnsavedraft.Visible = true;
                            statusandcommentsection.Visible = false;
                            uploaddocsection.Visible = true;
                        }
                    }
                }
                long ContractStatusID = Convert.ToInt64(ViewState["ContractStatusID"]);
                if(ContractStatusID == 8 || ContractStatusID == 23)
                {
                    enableDisableContractMileStoneTabControls();
                //    enableDisableSummaryTab();
                    enableDisableContractDetailTabControls();
                    btnSaveContract.Attributes.Add("disabled", "disabled");
                    lnkAddNewTask.Attributes.Add("disabled", "disabled");
                    btnEditContractDetail.Visible = false;

                    btnEditContractDetail.Attributes.Remove("disabled");
                   // TabTask_Click(sender, e);
                }

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divBranches');", tbxBranch.ClientID), true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "hideDivTree", "hideDivBranch();", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "MyPostBackScript", "FetchUSerDetail();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        #region Common


        
        private void BindContractStatusList(bool status)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                drdTemplateID.DataSource = null;
                drdTemplateID.DataBind();
                drdTemplateID.ClearSelection();

                drdTemplateID.DataTextField = "TemplateName";
                drdTemplateID.DataValueField = "ID";

                var statusList = ContractTaskManagement.GetApprovedTemplateList(customerID);
                if (status)
                {
                    statusList = statusList.Where(x => x.status == true).ToList();
                }
                drdTemplateID.DataSource = statusList;
                drdTemplateID.DataBind();
                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindContractStatusList(bool isForTask, bool isVisibleToUser)
        {
            try
            {
                ddlContractStatus.DataSource = null;
                ddlContractStatus.DataBind();
                ddlContractStatus.ClearSelection();

                ddlContractStatus.DataTextField = "StatusName";
                ddlContractStatus.DataValueField = "ID";

                var statusList = ContractTaskManagement.GetStatusList_All(isForTask, isVisibleToUser);

                ddlContractStatus.DataSource = statusList;
                ddlContractStatus.DataBind();

                items = statusList.Select(row => row.StatusName).ToArray();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindContractCategoryType()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                int CacheClearTime = Convert.ToInt32(ConfigurationManager.AppSettings["CacheClearMasterData"]);
                var key = "Conttype-" + Convert.ToString(customerID);
                var lstContractTypes = (List<Cont_tbl_TypeMaster>)HttpContext.Current.Cache[key];
                if (HttpContext.Current.Cache[key] == null)
                {
                    lstContractTypes = ContractTypeMasterManagement.GetContractTypes_All(customerID);
                    HttpContext.Current.Cache.Insert(key, lstContractTypes, null, DateTime.Now.AddMinutes(CacheClearTime), TimeSpan.Zero); // add it to cache
                }

                if (lstContractTypes.Count > 0)
                    lstContractTypes = lstContractTypes.OrderBy(row => row.TypeName).ToList();

                ddlContractType.DataTextField = "TypeName";
                ddlContractType.DataValueField = "ID";

                ddlContractType.DataSource = lstContractTypes;
                ddlContractType.DataBind();          
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lnkBtnRebind_Type_Click(object sender, EventArgs e)
        {
            var key = "Conttype-" + Convert.ToString(AuthenticationHelper.CustomerID);
            CacheHelper.Remove(key);
            BindContractCategoryType();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "rebindTypes", "rebindContractTypes();", true);
        }

        private bool BindContractSubType(long contractTypeID)
        {
            bool bindSuccess = false;
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                int CacheClearTime = Convert.ToInt32(ConfigurationManager.AppSettings["CacheClearMasterData"]);
                var key = "Contsubtype-" + Convert.ToString(customerID) + "-" + contractTypeID;
           
                var lstContractSubTypes = (List<Cont_SP_GetAllContractTypeSubType_Result>)HttpContext.Current.Cache[key];
                if (HttpContext.Current.Cache[key] == null)
                {
                    lstContractSubTypes = ContractTypeMasterManagement.GetContractSubType_All(customerID, contractTypeID);
                    HttpContext.Current.Cache.Insert(key, lstContractSubTypes, null, DateTime.Now.AddMinutes(CacheClearTime), TimeSpan.Zero); // add it to cache
                }

                if (lstContractSubTypes.Count > 0)
                {
                    ddlContractSubType.DataTextField = "SubTypeName";
                    ddlContractSubType.DataValueField = "ContractSubTypeID";
                    ddlContractSubType.DataSource = lstContractSubTypes;
                    ddlContractSubType.DataBind();                   
                    bindSuccess = true;
                }
                return bindSuccess;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return bindSuccess;
            }
        }

        protected void lnkBtnRebind_SubType_Click(object sender, EventArgs e)
        {
            if (ddlContractType.SelectedValue != null && ddlContractType.SelectedValue != "0" && ddlContractType.SelectedValue != "")
            {
                var key = "Contsubtype-" + Convert.ToString(AuthenticationHelper.CustomerID);
                CacheHelper.Remove(key);

                BindContractSubType(Convert.ToInt64(ddlContractType.SelectedValue));
                ScriptManager.RegisterStartupScript(this, this.GetType(), "rebindTypes", "rebindSubTypes();", true);
            }
        }

        private void BindFileTags()
        {
            try
            {
                if (ViewState["ContractInstanceID"] != null)
                {
                    int customerID = -1;
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                    long contractInstanceID = Convert.ToInt64(ViewState["ContractInstanceID"]);

                    if (contractInstanceID != 0)
                    {
                        var lstTags = ContractDocumentManagement.GetDistinctFileTags(customerID, contractInstanceID);

                        lstBoxFileTags.Items.Clear();

                        if (lstTags.Count > 0)
                        {
                            for (int i = 0; i < lstTags.Count; i++)
                            {
                                lstBoxFileTags.Items.Add(new ListItem(lstTags[i], lstTags[i]));
                            }
                        }

                        applyCSStoFileTag_ListItems();
                    }
                    else
                    {
                        lstBoxFileTags.DataSource = null;
                        lstBoxFileTags.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void applyCSStoFileTag_ListItems()
        {
            foreach (ListItem eachItem in lstBoxFileTags.Items)
            {
                if (eachItem.Selected)
                    eachItem.Attributes.Add("class", "label label-info-selected");
                else
                    eachItem.Attributes.Add("class", "label label-info");
                eachItem.Attributes.Add("onclick", "fcheckcontract(this)");
            }
        }

        protected void lstBoxFileTags_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindContractDocuments_All();

                List<ListItem> lstSelectedTags = new List<ListItem>();

                foreach (ListItem eachListItem in lstBoxFileTags.Items)
                {
                    if (eachListItem.Selected)
                        lstSelectedTags.Add(eachListItem);
                }

                //Re-Arrange File Tags
                var arrangedListItems = ContractManagement.ReArrange_FileTags(lstBoxFileTags);

                lstBoxFileTags.DataSource = arrangedListItems;
                lstBoxFileTags.DataBind();

                foreach (ListItem eachListItem in lstSelectedTags)
                {
                    if (lstBoxFileTags.Items.FindByValue(eachListItem.Value) != null)
                        lstBoxFileTags.Items.FindByValue(eachListItem.Value).Selected = true;
                }

                applyCSStoFileTag_ListItems();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindCustomerBranches()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                tvBranches.Nodes.Clear();
                NameValueHierarchy branch = null;

                //var branchs = CustomerBranchManagement.GetAllHierarchy(customerID);
                //var branchs = CustomerBranchManagement.GetAllHierarchyManagementSatutory(customerID);
                List<NameValueHierarchy> branchs;
                //string key = "LocationHierarchy" + AuthenticationHelper.CustomerID;
                //if (CacheHelper.Exists(key))
                //{
                //    CacheHelper.Get<List<NameValueHierarchy>>(key, out branchs);
                //}
                //else
                //{
                //branchs = CustomerBranchManagement.GetAllContractEntitiesAssignedHierarchyManagementSatutory(customerID,AuthenticationHelper.UserID,AuthenticationHelper.Role);
                //CacheHelper.Set<List<NameValueHierarchy>>(key, branchs);
                //}
                branchs = CustomerBranchManagement.GetAllHierarchyManagementSatutory(customerID);
                if (branchs.Count > 0)
                {
                    branch = branchs[0];
                }

                //tbxBranch.Text = "Select Entity/Location";
                tbxBranch.Text = "Click to Select";

                List<TreeNode> nodes = new List<TreeNode>();
                BindBranchesHierarchy(null, branch, nodes);
                foreach (TreeNode item in nodes)
                {
                    tvBranches.Nodes.Add(item);
                }

                tvBranches.CollapseAll();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvContractPopUp.IsValid = false;
                cvContractPopUp.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp, List<TreeNode> nodes)
        {
            try
            {
                if (nvp != null)
                {
                    foreach (var item in nvp.Children)
                    {
                        TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                        BindBranchesHierarchy(node, item, nodes);
                        if (parent == null)
                        {
                            nodes.Add(node);
                        }
                        else
                        {
                            parent.ChildNodes.Add(node);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvContractPopUp.IsValid = false;
                cvContractPopUp.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        public void BindDepartments()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            int userID = -1;
            userID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
            int CacheClearTime = Convert.ToInt32(ConfigurationManager.AppSettings["CacheClearMasterData"]);
            //var lstDepts = CompDeptManagement.GetAllDepartmentMasterList(customerID);
            var key = "Contdepart-" + Convert.ToString(customerID);
            var lstDepts = (List<Department>)HttpContext.Current.Cache[key];
            if (HttpContext.Current.Cache[key] == null)
            {
                lstDepts = CompDeptManagement.GetAllDepartmentMasterList(customerID);
                HttpContext.Current.Cache.Insert(key, lstDepts, null, DateTime.Now.AddMinutes(CacheClearTime), TimeSpan.Zero); // add it to cache
            }

            var TypeList = ContractMastersManagement.GetContractUserDepartmentMapping_Paging(customerID);
            if (TypeList.Count > 0)
            {
                var datalist = TypeList.Where(x => x.UserID == userID && x.IsContractOwner == true).ToList();
                if (datalist.Count > 0)
                {
                    List<long> getdept = datalist.Select(x => x.DeptID).ToList();

                    lstDepts = lstDepts.Where(x => getdept.Contains(x.ID)).ToList();
                }
                else
                    lstDepts.Clear();

                if (lstDepts.Count > 0)
                    lstDepts = lstDepts.OrderBy(row => row.Name).ToList();

                ddlDepartment.DataTextField = "Name";
                ddlDepartment.DataValueField = "ID";

                ddlDepartment.DataSource = lstDepts;
                ddlDepartment.DataBind();

                drpmilestoneDepartment.DataTextField = "Name";
                drpmilestoneDepartment.DataValueField = "ID";

                drpmilestoneDepartment.DataSource = lstDepts;
                drpmilestoneDepartment.DataBind();
            }
            //ddlDepartment.Items.Add(new ListItem("Add New", "0"));
        }


      
        protected void lnkBtnRebind_Dept_Click(object sender, EventArgs e)
        {
            
            var key = "Contdepart-" + Convert.ToString(AuthenticationHelper.CustomerID);
            CacheHelper.Remove(key);

            BindDepartments();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "rebindDepts", "rebindDepartments();", true);
        }

        protected void lnkBtnRebind_ContactPersonDept_Click(object sender, EventArgs e)
        {
            var key = "Contusers-" + Convert.ToString(AuthenticationHelper.CustomerID);
            CacheHelper.Remove(key);

            BindUsers();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "rebindTaskUser", "restoreSelectedTaskUsersNew();", true);
        }


        public void BindGrid(int contractID)
        {
            try
            {
                if (contractID != 0)
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        var lstLinkedContracts = (from row in entities.Cont_SP_GetAssignedContracts_All(Convert.ToInt32(AuthenticationHelper.CustomerID))
                                                  select row).ToList();
                        if (lstLinkedContracts.Count > 0)
                        {
                            lstLinkedContracts = (from g in lstLinkedContracts
                                                  group g by new
                                                  {
                                                      g.ID, //ContractID
                                                      g.CustomerID,
                                                      g.ContractNo,
                                                      g.ContractTitle,
                                                      g.ContractDetailDesc,
                                                      g.VendorIDs,
                                                      g.VendorNames,
                                                      g.CustomerBranchID,
                                                      g.BranchName,
                                                      g.DepartmentID,
                                                      g.DeptName,
                                                      g.ContractTypeID,
                                                      g.TypeName,
                                                      g.ContractSubTypeID,
                                                      g.SubTypeName,
                                                      g.ProposalDate,
                                                      g.AgreementDate,
                                                      g.EffectiveDate,
                                                      g.ReviewDate,
                                                      g.ExpirationDate,
                                                      g.CreatedOn,
                                                      g.UpdatedOn,
                                                      g.StatusName,
                                                      g.ContractAmt,
                                                      g.ContactPersonOfDepartment,
                                                      g.PaymentType,
                                                      g.AddNewClause
                                                  } into GCS
                                                  select new Cont_SP_GetAssignedContracts_All_Result()
                                                  {
                                                      ID = GCS.Key.ID, //ContractID
                                                      CustomerID = GCS.Key.CustomerID,
                                                      ContractNo = GCS.Key.ContractNo,
                                                      ContractTitle = GCS.Key.ContractTitle,
                                                      ContractDetailDesc = GCS.Key.ContractDetailDesc,
                                                      VendorIDs = GCS.Key.VendorIDs,
                                                      VendorNames = GCS.Key.VendorNames,
                                                      CustomerBranchID = GCS.Key.CustomerBranchID,
                                                      BranchName = GCS.Key.BranchName,
                                                      DepartmentID = GCS.Key.DepartmentID,
                                                      DeptName = GCS.Key.DeptName,
                                                      ContractTypeID = GCS.Key.ContractTypeID,
                                                      TypeName = GCS.Key.TypeName,
                                                      ContractSubTypeID = GCS.Key.ContractSubTypeID,
                                                      SubTypeName = GCS.Key.SubTypeName,
                                                      ProposalDate = GCS.Key.ProposalDate,
                                                      AgreementDate = GCS.Key.AgreementDate,
                                                      EffectiveDate = GCS.Key.EffectiveDate,
                                                      ReviewDate = GCS.Key.ReviewDate,
                                                      ExpirationDate = GCS.Key.ExpirationDate,
                                                      CreatedOn = GCS.Key.CreatedOn,
                                                      UpdatedOn = GCS.Key.UpdatedOn,
                                                      StatusName = GCS.Key.StatusName,
                                                      ContractAmt = GCS.Key.ContractAmt,
                                                      ContactPersonOfDepartment = GCS.Key.ContactPersonOfDepartment,
                                                      PaymentType = GCS.Key.PaymentType,
                                                      AddNewClause = GCS.Key.AddNewClause
                                                  }).ToList();
                        }
                        lstLinkedContracts = lstLinkedContracts.Where(entry => entry.ID != contractID).ToList();

                        grdContractList_LinkContract.DataSource = lstLinkedContracts;
                        grdContractList_LinkContract.DataBind();
                    }
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
        }

        public static void BindContractDocuments(GridView grd, ListBox lstBoxFileTags)
        {
            if (HttpContext.Current.Session["ContractInstanceID"] != null)
            {
                long ContractInstanceID = Convert.ToInt64(HttpContext.Current.Session["ContractInstanceID"]);

                List<ListItem> selectedItems = ContractManagement.GetSelectedItems(lstBoxFileTags);

                var selectedFileTags = selectedItems.Select(row => row.Text).ToList();

                List<Cont_SP_GetContractDocuments_FileTags_All_Result> lstContDocs = new List<Cont_SP_GetContractDocuments_FileTags_All_Result>();

                lstContDocs = ContractDocumentManagement.Cont_SP_GetContractDocuments_FileTags_All(Convert.ToInt32(AuthenticationHelper.CustomerID), ContractInstanceID, selectedFileTags);

                grd.DataSource = lstContDocs;
                grd.DataBind();

                foreach (ListItem eachItem in lstBoxFileTags.Items)
                {
                    eachItem.Attributes.Add("class", "label label-info");
                }

                lstContDocs.Clear();
                lstContDocs = null;

                //upContractDocUploadPopup.Update();
            }
        }

        [WebMethod]
        public static List<ListItem> GetDepartments()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            var lstDepartments = CompDeptManagement.GetAllDepartmentMasterList(customerID);

            List<ListItem> lstDepts = new List<ListItem>();

            if (lstDepartments.Count > 0)
            {
                lstDepartments = lstDepartments.OrderBy(row => row.Name).ToList();

                for (int i = 0; i < lstDepartments.Count; i++)
                {
                    lstDepts.Add(new ListItem
                    {
                        Value = lstDepartments.ToList()[i].ID.ToString(),
                        Text = lstDepartments.ToList()[i].Name,
                    });
                }

                //lstDepts.Add(new ListItem
                //{
                //    Value = "0",
                //    Text = "Add New",
                //});
            }

            return lstDepts;
        }

        [WebMethod]
        public static List<ListItem> GetContractTypes()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            var lstTypes = ContractTypeMasterManagement.GetContractTypes_All(customerID);

            List<ListItem> lstcontType = new List<ListItem>();

            if (lstTypes.Count > 0)
            {
                lstTypes = lstTypes.OrderBy(row => row.TypeName).ToList();

                for (int i = 0; i < lstTypes.Count; i++)
                {
                    lstcontType.Add(new ListItem
                    {
                        Value = lstTypes.ToList()[i].ID.ToString(),
                        Text = lstTypes.ToList()[i].TypeName,
                    });
                }

                //lstcontType.Add(new ListItem
                //{
                //    Value = "0",
                //    Text = "Add New",
                //});
            }

            return lstcontType;
        }

        [WebMethod]
        public static List<ListItem> GetContractSubTypes(string selectedContractType)
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            List<ListItem> lstContSubType = new List<ListItem>();

            long contTypeID = Convert.ToInt64(selectedContractType);
            if (contTypeID > 0)
            {
                var obj = ContractTypeMasterManagement.GetContractSubType_All(customerID, contTypeID);

                if (obj.Count > 0)
                {
                    obj = obj.OrderBy(row => row.SubTypeName).ToList();

                    for (int i = 0; i < obj.Count; i++)
                    {
                        lstContSubType.Add(new ListItem
                        {
                            Value = obj.ToList()[i].ContractSubTypeID.ToString(),
                            Text = obj.ToList()[i].SubTypeName,
                        });
                    }

                    //lstContSubType.Add(new ListItem
                    //{
                    //    Value = "0",
                    //    Text = "Add New",
                    //});
                }
            }
            return lstContSubType;
        }

        [WebMethod]
        public static List<ListItem> GetContractVendors()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            List<ListItem> lstVendors = new List<ListItem>();

            var obj = VendorDetails.GetVendors_All(customerID);

            if (obj.Count > 0)
            {
                obj = obj.OrderBy(row => row.VendorName).ToList();

                for (int i = 0; i < obj.Count; i++)
                {
                    lstVendors.Add(new ListItem
                    {
                        Value = obj.ToList()[i].ID.ToString(),
                        Text = obj.ToList()[i].VendorName,
                    });
                }

                //lstVendors.Add(new ListItem
                //{
                //    Value = "0",
                //    Text = "Add New",
                //});
            }

            return lstVendors;
        }

        [WebMethod]
        public static List<ListItem> GetTaskUsers()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            List<ListItem> lstInternalUsers = new List<ListItem>();

            var lstAllUsers = ContractUserManagement.GetAllUsers_Contract(customerID);

            //var allUsers = ContractUserManagement.GetRequiredUsers(lstAllUsers, 4); //4--All---Internal and External Users

            lstAllUsers = lstAllUsers.Where(row => row.IsActive == true).ToList();

            if (lstAllUsers.Count > 0)
            {
                for (int i = 0; i < lstAllUsers.Count; i++)
                {
                    lstInternalUsers.Add(new ListItem
                    {
                        Value = lstAllUsers.ToList()[i].ID.ToString(),
                        Text = lstAllUsers.ToList()[i].FirstName + " " + lstAllUsers.ToList()[i].LastName,
                    });
                }

                //lstInternalUsers.Add(new ListItem
                //{
                //    Value = "0",
                //    Text = "Add New",
                //});
            }

            return lstInternalUsers;
        }

        [WebMethod]
        public static List<ListItem> GetDocTypes()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            List<ListItem> lstdocumentType = new List<ListItem>();

            var lstAllDocTypes = ContractMastersManagement.GetContractDocTypes_All(customerID);
            if (lstAllDocTypes.Count > 0)
            {
                lstAllDocTypes = lstAllDocTypes.OrderBy(row => row.TypeName).ToList();

                for (int i = 0; i < lstAllDocTypes.Count; i++)
                {
                    lstdocumentType.Add(new ListItem
                    {
                        Value = lstAllDocTypes.ToList()[i].ID.ToString(),
                        Text = lstAllDocTypes.ToList()[i].TypeName,
                    });
                }

                //lstdocumentType.Add(new ListItem
                //{
                //    Value = "0",
                //    Text = "Add New",
                //});
            }

            return lstdocumentType;
        }

        [WebMethod]
        public static List<ListItem> GetCustomFields(string selectedContractType)
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            long typeID = Convert.ToInt64(selectedContractType);

            List<ListItem> lstCustomFields = new List<ListItem>();

            var lstAllCustomFields = ContractMastersManagement.GetCustomFields_All(customerID, typeID);

            if (lstAllCustomFields.Count > 0)
            {
                lstAllCustomFields = lstAllCustomFields.OrderBy(row => row.Label).ToList();

                for (int i = 0; i < lstAllCustomFields.Count; i++)
                {
                    lstCustomFields.Add(new ListItem
                    {
                        Value = lstAllCustomFields.ToList()[i].ID.ToString(),
                        Text = lstAllCustomFields.ToList()[i].Label,
                    });
                }

                //lstCustomFields.Add(new ListItem
                //{
                //    Value = "0",
                //    Text = "Add New",
                //});
            }

            return lstCustomFields;
        }
        public void BindPaymentTerms()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            int CacheClearTime = Convert.ToInt32(ConfigurationManager.AppSettings["CacheClearMasterData"]);
            var key = "Contpayterm-" + Convert.ToString(customerID);
            var lstpaymentterm = (List<Cont_tbl_PaymentTerm>)HttpContext.Current.Cache[key];
            if (HttpContext.Current.Cache[key] == null)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    lstpaymentterm = (from row in entities.Cont_tbl_PaymentTerm
                                  where row.IsDeleted == false
                                  select row).ToList();                    
                }
                HttpContext.Current.Cache.Insert(key, lstpaymentterm, null, DateTime.Now.AddMinutes(CacheClearTime), TimeSpan.Zero); // add it to cache
            }

            if (lstpaymentterm.Count > 0)
                lstpaymentterm = lstpaymentterm.OrderBy(row => row.Id).ToList();
            
            ddlPaymentTerm1.DataTextField = "PaymentTermName";
            ddlPaymentTerm1.DataValueField = "PaymentTermID";

            ddlPaymentTerm1.DataSource = lstpaymentterm;
            ddlPaymentTerm1.DataBind();
        }
        public void BindVendors()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            int CacheClearTime = Convert.ToInt32(ConfigurationManager.AppSettings["CacheClearMasterData"]);
            var key = "Contvender-" + Convert.ToString(customerID);
            var lstVendors = (List<Cont_tbl_VendorMaster>)HttpContext.Current.Cache[key];
            if (HttpContext.Current.Cache[key] == null)
            {
                lstVendors = VendorDetails.GetVendors_All(customerID);
                HttpContext.Current.Cache.Insert(key, lstVendors, null, DateTime.Now.AddMinutes(CacheClearTime), TimeSpan.Zero); // add it to cache
            }

            if (lstVendors.Count > 0)
                lstVendors = lstVendors.OrderBy(row => row.VendorName).ToList();

            //Drop-Down at Modal Pop-up
            lstBoxVendor.DataTextField = "VendorName";
            lstBoxVendor.DataValueField = "ID";

            lstBoxVendor.DataSource = lstVendors;
            lstBoxVendor.DataBind();

            //lstBoxVendor.Items.Add(new ListItem("Add New", "0"));
        }

        protected void lnkBtnRebind_Vendor_Click(object sender, EventArgs e)
        {
            var key = "Contvender-" + Convert.ToString(AuthenticationHelper.CustomerID);
            CacheHelper.Remove(key);
            BindVendors();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "rebindVendors", "restoreSelectedVendors();", true);
        }

        public void BindStatus(string RoleID)
        {
            try
            {
                ddlstatus.DataSource = null;
                ddlstatus.DataBind();
                ddlstatus.ClearSelection();
                
                ddlstatus.DataTextField = "Name";
                ddlstatus.DataValueField = "ID";

                var statusList = ContractManagement.GetStatusList(RoleID);
                
                ddlstatus.DataSource = statusList;
                ddlstatus.DataBind();            
            }
            catch(Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void BindUsers()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                int CacheClearTime = Convert.ToInt32(ConfigurationManager.AppSettings["CacheClearMasterData"]);
                var key = "Contusers-" + Convert.ToString(customerID);
                var lstAllUsers = (List<User>)HttpContext.Current.Cache[key];
                if (HttpContext.Current.Cache[key] == null)
                {
                    lstAllUsers = ContractUserManagement.GetAllUsers_Contract(customerID);
                    HttpContext.Current.Cache.Insert(key, lstAllUsers, null, DateTime.Now.AddMinutes(CacheClearTime), TimeSpan.Zero); // add it to cache
                }

                if (lstAllUsers.Count > 0)
                    lstAllUsers = lstAllUsers.Where(entry => entry.ContractRoleID != null).ToList();

                var internalUsers = ContractUserManagement.GetRequiredUsers(lstAllUsers, 1); //1--Internal---

                lstBoxOwner.DataValueField = "ID";
                lstBoxOwner.DataTextField = "Name";
                lstBoxOwner.DataSource = internalUsers;
                lstBoxOwner.DataBind();

                lstBoxApprover.DataValueField = "ID";
                lstBoxApprover.DataTextField = "Name";
                lstBoxApprover.DataSource = internalUsers;
                lstBoxApprover.DataBind();
                
                ddlReviewer.DataValueField = "ID";
                ddlReviewer.DataTextField = "Name";
                ddlReviewer.DataSource = internalUsers;
                ddlReviewer.DataBind();

                ddlApprover.DataValueField = "ID";
                ddlApprover.DataTextField = "Name";
                ddlApprover.DataSource = internalUsers;
                ddlApprover.DataBind();

                var allUsers = ContractUserManagement.GetRequiredUsers(lstAllUsers, 4); //4--All---Internal and External Users

                ddlCPDepartment.DataValueField = "ID";
                ddlCPDepartment.DataTextField = "Name";
                ddlCPDepartment.DataSource = allUsers;
                ddlCPDepartment.DataBind();

                lstBoxTaskUser.DataValueField = "ID";
                lstBoxTaskUser.DataTextField = "Name";
                lstBoxTaskUser.DataSource = allUsers;
                lstBoxTaskUser.DataBind();

                //lstBoxTaskUser.Items.Add(new ListItem("Add New", "0"));

                ddlUsers_AuditLog.DataValueField = "ID";
                ddlUsers_AuditLog.DataTextField = "Name";
                ddlUsers_AuditLog.DataSource = allUsers;
                ddlUsers_AuditLog.DataBind();

                lstAllUsers.Clear();
                lstAllUsers = null;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindTaskUsers()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                var lstAllUsers = ContractUserManagement.GetAllUsers_Contract(customerID);

                if (lstAllUsers.Count > 0)
                    lstAllUsers = lstAllUsers.Where(entry => entry.ContractRoleID != null).ToList();

                var allUsers = ContractUserManagement.GetRequiredUsers(lstAllUsers, 4); //4--All---Internal and External Users

                lstBoxTaskUser.DataValueField = "ID";
                lstBoxTaskUser.DataTextField = "Name";
                lstBoxTaskUser.DataSource = allUsers;
                lstBoxTaskUser.DataBind();

                //lstBoxTaskUser.Items.Add(new ListItem("Add New", "0"));

                lstAllUsers.Clear();
                lstAllUsers = null;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lnkBtnRebind_TaskUser_Click(object sender, EventArgs e)
        {
            BindTaskUsers();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "rebindTaskUser", "restoreSelectedTaskUsers();", true);
        }

        public void BindContractAuditLogs()
        {
            try
            {
                long totalRowCount = 0;

                if (ViewState["ContractInstanceID"] != null)
                {
                    int customerID = -1;
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                    int userID = -1;
                    if (!(string.IsNullOrEmpty(ddlUsers_AuditLog.SelectedValue)))
                    {
                        userID = Convert.ToInt32(ddlUsers_AuditLog.SelectedValue);
                    }

                    string fromDate = string.Empty;
                    string toDate = string.Empty;

                    long contractInstanceID = Convert.ToInt64(ViewState["ContractInstanceID"]);

                    if (contractInstanceID != 0)
                    {
                        var lstAuditLogs = ContractManagement.GetContractAuditLogs_All(customerID, contractInstanceID);

                        if (userID != -1 && userID != 0)
                            lstAuditLogs = lstAuditLogs.Where(row => row.CreatedBy == userID).ToList();

                        if (!(string.IsNullOrEmpty(txtFromDate_AuditLog.Text.Trim())))
                        {
                            try
                            {
                                fromDate = txtFromDate_AuditLog.Text.Trim();
                                lstAuditLogs = lstAuditLogs.Where(row => row.CreatedOn >= Convert.ToDateTime(fromDate)).ToList();
                            }
                            catch (Exception ex)
                            {
                                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                            }
                        }

                        if (!(string.IsNullOrEmpty(txtToDate_AuditLog.Text.Trim())))
                        {
                            try
                            {
                                toDate = txtToDate_AuditLog.Text.Trim();
                                lstAuditLogs = lstAuditLogs.Where(row => row.CreatedOn <= Convert.ToDateTime(toDate)).ToList();
                            }
                            catch (Exception)
                            {

                            }
                        }

                        divAuditLog_Vertical.InnerHtml = ShowAuditLog_Centered_Clickable(lstAuditLogs);
                        if (lstAuditLogs.Count > 0)
                        {
                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "EmptyAuditlog();", true);
                        }


                        //divAuditLog_Vertical.InnerHtml = ShowAuditLog_Clickable(lstAuditLogs);

                        //divAuditLog.InnerHtml = ShowAuditLog_Timeline(lstAuditLogs);
                        //divAuditLog.InnerHtml = ShowAuditLog_VerticalTimeline(lstAuditLogs);

                        //int PageNumber = 1;
                        //if (!(string.IsNullOrEmpty(ddlPageNo_AuditLog.SelectedValue)))
                        //{
                        //    PageNumber = Convert.ToInt32(ddlPageNo_AuditLog.SelectedValue);
                        //}

                        //var lstAuditLogs = ContractManagement.GetContractAuditLogs_Paging(customerID, contractInstanceID, Convert.ToInt32(gvContractAuditLog.PageSize), PageNumber);

                        //gvContractAuditLog.DataSource = lstAuditLogs;
                        //gvContractAuditLog.DataBind();

                        //if (lstAuditLogs.Count > 0)
                        //{
                        //    if (lstAuditLogs[0].TotalRowCount != null)
                        //        totalRowCount = Convert.ToInt64(lstAuditLogs[0].TotalRowCount);
                        //    else
                        //    {
                        //        foreach (var item in lstAuditLogs)
                        //        {
                        //            totalRowCount = Convert.ToInt32(item.TotalRowCount);
                        //            break;
                        //        }
                        //    }
                        //}
                    }
                    else
                    {
                        gvContractAuditLog.DataSource = null;
                        gvContractAuditLog.DataBind();
                    }
                }
                else
                {
                    gvContractAuditLog.DataSource = null;
                    gvContractAuditLog.DataBind();
                }

                Session["TotalRows"] = totalRowCount;
                //lblStartRecord_AuditLog.Text = ddlPageNo_AuditLog.SelectedValue;

                if (Convert.ToInt32(ViewState["PageNumberAuditLogFlagID"]) == 0)
                {
                    bindPageNumber(3);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindContractHistory(int customerID, long contractInstanceID)
        {
            if (contractInstanceID != 0)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var lstContractHistory = (from row in entities.Cont_SP_ContractHistory(contractInstanceID, customerID)
                                              select row).ToList();

                    if (lstContractHistory != null)
                    {
                        grdContractHistory.DataSource = lstContractHistory;
                        grdContractHistory.DataBind();

                        if (lstContractHistory.Count > 0)
                            divContractHistory.Visible = true;
                        else
                            divContractHistory.Visible = false;
                    }
                }
            }
            else
                divContractHistory.Visible = false;
        }

        private void HideShowGridColumns(GridView gridView, string headerTextToMatch, bool flag)
        {
            try
            {
                if (gridView == null)
                {
                    return;
                }

                // Loop through all of the columns in the grid.
                for (int i = 0; i < gridView.Columns.Count; i++)
                {
                    String headerText = gridView.Columns[i].HeaderText;

                    //Show Hide Columns with Specific headerText
                    if (headerText == headerTextToMatch)
                        gridView.Columns[i].Visible = flag;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindContractDetails()
        {
            try
            {
                if (ViewState["ContractInstanceID"] != null)
                {
                    int customerID = -1;
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                    long contractID = Convert.ToInt64(ViewState["ContractInstanceID"]);
                    var ExistingRecord = ContractManagement.ExistsContractDetails(contractID, customerID);

                    if (ExistingRecord != null)
                    {
                        txtTerms.Text = ExistingRecord.Terms.ToString();
                        txtTermination.Text = ExistingRecord.Termination.ToString();
                        txtMisc.Text = ExistingRecord.Misc.ToString();
                        txtLicense.Text = ExistingRecord.License.ToString();
                        txtWarranties.Text = ExistingRecord.Warranties.ToString();
                        txtGuarantees.Text = ExistingRecord.Guarantees.ToString();
                        txtConfidential.Text = ExistingRecord.Confidential.ToString();
                        txtOwnership.Text = ExistingRecord.Ownership.ToString();
                        txtIndemnification.Text = ExistingRecord.Indemnification.ToString();
                        txtGeneral.Text = ExistingRecord.General.ToString();
                        txtTimelines.Text = ExistingRecord.Timelines.ToString();
                        txtEscalation.Text = ExistingRecord.Escalation.ToString();
                        txtMilestones.Text = ExistingRecord.Milestone.ToString();
                        txtOthers.Text = ExistingRecord.Others.ToString();
                    }
                    else
                    {
                        clearContractDetailsControls();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void TabContract_Click(object sender, EventArgs e)
        {
            ViewState["PageLink"] = "ContractLink";
            liContractSummary.Attributes.Add("class", "active");
            liContractDetails.Attributes.Add("class", "");
            liDocument.Attributes.Add("class", "");
            liTask.Attributes.Add("class", "");
            liAuditLog.Attributes.Add("class", "");
            liContractTransaction.Attributes.Add("class", "");
            liMilestone.Attributes.Add("class", "");
            MainView.ActiveViewIndex = 0;
            long ContractStatusID = Convert.ToInt64(ViewState["ContractStatusID"]);
            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["ContractInstanceID"])))
            {
                int ContractID = Convert.ToInt32(ViewState["ContractInstanceID"]);
                BindContractListToLink(ContractID);

                long customerID = -1;
                customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;

                var getContractStatus = ContractManagement.GetContractStatus(ContractID, Convert.ToInt32(customerID));

                if (getContractStatus != null && getContractStatus.Count > 0)
                {
                    ddlContractStatus.ClearSelection();
                    if (ddlContractStatus.Items.FindByValue(getContractStatus[0].ContractStatusID.ToString()) != null)
                        ddlContractStatus.Items.FindByValue(getContractStatus[0].ContractStatusID.ToString()).Selected = true;
                }
            }

            if (ViewState["Mode"] != null)
            {
                if (Convert.ToInt32(ViewState["Mode"]) == 0)
                {
                    enableDisableContractSummaryTabControls(true);
                    toggleTextSaveButton_ContractSummaryTab(true);

                }
                else if (Convert.ToInt32(ViewState["Mode"]) == 1)
                {
                    bool historyFlag = false;
                    if (ViewState["FlagHistory"] != null)
                    {
                        historyFlag = Convert.ToBoolean(Convert.ToInt32(ViewState["FlagHistory"]));
                    }

                    enableDisableContractSummaryTabControls(false);
                    toggleTextSaveButton_ContractSummaryTab(false);

                    if (historyFlag)
                        showHideContractSummaryTabTopButtons(false);
                    else
                        showHideContractSummaryTabTopButtons(true);
                    if(ContractStatusID == 8 || ContractStatusID == 23)
                    {
                        btnEditContractDetail.Visible = false;
                    }
                }
            }
            
        }

        protected void TabContractDetail_Click(object sender, EventArgs e)
        {
            ViewState["PageLink"] = "ContractLink";
            showHideContractSummaryTabTopButtons(false);

            liContractSummary.Attributes.Add("class", "");
            liContractDetails.Attributes.Add("class", "active");
            liDocument.Attributes.Add("class", "");
            liTask.Attributes.Add("class", "");
            liAuditLog.Attributes.Add("class", "");
            liContractTransaction.Attributes.Add("class", "");
            liMilestone.Attributes.Add("class", "");
            MainView.ActiveViewIndex = 1;

            clearContractDetailsControls();
            BindContractDetails();
            BindContractListToLink(Convert.ToInt32(ViewState["ContractInstanceID"]));
        }

        protected void TabDocument_Click(object sender, EventArgs e)
        {
            ViewState["PageLink"] = "DocumentLink";
            showHideContractSummaryTabTopButtons(false);
            ViewState["PageNumberFlagID"] = 0;
            DropDownListPageNo.ClearSelection();

            liContractSummary.Attributes.Add("class", "");
            liContractDetails.Attributes.Add("class", "");
            liDocument.Attributes.Add("class", "active");
            liTask.Attributes.Add("class", "");
            liAuditLog.Attributes.Add("class", "");
            liContractTransaction.Attributes.Add("class", "");
            liMilestone.Attributes.Add("class", "");
            MainView.ActiveViewIndex = 2;

            //BindContractDocType();

            BindFileTags();
            BindContractDocuments_Paging();
            BindGrid(Convert.ToInt32(ViewState["ContractInstanceID"]));

            int ContractID = Convert.ToInt32(ViewState["ContractInstanceID"]);

            long customerID = -1;
            customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;

            var getContractStatus = ContractManagement.GetContractStatus(ContractID, Convert.ToInt32(customerID));

            if (ActiveContractsApprovals && getContractStatus[0].ContractStatusID.ToString() == "7" && AuthenticationHelper.Role != "MGMT" && AuthenticationHelper.Role != "CADMN")
            {
                string CheckStatus = CaseManagement.IFUserEnable(Convert.ToInt32(AuthenticationHelper.UserID), contID);

                if (CheckStatus == "Approved")
                {
                    DocViewResult = "1";
                }
            }
        }

        protected void TabTask_Click(object sender, EventArgs e)
        {
            ViewState["PageLink"] = "ContractLink";
            showHideContractSummaryTabTopButtons(false);

            liContractSummary.Attributes.Add("class", "");
            liContractDetails.Attributes.Add("class", "");
            liDocument.Attributes.Add("class", "");
            liTask.Attributes.Add("class", "active");
            liAuditLog.Attributes.Add("class", "");
            liContractTransaction.Attributes.Add("class", "");
            liMilestone.Attributes.Add("class", "");
            MainView.ActiveViewIndex = 3;

            ViewState["TaskMode"] = "0";

            ViewState["TaskPageFlag"] = "0";

            if (ViewState["ContractInstanceID"] != null)
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                long contractInstanceID = Convert.ToInt64(ViewState["ContractInstanceID"]);

                if (contractInstanceID != 0)
                {
                    BindContractTasks_Paging(customerID, contractInstanceID, Convert.ToInt32(grdTaskActivity.PageSize), Convert.ToInt32(grdTaskActivity.PageIndex) + 1, grdTaskActivity);
                }
                BindContractListToLink(Convert.ToInt32(ViewState["ContractInstanceID"]));

                var getContractStatus = ContractManagement.GetContractStatus(contractInstanceID, Convert.ToInt32(customerID));

                if (ActiveContractsApprovals && getContractStatus[0].ContractStatusID.ToString() == "7" && AuthenticationHelper.Role != "MGMT" && AuthenticationHelper.Role != "CADMN")
                {
                    string CheckStatus = CaseManagement.IFUserEnable(Convert.ToInt32(AuthenticationHelper.UserID), contID);

                    if (CheckStatus == "Approved")
                    {
                        DocViewResult = "1";
                    }
                }
            }

            HideShowTaskDiv(false);
        }

        protected void TabAuditLog_Click(object sender, EventArgs e)
        {
            ViewState["PageLink"] = "ContractLink";
            showHideContractSummaryTabTopButtons(false);

            liContractSummary.Attributes.Add("class", "");
            liContractDetails.Attributes.Add("class", "");
            liDocument.Attributes.Add("class", "");
            liTask.Attributes.Add("class", "");
            liAuditLog.Attributes.Add("class", "active");
            liContractTransaction.Attributes.Add("class", "");
            liMilestone.Attributes.Add("class", "");
            ViewState["PageNumberAuditLogFlagID"] = 0;

            MainView.ActiveViewIndex = 4;

            if (ViewState["ContractInstanceID"] != null)
            {
                BindContractAuditLogs();
            }
            BindContractListToLink(Convert.ToInt32(ViewState["ContractInstanceID"]));
        }

        public void setContractNumber()
        {
            try
            {
                var ConIniID = Request.QueryString["ConAccessID"];
                int coninid = Convert.ToInt32(ConIniID);
                ShowContractInitiator = CaseManagement.CheckForClientNew(Convert.ToInt32(AuthenticationHelper.CustomerID), "ShowContractInitiator");
                customizedid = CustomerManagement.GetCustomizedCustomerid(AuthenticationHelper.CustomerID, "ContractSubTypeWiseContractNo");
                if (customizedid == AuthenticationHelper.CustomerID)
                {
                    
                    if (!string.IsNullOrEmpty(ddlContractType.SelectedValue) && !string.IsNullOrEmpty(ddlContractSubType.SelectedValue)
                       && !string.IsNullOrEmpty(tbxBranch.Text))
                    {
                        var branchid=Convert.ToInt32(tvBranches.SelectedValue);
                        var contyid = Convert.ToInt32(ddlContractType.SelectedValue);
                        var subcontypeid= Convert.ToInt32(ddlContractSubType.SelectedValue);                     
                        txtContractNo.Text= VendorDetails.GetContractNumber(AuthenticationHelper.CustomerID, branchid, contyid, subcontypeid);                      
                    }
                }
                else if (ShowContractInitiator == true)
                {
                    if(coninid!=0)
                    {
                        string contractnum = GetcontractNum(coninid);
                        txtContractNo.Text = contractnum;
                    }
                        
                }
                 else
                {
                    string dateyear = DateTime.Now.Day + "" + DateTime.Now.Month + "" + DateTime.Now.Year;
                    if (!string.IsNullOrEmpty(ddlContractType.SelectedValue)
                        && !string.IsNullOrEmpty(tbxBranch.Text))
                    {
                        txtContractNo.Text = ddlContractType.SelectedItem.Text.Substring(0, 2) + "-" + tbxBranch.Text.Substring(0, 2) + "-" + dateyear;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvContractPopUp.IsValid = false;
            }      
        }

       public void GetTermSheetdata()
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var ConIniID = Request.QueryString["ConAccessID"];
                    int coninid = Convert.ToInt32(ConIniID);

                    var data = (from row in entities.tbl_Contract_TermSheet
                                where row.ID == coninid
                                select row).FirstOrDefault();

                    if (ShowContractInitiator == true)
                    {
                        if (coninid != 0)
                        {
                            string contractnum = GetcontractNum(coninid);
                            txtContractNo.Text = contractnum;
                            ddlDepartment.SelectedValue = data.departmentid.ToString();

                            if(data.Startdate !=null)
                            {
                                txtEffectiveDate.Text = Convert.ToDateTime(data.Startdate).ToString("dd-MM-yyyy");
                            }
                            else
                            {
                                txtEffectiveDate.Text = string.Empty;
                            }
                            if (data.Enddate != null)
                            {
                                txtExpirationDate.Text = Convert.ToDateTime(data.Enddate).ToString("dd-MM-yyyy");
                            }
                            else
                            {
                                txtExpirationDate.Text=string.Empty;
                            }
                             
                            if(data.PaymentTermid!=null)
                            {
                                if(data.PaymentTermid == 1)
                                {
                                    ddlPaymentTerm1.SelectedValue = "12";
                                }
                                else if(data.PaymentTermid == 2)
                                {
                                    ddlPaymentTerm1.SelectedValue = "30";
                                }
                                else if (data.PaymentTermid == 3)
                                {
                                    ddlPaymentTerm1.SelectedValue = "7";
                                }
                            }
                            
                            lstBoxVendor.SelectedValue = data.VendorId.ToString();



                        if (data.branchid != 0)
                        {
                            foreach (TreeNode node in tvBranches.Nodes)
                            {
                                if (node.Value == data.branchid.ToString())
                                {
                                    node.Selected = true;
                                }
                                foreach (TreeNode item1 in node.ChildNodes)
                                {
                                    //if (item1.Value == contractRecord.CustomerBranchID.ToString())
                                    //    item1.Selected = true;

                                    if (item1.Value == data.branchid.ToString())
                                    {
                                        item1.Selected = true;
                                    }
                                    foreach (TreeNode item2 in item1.ChildNodes)
                                    {
                                        if (item2.Value == data.branchid.ToString())
                                        {
                                            item2.Selected = true;
                                        }
                                        foreach (TreeNode item3 in item2.ChildNodes)
                                        {
                                            if (item3.Value == data.branchid.ToString())
                                            {
                                                item3.Selected = true;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                            tvBranches_SelectedNodeChanged(null, null);

                        }

                    }
                }
                    
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvContractPopUp.IsValid = false;
            }
        }
        protected void tvBranches_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxBranch.Text = tvBranches.SelectedNode != null ? tvBranches.SelectedNode.Text : "Select Entity/Location";
                //bool ShowContractInitiator = CaseManagement.CheckForClientNew(Convert.ToInt32(AuthenticationHelper.CustomerID), "ShowContractInitiator");
                //string coninstanceid = Request.QueryString["AccessID"];
                //if (ShowContractInitiator == false)
                //{
                setContractNumber();
                //}


                ScriptManager.RegisterStartupScript(this, this.GetType(), "hideDivTree", "hideDivBranch();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvContractPopUp.IsValid = false;
                cvContractPopUp.ErrorMessage = "Server Error Occurred. Please try again.";
                cvContractPopUp.CssClass = "alert alert-danger";
            }
        }

        protected void ddlContractType_SelectedIndexChanged(object sender, EventArgs e)
        {
            bool bindSuccess = false;

            if (ddlContractType.SelectedValue != null && ddlContractType.SelectedValue != "0" && ddlContractType.SelectedValue != "")
            {
                ViewState["ddlCustomFieldFilled"] = null;
                ViewState["dataTableCustomFields"] = null;

                ViewState["CustomefieldCount"] = null;

                ViewState["ContractTypeIDUpdated"] = "false";

                bindSuccess = BindContractSubType(Convert.ToInt64(ddlContractType.SelectedValue));

                lnkAddNewContractSubTypeModal.Visible = true;

                Session["ContractTypeID"] = ddlContractType.SelectedValue;

                BindCustomFields(grdCustomField); //, grdCustomField_History
            }

            if (!bindSuccess)
            {
                if (ddlContractSubType.Items.Count > 0)
                    ddlContractSubType.Items.Clear();

                if (ddlContractType.SelectedValue != null && ddlContractType.SelectedValue != "0" && ddlContractType.SelectedValue != "")
                {
                    lnkAddNewContractSubTypeModal.Visible = true;
                    //ddlContractSubType.Items.Add(new ListItem("Add New", "0"));
                }
                else
                    lnkAddNewContractSubTypeModal.Visible = false;
            }
            //setContractNumber();
            upContractTypeSubType.Update();
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "changeContractType", "ddlContractTypeChange(); ddlCustomFieldChange();", true);
        }

        public void showErrorMessages(List<string> lstErrMsgs, CustomValidator cvtoShowErrorMsg)
        {
            //string finalErrMsg = string.Join("<br/>", emsg.ToArray());

            string finalErrMsg = string.Empty;

            finalErrMsg += "<ol type='1'>";

            if (lstErrMsgs.Count > 0)
            {
                lstErrMsgs.ForEach(eachErrMsg =>
                {
                    finalErrMsg += "<li>" + eachErrMsg + "</li>";
                });

                finalErrMsg += "</ol>";
            }

            cvtoShowErrorMsg.IsValid = false;
            cvtoShowErrorMsg.ErrorMessage = finalErrMsg;
        }

        #endregion

        #region Contract Detail

        protected void btnClearContractControls_Click(object sender, EventArgs e)
        {
            try
            {
                clearContractControls();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void clearContractControls()
        {
            try
            {
                txtContractNo.Text = "";
                txtTitle.Text = "";
                tbxDescription.Text = "";

                tbxBranch.Text = "";
                tbxBranch.Text = "Select Entity/Location";
                lstBoxVendor.ClearSelection();
                ddlDepartment.ClearSelection();
                ddlCPDepartment.ClearSelection();

                txtProposalDate.Text = "";
                txtAgreementDate.Text = "";
                txtEffectiveDate.Text = "";

                txtReviewDate.Text = "";
                txtExpirationDate.Text = "";
                txtNoticeTerm.Text = "";
                txtlockinperiodDate.Text = "";
                tbxTaxes.Text = "";
                ddlContractType.ClearSelection();
                ddlContractSubType.ClearSelection();
                lstBoxOwner.ClearSelection();
                lstBoxApprover.ClearSelection();
                lstBoxUsers.ClearSelection();

                tbxContractAmt.Text = "";
                ddlPaymentTerm1.ClearSelection();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void clearContractDetailsControls()
        {
            txtTerms.Text = "";
            txtTermination.Text = "";
            txtMisc.Text = "";
            txtLicense.Text = "";
            txtWarranties.Text = "";
            txtGuarantees.Text = "";
            txtConfidential.Text = "";
            txtOwnership.Text = "";
            txtIndemnification.Text = "";
            txtGeneral.Text = "";
            txtTimelines.Text = "";
            txtEscalation.Text = "";
            txtMilestones.Text = "";
        }

        public void enableDisableContractDetailTabControls()
        {
          
            txtTerms.Enabled=false;
            txtTermination.Enabled = false;
           // txtTermination.Attributes.Remove("disabled");
            txtTerms.Enabled = false;
            txtMisc.Enabled = false;
            txtLicense.Enabled = false;
            txtWarranties.Enabled = false;
            txtGuarantees.Enabled = false;
            txtConfidential.Enabled = false;
            txtOwnership.Enabled = false;
            txtIndemnification.Enabled = false;
            txtGeneral.Enabled = false;
            txtTimelines.Enabled = false;
            txtEscalation.Enabled = false;
            txtMilestones.Enabled = false;
            txtOthers.Enabled = false;
            btnContractDetail.Enabled = false;

        }
        public void enableDisableDocumentsTabControls()
        {

            lnkBtnUploadDocument.Enabled = false;
            lnkBtn_RebindContractDoc.Enabled = false;
            lnkBtnUploadDocument.Attributes.Remove("disabled");
            lnkLinkContractDoc.Enabled = false;
            lnklinktotermsheet.Enabled = false;
            grdContractDocuments.Enabled = false;
        }
        public void enableDisableContractMileStoneTabControls ()
        {
            gridMilestone.Enabled = false;
            drpdownmilestone.Enabled=false;
            dropmilestonestatus.Enabled=false;  
            btnapplymilestone.Enabled = false;
            Button1.Enabled = false;
            btnExport.Enabled = false;
        }
        public void enableDisableSummaryTab()
        {
            ddlContractStatus.Enabled = false;

            txtContractNo.Enabled = false;
            txtTitle.Enabled = false;
            tbxDescription.Enabled = false;

            tbxBranch.Enabled = false;
            lstBoxVendor.Enabled = false;
            ddlDepartment.Enabled = false;

            ddlCPDepartment.Enabled = false;

            tbxAddNewClause.Enabled = false;
            txtProposalDate.Enabled = false;
            txtAgreementDate.Enabled = false;
            txtEffectiveDate.Enabled = false;

            txtReviewDate.Enabled = false;
            txtExpirationDate.Enabled = false;

            txtlockinperiodDate.Enabled = false;
            tbxTaxes.Enabled = false;

            txtNoticeTerm.Enabled = false;
            ddlNoticeTerm.Enabled = false;

            ddlPaymentType.Enabled = false;

            tbxContractAmt.Enabled = false;
            ddlPaymentTerm1.Enabled = false;

            lstBoxVendor.Enabled = false;
            ddlDepartment.Enabled = false;

            ddlContractType.Enabled = false;
            ddlContractSubType.Enabled = false;
            lstBoxOwner.Enabled = false;
            lstBoxApprover.Enabled = false;

            txtBoxProduct.Enabled = false;
        }
        public void enableDisableContractSummaryTabControls(bool flag)
        {
            try
            {
                string customer = ConfigurationManager.AppSettings["Statusreviewed"].ToString();

                long customerID = AuthenticationHelper.CustomerID;
                List<string> CustomerList = customer.Split(',').ToList();
                if (CustomerList.Count > 0)
                {
                    foreach (string PList in CustomerList)
                    {
                        if (PList == customerID.ToString())
                        {
                            if (ddlContractStatus.SelectedValue == "3")
                            {
                                flag = false;
                                break;
                            }
                        }
                    }
                }

                ddlContractStatus.Enabled = flag;

                txtContractNo.Enabled = flag;
                txtTitle.Enabled = flag;
                tbxDescription.Enabled = flag;

                tbxBranch.Enabled = flag;
                lstBoxVendor.Enabled = flag;
                ddlDepartment.Enabled = flag;

                ddlCPDepartment.Enabled = flag;

                tbxAddNewClause.Enabled = flag;
                txtProposalDate.Enabled = flag;
                txtAgreementDate.Enabled = flag;
                txtEffectiveDate.Enabled = flag;

                txtReviewDate.Enabled = flag;
                txtExpirationDate.Enabled = flag;

                txtlockinperiodDate.Enabled = flag;
                tbxTaxes.Enabled = flag;

                txtNoticeTerm.Enabled = flag;
                ddlNoticeTerm.Enabled = flag;

                ddlPaymentType.Enabled = flag;

                tbxContractAmt.Enabled = flag;
                ddlPaymentTerm1.Enabled = flag;

                lstBoxVendor.Enabled = flag;
                ddlDepartment.Enabled = flag;

                ddlContractType.Enabled = flag;
                ddlContractSubType.Enabled = flag;
                lstBoxOwner.Enabled = flag;
                lstBoxApprover.Enabled = flag;

                txtBoxProduct.Enabled = flag;

                //lnkShowAddNewVendorModal.Visible = flag;
                //lnkAddNewDepartmentModal.Visible = flag;
                //lnkAddNewContractTypeModal.Visible = flag;
                //lnkAddNewContractSubTypeModal.Visible = flag;

                if (flag)
                {
                    ddlDepartment.Attributes.Remove("disabled");
                    ddlCPDepartment.Attributes.Remove("disabled");
                    ddlNoticeTerm.Attributes.Remove("disabled");
                    ddlContractType.Attributes.Remove("disabled");
                    ddlContractSubType.Attributes.Remove("disabled");

                    lstBoxVendor.Attributes.Remove("disabled");
                    lstBoxOwner.Attributes.Remove("disabled");
                    lstBoxApprover.Attributes.Remove("disabled");

                    btnSaveContract.Attributes.Remove("disabled");
                    btnClearContractDetail.Attributes.Remove("disabled");

                    lnkShowAddNewVendorModal.Enabled = true;
                    lnkAddNewPDepartmentModal.Enabled = true;
                    lnkAddNewDepartmentModal.Enabled = true;
                    lnkAddNewContractTypeModal.Enabled = true;
                    lnkAddNewContractSubTypeModal.Enabled = true;
                }
                else
                {
                    ddlDepartment.Attributes.Add("disabled", "disabled");
                    ddlCPDepartment.Attributes.Add("disabled", "disabled");
                    ddlNoticeTerm.Attributes.Add("disabled", "disabled");
                    ddlContractType.Attributes.Add("disabled", "disabled");
                    ddlContractSubType.Attributes.Add("disabled", "disabled");

                    lstBoxOwner.Attributes.Add("disabled", "disabled");
                    lstBoxVendor.Attributes.Add("disabled", "disabled");
                    lstBoxApprover.Attributes.Add("disabled", "disabled");

                    btnSaveContract.Attributes.Add("disabled", "disabled");
                    btnClearContractDetail.Attributes.Add("disabled", "disabled");

                    lnkShowAddNewVendorModal.Enabled = false;
                    lnkAddNewDepartmentModal.Enabled = false;
                    lnkAddNewPDepartmentModal.Enabled = false;
                    lnkAddNewContractTypeModal.Enabled = false;
                    lnkAddNewContractSubTypeModal.Enabled = false;
                }

                if (grdCustomField != null)
                {
                    grdCustomField.Enabled = flag;
                    HideShowGridColumns(grdCustomField, "Action", flag);
                }
                uploaddocsection.Visible = false;
                statusandcommentsection.Visible = false;
                bool IsPerformercontract = ContractManagement.getcontractchecklist(Convert.ToInt32(AuthenticationHelper.CustomerID));
                if (IsPerformercontract)
                {
                    ddlContractStatus.Enabled = false;
                    if (ViewState["ContractInstanceID"] != null)
                    {
                        long contractInstanceID = Convert.ToInt64(ViewState["ContractInstanceID"]);
                        if (contractInstanceID > 0)
                        {
                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                            {
                                var getrecord = (from row in entities.Cont_tbl_ContractInstance
                                                 where row.ID == contractInstanceID
                                                 && row.IsDeleted == false
                                                 && row.IsReviewedContract == 0
                                                 select row).FirstOrDefault();
                                if (getrecord != null)
                                {
                                    ddlReviewer.Enabled = false;
                                    ddlApprover.Enabled = false;
                                    //btnEditContractDetail.Visible = false;
                                    lnksharesummary.Visible = false;
                                    lnkSendMailWithDoc.Visible = false;
                                    lnkLinkContract.Visible = false;
                                    lnklinktotermsheet.Visible = false;
                                    lblCustomField.Visible = false;
                                    grdCustomField.Visible = false;
                                    liContractDetails.Visible = false;
                                    liDocument.Visible = false;
                                    liTask.Visible = false;
                                    liAuditLog.Visible = false;
                                    liContractTransaction.Visible = true;
                                    //liContractTransaction.Visible = false;
                                    liMilestone.Visible = false;
                                    if (!string.IsNullOrEmpty(Request.QueryString["AccessRoleID"]))
                                    {
                                        if (Convert.ToInt32(Request.QueryString["AccessRoleID"]) == 4 || Convert.ToInt32(Request.QueryString["AccessRoleID"]) == 6)
                                        {
                                            statusandcommentsection.Visible = true;
                                            uploaddocsection.Visible = true;
                                        }
                                        if (Convert.ToInt32(Request.QueryString["AccessRoleID"]) == 38)
                                        {
                                            statusandcommentsection.Visible = true;
                                        }
                                    }
                                    else
                                    {
                                        var ContractCurrentStatus = (from row in entities.Cont_View_RecentContractReviweStatusTransaction
                                                                     where row.ContractID == contractInstanceID
                                                                     select row).FirstOrDefault();
                                        if (ContractCurrentStatus.ContractStatusID == 1)
                                        {
                                            statusandcommentsection.Visible = true;
                                            uploaddocsection.Visible = true;
                                            ddlReviewer.Enabled = true;
                                            ddlApprover.Enabled = true;
                                            lnksharesummary.Visible = false;
                                            lnkSendMailWithDoc.Visible = false;
                                            lnkLinkContract.Visible = false;
                                        }
                                    }                                   
                                }
                            }
                        }
                    }
                }
                else
                {
                    uploaddocsection.Visible = false;
                    statusandcommentsection.Visible = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void showHideContractSummaryTabTopButtons(bool flag)
        {
            try
            {
                int contractInstanceID = Convert.ToInt32(ViewState["ContractInstanceID"]);
                int UserID = Convert.ToInt32(AuthenticationHelper.UserID);
                string userid = Convert.ToString(UserID);
                topButtons.Visible = flag;
                //divContractHistory.Visible = flag;
                btnEditContractDetail.Visible = flag;
                lnksharesummary.Visible = flag;

                lnkSendMailWithDoc.Visible = flag;

                lnkLinkContract.Visible = flag;
                lnklinktotermsheet.Visible = flag;
                //lnkexecutivesummary.Visible = flag; 
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    if (IsCustID == Convert.ToString(AuthenticationHelper.CustomerID))

                    {
                        var queryResult = (from row in entities.Cont_tbl_VisibleToUser
                                           where row.ContractInstanceID == contractInstanceID
                                           && row.VisibkeUserID == userid
                                           && row.IsActive == true
                                           select row).FirstOrDefault();
                        //foreach (var item in queryResult)
                        //{
                        if(queryResult!=null)
                        { 
                            if (queryResult.VisibkeUserID == Convert.ToString(UserID) && queryResult.ContractInstanceID == contractInstanceID)
                            {
                                topButtons.Visible = false;
                                //divContractHistory.Visible = flag;
                                btnEditContractDetail.Visible = false;
                                lnksharesummary.Visible = false;
                                lnkSendMailWithDoc.Visible = false;
                                lnkLinkContract.Visible = false;
                                lnklinktotermsheet.Visible = false;
                            }
                            else
                            {   
                                topButtons.Visible = true;
                                btnEditContractDetail.Visible = true;
                                lnksharesummary.Visible = true; 
                                lnkSendMailWithDoc.Visible = true; 
                                lnkLinkContract.Visible = true;
                                lnklinktotermsheet.Visible = true;
                            }
                       } 
                    } 

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void showHideTabs(bool flag)
        {
            try
            {
                liContractDetails.Visible = flag;
                liDocument.Visible = flag;
                liTask.Visible = flag;
                liAuditLog.Visible = flag;
                liContractTransaction.Visible = flag;
                //liContractTransaction.Visible = true;
                liMilestone.Visible = flag;
                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void ShowHide_AddNewButtons(bool flag)
        {
            try
            {
                lnkBtnUploadDocument.Visible = flag;
                lnkAddNewTask.Visible = flag;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }



        public void toggleTextSaveButton_ContractSummaryTab(bool flag)
        {
            try
            {
                bool IsOnlycontract1 = ContractManagement.getcontractchecklist(Convert.ToInt32(AuthenticationHelper.CustomerID));
                if (IsOnlycontract1)
                {
                    if (flag) //Add Mode     
                    {
                        btnSaveContract.Text = "Send for Review";
                        btnClearContractDetail.Visible = flag;
                    }
                    else //Edit Mode
                    {
                        btnSaveContract.Text = "Update";
                        btnClearContractDetail.Visible = flag;
                    }
                }
                else
                {
                    if (flag) //Add Mode     
                    {
                        btnSaveContract.Text = "Save";
                        btnClearContractDetail.Visible = flag;
                    }
                    else //Edit Mode
                    {
                        btnSaveContract.Text = "Update";
                        btnClearContractDetail.Visible = flag;
                    }
                }

                btnSaveContract.Visible = true;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void toggleTextSaveButton_AddEditTask(bool flag)
        {
            try
            {
                if (flag) //Add Mode     
                {
                    rbTaskType.Enabled = flag;
                    btnTaskSave.Text = "Create & Assign";
                    btnTaskClear.Visible = flag;
                }
                else //Edit Mode
                {
                    rbTaskType.Enabled = flag;
                    btnTaskSave.Text = "Update";
                    btnTaskClear.Visible = flag;
                }

                btnTaskSave.Visible = true;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void enableDisableContractPopUpControls(bool flag)
        {
            try
            {
                btnEditContractDetail.Enabled = flag;
                lnksharesummary.Enabled = flag;
                lnkSendMailWithDoc.Enabled = flag;
                lnkLinkContract.Enabled = flag;
                lnklinktotermsheet.Visible = flag;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void btnEditContractControls_Click(object sender, EventArgs e)
        {
            try
            {
                drdTemplateID.Enabled = false;

                int ContractID = Convert.ToInt32(ViewState["ContractInstanceID"]);

                long customerID = -1;
                customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;

                var getContractStatus = ContractManagement.GetContractStatus(ContractID, Convert.ToInt32(customerID));

                if (ActiveContractsApprovals && getContractStatus[0].ContractStatusID.ToString() == "7" && AuthenticationHelper.Role != "MGMT" && AuthenticationHelper.Role != "CADMN")
                {
                    string CheckStatus = CaseManagement.IFUserEnable(Convert.ToInt32(AuthenticationHelper.UserID), contID);

                    if (CheckStatus == "Approved")
                    {
                        DocViewResult = "1";
                        enableDisableContractSummaryTabControls(true);
                    }
                    else if (CheckStatus == "Validity Expired" || CheckStatus == "Rejected")
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Request to Edit Contract Validity Expired or Rejected please clicked on My Reports/Contract Approval to check.')", true);
                    }
                    else if (CheckStatus == "Already Requested")
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Request to edit not accepted yet.')", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "showApprovalRequest", "showApprovalRequestPopup('" + contID + "');", true);
                    }
                }
                else
                {
                    enableDisableContractSummaryTabControls(true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        //protected void btnEditContractControls_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        drdTemplateID.Enabled = false;

        //        int ContractID = Convert.ToInt32(ViewState["ContractInstanceID"]);

        //        long customerID = -1;
        //        customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;

        //        var getContractStatus = ContractManagement.GetContractStatus(ContractID, Convert.ToInt32(customerID));

        //        if (ActiveContractsApprovals && getContractStatus[0].ContractStatusID.ToString() == "7" && RoleId != 8 && RoleId != 2)
        //        {
        //            string CheckStatus = CaseManagement.IFUserEnable(Convert.ToInt32(AuthenticationHelper.UserID), contID);

        //            if (CheckStatus == "Approved")
        //            {
        //                enableDisableContractSummaryTabControls(true);
        //            }
        //            else if (CheckStatus == "Validity Expired" || CheckStatus == "Rejected")
        //            {
        //                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Edit Validity expired,to extend validity please clicked on My Reports/Contract Approval.')", true);
        //            }
        //            else if (CheckStatus == "Already Requested")
        //            {
        //                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Request to edit not accepted yet.')", true);
        //            }
        //            else
        //            {
        //                ScriptManager.RegisterStartupScript(this, this.GetType(), "showApprovalRequest", "showApprovalRequestPopup('" + contID + "');", true);
        //            }
        //        }
        //        else
        //        {
        //            enableDisableContractSummaryTabControls(true);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //}


        protected void btnAddContract_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["Mode"] = 0;
                clearContractControls();

                showHideContractSummaryTabTopButtons(false);
                enableDisableContractPopUpControls(true);

                lblCustomField.Visible = false;
                divLinkedContracts.Visible = false;

                TabContract_Click(sender, e);

                showHideTabs(false);
                GetTermSheetdata();
                ddlContractType_SelectedIndexChanged(sender, e);

                ddlContractStatus.Enabled = false;
                ddlContractStatus.ClearSelection();
                if (ddlContractStatus.Items.FindByValue("1") != null) //1-Draft
                    ddlContractStatus.Items.FindByValue("1").Selected = true;

                AddSteps(items, statusBulletedList);
                SetProgress(0, statusBulletedList);

                ScriptManager.RegisterStartupScript(this, this.GetType(), "hideDivTree", "hideDivBranch();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnEditContract_Click(object sender, EventArgs e)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                ViewState["Mode"] = 1;
                showHideContractSummaryTabTopButtons(true);
                clearContractControls();
                TabContract_Click(sender, e);
                long contractInstanceID = 0;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["ContractInstanceID"])))
                {
                    contractInstanceID = Convert.ToInt32(ViewState["ContractInstanceID"]);
                    BindSectionDetail(Convert.ToInt32(contractInstanceID));
                    BindFieldDetail(Convert.ToInt32(contractInstanceID));
                    if (contractInstanceID != 0)
                    {                        
                        drdTemplateID.Enabled = false;
                        ViewState["Mode"] = 1;

                        var contractRecord = ContractManagement.GetContractDetailsByContractID(customerID, contractInstanceID);
                        //var contractRecord = ContractManagement.GetContractByID(contractInstanceID);

                        if (contractRecord != null)
                        {
                            //SET Current Contract Status
                            if (contractRecord.ContractStatusID != 0)
                                ViewState["ContractStatusID"] = contractRecord.ContractStatusID;

                            lnkexecutivesummary.Visible = false;                         
                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                            {
                                var getrecord = (from row in entities.Cont_tbl_ContractInstance
                                                 where row.ID == contractInstanceID
                                                 select row).FirstOrDefault();

                                if (getrecord != null)
                                {
                                    divtemplateID.Visible = false;
                                    liContractTransaction.Visible = true;//change 18 aug 2021
                                    //liContractTransaction.Visible = false;
                                    //liMilestone.Visible = false;
                                    liMilestone.Visible = true;
                                    if (getrecord.TemplateID != null)
                                    {
                                        drdTemplateID.SelectedValue = getrecord.TemplateID.ToString();
                                        divtemplateID.Visible = true;
                                        liContractTransaction.Visible = true;
                                        liMilestone.Visible = true;
                                        if (getrecord.Version != null)
                                        {
                                            if (contractRecord.ContractStatusID == 7)
                                            {
                                                lnkexecutivesummary.Visible = true;
                                            }
                                            else
                                            {
                                                if (getrecord.Version != "1.0")
                                                {
                                                    lnkexecutivesummary.Visible = true;
                                                }
                                            }
                                        }
                                    }

                                    //if (getrecord.Version != null)
                                    //{
                                    //    if (contractRecord.ContractStatusID == 7)
                                    //    {
                                    //        lnkexecutivesummary.Visible = true;                                           
                                    //    }
                                    //    else 
                                    //    {
                                    //        if (getrecord.Version != "1.0")
                                    //        {
                                    //            lnkexecutivesummary.Visible = true;                                               
                                    //        }
                                    //    }
                                    //}
                                }
                            }

                            ddlContractStatus.ClearSelection();

                            if (ddlContractStatus.Items.FindByValue(contractRecord.ContractStatusID.ToString()) != null)
                                ddlContractStatus.Items.FindByValue(contractRecord.ContractStatusID.ToString()).Selected = true;
                            
                            txtContractNo.Text = contractRecord.ContractNo;
                            txtTitle.Text = contractRecord.ContractTitle;
                            tbxDescription.Text = contractRecord.ContractDetailDesc;
                            tbxAddNewClause.Text = contractRecord.AddNewClause;

                            if (contractRecord.CustomerBranchID != 0)
                            {
                                foreach (TreeNode node in tvBranches.Nodes)
                                {
                                    if (node.Value == contractRecord.CustomerBranchID.ToString())
                                    {
                                        node.Selected = true;
                                    }
                                    foreach (TreeNode item1 in node.ChildNodes)
                                    {
                                        //if (item1.Value == contractRecord.CustomerBranchID.ToString())
                                        //    item1.Selected = true;

                                        if (item1.Value == contractRecord.CustomerBranchID.ToString())
                                        {
                                            item1.Selected = true;
                                        }
                                        foreach (TreeNode item2 in item1.ChildNodes)
                                        {
                                            if (item2.Value == contractRecord.CustomerBranchID.ToString())
                                            {
                                                item2.Selected = true;
                                            }
                                            foreach (TreeNode item3 in item2.ChildNodes)
                                            {
                                                if (item3.Value == contractRecord.CustomerBranchID.ToString())
                                                {
                                                    item3.Selected = true;
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            tvBranches_SelectedNodeChanged(null, null);

                            txtContractNo.Text = contractRecord.ContractNo;

                            #region Vendor

                            var lstVendorMapping = VendorDetails.GetVendorMapping(contractInstanceID);

                            if (lstBoxVendor.Items.Count > 0)
                            {
                                lstBoxVendor.ClearSelection();
                            }

                            if (lstVendorMapping.Count > 0)
                            {
                                foreach (var VendorID in lstVendorMapping)
                                {
                                    if (lstBoxVendor.Items.FindByValue(VendorID.ToString()) != null)
                                        lstBoxVendor.Items.FindByValue(VendorID.ToString()).Selected = true;
                                }
                            }

                            #endregion

                            ddlDepartment.ClearSelection();

                            if (ddlDepartment.Items.FindByValue(contractRecord.DepartmentID.ToString()) != null)
                                ddlDepartment.SelectedValue = contractRecord.DepartmentID.ToString();

                            if (contractRecord.ProposalDate != null)
                                txtProposalDate.Text = Convert.ToDateTime(contractRecord.ProposalDate).ToString("dd-MM-yyyy");
                            else
                                txtProposalDate.Text = string.Empty;

                            if (contractRecord.AgreementDate != null)
                                txtAgreementDate.Text = Convert.ToDateTime(contractRecord.AgreementDate).ToString("dd-MM-yyyy");
                            else
                                txtAgreementDate.Text = string.Empty;

                            if (contractRecord.EffectiveDate != null)
                                txtEffectiveDate.Text = Convert.ToDateTime(contractRecord.EffectiveDate).ToString("dd-MM-yyyy");
                            else
                                txtEffectiveDate.Text = string.Empty;

                            if (contractRecord.ReviewDate != null)
                                txtReviewDate.Text = Convert.ToDateTime(contractRecord.ReviewDate).ToString("dd-MM-yyyy");
                            else
                                txtReviewDate.Text = string.Empty;

                            if (contractRecord.ExpirationDate != null)
                                txtExpirationDate.Text = Convert.ToDateTime(contractRecord.ExpirationDate).ToString("dd-MM-yyyy");
                            else
                                txtExpirationDate.Text = string.Empty;

                            if (contractRecord.LockInPeriodDate != null)
                                txtlockinperiodDate.Text = Convert.ToDateTime(contractRecord.LockInPeriodDate).ToString("dd-MM-yyyy");
                            else
                                txtlockinperiodDate.Text = string.Empty;

                            if (contractRecord.Taxes != null)
                                tbxTaxes.Text = contractRecord.Taxes;
                            else
                                tbxTaxes.Text = string.Empty;

                            //if (contractRecord.ExpirationDate != null)
                                txtNoticeTerm.Text = Convert.ToString(contractRecord.NoticeTermNumber);
                            //else
                            //    txtNoticeTerm.Text = string.Empty;

                            if (ddlNoticeTerm.Items.FindByValue(contractRecord.NoticeTermType.ToString()) != null)
                                ddlNoticeTerm.SelectedValue = contractRecord.NoticeTermType.ToString();

                            ddlContractType.ClearSelection();

                            if (ddlContractType.Items.FindByValue(contractRecord.ContractTypeID.ToString()) != null)
                                ddlContractType.SelectedValue = contractRecord.ContractTypeID.ToString();

                            ddlContractType_SelectedIndexChanged(sender, e);

                            if (contractRecord.ContractSubTypeID != null)
                            {
                                ddlContractSubType.ClearSelection();

                                if (ddlContractSubType.Items.FindByValue(contractRecord.ContractSubTypeID.ToString()) != null)
                                    ddlContractSubType.SelectedValue = contractRecord.ContractSubTypeID.ToString();
                            }

                            #region Owner && Approvers                            

                            var lstContractUserAssignment = ContractManagement.GetContractUserAssignment_All(contractInstanceID);

                            if (lstContractUserAssignment.Count > 0)
                            {
                                lstBoxOwner.ClearSelection();
                                lstBoxApprover.ClearSelection();

                                foreach (var eachAssignmentRecord in lstContractUserAssignment)
                                {
                                    if (eachAssignmentRecord.RoleID == 3)
                                    {
                                        if (lstBoxOwner.Items.FindByValue(eachAssignmentRecord.UserID.ToString()) != null)
                                            lstBoxOwner.Items.FindByValue(eachAssignmentRecord.UserID.ToString()).Selected = true;
                                    }
                                    else if (eachAssignmentRecord.RoleID == 6)
                                    {
                                        if (lstBoxApprover.Items.FindByValue(eachAssignmentRecord.UserID.ToString()) != null)
                                            lstBoxApprover.Items.FindByValue(eachAssignmentRecord.UserID.ToString()).Selected = true;
                                    }
                                }
                            }
                           int contractID = Convert.ToInt32(ViewState["ContractInstanceID"]);
                            var lstUserAssignment = GetContractUserAssignment(contractID);
                            if (lstUserAssignment.Count > 0)
                            {
                                lstBoxUsers.ClearSelection();
                                foreach (var eachAssignmentRecord in lstUserAssignment)
                                {
                                    if (eachAssignmentRecord.ContractInstanceID == contractID)
                                        if (lstBoxUsers.Items.FindByValue(eachAssignmentRecord.VisibkeUserID.ToString()) != null)
                                            lstBoxUsers.Items.FindByValue(eachAssignmentRecord.VisibkeUserID.ToString()).Selected = true; 
                                }
                            }


                            //var lstAssignedUserIDs = ContractManagement.GetAssignedUsersByRoleID(contractInstanceID, 3); //3--Owner

                            //if (lstBoxOwner.Items.Count > 0)
                            //{
                            //    lstBoxOwner.ClearSelection();
                            //}

                            //if (lstAssignedUserIDs.Count > 0)
                            //{
                            //    foreach (var userID in lstAssignedUserIDs)
                            //    {
                            //        if (lstBoxOwner.Items.FindByValue(userID.ToString()) != null)
                            //            lstBoxOwner.Items.FindByValue(userID.ToString()).Selected = true;
                            //    }
                            //}                          

                            #endregion

                            if (contractRecord.ContractAmt != null)
                                tbxContractAmt.Text = contractRecord.ContractAmt.ToString();
                            else
                                tbxContractAmt.Text = string.Empty;

                            #region Payment Term

                            var lstpaymenttermMapping = ContractManagement.GetPaymentTermMapping(contractInstanceID);

                            if (ddlPaymentTerm1.Items.Count > 0)
                            {
                                ddlPaymentTerm1.ClearSelection();
                            }

                            if (lstpaymenttermMapping.Count > 0)
                            {
                                foreach (var paymenttermID in lstpaymenttermMapping)
                                {
                                    if (ddlPaymentTerm1.Items.FindByValue(paymenttermID.ToString()) != null)
                                        ddlPaymentTerm1.Items.FindByValue(paymenttermID.ToString()).Selected = true;
                                }
                            }

                            #endregion

                            //if (contractRecord.PaymentTermID != null)
                            //{
                            //    ddlPaymentTerm.ClearSelection();

                            //    if (ddlPaymentTerm.Items.FindByValue(contractRecord.PaymentTermID.ToString()) != null)
                            //        ddlPaymentTerm.SelectedValue = contractRecord.PaymentTermID.ToString();
                            //}


                            if (contractRecord.PaymentType != null)
                            {
                                ddlPaymentType.ClearSelection();

                                if (ddlPaymentType.Items.FindByValue(contractRecord.PaymentType.ToString()) != null)
                                    ddlPaymentType.SelectedValue = contractRecord.PaymentType.ToString();
                            }

                            if (contractRecord.ContactPersonOfDepartment != null)
                            {
                                ddlCPDepartment.ClearSelection();

                                if (ddlCPDepartment.Items.FindByValue(contractRecord.ContactPersonOfDepartment.ToString()) != null)
                                    ddlCPDepartment.SelectedValue = contractRecord.ContactPersonOfDepartment.ToString();
                            }

                            if (contractRecord.ProductItems != null)
                                txtBoxProduct.Text = contractRecord.ProductItems.ToString();
                            else
                                txtBoxProduct.Text = string.Empty;

                            if (contractRecord.AddNewClause != null)
                                tbxAddNewClause.Text = contractRecord.AddNewClause.ToString();
                            else
                                tbxAddNewClause.Text = string.Empty;

                            contractID = Convert.ToInt32(ViewState["ContractInstanceID"]);
                            var lstUserAssignments = GetContractUserAssignment(contractID);
                            if (lstUserAssignment.Count > 0)
                            {
                                lstBoxUsers.ClearSelection();
                                foreach (var eachAssignmentRecord in lstUserAssignments)
                                {
                                    if (eachAssignmentRecord.ContractInstanceID == contractID)
                                        if (lstBoxUsers.Items.FindByValue(eachAssignmentRecord.VisibkeUserID.ToString()) != null)
                                            lstBoxUsers.Items.FindByValue(eachAssignmentRecord.VisibkeUserID.ToString()).Selected = true;
                                }
                            }

                            BindContractHistory(customerID, Convert.ToInt64(contractInstanceID));

                            BindLinkedContracts(Convert.ToInt64(contractInstanceID));
                            //Bind Case To Link
                            BindContractListToLink(Convert.ToInt64(contractInstanceID));
                            
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnSaveContract_Click(object sender, EventArgs e)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                if (ViewState["Mode"] != null)
                {
                    bool formValidateSuccess = false;
                    bool saveSuccess = false;

                    List<Tuple<int, int>> lstContractUserMapping = new List<Tuple<int, int>>();
                    List<long> lstVendorMapping = new List<long>();

                    //List<int> lstOwnerMapping = new List<int>();
                    //List<int> lstApproverMapping = new List<int>();

                    #region Data Validation

                    List<string> lstErrorMsg = new List<string>();


                    if (subtype == 1)
                    {
                        if (ddlContractSubType.SelectedValue == "" || ddlContractSubType.SelectedValue == "-1")
                        {
                            lstErrorMsg.Add("Please select sub type.");
                        }
                        else
                        {
                            formValidateSuccess = true;
                        }
                    }

                    //Contract Number
                    if (String.IsNullOrEmpty(txtContractNo.Text.Trim()))
                        lstErrorMsg.Add("Required Contract Number.");
                    else
                        formValidateSuccess = true;

                    //Contract Title
                    if (String.IsNullOrEmpty(txtTitle.Text))
                        lstErrorMsg.Add("Required Contract Title");
                    else
                        formValidateSuccess = true;

                    //Contract Description
                    if (String.IsNullOrEmpty(tbxDescription.Text))
                        lstErrorMsg.Add("Required Contract Description");
                    else
                        formValidateSuccess = true;

                    //Branch Location
                    if (tvBranches.SelectedValue != "" && tvBranches.SelectedValue != "-1")
                        formValidateSuccess = true;
                    else
                        lstErrorMsg.Add("Select Entity/Branch/Location");

                    //Vendor/Contractor
                    int selectedVendorCount = 0;
                    foreach (ListItem eachVendor in lstBoxVendor.Items)
                    {
                        if (eachVendor.Selected)
                            selectedVendorCount++;
                    }

                    if (selectedVendorCount > 0)
                        formValidateSuccess = true;
                    else
                        lstErrorMsg.Add("Please select at least One Vendor.");

                    bool IsOnlycontract3 = ContractManagement.getcontractchecklist(Convert.ToInt32(AuthenticationHelper.CustomerID));
                    if (IsOnlycontract3)
                    {
                        if (!string.IsNullOrEmpty(Request.QueryString["AccessRoleID"]))
                        {
                            if (ddlstatus.SelectedValue != null && ddlstatus.SelectedValue != "" && ddlstatus.SelectedValue != "-1")
                                formValidateSuccess = true;
                            else
                                lstErrorMsg.Add("Please select status.");
                        }
                        else
                        {
                            if ((int)ViewState["Mode"] != 0)
                            {
                                if (ddlstatus.SelectedValue != null && ddlstatus.SelectedValue != "" && ddlstatus.SelectedValue != "-1")
                                    formValidateSuccess = true;
                                else
                                    lstErrorMsg.Add("Please select status.");
                            }
                        }

                        if (string.IsNullOrEmpty(Request.QueryString["AccessRoleID"]))
                        {
                            bool flagchkuserreviewer = false;
                            bool flagchkuserapprover = false;
                            //Reviewer
                            if (ddlReviewer.SelectedValue != "" && ddlReviewer.SelectedValue != "-1")
                            {
                                if (Convert.ToInt32(ddlReviewer.SelectedValue) != Convert.ToInt32(AuthenticationHelper.UserID))
                                {
                                    flagchkuserreviewer = true;
                                    formValidateSuccess = true;
                                }
                                else
                                    lstErrorMsg.Add("Please select different Reviewer.");

                            }
                            else
                                lstErrorMsg.Add("Please select Reviewer.");

                            //Approver
                            if (ddlApprover.SelectedValue != "" && ddlApprover.SelectedValue != "-1")
                            {
                                if (Convert.ToInt32(ddlApprover.SelectedValue) != Convert.ToInt32(AuthenticationHelper.UserID))
                                {
                                    flagchkuserapprover = true;
                                    formValidateSuccess = true;
                                }
                                else
                                    lstErrorMsg.Add("Please select different Approver.");
                            }
                            else
                                lstErrorMsg.Add("Please select Approver.");


                            if (flagchkuserreviewer == true && flagchkuserapprover == true)
                            {
                                if (ddlApprover.SelectedValue == ddlReviewer.SelectedValue)
                                {
                                    lstErrorMsg.Add("Approver and Reviewer should not be same. Please select different user.");
                                }
                            }
                        }
                    }
                    else
                    {
                        //Contact Person Of Department
                        if (ddlCPDepartment.SelectedValue != "" && ddlCPDepartment.SelectedValue != "-1")
                            formValidateSuccess = true;
                        else
                            lstErrorMsg.Add("Please select Contact Person of Department.");
                    }
                    //Department
                    if (ddlDepartment.SelectedValue != "" && ddlDepartment.SelectedValue != "-1")
                        formValidateSuccess = true;
                    else
                        lstErrorMsg.Add("Please select Department.");
                    
                    //Proposal Date
                    if (!string.IsNullOrEmpty(txtProposalDate.Text))
                    {
                        try
                        {
                            bool check = ContractCommonMethods.CheckValidDate(txtProposalDate.Text);
                            if (!check)
                            {
                                lstErrorMsg.Add("Please Check Proposal Date or Date should be in DD-MM-YYYY Format");
                            }
                        }
                        catch (Exception)
                        {
                            lstErrorMsg.Add("Please Check Proposal Date or Date should be in DD-MM-YYYY Format");
                        }
                    }

                    //Agreement Date
                    if (!string.IsNullOrEmpty(txtAgreementDate.Text))
                    {
                        try
                        {
                            bool check = ContractCommonMethods.CheckValidDate(txtAgreementDate.Text);
                            if (!check)
                            {
                                lstErrorMsg.Add("Please Check Agreement Date or Date should be in DD-MM-YYYY Format");
                            }
                        }
                        catch (Exception)
                        {
                            lstErrorMsg.Add("Please Check Agreement Date or Date should be in DD-MM-YYYY Format");
                        }
                    }
                    if (!IsOnlycontract3)
                    {
                        if (ddlContractStatus.SelectedItem.Text != "Draft")
                        {
                            //Effective Date
                            if (!string.IsNullOrEmpty(txtEffectiveDate.Text))
                            {
                                try
                                {
                                    bool check = ContractCommonMethods.CheckValidDate(txtEffectiveDate.Text);
                                    if (!check)
                                    {
                                        lstErrorMsg.Add("Please Check Start Date or Date should be in DD-MM-YYYY Format");
                                    }
                                }
                                catch (Exception)
                                {
                                    lstErrorMsg.Add("Please Check Start Date or Date should be in DD-MM-YYYY Format");
                                }
                            }
                            else
                                lstErrorMsg.Add("Start Date of contract required.");

                            //Expiration Date
                            if (!string.IsNullOrEmpty(txtExpirationDate.Text))
                            {
                                try
                                {
                                    bool check = ContractCommonMethods.CheckValidDate(txtExpirationDate.Text);
                                    if (!check)
                                    {
                                        lstErrorMsg.Add("Please Check Expiration Date or Date should be in DD-MM-YYYY Format");
                                    }
                                }
                                catch (Exception)
                                {
                                    lstErrorMsg.Add("Please Check Expiration Date or Date should be in DD-MM-YYYY Format");
                                }
                            }
                            else
                                lstErrorMsg.Add("End Date of contract required.");

                            if (!string.IsNullOrEmpty(txtlockinperiodDate.Text))
                            {
                                try
                                {
                                    bool check = ContractCommonMethods.CheckValidDate(txtlockinperiodDate.Text);
                                    if (!check)
                                    {
                                        lstErrorMsg.Add("Please Check Lock-In Period Date or Date should be in DD-MM-YYYY Format");
                                    }
                                }
                                catch (Exception)
                                {
                                    lstErrorMsg.Add("Please Check Lock-In Period Date or Date should be in DD-MM-YYYY Format");
                                }
                            }
                        }
                    }
                    //Contract Template
                    //if (drdTemplateID.SelectedValue != "" && drdTemplateID.SelectedValue != "-1")
                    //    formValidateSuccess = true;
                    //else
                    //    lstErrorMsg.Add("Please select Contract Template.");
                    
                    //Review Date
                    if (!string.IsNullOrEmpty(txtReviewDate.Text))
                    {
                        try
                        {
                            bool check = ContractCommonMethods.CheckValidDate(txtReviewDate.Text);
                            if (!check)
                            {
                                lstErrorMsg.Add("Please Check Review Date or Date should be in DD-MM-YYYY Format");
                            }
                        }
                        catch (Exception)
                        {
                            lstErrorMsg.Add("Please Check Review Date or Date should be in DD-MM-YYYY Format");
                        }
                    }


                    //Contract Type
                    if (ddlContractType.SelectedValue != "" && ddlContractType.SelectedValue != "-1")
                        formValidateSuccess = true;
                    else
                        lstErrorMsg.Add("Please select Contract Type.");

                    //Owner
                    int selectedOwnerCount = 0;
                    List<int> chklegalowner = new List<int>();
                    chklegalowner.Clear();
                    foreach (ListItem eachOwner in lstBoxOwner.Items)
                    {
                        if (eachOwner.Selected)
                        {
                            chklegalowner.Add(Convert.ToInt32(eachOwner.Value));
                            selectedOwnerCount++;
                        }
                    }

                    if (selectedOwnerCount > 0)
                        formValidateSuccess = true;
                    else
                        lstErrorMsg.Add("Please select at least One Contract Owner.");

                    //Approver
                    //int selectedApproverCount = 0;
                    //foreach (ListItem eachApprover in lstBoxApprover.Items)
                    //{
                    //    if (eachApprover.Selected)
                    //        selectedApproverCount++;
                    //}

                    //if (selectedApproverCount > 0)
                    //    formValidateSuccess = true;
                    //else
                    //    lstErrorMsg.Add("Select at least One Contract Approver");

                    //Contract Amt
                    if (!String.IsNullOrEmpty(tbxContractAmt.Text))
                    {
                        try
                        {
                            double n;
                            var isNumeric = double.TryParse(tbxContractAmt.Text, out n);
                            if (!isNumeric)
                            {
                                lstErrorMsg.Add("Enter Valid Contract Amount, Only numbers are allowed");
                            }
                        }
                        catch (Exception)
                        {
                            lstErrorMsg.Add("Enter Valid Contract Amount, Only numbers are allowed");
                        }
                    }
                    if (IsOnlycontract3)
                    {
                        if (chklegalowner.Count > 0)
                        {
                            bool isownersame = false;
                            if (ddlReviewer.SelectedValue != "" && ddlReviewer.SelectedValue != "-1")
                            {
                                if (chklegalowner.Contains(Convert.ToInt32(ddlReviewer.SelectedValue)))
                                {
                                    isownersame = true;
                                }
                            }
                            if (ddlApprover.SelectedValue != "" && ddlApprover.SelectedValue != "-1")
                            {
                                if (chklegalowner.Contains(Convert.ToInt32(ddlApprover.SelectedValue)))
                                {
                                    isownersame = true;
                                }
                            }
                            if (isownersame)
                            {
                                lstErrorMsg.Add("Approver and Reviewer should not be same legal. Please select different legal.");
                            }
                        }
                    }


                    if (lstErrorMsg.Count > 0)
                    {
                        formValidateSuccess = false;
                        showErrorMessages(lstErrorMsg, cvContractPopUp);
                    }

                    if (!cvContractPopUp.IsValid)
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "scrollUpPageScript", "scrollUpPage();", true);

                    #endregion

                    #region Save (Add/Edit)

                    if (formValidateSuccess)
                    {
                        long newContractID = 0;

                        Cont_tbl_ContractInstance contractRecord = new Cont_tbl_ContractInstance()
                        {
                            ContractType = 1,

                            CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID),
                            IsDeleted = false,

                            ContractNo = txtContractNo.Text,
                            ContractTitle = txtTitle.Text,
                            ContractDetailDesc = tbxDescription.Text,
                            AddNewClause = tbxAddNewClause.Text,
                            CustomerBranchID = Convert.ToInt32(tvBranches.SelectedValue),
                            DepartmentID = Convert.ToInt32(ddlDepartment.SelectedValue),
                            ContractTypeID = Convert.ToInt64(ddlContractType.SelectedValue),
                            CreatedBy = AuthenticationHelper.UserID,
                            UpdatedBy = AuthenticationHelper.UserID,
                            //ContactPersonOfDepartment = Convert.ToInt32(ddlCPDepartment.SelectedValue)
                        };

                        if (ddlCPDepartment.SelectedValue != "" && ddlCPDepartment.SelectedValue != "-1")
                            contractRecord.ContactPersonOfDepartment = Convert.ToInt32(ddlCPDepartment.SelectedValue);

                        if (!string.IsNullOrEmpty(txtProposalDate.Text))
                            contractRecord.ProposalDate = DateTimeExtensions.GetDate(txtProposalDate.Text);

                        if (!string.IsNullOrEmpty(txtAgreementDate.Text))
                            contractRecord.AgreementDate = DateTimeExtensions.GetDate(txtAgreementDate.Text);

                        if (!string.IsNullOrEmpty(txtReviewDate.Text))
                            contractRecord.ReviewDate = DateTimeExtensions.GetDate(txtReviewDate.Text);

                        if (!string.IsNullOrEmpty(txtEffectiveDate.Text))
                            contractRecord.EffectiveDate = DateTimeExtensions.GetDate(txtEffectiveDate.Text);

                        if (!string.IsNullOrEmpty(txtExpirationDate.Text))
                            contractRecord.ExpirationDate = DateTimeExtensions.GetDate(txtExpirationDate.Text);

                        if (!string.IsNullOrEmpty(txtlockinperiodDate.Text))
                            contractRecord.LockInPeriodDate = DateTimeExtensions.GetDate(txtlockinperiodDate.Text);

                        if (!string.IsNullOrEmpty(tbxTaxes.Text))
                            contractRecord.Taxes = tbxTaxes.Text;

                        if (ddlContractSubType.SelectedValue != "" && ddlContractSubType.SelectedValue != "-1")
                            contractRecord.ContractSubTypeID = Convert.ToInt64(ddlContractSubType.SelectedValue);

                        if (ddlPaymentType.SelectedValue != "" && ddlPaymentType.SelectedValue != "-1")
                            contractRecord.PaymentType = Convert.ToInt32(ddlPaymentType.SelectedValue);

                        if (ddlCPDepartment.SelectedValue != "" && ddlCPDepartment.SelectedValue != "-1")
                            contractRecord.ContactPersonOfDepartment = Convert.ToInt32(ddlCPDepartment.SelectedValue);

                        if (!string.IsNullOrEmpty(txtNoticeTerm.Text))
                        {
                            contractRecord.NoticeTermNumber = Convert.ToInt32(txtNoticeTerm.Text.Trim());

                            if (ddlNoticeTerm.SelectedValue != "" && ddlNoticeTerm.SelectedValue != "-1")
                                contractRecord.NoticeTermType = Convert.ToInt32(ddlNoticeTerm.SelectedValue);
                        }

                        if (!string.IsNullOrEmpty(tbxContractAmt.Text))
                            contractRecord.ContractAmt = Convert.ToDecimal(tbxContractAmt.Text);
                        else
                            contractRecord.ContractAmt = 0;

                        //if (ddlPaymentTerm.SelectedValue != "" && ddlPaymentTerm.SelectedValue != "-1")
                        //    contractRecord.PaymentTermID = Convert.ToInt32(ddlPaymentTerm.SelectedValue);

                        if (!string.IsNullOrEmpty(txtBoxProduct.Text))
                            contractRecord.ProductItems = Convert.ToString(txtBoxProduct.Text.Trim());

                        if (!string.IsNullOrEmpty(tbxAddNewClause.Text))
                            contractRecord.AddNewClause = Convert.ToString(tbxAddNewClause.Text.Trim());

                        #region ADD New Contract

                        if ((int)ViewState["Mode"] == 0)
                        {
                            //bool IsPerformercontract = ContractManagement.getcontractchecklist(customerID);
                            if (IsOnlycontract3)
                                contractRecord.IsReviewedContract = 0;
                            else
                                contractRecord.IsReviewedContract = 1;

                            if (drdTemplateID.SelectedValue != null && drdTemplateID.SelectedValue != "" && drdTemplateID.SelectedValue != "-1")
                            {
                                int tempID = Convert.ToInt32(drdTemplateID.SelectedValue);
                                contractRecord.TemplateID = tempID;

                                contractRecord.ApproverCount = ContractManagement.getApproverCount(customerID, tempID);
                            }
                            bool existCaseNo = ContractManagement.ExistsContractNo(customerID, contractRecord.ContractNo, 0);
                            if (!existCaseNo)
                            {
                                //if (!ContractManagement.ExistsContractTitle(customerID, contractRecord.ContractTitle, 0))
                                if (1 == 1)
                                {
                                    contractRecord.Version = "1.0";
                                    newContractID = ContractManagement.CreateContract(contractRecord);

                                    if (newContractID > 0)
                                    {
                                        ContractManagement.CreateAuditLog("C", newContractID, "Cont_tbl_ContractInstance", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "New Contract Created", true, Convert.ToInt32(newContractID));
                                        btnAssignCPD_Click(sender, e);
                                        saveSuccess = true;
                                    }
                                }
                                else
                                {
                                    cvContractPopUp.IsValid = false;
                                    cvContractPopUp.ErrorMessage = "Contract with Same Title already Exists.";
                                    cvContractPopUp.CssClass = "alert alert-danger";
                                    return;
                                }
                            }
                            else
                            {
                                cvContractPopUp.IsValid = false;
                                cvContractPopUp.ErrorMessage = "Contract with same Contract Number already exist.";
                                cvContractPopUp.CssClass = "alert alert-danger";
                            }

                            if (saveSuccess)
                            {
                                #region Contract Status Transaction - Draft

                                Cont_tbl_ContractStatusTransaction newStatusRecord = new Cont_tbl_ContractStatusTransaction()
                                {
                                    CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID),
                                    ContractID = newContractID,
                                    StatusID = Convert.ToInt64(ddlContractStatus.SelectedValue),
                                    StatusChangeOn = DateTime.Now,
                                    IsActive = true,
                                    CreatedBy = AuthenticationHelper.UserID,
                                    CreatedOn = DateTime.Now,
                                    UpdatedBy = AuthenticationHelper.UserID,
                                    UpdatedOn = DateTime.Now,
                                };

                                saveSuccess = ContractManagement.CreateContractStatusTransaction(newStatusRecord);

                                #endregion

                                #region Vendor Mapping

                                if (lstBoxVendor.Items.Count > 0)
                                {
                                    foreach (ListItem eachVendor in lstBoxVendor.Items)
                                    {
                                        if (eachVendor.Selected)
                                        {
                                            if (Convert.ToInt64(eachVendor.Value) != 0)
                                                lstVendorMapping.Add(Convert.ToInt64(eachVendor.Value));
                                        }
                                    }
                                }

                                if (lstVendorMapping.Count > 0)
                                {
                                    List<Cont_tbl_VendorMapping> lstVendorMapping_ToSave = new List<Cont_tbl_VendorMapping>();

                                    lstVendorMapping.ForEach(EachVendor =>
                                    {
                                        Cont_tbl_VendorMapping _vendorMappingRecord = new Cont_tbl_VendorMapping()
                                        {
                                            ContractID = newContractID,
                                            VendorID = EachVendor,
                                            IsActive = true,
                                            CreatedBy = AuthenticationHelper.UserID,
                                            CreatedOn = DateTime.Now,
                                        };

                                        lstVendorMapping_ToSave.Add(_vendorMappingRecord);
                                    });

                                    saveSuccess = VendorDetails.CreateUpdate_VendorMapping(lstVendorMapping_ToSave);
                                    if (saveSuccess)
                                    {
                                        ContractManagement.CreateAuditLog("C", newContractID, "Cont_tbl_VendorMapping", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Contract Vendor Mapping Created", true, Convert.ToInt32(newContractID));
                                    }

                                    //Refresh List
                                    lstVendorMapping_ToSave.Clear();
                                    lstVendorMapping_ToSave = null;

                                    lstVendorMapping.Clear();
                                    lstVendorMapping = null;
                                }
                                #endregion

                                #region Payment term Mapping
                                List<long> lstPaymenttermMapping = new List<long>();
                                if (ddlPaymentTerm1.Items.Count > 0)
                                {
                                    foreach (ListItem eachVendor in ddlPaymentTerm1.Items)
                                    {
                                        if (eachVendor.Selected)
                                        {
                                            if (Convert.ToInt64(eachVendor.Value) != -1)
                                                lstPaymenttermMapping.Add(Convert.ToInt64(eachVendor.Value));
                                        }
                                    }
                                }

                                if (lstPaymenttermMapping.Count > 0)
                                {
                                    List<Cont_tbl_PaymentTermMapping> lstPaymenttermMapping_ToSave = new List<Cont_tbl_PaymentTermMapping>();

                                    lstPaymenttermMapping.ForEach(EachPaymentterm =>
                                    {
                                        Cont_tbl_PaymentTermMapping paymenttermMappingRecord = new Cont_tbl_PaymentTermMapping()
                                        {
                                            ContractID = newContractID,
                                            PaymentTermID = EachPaymentterm,
                                            IsDeleted = false,
                                            CustomerID = Convert.ToInt64(AuthenticationHelper.CustomerID),
                                        };

                                        lstPaymenttermMapping_ToSave.Add(paymenttermMappingRecord);
                                    });

                                    saveSuccess = ContractManagement.CreateUpdate_PaymentTermMapping(lstPaymenttermMapping_ToSave);
                                    if (saveSuccess)
                                    {
                                        ContractManagement.CreateAuditLog("C", newContractID, "Cont_tbl_PaymentTermMapping", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Contract Payment Term Mapping Created", true, Convert.ToInt32(newContractID));
                                    }

                                    //Refresh List
                                    lstPaymenttermMapping_ToSave.Clear();
                                    lstPaymenttermMapping_ToSave = null;

                                    lstPaymenttermMapping.Clear();
                                    lstPaymenttermMapping = null;
                                }
                                #endregion

                                #region Save Custom Field

                                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["CustomefieldCount"])))
                                {
                                    List<Cont_tbl_CustomFieldValue> lstObjParameters = new List<Cont_tbl_CustomFieldValue>();

                                    //Select Which Grid to Loop based on Selected Category/Type
                                    GridView gridViewToCollectData = null;
                                    gridViewToCollectData = grdCustomField;

                                    if (gridViewToCollectData != null)
                                    {
                                        for (int i = 0; i < gridViewToCollectData.Rows.Count; i++)
                                        {
                                            Label lblID = (Label)gridViewToCollectData.Rows[i].FindControl("lblID");
                                            TextBox tbxLabelValue = (TextBox)gridViewToCollectData.Rows[i].FindControl("tbxLabelValue");

                                            if (lblID != null && tbxLabelValue != null)
                                            {
                                                if (!string.IsNullOrEmpty(lblID.Text) || lblID.Text != "0")
                                                {
                                                    Cont_tbl_CustomFieldValue ObjParameter = new Cont_tbl_CustomFieldValue()
                                                    {
                                                        ContractID = newContractID,

                                                        LabelID = Convert.ToInt64(lblID.Text),
                                                        LabelValue = tbxLabelValue.Text,

                                                        IsDeleted = false,
                                                        CreatedBy = AuthenticationHelper.UserID,
                                                        CreatedOn = DateTime.Now,
                                                        UpdatedBy = AuthenticationHelper.UserID,
                                                        UpdatedOn = DateTime.Now
                                                    };
                                                    lstObjParameters.Add(ObjParameter);
                                                    //saveSuccess = ContractManagement.CreateUpdateCustomsField(ObjParameter);
                                                }
                                            }
                                        }//End For Each

                                        saveSuccess = ContractManagement.CreateUpdate_CustomsFields(lstObjParameters);

                                        if (saveSuccess)
                                        {
                                            ContractManagement.CreateAuditLog("C", newContractID, "Cont_tbl_CustomFieldValue", "Update", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Contract Custom Parameter(s) Updated", true, Convert.ToInt32(newContractID));
                                        }
                                    }
                                }

                                #endregion

                                #region User Assignment                           

                                //De-Active All Previous Assignment
                                ContractManagement.DeActiveContractUserAssignments(newContractID, AuthenticationHelper.UserID);
                                List<Cont_tbl_UserAssignment> lstUserAssignmentRecord_ToSave = new List<Cont_tbl_UserAssignment>();
                                List<string> SelectedOwnersList = new List<string>();
                                int assignedRoleID = 3;
                                if (lstBoxOwner.Items.Count > 0)
                                {
                                    SelectedOwnersList.Clear();
                                    assignedRoleID = 3;
                                    foreach (ListItem eachOwner in lstBoxOwner.Items)
                                    {
                                        if (eachOwner.Selected)
                                        {
                                            if (Convert.ToInt32(eachOwner.Value) != 0)
                                            {
                                                SelectedOwnersList.Add(eachOwner.Text);
                                                lstContractUserMapping.Add(new Tuple<int, int>(Convert.ToInt32(eachOwner.Value), assignedRoleID));
                                            }
                                            //lstOwnerMapping.Add(Convert.ToInt32(eachOwner.Value));
                                        }
                                    }
                                }

                                if (lstBoxApprover.Items.Count > 0)
                                {
                                    assignedRoleID = 6;
                                    foreach (ListItem eachApprover in lstBoxApprover.Items)
                                    {
                                        if (eachApprover.Selected)
                                        {
                                            if (Convert.ToInt32(eachApprover.Value) != 0)
                                                lstContractUserMapping.Add(new Tuple<int, int>(Convert.ToInt32(eachApprover.Value), assignedRoleID));
                                            // lstApproverMapping.Add(Convert.ToInt32(eachApprover.Value));
                                        }
                                    }
                                }

                                if (lstContractUserMapping.Count > 0)
                                {
                                    lstContractUserMapping.ForEach(eachUser =>
                                    {
                                        Cont_tbl_UserAssignment newAssignment = new Cont_tbl_UserAssignment()
                                        {
                                            AssignmentType = 1,
                                            ContractID = newContractID,

                                            UserID = Convert.ToInt32(eachUser.Item1),
                                            RoleID = Convert.ToInt32(eachUser.Item2),

                                            IsActive = true,
                                            CreatedBy = AuthenticationHelper.UserID,
                                            UpdatedBy = AuthenticationHelper.UserID,
                                        };

                                        lstUserAssignmentRecord_ToSave.Add(newAssignment);

                                        //saveSuccess = ContractManagement.CreateUpdateContractUserAssignment(newAssignment);
                                    });
                                }

                                if (lstUserAssignmentRecord_ToSave.Count > 0)
                                    saveSuccess = ContractManagement.CreateUpdate_ContractUserAssignments(lstUserAssignmentRecord_ToSave);

                                if (saveSuccess)
                                {
                                    ContractManagement.CreateAuditLog("C", newContractID, "Cont_tbl_UserAssignment", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Contract Owner(s) Created.(" + String.Join(",", SelectedOwnersList) + ")", true, Convert.ToInt32(newContractID));
                                    //ContractManagement.CreateAuditLog("C", newContractID, "Cont_tbl_UserAssignment", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Contract Owner(s) Created", true);
                                }

                                SelectedOwnersList.Clear();
                                lstUserAssignmentRecord_ToSave.Clear();
                                lstUserAssignmentRecord_ToSave = null;


                                ContractUserAssignments(newContractID);
                                List<Cont_tbl_VisibleToUser> lstUserAssignment_ToSave = new List<Cont_tbl_VisibleToUser>();
                                List<string> SelectedUserList = new List<string>();
                                if (lstBoxUsers.Items.Count > 0)
                                {
                                    SelectedUserList.Clear();
                                    foreach (ListItem eachOwner in lstBoxUsers.Items)
                                    {
                                        if (eachOwner.Selected)
                                        {
                                            if (Convert.ToInt32(eachOwner.Value) != 0)
                                            {
                                                SelectedUserList.Add(eachOwner.Text);

                                                //  lstContractUserMapping.Add(Convert.ToInt32(eachOwner.Value));
                                            }
                                            //lstOwnerMapping.Add(Convert.ToInt32(eachOwner.Value));
                                        }
                                    }
                                }
                                SelectedUserList.Clear();
                                lstUserAssignment_ToSave.Clear();
                                #endregion
                            }

                            if (saveSuccess)
                            {
                                #region Contract Template
                                if (drdTemplateID.SelectedValue != null && drdTemplateID.SelectedValue != "")
                                {
                                    if (Convert.ToInt32(drdTemplateID.SelectedValue) > 0)
                                    {
                                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                        {
                                            int CID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                                            long TID = Convert.ToInt64(drdTemplateID.SelectedValue);

                                            var gettemplatesection1 = (from row in entities.Cont_tbl_TemplateSectionMapping
                                                                       where row.TemplateID == TID
                                                                       && row.IsActive == true
                                                                       && row.CustomerID == CID
                                                                       select row).ToList();
                                            if (gettemplatesection1.Count > 0)
                                            {
                                                foreach (var itemsection in gettemplatesection1)
                                                {
                                                    Cont_tbl_ContractTemplateSectionMapping ContracttemplateSection = new Cont_tbl_ContractTemplateSectionMapping()
                                                    {
                                                        ContractTemplateInstanceID = Convert.ToInt32(newContractID),
                                                        SectionOrder = itemsection.SectionOrder,
                                                        SectionID = itemsection.SectionID,
                                                        IsActive = true,
                                                        CustomerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID),
                                                        CreatedBy = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID),
                                                        CreatedOn = DateTime.Now,
                                                        UpdatedBy = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID),
                                                        UpdatedOn = DateTime.Now,
                                                        Headervisibilty = itemsection.Headervisibilty,
                                                        DeptID = itemsection.DeptID
                                                    };
                                                    ContractTemplateManagement.CreateContractTemplateSection(ContracttemplateSection);
                                                }
                                            }
                                            var gettemplatesection = (from row in entities.Cont_tbl_TemplateSectionMapping
                                                                      where row.TemplateID == TID
                                                                      && row.IsActive == true
                                                                      && row.CustomerID == CID
                                                                      select row).ToList();

                                            if (gettemplatesection.Count > 0)
                                            {
                                                string ContractVersion = ContractTemplateManagement.getVersion(Convert.ToInt32(newContractID), CID);

                                                foreach (var item in gettemplatesection)
                                                {
                                                    var datatemplatesection = (from row in entities.SP_ContractTemplateSectionData(Convert.ToInt32(newContractID), CID, TID, item.SectionID)
                                                                               select row).ToList();

                                                    ContractTemplateAssignment ContracttemplateAssignment = new ContractTemplateAssignment()
                                                    {
                                                        ContractTemplateInstanceID = Convert.ToInt32(newContractID),
                                                        RoleID = 3,
                                                        UserID = AuthenticationHelper.UserID,
                                                        IsActive = true,
                                                        SectionID = item.SectionID,
                                                        Visibility = 1
                                                    };
                                                    ContractTemplateManagement.CreateContractTemplateAssignment(ContracttemplateAssignment);

                                                    string templateSectionContent = datatemplatesection.FirstOrDefault();

                                                    ContractTemplateTransaction obj1 = new ContractTemplateTransaction();
                                                    obj1.ContractTemplateInstanceID = Convert.ToInt32(newContractID);
                                                    obj1.CreatedOn = DateTime.Now;
                                                    obj1.CreatedBy = AuthenticationHelper.UserID;
                                                    obj1.ContractTemplateContent = templateSectionContent;
                                                    obj1.CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User;
                                                    obj1.Status = 1;
                                                    obj1.OrderID = 1;
                                                    obj1.Version = ContractVersion;
                                                    obj1.SectionID = item.SectionID;
                                                    entities.ContractTemplateTransactions.Add(obj1);
                                                    entities.SaveChanges();
                                                }
                                            }
                                            RoleId = getRoleID(Convert.ToInt32(newContractID), AuthenticationHelper.UserID);
                                            TemplateId = Convert.ToInt32(newContractID);
                                            UId = Convert.ToInt32(AuthenticationHelper.UserID);
                                            CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                                        }
                                    }
                                }
                                #endregion

                                cvContractPopUp.IsValid = false;
                                cvContractPopUp.ErrorMessage = "Contract Created Successfully.";
                                VSContractPopup.CssClass = "alert alert-success";

                                ViewState["Mode"] = 1;
                                ViewState["ContractInstanceID"] = newContractID;

                                toggleTextSaveButton_ContractSummaryTab(false); //false -- Switch to Edit Mode

                                showHideTabs(true);
                                showHideContractSummaryTabTopButtons(true);
                                enableDisableContractSummaryTabControls(false);

                                //Bind Case To Link
                                BindContractListToLink(newContractID);
                            }

                            if (saveSuccess)
                            {

                                #region Sent Email to Assigned Users

                                string emailTemplate = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_Contract_Assignment;

                                if (!string.IsNullOrEmpty(emailTemplate))
                                {
                                    string accessURL = string.Empty;
                                    URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(AuthenticationHelper.CustomerID));
                                    if (Urloutput != null)
                                    {
                                        accessURL = Urloutput.URL;
                                    }
                                    else
                                    {
                                        accessURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                                    }

                                    //string accessURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                                    ContractManagement.SendMailtoAssignedUsers(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, contractRecord, lstContractUserMapping, emailTemplate, accessURL, Convert.ToInt32(newContractID));
                                }

                                #endregion
                            }

                            #region add Contract reviewe
                            if (IsOnlycontract3)
                            {
                                var DocData = DocumentManagement.GetTempContractDocumentData(Convert.ToInt32(AuthenticationHelper.UserID), Convert.ToInt32(AuthenticationHelper.CustomerID));
                                if (DocData.Count > 0)
                                {
                                    foreach (var item in DocData)
                                    {
                                        ContractDocument objdoc = new ContractDocument();
                                        objdoc.UserID = item.UserID;
                                        objdoc.CustomerID = item.CustomerID;
                                        objdoc.ContractID = newContractID;
                                        objdoc.DocName = item.DocName;
                                        objdoc.DocData = item.DocData;
                                        objdoc.DocPath = item.DocPath;
                                        objdoc.FileSize = item.FileSize;
                                        objdoc.IsfromComment = false;
                                        DocumentManagement.CreateContractDoc(objdoc);
                                    }
                                    DocumentManagement.DeleteAllTempContractDocumentFile(Convert.ToInt64(AuthenticationHelper.UserID), Convert.ToInt64(AuthenticationHelper.CustomerID));
                                }

                                if (ddlReviewer.SelectedValue != null && ddlReviewer.SelectedValue != "" && ddlReviewer.SelectedValue != "-1")
                                {
                                    ContractAssignment objass = new ContractAssignment();
                                    objass.ContractInstanceID = newContractID;
                                    objass.RoleID = 4;
                                    objass.UserID = Convert.ToInt64(ddlReviewer.SelectedValue);
                                    objass.IsActive = true;
                                    DocumentManagement.CreateContractAssignment(objass);
                                }

                                if (ddlApprover.SelectedValue != null && ddlApprover.SelectedValue != "" && ddlApprover.SelectedValue != "-1")
                                {
                                    ContractAssignment objass = new ContractAssignment();
                                    objass.ContractInstanceID = newContractID;
                                    objass.RoleID = 6;
                                    objass.UserID = Convert.ToInt64(ddlApprover.SelectedValue);
                                    objass.IsActive = true;
                                    DocumentManagement.CreateContractAssignment(objass);
                                }

                                Cont_tbl_ContractTransaction objtran = new Cont_tbl_ContractTransaction();
                                objtran.CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                                objtran.ContractID = newContractID;
                                objtran.StatusID = 2;
                                objtran.StatusChangeOn = DateTime.Now;
                                objtran.IsActive = true;
                                objtran.CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID);
                                objtran.CreatedOn = DateTime.Now;
                                objtran.UpdatedBy = Convert.ToInt32(AuthenticationHelper.UserID);
                                objtran.UpdatedOn = DateTime.Now;
                                objtran.Remark = tbxAddNewClause.Text;
                                DocumentManagement.CreateContractTrans(objtran);

                                Cont_tbl_ContractStatusTransaction newStatusRecord = new Cont_tbl_ContractStatusTransaction()
                                {
                                    CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID),
                                    ContractID = newContractID,
                                    StatusID = Convert.ToInt64(2),
                                    StatusChangeOn = DateTime.Now,
                                    IsActive = true,
                                    CreatedBy = AuthenticationHelper.UserID,
                                    CreatedOn = DateTime.Now,
                                    UpdatedBy = AuthenticationHelper.UserID,
                                    UpdatedOn = DateTime.Now,
                                };

                                saveSuccess = ContractManagement.CreateContractStatusTransaction(newStatusRecord);
                            }
                            #endregion
                            //lstApproverMapping.Clear();
                            //lstApproverMapping = null;

                            //lstOwnerMapping.Clear();
                            //lstOwnerMapping = null;
                        }

                        #endregion

                        #region Edit Contract

                        else if ((int)ViewState["Mode"] == 1)
                        {
                            if (ViewState["ContractInstanceID"] != null)
                            {
                                newContractID = Convert.ToInt64(ViewState["ContractInstanceID"]);

                                if (newContractID != 0)
                                {
                                    contractRecord.ID = newContractID;

                                    bool existContractNo = ContractManagement.ExistsContractNo(customerID, contractRecord.ContractNo, newContractID);
                                    if (!existContractNo)
                                    {
                                        //if (!ContractManagement.ExistsContractTitle(customerID, contractRecord.ContractTitle, newContractID))
                                        if (1 == 1)
                                        {
                                            saveSuccess = ContractManagement.UpdateContract(contractRecord);

                                            if (saveSuccess)
                                            {
                                                btnAssignCPD_Click(sender, e);
                                                ContractManagement.CreateAuditLog("C", newContractID, "Cont_tbl_ContractInstance", "Update", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Contract Detail(s) Updated", true, Convert.ToInt32(newContractID));
                                            }
                                        }
                                        else
                                        {
                                            cvContractPopUp.IsValid = false;
                                            cvContractPopUp.ErrorMessage = "Contract with Same Title already Exists.";
                                            cvContractPopUp.CssClass = "alert alert-danger";
                                            return;
                                        }
                                    }
                                    else
                                    {
                                        cvContractPopUp.IsValid = false;
                                        cvContractPopUp.ErrorMessage = "Contract with Same Contract Number already exists";
                                        cvContractPopUp.CssClass = "alert alert-danger";
                                    }

                                    if (saveSuccess)
                                    {
                                        bool matchSuccess = false;

                                        #region Contract Status Transaction

                                        Save_ContractStatus(newContractID);

                                        #endregion

                                        #region Payment term Mapping
                                        List<long> lstPaymenttermMapping = new List<long>();
                                        if (ddlPaymentTerm1.Items.Count > 0)
                                        {
                                            foreach (ListItem eachVendor in ddlPaymentTerm1.Items)
                                            {
                                                if (eachVendor.Selected)
                                                {
                                                    if (Convert.ToInt64(eachVendor.Value) != -1)
                                                        lstPaymenttermMapping.Add(Convert.ToInt64(eachVendor.Value));
                                                }
                                            }
                                        }

                                        if (lstPaymenttermMapping.Count > 0)
                                        {
                                            List<Cont_tbl_PaymentTermMapping> lstPaymenttermMapping_ToSave = new List<Cont_tbl_PaymentTermMapping>();

                                            lstPaymenttermMapping.ForEach(EachPaymentterm =>
                                            {
                                                Cont_tbl_PaymentTermMapping paymenttermMappingRecord = new Cont_tbl_PaymentTermMapping()
                                                {
                                                    ContractID = newContractID,
                                                    PaymentTermID = EachPaymentterm,
                                                    IsDeleted = false,
                                                    CustomerID = Convert.ToInt64(AuthenticationHelper.CustomerID),
                                                };

                                                lstPaymenttermMapping_ToSave.Add(paymenttermMappingRecord);
                                            });

                                            ContractManagement.DeActiveExistingPaymentTermMapping(newContractID);

                                            saveSuccess = ContractManagement.CreateUpdate_PaymentTermMapping(lstPaymenttermMapping_ToSave);
                                            if (saveSuccess)
                                            {
                                                ContractManagement.CreateAuditLog("C", newContractID, "Cont_tbl_PaymentTermMapping", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Contract Payment Term Mapping Created", true, Convert.ToInt32(newContractID));
                                            }

                                            //Refresh List
                                            lstPaymenttermMapping_ToSave.Clear();
                                            lstPaymenttermMapping_ToSave = null;

                                            lstPaymenttermMapping.Clear();
                                            lstPaymenttermMapping = null;
                                        }
                                        #endregion

                                        #region Vendor Mapping

                                        if (lstBoxVendor.Items.Count > 0)
                                        {
                                            foreach (ListItem eachVendor in lstBoxVendor.Items)
                                            {
                                                if (eachVendor.Selected)
                                                {
                                                    if (Convert.ToInt64(eachVendor.Value) != 0)
                                                        lstVendorMapping.Add(Convert.ToInt64(eachVendor.Value));
                                                }
                                            }
                                        }

                                        if (lstVendorMapping.Count > 0)
                                        {
                                            List<Cont_tbl_VendorMapping> lstVendorMapping_ToSave = new List<Cont_tbl_VendorMapping>();

                                            lstVendorMapping.ForEach(EachVendor =>
                                            {
                                                Cont_tbl_VendorMapping _vendorMappingRecord = new Cont_tbl_VendorMapping()
                                                {
                                                    ContractID = newContractID,
                                                    VendorID = EachVendor,
                                                    IsActive = true,
                                                    CreatedBy = AuthenticationHelper.UserID,
                                                    CreatedOn = DateTime.Now,
                                                };

                                                lstVendorMapping_ToSave.Add(_vendorMappingRecord);
                                            });

                                            var existingVendorMapping = VendorDetails.GetVendorMapping(newContractID);
                                            var assignedVendorIDs = lstVendorMapping_ToSave.Select(row => row.VendorID).ToList();

                                            if (existingVendorMapping.Count != assignedVendorIDs.Count)
                                            {
                                                matchSuccess = false;
                                            }
                                            else
                                            {
                                                matchSuccess = existingVendorMapping.Except(assignedVendorIDs).ToList().Count > 0 ? false : true;
                                            }

                                            if (!matchSuccess)
                                            {
                                                VendorDetails.DeActiveExistingVendorMapping(newContractID);

                                                saveSuccess = VendorDetails.CreateUpdate_VendorMapping(lstVendorMapping_ToSave);
                                                if (saveSuccess)
                                                {
                                                    ContractManagement.CreateAuditLog("C", newContractID, "Cont_tbl_VendorMapping", "Update", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Contract Vendor Updated", true, Convert.ToInt32(newContractID));
                                                }
                                            }
                                            else
                                                saveSuccess = false;

                                            //Refresh List
                                            lstVendorMapping_ToSave.Clear();
                                            lstVendorMapping_ToSave = null;

                                            lstVendorMapping.Clear();
                                            lstVendorMapping = null;
                                        }

                                        #endregion

                                        #region Update Custom Field                                        

                                        if (!string.IsNullOrEmpty(Convert.ToString(ViewState["CustomefieldCount"])))
                                        {
                                            List<Cont_tbl_CustomFieldValue> lstObjParameters = new List<Cont_tbl_CustomFieldValue>();

                                            //Select Which Grid to Loop based on Selected Category/Type
                                            GridView gridViewToCollectData = null;
                                            gridViewToCollectData = grdCustomField;

                                            if (gridViewToCollectData != null)
                                            {
                                                for (int i = 0; i < gridViewToCollectData.Rows.Count; i++)
                                                {
                                                    Label lblID = (Label)gridViewToCollectData.Rows[i].FindControl("lblID");
                                                    TextBox tbxLabelValue = (TextBox)gridViewToCollectData.Rows[i].FindControl("tbxLabelValue");

                                                    if (lblID != null && tbxLabelValue != null)
                                                    {
                                                        if (!string.IsNullOrEmpty(lblID.Text) || lblID.Text != "0")
                                                        {
                                                            Cont_tbl_CustomFieldValue ObjParameter = new Cont_tbl_CustomFieldValue()
                                                            {
                                                                ContractID = newContractID,

                                                                LabelID = Convert.ToInt64(lblID.Text),
                                                                LabelValue = tbxLabelValue.Text,

                                                                IsDeleted = false,
                                                                CreatedBy = AuthenticationHelper.UserID,
                                                                CreatedOn = DateTime.Now,
                                                                UpdatedBy = AuthenticationHelper.UserID,
                                                                UpdatedOn = DateTime.Now
                                                            };

                                                            lstObjParameters.Add(ObjParameter);

                                                            //saveSuccess = ContractManagement.CreateUpdateCustomsField(ObjParameter);
                                                        }
                                                    }
                                                }//End For Each

                                                var existingCustomFields = ContractManagement.GetCustomsFieldsContractWise_All(Convert.ToInt32(AuthenticationHelper.CustomerID), newContractID, Convert.ToInt64(ddlContractType.SelectedValue));

                                                var lstSavedCustomFields = existingCustomFields.Select(row => row.LableID).ToList();
                                                var assignedCustomFields = lstObjParameters.Select(row => row.LabelID).ToList();

                                                matchSuccess = false;

                                                if (lstSavedCustomFields.Count != assignedCustomFields.Count)
                                                {
                                                    matchSuccess = false;
                                                }
                                                else
                                                {
                                                    matchSuccess = lstSavedCustomFields.Except(assignedCustomFields).ToList().Count > 0 ? false : true;
                                                }

                                                if (!matchSuccess)
                                                {
                                                    saveSuccess = ContractManagement.DeActiveExistingCustomsFieldsByContractID(newContractID);
                                                    saveSuccess = ContractManagement.CreateUpdate_CustomsFields(lstObjParameters);
                                                }
                                                else
                                                    saveSuccess = false;

                                                if (saveSuccess)
                                                {
                                                    ContractManagement.CreateAuditLog("C", newContractID, "Cont_tbl_CustomFieldValue", "Update", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Contract Custom Parameter(s) Updated", true, Convert.ToInt32(newContractID));
                                                }
                                            }
                                        }

                                        #endregion

                                        #region User Assignment                           

                                        List<Cont_tbl_UserAssignment> lstUserAssignmentRecord_ToSave = new List<Cont_tbl_UserAssignment>();
                                        lstContractUserMapping.Clear();

                                        List<string> SelectedOwnersList = new List<string>();
                                        int assignedRoleID = 3;
                                        if (lstBoxOwner.Items.Count > 0)
                                        {
                                            SelectedOwnersList.Clear();
                                            assignedRoleID = 3;
                                            foreach (ListItem eachOwner in lstBoxOwner.Items)
                                            {
                                                if (eachOwner.Selected)
                                                {
                                                    if (Convert.ToInt32(eachOwner.Value) != 0)
                                                    {
                                                        SelectedOwnersList.Add(eachOwner.Text);
                                                        lstContractUserMapping.Add(new Tuple<int, int>(Convert.ToInt32(eachOwner.Value), assignedRoleID));
                                                    }
                                                    //lstOwnerMapping.Add(Convert.ToInt32(eachOwner.Value));
                                                }
                                            }
                                        }

                                        if (lstBoxApprover.Items.Count > 0)
                                        {
                                            assignedRoleID = 6;
                                            foreach (ListItem eachApprover in lstBoxApprover.Items)
                                            {
                                                if (eachApprover.Selected)
                                                {
                                                    if (Convert.ToInt32(eachApprover.Value) != 0)
                                                        lstContractUserMapping.Add(new Tuple<int, int>(Convert.ToInt32(eachApprover.Value), assignedRoleID));
                                                    // lstApproverMapping.Add(Convert.ToInt32(eachApprover.Value));
                                                }
                                            }
                                        }

                                        if (lstContractUserMapping.Count > 0)
                                        {
                                            lstContractUserMapping.ForEach(eachUser =>
                                            {
                                                Cont_tbl_UserAssignment newAssignment = new Cont_tbl_UserAssignment()
                                                {
                                                    AssignmentType = 1,
                                                    ContractID = newContractID,

                                                    UserID = Convert.ToInt32(eachUser.Item1),
                                                    RoleID = Convert.ToInt32(eachUser.Item2),

                                                    IsActive = true,
                                                    CreatedBy = AuthenticationHelper.UserID,
                                                    UpdatedBy = AuthenticationHelper.UserID,
                                                };

                                                lstUserAssignmentRecord_ToSave.Add(newAssignment);

                                                //saveSuccess = ContractManagement.CreateUpdateContractUserAssignment(newAssignment);
                                            });
                                        }

                                        var existingUserAssignment = ContractManagement.GetContractUserAssignment_All(newContractID);

                                        var assignedUserIDs = existingUserAssignment.Select(row => row.UserID).ToList();
                                        var currentUserIDs = lstUserAssignmentRecord_ToSave.Select(row => row.UserID).ToList();

                                        matchSuccess = false;

                                        if (assignedUserIDs.Count != currentUserIDs.Count)
                                        {
                                            matchSuccess = false;
                                        }
                                        else
                                        {
                                            matchSuccess = assignedUserIDs.Except(currentUserIDs).ToList().Count > 0 ? false : true;
                                        }

                                        if (!matchSuccess)
                                        {
                                            //De-Active All Previous Assignment
                                            ContractManagement.DeActiveContractUserAssignments(newContractID, AuthenticationHelper.UserID);

                                            if (lstUserAssignmentRecord_ToSave.Count > 0)
                                                saveSuccess = ContractManagement.CreateUpdate_ContractUserAssignments(lstUserAssignmentRecord_ToSave);

                                            if (saveSuccess)
                                            {
                                                ContractManagement.CreateAuditLog("C", newContractID, "Cont_tbl_UserAssignment", "Update", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Contract Owner(s) Updated.(" + String.Join(",", SelectedOwnersList) + ")", true, Convert.ToInt32(newContractID));
                                                //ContractManagement.CreateAuditLog("C", newContractID, "Cont_tbl_UserAssignment", "Update", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Contract Owner(s) Updated", true);
                                            }
                                        }
                                        else
                                            saveSuccess = true;

                                        SelectedOwnersList.Clear();
                                        lstUserAssignmentRecord_ToSave.Clear();
                                        lstUserAssignmentRecord_ToSave = null;

                                        #endregion
                                    }

                                    if (saveSuccess)
                                    {
                                        #region update Contract reviewe
                                        //bool IsPerformercontract = ContractManagement.getcontractchecklist(customerID);
                                        if (IsOnlycontract3)
                                        {
                                            if (!string.IsNullOrEmpty(Request.QueryString["AccessRoleID"]))
                                            {
                                                if (Convert.ToInt32(Request.QueryString["AccessRoleID"]) == 4
                                                    || Convert.ToInt32(Request.QueryString["AccessRoleID"]) == 6
                                                    || Convert.ToInt32(Request.QueryString["AccessRoleID"]) == 38)
                                                {
                                                    Cont_tbl_ContractStatusTransaction newStatusRecord = new Cont_tbl_ContractStatusTransaction()
                                                    {
                                                        CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID),
                                                        ContractID = newContractID,
                                                        StatusID = Convert.ToInt32(ddlstatus.SelectedValue),
                                                        StatusChangeOn = DateTime.Now,
                                                        IsActive = true,
                                                        CreatedBy = AuthenticationHelper.UserID,
                                                        CreatedOn = DateTime.Now,
                                                        UpdatedBy = AuthenticationHelper.UserID,
                                                        UpdatedOn = DateTime.Now,
                                                    };

                                                    saveSuccess = ContractManagement.CreateContractStatusTransaction(newStatusRecord);

                                                    Cont_tbl_ContractTransaction objtran1 = new Cont_tbl_ContractTransaction();
                                                    objtran1.CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                                                    objtran1.ContractID = newContractID;
                                                    objtran1.StatusID = Convert.ToInt32(ddlstatus.SelectedValue);
                                                    objtran1.StatusChangeOn = DateTime.Now;
                                                    objtran1.IsActive = true;
                                                    objtran1.CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID);
                                                    objtran1.CreatedOn = DateTime.Now;
                                                    objtran1.UpdatedBy = Convert.ToInt32(AuthenticationHelper.UserID);
                                                    objtran1.UpdatedOn = DateTime.Now;
                                                    objtran1.Remark = tbxAddNewClause.Text;
                                                    DocumentManagement.CreateContractTrans(objtran1);
                                                }
                                            }
                                            else
                                            {
                                                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                                {
                                                    var ContractCurrentStatus = (from row in entities.Cont_View_RecentContractReviweStatusTransaction
                                                                                 where row.ContractID == newContractID
                                                                                 select row).FirstOrDefault();

                                                    if (ContractCurrentStatus.ContractStatusID == 1)
                                                    {
                                                        if (ddlReviewer.SelectedValue != null && ddlReviewer.SelectedValue != "" && ddlReviewer.SelectedValue != "-1")
                                                        {
                                                            ContractAssignment objass = new ContractAssignment();
                                                            objass.ContractInstanceID = newContractID;
                                                            objass.RoleID = 4;
                                                            objass.UserID = Convert.ToInt64(ddlReviewer.SelectedValue);
                                                            objass.IsActive = true;
                                                            DocumentManagement.CreateContractAssignment(objass);
                                                        }

                                                        if (ddlApprover.SelectedValue != null && ddlApprover.SelectedValue != "" && ddlApprover.SelectedValue != "-1")
                                                        {
                                                            ContractAssignment objass = new ContractAssignment();
                                                            objass.ContractInstanceID = newContractID;
                                                            objass.RoleID = 6;
                                                            objass.UserID = Convert.ToInt64(ddlApprover.SelectedValue);
                                                            objass.IsActive = true;
                                                            DocumentManagement.CreateContractAssignment(objass);
                                                        }

                                                        Cont_tbl_ContractTransaction objtran = new Cont_tbl_ContractTransaction();
                                                        objtran.CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                                                        objtran.ContractID = newContractID;
                                                        objtran.StatusID = 2;
                                                        objtran.StatusChangeOn = DateTime.Now;
                                                        objtran.IsActive = true;
                                                        objtran.CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID);
                                                        objtran.CreatedOn = DateTime.Now;
                                                        objtran.UpdatedBy = Convert.ToInt32(AuthenticationHelper.UserID);
                                                        objtran.UpdatedOn = DateTime.Now;
                                                        objtran.Remark = tbxAddNewClause.Text;
                                                        DocumentManagement.CreateContractTrans(objtran);

                                                        Cont_tbl_ContractStatusTransaction newStatusRecord = new Cont_tbl_ContractStatusTransaction()
                                                        {
                                                            CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID),
                                                            ContractID = newContractID,
                                                            StatusID = Convert.ToInt64(2),
                                                            StatusChangeOn = DateTime.Now,
                                                            IsActive = true,
                                                            CreatedBy = AuthenticationHelper.UserID,
                                                            CreatedOn = DateTime.Now,
                                                            UpdatedBy = AuthenticationHelper.UserID,
                                                            UpdatedOn = DateTime.Now,
                                                        };

                                                        saveSuccess = ContractManagement.CreateContractStatusTransaction(newStatusRecord);
                                                    }
                                                }
                                            }
                                        }
                                        #endregion
                                        cvContractPopUp.IsValid = false;
                                        cvContractPopUp.ErrorMessage = "Contract Detail(s) Updated Successfully.";
                                        VSContractPopup.CssClass = "alert alert-success";

                                        ViewState["Mode"] = 1;
                                        ViewState["ContractInstanceID"] = newContractID;

                                        toggleTextSaveButton_ContractSummaryTab(false); //false -- Switch to Edit Mode

                                        showHideTabs(true);
                                        showHideContractSummaryTabTopButtons(true);
                                        enableDisableContractSummaryTabControls(false);
                                    }
                                }
                            }
                        }

                        #endregion

                        if (saveSuccess)
                        {
                            liContractTransaction.Visible = true;
                            //liContractTransaction.Visible = false;
                            //bool IsPerformercontract = ContractManagement.getcontractchecklist(customerID);
                            if (IsOnlycontract3)
                                liMilestone.Visible = false;
                            else
                                liMilestone.Visible = true;

                            //liMilestone.Visible = false;
                            if (drdTemplateID.SelectedValue != null && drdTemplateID.SelectedValue != "" && drdTemplateID.SelectedValue != "-1")
                            {
                                liContractTransaction.Visible = true;
                                liMilestone.Visible = true;
                            }
                            int contractInstanceID = Convert.ToInt32(ViewState["ContractInstanceID"]);
                            BindSectionDetail(Convert.ToInt32(contractInstanceID));
                            BindFieldDetail(Convert.ToInt32(contractInstanceID));
                        }
                    }

                    #endregion

                    btnsavedraft.Visible = false;
                    //bool IsOnlycontract34 = ContractManagement.getcontractchecklist(Convert.ToInt32(AuthenticationHelper.CustomerID));
                    if (IsOnlycontract3)
                    {
                        if ((int)ViewState["Mode"] == 1)
                        {
                            if (ViewState["ContractInstanceID"] != null)
                            {
                                long contractInstanceID = Convert.ToInt64(ViewState["ContractInstanceID"]);
                                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                {
                                    var ContractCurrentStatus = (from row in entities.Cont_View_RecentContractReviweStatusTransaction
                                                                 where row.ContractID == contractInstanceID
                                                                 select row).FirstOrDefault();
                                    if (ContractCurrentStatus.ContractStatusID == 1)
                                    {
                                        btnsavedraft.Visible = true;
                                    }
                                }
                            }
                        }
                        else
                        {
                            btnsavedraft.Visible = true;
                        }
                    }

                   
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvContractPopUp.IsValid = false;
                cvContractPopUp.ErrorMessage = "Something went wrong, Please try again";
            }
        }
        public static bool ContractUserAssignments(long contractID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var record = (from row in entities.Cont_tbl_VisibleToUser
                                  where row.ContractInstanceID == contractID
                                  && row.IsActive==true
                                  select row).ToList();

                    if (record.Count > 0)
                    {
                        record.ForEach(entry => entry.IsActive = false);
                      //  record.ForEach(entry => entry.UpdatedBy = loggedInUserID);
                        record.ForEach(entry => entry.UpdatedOn = DateTime.Now);  
                        entities.SaveChanges();
                    }

                    return true;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public class GetFieldInformation
        {
            //public int FieldID { get; set; }
            public string FieldName { get; set; }
            public string FieldValue { get; set; }
        }

        public class selectedFieldInformation
        {
            public int FieldID { get; set; }
            public string FieldName { get; set; }
        }

        public class getTemplateTransactionDetail
        {
            public long TemplateTransactionID { get; set; }
            public long TemplateInstanceID { get; set; }
            public long ComplianceStatusID { get; set; }
            public System.DateTime CreatedOn { get; set; }
            public string Status { get; set; }
            public Nullable<System.DateTime> StatusChangedOn { get; set; }
            public string TemplateContent { get; set; }
            public long RoleID { get; set; }
            public long UserID { get; set; }
            public int sectionID { get; set; }
            public string SectionHeader { get; set; }
        }


        public int getRoleID(int ContractTemplateId, int UserID)
        {
            int RoleID = -1;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                ContractTemplateAssignment data = (from row in entities.ContractTemplateAssignments
                                                   where row.ContractTemplateInstanceID == ContractTemplateId
                                                    && row.UserID == UserID
                                                   select row).FirstOrDefault();
                if (data != null)
                {
                    RoleID = Convert.ToInt32(data.RoleID);
                }
            }
            return RoleID;
        }
        #endregion

        //public bool ExporttoWordNew(int ContractTemplateID, int CustomerID, int TemplateID,string TemplateName)
        //{
        //    bool resultdata = false;

        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        var lstSections = (from row in entities.SP_ContractTemplateData(ContractTemplateID, CustomerID, TemplateID)
        //                           select row).FirstOrDefault();
        //        if (lstSections != null)
        //        {
        //            resultdata = true;
        //            System.Text.StringBuilder strExporttoWord = new System.Text.StringBuilder();
        //            string fileName = TemplateName;
        //            string templateName = TemplateName;

        //            //string fileName = lstSections.TemplateName + "-" + lstSections.Version;
        //            //string templateName = lstSections.TemplateName;

        //            strExporttoWord.Append(@"
        //        <html 
        //        xmlns:o='urn:schemas-microsoft-com:office:office' 
        //        xmlns:w='urn:schemas-microsoft-com:office:word'
        //        xmlns='http://www.w3.org/TR/REC-html40'>
        //        <head><title></title>

        //        <!--[if gte mso 9]>
        //        <xml>
        //        <w:WordDocument>
        //        <w:View>Print</w:View>
        //        <w:Zoom>90</w:Zoom>
        //        <w:DoNotOptimizeForBrowser/>
        //        </w:WordDocument>
        //        </xml>
        //        <![endif]-->

        //        <style>
        //        p.MsoFooter, li.MsoFooter, div.MsoFooter
        //        {
        //        margin:0in;
        //        margin-bottom:.0001pt;
        //        mso-pagination:widow-orphan;
        //        tab-stops:center 3.0in right 6.0in;
        //        font-size:12.0pt;
        //        }
        //        <style>

        //        <!-- /* Style Definitions */

        //        @page Section1
        //        {
        //        size:8.5in 11.0in; 
        //        margin:1.0in 1.25in 1.0in 1.25in ;
        //        mso-header-margin:.5in;
        //        mso-header: h1;
        //        mso-footer: f1; 
        //        mso-footer-margin:.5in;
        //         font-family:Arial;
        //        }

        //        div.Section1
        //        {
        //        page:Section1;
        //        }
        //        table#hrdftrtbl
        //        {
        //            margin:0in 0in 0in 9in;
        //        }

        //        .break { page-break-before: always; }
        //        -->
        //        </style></head>");

        //            strExporttoWord.Append(@"<body lang=EN-US style='tab-interval:.5in'> <div class=Section1>");

        //            strExporttoWord.Append(lstSections);

        //            strExporttoWord.Append(@"<table id='hrdftrtbl' border='1' cellspacing='0' cellpadding='0'>
        //        <tr>
        //            <td>
        //                <div style='mso-element:header' id=h1 >
        //                    <p class=MsoHeader style='text-align:left'>
        //                </div>
        //            </td>
        //            <td>
        //                <div style='mso-element:footer' id=f1>
        //                    <p class=MsoFooter>
        //                    <span style=mso-tab-count:2'></span>
        //                    <span style='mso-field-code:"" PAGE ""'></span>
        //                    of <span style='mso-field-code:"" NUMPAGES ""'></span>
        //                    </p>
        //                </div>
        //            </td>
        //        </tr>
        //        </table>
        //        </body>
        //        </html>");

        //            fileName = fileName + "-" + DateTime.Now.ToString("ddMMyyyy");

        //            Response.AppendHeader("Content-Type", "application/msword");
        //            Response.AppendHeader("Content-disposition", "attachment; filename=" + fileName + ".doc");
        //            Response.Write(strExporttoWord);
        //        }
        //    }
        //    return resultdata;
        //}


        #region Custom Field/Parameter
        private void intializeDataTableCustomField(GridView gridViewCustomField)
        {
            try
            {
                DataTable dtCustomField = new DataTable();

                DataRow drowCustomField = null;

                dtCustomField.Columns.Add(new DataColumn("LableID", typeof(string)));
                dtCustomField.Columns.Add(new DataColumn("Label", typeof(string)));
                dtCustomField.Columns.Add(new DataColumn("LabelValue", typeof(string)));

                drowCustomField = dtCustomField.NewRow();

                drowCustomField["LableID"] = string.Empty;
                drowCustomField["Label"] = string.Empty;
                drowCustomField["LabelValue"] = string.Empty;

                dtCustomField.Rows.Add(drowCustomField);

                ViewState["dataTableCustomFields"] = dtCustomField;

                gridViewCustomField.Visible = true;
                //gridViewCustomField_History.Visible = false;

                gridViewCustomField.DataSource = dtCustomField; /*Assign datasource to create one row with default values for the class you have*/
                gridViewCustomField.DataBind();

                //To Hide row
                gridViewCustomField.Rows[0].Visible = false;
                gridViewCustomField.Rows[0].Controls.Clear();

                //lblAddNewGround.Visible = true;               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindCustomFields(GridView gridViewCustomField) //, GridView gridViewCustomField_History
        {
            try
            {
                long contractInstanceID = 0;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["ContractInstanceID"])))
                {
                    contractInstanceID = Convert.ToInt64(ViewState["ContractInstanceID"]);
                }
                if (ddlContractType.SelectedValue != null && ddlContractType.SelectedValue != "0")  //&& CaseInstanceID != 0
                {
                    List<Cont_SP_GetCustomFieldsValues_Result> lstCustomFields = new List<Cont_SP_GetCustomFieldsValues_Result>();

                    if (contractInstanceID != 0)
                    {
                        lstCustomFields = ContractManagement.GetCustomsFieldsContractWise_All(Convert.ToInt32(AuthenticationHelper.CustomerID), contractInstanceID, Convert.ToInt64(ddlContractType.SelectedValue));

                        bool historyFlag = false;
                        if (ViewState["FlagHistory"] != null)
                        {
                            historyFlag = Convert.ToBoolean(Convert.ToInt32(ViewState["FlagHistory"]));
                        }

                        if (lstCustomFields != null && lstCustomFields.Count > 0)
                        {
                            ViewState["CustomefieldCount"] = lstCustomFields.Count;

                            gridViewCustomField.DataSource = lstCustomFields;
                            gridViewCustomField.DataBind();
                            gridViewCustomField.Visible = true;

                            if (!historyFlag)
                            {

                            }
                            else if (historyFlag)
                            {

                            }
                        }
                        else
                        {
                            Cont_SP_GetCustomFieldsValues_Result obj = new Cont_SP_GetCustomFieldsValues_Result(); //initialize empty class that may contain properties
                            lstCustomFields.Add(obj); //Add empty object to list

                            gridViewCustomField.DataSource = lstCustomFields; /*Assign datasource to create one row with default values for the class you have*//*Assign datasource to create one row with default values for the class you have*/
                            gridViewCustomField.DataBind(); //Bind that empty source     

                            //To Hide row
                            gridViewCustomField.Rows[0].Visible = false;
                            gridViewCustomField.Rows[0].Controls.Clear();

                            gridViewCustomField.Visible = true;

                            if (!historyFlag)
                            {

                            }
                            else if (historyFlag)
                            {

                            }
                        }
                    }
                    else
                    {
                        intializeDataTableCustomField(grdCustomField); //, grdCustomField_History
                    }

                    lblCustomField.Visible = true;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindCustomFieldDropDown(DropDownList ddlCustomField, GridView gridViewCustomField)
        {
            try
            {
                if (ddlContractType.SelectedValue != null && ddlContractType.SelectedValue != "" && ddlContractType.SelectedValue != "0")
                {
                    var customFields = ContractManagement.GetCustomsFieldsByContractType(Convert.ToInt32(AuthenticationHelper.CustomerID), Convert.ToInt64(ddlContractType.SelectedValue));

                    if (customFields.Count > 0)
                    {
                        //lblAddNewGround.Visible = true;
                        gridViewCustomField.Visible = true;

                        if (ddlCustomField.Items.Count > 0)
                            ddlCustomField.Items.Clear();

                        ddlCustomField.DataTextField = "Label";
                        ddlCustomField.DataValueField = "ID";

                        ddlCustomField.DataSource = customFields;
                        ddlCustomField.DataBind();

                        //ddlCustomField.Items.Add(new ListItem("Add New", "0"));

                        ViewState["ddlCustomFieldFilled"] = "1";
                    }
                    else
                    {
                        //lblAddNewGround.Visible = false;
                        gridViewCustomField.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdCustomField_Common_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                GridView gridView = (GridView)sender;

                if (gridView != null)
                {
                    if (e.Row.RowType == DataControlRowType.DataRow)
                    {
                        TextBox tbxLabelValue = (TextBox)e.Row.FindControl("tbxLabelValue");

                        if (!string.IsNullOrEmpty(Request.QueryString["HistoryFlag"]))
                        {
                            tbxLabelValue.Enabled = false;
                        }

                        //Hide Delete in Case of Total or Row with 0 ID
                        Label lblID = (Label)e.Row.FindControl("lblID");
                        LinkButton lnkBtnDeleteCustomField = (LinkButton)e.Row.FindControl("lnkBtnDeleteCustomField");

                        if (lblID != null && lnkBtnDeleteCustomField != null)
                        {
                            if (lblID.Text != "" && lblID.Text != "0")
                            {
                                e.Row.Enabled = true;
                                lnkBtnDeleteCustomField.Visible = true;
                            }
                            else
                            {
                                e.Row.Enabled = false;
                                lnkBtnDeleteCustomField.Visible = false;
                            }
                        }
                    }

                    if (e.Row.RowType == DataControlRowType.Footer)
                    {
                        DropDownList ddlFieldName_Footer = (DropDownList)e.Row.FindControl("ddlFieldName_Footer");

                        if (ddlFieldName_Footer != null)
                        {
                            BindCustomFieldDropDown(ddlFieldName_Footer, gridView);

                            foreach (GridViewRow gvr in gridView.Rows)
                            {
                                Label lblID = (Label)gvr.FindControl("lblID");

                                if (lblID != null)
                                {
                                    if (lblID.Text != "")
                                    {
                                        if (ddlFieldName_Footer.Items.FindByValue(lblID.Text) != null)
                                            ddlFieldName_Footer.Items.Remove(ddlFieldName_Footer.Items.FindByValue(lblID.Text));
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdCustomField_Common_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                GridView gridView = (GridView)sender;

                if (gridView != null)
                {
                    gridView.PageIndex = e.NewPageIndex;
                    BindCustomFields(grdCustomField); //, grdCustomField_History
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdCustomField_Common_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                long contractInstanceID = 0;
                bool deleteSuccess = false;

                if (ViewState["ContractInstanceID"] != null && e.CommandName.Equals("DeleteCustomField") && e.CommandArgument != null && ViewState["Mode"] != null)
                {
                    if ((int)ViewState["Mode"] == 0 && ViewState["dataTableCustomFields"] != null)
                    {
                        GridViewRow gvRow = (GridViewRow)(((Control)e.CommandSource).NamingContainer);
                        if (gvRow != null)
                        {
                            //GridViewRow gvRow = (GridViewRow)(sender).Parent.Parent;
                            int index = gvRow.RowIndex;

                            DataTable dtCustomField = ViewState["dataTableCustomFields"] as DataTable;
                            dtCustomField.Rows[index].Delete();

                            ViewState["dataTableCustomFields"] = dtCustomField;
                            ViewState["CustomefieldCount"] = dtCustomField.Rows.Count;

                            GridView gridView = (GridView)sender;

                            if (gridView != null)
                            {
                                gridView.DataSource = dtCustomField; /*Assign datasource to create one row with default values for the class you have*/
                                gridView.DataBind();
                            }
                        }
                    }
                    else if ((int)ViewState["Mode"] == 1)
                    {
                        long LableID = Convert.ToInt64(e.CommandArgument);

                        contractInstanceID = Convert.ToInt64(ViewState["ContractInstanceID"]);

                        if (contractInstanceID != 0 && LableID != 0)
                        {
                            deleteSuccess = ContractManagement.DeleteCustomFieldsContractWise(contractInstanceID, LableID);
                            if (deleteSuccess)
                            {
                                LitigationManagement.CreateAuditLog("C", contractInstanceID, "Cont_tbl_CustomFieldValue", "Delete", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Custom Field Deleted", true);

                                BindCustomFields(grdCustomField); //, grdCustomField_History
                            }
                        }
                    }
                }


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lnkBtnAddCustomField_Click(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["Mode"] != null)
                {
                    long lblID = 0;

                    DropDownList ddlFieldName_Footer = (DropDownList)grdCustomField.FooterRow.FindControl("ddlFieldName_Footer");
                    TextBox txtFieldValue_Footer = (TextBox)grdCustomField.FooterRow.FindControl("txtFieldValue_Footer");

                    if (ddlFieldName_Footer != null && txtFieldValue_Footer != null)
                    {
                        if ((int)ViewState["Mode"] == 0)
                        {
                            string lblName = ddlFieldName_Footer.SelectedItem.Text;
                            lblID = Convert.ToInt64(ddlFieldName_Footer.SelectedValue);

                            if (ViewState["dataTableCustomFields"] != null && lblID != 0)
                            {
                                DataTable dtcurrentTableCumtomFields = (DataTable)ViewState["dataTableCustomFields"];

                                DataRow drNewRow = dtcurrentTableCumtomFields.NewRow();

                                drNewRow["LableID"] = lblID;
                                drNewRow["Label"] = lblName;
                                drNewRow["LabelValue"] = txtFieldValue_Footer.Text;

                                //add new row to DataTable
                                dtcurrentTableCumtomFields.Rows.Add(drNewRow);

                                //Delete Rows with blank LblID (if Any)
                                dtcurrentTableCumtomFields = LitigationManagement.LoopAndDeleteBlankRows(dtcurrentTableCumtomFields);

                                ViewState["CustomefieldCount"] = dtcurrentTableCumtomFields.Rows.Count;

                                //Store the current data to ViewState
                                ViewState["CurrentTablePutCallDtls"] = dtcurrentTableCumtomFields;


                                //Rebind the Grid with the current data
                                grdCustomField.DataSource = dtcurrentTableCumtomFields;
                                grdCustomField.DataBind();

                            }
                        }//Add Mode End
                        else if ((int)ViewState["Mode"] == 1)
                        {
                            if (ViewState["ContractInstanceID"] != null)
                            {
                                long contractInstanceID = Convert.ToInt64(ViewState["ContractInstanceID"]);

                                if (txtFieldValue_Footer != null && ddlFieldName_Footer != null && contractInstanceID != 0)
                                {
                                    bool validateData = false;
                                    bool saveSuccess = false;

                                    if (!String.IsNullOrEmpty(ddlFieldName_Footer.SelectedValue) && ddlFieldName_Footer.SelectedValue != "0")
                                    {
                                        if (txtFieldValue_Footer.Text != "")
                                        {
                                            validateData = true;
                                        }
                                    }

                                    if (validateData)
                                    {
                                        lblID = Convert.ToInt64(ddlFieldName_Footer.SelectedValue);

                                        if (lblID != 0)
                                        {
                                            Cont_tbl_CustomFieldValue ObjParameter = new Cont_tbl_CustomFieldValue()
                                            {
                                                ContractID = contractInstanceID,
                                                LabelID = lblID,
                                                LabelValue = txtFieldValue_Footer.Text,
                                                IsDeleted = false,
                                                CreatedBy = AuthenticationHelper.UserID,
                                                CreatedOn = DateTime.Now,
                                                UpdatedBy = AuthenticationHelper.UserID,
                                            };

                                            saveSuccess = ContractManagement.CreateUpdateCustomsField(ObjParameter);

                                            if (saveSuccess)
                                            {
                                                ContractManagement.CreateAuditLog("C", contractInstanceID, "Cont_tbl_CustomFieldValue", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Custom Field Added", true, Convert.ToInt32(contractInstanceID));

                                                if (ViewState["ContractTypeIDUpdated"] != null)
                                                {
                                                    if (ViewState["ContractTypeIDUpdated"].ToString() == "false")
                                                    {
                                                        saveSuccess = ContractManagement.UpdateContractTypeID(contractInstanceID, Convert.ToInt64(ddlContractType.SelectedValue));
                                                        if (saveSuccess)
                                                        {
                                                            saveSuccess = ContractManagement.DeletePreviousCustomFields(contractInstanceID, Convert.ToInt64(ddlContractType.SelectedValue));

                                                            if (saveSuccess)
                                                                ViewState["ContractTypeIDUpdated"] = "true";
                                                        }
                                                    }
                                                }

                                                BindCustomFields(grdCustomField); //, grdCustomField_History

                                            }
                                        }
                                    }
                                }
                            }
                        }//Edit Mode End
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void TextChangedInsideGridView_TextChanged(object sender, EventArgs e)
        {
            GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent;
            if (currentRow != null)
            {
                if (currentRow.RowType == DataControlRowType.DataRow)
                {
                    TextBox tbxLabelValue = (TextBox)currentRow.FindControl("tbxLabelValue");
                    TextBox tbxInterestValue = (TextBox)currentRow.FindControl("tbxInterestValue");
                    TextBox tbxPenaltyValue = (TextBox)currentRow.FindControl("tbxPenaltyValue");
                    TextBox tbxRowTotalValue = (TextBox)currentRow.FindControl("tbxRowTotalValue");

                    if (tbxLabelValue != null && tbxInterestValue != null && tbxPenaltyValue != null && tbxRowTotalValue != null)
                    {
                        tbxRowTotalValue.Text = (LitigationManagement.csvToNumber(tbxLabelValue.Text) +
                            LitigationManagement.csvToNumber(tbxInterestValue.Text) +
                            LitigationManagement.csvToNumber(tbxPenaltyValue.Text)).ToString("N2");
                    }
                }
                else if (currentRow.RowType == DataControlRowType.Footer)
                {
                    TextBox txtFieldValue_Footer = (TextBox)currentRow.FindControl("txtFieldValue_Footer");
                    TextBox txtInterestValue_Footer = (TextBox)currentRow.FindControl("txtInterestValue_Footer");
                    TextBox txtPenaltyValue_Footer = (TextBox)currentRow.FindControl("txtPenaltyValue_Footer");
                    TextBox tbxRowTotalValue_Footer = (TextBox)currentRow.FindControl("tbxRowTotalValue_Footer");

                    if (txtFieldValue_Footer != null && txtInterestValue_Footer != null && txtPenaltyValue_Footer != null && tbxRowTotalValue_Footer != null)
                    {
                        tbxRowTotalValue_Footer.Text = (LitigationManagement.csvToNumber(txtFieldValue_Footer.Text) +
                            LitigationManagement.csvToNumber(txtInterestValue_Footer.Text) +
                            LitigationManagement.csvToNumber(txtPenaltyValue_Footer.Text)).ToString("N2");
                    }
                }
            }
        }

        protected void grdCustomField_History_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblIsAllowed = (Label)e.Row.FindControl("lblIsAllowed");
                    DropDownList ddlGroundResult = (DropDownList)e.Row.FindControl("ddlGroundResult");

                    if (ddlGroundResult != null && lblIsAllowed != null)
                    {
                        if (lblIsAllowed.Text != null && lblIsAllowed.Text != "")
                        {
                            string isAllowed = "0";

                            if (lblIsAllowed.Text == "True")
                                isAllowed = "1";
                            else if (lblIsAllowed.Text == "False")
                                isAllowed = "0";

                            ddlGroundResult.ClearSelection();

                            if (ddlGroundResult.Items.FindByValue(isAllowed) != null)
                                ddlGroundResult.Items.FindByValue(isAllowed).Selected = true;
                        }
                    }

                    Label lblPenalty = (Label)e.Row.FindControl("lblPenalty");

                    if (lblPenalty != null)
                    {
                        if (string.IsNullOrEmpty(lblPenalty.Text))
                            lblPenalty.Visible = false;
                    }

                    Label lblInterest = (Label)e.Row.FindControl("lblInterest");

                    if (lblInterest != null)
                    {
                        if (string.IsNullOrEmpty(lblInterest.Text))
                            lblInterest.Visible = false;
                    }

                    Label lblSettlementValue = (Label)e.Row.FindControl("lblSettlementValue");

                    if (lblSettlementValue != null)
                    {
                        if (string.IsNullOrEmpty(lblSettlementValue.Text))
                            lblSettlementValue.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        #endregion

        #region Contract - Linking

        private void BindLinkedContracts(long contractID)
        {
            if (contractID != 0)
            {
                var lstLinkedContracts = ContractManagement.GetLinkedContractList_All(contractID, Convert.ToInt32(AuthenticationHelper.CustomerID));

                grdLinkedContracts.DataSource = lstLinkedContracts;
                grdLinkedContracts.DataBind();

                if (lstLinkedContracts.Count > 0)
                    divLinkedContracts.Visible = true;
                else
                    divLinkedContracts.Visible = false;

                lstLinkedContracts.Clear();
                lstLinkedContracts = null;
                upLinkedContracts.Update();
            }
        }

        private void BindContractListToLink(long contactID)
        {

            if (contactID != 0)
            {
                long customerID = -1;
                customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;
                int vendorID = -1;
                int deptID = -1;
                long contractStatusID = -1;
                long contractTypeID = -1;
                List<int> branchList = new List<int>();
                var lstAssignedContracts = ContractManagement.GetAssignedContractsList(Convert.ToInt32(customerID), AuthenticationHelper.UserID, AuthenticationHelper.Role,
                    3, branchList, vendorID, deptID, contractStatusID, contractTypeID);

                if (lstAssignedContracts.Count > 0)
                    lstAssignedContracts = lstAssignedContracts.Where(row => row.ID != contactID).ToList();

                var lstAlreadyLinkedContracts = ContractManagement.GetLinkedContractIDs(contactID, Convert.ToInt32(customerID));

                if (lstAlreadyLinkedContracts.Count > 0)
                    lstAssignedContracts = lstAssignedContracts.Where(row => !lstAlreadyLinkedContracts.Contains(row.ID)).ToList();

                grdContractList_LinkContract.DataSource = lstAssignedContracts;
                grdContractList_LinkContract.DataBind();

                lstAssignedContracts.Clear();
                lstAssignedContracts = null;

            }
        }
        protected void grdLinkedContracts_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                bool deleteSuccess = false;
                if (e.CommandArgument != null && ViewState["ContractInstanceID"] != null)
                {
                    if (e.CommandName.Equals("ViewLinkedContract"))
                    {
                        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                        long contractID = Convert.ToInt64(commandArgs[0]);
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "scriptHistoryPopUp", "OpenContractHistoryPopup(" + contractID + "," + " true" + ",'L'" + ");", true);
                    }
                    else if (e.CommandName.Equals("DeleteContractLinking"))
                    {
                        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                        long contractID = Convert.ToInt64(commandArgs[0]);
                        long LinkedContractID = Convert.ToInt64(commandArgs[1]);

                        if (contractID != 0 && LinkedContractID != 0)
                        {
                            deleteSuccess = ContractManagement.DeleteContractLinking(contractID, LinkedContractID, AuthenticationHelper.UserID);

                            if (deleteSuccess)
                            {
                                BindLinkedContracts(contractID);
                                ContractManagement.CreateAuditLog("C", contractID, "Cont_tbl_ContractLinking", "Delete", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Contract Linking Deleted", true, Convert.ToInt32(contractID));

                                BindContractListToLink(contractID);

                                cvLinkedContracts.IsValid = false;
                                cvLinkedContracts.ErrorMessage = "Contract Linking Deleted Successfully";
                                vsLinkedContracts.CssClass = "alert alert-success";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdLinkedContracts_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    LinkButton lnkBtnDeleteContractLinking = (LinkButton)e.Row.FindControl("lnkBtnDeleteContractLinking");
                    if (lnkBtnDeleteContractLinking != null)
                    {
                        if (ViewState["ContractStatusID"] != null)
                        {
                            if (Convert.ToInt32(ViewState["ContractStatusID"]) == 9) //9-Renewed
                                lnkBtnDeleteContractLinking.Visible = false;
                            else
                                lnkBtnDeleteContractLinking.Visible = true;
                        }

                        if (!string.IsNullOrEmpty(Request.QueryString["HistoryFlag"]))
                        {
                            lnkBtnDeleteContractLinking.Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnSaveLinkContract_Click(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["PageLink"].Equals("ContractLink"))
                {
                    bool saveSuccess = false;
                    List<long> lstContractsToLink = new List<long>();
                    int totalRecordSaveCount = 0;

                    if (ViewState["ContractInstanceID"] != null)
                    {
                        long contractID = Convert.ToInt64(ViewState["ContractInstanceID"]);

                        for (int i = 0; i < grdContractList_LinkContract.Rows.Count; i++)
                        {
                            CheckBox chkRowLinkContracts = (CheckBox)grdContractList_LinkContract.Rows[i].FindControl("chkRowLinkContracts");

                            if (chkRowLinkContracts != null)
                            {
                                if (chkRowLinkContracts.Checked)
                                {
                                    Label lblContractIID = (Label)grdContractList_LinkContract.Rows[i].FindControl("lblContractIID");

                                    if (lblContractIID != null)
                                    {
                                        if (lblContractIID.Text != "")
                                            lstContractsToLink.Add(Convert.ToInt64(lblContractIID.Text));
                                    }
                                }
                            }
                        }

                        if (lstContractsToLink.Count > 0)
                        {
                            lstContractsToLink.ForEach(eachContractToLink =>
                            {
                                Cont_tbl_ContractLinking newRecord = new Cont_tbl_ContractLinking()
                                {
                                    ContractID = contractID,
                                    LinkedContractID = eachContractToLink,
                                    IsActive = true,
                                    CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID),

                                    CreatedBy = AuthenticationHelper.UserID,
                                    CreatedOn = DateTime.Now,

                                    UpdatedBy = AuthenticationHelper.UserID,
                                    UpdatedOn = DateTime.Now,
                                };

                                saveSuccess = ContractManagement.CreateUpdateContractLinking(newRecord);


                                if (saveSuccess)
                                    totalRecordSaveCount++;
                            });
                        }

                        if (saveSuccess)
                        {
                            BindLinkedContracts(contractID);
                            BindContractListToLink(contractID);
                            BindFileTags();
                            ContractManagement.CreateAuditLog("C", contractID, "Cont_tbl_ContractLinking", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, totalRecordSaveCount + " Contract(s) Linked", true, Convert.ToInt32(contractID));

                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "scriptUnCheckAllGridCheckBox", "unCheckAll('<%=grdContractList_LinkContract.ClientID %>');", true);

                            cvLinkContract.IsValid = false;
                            cvLinkContract.ErrorMessage = "Contract(s) Linked Successfully.";
                            //cvLinkContract.ErrorMessage = totalRecordSaveCount + "\t \t Contract(s) Linked Successfully";
                            vsLinkContract.CssClass = "alert alert-success";
                        }
                    }

                }
                else
                {
                    bool saveSuccess = false;
                    List<long> lstContractsToLink = new List<long>();
                    int totalRecordSaveCount = 0;

                    if (ViewState["ContractInstanceID"] != null)
                    {
                        long contractID = Convert.ToInt64(ViewState["ContractInstanceID"]);

                        for (int i = 0; i < grdContractList_LinkContract.Rows.Count; i++)
                        {
                            CheckBox chkRowLinkContracts = (CheckBox)grdContractList_LinkContract.Rows[i].FindControl("chkRowLinkContracts");

                            if (chkRowLinkContracts != null)
                            {
                                if (chkRowLinkContracts.Checked)
                                {
                                    Label lblContractIID = (Label)grdContractList_LinkContract.Rows[i].FindControl("lblContractIID");

                                    if (lblContractIID != null)
                                    {
                                        if (lblContractIID.Text != "")
                                            lstContractsToLink.Add(Convert.ToInt64(lblContractIID.Text));
                                    }
                                }
                            }
                        }


                        if (lstContractsToLink.Count > 0)
                        {
                            lstContractsToLink.ForEach(eachContractToLink =>
                            {
                                if (ViewState["ContractInstanceID"] != null)
                                {
                                    if (!string.IsNullOrEmpty(ViewState["ContractInstanceID"].ToString()))
                                    {
                                        
                                        int ContractInstanceID = Convert.ToInt32(ViewState["ContractInstanceID"]);

                                        if (ContractInstanceID > 0)
                                        {
                                            List<Cont_tbl_FileData> objFileData = new List<Cont_tbl_FileData>();
                                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                            {
                                                objFileData = (from row in entities.Cont_tbl_FileData
                                                               where row.IsDeleted == false
                                                               && row.ContractID == eachContractToLink
                                                               select row).ToList();

                                                #region Upload Document
                                                foreach (var item in objFileData)
                                                {
                                                    Cont_tbl_FileData objContDoc = new Cont_tbl_FileData()
                                                    {
                                                        ContractID = ContractInstanceID,
                                                        CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID),
                                                        DocTypeID = item.DocTypeID,
                                                        FileName = item.FileName,
                                                        FilePath = item.FilePath,
                                                        // FileTags = item.FileTags,
                                                        FileKey = item.FileKey,
                                                        Version = item.Version,
                                                        VersionDate = item.VersionDate,
                                                        CreatedBy = AuthenticationHelper.UserID,
                                                        CreatedOn = DateTime.Now,


                                                    };
                                                    entities.Cont_tbl_FileData.Add(objContDoc);
                                                    entities.SaveChanges();

                                                    List<Cont_tbl_FileDataTagsMapping> objTag = new List<Cont_tbl_FileDataTagsMapping>();
                                                    objTag = (from row in entities.Cont_tbl_FileDataTagsMapping
                                                              where row.FileID == item.ID
                                                              && row.IsActive == true
                                                              select row).ToList();

                                                    foreach (var Tagitem in objTag)
                                                    {
                                                        Cont_tbl_FileDataTagsMapping ObjFileTag = new Cont_tbl_FileDataTagsMapping
                                                        {
                                                        FileID=objContDoc.ID,
                                                        FileTag= Tagitem.FileTag,
                                                        IsActive=true,
                                                        CreatedBy=AuthenticationHelper.UserID,
                                                        CreatedOn=DateTime.Now
                                                        };
                                                        entities.Cont_tbl_FileDataTagsMapping.Add(ObjFileTag);
                                                        entities.SaveChanges();
                                                    }
                                                }
                                            }
                                            saveSuccess = true;
                                            if (saveSuccess)
                                            {
                                                ContractManagement.CreateAuditLog("C", ContractInstanceID, "Cont_tbl_FileData", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Contract Document(s) Uploaded", true, Convert.ToInt32(ViewState["ContractInstanceID"]));
                                                // BindContractDocuments_Paging();
                                            }

                                           /* if (saveSuccess)
                                            {
                                                cvContractDocument.IsValid = false;
                                                cvContractDocument.ErrorMessage = "Document(s) uploaded successfully";
                                                vsContractDocument.CssClass = "alert alert-success";
                                            }
                                            else
                                            {
                                                cvContractDocument.IsValid = false;
                                                cvContractDocument.ErrorMessage = "Select Document(s) to Upload";
                                            }
                                            */
                                           
                                            #endregion
                                        }
                                    }
                                    //saveSuccess = ContractManagement.CreateUpdateContractLinking(newRecord);
                                    if (saveSuccess)
                                        totalRecordSaveCount++;
                                }

                            });
                        }
                        if (saveSuccess)
                        {
                           
                            BindContractDocuments_All();
                            BindFileTags();

                            //ContractManagement.CreateAuditLog("C", contractID, "Cont_tbl_ContractLinking", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, totalRecordSaveCount + " Contract(s) Linked", true);

                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "scriptUnCheckAllGridCheckBox", "unCheckAll('<%=grdContractList_LinkContract.ClientID %>');", true);

                            cvLinkContract.IsValid = false;
                            cvLinkContract.ErrorMessage = totalRecordSaveCount + "\t \t Document(s) Linked Successfully";
                            vsLinkContract.CssClass = "alert alert-success";
                        }
                    }
                   
                }
               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        #endregion

        #region Contract-Document
        protected void lnkBtn_RebindContractDoc_Click(object sender, EventArgs e)
        {
            BindFileTags();
            BindContractDocuments_Paging();
        }

        public void BindContractDocuments_Paging()
        {
            try
            {
                if (ViewState["ContractInstanceID"] != null)
                {
                    long ContractInstanceID = Convert.ToInt64(ViewState["ContractInstanceID"]);

                    int PageNumber = 1;
                    if (!string.IsNullOrEmpty(DropDownListPageNo.SelectedValue) && DropDownListPageNo.SelectedValue != "0")
                    {
                        PageNumber = Convert.ToInt32(DropDownListPageNo.SelectedValue);
                    }

                    List<Cont_SP_GetContractDocuments_Paging_Result> lstContDocs = new List<Cont_SP_GetContractDocuments_Paging_Result>();

                    lstContDocs = ContractDocumentManagement.GetContractDocuments_Paging(Convert.ToInt32(AuthenticationHelper.CustomerID), ContractInstanceID, Convert.ToInt32(grdContractDocuments.PageSize), PageNumber);

                    if (lstContDocs.Count > 0)
                    {
                        lstContDocs = lstContDocs.OrderBy(row => row.FileName).ThenByDescending(row => row.VersionDate).ToList();
                    }

                    grdContractDocuments.DataSource = lstContDocs;
                    grdContractDocuments.DataBind();

                    int Totalcnt = 0;
                    if (lstContDocs.Count > 0)
                    {
                        BindFileTags();

                        foreach (var item in lstContDocs)
                        {
                            Totalcnt = Convert.ToInt32(item.TotalRowCount);
                            break;
                        }
                    }

                    Session["TotalRows"] = Totalcnt;
                    //lblStartRecord.Text = DropDownListPageNo.SelectedValue;
                    if (Convert.ToInt32(ViewState["PageNumberFlagID"]) == 0)
                    {
                        bindPageNumber(1);
                    }

                    upContractDocUploadPopup.Update();
                    upContractDocUploadPopup1.Update();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvContractDocument.IsValid = false;
                cvContractDocument.ErrorMessage = "Server Error Occurred. Please try again.";
                cvContractDocument.CssClass = "alert alert-danger";
            }
        }
        protected void lnkBtn_RebindContractTaskDoc_Click(object sender, EventArgs e)
        {
            BindContractDocuments_All(grdTaskContractDocuments);
        }
        public void BindContractDocuments_All(GridView grdtoBindDocuments)
        {
            try
            {
                if (ViewState["ContractInstanceID"] != null)
                {
                    long ContractInstanceID = Convert.ToInt64(ViewState["ContractInstanceID"]);

                    List<Cont_SP_GetContractDocuments_All_Result> lstContDocs = new List<Cont_SP_GetContractDocuments_All_Result>();

                    lstContDocs = ContractDocumentManagement.GetContractDocuments_All(Convert.ToInt32(AuthenticationHelper.CustomerID), ContractInstanceID);

                    grdtoBindDocuments.DataSource = lstContDocs;
                    grdtoBindDocuments.DataBind();
                    lstContDocs.Clear();
                    lstContDocs = null;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindContractDocuments_All()
        {
            try
            {
                if (ViewState["ContractInstanceID"] != null)
                {
                    long ContractInstanceID = Convert.ToInt64(ViewState["ContractInstanceID"]);

                    List<ListItem> selectedItems = ContractManagement.GetSelectedItems(lstBoxFileTags);

                    var selectedFileTags = selectedItems.Select(row => row.Text).ToList();

                    List<Cont_SP_GetContractDocuments_FileTags_All_Result> lstContDocs = new List<Cont_SP_GetContractDocuments_FileTags_All_Result>();

                    lstContDocs = ContractDocumentManagement.Cont_SP_GetContractDocuments_FileTags_All(Convert.ToInt32(AuthenticationHelper.CustomerID), ContractInstanceID, selectedFileTags);

                    grdContractDocuments.DataSource = lstContDocs;
                    grdContractDocuments.DataBind();

                    Session["TotalRows"] = lstContDocs.Count;
                    bindPageNumber(1);

                    lstContDocs.Clear();
                    lstContDocs = null;

                    upContractDocUploadPopup.Update();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindTaskDocuments_All(GridView grdtoBindDocuments, long contractID, long taskID)
        {
            try
            {
                if (contractID != 0 && taskID != 0)
                {
                    List<Cont_SP_GetContractDocuments_All_Result> lstContDocs = new List<Cont_SP_GetContractDocuments_All_Result>();

                    lstContDocs = ContractDocumentManagement.GetContractDocuments_All(Convert.ToInt32(AuthenticationHelper.CustomerID), contractID);

                    grdtoBindDocuments.DataSource = lstContDocs;
                    grdtoBindDocuments.DataBind();

                    if (lstContDocs.Count > 0)
                    {
                        var prevTaskDocumentMapping = ContractTaskManagement.GetTaskDocumentMapping(contractID, taskID);

                        if (prevTaskDocumentMapping != null)
                        {
                            if (prevTaskDocumentMapping.Count > 0)
                            {
                                long fileID = 0;

                                for (int i = 0; i < grdtoBindDocuments.Rows.Count; i++)
                                {
                                    if (grdtoBindDocuments.Rows[i].RowType == DataControlRowType.DataRow)
                                    {
                                        Label lblFileID = (Label)grdtoBindDocuments.Rows[i].FindControl("lblFileID");

                                        if (lblFileID != null)
                                        {
                                            fileID = Convert.ToInt64(lblFileID.Text);

                                            if (fileID != 0)
                                            {
                                                if (prevTaskDocumentMapping.Contains(fileID))
                                                {
                                                    CheckBox chkRowTaskDocument = (CheckBox)grdtoBindDocuments.Rows[i].FindControl("chkRowTaskDocument");

                                                    if (chkRowTaskDocument != null)
                                                    {
                                                        chkRowTaskDocument.Checked = true;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                } //END FOR
                            }
                        }

                        prevTaskDocumentMapping.Clear();
                        prevTaskDocumentMapping = null;

                    }

                    lstContDocs.Clear();
                    lstContDocs = null;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


        protected bool uploadDocuments(long contractID, long docTypeID, HttpFileCollection fileCollection, string fileUploadControlName, long? taskID, long? taskResponseID, string fileTags)
        {
            bool uploadSuccess = false;
            try
            {
                #region Upload Document

                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                string directoryPath = string.Empty;
                string fileName = string.Empty;

                Cont_tbl_FileData objContDoc = new Cont_tbl_FileData()
                {
                    ContractID = contractID,
                    DocTypeID = docTypeID,
                    TaskID = taskID,
                    TaskResponseID = taskResponseID,

                    CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID),
                    CreatedBy = AuthenticationHelper.UserID,
                    CreatedOn = DateTime.Now,
                    IsDeleted = false,

                };


                if (fileCollection.Count > 0)
                {
                    List<KeyValuePair<string, Byte[]>> fileList = new List<KeyValuePair<string, Byte[]>>();

                    if (contractID > 0)
                    {
                        for (int i = 0; i < fileCollection.Count; i++)
                        {
                            HttpPostedFile uploadedFile = fileCollection[i];

                            if (uploadedFile.ContentLength > 0)
                            {
                                string[] keys1 = fileCollection.Keys[i].Split('$');
                                if (keys1[keys1.Count() - 1].Equals(fileUploadControlName))
                                {
                                    fileName = uploadedFile.FileName;
                                }
                                objContDoc.FileName = fileName;                                
                                if (taskID == null && taskResponseID == null)
                                    directoryPath = Server.MapPath("~/ContractDocuments/" + customerID + "/" + Convert.ToInt64(contractID) + "/" + docTypeID + "/" + objContDoc.Version);
                                else if (taskID != null && taskResponseID == null)
                                    directoryPath = Server.MapPath("~/ContractDocuments/" + customerID + "/" + Convert.ToInt64(contractID) + "/" + taskID + "/" + docTypeID + "/" + objContDoc.Version);
                                else if (taskID != null && taskResponseID != null)
                                    directoryPath = Server.MapPath("~/ContractDocuments/" + customerID + "/" + Convert.ToInt64(contractID) + "/" + taskID + "/" + taskResponseID + "/" + docTypeID + "/" + objContDoc.Version);

                               
                                Guid fileKey1 = Guid.NewGuid();
                                string finalPath1 = Path.Combine(directoryPath, fileKey1 + Path.GetExtension(uploadedFile.FileName));
                                Stream fs = uploadedFile.InputStream;
                                BinaryReader br = new BinaryReader(fs);
                                Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                                fileList.Add(new KeyValuePair<string, Byte[]>(finalPath1, bytes));

                                objContDoc.FilePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                objContDoc.FileKey = fileKey1.ToString();
                                objContDoc.VersionDate = DateTime.Now;
                                objContDoc.CreatedOn = DateTime.Now;
                                objContDoc.FileSize = uploadedFile.ContentLength;

                                DocumentManagement.Contract_SaveDocFiles(fileList);
                                long newFileID = ContractDocumentManagement.CreateContractDocumentMapping(objContDoc);
                                
                                if (newFileID > 0 & !string.IsNullOrEmpty(fileTags))
                                {
                                    string[] arrFileTags = fileTags.Trim().Split(',');

                                    if (arrFileTags.Length > 0)
                                    {
                                        List<Cont_tbl_FileDataTagsMapping> lstFileTagMapping = new List<Cont_tbl_FileDataTagsMapping>();

                                        for (int j = 0; j < arrFileTags.Length; j++)
                                        {
                                            Cont_tbl_FileDataTagsMapping objFileTagMapping = new Cont_tbl_FileDataTagsMapping()
                                            {
                                                FileID = newFileID,
                                                FileTag = arrFileTags[j].Trim(),
                                                IsActive = true,
                                                CreatedBy = AuthenticationHelper.UserID,
                                                CreatedOn = DateTime.Now,
                                                UpdatedBy = AuthenticationHelper.UserID,
                                                UpdatedOn = DateTime.Now,
                                            };

                                            lstFileTagMapping.Add(objFileTagMapping);
                                        }

                                        if (lstFileTagMapping.Count > 0)
                                        {
                                            ContractDocumentManagement.CreateUpdate_FileTagsMapping(lstFileTagMapping);
                                        }
                                    }
                                }

                                fileList.Clear();
                            }

                        }//End For Each
                    }
                }

                #endregion

                return uploadSuccess;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return uploadSuccess;
            }
        }

        protected bool shareDocuments(GridView grdDocuments, long contractID, long taskID)
        {
            bool uploadSuccess = false;
            try
            {
                List<long> lstFileIDstoShare = new List<long>();
                List<Cont_tbl_TaskDocumentMapping> lstFilestoShare = new List<Cont_tbl_TaskDocumentMapping>();

                for (int i = 0; i < grdDocuments.Rows.Count; i++)
                {
                    CheckBox chkRowTaskDocument = (CheckBox)grdDocuments.Rows[i].FindControl("chkRowTaskDocument");

                    if (chkRowTaskDocument != null)
                    {
                        if (chkRowTaskDocument.Checked)
                        {
                            Label lblFileID = (Label)grdDocuments.Rows[i].FindControl("lblFileID");

                            if (lblFileID != null)
                            {
                                if (lblFileID.Text != "")
                                    lstFileIDstoShare.Add(Convert.ToInt64(lblFileID.Text));
                            }
                        }
                    }
                }

                if (lstFileIDstoShare.Count > 0)
                {
                    lstFileIDstoShare.ForEach(eachFile =>
                    {
                        Cont_tbl_TaskDocumentMapping objTaskDocShare = new Cont_tbl_TaskDocumentMapping()
                        {
                            ContractID = contractID,
                            TaskID = Convert.ToInt64(taskID),
                            FileID = eachFile,
                            CreatedBy = AuthenticationHelper.UserID,
                            CreatedOn = DateTime.Now,
                            UpdatedBy = AuthenticationHelper.UserID,
                            IsActive = true,
                        };

                        lstFilestoShare.Add(objTaskDocShare);
                    });

                    var exisingSharedFileIDs = ContractTaskManagement.GetTaskDocumentMapping(contractID, taskID);
                    var currentFileIDs = lstFilestoShare.Select(row => row.FileID).ToList();

                    bool matchSuccess = ContractCommonMethods.matchLists(exisingSharedFileIDs, currentFileIDs);

                    if (!matchSuccess)
                    {
                        ContractTaskManagement.DeActive_TaskDocumentMapping(contractID, taskID, AuthenticationHelper.UserID);

                        if (lstFilestoShare.Count > 0)
                            uploadSuccess = ContractTaskManagement.CreateUpdate_TaskDocumentMapping(lstFilestoShare);
                        else
                            uploadSuccess = false;
                    }
                    else
                        uploadSuccess = true;
                }

                return uploadSuccess;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return uploadSuccess;
            }
        }

        protected void rptDocmentVersionView_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                // var AllinOneDocumentList=null;
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                if (e.CommandName.Equals("View"))
                {
                    if (e.CommandArgument != null)
                    {
                        long fileID = Convert.ToInt64(e.CommandArgument);
                        if (fileID != 0)
                            ViewContractDocument(fileID);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                // cvDuplicateEntry.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        protected void rptDocmentVersionView_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);

                LinkButton lblDocumentVersionView = (LinkButton)e.Item.FindControl("lblDocumentVersionView");
                scriptManager.RegisterAsyncPostBackControl(lblDocumentVersionView);
            }
        }

        protected bool CanDownloadDoc()
        {
            try
            {
                bool result = true;
                if (ShowDownLoadIcon)
                {
                    if (DocViewResult != "0")
                    {
                        result = true;
                    }
                    else
                    {
                        result = false;
                    }
                }
                else
                {
                    result = true;
                }

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return false;
        }

        protected void grdContDocuments_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandArgument != null)
                {
                    GridView senderGridView = sender as GridView;

                    if (senderGridView != null)
                    {
                        CustomValidator cvtoShowErrorMsg = null;

                        if (senderGridView.ID == "grdTaskContractDocuments")
                            cvtoShowErrorMsg = cvTaskTab;
                        else
                            cvtoShowErrorMsg = cvContractDocument;

                        if (e.CommandName.Equals("DownloadContDoc"))
                        {
                            bool downloadSuccess = DownloadContractDocument(Convert.ToInt64(e.CommandArgument));

                            if (!downloadSuccess)
                            {
                                cvContractDocument.IsValid = false;
                                cvContractDocument.ErrorMessage = "Sorry!No File To Download";
                            }
                        }
                        else if (e.CommandName.Equals("DeleteContDoc"))
                        {
                            DeleteContractDocument(Convert.ToInt64(e.CommandArgument));

                            //Bind Contract Related Documents
                            if (ViewState["ContractInstanceID"] != null)
                            {
                                BindFileTags();
                                BindContractDocuments_Paging();
                            }
                        }
                        else if (e.CommandName.Equals("Info_Doc"))
                        {
                            if (ViewState["ContractInstanceID"] != null)
                            {
                                long fileID = Convert.ToInt64(e.CommandArgument);
                                long contractID = Convert.ToInt64(e.CommandArgument);

                                ScriptManager.RegisterStartupScript(this, this.GetType(), "showDocInfo", "OpenDocInfoPopup('" + fileID + "','" + contractID + "');", true);
                            }
                        }
                        else if (e.CommandName.Equals("shareContDoc"))
                        {
                            if (ViewState["ContractInstanceID"] != null)
                            {
                                int fID = Convert.ToInt32(e.CommandArgument);                               

                                long contractID = Convert.ToInt64(ViewState["ContractInstanceID"].ToString());
                                List<int> fileIds = new List<int>();
                                fileIds.Add(fID);
                                var fileID = String.Join(",", fileIds);

                                ScriptManager.RegisterStartupScript(this, this.GetType(), "showDocInfo", "OpenDocSharePopup('" + fileID + "','" + contractID + "');", true);
                            }
                        }
                        else if (e.CommandName.Equals("View_Doc"))
                        {
                            var AllinOneDocumentList = ContractDocumentManagement.GetContractDocumentByID(Convert.ToInt64(e.CommandArgument));

                            if (AllinOneDocumentList != null)
                            {
                                DocumentPath = string.Empty;

                                string filePath = Path.Combine(Server.MapPath(AllinOneDocumentList.FilePath), AllinOneDocumentList.FileKey + Path.GetExtension(AllinOneDocumentList.FileName));
                                if (AllinOneDocumentList.FilePath != null && File.Exists(filePath))
                                {
                                    string fileExtension = System.IO.Path.GetExtension(filePath);

                                    if (fileExtension.ToUpper() == ".ZIP" || fileExtension.ToUpper() == ".7Z" || fileExtension.ToUpper() == ".RAR")
                                    {
                                        lblMessage.Text = "";
                                        lblMessage.Text = "Compressed file(s) can not be preview, Please try to download file(s)";
                                    }
                                    else
                                    {
                                        string Folder = "~/TempFiles";
                                        string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                                        string DateFolder = Folder + "/" + File;
                                        string extension = System.IO.Path.GetExtension(filePath);
                                        Directory.CreateDirectory(Server.MapPath(DateFolder));
                                        if (!Directory.Exists(DateFolder))
                                        {
                                            Directory.CreateDirectory(Server.MapPath(DateFolder));
                                        }
                                        string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);
                                        string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                        string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;
                                        string FileName = DateFolder + "/" + User + "" + extension;
                                        FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                        BinaryWriter bw = new BinaryWriter(fs);
                                        if (AllinOneDocumentList.EnType == "M")
                                        {
                                            bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        else
                                        {
                                            bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        bw.Close();
                                        DocumentPath = FileName;
                                        DocumentPath = DocumentPath.Substring(2, DocumentPath.Length - 2);
                                    }

                                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenDocviewer('" + DocumentPath + "');", true);
                                }
                                else
                                {
                                    lblMessage.Text = "There is no file to preview";
                                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenDocviewer('" + DocumentPath + "');", true);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


        protected void grdContDocuments_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (ViewState["ContractInstanceID"] != null)
                {
                    grdContractDocuments.PageIndex = e.NewPageIndex;
                    BindContractDocuments_Paging();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public bool DownloadContractDocument(long contFileID)
        {
            bool downloadSuccess = false;
            try
            {
                var file = ContractDocumentManagement.GetContractDocumentByID(contFileID);

                if (file != null)
                {
                    if (file.FilePath != null)
                    {
                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                        if (filePath != null && File.Exists(filePath))
                        {
                            Response.Buffer = true;
                            Response.Clear();
                            Response.ClearContent();
                            Response.ClearHeaders();
                            Response.ContentType = "application/octet-stream";
                            Response.AddHeader("content-disposition", "attachment; filename=" + DocumentManagement.MakeValidFileName(file.FileName));
                            if (file.EnType == "M")
                            {
                                Response.BinaryWrite(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                            }
                            else
                            {
                                Response.BinaryWrite(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                            }
                            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                            ContractManagement.CreateAuditLog("CT", Convert.ToInt32(ViewState["ContractInstanceID"]), "Cont_tbl_FileData", "Download", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Contract Document Downloaded", true, Convert.ToInt32(ViewState["ContractInstanceID"]));

                            downloadSuccess = true;
                        }
                    }
                }

                return downloadSuccess;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return downloadSuccess;
            }
        }

        public void DeleteContractDocument(long contFileID)
        {
            try
            {
                if (contFileID != 0)
                {
                    bool deleteSuccess = false;
                    deleteSuccess = ContractDocumentManagement.DeleteContDocument(contFileID, AuthenticationHelper.UserID);
                    if (deleteSuccess)
                    {
                        ContractDocumentManagement.Delete_FileTagsMapping(contFileID, AuthenticationHelper.UserID);

                        BindFileTags();

                        cvContractDocument.IsValid = false;
                        cvContractDocument.ErrorMessage = "Document Deleted Successfully";
                        vsContractDocument.CssClass = "alert alert-success";

                        if (ViewState["ContractInstanceID"] != null)
                        {
                            ContractManagement.CreateAuditLog("CT", Convert.ToInt32(ViewState["ContractInstanceID"]), "Cont_tbl_FileData", "Delete", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Contract Document Deleted", true, Convert.ToInt32(ViewState["ContractInstanceID"]));
                        }
                    }
                    else
                    {
                        cvContractDocument.IsValid = false;
                        cvContractDocument.ErrorMessage = "Something went wrong, Please try again.";
                        vsContractDocument.CssClass = "alert alert-danger";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvContractDocument.IsValid = false;
                cvContractDocument.ErrorMessage = "Server Error Occurred. Please try again.";
                vsContractDocument.CssClass = "alert alert-danger";
            }
        }

        public bool ViewContractDocument(long contFileID)
        {
            bool viewSuccess = false;
            try
            {
                var file = ContractDocumentManagement.GetContractDocumentByID(contFileID);

                if (file != null)
                {
                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                    if (file.FilePath != null && File.Exists(filePath))
                    {
                        string Folder = "~/TempFiles";
                        string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                        string DateFolder = Folder + "/" + File;

                        string extension = System.IO.Path.GetExtension(filePath);

                        Directory.CreateDirectory(Server.MapPath(DateFolder));

                        if (!Directory.Exists(DateFolder))
                        {
                            Directory.CreateDirectory(Server.MapPath(DateFolder));
                        }

                        string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

                        string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                        string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                        string FileName = DateFolder + "/" + User + "" + extension;

                        FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                        BinaryWriter bw = new BinaryWriter(fs);
                        if (file.EnType == "M")
                        {
                            bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                        }
                        else
                        {
                            bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                        }
                        bw.Close();
                        DocumentPath = FileName;

                        DocumentPath = DocumentPath.Substring(2, DocumentPath.Length - 2);

                        lblMessage.Text = "";


                    }

                }

                return viewSuccess;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return viewSuccess;
            }
        }

        public void BindTaskResponses(long contractID, long taskID)
        {
            try
            {
                List<Cont_SP_GetTaskResponses_All_Result> lstTaskResponses = new List<Cont_SP_GetTaskResponses_All_Result>();

                lstTaskResponses = ContractTaskManagement.GetTaskResponses(contractID, taskID);

                if (lstTaskResponses != null && lstTaskResponses.Count > 0)
                {
                    lstTaskResponses = lstTaskResponses.OrderByDescending(entry => entry.StatusChangeOn).ToList();

                    //lstTaskResponses = lstTaskResponses.OrderByDescending(entry => entry.UpdatedOn).ThenByDescending(entry => entry.CreatedOn).ToList();
                    grdTaskResponseLog.DataSource = lstTaskResponses;
                    grdTaskResponseLog.DataBind();

                    divTaskResponseLog.Visible = true;
                }
                else
                {
                    clearTaskResponseLog();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvTaskTab.IsValid = false;
                cvTaskTab.ErrorMessage = "Something went wrong, Please try again";
                vsContractDocument.CssClass = "alert alert-danger";
            }
        }
        public void clearTaskResponseLog()
        {
            divTaskResponseLog.Visible = false;
            grdTaskResponseLog.DataSource = null;
            grdTaskResponseLog.DataBind();
        }

        public void clearTaskAuditLog()
        {
            collapseAuditLog.Visible = false;
            grdTransactionHistory.DataSource = null;
            grdTransactionHistory.DataBind();
        }

        protected void grdTaskResponseLog_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandArgument != null && ViewState["ContractInstanceID"] != null)
                {
                    long contractID = Convert.ToInt64(ViewState["ContractInstanceID"]);

                    if (e.CommandName.Equals("DownloadTaskResponseDoc"))
                    {
                        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                        long taskResponseID = Convert.ToInt64(commandArgs[0]);
                        long taskID = Convert.ToInt64(commandArgs[1]);

                        if (taskResponseID != 0 && taskID != 0 && contractID != 0)
                        {
                            var lstTaskResponseDocument = ContractTaskManagement.GetTaskResponseDocuments(contractID, taskID, taskResponseID);

                            if (lstTaskResponseDocument.Count > 0)
                            {
                                using (ZipFile responseDocZip = new ZipFile())
                                {
                                    int i = 0;
                                    foreach (var file in lstTaskResponseDocument)
                                    {
                                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                        if (file.FilePath != null && File.Exists(filePath))
                                        {                                            
                                            int idx = file.FileName.LastIndexOf('.');
                                            string str = file.FileName.Substring(0, idx) + "_" + file.Version + "." + file.FileName.Substring(idx + 1);

                                            if (!responseDocZip.ContainsEntry(file.DocTypeID + "/" + str))
                                            {
                                                if (file.EnType == "M")
                                                {
                                                    responseDocZip.AddEntry(file.DocTypeID + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                else
                                                {
                                                    responseDocZip.AddEntry(file.DocTypeID + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                            }
                                            i++;
                                        }
                                    }

                                    var zipMs = new MemoryStream();
                                    responseDocZip.Save(zipMs);
                                    zipMs.Position = 0;
                                    byte[] Filedata = zipMs.ToArray();
                                    Response.Buffer = true;
                                    Response.ClearContent();
                                    Response.ClearHeaders();
                                    Response.Clear();
                                    Response.ContentType = "application/zip";
                                    Response.AddHeader("content-disposition", "attachment; filename=TaskResponseDocuments-" + DateTime.Now.ToString("dd-MM-yyyy") + ".zip");
                                    Response.BinaryWrite(Filedata);
                                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                                }
                            }
                            else
                            {
                                cvTaskTab.IsValid = false;
                                cvTaskTab.ErrorMessage = "No Document Available for Download.";
                                cvTaskTab.CssClass = "alert alert-danger";
                                return;
                            }
                        }
                    }
                    else if (e.CommandName.Equals("ViewTaskResponseDoc"))
                    {
                        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                        long taskResponseID = Convert.ToInt64(commandArgs[0]);
                        long taskID = Convert.ToInt64(commandArgs[1]);

                        if (taskResponseID != 0 && taskID != 0 && contractID != 0)
                        {
                            var lstTaskDocument = ContractTaskManagement.GetTaskResponseDocuments(contractID, taskID, taskResponseID);

                            if (lstTaskDocument.Count > 0)
                            {
                                List<Cont_tbl_FileData> entitiesData = lstTaskDocument.Where(entry => entry.Version != null).ToList();

                                if (lstTaskDocument.Where(entry => entry.Version == null).ToList().Count > 0)
                                {
                                    Cont_tbl_FileData entityData = new Cont_tbl_FileData();
                                    entityData.Version = "1.0";
                                    entityData.ContractID = contractID;
                                    entityData.TaskID = taskID;
                                    entityData.TaskResponseID = taskResponseID;
                                    entitiesData.Add(entityData);
                                }

                                if (entitiesData.Count > 0)
                                {
                                    rptDocmentVersionView.DataSource = entitiesData.OrderBy(entry => entry.Version);
                                    rptDocmentVersionView.DataBind();

                                    //ViewContractDocument(entitiesData[0].ID);

                                    bool downloadSuccess = ViewContractDocument(entitiesData[0].ID);

                                    if (!downloadSuccess)
                                    {
                                        //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "HideShowTask_Script", "HideShowTaskDiv('true');", true);
                                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenDocviewer('" + DocumentPath + "');", true);
                                    }

                                    //foreach (var file in entitiesData)
                                    //{
                                    //    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                    //    if (file.FilePath != null && File.Exists(filePath))
                                    //    {
                                    //        string Folder = "~/TempFiles";
                                    //        string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                    //        string DateFolder = Folder + "/" + File;

                                    //        string extension = System.IO.Path.GetExtension(filePath);

                                    //        Directory.CreateDirectory(Server.MapPath(DateFolder));

                                    //        if (!Directory.Exists(DateFolder))
                                    //        {
                                    //            Directory.CreateDirectory(Server.MapPath(DateFolder));
                                    //        }

                                    //        string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

                                    //        string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                    //        string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                    //        string FileName = DateFolder + "/" + User + "" + extension;

                                    //        FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                    //        BinaryWriter bw = new BinaryWriter(fs);
                                    //        bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    //        bw.Close();
                                    //        DocumentPath = FileName;

                                    //        DocumentPath = DocumentPath.Substring(2, DocumentPath.Length - 2);

                                    //        lblMessage.Text = "";

                                    //        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenDocviewer('" + DocumentPath + "');", true);
                                    //    }
                                    //    else
                                    //    {
                                    //        lblMessage.Text = "There is no document available to preview";
                                    //    }
                                    //    break;
                                    //}
                                    //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);
                                }
                            }
                            else
                            {
                                cvTaskTab.IsValid = false;
                                cvTaskTab.ErrorMessage = "No Document Available for View.";
                                cvTaskTab.CssClass = "alert alert-danger";
                                return;
                            }
                        }
                    }
                    else if (e.CommandName.Equals("TaskReminder"))
                    {
                        bool sendSuccess = false;

                        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                        long taskID = Convert.ToInt64(commandArgs[0]);
                        int userID = Convert.ToInt32(commandArgs[1]);
                        int roleID = Convert.ToInt32(commandArgs[2]);

                        if (contractID != 0 && taskID != 0 && userID != 0 && roleID != 0)
                        {
                            var newTaskRecord = ContractTaskManagement.GetTaskDetailsByTaskID(contractID, taskID);

                            if (newTaskRecord != null)
                            {
                                List<Tuple<int, int>> lstTaskUserMapping = new List<Tuple<int, int>>();

                                lstTaskUserMapping.Add(new Tuple<int, int>(userID, roleID));

                                if (lstTaskUserMapping.Count > 0)
                                {
                                    sendSuccess = SendEmail_TaskAssignedUsers(Convert.ToInt32(AuthenticationHelper.CustomerID), contractID, newTaskRecord, lstTaskUserMapping, "Task Reminder");

                                    if (sendSuccess)
                                        ContractManagement.CreateAuditLog("C", Convert.ToInt64(contractID), "Cont_tbl_FileData", "Mail", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Email Sent with Contract Document(s)", true, Convert.ToInt32(taskID));
                                }
                            }
                        }

                        if (sendSuccess)
                        {
                            cvTaskTab.IsValid = false;
                            cvTaskTab.ErrorMessage = "An Email containing Task detail and access URL to provide response sent to assignee.";
                            vsTaskTab.CssClass = "alert alert-success";

                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "scrollUp_Script", "scrollUp();", true);
                        }
                        else
                        {
                            cvTaskTab.IsValid = false;
                            cvTaskTab.ErrorMessage = "Something went wrong, Please try again.";
                            vsTaskTab.CssClass = "alert alert-danger";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvTaskTab.IsValid = false;
                cvTaskTab.ErrorMessage = "Something went wrong, Please try again";
            }
        }


        protected void btnSendMail_Click(object sender, EventArgs e)
        {
            if (ViewState["ContractInstanceID"] != null)
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                string messageAll = string.Empty;
                long contractID = 0;

                bool formValidateSuccess = false;
                bool sendSuccess = false;

                List<string> lstTO = new List<string>();
                List<string> lstCc = new List<string>();
                List<string> lstBcc = new List<string>();

                List<string> lstErrorMsg = new List<string>();
                List<int> lstTaskUserMapping = new List<int>();
                string strReceiver = string.Empty;

                #region Data Validation

                if (String.IsNullOrEmpty(tbxMailTo.Text))
                    lstErrorMsg.Add("Provide at least One Email-ID to Send this Message");
                else
                {
                    var lstAllUsers = ContractUserManagement.GetAllUsers_Contract(customerID);

                    if (lstAllUsers.Count > 0)
                    {
                        string eachMail = string.Empty;
                        strReceiver = tbxMailTo.Text;
                        string[] arrEmails = strReceiver.Split(',');

                        for (int i = 0; i < arrEmails.Length; i++)
                        {
                            eachMail = arrEmails[i].Trim();
                            if (!string.IsNullOrEmpty(eachMail))
                            {
                                if (ZohoCRMAPI.IsValidEmail(eachMail))
                                {
                                    var userEmailExists = lstAllUsers.Where(row => row.Email == eachMail).FirstOrDefault();
                                    if (userEmailExists != null)
                                        lstTO.Add(arrEmails[i]);
                                    else
                                    {
                                        lstErrorMsg.Add("Please Check Email - '" + eachMail + "', You can able to Send Email only to Contract Product User(s) of Your Organization");
                                    }
                                }
                                else
                                {
                                    lstErrorMsg.Add("Invalid Email - '" + eachMail + "'");
                                }
                            }
                        }

                        if (lstTO.Count > 0)
                            formValidateSuccess = true;
                    }
                }

                if (String.IsNullOrEmpty(tbxMailMsg.Text))
                    lstErrorMsg.Add("Provide Message to Send");
                else
                    formValidateSuccess = true;

                int selectedDocumentCount = 0;
                foreach (GridViewRow eachGridRow in grdMailDocumentList.Rows)
                {
                    if (eachGridRow.RowType == DataControlRowType.DataRow)
                    {
                        CheckBox chkRowMailDocument = (CheckBox)eachGridRow.FindControl("chkRowMailDocument");

                        if (chkRowMailDocument != null)
                        {
                            if (chkRowMailDocument.Checked)
                                selectedDocumentCount++;
                        }
                    }
                }

                if (selectedDocumentCount == 0)
                    lstErrorMsg.Add("Provide Select at lease One Document to Send");
                else if (selectedDocumentCount > 0)
                    formValidateSuccess = true;

                if (lstErrorMsg.Count > 0)
                {
                    formValidateSuccess = false;
                    showErrorMessages(lstErrorMsg, cvMailDocument);
                }

                #endregion

                #region Send Mail

                if (formValidateSuccess)
                {
                    string mailMessageToSend = string.Empty;
                    contractID = Convert.ToInt64(ViewState["ContractInstanceID"]);

                    mailMessageToSend = getMailMessage(contractID, tbxMailMsg.Text);

                    if (!string.IsNullOrEmpty(mailMessageToSend))
                    {
                        List<Tuple<string, string>> attachmentwithPath = GetDocumentsToAttach();

                        string folderPath = string.Empty;
                        if (attachmentwithPath.Count > 0)
                        {
                            try
                            {
                                //EmailManager.SendGridMailwithAttachment(ConfigurationManager.AppSettings["SenderEmailAddress"], Convert.ToString(ConfigurationManager.AppSettings["ReplyEmailAddressName"]), lstTO, lstCc, lstBcc, "Litigation Case Summary with Documents", messageAll, attachmentwithPath);
                                sendSuccess = true;
                            }
                            catch (Exception ex)
                            {
                                sendSuccess = false;
                            }
                        }
                    }
                }

                if (sendSuccess)
                {
                    cvMailDocument.IsValid = false;
                    cvMailDocument.ErrorMessage = "Email Sent Successfully.";
                    //vsMailDocument.CssClass = "alert alert-success";
                    cvMailDocument.CssClass = "alert alert-success";

                    tbxMailMsg.Text = "";
                    tbxMailTo.Text = "";

                    ContractManagement.CreateAuditLog("C", Convert.ToInt64(contractID), "Cont_tbl_FileData", "Mail", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Email Sent with Contract Document(s)", true, Convert.ToInt32(contractID));
                }
                else
                {
                    cvMailDocument.IsValid = false;
                    cvMailDocument.ErrorMessage = "Please enter valid Email ID.";
                    cvMailDocument.CssClass = "alert alert-danger";
                }

                #endregion
            }
        }

        private string getMailMessage(long contractID, string msgText)
        {
            string message = string.Empty;
            var contractDetailRecord = ContractManagement.GetContractByID(contractID);

            if (contractDetailRecord != null)
            {
                string accessURL = string.Empty;
                URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(AuthenticationHelper.CustomerID));
                if (Urloutput != null)
                {
                    accessURL = Urloutput.URL;
                }
                else
                {
                    accessURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                }
                message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_Contract_DocumentWithSummary
                                   .Replace("@ContractNo", Convert.ToString(contractDetailRecord.ContractNo))
                                   .Replace("@ContractTitle", contractDetailRecord.ContractTitle)
                                   .Replace("@message", msgText)
                                   .Replace("@From", Convert.ToString(AuthenticationHelper.User))
                                   .Replace("@PortalURL", Convert.ToString(accessURL));

                //message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_Contract_DocumentWithSummary
                //                    .Replace("@ContractNo", Convert.ToString(contractDetailRecord.ContractNo))
                //                    .Replace("@ContractTitle", contractDetailRecord.ContractTitle)
                //                    .Replace("@message", msgText)
                //                    .Replace("@From", Convert.ToString(AuthenticationHelper.User))
                //                    .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]));

            }

            return message;
        }


        private List<Tuple<string, string>> GetDocumentsToAttach()
        {
            List<Tuple<string, string>> lstDocsToAttach = new List<Tuple<string, string>>();

            try
            {
                ArrayList DocumentList = new ArrayList();
                ArrayList DocumentFileNameList = new ArrayList();

                foreach (GridViewRow eachGridRow in grdMailDocumentList.Rows)
                {
                    if (eachGridRow.RowType == DataControlRowType.DataRow)
                    {
                        CheckBox chkRowMailDocument = (CheckBox)eachGridRow.FindControl("chkRowMailDocument");

                        if (chkRowMailDocument != null)
                        {
                            if (chkRowMailDocument.Checked)
                            {
                                Label lblID = (Label)eachGridRow.FindControl("lblID");
                                Label lblFileName = (Label)eachGridRow.FindControl("lblFileName");
                                Label lblFilePath = (Label)eachGridRow.FindControl("lblFilePath");

                                if (lblID != null)
                                {
                                    if (!string.IsNullOrEmpty(lblID.Text))
                                    {
                                        var documentRecord = ContractDocumentManagement.GetContractDocumentByID(Convert.ToInt64(lblID.Text));
                                        if (documentRecord != null)
                                        {
                                            string finalFilePath = string.Empty;
                                            string filePath = Path.Combine(Server.MapPath(documentRecord.FilePath), documentRecord.FileKey + Path.GetExtension(documentRecord.FileName));
                                            if (documentRecord.FilePath != null && File.Exists(filePath))
                                            {
                                                string folderPath = "~/TempFiles/" + DateTime.Now.ToString("ddMMyyyy");
                                                string extension = System.IO.Path.GetExtension(filePath);
                                                if (!Directory.Exists(folderPath))
                                                    Directory.CreateDirectory(Server.MapPath(folderPath));
                                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                                string userWiseFileName = documentRecord.FileName + "" + FileDate;
                                                finalFilePath = folderPath + "/" + userWiseFileName + "" + extension;
                                                FileStream fs = new FileStream(Server.MapPath(finalFilePath), FileMode.Create, FileAccess.ReadWrite);
                                                BinaryWriter bw = new BinaryWriter(fs);
                                                if (documentRecord.EnType == "M")
                                                {
                                                    bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                else
                                                {
                                                    bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                bw.Close();

                                                DocumentPath = finalFilePath;
                                                DocumentPath = DocumentPath.Substring(2, DocumentPath.Length - 2);

                                                //DocumentList.Add(lblID.Text);
                                                //DocumentFileNameList.Add(lblFilePath.Text);

                                                lstDocsToAttach.Add(new Tuple<string, string>(DocumentPath, finalFilePath));

                                                //ViewState["ListofFile"] = DocumentList;
                                                //ViewState["docsToAttach"] = lstDocsToAttach;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                }

                return lstDocsToAttach;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return lstDocsToAttach;
            }
        }

        public string ShowPriority(string priorityID)
        {
            try
            {
                if (!string.IsNullOrEmpty(priorityID))
                {
                    if (priorityID == "1")
                        return "High";
                    else if (priorityID == "2")
                        return "Medium";
                    else if (priorityID == "3")
                        return "Low";
                    else
                        return string.Empty;
                }
                else
                    return string.Empty;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return string.Empty;
            }
        }


        #endregion

        private int GetTotalPagesCount(int tabIndex) //TabIndex ---Bind Page No to which Drop down Page No
        {
            try
            {
                int totalPages = 0;
                int pageSize = 0;
                TotalRows.Value = Session["TotalRows"].ToString();

                if (tabIndex == 1) //Document Tab
                {
                    pageSize = Convert.ToInt32(grdContractDocuments.PageSize);
                }
                else if (tabIndex == 2) //Task Tab
                {
                    pageSize = Convert.ToInt32(grdTaskActivity.PageSize);
                }
                else if (tabIndex == 3) //Audit Log Tab
                {
                    pageSize = Convert.ToInt32(gvContractAuditLog.PageSize);
                }
                else if (tabIndex == 4) //Audit Log Tab
                {
                    pageSize = Convert.ToInt32(gridMilestone.PageSize);
                }

                if (pageSize != 0)
                {
                    totalPages = Convert.ToInt32(TotalRows.Value) / pageSize;

                    // total page item to be displyed
                    int pageItemRemain = Convert.ToInt32(TotalRows.Value) % pageSize;

                    // remaing no of pages
                    if (pageItemRemain > 0)// set total No of pages
                    {
                        totalPages = totalPages + 1;
                    }
                    else
                    {
                        totalPages = totalPages + 0;
                    }
                }

                return totalPages;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        private void bindPageNumber(int tabIndex) //TabIndex ---Bind Page No to which Drop down Page No
        {
            try
            {
                if (tabIndex != 0)
                {
                    int count = Convert.ToInt32(GetTotalPagesCount(tabIndex));


                    //Select Which Grid to Loop based on Selected Category/Type
                    DropDownListChosen.DropDownListChosen ddlPageNo = null;

                    if (tabIndex == 1) //Document Tab
                    {
                        ddlPageNo = DropDownListPageNo;
                    }
                    else if (tabIndex == 2) //Task Tab
                    {
                        ddlPageNo = ddlPageNo_Task;
                    }
                    else if (tabIndex == 3) //Audit Log Tab
                    {
                        ddlPageNo = ddlPageNo_AuditLog;
                    }
                    else if (tabIndex == 4) //MileStone Tab
                    {
                        ddlPageNo = DropDownListPageNoMilestone;
                    }

                    if (ddlPageNo.Items.Count > 0)
                    {
                        ddlPageNo.Items.Clear();
                    }

                    ddlPageNo.DataTextField = "ID";
                    ddlPageNo.DataValueField = "ID";

                    ddlPageNo.DataBind();
                    for (int i = 1; i <= count; i++)
                    {
                        string chkPageID = i.ToString();
                        ddlPageNo.Items.Add(chkPageID);
                    }
                    if (count > 0)
                    {
                        ddlPageNo.SelectedValue = ("1").ToString();
                    }
                    else if (count == 0)
                    {
                        ddlPageNo.Items.Add("0");
                        ddlPageNo.SelectedValue = ("0").ToString();
                    }

                    SetShowingRecords(tabIndex);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void SetShowingRecords(int tabIndex)//TabIndex ---Bind Page No to which Drop down Page No
        {
            try
            {
                if (tabIndex != 0)
                {
                    int PageSize = 0;
                    int PageNumber = 0;

                    int EndRecord = 0;
                    int TotalRecord = 0;
                    int TotalValue = 0;

                    if (tabIndex == 1) //Document Tab
                    {
                        PageSize = grdContractDocuments.PageSize;
                        PageNumber = Convert.ToInt32(DropDownListPageNo.SelectedValue);
                    }
                    else if (tabIndex == 2) //Task Tab
                    {
                        PageSize = grdTaskActivity.PageSize;
                        PageNumber = Convert.ToInt32(ddlPageNo_Task.SelectedValue);
                    }
                    else if (tabIndex == 3) //Audit Log Tab
                    {
                        PageSize = gvContractAuditLog.PageSize;
                        PageNumber = Convert.ToInt32(ddlPageNo_AuditLog.SelectedValue);
                    }
                    else if (tabIndex == 4) //Milestone Tab
                    {
                        PageSize = gridMilestone.PageSize;
                        PageNumber = Convert.ToInt32(DropDownListPageNoMilestone.SelectedValue);
                    }

                    TotalValue = PageSize * PageNumber;

                    if (Session["TotalRows"] != null)
                    {
                        TotalRecord = Convert.ToInt32(Session["TotalRows"]);
                        if (TotalRecord < TotalValue)
                        {
                            EndRecord = TotalRecord;
                        }
                        else
                        {
                            EndRecord = TotalValue;
                        }
                    }

                    if (tabIndex == 1)
                    {
                        if (TotalRecord != 0)
                            lblStartRecord.Text = Convert.ToString(PageSize * PageNumber - PageSize + 1);
                        else
                            lblStartRecord.Text = "0";

                        lblEndRecord.Text = Convert.ToString(EndRecord) + " ";
                        lblTotalRecord.Text = TotalRecord.ToString();
                    }
                    else if (tabIndex == 2)
                    {
                        if (TotalRecord != 0)
                            lblStartRecord_Task.Text = Convert.ToString(PageSize * PageNumber - PageSize + 1);
                        else
                            lblStartRecord_Task.Text = "0";

                        lblEndRecord_Task.Text = Convert.ToString(EndRecord) + " ";
                        lblTotalRecord_Task.Text = TotalRecord.ToString();
                    }
                    else if (tabIndex == 3)
                    {
                        if (TotalRecord != 0)
                            lblStartRecord_AuditLog.Text = Convert.ToString(PageSize * PageNumber - PageSize + 1);
                        else
                            lblStartRecord_AuditLog.Text = "0";

                        lblEndRecord_AuditLog.Text = Convert.ToString(EndRecord) + " ";
                        lblTotalRecord_AuditLog.Text = TotalRecord.ToString();
                    }
                    else if (tabIndex == 4)
                    {
                        if (TotalRecord != 0)
                            lblStartRecord_MileStone.Text = Convert.ToString(PageSize * PageNumber - PageSize + 1);
                        else
                            lblStartRecord_MileStone.Text = "0";

                        lblEndRecord_MileStone.Text = Convert.ToString(EndRecord) + " ";
                        lblTotalRecord_MileStone.Text = TotalRecord.ToString();
                    }

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }



        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DropDownListChosen.DropDownListChosen ddlSender = sender as DropDownListChosen.DropDownListChosen;

                if (ddlSender != null)
                {
                    int tabIndex = 0;
                    int chkSelectedPage = Convert.ToInt32(ddlSender.SelectedItem.ToString());

                    if (ddlSender.ID == "DropDownListPageNo")
                    {
                        ViewState["PageNumberFlagID"] = 1;

                        grdContractDocuments.PageIndex = chkSelectedPage - 1;
                        BindContractDocuments_Paging();

                        tabIndex = 1;
                    }
                    else if (ddlSender.ID == "ddlPageNo_Task")
                    {
                        if (ViewState["ContractInstanceID"] != null)
                        {
                            int customerID = -1;
                            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                            long contractInstanceID = Convert.ToInt64(ViewState["ContractInstanceID"]);

                            if (contractInstanceID != 0)
                            {
                                ViewState["TaskPageFlag"] = 1;
                                grdTaskActivity.PageIndex = chkSelectedPage - 1;
                                BindContractTasks_Paging(customerID, contractInstanceID, Convert.ToInt32(grdTaskActivity.PageSize), Convert.ToInt32(grdTaskActivity.PageIndex) + 1, grdTaskActivity);
                            }
                        }

                        tabIndex = 2;
                    }
                    else if (ddlSender.ID == "ddlPageNo_AuditLog")
                    {
                        ViewState["PageNumberAuditLogFlagID"] = 1;
                        gvContractAuditLog.PageIndex = chkSelectedPage - 1;
                        BindContractAuditLogs();

                        tabIndex = 3;
                    }
                    else if (ddlSender.ID == "DropDownListPageNoMilestone")
                    {
                        if (ViewState["ContractInstanceID"] != null)
                        {
                            int customerID = -1;
                            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                            long contractInstanceID = Convert.ToInt64(ViewState["ContractInstanceID"]);

                            if (contractInstanceID != 0)
                            {
                                ViewState["MilestonePageFlag"] = 1;
                                gridMilestone.PageIndex = chkSelectedPage - 1;
                                BindContractMilestone_Paging(customerID, contractInstanceID, Convert.ToInt32(gridMilestone.PageSize), Convert.ToInt32(gridMilestone.PageIndex) + 1, gridMilestone);
                            }
                        }                        
                        tabIndex = 4;
                    }
                    SetShowingRecords(tabIndex);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public string ShowAuditLog_Timeline(List<Cont_SP_AuditLogs_All_Result> lstAuditLogs)
        {
            try
            {
                StringBuilder strTimeLineHTML = new StringBuilder();

                if (lstAuditLogs.Count > 0)
                {
                    strTimeLineHTML.Append("<ul class=\"timeline timeline-horizontal\">");

                    lstAuditLogs.ForEach(eachAuditLog =>
                    {
                        strTimeLineHTML.Append("<li class=\"timeline-item\">" +
                                "<div class=\"timeline-badge primary\"><i class=\"fa fa-check\"></i></div>" +
                                "<div class=\"timeline-panel\">" +
                                    "<div class='timeline-heading'>" +
                                       " <h4 class=\"timeline-title\">" + eachAuditLog.CreatedByUser + "</h4>" +
                                       " <p><small class=\"text-muted\"><i class=\"fa fa-clock-o\"></i>" + eachAuditLog.CreatedOn.ToString("dd-MM-yyyy HH:mm:ss tt") + "</small></p>" +
                                   " </div>" +
                                    "<div class=\"timeline-body\">" +
                                        "<p>" + eachAuditLog.Remark + "</p>" +
                                   " </div>" +
                                "</div>" +
                            "</li>");
                    });

                    strTimeLineHTML.Append("</ul>");

                }

                return strTimeLineHTML.ToString();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return string.Empty;
            }
        }

        public string ShowAuditLog_VerticalTimeline(List<Cont_SP_AuditLogs_All_Result> lstAuditLogs)
        {
            try
            {
                StringBuilder strTimeLineHTML = new StringBuilder();

                if (lstAuditLogs.Count > 0)
                {
                    strTimeLineHTML.Append("<ul class=\"timeline\">");

                    lstAuditLogs.ForEach(eachAuditLog =>
                    {
                        strTimeLineHTML.Append("<li class=\"timeline-item\">" +
                                "<div class=\"timeline-badge primary\"><i class=\"fa fa-check\"></i></div>" +
                                "<div class=\"timeline-panel\">" +
                                    "<div class='timeline-heading'>" +
                                       " <h4 class=\"timeline-title\">" + eachAuditLog.CreatedByUser + "</h4>" +
                                       " <p><small class=\"text-muted\"><i class=\"fa fa-clock-o\"></i>" + eachAuditLog.CreatedOn.ToString("dd-MM-yyyy HH:mm:ss tt") + "</small></p>" +
                                   " </div>" +
                                    "<div class=\"timeline-body\">" +
                                        "<p>" + eachAuditLog.Remark + "</p>" +
                                   " </div>" +
                                "</div>" +
                            "</li>");
                    });

                    strTimeLineHTML.Append("</ul>");

                }

                return strTimeLineHTML.ToString();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return string.Empty;
            }
        }

        public string ShowAuditLog_Centered_Clickable(List<Cont_SP_AuditLogs_All_Result> lstAuditLogs)
        {
            try
            {
                StringBuilder strTimeLineHTML = new StringBuilder();

                if (lstAuditLogs.Count > 0)
                {
                    var minDate = lstAuditLogs.Select(row => row.CreatedOn).Min();
                    var maxDate = lstAuditLogs.Select(row => row.CreatedOn).Max();

                    if (minDate != null && maxDate != null)
                    {
                        strTimeLineHTML.Append("<ul class=\"timeline\">");

                        int accordionCount = 0;
                        var startDate = maxDate;
                        do
                        {
                            DateTime MonthStartDate = ContractCommonMethods.GetFirstDayOfMonth(startDate);
                            DateTime MonthEndDate = ContractCommonMethods.GetLastDayOfMonth(startDate);

                            var auditLogsMonthWise = lstAuditLogs.Where(row => row.CreatedOn >= MonthStartDate && row.CreatedOn <= MonthEndDate).OrderByDescending(row => row.CreatedOn).ToList();

                            if (auditLogsMonthWise.Count > 0)
                            {
                                accordionCount++;

                                string accordionCollapseClass = "panel-collapse collapse ";//change by Ruchi on 12th sep 2018

                                string faIcon = "fa fa-minus";
                                if (accordionCount > 1)
                                {
                                    accordionCollapseClass = "panel-collapse collapse";
                                    faIcon = "fa fa-minus";
                                }

                                if (accordionCount % 2 == 0)
                                    strTimeLineHTML.Append("<li class=\"timeline-inverted\">");
                                else
                                    strTimeLineHTML.Append("<li>");

                                strTimeLineHTML.Append("<div class=\"timeline-badge\" onclick=\"showHideAuditLog(divCollapse" + accordionCount + ",i" + accordionCount + ")\"" +
                                    "data-toggle=\"tooltip\" data-placement=\"bottom\" ToolTip=\"View Detail\"><i id=\"i" + accordionCount + "\" class=\"" + faIcon + "\"></i></div>");

                                strTimeLineHTML.Append("<div class=\"timeline-panel\">");

                                strTimeLineHTML.Append(
                                  "<h2 class=\"panel-title new-products-title\">" +
                                  CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(startDate.Month) + "-" + startDate.Year + "</h2>");
                                
                                strTimeLineHTML.Append("<div class=\"panel-heading clickable newProducts\" onclick=\"showHideAuditLog(divCollapse" + accordionCount + ",i" + accordionCount + ")\">" +
                                "<a class=\"newProductsLink\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapse" + accordionCount + "\">" +
                                //"<h2 class=\"panel-title new-products-title\">" +
                                //CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(startDate.Month) + "-" + startDate.Year + "</h2>" +
                                "</a>" +
                            "</div>");

                                strTimeLineHTML.Append("<div id=\"divCollapse" + accordionCount + "\" class=\"panel-body in\" style =\"max-height: 300px; overflow-y: auto; margin-top: 5px;\">" +
                                                        "<div id=\"collapse" + accordionCount + "\" class=" + accordionCollapseClass + "\">" +
                                                                "<ul class=newProductsList>");

                                auditLogsMonthWise.ForEach(eachAuditLog =>
                                {
                                    strTimeLineHTML.Append("<li>" +
                                         "<div class=\"timeline-body\">" +
                                          "<div class=\"col-md-3 pl0\">" +
                                                "<p><i class=\"fa fa-clock-o mlr5\"></i>" + eachAuditLog.CreatedOn.ToString("dd-MM-yyyy h:mm tt") + "</p>" +
                                            "</div>" +
                                             "<div class=\"col-md-2 pl0\">" +
                                                "<p class=\"timeline-title\">" + eachAuditLog.CreatedByUser + "</p>" +
                                             "</div>" +
                                            "<div class=\"col-md-7 pl0\">" +
                                                "<p>" + eachAuditLog.Remark + "</p>" +
                                            "</div>" +
                                         "</div>" +
                                   "</li>");
                                    strTimeLineHTML.Append("<div class=\"clearfix\"></div>");

                                    //<small class=\"text-muted\"></small>
                                });

                                strTimeLineHTML.Append("</ul></div>");
                                strTimeLineHTML.Append("</div>");

                                startDate = MonthStartDate.AddDays(-1);
                            }
                        } while (startDate >= minDate);

                        strTimeLineHTML.Append("</ul>");
                    }
                }

                return strTimeLineHTML.ToString();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return string.Empty;
            }
        }

        //public string ShowAuditLog_Centered_Clickable(List<Cont_SP_AuditLogs_All_Result> lstAuditLogs)
        //{
        //    try
        //    {
        //        StringBuilder strTimeLineHTML = new StringBuilder();

        //        if (lstAuditLogs.Count > 0)
        //        {
        //            var minDate = lstAuditLogs.Select(row => row.CreatedOn).Min();
        //            var maxDate = lstAuditLogs.Select(row => row.CreatedOn).Max();

        //            if (minDate != null && maxDate != null)
        //            {
        //                strTimeLineHTML.Append("<ul class=\"timeline\">");

        //                int accordionCount = 0;
        //                var startDate = maxDate;
        //                do
        //                {
        //                    DateTime MonthStartDate = ContractCommonMethods.GetFirstDayOfMonth(startDate);
        //                    DateTime MonthEndDate = ContractCommonMethods.GetLastDayOfMonth(startDate);

        //                    var auditLogsMonthWise = lstAuditLogs.Where(row => row.CreatedOn >= MonthStartDate && row.CreatedOn <= MonthEndDate).OrderByDescending(row => row.CreatedOn).ToList();

        //                    if (auditLogsMonthWise.Count > 0)
        //                    {
        //                        accordionCount++;

        //                        string accordionCollapseClass = "panel-collapse collapse ";//change by Ruchi on 12th sep 2018

        //                        string faIcon = "fa fa-minus";
        //                        if (accordionCount > 1)
        //                        {
        //                            accordionCollapseClass = "panel-collapse collapse";
        //                            faIcon = "fa fa-minus";
        //                        }

        //                        if (accordionCount % 2 == 0)
        //                            strTimeLineHTML.Append("<li class=\"timeline-inverted\">");
        //                        else
        //                            strTimeLineHTML.Append("<li>");

        //                        strTimeLineHTML.Append("<div class=\"timeline-badge\" onclick=\"showHideAuditLog(divCollapse" + accordionCount + ",i" + accordionCount + ")\"" +
        //                            "data-toggle=\"tooltip\" data-placement=\"bottom\" ToolTip=\"View Detail\"><i id=\"i" + accordionCount + "\" class=\"" + faIcon + "\"></i></div>");

        //                        strTimeLineHTML.Append("<div class=\"timeline-panel\">");

        //                        strTimeLineHTML.Append("<div class=\"panel-heading clickable newProducts\">" +
        //                           "<a class=\"newProductsLink\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapse" + accordionCount + "\">" +
        //                           "<h2 class=\"panel-title new-products-title\">" +
        //                           CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(startDate.Month) + "-" + startDate.Year + "</h2>" +
        //                           "</a>" +
        //                       "</div>");

        //                        //strTimeLineHTML.Append("<div class=\"panel-heading clickable newProducts\" onclick=\"showHideAuditLog(divCollapse" + accordionCount + ",i" + accordionCount + ")\">" +
        //                        //    "<a class=\"newProductsLink\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapse" + accordionCount + "\">" +
        //                        //    "<h2 class=\"panel-title new-products-title\">" +
        //                        //    CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(startDate.Month) + "-" + startDate.Year + "</h2>" +
        //                        //    "</a>" +
        //                        //"</div>");

        //                        strTimeLineHTML.Append("<div id=\"divCollapse" + accordionCount + "\" class=\"panel-body in\" style =\"max-height: 300px; overflow-y: auto; margin-top: 5px;\">" +
        //                                                "<div id=\"collapse" + accordionCount + "\" class=" + accordionCollapseClass + "\">" +
        //                                                        "<ul class=newProductsList>");

        //                        auditLogsMonthWise.ForEach(eachAuditLog =>
        //                        {
        //                            strTimeLineHTML.Append("<li>" +
        //                                 "<div class=\"timeline-body\">" +
        //                                  "<div class=\"col-md-3 pl0\">" +
        //                                        "<p><i class=\"fa fa-clock-o mlr5\"></i>" + eachAuditLog.CreatedOn.ToString("dd-MM-yyyy h:mm tt") + "</p>" +
        //                                    "</div>" +
        //                                     "<div class=\"col-md-2 pl0\">" +
        //                                        "<p class=\"timeline-title\">" + eachAuditLog.CreatedByUser + "</p>" +
        //                                     "</div>" +
        //                                    "<div class=\"col-md-7 pl0\">" +
        //                                        "<p>" + eachAuditLog.Remark + "</p>" +
        //                                    "</div>" +
        //                                 "</div>" +
        //                           "</li>");
        //                            strTimeLineHTML.Append("<div class=\"clearfix\"></div>");

        //                            //<small class=\"text-muted\"></small>
        //                        });

        //                        strTimeLineHTML.Append("</ul></div>");
        //                        strTimeLineHTML.Append("</div>");

        //                        startDate = MonthStartDate.AddDays(-1);
        //                    }
        //                } while (startDate >= minDate);

        //                strTimeLineHTML.Append("</ul>");
        //            }
        //        }

        //        return strTimeLineHTML.ToString();
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        return string.Empty;
        //    }
        //}

        public string ShowAuditLog_Clickable(List<Cont_SP_AuditLogs_All_Result> lstAuditLogs)
        {
            try
            {
                StringBuilder strTimeLineHTML = new StringBuilder();

                if (lstAuditLogs.Count > 0)
                {
                    var minDate = lstAuditLogs.Select(row => row.CreatedOn).Min();
                    var maxDate = lstAuditLogs.Select(row => row.CreatedOn).Max();

                    if (minDate != null && maxDate != null)
                    {
                        strTimeLineHTML.Append("<div class=\"timeline\">" +
                                                "<div class=\"line text-muted\"></div>");

                        int accordionCount = 0;
                        var startDate = maxDate;
                        do
                        {
                            DateTime MonthStartDate = ContractCommonMethods.GetFirstDayOfMonth(startDate);
                            DateTime MonthEndDate = ContractCommonMethods.GetLastDayOfMonth(startDate);

                            var auditLogsMonthWise = lstAuditLogs.Where(row => row.CreatedOn >= MonthStartDate && row.CreatedOn <= MonthEndDate).OrderByDescending(row => row.CreatedOn).ToList();

                            if (auditLogsMonthWise.Count > 0)
                            {
                                accordionCount++;

                                string accordionCollapseClass = "panel-collapse collapse in";//added by Ruchi on 9th sep 2018
                                string faIcon = "fa fa-minus";

                                if (accordionCount > 1)
                                {
                                    accordionCollapseClass = "panel-collapse collapse in";
                                    faIcon = "fa fa-minus";
                                }

                                strTimeLineHTML.Append("<article class=\"panel panel-primary product-panel\" style=\" margin-top: 20px;\">");

                                strTimeLineHTML.Append("<div class=\"panel-heading icon\" onclick=\"showHideAuditLog(divCollapse" + accordionCount + ",i" + accordionCount + ")\">" +
                                    "<a class=\"newProductsLinkIcon\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapse" + accordionCount + "\">" +
                                    "<i id=\"i" + accordionCount + "\" class=\"" + faIcon + "\"></i></a>" +
                                "</div>");


                                strTimeLineHTML.Append(
                                    "<h2 class=\"panel-title new-products-title\">" +
                                    CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(startDate.Month) + "-" + startDate.Year + "</h2>");


                                strTimeLineHTML.Append("<div class=\"panel-heading clickable newProducts\" onclick=\"showHideAuditLog(divCollapse" + accordionCount + ",i" + accordionCount + ")\">" +
                                    "<a class=\"newProductsLink\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapse" + accordionCount + "\">" +
                                        //"<h2 class=\"panel-title new-products-title\">" +
                                        //CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(startDate.Month) + "-" + startDate.Year + "</h2>" +
                                    "</a>" +
                                "</div>");

                                strTimeLineHTML.Append("<div id=\"divCollapse" + accordionCount + "\" class=\"panel-body in\" style=\"max-height: 300px; overflow-y: auto; border: 1px solid #c7c7cc; margin-top: 5px; border-radius: 5px\">" +
                                                        "<div id=\"collapse" + accordionCount + "\" class=" + accordionCollapseClass + "\">" +
                                                                "<ul class=newProductsList>");

                                auditLogsMonthWise.ForEach(eachAuditLog =>
                                {
                                    strTimeLineHTML.Append("<li>" +
                                         "<div class=\"timeline-body\">" +
                                             "<div class=\"col-md-2\">" +
                                                "<h4 class=\"timeline-title\">" + eachAuditLog.CreatedByUser + "</h4>" +
                                             "</div>" +
                                            "<div class=\"col-md-3\">" +
                                                "<p><i class=\"fa fa-clock-o mlr5\"></i>" + eachAuditLog.CreatedOn.ToString("dd-MM-yyyy HH:mm:ss tt") + "</p>" +
                                            "</div>" +
                                            "<div class=\"col-md-7\">" +
                                                "<p>" + eachAuditLog.Remark + "</p>" +
                                            "</div>" +
                                         "</div>" +
                                   "</li>");
                                    strTimeLineHTML.Append("<div class=\"clearfix\"></div>");

                                    //<small class=\"text-muted\"></small>
                                });

                                strTimeLineHTML.Append("</ul></div>");
                                strTimeLineHTML.Append("</article>");

                                startDate = MonthStartDate.AddDays(-1);
                            }
                        } while (startDate >= minDate);

                        strTimeLineHTML.Append("</div>");
                    }
                }

                return strTimeLineHTML.ToString();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return string.Empty;
            }
        }

        #region Contract-Task

        public void BindContractTasks_Paging(int customerID, long contractID, int pageSize, int pageNumber, GridView grd)
        {
            try
            {
                var lstTasks = ContractTaskManagement.GetContractTasks_Paging(customerID, contractID, pageSize, pageNumber);

                if (lstTasks.Count > 0)
                    lstTasks = lstTasks.OrderByDescending(row => row.UpdatedOn).ToList();

                grd.DataSource = lstTasks;
                grd.DataBind();

                long totalRowCount = 0;
                if (lstTasks.Count > 0)
                {
                    if (lstTasks[0].TotalRowCount != null)
                        totalRowCount = Convert.ToInt64(lstTasks[0].TotalRowCount);
                    else
                    {
                        foreach (var item in lstTasks)
                        {
                            totalRowCount = Convert.ToInt32(item.TotalRowCount);
                            break;
                        }
                    }
                }

                Session["TotalRows"] = totalRowCount;

                lstTasks.Clear();
                lstTasks = null;

                //lblStartRecord_Task.Text = DropDownListPageNo.SelectedValue;
                if (Convert.ToInt32(ViewState["TaskPageFlag"]) == 0)
                {
                    bindPageNumber(2);
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvTaskTab.IsValid = false;
                cvTaskTab.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        protected void btnCloseTask_Click(object sender, EventArgs e)
        {
            try
            {
                clearTaskControls();
                HideShowTaskDiv(false);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnClearTask_Click(object sender, EventArgs e)
        {
            try
            {
                clearTaskControls();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void clearTaskControls()
        {
            try
            {
                tbxTaskTitle.Text = "";
                tbxTaskDesc.Text = "";
                tbxTaskAssignDate.Text = "";
                tbxTaskDueDate.Text = "";
                tbxExpOutcome.Text = "";
                tbxTaskRemark.Text = "";

                lstBoxTaskUser.ClearSelection();
                ddlTaskPriority.ClearSelection();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
        }

        public void enableDisableTaskControls(bool flag)
        {
            try
            {
                tbxTaskTitle.Enabled = flag;
                tbxTaskDesc.Enabled = flag;
                tbxTaskDueDate.Enabled = flag;
                tbxExpOutcome.Enabled = flag;
                tbxTaskRemark.Enabled = flag;

                if (flag)
                {
                    lstBoxTaskUser.Attributes.Remove("disabled");
                    ddlTaskPriority.Attributes.Remove("disabled");
                }
                else
                {
                    lstBoxTaskUser.Attributes.Add("disabled", "disabled");
                    ddlTaskPriority.Attributes.Add("disabled", "disabled");
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
        }

        protected void btnSaveTask_Click(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["ContractInstanceID"] != null)
                {
                    long taskID = 0;
                    long contractID = Convert.ToInt64(ViewState["ContractInstanceID"]);
                    int Permission = 0;
                    int customerID = -1;
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                    bool formValidateSuccess = false;
                    bool saveSuccess = false;

                    List<string> lstErrorMsg = new List<string>();
                    List<Tuple<int, int>> lstTaskUserMapping = new List<Tuple<int, int>>();

                    #region Data Validation

                    if (String.IsNullOrEmpty(tbxTaskTitle.Text))
                        lstErrorMsg.Add("Provide Task Title");
                    else
                        formValidateSuccess = true;

                    if (String.IsNullOrEmpty(tbxTaskDesc.Text))
                        lstErrorMsg.Add("Provide Task Description");
                    else
                        formValidateSuccess = true;

                    if (rbTaskType.SelectedValue == "" || rbTaskType.SelectedValue == null)
                        lstErrorMsg.Add("Select Task Type");
                    else
                        formValidateSuccess = true;

                    int selectedUserCount = 0;

                    if (rbTaskType.SelectedValue != null || rbTaskType.SelectedValue != "")
                    {
                        if (rbTaskType.SelectedValue == "6")
                        {
                            foreach (ListItem eachUser in lstBoxTaskUser.Items)
                            {
                                if (eachUser.Selected)
                                    selectedUserCount++;
                            }
                        }
                        else
                        {
                            foreach (ListItem eachUser in lstBoxTaskUser.Items)
                            {
                                if (eachUser.Selected)
                                    selectedUserCount++;
                            }
                        }
                    }

                    if (selectedUserCount > 0)
                        formValidateSuccess = true;
                    else
                        lstErrorMsg.Add("Please select at least one user to assign.");

                    if (String.IsNullOrEmpty(tbxTaskAssignDate.Text))
                        lstErrorMsg.Add("Provide Task Assign On, If not available set is to Current Date.");
                    else
                    {
                        bool check = ContractCommonMethods.CheckDate(tbxTaskAssignDate.Text.Trim());
                        if (!check)
                        {
                            lstErrorMsg.Add("Invalid Task Assign On Date or Date should be in DD-MM-YYYY Format");
                        }
                        else
                            formValidateSuccess = true;
                    }

                    if (String.IsNullOrEmpty(tbxTaskDueDate.Text))
                        lstErrorMsg.Add("Provide Task Due Date.");
                    else
                    {
                        bool check = ContractCommonMethods.CheckDate(tbxTaskDueDate.Text.Trim());
                        if (!check)
                        {
                            lstErrorMsg.Add("Invalid Task Due Date or Date should be in DD-MM-YYYY Format");
                        }
                        else
                            formValidateSuccess = true;
                    }

                    if (!String.IsNullOrEmpty(tbxTaskAssignDate.Text) && !String.IsNullOrEmpty(tbxTaskDueDate.Text))
                    {
                        bool check = ContractCommonMethods.CheckDate(tbxTaskDueDate.Text.Trim());
                        bool check1 = ContractCommonMethods.CheckDate(tbxTaskAssignDate.Text.Trim());

                        if (check && check1)
                        {
                            if (Convert.ToDateTime(tbxTaskAssignDate.Text).Date <= Convert.ToDateTime(tbxTaskAssignDate.Text).Date)
                                formValidateSuccess = true;
                            else
                                lstErrorMsg.Add("Task Due Date should be greater than Task Assign On Date");
                        }
                    }

                    if (String.IsNullOrEmpty(ddlTaskPriority.SelectedValue) || ddlTaskPriority.SelectedValue == "-1" || ddlTaskPriority.SelectedValue == "0")
                        lstErrorMsg.Add("Select Task Priority");
                    else
                        formValidateSuccess = true;

                    if (lstErrorMsg.Count > 0)
                    {
                        formValidateSuccess = false;
                        showErrorMessages(lstErrorMsg, cvTaskTab);
                    }

                    #endregion                    

                    if (formValidateSuccess)
                    {
                        Cont_tbl_TaskInstance newTaskRecord = new Cont_tbl_TaskInstance();

                        newTaskRecord.ContractID = contractID;
                        newTaskRecord.IsActive = true;
                        newTaskRecord.TaskType = Convert.ToInt32(rbTaskType.SelectedValue);

                        newTaskRecord.TaskTitle = tbxTaskTitle.Text.Trim();
                        newTaskRecord.TaskDesc = tbxTaskDesc.Text.Trim();
                        newTaskRecord.AssignOn = DateTimeExtensions.GetDate(tbxTaskAssignDate.Text);
                        newTaskRecord.DueDate = DateTimeExtensions.GetDate(tbxTaskDueDate.Text);

                        if (!String.IsNullOrEmpty(ddlTaskPriority.SelectedValue))
                            newTaskRecord.PriorityID = Convert.ToInt32(ddlTaskPriority.SelectedValue);

                        if (tbxExpOutcome.Text != "")
                            newTaskRecord.ExpectedOutcome = tbxExpOutcome.Text;

                        if (tbxTaskRemark.Text != "")
                            newTaskRecord.Remark = tbxTaskRemark.Text.Trim();

                        newTaskRecord.CustomerID = customerID;
                        newTaskRecord.CreatedBy = AuthenticationHelper.UserID;
                        newTaskRecord.CreatedOn = DateTime.Now;
                        newTaskRecord.UpdatedBy = AuthenticationHelper.UserID;
                        newTaskRecord.UpdatedOn = DateTime.Now;
                        if (ddlPermission.SelectedValue != "" && ddlPermission.SelectedValue != "-1" && ddlPermission.SelectedValue != "0")
                        {
                            Permission = Convert.ToInt32(ddlPermission.SelectedValue);
                        }
                        newTaskRecord.Permission = Permission;

                        if (Convert.ToString(ViewState["TaskMode"]) == "0")
                        {
                            if (!ContractTaskManagement.ExistTaskTitle(tbxTaskTitle.Text.Trim(), contractID, 0, customerID))
                            {
                                taskID = ContractTaskManagement.CreateTask(newTaskRecord);                                    
                                if (taskID > 0)
                                {
                                    saveSuccess = true;
                                    ContractManagement.CreateAuditLog("C", contractID, "Cont_tbl_TaskInstance", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "New Task Created", true, Convert.ToInt32(taskID));
                                }
                            }
                            else
                            {
                                cvTaskTab.IsValid = false;
                                cvTaskTab.ErrorMessage = "Task with same title already exists";

                                tbxTaskTitle.Focus();
                                saveSuccess = false;
                                return;
                            }
                        }
                        else if (Convert.ToString(ViewState["TaskMode"]) == "1")
                        {
                            taskID = Convert.ToInt64(ViewState["TaskID"]);

                            if (!ContractTaskManagement.ExistTaskTitle(tbxTaskTitle.Text.Trim(), contractID, taskID, customerID))
                            {
                                newTaskRecord.ID = taskID;
                                newTaskRecord.ContractID = contractID;

                                saveSuccess = ContractTaskManagement.UpdateTask(newTaskRecord);

                                if (saveSuccess)
                                {
                                    ContractManagement.CreateAuditLog("C", contractID, "Cont_tbl_TaskInstance", "Update", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Task Detail(s) Updated", true, Convert.ToInt32(taskID));
                                }
                            }
                            else
                            {
                                cvTaskTab.IsValid = false;
                                cvTaskTab.ErrorMessage = "Task with same title already exists";
                                cvTaskTab.CssClass = "alert alert-danger";
                                tbxTaskTitle.Focus();
                                saveSuccess = false;
                                return;
                            }
                        }

                        if (saveSuccess)
                        {
                            #region Share Documents
                            //Save Task Related Uploaded Documents
                            if (grdTaskContractDocuments.Rows.Count > 0)
                            {
                                saveSuccess = shareDocuments(grdTaskContractDocuments, contractID, taskID);
                            }
                            #endregion

                            #region Task-User-Assignment

                            if (lstBoxTaskUser.Items.Count > 0)
                            {
                                if (rbTaskType.SelectedValue != null || rbTaskType.SelectedValue != "")
                                {
                                    int assignedRoleID = 4;
                                    if (rbTaskType.SelectedValue == "6")
                                    {
                                        assignedRoleID = 6;
                                        foreach (ListItem eachApprover in lstBoxTaskUser.Items) //lstBoxApprover
                                        {
                                            if (eachApprover.Selected)
                                            {
                                                if (Convert.ToInt32(eachApprover.Value) != 0)
                                                    lstTaskUserMapping.Add(new Tuple<int, int>(Convert.ToInt32(eachApprover.Value), assignedRoleID));
                                            }
                                        }
                                    }
                                    else
                                    {
                                        foreach (ListItem eachUser in lstBoxTaskUser.Items)
                                        {
                                            if (eachUser.Selected)
                                            {
                                                if (Convert.ToInt32(eachUser.Value) != 0)
                                                    lstTaskUserMapping.Add(new Tuple<int, int>(Convert.ToInt32(eachUser.Value), assignedRoleID));
                                            }
                                        }
                                    }

                                    if (lstTaskUserMapping.Count > 0)
                                    {
                                        //Add Logged-in User As Performer
                                        //lstTaskUserMapping.Add(new Tuple<int, int>(Convert.ToInt32(AuthenticationHelper.UserID), 3));

                                        List<Cont_tbl_TaskUserAssignment> lstUserAssignmentRecord_ToSave = new List<Cont_tbl_TaskUserAssignment>();

                                        lstTaskUserMapping.ForEach(eachUser =>
                                        {
                                            Cont_tbl_TaskUserAssignment newAssignment = new Cont_tbl_TaskUserAssignment()
                                            {
                                                CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID),

                                                ContractID = contractID,
                                                TaskID = taskID,

                                                UserID = Convert.ToInt32(eachUser.Item1),
                                                RoleID = Convert.ToInt32(eachUser.Item2),

                                                IsActive = true,
                                                LinkCreatedOn = DateTime.Now,
                                                URLExpired = false,

                                                CreatedBy = AuthenticationHelper.UserID,
                                                CreatedOn = DateTime.Now,
                                                UpdatedBy = AuthenticationHelper.UserID,
                                                UpdatedOn = DateTime.Now
                                            };

                                            string checkSum = Util.CalculateMD5Hash(contractID.ToString() + taskID.ToString() + newAssignment.UserID.ToString() + assignedRoleID.ToString());
                                            string accessURL = string.Empty;
                                            URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(AuthenticationHelper.CustomerID));
                                            if (Urloutput != null)
                                            {
                                                accessURL = Urloutput.URL;
                                            }
                                            else
                                            {
                                                accessURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                                            }
                                            newAssignment.AccessURL = Convert.ToString(accessURL) +
                                                                      "/ContractProduct/aspxPages/ContractTaskDetailsPage.aspx?" +
                                                                      "A=" + CryptographyManagement.Encrypt(contractID.ToString()) +
                                                                      "&B=" + CryptographyManagement.Encrypt(taskID.ToString()) +
                                                                      "&C=" + CryptographyManagement.Encrypt(newAssignment.UserID.ToString()) +
                                                                      "&D=" + CryptographyManagement.Encrypt(assignedRoleID.ToString()) +
                                                                      "&Checksum=" + checkSum;
                                            //newAssignment.AccessURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]) +
                                            //                          "/ContractProduct/aspxPages/ContractTaskDetailsPage.aspx?" +
                                            //                          "A=" + CryptographyManagement.Encrypt(contractID.ToString()) +
                                            //                          "&B=" + CryptographyManagement.Encrypt(taskID.ToString()) +
                                            //                          "&C=" + CryptographyManagement.Encrypt(newAssignment.UserID.ToString()) +
                                            //                          "&D=" + CryptographyManagement.Encrypt(assignedRoleID.ToString()) +
                                            //                          "&Checksum=" + checkSum;

                                            lstUserAssignmentRecord_ToSave.Add(newAssignment);
                                        });

                                        var existingTaskUserAssignments = ContractTaskManagement.GetContractTaskUserAssignment_RoleWise(Convert.ToInt32(AuthenticationHelper.CustomerID), contractID, taskID, assignedRoleID);

                                        var assignedUserIDs = existingTaskUserAssignments.Select(row => row.UserID).ToList();
                                        var CurrentUserIDs = lstUserAssignmentRecord_ToSave.Select(row => row.UserID).ToList();

                                        bool matchSuccess = false;

                                        if (assignedUserIDs.Count != CurrentUserIDs.Count)
                                        {
                                            matchSuccess = false;
                                        }
                                        else
                                        {
                                            matchSuccess = assignedUserIDs.Except(CurrentUserIDs).ToList().Count > 0 ? false : true;
                                        }

                                        if (!matchSuccess)
                                        {
                                            ContractTaskManagement.DeActive_TaskUserAssignments(taskID, contractID, AuthenticationHelper.UserID);

                                            if (lstUserAssignmentRecord_ToSave.Count > 0)
                                            {
                                                saveSuccess = ContractTaskManagement.CreateUpdate_TaskUserAssignments(lstUserAssignmentRecord_ToSave);

                                                if (saveSuccess)
                                                {
                                                    if (Convert.ToString(ViewState["TaskMode"]) == "0")
                                                        ContractManagement.CreateAuditLog("C", contractID, "Cont_tbl_TaskUserAssignment", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Task User Assignment(s) Created", true, Convert.ToInt32(taskID));
                                                    else if (Convert.ToString(ViewState["TaskMode"]) == "1")
                                                        ContractManagement.CreateAuditLog("C", contractID, "Cont_tbl_TaskUserAssignment", "Update", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Task User Assignment(s) Updated", true, Convert.ToInt32(taskID));
                                                }
                                            }

                                            if (saveSuccess)
                                            {
                                                int statusID = 11; //Task-Open
                                                string taskComments = string.Empty;

                                                if (tbxTaskRemark.Text != "")
                                                    taskComments = tbxTaskRemark.Text.Trim();
                                                else
                                                    taskComments = "New Task Assigned";

                                                saveSuccess = ContractTaskManagement.CreateUpdate_TaskTrasactions(lstUserAssignmentRecord_ToSave, AuthenticationHelper.UserID, statusID, taskComments);

                                                //Move Contract Status 
                                                if (saveSuccess)
                                                {
                                                    int newStatusID = 0;

                                                    if (newTaskRecord.TaskType == 4) //Review Task
                                                    {
                                                        newStatusID = 2; //Pending Review                                       
                                                    }
                                                    else if (newTaskRecord.TaskType == 6) //Approval Task
                                                    {
                                                        newStatusID = 4;  //Pending Approval                                        
                                                    }

                                                    if (newStatusID != 0)
                                                        saveSuccess = ContractTaskManagement.UpdateContractStatus(customerID, contractID, taskID, "Open", newStatusID);

                                                    if (saveSuccess)
                                                    {
                                                        ddlContractStatus.ClearSelection();

                                                        if (ddlContractStatus.Items.FindByValue(newStatusID.ToString()) != null)
                                                            ddlContractStatus.Items.FindByValue(newStatusID.ToString()).Selected = true;
                                                    }
                                                }

                                                #region Send Email to Assigned User(s)

                                                int userID = AuthenticationHelper.UserID;

                                                if (saveSuccess)
                                                {
                                                    bool sendSuccess = false;
                                                    sendSuccess = SendEmail_TaskAssignedUsers(customerID, contractID, newTaskRecord, lstTaskUserMapping, "Task Assignment ");

                                                    //Task<bool> task = new Task<bool>(() => SendEmail_TaskAssignedUsers(customerID, userID, contractID, newTaskRecord, lstTaskUserMapping, "Task Assignment"));
                                                    //task.Start();

                                                    //sendSuccess = await task;

                                                    //if (sendSuccess)
                                                    //{
                                                    //    cvTaskTab.IsValid = false;
                                                    //    cvTaskTab.ErrorMessage = "An Email containing task detail and access URL to provide response sent to assignee.";
                                                    //    vsTaskTab.CssClass = "alert alert-success";
                                                    //}
                                                }
                                                #endregion
                                            }
                                        }
                                        else
                                            saveSuccess = true;

                                        lstUserAssignmentRecord_ToSave.Clear();
                                        lstUserAssignmentRecord_ToSave = null;
                                    }
                                }
                            }

                            #endregion                            
                        }

                        if (saveSuccess)
                        {
                            clearTaskControls();
                            BindContractTasks_Paging(customerID, contractID, Convert.ToInt32(grdTaskActivity.PageSize), Convert.ToInt32(grdTaskActivity.PageIndex) + 1, grdTaskActivity);

                            HideShowTaskDiv(false);

                            cvTaskTab.IsValid = false;
                            cvTaskTab.ErrorMessage = "Task Saved Successfully. An Email containing task details and access URL to provide response, sent to assignee.";
                            vsTaskTab.CssClass = "alert alert-success";
                        }
                        else
                        {
                            cvTaskTab.IsValid = false;
                            cvTaskTab.ErrorMessage = "Something went wrong, Please try again.";
                            vsTaskTab.CssClass = "alert alert-danger";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        async Task<bool> SendEmail_TaskAssignedUsers_Async(int customerID, long ContractID, Cont_tbl_TaskInstance newTaskRecord, List<Tuple<int, int>> lstTaskUserMapping, string reminderType)
        {
            long TaskID = newTaskRecord.ID;
            bool saveSuccess = false;
            string accessURL = string.Empty;

            if (newTaskRecord != null)
            {
                string taskAssignedBy = ContractUserManagement.GetUserNameByUserID(newTaskRecord.CreatedBy);

                List<Cont_tbl_ReminderLog> lstReminderLogs = new List<Cont_tbl_ReminderLog>();

                foreach (var eachAssignedUser in lstTaskUserMapping)
                {
                    if (eachAssignedUser.Item2 != 3)
                    {
                        string portalurl = string.Empty;
                        URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(AuthenticationHelper.CustomerID));
                        if (Urloutput != null)
                        {
                            portalurl = Urloutput.URL;
                        }
                        else
                        {
                            portalurl = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                        }

                        accessURL = string.Empty;

                        if (UserManagement.GetUserTypeInternalExternalByUserID(Convert.ToInt32(eachAssignedUser.Item1), customerID))
                        {
                            accessURL = Convert.ToString(accessURL)
                                + "/ContractProduct/aspxPages/ContractTaskDetailsPage.aspx?TaskID=" + CryptographyManagement.Encrypt(TaskID.ToString())
                                + "&CID=" + CryptographyManagement.Encrypt(ContractID.ToString());
                        }
                        else
                        {
                            accessURL = Convert.ToString(accessURL);
                        }
                        //if (UserManagement.GetUserTypeInternalExternalByUserID(Convert.ToInt32(eachAssignedUser.Item1), customerID))
                        //{
                        //    accessURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"])
                        //        + "/ContractProduct/aspxPages/ContractTaskDetailsPage.aspx?TaskID=" + CryptographyManagement.Encrypt(TaskID.ToString())
                        //        + "&CID=" + CryptographyManagement.Encrypt(ContractID.ToString());
                        //}
                        //else
                        //{
                        //    accessURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                        //}

                        saveSuccess = SendTaskAssignmentMail(newTaskRecord, accessURL, customerID, ContractID, Convert.ToInt32(eachAssignedUser.Item1), taskAssignedBy);

                        if (saveSuccess)
                        {
                            bool sendSuccess = ContractTaskManagement.UpdateTaskAccessURL_Contract(ContractID, TaskID, Convert.ToInt32(AuthenticationHelper.UserID), Convert.ToInt32(eachAssignedUser.Item1), Convert.ToInt32(eachAssignedUser.Item2), accessURL);

                            if (sendSuccess)
                            {
                                //Save Reminder Log
                                Cont_tbl_ReminderLog objRemind = new Cont_tbl_ReminderLog()
                                {
                                    UserID = Convert.ToInt32(eachAssignedUser.Item1),
                                    Role = Convert.ToInt32(eachAssignedUser.Item2),
                                    TriggerType = reminderType,
                                    TriggerDate = DateTime.Now
                                };

                                lstReminderLogs.Add(objRemind);
                            }
                        }
                    }
                }

                if (lstReminderLogs.Count > 0)
                {
                    if (ContractManagement.Save_ContractReminderLogs(lstReminderLogs))
                    {
                        ContractManagement.CreateAuditLog("C", ContractID, "Cont_tbl_ReminderLog", "Sent", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, reminderType + "Email sent to " + lstReminderLogs.Count + " User(s)", true, Convert.ToInt32(TaskID));
                    }
                }
            }

            return saveSuccess;
        }

        private bool SendEmail_TaskAssignedUsers(int customerID, long ContractID, Cont_tbl_TaskInstance newTaskRecord, List<Tuple<int, int>> lstTaskUserMapping, string reminderType)
        {
            long TaskID = newTaskRecord.ID;
            bool saveSuccess = false;
            string accessURL = string.Empty;

            if (newTaskRecord != null)
            {
                string taskAssignedBy = ContractUserManagement.GetUserNameByUserID(newTaskRecord.CreatedBy);

                List<Cont_tbl_ReminderLog> lstReminderLogs = new List<Cont_tbl_ReminderLog>();

                foreach (var eachAssignedUser in lstTaskUserMapping)
                {
                    if (eachAssignedUser.Item2 != 3)
                    {
                        accessURL = string.Empty;

                        //if (UserManagement.GetUserTypeInternalExternalByUserID(Convert.ToInt32(eachAssignedUser.Item1), customerID))
                        //{
                        //    accessURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"])
                        //        + "/ContractProduct/aspxPages/ContractTaskDetailsPage.aspx?TaskID=" + CryptographyManagement.Encrypt(TaskID.ToString())
                        //        + "&CID=" + CryptographyManagement.Encrypt(ContractID.ToString());
                        //}
                        //else
                        //{
                        //    accessURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                        //}

                        saveSuccess = SendTaskAssignmentMail(newTaskRecord, accessURL, customerID, ContractID, Convert.ToInt32(eachAssignedUser.Item1), taskAssignedBy);

                        if (saveSuccess)
                        {
                            //bool sendSuccess = ContractTaskManagement.UpdateTaskAccessURL_Contract(ContractID, TaskID, Convert.ToInt32(AuthenticationHelper.UserID), Convert.ToInt32(eachAssignedUser.Item1), Convert.ToInt32(eachAssignedUser.Item2), accessURL);

                            //if (sendSuccess)
                            //{
                            //Save Reminder Log
                            Cont_tbl_ReminderLog objRemind = new Cont_tbl_ReminderLog()
                            {
                                UserID = Convert.ToInt32(eachAssignedUser.Item1),
                                Role = Convert.ToInt32(eachAssignedUser.Item2),
                                TriggerType = reminderType,
                                TriggerDate = DateTime.Now
                            };

                            lstReminderLogs.Add(objRemind);
                            //}
                        }
                    }
                }

                if (lstReminderLogs.Count > 0)
                {
                    if (ContractManagement.Save_ContractReminderLogs(lstReminderLogs))
                    {
                        ContractManagement.CreateAuditLog("C", ContractID, "Cont_tbl_ReminderLog", "Sent", customerID, AuthenticationHelper.UserID, reminderType + "Email sent to " + lstReminderLogs.Count + " User(s)", true, Convert.ToInt32(TaskID));
                    }
                }
            }

            return saveSuccess;
        }

        public bool SendTaskAssignmentMail(Cont_tbl_TaskInstance taskRecord, string accessURL, int customerID, long ContractID, int assignedTo, string taskAssignedBy)
        {
            try
            {
                if (taskRecord != null)
                {
                    User User = UserManagement.GetByID(Convert.ToInt32(assignedTo));

                    if (User != null)
                    {
                        if (User.Email != null && User.Email != "")
                        {
                            string taskPriority = string.Empty;

                            if (taskRecord.PriorityID != 0)
                            {
                                if (taskRecord.PriorityID == 1)
                                    taskPriority = "High";
                                else if (taskRecord.PriorityID == 2)
                                    taskPriority = "Medium";
                                else if (taskRecord.PriorityID == 3)
                                    taskPriority = "Low";
                            }

                            string username = string.Format("{0} {1}", User.FirstName, User.LastName);
                            string portalurl = string.Empty;
                            URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(AuthenticationHelper.CustomerID));
                            if (Urloutput != null)
                            {
                                portalurl = Urloutput.URL;
                            }
                            else
                            {
                                portalurl = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                            }
                            string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_Contract_TaskAssignment
                                                  .Replace("@User", username)
                                                  .Replace("@TaskTitle", taskRecord.TaskTitle)
                                                  .Replace("@TaskDesc", taskRecord.TaskDesc)
                                                  .Replace("@Priority", taskPriority)
                                                  .Replace("@DueDate", taskRecord.DueDate.ToString("dd-MM-yyyy"))
                                                  .Replace("@AssignedBy", taskAssignedBy.ToString())
                                                  .Replace("@Remark", taskRecord.Remark)
                                                  .Replace("@AccessURL", accessURL)
                                                  .Replace("@From", Convert.ToString(ConfigurationManager.AppSettings["ReplyEmailAddressName"]))
                                                  .Replace("@PortalURL", Convert.ToString(portalurl));

                            //string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_Contract_TaskAssignment
                            //                        .Replace("@User", username)
                            //                        .Replace("@TaskTitle", taskRecord.TaskTitle)
                            //                        .Replace("@TaskDesc", taskRecord.TaskDesc)
                            //                        .Replace("@Priority", taskPriority)
                            //                        .Replace("@DueDate", taskRecord.DueDate.ToString("dd-MM-yyyy"))
                            //                        .Replace("@AssignedBy", taskAssignedBy.ToString())
                            //                        .Replace("@Remark", taskRecord.Remark)
                            //                        .Replace("@AccessURL", accessURL)
                            //                        .Replace("@From", Convert.ToString(ConfigurationManager.AppSettings["ReplyEmailAddressName"]))
                            //                        .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]));

                            EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(new String[] { User.Email }), null, null, "Contract Notification-Task Assigned", message);

                            return true;
                        }
                        else
                            return false;
                    }
                    else
                        return false;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        protected void grdTaskActivity_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                if (ViewState["ContractInstanceID"] != null)
                {
                    grdTaskActivity.PageIndex = e.NewPageIndex;
                    BindContractTasks_Paging(customerID, Convert.ToInt64(ViewState["CaseInstanceID"]), Convert.ToInt32(grdTaskActivity.PageSize), Convert.ToInt32(grdTaskActivity.PageIndex) + 1, grdTaskActivity);

                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "gridPageIndexChanged", "gridPageIndexChanged();", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdTaskActivity_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                if (e.CommandArgument != null)
                {
                    if (ViewState["ContractInstanceID"] != null)
                    {
                        bool txnSuccess = false;
                        long contractID = Convert.ToInt64(ViewState["ContractInstanceID"]);
                        long taskID = Convert.ToInt64(e.CommandArgument);

                        //Update Task Status to Closed and Expire URL
                        if (e.CommandName.Equals("CloseTask"))
                        {
                            var closeSuccess = ContractTaskManagement.UpdateTaskStatus(customerID, AuthenticationHelper.UserID, contractID, taskID, "Closed");

                            if (closeSuccess)
                            {
                                //Update URL Status to De-Active
                                ContractTaskManagement.UpdateURLExpired(customerID, contractID, taskID, false);

                                ContractManagement.CreateAuditLog("C", contractID, "Cont_tbl_TaskTransaction", "Update", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Task Closed", true, Convert.ToInt32(taskID));

                                //Re-Bind Task
                                BindContractTasks_Paging(Convert.ToInt32(AuthenticationHelper.CustomerID), contractID, Convert.ToInt32(grdTaskActivity.PageSize), Convert.ToInt32(grdTaskActivity.PageIndex) + 1, grdTaskActivity);

                                cvTaskTab.IsValid = false;
                                cvTaskTab.ErrorMessage = "Task Closed Successfully.";
                                vsTaskTab.CssClass = "alert alert-success";
                            }
                        }

                        else if (e.CommandName.Equals("EditTask"))
                        {
                            ViewState["TaskMode"] = "1";

                            ViewState["TaskID"] = taskID;

                            if (taskID != 0 && contractID != 0)
                            {
                                var taskDetailRecord = ContractTaskManagement.GetTaskDetailsByTaskID(contractID, taskID);
                                if (taskDetailRecord != null)
                                {
                                    if (FlagIsApp == "MGMT" || FlagIsApp == "CADMN" || lstBoxOwner.Items.FindByValue(AuthenticationHelper.UserID.ToString()) != null)
                                    {
                                        DocViewResult = "1";
                                    }
                                    else
                                    {
                                        if (taskDetailRecord.Permission == 1)
                                        {
                                            DocViewResult = "1";
                                        }
                                    }
                                    tbxTaskTitle.Text = taskDetailRecord.TaskTitle;
                                    tbxTaskDesc.Text = taskDetailRecord.TaskDesc;
                                    ViewState["TaskTitle"] = taskDetailRecord.TaskTitle;
                                    ViewState["TaskAssignOn"] = taskDetailRecord.AssignOn.ToString("dd-MM-yyyy");
                                    ViewState["TaskInstanceID"] = taskID;
                                    BindTransactions();
                                    if (taskDetailRecord.AssignOn != null)
                                        tbxTaskAssignDate.Text = taskDetailRecord.AssignOn.ToString("dd-MM-yyyy");

                                    if (taskDetailRecord.DueDate != null)
                                        tbxTaskDueDate.Text = taskDetailRecord.DueDate.ToString("dd-MM-yyyy");

                                    ddlTaskPriority.ClearSelection();
                                    if (ddlTaskPriority.Items.FindByValue(taskDetailRecord.PriorityID.ToString()) != null)
                                        ddlTaskPriority.Items.FindByValue(taskDetailRecord.PriorityID.ToString()).Selected = true;

                                    ddlPermission.ClearSelection();
                                    if (ddlPermission.Items.FindByValue(taskDetailRecord.Permission.ToString()) != null)
                                        ddlPermission.Items.FindByValue(taskDetailRecord.Permission.ToString()).Selected = true;

                                    tbxExpOutcome.Text = taskDetailRecord.ExpectedOutcome;
                                    tbxTaskRemark.Text = taskDetailRecord.Remark;

                                    var lstTaskAssignedUsers = ContractTaskManagement.GetContractTaskUserAssignment(Convert.ToInt32(AuthenticationHelper.CustomerID), contractID, taskID);

                                    if (lstTaskAssignedUsers.Count > 0)
                                    {
                                        if (lstBoxTaskUser.Items.Count > 0)
                                        {
                                            lstBoxTaskUser.ClearSelection();
                                        }

                                        if (lstTaskAssignedUsers.Count > 0)
                                        {
                                            foreach (var userID in lstTaskAssignedUsers)
                                            {
                                                if (lstBoxTaskUser.Items.FindByValue(userID.UserID.ToString()) != null)
                                                    lstBoxTaskUser.Items.FindByValue(userID.UserID.ToString()).Selected = true;
                                            }
                                        }
                                    }

                                    BindTaskDocuments_All(grdTaskContractDocuments, contractID, taskID);

                                    BindTaskResponses(contractID, taskID);
                                    HideShowTaskDiv(true);
                                    toggleTextSaveButton_AddEditTask(false);

                                    //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "HideShowTask_Script", "HideShowTaskDiv('true');", true);
                                }
                            }
                        }
                        else if (e.CommandName.Equals("DeleteTask"))
                        {
                            txnSuccess = ContractTaskManagement.DeleteTask(contractID, taskID, AuthenticationHelper.UserID, Convert.ToInt32(AuthenticationHelper.CustomerID));

                            if (txnSuccess)
                            {
                                ContractManagement.CreateAuditLog("C", contractID, "Cont_tbl_TaskInstance", "Delete", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Task Deleted", true, Convert.ToInt32(taskID));

                                //Re-Bind Task
                                BindContractTasks_Paging(Convert.ToInt32(AuthenticationHelper.CustomerID), contractID, Convert.ToInt32(grdTaskActivity.PageSize), Convert.ToInt32(grdTaskActivity.PageIndex) + 1, grdTaskActivity);

                                cvTaskTab.IsValid = false;
                                cvTaskTab.ErrorMessage = "Task Deleted Successfully.";
                                vsTaskTab.CssClass = "alert alert-success";
                            }
                        }
                        else if (e.CommandName.Equals("TaskReminder"))
                        {
                            bool sendSuccess = false;

                            var newTaskRecord = ContractTaskManagement.GetTaskDetailsByTaskID(contractID, taskID);

                            if (newTaskRecord != null)
                            {
                                var TaskUserAssignment = ContractTaskManagement.GetContractTaskUserAssignment(Convert.ToInt32(AuthenticationHelper.CustomerID), contractID, taskID).ToList();

                                if (TaskUserAssignment.Count > 0)
                                {
                                    //Update URL Status to Active
                                    ContractTaskManagement.UpdateURLExpired(customerID, contractID, taskID, true);

                                    List<Tuple<int, int>> lstTaskUserMapping = new List<Tuple<int, int>>();
                                    foreach (var eachAssignedUser in TaskUserAssignment)
                                    {
                                        lstTaskUserMapping.Add(new Tuple<int, int>(eachAssignedUser.UserID, eachAssignedUser.RoleID));
                                    }

                                    sendSuccess = SendEmail_TaskAssignedUsers(customerID, contractID, newTaskRecord, lstTaskUserMapping, "Task Reminder");
                                }

                                if (sendSuccess)
                                {
                                    cvTaskTab.IsValid = false;
                                    cvTaskTab.ErrorMessage = "An Email containing Task detail and access URL to provide response sent to assignee.";
                                    vsTaskTab.CssClass = "alert alert-success";
                                }
                                else
                                {
                                    cvTaskTab.IsValid = false;
                                    cvTaskTab.ErrorMessage = "Something went wrong, Please try again.";
                                    vsTaskTab.CssClass = "alert alert-danger";
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvTaskTab.IsValid = false;
                cvTaskTab.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void grdEditTaskDocuments_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (ViewState["ContractInstanceID"] != null)
                {
                    int customerID = -1;
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                    long contFileID = 0;

                    if (e.CommandName.Equals("DownloadTaskEditDocument"))
                    {
                        contFileID = Convert.ToInt64(e.CommandArgument);

                        if (contFileID != 0)
                        {
                            bool downloadSuccess = DownloadContractDocument(contFileID);

                            if (!downloadSuccess)
                            {
                                cvTaskTab.IsValid = false;
                                cvTaskTab.ErrorMessage = "Something went wrong, Please try again.";
                                vsTaskTab.CssClass = "alert alert-danger";
                            }
                        }
                    }
                    else if (e.CommandName.Equals("ViewTaskEditDocument"))
                    {
                        contFileID = Convert.ToInt64(e.CommandArgument);

                        if (contFileID != 0)
                        {
                            bool downloadSuccess = ViewContractDocument(contFileID);

                            if (!downloadSuccess)
                            {
                                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "HideShowTask_Script", "HideShowTaskDiv('true');", true);
                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenDocviewer('" + DocumentPath + "');", true);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                cvTaskTab.IsValid = false;
                cvTaskTab.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void HideShowTaskDiv(bool flag)
        {
            if (flag)
            {
                pnlAddNewTask.Visible = true;
                pnlTaskList.Visible = false;
            }
            else if (!flag)
            {
                pnlAddNewTask.Visible = false;
                pnlTaskList.Visible = true;
            }

            //upContractTaskActivity.Update();
            //upAddEditTask.Update();
        }

        protected void lnkAddNewTask_Click(object sender, EventArgs e)
        {
            try
            {
                HideShowTaskDiv(true);
                AuditLog.Visible = false;
                clearTaskControls();
                clearTaskResponseLog();
                clearTaskAuditLog();

                toggleTextSaveButton_AddEditTask(true);

                tbxTaskAssignDate.Text = DateTime.Now.ToString("dd-MM-yyyy");

                BindContractDocuments_All(grdTaskContractDocuments);
                divnewtask.Visible = true;
                if (drdTemplateID.SelectedValue != null && drdTemplateID.SelectedValue != "" && drdTemplateID.SelectedValue != "-1")
                {
                    divnewtask.Visible = false;
                }
                
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Task_CheckAllDocument", "DefaultSelectAllTaskDocument();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        #endregion

        #region Contract Audit Log   

        protected void gvContractAuditLog_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (ViewState["ContractInstanceID"] != null)
                {
                    gvContractAuditLog.PageIndex = e.NewPageIndex;

                    BindContractAuditLogs();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        #endregion


        protected void lnkBtnUploadDocument_Click(object sender, EventArgs e)
        {
            var contractID = CryptographyManagement.Encrypt(ViewState["ContractInstanceID"].ToString());
            var FlagID = 1;
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenUploadDocumentPopup('" + contractID + "','" + FlagID + "');", true);
        }

        protected void lnkBtnAddNewTaskDoc_Click(object sender, EventArgs e)
        {
            var contractID = CryptographyManagement.Encrypt(ViewState["ContractInstanceID"].ToString());
            var FlagID = 2;
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenUploadDocumentPopup('" + contractID + "','" + FlagID + "');", true);
        }

        protected void btnSaveContDetail_Click(object sender, EventArgs e)
        {
            try
            {
                bool saveSuccess = false;
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                long contractID = Convert.ToInt64(ViewState["ContractInstanceID"]);

                var ExistingRecord = ContractManagement.ExistsContractDetails(contractID, customerID);

                if (ExistingRecord != null)
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        Cont_tbl_ContractDetail prevRecord = (from row in entities.Cont_tbl_ContractDetail
                                                              where row.ID == ExistingRecord.ID
                                                              select row).FirstOrDefault();
                        if (prevRecord != null)
                        {
                            prevRecord.IsActive = true;
                            prevRecord.Terms = txtTerms.Text.ToString();
                            prevRecord.Termination = txtTermination.Text.ToString();
                            prevRecord.Misc = txtMisc.Text.ToString();
                            prevRecord.License = txtLicense.Text.ToString();
                            prevRecord.Warranties = txtWarranties.Text.ToString();
                            prevRecord.Guarantees = txtGuarantees.Text.ToString();
                            prevRecord.Confidential = txtConfidential.Text.ToString();
                            prevRecord.Ownership = txtOwnership.Text.ToString();
                            prevRecord.Indemnification = txtIndemnification.Text.ToString();
                            prevRecord.General = txtGeneral.Text.ToString();
                            prevRecord.Timelines = txtTimelines.Text.ToString();
                            prevRecord.Escalation = txtEscalation.Text.ToString();
                            prevRecord.Milestone = txtMilestones.Text.ToString();
                            prevRecord.Others = txtOthers.Text.ToString();
                            prevRecord.UpdatedBy = Convert.ToInt32(AuthenticationHelper.UserID);
                            prevRecord.UpdatedOn = DateTime.Now;

                            entities.SaveChanges();

                            saveSuccess = true;
                        }
                    }
                }
                else
                {
                    Cont_tbl_ContractDetail newContRecord = new Cont_tbl_ContractDetail();

                    newContRecord.IsActive = true;
                    newContRecord.CustomerID = customerID;
                    newContRecord.ContractID = contractID;
                    newContRecord.Terms = txtTerms.Text.ToString();
                    newContRecord.Termination = txtTermination.Text.ToString();
                    newContRecord.Misc = txtMisc.Text.ToString();
                    newContRecord.License = txtLicense.Text.ToString();
                    newContRecord.Warranties = txtWarranties.Text.ToString();
                    newContRecord.Guarantees = txtGuarantees.Text.ToString();
                    newContRecord.Confidential = txtConfidential.Text.ToString();
                    newContRecord.Ownership = txtOwnership.Text.ToString();
                    newContRecord.Indemnification = txtIndemnification.Text.ToString();
                    newContRecord.General = txtGeneral.Text.ToString();
                    newContRecord.Timelines = txtTimelines.Text.ToString();
                    newContRecord.Escalation = txtEscalation.Text.ToString();
                    newContRecord.Milestone = txtMilestones.Text.ToString();
                    newContRecord.Others = txtOthers.Text.ToString();

                    newContRecord.CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID);
                    newContRecord.CreatedOn = DateTime.Now;

                    saveSuccess = ContractManagement.CreateContractDetails(newContRecord);
                }

                if (saveSuccess)
                {
                    cvContractDetails.IsValid = false;
                    cvContractDetails.ErrorMessage = "Contract Detail(s) Saved Successfully.";
                    vsContractDetails.CssClass = "alert alert-success";
                }
                else
                {
                    cvContractDetails.IsValid = false;
                    cvContractDetails.ErrorMessage = "Something went wrong, Please try again";
                    vsContractDetails.CssClass = "alert alert-danger";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void AddSteps(string[] items, System.Web.UI.WebControls.BulletedList ListName)
        {
            ListName.Items.Clear();
            ListName.Attributes["data-progtrckr-steps"] = items.Length.ToString();

            for (int i = 0; i < items.Length; i++)
            {
                ListName.Items.Add(new ListItem(items[i]));
            }
        }

        protected void SetProgress(long current, System.Web.UI.WebControls.BulletedList ListName)
        {
            for (int i = 0; i < ListName.Items.Count; i++)
            {
                ListName.Items[i].Attributes["class"] =
                    (i < current) ? "progtrckr-done" : (i == current) ? "progtrckr-current" : "progtrckr-todo";

                if (i == 3 && i > current)
                    ListName.Items[i].Attributes["class"] = "progtrckr-todo-closed";
            }
            if (current == 4)
                ListName.Items[Convert.ToInt32(current) - 1].Attributes["class"] = "progtrckr-closed";
        }

        public bool Save_ContractStatus(long contractID)
        {
            bool saveSuccess = false;
            try
            {
                #region Contract Status Transaction

                long newStatusID = 0;

                if (ViewState["ContractStatusID"] != null)
                {
                    long prevStatusID = Convert.ToInt64(ViewState["ContractStatusID"]);

                    if (prevStatusID != Convert.ToInt64(ddlContractStatus.SelectedValue))
                    {
                        newStatusID = Convert.ToInt64(ddlContractStatus.SelectedValue);
                    }
                }
                else
                {
                    newStatusID = Convert.ToInt64(ddlContractStatus.SelectedValue);
                }

                if (newStatusID != 0)
                {
                    Cont_tbl_ContractStatusTransaction newStatusRecord = new Cont_tbl_ContractStatusTransaction()
                    {
                        CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID),
                        ContractID = contractID,
                        StatusID = newStatusID,
                        StatusChangeOn = DateTime.Now,
                        IsActive = true,
                        CreatedBy = AuthenticationHelper.UserID,
                        CreatedOn = DateTime.Now,
                        UpdatedBy = AuthenticationHelper.UserID,
                        UpdatedOn = DateTime.Now,
                    };

                    saveSuccess = ContractManagement.CreateContractStatusTransaction(newStatusRecord);

                    if (saveSuccess)
                    {
                        ViewState["ContractStatusID"] = Convert.ToInt64(ddlContractStatus.SelectedValue);
                        ContractManagement.CreateAuditLog("C", contractID, "Cont_tbl_ContractStatusTransaction", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Contract Status Changed", true, Convert.ToInt32(newStatusID));
                    }
                }

                return saveSuccess;

                #endregion
            }
            catch (Exception ex)
            {
                return saveSuccess;
            }
        }

        protected void btnRenewContract_Click(object sender, EventArgs e)
        {
            bool renewSuccess = false;
            try
            {
                if (ViewState["ContractInstanceID"] != null)
                {
                    int customerID = -1;
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                    long oldContractID = Convert.ToInt64(ViewState["ContractInstanceID"]);
                    long newContractID = 0;

                    #region Contract Status Transaction - Renewed - Current Contract 

                    renewSuccess = Save_ContractStatus(oldContractID);

                    #endregion

                    if (renewSuccess)
                    {
                        renewSuccess = Save_ContractToContract_Renew(oldContractID, out newContractID);
                    }

                    if (renewSuccess)
                    {
                        cvRenewContract.IsValid = false;
                        cvRenewContract.ErrorMessage = "Contract Renewed Successfully";
                        vsRenewContract.CssClass = "alert alert-success";
                    }
                    else
                    {
                        cvRenewContract.IsValid = false;
                        cvRenewContract.ErrorMessage = "Something went wrong, Please try again";
                        vsRenewContract.CssClass = "alert alert-danger";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private bool Save_ContractToContract_Renew(long oldContractID, out long newContractID_Out)
        {
            bool saveSuccess = false;
            newContractID_Out = 0;
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                var oldContractRecord = ContractManagement.GetContractDetailsByContractID(customerID, oldContractID);

                if (oldContractRecord != null)
                {
                    long newContractID = 0;

                    #region Save New Contract Record

                    Cont_tbl_ContractInstance newContractRecord = new Cont_tbl_ContractInstance()
                    {
                        ContractType = oldContractRecord.ContractType,

                        CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID),
                        IsDeleted = false,

                        ContractNo = "Renewed-" + oldContractRecord.ContractNo,
                        ContractTitle = "Renewed-" + oldContractRecord.ContractTitle,
                        ContractDetailDesc = oldContractRecord.ContractDetailDesc,
                        CustomerBranchID = oldContractRecord.CustomerBranchID,
                        DepartmentID = oldContractRecord.DepartmentID,
                        ContractTypeID = oldContractRecord.ContractTypeID,
                        ContractSubTypeID = oldContractRecord.ContractSubTypeID,
                        //ProposalDate = oldContractRecord.ProposalDate,
                        //AgreementDate= oldContractRecord.AgreementDate,
                        EffectiveDate = oldContractRecord.EffectiveDate,
                        //ReviewDate = oldContractRecord.ReviewDate,
                        ExpirationDate = oldContractRecord.ExpirationDate,

                        NoticeTermNumber = oldContractRecord.NoticeTermNumber,
                        NoticeTermType = oldContractRecord.NoticeTermType,
                        PaymentTermID = oldContractRecord.PaymentTermID,
                        ContractAmt = oldContractRecord.ContractAmt,
                        AddNewClause = oldContractRecord.AddNewClause,
                        CreatedBy = AuthenticationHelper.UserID,
                        UpdatedBy = AuthenticationHelper.UserID
                    };

                    newContractID = ContractManagement.CreateContract(newContractRecord);

                    if (newContractID > 0)
                    {
                        ContractManagement.CreateAuditLog("C", oldContractID, "Cont_tbl_ContractInstance", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Contract Renewed and New Contract Created", true, Convert.ToInt32(newContractID));
                        ContractManagement.CreateAuditLog("C", newContractID, "Cont_tbl_ContractInstance", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Contract Created from Existing Contract", true, Convert.ToInt32(newContractID));
                        saveSuccess = true;
                    }

                    #endregion

                    if (saveSuccess)
                    {
                        newContractID_Out = newContractID;

                        #region Contract Mapping
                        Cont_tbl_ContractToContractMapping objContractMapping = new Cont_tbl_ContractToContractMapping()
                        {
                            OldContractID = oldContractID,
                            NewContractID = newContractID,
                            IsActive = true,
                            CreatedBy = AuthenticationHelper.UserID,
                            CreatedOn = DateTime.Now,

                            UpdatedBy = AuthenticationHelper.UserID,
                            UpdatedOn = DateTime.Now,
                        };

                        saveSuccess = ContractManagement.CreateUpdate_ContractToContractMapping(objContractMapping);

                        #endregion

                        #region Contract Status Transaction - Draft

                        Cont_tbl_ContractStatusTransaction newStatusRecord = new Cont_tbl_ContractStatusTransaction()
                        {
                            CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID),
                            ContractID = newContractID,
                            StatusID = Convert.ToInt64(1), //Draft
                            StatusChangeOn = DateTime.Now,
                            IsActive = true,
                            CreatedBy = AuthenticationHelper.UserID,
                            CreatedOn = DateTime.Now,
                            UpdatedBy = AuthenticationHelper.UserID,
                            UpdatedOn = DateTime.Now,
                        };

                        if (!ContractManagement.Exist_ContractStatusTransaction(newStatusRecord))
                            saveSuccess = ContractManagement.CreateContractStatusTransaction(newStatusRecord);

                        #endregion

                        #region Vendor Mapping

                        var lstVendorMapping = VendorDetails.GetVendorMapping(oldContractID);

                        if (lstVendorMapping.Count > 0)
                        {
                            List<Cont_tbl_VendorMapping> lstVendorMapping_ToSave = new List<Cont_tbl_VendorMapping>();

                            lstVendorMapping.ForEach(EachVendor =>
                            {
                                Cont_tbl_VendorMapping _vendorMappingRecord = new Cont_tbl_VendorMapping()
                                {
                                    ContractID = newContractID,
                                    VendorID = EachVendor,
                                    IsActive = true,
                                    CreatedBy = AuthenticationHelper.UserID,
                                    CreatedOn = DateTime.Now,
                                };

                                lstVendorMapping_ToSave.Add(_vendorMappingRecord);
                            });

                            saveSuccess = VendorDetails.CreateUpdate_VendorMapping(lstVendorMapping_ToSave);
                            if (saveSuccess)
                            {
                                ContractManagement.CreateAuditLog("C", newContractID, "Cont_tbl_VendorMapping", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Contract Vendor Mapping Created", true, Convert.ToInt32(newContractID));
                            }

                            //Refresh List
                            lstVendorMapping_ToSave.Clear();
                            lstVendorMapping_ToSave = null;

                            lstVendorMapping.Clear();
                            lstVendorMapping = null;
                        }

                        #endregion

                        #region Custom Field(s)

                        var existingCustomFields = ContractManagement.GetCustomsFieldsContractWise_All(Convert.ToInt32(AuthenticationHelper.CustomerID), oldContractID, oldContractRecord.ContractTypeID);

                        if (existingCustomFields.Count > 0)
                        {
                            List<Cont_tbl_CustomFieldValue> lstObjParameters = new List<Cont_tbl_CustomFieldValue>();

                            existingCustomFields.ForEach(EachCustomField =>
                            {
                                Cont_tbl_CustomFieldValue ObjParameter = new Cont_tbl_CustomFieldValue()
                                {
                                    ContractID = newContractID,

                                    LabelID = EachCustomField.LableID,
                                    LabelValue = EachCustomField.LabelValue,

                                    IsDeleted = false,
                                    CreatedBy = AuthenticationHelper.UserID,
                                    CreatedOn = DateTime.Now,
                                    UpdatedBy = AuthenticationHelper.UserID,
                                    UpdatedOn = DateTime.Now
                                };

                                lstObjParameters.Add(ObjParameter);
                            });

                            saveSuccess = ContractManagement.CreateUpdate_CustomsFields(lstObjParameters);

                            if (saveSuccess)
                            {
                                ContractManagement.CreateAuditLog("C", newContractID, "Cont_tbl_CustomFieldValue", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Contract Custom Parameter(s) Added", true, Convert.ToInt32(newContractID));
                            }
                        }

                        #endregion

                        #region User Assignment                           

                        var lstContractUserMapping = ContractManagement.GetContractUserAssignment_All(oldContractID);

                        if (lstContractUserMapping.Count > 0)
                        {
                            List<Cont_tbl_UserAssignment> lstUserAssignmentRecord_ToSave = new List<Cont_tbl_UserAssignment>();

                            lstContractUserMapping.ForEach(eachUser =>
                            {
                                Cont_tbl_UserAssignment newAssignment = new Cont_tbl_UserAssignment()
                                {
                                    AssignmentType = 1,
                                    ContractID = newContractID,

                                    UserID = Convert.ToInt32(eachUser.UserID),
                                    RoleID = Convert.ToInt32(eachUser.RoleID),

                                    IsActive = true,
                                    CreatedBy = AuthenticationHelper.UserID,
                                    UpdatedBy = AuthenticationHelper.UserID,
                                };

                                lstUserAssignmentRecord_ToSave.Add(newAssignment);
                            });

                            if (lstUserAssignmentRecord_ToSave.Count > 0)
                                saveSuccess = ContractManagement.CreateUpdate_ContractUserAssignments(lstUserAssignmentRecord_ToSave);

                            if (saveSuccess)
                            {
                                ContractManagement.CreateAuditLog("C", newContractID, "Cont_tbl_UserAssignment", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Contract Owner(s) Created", true, Convert.ToInt32(newContractID));
                            }

                            lstUserAssignmentRecord_ToSave.Clear();
                            lstUserAssignmentRecord_ToSave = null;
                        }

                        #endregion
                    }
                }
                return saveSuccess;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return saveSuccess;
            }
        }

        protected void btnApplyFilter_AuditLog_Click(object sender, EventArgs e)
        {
            try
            {
                BindContractAuditLogs();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnClearFilter_AuditLog_Click(object sender, EventArgs e)
        {
            try
            {
                ddlUsers_AuditLog.ClearSelection();

                txtFromDate_AuditLog.Text = string.Empty;
                txtToDate_AuditLog.Text = string.Empty;
                BindContractAuditLogs();

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


        protected void grdContractHistory_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandArgument != null)
                {
                    if (e.CommandName.Equals("ViewContractPopup"))
                    {
                        long contractID = Convert.ToInt64(e.CommandArgument);

                        if (contractID != 0)
                        {
                            string HistoryFlag = " true";
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "OpenContractHistoryPopup(" + contractID + "," + HistoryFlag + ");", true);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdContractHistory_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (ViewState["ContractInstanceID"] != null)
                {
                    int customerID = -1;
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                    grdContractHistory.PageIndex = e.NewPageIndex;

                    BindContractHistory(customerID, Convert.ToInt64(ViewState["ContractInstanceID"]));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

        }

        protected void grdTaskContractDocuments_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdTaskContractDocuments.PageIndex = e.NewPageIndex;
            BindContractDocuments_All(grdTaskContractDocuments);
        }

        protected void lnkContractTransaction_Click(object sender, EventArgs e)
        {
            ViewState["PageLink"] = "ContractLink";
            showHideContractSummaryTabTopButtons(false);

            int newContractID = -1;
            if (ViewState["ContractInstanceID"] != null)
            {
                newContractID = Convert.ToInt32(ViewState["ContractInstanceID"]);
            }
            RoleId = getRoleID(Convert.ToInt32(newContractID), AuthenticationHelper.UserID);
            ContractTemplateInstanceId = Convert.ToInt32(newContractID);
            UId = Convert.ToInt32(AuthenticationHelper.UserID);
            CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            
            liContractSummary.Attributes.Add("class", "");
            liContractDetails.Attributes.Add("class", "");
            liDocument.Attributes.Add("class", "");
            liTask.Attributes.Add("class", "");
            liAuditLog.Attributes.Add("class", "");
            liContractTransaction.Attributes.Add("class", "active");
            liMilestone.Attributes.Add("class", "");
            MainView.ActiveViewIndex = 5;

           
        }
        
        protected void lnkbtnMilestone_Click(object sender, EventArgs e)
        {
            ViewState["MilestonePageFlag"] = "0";
            ViewState["PageLink"] = "ContractLink";
            showHideContractSummaryTabTopButtons(false);
            //Button1.Enabled = false;
            int newContractID = -1;
            if (ViewState["ContractInstanceID"] != null)
            {
                newContractID = Convert.ToInt32(ViewState["ContractInstanceID"]);
            }
            RoleId = getRoleID(Convert.ToInt32(newContractID), AuthenticationHelper.UserID);
            ContractTemplateInstanceId = Convert.ToInt32(newContractID);
            UId = Convert.ToInt32(AuthenticationHelper.UserID);
            CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);

            liContractSummary.Attributes.Add("class", "");
            liContractDetails.Attributes.Add("class", "");
            liDocument.Attributes.Add("class", "");
            liTask.Attributes.Add("class", "");
            liAuditLog.Attributes.Add("class", "");
            liContractTransaction.Attributes.Add("class", "");
            liMilestone.Attributes.Add("class", "active");
            
            MainView.ActiveViewIndex = 6;
            HideShowMilestoneDiv(false);

             if (ViewState["ContractInstanceID"] != null)
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                long contractInstanceID = Convert.ToInt64(ViewState["ContractInstanceID"]);

                if (contractInstanceID != 0)
                {
                    BindContractMilestone_Paging(customerID, contractInstanceID, Convert.ToInt32(gridMilestone.PageSize), Convert.ToInt32(gridMilestone.PageIndex) + 1, gridMilestone);
                    BindMilestonedrp(contractInstanceID, customerID);
                }

                //using (ComplianceDBEntities entities = new ComplianceDBEntities())
                //{
                //    var getrecord = (from row in entities.Cont_tbl_ContractInstance
                //                     where row.ID == contractInstanceID
                //                     select row).FirstOrDefault();

                //    if (getrecord != null)
                //    {
                //        if (getrecord.Version != null)
                //        {
                //            int SectionTemplateID = getContractStatusID(Convert.ToInt32(contractInstanceID));
                //            if (SectionTemplateID == 7)
                //            {
                //                Button1.Enabled = true;
                //            }
                //            else
                //            {
                //                if (getrecord.Version != "1.0")
                //                {
                //                    Button1.Enabled = true;
                //                }
                //            }
                //        }
                //    }
                //}
            }
        }
        public void BindMilestonedrp(long contractID, long customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var getrecord = (from row in entities.Cont_tbl_MilestoneDetail
                                 where row.ContractTemplateID == contractID
                                 && row.CustomerID == customerID
                                 && row.IsActive == true
                                 select row).ToList();

                drpdownmilestone.DataValueField = "ID";
                drpdownmilestone.DataTextField = "Title";
                drpdownmilestone.DataSource = getrecord;
                drpdownmilestone.DataBind();
                drpdownmilestone.Items.Insert(0, new ListItem("Select Milestone", "-1"));

            }
        }
        public int getContractStatusID(int ContractTemplateId)
        {
            int StatusID = -1;
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var data = (from row in entities.Cont_View_RecentContractStatusTransaction
                                join row1 in entities.Cont_tbl_ContractInstance
                                on row.ContractID equals row1.ID
                                where row1.ID == ContractTemplateId
                                                       && row1.IsDeleted == false
                                                       && row.CustomerID == customerID
                                select row).FirstOrDefault();
                    if (data != null)
                    {
                        if (data.ContractStatusID != null)
                            StatusID = Convert.ToInt32(data.ContractStatusID);
                    }
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return StatusID;
        }
        #region Milestone

        //protected void lnkbtnMilestoneadd_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        HideShowMilestoneDiv(true);
        //        clearMileStoneControls();

        //        ViewState["MilestoneMode"] = "0";
        //        int newContractID = -1;
        //        if (ViewState["ContractInstanceID"] != null)
        //        {
        //            newContractID = Convert.ToInt32(ViewState["ContractInstanceID"]);
        //        }
                
        //        BindMilestoneResponses(newContractID, 0);
        //        divmilestoneresponselog.Visible = false;
        //        drpmilestonestatus.SelectedValue = "1";
        //        drpmilestonestatus.Enabled = false;

        //        divMilestoneTypelbl.Visible = true;
        //        divMilestoneType.Visible = true;
        //        RdoMilestoneType.SelectedValue = "2";
        //        divfrequencylbl.Visible = false;
        //        divfrequencydrp.Visible = false;
        //        //recurringcustomdate.Visible = false;
        //        grdNoticePayment.Visible = false;
        //        divduedatelbl.Visible = true;
        //        divduedate.Visible = true;
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //}

        protected void HideShowMilestoneDiv(bool flag)
        {
            if (flag)
            {
                Paneladdmilestone.Visible = true;
                PanelMilestonedetail.Visible = false;
            }
            else if (!flag)
            {
                Paneladdmilestone.Visible = false;
                PanelMilestonedetail.Visible = true;
            }
        }

        protected void btnmilestoneclosed_Click(object sender, EventArgs e)
        {
            try
            {
                clearMileStoneControls();
                HideShowMilestoneDiv(false);                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindSectionDetail(int ContractID)
        {
            if (ContractID != -1)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int customerID = -1;
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                    var entitiesData = (from row in entities.Cont_SP_GetContractSectionsDetail(customerID, Convert.ToInt32(ContractID))
                                        select row).ToList();

                    grdTemplateSection.DataSource = entitiesData;
                    grdTemplateSection.DataBind();
                }
            }           
        }

        public void BindFieldDetail(int ContractID)
        {
            if (ContractID != -1)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int customerID = -1;
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                    var entitiesData = (from row in entities.Cont_tbl_ContractField
                                        where row.IsActive == true
                                        select row).ToList();

                    grdContractField.DataSource = entitiesData;
                    grdContractField.DataBind();
                }
            }
        }

        public void BindDeptwiseuser(int dept)
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            var lstAllUsers = ContractUserManagement.GetAllUsers_Contract(customerID);

            if (lstAllUsers.Count > 0)
                lstAllUsers = lstAllUsers.Where(entry => entry.ContractRoleID != null).ToList();

            var allUsers = ContractUserManagement.GetRequiredUsers(lstAllUsers, 4); //4--All---Internal and External Users

            drpmilestoneDepartmentUser.DataValueField = "ID";
            drpmilestoneDepartmentUser.DataTextField = "Name";
            drpmilestoneDepartmentUser.DataSource = allUsers;
            drpmilestoneDepartmentUser.DataBind();
        }

        protected void drpmilestoneDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (drpmilestoneDepartment.SelectedValue != "" && drpmilestoneDepartment.SelectedValue != "-1")
                {
                    BindDeptwiseuser(Convert.ToInt32(drpmilestoneDepartment.SelectedValue));
                }         
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void clearMileStoneControls()
        {
            try
            {
                txtboxMilestonetitle.Text = "";
                txtboxMilestonedescription.Text = "";
                drpmilestoneDepartment.ClearSelection();
                drpmilestoneDepartmentUser.ClearSelection();
                txbmilestoneduedate.Text = ""; 
                drpmilestonestatus.SelectedValue = "1";
                //Tbxstartdate.Text = "";
                //Tbxenddate.Text = "";
                txbRemark.Text = "";
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
        }

        public class updateschedule
        {
            public DateTime StartDate { get; set; }
            public DateTime EndDate { get; set; }
            public DateTime DueDate { get; set; }
            public string Forperiod { get; set; }
            public int sequenceID { get; set; }
        }

        public class chk
        {
            public DateTime StartDate { get; set; }
            public DateTime EndDate { get; set; }
            public DateTime DueDate { get; set; }
            public int sequenceID { get; set; }
        }

        protected void btnCreateMilestone_Click(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["MilestoneMode"].ToString() == "0")
                {
                    int newContractID = -1;
                    if (ViewState["ContractInstanceID"] != null)
                    {
                        newContractID = Convert.ToInt32(ViewState["ContractInstanceID"]);
                    }

                    bool formValidateSuccess = false;
                    bool saveSuccess = false;

                    List<string> lstErrorMsg = new List<string>();

                    #region Data Validation

                    if (String.IsNullOrEmpty(txtboxMilestonetitle.Text))
                        lstErrorMsg.Add("Provide Milestone Title.");
                    else
                        formValidateSuccess = true;

                    if (String.IsNullOrEmpty(txtboxMilestonedescription.Text))
                        lstErrorMsg.Add("Provide Milestone Description");
                    else
                        formValidateSuccess = true;

                    if (drpmilestoneDepartment.SelectedValue == "" || drpmilestoneDepartment.SelectedValue == "-1")
                        lstErrorMsg.Add("Please select Department.");
                    else
                        formValidateSuccess = true;

                    if (drpmilestoneDepartmentUser.SelectedValue == "" || drpmilestoneDepartmentUser.SelectedValue == "-1")
                        lstErrorMsg.Add("Please select User.");
                    else
                        formValidateSuccess = true;

                    if (RdoMilestoneType.SelectedValue != "-1")
                    {
                        if (RdoMilestoneType.SelectedValue == "2")
                        {
                            if (String.IsNullOrEmpty(txbmilestoneduedate.Text))
                                lstErrorMsg.Add("Provide Due Date.");
                            else
                                formValidateSuccess = true;
                        }
                        else if (RdoMilestoneType.SelectedValue == "1")
                        {
                            if (DRPMilestonePeriod.SelectedValue == "-1")
                                lstErrorMsg.Add("Please Select Frequency.");
                            else
                                formValidateSuccess = true;

                            if (String.IsNullOrEmpty(txtEffectiveDate.Text))
                                lstErrorMsg.Add("Start Date of contract required.");
                            else
                                formValidateSuccess = true;

                            if (String.IsNullOrEmpty(txtExpirationDate.Text))
                                lstErrorMsg.Add("End Date of contract required.");
                            else
                                formValidateSuccess = true;
                        }
                    }

                    if (drpmilestonestatus.SelectedValue == "" || drpmilestonestatus.SelectedValue == "-1")
                        lstErrorMsg.Add("Select Status");
                    else
                        formValidateSuccess = true;

                    if (String.IsNullOrEmpty(txbRemark.Text))
                        lstErrorMsg.Add("Provide Remark");
                    else
                        formValidateSuccess = true;



                    if (DRPMilestonePeriod.SelectedValue != "" && DRPMilestonePeriod.SelectedValue != "-1")
                    {
                        var TempCustomSchedule = ContractManagement.getMilestonedetail(newContractID, DRPMilestonePeriod.SelectedValue, AuthenticationHelper.CustomerID);
                        if (TempCustomSchedule.Count > 0)
                        {
                            if (TempCustomSchedule.Count == 4 && DRPMilestonePeriod.SelectedValue == "Q")
                            {
                                formValidateSuccess = true;
                            }
                            else if (TempCustomSchedule.Count == 12 && DRPMilestonePeriod.SelectedValue == "M")
                            {
                                formValidateSuccess = true;
                            }
                            else if (TempCustomSchedule.Count == 1 && DRPMilestonePeriod.SelectedValue == "Y")
                            {
                                formValidateSuccess = true;
                            }
                            else if (TempCustomSchedule.Count == 2 && DRPMilestonePeriod.SelectedValue == "H")
                            {
                                formValidateSuccess = true;
                            }
                            else
                            {
                                lstErrorMsg.Add("Provide Qaurter");
                            }
                        }
                    }
                    if (lstErrorMsg.Count > 0)
                    {
                        formValidateSuccess = false;
                        showErrorMessages(lstErrorMsg, cvMilestoneTab);
                    }
                    #endregion

                    if (formValidateSuccess)
                    {
                        if (newContractID != -1)
                        {
                            int userID = -1;
                            userID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);

                            int customerID = -1;
                            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                            {
                                if (ViewState["MilestoneMode"].ToString() == "0")
                                {
                                    DateTime ContractStartDate = Convert.ToDateTime(txtEffectiveDate.Text);
                                    DateTime ContractEndDate = Convert.ToDateTime(txtExpirationDate.Text);

                                    if (RdoMilestoneType.SelectedValue == "1")//recurring custom
                                    {
                                        List<updateschedule> updatedata = new List<updateschedule>();

                                        if (DRPMilestonePeriod.SelectedValue != "-1")
                                        {                                           
                                            int firstyear = ContractStartDate.Year;
                                            int lastyear = ContractEndDate.Year;
                                            
                                            int month = ContractStartDate.Month;

                                            long tempId = 0;
                                            string frommonth = string.Empty;
                                            int Addmonth = 0;
                                            if (DRPMilestonePeriod.SelectedValue == "M")
                                            {
                                                Addmonth = 1;
                                            }
                                            if (DRPMilestonePeriod.SelectedValue == "Q")
                                            {
                                                Addmonth = 3;
                                            }
                                            if (DRPMilestonePeriod.SelectedValue == "H")
                                            {
                                                Addmonth = 6;
                                            }
                                            if (DRPMilestonePeriod.SelectedValue == "Y")
                                            {
                                                Addmonth = 12;
                                            }
                                            
                                            var TempCustomSchedule = ContractManagement.getMilestonedetail(newContractID, DRPMilestonePeriod.SelectedValue, AuthenticationHelper.CustomerID);
                                            if (TempCustomSchedule.Count > 0)
                                            {
                                                if (DRPMilestonePeriod.SelectedValue == "Y")
                                                {
                                                    tempId = TempCustomSchedule[0].ID;
                                                    frommonth = TempCustomSchedule[0].FromMonth;
                                                }
                                                else
                                                {
                                                    foreach (var item in TempCustomSchedule)
                                                    {

                                                        //int yeartemp1 = firstyear;
                                                        //DateTime d11 = DateTime.Parse(item.FromMonth);
                                                        //DateTime from11 = new DateTime(yeartemp1, Convert.ToInt32(d11.Month), Convert.ToInt32(d11.Day));
                                                        //DateTime d21 = DateTime.Parse(item.ToMonth);
                                                        //DateTime to11 = new DateTime(yeartemp1, Convert.ToInt32(d21.Month), Convert.ToInt32(d21.Day));

                                                        //if (from11.Month >= ContractStartDate.Month
                                                        //    && to11.Month <= ContractStartDate.Month)
                                                        //{

                                                        //}

                                                        //int yeartemp = firstyear;
                                                        //DateTime d1 = DateTime.Parse(item.FromMonth);
                                                        //DateTime from1 = new DateTime(yeartemp, Convert.ToInt32(d1.Month), Convert.ToInt32(d1.Day));
                                                        //DateTime d2 = DateTime.Parse(item.ToMonth);
                                                        //DateTime to1 = new DateTime(yeartemp, Convert.ToInt32(d2.Month), Convert.ToInt32(d2.Day));

                                                        //if (from1 >= ContractStartDate)
                                                        //{
                                                        //    if (to1 <= ContractStartDate)
                                                        //    {
                                                        //        tempId = item.ID;
                                                        //        frommonth = item.FromMonth;
                                                        //    }
                                                        //}

                                                        DateTime ContractStartDate1 = ContractStartDate;
                                                        int yeartemp = firstyear;
                                                        DateTime d1 = DateTime.Parse(item.FromMonth);
                                                        DateTime d2 = DateTime.Now;
                                                        if (item.ToMonth.Trim() == "29 February")
                                                        {
                                                            try
                                                            {
                                                                d2 = DateTime.Parse(item.ToMonth);
                                                            }
                                                            catch
                                                            {
                                                                d2 = DateTime.Parse("28 February");
                                                            }
                                                        }
                                                        else
                                                        {
                                                            d2 = DateTime.Parse(item.ToMonth);
                                                        }
                                                        DateTime frmtemp = new DateTime(yeartemp, Convert.ToInt32(d1.Month), Convert.ToInt32(d1.Day));
                                                        if (d1.Month >= 4)
                                                        {
                                                            if (d1.Month + Addmonth >= 13)
                                                            {
                                                                yeartemp = yeartemp + 1;
                                                                if (ContractStartDate1.Month>=1 && ContractStartDate1.Month <= 3)
                                                                {
                                                                    ContractStartDate1 = ContractStartDate1.AddYears(1);
                                                                }
                                                            }
                                                        }
                                                        DateTime totemp = DateTime.Now;
                                                        if (item.ToMonth.Trim() == "29 February")
                                                        {
                                                            try
                                                            {
                                                                totemp = new DateTime(yeartemp, Convert.ToInt32(d2.Month), Convert.ToInt32(d2.Day));
                                                            }
                                                            catch
                                                            {
                                                                d2 = DateTime.Parse("28 February");
                                                                totemp = new DateTime(yeartemp, Convert.ToInt32(d2.Month), Convert.ToInt32(d2.Day));
                                                            }
                                                        }
                                                        else
                                                        {
                                                            totemp = new DateTime(yeartemp, Convert.ToInt32(d2.Month), Convert.ToInt32(d2.Day));
                                                        }
                                                        if (frmtemp <= ContractStartDate1 && totemp > ContractStartDate1)
                                                        {
                                                            tempId = item.ID;
                                                            frommonth = item.FromMonth;
                                                        }
                                                    }
                                                }
                                            }   
                                            
                                            int cnt = 1;
                                            updatedata.Clear();
                                            if (tempId > 0 && !string.IsNullOrEmpty(frommonth))
                                            {
                                                if (DRPMilestonePeriod.SelectedValue == "M"
                                                    || DRPMilestonePeriod.SelectedValue == "H"
                                                   || DRPMilestonePeriod.SelectedValue == "Q"
                                                   || DRPMilestonePeriod.SelectedValue == "Y")
                                                {                                                    
                                                    #region Half-Yearly,Yearly
                                                    DateTime enddt = ContractStartDate;
                                                    while (enddt <= ContractEndDate)
                                                    {
                                                        if (cnt == 1)
                                                        {
                                                            #region first
                                                            var getdata = TempCustomSchedule.Where(x => x.FromMonth.Trim() == frommonth.Trim() && x.ID == tempId).FirstOrDefault();
                                                            DateTime getstartdt = DateTime.Parse(getdata.FromMonth);
                                                            DateTime getenddt = DateTime.Parse(getdata.ToMonth);
                                                            DateTime startdt = new DateTime(firstyear, Convert.ToInt32(getstartdt.Month), Convert.ToInt32(getstartdt.Day));
                                                            if (DRPMilestonePeriod.SelectedValue == "Y")
                                                            {
                                                                if (startdt.Month == 1 && startdt.Day == 1)
                                                                {
                                                                    //firstyear = firstyear + 1;
                                                                }
                                                                else
                                                                    firstyear = firstyear + 1;
                                                                //if (startdt.Month != 1 && startdt.Day != 1)
                                                                //{
                                                                //    firstyear = firstyear + 1;
                                                                //}                                              
                                                            }
                                                            else
                                                            {
                                                                if (startdt.Month != getenddt.Month)
                                                                {
                                                                    if (startdt.Month == 12 && startdt.Day != 1)
                                                                    {
                                                                        firstyear = firstyear + 1;
                                                                    }
                                                                    else if (startdt.Month >= 4)
                                                                    {
                                                                        //if (ContractStartDate.Month + Addmonth >= 13 && getenddt.Month != 12)
                                                                        //{
                                                                        //    firstyear = firstyear + 1;
                                                                        //}
                                                                        if (Addmonth == 3)
                                                                        {
                                                                            if (ContractStartDate.Month <= 2)
                                                                            {
                                                                                if (ContractStartDate.Month + Addmonth >= 13 && getenddt.Month != 12)
                                                                                {
                                                                                    firstyear = firstyear + 1;
                                                                                }
                                                                            }
                                                                            else
                                                                            {
                                                                                if (startdt.Month + Addmonth >= 13 && getenddt.Month != 12)
                                                                                {
                                                                                    firstyear = firstyear + 1;
                                                                                }
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            if (startdt.Month + Addmonth >= 13 && getenddt.Month != 12)
                                                                            {
                                                                                firstyear = firstyear + 1;
                                                                            }
                                                                        }
                                                                        //if (Addmonth == 1)
                                                                        //{
                                                                        //    if (startdt.Month + Addmonth >= 13)
                                                                        //    {
                                                                        //        firstyear = firstyear + 1;
                                                                        //    }
                                                                        //}
                                                                        //else
                                                                        //{
                                                                        //    if (startdt.Month + Addmonth > 13)
                                                                        //    {
                                                                        //        firstyear = firstyear + 1;
                                                                        //    }
                                                                        //}
                                                                    }
                                                                }
                                                            }
                                                            enddt = new DateTime(firstyear, Convert.ToInt32(getenddt.Month), Convert.ToInt32(getenddt.Day));
                                                            //DateTime duedatetemp = DateTime.Parse(getdata.DueDate);
                                                            DateTime duedatetemp = DateTime.Now;
                                                            if (getdata.DueDate == "29 February")                                                            
                                                                duedatetemp = DateTime.Parse("28 February");                                                            
                                                            else
                                                                duedatetemp = DateTime.Parse(getdata.DueDate);

                                                            DateTime Duedate = DateTime.Now;
                                                            if (ContractStartDate.Year == enddt.Year)
                                                            {
                                                                Duedate = new DateTime(firstyear, Convert.ToInt32(duedatetemp.Month), Convert.ToInt32(duedatetemp.Day));
                                                            }
                                                            else
                                                            {
                                                                if (ContractStartDate.Month == duedatetemp.Month)
                                                                {
                                                                    Duedate = new DateTime(ContractStartDate.Year, Convert.ToInt32(duedatetemp.Month), Convert.ToInt32(duedatetemp.Day));
                                                                }
                                                                else if (enddt.Month == duedatetemp.Month)
                                                                {
                                                                    Duedate = new DateTime(enddt.Year, Convert.ToInt32(duedatetemp.Month), Convert.ToInt32(duedatetemp.Day));
                                                                }
                                                                else
                                                                {
                                                                    DateTime StartDateDuedate = ContractStartDate;
                                                                    DateTime EndDateDuedate = enddt;

                                                                    while (StartDateDuedate <= EndDateDuedate)
                                                                    {
                                                                        if (StartDateDuedate.Month == duedatetemp.Month)
                                                                        {
                                                                            Duedate = new DateTime(StartDateDuedate.Year, Convert.ToInt32(duedatetemp.Month), Convert.ToInt32(duedatetemp.Day));
                                                                            break;
                                                                        }
                                                                        StartDateDuedate = StartDateDuedate.AddMonths(1);
                                                                    }
                                                                }
                                                            }

                                                            if (ContractEndDate <= enddt)
                                                            {
                                                                string ToMonth = getdata.ToMonth;
                                                                if (getdata.ToMonth.Trim() == "28 February")
                                                                {
                                                                    string tempmonth = string.Format("{0:dd MMMM}", enddt.AddDays(1));
                                                                    if (tempmonth.Trim() == "29 February")
                                                                    {
                                                                        ToMonth = tempmonth;
                                                                        enddt = enddt.AddDays(1);
                                                                    }
                                                                }

                                                                updatedata.Add(new updateschedule
                                                                {
                                                                    StartDate = ContractStartDate,
                                                                    EndDate = ContractEndDate,
                                                                    sequenceID = cnt,
                                                                    DueDate = Duedate,
                                                                    Forperiod = getdata.FromMonth + "-" + ToMonth
                                                                });
                                                                break;
                                                            }
                                                            else
                                                            {
                                                                string ToMonth = getdata.ToMonth;
                                                                if (getdata.ToMonth.Trim() == "28 February")
                                                                {
                                                                    string tempmonth = string.Format("{0:dd MMMM}", enddt.AddDays(1));
                                                                    if (tempmonth.Trim() == "29 February")
                                                                    {
                                                                        ToMonth = tempmonth;
                                                                        enddt = enddt.AddDays(1);
                                                                    }
                                                                }
                                                                updatedata.Add(new updateschedule
                                                                {
                                                                    StartDate = ContractStartDate,
                                                                    EndDate = enddt,
                                                                    sequenceID = cnt,
                                                                    DueDate = Duedate,
                                                                    Forperiod = getdata.FromMonth + "-" + ToMonth
                                                                });
                                                            }
                                                            #endregion
                                                        }
                                                        else
                                                        {                                                            
                                                            var getdata = TempCustomSchedule.Where(x => x.FromMonth.Trim() == frommonth.Trim()).FirstOrDefault();
                                                            DateTime getstartdt = DateTime.Parse(getdata.FromMonth);
                       
                                                            if (getstartdt.Month == 1 && getstartdt.Day == 1)
                                                            {
                                                                #region if 1st date in year
                                                                firstyear = firstyear + 1;
                                                                DateTime ssd = new DateTime(firstyear, Convert.ToInt32(getstartdt.Month), Convert.ToInt32(getstartdt.Day));
                                                                DateTime getenddt = DateTime.Parse(getdata.ToMonth);
                                                                enddt = new DateTime(firstyear, Convert.ToInt32(getenddt.Month), Convert.ToInt32(getenddt.Day));
                                                                DateTime duedatetemp = DateTime.Parse(getdata.DueDate);
                                                                DateTime Duedate = DateTime.Now;

                                                                Duedate = new DateTime(firstyear, Convert.ToInt32(duedatetemp.Month), Convert.ToInt32(duedatetemp.Day));

                                                                if (ContractEndDate <= enddt)
                                                                {
                                                                    string ToMonth = getdata.ToMonth;
                                                                    updatedata.Add(new updateschedule
                                                                    {
                                                                        StartDate = ssd,
                                                                        EndDate = ContractEndDate,
                                                                        sequenceID = cnt,
                                                                        DueDate = Duedate,
                                                                        Forperiod = getdata.FromMonth + "-" + ToMonth
                                                                    });
                                                                    break;
                                                                }
                                                                else
                                                                {
                                                                    string ToMonth = getdata.ToMonth;
                                                                    updatedata.Add(new updateschedule
                                                                    {
                                                                        StartDate = ssd,
                                                                        EndDate = enddt,
                                                                        sequenceID = cnt,
                                                                        DueDate = Duedate,
                                                                        Forperiod = getdata.FromMonth + "-" + ToMonth
                                                                        //Forperiod = getdata.FromMonth + "-" + getdata.ToMonth
                                                                    });
                                                                }
                                                                #endregion
                                                            }
                                                            else
                                                            {
                                                                #region get other all till end 
                                                                DateTime ssd = new DateTime(firstyear, Convert.ToInt32(getstartdt.Month), Convert.ToInt32(getstartdt.Day));
                                                                DateTime getenddt = DateTime.Now;
                                                                if (getdata.ToMonth == "29 February")
                                                                {
                                                                    try
                                                                    {
                                                                        getenddt = DateTime.Parse(getdata.ToMonth);
                                                                    }
                                                                    catch
                                                                    {
                                                                        getenddt = DateTime.Parse("28 February");
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    getenddt = DateTime.Parse(getdata.ToMonth);
                                                                }
                                                                //DateTime getenddt = DateTime.Parse(getdata.ToMonth);
                                                                if (DRPMilestonePeriod.SelectedValue == "Y")
                                                                {
                                                                    firstyear = firstyear + 1;
                                                                }
                                                                else
                                                                {
                                                                    if (ssd.Month != getenddt.Month)
                                                                    {
                                                                        if (ssd.Month == 12 && ssd.Day != 1)
                                                                        {
                                                                            firstyear = firstyear + 1;
                                                                        }
                                                                        else if (ssd.Month >= 4)
                                                                        {
                                                                            if (ssd.Month + Addmonth >= 13 && getenddt.Month != 12)
                                                                            {
                                                                                firstyear = firstyear + 1;
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                if (getdata.ToMonth == "29 February")
                                                                {
                                                                    try
                                                                    {
                                                                        enddt = new DateTime(firstyear, Convert.ToInt32(getenddt.Month), Convert.ToInt32(getenddt.Day));
                                                                    }
                                                                    catch {
                                                                        enddt = new DateTime(firstyear, Convert.ToInt32(getenddt.Month), Convert.ToInt32(getenddt.Day - 1));
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    enddt = new DateTime(firstyear, Convert.ToInt32(getenddt.Month), Convert.ToInt32(getenddt.Day));
                                                                }
                                                                //enddt = new DateTime(firstyear, Convert.ToInt32(getenddt.Month), Convert.ToInt32(getenddt.Day));

                                                                DateTime duedatetemp = DateTime.Now;
                                                                if (getdata.DueDate == "29 February")                                                                
                                                                    duedatetemp = DateTime.Parse("28 February");
                                                                else
                                                                    duedatetemp = DateTime.Parse(getdata.DueDate);
                                                                
                                                                //DateTime duedatetemp = DateTime.Parse(getdata.DueDate);
                                                                DateTime Duedate = DateTime.Now;
                                                                if (ssd.Year == enddt.Year)
                                                                {
                                                                    Duedate = new DateTime(firstyear, Convert.ToInt32(duedatetemp.Month), Convert.ToInt32(duedatetemp.Day));
                                                                }
                                                                else
                                                                {                                                                    
                                                                    if (ssd.Month == duedatetemp.Month)
                                                                    {
                                                                        Duedate = new DateTime(ssd.Year, Convert.ToInt32(duedatetemp.Month), Convert.ToInt32(duedatetemp.Day));
                                                                    }
                                                                    else if (enddt.Month == duedatetemp.Month)
                                                                    {
                                                                        Duedate = new DateTime(enddt.Year, Convert.ToInt32(duedatetemp.Month), Convert.ToInt32(duedatetemp.Day));
                                                                    }
                                                                    else
                                                                    {
                                                                        DateTime StartDateDuedate = ssd;
                                                                        DateTime EndDateDuedate = enddt;

                                                                        while (StartDateDuedate <= EndDateDuedate)
                                                                        {
                                                                            if (StartDateDuedate.Month == duedatetemp.Month)
                                                                            {
                                                                                Duedate = new DateTime(StartDateDuedate.Year, Convert.ToInt32(duedatetemp.Month), Convert.ToInt32(duedatetemp.Day));
                                                                                break;
                                                                            }
                                                                            StartDateDuedate = StartDateDuedate.AddMonths(1);
                                                                        }
                                                                    }                                                                    
                                                                }
                                                                if (ContractEndDate <= enddt)
                                                                {
                                                                    string ToMonth = getdata.ToMonth;
                                                                    if (getdata.ToMonth.Trim() == "28 February")
                                                                    {
                                                                        string tempmonth = string.Format("{0:dd MMMM}", enddt.AddDays(1));
                                                                        if (tempmonth.Trim() == "29 February")
                                                                        {
                                                                            ToMonth = tempmonth;
                                                                            enddt = enddt.AddDays(1);
                                                                        }
                                                                    }

                                                                    updatedata.Add(new updateschedule
                                                                    {
                                                                        StartDate = ssd,
                                                                        EndDate = ContractEndDate,
                                                                        sequenceID = cnt,
                                                                        DueDate = Duedate,
                                                                        Forperiod = getdata.FromMonth + "-" + getdata.ToMonth
                                                                    });
                                                                    break;
                                                                }
                                                                else
                                                                {
                                                                    string ToMonth = getdata.ToMonth;
                                                                    if (getdata.ToMonth.Trim() == "28 February")
                                                                    {
                                                                        string tempmonth = string.Format("{0:dd MMMM}", enddt.AddDays(1));
                                                                        if (tempmonth.Trim() == "29 February")
                                                                        {
                                                                            ToMonth = tempmonth;
                                                                            enddt = enddt.AddDays(1);
                                                                        }
                                                                    }

                                                                    updatedata.Add(new updateschedule
                                                                    {
                                                                        StartDate = ssd,
                                                                        EndDate = enddt,
                                                                        sequenceID = cnt,
                                                                        DueDate = Duedate,
                                                                        Forperiod = getdata.FromMonth + "-" + getdata.ToMonth
                                                                    });
                                                                }
                                                                #endregion
                                                            }
                                                        }
                                                        frommonth = string.Format("{0:dd MMMM}", enddt.AddDays(1));                                                      
                                                        cnt++;
                                                    }
                                                    #endregion
                                                }
                                                if (updatedata.Count > 0)
                                                {
                                                    updatedata = updatedata.OrderBy(x => x.sequenceID).ToList();
                                                    Cont_tbl_MilestoneDetail addmasterobj = new Cont_tbl_MilestoneDetail
                                                    {
                                                        ContractTemplateID = newContractID,
                                                        CustomerID = customerID,
                                                        Title = txtboxMilestonetitle.Text,
                                                        Description = txtboxMilestonedescription.Text,
                                                        remark = txbRemark.Text,
                                                        DeptID = Convert.ToInt32(drpmilestoneDepartment.SelectedValue),
                                                        UserID = Convert.ToInt32(drpmilestoneDepartmentUser.SelectedValue),
                                                        StatusID = Convert.ToInt32(drpmilestonestatus.SelectedValue),
                                                        CreatedBy = userID,
                                                        CreatedOn = DateTime.Now,
                                                        IsActive = true,
                                                        MileStonePeriod = DRPMilestonePeriod.SelectedValue,
                                                        MileStoneType = RdoMilestoneType.SelectedValue,
                                                        StartDay = ContractStartDate,
                                                        EndDay = ContractEndDate,
                                                    };

                                                    if (!string.IsNullOrEmpty(txbmilestoneduedate.Text))
                                                        addmasterobj.DueDate = DateTimeExtensions.GetDate(txbmilestoneduedate.Text);

                                                    entities.Cont_tbl_MilestoneDetail.Add(addmasterobj);
                                                    entities.SaveChanges();

                                                    if (addmasterobj.ID > 0)
                                                    {
                                                        foreach (var item in updatedata)
                                                        {
                                                            SaveMilestoneScheduled(newContractID, customerID, addmasterobj.ID, txtboxMilestonetitle.Text, txtboxMilestonedescription.Text,
                                                                      Convert.ToInt32(drpmilestoneDepartment.SelectedValue), Convert.ToInt32(drpmilestoneDepartmentUser.SelectedValue),
                                                                      item.DueDate, Convert.ToInt32(drpmilestonestatus.SelectedValue),
                                                                      txbRemark.Text, userID, item.StartDate, item.EndDate, item.Forperiod);
                                                        }
                                                        foreach (var item in TempCustomSchedule)
                                                        {
                                                            MasterMileStoneSchedule MasterOfCustomSchedule = new MasterMileStoneSchedule()
                                                            {
                                                                ContractID = newContractID,
                                                                MileStoneID = addmasterobj.ID,
                                                                FromMonth = item.FromMonth,
                                                                ToMonth = item.ToMonth,
                                                                DueDate = item.DueDate,
                                                                IsActive = true,
                                                                CustID = item.CustID
                                                            };
                                                            entities.MasterMileStoneSchedules.Add(MasterOfCustomSchedule);
                                                            entities.SaveChanges();
                                                        }
                                                        ContractManagement.deleteMilestonedetail(TempCustomSchedule);
                                                        BindMilestonedrp(newContractID, customerID);
                                                        BindContractMilestone_Paging(customerID, newContractID, Convert.ToInt32(gridMilestone.PageSize), Convert.ToInt32(gridMilestone.PageIndex) + 1, gridMilestone);

                                                        HideShowMilestoneDiv(false);
                                                        clearTaskControls();
                                                        cvMilestoneTab.IsValid = false;
                                                        cvMilestoneTab.ErrorMessage = "Milestone Details Saved Successfully.";
                                                        vsMilestoneTab.CssClass = "alert alert-success";
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else if (RdoMilestoneType.SelectedValue == "2")//Custom
                                    {
                                        Cont_tbl_MilestoneDetail addmasterobj = new Cont_tbl_MilestoneDetail
                                        {
                                            ContractTemplateID = newContractID,
                                            CustomerID = customerID,
                                            Title = txtboxMilestonetitle.Text,
                                            Description = txtboxMilestonedescription.Text,
                                            remark = txbRemark.Text,
                                            DeptID = Convert.ToInt32(drpmilestoneDepartment.SelectedValue),
                                            UserID = Convert.ToInt32(drpmilestoneDepartmentUser.SelectedValue),
                                            StatusID = Convert.ToInt32(drpmilestonestatus.SelectedValue),
                                            CreatedBy = userID,
                                            CreatedOn = DateTime.Now,
                                            IsActive = true,
                                            MileStonePeriod = DRPMilestonePeriod.SelectedValue,
                                            MileStoneType = RdoMilestoneType.SelectedValue,
                                            StartDay = ContractStartDate,
                                            EndDay = ContractEndDate,
                                        };

                                        if (!string.IsNullOrEmpty(txbmilestoneduedate.Text))
                                            addmasterobj.DueDate = DateTimeExtensions.GetDate(txbmilestoneduedate.Text);

                                        entities.Cont_tbl_MilestoneDetail.Add(addmasterobj);
                                        entities.SaveChanges();
                                        if (addmasterobj.ID > 0)
                                        {
                                            SaveMilestoneScheduled(newContractID, customerID, addmasterobj.ID, txtboxMilestonetitle.Text, txtboxMilestonedescription.Text,
                                                      Convert.ToInt32(drpmilestoneDepartment.SelectedValue), Convert.ToInt32(drpmilestoneDepartmentUser.SelectedValue),
                                                      DateTimeExtensions.GetDate(txbmilestoneduedate.Text),
                                                      Convert.ToInt32(drpmilestonestatus.SelectedValue), txbRemark.Text, userID, ContractStartDate, ContractEndDate, "OneTime");

                                            BindContractMilestone_Paging(customerID, newContractID, Convert.ToInt32(gridMilestone.PageSize), Convert.ToInt32(gridMilestone.PageIndex) + 1, gridMilestone);

                                            HideShowMilestoneDiv(false);

                                            clearTaskControls();

                                            cvMilestoneTab.IsValid = false;
                                            cvMilestoneTab.ErrorMessage = "Milestone Details Saved Successfully.";
                                            vsMilestoneTab.CssClass = "alert alert-success";

                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    bool formValidateSuccess = false;
                    bool saveSuccess = false;

                    List<string> lstErrorMsg = new List<string>();

                    #region Data Validation
                    
                    if (drpmilestonestatus.SelectedValue == "" || drpmilestonestatus.SelectedValue == "-1")
                        lstErrorMsg.Add("Select Status");
                    else
                        formValidateSuccess = true;

                    if (String.IsNullOrEmpty(txbRemark.Text))
                        lstErrorMsg.Add("Provide Remark");
                    else
                        formValidateSuccess = true;

                    if (lstErrorMsg.Count > 0)
                    {
                        formValidateSuccess = false;
                        showErrorMessages(lstErrorMsg, cvMilestoneTab);
                    }
                    #endregion

                    if (formValidateSuccess)
                    {
                        int newContractID = -1;
                        if (ViewState["ContractInstanceID"] != null)
                        {
                            newContractID = Convert.ToInt32(ViewState["ContractInstanceID"]);
                        }

                        if (newContractID != -1)
                        {
                            int userID = -1;
                            userID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);

                            int customerID = -1;
                            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                            {
                                int MilestoneID = Convert.ToInt32(ViewState["MilestoneID"]);

                                var updateobj = (from row in entities.Cont_tbl_MilestoneInstance
                                                 where row.ContractTemplateID == newContractID
                                                 && row.ID == MilestoneID
                                                 && row.CustomerID == customerID
                                                 select row).FirstOrDefault();

                                if (updateobj != null)
                                {
                                    //updateobj.Title = txtboxMilestonetitle.Text;
                                    //updateobj.Description = txtboxMilestonedescription.Text;
                                    //updateobj.DeptID = Convert.ToInt32(drpmilestoneDepartment.SelectedValue);
                                    //updateobj.UserID = Convert.ToInt32(drpmilestoneDepartmentUser.SelectedValue);
                                    updateobj.Remark = txbRemark.Text;
                                    if (!string.IsNullOrEmpty(txbmilestoneduedate.Text))
                                        updateobj.DueDate = DateTimeExtensions.GetDate(txbmilestoneduedate.Text);

                                    updateobj.StatusID = Convert.ToInt32(drpmilestonestatus.SelectedValue);
                                    updateobj.UpdateBy = userID;
                                    updateobj.UpdatedOn = DateTime.Now;
                                    entities.SaveChanges();
                                }

                                Cont_tbl_MileStoneTransaction Transactionobj = new Cont_tbl_MileStoneTransaction
                                {
                                    ContractID = newContractID,
                                    //CustomerID = customerID,
                                    MileStoneID = MilestoneID,
                                    StatusID = Convert.ToInt32(drpmilestonestatus.SelectedValue),
                                    Remark = txbRemark.Text,
                                    IsActive = true,
                                    CreatedBy = userID,
                                    CreatedOn = DateTime.Now,
                                };
                                entities.Cont_tbl_MileStoneTransaction.Add(Transactionobj);
                                entities.SaveChanges();
                                ViewState["MilestonePageFlag"] = "0";
                                BindContractMilestone_Paging(customerID, newContractID, Convert.ToInt32(gridMilestone.PageSize), Convert.ToInt32(gridMilestone.PageIndex) + 1, gridMilestone);

                                HideShowMilestoneDiv(false);

                                clearTaskControls();

                                cvMilestoneTab.IsValid = false;
                                cvMilestoneTab.ErrorMessage = "Milestone Details Saved Successfully.";
                                vsMilestoneTab.CssClass = "alert alert-success";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


        //protected void btnCreateMilestone_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (ViewState["MilestoneMode"].ToString() == "0")
        //        {
        //            bool formValidateSuccess = false;
        //            bool saveSuccess = false;

        //            List<string> lstErrorMsg = new List<string>();

        //            #region Data Validation

        //            if (String.IsNullOrEmpty(txtboxMilestonetitle.Text))
        //                lstErrorMsg.Add("Provide Milestone Title");
        //            else
        //                formValidateSuccess = true;

        //            if (String.IsNullOrEmpty(txtboxMilestonedescription.Text))
        //                lstErrorMsg.Add("Provide Milestone Description");
        //            else
        //                formValidateSuccess = true;

        //            if (drpmilestoneDepartment.SelectedValue == "" || drpmilestoneDepartment.SelectedValue == "-1")
        //                lstErrorMsg.Add("Select Department");
        //            else
        //                formValidateSuccess = true;

        //            if (drpmilestoneDepartmentUser.SelectedValue == "" || drpmilestoneDepartmentUser.SelectedValue == "-1")
        //                lstErrorMsg.Add("Select User");
        //            else
        //                formValidateSuccess = true;

        //            if (RdoMilestoneType.SelectedValue != "-1")
        //            {
        //                if (RdoMilestoneType.SelectedValue == "2")
        //                {
        //                    if (String.IsNullOrEmpty(txbmilestoneduedate.Text))
        //                        lstErrorMsg.Add("Provide Due Date");
        //                    else
        //                        formValidateSuccess = true;
        //                }
        //                else if (RdoMilestoneType.SelectedValue == "1")
        //                {
        //                    if (DRPMilestonePeriod.SelectedValue == "-1")
        //                        lstErrorMsg.Add("Please Select Frequency");
        //                    else
        //                        formValidateSuccess = true;

        //                    if (String.IsNullOrEmpty(Tbxstartdate.Text))
        //                        lstErrorMsg.Add("Provide Start Date");
        //                    else
        //                        formValidateSuccess = true;

        //                    if (String.IsNullOrEmpty(Tbxenddate.Text))
        //                        lstErrorMsg.Add("Provide End Date");
        //                    else
        //                        formValidateSuccess = true;
        //                }
        //            }

        //            if (drpmilestonestatus.SelectedValue == "" || drpmilestonestatus.SelectedValue == "-1")
        //                lstErrorMsg.Add("Select Status");
        //            else
        //                formValidateSuccess = true;

        //            if (String.IsNullOrEmpty(txbRemark.Text))
        //                lstErrorMsg.Add("Provide Remark");
        //            else
        //                formValidateSuccess = true;

        //            if (lstErrorMsg.Count > 0)
        //            {
        //                formValidateSuccess = false;
        //                showErrorMessages(lstErrorMsg, cvMilestoneTab);
        //            }
        //            #endregion

        //            if (formValidateSuccess)
        //            {
        //                int newContractID = -1;
        //                if (ViewState["ContractInstanceID"] != null)
        //                {
        //                    newContractID = Convert.ToInt32(ViewState["ContractInstanceID"]);
        //                }

        //                if (newContractID != -1)
        //                {
        //                    int userID = -1;
        //                    userID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);

        //                    int customerID = -1;
        //                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

        //                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //                    {
        //                        if (ViewState["MilestoneMode"].ToString() == "0")
        //                        {
        //                            if (RdoMilestoneType.SelectedValue == "1")//recurring custom
        //                            {
        //                                DateTime ContractStartDate = Convert.ToDateTime(Tbxstartdate.Text);
        //                                DateTime ContractEndDate = Convert.ToDateTime(Tbxenddate.Text);

        //                                List<updateschedule> updatedata = new List<updateschedule>();

        //                                if (DRPMilestonePeriod.SelectedValue == "Q")//Quarterly
        //                                {
        //                                    long setsequenceID = -1;
        //                                    var data = LitigationManagement.getMilestonedetail(newContractID, DRPMilestonePeriod.SelectedValue);
        //                                    foreach (var item in data)
        //                                    {
        //                                        DateTime FrequentStartDate = DateTime.Parse(item.FromMonth);
        //                                        DateTime FrequentEndDate = DateTime.Parse(item.ToMonth);
        //                                        if (FrequentStartDate.Date <= ContractStartDate.Date && FrequentEndDate.Date >= ContractStartDate.Date)
        //                                        {                                                  
        //                                            setsequenceID = item.ID;                                                    
        //                                            break;
        //                                        }
        //                                    }
        //                                    if (data.Count > 0)
        //                                    {

        //                                        //DateTime ContractStartDate = Convert.ToDateTime(Tbxstartdate.Text);
        //                                        //DateTime ContractEndDate = Convert.ToDateTime(Tbxenddate.Text);
        //                                        int firstyear = ContractStartDate.Year;
        //                                        int lastyear = ContractEndDate.Year;
        //                                        List<int> yearlist = new List<int>();

        //                                        if (firstyear <= lastyear)
        //                                        {
        //                                            yearlist.Add(firstyear);
        //                                            firstyear = firstyear + 1;
        //                                        }

        //                                        int cnt = 1;
        //                                        foreach (var item in yearlist)
        //                                        {
        //                                            data = data.OrderBy(x => x.SequenceID).ToList();
        //                                            foreach (var item1 in data)
        //                                            {
        //                                                DateTime temp = DateTime.Parse(item1.ToMonth);
        //                                                DateTime Cont_sd = new DateTime(item, Convert.ToInt32(temp.Month), Convert.ToInt32(temp.Day));
        //                                                DateTime Cont_ED = new DateTime(item, Convert.ToInt32(temp.Month), Convert.ToInt32(temp.Day));
        //                                                updatedata.Add(new updateschedule
        //                                                {
        //                                                    StartDate = Cont_sd,
        //                                                    EndDate = Cont_ED,
        //                                                    sequenceID = cnt
        //                                                });
        //                                                cnt++;
        //                                            }
        //                                        }


        //                                        //var detail = data.Where(x => x.ID == setsequenceID).FirstOrDefault();

        //                                        //DateTime Cont_sd = ContractStartDate;
        //                                        //DateTime temp = DateTime.Parse(detail.ToMonth);
        //                                        //DateTime Cont_ED = new DateTime(Cont_sd.Year, Convert.ToInt32(temp.Month), Convert.ToInt32(temp.Day));

        //                                        //int cnt = 1;
        //                                        //while (Cont_ED <= ContractEndDate)
        //                                        //{
        //                                        //    if (cnt == 1)
        //                                        //    {
        //                                        //        updatedata.Add(new updateschedule
        //                                        //        {
        //                                        //            StartDate = Cont_sd,
        //                                        //            EndDate = Cont_ED,
        //                                        //            sequenceID = cnt
        //                                        //        });
        //                                        //    }
        //                                        //    else
        //                                        //    {
        //                                        //        Cont_sd = Cont_ED.AddDays(1);
        //                                        //        if (Cont_sd.Month + 3 <= 12)
        //                                        //        {
        //                                        //            Cont_ED = new DateTime(Cont_sd.Year, Convert.ToInt32(Cont_sd.Month + 3), Convert.ToInt32(Cont_sd.Day));
        //                                        //            Cont_ED = Cont_ED.AddDays(-1);
        //                                        //        }
        //                                        //        else
        //                                        //        {
        //                                        //            int setmonth = (Cont_sd.Month + 3) - 12;
        //                                        //            Cont_ED = new DateTime(Cont_sd.Year + 1, Convert.ToInt32(setmonth), Convert.ToInt32(Cont_sd.Day));
        //                                        //            Cont_ED = Cont_ED.AddDays(-1);
        //                                        //        }

        //                                        //        updatedata.Add(new updateschedule
        //                                        //        {
        //                                        //            StartDate = Cont_sd,
        //                                        //            EndDate = Cont_ED,
        //                                        //            sequenceID = cnt
        //                                        //        });
        //                                        //    }
        //                                        //}


















        //                                        var detail = data.Where(x => x.ID == setsequenceID).FirstOrDefault();
        //                                        DateTime sd = ContractStartDate;
        //                                        DateTime Ed = DateTime.Parse(detail.ToMonth);
        //                                        DateTime duedate = detail.DueDate;
        //                                        int j = 1;
        //                                        while (Ed <= ContractEndDate)
        //                                        {
        //                                            if (j == 1)
        //                                            {
        //                                                updatedata.Add(new updateschedule
        //                                                {
        //                                                    StartDate = sd,
        //                                                    EndDate = Ed,
        //                                                    DueDate = duedate,
        //                                                    sequenceID = j
        //                                                });
        //                                            }
        //                                            else
        //                                            {
        //                                                string fromdata = string.Format("{0:dd MMMM}", Ed);
        //                                                var getdata= data.Where(x => x.FromMonth.Trim() == fromdata.Trim()).FirstOrDefault();                                                        
        //                                                if (getdata != null)
        //                                                {
        //                                                    updatedata.Add(new updateschedule
        //                                                    {
        //                                                        StartDate = DateTime.Parse(getdata.FromMonth),
        //                                                        EndDate = DateTime.Parse(getdata.ToMonth),
        //                                                        DueDate = getdata.DueDate,
        //                                                        sequenceID = j
        //                                                    });
        //                                                    Ed = DateTime.Parse(getdata.ToMonth);
        //                                                }
        //                                            }
        //                                            j++;
        //                                        }
        //                                    }
        //                                }


        //                                DateTime startdate = Convert.ToDateTime(Tbxstartdate.Text);
        //                                DateTime enddate = Convert.ToDateTime(Tbxenddate.Text);

        //                                Cont_tbl_MilestoneDetail addmasterobj = new Cont_tbl_MilestoneDetail
        //                                {
        //                                    ContractTemplateID = newContractID,
        //                                    CustomerID = customerID,
        //                                    Title = txtboxMilestonetitle.Text,
        //                                    Description = txtboxMilestonedescription.Text,
        //                                    remark = txbRemark.Text,
        //                                    DeptID = Convert.ToInt32(drpmilestoneDepartment.SelectedValue),
        //                                    UserID = Convert.ToInt32(drpmilestoneDepartmentUser.SelectedValue),
        //                                    StatusID = Convert.ToInt32(drpmilestonestatus.SelectedValue),
        //                                    CreatedBy = userID,
        //                                    CreatedOn = DateTime.Now,
        //                                    IsActive = true,
        //                                    StartDay = startdate,
        //                                    EndDay = enddate,
        //                                    MileStonePeriod = DRPMilestonePeriod.SelectedValue,
        //                                    MileStoneType = RdoMilestoneType.SelectedValue
        //                                };

        //                                //if (!string.IsNullOrEmpty(txbmilestoneduedate.Text))
        //                                //    addmasterobj.DueDate = DateTimeExtensions.GetDate(txbmilestoneduedate.Text);

        //                                entities.Cont_tbl_MilestoneDetail.Add(addmasterobj);
        //                                entities.SaveChanges();

        //                                if (addmasterobj.ID > 0)
        //                                {
        //                                    if (DRPMilestonePeriod.SelectedValue == "M")//monthly
        //                                    {
        //                                        #region Monthly
        //                                        DateTime nextDate = DateTime.Now;
        //                                        if (startdate.Month == 12)
        //                                        {
        //                                            nextDate = new DateTime(startdate.Year + 1, Convert.ToInt32(1), Convert.ToInt32(startdate.Day));
        //                                        }
        //                                        else
        //                                        {
        //                                            nextDate = new DateTime(startdate.Year, Convert.ToInt32(startdate.Month + 1), Convert.ToInt32(startdate.Day));
        //                                        }
        //                                        while (nextDate <= enddate)
        //                                        {
        //                                            SaveMilestoneScheduled(newContractID, customerID, addmasterobj.ID, txtboxMilestonetitle.Text, txtboxMilestonedescription.Text,
        //                                               Convert.ToInt32(drpmilestoneDepartment.SelectedValue), Convert.ToInt32(drpmilestoneDepartmentUser.SelectedValue),
        //                                               nextDate, Convert.ToInt32(drpmilestonestatus.SelectedValue), txbRemark.Text, userID);

        //                                            if (nextDate.Month == 12)
        //                                            {
        //                                                nextDate = new DateTime(nextDate.Year + 1, Convert.ToInt32(1), Convert.ToInt32(nextDate.Day));
        //                                            }
        //                                            else
        //                                            {
        //                                                nextDate = new DateTime(nextDate.Year, Convert.ToInt32(nextDate.Month + 1), Convert.ToInt32(nextDate.Day));
        //                                            }
        //                                        }
        //                                        #endregion
        //                                    }
        //                                    else if (DRPMilestonePeriod.SelectedValue == "Q")//Quarterly
        //                                    {
        //                                        #region Quarterly
        //                                        DateTime nextDate = DateTime.Now;
        //                                        if (startdate.Month == 12)
        //                                        {
        //                                            nextDate = new DateTime(startdate.Year + 1, Convert.ToInt32(3), Convert.ToInt32(startdate.Day));
        //                                        }
        //                                        else
        //                                        {
        //                                            if (startdate.Month + 3 <= 12)
        //                                            {
        //                                                nextDate = new DateTime(startdate.Year, Convert.ToInt32(startdate.Month + 3), Convert.ToInt32(startdate.Day));
        //                                            }
        //                                            else
        //                                            {
        //                                                int setmonth = (startdate.Month + 3) - 12;
        //                                                nextDate = new DateTime(startdate.Year + 1, Convert.ToInt32(setmonth), Convert.ToInt32(startdate.Day));
        //                                            }
        //                                        }
        //                                        while (nextDate <= enddate)
        //                                        {
        //                                            SaveMilestoneScheduled(newContractID, customerID, addmasterobj.ID, txtboxMilestonetitle.Text, txtboxMilestonedescription.Text,
        //                                               Convert.ToInt32(drpmilestoneDepartment.SelectedValue), Convert.ToInt32(drpmilestoneDepartmentUser.SelectedValue),
        //                                               nextDate, Convert.ToInt32(drpmilestonestatus.SelectedValue), txbRemark.Text, userID);

        //                                            if (nextDate.Month == 12)
        //                                            {
        //                                                nextDate = new DateTime(nextDate.Year + 1, Convert.ToInt32(3), Convert.ToInt32(nextDate.Day));
        //                                            }
        //                                            else
        //                                            {
        //                                                if (nextDate.Month + 3 <= 12)
        //                                                {
        //                                                    nextDate = new DateTime(nextDate.Year, Convert.ToInt32(nextDate.Month + 3), Convert.ToInt32(nextDate.Day));
        //                                                }
        //                                                else
        //                                                {
        //                                                    int setmonth = (nextDate.Month + 3) - 12;
        //                                                    nextDate = new DateTime(nextDate.Year + 1, Convert.ToInt32(setmonth), Convert.ToInt32(nextDate.Day));
        //                                                }
        //                                            }
        //                                        }
        //                                        #endregion
        //                                    }
        //                                    else if (DRPMilestonePeriod.SelectedValue == "H")//Half-Yearly
        //                                    {
        //                                        #region Half-Yearly
        //                                        DateTime nextDate = DateTime.Now;
        //                                        if (startdate.Month == 12)
        //                                        {
        //                                            nextDate = new DateTime(startdate.Year + 1, Convert.ToInt32(6), Convert.ToInt32(startdate.Day));
        //                                        }
        //                                        else
        //                                        {
        //                                            if (startdate.Month + 6 <= 12)
        //                                            {
        //                                                nextDate = new DateTime(startdate.Year, Convert.ToInt32(startdate.Month + 6), Convert.ToInt32(startdate.Day));
        //                                            }
        //                                            else
        //                                            {
        //                                                int setmonth = (startdate.Month + 6) - 12;
        //                                                nextDate = new DateTime(startdate.Year + 1, Convert.ToInt32(setmonth), Convert.ToInt32(startdate.Day));
        //                                            }
        //                                        }
        //                                        while (nextDate <= enddate)
        //                                        {
        //                                            SaveMilestoneScheduled(newContractID, customerID, addmasterobj.ID, txtboxMilestonetitle.Text, txtboxMilestonedescription.Text,
        //                                               Convert.ToInt32(drpmilestoneDepartment.SelectedValue), Convert.ToInt32(drpmilestoneDepartmentUser.SelectedValue),
        //                                               nextDate, Convert.ToInt32(drpmilestonestatus.SelectedValue), txbRemark.Text, userID);

        //                                            if (nextDate.Month == 12)
        //                                            {
        //                                                nextDate = new DateTime(nextDate.Year + 1, Convert.ToInt32(6), Convert.ToInt32(nextDate.Day));
        //                                            }
        //                                            else
        //                                            {
        //                                                if (nextDate.Month + 6 <= 12)
        //                                                {
        //                                                    nextDate = new DateTime(nextDate.Year, Convert.ToInt32(nextDate.Month + 6), Convert.ToInt32(nextDate.Day));
        //                                                }
        //                                                else
        //                                                {
        //                                                    int setmonth = (nextDate.Month + 6) - 12;
        //                                                    nextDate = new DateTime(nextDate.Year + 1, Convert.ToInt32(setmonth), Convert.ToInt32(nextDate.Day));
        //                                                }
        //                                            }
        //                                        }
        //                                        #endregion
        //                                    }
        //                                    else if (DRPMilestonePeriod.SelectedValue == "Y")//Yearly
        //                                    {
        //                                        #region Yearly
        //                                        DateTime nextDate = DateTime.Now;
        //                                        if (startdate.Month == 12)
        //                                        {
        //                                            nextDate = new DateTime(startdate.Year + 1, Convert.ToInt32(12), Convert.ToInt32(startdate.Day));
        //                                        }
        //                                        else
        //                                        {
        //                                            if (startdate.Month + 12 <= 12)
        //                                            {
        //                                                nextDate = new DateTime(startdate.Year, Convert.ToInt32(startdate.Month + 12), Convert.ToInt32(startdate.Day));
        //                                            }
        //                                            else
        //                                            {
        //                                                int setmonth = (startdate.Month + 12) - 12;
        //                                                nextDate = new DateTime(startdate.Year + 1, Convert.ToInt32(setmonth), Convert.ToInt32(startdate.Day));
        //                                            }
        //                                        }
        //                                        while (nextDate <= enddate)
        //                                        {
        //                                            SaveMilestoneScheduled(newContractID, customerID, addmasterobj.ID, txtboxMilestonetitle.Text, txtboxMilestonedescription.Text,
        //                                               Convert.ToInt32(drpmilestoneDepartment.SelectedValue), Convert.ToInt32(drpmilestoneDepartmentUser.SelectedValue),
        //                                               nextDate, Convert.ToInt32(drpmilestonestatus.SelectedValue), txbRemark.Text, userID);

        //                                            if (nextDate.Month == 12)
        //                                            {
        //                                                nextDate = new DateTime(nextDate.Year + 1, Convert.ToInt32(12), Convert.ToInt32(nextDate.Day));
        //                                            }
        //                                            else
        //                                            {
        //                                                if (nextDate.Month + 12 <= 12)
        //                                                {
        //                                                    nextDate = new DateTime(nextDate.Year, Convert.ToInt32(nextDate.Month + 12), Convert.ToInt32(nextDate.Day));
        //                                                }
        //                                                else
        //                                                {
        //                                                    int setmonth = (nextDate.Month + 12) - 12;
        //                                                    nextDate = new DateTime(nextDate.Year + 1, Convert.ToInt32(setmonth), Convert.ToInt32(nextDate.Day));
        //                                                }
        //                                            }
        //                                        }
        //                                        #endregion
        //                                    }
        //                                    BindContractMilestone_Paging(customerID, newContractID, Convert.ToInt32(gridMilestone.PageSize), Convert.ToInt32(gridMilestone.PageIndex) + 1, gridMilestone);

        //                                    HideShowMilestoneDiv(false);

        //                                    clearTaskControls();

        //                                    cvMilestoneTab.IsValid = false;
        //                                    cvMilestoneTab.ErrorMessage = "Milestone Detail Save Successfully.";
        //                                    vsMilestoneTab.CssClass = "alert alert-success";
        //                                }
        //                            }
        //                            else if (RdoMilestoneType.SelectedValue == "2")//Custom
        //                            {
        //                                Cont_tbl_MilestoneDetail addmasterobj = new Cont_tbl_MilestoneDetail
        //                                {
        //                                    ContractTemplateID = newContractID,
        //                                    CustomerID = customerID,
        //                                    Title = txtboxMilestonetitle.Text,
        //                                    Description = txtboxMilestonedescription.Text,
        //                                    remark = txbRemark.Text,
        //                                    DeptID = Convert.ToInt32(drpmilestoneDepartment.SelectedValue),
        //                                    UserID = Convert.ToInt32(drpmilestoneDepartmentUser.SelectedValue),
        //                                    StatusID = Convert.ToInt32(drpmilestonestatus.SelectedValue),
        //                                    CreatedBy = userID,
        //                                    CreatedOn = DateTime.Now,
        //                                    IsActive = true,
        //                                    MileStonePeriod = DRPMilestonePeriod.SelectedValue,
        //                                    MileStoneType = RdoMilestoneType.SelectedValue
        //                                };

        //                                if (!string.IsNullOrEmpty(txbmilestoneduedate.Text))
        //                                    addmasterobj.DueDate = DateTimeExtensions.GetDate(txbmilestoneduedate.Text);

        //                                entities.Cont_tbl_MilestoneDetail.Add(addmasterobj);
        //                                entities.SaveChanges();
        //                                if (addmasterobj.ID > 0)
        //                                {
        //                                    SaveMilestoneScheduled(newContractID, customerID, addmasterobj.ID, txtboxMilestonetitle.Text, txtboxMilestonedescription.Text,
        //                                              Convert.ToInt32(drpmilestoneDepartment.SelectedValue), Convert.ToInt32(drpmilestoneDepartmentUser.SelectedValue),
        //                                              DateTimeExtensions.GetDate(txbmilestoneduedate.Text), Convert.ToInt32(drpmilestonestatus.SelectedValue), txbRemark.Text, userID);

        //                                    BindContractMilestone_Paging(customerID, newContractID, Convert.ToInt32(gridMilestone.PageSize), Convert.ToInt32(gridMilestone.PageIndex) + 1, gridMilestone);

        //                                    HideShowMilestoneDiv(false);

        //                                    clearTaskControls();

        //                                    cvMilestoneTab.IsValid = false;
        //                                    cvMilestoneTab.ErrorMessage = "Milestone Detail Save Successfully.";
        //                                    vsMilestoneTab.CssClass = "alert alert-success";

        //                                }
        //                            }
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //        else
        //        {
        //            bool formValidateSuccess = false;
        //            bool saveSuccess = false;

        //            List<string> lstErrorMsg = new List<string>();

        //            #region Data Validation

        //            if (String.IsNullOrEmpty(txtboxMilestonetitle.Text))
        //                lstErrorMsg.Add("Provide Milestone Title");
        //            else
        //                formValidateSuccess = true;

        //            if (String.IsNullOrEmpty(txtboxMilestonedescription.Text))
        //                lstErrorMsg.Add("Provide Milestone Description");
        //            else
        //                formValidateSuccess = true;

        //            if (drpmilestoneDepartment.SelectedValue == "" || drpmilestoneDepartment.SelectedValue == "-1")
        //                lstErrorMsg.Add("Select Department");
        //            else
        //                formValidateSuccess = true;

        //            if (drpmilestoneDepartmentUser.SelectedValue == "" || drpmilestoneDepartmentUser.SelectedValue == "-1")
        //                lstErrorMsg.Add("Select User");
        //            else
        //                formValidateSuccess = true;

        //            if (String.IsNullOrEmpty(txbmilestoneduedate.Text))
        //                lstErrorMsg.Add("Provide Due Date");
        //            else
        //                formValidateSuccess = true;

        //            if (drpmilestonestatus.SelectedValue == "" || drpmilestonestatus.SelectedValue == "-1")
        //                lstErrorMsg.Add("Select Status");
        //            else
        //                formValidateSuccess = true;

        //            if (String.IsNullOrEmpty(txbRemark.Text))
        //                lstErrorMsg.Add("Provide Remark");
        //            else
        //                formValidateSuccess = true;

        //            if (lstErrorMsg.Count > 0)
        //            {
        //                formValidateSuccess = false;
        //                showErrorMessages(lstErrorMsg, cvMilestoneTab);
        //            }
        //            #endregion

        //            if (formValidateSuccess)
        //            {
        //                int newContractID = -1;
        //                if (ViewState["ContractInstanceID"] != null)
        //                {
        //                    newContractID = Convert.ToInt32(ViewState["ContractInstanceID"]);
        //                }

        //                if (newContractID != -1)
        //                {
        //                    int userID = -1;
        //                    userID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);

        //                    int customerID = -1;
        //                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

        //                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //                    {
        //                        int MilestoneID = Convert.ToInt32(ViewState["MilestoneID"]);

        //                        var updateobj = (from row in entities.Cont_tbl_MilestoneInstance
        //                                         where row.ContractTemplateID == newContractID
        //                                         && row.ID == MilestoneID
        //                                         && row.CustomerID == customerID
        //                                         select row).FirstOrDefault();

        //                        if (updateobj != null)
        //                        {
        //                            updateobj.Title = txtboxMilestonetitle.Text;
        //                            updateobj.Description = txtboxMilestonedescription.Text;
        //                            updateobj.DeptID = Convert.ToInt32(drpmilestoneDepartment.SelectedValue);
        //                            updateobj.UserID = Convert.ToInt32(drpmilestoneDepartmentUser.SelectedValue);
        //                            updateobj.Remark = txbRemark.Text;
        //                            if (!string.IsNullOrEmpty(txbmilestoneduedate.Text))
        //                                updateobj.DueDate = DateTimeExtensions.GetDate(txbmilestoneduedate.Text);

        //                            updateobj.StatusID = Convert.ToInt32(drpmilestonestatus.SelectedValue);
        //                            updateobj.UpdateBy = userID;
        //                            updateobj.UpdatedOn = DateTime.Now;
        //                            entities.SaveChanges();
        //                        }

        //                        Cont_tbl_MileStoneTransaction Transactionobj = new Cont_tbl_MileStoneTransaction
        //                        {
        //                            ContractID = newContractID,
        //                            //CustomerID = customerID,
        //                            MileStoneID = MilestoneID,
        //                            StatusID = Convert.ToInt32(drpmilestonestatus.SelectedValue),
        //                            Remark = txbRemark.Text,
        //                            IsActive = true,
        //                            CreatedBy = userID,
        //                            CreatedOn = DateTime.Now,
        //                        };
        //                        entities.Cont_tbl_MileStoneTransaction.Add(Transactionobj);
        //                        entities.SaveChanges();

        //                        BindContractMilestone_Paging(customerID, newContractID, Convert.ToInt32(gridMilestone.PageSize), Convert.ToInt32(gridMilestone.PageIndex) + 1, gridMilestone);

        //                        HideShowMilestoneDiv(false);

        //                        clearTaskControls();

        //                        cvMilestoneTab.IsValid = false;
        //                        cvMilestoneTab.ErrorMessage = "Milestone Detail Save Successfully.";
        //                        vsMilestoneTab.CssClass = "alert alert-success";
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //}


        //protected void btnCreateMilestone_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        bool formValidateSuccess = false;
        //        bool saveSuccess = false;

        //        List<string> lstErrorMsg = new List<string>();

        //        #region Data Validation

        //        if (String.IsNullOrEmpty(txtboxMilestonetitle.Text))
        //            lstErrorMsg.Add("Provide Milestone Title");
        //        else
        //            formValidateSuccess = true;

        //        if (String.IsNullOrEmpty(txtboxMilestonedescription.Text))
        //            lstErrorMsg.Add("Provide Milestone Description");
        //        else
        //            formValidateSuccess = true;

        //        if (drpmilestoneDepartment.SelectedValue == "" || drpmilestoneDepartment.SelectedValue == "-1")
        //            lstErrorMsg.Add("Select Department");
        //        else
        //            formValidateSuccess = true;

        //        if (drpmilestoneDepartmentUser.SelectedValue == "" || drpmilestoneDepartmentUser.SelectedValue == "-1")
        //            lstErrorMsg.Add("Select User");
        //        else
        //            formValidateSuccess = true;

        //        if (String.IsNullOrEmpty(txbmilestoneduedate.Text))
        //            lstErrorMsg.Add("Provide Due Date");
        //        else
        //            formValidateSuccess = true;

        //        if (drpmilestonestatus.SelectedValue == "" || drpmilestonestatus.SelectedValue == "-1")
        //            lstErrorMsg.Add("Select Status");
        //        else
        //            formValidateSuccess = true;

        //        if (String.IsNullOrEmpty(txbRemark.Text))
        //            lstErrorMsg.Add("Provide Remark");
        //        else
        //            formValidateSuccess = true;

        //        if (lstErrorMsg.Count > 0)
        //        {
        //            formValidateSuccess = false;
        //            showErrorMessages(lstErrorMsg, cvMilestoneTab);
        //        }
        //        #endregion

        //        if (formValidateSuccess)
        //        {
        //            int newContractID = -1;
        //            if (ViewState["ContractInstanceID"] != null)
        //            {
        //                newContractID = Convert.ToInt32(ViewState["ContractInstanceID"]);
        //            }

        //            if (newContractID != -1)
        //            {
        //                int userID = -1;
        //                userID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);

        //                int customerID = -1;
        //                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

        //                using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //                {
        //                    if (ViewState["MilestoneMode"].ToString() == "0")
        //                    {
        //                        if (txtEffectiveDate.Text != ""
        //                            && txtExpirationDate.Text != "")
        //                        {
        //                            if (RdoMilestoneType.SelectedValue == "1")//custom
        //                            {
        //                                DateTime startdate = Convert.ToDateTime(Tbxstartdate.Text);
        //                                DateTime enddate = Convert.ToDateTime(Tbxenddate.Text);

        //                                Cont_tbl_MilestoneDetail addmasterobj = new Cont_tbl_MilestoneDetail
        //                                {
        //                                    ContractTemplateID = newContractID,
        //                                    CustomerID = customerID,
        //                                    Title = txtboxMilestonetitle.Text,
        //                                    Description = txtboxMilestonedescription.Text,
        //                                    remark = txbRemark.Text,
        //                                    DeptID = Convert.ToInt32(drpmilestoneDepartment.SelectedValue),
        //                                    UserID = Convert.ToInt32(drpmilestoneDepartmentUser.SelectedValue),
        //                                    StatusID = Convert.ToInt32(drpmilestonestatus.SelectedValue),
        //                                    CreatedBy = userID,
        //                                    CreatedOn = DateTime.Now,
        //                                    IsActive = true,
        //                                    StartDay = startdate,
        //                                    EndDay = enddate,
        //                                    MileStonePeriod = DRPMilestonePeriod.SelectedValue,
        //                                    MileStoneType = RdoMilestoneType.SelectedValue
        //                                };

        //                                if (!string.IsNullOrEmpty(txbmilestoneduedate.Text))
        //                                    addmasterobj.DueDate = DateTimeExtensions.GetDate(txbmilestoneduedate.Text);

        //                                entities.Cont_tbl_MilestoneDetail.Add(addmasterobj);
        //                                entities.SaveChanges();

        //                                if (addmasterobj.ID > 0)
        //                                {
        //                                    if (DRPMilestonePeriod.SelectedValue == "M")//monthly
        //                                    {
        //                                        #region Monthly
        //                                        DateTime nextDate = DateTime.Now;
        //                                        if (startdate.Month == 12)
        //                                        {
        //                                            nextDate = new DateTime(startdate.Year + 1, Convert.ToInt32(1), Convert.ToInt32(startdate.Day));
        //                                        }
        //                                        else
        //                                        {
        //                                            nextDate = new DateTime(startdate.Year, Convert.ToInt32(startdate.Month + 1), Convert.ToInt32(startdate.Day));
        //                                        } 
        //                                        while (nextDate <= enddate)
        //                                        {
        //                                            SaveMilestoneScheduled(newContractID, customerID, addmasterobj.ID, txtboxMilestonetitle.Text, txtboxMilestonetitle.Text,
        //                                               Convert.ToInt32(drpmilestoneDepartment.SelectedValue), Convert.ToInt32(drpmilestoneDepartmentUser.SelectedValue),
        //                                               nextDate, Convert.ToInt32(drpmilestonestatus.SelectedValue), txbRemark.Text, userID);

        //                                            if (nextDate.Month == 12)
        //                                            {
        //                                                nextDate = new DateTime(nextDate.Year + 1, Convert.ToInt32(1), Convert.ToInt32(nextDate.Day));
        //                                            }
        //                                            else
        //                                            {
        //                                                nextDate = new DateTime(nextDate.Year, Convert.ToInt32(nextDate.Month + 1), Convert.ToInt32(nextDate.Day));
        //                                            }
        //                                        }
        //                                        #endregion
        //                                    }
        //                                    else if (DRPMilestonePeriod.SelectedValue == "Q")//Quarterly
        //                                    {
        //                                        #region Quarterly
        //                                        DateTime nextDate = DateTime.Now;
        //                                        if (startdate.Month == 12)
        //                                        {
        //                                            nextDate = new DateTime(startdate.Year + 1, Convert.ToInt32(3), Convert.ToInt32(startdate.Day));
        //                                        }
        //                                        else
        //                                        {
        //                                            if (startdate.Month + 3 <= 12)
        //                                            {
        //                                                nextDate = new DateTime(startdate.Year, Convert.ToInt32(startdate.Month + 3), Convert.ToInt32(startdate.Day));
        //                                            }
        //                                            else
        //                                            {
        //                                                int setmonth = (startdate.Month + 3) - 12;
        //                                                nextDate = new DateTime(startdate.Year + 1, Convert.ToInt32(setmonth), Convert.ToInt32(startdate.Day));
        //                                            }                                                    
        //                                        }
        //                                        while (nextDate <= enddate)
        //                                        {
        //                                            SaveMilestoneScheduled(newContractID, customerID, addmasterobj.ID, txtboxMilestonetitle.Text, txtboxMilestonetitle.Text,
        //                                               Convert.ToInt32(drpmilestoneDepartment.SelectedValue), Convert.ToInt32(drpmilestoneDepartmentUser.SelectedValue),
        //                                               nextDate, Convert.ToInt32(drpmilestonestatus.SelectedValue), txbRemark.Text, userID);

        //                                            if (nextDate.Month == 12)
        //                                            {
        //                                                nextDate = new DateTime(nextDate.Year + 1, Convert.ToInt32(3), Convert.ToInt32(nextDate.Day));
        //                                            }
        //                                            else
        //                                            {
        //                                                if (nextDate.Month + 3 <= 12)
        //                                                {
        //                                                    nextDate = new DateTime(nextDate.Year, Convert.ToInt32(nextDate.Month + 3), Convert.ToInt32(nextDate.Day));
        //                                                }
        //                                                else
        //                                                {
        //                                                    int setmonth = (nextDate.Month + 3) - 12;
        //                                                    nextDate = new DateTime(nextDate.Year + 1, Convert.ToInt32(setmonth), Convert.ToInt32(nextDate.Day));
        //                                                }
        //                                            }
        //                                        }
        //                                        #endregion
        //                                    }
        //                                    else if (DRPMilestonePeriod.SelectedValue == "H")//Half-Yearly
        //                                    {
        //                                        #region Half-Yearly
        //                                        DateTime nextDate = DateTime.Now;
        //                                        if (startdate.Month == 12)
        //                                        {
        //                                            nextDate = new DateTime(startdate.Year + 1, Convert.ToInt32(6), Convert.ToInt32(startdate.Day));
        //                                        }
        //                                        else
        //                                        {
        //                                            if (startdate.Month + 6 <= 12)
        //                                            {
        //                                                nextDate = new DateTime(startdate.Year, Convert.ToInt32(startdate.Month + 6), Convert.ToInt32(startdate.Day));
        //                                            }
        //                                            else
        //                                            {
        //                                                int setmonth = (startdate.Month + 6) - 12;
        //                                                nextDate = new DateTime(startdate.Year + 1, Convert.ToInt32(setmonth), Convert.ToInt32(startdate.Day));
        //                                            }
        //                                        }
        //                                        while (nextDate <= enddate)
        //                                        {
        //                                            SaveMilestoneScheduled(newContractID, customerID, addmasterobj.ID, txtboxMilestonetitle.Text, txtboxMilestonetitle.Text,
        //                                               Convert.ToInt32(drpmilestoneDepartment.SelectedValue), Convert.ToInt32(drpmilestoneDepartmentUser.SelectedValue),
        //                                               nextDate, Convert.ToInt32(drpmilestonestatus.SelectedValue), txbRemark.Text, userID);

        //                                            if (nextDate.Month == 12)
        //                                            {
        //                                                nextDate = new DateTime(nextDate.Year + 1, Convert.ToInt32(6), Convert.ToInt32(nextDate.Day));
        //                                            }
        //                                            else
        //                                            {
        //                                                if (nextDate.Month + 6 <= 12)
        //                                                {
        //                                                    nextDate = new DateTime(nextDate.Year, Convert.ToInt32(nextDate.Month + 6), Convert.ToInt32(nextDate.Day));
        //                                                }
        //                                                else
        //                                                {
        //                                                    int setmonth = (nextDate.Month + 6) - 12;
        //                                                    nextDate = new DateTime(nextDate.Year + 1, Convert.ToInt32(setmonth), Convert.ToInt32(nextDate.Day));
        //                                                }
        //                                            }
        //                                        }
        //                                        #endregion
        //                                    }
        //                                    else if (DRPMilestonePeriod.SelectedValue == "Y")//Yearly
        //                                    {
        //                                        #region Yearly
        //                                        DateTime nextDate = DateTime.Now;
        //                                        if (startdate.Month == 12)
        //                                        {
        //                                            nextDate = new DateTime(startdate.Year + 1, Convert.ToInt32(12), Convert.ToInt32(startdate.Day));
        //                                        }
        //                                        else
        //                                        {
        //                                            if (startdate.Month + 12 <= 12)
        //                                            {
        //                                                nextDate = new DateTime(startdate.Year, Convert.ToInt32(startdate.Month + 12), Convert.ToInt32(startdate.Day));
        //                                            }
        //                                            else
        //                                            {
        //                                                int setmonth = (startdate.Month + 12) - 12;
        //                                                nextDate = new DateTime(startdate.Year + 1, Convert.ToInt32(setmonth), Convert.ToInt32(startdate.Day));
        //                                            }
        //                                        }
        //                                        while (nextDate <= enddate)
        //                                        {
        //                                            SaveMilestoneScheduled(newContractID, customerID, addmasterobj.ID, txtboxMilestonetitle.Text, txtboxMilestonetitle.Text,
        //                                               Convert.ToInt32(drpmilestoneDepartment.SelectedValue), Convert.ToInt32(drpmilestoneDepartmentUser.SelectedValue),
        //                                               nextDate, Convert.ToInt32(drpmilestonestatus.SelectedValue), txbRemark.Text, userID);

        //                                            if (nextDate.Month == 12)
        //                                            {
        //                                                nextDate = new DateTime(nextDate.Year + 1, Convert.ToInt32(12), Convert.ToInt32(nextDate.Day));
        //                                            }
        //                                            else
        //                                            {
        //                                                if (nextDate.Month + 12 <= 12)
        //                                                {
        //                                                    nextDate = new DateTime(nextDate.Year, Convert.ToInt32(nextDate.Month + 12), Convert.ToInt32(nextDate.Day));
        //                                                }
        //                                                else
        //                                                {
        //                                                    int setmonth = (nextDate.Month + 12) - 12;
        //                                                    nextDate = new DateTime(nextDate.Year + 1, Convert.ToInt32(setmonth), Convert.ToInt32(nextDate.Day));
        //                                                }
        //                                            }
        //                                        }
        //                                        #endregion
        //                                    }
        //                                    BindContractMilestone_Paging(customerID, newContractID, Convert.ToInt32(gridMilestone.PageSize), Convert.ToInt32(gridMilestone.PageIndex) + 1, gridMilestone);

        //                                    HideShowMilestoneDiv(false);

        //                                    clearTaskControls();

        //                                    cvMilestoneTab.IsValid = false;
        //                                    cvMilestoneTab.ErrorMessage = "Milestone Detail Save Successfully.";
        //                                    vsMilestoneTab.CssClass = "alert alert-success";
        //                                }
        //                            }
        //                        }
        //                    }
        //                    else
        //                    {
        //                        int MilestoneID = Convert.ToInt32(ViewState["MilestoneID"]);

        //                        var updateobj = (from row in entities.Cont_tbl_MilestoneInstance
        //                                         where row.ContractTemplateID == newContractID
        //                                         && row.ID == MilestoneID
        //                                         && row.CustomerID == customerID
        //                                         select row).FirstOrDefault();

        //                        if (updateobj != null)
        //                        {
        //                            updateobj.Title = txtboxMilestonetitle.Text;
        //                            updateobj.Description = txtboxMilestonedescription.Text;
        //                            updateobj.DeptID = Convert.ToInt32(drpmilestoneDepartment.SelectedValue);
        //                            updateobj.UserID = Convert.ToInt32(drpmilestoneDepartmentUser.SelectedValue);
        //                            updateobj.Remark = txbRemark.Text;
        //                            if (!string.IsNullOrEmpty(txbmilestoneduedate.Text))
        //                                updateobj.DueDate = DateTimeExtensions.GetDate(txbmilestoneduedate.Text);

        //                            updateobj.StatusID = Convert.ToInt32(drpmilestonestatus.SelectedValue);
        //                            updateobj.UpdateBy = userID;
        //                            updateobj.UpdatedOn = DateTime.Now;
        //                            entities.SaveChanges();
        //                        }

        //                        Cont_tbl_MileStoneTransaction Transactionobj = new Cont_tbl_MileStoneTransaction
        //                        {
        //                            ContractID = newContractID,
        //                            //CustomerID = customerID,
        //                            MileStoneID = MilestoneID,
        //                            StatusID = Convert.ToInt32(drpmilestonestatus.SelectedValue),
        //                            Remark = txbRemark.Text,
        //                            IsActive = true,
        //                            CreatedBy = userID,
        //                            CreatedOn = DateTime.Now,
        //                        };
        //                        entities.Cont_tbl_MileStoneTransaction.Add(Transactionobj);
        //                        entities.SaveChanges();

        //                        BindContractMilestone_Paging(customerID, newContractID, Convert.ToInt32(gridMilestone.PageSize), Convert.ToInt32(gridMilestone.PageIndex) + 1, gridMilestone);

        //                        HideShowMilestoneDiv(false);

        //                        clearTaskControls();

        //                        cvMilestoneTab.IsValid = false;
        //                        cvMilestoneTab.ErrorMessage = "Milestone Detail Save Successfully.";
        //                        vsMilestoneTab.CssClass = "alert alert-success";
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //}

        public void SaveMilestoneScheduled(long newContractID,long customerID,long MilestoneMasterID,string Title,
            string Description,long deptId,long userID,DateTime DueDate,long StatusID,string Remark,long CreatedBy
            ,DateTime sd,DateTime ed,string forperiod)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                Cont_tbl_MilestoneInstance addobj = new Cont_tbl_MilestoneInstance
                {
                    ContractTemplateID = newContractID,
                    CustomerID = customerID,
                    MilestoneMasterID = MilestoneMasterID,
                    //Title = Title,
                    //Description = Description,
                    //DeptID = deptId,
                    //UserID = userID,
                    DueDate = DueDate,
                    StatusID = StatusID,
                    Remark = Remark,
                    CreatedBy = CreatedBy,
                    CreatedOn = DateTime.Now,
                    IsActive = true,
                    StartDate = sd,
                    EndDate = ed,
                    ForPeriod = forperiod
                };
                entities.Cont_tbl_MilestoneInstance.Add(addobj);
                entities.SaveChanges();

                int MilestoneID = Convert.ToInt32(addobj.ID);

                Cont_tbl_MileStoneTransaction Transactionobj = new Cont_tbl_MileStoneTransaction
                {
                    ContractID = newContractID,
                    MileStoneID = MilestoneID,
                    StatusID = StatusID,
                    Remark = Remark,
                    IsActive = true,
                    CreatedBy = Convert.ToInt32(CreatedBy),
                    CreatedOn = DateTime.Now,
                };
                entities.Cont_tbl_MileStoneTransaction.Add(Transactionobj);
                entities.SaveChanges();
            }
        }

        public void BindContractMilestone_Paging(int customerID, long contractID, int pageSize, int pageNumber, GridView grd)
        {
            try
            {
                int MilestoneID = -1;
                int MilestoneStatusID = -1;
                if (dropmilestonestatus.SelectedValue != "-1" && dropmilestonestatus.SelectedValue != "")
                {
                    MilestoneStatusID = Convert.ToInt32(dropmilestonestatus.SelectedValue);
                }
                if (drpdownmilestone.SelectedValue != "-1" && drpdownmilestone.SelectedValue != "")
                {
                    MilestoneID = Convert.ToInt32(drpdownmilestone.SelectedValue);
                }

                var lstMilestones = ContractTaskManagement.GetContractMileStone_Paging(customerID, contractID, pageSize, pageNumber, MilestoneID, MilestoneStatusID);

                if (lstMilestones.Count > 0)
                    lstMilestones = lstMilestones.OrderBy(row => row.StartDate).ToList();

                //if (lstMilestones.Count > 0)
                //    lstMilestones = lstMilestones.OrderByDescending(row => row.CreatedOn).ToList();

                grd.DataSource = lstMilestones;
                grd.DataBind();

                long totalRowCount = 0;
                if (lstMilestones.Count > 0)
                {
                    if (lstMilestones[0].TotalRowCount != null)
                        totalRowCount = Convert.ToInt64(lstMilestones[0].TotalRowCount);
                    else
                    {
                        foreach (var item in lstMilestones)
                        {
                            totalRowCount = Convert.ToInt32(item.TotalRowCount);
                            break;
                        }
                    }
                }

                Session["TotalRows"] = totalRowCount;

                lstMilestones.Clear();
                lstMilestones = null;

                //lblStartRecord_Task.Text = DropDownListPageNo.SelectedValue;
                if (Convert.ToInt32(ViewState["MilestonePageFlag"]) == 0)
                {
                    bindPageNumber(4);
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvTaskTab.IsValid = false;
                //cvTaskTab.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

       
        protected void gridMilestone_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                if (ViewState["ContractInstanceID"] != null)
                {
                    gridMilestone.PageIndex = e.NewPageIndex;
                    BindContractMilestone_Paging(customerID, Convert.ToInt64(ViewState["CaseInstanceID"]), Convert.ToInt32(gridMilestone.PageSize), Convert.ToInt32(gridMilestone.PageIndex) + 1, gridMilestone);                    
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        #endregion

        protected void gridMilestone_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                if (e.CommandArgument != null)
                {
                    if (ViewState["ContractInstanceID"] != null)
                    {
                        bool txnSuccess = false;
                        long contractID = Convert.ToInt64(ViewState["ContractInstanceID"]);
                        long contractStatusID = Convert.ToInt64(ViewState["ContractStatusID"]);
                        long MilestoneID = Convert.ToInt64(e.CommandArgument);
                        
                        if (e.CommandName.Equals("EditMilestone"))
                        {
                            ViewState["MilestoneMode"] = "1";

                            ViewState["MilestoneID"] = MilestoneID;

                            if (MilestoneID != 0 && contractID != 0)
                            {
                                var milestoneDetailRecord = ContractTaskManagement.GetMilestoneDetailsByID(contractID, MilestoneID, customerID);
                                if (milestoneDetailRecord != null)
                                {
                                    txtboxMilestonetitle.Text = milestoneDetailRecord[0].Title;
                                    txtboxMilestonedescription.Text = milestoneDetailRecord[0].Description;
                                    drpmilestoneDepartment.SelectedValue = milestoneDetailRecord[0].DeptID.ToString();
                                    drpmilestoneDepartment_SelectedIndexChanged(sender, e);
                                    drpmilestoneDepartmentUser.SelectedValue = milestoneDetailRecord[0].UserID.ToString();
                                    txbmilestoneduedate.Text = milestoneDetailRecord[0].DueDate.ToString("dd-MM-yyyy");
                                    drpmilestonestatus.SelectedValue = milestoneDetailRecord[0].StatusID.ToString();
                                    txbRemark.Text = milestoneDetailRecord[0].Remark.ToString();
                                    HideShowMilestoneDiv(true);

                                    BindMilestoneResponses(contractID, MilestoneID);
                                    divmilestoneresponselog.Visible = true;
                                    drpmilestonestatus.Enabled = true;
                                    
                                    divMilestoneTypelbl.Visible = false;
                                    divMilestoneType.Visible = false;
                                    divfrequencylbl.Visible = false;
                                    divfrequencydrp.Visible = false;
                                    //recurringcustomdate.Visible = false;

                                    divduedatelbl.Visible = true;
                                    divduedate.Visible = true;
                                    grdNoticePayment.Visible = false;

                                    txtboxMilestonetitle.Enabled = false;
                                    txtboxMilestonedescription.Enabled = false;
                                    drpmilestoneDepartment.Enabled = false;
                                    drpmilestoneDepartmentUser.Enabled = false;
                                }
                            }
                        }
                        else if (e.CommandName.Equals("DeleteMileStone"))
                        {
                            txnSuccess = ContractTaskManagement.DeleteMilestone(contractID, MilestoneID, AuthenticationHelper.UserID, Convert.ToInt32(AuthenticationHelper.CustomerID));

                            if (txnSuccess)
                            {
                                long contractInstanceID = Convert.ToInt64(ViewState["ContractInstanceID"]);

                                if (contractInstanceID != 0)
                                {
                                    ViewState["MilestonePageFlag"] = 0;
                                   
                                    BindContractMilestone_Paging(customerID, contractInstanceID, Convert.ToInt32(gridMilestone.PageSize), Convert.ToInt32(gridMilestone.PageIndex) + 1, gridMilestone);
                                }                               

                                cvMilestoneTab.IsValid = false;
                                cvMilestoneTab.ErrorMessage = "Milestone Deleted Successfully";
                                vsMilestoneTab.CssClass = "alert alert-success";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvMilestoneTab.IsValid = false;
                cvMilestoneTab.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        public void BindMilestoneResponses(long contractID, long MilestoneID)
        {
            try
            {
                List<Cont_SP_GetMilestoneResponses_All_Result> lstResponses = new List<Cont_SP_GetMilestoneResponses_All_Result>();

                lstResponses = ContractTaskManagement.GetMilestoneResponses(contractID, MilestoneID);

                if (lstResponses != null && lstResponses.Count > 0)
                {
                    lstResponses = lstResponses.OrderByDescending(row => row.CreatedOn).ToList();
                }

                grdmilestonekResponseLog.DataSource = lstResponses;
                grdmilestonekResponseLog.DataBind();

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvTaskResponse.IsValid = false;
                //cvTaskResponse.ErrorMessage = "Something went wrong, Please try again";
            }
        }
        public string ShowMilestoneResponseDocCount(long contractID, long MilestoneID, long MilestoneResponseID)
        {
            try
            {
                var docCount = ContractTaskManagement.GetMilestoneResponseDocuments(contractID, MilestoneID, MilestoneResponseID).Count;

                if (docCount == 0)
                    return "No Documents";
                else if (docCount == 1)
                    return "1 File";
                else if (docCount > 1)
                    return docCount + " Files";
                else
                    return "";
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }

        protected void grdmilestonekResponseLog_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandArgument != null)
                {
                    if (e.CommandName.Equals("DownloadMilestoneResponseDoc"))
                    {
                        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                        long milestoneResponseID = Convert.ToInt64(commandArgs[0]);
                        long milestoneID = Convert.ToInt64(commandArgs[1]);
                        long contractID = Convert.ToInt64(commandArgs[2]);

                        if (milestoneResponseID != 0 && milestoneID != 0 && contractID != 0)
                        {
                            var lstTaskResponseDocument = ContractTaskManagement.GetMilestoneResponseDocuments(contractID, milestoneID, milestoneResponseID);

                            if (lstTaskResponseDocument.Count > 0)
                            {
                                using (ZipFile responseDocZip = new ZipFile())
                                {
                                    int i = 0;
                                    foreach (var file in lstTaskResponseDocument)
                                    {
                                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            int idx = file.FileName.LastIndexOf('.');
                                            string str = file.FileName.Substring(0, idx) + "_" + file.Version + "." + file.FileName.Substring(idx + 1);

                                            if (!responseDocZip.ContainsEntry(file.DocTypeID + "/" + str))
                                            {
                                                if (file.EnType == "M")
                                                {
                                                    responseDocZip.AddEntry(file.DocTypeID + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                else
                                                {
                                                    responseDocZip.AddEntry(file.DocTypeID + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                            }
                                            i++;
                                        }
                                    }

                                    var zipMs = new MemoryStream();
                                    responseDocZip.Save(zipMs);
                                    zipMs.Position = 0;
                                    byte[] Filedata = zipMs.ToArray();
                                    Response.Buffer = true;
                                    Response.ClearContent();
                                    Response.ClearHeaders();
                                    Response.Clear();
                                    Response.ContentType = "application/zip";
                                    Response.AddHeader("content-disposition", "attachment; filename=MilestoneResponseDocuments-" + DateTime.Now.ToString("dd-MM-yyyy") + ".zip");
                                    Response.BinaryWrite(Filedata);
                                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                                }
                            }
                            else
                            {
                                //cvTaskResponse.IsValid = false;
                                //cvTaskResponse.ErrorMessage = "No Document Available for Download.";
                                return;
                            }
                        }
                    }
                    else if (e.CommandName.Equals("ViewMilestoneResponseDoc"))
                    {
                        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                        long milestoneResponseID = Convert.ToInt64(commandArgs[0]);
                        long milestoneID = Convert.ToInt64(commandArgs[1]);
                        long contractID = Convert.ToInt64(commandArgs[2]);

                        if (milestoneResponseID != 0 && milestoneID != 0 && contractID != 0)
                        {
                            var lstTaskDocument = ContractTaskManagement.GetMilestoneResponseDocuments(contractID, milestoneID, milestoneResponseID);

                            if (lstTaskDocument != null)
                            {
                                List<Cont_tbl_FileData> entitiesData = lstTaskDocument.Where(entry => entry.Version != null).ToList();

                                if (lstTaskDocument.Where(entry => entry.Version == null).ToList().Count > 0)
                                {
                                    Cont_tbl_FileData entityData = new Cont_tbl_FileData();
                                    entityData.Version = "1.0";
                                    entityData.ContractID = contractID;
                                    entityData.MilestoneID = Convert.ToInt32(milestoneID);
                                    entityData.MilestoneResponceID = Convert.ToInt32(milestoneResponseID);
                                    entitiesData.Add(entityData);
                                }

                                if (entitiesData.Count > 0)
                                {
                                    rptDocmentVersionView.DataSource = entitiesData.OrderBy(entry => entry.Version);
                                    rptDocmentVersionView.DataBind();

                                    int userID = -1;
                                    userID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);

                                    ViewContractDocument(entitiesData[0].ID, userID);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvTaskResponse.IsValid = false;
                //cvTaskResponse.ErrorMessage = "Something went wrong, Please try again";
            }
        }
        public void ViewContractDocument(long contFileID, int userID)
        {
            try
            {
                var fileRecord = ContractDocumentManagement.GetContractDocumentByID(contFileID);

                if (fileRecord != null)
                {
                    string filePath = Path.Combine(Server.MapPath(fileRecord.FilePath), fileRecord.FileKey + Path.GetExtension(fileRecord.FileName));

                    if (fileRecord.FilePath != null && File.Exists(filePath))
                    {
                        string Folder = "~/TempFiles";
                        string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                        string DateFolder = Folder + "/" + File;

                        string extension = System.IO.Path.GetExtension(filePath);

                        Directory.CreateDirectory(Server.MapPath(DateFolder));

                        if (!Directory.Exists(DateFolder))
                        {
                            Directory.CreateDirectory(Server.MapPath(DateFolder));
                        }

                        string customerID = Convert.ToString(UserManagement.GetByID(userID).CustomerID ?? 0);

                        string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                        string User = userID + "" + customerID + "" + FileDate;

                        string FileName = DateFolder + "/" + User + "" + extension;

                        FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                        BinaryWriter bw = new BinaryWriter(fs);
                        if (fileRecord.EnType == "M")
                        {
                            bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                        }
                        else
                        {
                            bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                        }
                        bw.Close();
                        DocumentPath = FileName;
                        DocumentPath = DocumentPath.Substring(2, DocumentPath.Length - 2);

                        lblMessage.Text = "";

                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenDocviewer('" + DocumentPath + "');", true);
                    }
                    else
                    {
                        lblMessage.Text = "There is no file to preview";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnExecutiveSummery_Click(object sender, EventArgs e)
        {
            List<int> SelectedSections = new List<int>();
            SelectedSections.Clear();

            #region Section
            if (grdTemplateSection != null)
            {
                for (int i = 0; i < grdTemplateSection.Rows.Count; i++)
                {
                    if (grdTemplateSection.Rows[i].RowType == DataControlRowType.DataRow)
                    {
                        CheckBox chkRow = (CheckBox)grdTemplateSection.Rows[i].FindControl("chkRow");
                        if (chkRow != null)
                        {
                            if (chkRow.Checked)
                            {
                                Label lblSectionID = (Label)grdTemplateSection.Rows[i].FindControl("lblSectionID");
                                if (lblSectionID != null)
                                {
                                    if (!string.IsNullOrEmpty(lblSectionID.Text) || lblSectionID.Text != "-1" || lblSectionID.Text != "0")
                                    {
                                        SelectedSections.Add(Convert.ToInt32(lblSectionID.Text));
                                    }
                                }
                            }
                        }
                    }
                }
            }
            #endregion

            List<selectedFieldInformation> SelectedFields = new List<selectedFieldInformation>();
            SelectedFields.Clear();

            #region Fields
            if (grdContractField != null)
            {
                for (int i = 0; i < grdContractField.Rows.Count; i++)
                {
                    if (grdContractField.Rows[i].RowType == DataControlRowType.DataRow)
                    {
                        CheckBox chkRow = (CheckBox)grdContractField.Rows[i].FindControl("chkRow");
                        if (chkRow != null)
                        {
                            if (chkRow.Checked)
                            {
                                Label lblFldName = (Label)grdContractField.Rows[i].FindControl("lblTemplateFieldName");
                                Label lblFldID = (Label)grdContractField.Rows[i].FindControl("lblTemplateFieldID");
                                if (lblFldID != null && lblFldName != null)
                                {
                                    if (!string.IsNullOrEmpty(lblFldName.Text) && !string.IsNullOrEmpty(lblFldID.Text) && lblFldID.Text != "-1" && lblFldID.Text != "0")
                                    {
                                        SelectedFields.Add(new selectedFieldInformation { FieldID = Convert.ToInt32(lblFldID.Text), FieldName = lblFldName.Text });
                                    }
                                }
                            }
                        }
                    }
                }
            }
            #endregion

            if (ViewState["ContractInstanceID"] != null)
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                int userID = -1;
                userID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                
                int contractInstanceID = Convert.ToInt32(ViewState["ContractInstanceID"]);
                ExporttoWordNew(userID, customerID, contractInstanceID, SelectedSections, SelectedFields);
            }                
        }

        public bool ExporttoWordNew(int userID, int CustomerID, int ContractTemplateID,List<int> sectionids, List<selectedFieldInformation> SelectedFields)
        {
            bool resultdata = false;

            string fileName = "ExecutiveSummary";

            List<GetFieldInformation> infoFields = new List<GetFieldInformation>();
            infoFields.Clear();

            if (SelectedFields.Count > 0)
            {
                foreach (var item in SelectedFields)
                {
                    if (item.FieldID == 1)//title
                    {
                        string value = string.Empty;
                        if (!string.IsNullOrEmpty(txtTitle.Text))
                        {
                            value = txtTitle.Text;
                        }                        
                        infoFields.Add(new GetFieldInformation { FieldValue = value, FieldName = item.FieldName });
                    }
                    if (item.FieldID == 2)//no
                    {
                        string value = string.Empty;
                        if (!string.IsNullOrEmpty(txtContractNo.Text))
                        {
                            value = txtContractNo.Text;
                        }
                        infoFields.Add(new GetFieldInformation { FieldValue = value, FieldName = item.FieldName });
                    }
                    if (item.FieldID == 3)//description
                    {
                        string value = string.Empty;
                        if (!string.IsNullOrEmpty(tbxDescription.Text))
                        {
                            value = tbxDescription.Text;
                        }
                        infoFields.Add(new GetFieldInformation { FieldValue = value, FieldName = item.FieldName });
                    }
                    if (item.FieldID == 4)//department
                    {
                        string value = string.Empty;
                        if (ddlDepartment.SelectedValue != null && ddlDepartment.SelectedValue != "-1")
                        {
                            value = ddlDepartment.SelectedItem.Text;
                        }
                        infoFields.Add(new GetFieldInformation { FieldValue = value, FieldName = item.FieldName });
                    }
                    if (item.FieldID == 5)//Owner Name
                    {
                        string value = string.Empty;
                        if (lstBoxOwner.SelectedValue != null && lstBoxOwner.SelectedValue != "-1")
                        {
                            value = lstBoxOwner.SelectedItem.Text;
                        }
                        infoFields.Add(new GetFieldInformation { FieldValue = value, FieldName = item.FieldName });
                    }
                    if (item.FieldID == 7)//Vendor Name
                    {
                        string value = string.Empty;
                        if (lstBoxVendor.SelectedValue != null && lstBoxVendor.SelectedValue != "-1")
                        {
                            value = lstBoxVendor.SelectedItem.Text;
                        }
                        infoFields.Add(new GetFieldInformation { FieldValue = value, FieldName = item.FieldName });
                    }                   
                    if (item.FieldID == 9)//Person Of Department
                    {
                        string value = string.Empty;
                        if (ddlCPDepartment.SelectedValue != null && ddlCPDepartment.SelectedValue != "-1")
                        {
                            value = ddlCPDepartment.SelectedItem.Text;
                        }
                        infoFields.Add(new GetFieldInformation { FieldValue = value, FieldName = item.FieldName });
                    }
                    if (item.FieldID == 10)//Proposal Date
                    {
                        string value = string.Empty;
                        if (!string.IsNullOrEmpty(txtProposalDate.Text))
                        {
                            value = txtProposalDate.Text;
                        }
                        infoFields.Add(new GetFieldInformation { FieldValue = value, FieldName = item.FieldName });
                    }
                    if (item.FieldID == 11)//Agreement Date
                    {
                        string value = string.Empty;
                        if (!string.IsNullOrEmpty(txtAgreementDate.Text))
                        {
                            value = txtAgreementDate.Text;
                        }
                        infoFields.Add(new GetFieldInformation { FieldValue = value, FieldName = item.FieldName });
                    }
                    if (item.FieldID == 12)//Start Date Effective Date
                    {
                        string value = string.Empty;
                        if (!string.IsNullOrEmpty(txtEffectiveDate.Text))
                        {
                            value = txtEffectiveDate.Text;
                        }
                        infoFields.Add(new GetFieldInformation { FieldValue = value, FieldName = item.FieldName });
                    }
                    if (item.FieldID == 13)//Review Date
                    {
                        string value = string.Empty;
                        if (!string.IsNullOrEmpty(txtReviewDate.Text))
                        {
                            value = txtReviewDate.Text;
                        }
                        infoFields.Add(new GetFieldInformation { FieldValue = value, FieldName = item.FieldName });
                    }
                    if (item.FieldID == 14)//END Date Expiration
                    {
                        string value = string.Empty;
                        if (!string.IsNullOrEmpty(txtExpirationDate.Text))
                        {
                            value = txtExpirationDate.Text;
                        }
                        infoFields.Add(new GetFieldInformation { FieldValue = value, FieldName = item.FieldName });
                    }
                    if (item.FieldID == 15)//Contract Type
                    {
                        string value = string.Empty;
                        if (ddlContractType.SelectedValue != null && ddlContractType.SelectedValue != "-1")
                        {
                            value = ddlContractType.SelectedItem.Text;
                        }
                        infoFields.Add(new GetFieldInformation { FieldValue = value, FieldName = item.FieldName });
                    }
                    if (item.FieldID == 16)//Contract SubType
                    {
                        string value = string.Empty;
                        if (ddlContractSubType.SelectedValue != null && ddlContractSubType.SelectedValue != "-1")
                        {
                            value = ddlContractSubType.SelectedItem.Text;
                        }
                        infoFields.Add(new GetFieldInformation { FieldValue = value, FieldName = item.FieldName });
                    }
                    if (item.FieldID == 17)//Amount
                    {
                        string value = string.Empty;
                        if (!string.IsNullOrEmpty(tbxContractAmt.Text))
                        {
                            value = tbxContractAmt.Text;
                        }
                        infoFields.Add(new GetFieldInformation { FieldValue = value, FieldName = item.FieldName });
                    }
                    if (item.FieldID == 19)//Payment Type
                    {
                        string value = string.Empty;
                        if (ddlPaymentType.SelectedValue != null && ddlPaymentType.SelectedValue != "-1")
                        {
                            value = ddlPaymentType.SelectedItem.Text;
                        }
                        infoFields.Add(new GetFieldInformation { FieldValue = value, FieldName = item.FieldName });
                    }
                    if (item.FieldID == 20)//Payment Term
                    {
                        string value = string.Empty;
                        if (ddlPaymentTerm1.Items.Count > 0)
                        {
                            foreach (ListItem eachVendor in ddlPaymentTerm1.Items)
                            {
                                if (eachVendor.Selected)
                                {
                                    if (Convert.ToInt64(eachVendor.Value) != -1)
                                    {
                                        if (!string.IsNullOrEmpty(value))
                                        {
                                            value = value + "," + eachVendor.Text;
                                        }
                                        else
                                        {
                                            value = eachVendor.Text;
                                        }
                                    }
                                }
                            }

                            infoFields.Add(new GetFieldInformation { FieldValue = value, FieldName = item.FieldName });
                        }
                        //string value = string.Empty;
                        //if (ddlPaymentTerm.SelectedValue != null && ddlPaymentTerm.SelectedValue != "-1")
                        //{
                        //    value = ddlPaymentTerm.SelectedItem.Text;
                        //}
                        //infoFields.Add(new GetFieldInformation { FieldValue = value, FieldName = item.FieldName });
                    }
                    if (item.FieldID == 21)//Products
                    {
                        string value = string.Empty;
                        if (!string.IsNullOrEmpty(txtBoxProduct.Text))
                        {
                            value = txtBoxProduct.Text;
                        }
                        infoFields.Add(new GetFieldInformation { FieldValue = value, FieldName = item.FieldName });
                    }
                    if (item.FieldID == 22)//Branch Name
                    {
                        string value = string.Empty;
                        if (!string.IsNullOrEmpty(tbxBranch.Text))
                        {
                            value = tbxBranch.Text;
                        }
                        infoFields.Add(new GetFieldInformation { FieldValue = value, FieldName = item.FieldName });
                    }
                    //if (item.FieldID == 25)//Branch Name Address
                    //{
                    //    string value = contractRecord.ContractDetailDesc;
                    //    infoFields.Add(new GetFieldInformation { FieldValue = value, FieldName = item.FieldName });
                    //}
                    //if (item.FieldID == 8)//Branch Address
                    //{
                    //    string value = contractRecord.ContractDetailDesc;
                    //    infoFields.Add(new GetFieldInformation { FieldValue = value, FieldName = item.FieldName });
                    //}
                }
            }


            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data1 = (from row in entities.Sp_GetContractTemplateDetailTransaction()
                             where row.UserID == userID
                             && row.ContractTemplateInstanceID == ContractTemplateID
                             && row.RoleID == 3
                             select row).ToList();

                if (data1.Count > 0)
                {
                    var data = (from row in data1
                                join row1 in entities.Cont_tbl_SectionMaster
                                on row.sectionID equals row1.ID
                                select new getTemplateTransactionDetail
                                {
                                    TemplateTransactionID = row.TemplateTransactionID,
                                    TemplateInstanceID = row.ContractTemplateInstanceID,
                                    ComplianceStatusID = row.ComplianceStatusID,
                                    CreatedOn = row.CreatedOn,
                                    Status = row.Status,
                                    StatusChangedOn = row.StatusChangedOn,
                                    TemplateContent = row.ContractTemplateContent,
                                    RoleID = row.RoleID,
                                    UserID = row.UserID,
                                    sectionID = Convert.ToInt32(row.sectionID),
                                    SectionHeader = row1.Header
                                }).ToList();


                    if (sectionids.Count > 0)
                    {
                        data = data.Where(x => sectionids.Contains(x.sectionID)).ToList();
                    }

                    System.Text.StringBuilder strExporttoWord = new System.Text.StringBuilder();

                                strExporttoWord.Append(@"
                            <html 
                            xmlns:o='urn:schemas-microsoft-com:office:office' 
                            xmlns:w='urn:schemas-microsoft-com:office:word'
                            xmlns='http://www.w3.org/TR/REC-html40'>
                            <head><title></title>

                            <!--[if gte mso 9]>
                            <xml>
                            <w:WordDocument>
                            <w:View>Print</w:View>
                            <w:Zoom>90</w:Zoom>
                            <w:DoNotOptimizeForBrowser/>
                            </w:WordDocument>
                            </xml>
                            <![endif]-->

                            <style>
                            p.MsoFooter, li.MsoFooter, div.MsoFooter
                            {
                            margin:0in;
                            margin-bottom:.0001pt;
                            mso-pagination:widow-orphan;
                            tab-stops:center 3.0in right 6.0in;
                            font-size:12.0pt;
                            }
                            <style>

                            <!-- /* Style Definitions */

                            @page Section1
                            {
                            size:8.5in 11.0in; 
                            margin:1.0in 1.25in 1.0in 1.25in ;
                            mso-header-margin:.5in;
                            mso-header: h1;
                            mso-footer: f1; 
                            mso-footer-margin:.5in;
                             font-family:Arial;
                            }

                            div.Section1
                            {
                            page:Section1;
                            }
                            table#hrdftrtbl
                            {
                                margin:0in 0in 0in 9in;
                            }

                            .break { page-break-before: always; }
                            -->
                            </style></head>");

                                strExporttoWord.Append(@"<body lang=EN-US style='tab-interval:.5in'> <div class=Section1>");

                    if (infoFields.Count > 0)
                    {
                        strExporttoWord.Append(@"<p><B>Executive Summery</B></p>");

                        strExporttoWord.Append(@"<div>");
                        strExporttoWord.Append(@"<table border='1'>");

                        strExporttoWord.Append(@"<tr>");
                        strExporttoWord.Append(@"<td><B>Parameter</B></td>");
                        strExporttoWord.Append(@"<td><B>value</B></td>");
                        strExporttoWord.Append(@"</tr>");

                        infoFields.ForEach(eachSection =>
                        {
                            strExporttoWord.Append(@"<tr>");
                            strExporttoWord.Append(@"<td>" + eachSection.FieldName + "</td>");
                            strExporttoWord.Append(@"<td>" + eachSection.FieldValue + "</td>");
                            strExporttoWord.Append(@"</tr>");
                        });

                        strExporttoWord.Append(@"</table>");
                        strExporttoWord.Append(@"</div>");
                    }

                    if (data.Count > 0)
                    {
                        data.ForEach(eachSection =>
                        {
                            strExporttoWord.Append(@"<div>");
                            strExporttoWord.Append(@"<p><B>" + eachSection.SectionHeader + "</B></p>");
                            strExporttoWord.Append(@"<p>" + eachSection.TemplateContent + "</p>");
                            strExporttoWord.Append(@"</div>");
                        });
                    }
                    strExporttoWord.Append(@"<table id='hrdftrtbl' border='1' cellspacing='0' cellpadding='0'>
                            <tr>
                                <td>
                                    <div style='mso-element:header' id=h1 >
                                        <p class=MsoHeader style='text-align:left'>
                                    </div>
                                </td>
                                <td>
                                    <div style='mso-element:footer' id=f1>
                                        <p class=MsoFooter>
                                        <span style=mso-tab-count:2'></span>
                                        <span style='mso-field-code:"" PAGE ""'></span>
                                        of <span style='mso-field-code:"" NUMPAGES ""'></span>
                                        </p>
                                    </div>
                                </td>
                            </tr>
                            </table>
                            </body>
                </html>");
                    
                    fileName = fileName + "-" + DateTime.Now.ToString("ddMMyyyy");

                    Response.AppendHeader("Content-Type", "application/msword");
                    Response.AppendHeader("Content-disposition", "attachment; filename=" + fileName + ".doc");
                    Response.Write(strExporttoWord);
                    resultdata = true;
                }
            }
            return resultdata;
        }

        protected void RdoMilestoneType_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (RdoMilestoneType.SelectedValue == "2")
            {
                divfrequencylbl.Visible = false;
                divfrequencydrp.Visible = false;
                //recurringcustomdate.Visible = false;

                divduedatelbl.Visible = true;
                divduedate.Visible = true;
                grdNoticePayment.Visible = false;
            }
            else
            {
                divfrequencylbl.Visible = true;
                divfrequencydrp.Visible = true;
                //recurringcustomdate.Visible = true;

                divduedatelbl.Visible = false;
                divduedate.Visible = false;
                int ContractID = -1;
                if (ViewState["ContractInstanceID"] != null)
                {
                    ContractID = Convert.ToInt32(ViewState["ContractInstanceID"]);
                }
                grdNoticePayment.Visible = true;
                if (DRPMilestonePeriod.SelectedValue != "-1")
                {
                    BindNoticePayments(ContractID, DRPMilestonePeriod.SelectedValue);
                }
                ViewState["PaymentMode"] = "New";
            }
        }

        public void BindNoticePayments(int ContractID,string Frequency)
        {
            try
            {
                List<TempMileStoneSchedule> lstNoticePayments = new List<TempMileStoneSchedule>();
                lstNoticePayments = ContractManagement.getMilestonedetail(ContractID, Frequency,Convert.ToInt64(AuthenticationHelper.CustomerID));
                
                if (lstNoticePayments != null && lstNoticePayments.Count > 0)
                {
                    grdNoticePayment.DataSource = lstNoticePayments;
                    grdNoticePayment.DataBind();
                }
                else
                {
                    TempMileStoneSchedule obj = new TempMileStoneSchedule(); //initialize empty class that may contain properties
                    lstNoticePayments.Add(obj); //Add empty object to list

                    grdNoticePayment.DataSource = lstNoticePayments; /*Assign datasource to create one row with default values for the class you have*/
                    grdNoticePayment.DataBind(); //Bind that empty source                    

                    //To Hide row
                    grdNoticePayment.Rows[0].Visible = false;
                    grdNoticePayment.Rows[0].Controls.Clear();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvNoticePayment.IsValid = false;
                cvNoticePayment.ErrorMessage = "Server Error Occurred. Please try again.";
                ValidationSummary4.CssClass = "alert alert-danger";
            }
        }

        protected void grdNoticePayment_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                TextBox tbxPaymentDate = (TextBox)grdNoticePayment.FooterRow.FindControl("tbxPaymentDate");
                TextBox tbxPaymentID = (TextBox)grdNoticePayment.FooterRow.FindControl("tbxPaymentID");
                TextBox tbxFromPaymentDate = (TextBox)grdNoticePayment.FooterRow.FindControl("tbxFromPaymentDate");
                TextBox tbxToPaymentDate = (TextBox)grdNoticePayment.FooterRow.FindControl("tbxToPaymentDate");
                
                int ContractID = -1;
                if (ViewState["ContractInstanceID"] != null)
                {
                    ContractID = Convert.ToInt32(ViewState["ContractInstanceID"]);
                }
                //int noticeInstanceID = Convert.ToInt32(ViewState["noticeInstanceID"]);
                int payId = Convert.ToInt32(e.CommandArgument);
                if (e.CommandArgument != null)
                {
                    if (e.CommandName.Equals("DeletePayment"))
                    {
                        ContractTaskManagement.DeleteCustomschedule(payId, Convert.ToInt32(AuthenticationHelper.UserID));
                        BindNoticePayments(ContractID, DRPMilestonePeriod.SelectedValue);
                    }
                    #region edit Payment
                    else if (e.CommandName.Equals("EditPayment"))
                    {
                        if (ContractID != 0 && payId != 0)
                        {
                            var Data = ContractManagement.GetPaymentDetailsByID(ContractID, payId);
                            tbxToPaymentDate.Text = Data.ToMonth;
                            tbxFromPaymentDate.Text = Data.FromMonth;
                            tbxPaymentDate.Text = Convert.ToDateTime(Data.DueDate).ToString("dd-MM-yyyy");
                            tbxPaymentID.Text = Convert.ToString(payId);
                            ViewState["PaymentMode"] = "Edit";                            
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);             
            }
        }

        protected void grdNoticePayment_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton lnkBtnDeletePayment = (LinkButton)e.Row.FindControl("lnkBtnDeletePayment");
                if (lnkBtnDeletePayment != null)
                {
                    var scriptManager = ScriptManager.GetCurrent(this.Page);
                    scriptManager.RegisterPostBackControl(lnkBtnDeletePayment);
                }
            }            
        }

        protected void grdNoticePayment_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (ViewState["ContractInstanceID"] != null)
                {
                    grdNoticePayment.PageIndex = e.NewPageIndex;

                    //Re-Bind Notice Payments
                    if (DRPMilestonePeriod.SelectedValue != "-1")
                    {
                        BindNoticePayments(Convert.ToInt32(ViewState["ContractInstanceID"]), DRPMilestonePeriod.SelectedValue);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void btnPaymentSave_Click(object sender, EventArgs e)
        {
            try
            {
                TextBox tbxPaymentDate = (TextBox)grdNoticePayment.FooterRow.FindControl("tbxPaymentDate");
                TextBox tbxPaymentID = (TextBox)grdNoticePayment.FooterRow.FindControl("tbxPaymentID");
                TextBox tbxfrommonthdt = (TextBox)grdNoticePayment.FooterRow.FindControl("tbxFromPaymentDate");
                TextBox tbxToPaymentDate = (TextBox)grdNoticePayment.FooterRow.FindControl("tbxToPaymentDate");
                

                int ContractID = -1;
                if (ViewState["ContractInstanceID"] != null)
                {
                    ContractID = Convert.ToInt32(ViewState["ContractInstanceID"]);
                }

                long newPaymentID = 0;
                if (ViewState["ContractInstanceID"] != null)
                {
                    if (tbxPaymentDate != null)
                    {
                        bool validateData = false;
                        bool saveSuccess = false;
                        if (Convert.ToString(ViewState["PaymentMode"]) == "Edit")
                        {
                            int ID = Convert.ToInt32(tbxPaymentID.Text);
                            var lstdetails1 = ContractManagement.getMilestonedetail(ContractID, DRPMilestonePeriod.SelectedValue, Convert.ToInt64(AuthenticationHelper.CustomerID));
                            var data = lstdetails1.Where(x => x.ID == ID).FirstOrDefault();
                            if (data != null)
                            {
                                if (data.FromMonth.Trim() != tbxfrommonthdt.Text.Trim())
                                {
                                    ViewState["PaymentMode"] = "New";
                                }
                            }                            
                        }

                        if (Convert.ToString(ViewState["PaymentMode"]) != "Edit")
                        {
                           var lstdetails = ContractManagement.getMilestonedetail(ContractID, DRPMilestonePeriod.SelectedValue, Convert.ToInt64(AuthenticationHelper.CustomerID));
                            foreach (var item in lstdetails)
                            {
                                ContractTaskManagement.DeleteCustomschedule(item.ID, Convert.ToInt32(AuthenticationHelper.UserID));                                
                            }
                            
                            if (DRPMilestonePeriod.SelectedValue != "-1")
                            {
                                int tillloop = 0;
                                int month = 0;
                                if (DRPMilestonePeriod.SelectedValue == "M")
                                {
                                    tillloop = 12;
                                    month = 1;
                                }
                                if (DRPMilestonePeriod.SelectedValue == "Q")
                                {
                                    tillloop = 4;
                                    month = 3;
                                }
                                if (DRPMilestonePeriod.SelectedValue == "H")
                                {
                                    tillloop = 2;
                                    month = 6;
                                }
                                if (DRPMilestonePeriod.SelectedValue == "Y")
                                {
                                    tillloop = 1;
                                    month = 12;
                                }
                                
                                DateTime frmdt = DateTime.Parse(tbxfrommonthdt.Text);
                                DateTime todt = DateTime.Parse(tbxToPaymentDate.Text);
                                DateTime Duedt = DateTime.Parse(tbxPaymentDate.Text);
                                int i = 0;
                                while (i < tillloop)
                                {
                                    if (i == 0)
                                    {
                                        TempMileStoneSchedule newRecord = new TempMileStoneSchedule()
                                        {
                                            ContractID = ContractID,
                                            MileStoneID = 1,
                                            FromMonth = string.Format("{0:dd MMMM}", frmdt),
                                            ToMonth = string.Format("{0:dd MMMM}", todt),
                                            DueDate = string.Format("{0:dd MMMM}", Duedt),
                                            IsDelete = false,
                                            CreatedBy = AuthenticationHelper.UserID,
                                            CreateOn = DateTime.Now,
                                            FrequencyType = DRPMilestonePeriod.SelectedValue,
                                            SequenceID = i + 1,
                                            CustID = AuthenticationHelper.CustomerID
                                        };
                                        saveSuccess = ContractManagement.CreateMilestoneLog(newRecord);
                                    }
                                    else
                                    {
                                        if (todt.Month == 2 && todt.Day == 28)
                                        {
                                            frmdt = todt.AddDays(2);
                                            if (frmdt.Day == 2)
                                            {
                                                frmdt = frmdt.AddDays(-1);
                                            }
                                        }
                                        else
                                            frmdt = todt.AddDays(1);

                                        if (frmdt.Month + month <= 12)
                                        {
                                            todt = new DateTime(frmdt.Year, Convert.ToInt32(frmdt.Month + month), Convert.ToInt32(frmdt.Day));
                                            todt = todt.AddDays(-1);
                                        }
                                        else
                                        {
                                            int setmonth = (frmdt.Month + month) - 12;
                                            if (setmonth == 2 && frmdt.Day > 28)
                                            {
                                                try
                                                {
                                                    todt = new DateTime(frmdt.Year + 1, Convert.ToInt32(setmonth), Convert.ToInt32(frmdt.Day));
                                                    todt = todt.AddDays(-1);
                                                }
                                                catch {
                                                    todt = new DateTime(frmdt.Year + 1, Convert.ToInt32(setmonth), Convert.ToInt32(28));
                                                    //todt = todt.AddDays(-1);
                                                }
                                            }
                                            else
                                            {
                                                todt = new DateTime(frmdt.Year + 1, Convert.ToInt32(setmonth), Convert.ToInt32(frmdt.Day));
                                                todt = todt.AddDays(-1);
                                            }
                                        }

                                        TempMileStoneSchedule newRecord = new TempMileStoneSchedule()
                                        {
                                            ContractID = ContractID,
                                            MileStoneID = 1,
                                            FromMonth = string.Format("{0:dd MMMM}", frmdt),
                                            ToMonth = string.Format("{0:dd MMMM}", todt),
                                            DueDate = string.Format("{0:dd MMMM}", todt),
                                            //DueDate = todt,
                                            IsDelete = false,
                                            CreatedBy = AuthenticationHelper.UserID,
                                            CreateOn = DateTime.Now,
                                            FrequencyType = DRPMilestonePeriod.SelectedValue,
                                            SequenceID = i + 1,
                                            CustID = AuthenticationHelper.CustomerID
                                        };
                                        saveSuccess = ContractManagement.CreateMilestoneLog(newRecord);
                                    }
                                    i++;
                                }
                            }
                            if (DRPMilestonePeriod.SelectedValue != "-1")
                            {
                                BindNoticePayments(ContractID, DRPMilestonePeriod.SelectedValue);
                            }
                        }
                        if (Convert.ToString(ViewState["PaymentMode"]) == "Edit")
                        {                            
                            TempMileStoneSchedule newRecord = new TempMileStoneSchedule()
                            {
                                ContractID = ContractID,
                                MileStoneID = 1,
                                FromMonth = tbxfrommonthdt.Text,
                                ToMonth = tbxToPaymentDate.Text,
                                DueDate = tbxPaymentDate.Text,
                                IsDelete = false,
                                UpdatedBy = AuthenticationHelper.UserID,
                            };

                            newRecord.ID = Convert.ToInt32(tbxPaymentID.Text);
                            newPaymentID = ContractManagement.UpdateMilestone(newRecord);                                                          
                            if (DRPMilestonePeriod.SelectedValue != "-1")
                            {
                                BindNoticePayments(ContractID, DRPMilestonePeriod.SelectedValue);
                            }
                        }
                    }
                }                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void dtbutton_Click(object sender, EventArgs e)
        {
            TextBox tbxfrommonthdt = (TextBox)grdNoticePayment.FooterRow.FindControl("tbxFromPaymentDate");
            TextBox tbxToPaymentDate = (TextBox)grdNoticePayment.FooterRow.FindControl("tbxToPaymentDate");
            TextBox tbxPaymentDate = (TextBox)grdNoticePayment.FooterRow.FindControl("tbxPaymentDate");
            if (DRPMilestonePeriod.SelectedValue != "-1")
            {
                int month = 0;
                if (DRPMilestonePeriod.SelectedValue == "M")
                    month = 1;

                if (DRPMilestonePeriod.SelectedValue == "Q")
                    month = 3;

                if (DRPMilestonePeriod.SelectedValue == "H")
                    month = 6;

                if (DRPMilestonePeriod.SelectedValue == "Y")
                    month = 12;

                DateTime frommonthdt = DateTime.Parse(tbxfrommonthdt.Text);

                if (frommonthdt.Month + month <= 12)
                {
                    if (frommonthdt.Month + month == 2)
                    {
                        if (frommonthdt.Day > 28)
                        {
                            DateTime nextDate = new DateTime(frommonthdt.Year, Convert.ToInt32(frommonthdt.Month + month), 28);
                            tbxToPaymentDate.Text = string.Format("{0:dd MMMM}", nextDate);
                        }
                        else
                        {
                            DateTime nextDate = new DateTime(frommonthdt.Year, Convert.ToInt32(frommonthdt.Month + month), Convert.ToInt32(frommonthdt.Day));
                            tbxToPaymentDate.Text = string.Format("{0:dd MMMM}", nextDate.AddDays(-1));
                        }
                    }
                    else
                    {
                        DateTime nextDate = new DateTime(frommonthdt.Year, Convert.ToInt32(frommonthdt.Month + month), Convert.ToInt32(frommonthdt.Day));
                        tbxToPaymentDate.Text = string.Format("{0:dd MMMM}", nextDate.AddDays(-1));
                    }
                }
                else
                {
                    int setmonth = (frommonthdt.Month + month) - 12;
                    if (setmonth == 2 && frommonthdt.Day > 28)
                    {
                        DateTime nextDate = new DateTime(frommonthdt.Year + 1, Convert.ToInt32(setmonth), 28);
                        tbxToPaymentDate.Text = string.Format("{0:dd MMMM}", nextDate.AddDays(-1));
                    }
                    else
                    {
                        DateTime nextDate = new DateTime(frommonthdt.Year + 1, Convert.ToInt32(setmonth), Convert.ToInt32(frommonthdt.Day));
                        tbxToPaymentDate.Text = string.Format("{0:dd MMMM}", nextDate.AddDays(-1));
                    }
                }
                //tbxPaymentDate
                //tbxPaymentDate.
            }
        }

        protected void DRPMilestonePeriod_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (RdoMilestoneType.SelectedValue == "2")
            {
                divfrequencylbl.Visible = false;
                divfrequencydrp.Visible = false;
                //recurringcustomdate.Visible = false;

                divduedatelbl.Visible = true;
                divduedate.Visible = true;
                grdNoticePayment.Visible = false;
            }
            else
            {
                divfrequencylbl.Visible = true;
                divfrequencydrp.Visible = true;
                //recurringcustomdate.Visible = true;

                divduedatelbl.Visible = false;
                divduedate.Visible = false;
                int ContractID = -1;
                if (ViewState["ContractInstanceID"] != null)
                {
                    ContractID = Convert.ToInt32(ViewState["ContractInstanceID"]);
                }
                grdNoticePayment.Visible = true;
                if (DRPMilestonePeriod.SelectedValue != "-1")
                {
                    BindNoticePayments(ContractID, DRPMilestonePeriod.SelectedValue);
                }
                ViewState["PaymentMode"] = "New";
            }
        }

        //protected void lnkbtnMilestoneadd12_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        HideShowMilestoneDiv(true);
        //        clearMileStoneControls();

        //        ViewState["MilestoneMode"] = "0";
        //        int newContractID = -1;
        //        if (ViewState["ContractInstanceID"] != null)
        //        {
        //            newContractID = Convert.ToInt32(ViewState["ContractInstanceID"]);
        //        }

        //        BindMilestoneResponses(newContractID, 0);
        //        divmilestoneresponselog.Visible = false;
        //        drpmilestonestatus.SelectedValue = "1";
        //        drpmilestonestatus.Enabled = false;

        //        divMilestoneTypelbl.Visible = true;
        //        divMilestoneType.Visible = true;
        //        RdoMilestoneType.SelectedValue = "2";
        //        divfrequencylbl.Visible = false;
        //        divfrequencydrp.Visible = false;
        //        //recurringcustomdate.Visible = false;
        //        grdNoticePayment.Visible = false;
        //        divduedatelbl.Visible = true;
        //        divduedate.Visible = true;
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //}

        protected void lnkbtnMilestoneadd12_Click1(object sender, EventArgs e)
        {
            try
            {
                HideShowMilestoneDiv(true);
                clearMileStoneControls();

                ViewState["MilestoneMode"] = "0";
                int newContractID = -1;
                if (ViewState["ContractInstanceID"] != null)
                {
                    newContractID = Convert.ToInt32(ViewState["ContractInstanceID"]);
                }

                BindMilestoneResponses(newContractID, 0);
                divmilestoneresponselog.Visible = false;
                drpmilestonestatus.SelectedValue = "1";
                drpmilestonestatus.Enabled = false;

                divMilestoneTypelbl.Visible = true;
                divMilestoneType.Visible = true;
                RdoMilestoneType.SelectedValue = "2";
                divfrequencylbl.Visible = false;
                divfrequencydrp.Visible = false;
                //recurringcustomdate.Visible = false;
                grdNoticePayment.Visible = false;
                divduedatelbl.Visible = true;
                divduedate.Visible = true;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            try
            {
                HideShowMilestoneDiv(true);
                clearMileStoneControls();

                ViewState["MilestoneMode"] = "0";
                int newContractID = -1;
                if (ViewState["ContractInstanceID"] != null)
                {
                    newContractID = Convert.ToInt32(ViewState["ContractInstanceID"]);
                }

                BindMilestoneResponses(newContractID, 0);
                divmilestoneresponselog.Visible = false;
                drpmilestonestatus.SelectedValue = "1";
                drpmilestonestatus.Enabled = false;

                divMilestoneTypelbl.Visible = true;
                divMilestoneType.Visible = true;
                RdoMilestoneType.SelectedValue = "2";
                divfrequencylbl.Visible = false;
                divfrequencydrp.Visible = false;
                //recurringcustomdate.Visible = false;
                grdNoticePayment.Visible = false;
                divduedatelbl.Visible = true;
                divduedate.Visible = true;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                HideShowMilestoneDiv(true);
                clearMileStoneControls();

                ViewState["MilestoneMode"] = "0";
                int newContractID = -1;
                if (ViewState["ContractInstanceID"] != null)
                {
                    newContractID = Convert.ToInt32(ViewState["ContractInstanceID"]);
                }

                BindMilestoneResponses(newContractID, 0);
                divmilestoneresponselog.Visible = false;
                drpmilestonestatus.SelectedValue = "1";
                drpmilestonestatus.Enabled = false;

                divMilestoneTypelbl.Visible = true;
                divMilestoneType.Visible = true;
                RdoMilestoneType.SelectedValue = "2";
                divfrequencylbl.Visible = false;
                divfrequencydrp.Visible = false;
                //recurringcustomdate.Visible = false;
                grdNoticePayment.Visible = false;
                divduedatelbl.Visible = true;
                divduedate.Visible = true;


                txtboxMilestonetitle.Enabled = true;
                txtboxMilestonedescription.Enabled = true;
                drpmilestoneDepartment.Enabled = true;
                drpmilestoneDepartmentUser.Enabled = true;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnapplymilestone_Click(object sender, EventArgs e)
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            long contractInstanceID = Convert.ToInt64(ViewState["ContractInstanceID"]);

            if (contractInstanceID != 0)
            {
                ViewState["MilestonePageFlag"] = "0";
                BindContractMilestone_Paging(customerID, contractInstanceID, Convert.ToInt32(gridMilestone.PageSize), Convert.ToInt32(gridMilestone.PageIndex) + 1, gridMilestone);
                //BindMilestonedrp(contractInstanceID, customerID);
            }
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            try
            {
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("AuditLogDetail");
                    DataTable ExcelData = null;
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        if (ViewState["ContractInstanceID"] != null)
                        {
                            int customerID = -1;
                            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                            int userID = -1;
                            if (!(string.IsNullOrEmpty(ddlUsers_AuditLog.SelectedValue)))
                            {
                                userID = Convert.ToInt32(ddlUsers_AuditLog.SelectedValue);
                            }

                            string fromDate = string.Empty;
                            string toDate = string.Empty;

                            long contractInstanceID = Convert.ToInt64(ViewState["ContractInstanceID"]);

                            if (contractInstanceID != 0)
                            {
                                var lstAuditLogs = ContractManagement.GetContractAuditLogs_All(customerID, contractInstanceID);

                                if (userID != -1 && userID != 0)
                                    lstAuditLogs = lstAuditLogs.Where(row => row.CreatedBy == userID).ToList();

                                if (!(string.IsNullOrEmpty(txtFromDate_AuditLog.Text.Trim())))
                                {
                                    try
                                    {
                                        fromDate = txtFromDate_AuditLog.Text.Trim();
                                        lstAuditLogs = lstAuditLogs.Where(row => row.CreatedOn >= Convert.ToDateTime(fromDate)).ToList();
                                    }
                                    catch (Exception ex)
                                    {
                                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                    }
                                }

                                if (!(string.IsNullOrEmpty(txtToDate_AuditLog.Text.Trim())))
                                {
                                    try
                                    {
                                        toDate = txtToDate_AuditLog.Text.Trim();
                                        lstAuditLogs = lstAuditLogs.Where(row => row.CreatedOn <= Convert.ToDateTime(toDate)).ToList();
                                    }
                                    catch (Exception)
                                    {

                                    }
                                }
                                
                                var cname = CustomerManagement.CustomerGetByIDName(Convert.ToInt32(customerID));
                                DataTable table = lstAuditLogs.ToDataTable();

                                DataView view = new System.Data.DataView(table);
                                List<Cont_SP_AuditLogs_All_Result> cb = new List<Cont_SP_AuditLogs_All_Result>();

                                ExcelData = view.ToTable("Selected", false, "CreatedOn", "CreatedByUser", "Remark");

                                exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                                exWorkSheet.Cells["A1"].Value = "Customer Name:";

                                exWorkSheet.Cells["B1"].Merge = true;
                                exWorkSheet.Cells["B1"].Value = cname;

                                exWorkSheet.Cells["A2"].Style.Font.Bold = true;
                                exWorkSheet.Cells["A2"].Value = "Report Name:";

                                //exWorkSheet.Cells["B2:C2"].Merge = true;
                                exWorkSheet.Cells["B2"].Value = "audit Log Report";

                                exWorkSheet.Cells["A3"].Style.Font.Bold = true;
                                exWorkSheet.Cells["A3"].Value = "Report Generated On:";

                                //exWorkSheet.Cells["B3:C3"].Merge = true;
                                exWorkSheet.Cells["B3"].Value = DateTime.Today.Date.ToString("dd-MMM-yyyy");

                                exWorkSheet.Cells["A4"].LoadFromDataTable(ExcelData, true);
                                exWorkSheet.Cells["A4"].Style.Font.Bold = true;
                                exWorkSheet.Cells["A4"].Style.Font.Size = 12;
                                exWorkSheet.Cells["A4"].Value = "CreatedOn";
                                exWorkSheet.Cells["A4"].AutoFitColumns(40);

                                exWorkSheet.Cells["B4"].Style.Font.Bold = true;
                                exWorkSheet.Cells["B4"].Style.Font.Size = 12;
                                exWorkSheet.Cells["B4"].Value = "CreatedByUser";
                                exWorkSheet.Cells["B4"].AutoFitColumns(40);

                                exWorkSheet.Cells["C4"].Style.Font.Bold = true;
                                exWorkSheet.Cells["C4"].Style.Font.Size = 12;
                                exWorkSheet.Cells["C4"].Value = "Remark";
                                exWorkSheet.Cells["C4"].AutoFitColumns(40);
                            }
                        }
                    }
                    using (ExcelRange col = exWorkSheet.Cells[1, 1, 4 + ExcelData.Rows.Count, 3])
                    {
                        col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;

                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        
                    }
                    using (ExcelRange col = exWorkSheet.Cells[1, 1, 4 + ExcelData.Rows.Count, 3])
                    {
                        col[1, 1, 4 + ExcelData.Rows.Count, 3].Style.Numberformat.Format = "dd/MMM/yyyy";
                    }

                    Byte[] fileBytes = exportPackge.GetAsByteArray();
                    Response.ClearContent();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=AuditLogDetails.xlsx");
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.ms-excel";
                    StringWriter sw = new StringWriter();
                    Response.BinaryWrite(fileBytes);
                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdTransactionHistory_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (ViewState["ContractInstanceID"] != null)
                {
                    grdTransactionHistory.PageIndex = e.NewPageIndex;

                    BindTransactions();
                }
                //grdTransactionHistory.PageIndex = e.NewPageIndex;
                //BindTransactions(Convert.ToInt32(ViewState["ContractInstanceID"]));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvContractPopUp.IsValid = false;
                cvContractPopUp.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindTransactions()
        {
            try
            {
                if (ViewState["ContractInstanceID"] != null)
                {
                    int customerID = -1;
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                    //long contractID = Convert.ToInt32(ViewState["contractID"]);
                    long contractID = 0;
                    contractID = Convert.ToInt64(ViewState["ContractInstanceID"]);
                    int contractInstanceID = Convert.ToInt32(ViewState["ContractInstanceID"]);
                    DateTime assigndate = Convert.ToDateTime(ViewState["TaskAssignOn"]);
                    string tasktitle = Convert.ToString(ViewState["TaskTitle"]);
                    int TaskInstanceID = Convert.ToInt32(ViewState["TaskInstanceID"]);
                    //var lstAuditLogs = ContractManagement.GetContractAuditLogs_All(customerID, contractID);
                    var lstAuditLogs = ContractManagement.GetTaskAuditLogs_All(customerID, contractID, assigndate,tasktitle, TaskInstanceID);

                    grdTransactionHistory.DataSource = lstAuditLogs;
                    grdTransactionHistory.DataBind();
                }
                else
                {
                    grdTransactionHistory.DataSource = null;
                    grdTransactionHistory.DataBind();
                    clearTaskAuditLog();
                }
                bindPageNumberNew();
                ShowGridDetail();
                //long contractID = Convert.ToInt32(ViewState["contractID"]);
                //grdTransactionHistory.DataSource = ContractManagement.GetContractAuditLogs_All(customerID, contractID);
                //grdTransactionHistory.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvContractPopUp.IsValid = false;
                cvContractPopUp.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdTransactionHistory.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);

                BindTransactions();
                //bindPageNumberNew();

                int count = Convert.ToInt32(GetTotalPagesCountNew());
                if (count > 0)
                {
                    int gridindex = grdTransactionHistory.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }

                ShowGridDetail();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void ShowGridDetail()
        {
            if (Session["TotalRows"] != null)
            {
                int PageSize = 0;
                int PageNumber = 0;

                if (!string.IsNullOrEmpty(ddlPageSize.SelectedItem.Text))
                    PageSize = Convert.ToInt32(ddlPageSize.SelectedItem.Text);

                if (!string.IsNullOrEmpty(DropDownListPageNo.SelectedValue))
                    PageNumber = Convert.ToInt32(DropDownListPageNo.SelectedValue);

                var EndRecord = 0;
                var TotalRecord = 0;
                var TotalValue = PageSize * PageNumber;

                TotalRecord = Convert.ToInt32(Session["TotalRows"]);
                if (TotalRecord < TotalValue)
                {
                    EndRecord = TotalRecord;
                }
                else
                {
                    EndRecord = TotalValue;
                }

                if (TotalRecord != 0)
                    lblStartRecord.Text = Convert.ToString(PageSize * PageNumber - PageSize + 1);
                else
                    lblStartRecord.Text = "0";

                lblEndRecord.Text = Convert.ToString(EndRecord) + " ";
                lblTotalRecord.Text = TotalRecord.ToString();
            }
            else
            {
                lblStartRecord.Text = "0";
                lblEndRecord.Text = "0 ";
                lblTotalRecord.Text = "0";
            }
        }

        protected void DropDownListPageNoNew_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNoNew.SelectedItem.ToString());

            grdTransactionHistory.PageIndex = chkSelectedPage - 1;
            grdTransactionHistory.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);

            BindTransactions();
            ShowGridDetail();
        }

        private void bindPageNumberNew()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCountNew());

                if (DropDownListPageNoNew.Items.Count > 0)
                {
                    DropDownListPageNoNew.Items.Clear();
                }

                DropDownListPageNoNew.DataTextField = "ID";
                DropDownListPageNoNew.DataValueField = "ID";

                DropDownListPageNoNew.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNoNew.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNoNew.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNoNew.Items.Add("0");
                    DropDownListPageNoNew.SelectedValue = ("0").ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private int GetTotalPagesCountNew()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        protected bool ISDocumentVisible(long conFileID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    bool result = false;
                    var details = (from row in entities.Cont_tbl_FileData
                                   where row.ID == conFileID
                                   select row).FirstOrDefault();
                    if (details != null)
                    {
                        if (ShowDownLoadIcon)
                        {
                            if (DocViewResult != "0")
                            {
                                result = true;
                            }
                            else
                            {
                                result = false;
                            }
                        }
                        else if (AuthenticationHelper.Role == "CADMN")
                        {
                            result = false;
                        }
                        else
                        {
                            result = true;
                        }

                    }

                    return result;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvNoticePopUp.IsValid = false;
                //cvNoticePopUp.ErrorMessage = "Server Error Occurred. Please try again.";
                //VSNoticePopup.CssClass = "alert alert-danger";
            }
            return false;
        }

        public static List<Cont_tbl_VisibleToUser> GetContractUserAssignment(int Contractid,int UserID)
        {
            string userid = Convert.ToString(UserID);
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var queryResult = (from row in entities.Cont_tbl_VisibleToUser
                                   where row.ContractInstanceID == Contractid
                                   && row.IsActive == true
                                   && row.VisibkeUserID == userid
                                   select row).ToList();
                return queryResult;
            }
        }
        public static List<Cont_tbl_VisibleToUser> GetContractUserAssignment(int Contractid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var queryResult = (from row in entities.Cont_tbl_VisibleToUser
                                   where row.ContractInstanceID == Contractid
                                   && row.IsActive == true  
                                   select row).ToList();
                return queryResult;
            }
        }
        //public static List<Cont_tbl_ContractInstance> GetContractUserId(int Contractid)
        //{
        //    //using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    //{

        //    //    var queryResult = (from row in entities.Cont_tbl_ContractInstance
        //    //                       join row1 in entities.Cont_tbl_UserAssignment
        //    //                       on row.ID equals row1.ContractID
        //    //                      // where (row.CreatedBy == UserID || row1.UserID == UserID || row1.CreatedBy == UserID)
        //    //                       select row).Distinct();  
        //    //    return queryResult;
        //    //}
        //}
        public void IsViewUser()
        {
            int userID = Convert.ToInt32(AuthenticationHelper.UserID);
            string userid = Convert.ToString(userID);
            int contractInstanceID = Convert.ToInt32(ViewState["ContractInstanceID"]);

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var queryResult = (from row in entities.Cont_tbl_ContractInstance
                                   where row.ID == contractInstanceID
                                   && row.IsDeleted == false
                                   select row).ToList();

                var queryResult1 = (from row in entities.Cont_tbl_UserAssignment
                                    where row.ContractID == contractInstanceID
                                      && row.IsActive == true
                                    select row).ToList();

                var queryResult3 = (from row in entities.Cont_tbl_VisibleToUser
                                    where row.ContractInstanceID == contractInstanceID
                                    && row.VisibkeUserID == userid
                                   && row.IsActive == true
                                    select row).FirstOrDefault();
                foreach (var item in queryResult)
                {
                    if (item.CreatedBy == userID)
                    {
                        btnEditContractDetail.Visible = true;
                        lnksharesummary.Visible = true;  
                        lnkSendMailWithDoc.Visible = true;
                        lnkLinkContract.Visible = true;
                        lnklinktotermsheet.Visible = true;
                    }
                }
                foreach (var item in queryResult1)
                {
                    if (item.UserID == userID)
                    {
                        btnEditContractDetail.Visible = true;
                        lnksharesummary.Visible = true; 
                        lnkSendMailWithDoc.Visible = true;  
                        lnkLinkContract.Visible = true;
                        lnklinktotermsheet.Visible = true;
                        lnklinktotermsheet.Visible = true;
                    }
                }
                if (queryResult3 != null)
                {
                    if (queryResult3.VisibkeUserID == Convert.ToString(userID) && queryResult3.ContractInstanceID == contractInstanceID)
                    {
                        btnEditContractDetail.Visible = false;
                        lnksharesummary.Visible = false;
                        lnkexecutivesummary.Visible = false;
                        lnkLinkContract.Visible = false;
                        lnklinktotermsheet.Visible = false;
                        enableDisableSummaryTab();
                        enableDisableContractDetailTabControls();
                        enableDisableDocumentsTabControls();
                        enableDisableContractMileStoneTabControls();
                        lnkSendMailWithDoc.Visible = false;
                        lnkLinkContractDoc.Visible = false;
                        ddlUsers_AuditLog.Enabled = false;
                        btnApplyFilter_AuditLog.Enabled = false;
                        btnClearFilter_AuditLog.Enabled = false;
                        btnExport.Enabled = false;
                        showHideContractSummaryTabTopButtons(false);
                    }
                    else
                    {
                        btnEditContractDetail.Visible = true;
                        lnksharesummary.Visible = true;
                        lnkSendMailWithDoc.Visible = true;
                        lnkLinkContract.Visible = true;
                        lnklinktotermsheet.Visible = true;
                    }
                }  
             
            }    
        }

        public void BindShareUsers(int contractid)
        {
            int userid = Convert.ToInt32(AuthenticationHelper.UserID);
            int customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);
            //int contractid = Convert.ToInt32(Request.QueryString["ContractInstanceID"]);
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                //var result = (from row in entities.SP_GetView_UserID(customerid,userid)
                //              select row).Distinct().ToList();

                var result = ContractUserManagement.GetUsers_Contract(customerid);
                var lstUsers = (from row in result
                                select new { ID = row.ID, Name = row.FirstName + " " + row.LastName }).OrderBy(entry => entry.Name).ToList();

                var data = (from row in entities.Cont_tbl_ContractInstance
                            join row1 in entities.Cont_tbl_UserAssignment
                            on row.ID equals row1.ContractID
                            where row.ID == contractid
                            select new
                            {
                                row.CreatedBy,
                                row1.UserID,
                            }).FirstOrDefault();
                lstUsers = lstUsers.Where(entry => entry.ID != data.CreatedBy && entry.ID != data.UserID).ToList();


                lstBoxUsers.DataValueField = "ID";
                lstBoxUsers.DataTextField = "Name";
                lstBoxUsers.DataSource = lstUsers;
                lstBoxUsers.DataBind();

                result.Clear();
                result = null;
            }
        }
        //public void BindShareUsers()
        //{
        //    try
        //    {
        //        int customerID = -1;
        //        customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
        //        int CacheClearTime = Convert.ToInt32(ConfigurationManager.AppSettings["CacheClearMasterData"]);
        //        var key = "Contusers-" + Convert.ToString(customerID);
        //        var lstAllUsers = (List<User>)HttpContext.Current.Cache[key];
        //        if (HttpContext.Current.Cache[key] == null)
        //        {
        //            lstAllUsers = ContractUserManagement.GetUsers_Contract(customerID);
        //            HttpContext.Current.Cache.Insert(key, lstAllUsers, null, DateTime.Now.AddMinutes(CacheClearTime), TimeSpan.Zero); // add it to cache
        //        }

        //        if (lstAllUsers.Count > 0)
        //            lstAllUsers = lstAllUsers.Where(entry => entry.ContractRoleID != null).ToList();

        //        var internalUsers = ContractUserManagement.GetRequiredUsers(lstAllUsers, 1); //1--Internal---

        //        lstBoxUsers.DataValueField = "ID";
        //        lstBoxUsers.DataTextField = "Name";
        //        lstBoxUsers.DataSource = internalUsers;
        //        lstBoxUsers.DataBind();

        //        lstBoxUsers.DataValueField = "ID";
        //        lstBoxUsers.DataTextField = "Name";
        //        lstBoxUsers.DataSource = internalUsers;
        //        lstBoxUsers.DataBind();



        //        var allUsers = ContractUserManagement.GetRequiredUsers(lstAllUsers, 4); //4--All---Internal and External Users

        //        ddlCPDepartment.DataValueField = "ID";
        //        ddlCPDepartment.DataTextField = "Name";
        //        ddlCPDepartment.DataSource = allUsers;
        //        ddlCPDepartment.DataBind();

        //        lstBoxTaskUser.DataValueField = "ID";
        //        lstBoxTaskUser.DataTextField = "Name";
        //        lstBoxTaskUser.DataSource = allUsers;
        //        lstBoxTaskUser.DataBind();

        //        //lstBoxTaskUser.Items.Add(new ListItem("Add New", "0"));

        //        ddlUsers_AuditLog.DataValueField = "ID";
        //        ddlUsers_AuditLog.DataTextField = "Name";
        //        ddlUsers_AuditLog.DataSource = allUsers;
        //        ddlUsers_AuditLog.DataBind();

        //        lstAllUsers.Clear();
        //        lstAllUsers = null;
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //}

        protected void btnPermission_Click(object sender, EventArgs e)
        {
            ComplianceDBEntities entities = new ComplianceDBEntities();
            List<long> lstUserMapping = new List<long>();
            List<long> lstUserRemoveMapping = new List<long>();
            List<Cont_tbl_VisibleToUser> lstUSerMapping_ToSaveFolder = new List<Cont_tbl_VisibleToUser>();
            bool saveSuccess = false;
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            long ExistUserID = Convert.ToInt64(ViewState["MyLabel"]);
            int contractInstanceID = Convert.ToInt32(ViewState["ContractInstanceID"]);
            #region Userlist
            foreach (ListItem eachUser in lstBoxUsers.Items)
            {
                if (eachUser.Selected)
                {
                    if (Convert.ToInt32(eachUser.Value) > 0)
                    {
                        lstUserMapping.Add(Convert.ToInt64(eachUser.Value));
                    }
                }
                else
                {
                    if (Convert.ToInt32(eachUser.Value) > 0)
                    {
                        lstUserRemoveMapping.Add(Convert.ToInt64(eachUser.Value));
                    }
                }
            }
            #endregion
            if (lstUserMapping.Count > 0)
            {    
                List<Cont_tbl_VisibleToUser> lstVendorMapping_ToSave = new List<Cont_tbl_VisibleToUser>(); 
                lstUserMapping.ForEach(EachVendor =>
                {
                    Cont_tbl_VisibleToUser UserMappingRecord = new Cont_tbl_VisibleToUser()
                    {
                        VisibkeUserID =Convert.ToString(EachVendor),
                        IsActive = true,
                        CreatedBy = AuthenticationHelper.UserID,
                        CreatedOn = DateTime.Now,
                        ContractInstanceID = contractInstanceID,
                        CustomerID= customerID
                    };
                    lstVendorMapping_ToSave.Add(UserMappingRecord);
                    entities.SaveChanges();
                });

                if (lstVendorMapping_ToSave.Count > 0)
                {
                    lstVendorMapping_ToSave.ForEach(eachRecord =>
                    {
                        saveSuccess = false;
                        entities.Cont_tbl_VisibleToUser.Add(eachRecord);
                        entities.SaveChanges();
                        saveSuccess = true;
                    });
                }
                if (saveSuccess)
                {
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "msgforuserupdate();", true);
                    // saveSuccess = true;
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "msgforusernotselect();", true);
            }

        }

        protected void btnAssignCPD_Click(object sender, EventArgs e)
        {
            ComplianceDBEntities entities = new ComplianceDBEntities();
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            int contractInstanceID = Convert.ToInt32(ViewState["ContractInstanceID"]);

            if (ddlCPDepartment.SelectedValue != "" && ddlCPDepartment.SelectedValue != "-1")
            {
                var data = (from row in entities.Cont_tbl_VisibleToUser
                            where row.ContractInstanceID == contractInstanceID
                            && row.VisibkeUserID == ddlCPDepartment.SelectedValue
                            select row).ToList();

                if (data.Count == 0)
                {
                    Cont_tbl_VisibleToUser UserMappingRecord = new Cont_tbl_VisibleToUser()
                    {
                        VisibkeUserID = Convert.ToString(ddlCPDepartment.SelectedValue),
                        IsActive = true,
                        CreatedBy = AuthenticationHelper.UserID,
                        CreatedOn = DateTime.Now,
                        ContractInstanceID = contractInstanceID,
                        CustomerID = customerID
                    };
                    entities.Cont_tbl_VisibleToUser.Add(UserMappingRecord);
                    entities.SaveChanges();
                }
            }
        }

        protected void lnksharesummary_Click(object sender, EventArgs e)
        {
            int contractID = Convert.ToInt32(ViewState["ContractInstanceID"]);
            var lstUserAssignment = GetContractUserAssignment(contractID);
            if (lstUserAssignment.Count > 0)
            {
                lstBoxUsers.ClearSelection();
                foreach (var eachAssignmentRecord in lstUserAssignment)
                {
                    if (eachAssignmentRecord.ContractInstanceID == contractID)
                        if (lstBoxUsers.Items.FindByValue(eachAssignmentRecord.VisibkeUserID.ToString()) != null)
                        {
                            lstBoxUsers.Items.FindByValue(eachAssignmentRecord.VisibkeUserID.ToString()).Selected = true;
                        }
                        else
                        {
                            lstBoxUsers.Items.FindByValue(eachAssignmentRecord.VisibkeUserID.ToString()).Selected = false;
                        }
                }

            }
        }

        protected void btnUnshare_Click(object sender, EventArgs e)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {  
                List<string> lstUserMapping = new List<string>();
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                int contractInstanceID = Convert.ToInt32(ViewState["ContractInstanceID"]);
                bool saveSuccess = false;
                #region Userlist
                foreach (ListItem eachUser in lstBoxUsers.Items)
                {
                    if (eachUser.Selected)
                    {
                        if (Convert.ToInt32(eachUser.Value) > 0)
                        {
                            lstUserMapping.Add((eachUser.Value));
                        }
                    }

                }
                if (lstUserMapping.Count > 0)
                {
                    foreach (var item in lstUserMapping)
                    {    
                        var data = (from row in entities.Cont_tbl_VisibleToUser
                                    where row.ContractInstanceID == contractInstanceID
                                    && row.VisibkeUserID == item    
                                    select row).ToList();

                        if(data.Count > 0)
                        {    
                            data.ForEach(entry => entry.VisibkeUserID = item);
                            data.ForEach(entry => entry.IsActive = false);
                            data.ForEach(entry => entry.UpdatedBy = Convert.ToInt32(AuthenticationHelper.UserID));
                            data.ForEach(entry => entry.UpdatedOn = DateTime.Now);
                            data.ForEach(entry => entry.CustomerID = customerID);
                            saveSuccess = true;
                        }
                    }
                    entities.SaveChanges();
                    foreach (var item in lstUserMapping)
                    {
                        lstBoxUsers.Items.FindByValue(item.ToString()).Selected = false; 
                    }
                    if(saveSuccess)
                    {
                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "msgforUnshareupdate();", true);

                    }

                }
                lnksharesummary_Click(sender, e);
            }
        }


        #endregion
        
        public void BindTempDocumentData(long UserID, long CustID)
        {
            try
            {
                grdDocument.DataSource = null;
                grdDocument.DataBind();

                if (ViewState["ContractInstanceID"] != "" && ViewState["ContractInstanceID"] != "-1" && Convert.ToInt64(ViewState["ContractInstanceID"]) > 0)
                {
                    long ContID = Convert.ToInt64(ViewState["ContractInstanceID"]);
                    var DocData = DocumentManagement.GetContractDocumentData(UserID, CustID, ContID);
                    if (DocData.Count > 0)
                    {
                        grdDocument.Visible = true;
                        grdDocument.DataSource = DocData;
                        grdDocument.DataBind();
                    }
                    else
                    {
                        grdDocument.Visible = false;
                    }
                }
                else
                {
                    var DocData = DocumentManagement.GetTempContractDocumentData(UserID, CustID);
                    if (DocData.Count > 0)
                    {
                        grdDocument.Visible = true;
                        grdDocument.DataSource = DocData;
                        grdDocument.DataBind();
                    }
                    else
                    {
                        grdDocument.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdDocument_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Delete Document"))
                {
                    long FileID = -1;
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    FileID = Convert.ToInt64(commandArgs[0]);
                    if (FileID > 0)
                    {
                        if (ViewState["ContractInstanceID"] != "" && ViewState["ContractInstanceID"] != "-1" && Convert.ToInt32(ViewState["ContractInstanceID"]) > 0)
                        {
                            if (Business.ComplianceManagement.DeleteContractDocumentFile(FileID))
                            {
                                BindTempDocumentData(Convert.ToInt64(AuthenticationHelper.UserID), Convert.ToInt64(AuthenticationHelper.CustomerID));
                                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Document deleted successfully.')", true);
                            }
                        }
                        else
                        {
                            if (Business.ComplianceManagement.DeleteTempContractDocumentFile(FileID))
                            {
                                BindTempDocumentData(Convert.ToInt64(AuthenticationHelper.UserID), Convert.ToInt64(AuthenticationHelper.CustomerID));
                                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Document deleted successfully.')", true);
                            }
                        }
                    }
                }
                else if (e.CommandName.Equals("Download Document"))
                {
                    long FileID = -1;
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    FileID = Convert.ToInt64(commandArgs[0]);
                    if (FileID > 0)
                    {
                        if (ViewState["ContractInstanceID"] != "" && ViewState["ContractInstanceID"] != "-1" && Convert.ToInt32(ViewState["ContractInstanceID"]) > 0)
                        {
                            using (ZipFile ComplianceZip = new ZipFile())
                            {
                                ContractDocument file = Business.ComplianceManagement.GetContractDocument(FileID);
                                if (file != null)
                                {
                                    Response.Buffer = true;
                                    Response.ClearContent();
                                    Response.ClearHeaders();
                                    Response.Clear();
                                    Response.ContentType = "application/octet-stream";
                                    Response.AddHeader("content-disposition", "attachment; filename= " + file.DocName);
                                    Response.TransmitFile(Server.MapPath(file.DocPath));
                                    Response.Flush();
                                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                                }
                            }
                            BindTempDocumentData(Convert.ToInt64(AuthenticationHelper.User), Convert.ToInt64(AuthenticationHelper.CustomerID));
                        }
                        else
                        {
                            using (ZipFile ComplianceZip = new ZipFile())
                            {
                                TempContractDocument file = Business.ComplianceManagement.GetTempContractDocument(FileID);
                                if (file != null)
                                {
                                    Response.Buffer = true;
                                    Response.ClearContent();
                                    Response.ClearHeaders();
                                    Response.Clear();
                                    Response.ContentType = "application/octet-stream";
                                    Response.AddHeader("content-disposition", "attachment; filename= " + file.DocName);
                                    Response.TransmitFile(Server.MapPath(file.DocPath));
                                    Response.Flush();
                                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                                }
                            }
                            BindTempDocumentData(Convert.ToInt64(AuthenticationHelper.User), Convert.ToInt64(AuthenticationHelper.CustomerID));
                        }
                    }
                }
                else if (e.CommandName.Equals("View Document"))
                {
                    long FileID = -1;
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    FileID = Convert.ToInt64(commandArgs[0]);
                    if (FileID > 0)
                    {
                        if (ViewState["ContractInstanceID"] != "" && ViewState["ContractInstanceID"] != "-1" && Convert.ToInt32(ViewState["ContractInstanceID"]) > 0)
                        {
                            ContractDocument file = Business.ComplianceManagement.GetContractDocument(FileID);
                            if (file != null)
                            {
                                string filePath = Path.Combine(Server.MapPath(file.DocPath));
                                if (file.DocName != null && File.Exists(filePath))
                                {
                                    string Folder = "~/TempFiles";
                                    string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                                    string DateFolder = Folder + "/" + File;
                                    string extension = System.IO.Path.GetExtension(filePath);
                                    if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                    {
                                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Zip file can't view please download it.')", true);
                                        //cvDuplicateEntry.IsValid = false;
                                        //cvDuplicateEntry.ErrorMessage = "Zip file can't view please download it.";
                                    }
                                    else
                                    {
                                        string CompDocReviewPath = file.DocPath;

                                        CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopenDocumentPriview('" + CompDocReviewPath + "');", true);
                                    }
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('There is no file to preview.')", true);
                                    //cvDuplicateEntry.IsValid = false;
                                    //cvDuplicateEntry.ErrorMessage = "There is no file to preview.";
                                }
                            }
                        }
                        else
                        {
                            TempContractDocument file = Business.ComplianceManagement.GetTempContractDocument(FileID);
                            if (file != null)
                            {
                                string filePath = Path.Combine(Server.MapPath(file.DocPath));
                                if (file.DocName != null && File.Exists(filePath))
                                {
                                    string Folder = "~/TempFiles";
                                    string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                                    string DateFolder = Folder + "/" + File;
                                    string extension = System.IO.Path.GetExtension(filePath);
                                    if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                    {
                                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Zip file can't view please download it.')", true);
                                        //cvDuplicateEntry.IsValid = false;
                                        //cvDuplicateEntry.ErrorMessage = "Zip file can't view please download it.";
                                    }
                                    else
                                    {
                                        string CompDocReviewPath = file.DocPath;

                                        CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopenDocumentPriview('" + CompDocReviewPath + "');", true);
                                    }
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('There is no file to preview.')", true);
                                    //cvDuplicateEntry.IsValid = false;
                                    //cvDuplicateEntry.ErrorMessage = "There is no file to preview.";
                                }
                            }
                        }
                    }
                }
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fCalltreeCollapsed();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again."; ;
            }
        }

        protected void grdDocument_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        protected void UploadDocument_Click(object sender, EventArgs e)
        {
            try
            {
                grdDocument.DataSource = null;
                grdDocument.DataBind();
                long UserID = Convert.ToInt64(AuthenticationHelper.UserID);
                long CustID = Convert.ToInt64(AuthenticationHelper.CustomerID);

                
                HttpFileCollection fileCollection = Request.Files;
                if (fileCollection.Count > 0)
                {
                    if (ViewState["ContractInstanceID"] != "" && ViewState["ContractInstanceID"] != "-1" && Convert.ToInt32(ViewState["ContractInstanceID"]) > 0)
                    {
                        ContractDocument tempContractDocument = null;
                        int ContId = Convert.ToInt32(ViewState["ContractInstanceID"]);
                        #region file upload
                        bool isBlankFile = false;
                        string[] validFileTypes = { "exe", "bat", "dll", "css", "js", "jsp", "php5", "pht", "asa", "cer", "asax", "swf", "xap", "aspx", "asp", "php", "reg", "rdp", };
                        for (int i = 0; i < fileCollection.Count; i++)
                        {
                            HttpPostedFile uploadfile = null;
                            uploadfile = fileCollection[i];
                            int filelength = uploadfile.ContentLength;
                            string fileName = Path.GetFileName(uploadfile.FileName);
                            string ext = System.IO.Path.GetExtension(uploadfile.FileName);
                            if (!string.IsNullOrEmpty(fileName))
                            {
                                if (filelength == 0)
                                {
                                    isBlankFile = true;
                                    break;
                                }
                                else if (ext == "")
                                {
                                    isBlankFile = true;
                                    break;
                                }
                                else
                                {
                                    if (ext != "")
                                    {
                                        for (int j = 0; j < validFileTypes.Length; j++)
                                        {
                                            if (ext == "." + validFileTypes[j])
                                            {
                                                isBlankFile = true;
                                                break;
                                            }
                                        }
                                    }

                                }
                            }
                        }
                        if (isBlankFile == false)
                        {
                            for (int i = 0; i < fileCollection.Count; i++)
                            {
                                HttpPostedFile uploadfile = null;
                                uploadfile = fileCollection[i];
                                string fileName = Path.GetFileName(uploadfile.FileName);
                                string directoryPath = null;

                                if (!string.IsNullOrEmpty(fileName))
                                {
                                    string[] keys = fileCollection.Keys[i].Split('$');
                                    directoryPath = Server.MapPath("~/TempDocuments/Contract/");

                                    DocumentManagement.CreateDirectory(directoryPath);
                                    string finalPath = Path.Combine(directoryPath, fileName);
                                    finalPath = finalPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");

                                    fileCollection[i].SaveAs(Server.MapPath(finalPath));

                                    Stream fs = uploadfile.InputStream;
                                    BinaryReader br = new BinaryReader(fs);
                                    Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                                    tempContractDocument = new ContractDocument()
                                    {
                                        ContractID = ContId,
                                        UserID = UserID,
                                        CustomerID = CustID,
                                        DocPath = finalPath,
                                        DocData = bytes,
                                        DocName = fileCollection[i].FileName,
                                        FileSize = uploadfile.ContentLength,
                                        IsfromComment = false
                                    };

                                    long _objTempDocumentID = DocumentManagement.CreateContractDocument(tempContractDocument);

                                    if (_objTempDocumentID > 0)
                                    {
                                        BindTempDocumentData(UserID, CustID);
                                    }
                                }
                            }
                        }
                        else
                        {
                            //cvDuplicateEntry.IsValid = false;
                            //cvDuplicateEntry.ErrorMessage = "Invalid file error. System does not support uploaded file.Please upload another file.";
                            ScriptManager.RegisterStartupScript(this, Page.GetType(), "topFunction", "topFunction();", true);
                        }
                        #endregion
                    }
                    else
                    {
                        TempContractDocument tempContractDocument = null;

                        #region file upload
                        bool isBlankFile = false;
                        string[] validFileTypes = { "exe", "bat", "dll", "css", "js", "jsp", "php5", "pht", "asa", "cer", "asax", "swf", "xap", "aspx", "asp", "php", "reg", "rdp", };
                        for (int i = 0; i < fileCollection.Count; i++)
                        {
                            HttpPostedFile uploadfile = null;
                            uploadfile = fileCollection[i];
                            int filelength = uploadfile.ContentLength;
                            string fileName = Path.GetFileName(uploadfile.FileName);
                            string ext = System.IO.Path.GetExtension(uploadfile.FileName);
                            if (!string.IsNullOrEmpty(fileName))
                            {
                                if (filelength == 0)
                                {
                                    isBlankFile = true;
                                    break;
                                }
                                else if (ext == "")
                                {
                                    isBlankFile = true;
                                    break;
                                }
                                else
                                {
                                    if (ext != "")
                                    {
                                        for (int j = 0; j < validFileTypes.Length; j++)
                                        {
                                            if (ext == "." + validFileTypes[j])
                                            {
                                                isBlankFile = true;
                                                break;
                                            }
                                        }
                                    }

                                }
                            }
                        }
                        if (isBlankFile == false)
                        {
                            for (int i = 0; i < fileCollection.Count; i++)
                            {
                                HttpPostedFile uploadfile = null;
                                uploadfile = fileCollection[i];
                                string fileName = Path.GetFileName(uploadfile.FileName);
                                string directoryPath = null;

                                if (!string.IsNullOrEmpty(fileName))
                                {
                                    string[] keys = fileCollection.Keys[i].Split('$');
                                    directoryPath = Server.MapPath("~/TempDocuments/Contract/");

                                    DocumentManagement.CreateDirectory(directoryPath);
                                    string finalPath = Path.Combine(directoryPath, fileName);
                                    finalPath = finalPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");

                                    fileCollection[i].SaveAs(Server.MapPath(finalPath));

                                    Stream fs = uploadfile.InputStream;
                                    BinaryReader br = new BinaryReader(fs);
                                    Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                                tempContractDocument = new TempContractDocument()
                                {
                                    UserID = UserID,
                                    CustomerID = CustID,
                                    DocPath = finalPath,
                                    DocData = bytes,
                                    DocName = fileCollection[i].FileName,
                                    FileSize = uploadfile.ContentLength
                                };

                                    long _objTempDocumentID = DocumentManagement.CreateTempContractDocument(tempContractDocument);

                                    if (_objTempDocumentID > 0)
                                    {
                                        BindTempDocumentData(UserID, CustID);
                                    }
                                }
                            }
                        }
                        else
                        {
                            //cvDuplicateEntry.IsValid = false;
                            //cvDuplicateEntry.ErrorMessage = "Invalid file error. System does not support uploaded file.Please upload another file.";
                            ScriptManager.RegisterStartupScript(this, Page.GetType(), "topFunction", "topFunction();", true);
                        }
                        #endregion
                    }
                }

                BindTempDocumentData(UserID, CustID);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnsavedraft_Click(object sender, EventArgs e)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                if (ViewState["Mode"] != null)
                {
                    bool formValidateSuccess = false;
                    bool saveSuccess = false;

                    List<Tuple<int, int>> lstContractUserMapping = new List<Tuple<int, int>>();
                    List<long> lstVendorMapping = new List<long>();

                    //List<int> lstOwnerMapping = new List<int>();
                    //List<int> lstApproverMapping = new List<int>();

                    #region Data Validation

                    List<string> lstErrorMsg = new List<string>();

                    //Contract Number
                    if (String.IsNullOrEmpty(txtContractNo.Text.Trim()))
                        lstErrorMsg.Add("Required Contract Number.");
                    else
                        formValidateSuccess = true;

                    //Contract Title
                    if (String.IsNullOrEmpty(txtTitle.Text))
                        lstErrorMsg.Add("Required Contract Title");
                    else
                        formValidateSuccess = true;

                    //Contract Description
                    if (String.IsNullOrEmpty(tbxDescription.Text))
                        lstErrorMsg.Add("Required Contract Description");
                    else
                        formValidateSuccess = true;

                    //Branch Location
                    if (tvBranches.SelectedValue != "" && tvBranches.SelectedValue != "-1")
                        formValidateSuccess = true;
                    else
                        lstErrorMsg.Add("Select Entity/Branch/Location");

                    //Vendor/Contractor
                    int selectedVendorCount = 0;
                    foreach (ListItem eachVendor in lstBoxVendor.Items)
                    {
                        if (eachVendor.Selected)
                            selectedVendorCount++;
                    }

                    if (selectedVendorCount > 0)
                        formValidateSuccess = true;
                    else
                        lstErrorMsg.Add("Please select at least One Vendor.");

                    //Department
                    if (ddlDepartment.SelectedValue != "" && ddlDepartment.SelectedValue != "-1")
                        formValidateSuccess = true;
                    else
                        lstErrorMsg.Add("Please select Department.");
                    
                    ////Contact Person Of Department
                    //if (ddlCPDepartment.SelectedValue != "" && ddlCPDepartment.SelectedValue != "-1")
                    //    formValidateSuccess = true;
                    //else
                    //    lstErrorMsg.Add("Please select Contact Person of Department.");

                    //Proposal Date
                    if (!string.IsNullOrEmpty(txtProposalDate.Text))
                    {
                        try
                        {
                            bool check = ContractCommonMethods.CheckValidDate(txtProposalDate.Text);
                            if (!check)
                            {
                                lstErrorMsg.Add("Please Check Proposal Date or Date should be in DD-MM-YYYY Format");
                            }
                        }
                        catch (Exception)
                        {
                            lstErrorMsg.Add("Please Check Proposal Date or Date should be in DD-MM-YYYY Format");
                        }
                    }

                    //Agreement Date
                    if (!string.IsNullOrEmpty(txtAgreementDate.Text))
                    {
                        try
                        {
                            bool check = ContractCommonMethods.CheckValidDate(txtAgreementDate.Text);
                            if (!check)
                            {
                                lstErrorMsg.Add("Please Check Agreement Date or Date should be in DD-MM-YYYY Format");
                            }
                        }
                        catch (Exception)
                        {
                            lstErrorMsg.Add("Please Check Agreement Date or Date should be in DD-MM-YYYY Format");
                        }
                    }

                    if (ddlContractStatus.SelectedItem.Text != "Draft")
                    {
                        //Effective Date
                        if (!string.IsNullOrEmpty(txtEffectiveDate.Text))
                        {
                            try
                            {
                                bool check = ContractCommonMethods.CheckValidDate(txtEffectiveDate.Text);
                                if (!check)
                                {
                                    lstErrorMsg.Add("Please Check Start Date or Date should be in DD-MM-YYYY Format");
                                }
                            }
                            catch (Exception)
                            {
                                lstErrorMsg.Add("Please Check Start Date or Date should be in DD-MM-YYYY Format");
                            }
                        }
                        else
                            lstErrorMsg.Add("Start Date of contract required.");

                        //Expiration Date
                        if (!string.IsNullOrEmpty(txtExpirationDate.Text))
                        {
                            try
                            {
                                bool check = ContractCommonMethods.CheckValidDate(txtExpirationDate.Text);
                                if (!check)
                                {
                                    lstErrorMsg.Add("Please Check Expiration Date or Date should be in DD-MM-YYYY Format");
                                }
                            }
                            catch (Exception)
                            {
                                lstErrorMsg.Add("Please Check Expiration Date or Date should be in DD-MM-YYYY Format");
                            }
                        }
                        else
                            lstErrorMsg.Add("End Date of contract required.");

                        if (!string.IsNullOrEmpty(txtlockinperiodDate.Text))
                        {
                            try
                            {
                                bool check = ContractCommonMethods.CheckValidDate(txtlockinperiodDate.Text);
                                if (!check)
                                {
                                    lstErrorMsg.Add("Please Check Lock-In Period Date or Date should be in DD-MM-YYYY Format");
                                }
                            }
                            catch (Exception)
                            {
                                lstErrorMsg.Add("Please Check Lock-In Period Date or Date should be in DD-MM-YYYY Format");
                            }
                        }
                    }

                    //Contract Template
                    //if (drdTemplateID.SelectedValue != "" && drdTemplateID.SelectedValue != "-1")
                    //    formValidateSuccess = true;
                    //else
                    //    lstErrorMsg.Add("Please select Contract Template.");

                    //Review Date
                    if (!string.IsNullOrEmpty(txtReviewDate.Text))
                    {
                        try
                        {
                            bool check = ContractCommonMethods.CheckValidDate(txtReviewDate.Text);
                            if (!check)
                            {
                                lstErrorMsg.Add("Please Check Review Date or Date should be in DD-MM-YYYY Format");
                            }
                        }
                        catch (Exception)
                        {
                            lstErrorMsg.Add("Please Check Review Date or Date should be in DD-MM-YYYY Format");
                        }
                    }


                    //Contract Type
                    if (ddlContractType.SelectedValue != "" && ddlContractType.SelectedValue != "-1")
                        formValidateSuccess = true;
                    else
                        lstErrorMsg.Add("Please select Contract Type.");

                    //Owner
                    int selectedOwnerCount = 0;
                    foreach (ListItem eachOwner in lstBoxOwner.Items)
                    {
                        if (eachOwner.Selected)
                            selectedOwnerCount++;
                    }

                    if (selectedOwnerCount > 0)
                        formValidateSuccess = true;
                    else
                        lstErrorMsg.Add("Please select at least One Contract Owner.");

                    //Approver
                    //int selectedApproverCount = 0;
                    //foreach (ListItem eachApprover in lstBoxApprover.Items)
                    //{
                    //    if (eachApprover.Selected)
                    //        selectedApproverCount++;
                    //}

                    //if (selectedApproverCount > 0)
                    //    formValidateSuccess = true;
                    //else
                    //    lstErrorMsg.Add("Select at least One Contract Approver");

                    //Contract Amt
                    if (!String.IsNullOrEmpty(tbxContractAmt.Text))
                    {
                        try
                        {
                            double n;
                            var isNumeric = double.TryParse(tbxContractAmt.Text, out n);
                            if (!isNumeric)
                            {
                                lstErrorMsg.Add("Enter Valid Contract Amount, only numbers are allowed");
                            }
                        }
                        catch (Exception)
                        {
                            lstErrorMsg.Add("Enter Valid Contract Amount, only numbers are allowed");
                        }
                    }




                    bool flagchkuserreviewer = false;
                    bool flagchkuserapprover = false;
                    //Reviewer
                    if (ddlReviewer.SelectedValue != "" && ddlReviewer.SelectedValue != "-1")
                    {
                        if (Convert.ToInt32(ddlReviewer.SelectedValue) != Convert.ToInt32(AuthenticationHelper.UserID))
                            flagchkuserreviewer = true;
                        else
                            lstErrorMsg.Add("Please select different Reviewer.");
                    }
                    //Approver
                    if (ddlApprover.SelectedValue != "" && ddlApprover.SelectedValue != "-1")
                    {
                        if (Convert.ToInt32(ddlApprover.SelectedValue) != Convert.ToInt32(AuthenticationHelper.UserID))
                            flagchkuserapprover = true;
                        else
                            lstErrorMsg.Add("Please select different Approver.");                        
                    }
                    if (flagchkuserreviewer == true && flagchkuserapprover == true)
                    {
                        if (ddlApprover.SelectedValue == ddlReviewer.SelectedValue)
                        {
                            lstErrorMsg.Add("Approver and Reviewer should not be same. Please select different user.");
                        }
                    }


                    if (lstErrorMsg.Count > 0)
                    {
                        formValidateSuccess = false;
                        showErrorMessages(lstErrorMsg, cvContractPopUp);
                    }

                    if (!cvContractPopUp.IsValid)
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "scrollUpPageScript", "scrollUpPage();", true);

                    #endregion

                    #region Save (Add/Edit)

                    if (formValidateSuccess)
                    {
                        long newContractID = 0;

                        Cont_tbl_ContractInstance contractRecord = new Cont_tbl_ContractInstance()
                        {
                            ContractType = 1,

                            CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID),
                            IsDeleted = false,

                            ContractNo = txtContractNo.Text,
                            ContractTitle = txtTitle.Text,
                            ContractDetailDesc = tbxDescription.Text,
                            AddNewClause = tbxAddNewClause.Text,
                            CustomerBranchID = Convert.ToInt32(tvBranches.SelectedValue),
                            DepartmentID = Convert.ToInt32(ddlDepartment.SelectedValue),
                            ContractTypeID = Convert.ToInt64(ddlContractType.SelectedValue),
                            CreatedBy = AuthenticationHelper.UserID,
                            UpdatedBy = AuthenticationHelper.UserID,                            
                        };

                        if (ddlCPDepartment.SelectedValue != "" && ddlCPDepartment.SelectedValue != "-1")
                            contractRecord.ContactPersonOfDepartment = Convert.ToInt32(ddlCPDepartment.SelectedValue);
                        
                        if (!string.IsNullOrEmpty(txtProposalDate.Text))
                            contractRecord.ProposalDate = DateTimeExtensions.GetDate(txtProposalDate.Text);

                        if (!string.IsNullOrEmpty(txtAgreementDate.Text))
                            contractRecord.AgreementDate = DateTimeExtensions.GetDate(txtAgreementDate.Text);

                        if (!string.IsNullOrEmpty(txtReviewDate.Text))
                            contractRecord.ReviewDate = DateTimeExtensions.GetDate(txtReviewDate.Text);

                        if (!string.IsNullOrEmpty(txtEffectiveDate.Text))
                            contractRecord.EffectiveDate = DateTimeExtensions.GetDate(txtEffectiveDate.Text);

                        if (!string.IsNullOrEmpty(txtExpirationDate.Text))
                            contractRecord.ExpirationDate = DateTimeExtensions.GetDate(txtExpirationDate.Text);

                        if (!string.IsNullOrEmpty(txtlockinperiodDate.Text))
                            contractRecord.LockInPeriodDate = DateTimeExtensions.GetDate(txtlockinperiodDate.Text);

                        if (!string.IsNullOrEmpty(tbxTaxes.Text))
                            contractRecord.Taxes = tbxTaxes.Text;

                        if (ddlContractSubType.SelectedValue != "" && ddlContractSubType.SelectedValue != "-1")
                            contractRecord.ContractSubTypeID = Convert.ToInt64(ddlContractSubType.SelectedValue);

                        if (ddlPaymentType.SelectedValue != "" && ddlPaymentType.SelectedValue != "-1")
                            contractRecord.PaymentType = Convert.ToInt32(ddlPaymentType.SelectedValue);

                        if (ddlCPDepartment.SelectedValue != "" && ddlCPDepartment.SelectedValue != "-1")
                            contractRecord.ContactPersonOfDepartment = Convert.ToInt32(ddlCPDepartment.SelectedValue);

                        if (!string.IsNullOrEmpty(txtNoticeTerm.Text))
                        {
                            contractRecord.NoticeTermNumber = Convert.ToInt32(txtNoticeTerm.Text.Trim());

                            if (ddlNoticeTerm.SelectedValue != "" && ddlNoticeTerm.SelectedValue != "-1")
                                contractRecord.NoticeTermType = Convert.ToInt32(ddlNoticeTerm.SelectedValue);
                        }

                        if (!string.IsNullOrEmpty(tbxContractAmt.Text))
                            contractRecord.ContractAmt = Convert.ToDecimal(tbxContractAmt.Text);
                        else
                            contractRecord.ContractAmt = 0;
                        
                        if (!string.IsNullOrEmpty(txtBoxProduct.Text))
                            contractRecord.ProductItems = Convert.ToString(txtBoxProduct.Text.Trim());

                        if (!string.IsNullOrEmpty(tbxAddNewClause.Text))
                            contractRecord.AddNewClause = Convert.ToString(tbxAddNewClause.Text.Trim());

                        if ((int)ViewState["Mode"] == 0)
                        {
                            #region ADD New Contract
                            //bool IsPerformercontract = ContractManagement.getcontractchecklist(customerID);
                            if (true)
                                contractRecord.IsReviewedContract = 0;
                            else
                                contractRecord.IsReviewedContract = 1;

                            if (drdTemplateID.SelectedValue != null && drdTemplateID.SelectedValue != "" && drdTemplateID.SelectedValue != "-1")
                            {
                                int tempID = Convert.ToInt32(drdTemplateID.SelectedValue);
                                contractRecord.TemplateID = tempID;

                                contractRecord.ApproverCount = ContractManagement.getApproverCount(customerID, tempID);
                            }
                            bool existCaseNo = ContractManagement.ExistsContractNo(customerID, contractRecord.ContractNo, 0);
                            if (!existCaseNo)
                            {
                                //if (!ContractManagement.ExistsContractTitle(customerID, contractRecord.ContractTitle, 0))
                                if (1 == 1)
                                {
                                    contractRecord.Version = "1.0";
                                    newContractID = ContractManagement.CreateContract(contractRecord);

                                    if (newContractID > 0)
                                    {
                                        ContractManagement.CreateAuditLog("C", newContractID, "Cont_tbl_ContractInstance", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "New Contract Created", true, Convert.ToInt32(newContractID));
                                        saveSuccess = true;
                                    }
                                }
                                else
                                {
                                    cvContractPopUp.IsValid = false;
                                    cvContractPopUp.ErrorMessage = "Contract with Same Title already Exists.";
                                    cvContractPopUp.CssClass = "alert alert-danger";
                                    return;
                                }
                            }
                            else
                            {
                                cvContractPopUp.IsValid = false;
                                cvContractPopUp.ErrorMessage = "Contract with same Contract Number already exist.";
                                cvContractPopUp.CssClass = "alert alert-danger";
                            }

                            if (saveSuccess)
                            {
                                #region Contract Status Transaction - Draft

                                Cont_tbl_ContractStatusTransaction newStatusRecord = new Cont_tbl_ContractStatusTransaction()
                                {
                                    CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID),
                                    ContractID = newContractID,
                                    StatusID = Convert.ToInt64(ddlContractStatus.SelectedValue),
                                    StatusChangeOn = DateTime.Now,
                                    IsActive = true,
                                    CreatedBy = AuthenticationHelper.UserID,
                                    CreatedOn = DateTime.Now,
                                    UpdatedBy = AuthenticationHelper.UserID,
                                    UpdatedOn = DateTime.Now,
                                };

                                saveSuccess = ContractManagement.CreateContractStatusTransaction(newStatusRecord);

                                Cont_tbl_ContractTransaction objtran = new Cont_tbl_ContractTransaction();
                                objtran.CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                                objtran.ContractID = newContractID;
                                objtran.StatusID = 1;
                                objtran.StatusChangeOn = DateTime.Now;
                                objtran.IsActive = true;
                                objtran.CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID);
                                objtran.CreatedOn = DateTime.Now;
                                objtran.UpdatedBy = Convert.ToInt32(AuthenticationHelper.UserID);
                                objtran.UpdatedOn = DateTime.Now;
                                objtran.Remark = tbxAddNewClause.Text;
                                DocumentManagement.CreateContractTrans(objtran);

                                #endregion

                                #region Vendor Mapping

                                if (lstBoxVendor.Items.Count > 0)
                                {
                                    foreach (ListItem eachVendor in lstBoxVendor.Items)
                                    {
                                        if (eachVendor.Selected)
                                        {
                                            if (Convert.ToInt64(eachVendor.Value) != 0)
                                                lstVendorMapping.Add(Convert.ToInt64(eachVendor.Value));
                                        }
                                    }
                                }

                                if (lstVendorMapping.Count > 0)
                                {
                                    List<Cont_tbl_VendorMapping> lstVendorMapping_ToSave = new List<Cont_tbl_VendorMapping>();

                                    lstVendorMapping.ForEach(EachVendor =>
                                    {
                                        Cont_tbl_VendorMapping _vendorMappingRecord = new Cont_tbl_VendorMapping()
                                        {
                                            ContractID = newContractID,
                                            VendorID = EachVendor,
                                            IsActive = true,
                                            CreatedBy = AuthenticationHelper.UserID,
                                            CreatedOn = DateTime.Now,
                                        };

                                        lstVendorMapping_ToSave.Add(_vendorMappingRecord);
                                    });

                                    saveSuccess = VendorDetails.CreateUpdate_VendorMapping(lstVendorMapping_ToSave);
                                    if (saveSuccess)
                                    {
                                        ContractManagement.CreateAuditLog("C", newContractID, "Cont_tbl_VendorMapping", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Contract Vendor Mapping Created", true, Convert.ToInt32(newContractID));
                                    }

                                    //Refresh List
                                    lstVendorMapping_ToSave.Clear();
                                    lstVendorMapping_ToSave = null;

                                    lstVendorMapping.Clear();
                                    lstVendorMapping = null;
                                }
                                #endregion

                                #region Payment term Mapping
                                List<long> lstPaymenttermMapping = new List<long>();
                                if (ddlPaymentTerm1.Items.Count > 0)
                                {
                                    foreach (ListItem eachVendor in ddlPaymentTerm1.Items)
                                    {
                                        if (eachVendor.Selected)
                                        {
                                            if (Convert.ToInt64(eachVendor.Value) != -1)
                                                lstPaymenttermMapping.Add(Convert.ToInt64(eachVendor.Value));
                                        }
                                    }
                                }

                                if (lstPaymenttermMapping.Count > 0)
                                {
                                    List<Cont_tbl_PaymentTermMapping> lstPaymenttermMapping_ToSave = new List<Cont_tbl_PaymentTermMapping>();

                                    lstPaymenttermMapping.ForEach(EachPaymentterm =>
                                    {
                                        Cont_tbl_PaymentTermMapping paymenttermMappingRecord = new Cont_tbl_PaymentTermMapping()
                                        {
                                            ContractID = newContractID,
                                            PaymentTermID = EachPaymentterm,
                                            IsDeleted = false,
                                            CustomerID = Convert.ToInt64(AuthenticationHelper.CustomerID),
                                        };

                                        lstPaymenttermMapping_ToSave.Add(paymenttermMappingRecord);
                                    });

                                    saveSuccess = ContractManagement.CreateUpdate_PaymentTermMapping(lstPaymenttermMapping_ToSave);
                                    if (saveSuccess)
                                    {
                                        ContractManagement.CreateAuditLog("C", newContractID, "Cont_tbl_PaymentTermMapping", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Contract Payment Term Mapping Created", true, Convert.ToInt32(newContractID));
                                    }

                                    //Refresh List
                                    lstPaymenttermMapping_ToSave.Clear();
                                    lstPaymenttermMapping_ToSave = null;

                                    lstPaymenttermMapping.Clear();
                                    lstPaymenttermMapping = null;
                                }
                                #endregion

                                #region Save Custom Field

                                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["CustomefieldCount"])))
                                {
                                    List<Cont_tbl_CustomFieldValue> lstObjParameters = new List<Cont_tbl_CustomFieldValue>();

                                    //Select Which Grid to Loop based on Selected Category/Type
                                    GridView gridViewToCollectData = null;
                                    gridViewToCollectData = grdCustomField;

                                    if (gridViewToCollectData != null)
                                    {
                                        for (int i = 0; i < gridViewToCollectData.Rows.Count; i++)
                                        {
                                            Label lblID = (Label)gridViewToCollectData.Rows[i].FindControl("lblID");
                                            TextBox tbxLabelValue = (TextBox)gridViewToCollectData.Rows[i].FindControl("tbxLabelValue");

                                            if (lblID != null && tbxLabelValue != null)
                                            {
                                                if (!string.IsNullOrEmpty(lblID.Text) || lblID.Text != "0")
                                                {
                                                    Cont_tbl_CustomFieldValue ObjParameter = new Cont_tbl_CustomFieldValue()
                                                    {
                                                        ContractID = newContractID,

                                                        LabelID = Convert.ToInt64(lblID.Text),
                                                        LabelValue = tbxLabelValue.Text,

                                                        IsDeleted = false,
                                                        CreatedBy = AuthenticationHelper.UserID,
                                                        CreatedOn = DateTime.Now,
                                                        UpdatedBy = AuthenticationHelper.UserID,
                                                        UpdatedOn = DateTime.Now
                                                    };
                                                    lstObjParameters.Add(ObjParameter);
                                                    //saveSuccess = ContractManagement.CreateUpdateCustomsField(ObjParameter);
                                                }
                                            }
                                        }//End For Each

                                        saveSuccess = ContractManagement.CreateUpdate_CustomsFields(lstObjParameters);

                                        if (saveSuccess)
                                        {
                                            ContractManagement.CreateAuditLog("C", newContractID, "Cont_tbl_CustomFieldValue", "Update", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Contract Custom Parameter(s) Updated", true, Convert.ToInt32(newContractID));
                                        }
                                    }
                                }

                                #endregion

                                #region User Assignment                           

                                //De-Active All Previous Assignment
                                ContractManagement.DeActiveContractUserAssignments(newContractID, AuthenticationHelper.UserID);
                                List<Cont_tbl_UserAssignment> lstUserAssignmentRecord_ToSave = new List<Cont_tbl_UserAssignment>();
                                List<string> SelectedOwnersList = new List<string>();
                                int assignedRoleID = 3;
                                if (lstBoxOwner.Items.Count > 0)
                                {
                                    SelectedOwnersList.Clear();
                                    assignedRoleID = 3;
                                    foreach (ListItem eachOwner in lstBoxOwner.Items)
                                    {
                                        if (eachOwner.Selected)
                                        {
                                            if (Convert.ToInt32(eachOwner.Value) != 0)
                                            {
                                                SelectedOwnersList.Add(eachOwner.Text);
                                                lstContractUserMapping.Add(new Tuple<int, int>(Convert.ToInt32(eachOwner.Value), assignedRoleID));
                                            }
                                            //lstOwnerMapping.Add(Convert.ToInt32(eachOwner.Value));
                                        }
                                    }
                                }

                                if (lstBoxApprover.Items.Count > 0)
                                {
                                    assignedRoleID = 6;
                                    foreach (ListItem eachApprover in lstBoxApprover.Items)
                                    {
                                        if (eachApprover.Selected)
                                        {
                                            if (Convert.ToInt32(eachApprover.Value) != 0)
                                                lstContractUserMapping.Add(new Tuple<int, int>(Convert.ToInt32(eachApprover.Value), assignedRoleID));
                                            // lstApproverMapping.Add(Convert.ToInt32(eachApprover.Value));
                                        }
                                    }
                                }

                                if (lstContractUserMapping.Count > 0)
                                {
                                    lstContractUserMapping.ForEach(eachUser =>
                                    {
                                        Cont_tbl_UserAssignment newAssignment = new Cont_tbl_UserAssignment()
                                        {
                                            AssignmentType = 1,
                                            ContractID = newContractID,

                                            UserID = Convert.ToInt32(eachUser.Item1),
                                            RoleID = Convert.ToInt32(eachUser.Item2),

                                            IsActive = true,
                                            CreatedBy = AuthenticationHelper.UserID,
                                            UpdatedBy = AuthenticationHelper.UserID,
                                        };

                                        lstUserAssignmentRecord_ToSave.Add(newAssignment);

                                        //saveSuccess = ContractManagement.CreateUpdateContractUserAssignment(newAssignment);
                                    });
                                }

                                if (lstUserAssignmentRecord_ToSave.Count > 0)
                                    saveSuccess = ContractManagement.CreateUpdate_ContractUserAssignments(lstUserAssignmentRecord_ToSave);

                                if (saveSuccess)
                                {
                                    ContractManagement.CreateAuditLog("C", newContractID, "Cont_tbl_UserAssignment", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Contract Owner(s) Created.(" + String.Join(",", SelectedOwnersList) + ")", true, Convert.ToInt32(newContractID));
                                    //ContractManagement.CreateAuditLog("C", newContractID, "Cont_tbl_UserAssignment", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Contract Owner(s) Created", true);
                                }

                                SelectedOwnersList.Clear();
                                lstUserAssignmentRecord_ToSave.Clear();
                                lstUserAssignmentRecord_ToSave = null;


                                ContractUserAssignments(newContractID);
                                List<Cont_tbl_VisibleToUser> lstUserAssignment_ToSave = new List<Cont_tbl_VisibleToUser>();
                                List<string> SelectedUserList = new List<string>();
                                if (lstBoxUsers.Items.Count > 0)
                                {
                                    SelectedUserList.Clear();
                                    foreach (ListItem eachOwner in lstBoxUsers.Items)
                                    {
                                        if (eachOwner.Selected)
                                        {
                                            if (Convert.ToInt32(eachOwner.Value) != 0)
                                            {
                                                SelectedUserList.Add(eachOwner.Text);

                                                //  lstContractUserMapping.Add(Convert.ToInt32(eachOwner.Value));
                                            }
                                            //lstOwnerMapping.Add(Convert.ToInt32(eachOwner.Value));
                                        }
                                    }
                                }
                                SelectedUserList.Clear();
                                lstUserAssignment_ToSave.Clear();
                                #endregion
                            }

                            if (saveSuccess)
                            {
                                cvContractPopUp.IsValid = false;
                                cvContractPopUp.ErrorMessage = "Contract Created Successfully.";
                                VSContractPopup.CssClass = "alert alert-success";

                                ViewState["Mode"] = 1;
                                ViewState["ContractInstanceID"] = newContractID;

                                toggleTextSaveButton_ContractSummaryTab(false); //false -- Switch to Edit Mode

                                showHideTabs(true);
                                showHideContractSummaryTabTopButtons(true);
                                enableDisableContractSummaryTabControls(false);

                                //Bind Case To Link
                                BindContractListToLink(newContractID);
                            }

                            #region add Contract reviewe
                            if (true)
                            {
                                var DocData = DocumentManagement.GetTempContractDocumentData(Convert.ToInt32(AuthenticationHelper.UserID), Convert.ToInt32(AuthenticationHelper.CustomerID));
                                if (DocData.Count > 0)
                                {
                                    foreach (var item in DocData)
                                    {
                                        ContractDocument objdoc = new ContractDocument();
                                        objdoc.UserID = item.UserID;
                                        objdoc.CustomerID = item.CustomerID;
                                        objdoc.ContractID = newContractID;
                                        objdoc.DocName = item.DocName;
                                        objdoc.DocData = item.DocData;
                                        objdoc.DocPath = item.DocPath;
                                        objdoc.FileSize = item.FileSize;
                                        objdoc.IsfromComment = false;
                                        DocumentManagement.CreateContractDoc(objdoc);
                                    }
                                    DocumentManagement.DeleteAllTempContractDocumentFile(Convert.ToInt64(AuthenticationHelper.UserID), Convert.ToInt64(AuthenticationHelper.CustomerID));
                                }
                            }

                            if (ddlReviewer.SelectedValue != null && ddlReviewer.SelectedValue != "" && ddlReviewer.SelectedValue != "-1")
                            {
                                ContractAssignment objass = new ContractAssignment();
                                objass.ContractInstanceID = newContractID;
                                objass.RoleID = 4;
                                objass.UserID = Convert.ToInt64(ddlReviewer.SelectedValue);
                                objass.IsActive = true;
                                DocumentManagement.CreateContractAssignment(objass);
                            }

                            if (ddlApprover.SelectedValue != null && ddlApprover.SelectedValue != "" && ddlApprover.SelectedValue != "-1")
                            {
                                ContractAssignment objass = new ContractAssignment();
                                objass.ContractInstanceID = newContractID;
                                objass.RoleID = 6;
                                objass.UserID = Convert.ToInt64(ddlApprover.SelectedValue);
                                objass.IsActive = true;
                                DocumentManagement.CreateContractAssignment(objass);
                            }

                            //btnsavedraft.Visible = false;
                            #endregion
                            #endregion
                        }
                        else
                        {
                            #region Edit Contract

                            newContractID = Convert.ToInt64(ViewState["ContractInstanceID"]);

                            if (newContractID != 0)
                            {
                                contractRecord.ID = newContractID;

                                bool existContractNo = ContractManagement.ExistsContractNo(customerID, contractRecord.ContractNo, newContractID);
                                if (!existContractNo)
                                {
                                    if (1 == 1)
                                    {
                                        saveSuccess = ContractManagement.UpdateContract(contractRecord);
                                        if (saveSuccess)
                                        {
                                            ContractManagement.CreateAuditLog("C", newContractID, "Cont_tbl_ContractInstance", "Update", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Contract Detail(s) Updated", true, Convert.ToInt32(newContractID));
                                        }
                                    }
                                    else
                                    {
                                        cvContractPopUp.IsValid = false;
                                        cvContractPopUp.ErrorMessage = "Contract with Same Title already Exists.";
                                        cvContractPopUp.CssClass = "alert alert-danger";
                                        return;
                                    }
                                }
                                else
                                {
                                    cvContractPopUp.IsValid = false;
                                    cvContractPopUp.ErrorMessage = "Contract with Same Contract Number already exists";
                                    cvContractPopUp.CssClass = "alert alert-danger";
                                }
                            }

                            #endregion
                            if (saveSuccess)
                            {
                                bool matchSuccess = false;

                                #region Payment term Mapping
                                List<long> lstPaymenttermMapping = new List<long>();
                                if (ddlPaymentTerm1.Items.Count > 0)
                                {
                                    foreach (ListItem eachVendor in ddlPaymentTerm1.Items)
                                    {
                                        if (eachVendor.Selected)
                                        {
                                            if (Convert.ToInt64(eachVendor.Value) != -1)
                                                lstPaymenttermMapping.Add(Convert.ToInt64(eachVendor.Value));
                                        }
                                    }
                                }

                                if (lstPaymenttermMapping.Count > 0)
                                {
                                    List<Cont_tbl_PaymentTermMapping> lstPaymenttermMapping_ToSave = new List<Cont_tbl_PaymentTermMapping>();

                                    lstPaymenttermMapping.ForEach(EachPaymentterm =>
                                    {
                                        Cont_tbl_PaymentTermMapping paymenttermMappingRecord = new Cont_tbl_PaymentTermMapping()
                                        {
                                            ContractID = newContractID,
                                            PaymentTermID = EachPaymentterm,
                                            IsDeleted = false,
                                            CustomerID = Convert.ToInt64(AuthenticationHelper.CustomerID),
                                        };

                                        lstPaymenttermMapping_ToSave.Add(paymenttermMappingRecord);
                                    });

                                    ContractManagement.DeActiveExistingPaymentTermMapping(newContractID);

                                    saveSuccess = ContractManagement.CreateUpdate_PaymentTermMapping(lstPaymenttermMapping_ToSave);
                                    if (saveSuccess)
                                    {
                                        ContractManagement.CreateAuditLog("C", newContractID, "Cont_tbl_PaymentTermMapping", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Contract Payment Term Mapping Created", true, Convert.ToInt32(newContractID));
                                    }

                                    //Refresh List
                                    lstPaymenttermMapping_ToSave.Clear();
                                    lstPaymenttermMapping_ToSave = null;

                                    lstPaymenttermMapping.Clear();
                                    lstPaymenttermMapping = null;
                                }
                                #endregion

                                #region Vendor Mapping

                                if (lstBoxVendor.Items.Count > 0)
                                {
                                    foreach (ListItem eachVendor in lstBoxVendor.Items)
                                    {
                                        if (eachVendor.Selected)
                                        {
                                            if (Convert.ToInt64(eachVendor.Value) != 0)
                                                lstVendorMapping.Add(Convert.ToInt64(eachVendor.Value));
                                        }
                                    }
                                }

                                if (lstVendorMapping.Count > 0)
                                {
                                    List<Cont_tbl_VendorMapping> lstVendorMapping_ToSave = new List<Cont_tbl_VendorMapping>();

                                    lstVendorMapping.ForEach(EachVendor =>
                                    {
                                        Cont_tbl_VendorMapping _vendorMappingRecord = new Cont_tbl_VendorMapping()
                                        {
                                            ContractID = newContractID,
                                            VendorID = EachVendor,
                                            IsActive = true,
                                            CreatedBy = AuthenticationHelper.UserID,
                                            CreatedOn = DateTime.Now,
                                        };

                                        lstVendorMapping_ToSave.Add(_vendorMappingRecord);
                                    });

                                    var existingVendorMapping = VendorDetails.GetVendorMapping(newContractID);
                                    var assignedVendorIDs = lstVendorMapping_ToSave.Select(row => row.VendorID).ToList();

                                    if (existingVendorMapping.Count != assignedVendorIDs.Count)
                                    {
                                        matchSuccess = false;
                                    }
                                    else
                                    {
                                        matchSuccess = existingVendorMapping.Except(assignedVendorIDs).ToList().Count > 0 ? false : true;
                                    }

                                    if (!matchSuccess)
                                    {
                                        VendorDetails.DeActiveExistingVendorMapping(newContractID);

                                        saveSuccess = VendorDetails.CreateUpdate_VendorMapping(lstVendorMapping_ToSave);
                                        if (saveSuccess)
                                        {
                                            ContractManagement.CreateAuditLog("C", newContractID, "Cont_tbl_VendorMapping", "Update", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Contract Vendor Updated", true, Convert.ToInt32(newContractID));
                                        }
                                    }
                                    else
                                        saveSuccess = false;

                                    //Refresh List
                                    lstVendorMapping_ToSave.Clear();
                                    lstVendorMapping_ToSave = null;

                                    lstVendorMapping.Clear();
                                    lstVendorMapping = null;
                                }

                                #endregion

                                #region Update Custom Field                                        

                                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["CustomefieldCount"])))
                                {
                                    List<Cont_tbl_CustomFieldValue> lstObjParameters = new List<Cont_tbl_CustomFieldValue>();

                                    //Select Which Grid to Loop based on Selected Category/Type
                                    GridView gridViewToCollectData = null;
                                    gridViewToCollectData = grdCustomField;

                                    if (gridViewToCollectData != null)
                                    {
                                        for (int i = 0; i < gridViewToCollectData.Rows.Count; i++)
                                        {
                                            Label lblID = (Label)gridViewToCollectData.Rows[i].FindControl("lblID");
                                            TextBox tbxLabelValue = (TextBox)gridViewToCollectData.Rows[i].FindControl("tbxLabelValue");

                                            if (lblID != null && tbxLabelValue != null)
                                            {
                                                if (!string.IsNullOrEmpty(lblID.Text) || lblID.Text != "0")
                                                {
                                                    Cont_tbl_CustomFieldValue ObjParameter = new Cont_tbl_CustomFieldValue()
                                                    {
                                                        ContractID = newContractID,

                                                        LabelID = Convert.ToInt64(lblID.Text),
                                                        LabelValue = tbxLabelValue.Text,

                                                        IsDeleted = false,
                                                        CreatedBy = AuthenticationHelper.UserID,
                                                        CreatedOn = DateTime.Now,
                                                        UpdatedBy = AuthenticationHelper.UserID,
                                                        UpdatedOn = DateTime.Now
                                                    };

                                                    lstObjParameters.Add(ObjParameter);

                                                    //saveSuccess = ContractManagement.CreateUpdateCustomsField(ObjParameter);
                                                }
                                            }
                                        }//End For Each

                                        var existingCustomFields = ContractManagement.GetCustomsFieldsContractWise_All(Convert.ToInt32(AuthenticationHelper.CustomerID), newContractID, Convert.ToInt64(ddlContractType.SelectedValue));

                                        var lstSavedCustomFields = existingCustomFields.Select(row => row.LableID).ToList();
                                        var assignedCustomFields = lstObjParameters.Select(row => row.LabelID).ToList();

                                        matchSuccess = false;

                                        if (lstSavedCustomFields.Count != assignedCustomFields.Count)
                                        {
                                            matchSuccess = false;
                                        }
                                        else
                                        {
                                            matchSuccess = lstSavedCustomFields.Except(assignedCustomFields).ToList().Count > 0 ? false : true;
                                        }

                                        if (!matchSuccess)
                                        {
                                            saveSuccess = ContractManagement.DeActiveExistingCustomsFieldsByContractID(newContractID);
                                            saveSuccess = ContractManagement.CreateUpdate_CustomsFields(lstObjParameters);
                                        }
                                        else
                                            saveSuccess = false;

                                        if (saveSuccess)
                                        {
                                            ContractManagement.CreateAuditLog("C", newContractID, "Cont_tbl_CustomFieldValue", "Update", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Contract Custom Parameter(s) Updated", true, Convert.ToInt32(newContractID));
                                        }
                                    }
                                }

                                #endregion

                                #region User Assignment                           

                                List<Cont_tbl_UserAssignment> lstUserAssignmentRecord_ToSave = new List<Cont_tbl_UserAssignment>();
                                lstContractUserMapping.Clear();

                                List<string> SelectedOwnersList = new List<string>();
                                int assignedRoleID = 3;
                                if (lstBoxOwner.Items.Count > 0)
                                {
                                    SelectedOwnersList.Clear();
                                    assignedRoleID = 3;
                                    foreach (ListItem eachOwner in lstBoxOwner.Items)
                                    {
                                        if (eachOwner.Selected)
                                        {
                                            if (Convert.ToInt32(eachOwner.Value) != 0)
                                            {
                                                SelectedOwnersList.Add(eachOwner.Text);
                                                lstContractUserMapping.Add(new Tuple<int, int>(Convert.ToInt32(eachOwner.Value), assignedRoleID));
                                            }
                                            //lstOwnerMapping.Add(Convert.ToInt32(eachOwner.Value));
                                        }
                                    }
                                }

                                if (lstBoxApprover.Items.Count > 0)
                                {
                                    assignedRoleID = 6;
                                    foreach (ListItem eachApprover in lstBoxApprover.Items)
                                    {
                                        if (eachApprover.Selected)
                                        {
                                            if (Convert.ToInt32(eachApprover.Value) != 0)
                                                lstContractUserMapping.Add(new Tuple<int, int>(Convert.ToInt32(eachApprover.Value), assignedRoleID));
                                            // lstApproverMapping.Add(Convert.ToInt32(eachApprover.Value));
                                        }
                                    }
                                }

                                if (lstContractUserMapping.Count > 0)
                                {
                                    lstContractUserMapping.ForEach(eachUser =>
                                    {
                                        Cont_tbl_UserAssignment newAssignment = new Cont_tbl_UserAssignment()
                                        {
                                            AssignmentType = 1,
                                            ContractID = newContractID,

                                            UserID = Convert.ToInt32(eachUser.Item1),
                                            RoleID = Convert.ToInt32(eachUser.Item2),

                                            IsActive = true,
                                            CreatedBy = AuthenticationHelper.UserID,
                                            UpdatedBy = AuthenticationHelper.UserID,
                                        };

                                        lstUserAssignmentRecord_ToSave.Add(newAssignment);

                                        //saveSuccess = ContractManagement.CreateUpdateContractUserAssignment(newAssignment);
                                    });
                                }

                                var existingUserAssignment = ContractManagement.GetContractUserAssignment_All(newContractID);

                                var assignedUserIDs = existingUserAssignment.Select(row => row.UserID).ToList();
                                var currentUserIDs = lstUserAssignmentRecord_ToSave.Select(row => row.UserID).ToList();

                                matchSuccess = false;

                                if (assignedUserIDs.Count != currentUserIDs.Count)
                                {
                                    matchSuccess = false;
                                }
                                else
                                {
                                    matchSuccess = assignedUserIDs.Except(currentUserIDs).ToList().Count > 0 ? false : true;
                                }

                                if (!matchSuccess)
                                {
                                    //De-Active All Previous Assignment
                                    ContractManagement.DeActiveContractUserAssignments(newContractID, AuthenticationHelper.UserID);

                                    if (lstUserAssignmentRecord_ToSave.Count > 0)
                                        saveSuccess = ContractManagement.CreateUpdate_ContractUserAssignments(lstUserAssignmentRecord_ToSave);

                                    if (saveSuccess)
                                    {
                                        ContractManagement.CreateAuditLog("C", newContractID, "Cont_tbl_UserAssignment", "Update", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Contract Owner(s) Updated.(" + String.Join(",", SelectedOwnersList) + ")", true, Convert.ToInt32(newContractID));
                                     }
                                }
                                else
                                    saveSuccess = true;

                                SelectedOwnersList.Clear();
                                lstUserAssignmentRecord_ToSave.Clear();
                                lstUserAssignmentRecord_ToSave = null;

                                #endregion

                                if (saveSuccess)
                                {
                                    #region update Contract reviewe
                                    //bool IsPerformercontract = ContractManagement.getcontractchecklist(customerID);
                                    if (true)
                                    {
                                        if (true)
                                        {
                                            if (ddlReviewer.SelectedValue != null && ddlReviewer.SelectedValue != "" && ddlReviewer.SelectedValue != "-1")
                                            {
                                                ContractAssignment objass = new ContractAssignment();
                                                objass.ContractInstanceID = newContractID;
                                                objass.RoleID = 4;
                                                objass.UserID = Convert.ToInt64(ddlReviewer.SelectedValue);
                                                objass.IsActive = true;
                                                DocumentManagement.CreateContractAssignment(objass);
                                            }

                                            if (ddlApprover.SelectedValue != null && ddlApprover.SelectedValue != "" && ddlApprover.SelectedValue != "-1")
                                            {
                                                ContractAssignment objass = new ContractAssignment();
                                                objass.ContractInstanceID = newContractID;
                                                objass.RoleID = 6;
                                                objass.UserID = Convert.ToInt64(ddlApprover.SelectedValue);
                                                objass.IsActive = true;
                                                DocumentManagement.CreateContractAssignment(objass);
                                            }
                                        }
                                    }
                                    #endregion
                                }
                            }
                            cvContractPopUp.IsValid = false;
                            cvContractPopUp.ErrorMessage = "Contract Updated Successfully.";
                            VSContractPopup.CssClass = "alert alert-success";
                        }
                        btnsavedraft.Visible = true;

                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvContractPopUp.IsValid = false;
                cvContractPopUp.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void lnkBtnShareDocument_Click(object sender, EventArgs e)
        {
            List<int> fileIds = new List<int>();
            if (grdContractDocuments != null)
            {
                for (int i = 0; i < grdContractDocuments.Rows.Count; i++)
                {
                    if (grdContractDocuments.Rows[i].RowType == DataControlRowType.DataRow)
                    {
                        int FileId = -1;
                        bool flagisallow = false;
                        CheckBox chkRow = (CheckBox)grdContractDocuments.Rows[i].FindControl("chkRow");

                        if (chkRow != null)
                        {
                            if (chkRow.Checked)
                            {
                                Label lblSelectedFileID = (Label)grdContractDocuments.Rows[i].FindControl("lblSelectedFileID");
                                if (lblSelectedFileID != null)
                                {
                                    FileId = Convert.ToInt32(lblSelectedFileID.Text);
                                    fileIds.Add(FileId);
                                }
                            }
                        }
                    }
                }
            }

            if (fileIds.Count > 0)
            {
                long contractID = Convert.ToInt64(ViewState["ContractInstanceID"].ToString());
                var fileID = String.Join(",", fileIds);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "showDocInfo", "OpenDocSharePopup('" + fileID + "','" + contractID + "');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "showDocInfo", "OpenNoDocSharePopup();", true);
            }
            //var contractID = CryptographyManagement.Encrypt(ViewState["ContractInstanceID"].ToString());
            //var FlagID = 1;
            //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenUploadDocumentPopup('" + contractID + "','" + FlagID + "');", true);
        }

        protected void gridMilestone_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            LinkButton lnkBtnResDeleteMileStone = (LinkButton)e.Row.FindControl("lnkBtnResDeleteMileStone");
            LinkButton lnkBtnEditMileStoneDoc = (LinkButton)e.Row.FindControl("lnkBtnEditMileStoneDoc");
            long ContractStatusID = Convert.ToInt64(ViewState["ContractStatusID"]);
            if (ContractStatusID == 8 || ContractStatusID == 23)
            {
                if (lnkBtnResDeleteMileStone != null)
                {
                    lnkBtnResDeleteMileStone.Visible = false;
                }
                if (lnkBtnEditMileStoneDoc != null)
                {
                    lnkBtnEditMileStoneDoc.Visible = false;
                }
            }
        }

        protected void grdTaskActivity_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            LinkButton lnkBtnEditTaskDoc = (LinkButton)e.Row.FindControl("lnkBtnEditTaskDoc");
            LinkButton lnkBtnTaskReminder = (LinkButton)e.Row.FindControl("lnkBtnTaskReminder");
            LinkButton lnkBtnResCloseTask = (LinkButton)e.Row.FindControl("lnkBtnResCloseTask");
            LinkButton lnkBtnResDeleteTask = (LinkButton)e.Row.FindControl("lnkBtnResDeleteTask");
            long ContractStatusID = Convert.ToInt64(ViewState["ContractStatusID"]);
            if (ContractStatusID == 8 || ContractStatusID == 23)
            {
                if (lnkBtnEditTaskDoc != null)
                {
                    lnkBtnEditTaskDoc.Visible = false;
                }
                if (lnkBtnTaskReminder != null)
                {
                    lnkBtnTaskReminder.Visible = false;
                }
                if (lnkBtnResCloseTask != null)
                {
                    lnkBtnResCloseTask.Visible = false;
                }
                if (lnkBtnResDeleteTask != null)
                {
                    lnkBtnResDeleteTask.Visible = false;
                }
            }

        }

        protected void lnklinktotermsheet_Click(object sender, EventArgs e)
        {
            string flag = "disableTermSheet";
            string coninstanceid= Request.QueryString["AccessID"];
            string contractno = Getcontractno(coninstanceid);
            string contractInitiatorID = GetcontractInitiatorno(contractno);
            if(contractInitiatorID== "0")
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('No Contract Term sheet linked to the Contract.')", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowDialogTermSheet('" + flag + "'," + contractInitiatorID + ");", true);

            }

        }
        public static string Getcontractno(string contractinstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.Cont_tbl_ContractInstance
                            where row.ID.ToString() == contractinstanceID
                            select row.ContractNo).FirstOrDefault();
                return data.ToString();
            }
        }
        public static string GetcontractNum(int contractIniID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.tbl_Contract_TermSheet
                            where row.ID == contractIniID
                            select row.Contract_no).FirstOrDefault();
                return data.ToString();
            }
        }
        public static string GetcontractInitiatorno(string contractID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.tbl_Contract_TermSheet
                            where row.Contract_no == contractID
                            select row.ID).FirstOrDefault();
                return data.ToString();
            }
        }
        protected void btnApprovalRequest_Click(object sender, EventArgs e)
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.tblContractEditApprovals
                             where row.CreatedBy == UId
                             && row.IsDeleted == false
                             && row.ContractInstanceID == contID
                             select row).FirstOrDefault();

                if (query != null)
                {
                    long contractInstanceID = 0;
                    var ExpiryDate = "";
                    if (!string.IsNullOrEmpty(Convert.ToString(ViewState["ContractInstanceID"])))
                    {
                        contractInstanceID = Convert.ToInt32(ViewState["ContractInstanceID"]);
                        if (contractInstanceID != 0)
                        {
                            var contractRecord = ContractManagement.GetContractDetailsByContractID(customerID, contractInstanceID);
                            User User = UserManagement.GetByID(Convert.ToInt32(query.RequestedTo));
                            User requestedByUser = UserManagement.GetByID(Convert.ToInt32(query.CreatedBy));

                            if (User != null)
                            {
                                if (User.Email != null && User.Email != "")
                                {
                                    string username = string.Format("{0} {1}", User.FirstName, User.LastName);
                                    string requestedByUsername = string.Format("{0} {1}", requestedByUser.FirstName, requestedByUser.LastName);
                                    string portalurl = string.Empty;
                                    URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(AuthenticationHelper.CustomerID));
                                    if (Urloutput != null)
                                    {
                                        portalurl = Urloutput.URL;
                                    }
                                    else
                                    {
                                        portalurl = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                                    }
                                    if (contractRecord.ExpirationDate != null)
                                    {
                                        ExpiryDate = Convert.ToDateTime(contractRecord.ExpirationDate).ToString("dd-MM-yyyy");
                                    }
                                    string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_ContractActiveEditApproval
                                                          .Replace("@User", username)
                                                          .Replace("@ContractTitle", contractRecord.ContractTitle)
                                                          .Replace("@ContractDetailDesc", contractRecord.ContractDetailDesc)
                                                          .Replace("@ExpirationDate", ExpiryDate)
                                                          .Replace("@UserID", requestedByUsername.ToString())
                                                          .Replace("@Remark", query.Remarks)
                                                          .Replace("@From", Convert.ToString(ConfigurationManager.AppSettings["ReplyEmailAddressName"]))
                                                          .Replace("@PortalURL", Convert.ToString(portalurl));


                                    EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(new String[] { User.Email }), null, null, "Contract Notification- Approval to Edit Active Contracts", message);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}