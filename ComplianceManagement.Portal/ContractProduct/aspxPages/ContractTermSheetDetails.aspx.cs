﻿using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.aspxPages
{
    public partial class ContractTermSheetDetails : System.Web.UI.Page
    {
        protected bool flag;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindGridData();
                bindPageNumber();
                ShowGridDetail();
               

            }
        }
        protected void lnkEditContract_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton)(sender);
                if (btn != null)
                {
                    long contractInitiatorID = Convert.ToInt64(btn.CommandArgument);

                    if (contractInitiatorID != 0)
                    {

                        //lblname.InnerText = "Edit Contract Initiator";
                        string flag = "Update";
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowDialog('" + flag + "'," + contractInitiatorID + ");", true);


                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnAddContract_Click(object sender, EventArgs e)
        {
            string Flag = "Save";
            int contractInitiatorID = -1;
            //lblname.InnerText = "Initiate Contract";
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowDialog('" + Flag + "'," + contractInitiatorID + ");", true);
            //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowDialog();", true);
        }

        private void BindGridData()
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int customerID = -1;
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                    int userid = AuthenticationHelper.UserID;
                    var details = (from row in entities.SP_GetContractTermSheetDetails()
                                   where (row.RequestTo == userid || row.CreatedBy == userid)
                                   select row).ToList();

                    grdContractor.DataSource = details;
                    grdContractor.DataBind();


                    flag = true;
                    Session["TotalRows"] = null;
                    if (details.Count > 0)
                    {
                        grdContractor.DataSource = details;
                        Session["TotalRows"] = details.Count;
                        grdContractor.DataBind();
                    }
                    else
                    {
                        grdContractor.DataSource = details;
                        grdContractor.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                
            }

        }


        protected void DeleteEntitiesAssignment(long ID)
        {

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Entitiesdata = (from row in entities.tbl_Contract_TermSheet
                                    where row.ID == ID
                                    select row).SingleOrDefault();

                Entitiesdata.Isdeleted = true;
                entities.SaveChanges();

            }
            cvDuplicateEntry.IsValid = false;
            cvDuplicateEntry.ErrorMessage = "Contract Initiator deleted successfully.";
            vsEntities.CssClass = "alert alert-success";
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideFilterTreeView", "$(\"#divFilterLocation\").hide(\"blind\", null, 5, function () { });", true);

        }

        protected void grdContractor_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int contractInitiatorID = -1;
            contractInitiatorID = Convert.ToInt32(e.CommandArgument);
            ViewState["ConInitiatorId"] = contractInitiatorID;
            if (e.CommandName.Equals("DELETE_ContractInitiator"))
            {
                DeleteEntitiesAssignment(contractInitiatorID);
                BindGridData();
                //BindEntitiesList();
                bindPageNumber();
                ShowGridDetail();
            }
            else if (e.CommandName.Equals("EditContract_Initiator"))
            {
                string Flag = "Update";
                //lblname.InnerText = "Initiate Contract";
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowDialog('" + Flag + "'," + contractInitiatorID + ");", true);
            }
        }


        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
                grdContractor.PageIndex = chkSelectedPage - 1;

                //SelectedPageNo.Text = (chkSelectedPage).ToString();
                grdContractor.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                //grdUser.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                BindGridData();
                ShowGridDetail();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void upComplianceTypeList_Load(object sender, EventArgs e)
        {
            try
            {
                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdContractor.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);

                BindGridData();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdContractor.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
                ShowGridDetail();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                // ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
                ShowGridDetail();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void ShowGridDetail()
        {
            if (!string.IsNullOrEmpty(Convert.ToString(Session["TotalRows"])) && Convert.ToString(Session["TotalRows"]) != "0")
            {
                var PageSize = Convert.ToInt32(ddlPageSize.SelectedItem.Text);
                var PageNumber = Convert.ToInt32(DropDownListPageNo.SelectedValue);
                var EndRecord = 0;
                var TotalRecord = 0;
                var TotalValue = PageSize * PageNumber;

                TotalRecord = Convert.ToInt32(Session["TotalRows"]);
                if (TotalRecord < TotalValue)
                {
                    EndRecord = TotalRecord;
                }
                else
                {
                    EndRecord = TotalValue;
                }
                lblStartRecord.Text = Convert.ToString(PageSize * PageNumber - PageSize + 1);
                lblEndRecord.Text = Convert.ToString(EndRecord) + " ";
                lblTotalRecord.Text = TotalRecord.ToString();
            }
            else
            {
                lblStartRecord.Text = "0 ";
                lblEndRecord.Text = "0 ";
                lblTotalRecord.Text = "0";
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = "0";
                if (!string.IsNullOrEmpty(Convert.ToString(Session["TotalRows"])))
                {
                    TotalRows.Value = Convert.ToString(Session["TotalRows"]);
                }

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        protected void grdContractor_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //if (e.Row.RowType == DataControlRowType.DataRow)
            //{
                LinkButton lnkBtnDeleteTermsheet = (LinkButton)e.Row.FindControl("LinkButton2");


            if (lnkBtnDeleteTermsheet != null)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                 scriptManager.RegisterPostBackControl(lnkBtnDeleteTermsheet);
                 string rolecode = AuthenticationHelper.Role;
                 List<int?> roles = ContractManagement.GetAssigned_Contractid(AuthenticationHelper.UserID);
                
                if (roles.Contains(7))
                {
                        lnkBtnDeleteTermsheet.Visible = false;
                }
                else
                {
                    lnkBtnDeleteTermsheet.Visible = true;
                }
                //}
            }
        }
    }
}