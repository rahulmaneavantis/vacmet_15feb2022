﻿using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.aspxPages
{
    public partial class ContractMilestoneListNew : System.Web.UI.Page
    {
        protected Cont_tbl_PageAuthorizationMaster authpage;
        protected int pageid = 5;
        private long CutomerID = AuthenticationHelper.CustomerID;
        protected bool flag;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                flag = false;
                BindContractdrp();
                BindMilestonedrp();
                BindContractMilestone();
                bindPageNumber();
              
            }
        }
        public void BindMilestonedrp()
        {
            long userID = AuthenticationHelper.UserID;
            long customerID = AuthenticationHelper.CustomerID;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var getrecord = (from row in entities.Cont_SP_AssignMilestone(customerID, userID)
                                 select row).ToList();

                if (DrpmilestoneContract.SelectedValue != "-1" && DrpmilestoneContract.SelectedValue != "")
                {
                    long ContractD = Convert.ToInt32(DrpmilestoneContract.SelectedValue);
                    getrecord = getrecord.Where(x => x.ContractTemplateID == ContractD).ToList();
                }
                drpdownmilestone.DataValueField = "ID";
                drpdownmilestone.DataTextField = "Title";
                drpdownmilestone.DataSource = getrecord;
                drpdownmilestone.DataBind();
                drpdownmilestone.Items.Insert(0, new ListItem("Select Milestone", "-1"));
                //drpdownmilestone.Items.Add(new ListItem("Select Milestone", "-1"));
            }
        }
        public void BindContractdrp()
        {
            long customerID = AuthenticationHelper.CustomerID;
            long userID = AuthenticationHelper.UserID;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var getrecord = (from row in entities.Cont_SP_AssignContractMilestone(customerID, userID)
                                 select row).ToList();

                DrpmilestoneContract.DataValueField = "ID";
                DrpmilestoneContract.DataTextField = "ContractTitle";
                DrpmilestoneContract.DataSource = getrecord;
                DrpmilestoneContract.DataBind();
                DrpmilestoneContract.Items.Insert(0, new ListItem("Select Contract", "-1"));
            }
        }
        protected void lnkApply_Click(object sender, EventArgs e)
        {
            BindContractMilestone();
            bindPageNumber();
        }
        protected void lnkBtnBindGrid_Click(object sender, EventArgs e)
        {
            try
            {
                BindContractMilestone();
                bindPageNumber();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindContractMilestone()
        {
            try
            {
                long UserID = AuthenticationHelper.UserID;
                var MilestoneList = ContractMastersManagement.GetContractMilestoneDetail(CutomerID, UserID);
                int MilestoneID = -1;
                int MilestoneStatusID = -1;
                int ContractD = -1;
                if (dropmilestonestatus.SelectedValue != "-1" && dropmilestonestatus.SelectedValue != "")
                {
                    MilestoneStatusID = Convert.ToInt32(dropmilestonestatus.SelectedValue);
                    MilestoneList = MilestoneList.Where(x => x.StatusID == MilestoneStatusID).ToList();
                }
                if (DrpmilestoneContract.SelectedValue != "-1" && DrpmilestoneContract.SelectedValue != "")
                {
                    ContractD = Convert.ToInt32(DrpmilestoneContract.SelectedValue);
                    MilestoneList = MilestoneList.Where(x => x.ContractTemplateID == ContractD).ToList();
                }
                
                if (drpdownmilestone.SelectedValue != "-1" && drpdownmilestone.SelectedValue != "")
                {
                    MilestoneID = Convert.ToInt32(drpdownmilestone.SelectedValue);
                    MilestoneList = MilestoneList.Where(x => x.MilestoneMasterID == MilestoneID).ToList();
                }
                string SortExpr = string.Empty;
                string CheckDirection = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(ViewState["Direction"])))
                    {
                        CheckDirection = Convert.ToString(ViewState["Direction"]);

                        SortExpr = Convert.ToString(ViewState["SortExpression"]);
                        if (CheckDirection == "Ascending")
                        {
                            MilestoneList = MilestoneList.OrderBy(entry => entry.GetType().GetProperty(SortExpr).GetValue(entry, null)).ToList();
                        }
                        else
                        {
                            CheckDirection = "Descending";
                            MilestoneList = MilestoneList.OrderByDescending(entry => entry.GetType().GetProperty(SortExpr).GetValue(entry, null)).ToList();
                        }
                    }
                }

                flag = true;
                Session["TotalRows"] = null;
                if (MilestoneList.Count > 0)
                {
                    grdContDoctType.DataSource = MilestoneList;
                    Session["TotalRows"] = MilestoneList.Count;
                    grdContDoctType.DataBind();
                }
                else
                {
                    grdContDoctType.DataSource = MilestoneList;
                    grdContDoctType.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvPageDocType.IsValid = false;
                cvPageDocType.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                grdContDoctType.PageIndex = 0;
                BindContractMilestone();
                bindPageNumber();
                SetShowingRecords();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvPageDocType.IsValid = false;
                cvPageDocType.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void lnkBtn_RebindGrid_Click(object sender, EventArgs e)
        {
            BindContractMilestone(); bindPageNumber();
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdContDoctType.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                BindContractMilestone();
                bindPageNumber();

                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdContDoctType.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }

                SetShowingRecords();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                throw ex;
            }
        }

        private void SetShowingRecords()
        {
            if (Session["TotalRows"] != null)
            {
                int PageSize = 0;
                int PageNumber = 0;

                if (!string.IsNullOrEmpty(ddlPageSize.SelectedItem.Text))
                    PageSize = Convert.ToInt32(ddlPageSize.SelectedItem.Text);

                if (!string.IsNullOrEmpty(DropDownListPageNo.SelectedValue))
                    PageNumber = Convert.ToInt32(DropDownListPageNo.SelectedValue);

                var EndRecord = 0;
                var TotalRecord = 0;
                var TotalValue = PageSize * PageNumber;

                TotalRecord = Convert.ToInt32(Session["TotalRows"]);
                if (TotalRecord < TotalValue)
                {
                    EndRecord = TotalRecord;
                }
                else
                {
                    EndRecord = TotalValue;
                }

                if (TotalRecord != 0)
                    lblStartRecord.Text = Convert.ToString(PageSize * PageNumber - PageSize + 1);
                else
                    lblStartRecord.Text = "0";

                lblEndRecord.Text = Convert.ToString(EndRecord) + " ";
                lblTotalRecord.Text = TotalRecord.ToString();
            }
            else
            {
                lblStartRecord.Text = "0 ";
                lblEndRecord.Text = "0 ";
                lblTotalRecord.Text = "0";
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                long CustomerID = AuthenticationHelper.CustomerID;
                int totalPages = 0;
                if (Session["TotalRows"] != null)
                {
                    TotalRows.Value = Session["TotalRows"].ToString();

                    totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                    // total page item to be displayed
                    int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                    // remaing no of pages
                    if (pageItemRemain > 0)// set total No of pages
                    {
                        totalPages = totalPages + 1;
                    }
                    else
                    {
                        totalPages = totalPages + 0;
                    }
                    // return totalPages;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdContDoctType.PageIndex = chkSelectedPage - 1;
            grdContDoctType.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            BindContractMilestone(); SetShowingRecords();
        }

        protected void btnAddNew_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["Mode"] = 0;
                string ID = null;

                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenDocTypePopup('" + ID + "');", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvPageDocType.IsValid = false;
                cvPageDocType.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void grdContDoctType_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                long CustomerID = AuthenticationHelper.CustomerID;

                if (e.CommandName.Equals("EDIT_Milestone"))
                {
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                    long milestoneID = Convert.ToInt64(commandArgs[0]);
                    long contractID = Convert.ToInt64(commandArgs[1]);

                    //long docTypeID = Convert.ToInt64(e.CommandArgument);
                    //ViewState["Mode"] = 1;
                    //ViewState["ContDocTypeID"] = docTypeID;

                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenDocTypePopup('" + milestoneID + "','" + contractID + "');", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvPageDocType.IsValid = false;
                cvPageDocType.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        //protected void grdContDoctType_Sorting(object sender, GridViewSortEventArgs e)
        //{
        //    try
        //    {
        //        int customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);
        //        var detailView = ContractMastersManagement.GetContractDocumentTypes_Paging(CutomerID, tbxFilter.Text.Trim().ToString());

        //        List<object> dataSource = new List<object>();
        //        foreach (var contInfo in detailView)
        //        {
        //            dataSource.Add(new
        //            {
        //                contInfo.RowID,
        //                contInfo.TypeName,
        //                contInfo.ID,
        //                contInfo.CreatedBy,
        //                contInfo.CreatedOn
        //            });
        //        }

        //        string SortExpr = string.Empty;
        //        if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
        //        {
        //            SortExpr = Convert.ToString(ViewState["SortExpression"]);
        //            if (SortExpr == e.SortExpression)
        //            {
        //                if (direction == SortDirection.Ascending)
        //                {
        //                    direction = SortDirection.Descending;
        //                }
        //                else
        //                {
        //                    direction = SortDirection.Ascending;
        //                }
        //            }
        //            else
        //            {
        //                direction = SortDirection.Ascending;
        //            }
        //        }

        //        if (direction == SortDirection.Ascending)
        //        {
        //            ViewState["Direction"] = "Ascending";
        //            dataSource = dataSource.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
        //        }
        //        else
        //        {
        //            ViewState["Direction"] = "Descending";
        //            dataSource = dataSource.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
        //        }

        //        ViewState["SortExpression"] = e.SortExpression;

        //        foreach (DataControlField field in grdContDoctType.Columns)
        //        {
        //            if (field.SortExpression == e.SortExpression)
        //            {
        //                ViewState["SortIndex"] = grdContDoctType.Columns.IndexOf(field);
        //            }
        //        }
        //        flag = true;
        //        grdContDoctType.DataSource = dataSource;
        //        grdContDoctType.DataBind();
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //}

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void grdContDoctType_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
            {
                System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
                sortImage.ImageAlign = ImageAlign.AbsMiddle;

                if (flag == true)
                {
                    if (direction == SortDirection.Ascending)
                    {
                        sortImage.ImageUrl = "../../Images/down_arrow1.png";
                        sortImage.AlternateText = "Ascending Order";
                    }
                    else
                    {
                        sortImage.ImageUrl = "../../Images/up_arrow1.png";
                        sortImage.AlternateText = "Descending Order";
                    }
                    headerRow.Cells[columnIndex].Controls.Add(sortImage);
                }
            }
        }

        protected void lnkPreviousSwitch_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/ContractProduct/Masters/ContractType.aspx", false);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lnkNextSwitch_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/ContractProduct/Masters/ContCustomFieldMasters.aspx", false);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdContDoctType_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            
        }

        protected void btnapplymilestone_Click(object sender, EventArgs e)
        {
            BindContractMilestone();
            bindPageNumber();
        }

        //protected void DrpmilestoneContract_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    BindMilestonedrp();
        //    BindContractMilestone();
        //    bindPageNumber();
        //}
    }
}