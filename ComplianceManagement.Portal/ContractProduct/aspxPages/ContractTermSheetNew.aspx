﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ContractTermSheetNew.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.aspxPages.ContractTermSheetNew" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
     <!-- Bootstrap CSS -->
    <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />

    <!-- bootstrap theme -->
    <link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />

    <!-- font icon -->
    <link href="~/NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!--external css-->
    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>
    <%--<script type="text/javascript" src="../../Newjs/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery.nicescroll.js"></script>--%>
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
    <script src="../../Newjs/bootstrap-multiselect.js" type="text/javascript"></script>
    <link href="~/NewCSS/litigation_custom_style.css" rel="stylesheet" />
    <%--<script src="../../Newjs/tag-scrolling.js" type="text/javascript"></script>
    <link href="../../NewCSS/tag-scrolling.css" rel="stylesheet" />--%>
    <script type="text/javascript" src="../../Newjs/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery.nicescroll.js"></script>

    <script src="../../Newjs/tag-scrolling.js" type="text/javascript"></script>
    <link href="../../NewCSS/tag-scrolling.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />
 
    <%--  <script type="text/javascript" src="../../Newjs/jquery-1.12.4.min.js"></script>--%>
    <script type="text/javascript" src="../../Newjs/kendo.all.min.js"></script>
    <%--    <script type="text/javascript" src="../../Newjs/jszip.min.js"></script>--%>
       <script src="https://avacdn.azureedge.net/newjs/jquery.mentionable.js"></script>
    <link href="https://avacdn.azureedge.net/newcss/jquery.mentionable.css" rel="stylesheet" />
    <style type="text/css">
        #ShowDatadiv > div {
            clear: both;
        }

        .componentWraper {
            margin: 18px;
            position: relative;
            border: 1.2px solid #bdbdbd;
            border-radius: 12px;
            padding: 20px;
            margin-top: 8px;
        }

            .componentWraper .componentTitle {
                position: absolute;
                top: -10px;
                background: #fff;
                padding: 0 10px;
                font-weight: 600;
                margin-top: -2px;
                font-size: 17px;
                color: #78909c;
            }

        hr {
            margin-top: 4px;
            margin-bottom: 10px;
            border: 0;
            border-top: 1px solid #eee;
        }

        .auditlog h2.panel-title.new-products-title {
            text-transform: uppercase;
            font-weight: bold;
            text-align: center;
            font-size: 20px;
        }

        div#ui-datepicker-div {
            z-index: 100 !important;
        }

    ul.multiselect-container.dropdown-menu
        {
           width: 100%;
           height: 218px;
           /* overflow-y: auto; */
           overflow-x: hidden;
        }
    .glyphicon-remove-circle
        {
           top: 1px;
           display: none;
           font-style: normal;
           font-weight: 400;
           line-height: 1;
           -webkit-font-smoothing: antialiased;
        }

        .multiselect-container>li
        {
        /*width: 100%;*/
          padding: 0;
          margin-right: -112px;
        }

        /*.input-group-btn {
            position: relative;
            white-space: nowrap;
            display: none;
        }*/
        b.caret {
            margin-top: 10px;
        }
        .chosen-container-single .chosen-single div b {
            margin-top: 3px;
            display: block;
            width: 100%;
            height: 100%;
            background: url(WebResource.axd?d=vcanw1OGyEXjITnW_Lw6o1Ipyyl_7xqXXsF4l85T_LM075DfB4Sa1X81JR-ERGJWebwjBZ50cFK6Pp0E393ZYqyZGOYHT_-VfdsqCmyDCfaRmJRJ_t0w8jjfIDakqF4U1oofqFWh029b9t4iZjam4LmYmixIQZtujQLO7ryIDAI1&t=637298087674161687) no-repeat 0px 6px;
        }
        .bootstrap-tagsinput .tag [data-role="remove"]:after {
            content: "";
            padding: 0px 2px;
        }

        .bootstrap-tagsinput {
            /*border: none;
            box-shadow: none;*/
        }

        .form-group {
            margin-bottom: 10px;
        }
        .btn-group
        {
            width:100%
        }
    </style>

    <style>
        .grdTaskContractDocuments .grdTaskContractDocuments_PageIndexChanging
        {
            text-align: right;
        }
        .tag .label {
            font-size: 100%;
        }

        span input[type=checkbox]:checked {
            color: #007aff;
            border: 1px solid;
            border-color: #007aff;
            background: none;
        }

        .label-info, .label-info-selected {
            font-size: 100%;
        }

            .label-info:active, .label-info:focus, .label-info:hover {
                color: #007aff;
                border: 1px solid;
                border-color: #007aff;
                background: 0 0;
            }

        .label-info-selected {
            color: #007aff;
            border: 1px solid;
            border-color: #007aff;
            background: 0 0;
        }

        textarea {
            resize: none;
            font-size: 13px;
            padding: 10px;
            height: 38px;
            min-height: 38px;
            max-height: 150px;
            width: 100%;
            box-sizing: border-box;
            overflow-y: auto;
        }

        button.multiselect.dropdown-toggle.btn.btn-default {
            text-align: left;
            border: 1px solid #c7c7cc;
            width: 100%;
        }
    </style> 
    <script type="text/javascript">


        $(document).ready(function () {
            debugger
            GetCommentDetails();
            
           <%-- $('#TxtComments').mentionable(
                '<% =KendoPath%>/Data/GetContractTSheetUSerDetailbySearch?customerID=<% =customerID%>&ContractId=<% =conInitiid%>', { parameterName: "search" }
                                
                );--%>
            
            $('[data-toggle="popover"]').popover();
            FetchUSerDetail();
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(InitializeRequest);
            prm.add_endRequest(EndRequest);

            
            BindControls();
            binddropdown();
            if ($("#<%=tbxFilterLocation.ClientID %>") != null) {
                $("#<%=tbxFilterLocation.ClientID %>").unbind('click');

                $("#<%=tbxFilterLocation.ClientID %>").click(function () {
                    $("#divFilterLocation").toggle("blind", null, 500, function () { });
                });
            }
            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            });

            $(document).tooltip({ selector: '[data-toggle="tooltip"]' });

            applyCSSDate();

            $("html").getNiceScroll().resize();


            $('#updateProgress').hide();

            $('textarea').on('input', function () {

                if (this.scrollHeight <= 150)
                    $(this).outerHeight(38).outerHeight(this.scrollHeight);

                if (this.scrollHeight >= 150)
                    $(this).outerHeight(150);
            });
        });

        function fopenDocumentPriview(file) {
            $('#DocumentPriview').modal('show');
            $('#docPriview').attr('src', "/docviewernew.aspx?docurl=" + file);
        }
        function fclosedDocumentPriview() {
           
            $('#DocumentPriview').modal('hide');
        }
        function OpenDocviewer(filePath) {
            $('#IFrameDocumentViewer').attr('src', "/docviewernew.aspx?docurl=" + filePath);
            $('#DocumentViewer').modal('show');
        }
        
        $(document).on("click", function (event) {
            if (event.target.id == "") {
                var idvid = $(event.target).closest('div');
                if ($(idvid).attr('id') != undefined && $(idvid).attr('id') != '' && $(idvid).attr('id').indexOf('<%= tvFilterLocation.ClientID %>') > -1) {
                    $("#divFilterLocation").show();
                } else {
                    $("#divFilterLocation").hide();
                }<%-- event.target.id != '<%= tbxFilterLocation.ClientID %>'--%>
            } else if (event.target.id != "" && event.target.id.indexOf('<%= tvFilterLocation.ClientID %>') > -1) {
                $("#divFilterLocation").show();
            } else if (event.target.id != '<%= tbxFilterLocation.ClientID %>') {
                $("#divFilterLocation").hide();
            } else if (event.target.id == '<%= tbxFilterLocation.ClientID %>') {
                $('<%= tbxFilterLocation.ClientID %>').unbind('click');

                $('<%= tbxFilterLocation.ClientID %>').click(function () {
                    $("#divFilterLocation").toggle("blind", null, 500, function () { });
                });
            }
        });
       
        function submitData() {
            debugger
            var SelectedValue = new Array();
            var inps = document.getElementsByName('mentioned_id[]');
            for (var i = 0; i < inps.length; i++) {
                if (inps[i].disabled != true) {
                    var inp = inps[i];
                    SelectedValue.push(inp.value);
                }
            }
            debugger
            if (SelectedValue.length == 0) {

                var data = new FormData();
                var files = $("#file-input").get(0).files;
                if (files.length > 0) {
                    data.append("UploadedImage", files[0]);
                }
                var commet = document.getElementById("TxtComments").value;

                data.append('Uids', SelectedValue);
                data.append('Comments', commet);
                data.append('UserId', <% =userid%>);
                data.append('CustId', <% =customerID%>);
                data.append('ContractID', <% =conInitiid%>);

                $.ajax({
                    url: '<% =KendoPath%>/data/ContractTermSheetCommentWithUploadFile',
                    type: 'post',
                    data: data,
                    contentType: false,
                    processData: false,
                    success: function (response) {
                       /* alert(1);*/
                        GetCommentDetails();
                        $('#TxtComments').val('');
                       <%-- $('#TxtComments').mentionable(
                              '<% =KendoPath%>/Data/GetContractTSheetUSerDetailbySearch?customerID=<% =customerID%>&ContractId=<% =conInitiid%>', { parameterName: "search" }

                          );--%>
                        setTimeout(function () {
                            GetCommentDetails(); // this will run after every 5 seconds
                        }, 1000);
                    },
                });
            }
            //else {
            //    alert('Please Select At least One User');
            //}
        }
        function ViewDiv(fileID,message_id)
        {
            debugger;
            $.ajax({
                type: "GET",
                url: '<% =KendoPath%>/Data/GetContractTermSheetCommentDetail?MsgID='+fileID+'&message_id='+message_id,
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                crossDomain: true,
                success: function (result) 
                {
                    debugger;
                    $('#DocumentPriview').modal('show');
                    $('#docPriview').attr('src', "/docviewernew.aspx?docurl=" + result[0].Message);
                },
                error: function (response) {
                    alert('Document not available');
                }
            });
        }
         function DownloadDiv(fileID,message_id)
        {
            debugger;
            $.ajax({
                type: "GET",
                url: '<% =KendoPath%>/Data/GetContractTermSheetCommentDetail?MsgID='+fileID+'&message_id='+message_id,
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                crossDomain: true,
                success: function (result) 
                {
                    debugger;
                    window.location.href = '<% =KendoPath%>/ExportReport/GetContractTermSheetCommentedFile?userpath=' + result[0].Message + '';                    
                },
                error: function (response) {
                    alert('Document not available');
                }
            });
            return false;
        }

        function showDiv(pageid) {
           
            document.getElementById(pageid + "_div").style.display = "block";
            document.getElementById(pageid + "_txt").value = "";
        }

        function alertselectedFilename(pageid)
        {       
            var thefile = document.getElementById(pageid + "_file");
            var createid="#"+pageid;
            $(createid + '_selectedfile').text(thefile.files.item(0).name);
        }

         function submitDiv(pageid,touserid,userid) {
            var valuetext = document.getElementById(pageid + "_txt").value;
          
            if (valuetext != "") 
            {
                var checktouserid = touserid;

                var checkuserid=<% =userid%>;
                if (touserid == checkuserid) {
                    checktouserid=userid
                }
                var data = new FormData();

                var chkfile = "#"+ pageid + "_file";

                var files = $(chkfile).get(0).files;
                if (files.length > 0) {
                    data.append("UploadedImage", files[0]);
                }
                var commet = document.getElementById("TxtComments").value;
          
                data.append('MsgID', pageid);
                data.append('Comments', valuetext);
                data.append('UserId', <% =userid%>);
                data.append('CustId', <% =customerID%>);
                data.append('ContractID', <% =conInitiid%>);
                data.append('touser_id', checktouserid);
                $.ajax({
                    url: '<% =KendoPath%>/data/PostSubContractTSheetCommentdata',
                    type: 'post',
                    data: data,
                    contentType: false,
                    processData: false,
                    success: function(response){
                        GetCommentDetails();
                        $('#TxtComments').val('');
                        setTimeout(function () {
                            GetCommentDetails() // this will run after every 5 seconds
                        }, 1000);
                    },
                });
            }
            else {
            }

        }
        function CloseDiv(pageid) {

            document.getElementById(pageid + "_div").style.display = "none";
        }
         function GetCommentDetails() {

            $.ajax({
                type: "GET",
                url: '<% =KendoPath%>/data/GetContractTermSheetCommentWithUploadFile?ContractId=<% =conInitiid%>&CustomerID=<% =customerID%>&userID=<% =userid%>',
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                crossDomain: true,
                success: function (result) {
                  
                    var MainCommnetId = new Array();
                    for (var i = 0; i < result.length; i++) {
                        if (result[i].message_id == 0) {
                            MainCommnetId.push([result[i].contractID, result[i].comment, result[i].user_id, result[i].created_at, result[i]._id, result[i].user_name,result[i].isfile,result[i].filepath,result[i].touser_id,result[i].message_id]);
                        }
                    }

                    var customers = new Array();
                    for (var i = 0; i < MainCommnetId.length; i++) 
                    {
                        if (MainCommnetId[i][6] == 1) 
                        {
                            customers.push('<div style="font-family: Roboto;max-width:90%;padding: 4px 10px 4px 5px;border: 1px solid #e4d8d8;width: auto;min-width: 234px;border-radius: 6px;margin-top: 12px;">' + MainCommnetId[i][1] + '</div></br > <div style="float: left;font-size: 12px;margin-top:-1%">' + MainCommnetId[i][5] + ',' + new Date(new Date(MainCommnetId[i][3])).toLocaleString()
                                                    + '<a style="font-size: 12px;margin-left: 8px;color:blue;cursor: pointer;" onclick=showDiv("' + MainCommnetId[i][4] + '");>Comments</a><a style="font-size: 12px;margin-left: 8px;color:blue;cursor: pointer;" onclick=ViewDiv("' + MainCommnetId[i][4] + '","' + MainCommnetId[i][9] + '");>View</a><a style="font-size: 12px;margin-left: 8px;color:blue;cursor: pointer;" onclick=DownloadDiv("' + MainCommnetId[i][4] + '","' + MainCommnetId[i][9] + '");>Download</a></div><div style="display:none;" id=' + MainCommnetId[i][4] + '_div></br><textarea type="text" style="font-family: Roboto;max-width: 45%;padding: 4px 10px 4px 5px;border: 1px solid #e4d8d8;width: auto;min-width: 60%;border-radius: 6px;min-height: 66px;" name=txtid id=' + MainCommnetId[i][4] + '_txt></textarea><label for=' + MainCommnetId[i][4] + '_file style="margin-left: 6px;"><a style="cursor: pointer; font-size: x-large;"><i class="fa fa-paperclip" data-toggle="tooltip" title="Attach a file to the comment"></i></a></label><input id=' + MainCommnetId[i][4] + '_file type="file" onchange=alertselectedFilename("' + MainCommnetId[i][4] + '") style="padding-top: 10px;display:none;" /> <label id=' + MainCommnetId[i][4] + '_selectedfile style="color: black;margin-left: 32px;"></label></br>&nbsp;&nbsp;&nbsp;<a style="cursor: pointer;color: blue;" onclick=submitDiv("' + MainCommnetId[i][4] + '","' + MainCommnetId[i][8] + '","' + MainCommnetId[i][2] + '");>submit</a>&nbsp;&nbsp;&nbsp;<a style="cursor: pointer;color: blue;" onclick=CloseDiv("' + MainCommnetId[i][4] + '");>Closed</a></div></br>');                        
                        }
                        else
                        {
                            customers.push('<div style="font-family: Roboto;max-width:90%;padding: 4px 10px 4px 5px;border: 1px solid #e4d8d8;width: auto;min-width: 234px;border-radius: 6px;margin-top: 12px;">' + MainCommnetId[i][1] + '</div></br > <div style="float: left;font-size: 12px;margin-top:-1%">' + MainCommnetId[i][5] + ',' + new Date(new Date(MainCommnetId[i][3])).toLocaleString()
                                + '<a style="font-size: 12px;margin-left: 8px;color:blue;cursor: pointer;" onclick=showDiv("' + MainCommnetId[i][4] + '");>Comments</a></div><div style="display:none;" id=' + MainCommnetId[i][4] + '_div></br><textarea type="text" style="font-family: Roboto;max-width: 45%;padding: 4px 10px 4px 5px;border: 1px solid #e4d8d8;width: auto;min-width: 60%;border-radius: 6px;min-height: 66px;" name=txtid id=' + MainCommnetId[i][4] + '_txt></textarea><label for=' + MainCommnetId[i][4] + '_file style="margin-left: 6px;"><a style="cursor: pointer; font-size: x-large;"><i class="fa fa-paperclip" data-toggle="tooltip" title="Attach a file to the comment"></i></a></label><input id=' + MainCommnetId[i][4] + '_file type="file" onchange=alertselectedFilename("' + MainCommnetId[i][4] + '") style="padding-top: 10px;display:none;" /> <label id=' + MainCommnetId[i][4] + '_selectedfile style="color: black;margin-left: 32px;"></label></br>&nbsp;&nbsp;&nbsp;<a style="cursor: pointer;color: blue;" onclick=submitDiv("' + MainCommnetId[i][4] + '","' + MainCommnetId[i][8] + '","' + MainCommnetId[i][2] + '");>submit</a>&nbsp;&nbsp;&nbsp;<a style="cursor: pointer;color: blue;" onclick=CloseDiv("' + MainCommnetId[i][4] + '");>Closed</a></div></br>');
                        }
                        for (var k = 0; k < result.length; k++) {
                            if (result[k].message_id == MainCommnetId[i][4]) 
                            {
                                if (result[k].isfile == 1) 
                                {
                                    customers.push('<div style="font-family: Roboto;background: #eaeaea;float: left;max-width:60%;padding: 4px 10px 4px 5px;border: 1px solid #e4d8d8;width: auto;min-width: 90%;border-radius: 6px;margin-right:10px;margin-top: 12px;">' + result[k].comment + '</div></br></br><div style="float: left;font-size: 12px;margin-right:10px">' + result[k].user_name + ',' + new Date(new Date(result[k].created_at)).toLocaleString() + '<a style="font-size: 12px;margin-left: 8px;color:blue;cursor: pointer;" onclick=ViewDiv("' + result[k]._id + '","' + result[k].message_id + '");>View</a><a style="font-size: 12px;margin-left: 8px;color:blue;cursor: pointer;" onclick=DownloadDiv("' + result[k]._id + '","' + result[k].message_id + '");>Download</a></div></br>');
                                }
                                else
                                {
                                    customers.push('<div style="font-family: Roboto;background: #eaeaea;float: left;max-width:60%;padding: 4px 10px 4px 5px;border: 1px solid #e4d8d8;width: auto;min-width: 90%;border-radius: 6px;margin-right:10px;margin-top: 12px;">' + result[k].comment + '</div></br></br><div style="float: left;font-size: 12px;margin-right:10px">' + result[k].user_name + ',' + new Date(new Date(result[k].created_at)).toLocaleString() + '</div></br>');
                                }
                            }
                        }
                    }
                    $("#ShowDatadiv").load();
                    $('#ShowDatadiv').html('');
                    $('#ShowDatadiv').html('<div style="min-height:45px;padding-left: 10px;padding-top: 10px;">' + customers.join('</div><div style="padding-left: 10px;padding-bottom: 18px;clear: both !important;padding-top: 5px;">') + '</div>');

                },
                error: function (response) {

                }
            });

        }
       <%-- $(document).ready(function () {
            debugger
            GetCommentDetails();
            $('#TxtComments').mentionable(
                '<% =KendoPath%>/Data/GetContractTSheetUSerDetailbySearch?customerID=<% =customerID%>&ContractId=<% =conInitiid%>', { parameterName: "search" }

            );

            $('[data-toggle="popover"]').popover();
            FetchUSerDetail();
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(InitializeRequest);
            prm.add_endRequest(EndRequest);
        });--%>


        function FetchUSerDetail() {
            debugger
            $('[id*=ddlrequestto]').select({
                //includeSelectAllOption: true,
                numberDisplayed: 2,
                buttonWidth: '100%',
                enableCaseInsensitiveFiltering: true,
                filterPlaceholder: 'Type to Search for User..',
                nSelectedText: ' - User(s) selected',
            });
        }
    </script>
      <script type="text/javascript">
        $(document).ready(function () {
            BindControls();
            applyCSSDate();
            
        });
     function hideDivBranch() {
         $('#divFilterLocation').hide("blind", null, 500, function () { });
     }

     function initializeJQueryUI(textBoxID, divID) {
         $("#" + textBoxID).unbind('click');

         $("#" + textBoxID).click(function () {
             $("#" + divID).toggle("blind", null, 500, function () { });
         });
     }
     var prm = Sys.WebForms.PageRequestManager.getInstance();
     prm.add_initializeRequest(InitializeRequest);
     prm.add_endRequest(EndRequest);
     function InitializeRequest(sender, args) { }
     function EndRequest(sender, args) { BindControls(); binddropdown(); }

     function binddropdown() {
         $(function () {
             $('i.glyphicon.glyphicon-search').removeClass('glyphicon glyphicon-search').addClass('fa fa-search color-black');
             $('i.glyphicon glyphicon-remove-circle').removeClass('glyphicon glyphicon-remove-circle').addClass(' fa fa-remove');
         });
     }
      function BindControls() {
            var startDate = new Date();
            $(function () {
                $('input[id*=txtenddate]').datepicker({
                    dateFormat: 'dd-mm-yy',
                    //maxDate: startDate,
                    numberOfMonths: 1,
                    changeMonth: true,
                    changeYear: true,
                });

                $('input[id*=txtstartdate]').datepicker({
                    dateFormat: 'dd-mm-yy',
                    //minDate: startDate,
                    numberOfMonths: 1,
                    changeMonth: true,
                    changeYear: true,
                });                

              
                $('input[id*=txtdate]').datepicker({
                    dateFormat: 'dd-mm-yy',
                    numberOfMonths: 1,
                    changeMonth: true,
                    changeYear: true,
                });

                $('[id*=ddlrequestto]').select({
                    //includeSelectAllOption: true,
                    numberDisplayed: 2,
                    buttonWidth: '100%',
                    enableCaseInsensitiveFiltering: true,
                    filterPlaceholder: 'Type to Search for User..',
                    nSelectedText: ' - User(s) selected',
                });
            });

      }
         function applyCSSDate() {
         
            $('#<%= txtstartdate.ClientID %>').removeClass();
            $('#<%= txtstartdate.ClientID %>').addClass('form-control');
            $('#<%= txtenddate.ClientID %>').removeClass();
            $('#<%= txtenddate.ClientID %>').addClass('form-control');
            $('#<%= txtdate.ClientID %>').removeClass();
            $('#<%= txtdate.ClientID %>').addClass('form-control');

           
        }
      </script>
      <script type="text/javascript">
     function fCheckTree(obj) {
            var id = $(obj).attr('data-attr');
            var elm = $("#" + id);
            $(elm).trigger('click');
        }
        function FnSearch() {

            var tree = document.getElementById('BodyContent_tvFilterLocation');
            var links = tree.getElementsByTagName('a');
            var keysrch = document.getElementById('BodyContent_tbxFilterLocation').value.toLowerCase();
            var keysrchlen = keysrch.length
            if (keysrchlen > 2) {
                $('#bindelement').html('');
                for (var i = 0; i < links.length; i++) {

                    var anch = $(links[i]);
                    var twoletter = $(anch).html().toLowerCase().indexOf(keysrch);
                    var getId = $(anch).attr('id');
                    var parendNode = '#' + getId + 'Nodes';
                    var childanchor = $(parendNode).find('a');
                    if (childanchor.length == 0) {
                        if (twoletter > -1) {

                            var idchild = $($(anch).siblings('input')).attr('name');                            
                            var createanchor = '<input type="checkbox" onclick="fCheckTree(this)"  data-attr="' + idchild + '" ><a  >' + anch.html() + '</a></br>';
                            $('#bindelement').append(createanchor);
                        }
                    }

                }
                $(tree).hide();
                $('#bindelement').show();
            } else {
                $('#bindelement').html('');
                $('#bindelement').hide();
                $(tree).show();
            }

        }

        function OnTreeClick(evt) {
            var src = window.event != window.undefined ? window.event.srcElement : evt.target;
            var isChkBoxClick = (src.tagName.toLowerCase() == "input" && src.type == "checkbox");
            if (isChkBoxClick) {
                var parentTable = GetParentByTagName("table", src);
                var nxtSibling = parentTable.nextSibling;
                if (nxtSibling && nxtSibling.nodeType == 1)//check if nxt sibling is not null & is an element node
                {
                    if (nxtSibling.tagName.toLowerCase() == "div") //if node has children
                    {
                        //check or uncheck children at all levels
                        CheckUncheckChildren(parentTable.nextSibling, src.checked);
                    }
                }
                //check or uncheck parents at all levels
                CheckUncheckParents(src, src.checked);
            }
        }

        function CheckUncheckChildren(childContainer, check) {
            var childChkBoxes = childContainer.getElementsByTagName("input");
            var childChkBoxCount = childChkBoxes.length;
            for (var i = 0; i < childChkBoxCount; i++) {
                childChkBoxes[i].checked = check;
            }
        }

        function CheckUncheckParents(srcChild, check) {
            var parentDiv = GetParentByTagName("div", srcChild);
            var parentNodeTable = parentDiv.previousSibling;

            if (parentNodeTable) {
                var checkUncheckSwitch;

                if (check) //checkbox checked
                {
                    var isAllSiblingsChecked = AreAllSiblingsChecked(srcChild);
                    if (isAllSiblingsChecked)
                        checkUncheckSwitch = true;
                    else
                        return; //do not need to check parent if any(one or more) child not checked
                }
                else //checkbox unchecked
                {
                    checkUncheckSwitch = false;
                }

                var inpElemsInParentTable = parentNodeTable.getElementsByTagName("input");
                if (inpElemsInParentTable.length > 0) {
                    var parentNodeChkBox = inpElemsInParentTable[0];
                    parentNodeChkBox.checked = checkUncheckSwitch;
                    //do the same recursively
                    CheckUncheckParents(parentNodeChkBox, checkUncheckSwitch);
                }
            }
        }

        function AreAllSiblingsChecked(chkBox) {
            var parentDiv = GetParentByTagName("div", chkBox);
            var childCount = parentDiv.childNodes.length;
            for (var i = 0; i < childCount; i++) {
                if (parentDiv.childNodes[i].nodeType == 1) //check if the child node is an element node
                {
                    if (parentDiv.childNodes[i].tagName.toLowerCase() == "table") {
                        var prevChkBox = parentDiv.childNodes[i].getElementsByTagName("input")[0];
                        //if any of sibling nodes are not checked, return false
                        if (!prevChkBox.checked) {
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        //utility function to get the container of an element by tagname
        function GetParentByTagName(parentTagName, childElementObj) {
            var parent = childElementObj.parentNode;
            while (parent.tagName.toLowerCase() != parentTagName.toLowerCase()) {
                parent = parent.parentNode;
            }
            return parent;
        }
       

</script>

      <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
         .chosen-container {
   
         margin-left: 32px;
       }
    </style>

      <script type="text/javascript">
        function initializeJQueryUI(textBoxID, divID) {

            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });


            $('#BodyContent_tbxFilterLocation').keyup(function () {
                FnSearch();
            });
        }
        
    </script>

      <script type="text/javascript">
    $(function () {
        $("[id*=tvFilterLocation] input[type=checkbox]").bind("click", function () {
            var table = $(this).closest("table");
            if (table.next().length > 0 && table.next()[0].tagName == "DIV") {
                //Is Parent CheckBox
                var childDiv = table.next();
                var isChecked = $(this).is(":checked");
                $("input[type=checkbox]", childDiv).each(function () {
                    if (isChecked) {
                        $(this).attr("checked", "checked");
                    } else {
                        $(this).removeAttr("checked");
                    }
                });
            } else {
                //Is Child CheckBox
                var parentDIV = $(this).closest("DIV");
                if ($("input[type=checkbox]", parentDIV).length == $("input[type=checkbox]:checked", parentDIV).length) {
                    $("input[type=checkbox]", parentDIV.prev()).attr("checked", "checked");
                } else {
                    $("input[type=checkbox]", parentDIV.prev()).removeAttr("checked");
                }
            }
        });


        $('#BodyContent_tbxFilterLocation').keyup(function () {
            FnSearch();
        });
    })
      </script>

      <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
        .excelgriddisplay{
            display:none;
        }
               .chosen-container-single .chosen-single{background:none !important;border:1px solid #c7c7cc   !important;height: 32px  !important;padding: 3px 0px 0px 14px !important; font-family:'Roboto', sans-serif !important; }

        .dd_chk_select {
            height: 30px !important;  
            text-align: center;          
            border-radius: 5px;
        }

        div.dd_chk_select div#caption
        {
            margin-top: 5px;
        }
    </style>
     <style>
        .scrolling-wrapper {
            overflow-x: auto;
            overflow-y: hidden;
            white-space: nowrap;
        }
    </style>
      <script type="text/javascript">
        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });


            $('#BodyContent_tbxFilterLocation').keyup(function () {
                FnSearch();
            });

        }
        function initializeRadioButtonsList(controlID) {
            $(controlID).buttonset();
          }
          function scrollUpPage() {
              debugger;
              $('html, body').animate({ scrollTop: '0px' }, 'slow');
          }

          function scrollDown() {
              $('html, body').animate({ scrollTop: $elem.height() }, 800);
          }
        //  function scrollUpPage() {
        //      debugger;

        //    $("#divMainView").animate({
        //        scrollTop: 0
        //    },
        //        'slow');
        //}
          function RefreshParent() {
              window.parent.location.href = window.parent.location.href;
          }
        
      </script>

      <style type="text/css">
        .dd_chk_select {
            
            height: 81px;
            height: 3px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc!important;        
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family:Roboto sans-serif !important;
            margin-bottom: 0px !important;
        }
         .chosen-results {  max-height: 75px !important
         }
        .chosen-container-single .chosen-single{background:none !important;border:1px solid #c7c7cc   !important;height: 32px  !important;padding: 3px 0px 0px 14px !important; font-family:'Roboto', sans-serif !important; }
    </style>

      <script type="text/javascript">

        $('#divShowDialog').on("show", function () {
            $(this).find(".modal-body").css("max-height", height);
        });

        $('#divShowDialog').on('show.bs.modal', function () {

            //alert("called");
            $('#divShowDialog').find('.modal-body').css({
                width: 'auto', //probably not needed
                height: 'auto', //probably not needed 
                'max-height': '100%'
            });
        });
          function ShowDialog(ConTermID) {
            var modalHeight = screen.height - 150;

            if (modalHeight < 0)
                modalHeight = 200;

            $('#divShowDialog').modal('show');
            debugger
            $('.modal-dialog').css('width', '95%');
            $('#showdetails').attr('width', '100%');
            $('#showdetails').attr('height', modalHeight + "px");
              $('#showdetails').attr('src', "/ContractProduct/aspxPages/ContractDetailsPage.aspx?ConAccessID=" + ConTermID + "&AccessID=0");
        };
      </script>

</head>
<body style="background-color: white">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="smContractDetailPage" EnablePartialRendering="true" runat="server" ScriptMode ="Release"></asp:ScriptManager>
        <div>
       <div class="mainDiv">
           <div class="col-md-12" style="background-color:#f7f7f7;height:47px">
                <header class="panel-heading tab-bg-primary" style="background: none !important;">
                    <ul class="nav nav-tabs">
                        <li class="active" id="liContractSummary" runat="server">
                            <asp:LinkButton ID="lnkBtnContractSummary"  runat="server" OnClick="TabContract_Click" CssClass="bgColor-gray">Term Sheet</asp:LinkButton>
                        </li>

                        <li class="" id="liContractRequestor" runat="server">
                            <asp:LinkButton ID="lnkContractRequestor"  CausesValidation="false" OnClick="lnkContractRequestor_Click" runat="server" Style="background-color: #f7f7f7;">Request for Contract</asp:LinkButton>
                        </li>
                       
                        <li class="" id="liAuditLog" runat="server">
                            <asp:LinkButton ID="lnkAuditLog"  CausesValidation="false" runat="server" OnClick="lnkAuditLog_Click" Style="background-color: #f7f7f7;">Audit Log(s)</asp:LinkButton>
                        </li>
                        
                       

                        
                    </ul>
                </header>
            </div>
          

            <div class="clearfix"></div>

            <asp:UpdateProgress ID="updateProgress" runat="server">
                <ProgressTemplate>
                    <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.3;">
                        <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                            AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 40%; left: 40%;" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
             <div id="divMainView">
                <asp:MultiView ID="MainView" runat="server">
                     <asp:View ID="ContractTermView" runat="server">
                        <div class="container">
                             <!--ContractInitiator Panel Start-->
                            

                              <div class="row Dashboard-white-widget">
                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-default">
                                        <div id="collapseDivContractTermsheet" class="panel-collapse collapse in">
                                            <div class="panel-body">
                                                <asp:UpdatePanel ID="upContractAuditLog" runat="server" OnLoad="upContractAuditLog_Load" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <div class="row">
                                                            <div style="margin-bottom: 7px">
                                                                <asp:ValidationSummary ID="ValidationSummary11" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                                                    ValidationGroup="validationContractterm" />
                                                                <asp:CustomValidator ID="cvContractterm" runat="server" EnableClientScript="False"
                                                                    ValidationGroup="validationContractterm" Display="None" />
                                                            </div>
                                                        </div>

                                                        <asp:Panel ID="Panel3" runat="server">
                                                             <div class="row">
                                                               <div class="form-group col-md-6">
                                                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                    <label style="width: 26.5%; display: block; float: left; font-size: 13px; color: #333;">
                                                                         Contract Summary</label>
                                                                     <asp:DropDownListChosen runat="server" ID="ddlcontractsummmary" DataPlaceHolder="Select Vendor" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                        CssClass="form-control" Width="60%">
                                                                         <asp:ListItem Text="New" Value="true"></asp:ListItem>
                                                                        <%--  <asp:ListItem Text="Current" Value="false"></asp:ListItem>--%>
                                                                         </asp:DropDownListChosen>
                                                                           <asp:RequiredFieldValidator ID="RequiredFieldValidator13" ErrorMessage="Please Select Contract summary"
                                                                        ControlToValidate="ddlcontractsummmary" runat="server" ValidationGroup="validationContractterm" Display="None" />
                                                                </div>
                                                                <div class="form-group col-md-6">
                                                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                    <label style="width: 26.5%; display: block; float: left; font-size: 13px; color: #333;">
                                                                        Contract No.</label>
                                                                     <asp:TextBox runat="server" ID="txtcontractno" TextMode="MultiLine" Style="width: 60%; margin-left: 189px; margin-top: 1px;height: 5px;" CssClass="form-control" autocomplete="off" />
                                                                 <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Please enter contract number."
                                                                    ControlToValidate="txtcontractno" runat="server" ValidationGroup="validationContractterm"
                                                                    Display="None" />
                                                                </div>

                                                            </div>
                                                             <div class="row">
                                                                <div class="form-group col-md-6">
                                                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                    <label style="width: 26.5%; display: block; float: left; font-size: 13px; color: #333;">
                                                                        Name of Vendor</label>
                                                                     <asp:DropDownListChosen runat="server" ID="ddlvendor" DataPlaceHolder="Select Vendor" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                        CssClass="form-control" Width="60%" />
                                                                           <asp:RequiredFieldValidator ID="RequiredFieldValidator10" ErrorMessage="Please Select Vendor"
                                                                        ControlToValidate="ddlvendor" runat="server" ValidationGroup="validationContractterm" Display="None" />
                                                                </div>

                                                                <div class="form-group col-md-6">
                                                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                    <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                        Vendor Address</label>
                                                                    <asp:TextBox runat="server" ID="txtvendoraddress" TextMode="MultiLine" Style="width: 60%;height: 5px; margin-left: 189px; margin-top: 1px;" CssClass="form-control" autocomplete="off" />
                                                             
                                                                </div>
                                                            </div>
                                                          <div class="row">
                                                            <div class="form-group col-md-12">
                                                                <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="width: 15.5%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Scope of Service</label>
                                                                <asp:TextBox runat="server" ID="txtscopeofservice" TextMode="MultiLine" Style="width: 79.8%;" CssClass="form-control"  autocomplete="off" />
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator15" ErrorMessage="Scope of Service can not be empty."
                                                                    ControlToValidate="txtscopeofservice" runat="server" ValidationGroup="validationContractterm"
                                                                    Display="None" />
                                                                
                                                            </div>
                                                        </div>
                                                             <div class="row">
                                                                <div class="form-group col-md-6">
                                                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                    <label for="txtstartdate" style="width:32%; display: block; float: left; font-size: 13px; color: #333;" class="control-label">
                                                                   <%-- <label for="txtstartdate" class="control-label">Start date</label>--%>Start date</label>
                                                                  <div class="input-group date">
                                                                    <span class="input-group-addon">
                                                                        <span class="fa fa-calendar color-black"></span>
                                                                    </span>
                                                                    <asp:TextBox ID="txtstartdate" runat="server" placeholder="DD-MM-YYYY" class="form-control" autocomplete="off" Style="width: 267.8px;" />
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator9" ErrorMessage="Enter start date"
                                                                    ControlToValidate="txtstartdate" runat="server" ValidationGroup="validationContractterm"
                                                                    Display="None" />
                                                                   </div>
                                                                </div>

                                                                <div class="form-group col-md-6">
                                                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                    <label style="width: 30%; display: block; float: left; font-size: 13px; color: #333;"/>
                                                                        End date</label>
                                                                      <div class="input-group date">
                                                                    <span class="input-group-addon">
                                                                        <span class="fa fa-calendar color-black"></span>
                                                                    </span>
                                                                    <asp:TextBox ID="txtenddate" runat="server" placeholder="DD-MM-YYYY" class="form-control" autocomplete="off" Style="width: 266.8px;" />
                                                                           <asp:RequiredFieldValidator ID="RequiredFieldValidator11" ErrorMessage="Enter end date"
                                                                    ControlToValidate="txtenddate" runat="server" ValidationGroup="validationContractterm"
                                                                    Display="None" />
                                                                </div>
                                                                </div>
                                                            </div>
                                                          <div class="row">
                                                            <div class="form-group col-md-12">
                                                                <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="width: 15.5%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Term</label>
                                                                <asp:TextBox runat="server" ID="txtterm" TextMode="MultiLine" Style="width: 80%;" CssClass="form-control"  autocomplete="off" />
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator16" ErrorMessage="Term can not be empty."
                                                                    ControlToValidate="txtterm" runat="server" ValidationGroup="validationContractterm"
                                                                    Display="None" />
                                                                
                                                            </div>
                                                        </div>
                                                              <div class="row">
                                                                <div class="form-group col-md-6">
                                                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                    <label style="width: 32%; display: block; float: left; font-size: 13px; color: #333;">
                                                                        Service Fees</label>
                                                                     <asp:TextBox runat="server" ID="txtservicefees" TextMode="MultiLine" Style="width: 60%;height: 5px; margin-left:178px; margin-top: 1px;" CssClass="form-control" autocomplete="off" />
                                                                          <asp:RequiredFieldValidator ID="RequiredFieldValidator17" ErrorMessage="Please enter service fees"
                                                                        ControlToValidate="txtservicefees" runat="server" ValidationGroup="validationContractterm" Display="None" />
                                                                </div>

                                                                <div class="form-group col-md-6">
                                                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                    <label style="width: 24%; display: block; float: left; font-size: 13px; color: #333;">
                                                                        Terms of Payment</label>
                                                                     <asp:DropDownListChosen runat="server" ID="ddltermpayment" DataPlaceHolder="Select term Payment" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                        CssClass="form-control" Width="61%">
                                                                            <asp:ListItem Text="Select term Payment" Value="-1"></asp:ListItem>
                                                                            <asp:ListItem Text="Annually " Value="1"></asp:ListItem>
                                                                            <asp:ListItem Text="Monthly" Value="2"></asp:ListItem>
                                                                            <asp:ListItem Text="Quarterly" Value="3"></asp:ListItem>
                                                                         </asp:DropDownListChosen>
                                                                     <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ErrorMessage="Please Select terms of payment." initialvalue="-1"
                                                                        ControlToValidate="ddltermpayment" runat="server" ValidationGroup="validationContractterm" Display="None" />
                                                                </div>
                                                            </div>
                                                             <div class="row">
                                                                <div class="form-group col-md-6">
                                                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                    <label style="width: 26.5%; display: block; float: left; font-size: 13px; color: #333;">
                                                                        Instruction related to Taxation</label>
                                                                    <asp:DropDownListChosen runat="server" ID="ddltaxation" DataPlaceHolder="Select taxation" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                      OnSelectedIndexChanged="ddltaxation_SelectedIndexChanged" AutoPostBack="true"  CssClass="form-control" Width="60%">
                                                                    
                                                                        <asp:ListItem Text="Yes" Value="true"></asp:ListItem>
                                                                         <asp:ListItem Text="No" Value="false"></asp:ListItem>
                                                                        </asp:DropDownListChosen>
                                                                </div>

                                                                <div class="form-group col-md-6" id="divadddetails1" runat="server">
                                                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                    <label style="width: 29.5%; display: block; float: left; font-size: 13px; color: #333;">
                                                                        Additional details</label>
                                                                    <asp:TextBox runat="server" ID="txtdetails1" Style="padding: 0px; padding-left: 10px; margin: 0px; width: 61%; background-color: #fff; cursor: pointer;"
                                                                        autocomplete="off" AutoCompleteType="None" CausesValidation="true" CssClass="form-control" />
                                                                </div>
                                                            </div>
                                                              <div class="row">
                                                                <div class="form-group col-md-6">
                                                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                    <label style="width: 26.5%; display: block; float: left; font-size: 13px; color: #333;">
                                                                        Whether Customer data is shared</label>
                                                                    <asp:DropDownListChosen runat="server" ID="ddlshared" DataPlaceHolder="Select data" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                        CssClass="form-control" Width="60%" AutoPostBack="true" OnSelectedIndexChanged="ddlshared_SelectedIndexChanged">
                                                                        <asp:ListItem Text="Yes" Value="true"></asp:ListItem>
                                                                         <asp:ListItem Text="No" Value="false"></asp:ListItem>
                                                                        </asp:DropDownListChosen>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator19" ErrorMessage="Please Select  Whether Customer data is shared." 
                                                                        ControlToValidate="ddlshared" runat="server" ValidationGroup="validationContractterm" Display="None" />
                                                                </div>

                                                                <div class="form-group col-md-6" id="divadddetails2" runat="server">
                                                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                    <label style="width: 29.5%; display: block; float: left; font-size: 13px; color: #333;">
                                                                        Additional details</label>
                                                                    <asp:TextBox runat="server" ID="txtdetails2" Style="padding: 0px; padding-left: 10px; margin: 0px; width: 61%; background-color: #fff; cursor: pointer;"
                                                                        autocomplete="off" AutoCompleteType="None" CausesValidation="true" CssClass="form-control" />
                                                                </div>
                                                            </div>
                                                             <div class="row">
                                                                <div class="form-group col-md-6">
                                                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                    <label style="width: 26.5%; display: block; float: left; font-size: 13px; color: #333;">
                                                                        Whether NDA executed if any confidential data being shared</label>
                                                                    <asp:DropDownListChosen runat="server" ID="ddlcondata1" DataPlaceHolder="Select data" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                        CssClass="form-control" Width="60%" AutoPostBack="true" OnSelectedIndexChanged="ddlcondata1_SelectedIndexChanged" >
                                                                          <asp:ListItem Text="Yes" Value="true"></asp:ListItem>
                                                                         <asp:ListItem Text="No" Value="false"></asp:ListItem>
                                                                        </asp:DropDownListChosen>
                                                                   
                                                                </div>

                                                               <div class="form-group col-md-6" id="divdetails" runat="server">
                                                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                    <label style="width: 29.5%; display: block; float: left; font-size: 13px; color: #333;">
                                                                        Additional details</label>
                                                                    <asp:TextBox runat="server" ID="txtdetails3" Style="padding: 0px; padding-left: 10px; margin: 0px; width: 61%; background-color: #fff; cursor: pointer;"
                                                                        autocomplete="off" AutoCompleteType="None" CausesValidation="true" CssClass="form-control" />
                                                                </div>
                                                            </div>
                                                              <div class="row">
                                                                <div class="form-group col-md-6">
                                                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                    <label style="width: 26.5%; display: block; float: left; font-size: 13px; color: #333;">
                                                                        Whether NDA executed if any confidential data being shared</label>
                                                                    <asp:DropDownListChosen runat="server" ID="ddlconfdata2" DataPlaceHolder="Select data" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                        CssClass="form-control" Width="60%">
                                                                          <asp:ListItem Text="Yes" Value="true"></asp:ListItem>
                                                                         <asp:ListItem Text="No" Value="false"></asp:ListItem>
                                                                        </asp:DropDownListChosen>
                                                                
                                                                </div>

                                                                <div class="form-group col-md-6">
                                                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                    <label style="width: 24%; display: block; float: left; font-size: 13px; color: #333;">
                                                                        If related to Related Party, Name of the related party</label>
                                                                    <asp:DropDownListChosen runat="server" ID="ddlrelparty" DataPlaceHolder="Select data" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                        CssClass="form-control" Width="60%">
                                                                          <asp:ListItem Text="Yes" Value="true"></asp:ListItem>
                                                                         <asp:ListItem Text="No" Value="false"></asp:ListItem>
                                                                        </asp:DropDownListChosen>
                                                                </div>
                                                            </div>
                                                              <div class="row">
                                                                <div class="form-group col-md-6">
                                                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                    <label style="width: 26.5%; display: block; float: left; font-size: 13px; color: #333;">
                                                                        Whether an Outsourcing Contract</label>
                                                                    <asp:DropDownListChosen runat="server" ID="ddloutscon" DataPlaceHolder="Select data" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                        CssClass="form-control" Width="60%">
                                                                       <asp:ListItem Text="Yes" Value="true"></asp:ListItem>
                                                                         <asp:ListItem Text="No" Value="false"></asp:ListItem>
                                                                        </asp:DropDownListChosen>
                                                                   
                                                                </div>

                                                                <div class="form-group col-md-6">
                                                                   
                                                                </div>
                                                            </div>
                                                             <div class="row">
                                                                <div class="form-group col-md-6">
                                                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                    <label style="width: 32%; display: block; float: left; font-size: 13px; color: #333;">
                                                                        Entity/Location</label>

                                                                    <asp:TextBox runat="server" ID="tbxFilterLocation" Style="padding: 0px; padding-left: 10px; margin: 0px; width: 60%; background-color: #fff;"
                                                                        autocomplete="off" AutoCompleteType="None"  CssClass="form-control"  />

                                                                    <div style="margin-left:30%; position: absolute; z-index: 10; width: 58%;" id="divFilterLocation">
                                                                        <asp:TreeView runat="server" ID="tvFilterLocation" BackColor="White" BorderColor="Black" OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged"
                                                                            BorderWidth="1" SelectedNodeStyle-Font-Bold="true" Height="150px"
                                                                            Style="overflow: auto; margin-top: -20px; border: 1px solid #c7c7cc; background-color: #ffffff; color: #8e8e93 !important;" ShowLines="true">
                                                                        </asp:TreeView>
                                                                    </div>
                                                                  <asp:RequiredFieldValidator ID="rfvBranch" ErrorMessage="Please Select Entity/Location." InitialValue="Select Entity/Location"
                                                                    ControlToValidate="tbxFilterLocation" runat="server" ValidationGroup="validationContractterm" Display="None" />
                                                                </div>

                                                                <div class="form-group col-md-6">
                                                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                    <label style="width: 24%; display: block; float: left; font-size: 13px; color: #333;">
                                                                        Department</label>
                                                                    <asp:DropDownListChosen runat="server" ID="ddldepartment" DataPlaceHolder="Select department" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                        CssClass="form-control" Width="60%" />
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ErrorMessage="Please Select department."
                                                                        ControlToValidate="ddldepartment" runat="server" ValidationGroup="validationContractterm" Display="None" />
                                                                </div>

                                                            </div>
                                                           
                                                            <div class="row">
                                                                <div class="form-group col-md-6">
                                                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                    <label style="width: 26%; display: block; float: left; font-size: 13px; color: #333;">
                                                                        SPOC Name</label>
                                                                    <asp:DropDownListChosen runat="server" ID="ddlSPOCName" DataPlaceHolder="Select User" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                        CssClass="form-control" Width="60%" />
                                                                  
                                                                </div>

                                                                <div class="form-group col-md-6">
                                                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                    <label style="width: 30%; display: block; float: left; font-size: 13px; color: #333;">
                                                                        SPOC Designation</label>
                                                                    <asp:TextBox runat="server" ID="txtspocdesignation" Style="padding: 0px; padding-left: 10px; margin: 0px; width: 60%; background-color: #fff; cursor: pointer;"
                                                                        autocomplete="off" AutoCompleteType="None" CausesValidation="true" CssClass="form-control" />
                                                                </div>
                                                            </div>
                                                         
                                                             <div class="row">
                                                                <div class="form-group col-md-6">
                                                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                    <label style="width: 26%; display: block; float: left; font-size: 13px; color: #333;">
                                                                        Type of Entity</label>
                                                                    <asp:DropDownListChosen runat="server" ID="ddlEntity" DataPlaceHolder="Select Entity" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                        CssClass="form-control" Width="60%">
                                                                         <asp:ListItem Text="Select Entity" Value="-1"></asp:ListItem>
                                                                        <asp:ListItem Text="Company" Value="1"></asp:ListItem>
                                                                        <asp:ListItem Text="Partnership" Value="2"></asp:ListItem>
                                                                        <asp:ListItem Text="Limited Liability Partnership" Value="3"></asp:ListItem>
                                                                        <asp:ListItem Text="Proprietary concern" Value="4"></asp:ListItem>
                                                                        <asp:ListItem Text="Others specify" Value="5"></asp:ListItem>
                                                                        </asp:DropDownListChosen>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="Please Select Entity/Location." InitialValue="Select Entity"
                                                                        ControlToValidate="ddlEntity" runat="server" ValidationGroup="validationContractterm" Display="None" />
                                                                </div>
                                                               
                                                                <div class="form-group col-md-6">
                                                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                    <label style="width: 24%; display: block; float: left; font-size: 13px; color: #333;">
                                                                        Level of Attention Required</label>
                                                                    <asp:DropDownListChosen runat="server" ID="ddlattention" DataPlaceHolder="Select Level" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                        CssClass="form-control" Width="60%">
                                                                     <asp:ListItem Text="Select Level" Value="-1"></asp:ListItem>
                                                                     <asp:ListItem Text="High" Value="1"></asp:ListItem>
                                                                    <asp:ListItem Text="Medium" Value="2"></asp:ListItem>
                                                                    <asp:ListItem Text="Low" Value="3"></asp:ListItem>
                                                                        </asp:DropDownListChosen>
                                                                </div>
                                                            </div>

                                                             <div class="row">
                                                                <div class="form-group col-md-12">
                                                                    <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                    <label style="display: block; float: left; font-size: 13px; color: #333;">
                                                                        Contract Objectives and other details that have been agreed and needs
                                                                        to be brought to the notice of Legal Department</label>
                                                                    </label>

                                                                </div>
                                                            </div>
                                                             <div class="row">
                                                              <div class="form-group col-md-12">
                                                                  <asp:TextBox runat="server" ID="tbxobjective" TextMode="MultiLine" Style="width: 80%; margin-left: 197px;" CssClass="form-control" autocomplete="off" />
                                                                   <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ErrorMessage="Contract Objective can not be empty."
                                                                        ControlToValidate="tbxobjective" runat="server" ValidationGroup="validationContractterm"
                                                                        Display="None" />--%>
                                                              </div>
                                                          </div>
                                                             <div class="row">
                                                                <div class="form-group col-md-6">
                                                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                    <label style="width: 26%; display: block; float: left; font-size: 13px; color: #333;">
                                                                        Risk assessment (For Risk Team)</label>
                                                                    <asp:DropDownListChosen runat="server" ID="ddlrisk1" DataPlaceHolder="Select Risk assessment" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                        CssClass="form-control" Width="60%">
                                                                         <asp:ListItem Text="Yes" Value="true"></asp:ListItem>
                                                                         <asp:ListItem Text="No" Value="false"></asp:ListItem>
                                                                        </asp:DropDownListChosen>
                                                                </div>

                                                                <div class="form-group col-md-6">
                                                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                    <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                        Date</label>
                                                                    <div class="input-group date">
                                                                    <span class="input-group-addon">
                                                                        <span class="fa fa-calendar color-black"></span>
                                                                    </span>
                                                                    <asp:TextBox ID="txtdate" runat="server" placeholder="DD-MM-YYYY" class="form-control" autocomplete="off" Style="width: 307.8px;" />
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator12" ErrorMessage="date can not be empty."
                                                                        ControlToValidate="txtdate" runat="server" ValidationGroup="validationContractterm"
                                                                        Display="None" />
                                                                </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="form-group col-md-12">
                                                                    <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                    <label style="width: 15.5%; display: block; float: left; font-size: 13px; color: #333;">
                                                                        Define Risk</label>
                                                                    <asp:TextBox runat="server" ID="txtrisk" TextMode="MultiLine" Style="width: 80%;" CssClass="form-control" autocomplete="off" />
                                                                </div>
                                                            </div>
                                                        <div class="row">
                                                            <div class="form-group col-md-12">
                                                                <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <label style="width: 15.5%; display: block; float: left; font-size: 13px; color: #333;">
                                                                  Consequence of defined Risk</label>
                                                                <asp:TextBox runat="server" ID="txtconrisk" TextMode="MultiLine" Style="width: 80%;" CssClass="form-control"  autocomplete="off" />
                                                              </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="form-group col-md-12">
                                                                <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <label style="width: 15.5%; display: block; float: left; font-size: 13px; color: #333;">
                                                                  Risk Mitigation Action</label>
                                                                <asp:TextBox runat="server" ID="txtmitigationaction" TextMode="MultiLine" Style="width: 80%;" CssClass="form-control"  autocomplete="off" />
                                                              </div>
                                                        </div>
                                                            <div class="row">
                                                                <div class="form-group col-md-12">
                                                                    <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                    <label style="display: block; float: left; font-size: 13px; color: #333;">
                                                                        Contract Compliance - Clauses that has to complied with or specific Representation and warranties
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="form-group col-md-12">
                                                                    <asp:TextBox runat="server" ID="txtclauses" TextMode="MultiLine" Style="width: 80%;margin-left: 199px;" CssClass="form-control" autocomplete="off" />
                                                                   <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ErrorMessage="Contract Clauses can not be empty."
                                                                        ControlToValidate="txtclauses" runat="server" ValidationGroup="validationContractterm"
                                                                        Display="None" />--%>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="form-group col-md-12">
                                                                    <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                    <label style="width: 15.4%; display: block; float: left; font-size: 13px; color: #333;">
                                                                        Escalation process if any</label>
                                                                    <asp:TextBox runat="server" ID="txtescprocess" TextMode="MultiLine" Style="width: 80%;" CssClass="form-control" autocomplete="off" />
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="form-group col-md-12">
                                                                    <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                    <label style="display: block; float: left; font-size: 13px; color: #333;">
                                                                        Exit/Termination plan(if any)- Important for the Owner department to contemplate at the inception.</label>
                                                                    </label>

                                                                </div>
                                                            </div>
                                                          <div class="row">
                                                              <div class="form-group col-md-12">
                                                                  <asp:TextBox runat="server" ID="txttermplan1" TextMode="MultiLine" Style="width: 80%;margin-left: 199px;" CssClass="form-control" autocomplete="off" />
                                                                   <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ErrorMessage="Contract Exit/Termination plan can not be empty."
                                                                        ControlToValidate="txttermplan1" runat="server" ValidationGroup="validationContractterm"
                                                                        Display="None" />--%>
                                                              </div>
                                                          </div>
                                                            <div class="row">
                                                                <div class="form-group col-md-6">
                                                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                    <label style="width: 26%; display: block; float: left; font-size: 13px; color: #333;">
                                                                        Termination Notice period</label>
                                                                    <asp:DropDownListChosen runat="server" ID="DropDownListChosen2" DataPlaceHolder="Select" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                        CssClass="form-control" Width="60%">
                                                                        <asp:ListItem Text="Select Notice Period" Value="1"></asp:ListItem>
                                                                        <asp:ListItem Text="30" Value="2"></asp:ListItem>
                                                                        <asp:ListItem Text="60" Value="3"></asp:ListItem>
                                                                        <asp:ListItem Text="90" Value="4"></asp:ListItem>
                                                                    </asp:DropDownListChosen>
                                                                </div>

                                                                <div class="form-group col-md-6">
                                                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                    <label style="width: 24%; display: block; float: left; font-size: 13px; color: #333;">
                                                                        Any other Termination Plan</label>
                                                                    <asp:TextBox ID="txttermplan2" runat="server" class="form-control" autocomplete="off" Style="width: 365.8px;" />
                                                                </div>
                                                            </div>
                                                             <div class="row">
                                                                <div class="form-group col-md-12">
                                                                    <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                    <label style="display: block; float: left; font-size: 13px; color: #333;">
                                                                        Audit approval if any - important for the Owner department to contemplate at the inception</label>
                                                                    </label>

                                                                </div>
                                                            </div>
                                                          <div class="row">
                                                              <div class="form-group col-md-12">
                                                                  <asp:TextBox runat="server" ID="txtauditappproval" TextMode="MultiLine" Style="width: 80.2%;margin-left: 199px;" CssClass="form-control" autocomplete="off" />
                                                                    
                                                              </div>
                                                          </div>
                                                           <div class="row">
                                                                <div class="form-group col-md-12">
                                                                    <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                    <label style="display: block; float: left; font-size: 13px; color: #333;">
                                                                       Intellectual Property Rights(IPR) - Important for the Owner department to contemplate at the inception.
                                                                  </label>
                                                                 </div>
                                                            </div>
                                                             <div class="row">
                                                                <div class="form-group col-md-6" id="div1" runat="server">
                                                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                    <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">Any type of services/product specifically developed for EGIC-
                                                                    </label>
                                                                        <asp:DropDownListChosen runat="server" ID="ddlEGIC" DataPlaceHolder="Select" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                        CssClass="form-control" Width="60%" AutoPostBack="true" OnSelectedIndexChanged="ddlEGIC_SelectedIndexChanged">
                                                                            <asp:ListItem Text="Yes" Value="true"></asp:ListItem>
                                                                         <asp:ListItem Text="No" Value="false"></asp:ListItem>
                                                                      </asp:DropDownListChosen>
                                                                    
                                                                </div>
                                                            </div>
                                                            <div class="row" id="divadddetails6" runat="server">
                                                                <div class="form-group col-md-12" id="divadddetails4" runat="server">
                                                                    <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                    <label style="width: 16%; display: block; float: left; font-size: 13px; color: #333;">
                                                                        Additional details</label>
                                                                    <asp:TextBox runat="server" ID="txtdetails4" TextMode="MultiLine" Style="width: 80%;" CssClass="form-control" autocomplete="off" />
                                                                

                                                                </div>
                                                            </div>
                                                             <div class="row">
                                                                <div class="form-group col-md-6">
                                                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                    <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                        The IPR rights of service/product developed for EGIC </label>
                                                                    <asp:DropDownListChosen runat="server" ID="DropDownListChosen1" DataPlaceHolder="Select Entity" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                        CssClass="form-control" Width="60%">
                                                                        <asp:ListItem Text="Joint IPR" Value="1"></asp:ListItem>
                                                                        <asp:ListItem Text="EGIC IPR" Value="2"></asp:ListItem>
                                                                       </asp:DropDownListChosen>
                                                                  
                                                                </div>
                                                               
                                                                <div class="form-group col-md-6">
                                                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                    <label style="width: 23%; display: block; float: left; font-size: 13px; color: #333;">
                                                                        Any Intellectual Property to be shared in course of arrangement</label>
                                                                    <asp:DropDownListChosen runat="server" ID="ddlarrange" DataPlaceHolder="Select" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                        CssClass="form-control" Width="62%" AutoPostBack="true" OnSelectedIndexChanged="ddlarrange_SelectedIndexChanged">
                                                                     <asp:ListItem Text="Yes" Value="true"></asp:ListItem>
                                                                    <asp:ListItem Text="No" Value="false"></asp:ListItem>
                                                                      </asp:DropDownListChosen>
                                                                </div>
                                                            </div>
                                                          
                                                         
                                                         <div class="row" id="divadddetails5" runat="server">
                                                            <div class="form-group col-md-12">
                                                                <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <label style="width: 15.5%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Additional details</label>
                                                                <asp:TextBox runat="server" ID="txtdetails5" TextMode="MultiLine" Style="width: 80%;" CssClass="form-control"  autocomplete="off" />
                                                            
                                                                
                                                            </div>
                                                        </div>
                                                            <div class="row">
                                                                <div class="form-group col-md-6">
                                                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                    <label style="width: 26%; display: block; float: left; font-size: 13px; color: #333;">
                                                                       Request To</label>
                                                                    <asp:DropDownListChosen runat="server" ID="ddlrequestto" DataPlaceHolder="Select Request To" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                        CssClass="form-control" Width="60%" />
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Please select Request To."
                                                                        ControlToValidate="ddlrequestto" runat="server" ValidationGroup="validationContractterm" Display="None" />
                                                                </div>

                                                                <div class="form-group col-md-6">
                                                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                    <label style="width: 23%; display: block; float: left; font-size: 13px; color: #333;">
                                                                        Approver</label>
                                                                    <asp:DropDownList runat="server" ID="ddlApproveruser"  AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                        CssClass="form-control" Width="62%">
                                                                     <asp:ListItem Text="Select Approver" Value="-1"></asp:ListItem>
                                                                     <asp:ListItem Text="MD" Value="MD"></asp:ListItem>
                                                                     <asp:ListItem Text="CFO" Value="CFO"></asp:ListItem>
                                                                     <asp:ListItem Text="Head" Value="Head"></asp:ListItem>
                                                                      </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <%--<div class="row">
                                                                <div class="form-group col-md-6">
                                                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                    <label style="width: 32%; display: block; float: left; font-size: 13px; color: #333;">
                                                                        Upload Document</label>
                                                                    <asp:FileUpload ID="FileUpload1" runat="server" AllowMultiple="true" CssClass="fileUploadClass" />
                                                                    
                                                                    
                                                                </div>
                                                                 <div class="form-group col-md-6">
                                                                   <asp:LinkButton ID="LinkButton2" runat="server" CssClass="btn btn-primary" Style="margin-left: -148px;"
                                                                        Text="<i class='fa fa-upload' aria-hidden='true'></i> Upload" OnClick="LinkButton2_Click" OnClientClick="if(!CheckValidation())return false;"
                                                                        ToolTip="Upload Selected Document(s)" data-toggle="tooltip">
                                                                    </asp:LinkButton>
                                                                   
                                                                </div>
                                                               
                                                            </div>--%>

                                                            <div id="uploaddocsection" runat="server">
                                                                <div class="row">
                                                                    <div class="form-group  col-md-4">
                                                                        <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                                                                            <ContentTemplate>
                                                                                 <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                                <label class="control-label">Upload Document(s)</label>
                                                                                <asp:FileUpload ID="fuSampleFile" Multiple="Multiple" runat="server"
                                                                                     Style="color: black" />
                                                                              <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Please Upload Document" ForeColor="Red"  ControlToValidate="fuSampleFile"  SetFocusOnError="true" ></asp:RequiredFieldValidator>--%>
                                                                           
                                                                                 <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ErrorMessage="Please Upload Document"
                                                                        ControlToValidate="fuSampleFile" runat="server" ValidationGroup="validationContractterm" Display="None" />

                                                                                 </ContentTemplate>
                                                                        </asp:UpdatePanel>
                                                                    </div>
                                                                   <%-- <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                                                                        <ContentTemplate>
                                                                            <asp:Button ID="UploadDocument" runat="server" Text="Upload Document"  OnClick="UploadDocument_Click"
                                                                                class="btn btn-search" data-toggle="tooltip" data-placement="top" ToolTip="Upload Document"
                                                                                CausesValidation="true" />
                                                                        </ContentTemplate>
                                                                        <Triggers>
                                                                            <asp:PostBackTrigger ControlID="UploadDocument" />
                                                                        </Triggers>
                                                                    </asp:UpdatePanel>--%>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="form-group  col-md-12">
                                                                        <asp:GridView runat="server" ID="grdDocument" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                                            PageSize="100" AllowPaging="true" OnRowCommand="grdDocument_RowCommand" OnRowDataBound="grdDocument_RowDataBound" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%">
                                                                            <Columns>
                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="2%">
                                                                                    <ItemTemplate>
                                                                                        <%#Container.DataItemIndex+1 %>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Document Name" ItemStyle-Width="10%">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 500px;">
                                                                                            <asp:Label ID="lblDocumentName" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("FileName") %>' ToolTip='<%# Eval("FileName") %>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Action" HeaderStyle-CssClass="text-right" ItemStyle-CssClass="text-right" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="8%">
                                                                                    <ItemTemplate>
                                                                                        <asp:UpdatePanel ID="upgrid" runat="server" UpdateMode="Conditional">
                                                                                            <ContentTemplate>
                                                                                                <asp:LinkButton ID="lnkDownloadDocument" runat="server" CommandName="Download Document" ToolTip="Download Document" data-toggle="tooltip"
                                                                                                    CommandArgument='<%# Eval("ID") %>'>
                                                        <img src='<%# ResolveUrl("~/Images/download_icon_new.png")%>' alt="Download Document" /></asp:LinkButton>
                                                                                                <asp:LinkButton ID="lnkViewDocument" runat="server" CommandName="View Document" ToolTip="View Document" data-toggle="tooltip"
                                                                                                    CommandArgument='<%# Eval("ID") %>'>
                                                        <img src='<%# ResolveUrl("~/Images/View-icon-new.png")%>' alt="View Document" /></asp:LinkButton>
                                                                                                <asp:LinkButton ID="lnkDeleteDocument" runat="server" CommandName="Delete Document" ToolTip="Delete Document" data-toggle="tooltip"
                                                                                                    OnClientClick="return confirm('Are you certain you want to delete this document?');"
       
                                                                                                    CommandArgument='<%# Eval("ID") %>'>
                                                        <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="Delete Document" /></asp:LinkButton>
                                                                                            </ContentTemplate>
                                                                                            <Triggers>
                                                                                                <asp:PostBackTrigger ControlID="lnkDownloadDocument" />
                                                                                            </Triggers>
                                                                                        </asp:UpdatePanel>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                            <RowStyle CssClass="clsROWgrid" />
                                                                            <HeaderStyle CssClass="clsheadergrid" />
                                                                            <PagerSettings Visible="false" />
                                                                            <PagerTemplate>
                                                                            </PagerTemplate>
                                                                            <EmptyDataTemplate>
                                                                                No Record Found
                                                                            </EmptyDataTemplate>
                                                                        </asp:GridView>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-12 text-center" style="margin-top: 45px;">
                                                                <div >
                                                                    <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                                                                        <ContentTemplate>
                                                                            <asp:Button Text="Save In Draft" runat="server" ID="btndraft" CssClass="btn btn-primary"
                                                                                OnClick="btndraft_Click" OnClientClick="scrollUpPage()"></asp:Button>
                                                                            <asp:Button Text="Send" runat="server" ID="btnSave" CssClass="btn btn-primary" Style="margin-left: 36px;"
                                                                              ValidationGroup="validationContractterm"  OnClick="btnSave_Click" OnClientClick="scrollUpPage()"></asp:Button>

                                                                        </ContentTemplate>
                                                                        <Triggers>
                                                                            <asp:PostBackTrigger ControlID="btndraft" />
                                                                            <asp:PostBackTrigger ControlID="btnSave" />
                                                                        </Triggers>
                                                                        </asp:UpdatePanel>
                                                                   <%--  <asp:Button Text="Save In Draft" runat="server" ID="btndraft" CssClass="btn btn-primary" 
                                                                   OnClick="btndraft_Click" OnClientClick="scrollUpPage()"></asp:Button>--%>
                                                                </div>
                                                                <div>
                                                                   <%-- <asp:Button Text="Send" runat="server" ID="btnSave" CssClass="btn btn-primary" Style="margin-left: 250px;margin-top: -55px;"
                                                                    ValidationGroup="validationContractterm" OnClick="btnSave_Click" OnClientClick="scrollUpPage()"></asp:Button>
                                                               --%>
                                                                    </div>
                                                               
                                                                
                                                                <asp:Button Text="Clear" runat="server" Visible="false" ID="btnClearCaseDetail" CssClass="btn btn-primary" Style="margin-left: 425px;margin-top: -98px;" />
                                                            </div>
                                                            
                                                        </asp:Panel>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                              
                        </div>
                    </asp:View>

                     <asp:View ID="ContractRequestorView" runat="server">
                        <div class="container">
                             <!--ContractInitiator Panel Start-->
                            

                              <div class="row Dashboard-white-widget">
                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-default">
                                       

                                        <div id="collapseDivConReq" class="panel-collapse collapse in">
                                            <div class="panel-body">
                                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                    <ContentTemplate>
                                                        <div class="row">
                                                            <div style="margin-bottom: 7px">
                                                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                                                    ValidationGroup="validationContractterm" />
                                                                <asp:CustomValidator ID="CustomValidator1" runat="server" EnableClientScript="False"
                                                                    ValidationGroup="validationContractterm" Display="None" />
                                                            </div>
                                                        </div>

                                                        <asp:Panel ID="Panel1" runat="server">
                                                             <div class="row">
                                                               <div class="form-group col-md-6">
                                                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                                                    <label style="width: 27%; display: block; float: left; font-size: 14px; color: #333;font-weight:bold">
                                                                         Summary   :</label>
                                                                    <asp:Label runat="server" ID="lblcontsummmary" style="color: #333;margin-left: 61px;"></asp:Label>
                                                                </div>
                                                                <div class="form-group col-md-6">
                                                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                    <label style="width: 27%; display: block; float: left; font-size: 14px; color: #333;font-weight:bold">
                                                                        Contract No.   :</label>
                                                                     <asp:Label runat="server" ID="lblcontractno" style="color: #333;margin-left: 61px;"> </asp:Label>
                                                                </div>

                                                            </div>
                                                             <div class="row">
                                                                <div class="form-group col-md-6">
                                                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                    <label style="width: 27%; display: block; float: left; font-size: 14px; color: #333;font-weight:bold">
                                                                        Name of Vendor :</label>
                                                                    <asp:Label runat="server" ID="lblnameofvendor" style="color: #333;margin-left: 61px;"></asp:Label>
                                                                </div>

                                                                <div class="form-group col-md-6">
                                                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                    <label style="width: 22%; display: block; float: left; font-size: 14px; color: #333;font-weight:bold">
                                                                        Vendor Address :</label>
                                                                    <asp:Label runat="server" ID="lblvaddress" style="color: #333;margin-left: 92px;"></asp:Label>
                                                                </div>
                                                            </div>
                                                          <div class="row">
                                                            <div class="form-group col-md-12">
                                                                <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <label style="width: 15.5%; display: block; float: left; font-size: 14px; color: #333;font-weight:bold">
                                                                    Scope of Service :</label>
                                                               <asp:Label runat="server" ID="lblscopeofservice" style="color: #333;margin-left: 32px;"></asp:Label>
                                                                
                                                            </div>
                                                        </div>
                                                             <div class="row">
                                                                <div class="form-group col-md-6">
                                                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                    <label for="txtstartdate" style="width:32%; display: block; float: left; font-size: 14px;font-weight:bold ;color: #333;" class="control-label">
                                                                   Start date :</label>
                                                                 <asp:Label runat="server" ID="lblstartdate" style="color: #333;margin-left: 29px;"></asp:Label>
                                                                </div>

                                                                <div class="form-group col-md-6">
                                                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                    <label style="width: 27%; display: block; float: left; font-size: 14px;font-weight:bold; color: #333;"/>
                                                                        End date :</label>
                                                                 <asp:Label runat="server" ID="lblenddate" style="color: #333;margin-left: 61px;"></asp:Label>
                                                                </div>
                                                            </div>
                                                          <div class="row">
                                                            <div class="form-group col-md-12">
                                                                <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <label style="width: 15.5%; display: block; float: left; font-size: 14px; color: #333;font-weight:bold;">
                                                                    Term :</label>
                                                               <asp:Label runat="server" ID="lblterm" style="color: #333;margin-left: 32px;"></asp:Label>
                                                                
                                                            </div>
                                                        </div>
                                                              <div class="row">
                                                                <div class="form-group col-md-6">
                                                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                    <label style="width: 32%; display: block; float: left; font-size: 14px; color: #333;font-weight:bold;">
                                                                        Service Fees :</label>
                                                                    <asp:Label runat="server" ID="lblserfees" style="color: #333;margin-left: 30px;"></asp:Label>
                                                                </div>

                                                                <div class="form-group col-md-6">
                                                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                    <label style="width: 19%; display: block; float: left; font-size: 14px; color: #333;font-weight:bold;">
                                                                        Terms of Payment :</label>
                                                                     <asp:Label runat="server" ID="lblTermsPayment" style="color: #333;margin-left: 105px;"></asp:Label>
                                                                </div>
                                                            </div>
                                                             <div class="row">
                                                                <div class="form-group col-md-6">
                                                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                    <label style="width: 26.5%; display: block; float: left; font-size: 14px; color: #333;font-weight:bold;">
                                                                        Instruction related to Taxation :</label>
                                                                   <asp:Label runat="server" ID="lbltaxationinstru" style="color: #333;margin-left: 61px;"></asp:Label>
                                                                </div>

                                                                <div class="form-group col-md-6" id="div2" runat="server">
                                                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                    <label style="width: 27%; display: block; float: left; font-size: 14px; color: #333;font-weight:bold;">
                                                                        Additional details :</label>
                                                                   <asp:Label runat="server" ID="lbltaxadddetails" style="color: #333;margin-left: 58px;"></asp:Label>
                                                                </div>
                                                            </div>
                                                              <div class="row">
                                                                <div class="form-group col-md-6">
                                                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                    <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;font-weight:bold;">
                                                                        Whether Customer data is shared :</label>
                                                                  <asp:Label runat="server" ID="lblshareddata" style="color: #333;margin-left: 61px;"></asp:Label>
                                                                </div>

                                                                <div class="form-group col-md-6" id="div3" runat="server">
                                                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                    <label style="width: 27%; display: block; float: left; font-size: 14px; color: #333;font-weight:bold;">
                                                                        Additional details :</label>
                                                                <asp:Label runat="server" ID="lblsharedadditionaldetails" style="color: #333;margin-left: 58px;"></asp:Label>
                                                                </div>
                                                            </div>
                                                             <div class="row">
                                                                <div class="form-group col-md-6">
                                                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                    <label style="width: 27%; display: block; float: left; font-size: 14px; color: #333;font-weight:bold;">
                                                                        Whether NDA executed if any confidential data being shared :</label>
                                                                  <asp:Label runat="server" ID="lblcondata1" style="color: #333;margin-left: 61px;"></asp:Label>
                                                                   
                                                                </div>

                                                               <div class="form-group col-md-6" id="div4" runat="server">
                                                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                    <label style="width: 27%; display: block; float: left; font-size: 14px; color: #333;font-weight:bold;">
                                                                        Additional details :</label>
                                                                   <asp:Label runat="server" ID="lblconaddtidetail" style="color: #333;margin-left: 58px;"></asp:Label>
                                                                </div>
                                                            </div>
                                                              <div class="row">
                                                                <div class="form-group col-md-6">
                                                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                    <label style="width: 27%; display: block; float: left; font-size: 14px; color: #333;font-weight:bold;">
                                                                        Whether NDA executed if any confidential data being shared :</label>
                                                                   <asp:Label runat="server" ID="lblcondata2" style="color: #333;margin-left: 61px;"></asp:Label>
                                                                
                                                                </div>

                                                                <div class="form-group col-md-6">
                                                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                    <label style="width: 22%; display: block; float: left; font-size: 14px; color: #333;font-weight:bold;">
                                                                        If related to Related Party , Name of the related party :</label>
                                                                    <asp:Label runat="server" ID="lblparty" style="color: #333;margin-left: 89px;"></asp:Label>
                                                                </div>
                                                            </div>
                                                              <div class="row">
                                                                <div class="form-group col-md-6">
                                                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                    <label style="width: 27%; display: block; float: left; font-size: 14px; color: #333;font-weight:bold;">
                                                                        Whether an Outsourcing Contract :</label>
                                                                  <asp:Label runat="server" ID="lbloutsourcingcont" style="color: #333;margin-left: 61px;"></asp:Label>
                                                                   
                                                                </div>

                                                                <div class="form-group col-md-6">
                                                                   
                                                                </div>
                                                            </div>
                                                             <div class="row">
                                                                <div class="form-group col-md-6">
                                                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                    <label style="width: 32%; display: block; float: left; font-size: 14px; color: #333;font-weight:bold;">
                                                                        Entity/Location :</label>
                                                                     <asp:Label runat="server" ID="lbllocation" style="color: #333;margin-left: 32px;"></asp:Label>
                                                                 
                                                                </div>

                                                                <div class="form-group col-md-6">
                                                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                    <label style="width: 22%; display: block; float: left; font-size: 14px; color: #333;font-weight:bold;">
                                                                        Department :</label>
                                                                  <asp:Label runat="server" ID="lbldept" style="color: #333;margin-left: 93px;"></asp:Label>            

                                                                </div>

                                                            </div>
                                                           
                                                            <div class="row">
                                                                <div class="form-group col-md-6">
                                                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                    <label style="width: 27%; display: block; float: left; font-size: 14px; color: #333;font-weight:bold;">
                                                                        SPOC Name :</label>
                                                                  <asp:Label runat="server" ID="lblspocname" style="color: #333;margin-left: 59px;"></asp:Label>
                                                                </div>

                                                                <div class="form-group col-md-6">
                                                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                    <label style="width: 34%; display: block; float: left; font-size: 14px; color: #333;font-weight:bold;">
                                                                        SPOC Designation :</label>
                                                                  <asp:Label runat="server" ID="lblspocdesign" style="color: #333;margin-left: 23px;"></asp:Label>
                                                                </div>
                                                            </div>
                                                         
                                                             <div class="row">
                                                                <div class="form-group col-md-6">
                                                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                    <label style="width: 27%; display: block; float: left; font-size: 14px; color: #333;font-weight:bold;">
                                                                        Type of Entity :</label>
                                                                <asp:Label runat="server" ID="lblentitytype" style="color: #333;margin-left: 60px;"></asp:Label>
                                                                </div>
                                                               
                                                                <div class="form-group col-md-6">
                                                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                    <label style="width: 22%; display: block; float: left; font-size: 14px; color: #333;font-weight:bold;">
                                                                        Level of Attention Required :</label>
                                                                   <asp:Label runat="server" ID="lblattenreq" style="color: #333;margin-left: 93px;"></asp:Label>
                                                                </div>
                                                            </div>

                                                             <div class="row">
                                                                <div class="form-group col-md-12">
                                                                    <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                    <label style="display: block; float: left; font-size: 14px; color: #333;font-weight:bold;">
                                                                        Contract Objectives and other details that have been agreed and needs
                                                                        to be brought to the notice of Legal Department :</label>
                                                                    </label>

                                                                </div>
                                                            </div>
                                                             <div class="row">
                                                              <div class="form-group col-md-12">
                                                                <asp:Label runat="server" ID="lblconobjective" style="color: #333;margin-left: 233px;"></asp:Label>
                                                              </div>
                                                          </div>
                                                             <div class="row">
                                                                <div class="form-group col-md-6">
                                                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                    <label style="width: 27%; display: block; float: left; font-size: 14px; color: #333;font-weight:bold;">
                                                                        Risk assessment :</label>
                                                                   <asp:Label runat="server" ID="lblriskassessment" style="color: #333;margin-left: 61px;"></asp:Label>
                                                                </div>

                                                                <div class="form-group col-md-6">
                                                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                    <label style="width: 27%; display: block; float: left; font-size: 14px; color: #333;font-weight:bold;">
                                                                        Date :</label>
                                                                   <asp:Label runat="server" ID="lbldate" style="color: #333;margin-left: 60px;"></asp:Label>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="form-group col-md-6">
                                                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                    <label style="width: 15.5%; display: block; float: left; font-size: 14px; color: #333;font-weight:bold;">
                                                                        Define Risk :</label>
                                                                   <asp:Label runat="server" ID="lbldefinerisk" style="color: #333;margin-left: 125px;"></asp:Label>
                                                                </div>
                                                                <div class="form-group col-md-6">
                                                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                    <label style="width: 27%; display: block; float: left; font-size: 14px; color: #333;font-weight:bold;">
                                                                       Reply expected by :</label>
                                                                   <asp:Label runat="server" ID="lblTAT" style="color: #333;margin-left: 60px;"></asp:Label>
                                                                </div>
                                                            </div>
                                                        <div class="row">
                                                            <div class="form-group col-md-12">
                                                                <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <label style="width: 15.5%; display: block; float: left; font-size: 14px; color: #333;font-weight:bold;">
                                                                  Consequence of defined Risk :</label>
                                                               <asp:Label runat="server" ID="lblconrisk" style="color: #333;margin-left: 32px;"></asp:Label>
                                                              </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="form-group col-md-12">
                                                                <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <label style="width: 15.5%; display: block; float: left; font-size: 14px; color: #333;font-weight:bold;">
                                                                  Risk Mitigation Action :</label>
                                                               <asp:Label runat="server" ID="lblriskmitigation" style="color: #333;margin-left: 32px;"></asp:Label>
                                                              </div>
                                                        </div>
                                                            <div class="row">
                                                                <div class="form-group col-md-12">
                                                                    <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                    <label style="display: block; float: left; font-size: 14px; color: #333;font-weight:bold;">
                                                                        Contract Compliance - Clauses that has to complied with or specific Representation and warranties :
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="form-group col-md-12">
                                                                    <asp:Label runat="server" ID="lblconclauses" style="color: #333;margin-left: 230px;"></asp:Label>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="form-group col-md-12">
                                                                    <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                    <label style="width: 15.4%; display: block; float: left; font-size: 14px; color: #333;font-weight:bold;">
                                                                        Escalation process if any :</label>
                                                                   <asp:Label runat="server" ID="lblescprocess" style="color: #333;margin-left: 32px;"></asp:Label>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="form-group col-md-12">
                                                                    <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                    <label style="display: block; float: left; font-size: 14px; color: #333;font-weight:bold;">
                                                                        Exit/Termination plan(if any)- Important for the Owner department to contemplate at the inception.:</label>
                                                                    </label>

                                                                </div>
                                                            </div>
                                                        <%--  <div class="row">
                                                              <div class="form-group col-md-12">
                                                                  <asp:Label runat="server" ID="lblexiteterplan" style="color: #333;margin-left: 32px;"></asp:Label>
                                                              </div>
                                                          </div>--%>
                                                            <div class="row">
                                                                <div class="form-group col-md-6">
                                                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                    <label style="width: 27%; display: block; float: left; font-size: 14px; color: #333;font-weight:bold;">
                                                                        Termination Notice period :</label>
                                                                   <asp:Label runat="server" ID="lbltermnoticeperiod" style="color: #333;margin-left: 58px;"></asp:Label>
                                                                </div>

                                                                <div class="form-group col-md-6">
                                                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                    <label style="width: 28%; display: block; float: left; font-size: 14px; color: #333;font-weight:bold;">
                                                                        Any other Termination Plan :</label>
                                                                    <asp:Label runat="server" ID="lblothertermplam" style="color: #333;margin-left: 53px;"></asp:Label>
                                                                </div>
                                                            </div>
                                                             <div class="row">
                                                                <div class="form-group col-md-12">
                                                                    <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                    <label style="display: block; float: left; font-size: 14px; color: #333;font-weight:bold;">
                                                                        Audit approval if any - important for the Owner department to contemplate at the inception :</label>
                                                                    </label>

                                                                </div>
                                                            </div>
                                                          <div class="row">
                                                              <div class="form-group col-md-12">
                                                                 <asp:Label runat="server" ID="lblauitapproval" style="color: #333;margin-left: 230px;"></asp:Label>
                                                              </div>
                                                          </div>
                                                           <div class="row">
                                                                <div class="form-group col-md-12">
                                                                    <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                    <label style="display: block; float: left; font-size: 14px; color: #333;font-weight:bold;">
                                                                       Intellectual Property Rights(IPR) - Important for the Owner department to contemplate at the inception.
                                                                  </label>
                                                                 </div>
                                                            </div>
                                                              <div class="row">
                                                                <div class="form-group col-md-12">
                                                                    <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                    <label style="display: block; float: left; font-size: 14px; color: #333;font-weight:bold;">
                                                                       Any type of Services/Product specifically developed for EGIC :
                                                                  </label>
                                                                 </div>
                                                            </div>
                                                             <div class="row">
                                                                <div class="form-group col-md-6" id="div5" runat="server">
                                                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                    <label style="width: 26%; display: block; float: left; font-size: 14px; color: #333;font-weight:bold;">
                                                                    </label>
                                                                       <asp:Label runat="server" ID="lblEGIC" style="color: #333;margin-left: 61px;"></asp:Label>
                                                                    
                                                                </div>
                                                            </div>
                                                            <div class="row" id="div6" runat="server">
                                                                <div class="form-group col-md-12" id="div7" runat="server">
                                                                    <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                    <label style="width: 15%; display: block; float: left; font-size: 14px; color: #333;font-weight:bold;">
                                                                        Additional details :</label>
                                                                    <asp:Label runat="server" ID="lblEGICdetails" style="color: #333;margin-left: 32px;"></asp:Label>
                                                                </div>
                                                            </div>
                                                             <div class="row">
                                                                <div class="form-group col-md-6">
                                                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                    <label style="width: 26%; display: block; float: left; font-size: 14px; color: #333;font-weight:bold;">
                                                                        The IPR rights of service/product developed for EGIC :</label>
                                                                   <asp:Label runat="server" ID="lblIPRRights" style="color: #333;margin-left: 56px;"></asp:Label>
                                                                  
                                                                </div>
                                                               
                                                                <div class="form-group col-md-6">
                                                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                    <label style="width: 22%; display: block; float: left; font-size: 14px; color: #333;font-weight:bold;">
                                                                        Any Intellectual Property to be shared in course of arrangement :</label>
                                                                   <asp:Label runat="server" ID="lblinteproperty" style="color: #333;margin-left: 86px;"></asp:Label>
                                                                </div>
                                                            </div>
                                                          
                                                         
                                                         <div class="row" id="div8" runat="server">
                                                            <div class="form-group col-md-12">
                                                                <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <label style="width: 15%; display: block; float: left; font-size: 14px; color: #333;font-weight:bold;">
                                                                    Additional details :</label>
                                                               <asp:Label runat="server" ID="lbladddetails" style="color: #333;margin-left: 32px;"></asp:Label>
                                                            </div>
                                                        </div>
                                                            <div class="row">
                                                                <div class="form-group col-md-6">
                                                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                    <label style="width: 26%; display: block; float: left; font-size: 14px; color: #333; font-weight: bold;">
                                                                        Reviewer :</label>
                                                                    <asp:Label runat="server" ID="lblreviewer" Style="color: #333; margin-left: 56px;"></asp:Label>

                                                                </div>

                                                                <div class="form-group col-md-6" >
                                                                     <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                    <label style="width: 22%; display: block; float: left; font-size: 14px; color: #333;font-weight:bold;">
                                                                      Approver :</label>
                                                                       <asp:Label runat="server" ID="lblapprover" Style="color: #333; margin-left: 56px;"></asp:Label>
                                                           
                                                                        
                                                                </div>
                                                            </div>
                                                            <div class="row" id="CommentContract" runat="server">
                                                                <div class="col-md-12" style="padding-top: 16px;">
                                                                    <div class="panel panel-default" style="border: 1px solid;">
                                                                        <div class="panel-heading" style="padding: 4px 19px; background-color: #1fd9e1; color: white">Comments</div>
                                                                        <div class="panel-body" style="padding: 9px; overflow-y: scroll; min-height: 300px; max-height: 301px;">
                                                                            <div id="ShowDatadiv" style="min-height: 300px; color: black;"></div>
                                                                        </div>
                                                                        <div class="componentWraper">
                                                                            <div style="height: auto">
                                                                                <textarea id="TxtComments" style="width: 99%; border: none;" name="txtcmt" placeholder="Write a comment.."></textarea>
                                                                                <hr />
                                                                                <div>
                                                                                    <label for="file-input">
                                                                                        <a style="cursor: pointer; font-size: x-large;" id="file"><i class="fa fa-paperclip" data-toggle="tooltip" title="Attach a file to the comment"></i></a>
                                                                                    </label>
                                                                                    <label id="selectedfile" style="color: black; margin-left: 32px;"></label>
                                                                                    <%--<a onclick="submitData();" style="cursor: pointer; font-size: x-large;" id="file"><i class="fa fa-paperclip" data-toggle="tooltip" title="Attach file"></i></a>--%>
                                                                                    <button onclick="submitData();"  class="k-button" style="cursor: pointer; float: right; background: #304FFE; color: white; height: 32px; width: 93px; border: none;" id="MainSubmit">Comment</button>
                                                                                </div>
                                                                                <div class="image-upload" id="" style="display: none;">
                                                                                    <input id="file-input" type="file" onchange="alertFilename()" style="padding-top: 10px;" />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <div class="form-group col-md-12 text-center">
                                                                <asp:Button Text="Create Contract" runat="server" ID="btnCreateContract" CssClass="btn btn-primary"
                                                                    onclick="btnCreateContract_Click"></asp:Button>
                                                                
                                                            </div>

                                                        </asp:Panel>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                              
                        </div>
                    </asp:View>

                     <asp:View ID="AuditLogView" runat="server">
                        <div style="width: 100%; float: left; margin-bottom: 15px">
                            <div class="container">
                                <div id="SixthTabAccordion">
                                    <div class="row Dashboard-white-widget">
                                        <div class="col-lg-12 col-md-12">
                                            <div class="panel panel-default">
                                                <div class="row" style="margin-top: 20px;">

                                                    <div id="collapseDivAuditLog" class="panel-collapse collapse in">
                                                        <div class="panel-body">
                                                            <asp:UpdatePanel ID="upCaseAuditLog" runat="server">
                                                                <ContentTemplate>
                                                                    <div class="row">
                                                                        <div style="margin-bottom: 7px">
                                                                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                                                                ValidationGroup="validationCaseAuditLog" />
                                                                            <asp:CustomValidator ID="cvCaseAuditLog" runat="server" EnableClientScript="False"
                                                                                ValidationGroup="validationCaseAuditLog" Display="None" />
                                                                        </div>
                                                                    </div>
                                                                    <div style="margin-left: 94%;">

                                                                        <asp:UpdatePanel ID="UpdatePanel11" runat="server" UpdateMode="Conditional">
                                                                            <ContentTemplate>
                                                                                <asp:LinkButton Text="Apply" runat="server" ID="btnExport" OnClick="btnExport_Click" Width="80%" Style="margin-top: 5px;" data-toggle="tooltip" ToolTip="Export to Excel">
                                                                                      <img src="../../Images/Excel _icon.png" alt="Export to Excel" title="Export to Excel" /> 
                                                                                </asp:LinkButton>
                                                                            </ContentTemplate>
                                                                            <Triggers>
                                                                                <asp:PostBackTrigger ControlID="btnExport" />
                                                                            </Triggers>
                                                                        </asp:UpdatePanel>
                                                                    </div>
                                                                    <asp:Panel ID="Panel2" runat="server">
                                                                        <div class="form-group col-md-12">
                                                                            <asp:GridView runat="server" ID="gvContractAuditLog" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                                                GridLines="None" PageSize="10" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%" ShowFooter="false"
                                                                                PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Right">
                                                                                <Columns>
                                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr No." ItemStyle-Width="10%" FooterStyle-Width="10%">
                                                                                        <ItemTemplate>
                                                                                            <%#Container.DataItemIndex+1 %>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>

                                                                                     <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Created By" ItemStyle-Width="25%" HeaderStyle-Width="25%">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lblCreatedBy" runat="server" Text='<%# Eval("FromUserName")%>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>

                                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Action taken" ItemStyle-Width="40%" HeaderStyle-Width="40%">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lblRemark" runat="server" Text='<%# Eval("Comment")%>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>

                                                                                   

                                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Created On" ItemStyle-Width="25%" HeaderStyle-Width="25%">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lblCreatedOn" runat="server" Text='<%# Eval("CreatedOn") != DBNull.Value ? Convert.ToDateTime(Eval("CreatedOn")).ToString("dd-MM-yyyy hh:mm:ss:tt") : ""%>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                </Columns>
                                                                                <RowStyle CssClass="clsROWgrid" />
                                                                                <HeaderStyle CssClass="clsheadergrid" />
                                                                                <EmptyDataTemplate>
                                                                                    No Records Found
                                                                                </EmptyDataTemplate>
                                                                            </asp:GridView>
                                                                        </div>
                                                                    </asp:Panel>
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:View>
                    </asp:MultiView>
                 </div>
        </div>
            <div>
                <div class="modal fade" id="DocumentViewer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden;">
                    <div class="modal-dialog w100per">
                        <div class="modal-content" style="width: 1043px;margin-left: -219px;">
                            <div class="modal-header">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                <label class="modal-header-custom">
                                    View Document(s)</label>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            </div>
                            <div class="modal-body" style="height: 570px;">
                                <div class="col-md-12 colpadding0">
                                    <div class="clearfix"></div>
                                    <div class="col-md-1 colpadding0">
                                        <table width="100%" style="text-align: left; margin-left: 5%;">
                                            <thead>
                                                <tr>
                                                    <td valign="top">
                                                        <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdatleMode="Conditional">
                                                            <ContentTemplate>
                                                                <asp:Repeater ID="rptDocmentVersionView" runat="server" OnItemCommand="rptDocmentVersionView_ItemCommand"
                                                                    OnItemDataBound="rptDocmentVersionView_ItemDataBound">
                                                                    <HeaderTemplate>
                                                                        <table id="tblComplianceDocumnets">
                                                                            <thead>
                                                                                <th>File Name</th>
                                                                            </thead>
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdatleMode="Conditional">
                                                                                    <ContentTemplate>
                                                                                        <asp:LinkButton CommandName="View" CommandArgument='<%# Eval("ID") %>' ID="lblDocumentVersionView"
                                                                                            runat="server" ToolTip='<%# Eval("FileName")%>' Text='<%# Eval("FileName").ToString() %>'></asp:LinkButton>
                                                                                    </ContentTemplate>
                                                                                    <Triggers>
                                                                                        <asp:AsyncPostBackTrigger ControlID="lblDocumentVersionView" />
                                                                                    </Triggers>
                                                                                </asp:UpdatePanel>
                                                                            </td>
                                                                        </tr>
                                                                    </ItemTemplate>
                                                                    <FooterTemplate>
                                                                        </table>
                                                                    </FooterTemplate>
                                                                </asp:Repeater>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:AsyncPostBackTrigger ControlID="rptDocmentVersionView" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>

                                    <div class="col-md-11 colpadding0">
                                        <asp:Label runat="server" ID="lblMessage" Style="color: red;"></asp:Label>
                                        <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; height: 550px; width: 100%;">
                                            <iframe src="about:blank" id="IFrameDocumentViewer" runat="server" width="100%" height="535px"></iframe>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal fade" id="DocumentPriview" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden; top: -3%;">
                    <div class="modal-dialog" style="width: 100%">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" onclick="fclosedDocumentPriview();" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body" style="height: 570px;">
                                <div style="width: 100%;">
                                    <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; height: 550px; width: 100%;">
                                        <iframe src="about:blank" id="docPriview" runat="server" width="100%" height="535px"></iframe>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="divShowDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bgColor-gray" style="height: 35px;">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label id="lblname" runat="server" class="modal-header-custom col-md-6 plr0"></label>
                        <%--Add New Contract--%>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="closeModal();RefreshParent();">&times;</button>
                    </div>

                    <div class="modal-body" style="background-color: #f7f7f7;">
                        <iframe id="showdetails" src="about:blank" width="95%" height="80%" frameborder="0"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
