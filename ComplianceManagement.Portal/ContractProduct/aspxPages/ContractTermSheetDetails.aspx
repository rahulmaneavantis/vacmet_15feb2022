﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ContractProduct.Master" AutoEventWireup="true" CodeBehind="ContractTermSheetDetails.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.aspxPages.ContractTermSheetDetails" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <script type="text/javascript">
        $(document).ready(function () {
            setactivemenu('leftworkspacemenu');
            fhead('My Workspace/ Contract Initiator(s)');
        });

       
        function ShowDialog(Flag, contractInitiatorID) {
            $('#divShowDialog').modal('show');
            $('#showdetails').attr('width', '100%');
            $('#showdetails').attr('height', '900px');
            $('.modal-dialog').css('width', '100%');
            //$('#showdetails').attr('src', "/ContractProduct/aspxPages/ContractTermSheet.aspx");
            $('#showdetails').attr('src', "/ContractProduct/aspxPages/ContractTermSheetNew.aspx?Flag=" + Flag + "&contractInitiatorID="+contractInitiatorID);
        };
        
        function RefreshParent() {
            window.location.href = window.location.href;
        }
        </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
   <asp:UpdatePanel ID="upComplianceTypeList" runat="server" UpdateMode="Conditional"
        OnLoad="upComplianceTypeList_Load">
        <ContentTemplate>

            <div id="maindiv">

           
           
             <div class="row">
                <asp:ValidationSummary ID="vsEntities" runat="server" Display="none" class="alert alert-block alert-danger fade in" 
                    ValidationGroup="EntitiesValidationGroup" />
                <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                    ValidationGroup="EntitiesValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
            </div>



       <div class="row Dashboard-white-widget mb0">
      
           <div class="col-md-2 colpadding0 w25per">
           

            <div class="col-md-12" style="margin-left: 980px;">
                <asp:LinkButton CssClass="btn btn-primary" runat="server" ID="btnAddContract"
                    data-toggle="tooltip" data-placement="bottom" ToolTip="Add New Contract" OnClick="btnAddContract_Click"><span class="AddNewspan1"><i class='fa fa-plus'></i></span>&nbsp;New</asp:LinkButton>
              
            </div>
        </div>
           <div style="margin-bottom: 4px">
                <asp:GridView runat="server" ID="grdContractor" AutoGenerateColumns="false" AllowSorting="true" OnRowCommand="grdContractor_RowCommand"
                    PageSize="10" AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%" ShowHeaderWhenEmpty="true"
                  OnrowDataBound="grdContractor_RowDataBound"  DataKeyNames="ID">
                    <Columns>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr.No." ItemStyle-Width="5%">
                            <ItemTemplate>
                                <%#Container.DataItemIndex+1 %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Contract_no" HeaderText="Contract No." SortExpression="Contract_no" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="15%" />
                         <asp:BoundField DataField="CustomerBranch" HeaderText="Entity/Branch/Location" SortExpression="Branch" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="20%" />
                          <asp:BoundField DataField="DepartmentName" HeaderText="Department Name" SortExpression="DepartmentName" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="20%" />
                  
                        <asp:TemplateField HeaderText="Start Date" ItemStyle-Width="15%" ItemStyle-CssClass="text-center">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Startdate") != null? Convert.ToDateTime(Eval("Startdate")).ToString("dd-MM-yyyy") : " " %>'
                                        ToolTip='<%# Eval("Startdate") != null ? Convert.ToDateTime(Eval("Startdate")).ToString("dd-MM-yyyy") : " " %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="End Date" ItemStyle-Width="15%" ItemStyle-CssClass="text-center">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Enddate") != null? Convert.ToDateTime(Eval("Enddate")).ToString("dd-MM-yyyy") : " " %>'
                                        ToolTip='<%# Eval("Enddate") != null ? Convert.ToDateTime(Eval("Enddate")).ToString("dd-MM-yyyy") : " " %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="15%">

                            <ItemTemplate>
                               <asp:UpdatePanel runat="server" ID="upContract" UpdateMode="Conditional">
                                   <ContentTemplate>
                                        <asp:LinkButton ID="lnkEditContract" runat="server" CommandName="EditContract_Initiator"
                                    CommandArgument='<%# Eval("Id") %>' OnClick="lnkEditContract_Click" 
                                    data-toggle="tooltip" data-placement="bottom" ToolTip="View/Edit Contract Initiator">
                                    <img src='<%# ResolveUrl("~/Images/edit_icon_new.png")%>' alt="Edit"/>
                                </asp:LinkButton>

                                <asp:LinkButton ID="LinkButton2" runat="server" Visible="false" CommandName="DELETE_ContractInitiator"
                                    CommandArgument='<%# Eval("Id") %>' OnClientClick="return confirm('Are you certain you want to delete Contract Initiator?');">
                                    <img src='<%# ResolveUrl("../../Images/delete_icon_new.png")%>' alt="Delete Contract Initiator" title="Delete Contract Initiator" /></asp:LinkButton>
                                   </ContentTemplate>
                                <%--   <Triggers>
                                       <asp:PostBackTrigger ControlID="LinkButton2" />
                                   </Triggers>--%>
                               </asp:UpdatePanel>
                               
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="clsROWgrid" />
                    <HeaderStyle CssClass="clsheadergrid" />
                    <PagerSettings Visible="false" />
                    <EmptyDataTemplate>
                        No Record Found
                    </EmptyDataTemplate>
                </asp:GridView>
            </div>
        </div>

      <div class="modal fade" id="divShowDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog" style="width:110%;height:500px;">
            <div class="modal-content">
                <div class="modal-header bgColor-gray" style="height: 43px;">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label id="lblname" runat="server" class="modal-header-custom col-md-6 plr0"> </label>
                      
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="RefreshParent()">&times;</button>
                </div>

                <div class="modal-body" style="margin-top:-13px;background-color: #f7f7f7;">
                    <iframe id="showdetails" src="about:blank"  frameborder="0"></iframe>
                </div>
            </div>
        </div>
    </div>

              <div class="row">
                <div class="col-md-12 colpadding0">
                    <div class="col-md-8 colpadding0">
                        <div runat="server" id="DivRecordsScrum" style="float: left; margin-top: 5px; color: #999">
                            <p style="padding-right: 0px !Important;">                                
                                <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>- 
                                <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                                <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                            </p>
                        </div>
                    </div>
                    <div class="col-md-1 colpadding0">
                        <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control" Style="width:100%; float: right; margin-right:-106%;"
                            AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" >
                            <asp:ListItem Text="5" />
                            <asp:ListItem Text="10" Selected="True" />
                            <asp:ListItem Text="20" />
                            <asp:ListItem Text="50" />
                        </asp:DropDownList>
                    </div>
                   
                    <div class="col-md-2 colpadding0" style="float: right;">
                        <div style="float: left; width: 50%">
                            <p class="clsPageNo">Page</p>
                        </div>
                        <div style="float: left; width: 50%">
                            <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true" DataPlaceHolder="No" AllowSingleDeselect="false"
                                class="form-control m-bot15" Width="100%" Height="30px" OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">
                            </asp:DropDownListChosen>
                        </div>
                    </div>
                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                </div>
            </div>
            
        </div>
       </ContentTemplate>
  </asp:UpdatePanel>
</asp:Content>
