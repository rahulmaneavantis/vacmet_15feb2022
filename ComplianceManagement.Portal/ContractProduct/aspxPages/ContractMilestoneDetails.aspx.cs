﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Ionic.Zip;

namespace com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.aspxPages
{
    public partial class ContractMilestoneDetails : System.Web.UI.Page
    {
        public static string DocumentPath = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["AccessID"]))
                {
                    var contractInstanceID = Request.QueryString["AccessID"];
                    if (contractInstanceID != "")
                    {
                        ViewState["ContractInstanceID"] = contractInstanceID;
                        int customerID = -1;
                        customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                        int UserID = -1;
                        UserID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                        BindMilestonedrp(Convert.ToInt64(contractInstanceID), customerID);

                        BindDepartments();
                        binduser();
                        BindContractMilestone(customerID, Convert.ToInt64(contractInstanceID), UserID);
                    }
                }
            }
        }
        public void BindMilestonedrp(long contractID, long customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var getrecord = (from row in entities.Cont_tbl_MilestoneDetail
                                 where row.ContractTemplateID == contractID
                                 && row.CustomerID == customerID
                                 && row.IsActive == true
                                 select row).ToList();

                drpdownmilestone.DataValueField = "ID";
                drpdownmilestone.DataTextField = "Title";
                drpdownmilestone.DataSource = getrecord;
                drpdownmilestone.DataBind();
                drpdownmilestone.Items.Insert(0, new ListItem("Select Milestone", "-1"));
                //drpdownmilestone.Items.Add(new ListItem("Select Milestone", "-1"));
            }
        }

        public void BindContractMilestone(int customerID, long contractID,int UserID)
        {
            try
            {
                var lstMilestones = ContractTaskManagement.GetContractMileStone(customerID, contractID, UserID);

                if (lstMilestones.Count > 0)
                    lstMilestones = lstMilestones.OrderBy(row => row.StartDate).ToList();

                int MilestoneID = -1;
                int MilestoneStatusID = -1;
                if (dropmilestonestatus.SelectedValue != "-1" && dropmilestonestatus.SelectedValue != "")
                {
                    MilestoneStatusID = Convert.ToInt32(dropmilestonestatus.SelectedValue);
                    lstMilestones = lstMilestones.Where(x => x.StatusID == MilestoneStatusID).ToList();
                }
                if (drpdownmilestone.SelectedValue != "-1" && drpdownmilestone.SelectedValue != "")
                {
                    MilestoneID = Convert.ToInt32(drpdownmilestone.SelectedValue);
                    lstMilestones = lstMilestones.Where(x => x.MilestoneMasterID == MilestoneID).ToList();
                }

                //if (lstMilestones.Count > 0)
                //    lstMilestones = lstMilestones.OrderByDescending(row => row.CreatedOn).ToList();

                gridMilestone.DataSource = lstMilestones;
                gridMilestone.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvTaskTab.IsValid = false;
                //cvTaskTab.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }
        protected void gridMilestone_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                if (e.CommandArgument != null)
                {
                    if (ViewState["ContractInstanceID"] != null)
                    {
                        long contractID = Convert.ToInt64(ViewState["ContractInstanceID"]);
                        long MilestoneID = Convert.ToInt64(e.CommandArgument);

                        if (e.CommandName.Equals("EditMilestone"))
                        {
                            ViewState["MilestoneID"] = MilestoneID;

                            if (MilestoneID != 0 && contractID != 0)
                            {
                                var milestoneDetailRecord = ContractTaskManagement.GetMilestoneDetailsByID(contractID, MilestoneID, customerID);
                                if (milestoneDetailRecord != null)
                                {
                                    txtboxMilestonetitle.Text = milestoneDetailRecord[0].Title;
                                    txtboxMilestonedescription.Text = milestoneDetailRecord[0].Description;


                                    drpmilestoneDepartment.SelectedValue = milestoneDetailRecord[0].DeptID.ToString();
                                    lblmilestoneDepartment.Text = drpmilestoneDepartment.SelectedItem.ToString();

                                    drpmilestoneDepartmentUser.SelectedValue = milestoneDetailRecord[0].UserID.ToString();
                                    lblmilestoneUser.Text = drpmilestoneDepartmentUser.SelectedItem.ToString();

                                    txbmilestoneduedate.Text = milestoneDetailRecord[0].DueDate.ToString("dd-MM-yyyy");
                                    drpmilestonestatus.SelectedValue = milestoneDetailRecord[0].StatusID.ToString();

                                    tbxremarkmilestone.Text = "";

                                    BindMilestoneResponses(contractID, MilestoneID);
                                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenMilestonePopup();", true);                                  
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvMilestoneTab.IsValid = false;
                //cvMilestoneTab.ErrorMessage = "Something went wrong, Please try again";
            }
        }
        public void binduser()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                var lstAllUsers = ContractUserManagement.GetAllUsers_Contract(customerID);

                if (lstAllUsers.Count > 0)
                    lstAllUsers = lstAllUsers.Where(entry => entry.ContractRoleID != null).ToList();

                var allUsers = ContractUserManagement.GetRequiredUsers(lstAllUsers, 4); //4--All---Internal and External Users

                drpmilestoneDepartmentUser.DataValueField = "ID";
                drpmilestoneDepartmentUser.DataTextField = "Name";
                drpmilestoneDepartmentUser.DataSource = allUsers;
                drpmilestoneDepartmentUser.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void BindDepartments()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            var lstDepts = CompDeptManagement.GetAllDepartmentMasterList(customerID);

            if (lstDepts.Count > 0)
                lstDepts = lstDepts.OrderBy(row => row.Name).ToList();

            drpmilestoneDepartment.DataTextField = "Name";
            drpmilestoneDepartment.DataValueField = "ID";

            drpmilestoneDepartment.DataSource = lstDepts;
            drpmilestoneDepartment.DataBind();
            //ddlDepartment.Items.Add(new ListItem("Add New", "0"));
        }

        protected void btnSaveMilestone_Click(object sender, EventArgs e)
        {
            try
            {
                bool validateData = false;
                bool isBlankFile = false;
                long contractID = -1;
                if (!string.IsNullOrEmpty(Request.QueryString["AccessID"]))
                {
                    contractID = Convert.ToInt32(Request.QueryString["AccessID"]);
                }

                long MilestoneID = -1;
                if (!string.IsNullOrEmpty(Request.QueryString["AccessID"]))
                {
                    MilestoneID = Convert.ToInt32(ViewState["MilestoneID"]);
                }

                if (contractID != 0 && MilestoneID != 0)
                {          
                    int customerID = -1;
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                    int userID = -1;
                    userID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                                                    
                    HttpFileCollection fileCollection = Request.Files;
                    if (fileCollection.Count > 0)
                    {
                        string[] InvalidFileTypes = { "exe", "bat", "dll", "css", "js", "jsp", "php5", "pht", "asa", "cer", "asax", "swf", "xap", "aspx", "asp", "php", "reg", "rdp", };
                        for (int i = 0; i < fileCollection.Count; i++)
                        {
                            HttpPostedFile uploadfile = null;
                            uploadfile = fileCollection[i];
                            int filelength = uploadfile.ContentLength;
                            string fileName = System.IO.Path.GetFileName(uploadfile.FileName);
                            string ext = System.IO.Path.GetExtension(uploadfile.FileName);
                            if (!string.IsNullOrEmpty(fileName))
                            {
                                if (filelength == 0)
                                {
                                    isBlankFile = true;
                                    break;
                                }
                                else if (ext == "")
                                {
                                    isBlankFile = true;
                                    break;
                                }
                                else
                                {
                                    if (ext != "")
                                    {
                                        for (int j = 0; j < InvalidFileTypes.Length; j++)
                                        {
                                            if (ext == "." + InvalidFileTypes[j])
                                            {
                                                isBlankFile = true;
                                                break;
                                            }
                                        }
                                    }

                                }
                            }
                        }
                    }

                    if (drpmilestonestatus.SelectedValue != "" && drpmilestonestatus.SelectedValue != "-1" && drpmilestonestatus.SelectedValue != "0")
                    {
                        if (tbxremarkmilestone.Text != "")
                        {
                            if (isBlankFile == false)
                            {
                                validateData = true;

                            }
                            else
                            {
                                cvTaskResponse.IsValid = false;
                                cvTaskResponse.ErrorMessage = "Invalid file error. System does not support uploaded file.Please upload another file.";
                                return;
                            }
                        }
                        else
                        {
                            cvTaskResponse.IsValid = false;
                            cvTaskResponse.ErrorMessage = "Required Comment";
                            return;
                        }
                    }
                    else
                    {
                        cvTaskResponse.IsValid = false;
                        cvTaskResponse.ErrorMessage = "Select Status";
                        return;
                    }

                    if (validateData)//validation
                    {
                        #region Update information
                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        {
                            var updateobj = (from row in entities.Cont_tbl_MilestoneInstance
                                             where row.ContractTemplateID == contractID
                                             && row.ID == MilestoneID
                                             && row.CustomerID == customerID
                                             select row).FirstOrDefault();

                            if (updateobj != null)
                            {
                                updateobj.StatusID = Convert.ToInt32(drpmilestonestatus.SelectedValue);
                                updateobj.Remark = tbxremarkmilestone.Text;
                                updateobj.UpdateBy = userID;
                                updateobj.UpdatedOn = DateTime.Now;
                                entities.SaveChanges();

                                Cont_tbl_MileStoneTransaction Transactionobj = new Cont_tbl_MileStoneTransaction
                                {
                                    ContractID = contractID,
                                    //CustomerID = customerID,
                                    MileStoneID = MilestoneID,
                                    StatusID = Convert.ToInt32(drpmilestonestatus.SelectedValue),
                                    Remark = tbxremarkmilestone.Text,
                                    IsActive = true,
                                    CreatedBy = userID,
                                    CreatedOn = DateTime.Now,
                                };
                                entities.Cont_tbl_MileStoneTransaction.Add(Transactionobj);
                                entities.SaveChanges();

                                int MilestoneResponceID = Convert.ToInt32(Transactionobj.ID);

                                if (fuTaskResponseDocUpload.HasFiles)
                                {
                                    UploadDocuments(Convert.ToInt32(MilestoneResponceID), Convert.ToInt32(MilestoneID), customerID, userID, contractID, Request.Files, "fuTaskResponseDocUpload");
                                }
                                int UserID = -1;
                                UserID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);

                                BindContractMilestone(customerID, Convert.ToInt64(contractID), UserID);
                            }
                        }
                        #endregion
                    }
                }                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected bool UploadDocuments(int MilestoneResponceID,int milestone,int customerID, int userID, long contractID, HttpFileCollection fileCollection, string fileUploadControlName)
        {
            bool uploadSuccess = false;
            try
            {
                if (customerID != null)
                {
                    string directoryPath = string.Empty;
                    string fileName = string.Empty;

                    Cont_tbl_FileData objContDoc = new Cont_tbl_FileData()
                    {
                        ContractID = contractID,
                        DocTypeID = 0,
                        TaskID = null,
                        TaskResponseID = null,
                        MilestoneID = milestone,
                        MilestoneResponceID= MilestoneResponceID,
                        CustomerID = customerID,
                        CreatedBy = userID,
                        CreatedOn = DateTime.Now,
                        IsDeleted = false,
                    };

                    if (fileCollection.Count > 0)
                    {
                        List<KeyValuePair<string, Byte[]>> fileList = new List<KeyValuePair<string, Byte[]>>();

                        if (contractID > 0)
                        {
                            for (int i = 0; i < fileCollection.Count; i++)
                            {
                                HttpPostedFile uploadedFile = fileCollection[i];

                                if (uploadedFile.ContentLength > 0)
                                {
                                    string[] keys1 = fileCollection.Keys[i].Split('$');

                                    if (keys1[keys1.Count() - 1].Equals(fileUploadControlName))
                                    {
                                        fileName = uploadedFile.FileName;
                                    }

                                    objContDoc.FileName = fileName;

                                    //Get Document Version
                                    var contractDocVersion = ContractDocumentManagement.ExistsContractDocumentReturnVersion(objContDoc);

                                    contractDocVersion++;
                                    objContDoc.Version = contractDocVersion + ".0";

                                    if (milestone != -1)
                                        directoryPath = Server.MapPath("~/ContractDocuments/" + customerID + "/" + Convert.ToInt32(contractID) + "/" + milestone + "/" + objContDoc.Version);
                                    
                                    if (!Directory.Exists(directoryPath))
                                        Directory.CreateDirectory(directoryPath);

                                    Guid fileKey1 = Guid.NewGuid();
                                    string finalPath1 = Path.Combine(directoryPath, fileKey1 + Path.GetExtension(uploadedFile.FileName));
                                    Stream fs = uploadedFile.InputStream;
                                    BinaryReader br = new BinaryReader(fs);
                                    Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                                    fileList.Add(new KeyValuePair<string, Byte[]>(finalPath1, bytes));

                                    objContDoc.FilePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                    objContDoc.FileKey = fileKey1.ToString();
                                    objContDoc.VersionDate = DateTime.Now;
                                    objContDoc.CreatedOn = DateTime.Now;
                                    objContDoc.FileSize = uploadedFile.ContentLength;
                                    DocumentManagement.Contract_SaveDocFiles(fileList);
                                    long newFileID = ContractDocumentManagement.CreateContractDocumentMapping(objContDoc);

                                    uploadSuccess = true;
                                    
                                    fileList.Clear();
                                }

                            }//End For Each
                        }
                    }
                }

                return uploadSuccess;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return uploadSuccess;
            }  
        }

        public void BindMilestoneResponses(long contractID, long MilestoneID)
        {
            try
            {
                List<Cont_SP_GetMilestoneResponses_All_Result> lstResponses = new List<Cont_SP_GetMilestoneResponses_All_Result>();

                lstResponses = ContractTaskManagement.GetMilestoneResponses(contractID, MilestoneID);

                if (lstResponses != null && lstResponses.Count > 0)
                {
                    lstResponses = lstResponses.OrderByDescending(row => row.CreatedOn).ToList();
                }

                grdmilestonekResponseLog.DataSource = lstResponses;
                grdmilestonekResponseLog.DataBind();

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvTaskResponse.IsValid = false;
                //cvTaskResponse.ErrorMessage = "Something went wrong, Please try again";
            }
        }
        public string ShowMilestoneResponseDocCount(long contractID, long MilestoneID, long MilestoneResponseID)
        {
            try
            {
                var docCount = ContractTaskManagement.GetMilestoneResponseDocuments(contractID, MilestoneID, MilestoneResponseID).Count;

                if (docCount == 0)
                    return "No Documents";
                else if (docCount == 1)
                    return "1 File";
                else if (docCount > 1)
                    return docCount + " Files";
                else
                    return "";
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }

        protected void grdmilestonekResponseLog_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandArgument != null)
                {
                    if (e.CommandName.Equals("DownloadTaskResponseDoc"))
                    {
                        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                        long milestoneResponseID = Convert.ToInt64(commandArgs[0]);
                        long milestoneID = Convert.ToInt64(commandArgs[1]);
                        long contractID = Convert.ToInt64(commandArgs[2]);

                        if (milestoneResponseID != 0 && milestoneID != 0 && contractID != 0)
                        {
                            var lstTaskResponseDocument = ContractTaskManagement.GetMilestoneResponseDocuments(contractID, milestoneID, milestoneResponseID);

                            if (lstTaskResponseDocument.Count > 0)
                            {
                                using (ZipFile responseDocZip = new ZipFile())
                                {
                                    int i = 0;
                                    foreach (var file in lstTaskResponseDocument)
                                    {
                                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            int idx = file.FileName.LastIndexOf('.');
                                            string str = file.FileName.Substring(0, idx) + "_" + file.Version + "." + file.FileName.Substring(idx + 1);

                                            if (!responseDocZip.ContainsEntry(file.DocTypeID + "/" + str))
                                            {
                                                if (file.EnType == "M")
                                                {
                                                    responseDocZip.AddEntry(file.DocTypeID + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                else
                                                {
                                                    responseDocZip.AddEntry(file.DocTypeID + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                            }
                                            i++;
                                        }
                                    }

                                    var zipMs = new MemoryStream();
                                    responseDocZip.Save(zipMs);
                                    zipMs.Position = 0;
                                    byte[] Filedata = zipMs.ToArray();
                                    Response.Buffer = true;
                                    Response.ClearContent();
                                    Response.ClearHeaders();
                                    Response.Clear();
                                    Response.ContentType = "application/zip";
                                    Response.AddHeader("content-disposition", "attachment; filename=MilestoneResponseDocuments-" + DateTime.Now.ToString("dd-MM-yyyy") + ".zip");
                                    Response.BinaryWrite(Filedata);
                                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                                }
                            }
                            else
                            {
                                //cvTaskResponse.IsValid = false;
                                //cvTaskResponse.ErrorMessage = "No Document Available for Download.";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "rebindTaskUser", "OpenMilestonePopup();", true);
                            }
                        }
                    }
                    else if (e.CommandName.Equals("ViewTaskResponseDoc"))
                    {
                            string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                            long milestoneResponseID = Convert.ToInt64(commandArgs[0]);
                            long milestoneID = Convert.ToInt64(commandArgs[1]);
                            long contractID = Convert.ToInt64(commandArgs[2]);

                        if (milestoneResponseID != 0 && milestoneID != 0 && contractID != 0)
                        {
                            var lstTaskDocument = ContractTaskManagement.GetMilestoneResponseDocuments(contractID, milestoneID, milestoneResponseID);

                            if (lstTaskDocument != null)
                            {
                                List<Cont_tbl_FileData> entitiesData = lstTaskDocument.Where(entry => entry.Version != null).ToList();

                                if (lstTaskDocument.Where(entry => entry.Version == null).ToList().Count > 0)
                                {
                                    Cont_tbl_FileData entityData = new Cont_tbl_FileData();
                                    entityData.Version = "1.0";
                                    entityData.ContractID = contractID;
                                    entityData.MilestoneID = Convert.ToInt32(milestoneID);
                                    entityData.MilestoneResponceID = Convert.ToInt32(milestoneResponseID);
                                    entitiesData.Add(entityData);
                                }

                                if (entitiesData.Count > 0)
                                {
                                    rptDocmentVersionView.DataSource = entitiesData.OrderBy(entry => entry.Version);
                                    rptDocmentVersionView.DataBind();

                                    int userID = -1;
                                    userID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);

                                    ViewContractDocument(entitiesData[0].ID, userID);
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "rebindTaskUser", "OpenMilestonePopup();", true);
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "rebindTaskUser", "OpenMilestonePopup();", true);
                            }
                        }                        
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvTaskResponse.IsValid = false;
                //cvTaskResponse.ErrorMessage = "Something went wrong, Please try again";
            }
        }
        public void ViewContractDocument(long contFileID, int userID)
        {
            try
            {
                var fileRecord = ContractDocumentManagement.GetContractDocumentByID(contFileID);

                if (fileRecord != null)
                {
                    string filePath = Path.Combine(Server.MapPath(fileRecord.FilePath), fileRecord.FileKey + Path.GetExtension(fileRecord.FileName));

                    if (fileRecord.FilePath != null && File.Exists(filePath))
                    {
                        string Folder = "~/TempFiles";
                        string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                        string DateFolder = Folder + "/" + File;

                        string extension = System.IO.Path.GetExtension(filePath);

                        Directory.CreateDirectory(Server.MapPath(DateFolder));

                        if (!Directory.Exists(DateFolder))
                        {
                            Directory.CreateDirectory(Server.MapPath(DateFolder));
                        }

                        string customerID = Convert.ToString(UserManagement.GetByID(userID).CustomerID ?? 0);

                        string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                        string User = userID + "" + customerID + "" + FileDate;

                        string FileName = DateFolder + "/" + User + "" + extension;

                        FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                        BinaryWriter bw = new BinaryWriter(fs);
                        if (fileRecord.EnType == "M")
                        {
                            bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                        }
                        else
                        {
                            bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                        }
                        bw.Close();
                        DocumentPath = FileName;
                        DocumentPath = DocumentPath.Substring(2, DocumentPath.Length - 2);

                        lblMessage.Text = "";

                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenDocviewer('" + DocumentPath + "');", true);
                    }
                    else
                    {
                        lblMessage.Text = "There is no file to preview";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void rptDocmentVersionView_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);

                LinkButton lblDocumentVersionView = (LinkButton)e.Item.FindControl("lblDocumentVersionView");
                scriptManager.RegisterAsyncPostBackControl(lblDocumentVersionView);
            }
        }
        protected void rptDocmentVersionView_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                int userID = -1;
                userID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                if (e.CommandName.Equals("View"))
                {
                    if (e.CommandArgument != null)
                    {
                        long fileID = Convert.ToInt64(e.CommandArgument);
                        if (fileID != 0)
                            ViewContractDocument(fileID, userID);
                    }
                }                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                // cvDuplicateEntry.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        protected void btnmilestoneclosed1_Click(object sender, EventArgs e)
        {

            long contractID = -1;
            if (!string.IsNullOrEmpty(Request.QueryString["AccessID"]))
            {
                contractID = Convert.ToInt32(Request.QueryString["AccessID"]);
            }
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);


            int UserID = -1;
            UserID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);

            BindContractMilestone(customerID, Convert.ToInt64(contractID), UserID);

            ScriptManager.RegisterStartupScript(this, this.GetType(), "rebindTaskUser", "closemodel();", true);
        }

        protected void btnapplymilestone_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["AccessID"]))
            {
                var contractInstanceID = Request.QueryString["AccessID"];
                if (contractInstanceID != "")
                {
                    ViewState["ContractInstanceID"] = contractInstanceID;
                    int customerID = -1;
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                    int UserID = -1;
                    UserID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);

                    BindContractMilestone(customerID, Convert.ToInt64(contractInstanceID), UserID);
                }
            }
        }
    }
}