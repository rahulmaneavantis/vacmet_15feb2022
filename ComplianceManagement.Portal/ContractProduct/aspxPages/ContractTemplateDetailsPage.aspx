﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ContractTemplateDetailsPage.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.aspxPages.ContractTemplateDetailsPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" style="background: #f7f7f7;">
<head runat="server">
    <title>Contract Detail</title>
    <!-- Bootstrap CSS -->
    <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- bootstrap theme -->
    <link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />
        <!-- font icon -->
    <link href="~/NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <!--external css-->
    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/contract_custom_style.css" rel="stylesheet" />
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <%--<link href="~/NewCSS/jquery-ui.css" rel="stylesheet" type="text/css" />--%>

    <%--<script src="https://code.jquery.com/jquery-1.11.3.js"></script>--%>
    <script type="text/javascript" src="/Newjs/jquery.js"></script>
    <script type="text/javascript" src="/Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="/Newjs/bootstrap.min.js"></script>
    <%--<script type="text/javascript" src="../../Newjs/jquery-1.8.3.min.js"></script>--%>

    <link href="~/NewCSS/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
    <script src="/Newjs/bootstrap-multiselect.js" type="text/javascript"></script>

    <script src="/Newjs/bootstrap-tagsinput.js"></script>
    <link href="/NewCSS/bootstrap-tagsinput.css" rel="stylesheet" />

    <link href="/NewCSS/timeline.css" rel="stylesheet" />
    <!-- nice scroll -->
    <script type="text/javascript" src="/Newjs/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="/Newjs/jquery.nicescroll.js"></script>

    <script src="/Newjs/tag-scrolling.js" type="text/javascript"></script>
    <link href="/NewCSS/tag-scrolling.css" rel="stylesheet" />

    <style type="text/css">
        .bootstrap-tagsinput .tag [data-role="remove"]:after {
            content: "";
            padding: 0px 2px;
        }

        .bootstrap-tagsinput {
            /*border: none;
            box-shadow: none;*/
        }

        .form-group {
            margin-bottom: 10px;
        }
        .btn-group
        {
            width:100%
        }
    </style>

    <style>
        
        .tag .label {
            font-size: 100%;
        }

        span input[type=checkbox]:checked {
            color: #007aff;
            border: 1px solid;
            border-color: #007aff;
            background: none;
        }

        .label-info, .label-info-selected {
            font-size: 100%;
        }

            .label-info:active, .label-info:focus, .label-info:hover {
                color: #007aff;
                border: 1px solid;
                border-color: #007aff;
                background: 0 0;
            }

        .label-info-selected {
            color: #007aff;
            border: 1px solid;
            border-color: #007aff;
            background: 0 0;
        }

        textarea {
            resize: none;
            font-size: 13px;
            padding: 10px;
            height: 38px;
            min-height: 38px;
            max-height: 150px;
            width: 100%;
            box-sizing: border-box;
            overflow-y: auto;
        }
    </style>

    <script type="text/javascript">

        function showHideAuditLog(divID, iID) {
           
            if ($(iID).attr('class').indexOf('fa fa-plus') > -1) {
                $(iID).attr("class", "fa fa-minus");
                $(divID).collapse('toggle');
            } else if ($(iID).attr('class').indexOf('fa fa-minus') > -1) {
                $(iID).attr("class", "fa fa-plus");
                $(divID).collapse('toggle');
            }
        }

        //Working Code Backup
        //function showHideAuditLog(divID, iID) {
        //    
        //    if ($(iID).attr('class').indexOf('fa fa-plus') > -1) {
        //        $(iID).attr("class", "fa fa-minus");
        //        $(divID).css({ "border": "1px solid #c7c7cc" });
        //        //$(divID).collapse('toggle');
        //    } else if ($(iID).attr('class').indexOf('fa fa-minus') > -1) {
        //        $(iID).attr("class", "fa fa-plus");
        //        $(divID).css({ "border": "0px" });
        //        // $(divID).collapse('toggle');
        //    }
        //}

        jQuery(window).load(function () {
            $('#updateProgress').hide();
        });

        $(document).ready(function () {
            $('[data-toggle="popover"]').popover();

            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(InitializeRequest);
            prm.add_endRequest(EndRequest);

            BindControls();

            if ($("#<%=tbxBranch.ClientID %>") != null) {
                $("#<%=tbxBranch.ClientID %>").unbind('click');

                $("#<%=tbxBranch.ClientID %>").click(function () {
                    $("#divBranches").toggle("blind", null, 500, function () { });
                });
            }
            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            });

            $(document).tooltip({ selector: '[data-toggle="tooltip"]' });

            $("input[name='rbContractEndType']").change(function () {
                switch ($(this).val()) {
                    case 'E':
                        $('#divExpirationDate').show();
                        $('#divDuration').hide();
                        break;
                    case 'D':
                        $('#divDuration').show();
                        $('#divExpirationDate').hide();
                        break;
                }
            });

            applyCSSDate();

            $("html").getNiceScroll().resize();
            

            $('#updateProgress').hide();

            $('i.glyphicon.glyphicon-search').removeClass('glyphicon glyphicon-search').addClass('fa fa-search color-black');
            $('i.glyphicon glyphicon-remove-circle').removeClass('glyphicon glyphicon-remove-circle').addClass(' fa fa-remove');

            $('#txtFileTags').tagsinput({});
            $('#lblFileTags').tagsinput({});

            $('input[id*=lstBoxFileTags]').hide();

            $('textarea').on('input', function () {

                if (this.scrollHeight <= 150)
                    $(this).outerHeight(38).outerHeight(this.scrollHeight);

                if (this.scrollHeight >= 150)
                    $(this).outerHeight(150);
            });
        });

        function scrollUp() {
            $('html, body').animate({ scrollTop: '0px' }, 800);
        }

        function scrollDown() {
            $('html, body').animate({ scrollTop: $elem.height() }, 800);
        }

        function scrollUpPage() {
            $("#divMainView").animate({ scrollTop: 0 }, 'slow');
        }

        function hide(object) {
            if (object != null)
                object.style.display = "none";
        }
        function ClosedSectionTemplate()
        {
            document.getElementById('<%= lnkContractTransaction.ClientID %>').click();            
        }

        function show(object) {
            if (object != null)
                object.style.display = "block";
        }

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }

        $('.btn-minimize').click(function () {
            var s1 = $(this).find('i');
            if ($(this).hasClass('collapsed')) {
                $(s1).removeClass('fa-chevron-down');
                $(s1).addClass('fa-chevron-up');
            } else {
                $(s1).removeClass('fa-chevron-up');
                $(s1).addClass('fa-chevron-down');
            }
        });

        function btnminimize(obj) {
            var s1 = $(obj).find('i');
            if ($(obj).hasClass('collapsed')) {

                $(s1).removeClass('fa-chevron-up');
                $(s1).addClass('fa-chevron-down');
            } else {
                $(s1).removeClass('fa-chevron-down');
                $(s1).addClass('fa-chevron-up');
            }
        }

        function InitializeRequest(sender, args) { }
        function EndRequest(sender, args) { BindControls(); }

        function BindControls() {
            var startDate = new Date();
            $(function () {
                $('input[id*=txtProposalDate]').datepicker({
                    dateFormat: 'dd-mm-yy',
                    //maxDate: startDate,
                    numberOfMonths: 1,
                    changeMonth: true,
                    changeYear: true,
                });

                $('input[id*=txtAgreementDate]').datepicker({
                    dateFormat: 'dd-mm-yy',
                    //maxDate: startDate,
                    numberOfMonths: 1,
                    changeMonth: true,
                    changeYear: true,
                });

                $('input[id*=txtEffectiveDate]').datepicker({
                    dateFormat: 'dd-mm-yy',
                    numberOfMonths: 1,
                    changeMonth: true,
                    changeYear: true,
                });

                $('input[id*=txtReviewDate]').datepicker({
                    dateFormat: 'dd-mm-yy',
                    numberOfMonths: 1,
                    changeMonth: true,
                    changeYear: true,
                });

                $('input[id*=txtExpirationDate]').datepicker({
                    dateFormat: 'dd-mm-yy',
                    //minDate: startDate,
                    numberOfMonths: 1,
                    changeMonth: true,
                    changeYear: true,
                });

                $('input[id*=txtlockinperiodDate]').datepicker({
                    dateFormat: 'dd-mm-yy',
                    //minDate: startDate,
                    numberOfMonths: 1,
                    changeMonth: true,
                    changeYear: true,
                });

                $('input[id*=tbxTaskDueDate]').datepicker({
                    dateFormat: 'dd-mm-yy',
                    minDate: startDate,
                    numberOfMonths: 1,
                    changeMonth: true,
                    changeYear: true,
                });

                $('input[id*=tbxTaskAssignDate]').datepicker({
                    dateFormat: 'dd-mm-yy',
                    minDate: startDate,
                    numberOfMonths: 1,
                    changeMonth: true,
                    changeYear: true,
                });

                $('input[id*=txtFromDate_AuditLog]').datepicker({
                    dateFormat: 'dd-mm-yy',
                    numberOfMonths: 1,
                    changeMonth: true,
                    changeYear: true,
                });

                $('input[id*=txtToDate_AuditLog]').datepicker({
                    dateFormat: 'dd-mm-yy',
                    numberOfMonths: 1,
                    changeMonth: true,
                    changeYear: true,
                });
            });

            $(function () {
                $('[id*=lstBoxVendor]').multiselect({
                    includeSelectAllOption: true,
                    numberDisplayed: 2,
                    buttonWidth: '100%',
                    enableCaseInsensitiveFiltering: true,
                    filterPlaceholder: 'Type to Search for Vendor..',
                    nSelectedText: ' - Vendor(s) selected',
                   enableFiltering: true,
                });

                $('[id*=ddlPaymentTerm1]').multiselect({
                    includeSelectAllOption: true,
                    numberDisplayed: 2,
                    buttonWidth: '100%',
                    enableCaseInsensitiveFiltering: true,
                    filterPlaceholder: 'Type to Search for Payment Term..',
                    nSelectedText: ' - Payment Term(s) selected',
                    enableFiltering: true,
                });
                
                $('[id*=lstBoxOwner]').multiselect({
                    includeSelectAllOption: true,
                    numberDisplayed: 2,
                    buttonWidth: '100%',
                    enableCaseInsensitiveFiltering: true,
                    filterPlaceholder: 'Type to Search for User..',
                    nSelectedText: ' - Owner(s) selected',
                });

                $('[id*=lstBoxApprover]').multiselect({
                    includeSelectAllOption: true,
                    numberDisplayed: 2,
                    buttonWidth: '100%',
                    enableCaseInsensitiveFiltering: true,
                    filterPlaceholder: 'Type to Search for User..',
                    nSelectedText: ' - Approver(s) selected',
                });

            });
        }

        function hideDivBranch() {
            $('#divBranches').hide("blind", null, 0, function () { });
        }

        function applyCSSDate() {
            $('#<%= txtProposalDate.ClientID %>').removeClass();
            $('#<%= txtProposalDate.ClientID %>').addClass('form-control');

            $('#<%= txtAgreementDate.ClientID %>').removeClass();
            $('#<%= txtAgreementDate.ClientID %>').addClass('form-control');

            $('#<%= txtEffectiveDate.ClientID %>').removeClass();
            $('#<%= txtEffectiveDate.ClientID %>').addClass('form-control');

            $('#<%= txtReviewDate.ClientID %>').removeClass();
            $('#<%= txtReviewDate.ClientID %>').addClass('form-control');

            $('#<%= txtExpirationDate.ClientID %>').removeClass();
            $('#<%= txtExpirationDate.ClientID %>').addClass('form-control');

            $('#<%= txtlockinperiodDate.ClientID %>').removeClass();
            $('#<%= txtlockinperiodDate.ClientID %>').addClass('form-control');
            
            $('#<%=txtNoticeTerm.ClientID %>').removeClass();
            $('#<%= txtNoticeTerm.ClientID %>').addClass('form-control');
        }

        function OpenDocviewer(filePath) {
            $('#IFrameDocumentViewer').attr('src', "/docviewer.aspx?docurl=" + filePath);
            $('#DocumentViewer').modal('show');
        }
        function OpenUploadDocumentPopup(cid, flag) {
            $('#AddDocumentsPopUp').modal('show');
            $('#IframeAddDocuments').attr('src', "/ContractProduct/aspxPages/UploadContractDocuments.aspx?CID=" + cid + "&FlagID=" + flag);
        };
        function OpenUploadDocumentForTaskPopup(flag) {
            
        }
        function CloseUploadDocumentPopup(flag) {
            $('#AddDocumentsPopUp').modal('hide');
            if (flag == 1) {
            
            }
            else {
            
            }
        }
        
        function OpenContractLinkingPopup() {
            $('#divLinkContractPopup').modal('show');
            
        }
        function OpenContractDocLinkingPopup() {
            
            
            $('#divLinkContractPopup').modal('show');
             unCheckAll('<%=grdContractList_LinkContract.ClientID %>');
            }
        function OpenSendMailPopup() {
            $('#divMailDocumentPopup').modal('show');
            unCheckAll('<%=grdMailDocumentList.ClientID %>');
        }

        function OpenContractHistoryPopup(ContractID, HistoryFlag) {
            $('#divContractHistoryPopup').modal('show');
            $('#IFrameContractHistory').attr('src', "/ContractProduct/aspxPages/ContractDetailsPage.aspx?AccessID=" + ContractID + "&HistoryFlag=" + HistoryFlag);
        }

        function OpenDocInfoPopup(FID, CID) {
            $('#divDocumentInfoPopup').modal('show');
            $('#Iframe_DocInfo').attr('src', "/ContractProduct/aspxPages/ShowDocInfo.aspx?AccessID=" + FID + "&CID=" + CID);
        }

    </script>

    <script type="text/javascript">
        //Contract Status ddlContractStatus
        function ddlContractStatusChange() {
            var selectedStatus = $("#<%=ddlContractStatus.ClientID %>").find("option:selected").text();

            if (selectedStatus != null) {
                if (selectedStatus.includes("Renewed")) {
                    $('#divRenewContractPopup').modal('show');
                }
                else {
                    $('#divRenewContractPopup').modal('hide');
                }
            }
        }


        function rblTaskTypeChange() {
            var selectedTaskType = $('input[name=rbTaskType]:checked').val();
            if (selectedTaskType != null) {
                if (selectedTaskType.includes("6")) {
                    $("#divTaskAssignTo").hide();
                }
                else {
                    $("#divTaskAssignTo").show();
                }
            }
        }

        //Custom Field
        function ddlCustomFieldChange() {
            var selectedCustomField = $(".unique-footer-select").find("option:selected").text();

            if (selectedCustomField != null) {
                if (selectedCustomField.includes("Add New")) {
                    //$("#imgAddNewCustomField").show();
                }
                else {
                    $("#imgAddNewCustomField").hide();
                }
            }
        }

        function OpenAddNewCustomFieldPopUp() {
            var ContractTypeList = document.getElementById("ddlContractType");

            if (ContractTypeList != null || ContractTypeList != undefined) {
                var selectedContractType = ContractTypeList.options[ContractTypeList.selectedIndex].value;
                $('#divCustomFieldPopUp').modal('show');
                $('#IframeCustomflds').attr('src', "/ContractProduct/Masters/AddCustomField.aspx?TypeID=" + selectedContractType + "");
            }
        }

        function CloseCustomFieldPopup() {
            $('#divCustomFieldPopUp').modal('hide');

            $.ajax({
                type: "POST",
                url: "/ContractProduct/Masters/AddCustomField.aspx/getCustomFieldID",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    if (response != '') {
                        var customfieldID = response;
                        bindContCustomField(customfieldID);
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) { },
            });
        }

        function bindContCustomField(customfieldID) {

            var ContractTypeList = document.getElementById("ddlContractType");
            var selectedContractType = ContractTypeList.options[ContractTypeList.selectedIndex].value;
            var ddlFieldName_Footer = $(".unique-footer-select");
            if (ddlFieldName_Footer != null || ddlFieldName_Footer != undefined) {
                ddlFieldName_Footer.empty().append('<option selected="selected" value="0" disabled = "disabled">Loading.....</option>');

                ddlFieldName_Footer.trigger("chosen:updated");
                ddlFieldName_Footer.trigger("liszt:updated");

                $.ajax({
                    type: "POST",
                    url: "/ContractProduct/aspxPages/ContractDetailsPage.aspx/GetCustomFields",
                    data: '{ "selectedContractType": ' + selectedContractType + ' }',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        ddlFieldName_Footer.empty();
                        $.each(response, function () {
                            for (var i = 0; i < response.d.length; i++) {
                                ddlFieldName_Footer.append("<option value='" + response.d[i].Value + "'>" + response.d[i].Text + "</option>");
                            }
                        });

                        var CustID = customfieldID.d;
                        $(".unique-footer-select").find('option[Value=' + CustID + ']').attr('selected', 'selected');

                        ddlFieldName_Footer.trigger("chosen:updated");
                        ddlFieldName_Footer.trigger("liszt:updated");

                        //ddlCustomFieldChange();
                    },
                    failure: function (response) {
                    },
                    error: function (response) {
                    }
                });
            }
        }

        //Vendor
        function ddlVendorChange() {
            var selectedVendor = $("#<%=lstBoxVendor.ClientID %>").find("option:selected").text();
            if (selectedVendor != null) {
                //if (selectedVendor == "")
                if (selectedVendor.includes("Add New")) {
                    $("#lnkShowAddNewVendorModal").show();
                }
                else {
                    $("#lnkShowAddNewVendorModal").hide();
                }
            }
        }

        function OpenAddNewVendorPopup() {
            $('#AddVendorPopUp').modal('show');
            $('#IframeParty').attr('src', "/ContractProduct/Masters/AddVendor.aspx");
        }

        function ClosePopVendor() {
            $('#AddVendorPopUp').modal('hide');
            $.ajax({
                type: "POST",
                url: "/ContractProduct/Masters/AddVendor.aspx/getvendorID",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    if (response != '') {
                        var vendorID = response.d;
                        bindVendors(vendorID);
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) { },
            });
            restoreSelectedVendors();
            // rebindddl();
        }

        $(function rebindddl() {
            $('[id*=lstBoxVendor]').multiselect({
                includeSelectAllOption: true,
                numberDisplayed: 2,
                buttonWidth: '100%',
                enableFiltering: true,
            });
        });

        function bindVendors(vendorID) {

            var lstBoxVendor = $('#<%=lstBoxVendor.ClientID %>');
            if (lstBoxVendor != null || lstBoxVendor != undefined) {

                var length = lstBoxVendor.children('option').length;
                var countries = [];
                var selectedStr = "";

                $('#<%=lstBoxVendor.ClientID %> option').each(function () {
                    if (this.selected) {
                        if (this.value != 0) {
                            if (selectedStr.length > 0)
                                selectedStr = selectedStr + "," + "'" + this.value + "'";
                            else
                                selectedStr = "'" + this.value + "'";
                        }
                    }
                });

                if (selectedStr.length > 0)
                    selectedStr = selectedStr + "," + "'" + vendorID + "'";
                else
                    selectedStr = "'" + vendorID + "'";

                $('#<% =hdnSelectedVendors.ClientID %>').attr('value', selectedStr);

                lstBoxVendor.empty().append('<option selected="selected" value="0" disabled = "disabled">Loading.....</option>');
                $('[id*=lstBoxVendor]').multiselect('rebuild');

                document.getElementById('<%= lnkBtnRebind_Vendor.ClientID %>').click();
                //RebindPopuplstBoxVendor();
                //$.ajax({
                //    type: "POST",
                //    url: "/ContractProduct/aspxPages/ContractDetailsPage.aspx/GetContractVendors",
                //    data: '{}',
                //    contentType: "application/json; charset=utf-8",
                //    dataType: "json",
                //    success: function (response) {
                //        lstBoxVendor.empty();
                //        $.each(response, function () {
                //            for (var i = 0; i < response.d.length; i++) {
                //                lstBoxVendor.append("<option value='" + response.d[i].Value + "'>" + response.d[i].Text + "</option>");
                //            }
                //        });

                //        $('[id*=lstBoxVendor]').multiselect('rebuild');
                //        var conOriandOldvenderID = selectedStr + "," + "'" + vendorID + "'";
                //        var value = conOriandOldvenderID.split(",");
                //        for (var i = 0; i < value.length; i++) {
                //            $("#lstBoxVendor option[value=" + value[i] + "]").prop('selected', true);
                //        }
                //        $('[id*=lstBoxVendor]').multiselect('refresh');

                //        //ddlVendorChange();
                //    },
                //    failure: function (response) {
                //    },
                //    error: function (response) {
                //    }
                //});
            }
        }

        //function RebindPopuplstBoxVendor() {

        //    $('[id*=lstBoxVendor]').multiselect({
        //        includeSelectAllOption: true,
        //        numberDisplayed: 2,
        //        buttonWidth: '100%',
        //        enableCaseInsensitiveFiltering: true,
        //        filterPlaceholder: 'Type to Search for Vendor..',
        //        nSelectedText: ' - Vendor(s) selected',
        //    });
        //}

        function restoreSelectedVendors() {
            debugger;
            var selectedStr = $('#<% =hdnSelectedVendors.ClientID %>').val();

            //alert(selectedStr);

            $('[id*=lstBoxVendor]').multiselect('rebuild');

            var value = selectedStr.split(",");
            for (var i = 0; i < value.length; i++) {
                $("#lstBoxVendor option[value=" + value[i] + "]").prop('selected', true);
            }
          
            $('[id*=lstBoxVendor]').multiselect('refresh');
        }

        //Department
        function ddlDepartmentChange() {
            var selectedDeptID = $("#<%=ddlDepartment.ClientID %>").val();
            if (selectedDeptID != null) {
                if (selectedDeptID == "0") {
                    $("#lnkAddNewDepartmentModal").show();
                }
                else {
                    $("#lnkAddNewDepartmentModal").hide();
                }
            }
        }

        function OpenDepartmentPopup(deptID) {
            $('#AddDepartmentPopUp').modal('show');
            $('#IframeDepartment').attr('src', "/ContractProduct/Masters/AddNewDepartment.aspx?DepartmentID=" + deptID);
        }

        function ClosePopDepartment() {
            $('#AddDepartmentPopUp').modal('hide');

            var ddlDepartment = $('#<%=ddlDepartment.ClientID %>');
            if (ddlDepartment != null || ddlDepartment != undefined) {
                ddlDepartment.empty().append('<option selected="selected" value="0" disabled = "disabled">Loading.....</option>');
                ddlDepartment.trigger("chosen:updated");
                ddlDepartment.trigger("liszt:updated");
            }

            document.getElementById('<%= lnkBtnRebind_Dept.ClientID %>').click();
            }

            function rebindDepartments() {

                $.ajax({
                    type: "POST",
                    url: "/ContractProduct/Masters/AddNewDepartment.aspx/getDepatmentID",
                    data: '{}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        if (response != '') {
                            var departmentID = response;
                            var deptID = response.d;
                            $('#ddlDepartment').find('option[Value=' + deptID + ']').attr('selected', 'selected');

                            var ddlDepartment = $('#<%=ddlDepartment.ClientID %>');

                        if (ddlDepartment != null || ddlDepartment != undefined) {
                            ddlDepartment.trigger("chosen:updated");
                            ddlDepartment.trigger("liszt:updated");
                        }
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) { },
            });
         }
        //Contact Person Of Department 
    
        <%--   function ddlContactPersonDepartmentChange() {
            var selectedPDeptID = $("#<%=ddlCPDepartment.ClientID %>").val();
            if (selectedPDeptID != null) {
                if (selectedPDeptID == "0") {
                    $("#lnkAddNewPDepartmentModal").show();
                }
                else {
                    $("#lnkAddNewPDepartmentModal").hide();
                }
            }
        }

        function OpenDepartmentPPopup(deptID) {
            $('#AddPDepartmentPopUp').modal('show');
            $('#IframePDepartment').attr('src', "/ContractProduct/Masters/AddContactPerson.aspx?DepartmentID=" + deptID);
        }

        function ClosePopDepartment() {
            $('#AddPDepartmentPopUp').modal('hide');

            var ddlCPDepartment = $('#<%=ddlCPDepartment.ClientID %>');
            if (ddlCPDepartment != null || ddlCPDepartment != undefined) {
                ddlCPDepartment.empty().append('<option selected="selected" value="0" disabled = "disabled">Loading.....</option>');
                ddlCPDepartment.trigger("chosen:updated");
                ddlCPDepartment.trigger("liszt:updated");
            }

            document.getElementById('<%= lnkBtnRebind_ContactDept.ClientID %>').click();
            }

            function rebindDepartments() {

                $.ajax({
                    type: "POST",
                    url: "/ContractProduct/Masters/AddContactPerson.aspx/getDepatmentID",
                    data: '{}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        if (response != '') {
                            var departmentID = response;
                            var deptID = response.d;
                            $('#ddlCPDepartment').find('option[Value=' + deptID + ']').attr('selected', 'selected');

                            var ddlCPDepartment = $('#<%=ddlCPDepartment.ClientID %>');

                            if (ddlCPDepartment != null || ddlCPDepartment != undefined) {
                            ddlCPDepartment.trigger("chosen:updated");
                            ddlCPDepartment.trigger("liszt:updated");
                        }
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) { },
            });
        }--%>
        //Contact Person Of Department

        function ddlContactPersonDepartmentChange() {
            var selectedVendor = $("#<%=ddlCPDepartment.ClientID %>").find("option:selected").text();
            if (selectedVendor != null) {
                //if (selectedVendor == "")
                if (selectedVendor.includes("Add New")) {lnkAddNewPDepartmentModal
                    $("#").show();
                }
                else {
                    $("#lnkAddNewPDepartmentModal").hide();
                }
            }
        }

        function OpenDepartmentPPopup() {
            $('#AddPDepartmentPopUp').modal('show');
            $('#IframePDepartment').attr('src', "/ContractProduct/Masters/AddContactPerson.aspx");
        }


        function ClosePopContactPerson() {
             $('#AddPDepartmentPopUp').modal('hide');

            var ddlCPDepartment = $('#<%=ddlCPDepartment.ClientID %>');
            if (ddlCPDepartment != null || ddlCPDepartment != undefined) {
                ddlCPDepartment.empty().append('<option selected="selected" value="0" disabled = "disabled">Loading.....</option>');
                ddlCPDepartment.trigger("chosen:updated");
                ddlCPDepartment.trigger("liszt:updated");
            }

            document.getElementById('<%= lnkBtnRebind_ContactDept.ClientID %>').click();
            }


        //function ClosePopDepartment() {
        //    $('#AddUserPopUp').modal('hide');
        //    debugger;
        //    $.ajax({
        //        type: "POST",
        //        url: "/ContractProduct/Masters/AddContactPerson.aspx/getContUserID",
        //        data: '{}',
        //        contentType: "application/json; charset=utf-8",
        //        dataType: "json",
        //        success: function (response) {
        //            if (response != '') {
        //                var contuserID = response.d;
        //                bindAssignUsersNew(contuserID);
        //            }
        //        },
        //        error: function (XMLHttpRequest, textStatus, errorThrown) { },
        //    });
        //}

        function bindAssignUsersNew(newUserID) {
            var ddlCPDepartment = $('#<%=ddlCPDepartment.ClientID %>');
            if (ddlCPDepartment != null || ddlCPDepartment != undefined) {

                var length = ddlCPDepartment.children('option').length;

                var countries = [];
                var selectedStr = "";

                $('#<%=ddlCPDepartment.ClientID %> option').each(function () {
                    if (this.selected) {
                        if (this.value != 0) {
                            if (selectedStr.length > 0)
                                selectedStr = selectedStr + "," + "'" + this.value + "'";
                            else
                                selectedStr = selectedStr + "'" + this.value + "'";
                        }
                    }
                });

                if (selectedStr.length > 0)
                    selectedStr = selectedStr + "," + "'" + newUserID + "'";
                else
                    selectedStr = "'" + newUserID + "'";

                $('#<% =hdnSelectedTaskUsers.ClientID %>').attr('value', selectedStr);

                ddlCPDepartment.empty().append('<option selected="selected" value="0" disabled = "disabled">Loading.....</option>');
                $('[id*=ddlCPDepartment]').multiselect('rebuild');

                document.getElementById('<%= lnkAddNewPDepartmentModal.ClientID %>').click();

               
                function restoreSelectedTaskUsersNew() {
                    debugger;
                    var selectedStr = $('#<% =hdnSelectedTaskUsers.ClientID %>').val();

                    //alert(selectedStr);

                    var value = selectedStr.split(",");
                    for (var i = 0; i < value.length; i++) {
                        $("#ddlCPDepartment option[value=" + value[i] + "]").prop('selected', true);
                    }

                    $('[id*=ddlCPDepartment]').multiselect('refresh');
                    $('[id*=ddlCPDepartment]').multiselect('rebuild');

                    $('[id*=ddlCPDepartment]').multiselect({
                        includeSelectAllOption: true,
                        numberDisplayed: 2,
                        buttonWidth: '100%',
                        enableCaseInsensitiveFiltering: true,
                        filterPlaceholder: 'Type to Search for User..',
                        nSelectedText: ' - User(s) selected',
                    });

                }
            }
        }
        

       
       





        //Contract Type
        function ddlContractTypeChange() {
            var selectedContractTypeID = $("#<%=ddlContractType.ClientID %>").val();

            if (selectedContractTypeID != null) {
                if (selectedContractTypeID == "0") {
                    $("#lnkAddNewContractTypeModal").show();
                }
                else {
                    $("#lnkAddNewContractTypeModal").hide();
                }
            }
        }

        function OpenAddNewTypePopup() {
            var typeID = '';
            $('#AddContractType').modal('show');
            $('#IframeCategoryType').attr('src', "/ContractProduct/Masters/AddType.aspx?CaseTypeId=" + typeID);
        }

        function CloseContractTypePopUp() {
            $('#AddContractType').modal('hide');

            var ddlContractType = $('#<%=ddlContractType.ClientID %>');
            if (ddlContractType != null || ddlContractType != undefined) {
                ddlContractType.empty().append('<option selected="selected" value="0" disabled = "disabled">Loading.....</option>');
                ddlContractType.trigger("chosen:updated");
                ddlContractType.trigger("liszt:updated");
            }

            document.getElementById('<%= lnkBtnRebind_Type.ClientID %>').click();

            // $.ajax({
            //     type: "POST",
            //     url: "/ContractProduct//Masters/AddType.aspx/getTypeID",
            //     data: '{}',
            //     contentType: "application/json; charset=utf-8",
            //     dataType: "json",
            //     success: function (response) {
            //         if (response != '') {
            //             var typeID = response;
            //             bindContractTypes(typeID);
            //         }
            //     },
            //     error:
            //function (XMLHttpRequest, textStatus, errorThrown) { },
            // });
        }

        function rebindContractTypes() {

            $.ajax({
                type: "POST",
                url: "/ContractProduct//Masters/AddType.aspx/getTypeID",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    if (response != '') {
                        var typeID = response.d;
                        $('#ddlContractType').find('option[Value=' + typeID + ']').attr('selected', 'selected');
                        var ddlContractType = $('#<%=ddlContractType.ClientID %>');

                        if (ddlContractType != null || ddlContractType != undefined) {
                            ddlContractType.trigger("chosen:updated");
                            ddlContractType.trigger("liszt:updated");
                        }
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) { },
            });
        }

        function bindContractTypes(typeID) {
            var ddlContractType = $('#<%=ddlContractType.ClientID %>');
            if (ddlContractType != null || ddlContractType != undefined) {
                ddlContractType.empty().append('<option selected="selected" value="0" disabled = "disabled">Loading.....</option>');
                ddlContractType.trigger("chosen:updated");
                ddlContractType.trigger("liszt:updated");
                $.ajax({
                    type: "POST",
                    url: "/ContractProduct/aspxPages/ContractDetailsPage.aspx/GetContractTypes",
                    data: '{}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        ddlContractType.empty();
                        $.each(response, function () {
                            for (var i = 0; i < response.d.length; i++) {
                                ddlContractType.append("<option value='" + response.d[i].Value + "'>" + response.d[i].Text + "</option>");
                            }
                            //ddlContractType.append('<option value="0">Add New</option>');
                        });
                        var TID = typeID.d;
                        $('#ddlContractType').find('option[Value=' + TID + ']').attr('selected', 'selected');
                        ddlContractType.trigger("chosen:updated");
                        ddlContractType.trigger("liszt:updated");

                        //ddlContractTypeChange();
                    },
                    failure: function (response) {
                    },
                    error: function (response) {
                    }
                });
            }
        }

        //Contract Sub-Type
        function ddlContractSubTypeChange() {
            var selectedSubContID = $("#<%=ddlContractSubType.ClientID %>").val();
            if (selectedSubContID != null) {
                if (selectedSubContID == "0") {
                   // $("#lnkAddNewContractSubTypeModal").show();
                }
                else {
                    $("#lnkAddNewContractSubTypeModal").hide();
                }
            }
        }

        function OpenAddNewSubTypePopup() {
            var typeID = '';
            $('#AddContractSubTypePopUp').modal('show');
            $('#Iframesubconttype').attr('src', "/ContractProduct/Masters/AddContSubType.aspx?ContractTypeId=" + typeID);
        }

        function CloseContractSubTypePopUp() {
            $('#AddContractSubTypePopUp').modal('hide');

            var ddlContractSubType = $('#<%=ddlContractSubType.ClientID %>');
            if (ddlContractSubType != null || ddlContractSubType != undefined) {
                ddlContractSubType.empty().append('<option selected="selected" value="0" disabled = "disabled">Loading.....</option>');
                ddlContractSubType.trigger("chosen:updated");
                ddlContractSubType.trigger("liszt:updated");
            }

            document.getElementById('<%= lnkBtnRebind_SubType.ClientID %>').click();

            // $.ajax({
            //     type: "POST",
            //     url: "/ContractProduct/Masters/AddContSubType.aspx/getSubTypeID",
            //     data: '{}',
            //     contentType: "application/json; charset=utf-8",
            //     dataType: "json",
            //     success: function (response) {
            //         if (response != '') {
            //             var subtypeID = response;
            //             bindContractSubTypes(subtypeID);
            //         }
            //     },
            //     error:
            //function (XMLHttpRequest, textStatus, errorThrown) { },
            // });
        }

        function rebindSubTypes() {

            $.ajax({
                type: "POST",
                url: "/ContractProduct/Masters/AddContSubType.aspx/getSubTypeID",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    if (response != '') {
                        var subtypeID = response.d;

                        $('#ddlContractSubType').find('option[Value=' + subtypeID + ']').attr('selected', 'selected');
                        var ddlContractSubType = $('#<%=ddlContractSubType.ClientID %>');

                        if (ddlContractSubType != null || ddlContractSubType != undefined) {
                            ddlContractSubType.trigger("chosen:updated");
                            ddlContractSubType.trigger("liszt:updated");
                        }
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) { },
            });
        }

        function bindContractSubTypes(subtypeID) {
            var ContractTypeList = document.getElementById("ddlContractType");
            var selectedContractType = ContractTypeList.options[ContractTypeList.selectedIndex].value;
            var ddlContractSubType = $('#<%=ddlContractSubType.ClientID %>');

            if (ddlContractSubType != null || ddlContractSubType != undefined) {
                ddlContractSubType.empty().append('<option selected="selected" value="0" disabled = "disabled">Loading.....</option>');
                ddlContractSubType.trigger("chosen:updated");
                ddlContractSubType.trigger("liszt:updated");
                $.ajax({
                    type: "POST",
                    url: "/ContractProduct/aspxPages/ContractDetailsPage.aspx/GetContractSubTypes",
                    data: '{ "selectedContractType": ' + selectedContractType + ' }',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {

                        ddlContractSubType.empty();
                        $.each(response, function () {
                            for (var i = 0; i < response.d.length; i++) {
                                ddlContractSubType.append("<option value='" + response.d[i].Value + "'>" + response.d[i].Text + "</option>");
                            }
                            //ddlContractSubType.append('<option value="0">Add New</option>');
                        });

                        var stypeID = subtypeID.d;
                        $('#ddlContractSubType').find('option[Value=' + stypeID + ']').attr('selected', 'selected');

                        ddlContractSubType.trigger("chosen:updated");
                        ddlContractSubType.trigger("liszt:updated");

                        //ddlContractSubTypeChange();
                    },
                    failure: function (response) {
                    },
                    error: function (response) {
                    }
                });
            }
        }


        function OpenAddUserDetailPop() {
            $('#AddUserPopUp').modal('show');
            $('#IframeAddUser').attr('src', "/ContractProduct/Masters/AddContractUser.aspx");
        }

        function ClosePopUser() {
            $('#AddUserPopUp').modal('hide');
            debugger;
            $.ajax({
                type: "POST",
                url: "/ContractProduct/Masters/AddContractUser.aspx/getContUserID",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    if (response != '') {
                        var contuserID = response.d;
                        bindAssignUsers(contuserID);
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) { },
            });
        }

        
    </script>

    <script type="text/javascript">
        function checkAll(chkHeader, gridName) {
            var selectedRowCount = 0;
            var grid = document.getElementById("<%=grdContractList_LinkContract.ClientID %>");
            if (grid != null) {
                if (chkHeader != null) {
                    //Get all input elements in Gridview
                    var inputList = grid.getElementsByTagName("input");

                    for (var i = 1; i < inputList.length; i++) {
                        if (inputList[i].type == "checkbox") {
                            if (chkHeader.checked) {
                                inputList[i].checked = true;
                                selectedRowCount++;
                            }
                            else if (!chkHeader.checked)
                                inputList[i].checked = false;
                        }
                    }
                }

                var btnSaveLinkContract = document.getElementById("<%=btnSaveLinkContract.ClientID %>");
                var lblTotalContractSelected = document.getElementById("<%=lblTotalContractSelected.ClientID %>");
                if ((btnSaveLinkContract != null || btnSaveLinkContract != undefined) && (lblTotalContractSelected != null || lblTotalContractSelected != undefined)) {
                    if (selectedRowCount > 0) {
                        lblTotalContractSelected.innerHTML = selectedRowCount + " Selected";
                        divLinkContractSaveCount.style.display = "block";
                    }
                    else {
                        lblTotalContractSelected.innerHTML = "";;
                        divLinkContractSaveCount.style.display = "none";
                    }
                }
            }
        }

        function checkUncheckRow(clickedCheckBoxObj) {
            var selectedRowCount = 0;
            //Get the reference of GridView
            var grid = document.getElementById("<%=grdContractList_LinkContract.ClientID %>");

            //Get all input elements in Gridview
            var inputList = grid.getElementsByTagName("input");

            //The First element is the Header Checkbox
            var headerCheckBox = inputList[0];
            var checked = true;
            for (var i = 0; i < inputList.length; i++) {
                //Based on all or none checkboxes are checked check/uncheck Header Checkbox                
                if (inputList[i].type == "checkbox" && inputList[i] != headerCheckBox) {
                    if (!inputList[i].checked) {
                        checked = false;
                    }
                    if (inputList[i].checked) {
                        selectedRowCount++;
                    }
                }
            }

            headerCheckBox.checked = checked;

            var btnSaveLinkContract = document.getElementById("<%=btnSaveLinkContract.ClientID %>");
            var lblTotalContractSelected = document.getElementById("<%=lblTotalContractSelected.ClientID %>");
            if ((btnSaveLinkContract != null || btnSaveLinkContract != undefined) && (lblTotalContractSelected != null || lblTotalContractSelected != undefined)) {
                if (selectedRowCount > 0) {
                    lblTotalContractSelected.innerHTML = selectedRowCount + " Selected";
                    divLinkContractSaveCount.style.display = "block";
                }
                else {
                    lblTotalContractSelected.innerHTML = "";;
                    divLinkContractSaveCount.style.display = "none";
                }
            }
        }

        function unCheckAll(gridName) {
            var grid = document.getElementById(gridName);
            if (grid != null) {
                //Get all input elements in Gridview
                var inputList = grid.getElementsByTagName("input");

                for (var i = 1; i < inputList.length; i++) {
                    if (inputList[i].type == "checkbox") {
                        inputList[i].checked = false;
                    }
                }
            }
        }

    </script>

    <script type="text/javascript">
        function checkAll_MailDocument(chkHeader) {
            var selectedRowCount = 0;
            var grid = document.getElementById("<%=grdMailDocumentList.ClientID %>");
            if (grid != null) {
                if (chkHeader != null) {
                    //Get all input elements in Gridview
                    var inputList = grid.getElementsByTagName("input");

                    for (var i = 1; i < inputList.length; i++) {
                        if (inputList[i].type == "checkbox") {
                            if (chkHeader.checked) {
                                inputList[i].checked = true;
                                selectedRowCount++;
                            }
                            else if (!chkHeader.checked)
                                inputList[i].checked = false;
                        }
                    }
                }

                var btnSaveLinkContract = document.getElementById("<%=btnSendMail.ClientID %>");
                var lblTotalContractSelected = document.getElementById("<%=lblTotalDocumentSelected.ClientID %>");
                if ((btnSaveLinkContract != null || btnSaveLinkContract != undefined) && (lblTotalContractSelected != null || lblTotalContractSelected != undefined)) {
                    if (selectedRowCount > 0) {
                        lblTotalContractSelected.innerHTML = selectedRowCount + " Selected";
                        divSendDocCount.style.display = "block";
                    }
                    else {
                        lblTotalContractSelected.innerHTML = "";;
                        divSendDocCount.style.display = "none";
                    }
                }
            }
        }


        function checkUncheckRow_MailDocument(clickedCheckBoxObj) {
            var selectedRowCount = 0;
            //Get the reference of GridView
            var grid = document.getElementById("<%=grdMailDocumentList.ClientID %>");

            //Get all input elements in Gridview
            var inputList = grid.getElementsByTagName("input");

            //The First element is the Header Checkbox
            var headerCheckBox = inputList[0];
            var checked = true;
            for (var i = 0; i < inputList.length; i++) {
                //Based on all or none checkboxes are checked check/uncheck Header Checkbox                
                if (inputList[i].type == "checkbox" && inputList[i] != headerCheckBox) {
                    if (!inputList[i].checked) {
                        checked = false;
                    }
                    if (inputList[i].checked) {
                        selectedRowCount++;
                    }
                }
            }

            headerCheckBox.checked = checked;

            var btnSaveLinkContract = document.getElementById("<%=btnSendMail.ClientID %>");
            var lblTotalContractSelected = document.getElementById("<%=lblTotalDocumentSelected.ClientID %>");
            if ((btnSaveLinkContract != null || btnSaveLinkContract != undefined) && (lblTotalContractSelected != null || lblTotalContractSelected != undefined)) {
                if (selectedRowCount > 0) {
                    lblTotalContractSelected.innerHTML = selectedRowCount + " Selected";
                    divSendDocCount.style.display = "block";
                }
                else {
                    lblTotalContractSelected.innerHTML = "";;
                    divSendDocCount.style.display = "none";
                }
            }
        }
    </script>


    <script type="text/javascript">
        function checkAll_Import(chkHeader) {
            var selectedRowCount = 0;
            var grid = document.getElementById("<%=grdContractDocument_Renew.ClientID %>");
            if (grid != null) {
                if (chkHeader != null) {
                    //Get all input elements in Gridview
                    var inputList = grid.getElementsByTagName("input");

                    for (var i = 1; i < inputList.length; i++) {
                        if (inputList[i].type == "checkbox") {
                            if (chkHeader.checked) {
                                inputList[i].checked = true;
                                selectedRowCount++;
                            }
                            else if (!chkHeader.checked)
                                inputList[i].checked = false;
                        }
                    }
                }

                var lblTotalSelected = document.getElementById("<%=lblImportDocCount.ClientID %>");
                if ((lblTotalSelected != null || lblTotalSelected != undefined)) {
                    if (selectedRowCount > 0) {
                        lblTotalSelected.innerHTML = selectedRowCount + " Selected";
                        divDocCount_ImportRenew.style.display = "block";
                    }
                    else {
                        lblTotalSelected.innerHTML = "";;
                        divDocCount_ImportRenew.style.display = "none";
                    }
                }
            }
        }

        function checkUncheckRow_Import(clickedCheckBoxObj) {
            var selectedRowCount = 0;
            //Get the reference of GridView
            var grid = document.getElementById("<%=grdContractDocument_Renew.ClientID %>");

            //Get all input elements in Gridview
            var inputList = grid.getElementsByTagName("input");

            //The First element is the Header Checkbox
            var headerCheckBox = inputList[0];
            var checked = true;
            for (var i = 0; i < inputList.length; i++) {
                //Based on all or none checkboxes are checked check/uncheck Header Checkbox                
                if (inputList[i].type == "checkbox" && inputList[i] != headerCheckBox) {
                    if (!inputList[i].checked) {
                        checked = false;
                    }
                    if (inputList[i].checked) {
                        selectedRowCount++;
                    }
                }
            }

            headerCheckBox.checked = checked;

            var lblTotalSelected = document.getElementById("<%=lblImportDocCount.ClientID %>");
            if ((lblTotalSelected != null || lblTotalSelected != undefined)) {
                if (selectedRowCount > 0) {
                    lblTotalSelected.innerHTML = selectedRowCount + " Selected";
                    divDocCount_ImportRenew.style.display = "block";
                }
                else {
                    lblTotalSelected.innerHTML = "";;
                    divDocCount_ImportRenew.style.display = "none";
                }
            }
        }
        function fcheckcontract(obj) {
            var span = $(obj).parent('span.label.label - info');
            $(span).addClass('label-info-selected')
        }
    </script>

    <style>
        .form-group.required .control-label:after {
            content: "";
        }
        .panel-heading .nav > li > a {
            font-size: 15px !important;
            border-bottom: 0px;
            background: none;
        }
    </style>

    <style type="text/css">
        ol.progtrckr {
            margin: 0;
            padding: 0;
            list-style-type: none;
        }

            ol.progtrckr li {
                display: inline-block;
                /*text-align: center;*/
                line-height: 3em;
            }

            ol.progtrckr[data-progtrckr-steps="2"] li {
                width: 25%;
            }

            ol.progtrckr[data-progtrckr-steps="3"] li {
                width: 25%;
            }

            ol.progtrckr[data-progtrckr-steps="4"] li {
                width: 25%;
            }
            /*ol.progtrckr[data-progtrckr-steps="5"] li { width: 19%; }
ol.progtrckr[data-progtrckr-steps="6"] li { width: 16%; }
ol.progtrckr[data-progtrckr-steps="7"] li { width: 14%; }
ol.progtrckr[data-progtrckr-steps="8"] li { width: 12%; }
ol.progtrckr[data-progtrckr-steps="9"] li { width: 11%; }*/

            ol.progtrckr li.progtrckr-done {
                color: black;
                border-bottom: 4px solid yellowgreen;
            }

            ol.progtrckr li.progtrckr-closed {
                color: black;
                border-bottom: 0px solid yellowgreen;
                width: 0%;
            }

            ol.progtrckr li.progtrckr-todo {
                color: silver;
                border-bottom: 4px solid silver;
            }

            ol.progtrckr li.progtrckr-todo-closed {
                color: silver;
                border-bottom: 0px solid silver;
                width: 0%;
            }

            ol.progtrckr li.progtrckr-current {
                color: black;
                border-bottom: 4px solid silver;
            }

            ol.progtrckr li:after {
                content: "\00a0\00a0";
            }

            ol.progtrckr li:before {
                position: relative;
                bottom: -2.5em;
                float: left;
                /*left: 50%;*/
                line-height: 1em;
            }

            ol.progtrckr li.progtrckr-closed:before {
                content: "\2714";
                color: white;
                background-color: yellowgreen;
                height: 1.2em;
                width: 1.2em;
                line-height: 1.2em;
                border: none;
                border-radius: 1.2em;
            }

            ol.progtrckr li.progtrckr-done:before {
                content: "\2714";
                color: white;
                background-color: yellowgreen;
                height: 1.2em;
                width: 1.2em;
                line-height: 1.2em;
                border: none;
                border-radius: 1.2em;
            }

            ol.progtrckr li.progtrckr-todo:before {
                content: "\039F";
                color: silver;
                background-color: white;
                font-size: 1.5em;
                bottom: -1.6em;
            }

            ol.progtrckr li.progtrckr-todo-closed:before {
                content: "\039F";
                color: silver;
                background-color: white;
                font-size: 1.5em;
                bottom: -1.6em;
            }

            ol.progtrckr li.progtrckr-current:before {
                content: "\039F";
                color: #A16BBE;
                background-color: white;
                font-size: 1.5em;
                bottom: -1.6em;
            }
    </style>

    <style>
        .scrolling-wrapper {
            overflow-x: auto;
            overflow-y: hidden;
            white-space: nowrap;
        }
    </style>

    <script>
        var view = $("#tslshow");
        var move = "100px";
        var sliderLimit = -750;


        $("#rightArrow").click(function () {
            // alert("right");
            var currentPosition = parseInt(view.css("left"));
            if (currentPosition >= sliderLimit) view.stop(false, true).animate({ left: "-=" + move }, { duration: 400 })

        });

        $("#leftArrow").click(function () {
            // alert("left");
            var currentPosition = parseInt(view.css("left"));
            if (currentPosition < 0) view.stop(false, true).animate({ left: "+=" + move }, { duration: 400 })

        });

    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="smContractDetailPage" EnablePartialRendering="true" runat="server"></asp:ScriptManager>
        
        <asp:HiddenField ID="hdnSelectedVendors" runat="server" />
        <asp:HiddenField ID="hdnSelectedTaskUsers" runat="server" />
        <div>
        <div class="mainDiv" style="background-color: #f7f7f7;">
            <div class="col-md-10">
                <header class="panel-heading tab-bg-primary" style="background: none !important;">
                    <ul class="nav nav-tabs">
                        <li class="active" id="liContractSummary" runat="server">
                            <asp:LinkButton ID="lnkBtnContractSummary" OnClick="TabContract_Click" runat="server" CssClass="bgColor-gray">Summary</asp:LinkButton>
                        </li>

                        <li class="" id="liContractTransaction" runat="server">
                            <asp:LinkButton ID="lnkContractTransaction" OnClick="lnkContractTransaction_Click" CausesValidation="false" runat="server" Style="background-color: #f7f7f7;">Agreement</asp:LinkButton>
                        </li>

                        <li class="" id="liContractDetails" runat="server" style="display:none;">
                            <asp:LinkButton ID="LnkBtnContractDetails" OnClick="TabContractDetail_Click" runat="server" CssClass="bgColor-gray">Detail(s)</asp:LinkButton>
                        </li>

                        <li class="" id="liDocument" runat="server"  style="display:none;">
                            <asp:LinkButton ID="lnkBtnDocument" OnClick="TabDocument_Click" runat="server" Style="background-color: #f7f7f7;">Document(s)</asp:LinkButton>
                        </li>

                        <li class="" id="liTask" runat="server"  style="display:none;">
                            <asp:LinkButton ID="lnkBtnTask" OnClick="TabTask_Click" runat="server" Style="background-color: #f7f7f7;">Task(s)</asp:LinkButton>
                        </li>

                        <li class="" id="liAuditLog" runat="server"  style="display:none;">
                            <asp:LinkButton ID="lnkAuditLog" OnClick="TabAuditLog_Click" CausesValidation="false" runat="server" Style="background-color: #f7f7f7;">Audit Log(s)</asp:LinkButton>
                        </li>

                        
                    </ul>
                </header>
            </div>
            <div class="col-md-2 text-right" id="topButtons" runat="server">                
            </div>

            <div class="clearfix"></div>

            <asp:UpdateProgress ID="updateProgress" runat="server">
                <ProgressTemplate>
                    <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.3;">
                        <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                            AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 40%; left: 40%;" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>

            <div id="divMainView">
                <asp:MultiView ID="MainView" runat="server">
                    <asp:View ID="ContractSummaryView" runat="server">
                        <div class="container">

                            <div id="divContractSummary" class="row Dashboard-white-widget">
                                <!--ContractDetail Panel Start-->
                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-default">

                                        <div id="collapseDivContractSummary" class="panel-collapse collapse in">
                                            <div class="row d-none">
                                                <div id="divContractStatus" style="width: 100%; margin-top: 5px;">
                                                    <asp:BulletedList class="progtrckr" ID="statusBulletedList" runat="server" BulletStyle="Numbered">
                                                    </asp:BulletedList>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <asp:ValidationSummary ID="VSContractPopup" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                                    ValidationGroup="ContractPopUpValidationGroup" />
                                                <asp:CustomValidator ID="cvContractPopUp" runat="server" EnableClientScript="False"
                                                    ValidationGroup="ContractPopUpValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                                            </div>

                                            <div class="panel-body">
                                                <div class="row" style="display:none;">
                                                    <div class="col-md-12" id="Div3" runat="server">
                                                        <asp:UpdatePanel ID="upSendMailPop" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <div class="pull-right" style="margin-right: 10px;">
                                                                    <asp:LinkButton runat="server" ID="lnkLinkContract" OnClientClick="OpenContractLinkingPopup();"
                                                                        data-toggle="tooltip" data-placement="top" ToolTip="Link Contract with Other Contract(s)">
                            <img src="/Images/link-icon.png" alt="Link" />
                                                                    </asp:LinkButton>

                                                                    <asp:LinkButton runat="server" ID="lnkSendMailWithDoc" OnClientClick="OpenSendMailPopup();"
                                                                        data-toggle="tooltip" data-placement="top" ToolTip="Send E-Mail with Document(s)">
                            <img src="/Images/send-mail-icon.png" alt="Send" />
                                                                    </asp:LinkButton>

                                                                    <asp:LinkButton runat="server" ID="btnEditContractDetail" Style="margin-bottom: 10px;" OnClick="btnEditContractControls_Click"
                                                                        data-toggle="tooltip" data-placement="top" ToolTip="Edit Contract Detail(s)">
                            <img src="/Images/edit_icon_new.png" alt="Edit" />
                                                                    </asp:LinkButton>
                                                                </div>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:PostBackTrigger ControlID="btnEditContractDetail" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                </div>
                                                <asp:Panel ID="pnlContract" runat="server">
                                                    <div class="container plr0">
                                                        <div class="row">
                                                        <div class="form-group col-md-4">
                                                                <label for="ddlContractStatus" class="control-label">Template</label>
                                                                <asp:DropDownListChosen runat="server" ID="drdTemplateID" AllowSingleDeselect="false"
                                                                DataPlaceHolder="Select Template" DisableSearchThreshold="5"
                                                                    class="form-control" Width="100%" />
                                                            </div>
                                                            <div class="form-group required col-md-4">
                                                                <label for="ddlContractStatus" class="control-label">Contract Status</label>
                                                                <asp:DropDownListChosen runat="server" ID="ddlContractStatus" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                    class="form-control" Width="100%" onchange="ddlContractStatusChange();" />
                                                            </div>

                                                            <div class="form-group required col-md-4">
                                                                <label for="txtContractNo" class="control-label">Contract Number</label>
                                                                <asp:TextBox runat="server" ID="txtContractNo" CssClass="form-control" autocomplete="off" />
                                                                <asp:RequiredFieldValidator ID="rfvContractNo" ErrorMessage="Please Enter Contract Number."
                                                                    ControlToValidate="txtContractNo" runat="server" ValidationGroup="ContractPopUpValidationGroup" Display="None" />
                                                            </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="form-group required col-md-12">
                                                                    <label for="txtTitle" class="control-label">Contract Title</label>
                                                                <asp:TextBox runat="server" ID="txtTitle" CssClass="form-control" autocomplete="off" />
                                                                <asp:RequiredFieldValidator ID="rfvContractTitle" ErrorMessage="Please Enter Contract Title"
                                                                    ControlToValidate="txtTitle" runat="server" ValidationGroup="ContractPopUpValidationGroup"
                                                                    Display="None" />
                                                                </div>
                                                            </div>
                                                        <div class="row">
                                                            <div class="form-group required col-md-12">
                                                                <label for="tbxDescription" class="control-label">Description</label>
                                                                <asp:TextBox runat="server" ID="tbxDescription" TextMode="MultiLine" CssClass="form-control" autocomplete="off" />
                                                                <asp:RequiredFieldValidator ID="rfvContractDesc" ErrorMessage="Please Enter Contract Description"
                                                                    ControlToValidate="tbxDescription" runat="server" ValidationGroup="ContractPopUpValidationGroup"
                                                                    Display="None" />
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="form-group required col-md-4">
                                                                <label for="tbxBranch" class="control-label">Entity/Branch/Location</label>
                                                                <asp:TextBox runat="server" ID="tbxBranch" CssClass="form-control bgColor-white" ReadOnly="true" autocomplete="off" />

                                                                <div style="position: absolute; z-index: 10; width: 92%" id="divBranches">
                                                                    <asp:TreeView runat="server" ID="tvBranches" BackColor="White" BorderColor="Black"
                                                                        BorderWidth="1" SelectedNodeStyle-Font-Bold="true" Height="150px" OnSelectedNodeChanged="tvBranches_SelectedNodeChanged"
                                                                        Style="overflow: auto; margin-top: -20px; border: 1px solid #c7c7cc; background-color: #ffffff; color: #8e8e93 !important;"
                                                                        ShowLines="true">
                                                                    </asp:TreeView>
                                                                </div>
                                                                <asp:RequiredFieldValidator ID="rfvBranch" ErrorMessage="Please Select Entity/Location." InitialValue="Select Entity/Location"
                                                                    ControlToValidate="tbxBranch" runat="server" ValidationGroup="ContractPopUpValidationGroup" Display="None" />
                                                            </div>

                                                            <div class="form-group required col-md-4">
                                                                <label for="lstBoxVendor" class="control-label  col-md-12 col-sm-12 col-xm-12 colpadding0">Vendor</label>
                                                                <div class="col-md-11 col-sm-11 col-xs-11 plr0">
                                                                    
                                                                    <asp:ListBox ID="lstBoxVendor" CssClass="form-control" runat="server" SelectionMode="Multiple" Width="100%"></asp:ListBox>
                                                                    <%--onchange="ddlVendorChange()"--%>
                                                                    <asp:RequiredFieldValidator ID="rfvVendor" ErrorMessage="Please Select Vendor"
                                                                        ControlToValidate="lstBoxVendor" runat="server" ValidationGroup="ContractPopUpValidationGroup" Display="None" />
                                                                    <asp:LinkButton ID="lnkBtnRebind_Vendor" OnClick="lnkBtnRebind_Vendor_Click" Style="float: right; display: none;" Width="100%" runat="server"> 
                                                                    </asp:LinkButton>
                                                                </div>
                                                                <div class="col-md-1 col-sm-1 col-xs-1 plr0 mt5 text-right">
                                                                    <%-- <label for="lnkShowAddNewVendorModal" class="hidden-label">A</label>--%>
                                                                    <%--<img id="lnkShowAddNewVendorModal" runat="server" src="/Images/add_icon_new.png" onclick="OpenAddNewVendorPopup()"
                                                                        alt="Add" data-toggle="tooltip" data-placement="bottom" title="Add New Vendor" />--%>
                                                                    <asp:ImageButton ID="lnkShowAddNewVendorModal" runat="server" ImageUrl="/Images/add_icon_new.png" OnClientClick="javascript:OpenAddNewVendorPopup(); return false;"
                                                                        data-toggle="tooltip" data-placement="bottom" ToolTip="Add New Vendor" visible="false"/>
                                                                    <%--style="display: none;"--%>
                                                                </div>
                                                            </div>
                                                            <div class="form-group required col-md-4">
                                                                <label for="ddlDepartment" class="control-label">Department</label>
                                                                <div class="col-md-11 col-sm-11 col-xs-11 plr0">
                                                                    <asp:DropDownListChosen runat="server" ID="ddlDepartment" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                        DataPlaceHolder="Select Department" class="form-control" Width="100%" />
                                                                    <%--onchange="ddlDepartmentChange()" --%>
                                                                    <asp:RequiredFieldValidator ID="rfvDept" ErrorMessage="Please Select Department" InitialValue="0"
                                                                        ControlToValidate="ddlDepartment" runat="server" ValidationGroup="ContractPopUpValidationGroup" Display="None" />
                                                                   <asp:LinkButton ID="lnkBtnRebind_Dept" OnClick="lnkBtnRebind_Dept_Click" Style="float: right; display: none;" Width="100%" runat="server"> 
                                                                    </asp:LinkButton> 
                                                                </div>
                                                                <div class="col-md-1 col-sm-1 col-xs-1 plr0 mt5 text-right">
                                                                    <%-- <img id="lnkAddNewDepartmentModal" runat="server" src="/Images/add_icon_new.png" onclick="OpenDepartmentPopup('')"
                                                                        alt="Add" data-toggle="tooltip" data-placement="bottom" title="Add New Department" />--%>
                                                                    <asp:ImageButton ID="lnkAddNewDepartmentModal" runat="server" ImageUrl="/Images/add_icon_new.png" OnClientClick="javascript:OpenDepartmentPopup(''); return false;"
                                                                        data-toggle="tooltip" data-placement="bottom" ToolTip="Add New Department" visible="false"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                       
                                                        <div class="row">

                                                              <div class="form-group  required col-md-4">
                                                                <label for="ddlCPDepartment" class="control-label">Contact Person Of Department</label>
                                                                <div class="col-md-11 col-sm-11 col-xs-11 plr0">
                                                                    <asp:DropDownListChosen runat="server" ID="ddlCPDepartment" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                        DataPlaceHolder="Select Contact Person Of Department " class="form-control" Width="100%" />
                                                                    <%--onchange="ddlCPDepartmentChange()" --%>
                                                                    <asp:RequiredFieldValidator ID="rfvCPDepartment" ErrorMessage="Please Select Contact Person Of Department" InitialValue="0"
                                                                        ControlToValidate="ddlCPDepartment" runat="server" ValidationGroup="ContractPopUpValidationGroup" Display="None" />
                                                                    <asp:LinkButton ID="lnkBtnRebind_ContactDept" OnClick="lnkBtnRebind_ContactPersonDept_Click" Style="float: right; display: none;" Width="100%" runat="server"> 
                                                                    </asp:LinkButton> 
                                                                </div>

                                                                  <div class="col-md-1 col-sm-1 col-xs-1 plr0 mt5 text-right">
                                                                    <%-- <img id="lnkAddNewDepartmentModal" runat="server" src="/Images/add_icon_new.png" onclick="OpenDepartmentPopup('')"
                                                                        alt="Add" data-toggle="tooltip" data-placement="bottom" title="Add New Department" />--%>
                                                                    <asp:ImageButton ID="lnkAddNewPDepartmentModal" runat="server" ImageUrl="/Images/add_icon_new.png" OnClientClick="javascript:OpenDepartmentPPopup(''); return false;"
                                                                        data-toggle="tooltip" data-placement="bottom" ToolTip="Add New Contact Person Of Department" visible="false"/>
                                                                </div>
                                                                  </div>

                                                            <div class="form-group col-md-4">
                                                                <label for="txtProposalDate" class="control-label">Proposal Date</label>
                                                                <div class="input-group date">
                                                                    <span class="input-group-addon">
                                                                        <span class="fa fa-calendar color-black"></span>
                                                                    </span>
                                                                    <asp:TextBox ID="txtProposalDate" runat="server" placeholder="DD-MM-YYYY" class="form-control" autocomplete="off" />
                                                                </div>
                                                            </div>

                                                            <div class="form-group col-md-4">
                                                                <label for="txtAgreementDate" class="control-label">Agreement Date</label>
                                                                <div class="input-group date">
                                                                    <span class="input-group-addon">
                                                                        <span class="fa fa-calendar color-black"></span>
                                                                    </span>
                                                                    <asp:TextBox ID="txtAgreementDate" runat="server" placeholder="DD-MM-YYYY" class="form-control" autocomplete="off" />
                                                                </div>
                                                            </div>

                                                            
                                                        </div>

                                                        <div class="row">

                                                            <div class="form-group col-md-4">
                                                                <label for="txtEffectiveDate" class="control-label">Start Date</label>
                                                                <div class="input-group date">
                                                                    <span class="input-group-addon">
                                                                        <span class="fa fa-calendar color-black"></span>
                                                                    </span>
                                                                    <asp:TextBox ID="txtEffectiveDate" runat="server" placeholder="DD-MM-YYYY" class="form-control" autocomplete="off" />
                                                                </div>
                                                            </div>

                                                            <div class="form-group col-md-4">
                                                                <label for="txtAgreementDate" class="control-label">Review Date</label>
                                                                <div class="input-group date">
                                                                    <span class="input-group-addon">
                                                                        <span class="fa fa-calendar color-black"></span>
                                                                    </span>
                                                                    <asp:TextBox ID="txtReviewDate" runat="server" placeholder="DD-MM-YYYY" class="form-control" autocomplete="off" />
                                                                </div>
                                                            </div>

                                                            <div class="form-group col-md-4 d-none">
                                                                <label for="rbContractEndType" class="control-label">&nbsp;</label>
                                                                <asp:RadioButtonList ID="rbContractEndType" runat="server" RepeatDirection="Horizontal">
                                                                    <asp:ListItem class="radio-inline" Text="End Date" Value="E" Selected="True"></asp:ListItem>
                                                                    <asp:ListItem class="radio-inline" Text="Duration " Value="D"></asp:ListItem>
                                                                </asp:RadioButtonList>
                                                            </div>

                                                            <div class="form-group  col-md-4" id="divExpirationDate">
                                                                <label for="txtExpirationDate" class="control-label">End Date</label>
                                                                <div class="input-group date">
                                                                    <span class="input-group-addon">
                                                                        <span class="fa fa-calendar color-black"></span>
                                                                    </span>
                                                                    <asp:TextBox ID="txtExpirationDate" runat="server" placeholder="DD-MM-YYYY" class="form-control" autocomplete="off" />
                                                                </div> </div>

                                                            <div class="form-group col-md-4" id="divDuration" style="display: none;">
                                                                <label for="txtDuration">Duration</label>
                                                                <div class="input-group">
                                                                    <asp:TextBox ID="txtDuration" runat="server" placeholder="" class="form-control col-md-8" autocomplete="off" />
                                                                    <div class="input-group-btn">
                                                                        <asp:DropDownList ID="ddlDuration" runat="server" CssClass="form-control col-md-4">
                                                                            <asp:ListItem Text="Days" Value="D" Selected="True"></asp:ListItem>
                                                                            <asp:ListItem Text="Weeks" Value="W"></asp:ListItem>
                                                                            <asp:ListItem Text="Months" Value="M"></asp:ListItem>
                                                                            <asp:ListItem Text="Years" Value="Y"></asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                           
                                                        </div>

                                                        <div class="row">

                                                             <div class="form-group col-md-4">
                                                                <label for="txtNoticeTerm" class="control-label">Notice Term</label>
                                                                <div class="input-group">
                                                                    <asp:TextBox ID="txtNoticeTerm" runat="server" class="form-control col-md-8" autocomplete="off" onkeydown="return ((event.keyCode>=65 && event.keyCode>=96 && event.keyCode<=105)||!(event.keyCode>=65) && event.keyCode!=32);" />
                                                                    <%--onkeydown = "return (!(event.keyCode>=65) && event.keyCode!=32);"--%>
                                                                    <div class="input-group-btn">
                                                                        <asp:DropDownList ID="ddlNoticeTerm" runat="server" CssClass="form-control col-md-4"
                                                                            Style="border-bottom-right-radius: 4px; border-top-right-radius: 4px; border-left: none;">
                                                                            <asp:ListItem Text="Days" Value="1" Selected="True"></asp:ListItem>
                                                                            <asp:ListItem Text="Weeks" Value="2"></asp:ListItem>
                                                                            <asp:ListItem Text="Months" Value="3"></asp:ListItem>
                                                                            <asp:ListItem Text="Years" Value="4"></asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <asp:UpdatePanel ID="upContractTypeSubType" runat="server" UpdateMode="Conditional">
                                                                <ContentTemplate>

                                                                    <div class="form-group required col-md-4">
                                                                        <label for="ddlContractType" class="control-label">Contract Type</label>
                                                                        <div class="col-md-11 col-sm-11 col-xs-11 plr0">
                                                                            <asp:DropDownListChosen runat="server" AutoPostBack="true" ID="ddlContractType" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                                class="form-control" Width="100%" DataPlaceHolder="Select Contract Type" OnSelectedIndexChanged="ddlContractType_SelectedIndexChanged">
                                                                            </asp:DropDownListChosen>
                                                                            <%--onchange="ddlContractTypeChange()"--%>
                                                                            <asp:RequiredFieldValidator ID="rfvContractCategory" ErrorMessage="Please Select Contract Type" InitialValue="0"
                                                                                ControlToValidate="ddlContractType" runat="server" ValidationGroup="ContractPopUpValidationGroup" Display="None" />
                                                                            <asp:LinkButton ID="lnkBtnRebind_Type" OnClick="lnkBtnRebind_Type_Click" Style="float: right; display: none;" Width="100%" runat="server"> 
                                                                            </asp:LinkButton>
                                                                        </div>
                                                                        <div class="col-md-1 col-sm-1 col-xs-1 plr0 mt5 text-right">
                                                                            <%-- <img id="lnkAddNewContractTypeModal" runat="server" src="/Images/add_icon_new.png"
                                                                                onclick="OpenAddNewTypePopup()" alt="Add" data-toggle="tooltip" data-placement="bottom" title="Add New Contract Type" />--%>
                                                                            <asp:ImageButton ID="lnkAddNewContractTypeModal" runat="server" ImageUrl="/Images/add_icon_new.png" OnClientClick="javascript:OpenAddNewTypePopup(); return false;"
                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip="Add New Contract Type" visible="false"/>
                                                                            <%--style="display: none;"--%>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group col-md-4">
                                                                        <label for="ddlContractSubType" class="control-label">Contract Sub-Type</label>
                                                                        <div class="col-md-11 col-sm-11 col-xs-11 plr0">
                                                                            <asp:DropDownListChosen runat="server" ID="ddlContractSubType" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                                class="form-control" Width="100%" DataPlaceHolder="Select Contract Type">
                                                                            </asp:DropDownListChosen>
                                                                            <%--onchange="ddlContractSubTypeChange()"--%>
                                                                            <asp:LinkButton ID="lnkBtnRebind_SubType" OnClick="lnkBtnRebind_SubType_Click" Style="float: right; display: none;" Width="100%" runat="server"> 
                                                                            </asp:LinkButton>
                                                                        </div>
                                                                        <div class="col-md-1 col-sm-1 col-xs-1 plr0 mt5 text-right" style="display:none;">
                                                                            <%--<img id="lnkAddNewContractSubTypeModal" runat="server" src="/Images/add_icon_new.png"
                                                                                onclick="OpenAddNewSubTypePopup()" alt="Add" data-toggle="tooltip" data-placement="bottom" title="Add New Contract Sub-Type" />--%>

                                                                            <asp:ImageButton ID="lnkAddNewContractSubTypeModal" runat="server" ImageUrl="/Images/add_icon_new.png" OnClientClick="javascript:OpenAddNewSubTypePopup(); return false;"
                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip="Add New Contract Sub-Type" visible="false" />
                                                                        </div>
                                                                    </div>
                                                                </ContentTemplate>
                                                                <Triggers>
                                                                    <%--<asp:AsyncPostBackTrigger ControlID="ddlContractType" EventName="SelectedIndexChanged" />--%>
                                                                    <%-- <asp:PostBackTrigger  ControlID="ddlContractType" />--%>
                                                                </Triggers>
                                                            </asp:UpdatePanel>

                                                            
                                                        </div>

                                                        <div class="row">

                                                            <div class="form-group required col-md-4">
                                                                <label for="lstBoxOwner" class="control-label">Contract Owner(s)</label>
                                                                <asp:ListBox ID="lstBoxOwner" CssClass="form-control" runat="server" SelectionMode="Multiple"></asp:ListBox>
                                                                <asp:RequiredFieldValidator ID="rfvOwner" ErrorMessage="Please Select Owner"
                                                                    ControlToValidate="lstBoxOwner" runat="server" ValidationGroup="ContractPopUpValidationGroup" Display="None" />
                                                            </div>
                                                            <div class="form-group col-md-4">
                                                                <label for="tbxContractAmt" class="control-label">Contract Amount/Value</label>
                                                                <asp:TextBox runat="server" ID="tbxContractAmt" CssClass="form-control" autocomplete="off" />
                                                                <asp:RegularExpressionValidator ID="revContractAmt" Display="None" runat="server"
                                                                    ValidationGroup="ContractPopUpValidationGroup" ErrorMessage="Please enter a valid Contract Amount/Value"
                                                                    ControlToValidate="tbxContractAmt" ValidationExpression="[0-9]+(\.[0-9][0-9]?)?"></asp:RegularExpressionValidator>
                                                            </div>

                                                             <div class="form-group col-md-4">
                                                                <label for="tbxTaxes" class="control-label">Taxes</label>
                                                                <asp:TextBox runat="server" ID="tbxTaxes" CssClass="form-control" autocomplete="off" />
                                                            </div>
                                                            
                                                            

                                                            
                                                            </div>
                                                        <div class="row">
                                                         <div class="form-group col-md-4">
                                                                <label for="ddlPaymentType" class="control-label">Payment Type</label>
                                                                <asp:DropDownListChosen runat="server" ID="ddlPaymentType" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                    class="form-control" Width="100%" DataPlaceHolder="Select Payment Type ">
                                                                    <asp:ListItem Text="Select Payment Type" Value="-1" Selected="True" ></asp:ListItem>
                                                                    <asp:ListItem Text="Payee" Value="0"></asp:ListItem>
                                                                    <asp:ListItem Text="Receipt" Value="1"></asp:ListItem>
                                                                    
                                                                </asp:DropDownListChosen>
                                                                <asp:RequiredFieldValidator ID="rfvPaymentType" ErrorMessage="Please Select Payment Type"
                                                                    ControlToValidate="ddlPaymentType" runat="server" ValidationGroup="ContractPopUpValidationGroup" Display="None" />
                                                            </div>
                                                            <div class="form-group col-md-4">
                                                                <label for="ddlPaymentTerm" class="control-label">Payment Term</label>
                                                                 <asp:ListBox ID="ddlPaymentTerm1" CssClass="form-control" runat="server" SelectionMode="Multiple" Width="100%">
                                                                </asp:ListBox>

                                                                <%--<asp:DropDownListChosen runat="server" ID="ddlPaymentTerm" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                    class="form-control" Width="100%" DataPlaceHolder="Select Payment Term">
                                                                    <asp:ListItem Text="One-Time" Value="0" Selected="True"></asp:ListItem>
                                                                    <asp:ListItem Text="Daily" Value="1"></asp:ListItem>
                                                                    <asp:ListItem Text="Weekly" Value="7"></asp:ListItem>
                                                                    <asp:ListItem Text="Periodically" Value="15"></asp:ListItem>
                                                                    <asp:ListItem Text="Monthly" Value="30"></asp:ListItem>
                                                                    <asp:ListItem Text="Quarterly" Value="4"></asp:ListItem>
                                                                    <asp:ListItem Text="Half-Yearly" Value="6"></asp:ListItem>
                                                                    <asp:ListItem Text="Yearly" Value="12"></asp:ListItem>
                                                                </asp:DropDownListChosen>
                                                                <asp:RequiredFieldValidator ID="rfvPaymentTerm" ErrorMessage="Please Select Payment Term"
                                                                    ControlToValidate="ddlPaymentTerm" runat="server" ValidationGroup="ContractPopUpValidationGroup" Display="None" />--%>
                                                            </div>

                                                            <div class="form-group col-md-4">
                                                                <label for="txtBoxProduct" class="control-label">Product/Items(s)</label>
                                                                <asp:TextBox ID="txtBoxProduct" CssClass="form-control" runat="server"></asp:TextBox>
                                                            </div>

                                                            <div class="form-group required col-md-4 d-none">
                                                                <label for="lstBoxApprover" class="control-label">Contract Approver(s)</label>
                                                                <asp:ListBox ID="lstBoxApprover" CssClass="form-control" runat="server" SelectionMode="Multiple"></asp:ListBox>
                                                                <%-- <asp:RequiredFieldValidator ID="rfvApprover" ErrorMessage="Please Select Approver"
                                                                    ControlToValidate="lstBoxApprover" runat="server" ValidationGroup="ContractPopUpValidationGroup" Display="None" />--%>
                                                            </div>
                                                        </div>
                                                         <div class="row">
                                                          <div class="form-group col-md-4">
                                                                <label for="txtlockinperiodDate" class="control-label">Lock-in Period</label>
                                                                <div class="input-group date">
                                                                    <span class="input-group-addon">
                                                                        <span class="fa fa-calendar color-black"></span>
                                                                    </span>
                                                                    <asp:TextBox ID="txtlockinperiodDate" runat="server" placeholder="DD-MM-YYYY" class="form-control" autocomplete="off" />
                                                                </div>
                                                            </div>
                                                            </div>
                                                         <div class="row">
                                                            <div class="form-group  col-md-12">
                                                                <label for="tbxAddNewClause" class="control-label">Add New Clause</label>
                                                                <asp:TextBox runat="server" ID="tbxAddNewClause" TextMode="MultiLine" CssClass="form-control" autocomplete="off" />
                                                                
                                                            </div>
                                                        </div>
                                                       
                                                           <div class="form-group col-md-12 colpadding0">
                                                                <label for="grdCustomField" id="lblCustomField" runat="server" class="control-label">Additional Field(s)</label>
                                                                <asp:UpdatePanel ID="upCustomField" runat="server">
                                                                    <ContentTemplate>
                                                                        <asp:GridView runat="server" ID="grdCustomField" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                                            GridLines="None" AllowPaging="true" AutoPostBack="true" CssClass="table" ShowFooter="true" ShowHeader="false"
                                                                            PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Right" OnRowCommand="grdCustomField_Common_RowCommand"
                                                                            OnRowDataBound="grdCustomField_Common_RowDataBound">
                                                                            <Columns>
                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%" Visible="false">
                                                                                    <ItemTemplate>
                                                                                        <%#Container.DataItemIndex+1 %>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField Visible="false">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblID" runat="server" Text='<%# Eval("LableID") %>'
                                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("LableID") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-Width="45%" FooterStyle-Width="45%" FooterStyle-CssClass="colpadding0"><%--HeaderText="Field"--%>
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; margin-top: 5px;">
                                                                                            <asp:Label ID="lblName" runat="server" Text='<%# Eval("Label") %>'
                                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Label") %>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                    <FooterTemplate>
                                                                                        <asp:DropDownListChosen runat="server" ID="ddlFieldName_Footer"     CssClass="plr0 unique-footer-select" DataPlaceHolder="Select"
                                                                                            AllowSingleDeselect="false" DisableSearchThreshold="5" Width="100%">
                                                                                            <%--onchange="ddlCustomFieldChange()"--%>
                                                                                        </asp:DropDownListChosen>
                                                                                        <%--<img id="imgAddNewCustomField" src='<%# ResolveUrl("~/Images/add_icon_new.png")%>'
                                                                                            onclick="OpenAddNewCustomFieldPopUp()" alt="Add" data-toggle="tooltip" data-placement="bottom" title=" Add New Custom Field" />--%>
                                                                                    </FooterTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-Width="0.5%" FooterStyle-Width="0.5%" FooterStyle-CssClass="text-center">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblImgAddNew" runat="server" Visible="false"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                    <FooterTemplate>
                                                                                        <img id="imgAddNewCustomField" src='<%# ResolveUrl("~/Images/add_icon_new.png")%>' onclick="OpenAddNewCustomFieldPopUp()" alt="Add"
                                                                                            data-toggle="tooltip" data-placement="bottom" title="Add New Custom Field" class="mt5" style="display:none;" />
                                                                                    </FooterTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-Width="45%" FooterStyle-Width="45%"><%--HeaderText="Value"--%>
                                                                                    <ItemTemplate>
                                                                                        <asp:TextBox ID="tbxLabelValue" runat="server" CssClass="form-control" PlaceHolder="Value" ReadOnly="true" Text='<%# Eval("labelValue") %>'></asp:TextBox>
                                                                                    </ItemTemplate>
                                                                                    <FooterTemplate>
                                                                                        <asp:TextBox runat="server" AutoPostBack="true" ID="txtFieldValue_Footer" PlaceHolder="Value"   CssClass="form-control" />
                                                                                    </FooterTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-Width="4.5%" FooterStyle-Width="4.5%"
                                                                                    ItemStyle-CssClass="text-right" HeaderStyle-CssClass="text-right" FooterStyle-CssClass="text-right"><%--HeaderText="Action"--%>
                                                                                    <ItemTemplate>
                                                                                        <asp:UpdatePanel runat="server" ID="upCustomFieldDelete" UpdateMode="Conditional" class="mt5">
                                                                                            <ContentTemplate>
                                                                                                <asp:LinkButton CommandArgument='<%# Eval("LableID")%>' ID="lnkBtnDeleteCustomField"
                                                                                                    AutoPostBack="true" CommandName="DeleteCustomField" runat="server"
                                                                                                    data-toggle="tooltip" data-placement="bottom" ToolTip="Delete"
                                                                                                    OnClientClick="return confirm('Are you sure!! You want to Delete this Custom Parameter?');">
                                                                                                    <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="Delete" />
                                                                                                </asp:LinkButton>
                                                                                            </ContentTemplate>
                                                                                        </asp:UpdatePanel>
                                                                                    </ItemTemplate>
                                                                                    <FooterTemplate>
                                                                                        <asp:LinkButton ID="lnkBtnAddCustomField" Visible="false" runat="server" AutoPostBack="true" OnClick="lnkBtnAddCustomField_Click"
                                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip="Save Custom Field With Value"
                                                                                            Text="Add" CssClass="btn btn-primary">                                                                                           
                                                                                        </asp:LinkButton>
                                                                                        <%--<img src='<%# ResolveUrl("~/Images/add_icon_new.png")%>' alt="Save" />--%>
                                                                                    </FooterTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                            <%--<RowStyle CssClass="clsROWgrid" />--%>
                                                                            <%--<HeaderStyle CssClass="clsheadergrid" />--%>
                                                                            <EmptyDataTemplate>
                                                                                No Records Found
                                                                            </EmptyDataTemplate>
                                                                        </asp:GridView>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </asp:Panel>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--Contract Detail Panel End-->
                                <div class="col-lg-12 col-md-12">
                                    <%--<p style="color: red;"><strong>Note:</strong> (*) fields are compulsory</p>--%>
                                </div>
                            </div>

                            <div class="form-group col-md-12 text-center" style="display:none;">
                                <asp:Button Text="Save" runat="server" ID="btnSaveContract" CssClass="btn btn-primary" OnClick="btnSaveContract_Click" OnClientClick="scrollUpPage()"
                                    ValidationGroup="ContractPopUpValidationGroup"></asp:Button>
                                <asp:Button Text="Clear" runat="server" ID="btnClearContractDetail" CssClass="btn btn-primary" OnClick="btnClearContractControls_Click" />
                            </div>

                            <div id="divContractHistory" runat="server" class="row Dashboard-white-widget" visible="false">
                                <!--Contract History Panel Start-->
                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-default">

                                        <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;" data-toggle="tooltip" data-placement="bottom" title="View Contract History">
                                            <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseDivContractHistoryDetails">
                                                <a>
                                                    <h2>Contract History</h2>
                                                </a>
                                                <div class="panel-actions">
                                                    <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion"
                                                        href="#collapseDivContractHistoryDetails">
                                                        <i class="fa fa-chevron-up"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="collapseDivContractHistoryDetails" class="panel-collapse collapse in">
                                            <div class="panel-body">
                                                <asp:Panel ID="Panel2" runat="server">
                                                    <div class="row">
                                                        <asp:UpdatePanel ID="upContractHistory" runat="server">
                                                            <ContentTemplate>
                                                                <asp:GridView runat="server" ID="grdContractHistory" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                                    GridLines="None" PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%"
                                                                    PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Right" OnRowCommand="grdContractHistory_RowCommand" OnPageIndexChanging="grdContractHistory_PageIndexChanging">

                                                                    <Columns>
                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%">
                                                                            <ItemTemplate>
                                                                              <%#Container.DataItemIndex+1 %>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="Contract No" ItemStyle-Width="20%">
                                                                            <ItemTemplate>
                                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                                                    <asp:Label ID="lblContractNo" runat="server" Text='<%# Eval("ContractNo") %>'
                                                                                        data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("ContractNo") %>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="Contract Title" ItemStyle-Width="25%">
                                                                            <ItemTemplate>
                                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                                                    <asp:Label ID="lblContractTitle" runat="server" Text='<%# Eval("ContractTitle") %>'
                                                                                        data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("ContractTitle") %>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="Vendor" ItemStyle-Width="15%">
                                                                            <ItemTemplate>
                                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px">
                                                                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("VendorNames") %>' ToolTip='<%# Eval("VendorNames") %>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="End Date" ItemStyle-Width="15%" ItemStyle-CssClass="text-center">
                                                                            <ItemTemplate>
                                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px">
                                                                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ExpirationDate") != null? Convert.ToDateTime(Eval("ExpirationDate")).ToString("dd-MM-yyyy") : " " %>'
                                                                                        ToolTip='<%# Eval("ExpirationDate") != null ? Convert.ToDateTime(Eval("ExpirationDate")).ToString("dd-MM-yyyy") : " " %>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="Action" ItemStyle-Width="10%" ItemStyle-CssClass="text-right" HeaderStyle-CssClass="text-right">
                                                                            <ItemTemplate>
                                                                                <asp:UpdatePanel runat="server" ID="UpdateHist" UpdateMode="Always">
                                                                                    <ContentTemplate>
                                                                                        <asp:LinkButton CommandArgument='<%# Eval("ID")%>'
                                                                                            AutoPostBack="true" CommandName="ViewContractPopup" ID="lnkViewContractHistory" runat="server"
                                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip="View">
                                                                                         <img src='<%# ResolveUrl("~/Images/eye.png")%>' alt="View"/>
                                                                                        </asp:LinkButton>
                                                                                    </ContentTemplate>
                                                                                    <Triggers>
                                                                                        <%--<asp:PostBackTrigger ControlID="lnkBtnDownLoadCaseDoc" />
                                                                                                <asp:PostBackTrigger ControlID="lnkBtnDeleteCaseDoc" />--%>
                                                                                    </Triggers>
                                                                                </asp:UpdatePanel>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                    <RowStyle CssClass="clsROWgrid" />
                                                                    <HeaderStyle CssClass="clsheadergrid" />
                                                                    <EmptyDataTemplate>
                                                                        No Records Found
                                                                    </EmptyDataTemplate>
                                                                </asp:GridView>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                </asp:Panel>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <!--Contract History Panel End-->
                            </div>

                            <!--Linked Contracts Panel Start-->
                            <asp:UpdatePanel ID="upLinkedContracts" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div id="divLinkedContracts" runat="server" class="row Dashboard-white-widget" visible="false">

                                        <div class="col-lg-12 col-md-12">
                                            <div class="panel panel-default">

                                                <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;" data-toggle="tooltip" data-placement="bottom" title="View Linked Contract(s)">
                                                    <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseDivLinkedContractDetails">
                                                        <a>
                                                            <h2>Linked Contract(s)</h2>
                                                        </a>
                                                        <div class="panel-actions">
                                                            <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseDivLinkedContractDetails">
                                                                <i class="fa fa-chevron-up"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div id="collapseDivLinkedContractDetails" class="panel-collapse collapse in">
                                                    <div class="panel-body">
                                                        <div class="col-md-12 plr0">
                                                            <asp:ValidationSummary ID="vsLinkedContracts" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                                                ValidationGroup="LinkedContractsValidationGroup" />
                                                            <asp:CustomValidator ID="cvLinkedContracts" runat="server" EnableClientScript="False"
                                                                ValidationGroup="LinkedContractsValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                                                        </div>

                                                        <asp:Panel ID="Panel4" runat="server">
                                                            <div class="row">
                                                                <asp:GridView runat="server" ID="grdLinkedContracts" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                                    GridLines="None" AutoPostBack="true" CssClass="table" Width="100%"
                                                                    OnRowCommand="grdLinkedContracts_RowCommand" OnRowDataBound="grdLinkedContracts_RowDataBound">
                                                                    <Columns>
                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%">
                                                                            <ItemTemplate>
                                                                                <%#Container.DataItemIndex+1 %>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="Contract No." ItemStyle-Width="20%">
                                                                            <ItemTemplate>
                                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px">
                                                                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ContractNo") %>' ToolTip='<%# Eval("ContractNo") %>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="Contract" ItemStyle-Width="30%">
                                                                            <ItemTemplate>
                                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                                                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ContractTitle") %>' ToolTip='<%# Eval("ContractTitle") %>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="Vendor" ItemStyle-Width="15%">
                                                                            <ItemTemplate>
                                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("VendorNames") %>' ToolTip='<%# Eval("VendorNames") %>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="Status" ItemStyle-Width="10%">
                                                                            <ItemTemplate>
                                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="Department" ItemStyle-Width="10%" Visible="false">
                                                                            <ItemTemplate>
                                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("DeptName") %>' ToolTip='<%# Eval("DeptName") %>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="Action" ItemStyle-Width="10%" ItemStyle-CssClass="text-right" HeaderStyle-CssClass="text-right">
                                                                            <ItemTemplate>
                                                                                <asp:UpdatePanel runat="server" ID="upLinkedContractAction" UpdateMode="Always">
                                                                                    <ContentTemplate>
                                                                                        <asp:LinkButton CommandArgument='<%# Eval("LinkedContractID") %>'
                                                                                            AutoPostBack="true" CommandName="ViewLinkedContract" ID="lnkViewLinkedContract" runat="server"
                                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip="View Contract Detail(s)">
                                                                                            <img src='<%# ResolveUrl("~/Images/View-icon-new.png")%>' alt="View"/>
                                                                                        </asp:LinkButton>

                                                                                        <asp:LinkButton CommandArgument='<%# Eval("ContractID")+","+Eval("LinkedContractID") %>'
                                                                                            AutoPostBack="true" CommandName="DeleteContractLinking" runat="server" ID="lnkBtnDeleteContractLinking"
                                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip="Delete this Contract Linking"
                                                                                            OnClientClick="return confirm('Are you sure!! You want to Delete this Linking with Contract?');">
                                                                                            <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="Delete" />
                                                                                        </asp:LinkButton>
                                                                                    </ContentTemplate>
                                                                                </asp:UpdatePanel>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                    <RowStyle CssClass="clsROWgrid" />
                                                                    <HeaderStyle CssClass="clsheadergrid" />
                                                                    <EmptyDataTemplate>
                                                                        No Contract Linked yet
                                                                    </EmptyDataTemplate>
                                                                </asp:GridView>
                                                            </div>
                                                        </asp:Panel>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <!--Linked Contracts  Panel End-->

                            <div class="form-group col-md-12" style="margin-left: 10px; float: left;">
                                <%--<p style="color: red;"><strong>Note:</strong> (*) fields are compulsory</p>--%>
                            </div>
                        </div>
                    </asp:View>

                    <asp:View ID="ContractDetailsView" runat="server">
                      
                    </asp:View>

                    <!--Contract Document Panel Start-->
                    <asp:View ID="DocumentView" runat="server">
                        
                          
                    </asp:View>
                    <!--Contract Document Panel End-->

                    <asp:View ID="TaskView" runat="server">
                      
                    </asp:View>

                    <asp:View ID="AuditLogView" runat="server">
                      
                    </asp:View>

                    <asp:View ID="ContractTransaction" runat="server">                     
                        <div>
                            <iframe id="showdetails" src="../Common/ContractTemplateTransactionReviwerDetail.aspx?UID=<% =UId%>&RID=<% =RoleId%>&ContractTID=<% =ContractTemplateInstanceId%>&CID=<% =CustId%>" width="100%" height="610px;" frameborder="0"></iframe>                     f       
                        </div>
                    </asp:View>

                </asp:MultiView>
            </div>
            <%--Contract CustomFields popup--%>
            <div class="modal fade" id="divCustomFieldPopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog" style="width: 35%;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                            <iframe src="about:blank" id="IframeCustomflds" runat="server" frameborder="0" width="100%" height="250px"></iframe>
                        </div>
                    </div>
                </div>
            </div>

            <%--Department Popup--%>
            <div class="modal fade" id="AddDepartmentPopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog w35per">
                    <div class="modal-content">
                        <div class="modal-header">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label class="modal-header-custom">
                                Add New Department</label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                            <iframe src="about:blank" id="IframeDepartment" frameborder="0" runat="server" width="100%" height="200px"></iframe>
                        </div>
                    </div>
                </div>
            </div>

             <%--Contact Person Department Popup--%>
            <div class="modal fade" id="AddPDepartmentPopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog " style="width: 40%;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label class="modal-header-custom">
                                Add New Contact Person of  Department</label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                            <iframe src="about:blank" id="IframePDepartment" frameborder="0" runat="server" width="100%" height="500px"></iframe>
                        </div>
                    </div>
                </div>
            </div>

            <%--Act Popup--%>
            <div class="modal fade" id="AddActPopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog" style="width: 40%;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label class="modal-header-custom">
                                Add New Act</label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                            <iframe src="about:blank" id="IframeAct" frameborder="0" runat="server" width="100%" height="170px"></iframe>
                        </div>
                    </div>
                </div>
            </div>

            <%--Add New Vendor--%>
            <div class="modal fade" id="AddVendorPopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog w90per">
                    <div class="modal-content">
                        <div class="modal-header">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label class="modal-header-custom">
                                Add New Vendor</label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                            <iframe src="about:blank" id="IframeParty" frameborder="0" runat="server" width="100%" height="500px"></iframe>
                        </div>
                    </div>
                </div>
            </div>

            <%--Add Legal Contract category--%>
            <div class="modal fade" id="AddContractType" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label class="modal-header-custom">
                                Add New Contract Type</label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                            <iframe src="about:blank" id="IframeCategoryType" frameborder="0" runat="server" width="100%" height="200px"></iframe>
                        </div>
                    </div>
                </div>
            </div>

            <%--Add Contract SubType--%>
            <div class="modal fade" id="AddContractSubTypePopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog" style="width: 35%;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label class="modal-header-custom">
                                Add New Contract Sub-Type</label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                            <iframe src="about:blank" id="Iframesubconttype" runat="server" frameborder="0" width="100%" height="250px"></iframe>
                        </div>
                    </div>
                </div>
            </div>

            <%--Add Court--%>
            <div class="modal fade" id="AddCourtsPopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog" style="width: 45%;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label class="modal-header-custom">
                                Add New Court</label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                            <iframe src="about:blank" id="IframeCourt" frameborder="0" runat="server" width="100%" height="450px"></iframe>
                        </div>
                    </div>
                </div>
            </div>

            <%--Add User--%>
            <div class="modal fade" id="AddUserPopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog" style="width: 40%;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label class="modal-header-custom">
                                Add New User</label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                            <iframe src="about:blank" id="IframeAddUser" frameborder="0" runat="server" width="100%" height="400px"></iframe>
                        </div>
                    </div>
                </div>
            </div>

            <div>
                <div class="modal fade" id="DocumentViewer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden;">
                    <div class="modal-dialog w100per">
                        <div class="modal-content">
                            <div class="modal-header">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                <label class="modal-header-custom">
                                    View Document(s)</label>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            </div>
                            <div class="modal-body" style="height: 570px;">
                                <div class="col-md-12 colpadding0">
                                    <div class="clearfix"></div>
                                    

                                    <div class="col-md-11 colpadding0">
                                        <asp:Label runat="server" ID="lblMessage" Style="color: red;"></asp:Label>
                                        <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; height: 550px; width: 100%;">
                                            <iframe src="about:blank" id="IFrameDocumentViewer" runat="server" width="100%" height="535px"></iframe>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <%--Contract History Popup--%>
            <div class="modal fade" id="divContractHistoryPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog p0" style="width: 100%;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label class="modal-header-custom" id="historyPopUpHeader">
                                Contract History</label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                            <iframe src="about:blank" id="IFrameContractHistory" frameborder="0" runat="server" width="100%" height="450px"></iframe>
                        </div>
                    </div>
                </div>
            </div>

            <%--Link Contract(s) Popup--%>
            <div class="modal fade" id="divLinkContractPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog p5" style="width: 80%;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label class="modal-header-custom">
                                Link Contract(s)</label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                            <asp:UpdatePanel ID="upLinkContracts" runat="server">
                                <ContentTemplate>
                                    <div style="margin-bottom: 7px">
                                        <asp:ValidationSummary ID="vsLinkContract" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                            ValidationGroup="VGLinkContracts" />
                                        <asp:CustomValidator ID="cvLinkContract" runat="server" EnableClientScript="False"
                                            ValidationGroup="VGLinkContracts" Display="None" />
                                    </div>
                                    <div class="row" style="margin: 10px">
                                        <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: #ff0000;">*</label>
                                        <label style="display: block; float: left; font-size: 13px; color: #333;">Select One or More Contract(s) and Click on Save to Link Contract(s)</label>
                                    </div>
                                    <div class="row" style="margin: 10px; max-height: 350px; overflow-y: auto;">
                                        <asp:GridView runat="server" ID="grdContractList_LinkContract" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                            AllowPaging="false" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%" DataKeyNames="ID" >
                                            <Columns>
                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblContractIID" runat="server" Text='<%# Eval("ID") %>' Visible="false"></asp:Label>
                                                        <%#Container.DataItemIndex+1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="5%">
                                                    <HeaderTemplate>
                                                        <asp:CheckBox ID="chkHeaderLinkContracts" runat="server" onclick="javascript:checkAll(this,'grdContractList_LinkContract')" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkRowLinkContracts" runat="server" onclick="javascript:checkUncheckRow(this)" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Contract No." ItemStyle-Width="20%">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px">
                                                            <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ContractNo") %>' ToolTip='<%# Eval("ContractNo") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Contract" ItemStyle-Width="30%">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                                            <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ContractTitle") %>' ToolTip='<%# Eval("ContractTitle") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Vendor" ItemStyle-Width="15%">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                            <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("VendorNames") %>' ToolTip='<%# Eval("VendorNames") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Status" ItemStyle-Width="10%">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                            <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("StatusName") %>' ToolTip='<%# Eval("StatusName") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Department" ItemStyle-Width="10%" Visible="false">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                            <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("DeptName") %>' ToolTip='<%# Eval("DeptName") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                            </Columns>
                                            <RowStyle CssClass="clsROWgrid" />
                                            <HeaderStyle CssClass="clsheadergrid" />
                                            <PagerSettings Visible="false" />
                                            <PagerTemplate>
                                            </PagerTemplate>
                                            <EmptyDataTemplate>
                                                No Record Found
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                    </div>
                                    <div id="divLinkContractSaveCount" class="row col-md-12 plr0" style="display: none;">
                                        <div class="col-md-5 text-left">
                                            <asp:Label runat="server" ID="lblTotalContractSelected" Text="" CssClass="control-label"></asp:Label>
                                        </div>
                                        <div class="col-md-7 text-left">
                                            <asp:Button Text="Save" runat="server" ID="btnSaveLinkContract" CssClass="btn btn-primary"
                                                OnClick="btnSaveLinkContract_Click" ValidationGroup="VGLinkContracts"></asp:Button>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
            <%--END--Link Contract(s) Popup--%>

            <%--Mail Document Popup--%>
            <div class="modal fade" id="divMailDocumentPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog" style="width: 50%;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label class="modal-header-custom">
                                Send Mail with Documents</label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                            <asp:UpdatePanel ID="upMailDocument" runat="server">
                                <ContentTemplate>
                                    <div class="row">
                                        <div class="form-group required col-md-12">
                                            <asp:ValidationSummary ID="vsMailDocument" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                                ValidationGroup="mailDocumentValidationGroup" />
                                            <asp:CustomValidator ID="cvMailDocument" runat="server" EnableClientScript="False"
                                                ValidationGroup="mailDocumentValidationGroup" Display="None" />
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group required col-md-12">
                                            <label for="tbxMailTo" class="control-label">To</label>
                                            <asp:TextBox runat="server" ID="tbxMailTo" CssClass="form-control" autocomplete="off" />
                                            <asp:RequiredFieldValidator ID="rfvMailTo" ErrorMessage="Required To"
                                                ControlToValidate="tbxMailTo" runat="server" ValidationGroup="mailDocumentValidationGroup" Display="None" />
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group required col-md-12">
                                            <label for="tbxMailMsg" class="control-label">Message</label>
                                            <asp:TextBox runat="server" ID="tbxMailMsg" TextMode="MultiLine" CssClass="form-control" />
                                            <asp:RequiredFieldValidator ID="rfvMailMsg" ErrorMessage="Required Message to sent"
                                                ControlToValidate="tbxMailMsg" runat="server" ValidationGroup="mailDocumentValidationGroup" Display="None" />
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group required col-md-12">
                                            <asp:GridView runat="server" ID="grdMailDocumentList" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                AllowPaging="false" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%" DataKeyNames="FileID">
                                                <Columns>
                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%">
                                                        <ItemTemplate>
                                                            <%#Container.DataItemIndex+1 %>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="5%">
                                                        <HeaderTemplate>
                                                            <asp:CheckBox ID="chkHeaderMailDocument" runat="server" onclick="javascript:checkAll_MailDocument(this)" />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkRowMailDocument" runat="server" onclick="javascript:checkUncheckRow_MailDocument(this)" />
                                                            <asp:Label ID="lblID" runat="server" Text='<%# Eval("FileID") %>' Visible="false"></asp:Label>
                                                            <asp:Label ID="lblFileName" runat="server" Text='<%# Eval("FileName") %>' Visible="false"></asp:Label>
                                                            <asp:Label ID="lblFilePath" runat="server" Text='<%# Eval("FilePath") %>' Visible="false"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Type" ItemStyle-Width="25%">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblDocType" runat="server" Text='<%# Eval("DocTypeName") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Document" ItemStyle-Width="60%">
                                                        <ItemTemplate>
                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                                <asp:Label ID="lblDocument" runat="server" Text='<%# Eval("FileName") %>' ToolTip='<%# Eval("FileName") %>'></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Version" ItemStyle-Width="5%" Visible="false">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblDocVersion" runat="server" Text='<%# Eval("Version") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <PagerStyle HorizontalAlign="Right" />
                                                <RowStyle CssClass="clsROWgrid" />
                                                <HeaderStyle CssClass="clsheadergrid" />
                                                <PagerTemplate>
                                                </PagerTemplate>
                                                <EmptyDataTemplate>
                                                    No Record Found
                                                </EmptyDataTemplate>
                                            </asp:GridView>
                                        </div>
                                    </div>

                                    <div id="divSendDocCount" class="row col-md-12 plr0" style="display: none;">
                                        <div class="col-md-5 text-left">
                                            <asp:Label runat="server" ID="lblTotalDocumentSelected" Text="" CssClass="control-label"></asp:Label>
                                        </div>
                                        <div class="col-md-7 text-left">
                                            <asp:Button Text="Send" runat="server" ID="btnSendMail" CssClass="btn btn-primary"></asp:Button>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
            <%--END--Mail Document Popup--%>

            <div class="modal fade" id="AddDocumentsPopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label class="modal-header-custom">
                                Upload Document(s)</label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                            <iframe src="about:blank" id="IframeAddDocuments" frameborder="0" runat="server" style="width:100%;height:320px;"></iframe>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="divDocumentInfoPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog p5" style="width: 50%;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label class="modal-header-custom">
                                Document Details</label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                            <iframe src="about:blank" id="Iframe_DocInfo" frameborder="0" runat="server" width="100%" height="400px"></iframe>
                        </div>
                    </div>
                </div>
            </div>


            <div class="modal fade" id="divRenewContractPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog p5" style="width: 50%;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label class="modal-header-custom">
                                Renew Contract</label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                            <div class="row form-group col-md-12">
                                <asp:ValidationSummary ID="vsRenewContract" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                    ValidationGroup="RenewContractValidationGroup" />
                                <asp:CustomValidator ID="cvRenewContract" runat="server" EnableClientScript="False"
                                    ValidationGroup="RenewContractValidationGroup" Display="None" />
                            </div>

                            <div class="row col-md-12 float-left">
                                <label for="txtTitle" class="control-label">Do you want to create an new contract with the same details as this contract?</label>
                            </div>

                            <div class="row col-md-12">
                                <label for="fuTaskDocUpload" class="control-label">Choose Contract Document(s) to Import</label>
                            </div>

                            <div class="row col-md-12" style="min-height: 50px; max-height: 300px; overflow-y: auto; display: none;">
                                <asp:UpdatePanel ID="upRenewContractModal" runat="server">
                                    <ContentTemplate>
                                        <asp:GridView runat="server" ID="grdContractDocument_Renew" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                            GridLines="None" AllowPaging="false" AutoPostBack="true" CssClass="table" Width="100%" ShowFooter="false">
                                            <Columns>
                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="5%">
                                                    <HeaderTemplate>
                                                        <asp:CheckBox ID="chkHeader_Import" runat="server" onclick="javascript:checkAll_Import(this)" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkRow_Import" runat="server" onclick="javascript:checkUncheckRow_Import(this)" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%">
                                                    <ItemTemplate>
                                                        <%#Container.DataItemIndex+1 %>
                                                        <asp:Label ID="lblFileID" runat="server" Text='<%# Eval("FileID") %>' Visible="false"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Type" ItemStyle-Width="15%">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDocType" runat="server" Text='<%# Eval("DocTypeName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Document" ItemStyle-Width="30%">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 400px">
                                                            <asp:Label ID="lblFileName" runat="server" Text='<%# Eval("FileName") %>'
                                                                data-toggle="tooltip" data-placement="right" ToolTip='<%# Eval("FileName") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Version" ItemStyle-Width="5%">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDocVersion" runat="server" Text='<%# Eval("Version") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Uploaded By" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="15%">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                            <asp:Label ID="lblUploadedBy" runat="server" Text='<%# Eval("UploadedByName") %>'
                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("UploadedByName") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Uploaded On" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="15%">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                            <asp:Label ID="lblUploadedOn" runat="server" Text='<%# Eval("CreatedOn") != DBNull.Value ? Convert.ToDateTime(Eval("CreatedOn")).ToString("dd-MM-yyyy") : "" %>'
                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("CreatedOn") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <RowStyle CssClass="clsROWgrid" />
                                            <HeaderStyle CssClass="clsheadergrid" />
                                            <EmptyDataTemplate>
                                                No Records Found
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>

                            <div id="divDocCount_ImportRenew" class="row col-md-12 pl0" style="display: none;">
                                <div class="col-md-12 text-left">
                                    <asp:Label runat="server" ID="lblImportDocCount" Text="" CssClass="control-label"></asp:Label>
                                </div>
                            </div>

                            <div class="row col-md-12 text-center">
                                <asp:Button Text="Yes" runat="server" ID="btnRenewContract" CssClass="btn btn-primary" OnClick="btnRenewContract_Click" />
                                <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="btn btn-primary" data-dismiss="modal" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="divShowDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header bgColor-gray" style="height: 30px;">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label class="modal-header-custom col-md-6 plr0">
                                View Contract Detail(s)</label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>

                        <div class="modal-body" style="background-color: #f7f7f7;">
                            <iframe id="showdetails" src="about:blank" width="95%" height="80%" frameborder="0"></iframe>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </form>
    
</body>
</html>

