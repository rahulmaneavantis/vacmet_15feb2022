﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Ionic.Zip;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using System.Web.Services;
using System.Threading.Tasks;
using System.Text;
using System.Globalization;
using com.VirtuosoITech.ComplianceManagement.Business.DataContract;

namespace com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.aspxPages
{
    public partial class ContractTemplateDetailsPage : System.Web.UI.Page
    {
        public static string DocumentPath = "";

        protected static String[] items;

        public static List<long> Branchlist = new List<long>();
        protected static int TemplateId;
        protected static int CustId;
        protected static int UId;
        protected static int RoleId;
        protected static int ContractTemplateInstanceId;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                VSContractPopup.CssClass = "alert alert-danger";                
                items = new String[] { "Not Started", "In Progress", "Under Review", "Closed" };

                //Page.Header.DataBind();

                if (!IsPostBack)
                {
                    BindPaymentTerms();
                    BindContractStatusList();
                    BindVendors();
                    BindCustomerBranches();
                    BindDepartments();
                    BindUsers();
                    //  BindGrid(Convert.ToInt32(ViewState["ContractInstanceID"]));
                    BindContractCategoryType();
                    BindContractStatusList(false, true);

                    ViewState["PageLink"] = "ContractLink";

                    if (!string.IsNullOrEmpty(Request.QueryString["AccessID"])
                        && !string.IsNullOrEmpty(Request.QueryString["RID"]))
                    {
                        var contractInstanceID = Request.QueryString["AccessID"];
                        if (contractInstanceID != "")
                        {
                            ViewState["ContractInstanceID"] = contractInstanceID;
                            Session["ContractInstanceID"] = contractInstanceID;

                            if (!string.IsNullOrEmpty(Request.QueryString["HistoryFlag"]))
                            {
                                ViewState["FlagHistory"] = "1";
                            }
                            else
                            {
                                ViewState["FlagHistory"] = null;
                            }

                            if (Convert.ToInt64(contractInstanceID) == 0)
                            {
                                btnAddContract_Click(sender, e);  //Add New Detail
                            }
                            else
                            {
                                btnEditContract_Click(sender, e); //Edit Detail 
                                RoleId = getRoleID(Convert.ToInt32(contractInstanceID), AuthenticationHelper.UserID);
                                TemplateId = Convert.ToInt32(contractInstanceID);
                                UId = Convert.ToInt32(AuthenticationHelper.UserID);
                                CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                            }
                        }
                    }

                    ViewState["CustomefieldCount"] = null;

                    //Show Hide Grid Control - Enable/Disable Form Controls
                    if (ViewState["ContractStatusID"] != null)
                    {
                        if (Convert.ToInt64(ViewState["ContractStatusID"]) == 9)
                        {
                            showHideContractSummaryTabTopButtons(false);
                            enableDisableContractPopUpControls(false);
                        }
                        else
                        {
                            showHideContractSummaryTabTopButtons(true);
                            enableDisableContractPopUpControls(true);
                        }
                    }
                    else
                    {
                        enableDisableContractPopUpControls(true);
                    }

                    if (!string.IsNullOrEmpty(Request.QueryString["HistoryFlag"]))
                    {
                        ViewState["FlagHistory"] = "1";

                        enableDisableContractPopUpControls(false);
                        showHideContractSummaryTabTopButtons(false);
                
                    }
                    else
                    {
                        ViewState["FlagHistory"] = null;
                
                    }
                }

                
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divBranches');", tbxBranch.ClientID), true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "hideDivTree", "hideDivBranch();", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        #region Common



        private void BindContractStatusList()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                drdTemplateID.DataSource = null;
                drdTemplateID.DataBind();
                drdTemplateID.ClearSelection();

                drdTemplateID.DataTextField = "TemplateName";
                drdTemplateID.DataValueField = "ID";

                var statusList = ContractTaskManagement.GetApprovedTemplateList(customerID);

                drdTemplateID.DataSource = statusList;
                drdTemplateID.DataBind();

                drdTemplateID.Enabled = false;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindContractStatusList(bool isForTask, bool isVisibleToUser)
        {
            try
            {
                ddlContractStatus.DataSource = null;
                ddlContractStatus.DataBind();
                ddlContractStatus.ClearSelection();

                ddlContractStatus.DataTextField = "StatusName";
                ddlContractStatus.DataValueField = "ID";

                var statusList = ContractTaskManagement.GetStatusList_All(isForTask, isVisibleToUser);

                ddlContractStatus.DataSource = statusList;
                ddlContractStatus.DataBind();

                items = statusList.Select(row => row.StatusName).ToArray();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindContractCategoryType()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                var lstContractTypes = ContractTypeMasterManagement.GetContractTypes_All(customerID);

                if (lstContractTypes.Count > 0)
                    lstContractTypes = lstContractTypes.OrderBy(row => row.TypeName).ToList();

                ddlContractType.DataTextField = "TypeName";
                ddlContractType.DataValueField = "ID";

                ddlContractType.DataSource = lstContractTypes;
                ddlContractType.DataBind();

                //ddlContractType.Items.Add(new ListItem("Add New", "0"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lnkBtnRebind_Type_Click(object sender, EventArgs e)
        {
            BindContractCategoryType();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "rebindTypes", "rebindContractTypes();", true);
        }

        private bool BindContractSubType(long contractTypeID)
        {
            bool bindSuccess = false;
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                var lstContractSubTypes = ContractTypeMasterManagement.GetContractSubType_All(customerID, contractTypeID);
                if (lstContractSubTypes.Count > 0)
                {
                    ddlContractSubType.DataTextField = "SubTypeName";
                    ddlContractSubType.DataValueField = "ContractSubTypeID";
                    ddlContractSubType.DataSource = lstContractSubTypes;
                    ddlContractSubType.DataBind();
                    //ddlContractSubType.Items.Add(new ListItem("Add New", "0"));
                    bindSuccess = true;
                }
                return bindSuccess;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return bindSuccess;
            }
        }

        protected void lnkBtnRebind_SubType_Click(object sender, EventArgs e)
        {
            if (ddlContractType.SelectedValue != null && ddlContractType.SelectedValue != "0" && ddlContractType.SelectedValue != "")
            {
                BindContractSubType(Convert.ToInt64(ddlContractType.SelectedValue));
                ScriptManager.RegisterStartupScript(this, this.GetType(), "rebindTypes", "rebindSubTypes();", true);
            }
        }

        
        
        
        private void BindCustomerBranches()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                tvBranches.Nodes.Clear();
                NameValueHierarchy branch = null;

                //var branchs = CustomerBranchManagement.GetAllHierarchy(customerID);
                //var branchs = CustomerBranchManagement.GetAllHierarchyManagementSatutory(customerID);
                List<NameValueHierarchy> branchs;
                string key = "LocationHierarchy" + AuthenticationHelper.CustomerID;
                if (CacheHelper.Exists(key))
                {
                    CacheHelper.Get<List<NameValueHierarchy>>(key, out branchs);
                }
                else
                {
                    branchs = CustomerBranchManagement.GetAllHierarchyManagementSatutory(customerID);
                    CacheHelper.Set<List<NameValueHierarchy>>(key, branchs);
                }
                if (branchs.Count > 0)
                {
                    branch = branchs[0];
                }

                //tbxBranch.Text = "Select Entity/Location";
                tbxBranch.Text = "Click to Select";

                List<TreeNode> nodes = new List<TreeNode>();
                BindBranchesHierarchy(null, branch, nodes);
                foreach (TreeNode item in nodes)
                {
                    tvBranches.Nodes.Add(item);
                }

                tvBranches.CollapseAll();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvContractPopUp.IsValid = false;
                cvContractPopUp.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp, List<TreeNode> nodes)
        {
            try
            {
                if (nvp != null)
                {
                    foreach (var item in nvp.Children)
                    {
                        TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                        BindBranchesHierarchy(node, item, nodes);
                        if (parent == null)
                        {
                            nodes.Add(node);
                        }
                        else
                        {
                            parent.ChildNodes.Add(node);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvContractPopUp.IsValid = false;
                cvContractPopUp.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        public void BindDepartments()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            var lstDepts = CompDeptManagement.GetAllDepartmentMasterList(customerID);

            if (lstDepts.Count > 0)
                lstDepts = lstDepts.OrderBy(row => row.Name).ToList();

            ddlDepartment.DataTextField = "Name";
            ddlDepartment.DataValueField = "ID";

            ddlDepartment.DataSource = lstDepts;
            ddlDepartment.DataBind();

            //ddlDepartment.Items.Add(new ListItem("Add New", "0"));
        }


        protected void lnkBtnRebind_Dept_Click(object sender, EventArgs e)
        {
            BindDepartments();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "rebindDepts", "rebindDepartments();", true);
        }

        protected void lnkBtnRebind_ContactPersonDept_Click(object sender, EventArgs e)
        {

            BindUsers();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "rebindTaskUser", "restoreSelectedTaskUsersNew();", true);
        }


        public void BindGrid(int contractID)
        {
            try
            {
                if (contractID != 0)
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        var lstLinkedContracts = (from row in entities.Cont_SP_GetAssignedContracts_All(Convert.ToInt32(AuthenticationHelper.CustomerID))
                                                  select row).ToList();
                        if (lstLinkedContracts.Count > 0)
                        {
                            lstLinkedContracts = (from g in lstLinkedContracts
                                                  group g by new
                                                  {
                                                      g.ID, //ContractID
                                                      g.CustomerID,
                                                      g.ContractNo,
                                                      g.ContractTitle,
                                                      g.ContractDetailDesc,
                                                      g.VendorIDs,
                                                      g.VendorNames,
                                                      g.CustomerBranchID,
                                                      g.BranchName,
                                                      g.DepartmentID,
                                                      g.DeptName,
                                                      g.ContractTypeID,
                                                      g.TypeName,
                                                      g.ContractSubTypeID,
                                                      g.SubTypeName,
                                                      g.ProposalDate,
                                                      g.AgreementDate,
                                                      g.EffectiveDate,
                                                      g.ReviewDate,
                                                      g.ExpirationDate,
                                                      g.CreatedOn,
                                                      g.UpdatedOn,
                                                      g.StatusName,
                                                      g.ContractAmt,
                                                      g.ContactPersonOfDepartment,
                                                      g.PaymentType,
                                                      g.AddNewClause
                                                  } into GCS
                                                  select new Cont_SP_GetAssignedContracts_All_Result()
                                                  {
                                                      ID = GCS.Key.ID, //ContractID
                                                      CustomerID = GCS.Key.CustomerID,
                                                      ContractNo = GCS.Key.ContractNo,
                                                      ContractTitle = GCS.Key.ContractTitle,
                                                      ContractDetailDesc = GCS.Key.ContractDetailDesc,
                                                      VendorIDs = GCS.Key.VendorIDs,
                                                      VendorNames = GCS.Key.VendorNames,
                                                      CustomerBranchID = GCS.Key.CustomerBranchID,
                                                      BranchName = GCS.Key.BranchName,
                                                      DepartmentID = GCS.Key.DepartmentID,
                                                      DeptName = GCS.Key.DeptName,
                                                      ContractTypeID = GCS.Key.ContractTypeID,
                                                      TypeName = GCS.Key.TypeName,
                                                      ContractSubTypeID = GCS.Key.ContractSubTypeID,
                                                      SubTypeName = GCS.Key.SubTypeName,
                                                      ProposalDate = GCS.Key.ProposalDate,
                                                      AgreementDate = GCS.Key.AgreementDate,
                                                      EffectiveDate = GCS.Key.EffectiveDate,
                                                      ReviewDate = GCS.Key.ReviewDate,
                                                      ExpirationDate = GCS.Key.ExpirationDate,
                                                      CreatedOn = GCS.Key.CreatedOn,
                                                      UpdatedOn = GCS.Key.UpdatedOn,
                                                      StatusName = GCS.Key.StatusName,
                                                      ContractAmt = GCS.Key.ContractAmt,
                                                      ContactPersonOfDepartment = GCS.Key.ContactPersonOfDepartment,
                                                      PaymentType = GCS.Key.PaymentType,
                                                      AddNewClause = GCS.Key.AddNewClause
                                                  }).ToList();
                        }
                        lstLinkedContracts = lstLinkedContracts.Where(entry => entry.ID != contractID).ToList();

                        grdContractList_LinkContract.DataSource = lstLinkedContracts;
                        grdContractList_LinkContract.DataBind();
                    }
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
        }

        public static void BindContractDocuments(GridView grd, ListBox lstBoxFileTags)
        {
            if (HttpContext.Current.Session["ContractInstanceID"] != null)
            {
                long ContractInstanceID = Convert.ToInt64(HttpContext.Current.Session["ContractInstanceID"]);

                List<ListItem> selectedItems = ContractManagement.GetSelectedItems(lstBoxFileTags);

                var selectedFileTags = selectedItems.Select(row => row.Text).ToList();

                List<Cont_SP_GetContractDocuments_FileTags_All_Result> lstContDocs = new List<Cont_SP_GetContractDocuments_FileTags_All_Result>();

                lstContDocs = ContractDocumentManagement.Cont_SP_GetContractDocuments_FileTags_All(Convert.ToInt32(AuthenticationHelper.CustomerID), ContractInstanceID, selectedFileTags);

                grd.DataSource = lstContDocs;
                grd.DataBind();

                foreach (ListItem eachItem in lstBoxFileTags.Items)
                {
                    eachItem.Attributes.Add("class", "label label-info");
                }

                lstContDocs.Clear();
                lstContDocs = null;

                //upContractDocUploadPopup.Update();
            }
        }

        [WebMethod]
        public static List<ListItem> GetDepartments()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            var lstDepartments = CompDeptManagement.GetAllDepartmentMasterList(customerID);

            List<ListItem> lstDepts = new List<ListItem>();

            if (lstDepartments.Count > 0)
            {
                lstDepartments = lstDepartments.OrderBy(row => row.Name).ToList();

                for (int i = 0; i < lstDepartments.Count; i++)
                {
                    lstDepts.Add(new ListItem
                    {
                        Value = lstDepartments.ToList()[i].ID.ToString(),
                        Text = lstDepartments.ToList()[i].Name,
                    });
                }

                //lstDepts.Add(new ListItem
                //{
                //    Value = "0",
                //    Text = "Add New",
                //});
            }

            return lstDepts;
        }

        [WebMethod]
        public static List<ListItem> GetContractTypes()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            var lstTypes = ContractTypeMasterManagement.GetContractTypes_All(customerID);

            List<ListItem> lstcontType = new List<ListItem>();

            if (lstTypes.Count > 0)
            {
                lstTypes = lstTypes.OrderBy(row => row.TypeName).ToList();

                for (int i = 0; i < lstTypes.Count; i++)
                {
                    lstcontType.Add(new ListItem
                    {
                        Value = lstTypes.ToList()[i].ID.ToString(),
                        Text = lstTypes.ToList()[i].TypeName,
                    });
                }

                //lstcontType.Add(new ListItem
                //{
                //    Value = "0",
                //    Text = "Add New",
                //});
            }

            return lstcontType;
        }

        [WebMethod]
        public static List<ListItem> GetContractSubTypes(string selectedContractType)
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            List<ListItem> lstContSubType = new List<ListItem>();

            long contTypeID = Convert.ToInt64(selectedContractType);
            if (contTypeID > 0)
            {
                var obj = ContractTypeMasterManagement.GetContractSubType_All(customerID, contTypeID);

                if (obj.Count > 0)
                {
                    obj = obj.OrderBy(row => row.SubTypeName).ToList();

                    for (int i = 0; i < obj.Count; i++)
                    {
                        lstContSubType.Add(new ListItem
                        {
                            Value = obj.ToList()[i].ContractSubTypeID.ToString(),
                            Text = obj.ToList()[i].SubTypeName,
                        });
                    }

                    //lstContSubType.Add(new ListItem
                    //{
                    //    Value = "0",
                    //    Text = "Add New",
                    //});
                }
            }
            return lstContSubType;
        }

        [WebMethod]
        public static List<ListItem> GetContractVendors()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            List<ListItem> lstVendors = new List<ListItem>();

            var obj = VendorDetails.GetVendors_All(customerID);

            if (obj.Count > 0)
            {
                obj = obj.OrderBy(row => row.VendorName).ToList();

                for (int i = 0; i < obj.Count; i++)
                {
                    lstVendors.Add(new ListItem
                    {
                        Value = obj.ToList()[i].ID.ToString(),
                        Text = obj.ToList()[i].VendorName,
                    });
                }

                //lstVendors.Add(new ListItem
                //{
                //    Value = "0",
                //    Text = "Add New",
                //});
            }

            return lstVendors;
        }

        [WebMethod]
        public static List<ListItem> GetTaskUsers()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            List<ListItem> lstInternalUsers = new List<ListItem>();

            var lstAllUsers = ContractUserManagement.GetAllUsers_Contract(customerID);

            //var allUsers = ContractUserManagement.GetRequiredUsers(lstAllUsers, 4); //4--All---Internal and External Users

            lstAllUsers = lstAllUsers.Where(row => row.IsActive == true).ToList();

            if (lstAllUsers.Count > 0)
            {
                for (int i = 0; i < lstAllUsers.Count; i++)
                {
                    lstInternalUsers.Add(new ListItem
                    {
                        Value = lstAllUsers.ToList()[i].ID.ToString(),
                        Text = lstAllUsers.ToList()[i].FirstName + " " + lstAllUsers.ToList()[i].LastName,
                    });
                }

                //lstInternalUsers.Add(new ListItem
                //{
                //    Value = "0",
                //    Text = "Add New",
                //});
            }

            return lstInternalUsers;
        }

        [WebMethod]
        public static List<ListItem> GetDocTypes()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            List<ListItem> lstdocumentType = new List<ListItem>();

            var lstAllDocTypes = ContractMastersManagement.GetContractDocTypes_All(customerID);
            if (lstAllDocTypes.Count > 0)
            {
                lstAllDocTypes = lstAllDocTypes.OrderBy(row => row.TypeName).ToList();

                for (int i = 0; i < lstAllDocTypes.Count; i++)
                {
                    lstdocumentType.Add(new ListItem
                    {
                        Value = lstAllDocTypes.ToList()[i].ID.ToString(),
                        Text = lstAllDocTypes.ToList()[i].TypeName,
                    });
                }

                //lstdocumentType.Add(new ListItem
                //{
                //    Value = "0",
                //    Text = "Add New",
                //});
            }

            return lstdocumentType;
        }

        [WebMethod]
        public static List<ListItem> GetCustomFields(string selectedContractType)
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            long typeID = Convert.ToInt64(selectedContractType);

            List<ListItem> lstCustomFields = new List<ListItem>();

            var lstAllCustomFields = ContractMastersManagement.GetCustomFields_All(customerID, typeID);

            if (lstAllCustomFields.Count > 0)
            {
                lstAllCustomFields = lstAllCustomFields.OrderBy(row => row.Label).ToList();

                for (int i = 0; i < lstAllCustomFields.Count; i++)
                {
                    lstCustomFields.Add(new ListItem
                    {
                        Value = lstAllCustomFields.ToList()[i].ID.ToString(),
                        Text = lstAllCustomFields.ToList()[i].Label,
                    });
                }

                //lstCustomFields.Add(new ListItem
                //{
                //    Value = "0",
                //    Text = "Add New",
                //});
            }

            return lstCustomFields;
        }
        public void BindPaymentTerms()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            var key = "Contpayterm-" + Convert.ToString(customerID);
            var lstpaymentterm = (List<Cont_tbl_PaymentTerm>)HttpContext.Current.Cache[key];
            if (HttpContext.Current.Cache[key] == null)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    lstpaymentterm = (from row in entities.Cont_tbl_PaymentTerm
                                      where row.IsDeleted == false
                                      select row).ToList();
                }
                HttpContext.Current.Cache.Insert(key, lstpaymentterm, null, DateTime.Now.AddMinutes(1440), TimeSpan.Zero); // add it to cache
            }

            if (lstpaymentterm.Count > 0)
                lstpaymentterm = lstpaymentterm.OrderBy(row => row.Id).ToList();

            ddlPaymentTerm1.DataTextField = "PaymentTermName";
            ddlPaymentTerm1.DataValueField = "PaymentTermID";

            ddlPaymentTerm1.DataSource = lstpaymentterm;
            ddlPaymentTerm1.DataBind();
        }
        public void BindVendors()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            var lstVendors = VendorDetails.GetVendors_All(customerID);

            if (lstVendors.Count > 0)
                lstVendors = lstVendors.OrderBy(row => row.VendorName).ToList();

            //Drop-Down at Modal Pop-up
            lstBoxVendor.DataTextField = "VendorName";
            lstBoxVendor.DataValueField = "ID";

            lstBoxVendor.DataSource = lstVendors;
            lstBoxVendor.DataBind();

            //lstBoxVendor.Items.Add(new ListItem("Add New", "0"));
        }

        protected void lnkBtnRebind_Vendor_Click(object sender, EventArgs e)
        {
            BindVendors();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "rebindVendors", "restoreSelectedVendors();", true);
        }

        public void BindUsers()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                var lstAllUsers = ContractUserManagement.GetAllUsers_Contract(customerID);

                if (lstAllUsers.Count > 0)
                    lstAllUsers = lstAllUsers.Where(entry => entry.ContractRoleID != null).ToList();

                var internalUsers = ContractUserManagement.GetRequiredUsers(lstAllUsers, 1); //1--Internal---

                lstBoxOwner.DataValueField = "ID";
                lstBoxOwner.DataTextField = "Name";
                lstBoxOwner.DataSource = internalUsers;
                lstBoxOwner.DataBind();

                lstBoxApprover.DataValueField = "ID";
                lstBoxApprover.DataTextField = "Name";
                lstBoxApprover.DataSource = internalUsers;
                lstBoxApprover.DataBind();



                var allUsers = ContractUserManagement.GetRequiredUsers(lstAllUsers, 4); //4--All---Internal and External Users

                ddlCPDepartment.DataValueField = "ID";
                ddlCPDepartment.DataTextField = "Name";
                ddlCPDepartment.DataSource = allUsers;
                ddlCPDepartment.DataBind();
                
                lstAllUsers.Clear();
                lstAllUsers = null;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindTaskUsers()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                var lstAllUsers = ContractUserManagement.GetAllUsers_Contract(customerID);

                if (lstAllUsers.Count > 0)
                    lstAllUsers = lstAllUsers.Where(entry => entry.ContractRoleID != null).ToList();

                var allUsers = ContractUserManagement.GetRequiredUsers(lstAllUsers, 4); //4--All---Internal and External Users
                
                //lstBoxTaskUser.Items.Add(new ListItem("Add New", "0"));

                lstAllUsers.Clear();
                lstAllUsers = null;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lnkBtnRebind_TaskUser_Click(object sender, EventArgs e)
        {
            BindTaskUsers();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "rebindTaskUser", "restoreSelectedTaskUsers();", true);
        }

        public void BindContractAuditLogs()
        {
            try
            {
                long totalRowCount = 0;

                if (ViewState["ContractInstanceID"] != null)
                {
                    int customerID = -1;
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                    int userID = -1;
                
                    string fromDate = string.Empty;
                    string toDate = string.Empty;

                    long contractInstanceID = Convert.ToInt64(ViewState["ContractInstanceID"]);

                    if (contractInstanceID != 0)
                    {
                        var lstAuditLogs = ContractManagement.GetContractAuditLogs_All(customerID, contractInstanceID);

                        if (userID != -1 && userID != 0)
                            lstAuditLogs = lstAuditLogs.Where(row => row.CreatedBy == userID).ToList();

                
                    }
                    else
                    {
                
                    }
                }
                else
                {
                
                }

                Session["TotalRows"] = totalRowCount;
                //lblStartRecord_AuditLog.Text = ddlPageNo_AuditLog.SelectedValue;

                if (Convert.ToInt32(ViewState["PageNumberAuditLogFlagID"]) == 0)
                {
            
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindContractHistory(int customerID, long contractInstanceID)
        {
            if (contractInstanceID != 0)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var lstContractHistory = (from row in entities.Cont_SP_ContractHistory(contractInstanceID, customerID)
                                              select row).ToList();

                    if (lstContractHistory != null)
                    {
                        grdContractHistory.DataSource = lstContractHistory;
                        grdContractHistory.DataBind();

                        if (lstContractHistory.Count > 0)
                            divContractHistory.Visible = true;
                        else
                            divContractHistory.Visible = false;
                    }
                }
            }
            else
                divContractHistory.Visible = false;
        }

        private void HideShowGridColumns(GridView gridView, string headerTextToMatch, bool flag)
        {
            try
            {
                if (gridView == null)
                {
                    return;
                }

                // Loop through all of the columns in the grid.
                for (int i = 0; i < gridView.Columns.Count; i++)
                {
                    String headerText = gridView.Columns[i].HeaderText;

                    //Show Hide Columns with Specific headerText
                    if (headerText == headerTextToMatch)
                        gridView.Columns[i].Visible = flag;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


        protected void TabContract_Click(object sender, EventArgs e)
        {
            ViewState["PageLink"] = "ContractLink";
            liContractSummary.Attributes.Add("class", "active");
            liContractDetails.Attributes.Add("class", "");
            liDocument.Attributes.Add("class", "");
            liTask.Attributes.Add("class", "");
            liAuditLog.Attributes.Add("class", "");
            liContractTransaction.Attributes.Add("class", "");

            MainView.ActiveViewIndex = 0;
            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["ContractInstanceID"])))
            {
                int ContractID = Convert.ToInt32(ViewState["ContractInstanceID"]);
                BindContractListToLink(ContractID);
            }

            if (ViewState["Mode"] != null)
            {
                if (Convert.ToInt32(ViewState["Mode"]) == 0)
                {
                    enableDisableContractSummaryTabControls(true);
                    toggleTextSaveButton_ContractSummaryTab(true);

                }
                else if (Convert.ToInt32(ViewState["Mode"]) == 1)
                {
                    bool historyFlag = false;
                    if (ViewState["FlagHistory"] != null)
                    {
                        historyFlag = Convert.ToBoolean(Convert.ToInt32(ViewState["FlagHistory"]));
                    }

                    enableDisableContractSummaryTabControls(false);
                    toggleTextSaveButton_ContractSummaryTab(false);

                    if (historyFlag)
                        showHideContractSummaryTabTopButtons(false);
                    else
                        showHideContractSummaryTabTopButtons(true);
                }
            }
        }

        protected void TabContractDetail_Click(object sender, EventArgs e)
        {
            ViewState["PageLink"] = "ContractLink";
            showHideContractSummaryTabTopButtons(false);

            liContractSummary.Attributes.Add("class", "");
            liContractDetails.Attributes.Add("class", "active");
            liDocument.Attributes.Add("class", "");
            liTask.Attributes.Add("class", "");
            liAuditLog.Attributes.Add("class", "");
            liContractTransaction.Attributes.Add("class", "");

            MainView.ActiveViewIndex = 1;
            
            BindContractListToLink(Convert.ToInt32(ViewState["ContractInstanceID"]));
        }

        protected void TabDocument_Click(object sender, EventArgs e)
        {
            ViewState["PageLink"] = "DocumentLink";
            showHideContractSummaryTabTopButtons(false);
            ViewState["PageNumberFlagID"] = 0;
            

            liContractSummary.Attributes.Add("class", "");
            liContractDetails.Attributes.Add("class", "");
            liDocument.Attributes.Add("class", "active");
            liTask.Attributes.Add("class", "");
            liAuditLog.Attributes.Add("class", "");
            liContractTransaction.Attributes.Add("class", "");

            MainView.ActiveViewIndex = 2;

            //BindContractDocType();
            
            BindGrid(Convert.ToInt32(ViewState["ContractInstanceID"]));
        }

        protected void TabTask_Click(object sender, EventArgs e)
        {
            ViewState["PageLink"] = "ContractLink";
            showHideContractSummaryTabTopButtons(false);

            liContractSummary.Attributes.Add("class", "");
            liContractDetails.Attributes.Add("class", "");
            liDocument.Attributes.Add("class", "");
            liTask.Attributes.Add("class", "active");
            liAuditLog.Attributes.Add("class", "");
            liContractTransaction.Attributes.Add("class", "");

            MainView.ActiveViewIndex = 3;

            ViewState["TaskMode"] = "0";

            ViewState["TaskPageFlag"] = "0";

            if (ViewState["ContractInstanceID"] != null)
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                long contractInstanceID = Convert.ToInt64(ViewState["ContractInstanceID"]);

                if (contractInstanceID != 0)
                {
            
                }
                BindContractListToLink(Convert.ToInt32(ViewState["ContractInstanceID"]));
            }

            
        }

        protected void TabAuditLog_Click(object sender, EventArgs e)
        {
            ViewState["PageLink"] = "ContractLink";
            showHideContractSummaryTabTopButtons(false);

            liContractSummary.Attributes.Add("class", "");
            liContractDetails.Attributes.Add("class", "");
            liDocument.Attributes.Add("class", "");
            liTask.Attributes.Add("class", "");
            liAuditLog.Attributes.Add("class", "active");
            liContractTransaction.Attributes.Add("class", "");
            ViewState["PageNumberAuditLogFlagID"] = 0;

            MainView.ActiveViewIndex = 4;

            if (ViewState["ContractInstanceID"] != null)
            {
                BindContractAuditLogs();
            }
            BindContractListToLink(Convert.ToInt32(ViewState["ContractInstanceID"]));
        }

        protected void tvBranches_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxBranch.Text = tvBranches.SelectedNode != null ? tvBranches.SelectedNode.Text : "Select Entity/Location";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "hideDivTree", "hideDivBranch();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvContractPopUp.IsValid = false;
                cvContractPopUp.ErrorMessage = "Server Error Occurred. Please try again.";
                cvContractPopUp.CssClass = "alert alert-danger";
            }
        }

        protected void ddlContractType_SelectedIndexChanged(object sender, EventArgs e)
        {
            bool bindSuccess = false;

            if (ddlContractType.SelectedValue != null && ddlContractType.SelectedValue != "0" && ddlContractType.SelectedValue != "")
            {
                ViewState["ddlCustomFieldFilled"] = null;
                ViewState["dataTableCustomFields"] = null;

                ViewState["CustomefieldCount"] = null;

                ViewState["ContractTypeIDUpdated"] = "false";

                bindSuccess = BindContractSubType(Convert.ToInt64(ddlContractType.SelectedValue));

                lnkAddNewContractSubTypeModal.Visible = true;

                Session["ContractTypeID"] = ddlContractType.SelectedValue;

                BindCustomFields(grdCustomField); //, grdCustomField_History
            }

            if (!bindSuccess)
            {
                if (ddlContractSubType.Items.Count > 0)
                    ddlContractSubType.Items.Clear();

                if (ddlContractType.SelectedValue != null && ddlContractType.SelectedValue != "0" && ddlContractType.SelectedValue != "")
                {
                    lnkAddNewContractSubTypeModal.Visible = true;
                    //ddlContractSubType.Items.Add(new ListItem("Add New", "0"));
                }
                else
                    lnkAddNewContractSubTypeModal.Visible = false;
            }

            upContractTypeSubType.Update();
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "changeContractType", "ddlContractTypeChange(); ddlCustomFieldChange();", true);
        }

        public void showErrorMessages(List<string> lstErrMsgs, CustomValidator cvtoShowErrorMsg)
        {
            //string finalErrMsg = string.Join("<br/>", emsg.ToArray());

            string finalErrMsg = string.Empty;

            finalErrMsg += "<ol type='1'>";

            if (lstErrMsgs.Count > 0)
            {
                lstErrMsgs.ForEach(eachErrMsg =>
                {
                    finalErrMsg += "<li>" + eachErrMsg + "</li>";
                });

                finalErrMsg += "</ol>";
            }

            cvtoShowErrorMsg.IsValid = false;
            cvtoShowErrorMsg.ErrorMessage = finalErrMsg;
        }

        #endregion

        #region Contract Detail

        protected void btnClearContractControls_Click(object sender, EventArgs e)
        {
            try
            {
                clearContractControls();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void clearContractControls()
        {
            try
            {
                txtContractNo.Text = "";
                txtTitle.Text = "";
                tbxDescription.Text = "";

                tbxBranch.Text = "";
                tbxBranch.Text = "Select Entity/Location";
                lstBoxVendor.ClearSelection();
                ddlDepartment.ClearSelection();
                ddlCPDepartment.ClearSelection();

                txtProposalDate.Text = "";
                txtAgreementDate.Text = "";
                txtEffectiveDate.Text = "";

                txtReviewDate.Text = "";
                txtExpirationDate.Text = "";
                txtNoticeTerm.Text = "";

                ddlContractType.ClearSelection();
                ddlContractSubType.ClearSelection();
                lstBoxOwner.ClearSelection();
                lstBoxApprover.ClearSelection();

                tbxContractAmt.Text = "";
                ddlPaymentTerm1.ClearSelection();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        
        public void enableDisableContractSummaryTabControls(bool flag)
        {
            try
            {
                ddlContractStatus.Enabled = flag;

                txtContractNo.Enabled = flag;
                txtTitle.Enabled = flag;
                tbxDescription.Enabled = flag;

                tbxBranch.Enabled = flag;
                lstBoxVendor.Enabled = flag;
                ddlDepartment.Enabled = flag;


                ddlCPDepartment.Enabled = flag;

                tbxAddNewClause.Enabled = flag;
                txtProposalDate.Enabled = flag;
                txtAgreementDate.Enabled = flag;
                txtEffectiveDate.Enabled = flag;

                txtReviewDate.Enabled = flag;
                txtExpirationDate.Enabled = flag;

                txtNoticeTerm.Enabled = flag;
                ddlNoticeTerm.Enabled = flag;

                ddlPaymentType.Enabled = flag;

                tbxContractAmt.Enabled = flag;
                ddlPaymentTerm1.Enabled = flag;

                lstBoxVendor.Enabled = flag;
                ddlDepartment.Enabled = flag;

                ddlContractType.Enabled = flag;
                ddlContractSubType.Enabled = flag;
                lstBoxOwner.Enabled = flag;
                lstBoxApprover.Enabled = flag;
                txtBoxProduct.Enabled = flag;
                txtlockinperiodDate.Enabled = flag;
                tbxTaxes.Enabled = flag;
                //lnkShowAddNewVendorModal.Visible = flag;
                //lnkAddNewDepartmentModal.Visible = flag;
                //lnkAddNewContractTypeModal.Visible = flag;
                //lnkAddNewContractSubTypeModal.Visible = flag;

                if (flag)
                {
                    ddlDepartment.Attributes.Remove("disabled");
                    ddlCPDepartment.Attributes.Remove("disabled");
                    ddlNoticeTerm.Attributes.Remove("disabled");
                    ddlContractType.Attributes.Remove("disabled");
                    ddlContractSubType.Attributes.Remove("disabled");

                    lstBoxVendor.Attributes.Remove("disabled");
                    lstBoxOwner.Attributes.Remove("disabled");
                    lstBoxApprover.Attributes.Remove("disabled");
                    ddlPaymentTerm1.Attributes.Remove("disabled");
                    btnSaveContract.Attributes.Remove("disabled");
                    btnClearContractDetail.Attributes.Remove("disabled");

                    lnkShowAddNewVendorModal.Enabled = true;
                    lnkAddNewPDepartmentModal.Enabled = true;
                    lnkAddNewDepartmentModal.Enabled = true;
                    lnkAddNewContractTypeModal.Enabled = true;
                    lnkAddNewContractSubTypeModal.Enabled = true;
                }
                else
                {
                    ddlDepartment.Attributes.Add("disabled", "disabled");
                    ddlCPDepartment.Attributes.Add("disabled", "disabled");
                    ddlNoticeTerm.Attributes.Add("disabled", "disabled");
                    ddlContractType.Attributes.Add("disabled", "disabled");
                    ddlContractSubType.Attributes.Add("disabled", "disabled");
                    ddlPaymentTerm1.Attributes.Add("disabled", "disabled");
                    lstBoxOwner.Attributes.Add("disabled", "disabled");
                    lstBoxVendor.Attributes.Add("disabled", "disabled");
                    lstBoxApprover.Attributes.Add("disabled", "disabled");

                    btnSaveContract.Attributes.Add("disabled", "disabled");
                    btnClearContractDetail.Attributes.Add("disabled", "disabled");

                    lnkShowAddNewVendorModal.Enabled = false;
                    lnkAddNewDepartmentModal.Enabled = false;
                    lnkAddNewPDepartmentModal.Enabled = false;
                    lnkAddNewContractTypeModal.Enabled = false;
                    lnkAddNewContractSubTypeModal.Enabled = false;
                }

                if (grdCustomField != null)
                {
                    grdCustomField.Enabled = flag;
                    HideShowGridColumns(grdCustomField, "Action", flag);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void showHideContractSummaryTabTopButtons(bool flag)
        {
            try
            {
                topButtons.Visible = flag;
                //divContractHistory.Visible = flag;
                btnEditContractDetail.Visible = flag;

                lnkSendMailWithDoc.Visible = flag;

                lnkLinkContract.Visible = flag;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void showHideTabs(bool flag)
        {
            try
            {
                liContractDetails.Visible = flag;
                liDocument.Visible = flag;
                liTask.Visible = flag;
                liAuditLog.Visible = flag;
                liContractTransaction.Visible = flag;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        


        public void toggleTextSaveButton_ContractSummaryTab(bool flag)
        {
            try
            {
                if (flag) //Add Mode     
                {
                    btnSaveContract.Text = "Save";
                    btnClearContractDetail.Visible = flag;
                }
                else //Edit Mode
                {
                    btnSaveContract.Text = "Update";
                    btnClearContractDetail.Visible = flag;
                }

                btnSaveContract.Visible = true;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        
        public void enableDisableContractPopUpControls(bool flag)
        {
            try
            {
                btnEditContractDetail.Enabled = flag;
                lnkSendMailWithDoc.Enabled = flag;
                lnkLinkContract.Enabled = flag;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnEditContractControls_Click(object sender, EventArgs e)
        {
            try
            {
                enableDisableContractSummaryTabControls(true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnAddContract_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["Mode"] = 0;
                clearContractControls();

                showHideContractSummaryTabTopButtons(false);
                enableDisableContractPopUpControls(true);

                lblCustomField.Visible = false;
                divLinkedContracts.Visible = false;

                TabContract_Click(sender, e);

                showHideTabs(false);

                ddlContractType_SelectedIndexChanged(sender, e);

                ddlContractStatus.Enabled = false;

                if (ddlContractStatus.Items.FindByValue("1") != null) //1-Draft
                    ddlContractStatus.Items.FindByValue("1").Selected = true;

                AddSteps(items, statusBulletedList);
                SetProgress(0, statusBulletedList);

                ScriptManager.RegisterStartupScript(this, this.GetType(), "hideDivTree", "hideDivBranch();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnEditContract_Click(object sender, EventArgs e)
        {
            try
            {

                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                ViewState["Mode"] = 1;
                showHideContractSummaryTabTopButtons(true);
                clearContractControls();
                TabContract_Click(sender, e);
                long contractInstanceID = 0;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["ContractInstanceID"])))
                {
                    contractInstanceID = Convert.ToInt32(ViewState["ContractInstanceID"]);

                    if (contractInstanceID != 0)
                    {
                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        {
                            var getrecord = (from row in entities.Cont_tbl_ContractInstance
                                             where row.ID == contractInstanceID
                                             select row).FirstOrDefault();
                            if (getrecord != null)
                            {
                                if (getrecord.TemplateID != null)
                                {
                                    drdTemplateID.SelectedValue = getrecord.TemplateID.ToString();
                                }
                            }
                        }
                        ViewState["Mode"] = 1;

                        var contractRecord = ContractManagement.GetContractDetailsByContractID(customerID, contractInstanceID);
                        //var contractRecord = ContractManagement.GetContractByID(contractInstanceID);

                        if (contractRecord != null)
                        {                            
                            //SET Current Contract Status

                            if (contractRecord.ContractStatusID != 0)
                                ViewState["ContractStatusID"] = contractRecord.ContractStatusID;

                            ddlContractStatus.ClearSelection();

                            if (ddlContractStatus.Items.FindByValue(contractRecord.ContractStatusID.ToString()) != null)
                                ddlContractStatus.Items.FindByValue(contractRecord.ContractStatusID.ToString()).Selected = true;

                            txtContractNo.Text = contractRecord.ContractNo;
                            txtTitle.Text = contractRecord.ContractTitle;
                            tbxDescription.Text = contractRecord.ContractDetailDesc;
                            tbxAddNewClause.Text = contractRecord.AddNewClause;

                            if (contractRecord.CustomerBranchID != 0)
                            {
                                foreach (TreeNode node in tvBranches.Nodes)
                                {
                                    if (node.Value == contractRecord.CustomerBranchID.ToString())
                                    {
                                        node.Selected = true;
                                    }
                                    foreach (TreeNode item1 in node.ChildNodes)
                                    {
                                        if (item1.Value == contractRecord.CustomerBranchID.ToString())
                                            item1.Selected = true;
                                    }
                                }
                            }

                            tvBranches_SelectedNodeChanged(null, null);

                            #region Vendor

                            var lstVendorMapping = VendorDetails.GetVendorMapping(contractInstanceID);

                            if (lstBoxVendor.Items.Count > 0)
                            {
                                lstBoxVendor.ClearSelection();
                            }

                            if (lstVendorMapping.Count > 0)
                            {
                                foreach (var VendorID in lstVendorMapping)
                                {
                                    if (lstBoxVendor.Items.FindByValue(VendorID.ToString()) != null)
                                        lstBoxVendor.Items.FindByValue(VendorID.ToString()).Selected = true;
                                }
                            }

                            #endregion

                            ddlDepartment.ClearSelection();

                            if (ddlDepartment.Items.FindByValue(contractRecord.DepartmentID.ToString()) != null)
                                ddlDepartment.SelectedValue = contractRecord.DepartmentID.ToString();

                            if (contractRecord.ProposalDate != null)
                                txtProposalDate.Text = Convert.ToDateTime(contractRecord.ProposalDate).ToString("dd-MM-yyyy");
                            else
                                txtProposalDate.Text = string.Empty;

                            if (contractRecord.AgreementDate != null)
                                txtAgreementDate.Text = Convert.ToDateTime(contractRecord.AgreementDate).ToString("dd-MM-yyyy");
                            else
                                txtAgreementDate.Text = string.Empty;

                            if (contractRecord.EffectiveDate != null)
                                txtEffectiveDate.Text = Convert.ToDateTime(contractRecord.EffectiveDate).ToString("dd-MM-yyyy");
                            else
                                txtEffectiveDate.Text = string.Empty;

                            if (contractRecord.ReviewDate != null)
                                txtReviewDate.Text = Convert.ToDateTime(contractRecord.ReviewDate).ToString("dd-MM-yyyy");
                            else
                                txtReviewDate.Text = string.Empty;

                            if (contractRecord.ExpirationDate != null)
                                txtExpirationDate.Text = Convert.ToDateTime(contractRecord.ExpirationDate).ToString("dd-MM-yyyy");
                            else
                                txtExpirationDate.Text = string.Empty;

                            if (contractRecord.ExpirationDate != null)
                                txtNoticeTerm.Text = Convert.ToString(contractRecord.NoticeTermNumber);
                            else
                                txtNoticeTerm.Text = string.Empty;

                            if (ddlNoticeTerm.Items.FindByValue(contractRecord.NoticeTermType.ToString()) != null)
                                ddlNoticeTerm.SelectedValue = contractRecord.NoticeTermType.ToString();

                            ddlContractType.ClearSelection();

                            if (ddlContractType.Items.FindByValue(contractRecord.ContractTypeID.ToString()) != null)
                                ddlContractType.SelectedValue = contractRecord.ContractTypeID.ToString();

                            ddlContractType_SelectedIndexChanged(sender, e);

                            if (contractRecord.ContractSubTypeID != null)
                            {
                                ddlContractSubType.ClearSelection();

                                if (ddlContractSubType.Items.FindByValue(contractRecord.ContractSubTypeID.ToString()) != null)
                                    ddlContractSubType.SelectedValue = contractRecord.ContractSubTypeID.ToString();
                            }

                            #region Owner && Approvers                            

                            var lstContractUserAssignment = ContractManagement.GetContractUserAssignment_All(contractInstanceID);

                            if (lstContractUserAssignment.Count > 0)
                            {
                                lstBoxOwner.ClearSelection();
                                lstBoxApprover.ClearSelection();

                                foreach (var eachAssignmentRecord in lstContractUserAssignment)
                                {
                                    if (eachAssignmentRecord.RoleID == 3)
                                    {
                                        if (lstBoxOwner.Items.FindByValue(eachAssignmentRecord.UserID.ToString()) != null)
                                            lstBoxOwner.Items.FindByValue(eachAssignmentRecord.UserID.ToString()).Selected = true;
                                    }
                                    else if (eachAssignmentRecord.RoleID == 6)
                                    {
                                        if (lstBoxApprover.Items.FindByValue(eachAssignmentRecord.UserID.ToString()) != null)
                                            lstBoxApprover.Items.FindByValue(eachAssignmentRecord.UserID.ToString()).Selected = true;
                                    }
                                }
                            }

                            //var lstAssignedUserIDs = ContractManagement.GetAssignedUsersByRoleID(contractInstanceID, 3); //3--Owner

                            //if (lstBoxOwner.Items.Count > 0)
                            //{
                            //    lstBoxOwner.ClearSelection();
                            //}

                            //if (lstAssignedUserIDs.Count > 0)
                            //{
                            //    foreach (var userID in lstAssignedUserIDs)
                            //    {
                            //        if (lstBoxOwner.Items.FindByValue(userID.ToString()) != null)
                            //            lstBoxOwner.Items.FindByValue(userID.ToString()).Selected = true;
                            //    }
                            //}                          

                            #endregion

                            if (contractRecord.ContractAmt != null)
                                tbxContractAmt.Text = contractRecord.ContractAmt.ToString();
                            else
                                tbxContractAmt.Text = string.Empty;

                            #region Payment Term

                            var lstpaymenttermMapping = ContractManagement.GetPaymentTermMapping(contractInstanceID);

                            if (ddlPaymentTerm1.Items.Count > 0)
                            {
                                ddlPaymentTerm1.ClearSelection();
                            }

                            if (lstpaymenttermMapping.Count > 0)
                            {
                                foreach (var paymenttermID in lstpaymenttermMapping)
                                {
                                    if (ddlPaymentTerm1.Items.FindByValue(paymenttermID.ToString()) != null)
                                        ddlPaymentTerm1.Items.FindByValue(paymenttermID.ToString()).Selected = true;
                                }
                            }

                            #endregion

                            //if (contractRecord.PaymentTermID != null)
                            //{
                            //    ddlPaymentTerm.ClearSelection();
                            //    if (ddlPaymentTerm.Items.FindByValue(contractRecord.PaymentTermID.ToString()) != null)
                            //        ddlPaymentTerm.SelectedValue = contractRecord.PaymentTermID.ToString();
                            //}


                            if (contractRecord.PaymentType != null)
                            {
                                ddlPaymentType.ClearSelection();

                                if (ddlPaymentType.Items.FindByValue(contractRecord.PaymentType.ToString()) != null)
                                    ddlPaymentType.SelectedValue = contractRecord.PaymentType.ToString();
                            }

                            if (contractRecord.ContactPersonOfDepartment != null)
                            {
                                ddlCPDepartment.ClearSelection();

                                if (ddlCPDepartment.Items.FindByValue(contractRecord.ContactPersonOfDepartment.ToString()) != null)
                                    ddlCPDepartment.SelectedValue = contractRecord.ContactPersonOfDepartment.ToString();
                            }

                            if (contractRecord.ProductItems != null)
                                txtBoxProduct.Text = contractRecord.ProductItems.ToString();
                            else
                                txtBoxProduct.Text = string.Empty;

                            if (contractRecord.LockInPeriodDate != null)
                                txtlockinperiodDate.Text = Convert.ToDateTime(contractRecord.LockInPeriodDate).ToString("dd-MM-yyyy");
                            else
                                txtlockinperiodDate.Text = string.Empty;

                            if (contractRecord.Taxes != null)
                                tbxTaxes.Text = contractRecord.Taxes;
                            else
                                tbxTaxes.Text = string.Empty;

                            if (contractRecord.AddNewClause != null)
                                tbxAddNewClause.Text = contractRecord.AddNewClause.ToString();
                            else
                                tbxAddNewClause.Text = string.Empty;

                            BindContractHistory(customerID, Convert.ToInt64(contractInstanceID));

                            BindLinkedContracts(Convert.ToInt64(contractInstanceID));
                            //Bind Case To Link
                            BindContractListToLink(Convert.ToInt64(contractInstanceID));
                            
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnSaveContract_Click(object sender, EventArgs e)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                if (ViewState["Mode"] != null)
                {
                    bool formValidateSuccess = false;
                    bool saveSuccess = false;

                    List<Tuple<int, int>> lstContractUserMapping = new List<Tuple<int, int>>();
                    List<long> lstVendorMapping = new List<long>();

                    //List<int> lstOwnerMapping = new List<int>();
                    //List<int> lstApproverMapping = new List<int>();

                    #region Data Validation

                    List<string> lstErrorMsg = new List<string>();

                    //Contract Number
                    if (String.IsNullOrEmpty(txtContractNo.Text))
                        lstErrorMsg.Add("Required Contract Number");
                    else
                        formValidateSuccess = true;

                    //Contract Title
                    if (String.IsNullOrEmpty(txtTitle.Text))
                        lstErrorMsg.Add("Required Contract Title");
                    else
                        formValidateSuccess = true;

                    //Contract Description
                    if (String.IsNullOrEmpty(tbxDescription.Text))
                        lstErrorMsg.Add("Required Contract Description");
                    else
                        formValidateSuccess = true;

                    //Branch Location
                    if (tvBranches.SelectedValue != "" && tvBranches.SelectedValue != "-1")
                        formValidateSuccess = true;
                    else
                        lstErrorMsg.Add("Select Entity/Branch/Location");

                    //Vendor/Contractor
                    int selectedVendorCount = 0;
                    foreach (ListItem eachVendor in lstBoxVendor.Items)
                    {
                        if (eachVendor.Selected)
                            selectedVendorCount++;
                    }

                    if (selectedVendorCount > 0)
                        formValidateSuccess = true;
                    else
                        lstErrorMsg.Add("Select at least One Vendor");

                    //Department
                    if (ddlDepartment.SelectedValue != "" && ddlDepartment.SelectedValue != "-1")
                        formValidateSuccess = true;
                    else
                        lstErrorMsg.Add("Select Department");
                    //Contact Person Of Department
                    if (ddlCPDepartment.SelectedValue != "" && ddlCPDepartment.SelectedValue != "-1")
                        formValidateSuccess = true;
                    else
                        lstErrorMsg.Add("Select Contact Person Of Department");

                    //Proposal Date
                    if (!string.IsNullOrEmpty(txtProposalDate.Text))
                    {
                        try
                        {
                            bool check = ContractCommonMethods.CheckValidDate(txtProposalDate.Text);
                            if (!check)
                            {
                                lstErrorMsg.Add("Please Check Proposal Date or Date should be in DD-MM-YYYY Format");
                            }
                        }
                        catch (Exception)
                        {
                            lstErrorMsg.Add("Please Check Proposal Date or Date should be in DD-MM-YYYY Format");
                        }
                    }

                    //Agreement Date
                    if (!string.IsNullOrEmpty(txtAgreementDate.Text))
                    {
                        try
                        {
                            bool check = ContractCommonMethods.CheckValidDate(txtAgreementDate.Text);
                            if (!check)
                            {
                                lstErrorMsg.Add("Please Check Agreement Date or Date should be in DD-MM-YYYY Format");
                            }
                        }
                        catch (Exception)
                        {
                            lstErrorMsg.Add("Please Check Agreement Date or Date should be in DD-MM-YYYY Format");
                        }
                    }

                    if (ddlContractStatus.SelectedItem.Text != "Draft")
                    {
                        //Effective Date
                        if (!string.IsNullOrEmpty(txtEffectiveDate.Text))
                        {
                            try
                            {
                                bool check = ContractCommonMethods.CheckValidDate(txtEffectiveDate.Text);
                                if (!check)
                                {
                                    lstErrorMsg.Add("Please Check Start Date or Date should be in DD-MM-YYYY Format");
                                }
                            }
                            catch (Exception)
                            {
                                lstErrorMsg.Add("Please Check Start Date or Date should be in DD-MM-YYYY Format");
                            }
                        }
                        else
                            lstErrorMsg.Add("Required Start Date");

                        //Expiration Date
                        if (!string.IsNullOrEmpty(txtExpirationDate.Text))
                        {
                            try
                            {
                                bool check = ContractCommonMethods.CheckValidDate(txtExpirationDate.Text);
                                if (!check)
                                {
                                    lstErrorMsg.Add("Please Check Expiration Date or Date should be in DD-MM-YYYY Format");
                                }
                            }
                            catch (Exception)
                            {
                                lstErrorMsg.Add("Please Check Expiration Date or Date should be in DD-MM-YYYY Format");
                            }
                        }
                        else
                            lstErrorMsg.Add("Required End Date");
                    }

                    //Review Date
                    if (!string.IsNullOrEmpty(txtReviewDate.Text))
                    {
                        try
                        {
                            bool check = ContractCommonMethods.CheckValidDate(txtReviewDate.Text);
                            if (!check)
                            {
                                lstErrorMsg.Add("Please Check Review Date or Date should be in DD-MM-YYYY Format");
                            }
                        }
                        catch (Exception)
                        {
                            lstErrorMsg.Add("Please Check Review Date or Date should be in DD-MM-YYYY Format");
                        }
                    }


                    //Contract Type
                    if (ddlContractType.SelectedValue != "" && ddlContractType.SelectedValue != "-1")
                        formValidateSuccess = true;
                    else
                        lstErrorMsg.Add("Select Contract Type");

                    //Owner
                    int selectedOwnerCount = 0;
                    foreach (ListItem eachOwner in lstBoxOwner.Items)
                    {
                        if (eachOwner.Selected)
                            selectedOwnerCount++;
                    }

                    if (selectedOwnerCount > 0)
                        formValidateSuccess = true;
                    else
                        lstErrorMsg.Add("Select at least One Contract Owner");

                    //Approver
                    //int selectedApproverCount = 0;
                    //foreach (ListItem eachApprover in lstBoxApprover.Items)
                    //{
                    //    if (eachApprover.Selected)
                    //        selectedApproverCount++;
                    //}

                    //if (selectedApproverCount > 0)
                    //    formValidateSuccess = true;
                    //else
                    //    lstErrorMsg.Add("Select at least One Contract Approver");

                    //Contract Amt
                    if (!String.IsNullOrEmpty(tbxContractAmt.Text))
                    {
                        try
                        {
                            double n;
                            var isNumeric = double.TryParse(tbxContractAmt.Text, out n);
                            if (!isNumeric)
                            {
                                lstErrorMsg.Add("Enter Valid Contract Amount, Only numbers are allowed");
                            }
                        }
                        catch (Exception)
                        {
                            lstErrorMsg.Add("Enter Valid Contract Amount, Only numbers are allowed");
                        }
                    }


                    if (lstErrorMsg.Count > 0)
                    {
                        formValidateSuccess = false;
                        showErrorMessages(lstErrorMsg, cvContractPopUp);
                    }

                    if (!cvContractPopUp.IsValid)
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "scrollUpPageScript", "scrollUpPage();", true);

                    #endregion

                    #region Save (Add/Edit)

                    if (formValidateSuccess)
                    {
                        long newContractID = 0;

                        Cont_tbl_ContractInstance contractRecord = new Cont_tbl_ContractInstance()
                        {
                            ContractType = 1,

                            CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID),
                            IsDeleted = false,

                            ContractNo = txtContractNo.Text,
                            ContractTitle = txtTitle.Text,
                            ContractDetailDesc = tbxDescription.Text,
                            AddNewClause = tbxAddNewClause.Text,
                            CustomerBranchID = Convert.ToInt32(tvBranches.SelectedValue),
                            DepartmentID = Convert.ToInt32(ddlDepartment.SelectedValue),
                            ContractTypeID = Convert.ToInt64(ddlContractType.SelectedValue),
                            CreatedBy = AuthenticationHelper.UserID,
                            UpdatedBy = AuthenticationHelper.UserID,
                            ContactPersonOfDepartment = Convert.ToInt32(ddlCPDepartment.SelectedValue)
                        };

                        if (!string.IsNullOrEmpty(txtProposalDate.Text))
                            contractRecord.ProposalDate = DateTimeExtensions.GetDate(txtProposalDate.Text);

                        if (!string.IsNullOrEmpty(txtAgreementDate.Text))
                            contractRecord.AgreementDate = DateTimeExtensions.GetDate(txtAgreementDate.Text);

                        if (!string.IsNullOrEmpty(txtReviewDate.Text))
                            contractRecord.ReviewDate = DateTimeExtensions.GetDate(txtReviewDate.Text);

                        if (!string.IsNullOrEmpty(txtEffectiveDate.Text))
                            contractRecord.EffectiveDate = DateTimeExtensions.GetDate(txtEffectiveDate.Text);

                        if (!string.IsNullOrEmpty(txtExpirationDate.Text))
                            contractRecord.ExpirationDate = DateTimeExtensions.GetDate(txtExpirationDate.Text);

                        if (ddlContractSubType.SelectedValue != "" && ddlContractSubType.SelectedValue != "-1")
                            contractRecord.ContractSubTypeID = Convert.ToInt64(ddlContractSubType.SelectedValue);

                        if (ddlPaymentType.SelectedValue != "" && ddlPaymentType.SelectedValue != "-1")
                            contractRecord.PaymentType = Convert.ToInt32(ddlPaymentType.SelectedValue);

                        if (ddlCPDepartment.SelectedValue != "" && ddlCPDepartment.SelectedValue != "-1")
                            contractRecord.ContactPersonOfDepartment = Convert.ToInt32(ddlCPDepartment.SelectedValue);

                        if (!string.IsNullOrEmpty(txtNoticeTerm.Text))
                        {
                            contractRecord.NoticeTermNumber = Convert.ToInt32(txtNoticeTerm.Text.Trim());

                            if (ddlNoticeTerm.SelectedValue != "" && ddlNoticeTerm.SelectedValue != "-1")
                                contractRecord.NoticeTermType = Convert.ToInt32(ddlNoticeTerm.SelectedValue);
                        }

                        if (!string.IsNullOrEmpty(tbxContractAmt.Text))
                            contractRecord.ContractAmt = Convert.ToDecimal(tbxContractAmt.Text);
                        else
                            contractRecord.ContractAmt = 0;

                        //if (ddlPaymentTerm.SelectedValue != "" && ddlPaymentTerm.SelectedValue != "-1")
                        //    contractRecord.PaymentTermID = Convert.ToInt32(ddlPaymentTerm.SelectedValue);

                        if (!string.IsNullOrEmpty(txtBoxProduct.Text))
                            contractRecord.ProductItems = Convert.ToString(txtBoxProduct.Text.Trim());

                        if (!string.IsNullOrEmpty(tbxAddNewClause.Text))
                            contractRecord.AddNewClause = Convert.ToString(tbxAddNewClause.Text.Trim());

                        #region ADD New Contract

                        if ((int)ViewState["Mode"] == 0)
                        {
                            bool existCaseNo = ContractManagement.ExistsContractNo(customerID, contractRecord.ContractNo, 0);
                            if (!existCaseNo)
                            {
                                //if (!ContractManagement.ExistsContractTitle(customerID, contractRecord.ContractTitle, 0))
                                if (1 == 1)
                                {
                                    newContractID = ContractManagement.CreateContract(contractRecord);

                                    if (newContractID > 0)
                                    {
                                        ContractManagement.CreateAuditLog("C", newContractID, "Cont_tbl_ContractInstance", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "New Contract Created", true, Convert.ToInt32(newContractID));
                                        saveSuccess = true;
                                    }
                                }
                                else
                                {
                                    cvContractPopUp.IsValid = false;
                                    cvContractPopUp.ErrorMessage = "Contract with Same Title already Exists.";
                                    cvContractPopUp.CssClass = "alert alert-danger";
                                    return;
                                }
                            }
                            else
                            {
                                cvContractPopUp.IsValid = false;
                                cvContractPopUp.ErrorMessage = "Contract with Same Contract Number already exists";
                                cvContractPopUp.CssClass = "alert alert-danger";
                            }

                            if (saveSuccess)
                            {
                                #region Contract Status Transaction - Draft

                                Cont_tbl_ContractStatusTransaction newStatusRecord = new Cont_tbl_ContractStatusTransaction()
                                {
                                    CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID),
                                    ContractID = newContractID,
                                    StatusID = Convert.ToInt64(ddlContractStatus.SelectedValue),
                                    StatusChangeOn = DateTime.Now,
                                    IsActive = true,
                                    CreatedBy = AuthenticationHelper.UserID,
                                    CreatedOn = DateTime.Now,
                                    UpdatedBy = AuthenticationHelper.UserID,
                                    UpdatedOn = DateTime.Now,
                                };

                                saveSuccess = ContractManagement.CreateContractStatusTransaction(newStatusRecord);

                                #endregion

                                #region Vendor Mapping

                                if (lstBoxVendor.Items.Count > 0)
                                {
                                    foreach (ListItem eachVendor in lstBoxVendor.Items)
                                    {
                                        if (eachVendor.Selected)
                                        {
                                            if (Convert.ToInt64(eachVendor.Value) != 0)
                                                lstVendorMapping.Add(Convert.ToInt64(eachVendor.Value));
                                        }
                                    }
                                }

                                if (lstVendorMapping.Count > 0)
                                {
                                    List<Cont_tbl_VendorMapping> lstVendorMapping_ToSave = new List<Cont_tbl_VendorMapping>();

                                    lstVendorMapping.ForEach(EachVendor =>
                                    {
                                        Cont_tbl_VendorMapping _vendorMappingRecord = new Cont_tbl_VendorMapping()
                                        {
                                            ContractID = newContractID,
                                            VendorID = EachVendor,
                                            IsActive = true,
                                            CreatedBy = AuthenticationHelper.UserID,
                                            CreatedOn = DateTime.Now,
                                        };

                                        lstVendorMapping_ToSave.Add(_vendorMappingRecord);
                                    });

                                    saveSuccess = VendorDetails.CreateUpdate_VendorMapping(lstVendorMapping_ToSave);
                                    if (saveSuccess)
                                    {
                                        ContractManagement.CreateAuditLog("C", newContractID, "Cont_tbl_VendorMapping", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Contract Vendor Mapping Created", true, Convert.ToInt32(newContractID));
                                    }

                                    //Refresh List
                                    lstVendorMapping_ToSave.Clear();
                                    lstVendorMapping_ToSave = null;

                                    lstVendorMapping.Clear();
                                    lstVendorMapping = null;
                                }
                                #endregion

                                #region Save Custom Field

                                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["CustomefieldCount"])))
                                {
                                    List<Cont_tbl_CustomFieldValue> lstObjParameters = new List<Cont_tbl_CustomFieldValue>();

                                    //Select Which Grid to Loop based on Selected Category/Type
                                    GridView gridViewToCollectData = null;
                                    gridViewToCollectData = grdCustomField;

                                    if (gridViewToCollectData != null)
                                    {
                                        for (int i = 0; i < gridViewToCollectData.Rows.Count; i++)
                                        {
                                            Label lblID = (Label)gridViewToCollectData.Rows[i].FindControl("lblID");
                                            TextBox tbxLabelValue = (TextBox)gridViewToCollectData.Rows[i].FindControl("tbxLabelValue");

                                            if (lblID != null && tbxLabelValue != null)
                                            {
                                                if (!string.IsNullOrEmpty(lblID.Text) || lblID.Text != "0")
                                                {
                                                    Cont_tbl_CustomFieldValue ObjParameter = new Cont_tbl_CustomFieldValue()
                                                    {
                                                        ContractID = newContractID,

                                                        LabelID = Convert.ToInt64(lblID.Text),
                                                        LabelValue = tbxLabelValue.Text,

                                                        IsDeleted = false,
                                                        CreatedBy = AuthenticationHelper.UserID,
                                                        CreatedOn = DateTime.Now,
                                                        UpdatedBy = AuthenticationHelper.UserID,
                                                        UpdatedOn = DateTime.Now
                                                    };
                                                    lstObjParameters.Add(ObjParameter);
                                                    //saveSuccess = ContractManagement.CreateUpdateCustomsField(ObjParameter);
                                                }
                                            }
                                        }//End For Each

                                        saveSuccess = ContractManagement.CreateUpdate_CustomsFields(lstObjParameters);

                                        if (saveSuccess)
                                        {
                                            ContractManagement.CreateAuditLog("C", newContractID, "Cont_tbl_CustomFieldValue", "Update", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Contract Custom Parameter(s) Updated", true, Convert.ToInt32(newContractID));
                                        }
                                    }
                                }

                                #endregion

                                #region User Assignment                           

                                //De-Active All Previous Assignment
                                ContractManagement.DeActiveContractUserAssignments(newContractID, AuthenticationHelper.UserID);
                                List<Cont_tbl_UserAssignment> lstUserAssignmentRecord_ToSave = new List<Cont_tbl_UserAssignment>();

                                int assignedRoleID = 3;
                                if (lstBoxOwner.Items.Count > 0)
                                {
                                    assignedRoleID = 3;
                                    foreach (ListItem eachOwner in lstBoxOwner.Items)
                                    {
                                        if (eachOwner.Selected)
                                        {
                                            if (Convert.ToInt32(eachOwner.Value) != 0)
                                                lstContractUserMapping.Add(new Tuple<int, int>(Convert.ToInt32(eachOwner.Value), assignedRoleID));
                                            //lstOwnerMapping.Add(Convert.ToInt32(eachOwner.Value));
                                        }
                                    }
                                }

                                if (lstBoxApprover.Items.Count > 0)
                                {
                                    assignedRoleID = 6;
                                    foreach (ListItem eachApprover in lstBoxApprover.Items)
                                    {
                                        if (eachApprover.Selected)
                                        {
                                            if (Convert.ToInt32(eachApprover.Value) != 0)
                                                lstContractUserMapping.Add(new Tuple<int, int>(Convert.ToInt32(eachApprover.Value), assignedRoleID));
                                            // lstApproverMapping.Add(Convert.ToInt32(eachApprover.Value));
                                        }
                                    }
                                }

                                if (lstContractUserMapping.Count > 0)
                                {
                                    lstContractUserMapping.ForEach(eachUser =>
                                    {
                                        Cont_tbl_UserAssignment newAssignment = new Cont_tbl_UserAssignment()
                                        {
                                            AssignmentType = 1,
                                            ContractID = newContractID,

                                            UserID = Convert.ToInt32(eachUser.Item1),
                                            RoleID = Convert.ToInt32(eachUser.Item2),

                                            IsActive = true,
                                            CreatedBy = AuthenticationHelper.UserID,
                                            UpdatedBy = AuthenticationHelper.UserID,
                                        };

                                        lstUserAssignmentRecord_ToSave.Add(newAssignment);

                                        //saveSuccess = ContractManagement.CreateUpdateContractUserAssignment(newAssignment);
                                    });
                                }

                                if (lstUserAssignmentRecord_ToSave.Count > 0)
                                    saveSuccess = ContractManagement.CreateUpdate_ContractUserAssignments(lstUserAssignmentRecord_ToSave);

                                if (saveSuccess)
                                {
                                    ContractManagement.CreateAuditLog("C", newContractID, "Cont_tbl_UserAssignment", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Contract Owner(s) Created", true, Convert.ToInt32(newContractID));
                                }

                                lstUserAssignmentRecord_ToSave.Clear();
                                lstUserAssignmentRecord_ToSave = null;



                                #endregion
                            }

                            if (saveSuccess)
                            {
                                if (drdTemplateID.SelectedValue != null && drdTemplateID.SelectedValue != "")
                                {
                                    if (Convert.ToInt32(drdTemplateID.SelectedValue) > 0)
                                    {
                                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                        {
                                            int CID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                                            long TID = Convert.ToInt64(drdTemplateID.SelectedValue);

                                            var gettemplatesection1 = (from row in entities.Cont_tbl_TemplateSectionMapping
                                                                       where row.TemplateID == TID
                                                                       && row.IsActive == true
                                                                       && row.CustomerID == CID
                                                                       select row).ToList();
                                            if (gettemplatesection1.Count > 0)
                                            {
                                                foreach (var itemsection in gettemplatesection1)
                                                {
                                                    Cont_tbl_ContractTemplateSectionMapping ContracttemplateSection = new Cont_tbl_ContractTemplateSectionMapping()
                                                    {
                                                        ContractTemplateInstanceID = Convert.ToInt32(newContractID),
                                                        SectionOrder = itemsection.SectionOrder,
                                                        SectionID = itemsection.SectionID,
                                                        IsActive = true,
                                                        CustomerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID),
                                                        CreatedBy = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID),
                                                        CreatedOn = DateTime.Now,
                                                        UpdatedBy = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID),
                                                        UpdatedOn = DateTime.Now
                                                    };
                                                    ContractTemplateManagement.CreateContractTemplateSection(ContracttemplateSection);
                                                }
                                            }
                                            var gettemplatesection = (from row in entities.Cont_tbl_TemplateSectionMapping
                                                                      where row.TemplateID == TID
                                                                      && row.IsActive == true
                                                                      && row.CustomerID == CID
                                                                      select row).ToList();

                                            if (gettemplatesection.Count > 0)
                                            {
                                                foreach (var item in gettemplatesection)
                                                {
                                                    var datatemplatesection = (from row in entities.SP_ContractTemplateSectionData(Convert.ToInt32(newContractID), CID, TID, item.SectionID)
                                                                               select row).ToList();

                                                    ContractTemplateAssignment ContracttemplateAssignment = new ContractTemplateAssignment()
                                                    {
                                                        ContractTemplateInstanceID = Convert.ToInt32(newContractID),
                                                        RoleID = 3,
                                                        UserID = AuthenticationHelper.UserID,
                                                        IsActive = true,
                                                        SectionID = item.SectionID
                                                    };
                                                    ContractTemplateManagement.CreateContractTemplateAssignment(ContracttemplateAssignment);

                                                    string templateSectionContent = datatemplatesection.FirstOrDefault();

                                                    ContractTemplateTransaction obj1 = new ContractTemplateTransaction();
                                                    obj1.ContractTemplateInstanceID = Convert.ToInt32(newContractID);
                                                    obj1.CreatedOn = DateTime.Now;
                                                    obj1.CreatedBy = AuthenticationHelper.UserID;
                                                    obj1.ContractTemplateContent = templateSectionContent;
                                                    obj1.CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User;
                                                    obj1.Status = 1;
                                                    obj1.OrderID = 1;
                                                    obj1.SectionID = item.SectionID;
                                                    entities.ContractTemplateTransactions.Add(obj1);
                                                    entities.SaveChanges();
                                                }
                                            }
                                            RoleId = getRoleID(Convert.ToInt32(newContractID), AuthenticationHelper.UserID);
                                            TemplateId = Convert.ToInt32(newContractID);
                                            UId = Convert.ToInt32(AuthenticationHelper.UserID);
                                            CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                                        }
                                    }
                                }

                                cvContractPopUp.IsValid = false;
                                cvContractPopUp.ErrorMessage = "Contract Created Sucessfully";
                                VSContractPopup.CssClass = "alert alert-success";

                                ViewState["Mode"] = 1;
                                ViewState["ContractInstanceID"] = newContractID;

                                toggleTextSaveButton_ContractSummaryTab(false); //false -- Switch to Edit Mode

                                showHideTabs(true);
                                showHideContractSummaryTabTopButtons(true);
                                enableDisableContractSummaryTabControls(false);

                                //Bind Case To Link
                                BindContractListToLink(newContractID);
                            }

                            if (saveSuccess)
                            {


                                #region Sent Email to Assigned Users

                                string emailTemplate = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_Contract_Assignment;

                                if (!string.IsNullOrEmpty(emailTemplate))
                                {
                                    string accessURL = string.Empty;
                                    URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(AuthenticationHelper.CustomerID));
                                    if (Urloutput != null)
                                    {
                                        accessURL = Urloutput.URL;
                                    }
                                    else
                                    {
                                        accessURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                                    }
                                    //string accessURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                                    ContractManagement.SendMailtoAssignedUsers(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, contractRecord, lstContractUserMapping, emailTemplate, accessURL, Convert.ToInt32(newContractID));
                                }

                                #endregion
                            }

                            //lstApproverMapping.Clear();
                            //lstApproverMapping = null;

                            //lstOwnerMapping.Clear();
                            //lstOwnerMapping = null;
                        }

                        #endregion

                        #region Edit Contract

                        else if ((int)ViewState["Mode"] == 1)
                        {
                            if (ViewState["ContractInstanceID"] != null)
                            {
                                newContractID = Convert.ToInt64(ViewState["ContractInstanceID"]);

                                if (newContractID != 0)
                                {
                                    contractRecord.ID = newContractID;

                                    bool existContractNo = ContractManagement.ExistsContractNo(customerID, contractRecord.ContractNo, newContractID);
                                    if (!existContractNo)
                                    {
                                        //if (!ContractManagement.ExistsContractTitle(customerID, contractRecord.ContractTitle, newContractID))
                                        if (1 == 1)
                                        {
                                            saveSuccess = ContractManagement.UpdateContract(contractRecord);

                                            if (saveSuccess)
                                            {
                                                ContractManagement.CreateAuditLog("C", newContractID, "Cont_tbl_ContractInstance", "Update", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Contract Detail(s) Updated", true, Convert.ToInt32(newContractID));
                                            }
                                        }
                                        else
                                        {
                                            cvContractPopUp.IsValid = false;
                                            cvContractPopUp.ErrorMessage = "Contract with Same Title already Exists.";
                                            cvContractPopUp.CssClass = "alert alert-danger";
                                            return;
                                        }
                                    }
                                    else
                                    {
                                        cvContractPopUp.IsValid = false;
                                        cvContractPopUp.ErrorMessage = "Contract with Same Contract Number already exists";
                                        cvContractPopUp.CssClass = "alert alert-danger";
                                    }

                                    if (saveSuccess)
                                    {
                                        bool matchSuccess = false;

                                        #region Contract Status Transaction

                                        Save_ContractStatus(newContractID);

                                        #endregion

                                        #region Vendor Mapping

                                        if (lstBoxVendor.Items.Count > 0)
                                        {
                                            foreach (ListItem eachVendor in lstBoxVendor.Items)
                                            {
                                                if (eachVendor.Selected)
                                                {
                                                    if (Convert.ToInt64(eachVendor.Value) != 0)
                                                        lstVendorMapping.Add(Convert.ToInt64(eachVendor.Value));
                                                }
                                            }
                                        }

                                        if (lstVendorMapping.Count > 0)
                                        {
                                            List<Cont_tbl_VendorMapping> lstVendorMapping_ToSave = new List<Cont_tbl_VendorMapping>();

                                            lstVendorMapping.ForEach(EachVendor =>
                                            {
                                                Cont_tbl_VendorMapping _vendorMappingRecord = new Cont_tbl_VendorMapping()
                                                {
                                                    ContractID = newContractID,
                                                    VendorID = EachVendor,
                                                    IsActive = true,
                                                    CreatedBy = AuthenticationHelper.UserID,
                                                    CreatedOn = DateTime.Now,
                                                };

                                                lstVendorMapping_ToSave.Add(_vendorMappingRecord);
                                            });

                                            var existingVendorMapping = VendorDetails.GetVendorMapping(newContractID);
                                            var assignedVendorIDs = lstVendorMapping_ToSave.Select(row => row.VendorID).ToList();

                                            if (existingVendorMapping.Count != assignedVendorIDs.Count)
                                            {
                                                matchSuccess = false;
                                            }
                                            else
                                            {
                                                matchSuccess = existingVendorMapping.Except(assignedVendorIDs).ToList().Count > 0 ? false : true;
                                            }

                                            if (!matchSuccess)
                                            {
                                                VendorDetails.DeActiveExistingVendorMapping(newContractID);

                                                saveSuccess = VendorDetails.CreateUpdate_VendorMapping(lstVendorMapping_ToSave);
                                                if (saveSuccess)
                                                {
                                                    ContractManagement.CreateAuditLog("C", newContractID, "Cont_tbl_VendorMapping", "Update", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Contract Vendor Updated", true, Convert.ToInt32(newContractID));
                                                }
                                            }
                                            else
                                                saveSuccess = false;

                                            //Refresh List
                                            lstVendorMapping_ToSave.Clear();
                                            lstVendorMapping_ToSave = null;

                                            lstVendorMapping.Clear();
                                            lstVendorMapping = null;
                                        }

                                        #endregion

                                        #region Update Custom Field                                        

                                        if (!string.IsNullOrEmpty(Convert.ToString(ViewState["CustomefieldCount"])))
                                        {
                                            List<Cont_tbl_CustomFieldValue> lstObjParameters = new List<Cont_tbl_CustomFieldValue>();

                                            //Select Which Grid to Loop based on Selected Category/Type
                                            GridView gridViewToCollectData = null;
                                            gridViewToCollectData = grdCustomField;

                                            if (gridViewToCollectData != null)
                                            {
                                                for (int i = 0; i < gridViewToCollectData.Rows.Count; i++)
                                                {
                                                    Label lblID = (Label)gridViewToCollectData.Rows[i].FindControl("lblID");
                                                    TextBox tbxLabelValue = (TextBox)gridViewToCollectData.Rows[i].FindControl("tbxLabelValue");

                                                    if (lblID != null && tbxLabelValue != null)
                                                    {
                                                        if (!string.IsNullOrEmpty(lblID.Text) || lblID.Text != "0")
                                                        {
                                                            Cont_tbl_CustomFieldValue ObjParameter = new Cont_tbl_CustomFieldValue()
                                                            {
                                                                ContractID = newContractID,

                                                                LabelID = Convert.ToInt64(lblID.Text),
                                                                LabelValue = tbxLabelValue.Text,

                                                                IsDeleted = false,
                                                                CreatedBy = AuthenticationHelper.UserID,
                                                                CreatedOn = DateTime.Now,
                                                                UpdatedBy = AuthenticationHelper.UserID,
                                                                UpdatedOn = DateTime.Now
                                                            };

                                                            lstObjParameters.Add(ObjParameter);

                                                            //saveSuccess = ContractManagement.CreateUpdateCustomsField(ObjParameter);
                                                        }
                                                    }
                                                }//End For Each

                                                var existingCustomFields = ContractManagement.GetCustomsFieldsContractWise_All(Convert.ToInt32(AuthenticationHelper.CustomerID), newContractID, Convert.ToInt64(ddlContractType.SelectedValue));

                                                var lstSavedCustomFields = existingCustomFields.Select(row => row.LableID).ToList();
                                                var assignedCustomFields = lstObjParameters.Select(row => row.LabelID).ToList();

                                                matchSuccess = false;

                                                if (lstSavedCustomFields.Count != assignedCustomFields.Count)
                                                {
                                                    matchSuccess = false;
                                                }
                                                else
                                                {
                                                    matchSuccess = lstSavedCustomFields.Except(assignedCustomFields).ToList().Count > 0 ? false : true;
                                                }

                                                if (!matchSuccess)
                                                {
                                                    saveSuccess = ContractManagement.DeActiveExistingCustomsFieldsByContractID(newContractID);
                                                    saveSuccess = ContractManagement.CreateUpdate_CustomsFields(lstObjParameters);
                                                }
                                                else
                                                    saveSuccess = false;

                                                if (saveSuccess)
                                                {
                                                    ContractManagement.CreateAuditLog("C", newContractID, "Cont_tbl_CustomFieldValue", "Update", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Contract Custom Parameter(s) Updated", true, Convert.ToInt32(newContractID));
                                                }
                                            }
                                        }

                                        #endregion

                                        #region User Assignment                           

                                        List<Cont_tbl_UserAssignment> lstUserAssignmentRecord_ToSave = new List<Cont_tbl_UserAssignment>();
                                        lstContractUserMapping.Clear();

                                        int assignedRoleID = 3;
                                        if (lstBoxOwner.Items.Count > 0)
                                        {
                                            assignedRoleID = 3;
                                            foreach (ListItem eachOwner in lstBoxOwner.Items)
                                            {
                                                if (eachOwner.Selected)
                                                {
                                                    if (Convert.ToInt32(eachOwner.Value) != 0)
                                                        lstContractUserMapping.Add(new Tuple<int, int>(Convert.ToInt32(eachOwner.Value), assignedRoleID));
                                                    //lstOwnerMapping.Add(Convert.ToInt32(eachOwner.Value));
                                                }
                                            }
                                        }

                                        if (lstBoxApprover.Items.Count > 0)
                                        {
                                            assignedRoleID = 6;
                                            foreach (ListItem eachApprover in lstBoxApprover.Items)
                                            {
                                                if (eachApprover.Selected)
                                                {
                                                    if (Convert.ToInt32(eachApprover.Value) != 0)
                                                        lstContractUserMapping.Add(new Tuple<int, int>(Convert.ToInt32(eachApprover.Value), assignedRoleID));
                                                    // lstApproverMapping.Add(Convert.ToInt32(eachApprover.Value));
                                                }
                                            }
                                        }

                                        if (lstContractUserMapping.Count > 0)
                                        {
                                            lstContractUserMapping.ForEach(eachUser =>
                                            {
                                                Cont_tbl_UserAssignment newAssignment = new Cont_tbl_UserAssignment()
                                                {
                                                    AssignmentType = 1,
                                                    ContractID = newContractID,

                                                    UserID = Convert.ToInt32(eachUser.Item1),
                                                    RoleID = Convert.ToInt32(eachUser.Item2),

                                                    IsActive = true,
                                                    CreatedBy = AuthenticationHelper.UserID,
                                                    UpdatedBy = AuthenticationHelper.UserID,
                                                };

                                                lstUserAssignmentRecord_ToSave.Add(newAssignment);

                                                //saveSuccess = ContractManagement.CreateUpdateContractUserAssignment(newAssignment);
                                            });
                                        }

                                        var existingUserAssignment = ContractManagement.GetContractUserAssignment_All(newContractID);

                                        var assignedUserIDs = existingUserAssignment.Select(row => row.UserID).ToList();
                                        var currentUserIDs = lstUserAssignmentRecord_ToSave.Select(row => row.UserID).ToList();

                                        matchSuccess = false;

                                        if (assignedUserIDs.Count != currentUserIDs.Count)
                                        {
                                            matchSuccess = false;
                                        }
                                        else
                                        {
                                            matchSuccess = assignedUserIDs.Except(currentUserIDs).ToList().Count > 0 ? false : true;
                                        }

                                        if (!matchSuccess)
                                        {
                                            //De-Active All Previous Assignment
                                            ContractManagement.DeActiveContractUserAssignments(newContractID, AuthenticationHelper.UserID);

                                            if (lstUserAssignmentRecord_ToSave.Count > 0)
                                                saveSuccess = ContractManagement.CreateUpdate_ContractUserAssignments(lstUserAssignmentRecord_ToSave);

                                            if (saveSuccess)
                                            {
                                                ContractManagement.CreateAuditLog("C", newContractID, "Cont_tbl_UserAssignment", "Update", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Contract Owner(s) Updated", true, Convert.ToInt32(newContractID));
                                            }
                                        }
                                        else
                                            saveSuccess = true;

                                        lstUserAssignmentRecord_ToSave.Clear();
                                        lstUserAssignmentRecord_ToSave = null;

                                        #endregion
                                    }

                                    if (saveSuccess)
                                    {
                                        cvContractPopUp.IsValid = false;
                                        cvContractPopUp.ErrorMessage = "Contract Detail(s) Updated Sucessfully";
                                        VSContractPopup.CssClass = "alert alert-success";

                                        ViewState["Mode"] = 1;
                                        ViewState["ContractInstanceID"] = newContractID;

                                        toggleTextSaveButton_ContractSummaryTab(false); //false -- Switch to Edit Mode

                                        showHideTabs(true);
                                        showHideContractSummaryTabTopButtons(true);
                                        enableDisableContractSummaryTabControls(false);
                                    }
                                }
                            }
                        }

                        #endregion

                    }

                    #endregion
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvContractPopUp.IsValid = false;
                cvContractPopUp.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        public class getTemplateTransactionDetail
        {
            public long TemplateTransactionID { get; set; }
            public long TemplateInstanceID { get; set; }
            public long ComplianceStatusID { get; set; }
            public System.DateTime CreatedOn { get; set; }
            public string Status { get; set; }
            public Nullable<System.DateTime> StatusChangedOn { get; set; }
            public string TemplateContent { get; set; }
            public long RoleID { get; set; }
            public long UserID { get; set; }
            public int sectionID { get; set; }
            public int orderID { get; set; }
        }


        public int getRoleID(int ContractTemplateId, int UserID)
        {
            int RoleID = -1;
            if (!string.IsNullOrEmpty(Request.QueryString["RID"]))
                RoleID = Convert.ToInt32(Request.QueryString["RID"]);

                //using (ComplianceDBEntities entities = new ComplianceDBEntities())
                //{
                //    ContractTemplateAssignment data = (from row in entities.ContractTemplateAssignments
                //                                       where row.ContractTemplateInstanceID == ContractTemplateId
                //                                        && row.UserID == UserID
                //                                       select row).FirstOrDefault();
                //    if (data != null)
                //    {
                //        RoleID = Convert.ToInt32(data.RoleID);
                //    }
                //}
                return RoleID;
        }
        #endregion

        //public bool ExporttoWordNew(int ContractTemplateID, int CustomerID, int TemplateID, string TemplateName)
        //{
        //    bool resultdata = false;

        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        var lstSections = (from row in entities.SP_ContractTemplateData(ContractTemplateID, CustomerID, TemplateID)
        //                           select row).FirstOrDefault();
        //        if (lstSections != null)
        //        {
        //            resultdata = true;
        //            System.Text.StringBuilder strExporttoWord = new System.Text.StringBuilder();
        //            string fileName = TemplateName;
        //            string templateName = TemplateName;

        //            //string fileName = lstSections.TemplateName + "-" + lstSections.Version;
        //            //string templateName = lstSections.TemplateName;

        //            strExporttoWord.Append(@"
        //        <html 
        //        xmlns:o='urn:schemas-microsoft-com:office:office' 
        //        xmlns:w='urn:schemas-microsoft-com:office:word'
        //        xmlns='http://www.w3.org/TR/REC-html40'>
        //        <head><title></title>

        //        <!--[if gte mso 9]>
        //        <xml>
        //        <w:WordDocument>
        //        <w:View>Print</w:View>
        //        <w:Zoom>90</w:Zoom>
        //        <w:DoNotOptimizeForBrowser/>
        //        </w:WordDocument>
        //        </xml>
        //        <![endif]-->

        //        <style>
        //        p.MsoFooter, li.MsoFooter, div.MsoFooter
        //        {
        //        margin:0in;
        //        margin-bottom:.0001pt;
        //        mso-pagination:widow-orphan;
        //        tab-stops:center 3.0in right 6.0in;
        //        font-size:12.0pt;
        //        }
        //        <style>

        //        <!-- /* Style Definitions */

        //        @page Section1
        //        {
        //        size:8.5in 11.0in; 
        //        margin:1.0in 1.25in 1.0in 1.25in ;
        //        mso-header-margin:.5in;
        //        mso-header: h1;
        //        mso-footer: f1; 
        //        mso-footer-margin:.5in;
        //         font-family:Arial;
        //        }

        //        div.Section1
        //        {
        //        page:Section1;
        //        }
        //        table#hrdftrtbl
        //        {
        //            margin:0in 0in 0in 9in;
        //        }

        //        .break { page-break-before: always; }
        //        -->
        //        </style></head>");

        //            strExporttoWord.Append(@"<body lang=EN-US style='tab-interval:.5in'> <div class=Section1>");

        //            strExporttoWord.Append(lstSections);

        //            strExporttoWord.Append(@"<table id='hrdftrtbl' border='1' cellspacing='0' cellpadding='0'>
        //        <tr>
        //            <td>
        //                <div style='mso-element:header' id=h1 >
        //                    <p class=MsoHeader style='text-align:left'>
        //                </div>
        //            </td>
        //            <td>
        //                <div style='mso-element:footer' id=f1>
        //                    <p class=MsoFooter>
        //                    <span style=mso-tab-count:2'></span>
        //                    <span style='mso-field-code:"" PAGE ""'></span>
        //                    of <span style='mso-field-code:"" NUMPAGES ""'></span>
        //                    </p>
        //                </div>
        //            </td>
        //        </tr>
        //        </table>
        //        </body>
        //        </html>");

        //            fileName = fileName + "-" + DateTime.Now.ToString("ddMMyyyy");

        //            Response.AppendHeader("Content-Type", "application/msword");
        //            Response.AppendHeader("Content-disposition", "attachment; filename=" + fileName + ".doc");
        //            Response.Write(strExporttoWord);
        //        }
        //    }
        //    return resultdata;
        //}


        #region Custom Field/Parameter
        private void intializeDataTableCustomField(GridView gridViewCustomField)
        {
            try
            {
                DataTable dtCustomField = new DataTable();

                DataRow drowCustomField = null;

                dtCustomField.Columns.Add(new DataColumn("LableID", typeof(string)));
                dtCustomField.Columns.Add(new DataColumn("Label", typeof(string)));
                dtCustomField.Columns.Add(new DataColumn("LabelValue", typeof(string)));

                drowCustomField = dtCustomField.NewRow();

                drowCustomField["LableID"] = string.Empty;
                drowCustomField["Label"] = string.Empty;
                drowCustomField["LabelValue"] = string.Empty;

                dtCustomField.Rows.Add(drowCustomField);

                ViewState["dataTableCustomFields"] = dtCustomField;

                gridViewCustomField.Visible = true;
                //gridViewCustomField_History.Visible = false;

                gridViewCustomField.DataSource = dtCustomField; /*Assign datasource to create one row with default values for the class you have*/
                gridViewCustomField.DataBind();

                //To Hide row
                gridViewCustomField.Rows[0].Visible = false;
                gridViewCustomField.Rows[0].Controls.Clear();

                //lblAddNewGround.Visible = true;               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindCustomFields(GridView gridViewCustomField) //, GridView gridViewCustomField_History
        {
            try
            {
                long contractInstanceID = 0;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["ContractInstanceID"])))
                {
                    contractInstanceID = Convert.ToInt64(ViewState["ContractInstanceID"]);
                }
                if (ddlContractType.SelectedValue != null && ddlContractType.SelectedValue != "0")  //&& CaseInstanceID != 0
                {
                    List<Cont_SP_GetCustomFieldsValues_Result> lstCustomFields = new List<Cont_SP_GetCustomFieldsValues_Result>();

                    if (contractInstanceID != 0)
                    {
                        lstCustomFields = ContractManagement.GetCustomsFieldsContractWise_All(Convert.ToInt32(AuthenticationHelper.CustomerID), contractInstanceID, Convert.ToInt64(ddlContractType.SelectedValue));

                        bool historyFlag = false;
                        if (ViewState["FlagHistory"] != null)
                        {
                            historyFlag = Convert.ToBoolean(Convert.ToInt32(ViewState["FlagHistory"]));
                        }

                        if (lstCustomFields != null && lstCustomFields.Count > 0)
                        {
                            ViewState["CustomefieldCount"] = lstCustomFields.Count;

                            gridViewCustomField.DataSource = lstCustomFields;
                            gridViewCustomField.DataBind();
                            gridViewCustomField.Visible = true;

                            if (!historyFlag)
                            {

                            }
                            else if (historyFlag)
                            {

                            }
                        }
                        else
                        {
                            Cont_SP_GetCustomFieldsValues_Result obj = new Cont_SP_GetCustomFieldsValues_Result(); //initialize empty class that may contain properties
                            lstCustomFields.Add(obj); //Add empty object to list

                            gridViewCustomField.DataSource = lstCustomFields; /*Assign datasource to create one row with default values for the class you have*//*Assign datasource to create one row with default values for the class you have*/
                            gridViewCustomField.DataBind(); //Bind that empty source     

                            //To Hide row
                            gridViewCustomField.Rows[0].Visible = false;
                            gridViewCustomField.Rows[0].Controls.Clear();

                            gridViewCustomField.Visible = true;

                            if (!historyFlag)
                            {

                            }
                            else if (historyFlag)
                            {

                            }
                        }
                    }
                    else
                    {
                        intializeDataTableCustomField(grdCustomField); //, grdCustomField_History
                    }

                    lblCustomField.Visible = true;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindCustomFieldDropDown(DropDownList ddlCustomField, GridView gridViewCustomField)
        {
            try
            {
                if (ddlContractType.SelectedValue != null && ddlContractType.SelectedValue != "" && ddlContractType.SelectedValue != "0")
                {
                    var customFields = ContractManagement.GetCustomsFieldsByContractType(Convert.ToInt32(AuthenticationHelper.CustomerID), Convert.ToInt64(ddlContractType.SelectedValue));

                    if (customFields.Count > 0)
                    {
                        //lblAddNewGround.Visible = true;
                        gridViewCustomField.Visible = true;

                        if (ddlCustomField.Items.Count > 0)
                            ddlCustomField.Items.Clear();

                        ddlCustomField.DataTextField = "Label";
                        ddlCustomField.DataValueField = "ID";

                        ddlCustomField.DataSource = customFields;
                        ddlCustomField.DataBind();

                        //ddlCustomField.Items.Add(new ListItem("Add New", "0"));

                        ViewState["ddlCustomFieldFilled"] = "1";
                    }
                    else
                    {
                        //lblAddNewGround.Visible = false;
                        gridViewCustomField.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdCustomField_Common_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                GridView gridView = (GridView)sender;

                if (gridView != null)
                {
                    if (e.Row.RowType == DataControlRowType.DataRow)
                    {
                        TextBox tbxLabelValue = (TextBox)e.Row.FindControl("tbxLabelValue");

                        if (!string.IsNullOrEmpty(Request.QueryString["HistoryFlag"]))
                        {
                            tbxLabelValue.Enabled = false;
                        }

                        //Hide Delete in Case of Total or Row with 0 ID
                        Label lblID = (Label)e.Row.FindControl("lblID");
                        LinkButton lnkBtnDeleteCustomField = (LinkButton)e.Row.FindControl("lnkBtnDeleteCustomField");

                        if (lblID != null && lnkBtnDeleteCustomField != null)
                        {
                            if (lblID.Text != "" && lblID.Text != "0")
                            {
                                e.Row.Enabled = true;
                                lnkBtnDeleteCustomField.Visible = true;
                            }
                            else
                            {
                                e.Row.Enabled = false;
                                lnkBtnDeleteCustomField.Visible = false;
                            }
                        }
                    }

                    if (e.Row.RowType == DataControlRowType.Footer)
                    {
                        DropDownList ddlFieldName_Footer = (DropDownList)e.Row.FindControl("ddlFieldName_Footer");

                        if (ddlFieldName_Footer != null)
                        {
                            BindCustomFieldDropDown(ddlFieldName_Footer, gridView);

                            foreach (GridViewRow gvr in gridView.Rows)
                            {
                                Label lblID = (Label)gvr.FindControl("lblID");

                                if (lblID != null)
                                {
                                    if (lblID.Text != "")
                                    {
                                        if (ddlFieldName_Footer.Items.FindByValue(lblID.Text) != null)
                                            ddlFieldName_Footer.Items.Remove(ddlFieldName_Footer.Items.FindByValue(lblID.Text));
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdCustomField_Common_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                GridView gridView = (GridView)sender;

                if (gridView != null)
                {
                    gridView.PageIndex = e.NewPageIndex;
                    BindCustomFields(grdCustomField); //, grdCustomField_History
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdCustomField_Common_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                long contractInstanceID = 0;
                bool deleteSuccess = false;

                if (ViewState["ContractInstanceID"] != null && e.CommandName.Equals("DeleteCustomField") && e.CommandArgument != null && ViewState["Mode"] != null)
                {
                    if ((int)ViewState["Mode"] == 0 && ViewState["dataTableCustomFields"] != null)
                    {
                        GridViewRow gvRow = (GridViewRow)(((Control)e.CommandSource).NamingContainer);
                        if (gvRow != null)
                        {
                            //GridViewRow gvRow = (GridViewRow)(sender).Parent.Parent;
                            int index = gvRow.RowIndex;

                            DataTable dtCustomField = ViewState["dataTableCustomFields"] as DataTable;
                            dtCustomField.Rows[index].Delete();

                            ViewState["dataTableCustomFields"] = dtCustomField;
                            ViewState["CustomefieldCount"] = dtCustomField.Rows.Count;

                            GridView gridView = (GridView)sender;

                            if (gridView != null)
                            {
                                gridView.DataSource = dtCustomField; /*Assign datasource to create one row with default values for the class you have*/
                                gridView.DataBind();
                            }
                        }
                    }
                    else if ((int)ViewState["Mode"] == 1)
                    {
                        long LableID = Convert.ToInt64(e.CommandArgument);

                        contractInstanceID = Convert.ToInt64(ViewState["ContractInstanceID"]);

                        if (contractInstanceID != 0 && LableID != 0)
                        {
                            deleteSuccess = ContractManagement.DeleteCustomFieldsContractWise(contractInstanceID, LableID);
                            if (deleteSuccess)
                            {
                                LitigationManagement.CreateAuditLog("C", contractInstanceID, "Cont_tbl_CustomFieldValue", "Delete", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Custom Field Deleted", true);

                                BindCustomFields(grdCustomField); //, grdCustomField_History
                            }
                        }
                    }
                }


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lnkBtnAddCustomField_Click(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["Mode"] != null)
                {
                    long lblID = 0;

                    DropDownList ddlFieldName_Footer = (DropDownList)grdCustomField.FooterRow.FindControl("ddlFieldName_Footer");
                    TextBox txtFieldValue_Footer = (TextBox)grdCustomField.FooterRow.FindControl("txtFieldValue_Footer");

                    if (ddlFieldName_Footer != null && txtFieldValue_Footer != null)
                    {
                        if ((int)ViewState["Mode"] == 0)
                        {
                            string lblName = ddlFieldName_Footer.SelectedItem.Text;
                            lblID = Convert.ToInt64(ddlFieldName_Footer.SelectedValue);

                            if (ViewState["dataTableCustomFields"] != null && lblID != 0)
                            {
                                DataTable dtcurrentTableCumtomFields = (DataTable)ViewState["dataTableCustomFields"];

                                DataRow drNewRow = dtcurrentTableCumtomFields.NewRow();

                                drNewRow["LableID"] = lblID;
                                drNewRow["Label"] = lblName;
                                drNewRow["LabelValue"] = txtFieldValue_Footer.Text;

                                //add new row to DataTable
                                dtcurrentTableCumtomFields.Rows.Add(drNewRow);

                                //Delete Rows with blank LblID (if Any)
                                dtcurrentTableCumtomFields = LitigationManagement.LoopAndDeleteBlankRows(dtcurrentTableCumtomFields);

                                ViewState["CustomefieldCount"] = dtcurrentTableCumtomFields.Rows.Count;

                                //Store the current data to ViewState
                                ViewState["CurrentTablePutCallDtls"] = dtcurrentTableCumtomFields;


                                //Rebind the Grid with the current data
                                grdCustomField.DataSource = dtcurrentTableCumtomFields;
                                grdCustomField.DataBind();

                            }
                        }//Add Mode End
                        else if ((int)ViewState["Mode"] == 1)
                        {
                            if (ViewState["ContractInstanceID"] != null)
                            {
                                long contractInstanceID = Convert.ToInt64(ViewState["ContractInstanceID"]);

                                if (txtFieldValue_Footer != null && ddlFieldName_Footer != null && contractInstanceID != 0)
                                {
                                    bool validateData = false;
                                    bool saveSuccess = false;

                                    if (!String.IsNullOrEmpty(ddlFieldName_Footer.SelectedValue) && ddlFieldName_Footer.SelectedValue != "0")
                                    {
                                        if (txtFieldValue_Footer.Text != "")
                                        {
                                            validateData = true;
                                        }
                                    }

                                    if (validateData)
                                    {
                                        lblID = Convert.ToInt64(ddlFieldName_Footer.SelectedValue);

                                        if (lblID != 0)
                                        {
                                            Cont_tbl_CustomFieldValue ObjParameter = new Cont_tbl_CustomFieldValue()
                                            {
                                                ContractID = contractInstanceID,
                                                LabelID = lblID,
                                                LabelValue = txtFieldValue_Footer.Text,
                                                IsDeleted = false,
                                                CreatedBy = AuthenticationHelper.UserID,
                                                CreatedOn = DateTime.Now,
                                                UpdatedBy = AuthenticationHelper.UserID,
                                            };

                                            saveSuccess = ContractManagement.CreateUpdateCustomsField(ObjParameter);

                                            if (saveSuccess)
                                            {
                                                ContractManagement.CreateAuditLog("C", contractInstanceID, "Cont_tbl_CustomFieldValue", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Custom Field Added", true, Convert.ToInt32(contractInstanceID));

                                                if (ViewState["ContractTypeIDUpdated"] != null)
                                                {
                                                    if (ViewState["ContractTypeIDUpdated"].ToString() == "false")
                                                    {
                                                        saveSuccess = ContractManagement.UpdateContractTypeID(contractInstanceID, Convert.ToInt64(ddlContractType.SelectedValue));
                                                        if (saveSuccess)
                                                        {
                                                            saveSuccess = ContractManagement.DeletePreviousCustomFields(contractInstanceID, Convert.ToInt64(ddlContractType.SelectedValue));

                                                            if (saveSuccess)
                                                                ViewState["ContractTypeIDUpdated"] = "true";
                                                        }
                                                    }
                                                }

                                                BindCustomFields(grdCustomField); //, grdCustomField_History

                                            }
                                        }
                                    }
                                }
                            }
                        }//Edit Mode End
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void TextChangedInsideGridView_TextChanged(object sender, EventArgs e)
        {
            GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent;
            if (currentRow != null)
            {
                if (currentRow.RowType == DataControlRowType.DataRow)
                {
                    TextBox tbxLabelValue = (TextBox)currentRow.FindControl("tbxLabelValue");
                    TextBox tbxInterestValue = (TextBox)currentRow.FindControl("tbxInterestValue");
                    TextBox tbxPenaltyValue = (TextBox)currentRow.FindControl("tbxPenaltyValue");
                    TextBox tbxRowTotalValue = (TextBox)currentRow.FindControl("tbxRowTotalValue");

                    if (tbxLabelValue != null && tbxInterestValue != null && tbxPenaltyValue != null && tbxRowTotalValue != null)
                    {
                        tbxRowTotalValue.Text = (LitigationManagement.csvToNumber(tbxLabelValue.Text) +
                            LitigationManagement.csvToNumber(tbxInterestValue.Text) +
                            LitigationManagement.csvToNumber(tbxPenaltyValue.Text)).ToString("N2");
                    }
                }
                else if (currentRow.RowType == DataControlRowType.Footer)
                {
                    TextBox txtFieldValue_Footer = (TextBox)currentRow.FindControl("txtFieldValue_Footer");
                    TextBox txtInterestValue_Footer = (TextBox)currentRow.FindControl("txtInterestValue_Footer");
                    TextBox txtPenaltyValue_Footer = (TextBox)currentRow.FindControl("txtPenaltyValue_Footer");
                    TextBox tbxRowTotalValue_Footer = (TextBox)currentRow.FindControl("tbxRowTotalValue_Footer");

                    if (txtFieldValue_Footer != null && txtInterestValue_Footer != null && txtPenaltyValue_Footer != null && tbxRowTotalValue_Footer != null)
                    {
                        tbxRowTotalValue_Footer.Text = (LitigationManagement.csvToNumber(txtFieldValue_Footer.Text) +
                            LitigationManagement.csvToNumber(txtInterestValue_Footer.Text) +
                            LitigationManagement.csvToNumber(txtPenaltyValue_Footer.Text)).ToString("N2");
                    }
                }
            }
        }

        protected void grdCustomField_History_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblIsAllowed = (Label)e.Row.FindControl("lblIsAllowed");
                    DropDownList ddlGroundResult = (DropDownList)e.Row.FindControl("ddlGroundResult");

                    if (ddlGroundResult != null && lblIsAllowed != null)
                    {
                        if (lblIsAllowed.Text != null && lblIsAllowed.Text != "")
                        {
                            string isAllowed = "0";

                            if (lblIsAllowed.Text == "True")
                                isAllowed = "1";
                            else if (lblIsAllowed.Text == "False")
                                isAllowed = "0";

                            ddlGroundResult.ClearSelection();

                            if (ddlGroundResult.Items.FindByValue(isAllowed) != null)
                                ddlGroundResult.Items.FindByValue(isAllowed).Selected = true;
                        }
                    }

                    Label lblPenalty = (Label)e.Row.FindControl("lblPenalty");

                    if (lblPenalty != null)
                    {
                        if (string.IsNullOrEmpty(lblPenalty.Text))
                            lblPenalty.Visible = false;
                    }

                    Label lblInterest = (Label)e.Row.FindControl("lblInterest");

                    if (lblInterest != null)
                    {
                        if (string.IsNullOrEmpty(lblInterest.Text))
                            lblInterest.Visible = false;
                    }

                    Label lblSettlementValue = (Label)e.Row.FindControl("lblSettlementValue");

                    if (lblSettlementValue != null)
                    {
                        if (string.IsNullOrEmpty(lblSettlementValue.Text))
                            lblSettlementValue.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        #endregion

        #region Contract - Linking

        private void BindLinkedContracts(long contractID)
        {
            if (contractID != 0)
            {
                var lstLinkedContracts = ContractManagement.GetLinkedContractList_All(contractID, Convert.ToInt32(AuthenticationHelper.CustomerID));

                grdLinkedContracts.DataSource = lstLinkedContracts;
                grdLinkedContracts.DataBind();

                if (lstLinkedContracts.Count > 0)
                    divLinkedContracts.Visible = true;
                else
                    divLinkedContracts.Visible = false;

                lstLinkedContracts.Clear();
                lstLinkedContracts = null;
                upLinkedContracts.Update();
            }
        }

        private void BindContractListToLink(long contactID)
        {

            if (contactID != 0)
            {
                long customerID = -1;
                customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;
                int vendorID = -1;
                int deptID = -1;
                long contractStatusID = -1;
                long contractTypeID = -1;
                List<int> branchList = new List<int>();
                var lstAssignedContracts = ContractManagement.GetAssignedContractsList(Convert.ToInt32(customerID), AuthenticationHelper.UserID, AuthenticationHelper.Role,
                    3, branchList, vendorID, deptID, contractStatusID, contractTypeID);

                if (lstAssignedContracts.Count > 0)
                    lstAssignedContracts = lstAssignedContracts.Where(row => row.ID != contactID).ToList();

                var lstAlreadyLinkedContracts = ContractManagement.GetLinkedContractIDs(contactID, Convert.ToInt32(customerID));

                if (lstAlreadyLinkedContracts.Count > 0)
                    lstAssignedContracts = lstAssignedContracts.Where(row => !lstAlreadyLinkedContracts.Contains(row.ID)).ToList();

                grdContractList_LinkContract.DataSource = lstAssignedContracts;
                grdContractList_LinkContract.DataBind();

                lstAssignedContracts.Clear();
                lstAssignedContracts = null;
            }


        }
        protected void grdLinkedContracts_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                bool deleteSuccess = false;
                if (e.CommandArgument != null && ViewState["ContractInstanceID"] != null)
                {
                    if (e.CommandName.Equals("ViewLinkedContract"))
                    {
                        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                        long contractID = Convert.ToInt64(commandArgs[0]);
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "scriptHistoryPopUp", "OpenContractHistoryPopup(" + contractID + "," + " true" + ",'L'" + ");", true);
                    }
                    else if (e.CommandName.Equals("DeleteContractLinking"))
                    {
                        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                        long contractID = Convert.ToInt64(commandArgs[0]);
                        long LinkedContractID = Convert.ToInt64(commandArgs[1]);

                        if (contractID != 0 && LinkedContractID != 0)
                        {
                            deleteSuccess = ContractManagement.DeleteContractLinking(contractID, LinkedContractID, AuthenticationHelper.UserID);

                            if (deleteSuccess)
                            {
                                BindLinkedContracts(contractID);
                                ContractManagement.CreateAuditLog("C", contractID, "Cont_tbl_ContractLinking", "Delete", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Contract Linking Deleted", true, Convert.ToInt32(contractID));

                                BindContractListToLink(contractID);

                                cvLinkedContracts.IsValid = false;
                                cvLinkedContracts.ErrorMessage = "Contract Linking Deleted Successfully";
                                vsLinkedContracts.CssClass = "alert alert-success";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdLinkedContracts_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    LinkButton lnkBtnDeleteContractLinking = (LinkButton)e.Row.FindControl("lnkBtnDeleteContractLinking");
                    if (lnkBtnDeleteContractLinking != null)
                    {
                        if (ViewState["ContractStatusID"] != null)
                        {
                            if (Convert.ToInt32(ViewState["ContractStatusID"]) == 9) //9-Renewed
                                lnkBtnDeleteContractLinking.Visible = false;
                            else
                                lnkBtnDeleteContractLinking.Visible = true;
                        }

                        if (!string.IsNullOrEmpty(Request.QueryString["HistoryFlag"]))
                        {
                            lnkBtnDeleteContractLinking.Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnSaveLinkContract_Click(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["PageLink"].Equals("ContractLink"))
                {
                    bool saveSuccess = false;
                    List<long> lstContractsToLink = new List<long>();
                    int totalRecordSaveCount = 0;

                    if (ViewState["ContractInstanceID"] != null)
                    {
                        long contractID = Convert.ToInt64(ViewState["ContractInstanceID"]);

                        for (int i = 0; i < grdContractList_LinkContract.Rows.Count; i++)
                        {
                            CheckBox chkRowLinkContracts = (CheckBox)grdContractList_LinkContract.Rows[i].FindControl("chkRowLinkContracts");

                            if (chkRowLinkContracts != null)
                            {
                                if (chkRowLinkContracts.Checked)
                                {
                                    Label lblContractIID = (Label)grdContractList_LinkContract.Rows[i].FindControl("lblContractIID");

                                    if (lblContractIID != null)
                                    {
                                        if (lblContractIID.Text != "")
                                            lstContractsToLink.Add(Convert.ToInt64(lblContractIID.Text));
                                    }
                                }
                            }
                        }

                        if (lstContractsToLink.Count > 0)
                        {
                            lstContractsToLink.ForEach(eachContractToLink =>
                            {
                                Cont_tbl_ContractLinking newRecord = new Cont_tbl_ContractLinking()
                                {
                                    ContractID = contractID,
                                    LinkedContractID = eachContractToLink,
                                    IsActive = true,
                                    CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID),

                                    CreatedBy = AuthenticationHelper.UserID,
                                    CreatedOn = DateTime.Now,

                                    UpdatedBy = AuthenticationHelper.UserID,
                                    UpdatedOn = DateTime.Now,
                                };

                                saveSuccess = ContractManagement.CreateUpdateContractLinking(newRecord);


                                if (saveSuccess)
                                    totalRecordSaveCount++;
                            });
                        }

                        if (saveSuccess)
                        {
                            BindLinkedContracts(contractID);
                            BindContractListToLink(contractID);
                        
                            ContractManagement.CreateAuditLog("C", contractID, "Cont_tbl_ContractLinking", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, totalRecordSaveCount + " Contract(s) Linked", true, Convert.ToInt32(contractID));

                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "scriptUnCheckAllGridCheckBox", "unCheckAll('<%=grdContractList_LinkContract.ClientID %>');", true);

                            cvLinkContract.IsValid = false;
                            cvLinkContract.ErrorMessage = totalRecordSaveCount + "\t \t Contract(s) Linked Successfully";
                            vsLinkContract.CssClass = "alert alert-success";
                        }
                    }

                }
                else
                {
                    bool saveSuccess = false;
                    List<long> lstContractsToLink = new List<long>();
                    int totalRecordSaveCount = 0;

                    if (ViewState["ContractInstanceID"] != null)
                    {
                        long contractID = Convert.ToInt64(ViewState["ContractInstanceID"]);

                        for (int i = 0; i < grdContractList_LinkContract.Rows.Count; i++)
                        {
                            CheckBox chkRowLinkContracts = (CheckBox)grdContractList_LinkContract.Rows[i].FindControl("chkRowLinkContracts");

                            if (chkRowLinkContracts != null)
                            {
                                if (chkRowLinkContracts.Checked)
                                {
                                    Label lblContractIID = (Label)grdContractList_LinkContract.Rows[i].FindControl("lblContractIID");

                                    if (lblContractIID != null)
                                    {
                                        if (lblContractIID.Text != "")
                                            lstContractsToLink.Add(Convert.ToInt64(lblContractIID.Text));
                                    }
                                }
                            }
                        }


                        if (lstContractsToLink.Count > 0)
                        {
                            lstContractsToLink.ForEach(eachContractToLink =>
                            {
                                if (ViewState["ContractInstanceID"] != null)
                                {
                                    if (!string.IsNullOrEmpty(ViewState["ContractInstanceID"].ToString()))
                                    {

                                        int ContractInstanceID = Convert.ToInt32(ViewState["ContractInstanceID"]);

                                        if (ContractInstanceID > 0)
                                        {
                                            List<Cont_tbl_FileData> objFileData = new List<Cont_tbl_FileData>();
                                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                            {
                                                objFileData = (from row in entities.Cont_tbl_FileData
                                                               where row.IsDeleted == false
                                                               && row.ContractID == eachContractToLink
                                                               select row).ToList();

                                                #region Upload Document
                                                foreach (var item in objFileData)
                                                {
                                                    Cont_tbl_FileData objContDoc = new Cont_tbl_FileData()
                                                    {
                                                        ContractID = ContractInstanceID,
                                                        CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID),
                                                        DocTypeID = item.DocTypeID,
                                                        FileName = item.FileName,
                                                        FilePath = item.FilePath,
                                                        // FileTags = item.FileTags,
                                                        FileKey = item.FileKey,
                                                        Version = item.Version,
                                                        VersionDate = item.VersionDate,
                                                        CreatedBy = AuthenticationHelper.UserID,
                                                        CreatedOn = DateTime.Now,


                                                    };
                                                    entities.Cont_tbl_FileData.Add(objContDoc);
                                                    entities.SaveChanges();

                                                    List<Cont_tbl_FileDataTagsMapping> objTag = new List<Cont_tbl_FileDataTagsMapping>();
                                                    objTag = (from row in entities.Cont_tbl_FileDataTagsMapping
                                                              where row.FileID == item.ID
                                                              && row.IsActive == true
                                                              select row).ToList();

                                                    foreach (var Tagitem in objTag)
                                                    {
                                                        Cont_tbl_FileDataTagsMapping ObjFileTag = new Cont_tbl_FileDataTagsMapping
                                                        {
                                                            FileID = objContDoc.ID,
                                                            FileTag = Tagitem.FileTag,
                                                            IsActive = true,
                                                            CreatedBy = AuthenticationHelper.UserID,
                                                            CreatedOn = DateTime.Now
                                                        };
                                                        entities.Cont_tbl_FileDataTagsMapping.Add(ObjFileTag);
                                                        entities.SaveChanges();
                                                    }
                                                }
                                            }
                                            saveSuccess = true;
                                            if (saveSuccess)
                                            {
                                                ContractManagement.CreateAuditLog("C", ContractInstanceID, "Cont_tbl_FileData", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Contract Document(s) Uploaded", true, Convert.ToInt32(contractID));
                                                // BindContractDocuments_Paging();
                                            }

                                            /* if (saveSuccess)
                                             {
                                                 cvContractDocument.IsValid = false;
                                                 cvContractDocument.ErrorMessage = "Document(s) uploaded successfully";
                                                 vsContractDocument.CssClass = "alert alert-success";
                                             }
                                             else
                                             {
                                                 cvContractDocument.IsValid = false;
                                                 cvContractDocument.ErrorMessage = "Select Document(s) to Upload";
                                             }
                                             */

                                            #endregion
                                        }
                                    }
                                    //saveSuccess = ContractManagement.CreateUpdateContractLinking(newRecord);
                                    if (saveSuccess)
                                        totalRecordSaveCount++;
                                }

                            });
                        }
                        if (saveSuccess)
                        {
                            //ContractManagement.CreateAuditLog("C", contractID, "Cont_tbl_ContractLinking", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, totalRecordSaveCount + " Contract(s) Linked", true);

                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "scriptUnCheckAllGridCheckBox", "unCheckAll('<%=grdContractList_LinkContract.ClientID %>');", true);

                            cvLinkContract.IsValid = false;
                            cvLinkContract.ErrorMessage = totalRecordSaveCount + "\t \t Document(s) Linked Successfully";
                            vsLinkContract.CssClass = "alert alert-success";
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        #endregion

        
        public string ShowAuditLog_Timeline(List<Cont_SP_AuditLogs_All_Result> lstAuditLogs)
        {
            try
            {
                StringBuilder strTimeLineHTML = new StringBuilder();

                if (lstAuditLogs.Count > 0)
                {
                    strTimeLineHTML.Append("<ul class=\"timeline timeline-horizontal\">");

                    lstAuditLogs.ForEach(eachAuditLog =>
                    {
                        strTimeLineHTML.Append("<li class=\"timeline-item\">" +
                                "<div class=\"timeline-badge primary\"><i class=\"fa fa-check\"></i></div>" +
                                "<div class=\"timeline-panel\">" +
                                    "<div class='timeline-heading'>" +
                                       " <h4 class=\"timeline-title\">" + eachAuditLog.CreatedByUser + "</h4>" +
                                       " <p><small class=\"text-muted\"><i class=\"fa fa-clock-o\"></i>" + eachAuditLog.CreatedOn.ToString("dd-MM-yyyy HH:mm:ss tt") + "</small></p>" +
                                   " </div>" +
                                    "<div class=\"timeline-body\">" +
                                        "<p>" + eachAuditLog.Remark + "</p>" +
                                   " </div>" +
                                "</div>" +
                            "</li>");
                    });

                    strTimeLineHTML.Append("</ul>");

                }

                return strTimeLineHTML.ToString();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return string.Empty;
            }
        }

        public string ShowAuditLog_VerticalTimeline(List<Cont_SP_AuditLogs_All_Result> lstAuditLogs)
        {
            try
            {
                StringBuilder strTimeLineHTML = new StringBuilder();

                if (lstAuditLogs.Count > 0)
                {
                    strTimeLineHTML.Append("<ul class=\"timeline\">");

                    lstAuditLogs.ForEach(eachAuditLog =>
                    {
                        strTimeLineHTML.Append("<li class=\"timeline-item\">" +
                                "<div class=\"timeline-badge primary\"><i class=\"fa fa-check\"></i></div>" +
                                "<div class=\"timeline-panel\">" +
                                    "<div class='timeline-heading'>" +
                                       " <h4 class=\"timeline-title\">" + eachAuditLog.CreatedByUser + "</h4>" +
                                       " <p><small class=\"text-muted\"><i class=\"fa fa-clock-o\"></i>" + eachAuditLog.CreatedOn.ToString("dd-MM-yyyy HH:mm:ss tt") + "</small></p>" +
                                   " </div>" +
                                    "<div class=\"timeline-body\">" +
                                        "<p>" + eachAuditLog.Remark + "</p>" +
                                   " </div>" +
                                "</div>" +
                            "</li>");
                    });

                    strTimeLineHTML.Append("</ul>");

                }

                return strTimeLineHTML.ToString();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return string.Empty;
            }
        }

        public string ShowAuditLog_Centered_Clickable(List<Cont_SP_AuditLogs_All_Result> lstAuditLogs)
        {
            try
            {
                StringBuilder strTimeLineHTML = new StringBuilder();

                if (lstAuditLogs.Count > 0)
                {
                    var minDate = lstAuditLogs.Select(row => row.CreatedOn).Min();
                    var maxDate = lstAuditLogs.Select(row => row.CreatedOn).Max();

                    if (minDate != null && maxDate != null)
                    {
                        strTimeLineHTML.Append("<ul class=\"timeline\">");

                        int accordionCount = 0;
                        var startDate = maxDate;
                        do
                        {
                            DateTime MonthStartDate = ContractCommonMethods.GetFirstDayOfMonth(startDate);
                            DateTime MonthEndDate = ContractCommonMethods.GetLastDayOfMonth(startDate);

                            var auditLogsMonthWise = lstAuditLogs.Where(row => row.CreatedOn >= MonthStartDate && row.CreatedOn <= MonthEndDate).OrderByDescending(row => row.CreatedOn).ToList();

                            if (auditLogsMonthWise.Count > 0)
                            {
                                accordionCount++;

                                string accordionCollapseClass = "panel-collapse collapse ";//change by Ruchi on 12th sep 2018

                                string faIcon = "fa fa-minus";
                                if (accordionCount > 1)
                                {
                                    accordionCollapseClass = "panel-collapse collapse";
                                    faIcon = "fa fa-minus";
                                }

                                if (accordionCount % 2 == 0)
                                    strTimeLineHTML.Append("<li class=\"timeline-inverted\">");
                                else
                                    strTimeLineHTML.Append("<li>");

                                strTimeLineHTML.Append("<div class=\"timeline-badge\" onclick=\"showHideAuditLog(divCollapse" + accordionCount + ",i" + accordionCount + ")\"" +
                                    "data-toggle=\"tooltip\" data-placement=\"bottom\" ToolTip=\"View Detail\"><i id=\"i" + accordionCount + "\" class=\"" + faIcon + "\"></i></div>");

                                strTimeLineHTML.Append("<div class=\"timeline-panel\">");

                                strTimeLineHTML.Append("<div class=\"panel-heading clickable newProducts\" onclick=\"showHideAuditLog(divCollapse" + accordionCount + ",i" + accordionCount + ")\">" +
                                    "<a class=\"newProductsLink\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapse" + accordionCount + "\">" +
                                    "<h2 class=\"panel-title new-products-title\">" +
                                    CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(startDate.Month) + "-" + startDate.Year + "</h2>" +
                                    "</a>" +
                                "</div>");

                                strTimeLineHTML.Append("<div id=\"divCollapse" + accordionCount + "\" class=\"panel-body in\" style =\"max-height: 300px; overflow-y: auto; margin-top: 5px;\">" +
                                                        "<div id=\"collapse" + accordionCount + "\" class=" + accordionCollapseClass + "\">" +
                                                                "<ul class=newProductsList>");

                                auditLogsMonthWise.ForEach(eachAuditLog =>
                                {
                                    strTimeLineHTML.Append("<li>" +
                                         "<div class=\"timeline-body\">" +
                                          "<div class=\"col-md-3 pl0\">" +
                                                "<p><i class=\"fa fa-clock-o mlr5\"></i>" + eachAuditLog.CreatedOn.ToString("dd-MM-yyyy h:mm tt") + "</p>" +
                                            "</div>" +
                                             "<div class=\"col-md-2 pl0\">" +
                                                "<p class=\"timeline-title\">" + eachAuditLog.CreatedByUser + "</p>" +
                                             "</div>" +
                                            "<div class=\"col-md-7 pl0\">" +
                                                "<p>" + eachAuditLog.Remark + "</p>" +
                                            "</div>" +
                                         "</div>" +
                                   "</li>");
                                    strTimeLineHTML.Append("<div class=\"clearfix\"></div>");

                                    //<small class=\"text-muted\"></small>
                                });

                                strTimeLineHTML.Append("</ul></div>");
                                strTimeLineHTML.Append("</div>");

                                startDate = MonthStartDate.AddDays(-1);
                            }
                        } while (startDate >= minDate);

                        strTimeLineHTML.Append("</ul>");
                    }
                }

                return strTimeLineHTML.ToString();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return string.Empty;
            }
        }

        public string ShowAuditLog_Clickable(List<Cont_SP_AuditLogs_All_Result> lstAuditLogs)
        {
            try
            {
                StringBuilder strTimeLineHTML = new StringBuilder();

                if (lstAuditLogs.Count > 0)
                {
                    var minDate = lstAuditLogs.Select(row => row.CreatedOn).Min();
                    var maxDate = lstAuditLogs.Select(row => row.CreatedOn).Max();

                    if (minDate != null && maxDate != null)
                    {
                        strTimeLineHTML.Append("<div class=\"timeline\">" +
                                                "<div class=\"line text-muted\"></div>");

                        int accordionCount = 0;
                        var startDate = maxDate;
                        do
                        {
                            DateTime MonthStartDate = ContractCommonMethods.GetFirstDayOfMonth(startDate);
                            DateTime MonthEndDate = ContractCommonMethods.GetLastDayOfMonth(startDate);

                            var auditLogsMonthWise = lstAuditLogs.Where(row => row.CreatedOn >= MonthStartDate && row.CreatedOn <= MonthEndDate).OrderByDescending(row => row.CreatedOn).ToList();

                            if (auditLogsMonthWise.Count > 0)
                            {
                                accordionCount++;

                                string accordionCollapseClass = "panel-collapse collapse in";//added by Ruchi on 9th sep 2018
                                string faIcon = "fa fa-minus";

                                if (accordionCount > 1)
                                {
                                    accordionCollapseClass = "panel-collapse collapse in";
                                    faIcon = "fa fa-minus";
                                }

                                strTimeLineHTML.Append("<article class=\"panel panel-primary product-panel\" style=\" margin-top: 20px;\">");

                                strTimeLineHTML.Append("<div class=\"panel-heading icon\" onclick=\"showHideAuditLog(divCollapse" + accordionCount + ",i" + accordionCount + ")\">" +
                                    "<a class=\"newProductsLinkIcon\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapse" + accordionCount + "\">" +
                                    "<i id=\"i" + accordionCount + "\" class=\"" + faIcon + "\"></i></a>" +
                                "</div>");

                                strTimeLineHTML.Append("<div class=\"panel-heading clickable newProducts\" onclick=\"showHideAuditLog(divCollapse" + accordionCount + ",i" + accordionCount + ")\">" +
                                    "<a class=\"newProductsLink\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapse" + accordionCount + "\">" +
                                    "<h2 class=\"panel-title new-products-title\">" +
                                    CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(startDate.Month) + "-" + startDate.Year + "</h2>" +
                                    "</a>" +
                                "</div>");

                                strTimeLineHTML.Append("<div id=\"divCollapse" + accordionCount + "\" class=\"panel-body in\" style=\"max-height: 300px; overflow-y: auto; border: 1px solid #c7c7cc; margin-top: 5px; border-radius: 5px\">" +
                                                        "<div id=\"collapse" + accordionCount + "\" class=" + accordionCollapseClass + "\">" +
                                                                "<ul class=newProductsList>");

                                auditLogsMonthWise.ForEach(eachAuditLog =>
                                {
                                    strTimeLineHTML.Append("<li>" +
                                         "<div class=\"timeline-body\">" +
                                             "<div class=\"col-md-2\">" +
                                                "<h4 class=\"timeline-title\">" + eachAuditLog.CreatedByUser + "</h4>" +
                                             "</div>" +
                                            "<div class=\"col-md-3\">" +
                                                "<p><i class=\"fa fa-clock-o mlr5\"></i>" + eachAuditLog.CreatedOn.ToString("dd-MM-yyyy HH:mm:ss tt") + "</p>" +
                                            "</div>" +
                                            "<div class=\"col-md-7\">" +
                                                "<p>" + eachAuditLog.Remark + "</p>" +
                                            "</div>" +
                                         "</div>" +
                                   "</li>");
                                    strTimeLineHTML.Append("<div class=\"clearfix\"></div>");

                                    //<small class=\"text-muted\"></small>
                                });

                                strTimeLineHTML.Append("</ul></div>");
                                strTimeLineHTML.Append("</article>");

                                startDate = MonthStartDate.AddDays(-1);
                            }
                        } while (startDate >= minDate);

                        strTimeLineHTML.Append("</div>");
                    }
                }

                return strTimeLineHTML.ToString();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return string.Empty;
            }
        }


        protected void lnkBtnUploadDocument_Click(object sender, EventArgs e)
        {
            var contractID = CryptographyManagement.Encrypt(ViewState["ContractInstanceID"].ToString());
            var FlagID = 1;
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenUploadDocumentPopup('" + contractID + "','" + FlagID + "');", true);
        }

        protected void lnkBtnAddNewTaskDoc_Click(object sender, EventArgs e)
        {
            var contractID = CryptographyManagement.Encrypt(ViewState["ContractInstanceID"].ToString());
            var FlagID = 2;
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenUploadDocumentPopup('" + contractID + "','" + FlagID + "');", true);
        }

        
        protected void AddSteps(string[] items, System.Web.UI.WebControls.BulletedList ListName)
        {
            ListName.Items.Clear();
            ListName.Attributes["data-progtrckr-steps"] = items.Length.ToString();

            for (int i = 0; i < items.Length; i++)
            {
                ListName.Items.Add(new ListItem(items[i]));
            }
        }

        protected void SetProgress(long current, System.Web.UI.WebControls.BulletedList ListName)
        {
            for (int i = 0; i < ListName.Items.Count; i++)
            {
                ListName.Items[i].Attributes["class"] =
                    (i < current) ? "progtrckr-done" : (i == current) ? "progtrckr-current" : "progtrckr-todo";

                if (i == 3 && i > current)
                    ListName.Items[i].Attributes["class"] = "progtrckr-todo-closed";
            }
            if (current == 4)
                ListName.Items[Convert.ToInt32(current) - 1].Attributes["class"] = "progtrckr-closed";
        }

        public bool Save_ContractStatus(long contractID)
        {
            bool saveSuccess = false;
            try
            {
                #region Contract Status Transaction

                long newStatusID = 0;

                if (ViewState["ContractStatusID"] != null)
                {
                    long prevStatusID = Convert.ToInt64(ViewState["ContractStatusID"]);

                    if (prevStatusID != Convert.ToInt64(ddlContractStatus.SelectedValue))
                    {
                        newStatusID = Convert.ToInt64(ddlContractStatus.SelectedValue);
                    }
                }
                else
                {
                    newStatusID = Convert.ToInt64(ddlContractStatus.SelectedValue);
                }

                if (newStatusID != 0)
                {
                    Cont_tbl_ContractStatusTransaction newStatusRecord = new Cont_tbl_ContractStatusTransaction()
                    {
                        CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID),
                        ContractID = contractID,
                        StatusID = newStatusID,
                        StatusChangeOn = DateTime.Now,
                        IsActive = true,
                        CreatedBy = AuthenticationHelper.UserID,
                        CreatedOn = DateTime.Now,
                        UpdatedBy = AuthenticationHelper.UserID,
                        UpdatedOn = DateTime.Now,
                    };

                    saveSuccess = ContractManagement.CreateContractStatusTransaction(newStatusRecord);

                    if (saveSuccess)
                    {
                        ViewState["ContractStatusID"] = Convert.ToInt64(ddlContractStatus.SelectedValue);
                        ContractManagement.CreateAuditLog("C", contractID, "Cont_tbl_ContractStatusTransaction", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Contract Status Changed", true, Convert.ToInt32(contractID));
                    }
                }

                return saveSuccess;

                #endregion
            }
            catch (Exception ex)
            {
                return saveSuccess;
            }
        }

        protected void btnRenewContract_Click(object sender, EventArgs e)
        {
            bool renewSuccess = false;
            try
            {
                if (ViewState["ContractInstanceID"] != null)
                {
                    int customerID = -1;
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                    long oldContractID = Convert.ToInt64(ViewState["ContractInstanceID"]);
                    long newContractID = 0;

                    #region Contract Status Transaction - Renewed - Current Contract 

                    renewSuccess = Save_ContractStatus(oldContractID);

                    #endregion

                    if (renewSuccess)
                    {
                        renewSuccess = Save_ContractToContract_Renew(oldContractID, out newContractID);
                    }

                    if (renewSuccess)
                    {
                        cvRenewContract.IsValid = false;
                        cvRenewContract.ErrorMessage = "Contract Renewed Successfully";
                        vsRenewContract.CssClass = "alert alert-success";
                    }
                    else
                    {
                        cvRenewContract.IsValid = false;
                        cvRenewContract.ErrorMessage = "Something went wrong, Please try again";
                        vsRenewContract.CssClass = "alert alert-danger";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private bool Save_ContractToContract_Renew(long oldContractID, out long newContractID_Out)
        {
            bool saveSuccess = false;
            newContractID_Out = 0;
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                var oldContractRecord = ContractManagement.GetContractDetailsByContractID(customerID, oldContractID);

                if (oldContractRecord != null)
                {
                    long newContractID = 0;

                    #region Save New Contract Record

                    Cont_tbl_ContractInstance newContractRecord = new Cont_tbl_ContractInstance()
                    {
                        ContractType = oldContractRecord.ContractType,

                        CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID),
                        IsDeleted = false,

                        ContractNo = "Renewed-" + oldContractRecord.ContractNo,
                        ContractTitle = "Renewed-" + oldContractRecord.ContractTitle,
                        ContractDetailDesc = oldContractRecord.ContractDetailDesc,
                        CustomerBranchID = oldContractRecord.CustomerBranchID,
                        DepartmentID = oldContractRecord.DepartmentID,
                        ContractTypeID = oldContractRecord.ContractTypeID,
                        ContractSubTypeID = oldContractRecord.ContractSubTypeID,
                        //ProposalDate = oldContractRecord.ProposalDate,
                        //AgreementDate= oldContractRecord.AgreementDate,
                        EffectiveDate = oldContractRecord.EffectiveDate,
                        //ReviewDate = oldContractRecord.ReviewDate,
                        ExpirationDate = oldContractRecord.ExpirationDate,

                        NoticeTermNumber = oldContractRecord.NoticeTermNumber,
                        NoticeTermType = oldContractRecord.NoticeTermType,
                        PaymentTermID = oldContractRecord.PaymentTermID,
                        ContractAmt = oldContractRecord.ContractAmt,
                        AddNewClause = oldContractRecord.AddNewClause,
                        CreatedBy = AuthenticationHelper.UserID,
                        UpdatedBy = AuthenticationHelper.UserID
                    };

                    newContractID = ContractManagement.CreateContract(newContractRecord);

                    if (newContractID > 0)
                    {
                        ContractManagement.CreateAuditLog("C", oldContractID, "Cont_tbl_ContractInstance", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Contract Renewed and New Contract Created", true, Convert.ToInt32(newContractID));
                        ContractManagement.CreateAuditLog("C", newContractID, "Cont_tbl_ContractInstance", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Contract Created from Existing Contract", true, Convert.ToInt32(newContractID));
                        saveSuccess = true;
                    }

                    #endregion

                    if (saveSuccess)
                    {
                        newContractID_Out = newContractID;

                        #region Contract Mapping
                        Cont_tbl_ContractToContractMapping objContractMapping = new Cont_tbl_ContractToContractMapping()
                        {
                            OldContractID = oldContractID,
                            NewContractID = newContractID,
                            IsActive = true,
                            CreatedBy = AuthenticationHelper.UserID,
                            CreatedOn = DateTime.Now,

                            UpdatedBy = AuthenticationHelper.UserID,
                            UpdatedOn = DateTime.Now,
                        };

                        saveSuccess = ContractManagement.CreateUpdate_ContractToContractMapping(objContractMapping);

                        #endregion

                        #region Contract Status Transaction - Draft

                        Cont_tbl_ContractStatusTransaction newStatusRecord = new Cont_tbl_ContractStatusTransaction()
                        {
                            CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID),
                            ContractID = newContractID,
                            StatusID = Convert.ToInt64(1), //Draft
                            StatusChangeOn = DateTime.Now,
                            IsActive = true,
                            CreatedBy = AuthenticationHelper.UserID,
                            CreatedOn = DateTime.Now,
                            UpdatedBy = AuthenticationHelper.UserID,
                            UpdatedOn = DateTime.Now,
                        };

                        if (!ContractManagement.Exist_ContractStatusTransaction(newStatusRecord))
                            saveSuccess = ContractManagement.CreateContractStatusTransaction(newStatusRecord);

                        #endregion

                        #region Vendor Mapping

                        var lstVendorMapping = VendorDetails.GetVendorMapping(oldContractID);

                        if (lstVendorMapping.Count > 0)
                        {
                            List<Cont_tbl_VendorMapping> lstVendorMapping_ToSave = new List<Cont_tbl_VendorMapping>();

                            lstVendorMapping.ForEach(EachVendor =>
                            {
                                Cont_tbl_VendorMapping _vendorMappingRecord = new Cont_tbl_VendorMapping()
                                {
                                    ContractID = newContractID,
                                    VendorID = EachVendor,
                                    IsActive = true,
                                    CreatedBy = AuthenticationHelper.UserID,
                                    CreatedOn = DateTime.Now,
                                };

                                lstVendorMapping_ToSave.Add(_vendorMappingRecord);
                            });

                            saveSuccess = VendorDetails.CreateUpdate_VendorMapping(lstVendorMapping_ToSave);
                            if (saveSuccess)
                            {
                                ContractManagement.CreateAuditLog("C", newContractID, "Cont_tbl_VendorMapping", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Contract Vendor Mapping Created", true, Convert.ToInt32(newContractID));
                            }

                            //Refresh List
                            lstVendorMapping_ToSave.Clear();
                            lstVendorMapping_ToSave = null;

                            lstVendorMapping.Clear();
                            lstVendorMapping = null;
                        }

                        #endregion

                        #region Custom Field(s)

                        var existingCustomFields = ContractManagement.GetCustomsFieldsContractWise_All(Convert.ToInt32(AuthenticationHelper.CustomerID), oldContractID, oldContractRecord.ContractTypeID);

                        if (existingCustomFields.Count > 0)
                        {
                            List<Cont_tbl_CustomFieldValue> lstObjParameters = new List<Cont_tbl_CustomFieldValue>();

                            existingCustomFields.ForEach(EachCustomField =>
                            {
                                Cont_tbl_CustomFieldValue ObjParameter = new Cont_tbl_CustomFieldValue()
                                {
                                    ContractID = newContractID,

                                    LabelID = EachCustomField.LableID,
                                    LabelValue = EachCustomField.LabelValue,

                                    IsDeleted = false,
                                    CreatedBy = AuthenticationHelper.UserID,
                                    CreatedOn = DateTime.Now,
                                    UpdatedBy = AuthenticationHelper.UserID,
                                    UpdatedOn = DateTime.Now
                                };

                                lstObjParameters.Add(ObjParameter);
                            });

                            saveSuccess = ContractManagement.CreateUpdate_CustomsFields(lstObjParameters);

                            if (saveSuccess)
                            {
                                ContractManagement.CreateAuditLog("C", newContractID, "Cont_tbl_CustomFieldValue", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Contract Custom Parameter(s) Added", true, Convert.ToInt32(newContractID));
                            }
                        }

                        #endregion

                        #region User Assignment                           

                        var lstContractUserMapping = ContractManagement.GetContractUserAssignment_All(oldContractID);

                        if (lstContractUserMapping.Count > 0)
                        {
                            List<Cont_tbl_UserAssignment> lstUserAssignmentRecord_ToSave = new List<Cont_tbl_UserAssignment>();

                            lstContractUserMapping.ForEach(eachUser =>
                            {
                                Cont_tbl_UserAssignment newAssignment = new Cont_tbl_UserAssignment()
                                {
                                    AssignmentType = 1,
                                    ContractID = newContractID,

                                    UserID = Convert.ToInt32(eachUser.UserID),
                                    RoleID = Convert.ToInt32(eachUser.RoleID),

                                    IsActive = true,
                                    CreatedBy = AuthenticationHelper.UserID,
                                    UpdatedBy = AuthenticationHelper.UserID,
                                };

                                lstUserAssignmentRecord_ToSave.Add(newAssignment);
                            });

                            if (lstUserAssignmentRecord_ToSave.Count > 0)
                                saveSuccess = ContractManagement.CreateUpdate_ContractUserAssignments(lstUserAssignmentRecord_ToSave);

                            if (saveSuccess)
                            {
                                ContractManagement.CreateAuditLog("C", newContractID, "Cont_tbl_UserAssignment", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Contract Owner(s) Created", true, Convert.ToInt32(newContractID));
                            }

                            lstUserAssignmentRecord_ToSave.Clear();
                            lstUserAssignmentRecord_ToSave = null;
                        }

                        #endregion
                    }
                }
                return saveSuccess;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return saveSuccess;
            }
        }

        protected void btnApplyFilter_AuditLog_Click(object sender, EventArgs e)
        {
            try
            {
                BindContractAuditLogs();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        
        protected void grdContractHistory_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandArgument != null)
                {
                    if (e.CommandName.Equals("ViewContractPopup"))
                    {
                        long contractID = Convert.ToInt64(e.CommandArgument);

                        if (contractID != 0)
                        {
                            string HistoryFlag = " true";
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "OpenContractHistoryPopup(" + contractID + "," + HistoryFlag + ");", true);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdContractHistory_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (ViewState["ContractInstanceID"] != null)
                {
                    int customerID = -1;
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                    grdContractHistory.PageIndex = e.NewPageIndex;

                    BindContractHistory(customerID, Convert.ToInt64(ViewState["ContractInstanceID"]));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

        }

        protected void grdTaskContractDocuments_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
         
        }

        protected void lnkContractTransaction_Click(object sender, EventArgs e)
        {
            ViewState["PageLink"] = "ContractLink";
            showHideContractSummaryTabTopButtons(false);

            int newContractID = -1;
            if (ViewState["ContractInstanceID"] != null)
            {
                newContractID = Convert.ToInt32(ViewState["ContractInstanceID"]);
            }
            RoleId = getRoleID(Convert.ToInt32(newContractID), AuthenticationHelper.UserID);
            ContractTemplateInstanceId = Convert.ToInt32(newContractID);
            UId = Convert.ToInt32(AuthenticationHelper.UserID);
            CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            
            liContractSummary.Attributes.Add("class", "");
            liContractDetails.Attributes.Add("class", "");
            liDocument.Attributes.Add("class", "");
            liTask.Attributes.Add("class", "");
            liAuditLog.Attributes.Add("class", "");
            liContractTransaction.Attributes.Add("class", "active");

            MainView.ActiveViewIndex = 5;


        }
    }
}