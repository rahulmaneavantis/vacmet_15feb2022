﻿<%@ Page Language="C#" AutoEventWireup="true" Culture="en-GB" CodeBehind="ContractDetailsPage.aspx.cs" EnableEventValidation="false" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.aspxPages.ContractDetailsPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" style="background: #f7f7f7;width:99%;">
<head runat="server">
    <title>Contract Detail</title>
    <!-- Bootstrap CSS -->
    <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- bootstrap theme -->
    <link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />
        <!-- font icon -->
    <link href="~/NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <!--external css-->
    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/contract_custom_style.css" rel="stylesheet" />
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="/Newjs/jquery.js"></script>
    <script type="text/javascript" src="/Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="/Newjs/bootstrap.min.js"></script>


    <link href="~/NewCSS/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
    <script src="/Newjs/bootstrap-multiselect.js" type="text/javascript"></script>

    <script src="/Newjs/bootstrap-tagsinput.js"></script>
    <link href="/NewCSS/bootstrap-tagsinput.css" rel="stylesheet" />

    <link href="/NewCSS/timeline.css" rel="stylesheet" />
    <!-- nice scroll -->
    <script type="text/javascript" src="/Newjs/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="/Newjs/jquery.nicescroll.js"></script>

    <script src="/Newjs/tag-scrolling.js" type="text/javascript"></script>
    <link href="/NewCSS/tag-scrolling.css" rel="stylesheet" />

    <script src="https://avacdn.azureedge.net/newjs/jquery.mentionable.js"></script>
    <link href="https://avacdn.azureedge.net/newcss/jquery.mentionable.css" rel="stylesheet" />

     <link href="/NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="/NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="/NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="/NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />
    <script type="text/javascript" src="../../Newjs/kendo.all.min.js"></script>

    <style type="text/css">
        #ShowDatadiv > div {
            clear: both;
        }

        .componentWraper {
            margin: 18px;
            position: relative;
            border: 1.2px solid #bdbdbd;
            border-radius: 12px;
            padding: 20px;
            margin-top: 8px;
        }

            .componentWraper .componentTitle {
                position: absolute;
                top: -10px;
                background: #fff;
                padding: 0 10px;
                font-weight: 600;
                margin-top: -2px;
                font-size: 17px;
                color: #78909c;
            }

        hr {
            margin-top: 4px;
            margin-bottom: 10px;
            border: 0;
            border-top: 1px solid #eee;
        }

        .auditlog h2.panel-title.new-products-title {
            text-transform: uppercase;
            font-weight: bold;
            text-align: center;
            font-size: 20px;
        }

        div#ui-datepicker-div {
            z-index: 100 !important;
        }

    ul.multiselect-container.dropdown-menu
        {
           width: 100%;
           height: 218px;
           /* overflow-y: auto; */
           overflow-x: hidden;
        }
    .glyphicon-remove-circle
        {
           top: 1px;
           display: none;
           font-style: normal;
           font-weight: 400;
           line-height: 1;
           -webkit-font-smoothing: antialiased;
        }

        .multiselect-container>li
        {
        /*width: 100%;*/
          padding: 0;
          margin-right: -112px;
        }

        /*.input-group-btn {
            position: relative;
            white-space: nowrap;
            display: none;
        }*/
        b.caret {
            margin-top: 10px;
        }
        .chosen-container-single .chosen-single div b {
            margin-top: 3px;
            display: block;
            width: 100%;
            height: 100%;
            background: url(WebResource.axd?d=vcanw1OGyEXjITnW_Lw6o1Ipyyl_7xqXXsF4l85T_LM075DfB4Sa1X81JR-ERGJWebwjBZ50cFK6Pp0E393ZYqyZGOYHT_-VfdsqCmyDCfaRmJRJ_t0w8jjfIDakqF4U1oofqFWh029b9t4iZjam4LmYmixIQZtujQLO7ryIDAI1&t=637298087674161687) no-repeat 0px 6px;
        }
        .bootstrap-tagsinput .tag [data-role="remove"]:after {
            content: "";
            padding: 0px 2px;
        }

        .bootstrap-tagsinput {
            /*border: none;
            box-shadow: none;*/
        }

        .form-group {
            margin-bottom: 10px;
        }
        .btn-group
        {
            width:100%
        }
    </style>

    <style>
        .grdTaskContractDocuments .grdTaskContractDocuments_PageIndexChanging
        {
            text-align: right;
        }
        .tag .label {
            font-size: 100%;
        }

        span input[type=checkbox]:checked {
            color: #007aff;
            border: 1px solid;
            border-color: #007aff;
            background: none;
        }

        .label-info, .label-info-selected {
            font-size: 100%;
        }

            .label-info:active, .label-info:focus, .label-info:hover {
                color: #007aff;
                border: 1px solid;
                border-color: #007aff;
                background: 0 0;
            }

        .label-info-selected {
            color: #007aff;
            border: 1px solid;
            border-color: #007aff;
            background: 0 0;
        }

        textarea {
            resize: none;
            font-size: 13px;
            padding: 10px;
            height: 38px;
            min-height: 38px;
            max-height: 150px;
            width: 100%;
            box-sizing: border-box;
            overflow-y: auto;
        }

        button.multiselect.dropdown-toggle.btn.btn-default {
            text-align: left;
            border: 1px solid #c7c7cc;
            width: 100%;
        }
    </style>

    <script type="text/javascript">
         function checkUncheckRowField() {

            var selectedRowCount = 0;
            //Get the reference of GridView
            var grid = document.getElementById("<%=grdContractDocuments.ClientID %>");

            //Get all input elements in Gridview
            var inputList = grid.getElementsByTagName("input");

            //The First element is the Header Checkbox
            var headerCheckBox = inputList[0];
            var checked = true;
            for (var i = 0; i < inputList.length; i++) {
                //Based on all or none checkboxes are checked check/uncheck Header Checkbox                
                if (inputList[i].type == "checkbox" && inputList[i] != headerCheckBox) {
                    if (!inputList[i].checked) {
                        checked = false;
                    }
                    if (inputList[i].checked) {
                        selectedRowCount++;
                    }
                }
            }

            headerCheckBox.checked = checked;

        }
		 function checkAllField(chkHeader) {
            var selectedRowCount = 0;
            var grid = document.getElementById("<%=grdContractDocuments.ClientID %>");
            if (grid != null) {
                if (chkHeader != null) {
                    //Get all input elements in Gridview
                    var inputList = grid.getElementsByTagName("input");

                    for (var i = 1; i < inputList.length; i++) {
                        if (inputList[i].type == "checkbox") {
                            if (chkHeader.checked) {
                                inputList[i].checked = true;
                                selectedRowCount++;
                            }
                            else if (!chkHeader.checked)
                                inputList[i].checked = false;
                        }
                    }
                }
            }
        }
        function CloseMe() {
            window.parent.ClosePopVendor();
        }
        function EmptyAuditlog() {
        }
        function msgforUnshareupdate() {
            alert("Contract unshared Successfully.");
        }
        function msgforuserupdate() {
            alert("Contract shared to user(s) Successfully.");
        }
        function msgforusernotselect() {
            alert("Please select user(s) to share contract.");
        }
        function showHideAuditLog(divID, iID) {
           
            if ($(iID).attr('class').indexOf('fa fa-plus') > -1) {
                $(iID).attr("class", "fa fa-minus");
                $(divID).collapse('toggle');
            } else if ($(iID).attr('class').indexOf('fa fa-minus') > -1) {
                $(iID).attr("class", "fa fa-plus");
                $(divID).collapse('toggle');
            }
        }
        
        jQuery(window).load(function () {
            $('#updateProgress').hide();
        });


         function submitData() {
          
            var SelectedValue = new Array();
            var inps = document.getElementsByName('mentioned_id[]');
            for (var i = 0; i < inps.length; i++) {
                if (inps[i].disabled != true) {
                    var inp = inps[i];
                    SelectedValue.push(inp.value);
                }
            }

            if (SelectedValue.length>0) 
            {
    
                var data = new FormData();
                var files = $("#file-input").get(0).files;
                if (files.length > 0) {
                    data.append("UploadedImage", files[0]);
                }
                var commet = document.getElementById("TxtComments").value;
           
                data.append('Uids', SelectedValue);
                data.append('Comments', commet);
                data.append('UserId', <% =UId%>);
                data.append('CustId', <% =CustId%>);
                data.append('ContractID', <% =contID%>);

                $.ajax({
                    url: '<% =KendoPath%>/data/ContractCommentWithUploadFile',
                    type: 'post',
                    data: data,
                    contentType: false,
                    processData: false,
                    success: function(response){
                        GetCommentDetails();
                        $('#TxtComments').val('');
                        setTimeout(function () {
                            GetCommentDetails() // this will run after every 5 seconds
                        }, 1000);
                    },
                });
            }
            else
            {
                alert('Please Select At least One User');
            }

         }
        function ViewDiv(fileID,message_id)
        {
            debugger;
            $.ajax({
                type: "GET",
                url: '<% =KendoPath%>/Data/GetContractCommentDetail?MsgID='+fileID+'&message_id='+message_id,
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                crossDomain: true,
                success: function (result) 
                {
                    debugger;
                    $('#DocumentPriview').modal('show');
                    $('#docPriview').attr('src', "/docviewer.aspx?docurl=" + result[0].Message);
                },
                error: function (response) {
                    alert('Document not available');
                }
            });
        }
            
            
        

      <%--  function DownloadDiv(fileID,message_id)
        {
            debugger;
            $.ajax({
                type: "GET",
                url: '<% =KendoPath%>/Data/GetContractCommentDetail?MsgID='+fileID+'&message_id='+message_id,
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                crossDomain: true,
                success: function (result) 
                {
                    debugger;
                    window.location.href = '<% =KendoPath%>/ExportReport/GetContractCommentedFile?userpath=' + result[0].Message + '';                    
                },
                error: function (response) {
                    alert('Document not available');
                }
            });
            return false;
        }
                success: function (result) 
                {
                    debugger;
                    $('#DocumentPriview').modal('show');
                    $('#docPriview').attr('src', "/docviewer.aspx?docurl=" + result[0].Message);
                },
                error: function (response) {
                    alert('Document not available');
                }
            });
        } --%>
            
            
        

        function DownloadDiv(fileID,message_id)
        {
            debugger;
            $.ajax({
                type: "GET",
                url: '<% =KendoPath%>/Data/GetContractCommentDetail?MsgID='+fileID+'&message_id='+message_id,
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                crossDomain: true,
                success: function (result) 
                {
                    debugger;
                    window.location.href = '<% =KendoPath%>/ExportReport/GetContractCommentedFile?userpath=' + result[0].Message + '';                    
                },
                error: function (response) {
                    alert('Document not available');
                }
            });
            return false;
        }

        function showDiv(pageid) {
           
            document.getElementById(pageid + "_div").style.display = "block";
            document.getElementById(pageid + "_txt").value = "";
        }

        function alertselectedFilename(pageid)
        {       
            var thefile = document.getElementById(pageid + "_file");
            var createid="#"+pageid;
            $(createid + '_selectedfile').text(thefile.files.item(0).name);
        }

         function submitDiv(pageid,touserid,userid) {
            var valuetext = document.getElementById(pageid + "_txt").value;
          
            if (valuetext != "") 
            {
                var checktouserid = touserid;

                var checkuserid=<% =UId%>;
                if (touserid == checkuserid) {
                    checktouserid=userid
                }
                var data = new FormData();

                var chkfile = "#"+ pageid + "_file";

                var files = $(chkfile).get(0).files;
                if (files.length > 0) {
                    data.append("UploadedImage", files[0]);
                }
                var commet = document.getElementById("TxtComments").value;
          
                data.append('MsgID', pageid);
                data.append('Comments', valuetext);
                data.append('UserId', <% =UId%>);
                data.append('CustId', <% =CustId%>);
                data.append('ContractID', <% =contID%>);
                data.append('touser_id', checktouserid);
                $.ajax({
                    url: '<% =KendoPath%>/data/PostSubContractCommentdata',
                    type: 'post',
                    data: data,
                    contentType: false,
                    processData: false,
                    success: function(response){
                        GetCommentDetails();
                        $('#TxtComments').val('');
                        setTimeout(function () {
                            GetCommentDetails() // this will run after every 5 seconds
                        }, 1000);
                    },
                });
            }
            else {
            }

        }
        function CloseDiv(pageid) {

            document.getElementById(pageid + "_div").style.display = "none";
        }


           function GetCommentDetails() {

            $.ajax({
                type: "GET",
                url: '<% =KendoPath%>/data/GetContractCommentWithUploadFile?ContractId=<% =contID%>&CustomerID=<% =CustId%>&userID=<% =UId%>',
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                crossDomain: true,
                success: function (result) {
                  
                    var MainCommnetId = new Array();
                    for (var i = 0; i < result.length; i++) {
                        if (result[i].message_id == 0) {
                            MainCommnetId.push([result[i].contractID, result[i].comment, result[i].user_id, result[i].created_at, result[i]._id, result[i].user_name,result[i].isfile,result[i].filepath,result[i].touser_id,result[i].message_id]);
                        }
                    }

                    var customers = new Array();
                    for (var i = 0; i < MainCommnetId.length; i++) 
                    {
                        if (MainCommnetId[i][6] == 1) 
                        {
                            customers.push('<div style="font-family: Roboto;max-width:90%;padding: 4px 10px 4px 5px;border: 1px solid #e4d8d8;width: auto;min-width: 234px;border-radius: 6px;margin-top: 12px;">' + MainCommnetId[i][1] + '</div></br > <div style="float: left;font-size: 12px;margin-top:-1%">' + MainCommnetId[i][5] + ',' + new Date(new Date(MainCommnetId[i][3])).toLocaleString()
                                                    + '<a style="font-size: 12px;margin-left: 8px;color:blue;cursor: pointer;" onclick=showDiv("' + MainCommnetId[i][4] + '");>Comments</a><a style="font-size: 12px;margin-left: 8px;color:blue;cursor: pointer;" onclick=ViewDiv("' + MainCommnetId[i][4] + '","' + MainCommnetId[i][9] + '");>View</a><a style="font-size: 12px;margin-left: 8px;color:blue;cursor: pointer;" onclick=DownloadDiv("' + MainCommnetId[i][4] + '","' + MainCommnetId[i][9] + '");>Download</a></div><div style="display:none;" id=' + MainCommnetId[i][4] + '_div></br><textarea type="text" style="font-family: Roboto;max-width: 45%;padding: 4px 10px 4px 5px;border: 1px solid #e4d8d8;width: auto;min-width: 60%;border-radius: 6px;min-height: 66px;" name=txtid id=' + MainCommnetId[i][4] + '_txt></textarea><label for=' + MainCommnetId[i][4] + '_file style="margin-left: 6px;"><a style="cursor: pointer; font-size: x-large;"><i class="fa fa-paperclip" data-toggle="tooltip" title="Attach a file to the comment"></i></a></label><input id=' + MainCommnetId[i][4] + '_file type="file" onchange=alertselectedFilename("' + MainCommnetId[i][4] + '") style="padding-top: 10px;display:none;" /> <label id=' + MainCommnetId[i][4] + '_selectedfile style="color: black;margin-left: 32px;"></label></br>&nbsp;&nbsp;&nbsp;<a style="cursor: pointer;color: blue;" onclick=submitDiv("' + MainCommnetId[i][4] + '","' + MainCommnetId[i][8] + '","' + MainCommnetId[i][2] + '");>submit</a>&nbsp;&nbsp;&nbsp;<a style="cursor: pointer;color: blue;" onclick=CloseDiv("' + MainCommnetId[i][4] + '");>Closed</a></div></br>');                        
                        }
                        else
                        {
                            customers.push('<div style="font-family: Roboto;max-width:90%;padding: 4px 10px 4px 5px;border: 1px solid #e4d8d8;width: auto;min-width: 234px;border-radius: 6px;margin-top: 12px;">' + MainCommnetId[i][1] + '</div></br > <div style="float: left;font-size: 12px;margin-top:-1%">' + MainCommnetId[i][5] + ',' + new Date(new Date(MainCommnetId[i][3])).toLocaleString()
                                + '<a style="font-size: 12px;margin-left: 8px;color:blue;cursor: pointer;" onclick=showDiv("' + MainCommnetId[i][4] + '");>Comments</a></div><div style="display:none;" id=' + MainCommnetId[i][4] + '_div></br><textarea type="text" style="font-family: Roboto;max-width: 45%;padding: 4px 10px 4px 5px;border: 1px solid #e4d8d8;width: auto;min-width: 60%;border-radius: 6px;min-height: 66px;" name=txtid id=' + MainCommnetId[i][4] + '_txt></textarea><label for=' + MainCommnetId[i][4] + '_file style="margin-left: 6px;"><a style="cursor: pointer; font-size: x-large;"><i class="fa fa-paperclip" data-toggle="tooltip" title="Attach a file to the comment"></i></a></label><input id=' + MainCommnetId[i][4] + '_file type="file" onchange=alertselectedFilename("' + MainCommnetId[i][4] + '") style="padding-top: 10px;display:none;" /> <label id=' + MainCommnetId[i][4] + '_selectedfile style="color: black;margin-left: 32px;"></label></br>&nbsp;&nbsp;&nbsp;<a style="cursor: pointer;color: blue;" onclick=submitDiv("' + MainCommnetId[i][4] + '","' + MainCommnetId[i][8] + '","' + MainCommnetId[i][2] + '");>submit</a>&nbsp;&nbsp;&nbsp;<a style="cursor: pointer;color: blue;" onclick=CloseDiv("' + MainCommnetId[i][4] + '");>Closed</a></div></br>');
                        }
                        for (var k = 0; k < result.length; k++) {
                            if (result[k].message_id == MainCommnetId[i][4]) 
                            {
                                if (result[k].isfile == 1) 
                                {
                                    customers.push('<div style="font-family: Roboto;background: #eaeaea;float: left;max-width:60%;padding: 4px 10px 4px 5px;border: 1px solid #e4d8d8;width: auto;min-width: 90%;border-radius: 6px;margin-right:10px;margin-top: 12px;">' + result[k].comment + '</div></br></br><div style="float: left;font-size: 12px;margin-right:10px">' + result[k].user_name + ',' + new Date(new Date(result[k].created_at)).toLocaleString() + '<a style="font-size: 12px;margin-left: 8px;color:blue;cursor: pointer;" onclick=ViewDiv("' + result[k]._id + '","' + result[k].message_id + '");>View</a><a style="font-size: 12px;margin-left: 8px;color:blue;cursor: pointer;" onclick=DownloadDiv("' + result[k]._id + '","' + result[k].message_id + '");>Download</a></div></br>');
                                }
                                else
                                {
                                    customers.push('<div style="font-family: Roboto;background: #eaeaea;float: left;max-width:60%;padding: 4px 10px 4px 5px;border: 1px solid #e4d8d8;width: auto;min-width: 90%;border-radius: 6px;margin-right:10px;margin-top: 12px;">' + result[k].comment + '</div></br></br><div style="float: left;font-size: 12px;margin-right:10px">' + result[k].user_name + ',' + new Date(new Date(result[k].created_at)).toLocaleString() + '</div></br>');
                                }
                            }
                        }
                    }
                    $("#ShowDatadiv").load();
                    $('#ShowDatadiv').html('');
                    $('#ShowDatadiv').html('<div style="min-height:45px;padding-left: 10px;padding-top: 10px;">' + customers.join('</div><div style="padding-left: 10px;padding-bottom: 18px;clear: both !important;padding-top: 5px;">') + '</div>');

                },
                error: function (response) {

                }
            });

        }


        
        $(document).ready(function () {
            GetCommentDetails();
            $('#TxtComments').mentionable(                
                '<% =KendoPath%>/Data/GetContractUSerDetailbySearch?customerID=<% =CustId%>&ContractId=<% =contID%>', { parameterName: "search" }
                //'<% =KendoPath%>/Data/GetUSerDetailbySearch?customerID=<% =CustId%>', { parameterName: "search" }                
                );
            
            $('[data-toggle="popover"]').popover();
            FetchUSerDetail();
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(InitializeRequest);
            prm.add_endRequest(EndRequest);

           
            BindControls();
            binddropdown();
            if ($("#<%=tbxBranch.ClientID %>") != null) {
                $("#<%=tbxBranch.ClientID %>").unbind('click');

                $("#<%=tbxBranch.ClientID %>").click(function () {
                    $("#divBranches").toggle("blind", null, 500, function () { });
                });
            }
            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            });

            $(document).tooltip({ selector: '[data-toggle="tooltip"]' });

            $("input[name='rbContractEndType']").change(function () {
                switch ($(this).val()) {
                    case 'E':
                        $('#divExpirationDate').show();
                        $('#divDuration').hide();
                        break;
                    case 'D':
                        $('#divDuration').show();
                        $('#divExpirationDate').hide();
                        break;
                }
            });

            applyCSSDate();

            $("html").getNiceScroll().resize();
            //ddlVendorChange();
            //ddlDepartmentChange();
            //ddlContractTypeChange();
            //ddlContractSubTypeChange();

            //ddlCustomFieldChange();
            //ddlTaskUserChange();

            $('#updateProgress').hide();

            //$('i.glyphicon.glyphicon-search').removeClass('glyphicon glyphicon-search').addClass('fa fa-search color-black');
            //$('i.glyphicon glyphicon-remove-circle').removeClass('glyphicon glyphicon-remove-circle').addClass(' fa fa-remove');

            //$('i.glyphicon.glyphicon-search').removeClass('glyphicon glyphicon-search').addClass('fa fa-search color-black');
            //$('i.glyphicon.glyphicon-remove').removeClass('glyphicon glyphicon-remove').addClass('fa fa-remove');


            $('#txtFileTags').tagsinput({});
            $('#lblFileTags').tagsinput({});

            $('input[id*=lstBoxFileTags]').hide();

            $('textarea').on('input', function () {

                if (this.scrollHeight <= 150)
                    $(this).outerHeight(38).outerHeight(this.scrollHeight);

                if (this.scrollHeight >= 150)
                    $(this).outerHeight(150);
            });
        });

        function scrollUp() {
            $('html, body').animate({ scrollTop: '0px' }, 800);
        }

        function scrollDown() {
            $('html, body').animate({ scrollTop: $elem.height() }, 800);
        }

        function scrollUpPage() {
            $("#divMainView").animate({ scrollTop: 0 }, 'slow');
        }

        function hide(object) {
            if (object != null)
                object.style.display = "none";
        }

        function show(object) {
            if (object != null)
                object.style.display = "block";
        }

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }

        $('.btn-minimize').click(function () {
            var s1 = $(this).find('i');
            if ($(this).hasClass('collapsed')) {
                $(s1).removeClass('fa-chevron-down');
                $(s1).addClass('fa-chevron-up');
            } else {
                $(s1).removeClass('fa-chevron-up');
                $(s1).addClass('fa-chevron-down');
            }
        });

        function btnminimize(obj) {
            var s1 = $(obj).find('i');
            if ($(obj).hasClass('collapsed')) {

                $(s1).removeClass('fa-chevron-up');
                $(s1).addClass('fa-chevron-down');
            } else {
                $(s1).removeClass('fa-chevron-down');
                $(s1).addClass('fa-chevron-up');
            }
        }

        function InitializeRequest(sender, args) { }
        function EndRequest(sender, args) { BindControls(); binddropdown(); }

        function binddropdown() {
            $(function () {
                $('i.glyphicon.glyphicon-search').removeClass('glyphicon glyphicon-search').addClass('fa fa-search color-black');
                $('i.glyphicon glyphicon-remove-circle').removeClass('glyphicon glyphicon-remove-circle').addClass(' fa fa-remove');
            });
        }

        function BindControls() {
            var startDate = new Date();
            $(function () {
                $('input[id*=txbmilestoneduedate]').datepicker({
                    dateFormat: 'dd-mm-yy',
                    //maxDate: startDate,
                    numberOfMonths: 1,
                    changeMonth: true,
                    changeYear: true,
                });

                //$('input[id*=Tbxstartdate]').datepicker({
                //    dateFormat: 'dd-mm-yy',
                //    //maxDate: startDate,
                //    numberOfMonths: 1,
                //    changeMonth: true,
                //    changeYear: true,
                //});
                
                //$('input[id*=Tbxenddate]').datepicker({
                //    dateFormat: 'dd-mm-yy',
                //    //maxDate: startDate,
                //    numberOfMonths: 1,
                //    changeMonth: true,
                //    changeYear: true,
                //});

                $('input[id*=txtProposalDate]').datepicker({
                    dateFormat: 'dd-mm-yy',
                    //maxDate: startDate,
                    numberOfMonths: 1,
                    changeMonth: true,
                    changeYear: true,
                });

                $('input[id*=txtAgreementDate]').datepicker({
                    dateFormat: 'dd-mm-yy',
                    //maxDate: startDate,
                    numberOfMonths: 1,
                    changeMonth: true,
                    changeYear: true,
                });

                $('input[id*=txtEffectiveDate]').datepicker({
                    dateFormat: 'dd-mm-yy',
                    numberOfMonths: 1,
                    changeMonth: true,
                    changeYear: true,
                });

                $('input[id*=txtReviewDate]').datepicker({
                    dateFormat: 'dd-mm-yy',
                    numberOfMonths: 1,
                    changeMonth: true,
                    changeYear: true,
                });

                $('input[id*=txtExpirationDate]').datepicker({
                    dateFormat: 'dd-mm-yy',
                    //minDate: startDate,
                    numberOfMonths: 1,
                    changeMonth: true,
                    changeYear: true,
                });

                $('input[id*=txtlockinperiodDate]').datepicker({
                    dateFormat: 'dd-mm-yy',
                    //minDate: startDate,
                    numberOfMonths: 1,
                    changeMonth: true,
                    changeYear: true,
                });                

                $('input[id*=tbxTaskDueDate]').datepicker({
                    dateFormat: 'dd-mm-yy',
                    minDate: startDate,
                    numberOfMonths: 1,
                    changeMonth: true,
                    changeYear: true,
                });

                $('input[id*=tbxTaskAssignDate]').datepicker({
                    dateFormat: 'dd-mm-yy',
                    minDate: startDate,
                    numberOfMonths: 1,
                    changeMonth: true,
                    changeYear: true,
                });

                $('input[id*=txtFromDate_AuditLog]').datepicker({
                    dateFormat: 'dd-mm-yy',
                    numberOfMonths: 1,
                    changeMonth: true,
                    changeYear: true,
                });

                $('input[id*=txtToDate_AuditLog]').datepicker({
                    dateFormat: 'dd-mm-yy',
                    numberOfMonths: 1,
                    changeMonth: true,
                    changeYear: true,
                });
            });

            $(function () {
                $('[id*=lstBoxVendor]').multiselect({
                    includeSelectAllOption: true,
                    numberDisplayed: 2,
                    buttonWidth: '100%',
                    enableCaseInsensitiveFiltering: true,
                    filterPlaceholder: 'Type to Search for Vendor..',
                    nSelectedText: ' - Vendor(s) selected',
                   enableFiltering: true,
                });

                $('[id*=ddlPaymentTerm1]').multiselect({
                    includeSelectAllOption: true,
                    numberDisplayed: 2,
                    buttonWidth: '100%',
                    enableCaseInsensitiveFiltering: true,
                    filterPlaceholder: 'Type to Search for Payment Term..',
                    nSelectedText: ' - Payment Term(s) selected',
                    enableFiltering: true,
                });                

                $('[id*=lstBoxOwner]').multiselect({
                    includeSelectAllOption: true,
                    numberDisplayed: 2,
                    buttonWidth: '100%',
                    enableCaseInsensitiveFiltering: true,
                    filterPlaceholder: 'Type to Search for User..',
                    nSelectedText: ' - Owner(s) selected',
                });

                $('[id*=lstBoxApprover]').multiselect({
                    includeSelectAllOption: true,
                    numberDisplayed: 2,
                    buttonWidth: '100%',
                    enableCaseInsensitiveFiltering: true,
                    filterPlaceholder: 'Type to Search for User..',
                    nSelectedText: ' - Approver(s) selected',
                });

                $('[id*=lstBoxTaskUser]').multiselect({
                    includeSelectAllOption: true,
                    numberDisplayed: 2,
                    buttonWidth: '100%',
                    enableCaseInsensitiveFiltering: true,
                    filterPlaceholder: 'Type to Search for User..',
                    nSelectedText: ' - User(s) selected',
                });
                $('[id*=lstBoxUsers]').multiselect({
                    includeSelectAllOption: true,
                    numberDisplayed: 2,
                    buttonWidth: '100%',
                    enableCaseInsensitiveFiltering: true,
                    filterPlaceholder: 'Type to Search for User..',
                    nSelectedText: ' - User(s) selected',
                });
              
            });

            $(function () {
                $('input[id*=tbxPaymentDate]').datepicker(
                    {
                        changeMonth: true,
                        yearRange: "2020:2020",
                        showButtonPanel: true,
                        dateFormat: 'dd MM',
                        beforeShow: function (el, inst) {
                            setTimeout(function () {
                                inst.dpDiv.find('.ui-datepicker-year').hide();
                            }, 0);
                        },
                    }).focus(function () {
                       
                        $(".ui-datepicker-year").hide();
                    }).change(function () {
                        
                    });
            });

            $(function () {
                $('input[id*=tbxFromPaymentDate]').datepicker(
                    {
                        changeMonth: true,
                        yearRange: "2020:2020",
                        showButtonPanel: true,
                        dateFormat: 'dd MM',
                        beforeShow: function (el, inst) {
                            setTimeout(function () {
                                inst.dpDiv.find('.ui-datepicker-year').hide();
                            }, 0);
                        },
                    }).focus(function () {
                       // alert(12);
                        $(".ui-datepicker-year").hide();
                    }).change(function () {
                       
                        document.getElementById('<%= dtbutton.ClientID %>').click();
                        //alert(1);

                    });
            });
            $(function () {
                $('input[id*=tbxToPaymentDate]').datepicker(
                    {
                        changeMonth: true,
                        yearRange: "2020:2020",
                        showButtonPanel: true,
                        dateFormat: 'dd MM',
                        beforeShow: function (el, inst) {
                            setTimeout(function () {
                                inst.dpDiv.find('.ui-datepicker-year').hide();
                            }, 0);
                        },
                    }).focus(function () {
                        $(".ui-datepicker-year").hide();
                    });
            });
            
        }

        function bindMultiSelect() {
            $('[id*=lstBoxTaskUser]').multiselect({
                includeSelectAllOption: true,
                numberDisplayed: 2,
                buttonWidth: '100%',
                enableCaseInsensitiveFiltering: true,
                filterPlaceholder: 'Type to Search for User..',
                nSelectedText: ' - User(s) selected',
            });
        }
        function hideDivBranch() {
            $('#divBranches').hide("blind", null, 0, function () { });
        }

        function applyCSSDate() {
            $('#<%= txbmilestoneduedate.ClientID %>').removeClass();
            $('#<%= txbmilestoneduedate.ClientID %>').addClass('form-control');

            <%-- $('#<%= Tbxstartdate.ClientID %>').removeClass();
            $('#<%= Tbxstartdate.ClientID %>').addClass('form-control');

             $('#<%= Tbxenddate.ClientID %>').removeClass();
            $('#<%= Tbxenddate.ClientID %>').addClass('form-control');--%>

            $('#<%= txtProposalDate.ClientID %>').removeClass();
            $('#<%= txtProposalDate.ClientID %>').addClass('form-control');

            $('#<%= txtAgreementDate.ClientID %>').removeClass();
            $('#<%= txtAgreementDate.ClientID %>').addClass('form-control');

            $('#<%= txtEffectiveDate.ClientID %>').removeClass();
            $('#<%= txtEffectiveDate.ClientID %>').addClass('form-control');

            $('#<%= txtReviewDate.ClientID %>').removeClass();
            $('#<%= txtReviewDate.ClientID %>').addClass('form-control');

            $('#<%= txtExpirationDate.ClientID %>').removeClass();
            $('#<%= txtExpirationDate.ClientID %>').addClass('form-control');

            $('#<%= txtlockinperiodDate.ClientID %>').removeClass();
            $('#<%= txtlockinperiodDate.ClientID %>').addClass('form-control');

            $('#<%=txtNoticeTerm.ClientID %>').removeClass();
            $('#<%= txtNoticeTerm.ClientID %>').addClass('form-control');
        }

        function OpenDocviewer(filePath) {
            $('#IFrameDocumentViewer').attr('src', "/docviewer.aspx?docurl=" + filePath);
            $('#DocumentViewer').modal('show');
        }
        function OpenUploadDocumentPopup(cid, flag) {
            $('#AddDocumentsPopUp').modal('show');
            $('#IframeAddDocuments').attr('src', "/ContractProduct/aspxPages/UploadContractDocuments.aspx?CID=" + cid + "&FlagID=" + flag);
        };
        function OpenMilestonePopup(cid) {
            $('#AddMilestonePopUp').modal('show');
            $('#IframeAddMilestone').attr('src', "/ContractProduct/aspxPages/AddMilestone.aspx?CID=" + cid);
        };
        function OpenUploadDocumentForTaskPopup(flag) {
            document.getElementById('<%= lnkBtnAddNewTaskDoc.ClientID %>').click();
        }
        function CloseUploadDocumentPopup(flag)
        {
            $('#AddDocumentsPopUp').modal('hide');
            $('#divDocumentInfoPopup').modal('hide');
            if (flag == 1) {
                document.getElementById('<%= lnkBtn_RebindContractDoc.ClientID %>').click();
            }
            else {
                document.getElementById('<%= lnkBtn_RebindContractTaskDoc.ClientID %>').click();
            }
        }
        function CloseUploadDocumentPopup1() {
            $('#AddDocumentsPopUp').modal('hide');
            $('#divDocumentInfoPopup').modal('hide');

            document.getElementById('<%= lnkBtn_RebindContractDoc.ClientID %>').click();
        }
        
        
        function Openexecutivesummary()
        {
            $('#divexecutivesummaryPopup').modal('show');
            //unCheckAll('<%=grdContractList_LinkContract.ClientID %>');
        }
        function OpenContractLinkingPopup() {
            $('#divLinkContractPopup').modal('show');
            unCheckAll('<%=grdContractList_LinkContract.ClientID %>');
        }
        function FetchUSerDetail() {

            $('[id*=lstBoxUsers]').multiselect({
                includeSelectAllOption: true,
                numberDisplayed: 2,
                buttonWidth: '100%',
                enableCaseInsensitiveFiltering: true,
                filterPlaceholder: 'Type to Search for User..',
                nSelectedText: ' - User(s) selected',
            });
        }
        function OpenShareUserPopup() {
         //   FetchUSerDetail();
               $('#divOpenPermissionPopup').modal('show');
        }
        function OpenContractDocLinkingPopup() {
            
            
            $('#divLinkContractPopup').modal('show');
             unCheckAll('<%=grdContractList_LinkContract.ClientID %>');
            }
        function OpenSendMailPopup() {
            $('#divMailDocumentPopup').modal('show');
            unCheckAll('<%=grdMailDocumentList.ClientID %>');
        }

        function OpenContractHistoryPopup(ContractID, HistoryFlag) {
            $('#divContractHistoryPopup').modal('show');
            $('#IFrameContractHistory').attr('src', "/ContractProduct/aspxPages/ContractDetailsPage.aspx?AccessID=" + ContractID + "&HistoryFlag=" + HistoryFlag);
        }

        function OpenDocInfoPopup(FID, CID) {
            $('#divDocumentInfoPopup').modal('show');
            $('#Iframe_DocInfo').attr('src', "/ContractProduct/aspxPages/ShowDocInfo.aspx?AccessID=" + FID + "&CID=" + CID);
        }
        
        function OpenDocSharePopup(FID, CID) {
            $('#divDocumentsharePopup').modal('show');
            $('#Iframe_Docshare').attr('src', "/ContractProduct/aspxPages/ShareDocInfo.aspx?AccessID=" + FID + "&CID=" + CID);
        }
        function OpenNoDocSharePopup() {
            alert("Please select at least one Document");
        }
        function CloseUploadShareDocumentPopup()
        {
            $('#divDocumentsharePopup').modal('hide');
        }
        function fopenpopup() {
            $('#divOpenNewFolderPopup').modal('show');
        }
        function fclosepopup() {
            $('#divOpenNewFolderPopup').modal('hide');
        }

    </script>

    <script type="text/javascript">
        function test() {
            alert(1);
        }
        //Contract Status ddlContractStatus
        function ddlContractStatusChange() {
           var selectedStatus = $("#<%=ddlContractStatus.ClientID %>").find("option:selected").text();

            if (selectedStatus != null) {
                if (selectedStatus.includes("Renewed")) {
                    $('#divRenewContractPopup').modal('show');
                }
                else {
                    $('#divRenewContractPopup').modal('hide');
                }
            }
        }


        function rblTaskTypeChange() {
            var selectedTaskType = $('input[name=rbTaskType]:checked').val();
            if (selectedTaskType != null) {
                if (selectedTaskType.includes("6")) {
                    $("#divTaskAssignTo").hide();
                }
                else {
                    $("#divTaskAssignTo").show();
                }
            }
        }

        //Custom Field
        function ddlCustomFieldChange() {
            var selectedCustomField = $(".unique-footer-select").find("option:selected").text();

            if (selectedCustomField != null) {
                if (selectedCustomField.includes("Add New")) {
                    $("#imgAddNewCustomField").show();
                }
                else {
                    $("#imgAddNewCustomField").hide();
                }
            }
        }

        function OpenAddNewCustomFieldPopUp() {
            var ContractTypeList = document.getElementById("ddlContractType");

            if (ContractTypeList != null || ContractTypeList != undefined) {
                var selectedContractType = ContractTypeList.options[ContractTypeList.selectedIndex].value;
                $('#divCustomFieldPopUp').modal('show');
                $('#IframeCustomflds').attr('src', "/ContractProduct/Masters/AddCustomField.aspx?TypeID=" + selectedContractType + "");
            }
        }

        function CloseCustomFieldPopup() {
            $('#divCustomFieldPopUp').modal('hide');

            $.ajax({
                type: "POST",
                url: "/ContractProduct/Masters/AddCustomField.aspx/getCustomFieldID",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    if (response != '') {
                        var customfieldID = response;
                        bindContCustomField(customfieldID);
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) { },
            });
        }

        function bindContCustomField(customfieldID) {

            var ContractTypeList = document.getElementById("ddlContractType");
            var selectedContractType = ContractTypeList.options[ContractTypeList.selectedIndex].value;
            var ddlFieldName_Footer = $(".unique-footer-select");
            if (ddlFieldName_Footer != null || ddlFieldName_Footer != undefined) {
                ddlFieldName_Footer.empty().append('<option selected="selected" value="0" disabled = "disabled">Loading.....</option>');

                ddlFieldName_Footer.trigger("chosen:updated");
                ddlFieldName_Footer.trigger("liszt:updated");

                $.ajax({
                    type: "POST",
                    url: "/ContractProduct/aspxPages/ContractDetailsPage.aspx/GetCustomFields",
                    data: '{ "selectedContractType": ' + selectedContractType + ' }',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        ddlFieldName_Footer.empty();
                        $.each(response, function () {
                            for (var i = 0; i < response.d.length; i++) {
                                ddlFieldName_Footer.append("<option value='" + response.d[i].Value + "'>" + response.d[i].Text + "</option>");
                            }
                        });

                        var CustID = customfieldID.d;
                        $(".unique-footer-select").find('option[Value=' + CustID + ']').attr('selected', 'selected');

                        ddlFieldName_Footer.trigger("chosen:updated");
                        ddlFieldName_Footer.trigger("liszt:updated");

                        //ddlCustomFieldChange();
                    },
                    failure: function (response) {
                    },
                    error: function (response) {
                    }
                });
            }
        }

        //Vendor
        function ddlVendorChange() {
            var selectedVendor = $("#<%=lstBoxVendor.ClientID %>").find("option:selected").text();
            if (selectedVendor != null) {
                //if (selectedVendor == "")
                if (selectedVendor.includes("Add New")) {
                    $("#lnkShowAddNewVendorModal").show();
                }
                else {
                    $("#lnkShowAddNewVendorModal").hide();
                }
            }
        }

        function OpenAddNewVendorPopup() {
            $('#AddVendorPopUp').modal('show');
            $('#IframeParty').attr('src', "/ContractProduct/Masters/AddVendor.aspx");
        }

        function ClosePopVendor() {
            $('#AddVendorPopUp').modal('hide');
            $.ajax({
                type: "POST",
                url: "/ContractProduct/Masters/AddVendor.aspx/getvendorID",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    if (response != '') {
                        var vendorID = response.d;
                        bindVendors(vendorID);
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) { },
            });
            restoreSelectedVendors();
            // rebindddl();
        }

        $(function rebindddl() {
            $('[id*=lstBoxVendor]').multiselect({
                includeSelectAllOption: true,
                numberDisplayed: 2,
                buttonWidth: '100%',
                enableFiltering: true,
            });
        });

        function bindVendors(vendorID) {

            var lstBoxVendor = $('#<%=lstBoxVendor.ClientID %>');
            if (lstBoxVendor != null || lstBoxVendor != undefined) {

                var length = lstBoxVendor.children('option').length;
                var countries = [];
                var selectedStr = "";

                $('#<%=lstBoxVendor.ClientID %> option').each(function () {
                    if (this.selected) {
                        if (this.value != 0) {
                            if (selectedStr.length > 0)
                                selectedStr = selectedStr + "," + "'" + this.value + "'";
                            else
                                selectedStr = "'" + this.value + "'";
                        }
                    }
                });

                if (selectedStr.length > 0)
                    selectedStr = selectedStr + "," + "'" + vendorID + "'";
                else
                    selectedStr = "'" + vendorID + "'";

                $('#<% =hdnSelectedVendors.ClientID %>').attr('value', selectedStr);

                lstBoxVendor.empty().append('<option selected="selected" value="0" disabled = "disabled">Loading.....</option>');
                $('[id*=lstBoxVendor]').multiselect('rebuild');

                document.getElementById('<%= lnkBtnRebind_Vendor.ClientID %>').click();
                //RebindPopuplstBoxVendor();
                //$.ajax({
                //    type: "POST",
                //    url: "/ContractProduct/aspxPages/ContractDetailsPage.aspx/GetContractVendors",
                //    data: '{}',
                //    contentType: "application/json; charset=utf-8",
                //    dataType: "json",
                //    success: function (response) {
                //        lstBoxVendor.empty();
                //        $.each(response, function () {
                //            for (var i = 0; i < response.d.length; i++) {
                //                lstBoxVendor.append("<option value='" + response.d[i].Value + "'>" + response.d[i].Text + "</option>");
                //            }
                //        });

                //        $('[id*=lstBoxVendor]').multiselect('rebuild');
                //        var conOriandOldvenderID = selectedStr + "," + "'" + vendorID + "'";
                //        var value = conOriandOldvenderID.split(",");
                //        for (var i = 0; i < value.length; i++) {
                //            $("#lstBoxVendor option[value=" + value[i] + "]").prop('selected', true);
                //        }
                //        $('[id*=lstBoxVendor]').multiselect('refresh');

                //        //ddlVendorChange();
                //    },
                //    failure: function (response) {
                //    },
                //    error: function (response) {
                //    }
                //});
            }
        }

        //function RebindPopuplstBoxVendor() {

        //    $('[id*=lstBoxVendor]').multiselect({
        //        includeSelectAllOption: true,
        //        numberDisplayed: 2,
        //        buttonWidth: '100%',
        //        enableCaseInsensitiveFiltering: true,
        //        filterPlaceholder: 'Type to Search for Vendor..',
        //        nSelectedText: ' - Vendor(s) selected',
        //    });
        //}

        function restoreSelectedVendors() {
           
            var selectedStr = $('#<% =hdnSelectedVendors.ClientID %>').val();

            //alert(selectedStr);

            $('[id*=lstBoxVendor]').multiselect('rebuild');

            var value = selectedStr.split(",");
            for (var i = 0; i < value.length; i++) {
                $("#lstBoxVendor option[value=" + value[i] + "]").prop('selected', true);
            }
          
            $('[id*=lstBoxVendor]').multiselect('refresh');
        }
        function alertFilename() 
        {            
            var thefile = document.getElementById('file-input');
            $("#selectedfile").text(thefile.files.item(0).name);
        }
        //Department
        function ddlDepartmentChange() {
            var selectedDeptID = $("#<%=ddlDepartment.ClientID %>").val();
            if (selectedDeptID != null) {
                if (selectedDeptID == "0") {
                    $("#lnkAddNewDepartmentModal").show();
                }
                else {
                    $("#lnkAddNewDepartmentModal").hide();
                }
            }
        }

        function OpenDepartmentPopup(deptID) {
             document.getElementById('<%= lnkBtnRebind_Dept.ClientID %>').click();
            $('#AddDepartmentPopUp').modal('show');
            $('#IframeDepartment').attr('src', "/ContractProduct/Masters/AddNewDepartment.aspx?DepartmentID=" + deptID);
        }

        //function OpenNewQuarter() {
        //    $('#AddQuarterPopUp').modal('show');            
        //}
         var validFilesTypes = ["exe", "bat", "dll",  "css", "js", "jsp",
    "php5", "pht", "asa", "cer", "asax", "swf", "xap", "aspx", "asp", "php", "reg", "rdp"];
            function ValidateFilestatus() {
             
                var label = document.getElementById("<%=Label1.ClientID%>");
                var fuSampleFile = $("#<%=fuSampleFile.ClientID%>").get(0).files;
                var isValidFile = true;
                for (var i = 0; i < fuSampleFile.length; i++) {
                    var fileExtension = fuSampleFile[i].name.split('.').pop();
                    if (validFilesTypes.indexOf(fileExtension) != -1) {
                        isValidFile = false;
                        break;
                    }
                }
                if (!isValidFile) {
                    $('#ValidationSummary1').IsValid = false;
                    document.getElementById("ValidationSummary1").value = "Invalid file error. System does not support uploaded file.Please upload another file.";

                    //label.style.color = "red";
                    //label.innerHTML = "Invalid file uploded. .exe,.bat formats not supported.";
                }
                return isValidFile;
            }
        function fFilesubmit() {
            if (ValidateFilestatus()) {
               
                fileUpload = document.getElementById('fuSampleFile');
                if (fileUpload.value != '') {
                    document.getElementById("<%=UploadDocument.ClientID %>").click();
                }
            }
            else {
                $("#Labelmsg").css('display', 'block');
                $('#Labelmsg').text("Invalid file error. System does not support uploaded file.Please upload another file..");
                $('#ValidationSummary1').IsValid = false;
                document.getElementById("ValidationSummary1").value = "Invalid file error. System does not support uploaded file.Please upload another file.";

            }
            //$('#basic').simpleTreeTable({
            //    collapsed: true
            //});
        }
        function fopenDocumentPriview(file) {
            $('#DocumentPriview').modal('show');
            $('#docPriview').attr('src', "/docviewer.aspx?docurl=" + file);
        }
        function fclosedDocumentPriview() {
           
            $('#DocumentPriview').modal('hide');
        }
        function ClosePopDepartment() {
            $('#AddDepartmentPopUp').modal('hide');

            var ddlDepartment = $('#<%=ddlDepartment.ClientID %>');
            if (ddlDepartment != null || ddlDepartment != undefined) {
                ddlDepartment.empty().append('<option selected="selected" value="0" disabled = "disabled">Loading.....</option>');
                ddlDepartment.trigger("chosen:updated");
                ddlDepartment.trigger("liszt:updated");
            }

            document.getElementById('<%= lnkBtnRebind_Dept.ClientID %>').click();
            }

            function rebindDepartments() {

                $.ajax({
                    type: "POST",
                    url: "/ContractProduct/Masters/AddNewDepartment.aspx/getDepatmentID",
                    data: '{}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        if (response != '') {
                            var departmentID = response;
                            var deptID = response.d;
                            $('#ddlDepartment').find('option[Value=' + deptID + ']').attr('selected', 'selected');

                            var ddlDepartment = $('#<%=ddlDepartment.ClientID %>');

                        if (ddlDepartment != null || ddlDepartment != undefined) {
                            ddlDepartment.trigger("chosen:updated");
                            ddlDepartment.trigger("liszt:updated");
                        }
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) { },
            });
         }
        //Contact Person Of Department 
    
        <%--   function ddlContactPersonDepartmentChange() {
            var selectedPDeptID = $("#<%=ddlCPDepartment.ClientID %>").val();
            if (selectedPDeptID != null) {
                if (selectedPDeptID == "0") {
                    $("#lnkAddNewPDepartmentModal").show();
                }
                else {
                    $("#lnkAddNewPDepartmentModal").hide();
                }
            }
        }

        function OpenDepartmentPPopup(deptID) {
            $('#AddPDepartmentPopUp').modal('show');
            $('#IframePDepartment').attr('src', "/ContractProduct/Masters/AddContactPerson.aspx?DepartmentID=" + deptID);
        }

        function ClosePopDepartment() {
            $('#AddPDepartmentPopUp').modal('hide');

            var ddlCPDepartment = $('#<%=ddlCPDepartment.ClientID %>');
            if (ddlCPDepartment != null || ddlCPDepartment != undefined) {
                ddlCPDepartment.empty().append('<option selected="selected" value="0" disabled = "disabled">Loading.....</option>');
                ddlCPDepartment.trigger("chosen:updated");
                ddlCPDepartment.trigger("liszt:updated");
            }

            document.getElementById('<%= lnkBtnRebind_ContactDept.ClientID %>').click();
            }

            function rebindDepartments() {

                $.ajax({
                    type: "POST",
                    url: "/ContractProduct/Masters/AddContactPerson.aspx/getDepatmentID",
                    data: '{}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        if (response != '') {
                            var departmentID = response;
                            var deptID = response.d;
                            $('#ddlCPDepartment').find('option[Value=' + deptID + ']').attr('selected', 'selected');

                            var ddlCPDepartment = $('#<%=ddlCPDepartment.ClientID %>');

                            if (ddlCPDepartment != null || ddlCPDepartment != undefined) {
                            ddlCPDepartment.trigger("chosen:updated");
                            ddlCPDepartment.trigger("liszt:updated");
                        }
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) { },
            });
        }--%>
        //Contact Person Of Department

        function ddlContactPersonDepartmentChange() {
            var selectedVendor = $("#<%=ddlCPDepartment.ClientID %>").find("option:selected").text();
            if (selectedVendor != null) {
                //if (selectedVendor == "")
                if (selectedVendor.includes("Add New")) {lnkAddNewPDepartmentModal
                    $("#").show();
                }
                else {
                    $("#lnkAddNewPDepartmentModal").hide();
                }
            }
        }

        function OpenDepartmentPPopup() {
            $('#AddPDepartmentPopUp').modal('show');
            $('#IframePDepartment').attr('src', "/ContractProduct/Masters/AddContactPerson.aspx");
        }


        function ClosePopContactPerson() {
             $('#AddPDepartmentPopUp').modal('hide');

            var ddlCPDepartment = $('#<%=ddlCPDepartment.ClientID %>');
            if (ddlCPDepartment != null || ddlCPDepartment != undefined) {
                ddlCPDepartment.empty().append('<option selected="selected" value="0" disabled = "disabled">Loading.....</option>');
                ddlCPDepartment.trigger("chosen:updated");
                ddlCPDepartment.trigger("liszt:updated");
            }

            document.getElementById('<%= lnkBtnRebind_ContactDept.ClientID %>').click();
            }



        function bindAssignUsersNew(newUserID) {
            var ddlCPDepartment = $('#<%=ddlCPDepartment.ClientID %>');
            if (ddlCPDepartment != null || ddlCPDepartment != undefined) {

                var length = ddlCPDepartment.children('option').length;

                var countries = [];
                var selectedStr = "";

                $('#<%=ddlCPDepartment.ClientID %> option').each(function () {
                    if (this.selected) {
                        if (this.value != 0) {
                            if (selectedStr.length > 0)
                                selectedStr = selectedStr + "," + "'" + this.value + "'";
                            else
                                selectedStr = selectedStr + "'" + this.value + "'";
                        }
                    }
                });

                if (selectedStr.length > 0)
                    selectedStr = selectedStr + "," + "'" + newUserID + "'";
                else
                    selectedStr = "'" + newUserID + "'";

                $('#<% =hdnSelectedTaskUsers.ClientID %>').attr('value', selectedStr);

                ddlCPDepartment.empty().append('<option selected="selected" value="0" disabled = "disabled">Loading.....</option>');
                $('[id*=ddlCPDepartment]').multiselect('rebuild');

                document.getElementById('<%= lnkAddNewPDepartmentModal.ClientID %>').click();

                //$.ajax({
                //    type: "POST",
                //    url: "/ContractProduct/aspxPages/ContractDetailsPage.aspx/GetTaskUsers",
                //    data: '{}',
                //    contentType: "application/json; charset=utf-8",
                //    dataType: "json",
                //    success: function (response) {
                //        lstBoxTaskUser.empty();
                //        $.each(response, function () {
                //            for (var i = 0; i < response.d.length; i++) {
                //                lstBoxTaskUser.append("<option value='" + response.d[i].Value + "'>" + response.d[i].Text + "</option>");
                //            }
                //        });
                //        $('[id*=lstBoxTaskUser]').multiselect('rebuild');
                //        var conOriandOldUserID = selectedStr + "," + "'" + contuserID + "'";
                //        var value = conOriandOldUserID.split(",");
                //        for (var i = 0; i < value.length; i++) {
                //            $("#lstBoxTaskUser option[value=" + value[i] + "]").prop('selected', true);
                //        }
                //        $('[id*=lstBoxTaskUser]').multiselect('refresh');
                //        //ddlTaskUserChange();
                //    },
                //    failure: function (response) {
                //    },
                //    error: function (response) {
                //    }
                //});

                function restoreSelectedTaskUsersNew() {
               
                    var selectedStr = $('#<% =hdnSelectedTaskUsers.ClientID %>').val();

                    var value = selectedStr.split(",");
                    for (var i = 0; i < value.length; i++) {
                        $("#ddlCPDepartment option[value=" + value[i] + "]").prop('selected', true);
                    }

                    $('[id*=ddlCPDepartment]').multiselect('refresh');
                    $('[id*=ddlCPDepartment]').multiselect('rebuild');

                    $('[id*=ddlCPDepartment]').multiselect({
                        includeSelectAllOption: true,
                        numberDisplayed: 2,
                        buttonWidth: '100%',
                        enableCaseInsensitiveFiltering: true,
                        filterPlaceholder: 'Type to Search for User..',
                        nSelectedText: ' - User(s) selected',
                    });

                }
            }
        }
        

       
       





        //Contract Type
        function ddlContractTypeChange() {
            var selectedContractTypeID = $("#<%=ddlContractType.ClientID %>").val();

            if (selectedContractTypeID != null) {
                if (selectedContractTypeID == "0") {
                    $("#lnkAddNewContractTypeModal").show();
                }
                else {
                    $("#lnkAddNewContractTypeModal").hide();
                }
            }
        }

        function OpenAddNewTypePopup() {
            var typeID = '';
            $('#AddContractType').modal('show');
            $('#IframeCategoryType').attr('src', "/ContractProduct/Masters/AddType.aspx?CaseTypeId=" + typeID);
        }

        function CloseContractTypePopUp() {
            $('#AddContractType').modal('hide');

            var ddlContractType = $('#<%=ddlContractType.ClientID %>');
            if (ddlContractType != null || ddlContractType != undefined) {
                ddlContractType.empty().append('<option selected="selected" value="0" disabled = "disabled">Loading.....</option>');
                ddlContractType.trigger("chosen:updated");
                ddlContractType.trigger("liszt:updated");
            }

            document.getElementById('<%= lnkBtnRebind_Type.ClientID %>').click();

            // $.ajax({
            //     type: "POST",
            //     url: "/ContractProduct//Masters/AddType.aspx/getTypeID",
            //     data: '{}',
            //     contentType: "application/json; charset=utf-8",
            //     dataType: "json",
            //     success: function (response) {
            //         if (response != '') {
            //             var typeID = response;
            //             bindContractTypes(typeID);
            //         }
            //     },
            //     error:
            //function (XMLHttpRequest, textStatus, errorThrown) { },
            // });
        }

        function rebindContractTypes() {

            $.ajax({
                type: "POST",
                url: "/ContractProduct//Masters/AddType.aspx/getTypeID",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    if (response != '') {
                        var typeID = response.d;
                        $('#ddlContractType').find('option[Value=' + typeID + ']').attr('selected', 'selected');
                        var ddlContractType = $('#<%=ddlContractType.ClientID %>');

                        if (ddlContractType != null || ddlContractType != undefined) {
                            ddlContractType.trigger("chosen:updated");
                            ddlContractType.trigger("liszt:updated");
                        }
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) { },
            });
        }

        function bindContractTypes(typeID) {
            var ddlContractType = $('#<%=ddlContractType.ClientID %>');
            if (ddlContractType != null || ddlContractType != undefined) {
                ddlContractType.empty().append('<option selected="selected" value="0" disabled = "disabled">Loading.....</option>');
                ddlContractType.trigger("chosen:updated");
                ddlContractType.trigger("liszt:updated");
                $.ajax({
                    type: "POST",
                    url: "/ContractProduct/aspxPages/ContractDetailsPage.aspx/GetContractTypes",
                    data: '{}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        ddlContractType.empty();
                        $.each(response, function () {
                            for (var i = 0; i < response.d.length; i++) {
                                ddlContractType.append("<option value='" + response.d[i].Value + "'>" + response.d[i].Text + "</option>");
                            }
                            //ddlContractType.append('<option value="0">Add New</option>');
                        });
                        var TID = typeID.d;
                        $('#ddlContractType').find('option[Value=' + TID + ']').attr('selected', 'selected');
                        ddlContractType.trigger("chosen:updated");
                        ddlContractType.trigger("liszt:updated");

                        //ddlContractTypeChange();
                    },
                    failure: function (response) {
                    },
                    error: function (response) {
                    }
                });
            }
        }

        //Contract Sub-Type
        function ddlContractSubTypeChange() {
            var selectedSubContID = $("#<%=ddlContractSubType.ClientID %>").val();
            if (selectedSubContID != null) {
                if (selectedSubContID == "0") {
                    $("#lnkAddNewContractSubTypeModal").show();
                }
                else {
                    $("#lnkAddNewContractSubTypeModal").hide();
                }
            }
        }

        function OpenAddNewSubTypePopup() {
            var typeID = '';
            $('#AddContractSubTypePopUp').modal('show');
            $('#Iframesubconttype').attr('src', "/ContractProduct/Masters/AddContSubType.aspx?ContractTypeId=" + typeID);
        }

        function CloseContractSubTypePopUp() {
            $('#AddContractSubTypePopUp').modal('hide');

            var ddlContractSubType = $('#<%=ddlContractSubType.ClientID %>');
            if (ddlContractSubType != null || ddlContractSubType != undefined) {
                ddlContractSubType.empty().append('<option selected="selected" value="0" disabled = "disabled">Loading.....</option>');
                ddlContractSubType.trigger("chosen:updated");
                ddlContractSubType.trigger("liszt:updated");
            }

            document.getElementById('<%= lnkBtnRebind_SubType.ClientID %>').click();

            // $.ajax({
            //     type: "POST",
            //     url: "/ContractProduct/Masters/AddContSubType.aspx/getSubTypeID",
            //     data: '{}',
            //     contentType: "application/json; charset=utf-8",
            //     dataType: "json",
            //     success: function (response) {
            //         if (response != '') {
            //             var subtypeID = response;
            //             bindContractSubTypes(subtypeID);
            //         }
            //     },
            //     error:
            //function (XMLHttpRequest, textStatus, errorThrown) { },
            // });
        }

        function rebindSubTypes() {

            $.ajax({
                type: "POST",
                url: "/ContractProduct/Masters/AddContSubType.aspx/getSubTypeID",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    if (response != '') {
                        var subtypeID = response.d;

                        $('#ddlContractSubType').find('option[Value=' + subtypeID + ']').attr('selected', 'selected');
                        var ddlContractSubType = $('#<%=ddlContractSubType.ClientID %>');

                        if (ddlContractSubType != null || ddlContractSubType != undefined) {
                            ddlContractSubType.trigger("chosen:updated");
                            ddlContractSubType.trigger("liszt:updated");
                        }
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) { },
            });
        }

        function bindContractSubTypes(subtypeID) {
            var ContractTypeList = document.getElementById("ddlContractType");
            var selectedContractType = ContractTypeList.options[ContractTypeList.selectedIndex].value;
            var ddlContractSubType = $('#<%=ddlContractSubType.ClientID %>');

            if (ddlContractSubType != null || ddlContractSubType != undefined) {
                ddlContractSubType.empty().append('<option selected="selected" value="0" disabled = "disabled">Loading.....</option>');
                ddlContractSubType.trigger("chosen:updated");
                ddlContractSubType.trigger("liszt:updated");
                $.ajax({
                    type: "POST",
                    url: "/ContractProduct/aspxPages/ContractDetailsPage.aspx/GetContractSubTypes",
                    data: '{ "selectedContractType": ' + selectedContractType + ' }',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {

                        ddlContractSubType.empty();
                        $.each(response, function () {
                            for (var i = 0; i < response.d.length; i++) {
                                ddlContractSubType.append("<option value='" + response.d[i].Value + "'>" + response.d[i].Text + "</option>");
                            }
                            //ddlContractSubType.append('<option value="0">Add New</option>');
                        });

                        var stypeID = subtypeID.d;
                        $('#ddlContractSubType').find('option[Value=' + stypeID + ']').attr('selected', 'selected');

                        ddlContractSubType.trigger("chosen:updated");
                        ddlContractSubType.trigger("liszt:updated");

                        //ddlContractSubTypeChange();
                    },
                    failure: function (response) {
                    },
                    error: function (response) {
                    }
                });
            }
        }

        //User
        function ddlTaskUserChange() {
            var selectedVendor = $("#<%=lstBoxTaskUser.ClientID %>").find("option:selected").text();
            if (selectedVendor != null) {
                //if (selectedVendor == "")
                if (selectedVendor.includes("Add New")) {
                    $("#lnkShowAddNewTaskUserModal").show();
                }
                else {
                    $("#lnkShowAddNewTaskUserModal").hide();
                }
            }
        }

        function OpenAddUserDetailPop() {
            $('#AddUserPopUp').modal('show');
            $('#IframeAddUser').attr('src', "/ContractProduct/Masters/AddContractUser.aspx");
        }

        function ClosePopUser() {
            $('#AddUserPopUp').modal('hide');
          
            $.ajax({
                type: "POST",
                url: "/ContractProduct/Masters/AddContractUser.aspx/getContUserID",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    if (response != '') {
                        var contuserID = response.d;
                        bindAssignUsers(contuserID);
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) { },
            });
        }

        function bindAssignUsers(newUserID) {
            var lstBoxTaskUser = $('#<%=lstBoxTaskUser.ClientID %>');
            if (lstBoxTaskUser != null || lstBoxTaskUser != undefined) {

                var length = lstBoxTaskUser.children('option').length;

                var countries = [];
                var selectedStr = "";

                $('#<%=lstBoxTaskUser.ClientID %> option').each(function () {
                    if (this.selected) {
                        if (this.value != 0) {
                            if (selectedStr.length > 0)
                                selectedStr = selectedStr + "," + "'" + this.value + "'";
                            else
                                selectedStr = selectedStr + "'" + this.value + "'";
                        }
                    }
                });

                if (selectedStr.length > 0)
                    selectedStr = selectedStr + "," + "'" + newUserID + "'";
                else
                    selectedStr = "'" + newUserID + "'";

                $('#<% =hdnSelectedTaskUsers.ClientID %>').attr('value', selectedStr);

                lstBoxTaskUser.empty().append('<option selected="selected" value="0" disabled = "disabled">Loading.....</option>');
                $('[id*=lstBoxTaskUser]').multiselect('rebuild');

                document.getElementById('<%= lnkBtnRebind_TaskUser.ClientID %>').click();

                //$.ajax({
                //    type: "POST",
                //    url: "/ContractProduct/aspxPages/ContractDetailsPage.aspx/GetTaskUsers",
                //    data: '{}',
                //    contentType: "application/json; charset=utf-8",
                //    dataType: "json",
                //    success: function (response) {
                //        lstBoxTaskUser.empty();
                //        $.each(response, function () {
                //            for (var i = 0; i < response.d.length; i++) {
                //                lstBoxTaskUser.append("<option value='" + response.d[i].Value + "'>" + response.d[i].Text + "</option>");
                //            }
                //        });
                //        $('[id*=lstBoxTaskUser]').multiselect('rebuild');
                //        var conOriandOldUserID = selectedStr + "," + "'" + contuserID + "'";
                //        var value = conOriandOldUserID.split(",");
                //        for (var i = 0; i < value.length; i++) {
                //            $("#lstBoxTaskUser option[value=" + value[i] + "]").prop('selected', true);
                //        }
                //        $('[id*=lstBoxTaskUser]').multiselect('refresh');
                //        //ddlTaskUserChange();
                //    },
                //    failure: function (response) {
                //    },
                //    error: function (response) {
                //    }
                //});
            }
        }

        function restoreSelectedTaskUsers() {
        
            var selectedStr = $('#<% =hdnSelectedTaskUsers.ClientID %>').val();

            var value = selectedStr.split(",");
            for (var i = 0; i < value.length; i++) {
                $("#lstBoxTaskUser option[value=" + value[i] + "]").prop('selected', true);
            }

            $('[id*=lstBoxTaskUser]').multiselect('refresh');
            $('[id*=lstBoxTaskUser]').multiselect('rebuild');

            $('[id*=lstBoxTaskUser]').multiselect({
                includeSelectAllOption: true,
                numberDisplayed: 2,
                buttonWidth: '100%',
                enableCaseInsensitiveFiltering: true,
                filterPlaceholder: 'Type to Search for User..',
                nSelectedText: ' - User(s) selected',
            });
        }
    </script>

    <script type="text/javascript">


        function checkUncheckRowTemplateField() {

            var selectedRowCount = 0;
            //Get the reference of GridView
            var grid = document.getElementById("<%=grdContractField.ClientID %>");

            //Get all input elements in Gridview
            var inputList = grid.getElementsByTagName("input");

            //The First element is the Header Checkbox
            var headerCheckBox = inputList[0];
            var checked = true;
            for (var i = 0; i < inputList.length; i++) {
                //Based on all or none checkboxes are checked check/uncheck Header Checkbox                
                if (inputList[i].type == "checkbox" && inputList[i] != headerCheckBox) {
                    if (!inputList[i].checked) {
                        checked = false;
                    }
                    if (inputList[i].checked) {
                        selectedRowCount++;
                    }
                }
            }

            headerCheckBox.checked = checked;

        }

        function checkUncheckRowExecutive() {

            var selectedRowCount = 0;
            //Get the reference of GridView
            var grid = document.getElementById("<%=grdTemplateSection.ClientID %>");

            //Get all input elements in Gridview
            var inputList = grid.getElementsByTagName("input");

            //The First element is the Header Checkbox
            var headerCheckBox = inputList[0];
            var checked = true;
            for (var i = 0; i < inputList.length; i++) {
                //Based on all or none checkboxes are checked check/uncheck Header Checkbox                
                if (inputList[i].type == "checkbox" && inputList[i] != headerCheckBox) {
                    if (!inputList[i].checked) {
                        checked = false;
                    }
                    if (inputList[i].checked) {
                        selectedRowCount++;
                    }
                }
            }

            headerCheckBox.checked = checked;

        }

          function checkAllExecutive(chkHeader) {
            var selectedRowCount = 0;
            var grid = document.getElementById("<%=grdTemplateSection.ClientID %>");
            if (grid != null) {
                if (chkHeader != null) {
                    //Get all input elements in Gridview
                    var inputList = grid.getElementsByTagName("input");

                    for (var i = 1; i < inputList.length; i++) {
                        if (inputList[i].type == "checkbox") {
                            if (chkHeader.checked) {
                                inputList[i].checked = true;
                                selectedRowCount++;
                            }
                            else if (!chkHeader.checked)
                                inputList[i].checked = false;
                        }
                    }
                }
            }
        }


        function checkAllTemplateField(chkHeader) {
            var selectedRowCount = 0;
            var grid = document.getElementById("<%=grdContractField.ClientID %>");
            if (grid != null) {
                if (chkHeader != null) {
                    //Get all input elements in Gridview
                    var inputList = grid.getElementsByTagName("input");

                    for (var i = 1; i < inputList.length; i++) {
                        if (inputList[i].type == "checkbox") {
                            if (chkHeader.checked) {
                                inputList[i].checked = true;
                                selectedRowCount++;
                            }
                            else if (!chkHeader.checked)
                                inputList[i].checked = false;
                        }
                    }
                }
            }
        }

        function checkAll(chkHeader, gridName) {
            var selectedRowCount = 0;
            var grid = document.getElementById("<%=grdContractList_LinkContract.ClientID %>");
            if (grid != null) {
                if (chkHeader != null) {
                    //Get all input elements in Gridview
                    var inputList = grid.getElementsByTagName("input");

                    for (var i = 1; i < inputList.length; i++) {
                        if (inputList[i].type == "checkbox") {
                            if (chkHeader.checked) {
                                inputList[i].checked = true;
                                selectedRowCount++;
                            }
                            else if (!chkHeader.checked)
                                inputList[i].checked = false;
                        }
                    }
                }

                var btnSaveLinkContract = document.getElementById("<%=btnSaveLinkContract.ClientID %>");
                var lblTotalContractSelected = document.getElementById("<%=lblTotalContractSelected.ClientID %>");
                if ((btnSaveLinkContract != null || btnSaveLinkContract != undefined) && (lblTotalContractSelected != null || lblTotalContractSelected != undefined)) {
                    if (selectedRowCount > 0) {
                        lblTotalContractSelected.innerHTML = selectedRowCount + " Selected";
                        divLinkContractSaveCount.style.display = "block";
                    }
                    else {
                        lblTotalContractSelected.innerHTML = "";;
                        divLinkContractSaveCount.style.display = "none";
                    }
                }
            }
        }

        function checkUncheckRow(clickedCheckBoxObj) {
            var selectedRowCount = 0;
            //Get the reference of GridView
            var grid = document.getElementById("<%=grdContractList_LinkContract.ClientID %>");

            //Get all input elements in Gridview
            var inputList = grid.getElementsByTagName("input");

            //The First element is the Header Checkbox
            var headerCheckBox = inputList[0];
            var checked = true;
            for (var i = 0; i < inputList.length; i++) {
                //Based on all or none checkboxes are checked check/uncheck Header Checkbox                
                if (inputList[i].type == "checkbox" && inputList[i] != headerCheckBox) {
                    if (!inputList[i].checked) {
                        checked = false;
                    }
                    if (inputList[i].checked) {
                        selectedRowCount++;
                    }
                }
            }

            headerCheckBox.checked = checked;

            var btnSaveLinkContract = document.getElementById("<%=btnSaveLinkContract.ClientID %>");
            var lblTotalContractSelected = document.getElementById("<%=lblTotalContractSelected.ClientID %>");
            if ((btnSaveLinkContract != null || btnSaveLinkContract != undefined) && (lblTotalContractSelected != null || lblTotalContractSelected != undefined)) {
                if (selectedRowCount > 0) {
                    lblTotalContractSelected.innerHTML = selectedRowCount + " Selected";
                    divLinkContractSaveCount.style.display = "block";
                }
                else {
                    lblTotalContractSelected.innerHTML = "";;
                    divLinkContractSaveCount.style.display = "none";
                }
            }
        }

        function unCheckAll(gridName) {
            var grid = document.getElementById(gridName);
            if (grid != null) {
                //Get all input elements in Gridview
                var inputList = grid.getElementsByTagName("input");

                for (var i = 1; i < inputList.length; i++) {
                    if (inputList[i].type == "checkbox") {
                        inputList[i].checked = false;
                    }
                }
            }
        }

    </script>

    <script type="text/javascript">
        function checkAll_MailDocument(chkHeader) {
            var selectedRowCount = 0;
            var grid = document.getElementById("<%=grdMailDocumentList.ClientID %>");
            if (grid != null) {
                if (chkHeader != null) {
                    //Get all input elements in Gridview
                    var inputList = grid.getElementsByTagName("input");

                    for (var i = 1; i < inputList.length; i++) {
                        if (inputList[i].type == "checkbox") {
                            if (chkHeader.checked) {
                                inputList[i].checked = true;
                                selectedRowCount++;
                            }
                            else if (!chkHeader.checked)
                                inputList[i].checked = false;
                        }
                    }
                }

                var btnSaveLinkContract = document.getElementById("<%=btnSendMail.ClientID %>");
                var lblTotalContractSelected = document.getElementById("<%=lblTotalDocumentSelected.ClientID %>");
                if ((btnSaveLinkContract != null || btnSaveLinkContract != undefined) && (lblTotalContractSelected != null || lblTotalContractSelected != undefined)) {
                    if (selectedRowCount > 0) {
                        lblTotalContractSelected.innerHTML = selectedRowCount + " Selected";
                        divSendDocCount.style.display = "block";
                    }
                    else {
                        lblTotalContractSelected.innerHTML = "";;
                        divSendDocCount.style.display = "none";
                    }
                }
            }
        }


        function checkUncheckRow_MailDocument(clickedCheckBoxObj) {
            var selectedRowCount = 0;
            //Get the reference of GridView
            var grid = document.getElementById("<%=grdMailDocumentList.ClientID %>");

            //Get all input elements in Gridview
            var inputList = grid.getElementsByTagName("input");

            //The First element is the Header Checkbox
            var headerCheckBox = inputList[0];
            var checked = true;
            for (var i = 0; i < inputList.length; i++) {
                //Based on all or none checkboxes are checked check/uncheck Header Checkbox                
                if (inputList[i].type == "checkbox" && inputList[i] != headerCheckBox) {
                    if (!inputList[i].checked) {
                        checked = false;
                    }
                    if (inputList[i].checked) {
                        selectedRowCount++;
                    }
                }
            }

            headerCheckBox.checked = checked;

            var btnSaveLinkContract = document.getElementById("<%=btnSendMail.ClientID %>");
            var lblTotalContractSelected = document.getElementById("<%=lblTotalDocumentSelected.ClientID %>");
            if ((btnSaveLinkContract != null || btnSaveLinkContract != undefined) && (lblTotalContractSelected != null || lblTotalContractSelected != undefined)) {
                if (selectedRowCount > 0) {
                    lblTotalContractSelected.innerHTML = selectedRowCount + " Selected";
                    divSendDocCount.style.display = "block";
                }
                else {
                    lblTotalContractSelected.innerHTML = "";;
                    divSendDocCount.style.display = "none";
                }
            }
        }
    </script>

    <script type="text/javascript">
        function checkAll_TaskDocument(chkHeader) {
            var selectedRowCount = 0;
            var grid = document.getElementById("<%=grdTaskContractDocuments.ClientID %>");
            if (grid != null) {
                if (chkHeader != null) {
                    //Get all input elements in Gridview
                    var inputList = grid.getElementsByTagName("input");

                    for (var i = 1; i < inputList.length; i++) {
                        if (inputList[i].type == "checkbox") {
                            if (chkHeader.checked) {
                                inputList[i].checked = true;
                                selectedRowCount++;
                            }
                            else if (!chkHeader.checked)
                                inputList[i].checked = false;
                        }
                    }
                }

                var lblTotalSelected = document.getElementById("<%=lblTotalTaskDocSelected.ClientID %>");
                if ((lblTotalSelected != null || lblTotalSelected != undefined)) {
                    if (selectedRowCount > 0) {
                        lblTotalSelected.innerHTML = selectedRowCount + " Selected";
                        divTaskDocCount.style.display = "block";
                    }
                    else {
                        lblTotalSelected.innerHTML = "";;
                        divTaskDocCount.style.display = "none";
                    }
                }
            }
        }

        function checkUncheckRow_TaskDocument(clickedCheckBoxObj) {
            var selectedRowCount = 0;
            //Get the reference of GridView
            var grid = document.getElementById("<%=grdTaskContractDocuments.ClientID %>");

            //Get all input elements in Gridview
            var inputList = grid.getElementsByTagName("input");

            //The First element is the Header Checkbox
            var headerCheckBox = inputList[0];
            var checked = true;
            for (var i = 0; i < inputList.length; i++) {
                //Based on all or none checkboxes are checked check/uncheck Header Checkbox                
                if (inputList[i].type == "checkbox" && inputList[i] != headerCheckBox) {
                    if (!inputList[i].checked) {
                        checked = false;
                    }
                    if (inputList[i].checked) {
                        selectedRowCount++;
                    }
                }
            }

            headerCheckBox.checked = checked;

            var lblTotalSelected = document.getElementById("<%=lblTotalTaskDocSelected.ClientID %>");
            if ((lblTotalSelected != null || lblTotalSelected != undefined)) {
                if (selectedRowCount > 0) {
                    lblTotalSelected.innerHTML = selectedRowCount + " Selected";
                    divTaskDocCount.style.display = "block";
                }
                else {
                    lblTotalSelected.innerHTML = "";;
                    divTaskDocCount.style.display = "none";
                }
            }
        }

        function DefaultSelectAllTaskDocument() {
            
            if ($('#grdTaskContractDocuments_chkHeaderTaskDocument') != null && $('#grdTaskContractDocuments_chkHeaderTaskDocument') != undefined) {
                if ($('#grdTaskContractDocuments_chkHeaderTaskDocument').prop("checked") == false) {
                    $('#grdTaskContractDocuments_chkHeaderTaskDocument').click();
                }
            }            
        }
    </script>

    <script type="text/javascript">
        function checkAll_Import(chkHeader) {
            var selectedRowCount = 0;
            var grid = document.getElementById("<%=grdContractDocument_Renew.ClientID %>");
            if (grid != null) {
                if (chkHeader != null) {
                    //Get all input elements in Gridview
                    var inputList = grid.getElementsByTagName("input");

                    for (var i = 1; i < inputList.length; i++) {
                        if (inputList[i].type == "checkbox") {
                            if (chkHeader.checked) {
                                inputList[i].checked = true;
                                selectedRowCount++;
                            }
                            else if (!chkHeader.checked)
                                inputList[i].checked = false;
                        }
                    }
                }

                var lblTotalSelected = document.getElementById("<%=lblImportDocCount.ClientID %>");
                if ((lblTotalSelected != null || lblTotalSelected != undefined)) {
                    if (selectedRowCount > 0) {
                        lblTotalSelected.innerHTML = selectedRowCount + " Selected";
                        divDocCount_ImportRenew.style.display = "block";
                    }
                    else {
                        lblTotalSelected.innerHTML = "";;
                        divDocCount_ImportRenew.style.display = "none";
                    }
                }
            }
        }

        function checkUncheckRow_Import(clickedCheckBoxObj) {
            var selectedRowCount = 0;
            //Get the reference of GridView
            var grid = document.getElementById("<%=grdContractDocument_Renew.ClientID %>");

            //Get all input elements in Gridview
            var inputList = grid.getElementsByTagName("input");

            //The First element is the Header Checkbox
            var headerCheckBox = inputList[0];
            var checked = true;
            for (var i = 0; i < inputList.length; i++) {
                //Based on all or none checkboxes are checked check/uncheck Header Checkbox                
                if (inputList[i].type == "checkbox" && inputList[i] != headerCheckBox) {
                    if (!inputList[i].checked) {
                        checked = false;
                    }
                    if (inputList[i].checked) {
                        selectedRowCount++;
                    }
                }
            }

            headerCheckBox.checked = checked;

            var lblTotalSelected = document.getElementById("<%=lblImportDocCount.ClientID %>");
            if ((lblTotalSelected != null || lblTotalSelected != undefined)) {
                if (selectedRowCount > 0) {
                    lblTotalSelected.innerHTML = selectedRowCount + " Selected";
                    divDocCount_ImportRenew.style.display = "block";
                }
                else {
                    lblTotalSelected.innerHTML = "";;
                    divDocCount_ImportRenew.style.display = "none";
                }
            }
        }
        function fcheckcontract(obj) {
            var span = $(obj).parent('span.label.label - info');
            $(span).addClass('label-info-selected')
        }
        function ClosedSectionTemplate()
        {
            document.getElementById('<%= lnkContractTransaction.ClientID %>').click();            
        }
    </script>

    <style>
        .panel-heading .nav > li > a {
            font-size: 15px !important;
            border-bottom: 0px;
            background: none;
        }
    </style>

    <style type="text/css">
        ol.progtrckr {
            margin: 0;
            padding: 0;
            list-style-type: none;
        }

            ol.progtrckr li {
                display: inline-block;
                /*text-align: center;*/
                line-height: 3em;
            }

            ol.progtrckr[data-progtrckr-steps="2"] li {
                width: 25%;
            }

            ol.progtrckr[data-progtrckr-steps="3"] li {
                width: 25%;
            }

            ol.progtrckr[data-progtrckr-steps="4"] li {
                width: 25%;
            }
            /*ol.progtrckr[data-progtrckr-steps="5"] li { width: 19%; }
ol.progtrckr[data-progtrckr-steps="6"] li { width: 16%; }
ol.progtrckr[data-progtrckr-steps="7"] li { width: 14%; }
ol.progtrckr[data-progtrckr-steps="8"] li { width: 12%; }
ol.progtrckr[data-progtrckr-steps="9"] li { width: 11%; }*/

            ol.progtrckr li.progtrckr-done {
                color: black;
                border-bottom: 4px solid yellowgreen;
            }

            ol.progtrckr li.progtrckr-closed {
                color: black;
                border-bottom: 0px solid yellowgreen;
                width: 0%;
            }

            ol.progtrckr li.progtrckr-todo {
                color: silver;
                border-bottom: 4px solid silver;
            }

            ol.progtrckr li.progtrckr-todo-closed {
                color: silver;
                border-bottom: 0px solid silver;
                width: 0%;
            }

            ol.progtrckr li.progtrckr-current {
                color: black;
                border-bottom: 4px solid silver;
            }

            ol.progtrckr li:after {
                content: "\00a0\00a0";
            }

            ol.progtrckr li:before {
                position: relative;
                bottom: -2.5em;
                float: left;
                /*left: 50%;*/
                line-height: 1em;
            }

            ol.progtrckr li.progtrckr-closed:before {
                content: "\2714";
                color: white;
                background-color: yellowgreen;
                height: 1.2em;
                width: 1.2em;
                line-height: 1.2em;
                border: none;
                border-radius: 1.2em;
            }

            ol.progtrckr li.progtrckr-done:before {
                content: "\2714";
                color: white;
                background-color: yellowgreen;
                height: 1.2em;
                width: 1.2em;
                line-height: 1.2em;
                border: none;
                border-radius: 1.2em;
            }

            ol.progtrckr li.progtrckr-todo:before {
                content: "\039F";
                color: silver;
                background-color: white;
                font-size: 1.5em;
                bottom: -1.6em;
            }

            ol.progtrckr li.progtrckr-todo-closed:before {
                content: "\039F";
                color: silver;
                background-color: white;
                font-size: 1.5em;
                bottom: -1.6em;
            }

            ol.progtrckr li.progtrckr-current:before {
                content: "\039F";
                color: #A16BBE;
                background-color: white;
                font-size: 1.5em;
                bottom: -1.6em;
            }

            .clsPageNo {
            margin-top: 5%;
            margin-left: 60%;
            color: #666666;
           }

            .panel .panel-heading h2 {
             height: 100%;
             width: auto;
             display: inline-block;
             font-size: 18px;
             position: relative;
             margin: 0;
             line-height: 34px;
             font-weight: 400;
             letter-spacing: 0;
          }
    </style>

    <style>
        .scrolling-wrapper {
            overflow-x: auto;
            overflow-y: hidden;
            white-space: nowrap;
        }
    </style>

    <script>
        var view = $("#tslshow");
        var move = "100px";
        var sliderLimit = -750;


        $("#rightArrow").click(function () {
            // alert("right");
            var currentPosition = parseInt(view.css("left"));
            if (currentPosition >= sliderLimit) view.stop(false, true).animate({ left: "-=" + move }, { duration: 400 })

        });

        $("#leftArrow").click(function () {
            // alert("left");
            var currentPosition = parseInt(view.css("left"));
            if (currentPosition < 0) view.stop(false, true).animate({ left: "+=" + move }, { duration: 400 })

        });

    </script>

    <script type="text/javascript">

        $('#divShowDialogTermSheet').on("show", function () {
            $(this).find(".modal-body").css("max-height", height);
        });

        $('#divShowDialogTermSheet').on('show.bs.modal', function () {

            //alert("called");
            $('#divShowDialogTermSheet').find('.modal-body').css({
                width: 'auto', //probably not needed
                height: 'auto', //probably not needed 
                'max-height': '100%'
            });
        });
        function ShowDialogTermSheet(Flag, contractInitiatorID) {
            $('#divShowDialogTermSheet').modal('show');
            $('#showdetailsTermSheet').attr('width', '100%');
            $('#showdetailsTermSheet').attr('height', '900px');
            $('.modal-dialog').css('width', '100%');
            //$('#showdetails').attr('src', "/ContractProduct/aspxPages/ContractTermSheet.aspx");
            $('#showdetailsTermSheet').attr('src', "/ContractProduct/aspxPages/ContractTermSheetNew.aspx?Flag=" + Flag + "&contractInitiatorID=" + contractInitiatorID);
        };
    </script>

       <script>

           var ContID = "";
           function showApprovalRequestPopup(ID) {
               $("#divForRequestToEdit").kendoWindow({
                   modal: true,
                   pinned: true,
                   width: "54%",
                   height: "33%",
                   title: "Request to Edit",
                   visible: false,
                   draggable: false,
                   refresh: true,
                   actions: [
                       "Close"
                   ]

               }).data("kendoWindow").open().center();

               ContID = ID;

               $("#drpDownManagementUser").kendoDropDownList({
                   autoClose: true,
                   autoWidth: true,
                   filter: "contains",
                   dataTextField: "FirstName",
                   dataValueField: "ID",
                   template: '<span style="width:10px" class="k-state-default">#: data.FirstName#</span>' + " " +
                       '<span class="k-state-default">#: data.LastName#</span>',
                   valueTemplate: '<span class="selected-value">#: data.FirstName#</span>' + " " + '<span class="selected-value">#: data.LastName#</span>',
                   optionLabel: "Select User",
                   change: function (e) {
                   },
                   dataSource: {
                       severFiltering: true,
                       transport: {
                           read: {
                               url: "/Main/GetAllManagementUserForContract?&customerID=<% =CustId%>&RoleID=8&UserID=0",
                            dataType: "json",
                        }
                    },
                }
            });
           }

           function btnClearRequest_click() {
               $("#drpDownManagementUser").data("kendoDropDownList").select(0);
               $("#txtUserRemarks").val('');
           }

           function btnSendRequest_click() {
               if (($("#txtUserRemarks").val()).trim() == "") {
                   alert("Enter Remarks");
               }
               if ($("#drpDownManagementUser").val() == 0) {
                   alert("Select User");
               }
               if (($("#txtUserRemarks").val()).trim() != "" && $("#drpDownManagementUser").val() != 0) {
                   $.ajax({
                       contentType: 'application/json; charset=utf-8',
                       dataType: 'json',
                       type: 'POST',
                       url: '/Main/SaveContractApprovalStatus?ContractInstanceID=' + ContID + '&RequestStatus=Requested&Remarks=' + $("#txtUserRemarks").val() + '&CreatedBy=<%=UId%>&Validtill=&RequestToMGMTUserID=' + $("#drpDownManagementUser").val(),
                    // data: DetailsObj,
                    success: function (response) {
                        if (response == "Success") {
                            alert("Requested Sent Successfully");
                            if ($("#divForRequestToEdit").data("kendoWindow")) {
                                $("#divForRequestToEdit").data("kendoWindow").close();
                                btnClearRequest_click();
                                document.getElementById('<%= btnApprovalRequest.ClientID %>').click();
                            }
                        }
                        else {
                            alert("Error");
                        }
                    }
                });
            }
        }
       </script>
    
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="smContractDetailPage" EnablePartialRendering="true" runat="server" ScriptMode ="Release"></asp:ScriptManager>
        
        <asp:HiddenField ID="hdnSelectedVendors" runat="server" />
        <asp:HiddenField ID="hdnSelectedTaskUsers" runat="server" />
        <div>

        <div class="mainDiv" style="background-color: #f7f7f7;">
            <div class="col-md-10">
                <header class="panel-heading tab-bg-primary" style="background: none !important;">
                    <ul class="nav nav-tabs">
                        <li class="active" id="liContractSummary" runat="server">
                            <asp:LinkButton ID="lnkBtnContractSummary" OnClick="TabContract_Click" runat="server" CssClass="bgColor-gray">Summary</asp:LinkButton>
                        </li>

                        <li class="" id="liContractTransaction" runat="server">
                            <asp:LinkButton ID="lnkContractTransaction" OnClick="lnkContractTransaction_Click" CausesValidation="false" runat="server" Style="background-color: #f7f7f7;">Agreement</asp:LinkButton>
                        </li>
                        <li class="" id="liContractDetails" runat="server">
                            <asp:LinkButton ID="LnkBtnContractDetails" OnClick="TabContractDetail_Click" runat="server" CssClass="bgColor-gray">Detail(s)</asp:LinkButton>
                        </li>

                        <li class="" id="liDocument" runat="server">
                            <asp:LinkButton ID="lnkBtnDocument" OnClick="TabDocument_Click" runat="server" Style="background-color: #f7f7f7;">Document(s)</asp:LinkButton>
                        </li>
                        <li class="" id="liMilestone" runat="server">
                            <asp:LinkButton ID="lnkbtnMilestone" OnClick="lnkbtnMilestone_Click" CausesValidation="false" runat="server" Style="background-color: #f7f7f7;">Milestone</asp:LinkButton>
                        </li>
                        <li class="" id="liTask" runat="server">
                            <asp:LinkButton ID="lnkBtnTask" OnClick="TabTask_Click" runat="server" Style="background-color: #f7f7f7;">Task(s)</asp:LinkButton>
                        </li>
                        <li class="" id="liAuditLog" runat="server">
                            <asp:LinkButton ID="lnkAuditLog" OnClick="TabAuditLog_Click" CausesValidation="false" runat="server" Style="background-color: #f7f7f7;">Audit Log(s)</asp:LinkButton>
                        </li>
                        
                       

                        
                    </ul>
                </header>
            </div>
            <div class="col-md-2 text-right" id="topButtons" runat="server">
                <%--<asp:UpdatePanel ID="upSendMailPop" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div style="margin-right:10px;">
                            <asp:LinkButton runat="server" ID="lnkLinkContract" OnClientClick="OpenContractLinkingPopup();"
                                 data-toggle="tooltip" data-placement="top" ToolTip="Link Contract with Other Contract(s)">
                            <img src="/Images/link-icon.png" alt="Link" />
                            </asp:LinkButton>

                            <asp:LinkButton runat="server" ID="lnkSendMailWithDoc" OnClientClick="OpenSendMailPopup();"
                                 data-toggle="tooltip" data-placement="top" ToolTip="Send E-Mail with Document(s)">
                            <img src="/Images/send-mail-icon.png" alt="Send" />
                            </asp:LinkButton>

                            <asp:LinkButton runat="server" ID="btnEditContractDetail" Style="margin-bottom: 10px;" OnClick="btnEditContractControls_Click"
                                 data-toggle="tooltip" data-placement="top" ToolTip="Edit Contract Detail(s)">
                            <img src="/Images/edit_icon_new.png" alt="Edit" />
                            </asp:LinkButton>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnEditContractDetail" />
                    </Triggers>
                </asp:UpdatePanel>--%>
            </div>

            <div class="clearfix"></div>

            <asp:UpdateProgress ID="updateProgress" runat="server">
                <ProgressTemplate>
                    <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.3;">
                        <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                            AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 40%; left: 40%;" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <asp:Button ID="btnApprovalRequest" Style="display: none;" runat="server" OnClick="btnApprovalRequest_Click" />
            <div id="divMainView">
                <asp:MultiView ID="MainView" runat="server">
                    <asp:View ID="ContractSummaryView" runat="server">
                        <div class="container">

                            <div id="divContractSummary" class="row Dashboard-white-widget">
                                <!--ContractDetail Panel Start-->
                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-default">

                                        <%--<div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;" data-toggle="tooltip" data-placement="bottom" title=" View Contract Detail(s)">
                                            <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseDivContractSummary">
                                                <a>
                                                    <h2></h2>
                                                </a>
                                                <div class="panel-actions">
                                                    <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseDivContractSummary">
                                                        <i class="fa fa-chevron-up"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>--%>

                                        <div id="collapseDivContractSummary" class="panel-collapse collapse in">
                                            <div class="row d-none">
                                                <div id="divContractStatus" style="width: 100%; margin-top: 5px;">
                                                    <asp:BulletedList class="progtrckr" ID="statusBulletedList" runat="server" BulletStyle="Numbered">
                                                    </asp:BulletedList>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <asp:ValidationSummary ID="VSContractPopup" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                                    ValidationGroup="ContractPopUpValidationGroup" />
                                                <asp:CustomValidator ID="cvContractPopUp" runat="server" EnableClientScript="False"
                                                    ValidationGroup="ContractPopUpValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                                            </div>

                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-md-12" id="Div3" runat="server">
                                                        <asp:UpdatePanel ID="upSendMailPop" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <div class="pull-right" style="margin-right: 10px;">
                                                                       <asp:LinkButton runat="server" ID="lnksharesummary" OnClientClick="OpenShareUserPopup();FetchUSerDetail();" OnClick="lnksharesummary_Click"
                                                                        data-toggle="tooltip" data-placement="top" ToolTip="Click to Share User">
                            <img src="/Images/share-icons.png"  alt="Link" />
                                                                    </asp:LinkButton>
                             <asp:LinkButton runat="server" ID="lnkexecutivesummary" OnClientClick="Openexecutivesummary();"
                                                                        data-toggle="tooltip" data-placement="top" ToolTip="Executive Summary">
                            <img src="/Images/executive_summary.png" alt="Link" />
                                                                    </asp:LinkButton>
                                                                   
                                                                    <asp:LinkButton runat="server" ID="lnkLinkContract" OnClientClick="OpenContractLinkingPopup();"
                                                                        data-toggle="tooltip" data-placement="top" ToolTip="Link Contract with Other Contract(s)">
                            <img src="/Images/link-icon.png" alt="Link" />
                                                                    </asp:LinkButton>

                                                                    <asp:LinkButton runat="server" ID="lnkSendMailWithDoc" OnClientClick="OpenSendMailPopup();"
                                                                        data-toggle="tooltip" data-placement="top" ToolTip="Send Email with Document(s)">
                            <img src="/Images/send-mail-icon.png" alt="Send" />
                                                                    </asp:LinkButton>

                                                                    <asp:LinkButton runat="server" ID="btnEditContractDetail" Style="margin-bottom: 10px;" OnClick="btnEditContractControls_Click"
                                                                        data-toggle="tooltip" data-placement="top" ToolTip="Edit Contract Detail(s)">
                            <img src="/Images/edit_icon_new.png" alt="Edit" />
                                                                    </asp:LinkButton>
                                                                    <% if (ShowContractInitiator == true)
                                                                        {%>
                                                                     <asp:LinkButton runat="server" ID="lnklinktotermsheet" autopostback="true" Visible="false" onclick="lnklinktotermsheet_Click"
                                                                        data-toggle="tooltip" data-placement="top" ToolTip="View linked Contract Initiator">
                             <img src="../../Images/View-icon-new.png" alt="View" title="" />
                                                                    </asp:LinkButton>
                                                                    <%} %>
                                                                </div>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:PostBackTrigger ControlID="btnEditContractDetail" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                </div>
                                                <asp:Panel ID="pnlContract" runat="server">
                                                    <div class="container plr0">


                                                        <div class="row">

                                                            <asp:UpdatePanel ID="upContractTypeSubType" runat="server" UpdateMode="Conditional">
                                                                <ContentTemplate>

                                                                    <div class="form-group required col-md-4">
                                                                        <label for="ddlContractType" class="control-label">Contract Type</label>
                                                                        <div class="col-md-11 col-sm-11 col-xs-11 plr0">
                                                                            <asp:DropDownListChosen runat="server" AutoPostBack="true" ID="ddlContractType" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                                class="form-control" Width="100%" DataPlaceHolder="Select Contract Type" OnSelectedIndexChanged="ddlContractType_SelectedIndexChanged">
                                                                            </asp:DropDownListChosen>
                                                                            <%--onchange="ddlContractTypeChange()"--%>
                                                                            <asp:RequiredFieldValidator ID="rfvContractCategory" ErrorMessage="Please Select Contract Type."
                                                                                ControlToValidate="ddlContractType" runat="server" ValidationGroup="ContractPopUpValidationGroup" Display="None" />
                                                                            <asp:LinkButton ID="lnkBtnRebind_Type" OnClick="lnkBtnRebind_Type_Click" Style="float: right; display: none;" Width="100%" runat="server"> 
                                                                            </asp:LinkButton>
                                                                        </div>
                                                                        <div class="col-md-1 col-sm-1 col-xs-1 plr0 mt5 text-right">
                                                                            <%-- <img id="lnkAddNewContractTypeModal" runat="server" src="/Images/add_icon_new.png"
                                                                                onclick="OpenAddNewTypePopup()" alt="Add" data-toggle="tooltip" data-placement="bottom" title="Add New Contract Type" />--%>
                                                                            <asp:ImageButton ID="lnkAddNewContractTypeModal" runat="server" ImageUrl="/Images/add_icon_new.png" OnClientClick="javascript:OpenAddNewTypePopup(); return false;"
                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip="Add New Contract Type" />
                                                                            <%--style="display: none;"--%>
                                                                        </div>
                                                                    </div>

                                                                     <% if (subtype == 1) {%>
                                                                             <div class="form-group required col-md-4">
                                                                          <%} else {%> 
                                                                            <div class="form-group col-md-4">
                                                                          <%}%> 
                                                                        <label for="ddlContractSubType" class="control-label">Contract Sub-Type</label>
                                                                        <div class="col-md-11 col-sm-11 col-xs-11 plr0">
                                                                            <asp:DropDownListChosen runat="server" ID="ddlContractSubType" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                                class="form-control" Width="100%" DataPlaceHolder="Select Contract Type">
                                                                            </asp:DropDownListChosen>
                                                                              <%--<% if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID ==customizedid) {%> 
                                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Please Select Contract Sub-Type."
                                                                                    ControlToValidate="ddlContractSubType" runat="server" ValidationGroup="ContractPopUpValidationGroup" Display="None" />
                                                                                  <%}%>--%>
                                                                            <asp:LinkButton ID="lnkBtnRebind_SubType" OnClick="lnkBtnRebind_SubType_Click" Style="float: right; display: none;" Width="100%" runat="server"> 
                                                                            </asp:LinkButton>
                                                                        </div>
                                                                        <div class="col-md-1 col-sm-1 col-xs-1 plr0 mt5 text-right">
                                                                            <%--<img id="lnkAddNewContractSubTypeModal" runat="server" src="/Images/add_icon_new.png"
                                                                                onclick="OpenAddNewSubTypePopup()" alt="Add" data-toggle="tooltip" data-placement="bottom" title="Add New Contract Sub-Type" />--%>

                                                                            <asp:ImageButton ID="lnkAddNewContractSubTypeModal" runat="server" ImageUrl="/Images/add_icon_new.png" OnClientClick="javascript:OpenAddNewSubTypePopup(); return false;"
                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip="Add New Contract Sub-Type" />
                                                                        </div>
                                                                    </div>
                                                                </ContentTemplate>
                                                                <Triggers>
                                                                    <%--<asp:AsyncPostBackTrigger ControlID="ddlContractType" EventName="SelectedIndexChanged" />--%>
                                                                    <%-- <asp:PostBackTrigger  ControlID="ddlContractType" />--%>
                                                                </Triggers>
                                                            </asp:UpdatePanel>

                                                            <div class="form-group required col-md-4">
                                                                <label for="tbxBranch" class="control-label">Entity/Branch/Location</label>
                                                                <asp:TextBox runat="server" ID="tbxBranch" CssClass="form-control bgColor-white" ReadOnly="true" autocomplete="off" />

                                                                <div style="position: absolute; z-index: 10; width: 92%" id="divBranches">
                                                                    <asp:TreeView runat="server" ID="tvBranches" BackColor="White" BorderColor="Black"
                                                                        BorderWidth="1" SelectedNodeStyle-Font-Bold="true" Height="150px" OnSelectedNodeChanged="tvBranches_SelectedNodeChanged"
                                                                        Style="overflow: auto; margin-top: -20px; border: 1px solid #c7c7cc; background-color: #ffffff; color: #8e8e93 !important;"
                                                                        ShowLines="true">
                                                                    </asp:TreeView>
                                                                </div>
                                                                <asp:RequiredFieldValidator ID="rfvBranch" ErrorMessage="Please Select Entity/Location." InitialValue="Select Entity/Location"
                                                                    ControlToValidate="tbxBranch" runat="server" ValidationGroup="ContractPopUpValidationGroup" Display="None" />
                                                            </div>

                                                    </div>

                                                        <div class="row">
                                                            
                                                            
                                                            <div class="form-group col-md-4" id="divtemplateID" runat="server">
                                                                <label for="drdTemplateID" class="control-label">Template</label>
                                                                <asp:DropDownListChosen runat="server" ID="drdTemplateID" AllowSingleDeselect="false"
                                                                DataPlaceHolder="Select Template" DisableSearchThreshold="5"
                                                                    class="form-control" Width="100%" />                                                                  
                                                            </div>

                                                            <div class="form-group required col-md-4">
                                                                <label for="ddlContractStatus" class="control-label">Contract Status</label>
                                                                <asp:DropDownListChosen runat="server" ID="ddlContractStatus" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                    class="form-control" Width="100%" onchange="ddlContractStatusChange();" />
                                                            </div>

                                                            <div class="form-group required col-md-4">
                                                                <label for="txtContractNo" class="control-label">Contract Number</label>
                                                                <asp:TextBox runat="server" ID="txtContractNo" CssClass="form-control" autocomplete="off" />
                                                                <asp:RequiredFieldValidator ID="rfvContractNo" ErrorMessage="Please Enter Contract Number."
                                                                    ControlToValidate="txtContractNo" runat="server" ValidationGroup="ContractPopUpValidationGroup" Display="None" />
                                                            </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="form-group required col-md-12">
                                                                    <label for="txtTitle" class="control-label">Contract Title</label>
                                                                <asp:TextBox runat="server" ID="txtTitle" CssClass="form-control" autocomplete="off" />
                                                                <asp:RequiredFieldValidator ID="rfvContractTitle" ErrorMessage="Please Enter Contract Title."
                                                                    ControlToValidate="txtTitle" runat="server" ValidationGroup="ContractPopUpValidationGroup"
                                                                    Display="None" />
                                                                </div>
                                                            </div>
                                                        <div class="row">
                                                            <div class="form-group required col-md-12">
                                                                <label for="tbxDescription" class="control-label">Description</label>
                                                                <asp:TextBox runat="server" ID="tbxDescription" TextMode="MultiLine" CssClass="form-control" autocomplete="off" />
                                                                <asp:RequiredFieldValidator ID="rfvContractDesc" ErrorMessage="Please Enter Contract Description."
                                                                    ControlToValidate="tbxDescription" runat="server" ValidationGroup="ContractPopUpValidationGroup"
                                                                    Display="None" />
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            

                                                            <div class="form-group required col-md-4">
                                                                <label for="lstBoxVendor" class="control-label  col-md-12 col-sm-12 col-xm-12 colpadding0">Vendor</label>
                                                                <div class="col-md-11 col-sm-11 col-xs-11 plr0">
                                                                    
                                                                    <asp:ListBox ID="lstBoxVendor" CssClass="form-control" runat="server" SelectionMode="Multiple" Width="100%"></asp:ListBox>
                                                                    <%--onchange="ddlVendorChange()"--%>
                                                                    <asp:RequiredFieldValidator ID="rfvVendor" ErrorMessage="Please Select Vendor."
                                                                        ControlToValidate="lstBoxVendor" runat="server" ValidationGroup="ContractPopUpValidationGroup" Display="None" />
                                                                    <asp:LinkButton ID="lnkBtnRebind_Vendor" OnClick="lnkBtnRebind_Vendor_Click" Style="float: right; display: none;" Width="100%" runat="server"> 
                                                                    </asp:LinkButton>
                                                                </div>
                                                                <div class="col-md-1 col-sm-1 col-xs-1 plr0 mt5 text-right">
                                                                    <%-- <label for="lnkShowAddNewVendorModal" class="hidden-label">A</label>--%>
                                                                    <%--<img id="lnkShowAddNewVendorModal" runat="server" src="/Images/add_icon_new.png" onclick="OpenAddNewVendorPopup()"
                                                                        alt="Add" data-toggle="tooltip" data-placement="bottom" title="Add New Vendor" />--%>
                                                                    <asp:ImageButton ID="lnkShowAddNewVendorModal" runat="server" ImageUrl="/Images/add_icon_new.png" OnClientClick="javascript:OpenAddNewVendorPopup(); return false;"
                                                                        data-toggle="tooltip" data-placement="bottom" ToolTip="Add New Vendor" />
                                                                    <%--style="display: none;"--%>
                                                                </div>
                                                            </div>
                                                            <div class="form-group required col-md-4">
                                                                <label for="ddlDepartment" class="control-label">Department</label>
                                                                <div class="col-md-11 col-sm-11 col-xs-11 plr0">
                                                                    <asp:DropDownListChosen runat="server" ID="ddlDepartment" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                        DataPlaceHolder="Select Department" class="form-control" Width="100%" />
                                                                    <%--onchange="ddlDepartmentChange()" --%>
                                                                    <asp:RequiredFieldValidator ID="rfvDept" ErrorMessage="Please Select Department." 
                                                                        ControlToValidate="ddlDepartment" runat="server" ValidationGroup="ContractPopUpValidationGroup" />
                                                                   <asp:LinkButton ID="lnkBtnRebind_Dept" OnClick="lnkBtnRebind_Dept_Click" Style="float: right;" Width="100%" runat="server"> 
                                                                    </asp:LinkButton> 
                                                                </div>
                                                                <div class="col-md-1 col-sm-1 col-xs-1 plr0 mt5 text-right">
                                                                    <%-- <img id="lnkAddNewDepartmentModal" runat="server" src="/Images/add_icon_new.png" onclick="OpenDepartmentPopup('')"
                                                                        alt="Add" data-toggle="tooltip" data-placement="bottom" title="Add New Department" />--%>
                                                                    <asp:ImageButton ID="lnkAddNewDepartmentModal" runat="server"  Style="display:none;" ImageUrl="/Images/add_icon_new.png" OnClientClick="javascript:OpenDepartmentPopup(''); return false;"
                                                                        data-toggle="tooltip" data-placement="bottom" ToolTip="Add New Department" />
                                                                </div>
                                                            </div>
                                                        
                                                         <div class="form-group col-md-4">
                                                             <%if (RenameNoticeTerm == true) %>
                                                             <%{ %>
                                                              <label for="txtNoticeTerm" class="control-label">Notice Period</label>
                                                             <%} %>
                                                              <%else if (RenameNoticeTerm != true) %>
                                                              <%{ %>
                                                              <label for="txtNoticeTerm" class="control-label">Notice Term</label>
                                                              <%} %>
                                                               
                                                                <div class="input-group">
                                                                    <asp:TextBox ID="txtNoticeTerm" runat="server" class="form-control col-md-8" autocomplete="off" onkeydown="return ((event.keyCode>=65 && event.keyCode>=96 && event.keyCode<=105)||!(event.keyCode>=65) && event.keyCode!=32);" />
                                                                    <%--onkeydown = "return (!(event.keyCode>=65) && event.keyCode!=32);"--%>
                                                                    <div class="input-group-btn">
                                                                        <asp:DropDownList ID="ddlNoticeTerm" runat="server" CssClass="form-control col-md-4"
                                                                            Style="padding-right:9px;border-bottom-right-radius: 4px; border-top-right-radius: 4px; border-left: none;">
                                                                            <asp:ListItem Text="Days" Value="1" Selected="True"></asp:ListItem>
                                                                            <asp:ListItem Text="Weeks" Value="2"></asp:ListItem>
                                                                            <asp:ListItem Text="Months" Value="3"></asp:ListItem>
                                                                            <asp:ListItem Text="Years" Value="4"></asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        
                                                        </div>
                                                       
                                                        <div class="row">

                                                              <div class="form-group col-md-4">
                                                                <label for="ddlCPDepartment" class="control-label">Contact Person of Department</label>
                                                                  <label id="lblddlCPDepartment" runat="server" style="color: red;">*</label>
                                                                <div class="col-md-11 col-sm-11 col-xs-11 plr0">
                                                                    <asp:DropDownListChosen runat="server" ID="ddlCPDepartment" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                        DataPlaceHolder="Select Contact Person of Department " class="form-control" Width="100%" />
                                                                    <%--onchange="ddlCPDepartmentChange()" --%>
                                                                  <%--  <asp:RequiredFieldValidator ID="rfvCPDepartment" ErrorMessage="Please Select Contact Person of Department."
                                                                        ControlToValidate="ddlCPDepartment" runat="server" ValidationGroup="ContractPopUpValidationGroup" Display="None" />--%>
                                                                    <asp:LinkButton ID="lnkBtnRebind_ContactDept" OnClick="lnkBtnRebind_ContactPersonDept_Click" Style="float: right; display: none;" Width="100%" runat="server"> 
                                                                    </asp:LinkButton> 
                                                                </div>

                                                                  <div class="col-md-1 col-sm-1 col-xs-1 plr0 mt5 text-right">
                                                                    <%-- <img id="lnkAddNewDepartmentModal" runat="server" src="/Images/add_icon_new.png" onclick="OpenDepartmentPopup('')"
                                                                        alt="Add" data-toggle="tooltip" data-placement="bottom" title="Add New Department" />--%>
                                                                    <asp:ImageButton ID="lnkAddNewPDepartmentModal" runat="server" ImageUrl="/Images/add_icon_new.png" OnClientClick="javascript:OpenDepartmentPPopup(''); return false;"
                                                                        data-toggle="tooltip" data-placement="bottom" ToolTip="Add New Contact Person of Department" />
                                                                </div>
                                                                  </div>

                                                            <div class="form-group col-md-4">
                                                                <label for="txtProposalDate" class="control-label">Proposal Date</label>
                                                                <div class="input-group date">
                                                                    <span class="input-group-addon">
                                                                        <span class="fa fa-calendar color-black"></span>
                                                                    </span>
                                                                    <asp:TextBox ID="txtProposalDate" runat="server" placeholder="DD-MM-YYYY" class="form-control" autocomplete="off" />
                                                                </div>
                                                            </div>

                                                            <div class="form-group col-md-4">
                                                                <label for="txtAgreementDate" class="control-label">Agreement Date</label>
                                                                <div class="input-group date">
                                                                    <span class="input-group-addon">
                                                                        <span class="fa fa-calendar color-black"></span>
                                                                    </span>
                                                                    <asp:TextBox ID="txtAgreementDate" runat="server" placeholder="DD-MM-YYYY" class="form-control" autocomplete="off" />
                                                                </div>
                                                            </div>

                                                            
                                                        </div>

                                                        <div class="row">

                                                            <div class="form-group col-md-4">
                                                                <label for="txtEffectiveDate" class="control-label">Start Date</label>
                                                                <div class="input-group date">
                                                                    <span class="input-group-addon">
                                                                        <span class="fa fa-calendar color-black"></span>
                                                                    </span>
                                                                    <asp:TextBox ID="txtEffectiveDate" runat="server" placeholder="DD-MM-YYYY" class="form-control" autocomplete="off" />
                                                                </div>
                                                            </div>

                                                            <div class="form-group col-md-4">
                                                                <label for="txtAgreementDate" class="control-label">Review Date</label>
                                                                <div class="input-group date">
                                                                    <span class="input-group-addon">
                                                                        <span class="fa fa-calendar color-black"></span>
                                                                    </span>
                                                                    <asp:TextBox ID="txtReviewDate" runat="server" placeholder="DD-MM-YYYY" class="form-control" autocomplete="off" />
                                                                </div>
                                                            </div>

                                                            <div class="form-group col-md-4 d-none">
                                                                <label for="rbContractEndType" class="control-label">&nbsp;</label>
                                                                <asp:RadioButtonList ID="rbContractEndType" runat="server" RepeatDirection="Horizontal">
                                                                    <asp:ListItem class="radio-inline" Text="End Date" Value="E" Selected="True"></asp:ListItem>
                                                                    <asp:ListItem class="radio-inline" Text="Duration " Value="D"></asp:ListItem>
                                                                </asp:RadioButtonList>
                                                            </div>

                                                            <div class="form-group  col-md-4" id="divExpirationDate">
                                                                <label for="txtExpirationDate" class="control-label">End Date</label>
                                                                <div class="input-group date">
                                                                    <span class="input-group-addon">
                                                                        <span class="fa fa-calendar color-black"></span>
                                                                    </span>
                                                                    <asp:TextBox ID="txtExpirationDate" runat="server" placeholder="DD-MM-YYYY" class="form-control" autocomplete="off" />
                                                                </div> </div>

                                                            <div class="form-group col-md-4" id="divDuration" style="display: none;">
                                                                <label for="txtDuration">Duration</label>
                                                                <div class="input-group">
                                                                    <asp:TextBox ID="txtDuration" runat="server" placeholder="" class="form-control col-md-8" autocomplete="off" />
                                                                    <div class="input-group-btn">
                                                                        <asp:DropDownList ID="ddlDuration" runat="server" CssClass="form-control col-md-4">
                                                                            <asp:ListItem Text="Days" Value="D" Selected="True"></asp:ListItem>
                                                                            <asp:ListItem Text="Weeks" Value="W"></asp:ListItem>
                                                                            <asp:ListItem Text="Months" Value="M"></asp:ListItem>
                                                                            <asp:ListItem Text="Years" Value="Y"></asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                           
                                                        </div>

                                                        <div class="row">

                                                            

                                                          

                                                            
                                                        </div>

                                                        <div class="row">

                                                            <div class="form-group required col-md-4">
                                                                <label for="lstBoxOwner" class="control-label" id="lblBoxOwner" runat="server">Contract Owner(s)</label>
                                                                <asp:ListBox ID="lstBoxOwner" CssClass="form-control" runat="server" SelectionMode="Multiple"></asp:ListBox>
                                                                <asp:RequiredFieldValidator ID="rfvOwner" ErrorMessage="Please Select Owner."
                                                                    ControlToValidate="lstBoxOwner" runat="server" ValidationGroup="ContractPopUpValidationGroup" Display="None" />
                                                            </div>
                                                            <div class="form-group col-md-4">
                                                                <label for="tbxContractAmt" class="control-label">Contract Amount/Value</label>
                                                                <asp:TextBox runat="server" ID="tbxContractAmt" CssClass="form-control" autocomplete="off" />
                                                                <asp:RegularExpressionValidator ID="revContractAmt" Display="None" runat="server"
                                                                    ValidationGroup="ContractPopUpValidationGroup" ErrorMessage="Please enter a valid Contract Amount/Value"
                                                                    ControlToValidate="tbxContractAmt" ValidationExpression="[0-9]+(\.[0-9][0-9]?)?"></asp:RegularExpressionValidator>
                                                            </div>

                                                           
                                                             <div class="form-group col-md-4">
                                                                <label for="tbxTaxes" class="control-label">Taxes</label>
                                                                <asp:TextBox runat="server" ID="tbxTaxes" CssClass="form-control" autocomplete="off" />
                                                                <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="None" runat="server"
                                                                    ValidationGroup="ContractPopUpValidationGroup" ErrorMessage="Please enter a valid Contract Amount/Value"
                                                                    ControlToValidate="tbxContractAmt" ValidationExpression="[0-9]+(\.[0-9][0-9]?)?"></asp:RegularExpressionValidator>--%>
                                                            </div>
                                                            

                                                            
                                                            </div>
                                                        <div class="row">
                                                            <div class="form-group col-md-4">
                                                                <label for="ddlPaymentType" class="control-label">Payment Type</label>
                                                                <asp:DropDownListChosen runat="server" ID="ddlPaymentType" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                    class="form-control" Width="100%" DataPlaceHolder="Select Payment Type ">
                                                                    <asp:ListItem Text="Select Payment Type" Value="-1" Selected="True"></asp:ListItem>
                                                                    <asp:ListItem Text="Payee" Value="0"></asp:ListItem>
                                                                    <asp:ListItem Text="Receipt" Value="1"></asp:ListItem>

                                                                </asp:DropDownListChosen>
                                                                <asp:RequiredFieldValidator ID="rfvPaymentType" ErrorMessage="Please Select Payment Type."
                                                                    ControlToValidate="ddlPaymentType" runat="server" ValidationGroup="ContractPopUpValidationGroup" Display="None" />
                                                            </div>

                                                            <div class="form-group col-md-4">
                                                                <label for="ddlPaymentTerm1" class="control-label">Payment Term</label>
                                                                
                                                                <asp:ListBox ID="ddlPaymentTerm1" CssClass="form-control" runat="server" SelectionMode="Multiple" Width="100%">

                                                                </asp:ListBox>
                                                                <%--<asp:DropDownListChosen runat="server" ID="ddlPaymentTerm" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                    class="form-control" Width="100%" DataPlaceHolder="Select Payment Term">
                                                                    <asp:ListItem Text="One-Time" Value="0" Selected="True"></asp:ListItem>
                                                                    <asp:ListItem Text="Daily" Value="1"></asp:ListItem>
                                                                    <asp:ListItem Text="Weekly" Value="7"></asp:ListItem>
                                                                    <asp:ListItem Text="Periodically" Value="15"></asp:ListItem>
                                                                    <asp:ListItem Text="Monthly" Value="30"></asp:ListItem>
                                                                    <asp:ListItem Text="Quarterly" Value="4"></asp:ListItem>
                                                                    <asp:ListItem Text="Half-Yearly" Value="6"></asp:ListItem>
                                                                    <asp:ListItem Text="Yearly" Value="12"></asp:ListItem>
                                                                </asp:DropDownListChosen>--%>
                                                               <%-- <asp:RequiredFieldValidator ID="rfvPaymentTerm" ErrorMessage="Please Select Payment Term"
                                                                    ControlToValidate="ddlPaymentTerm" runat="server" ValidationGroup="ContractPopUpValidationGroup" Display="None" />--%>
                                                            </div>

                                                            <div class="form-group col-md-4">
                                                                <label for="txtBoxProduct" class="control-label">Product/Items(s)</label>
                                                                <asp:TextBox ID="txtBoxProduct" CssClass="form-control" runat="server"></asp:TextBox>
                                                            </div>

                                                            <div class="form-group required col-md-4 d-none">
                                                                <label for="lstBoxApprover" class="control-label">Contract Approver(s)</label>
                                                                <asp:ListBox ID="lstBoxApprover" CssClass="form-control" runat="server" SelectionMode="Multiple"></asp:ListBox>
                                                                <%-- <asp:RequiredFieldValidator ID="rfvApprover" ErrorMessage="Please Select Approver"
                                                                    ControlToValidate="lstBoxApprover" runat="server" ValidationGroup="ContractPopUpValidationGroup" Display="None" />--%>
                                                            </div>
                                                        </div>
                                                         <div class="row">
                                                             <div class="form-group col-md-4">
                                                                <label for="txtlockinperiodDate" class="control-label">Lock-in Period</label>
                                                                <div class="input-group date">
                                                                    <span class="input-group-addon">
                                                                        <span class="fa fa-calendar color-black"></span>
                                                                    </span>
                                                                    <asp:TextBox ID="txtlockinperiodDate" runat="server" placeholder="DD-MM-YYYY" class="form-control" autocomplete="off" />
                                                                </div>
                                                            </div>
                                                             <div  runat="server" id="IsApproverReviewer">
                                                               <div class="form-group required col-md-4">
                                                                <label for="ddlReviewer" class="control-label">Reviewer</label>
                                                                     <asp:DropDownListChosen runat="server" ID="ddlReviewer" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                        DataPlaceHolder="Select Reviewer" class="form-control" Width="100%" />
                                                            </div>
                                                             <div class="form-group required col-md-4">
                                                                <label for="ddlApprover" class="control-label">Approver</label>
                                                                                                                                  
                                                                        <asp:DropDownListChosen runat="server" ID="ddlApprover" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                        DataPlaceHolder="Select Approver " class="form-control" Width="100%" />
                                                              </div>
                                                            </div>
                                                         </div>
                                                         <div class="row">
                                                            <div class="form-group  col-md-12">
                                                                <%--<label for="tbxAddNewClause" class="control-label">Add New Clause</label>--%>
                                                                <label for="tbxAddNewClause" class="control-label">Remark</label>
                                                                <asp:TextBox runat="server" ID="tbxAddNewClause" TextMode="MultiLine" CssClass="form-control" autocomplete="off" />
                                                                
                                                            </div>
                                                        </div>
                                                        <div id="uploaddocsection" runat="server">
                                                           <div class="row">
                                                            <div class="form-group  col-md-4">
                                                                   <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                                                                    <ContentTemplate>
                                                                 <label class="control-label">Upload Document(s)</label>
                                                                <asp:FileUpload ID="fuSampleFile" Multiple="Multiple" runat="server" 
                                                                    onchange="fFilesubmit()" Style="color: black" />
                                                                           </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </div>
                                                              <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                                                                    <ContentTemplate>
                                                                        <asp:Button ID="UploadDocument" runat="server" Text="Upload Document"  Style="display: none;" OnClick="UploadDocument_Click"
                                                                            class="btn btn-search" data-toggle="tooltip" data-placement="top" ToolTip="Upload Document"
                                                                            CausesValidation="true" />
                                                                    </ContentTemplate>
                                                                  <Triggers>
                                                                      <asp:PostBackTrigger ControlID="UploadDocument" />
                                                                  </Triggers>
                                                                </asp:UpdatePanel>
                                                        </div>
                                                        <div class="row">
                                                             <div class="form-group  col-md-12">
                                                                    <asp:GridView runat="server" ID="grdDocument" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                                    PageSize="100" AllowPaging="true" OnRowCommand="grdDocument_RowCommand" OnRowDataBound="grdDocument_RowDataBound" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%">
                                                                    <Columns>
                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="2%">
                                                                            <ItemTemplate>
                                                                                <%#Container.DataItemIndex+1 %>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Document Name" ItemStyle-Width="10%">
                                                                            <ItemTemplate>
                                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 500px;">
                                                                                    <asp:Label ID="lblDocumentName" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("DocName") %>' ToolTip='<%# Eval("DocName") %>'></asp:Label>                                                                                    
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                           <asp:TemplateField HeaderText="Action" HeaderStyle-CssClass="text-right" ItemStyle-CssClass="text-right" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="8%">
                                                                            <ItemTemplate>
                                                                                <asp:UpdatePanel ID="upgrid" runat="server" UpdateMode="Conditional">
                                                                                    <ContentTemplate>
                                                                                        <asp:LinkButton ID="lnkDownloadDocument" runat="server" CommandName="Download Document" ToolTip="Download Document" data-toggle="tooltip"
                                                                                            CommandArgument='<%# Eval("Id") %>'>
                                                        <img src='<%# ResolveUrl("~/Images/download_icon_new.png")%>' alt="Download Document" /></asp:LinkButton>
                                                                                        <asp:LinkButton ID="lnkViewDocument" runat="server" CommandName="View Document" ToolTip="View Document" data-toggle="tooltip"
                                                                                            CommandArgument='<%# Eval("Id") %>'>
                                                        <img src='<%# ResolveUrl("~/Images/View-icon-new.png")%>' alt="View Document" /></asp:LinkButton>
                                                                                        <asp:LinkButton ID="lnkDeleteDocument" runat="server" CommandName="Delete Document" ToolTip="Delete Document" data-toggle="tooltip"
                                                                                            CommandArgument='<%# Eval("Id") %>'>
                                                        <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="Delete Document" /></asp:LinkButton>
                                                                                    </ContentTemplate>
                                                                                    <Triggers>
                                                                                        <asp:PostBackTrigger ControlID="lnkDownloadDocument" />
                                                                                    </Triggers>
                                                                                </asp:UpdatePanel>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                    <RowStyle CssClass="clsROWgrid" />
                                                                    <HeaderStyle CssClass="clsheadergrid" />
                                                                    <PagerSettings Visible="false" />
                                                                    <PagerTemplate>
                                                                    </PagerTemplate>
                                                                    <EmptyDataTemplate>
                                                                        No Record Found
                                                                    </EmptyDataTemplate>
                                                                </asp:GridView>
                                                                 </div>
                                                        </div>
                                                        </div>
                                                            <div id="statusandcommentsection" runat="server">
                                                          <div class="row">
                                                            <div class="form-group col-md-4">
                                                               
                                                                <label for="ddlstatus" class="control-label">Status</label>
                                                                     <asp:DropDownListChosen runat="server" ID="ddlstatus" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                        DataPlaceHolder="Select Status" class="form-control" Width="100%" />
                                                                         
                                                            </div>
                                                        </div>
                                                         <div class="row" style="display:none;">
                                                            <div class="form-group  col-md-12">
                                                                <label for="tbxComment" class="control-label">Comment</label>
                                                                <asp:TextBox runat="server" ID="tbxComment" TextMode="MultiLine" CssClass="form-control" autocomplete="off" />
                                                                
                                                            </div>
                                                        </div>
                                                               
                                                            </div>

                                                       

                                                      <div class="row">
                                                           <div class="form-group col-md-12">
                                                                <label for="grdCustomField" id="lblCustomField" style="display:none;" runat="server" class="control-label">Additional Field(s) According to Contract Type</label>
                                                                <asp:UpdatePanel ID="upCustomField" runat="server">
                                                                    <ContentTemplate>
                                                                        <asp:GridView runat="server" ID="grdCustomField" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                                            GridLines="None" AllowPaging="true" AutoPostBack="true" CssClass="table" ShowFooter="true" ShowHeader="false"
                                                                            PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Right" OnRowCommand="grdCustomField_Common_RowCommand"
                                                                            OnRowDataBound="grdCustomField_Common_RowDataBound">
                                                                            <Columns>
                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr.No." ItemStyle-Width="5%" Visible="false">
                                                                                    <ItemTemplate>
                                                                                        <%#Container.DataItemIndex+1 %>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField Visible="false">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblID" runat="server" Text='<%# Eval("LableID") %>'
                                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("LableID") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-Width="45%" FooterStyle-Width="45%" FooterStyle-CssClass="colpadding0"><%--HeaderText="Field"--%>
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; margin-top: 5px;">
                                                                                            <asp:Label ID="lblName" runat="server" Text='<%# Eval("Label") %>'
                                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Label") %>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                    <FooterTemplate>
                                                                                        <asp:DropDownListChosen runat="server" ID="ddlFieldName_Footer"     CssClass="plr0 unique-footer-select" DataPlaceHolder="Select"
                                                                                            AllowSingleDeselect="false" DisableSearchThreshold="5" Width="100%">
                                                                                            <%--onchange="ddlCustomFieldChange()"--%>
                                                                                        </asp:DropDownListChosen>
                                                                                        <%--<img id="imgAddNewCustomField" src='<%# ResolveUrl("~/Images/add_icon_new.png")%>'
                                                                                            onclick="OpenAddNewCustomFieldPopUp()" alt="Add" data-toggle="tooltip" data-placement="bottom" title=" Add New Custom Field" />--%>
                                                                                    </FooterTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-Width="0.5%" FooterStyle-Width="0.5%" FooterStyle-CssClass="text-center">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblImgAddNew" runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                    <FooterTemplate>
                                                                                        <img id="imgAddNewCustomField" src='<%# ResolveUrl("~/Images/add_icon_new.png")%>' onclick="OpenAddNewCustomFieldPopUp()" alt="Add"
                                                                                            data-toggle="tooltip" data-placement="bottom" title="Add New Custom Field" class="mt5" />
                                                                                    </FooterTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-Width="45%" FooterStyle-Width="45%"><%--HeaderText="Value"--%>
                                                                                    <ItemTemplate>
                                                                                        <asp:TextBox ID="tbxLabelValue" runat="server" CssClass="form-control" PlaceHolder="Value" ReadOnly="true" Text='<%# Eval("labelValue") %>'></asp:TextBox>
                                                                                    </ItemTemplate>
                                                                                    <FooterTemplate>
                                                                                        <asp:TextBox runat="server" AutoPostBack="true" ID="txtFieldValue_Footer" PlaceHolder="Value"   CssClass="form-control" />
                                                                                    </FooterTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-Width="4.5%" FooterStyle-Width="4.5%"
                                                                                    ItemStyle-CssClass="text-right" HeaderStyle-CssClass="text-right" FooterStyle-CssClass="text-right"><%--HeaderText="Action"--%>
                                                                                    <ItemTemplate>
                                                                                        <asp:UpdatePanel runat="server" ID="upCustomFieldDelete" UpdateMode="Conditional" class="mt5">
                                                                                            <ContentTemplate>
                                                                                                <asp:LinkButton CommandArgument='<%# Eval("LableID")%>' ID="lnkBtnDeleteCustomField"
                                                                                                    AutoPostBack="true" CommandName="DeleteCustomField" runat="server"
                                                                                                    data-toggle="tooltip" data-placement="bottom" ToolTip="Delete"
                                                                                                    OnClientClick="return confirm('Are you sure! You want to Delete this record?');">
                                                                                                    <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="Delete" />
                                                                                                </asp:LinkButton>
                                                                                            </ContentTemplate>
                                                                                        </asp:UpdatePanel>
                                                                                    </ItemTemplate>
                                                                                    <FooterTemplate>
                                                                                        <asp:LinkButton ID="lnkBtnAddCustomField" runat="server" AutoPostBack="true" OnClick="lnkBtnAddCustomField_Click"
                                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip="Save Custom Field With Value"
                                                                                            Text="Add" CssClass="btn btn-primary">                                                                                           
                                                                                        </asp:LinkButton>
                                                                                        <%--<img src='<%# ResolveUrl("~/Images/add_icon_new.png")%>' alt="Save" />--%>
                                                                                    </FooterTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                            <%--<RowStyle CssClass="clsROWgrid" />--%>
                                                                            <%--<HeaderStyle CssClass="clsheadergrid" />--%>
                                                                            <EmptyDataTemplate>
                                                                                No Records Found
                                                                            </EmptyDataTemplate>
                                                                        </asp:GridView>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </div>
                                                        </div>
                                                        </div>
                                                    </div>
                                                </asp:Panel>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--Contract Detail Panel End-->
                                <div class="col-lg-12 col-md-12">
                                    <p style="color: red;"><strong>Note:</strong> (*) fields are compulsory</p>
                                </div>
                            </div>

                            <div class="form-group col-md-12 text-center">
                                 <asp:Button Text="Save Draft" runat="server" ID="btnsavedraft" OnClick="btnsavedraft_Click" CssClass="btn btn-primary"></asp:Button>
                                <asp:Button Text="Save" runat="server" ID="btnSaveContract" CssClass="btn btn-primary" OnClick="btnSaveContract_Click" OnClientClick="scrollUpPage()"
                                    ValidationGroup="ContractPopUpValidationGroup"></asp:Button>
                                <asp:Button Text="Clear" runat="server" ID="btnClearContractDetail" CssClass="btn btn-primary" OnClick="btnClearContractControls_Click" />
                            </div>

                        <div class="row" id="CommentContract" runat="server">
                            <div class="col-md-12" style="padding-top: 16px;">
                                <div class="panel panel-default" style="border: 1px solid;">
                                    <div class="panel-heading" style="padding: 4px 19px; background-color: #1fd9e1; color: white">Comments</div>
                                    <div class="panel-body" style="padding: 9px; overflow-y: scroll; min-height: 300px; max-height: 301px;">
                                        <div id="ShowDatadiv" style="min-height: 300px; color: black;"></div>
                                    </div>
                                    <div class="componentWraper">
                                        <div style="height: auto">
                                            <textarea id="TxtComments" style="width: 99%; border: none;" name="txtcmt" placeholder="Write a comment.."></textarea>
                                            <hr />
                                            <div>
                                                <label for="file-input">
                                                    <a style="cursor: pointer; font-size: x-large;" id="file"><i class="fa fa-paperclip" data-toggle="tooltip" title="Attach a file to the comment"></i></a>
                                                </label>
                                                <label id="selectedfile" style="color: black; margin-left: 32px;"></label>
                                                <%--<a onclick="submitData();" style="cursor: pointer; font-size: x-large;" id="file"><i class="fa fa-paperclip" data-toggle="tooltip" title="Attach file"></i></a>--%>
                                                <button onclick="submitData();" class="k-button" style="cursor: pointer; float: right; background: #304FFE; color: white; height: 32px; width: 93px; border: none;" id="MainSubmit">Comment</button>
                                            </div>
                                            <div class="image-upload" id="" style="display: none;">
                                                <input id="file-input" type="file" onchange="alertFilename()" style="padding-top: 10px;" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                            <div id="divContractHistory" runat="server" class="row Dashboard-white-widget" visible="false">
                                <!--Contract History Panel Start-->
                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-default">

                                        <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;" data-toggle="tooltip" data-placement="bottom" title="View Contract History">
                                            <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseDivContractHistoryDetails">
                                                <a>
                                                    <h2>Contract History</h2>
                                                </a>
                                                <div class="panel-actions">
                                                    <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion"
                                                        href="#collapseDivContractHistoryDetails">
                                                        <i class="fa fa-chevron-up"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="collapseDivContractHistoryDetails" class="panel-collapse collapse in">
                                            <div class="panel-body">
                                                <asp:Panel ID="Panel2" runat="server">
                                                    <div class="row">
                                                        <asp:UpdatePanel ID="upContractHistory" runat="server">
                                                            <ContentTemplate>
                                                                <asp:GridView runat="server" ID="grdContractHistory" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                                    GridLines="None" PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%"
                                                                    PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Right" OnRowCommand="grdContractHistory_RowCommand" OnPageIndexChanging="grdContractHistory_PageIndexChanging">

                                                                    <Columns>
                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr.No." ItemStyle-Width="5%">
                                                                            <ItemTemplate>
                                                                              <%#Container.DataItemIndex+1 %>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="Contract No" ItemStyle-Width="20%">
                                                                            <ItemTemplate>
                                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                                                    <asp:Label ID="lblContractNo" runat="server" Text='<%# Eval("ContractNo") %>'
                                                                                        data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("ContractNo") %>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="Contract Title" ItemStyle-Width="25%">
                                                                            <ItemTemplate>
                                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                                                    <asp:Label ID="lblContractTitle" runat="server" Text='<%# Eval("ContractTitle") %>'
                                                                                        data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("ContractTitle") %>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="Vendor" ItemStyle-Width="15%">
                                                                            <ItemTemplate>
                                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px">
                                                                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("VendorNames") %>' ToolTip='<%# Eval("VendorNames") %>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="End Date" ItemStyle-Width="15%" ItemStyle-CssClass="text-center">
                                                                            <ItemTemplate>
                                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px">
                                                                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ExpirationDate") != null? Convert.ToDateTime(Eval("ExpirationDate")).ToString("dd-MM-yyyy") : " " %>'
                                                                                        ToolTip='<%# Eval("ExpirationDate") != null ? Convert.ToDateTime(Eval("ExpirationDate")).ToString("dd-MM-yyyy") : " " %>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="Action" ItemStyle-Width="10%" ItemStyle-CssClass="text-right" HeaderStyle-CssClass="text-right">
                                                                            <ItemTemplate>
                                                                                <asp:UpdatePanel runat="server" ID="UpdateHist" UpdateMode="Always">
                                                                                    <ContentTemplate>
                                                                                        <asp:LinkButton CommandArgument='<%# Eval("ID")%>'
                                                                                            AutoPostBack="true" CommandName="ViewContractPopup" ID="lnkViewContractHistory" runat="server"
                                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip="View">
                                                                                         <img src='<%# ResolveUrl("~/Images/eye.png")%>' alt="View"/>
                                                                                        </asp:LinkButton>
                                                                                    </ContentTemplate>
                                                                                    <Triggers>
                                                                                        <%--<asp:PostBackTrigger ControlID="lnkBtnDownLoadCaseDoc" />
                                                                                                <asp:PostBackTrigger ControlID="lnkBtnDeleteCaseDoc" />--%>
                                                                                    </Triggers>
                                                                                </asp:UpdatePanel>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                    <RowStyle CssClass="clsROWgrid" />
                                                                    <HeaderStyle CssClass="clsheadergrid" />
                                                                    <EmptyDataTemplate>
                                                                        No Records Found
                                                                    </EmptyDataTemplate>
                                                                </asp:GridView>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                </asp:Panel>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <!--Contract History Panel End-->
                            </div>

                            <!--Linked Contracts Panel Start-->
                            <asp:UpdatePanel ID="upLinkedContracts" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div id="divLinkedContracts" runat="server" class="row Dashboard-white-widget" visible="false">

                                        <div class="col-lg-12 col-md-12">
                                            <div class="panel panel-default">

                                                <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;" data-toggle="tooltip" data-placement="bottom" title="View Linked Contract(s)">
                                                    <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseDivLinkedContractDetails">
                                                        <a>
                                                            <h2>Linked Contract(s)</h2>
                                                        </a>
                                                        <div class="panel-actions">
                                                            <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseDivLinkedContractDetails">
                                                                <i class="fa fa-chevron-up"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div id="collapseDivLinkedContractDetails" class="panel-collapse collapse in">
                                                    <div class="panel-body">
                                                        <div class="col-md-12 plr0">
                                                            <asp:ValidationSummary ID="vsLinkedContracts" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                                                ValidationGroup="LinkedContractsValidationGroup" />
                                                            <asp:CustomValidator ID="cvLinkedContracts" runat="server" EnableClientScript="False"
                                                                ValidationGroup="LinkedContractsValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                                                        </div>

                                                        <asp:Panel ID="Panel4" runat="server">
                                                            <div class="row">
                                                                <asp:GridView runat="server" ID="grdLinkedContracts" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                                    GridLines="None" AutoPostBack="true" CssClass="table" Width="100%"
                                                                    OnRowCommand="grdLinkedContracts_RowCommand" OnRowDataBound="grdLinkedContracts_RowDataBound">
                                                                    <Columns>
                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr.No." ItemStyle-Width="5%">
                                                                            <ItemTemplate>
                                                                                <%#Container.DataItemIndex+1 %>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="Contract No" ItemStyle-Width="20%">
                                                                            <ItemTemplate>
                                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px">
                                                                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ContractNo") %>' ToolTip='<%# Eval("ContractNo") %>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="Contract" ItemStyle-Width="30%">
                                                                            <ItemTemplate>
                                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                                                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ContractTitle") %>' ToolTip='<%# Eval("ContractTitle") %>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="Vendor" ItemStyle-Width="15%">
                                                                            <ItemTemplate>
                                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("VendorNames") %>' ToolTip='<%# Eval("VendorNames") %>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="Status" ItemStyle-Width="10%">
                                                                            <ItemTemplate>
                                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="Department" ItemStyle-Width="10%" Visible="false">
                                                                            <ItemTemplate>
                                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("DeptName") %>' ToolTip='<%# Eval("DeptName") %>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="Action" ItemStyle-Width="10%" ItemStyle-CssClass="text-right" HeaderStyle-CssClass="text-right">
                                                                            <ItemTemplate>
                                                                                <asp:UpdatePanel runat="server" ID="upLinkedContractAction" UpdateMode="Always">
                                                                                    <ContentTemplate>
                                                                                        <asp:LinkButton CommandArgument='<%# Eval("LinkedContractID") %>'
                                                                                            AutoPostBack="true" CommandName="ViewLinkedContract" ID="lnkViewLinkedContract" runat="server"
                                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip="View Contract Detail(s)">
                                                                                            <img src='<%# ResolveUrl("~/Images/View-icon-new.png")%>' alt="View"/>
                                                                                        </asp:LinkButton>

                                                                                        <asp:LinkButton CommandArgument='<%# Eval("ContractID")+","+Eval("LinkedContractID") %>'
                                                                                            AutoPostBack="true" CommandName="DeleteContractLinking" runat="server" ID="lnkBtnDeleteContractLinking"
                                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip="Delete this Contract Linking"
                                                                                            OnClientClick="return confirm('Are you sure! You want to Delete this Linking with Contract?');">
                                                                                            <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="Delete" />
                                                                                        </asp:LinkButton>
                                                                                    </ContentTemplate>
                                                                                </asp:UpdatePanel>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                    <RowStyle CssClass="clsROWgrid" />
                                                                    <HeaderStyle CssClass="clsheadergrid" />
                                                                    <EmptyDataTemplate>
                                                                        No Contract Linked yet
                                                                    </EmptyDataTemplate>
                                                                </asp:GridView>
                                                            </div>
                                                        </asp:Panel>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <!--Linked Contracts  Panel End-->

                            <div class="form-group col-md-12" style="margin-left: 10px; float: left;">
                                <%--<p style="color: red;"><strong>Note:</strong> (*) fields are compulsory</p>--%>
                            </div>
                        </div>
                    </asp:View>

                    <asp:View ID="ContractDetailsView" runat="server">
                        <div id="divContractDetail" class="row Dashboard-white-widget">
                            <div class=" row col-md-12 plr0">
                                <asp:ValidationSummary ID="vsContractDetails" runat="server" Display="none" style="margin-left: 26px;" class="alert alert-block alert-danger fade in"
                                    ValidationGroup="ContractDetailsValidationGroup" />
                                <asp:CustomValidator ID="cvContractDetails" runat="server" EnableClientScript="False"
                                    ValidationGroup="ContractDetailsValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                            </div>

                            <div class="row">
                                <div class="form-group col-md-12">
                                    <div class="form-group col-md-6">
                                        <label for="txtTerms" class="control-label">Terms</label>
                                        <asp:TextBox name="txtTerms" ID="txtTerms" runat="server" Rows="4" CssClass="form-control w100per" TextMode="MultiLine" />
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="txtTermination" class="control-label">Termination</label>
                                        <asp:TextBox name="txtTermination" ID="txtTermination" runat="server" Rows="4" CssClass="form-control w100per" TextMode="MultiLine" />
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-12">
                                    <div class="form-group col-md-6">
                                        <label for="txtMisc" class="control-label">Misc</label>
                                        <asp:TextBox name="txtMisc" ID="txtMisc" runat="server" Rows="4" CssClass="form-control w100per" TextMode="MultiLine" />
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="txtLicense" class="control-label">License</label>
                                        <asp:TextBox name="txtLicense" ID="txtLicense" runat="server" Rows="4" CssClass="form-control w100per" TextMode="MultiLine" />
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-12">
                                    <div class="form-group col-md-6">
                                        <label for="txtWarranties" class="control-label">Warranties</label>
                                        <asp:TextBox name="txtWarranties" ID="txtWarranties" runat="server" Rows="4" CssClass="form-control w100per" TextMode="MultiLine" />
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="txtGuarantees" class="control-label">Guarantees</label>
                                        <asp:TextBox name="txtGuarantees" ID="txtGuarantees" runat="server" Rows="4" CssClass="form-control w100per" TextMode="MultiLine" />
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-12">
                                    <div class="form-group col-md-6">
                                        <label for="txtConfidential" class="control-label">Confidential</label>
                                        <asp:TextBox name="txtConfidential" ID="txtConfidential" runat="server" Rows="4" CssClass="form-control w100per" TextMode="MultiLine" />
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="txtOwnership" class="control-label">Ownership</label>
                                        <asp:TextBox name="txtOwnership" ID="txtOwnership" runat="server" Rows="4" CssClass="form-control w100per" TextMode="MultiLine" />
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-12">
                                    <div class="form-group col-md-6">
                                        <label for="txtIndemnification" class="control-label">Indemnification</label>
                                        <asp:TextBox name="txtIndemnification" ID="txtIndemnification" runat="server" Rows="4" CssClass="form-control w100per" TextMode="MultiLine" />

                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="txtGeneral" class="control-label">General</label>
                                        <asp:TextBox name="txtGeneral" ID="txtGeneral" runat="server" Rows="4" CssClass="form-control w100per" TextMode="MultiLine" />
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-12">
                                    <div class="form-group col-md-6">
                                        <label for="txtTimelines" class="control-label">Timelines</label>
                                        <asp:TextBox name="txtTimelines" ID="txtTimelines" runat="server" Rows="4" CssClass="form-control w100per" TextMode="MultiLine" />

                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="txtEscalation" class="control-label">Escalation Clauses/Penalties</label>
                                        <asp:TextBox name="txtEscalation" ID="txtEscalation" runat="server" Rows="4" CssClass="form-control w100per" TextMode="MultiLine" />
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-12">
                                    <div class="form-group col-md-6">
                                        <label for="txtMilestones" class="control-label">Milestones with Details</label>
                                        <asp:TextBox name="txtMilestones" ID="txtMilestones" runat="server" Rows="4" CssClass="form-control w100per" TextMode="MultiLine" />
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for="txtOthers" class="control-label">Other(s)</label>
                                        <asp:TextBox name="txtOthers" ID="txtOthers" runat="server" Rows="4" CssClass="form-control w100per" TextMode="MultiLine" />
                                    </div>
                                </div>
                            </div>
                            <div class="row form-group col-md-12 text-center">
                                <asp:Button Text="Save" runat="server" ID="btnContractDetail" CssClass="btn btn-primary" OnClick="btnSaveContDetail_Click"></asp:Button>
                            </div>
                        </div>
                    </asp:View>

                    <!--Contract Document Panel Start-->
                    <asp:View ID="DocumentView" runat="server">
                        <div class="container">

                            <div id="divContractDocuments" class="row Dashboard-white-widget">
                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-default">
                                        
                                        <div id="collapseDivContractDocument" class="panel-collapse collapse in">
                                            <div class="panel-body">
                                               <div class="row colpadding0">
                                                   <div class="col-md-12 colpadding0">
                                                    <asp:ValidationSummary ID="vsContractDocument" runat="server" Display="none" class="alert alert-block alert-danger fade in" style="padding-left:5%"
                                                        ValidationGroup="ContractDocumentPopUpValidationGroup" />
                                                    <asp:CustomValidator ID="cvContractDocument" runat="server" EnableClientScript="False"
                                                        ValidationGroup="ContractDocumentPopUpValidationGroup" Display="none"/>
                                                       </div>  
                                                        </div> 
                                             
                                                <div class="row col-md-12">
                                                    <div id="outerDivFileTags" runat="server" class="col-md-10 colpadding0">
                                                        <div id="leftArrow" class="scroller scroller-left mt5 mb5 col-md-1 colpadding0" style="width: 3%">
                                                            <span class="arrow-button arrow-button-right">
                                                                <i class="fa fa-chevron-left color-black"></i>
                                                            </span>
                                                        </div>
                                                        <div id="rightArrow" class="scroller scroller-right mt5 mb5 col-md-1 colpadding0" style="width: 3%">
                                                            <span runat="server" class="arrow-button arrow-button-left">
                                                                <i class="fa fa-chevron-right color-black"></i>
                                                            </span>
                                                        </div>

                                                        <div class="divFileTags col-md-10 colpadding0" style="overflow-x: hidden; overflow-y: hidden; width: 70%;">
                                                            <asp:CheckBoxList ID="lstBoxFileTags" runat="server" CssClass="mt5" RepeatDirection="Horizontal" TextAlign="Left"
                                                                OnSelectedIndexChanged="lstBoxFileTags_SelectedIndexChanged" AutoPostBack="true">
                                                            </asp:CheckBoxList>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="row">
                                                    <div class="col-md-2 colpadding0 text-right">
                                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                       
                                                            
                                                        
                                                            <label for="lnkBtnUploadDocument" class="hidden-label"></label>
                                                        <asp:LinkButton ID="lnkBtnUploadDocument" runat="server" data-placement="bottom" CssClass="btn btn-primary"
                                                            ToolTip="Upload Contract Document(s)" data-toggle="tooltip" OnClick="lnkBtnUploadDocument_Click">
                                                           <span class="AddNewspan1"><i class='fa fa-plus'></i></span>&nbsp;New</asp:LinkButton>


							<asp:LinkButton ID="lnkBtnShareDocument" runat="server" data-placement="bottom" CssClass="btn btn-primary"
                                                                ToolTip="Share Contract Document(s)" data-toggle="tooltip" OnClick="lnkBtnShareDocument_Click">
                                                           <span class="AddNewspan1"><i class='fa fa-plus'></i></span>&nbsp;Share</asp:LinkButton>	

                                                        <asp:LinkButton ID="lnkBtn_RebindContractDoc" OnClick="lnkBtn_RebindContractDoc_Click"
                                                            Style="float: right; display: none;" Width="100%" runat="server"> 
                                                        </asp:LinkButton>

                                                        
                                                        <asp:LinkButton runat="server" ID="lnkLinkContractDoc" OnClientClick="OpenContractDocLinkingPopup();"
                                                                        data-toggle="tooltip" data-placement="top" ToolTip="Link Contract with Other Contract(s)">
                                                       <img src="/Images/link-icon.png" alt="Link" / >

                                                                    </asp:LinkButton>
                                                            </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                    </div>
                                                    </div>
                                                <div id="divDocument" runat="server" class="row col-md-12 plr0">
                                                    <asp:UpdatePanel ID="upContractDocUploadPopup" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <asp:GridView runat="server" ID="grdContractDocuments" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                                GridLines="None" PageSize="8" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%" ShowFooter="false"
                                                                PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Right"
                                                                OnRowCommand="grdContDocuments_RowCommand" OnPageIndexChanging="grdContDocuments_PageIndexChanging">
                                                                <Columns>
                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="5%">
                                                                        <HeaderTemplate>
                                                                            <asp:CheckBox ID="chkHeader" runat="server" onclick="javascript:checkAllField(this)" />
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblSelectedFileID" runat="server" Text='<%# Eval("FileID") %>' Visible="false"></asp:Label>
                                                                            <asp:CheckBox ID="chkRow" runat="server" onclick="javascript:checkUncheckRowField(this)" />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr.No." ItemStyle-Width="5%">
                                                                        <ItemTemplate>
                                                                            <%#Container.DataItemIndex+1 %>
                                                                            <%--<asp:Label ID="lblRowID" runat="server" Text='<%# Eval("RowID") %>'></asp:Label>--%>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="Type" ItemStyle-Width="15%">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblDocType" runat="server" Text='<%# Eval("DocTypeName") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="Document" ItemStyle-Width="40%">
                                                                        <ItemTemplate>
                                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; x">
                                                                                <asp:Label ID="lblFileName" runat="server" Text='<%# Eval("FileName") %>'
                                                                                    data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("FileName") %>'></asp:Label>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <%-- <asp:TemplateField HeaderText="Tags" ItemStyle-Width="20%" Visible="false">
                                                                        <ItemTemplate>
                                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width:200px;" data-toggle="tooltip" data-placement="bottom" title='<%# Eval("FileTag") %>'>
                                                                                <asp:TextBox runat="server" ID="txtFileTags" CssClass="form-control" ClientIDMode="Static" data-role="tagsinput"
                                                                                     autocomplete="off" Text='<%# Eval("FileTag") %>' Enabled="false" />                                                                               
                                                                            </div>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>--%>

                                                                    <asp:TemplateField HeaderText="Version" ItemStyle-Width="5%">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblDocVersion" runat="server" Text='<%# Eval("Version") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="Uploaded By" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="10%" Visible="false">
                                                                        <ItemTemplate>
                                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                                                <asp:Label ID="lblUploadedBy" runat="server" Text='<%# Eval("UploadedByName") %>'
                                                                                    data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("UploadedByName") %>'></asp:Label>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="Uploaded On" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="10%" Visible="false">
                                                                        <ItemTemplate>
                                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                                <asp:Label ID="lblUploadedOn" runat="server" Text='<%# Eval("CreatedOn") != DBNull.Value ? Convert.ToDateTime(Eval("CreatedOn")).ToString("dd-MM-yyyy") : "" %>'
                                                                                    data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("CreatedOn") != DBNull.Value ? Convert.ToDateTime(Eval("CreatedOn")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="Action" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                                                                        <ItemTemplate>
                                                                            <asp:UpdatePanel runat="server" ID="aa1naa" UpdateMode="Always">
                                                                                <ContentTemplate>
                                                                                    <asp:LinkButton CommandArgument='<%# Eval("FileID") %>' AutoPostBack="true" CommandName="Info_Doc"
                                                                                        ID="lnkBtnInfoDoc" runat="server" data-toggle="tooltip" data-placement="bottom" ToolTip="View Document Details">
                                                                                        <img src='<%# ResolveUrl("~/Images/Info_icon.png")%>' alt="Details" />
                                                                                    </asp:LinkButton>

                                                                                    <asp:LinkButton CommandArgument='<%# Eval("FileID") %>' AutoPostBack="true" CommandName="View_Doc" Visible='<%# ISDocumentVisible((long)Eval("FileID")) %>'
                                                                                        ID="lnkBtnViewDocContract" runat="server" data-toggle="tooltip" data-placement="bottom" ToolTip="View">
                                                                                        <img src='<%# ResolveUrl("~/img/view-doc.png")%>' alt="View" />
                                                                                    </asp:LinkButton>

                                                                                    <asp:LinkButton CommandArgument='<%# Eval("FileID")%>' CommandName="DownloadContDoc" Visible='<%# ISDocumentVisible((long)Eval("FileID")) %>'
                                                                                        ID="lnkBtnDownLoadContractDoc" runat="server" data-toggle="tooltip" data-placement="bottom" ToolTip="Download">
                                                                                        <img src='<%# ResolveUrl("~/Images/download_icon_new.png")%>' alt="Download" /> 
                                                                                    </asp:LinkButton>

                                                                                    <asp:LinkButton CommandArgument='<%# Eval("FileID")%>' AutoPostBack="true" CommandName="DeleteContDoc" Visible='<%# CanDownloadDoc() %>'
                                                                                        OnClientClick="return confirm('Are you certain you want to delete this document?');"
                                                                                        ID="lnkBtnDeleteContractDoc" runat="server" data-toggle="tooltip" data-placement="bottom" ToolTip="Delete">
                                                                                        <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="Delete" /> 
                                                                                    </asp:LinkButton>
                                                                                      <asp:LinkButton CommandArgument='<%# Eval("FileID")%>' AutoPostBack="true" CommandName="shareContDoc"
                                                                                        OnClientClick="return confirm('Are you certain you want to share this document?');"
                                                                                        ID="LinkButton1" runat="server" data-toggle="tooltip" data-placement="bottom" ToolTip="share">
                                                                                        <img src='<%# ResolveUrl("~/Images/share-icons.png")%>' alt="Delete" /> 
                                                                                    </asp:LinkButton>
                                                                                </ContentTemplate>
                                                                                <Triggers>
                                                                                    <asp:PostBackTrigger ControlID="lnkBtnDownLoadContractDoc" />
                                                                                    <asp:PostBackTrigger ControlID="lnkBtnDeleteContractDoc" />
                                                                                </Triggers>
                                                                            </asp:UpdatePanel>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                                <RowStyle CssClass="clsROWgrid" />
                                                                <HeaderStyle CssClass="clsheadergrid" />
                                                                <EmptyDataTemplate>
                                                                    No Records Found
                                                                </EmptyDataTemplate>
                                                            </asp:GridView>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </div>

                                                <div class="row col-md-12 plr0">
                                                <asp:UpdatePanel ID="upContractDocUploadPopup1" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <div class="col-md-10">
                                                        <div runat="server" id="DivRecordsScrum" style="float: left; margin-top: 5px; color: #999">
                                                            <p style="padding-right: 0px !Important;">
                                                                <asp:Label ID="Label4" runat="server" Text="Showing " Visible="false"></asp:Label>
                                                                <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>-<asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                                of 
                                                                <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2 text-right">
                                                        <div class="col-md-6 plr0 text-right">
                                                            <p class="clsPageNo">Page</p>
                                                        </div>
                                                        <div class="col-md-6 col-sm-6 col-xs-6 pr0">
                                                            <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true" DataPlaceHolder="No" AllowSingleDeselect="false"
                                                                OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged" class="form-control m-bot15" Width="100%" Height="30px">
                                                            </asp:DropDownListChosen>
                                                        </div>
                                                    </div>
                                                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                                   </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </asp:View>
                    <!--Contract Document Panel End-->

                    <asp:View ID="TaskView" runat="server">
                        <div class="container">
                            <div id="DivTaskCollapsOne" class="row Dashboard-white-widget">
                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-default">
                                        <div id="CollapsDivTaskFirstPanel" class="panel-collapse">                                           
                                            <div class="panel-collapse collapse in">
                                                <asp:UpdatePanel ID="upContractTaskActivity" runat="server">
                                                    <ContentTemplate>
                                                        <div class="row col-md-12 plr0">
                                                            <asp:ValidationSummary ID="vsTaskTab" runat="server" Display="None" class="alert alert-block alert-danger fade in"
                                                                ValidationGroup="TaskTabValidationGroup" />
                                                            <asp:CustomValidator ID="cvTaskTab" runat="server" EnableClientScript="False" Display="None"
                                                                ValidationGroup="TaskTabValidationGroup" class="alert alert-block alert-danger fade in" />
                                                        </div>
                                                        <asp:Panel ID="pnlTaskList" runat="server" class="col-md-12 plr0">
                                                            <div class="col-md-12 plr0 text-right">
                                                                <asp:LinkButton ID="lnkAddNewTask" runat="server" OnClick="lnkAddNewTask_Click" CssClass="btn btn-primary"
                                                                    ToolTip="Add New Task" data-toggle="tooltip" data-placement="bottom">
                                                                    <span class="AddNewspan1"><i class='fa fa-plus'></i></span>&nbsp;New</asp:LinkButton>
                                                            </div>

                                                            <div class="row">
                                                                <asp:GridView runat="server" ID="grdTaskActivity" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                                    GridLines="None" PageSize="10" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%" DataKeyNames="TaskID"
                                                                    PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Right"
                                                                    OnPageIndexChanging="grdTaskActivity_OnPageIndexChanging" OnRowCommand="grdTaskActivity_RowCommand" OnRowDataBound="grdTaskActivity_RowDataBound">
                                                                    <Columns>
                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr.No." ItemStyle-Width="5%">
                                                                            <ItemTemplate>
                                                                                <%#Container.DataItemIndex+1 %>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Task" ItemStyle-Wrap="true" ItemStyle-Width="25%">
                                                                            <ItemTemplate>
                                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px">
                                                                                    <asp:Label ID="lblTask" runat="server" Text='<%# Eval("TaskTitle") %>'
                                                                                        data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("TaskTitle") %>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Priority" ItemStyle-Width="10%">
                                                                            <ItemTemplate>
                                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                                    <asp:Label ID="lblTaskDesc" runat="server"
                                                                                        Text='<%# ShowPriority(Convert.ToString(Eval("PriorityID"))) %>'
                                                                                        ToolTip='<%# ShowPriority(Convert.ToString(Eval("PriorityID"))) %>'
                                                                                        data-toggle="tooltip" data-placement="bottom">
                                                                                    </asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Assign On" ItemStyle-Width="10%">
                                                                            <ItemTemplate>
                                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px;">
                                                                                    <asp:Label ID="lblAssignOn" runat="server" Text='<%# Eval("AssignOn") != null ? Convert.ToDateTime(Eval("AssignOn")).ToString("dd-MM-yyyy") : "" %>'
                                                                                        data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("AssignOn") != null ? Convert.ToDateTime(Eval("AssignOn")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Due Date" ItemStyle-Width="10%">
                                                                            <ItemTemplate>
                                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px;">
                                                                                    <asp:Label ID="lblDueDate" runat="server" Text='<%# Eval("DueDate") != null ? Convert.ToDateTime(Eval("DueDate")).ToString("dd-MM-yyyy") : "" %>'
                                                                                        data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("DueDate") != null ? Convert.ToDateTime(Eval("DueDate")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Date Created" ItemStyle-Width="10%">
                                                                            <ItemTemplate>
                                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                                    <asp:Label ID="lblCreatedOn" runat="server"
                                                                                        Text='<%# Eval("CreatedOn") != null ? Convert.ToDateTime(Eval("CreatedOn")).ToString("dd-MM-yyyy") : "" %>'
                                                                                        ToolTip='<%# Eval("CreatedOn") != null ? Convert.ToDateTime(Eval("CreatedOn")).ToString("dd-MM-yyyy") : "" %>'
                                                                                        data-toggle="tooltip" data-placement="bottom"></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Created By" ItemStyle-Width="20%">
                                                                            <ItemTemplate>
                                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                                    <asp:Label ID="lblCreatedBy" runat="server" Text='<%# Eval("CreatedByName") %>'
                                                                                        data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("CreatedByName") %>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                         <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Status" ItemStyle-Width="5%" FooterStyle-Width="5%">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblTaskStatus" runat="server" Text='<%# Eval("StatusName") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="Action" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center" ItemStyle-Width="10%">
                                                                            <ItemTemplate>
                                                                                <asp:UpdatePanel runat="server" ID="upTaskActivityAction" UpdateMode="Conditional">
                                                                                    <ContentTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                                                            <asp:LinkButton CommandArgument='<%# Eval("TaskID")%>' AutoPostBack="true" CommandName="EditTask" Visible='<%# CanDownloadDoc() %>'
                                                                                                ID="lnkBtnEditTaskDoc" runat="server" data-toggle="tooltip" data-placement="bottom" ToolTip="Edit/View Task Details">
                                                                                                <img src='<%# ResolveUrl("~/Images/edit_icon_new.png")%>' alt="Edit"/>
                                                                                            </asp:LinkButton>

                                                                                            <asp:LinkButton CommandArgument='<%# Eval("TaskID")%>' AutoPostBack="true" CommandName="TaskReminder"
                                                                                                data-toggle="tooltip" ToolTip="Sent Reminder or Activate Access URL to All Assigned User(s)"
                                                                                                ID="lnkBtnTaskReminder" runat="server" data-placement="bottom">                                                                                                            
                                                                                                <img src='<%# ResolveUrl("~/Images/send-mail-icon.png")%>' alt="Send" /> <%--width="15" height="15" CssClass="btn btn-primary" CssClass="btn btn-primary btn-circle"--%>
                                                                                            </asp:LinkButton>

                                                                                            <asp:LinkButton CommandArgument='<%# Eval("TaskID")%>' AutoPostBack="true" CommandName="CloseTask"
                                                                                                ID="lnkBtnResCloseTask" runat="server" data-toggle="tooltip" data-placement="bottom"
                                                                                                    Visible='<%# Eval("TaskStatusID").ToString() == "14" ? false : true %>'
                                                                                                ToolTip="Close Task. Once Task Closed, Assigned User(s) will not be able to provide response."
                                                                                                OnClientClick="return confirm('Are you sure!! You want to close this Task?');">
                                                                                                <img src='<%# ResolveUrl("~/Images/change_status_icon_new.png")%>' alt="Close" />
                                                                                            </asp:LinkButton>

                                                                                            <asp:LinkButton CommandArgument='<%# Eval("TaskID")%>' AutoPostBack="true" CommandName="DeleteTask" Visible='<%# CanDownloadDoc() %>'
                                                                                                OnClientClick="return confirm('Are you sure!! You want to Delete this Task Detail?');" data-toggle="tooltip" data-placement="bottom"
                                                                                                ID="lnkBtnResDeleteTask" runat="server" ToolTip="Delete Task">
                                                                                                <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="Delete" />
                                                                                            </asp:LinkButton>
                                                                                        </div>
                                                                                    </ContentTemplate>
                                                                                    <Triggers>
                                                                                        <asp:PostBackTrigger ControlID="lnkBtnResDeleteTask" />
                                                                                    </Triggers>
                                                                                </asp:UpdatePanel>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                    <RowStyle CssClass="clsROWgrid" />
                                                                    <HeaderStyle CssClass="clsheadergrid" />
                                                                    <EmptyDataTemplate>
                                                                        No Records Found
                                                                    </EmptyDataTemplate>
                                                                </asp:GridView>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-md-12 colpadding0">
                                                                    <div class="col-md-10 colpadding0">
                                                                        <div runat="server" id="Div2" style="float: left; margin-top: 5px; color: #999">
                                                                            <p style="padding-right: 0px !Important;">
                                                                                <asp:Label ID="Label2" runat="server" Text="Showing " Visible="false"></asp:Label>
                                                                                <asp:Label ID="lblStartRecord_Task" Font-Bold="true" runat="server" Text=""></asp:Label>-<asp:Label ID="lblEndRecord_Task" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                                                of
                                                                                <asp:Label ID="lblTotalRecord_Task" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                                            </p>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-2 colpadding0 text-right">
                                                                        <div class="col-md-6 plr0 text-right">
                                                                            <p class="clsPageNo">Page</p>
                                                                        </div>
                                                                        <div class="col-md-6 col-sm-6 col-xs-6 pr0">
                                                                            <asp:DropDownListChosen runat="server" ID="ddlPageNo_Task" AutoPostBack="true" DataPlaceHolder="No" AllowSingleDeselect="false"
                                                                                OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged" class="form-control" Width="80%" Height="30px">
                                                                            </asp:DropDownListChosen>
                                                                        </div>
                                                                    </div>
                                                                    <asp:HiddenField ID="TasksTotalRows" runat="server" Value="0" />
                                                                </div>
                                                            </div>
                                                        </asp:Panel>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>

                                                <asp:UpdatePanel ID="upAddEditTask" runat="server">
                                                    <ContentTemplate>
                                                        <asp:Panel ID="pnlAddNewTask" runat="server">
                                                            <div class="container plr0">

                                                                <div class="row">
                                                                    <div class="form-group required col-md-3 w25per" id="divnewtask" runat="server">
                                                                        <label for="rbTaskType" class="control-label">Type</label>
                                                                        <fieldset style="border-style: solid; border-radius: 5px; border-width: 1px; border-color: #c7c7cc; width: 100%; height: 33px;">
                                                                            <asp:RadioButtonList ID="rbTaskType" runat="server" RepeatDirection="Horizontal">
                                                                                <%--onchange="rblTaskTypeChange();"--%>
                                                                                <asp:ListItem class="radio-inline" Text="Review" Value="4" ></asp:ListItem>
                                                                                <asp:ListItem class="radio-inline" Text="Approval " Value="6"></asp:ListItem>
                                                                                <asp:ListItem class="radio-inline" Text="Other" Value="1" Selected="True"></asp:ListItem>
                                                                            </asp:RadioButtonList>
                                                                        </fieldset>
                                                                    </div>

                                                                    <div class="form-group required col-md-9 col-sm-12 col-xs-12 w75per">
                                                                        <label for="tbxTaskTitle" class="control-label">Title</label>
                                                                        <asp:TextBox runat="server" ID="tbxTaskTitle" CssClass="form-control" autocomplete="off" />
                                                                        <asp:RequiredFieldValidator ID="rfvTaskTitle" ErrorMessage="Please Enter Task Title"
                                                                            ControlToValidate="tbxTaskTitle" runat="server" ValidationGroup="TaskTabValidationGroup" Display="None" />
                                                                    </div>
                                                                </div>

                                                                <div class="row">
                                                                    <div class="form-group required col-md-12">
                                                                        <label for="tbxTaskDesc" class="control-label">Description/ Specific Instruction(s)</label>
                                                                        <asp:TextBox runat="server" ID="tbxTaskDesc" CssClass="form-control" autocomplete="off" TextMode="MultiLine" />
                                                                        <asp:RequiredFieldValidator ID="rfvTaskDesc" ErrorMessage="Please Enter Task Description"
                                                                            ControlToValidate="tbxTaskDesc" runat="server" ValidationGroup="TaskTabValidationGroup" Display="None" />
                                                                    </div>
                                                                </div>

                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="form-group required col-md-3">
                                                                            <label for="ddlTaskPriority" class="control-label">Priority</label>
                                                                            <asp:DropDownListChosen runat="server" ID="ddlTaskPriority" DataPlaceHolder="Select Task Priority"
                                                                                AllowSingleDeselect="false" DisableSearchThreshold="5" CssClass="form-control" Width="100%">
                                                                                <asp:ListItem Text="High" Value="1"></asp:ListItem>
                                                                                <asp:ListItem Text="Medium" Value="2"></asp:ListItem>
                                                                                <asp:ListItem Text="Low" Value="3"></asp:ListItem>
                                                                            </asp:DropDownListChosen>
                                                                        </div>

                                                                        <div class="form-group required col-md-2">
                                                                            <label for="tbxTaskAssignDate" class="control-label">Assign On</label>
                                                                            <a data-toggle="popover" data-trigger="focus"
                                                                                data-content="Task Assign To-refers to Task will be assigned to Assignee on provided date, By this way you can able to assign future task to User(s) in advance">
                                                                                <span class="fa fa-question-circle"></span>
                                                                            </a>
                                                                            <div class="input-group date">
                                                                                <span class="input-group-addon">
                                                                                    <span class="fa fa-calendar color-black"></span>
                                                                                </span>
                                                                                <asp:TextBox ID="tbxTaskAssignDate" runat="server" placeholder="DD-MM-YYYY" class="form-control" autocomplete="off" />
                                                                            </div>
                                                                            <asp:RequiredFieldValidator ID="rfvTaskAssignDate" ErrorMessage="Please Enter Task Assign Date"
                                                                                ControlToValidate="tbxTaskAssignDate" runat="server" ValidationGroup="TaskTabValidationGroup" Display="None" />
                                                                            <asp:CompareValidator ID="cmpTaskDueDate" ControlToCompare="tbxTaskDueDate" runat="server"
                                                                                ControlToValidate="tbxTaskAssignDate" Type="Date" Operator="LessThanEqual" CultureInvariantValues="true"
                                                                                ErrorMessage="Task Assign On Date should be less than Task Due Date" ValidationGroup="TaskTabValidationGroup" Display="None">
                                                                            </asp:CompareValidator>
                                                                        </div>

                                                                        <div class="form-group required col-md-2">

                                                                            <label for="tbxTaskDueDate" class="control-label">Due Date</label>


                                                                            <div class="input-group date">
                                                                                <span class="input-group-addon">
                                                                                    <span class="fa fa-calendar color-black"></span>
                                                                                </span>
                                                                                <asp:TextBox ID="tbxTaskDueDate" runat="server" placeholder="DD-MM-YYYY" class="form-control" autocomplete="off" />
                                                                            </div>
                                                                            <asp:RequiredFieldValidator ID="rfvTaskDueDate" ErrorMessage="Please Select Task Due Date"
                                                                                ControlToValidate="tbxTaskDueDate" runat="server" ValidationGroup="TaskTabValidationGroup" Display="None" />
                                                                            <asp:CompareValidator ID="cmpTaskDate" ControlToCompare="tbxTaskAssignDate" ValidationGroup="TaskTabValidationGroup"
                                                                                ControlToValidate="tbxTaskDueDate" Type="Date" Operator="GreaterThanEqual" Display="None"
                                                                                ErrorMessage="Task Due Date should be greater than Task Assign On Date" runat="server"></asp:CompareValidator>
                                                                        </div>

                                                                        <div id="divTaskAssignTo" class="form-group required col-md-5">
                                                                            <div class="col-md-12">
                                                                                <label for="lstBoxTaskUser" class="control-label">Assign To</label>
                                                                            </div>
                                                                            <div class="col-md-12">
                                                                                <div class="col-md-10 col-sm-10 col-xs-10 plr0">
                                                                                    <asp:ListBox ID="lstBoxTaskUser" CssClass="form-control" runat="server" SelectionMode="Multiple" Width="100%"></asp:ListBox>
                                                                                    <%-- onchange="ddlTaskUserChange()"--%>
                                                                                    <asp:LinkButton ID="lnkBtnRebind_TaskUser" OnClick="lnkBtnRebind_TaskUser_Click" Style="float: right; display: none;" Width="100%" runat="server"> 
                                                                                    </asp:LinkButton>
                                                                                </div>
                                                                                <div class="col-md-2 col-sm-2 col-xs-2 plr5 mt5 text-center">

                                                                                    <img id="lnkShowAddNewTaskUserModal" src="/Images/add_icon_new.png"
                                                                                        onclick="OpenAddUserDetailPop()" alt="Add" data-toggle="tooltip" data-placement="bottom" title="Add New User" />
                                                                                    <%-- <label for="lnkShowAddNewTaskUserModal" class="hidden-label w100per">&nbsp;</label>--%>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div id="divShowPermission" runat="server" class="form-group required col-md-3" style="float:initial">
                                                                        <label for="ddlPermission" class="control-label">Permission</label>
                                                                        <asp:DropDownListChosen runat="server" ID="ddlPermission" DataPlaceHolder="Select Permission"
                                                                            AllowSingleDeselect="false" DisableSearchThreshold="5" CssClass="form-control" Width="100%">
                                                                            <asp:ListItem Text="View & Download" Value="1"></asp:ListItem>
                                                                            <asp:ListItem Text="View" Value="2"></asp:ListItem>
                                                                        </asp:DropDownListChosen>
                                                                    </div>
                                                                    </div>
                                                                </div>

                                                                <div class="row d-none">
                                                                    <div class="form-group col-md-12">
                                                                        <label for="tbxTaskDesc" class="control-label">Expected Outcome/ Specific Instruction(s)</label>
                                                                        <asp:TextBox runat="server" ID="tbxExpOutcome" CssClass="form-control" autocomplete="off" TextMode="MultiLine" />
                                                                    </div>
                                                                </div>

                                                                <div class="row d-none">
                                                                    <div class="form-group col-md-12">
                                                                        <label for="tbxTaskRemark" class="control-label">Comment(s)</label>
                                                                        <asp:TextBox runat="server" ID="tbxTaskRemark" CssClass="form-control" autocomplete="off" />
                                                                    </div>
                                                                </div>

                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <label for="fuTaskDocUpload" class="control-label">Choose Contract Document(s) to Share</label>
                                                                        <img id="imgAddNewTaskDoc" runat="server" src="/Images/add_icon_new.png" onclick="OpenUploadDocumentForTaskPopup()"
                                                                            alt="Add" data-toggle="tooltip" data-placement="bottom" title="Add New Document(s) Related to Contract" />
                                                                        <asp:LinkButton ID="lnkBtnAddNewTaskDoc" OnClick="lnkBtnAddNewTaskDoc_Click"
                                                                            Style="float: right; display: none;" Width="100%" runat="server"> 
                                                                        </asp:LinkButton>
                                                                        <asp:LinkButton ID="lnkBtn_RebindContractTaskDoc" OnClick="lnkBtn_RebindContractTaskDoc_Click"
                                                                            Style="float: right; display: none;" Width="100%" runat="server"> 
                                                                        </asp:LinkButton>
                                                                    </div>
                                                                </div>

                                                                <div class="row col-md-12" style="min-height: 50px; max-height: 300px; overflow:hidden">
                                                                    <asp:UpdatePanel ID="upTaskContractDocuments" runat="server">
                                                                        <ContentTemplate>
                                                                            <asp:GridView runat="server" ID="grdTaskContractDocuments" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                                                GridLines="None" AllowPaging="True" AutoPostBack="true" CssClass="table" Width="100%" ShowFooter="false"
                                                                                OnRowCommand="grdContDocuments_RowCommand" OnPageIndexChanging="grdTaskContractDocuments_PageIndexChanging" PagerSettings-FirstPageText="First" PagerSettings-LastPageText="Last" PagerSettings-Mode="NumericFirstLast" PagerSettings-PageButtonCount="5" PageSize="4">
                                                                                <PagerStyle HorizontalAlign="Right"/>
                                                                                <Columns>
                                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="5%">
                                                                                        <HeaderTemplate>
                                                                                            <asp:CheckBox ID="chkHeaderTaskDocument" runat="server" onclick="javascript:checkAll_TaskDocument(this)" />
                                                                                            <%--checkAll_MailDocument--%>
                                                                                        </HeaderTemplate>
                                                                                        <ItemTemplate>
                                                                                            <asp:CheckBox ID="chkRowTaskDocument" runat="server" onclick="javascript:checkUncheckRow_TaskDocument(this)" />
                                                                                            <%--checkUncheckRow_MailDocument--%>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>

                                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr.No." ItemStyle-Width="5%">
                                                                                        <ItemTemplate>
                                                                                            <%#Container.DataItemIndex+1 %>
                                                                                            <asp:Label ID="lblFileID" runat="server" Text='<%# Eval("FileID") %>' Visible="false"></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>

                                                                                    <asp:TemplateField HeaderText="Document Type" ItemStyle-Width="15%">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lblDocType" runat="server" Text='<%# Eval("DocTypeName") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>

                                                                                    <asp:TemplateField HeaderText="Document Name" ItemStyle-Width="30%">
                                                                                        <ItemTemplate>
                                                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                                                                <asp:Label ID="lblFileName" runat="server" Text='<%# Eval("FileName") %>'
                                                                                                    data-toggle="tooltip" data-placement="right" ToolTip='<%# Eval("FileName") %>'></asp:Label>
                                                                                            </div>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>

                                                                                    <asp:TemplateField HeaderText="Version" ItemStyle-Width="5%">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lblDocVersion" runat="server" Text='<%# Eval("Version") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>

                                                                                    <asp:TemplateField HeaderText="Uploaded By" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="15%">
                                                                                        <ItemTemplate>
                                                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                                                                <asp:Label ID="lblUploadedBy" runat="server" Text='<%# Eval("UploadedByName") %>'
                                                                                                    data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("UploadedByName") %>'></asp:Label>
                                                                                            </div>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>

                                                                                    <asp:TemplateField HeaderText="Uploaded On" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="15%">
                                                                                        <ItemTemplate>
                                                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                                                <asp:Label ID="lblUploadedOn" runat="server" Text='<%# Eval("CreatedOn") != DBNull.Value ? Convert.ToDateTime(Eval("CreatedOn")).ToString("dd-MM-yyyy") : "" %>'
                                                                                                    data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("CreatedOn") != DBNull.Value ? Convert.ToDateTime(Eval("CreatedOn")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                                                                                            </div>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>

                                                                                    <asp:TemplateField HeaderText="Action" ItemStyle-Width="15%" HeaderStyle-CssClass="text-right" ItemStyle-CssClass="text-right">
                                                                                        <ItemTemplate>
                                                                                            <asp:UpdatePanel runat="server" ID="aa1naa" UpdateMode="Always">
                                                                                                <ContentTemplate>
                                                                                                    <asp:LinkButton CommandArgument='<%# Eval("FileID") %>' AutoPostBack="true" CommandName="View_Doc"
                                                                                                        ID="lnkBtnViewTaskDoc" runat="server" data-toggle="tooltip" data-placement="left" ToolTip="View Document">
                                                                                                        <img src='<%# ResolveUrl("~/Images/view-doc.png")%>' alt="View" />
                                                                                                    </asp:LinkButton>

                                                                                                    <asp:LinkButton CommandArgument='<%# Eval("FileID")%>' CommandName="DownloadContDoc" Visible='<%# CanDownloadDoc() %>'
                                                                                                        ID="lnkBtnDownloadTaskDoc" runat="server" data-toggle="tooltip" data-placement="left" ToolTip="Download Document">
                                                                                                        <img src='<%# ResolveUrl("~/Images/download_icon_new.png")%>' alt="Download" /> 
                                                                                                    </asp:LinkButton>
                                                                                                </ContentTemplate>
                                                                                                <Triggers>
                                                                                                    <asp:PostBackTrigger ControlID="lnkBtnDownloadTaskDoc" />
                                                                                                </Triggers>
                                                                                            </asp:UpdatePanel>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                </Columns>
                                                                                <RowStyle CssClass="clsROWgrid" />
                                                                                <HeaderStyle CssClass="clsheadergrid" />
                                                                                <EmptyDataTemplate>
                                                                                    No Records Found
                                                                                </EmptyDataTemplate>
                                                                            </asp:GridView>
                                                                        </ContentTemplate>
                                                                    </asp:UpdatePanel>
                                                                </div>

                                                                <div id="divTaskDocCount" class="row col-md-12 pl0" style="display: none;">
                                                                    <div class="col-md-12 text-left">
                                                                        <asp:Label runat="server" ID="lblTotalTaskDocSelected" Text="" CssClass="control-label"></asp:Label>
                                                                    </div>
                                                                </div>

                                                                <div class="row">
                                                                    <div class="form-group col-md-12" style="text-align: center;">
                                                                        <asp:Button Text="Create & Assign" runat="server" ID="btnTaskSave" CssClass="btn btn-primary"
                                                                            ValidationGroup="TaskTabValidationGroup" OnClick="btnSaveTask_Click"></asp:Button>

                                                                        <asp:Button Text="Clear" runat="server" ID="btnTaskClear" CssClass="btn btn-primary" OnClick="btnClearTask_Click" />

                                                                        <asp:Button Text="Close" runat="server" ID="btnTaskClose" CssClass="btn btn-primary" OnClick="btnCloseTask_Click" />
                                                                        <%--OnClientClick="javascript:HideShowTaskDiv('false');"--%>
                                                                    </div>
                                                                </div>

                                                                <div class="row col-md-12" id="divTaskResponseLog" runat="server">
                                                                    <label for="grdTaskResponseLog" class="control-label">Task Response(s)</label>
                                                                    <asp:GridView ID="grdTaskResponseLog" runat="server" AutoGenerateColumns="false" CssClass="table" AllowPaging="false"
                                                                        Width="100%" ShowHeaderWhenEmpty="true" GridLines="None" PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Right"
                                                                        OnRowCommand="grdTaskResponseLog_RowCommand">
                                                                        <Columns>
                                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr.No." ItemStyle-Width="5%">
                                                                                <ItemTemplate>
                                                                                    <%#Container.DataItemIndex+1 %>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="User" ItemStyle-Width="20%">
                                                                                <ItemTemplate>
                                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                                                        <asp:Label ID="lblFileName" runat="server" Text='<%# Eval("AssignToName") %>'
                                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("AssignToName") %>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="Status" HeaderStyle-Width="10%" ItemStyle-Width="10%">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblStatus" runat="server" Text='<%# Eval("StatusName") %>'
                                                                                        data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("StatusName") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="Created By" HeaderStyle-Width="15%" ItemStyle-Width="15%" Visible="false">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblCreatedByName" runat="server" Text='<%# Eval("CreatedByName") %>'
                                                                                        data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("CreatedByName") %>'>
                                                                                    </asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="Date Created" HeaderStyle-Width="20%" ItemStyle-Width="20%">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblStatusChangedOn" runat="server" Text='<%# Eval("StatusChangeOn") != null ? Convert.ToDateTime(Eval("StatusChangeOn")).ToString("dd-MM-yyyy HH:mm:ss tt") : "" %>'
                                                                                        data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("StatusChangeOn") != null ? Convert.ToDateTime(Eval("StatusChangeOn")).ToString("dd-MM-yyyy HH:mm:ss tt") : "" %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="Comment" ItemStyle-Width="30%" HeaderStyle-Width="30%">
                                                                                <ItemTemplate>
                                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                                                        <asp:Label ID="lblComment" runat="server" Text='<%# Eval("Comment") %>'
                                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Comment") %>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="Action" ItemStyle-Width="15%" HeaderStyle-Width="15%"
                                                                                ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center">
                                                                                <ItemTemplate>
                                                                                    <asp:UpdatePanel ID="upTaskResponseAction" runat="server" UpdateMode="Conditional">
                                                                                        <ContentTemplate>
                                                                                            <asp:LinkButton
                                                                                                CommandArgument='<%# Eval("ID")+","+ Eval("TaskID")%>' CommandName="ViewTaskResponseDoc"
                                                                                                ID="lnkBtnViewDocument" runat="server"
                                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip="View">
                                                                                        <img src='<%# ResolveUrl("~/Images/view-doc.png")%>' alt="View" />
                                                                                            </asp:LinkButton>

                                                                                            <asp:LinkButton
                                                                                                CommandArgument='<%# Eval("ID")+","+ Eval("TaskID")%>' CommandName="DownloadTaskResponseDoc"
                                                                                                ID="lnkBtnDownloadTaskResDoc" runat="server"
                                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip="Download" Visible='<%# CanDownloadDoc() %>'>
                                                                                        <img src='<%# ResolveUrl("~/Images/download_icon_new.png")%>' alt="Download"/>
                                                                                            </asp:LinkButton>

                                                                                            <asp:LinkButton CommandArgument='<%# Eval("TaskID") +","+ Eval("UserID") +","+ Eval("RoleID") %>' AutoPostBack="true"
                                                                                                CommandName="TaskReminder" ID="lnkBtnResTaskReminder" runat="server"
                                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip="Sent Reminder or Activate Access URL to this Assigned User(s)">                                                                                                            
                                                                                        <img src='<%# ResolveUrl("~/Images/Notification.png")%>' alt="Send"/> <%--width="15" height="15" CssClass="btn btn-primary" CssClass="btn btn-primary btn-circle"--%>
                                                                                            </asp:LinkButton>
                                                                                        </ContentTemplate>
                                                                                        <Triggers>
                                                                                            <asp:PostBackTrigger ControlID="lnkBtnDownloadTaskResDoc" />
                                                                                        </Triggers>
                                                                                    </asp:UpdatePanel>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                        </Columns>
                                                                        <RowStyle CssClass="clsROWgrid" />
                                                                        <HeaderStyle CssClass="clsheadergrid" />
                                                                        <EmptyDataTemplate>
                                                                            No Response Submitted yet.
                                                                        </EmptyDataTemplate>
                                                                    </asp:GridView>
                                                                </div>
                                                            </div>

                                                    <!--AuditLog panel start-->
                                                    <div id="AuditLog" runat="server" class="row Dashboard-white-widget">
                                                        <div class="dashboard">
                                                            <div class="col-lg-12 col-md-12">
                                                                <div class="panel panel-default">
                                                                    <div class="panel panel-default" style="margin-bottom: 1px;">
                                                                        <div class="panel-heading">
                                                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseAuditLog">
                                                                                <h2>Audit Log</h2>
                                                                            </a>
                                                                            <div class="panel-actions">
                                                                                <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseAuditLog"><i class="fa fa-chevron-up"></i></a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                   
                                                                    <div id="collapseAuditLog" runat="server" class="panel-collapse collapse in">
                                                                        <div runat="server" id="log" style="text-align: left;">
                                                                            <%--<fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; margin-top: 5px;">--%>
                                                                                <div style="margin-bottom: 7px; clear: both; margin-top: 10px">
                                                                                    <asp:GridView runat="server" AutoPostBack="true" ID="grdTransactionHistory" AutoGenerateColumns="false" AllowSorting="true" 
                                                                                        AllowPaging="true" PageSize="5" CssClass="table" GridLines="Horizontal" OnPageIndexChanging="grdTransactionHistory_OnPageIndexChanging"
                                                                                        BorderWidth="0px">
                                                                                        <Columns>
                                                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr.No." ItemStyle-Width="10%" FooterStyle-Width="10%">
                                                                                                   <ItemTemplate>
                                                                                                       <%#Container.DataItemIndex+1 %>
                                                                                                   </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                   
                                                                                            <asp:TemplateField HeaderText="Contract No." ItemStyle-Width="15%">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="lblDocType" runat="server" data-toggle="tooltip" ToolTip='<%# Eval("ContractNo")%>' Text='<%# Eval("ContractNo") %>'></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                   
                                                                                            <asp:TemplateField HeaderText="Contract Title" ItemStyle-Width="15%">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="lblDocType" runat="server" data-toggle="tooltip" ToolTip='<%# Eval("ContractTitle")%>' Text='<%# Eval("ContractTitle") %>'></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                   
                                                                                            <asp:TemplateField HeaderText="Action" ItemStyle-Width="15%">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="lblDocType" runat="server" data-toggle="tooltip" ToolTip='<%# Eval("Action")%>' Text='<%# Eval("Action") %>'></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                   
                                                                                            <asp:TemplateField HeaderText="Dated On" ItemStyle-Width="15%">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="lblDocType" runat="server" data-toggle="tooltip" ToolTip='<%# Eval("CreatedOn")%>' Text='<%# Eval("CreatedOn")%>'></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                   
                                                                                            <asp:TemplateField HeaderText="Remark" ItemStyle-Width="15%">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="lblDocType" runat="server" data-toggle="tooltip" ToolTip='<%# Eval("Remark")%>' Text='<%# Eval("Remark") %>'></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                        </Columns>
                                                                                        <PagerStyle HorizontalAlign="Right" />
                                                                                        <PagerTemplate>
                                                                                            <table style="display: none">
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:PlaceHolder ID="ph1" runat="server"></asp:PlaceHolder>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </PagerTemplate>
                                                                                    </asp:GridView>
                                                                                </div>
                                                                            <%--</fieldset> --%>                                          
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-12 colpadding0">
                                                                    <div class="col-md-8 colpadding0">
                                                                        <%--<div runat="server" id="Div1" style="float: left; margin-top: 5px; color: #999">
                                                                            <p style="padding-right: 0px !Important;">
                                                                                <asp:Label ID="Label4" runat="server" Text="Showing "></asp:Label>
                                                                                <asp:Label ID="Label1" Font-Bold="true" runat="server" Text=""></asp:Label>- 
                                                                                <asp:Label ID="Label2" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                                                                                <asp:Label ID="Label3" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                                            </p>
                                                                        </div>--%>
                                                                    </div>
                                                                    <div class="col-md-2 colpadding0">
                                                                        <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 37%; float: right; height:32px !important; margin-top: 2px;margin-left: 131px; position: absolute;"
                                                                            AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                                                                            <asp:ListItem Text="5" Selected="True" />
                                                                            <asp:ListItem Text="10" />
                                                                            <asp:ListItem Text="20" />
                                                                            <asp:ListItem Text="50" />
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div class="col-md-2 colpadding0" style="float: right;">
                                                                        <div style="float: left; width: 60%">
                                                                            <p class="clsPageNo">Page</p>
                                                                        </div>
                                                                        <div style="float: left; width: 40%">
                                                                            <asp:DropDownListChosen runat="server" ID="DropDownListPageNoNew" AutoPostBack="true" DataPlaceHolder="No" AllowSingleDeselect="false"
                                                                                OnSelectedIndexChanged="DropDownListPageNoNew_SelectedIndexChanged" class="form-control m-bot15" Width="80%" Height="30px">
                                                                            </asp:DropDownListChosen>
                                                                        </div>
                                                                    </div>
                                                                    <asp:HiddenField ID="HiddenField2" runat="server" Value="0" />
                                                                </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--AuditLog panel end-->
                                                    </asp:Panel>
                                                    </ContentTemplate>
                                                    <%-- <Triggers>
                                                        <asp:PostBackTrigger ControlID="btnTaskSave" />
                                                    </Triggers>--%>
                                                </asp:UpdatePanel>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:View>

                    <asp:View ID="AuditLogView" runat="server">
                        <div class="container">
                            <div class="row Dashboard-white-widget">
                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-default">
                                        <%--<div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;" data-toggle="tooltip" data-placement="bottom">
                                                    <div class="panel-heading">
                                                        <%-- data-toggle="collapse" data-parent="#SixthTabAccordion" href="#collapseDivAuditLog"
                                                    </div>
                                                </div>--%>

                                        <div id="collapseDivAuditLog" class="panel-collapse collapse in">
                                            <div class="panel-body">
                                                <asp:UpdatePanel ID="upContractAuditLog" runat="server">
                                                    <ContentTemplate>
                                                        <div class="row">
                                                            <div style="margin-bottom: 7px">
                                                                <asp:ValidationSummary ID="ValidationSummary11" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                                                    ValidationGroup="validationContractAuditLog" />
                                                                <asp:CustomValidator ID="cvContractAuditLog" runat="server" EnableClientScript="False"
                                                                    ValidationGroup="validationContractAuditLog" Display="None" />
                                                            </div>
                                                        </div>

                                                        <asp:Panel ID="Panel3" runat="server">
                                                            <div class="col-md-12">
                                                                <div class="col-md-3 colpadding0">
                                                                    <label for="ddlUsers_AuditLog" class="control-label">Users</label>
                                                                    <asp:DropDownListChosen runat="server" ID="ddlUsers_AuditLog" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                        class="form-control" Width="100%" DataPlaceHolder="Select User">
                                                                    </asp:DropDownListChosen>
                                                                </div>

                                                                <div class="form-group col-md-2">
                                                                    <label for="txtFromDate_AuditLog" class="control-label">From Date</label>
                                                                    <div class="input-group date">
                                                                        <span class="input-group-addon">
                                                                            <span class="fa fa-calendar color-black"></span>
                                                                        </span>
                                                                        <asp:TextBox ID="txtFromDate_AuditLog" runat="server" placeholder="DD-MM-YYYY" class="form-control" autocomplete="off" />
                                                                    </div>
                                                                </div>

                                                                <div class="form-group col-md-2">
                                                                    <label for="txtToDate_AuditLog" class="control-label">To Date</label>
                                                                    <div class="input-group date">
                                                                        <span class="input-group-addon">
                                                                            <span class="fa fa-calendar color-black"></span>
                                                                        </span>
                                                                        <asp:TextBox ID="txtToDate_AuditLog" runat="server" placeholder="DD-MM-YYYY" class="form-control" autocomplete="off" />
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-3 colpadding0">
                                                                    <div class="col-md-6 text-right">
                                                                        <label for="btnApplyFilter_AuditLog" class="hidden-label">To Date</label>
                                                                        <asp:Button ID="btnApplyFilter_AuditLog" class="btn btn-primary" runat="server" Text="Apply Filter(s)" OnClick="btnApplyFilter_AuditLog_Click" />
                                                                    </div>
                                                                    <div class="col-md-6 text-right">
                                                                        <label for="btnClearFilter_AuditLog" class="hidden-label">To Date</label>
                                                                        <asp:Button ID="btnClearFilter_AuditLog" class="btn btn-primary" runat="server" Text="Clear Filter(s)" OnClick="btnClearFilter_AuditLog_Click" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                              <%--  Added by renuka--%>
                                                         <div class="form-group col-md-2">
                                                             <div class="input-group" style="margin-top: 26px;">

                                                                        <asp:UpdatePanel ID="UpdatePanel11" runat="server" UpdateMode="Conditional">
                                                                            <ContentTemplate>
                                                                                <asp:LinkButton Text="Apply" runat="server" ID="btnExport" OnClick="btnExport_Click" data-toggle="tooltip" ToolTip="Export to Excel">
                                                                                      <img src="../../Images/Excel _icon.png" alt="Export to Excel" title="Export to Excel" /> 
                                                                                </asp:LinkButton>
                                                                            </ContentTemplate>
                                                                            <Triggers>
                                                                                <asp:PostBackTrigger ControlID="btnExport" />
                                                                            </Triggers>
                                                                        </asp:UpdatePanel>
                                                                    </div>
                                                                    </div>
                                                       <%-- End--%>
                                                            <div class="clearfix"></div>

                                                            <div id="divAuditLog" runat="server" class="scrolling-wrapper">
                                                                <asp:GridView Visible="false" runat="server" ID="gvContractAuditLog" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                                    GridLines="None" PageSize="10" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%" ShowFooter="false"
                                                                    PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Right" OnPageIndexChanging="gvContractAuditLog_PageIndexChanging">
                                                                    <Columns>
                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr.No." ItemStyle-Width="5%" FooterStyle-Width="5%">
                                                                            <ItemTemplate>
                                                                                <%--<%#Container.DataItemIndex+1 %>--%>
                                                                                <asp:Label ID="lblRowID" runat="server" Text='<%# Eval("RowID") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Remark" ItemStyle-Width="45%" HeaderStyle-Width="45%">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblRemark" runat="server" Text='<%# Eval("Remark")%>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Created By" ItemStyle-Width="25%" HeaderStyle-Width="25%">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblCreatedBy" runat="server" Text='<%# Eval("CreatedByUser")%>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField ItemStyle-CssClass="text-right" HeaderStyle-CssClass="text-right" HeaderText="Created On" ItemStyle-Width="25%" HeaderStyle-Width="25%">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblCreatedOn" runat="server" Text='<%# Eval("CreatedOn") != DBNull.Value ? Convert.ToDateTime(Eval("CreatedOn")).ToString("dd-MM-yyyy hh:mm:ss:tt") : ""%>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                    <RowStyle CssClass="clsROWgrid" />
                                                                    <HeaderStyle CssClass="clsheadergrid" />
                                                                    <EmptyDataTemplate>
                                                                        No Records Found
                                                                    </EmptyDataTemplate>
                                                                </asp:GridView>
                                                            </div>

                                                            <div id="divAuditLog_Vertical" class="auditlog" runat="server">
                                                            </div>

                                                            <div class="row d-none">
                                                                <div class="col-md-12 colpadding0">
                                                                    <div class="col-md-10 colpadding0">
                                                                        <div runat="server" id="Div1" style="float: left; margin-top: 5px; color: #999">
                                                                            <p style="padding-right: 0px !Important;">
                                                                                <asp:Label ID="Label1" runat="server" Text="Showing " Visible="false"></asp:Label>
                                                                                <asp:Label ID="lblStartRecord_AuditLog" Font-Bold="true" runat="server" Text=""></asp:Label>-<asp:Label ID="lblEndRecord_AuditLog" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                                                of
                                                                                        <asp:Label ID="lblTotalRecord_AuditLog" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                                            </p>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-2 colpadding0 text-right">
                                                                        <div class="col-md-6 plr0 text-right">
                                                                            <p class="clsPageNo">Page</p>
                                                                        </div>
                                                                        <div class="col-md-6 col-sm-6 col-xs-6 pr0">
                                                                            <asp:DropDownListChosen runat="server" ID="ddlPageNo_AuditLog" AutoPostBack="true" DataPlaceHolder="No" AllowSingleDeselect="false"
                                                                                OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged" class="form-control" Width="100%" Height="30px">
                                                                            </asp:DropDownListChosen>
                                                                        </div>
                                                                    </div>
                                                                    <asp:HiddenField ID="totalRows_AuditLog" runat="server" Value="0" />
                                                                </div>
                                                            </div>
                                                        </asp:Panel>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:View>

                    <asp:View ID="ContractTransaction" runat="server">
                     
                                  <div>                                    
                                    <iframe id="showdetails" src="../Common/ContractTemplateTransactionDetail.aspx?UID=<% =UId%>&RID=<% =RoleId%>&ContractTID=<% =ContractTemplateInstanceId%>&CID=<% =CustId%>" width="100%" height="700px;" frameborder="0"></iframe>
                                </div>                     
                    </asp:View>

                     <asp:View ID="MilestoneView" runat="server">

                         <div class="container">
                             <div id="DivTaskCollapsOne1" class="row Dashboard-white-widget">
                                 <div class="col-lg-12 col-md-12">
                                     <div class="panel panel-default">
                                         <div id="CollapsDivTaskFirstPanel1" class="panel-collapse">
                                             <div class="panel-collapse collapse in">
                                               <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                                    <ContentTemplate>
                                                        <div class="row col-md-12 plr0">
                                                            <asp:ValidationSummary ID="vsMilestoneTab" runat="server" Display="None" class="alert alert-block alert-danger fade in"
                                                                ValidationGroup="MilestoneTabValidationGroup" />
                                                            <asp:CustomValidator ID="cvMilestoneTab" runat="server" EnableClientScript="False" Display="None"
                                                                ValidationGroup="MilestoneTabValidationGroup" class="alert alert-block alert-danger fade in" />
                                                        </div>
                                                        
                                                        <asp:Panel ID="PanelMilestonedetail" runat="server" class="col-md-12 plr0">
                                                            <div class="col-md-12 plr0 text-right">
                                                            <div class="col-md-2 text-left">
                                                                <asp:DropDownList runat="server" ID="drpdownmilestone"
                                                                    AllowSingleDeselect="false"
                                                                    class="form-control" Width="100%" DataPlaceHolder="Select Milestone">
                                                                </asp:DropDownList>
                                                                </div>
                                                                <div class="col-md-2 text-right">
                                                                 <asp:DropDownList ID="dropmilestonestatus" runat="server" CssClass="form-control">                                                                            
                                                                             <asp:ListItem Text="Select Status" Value="-1"></asp:ListItem>
                                                                             <asp:ListItem Text="In Progress" Value="1"></asp:ListItem>
                                                                             <asp:ListItem Text="Open" Value="2"></asp:ListItem>
                                                                             <asp:ListItem Text="Completed" Value="3"></asp:ListItem>
                                                                         </asp:DropDownList>
                                                                         </div>
                                                                         <asp:Button ID="btnapplymilestone" Text="Apply" CssClass="btn btn-primary" runat="server" OnClick="btnapplymilestone_Click" />

                                                              <asp:Button ID="Button1" Text="New" CssClass="btn btn-primary" runat="server" OnClick="Button1_Click" />

                                                                    <%--  <asp:LinkButton ID="LinkButton1"  runat="server" OnClick="LinkButton1_Click" 
                                                             CssClass="btn btn-primary"
                                                                    ToolTip="Add New Milestone" data-toggle="tooltip" data-placement="bottom">
                                                                    <span class="AddNewspan1"><i class='fa fa-plus'></i></span>&nbsp;New</asp:LinkButton>--%>

                                                             <%--<asp:LinkButton ID="lnkbtnMilestoneadd12"  runat="server" OnClientClick="test()" OnClick="lnkbtnMilestoneadd12_Click" 
                                                             CssClass="btn btn-primary"
                                                                    ToolTip="Add New Milestone" data-toggle="tooltip" data-placement="bottom">
                                                                    <span class="AddNewspan1"><i class='fa fa-plus'></i></span>&nbsp;New</asp:LinkButton>--%>
                                                                                                                                
                                                           <%--     <asp:LinkButton ID="lnkbtnMilestoneadd"  runat="server"  AutoPostBack="true" 
                                                                CssClass="btn btn-primary"
                                                                    ToolTip="Add New Milestone" data-toggle="tooltip" data-placement="bottom">
                                                                    <span class="AddNewspan1"><i class='fa fa-plus'></i></span>&nbsp;New</asp:LinkButton>--%>
                                                            </div>

                                                            <div class="row">
                                                                <asp:GridView runat="server" ID="gridMilestone" AutoGenerateColumns="false" AllowSorting="true" 
                                                                ShowHeaderWhenEmpty="true" OnRowCommand="gridMilestone_RowCommand"
                                                                    GridLines="None" PageSize="10" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%" 
                                                                    DataKeyNames="ID"
                                                                    PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Right"
                                                                    OnPageIndexChanging="gridMilestone_PageIndexChanging" OnRowDataBound="gridMilestone_RowDataBound">
                                                                    <Columns>
                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr.No." ItemStyle-Width="5%">
                                                                            <ItemTemplate>
                                                                                <%#Container.DataItemIndex+1 %>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Title" ItemStyle-Wrap="true" ItemStyle-Width="15%">
                                                                            <ItemTemplate>
                                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                                                    <asp:Label ID="lbl21" runat="server" Text='<%# Eval("Title") %>'
                                                                                        data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Title") %>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                      <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Description" ItemStyle-Wrap="true" ItemStyle-Width="15%">
                                                                            <ItemTemplate>
                                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                                                    <asp:Label ID="lbl4" runat="server" Text='<%# Eval("Description") %>'
                                                                                        data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Description") %>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                          <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Department" ItemStyle-Wrap="true" ItemStyle-Width="15%">
                                                                            <ItemTemplate>
                                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                                                    <asp:Label ID="lbl2" runat="server" Text='<%# Eval("DeptName") %>'
                                                                                        data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("DeptName") %>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Assigned User" ItemStyle-Wrap="true" ItemStyle-Width="15%">
                                                                            <ItemTemplate>
                                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                                                    <asp:Label ID="lbl1" runat="server" Text='<%# Eval("AssignUserName") %>'
                                                                                        data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("AssignUserName") %>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Start Date" ItemStyle-Width="20%">
                                                                            <ItemTemplate>
                                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px;">
                                                                                    <asp:Label ID="lblStartDate" runat="server" Text='<%# Eval("StartDate") != null ? Convert.ToDateTime(Eval("StartDate")).ToString("dd-MM-yyyy") : "" %>'
                                                                                        data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("StartDate") != null ? Convert.ToDateTime(Eval("StartDate")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="End Date" ItemStyle-Width="20%">
                                                                            <ItemTemplate>
                                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px;">
                                                                                    <asp:Label ID="lblEndDate" runat="server" Text='<%# Eval("EndDate") != null ? Convert.ToDateTime(Eval("EndDate")).ToString("dd-MM-yyyy") : "" %>'
                                                                                        data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("EndDate") != null ? Convert.ToDateTime(Eval("EndDate")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Due Date" ItemStyle-Width="10%">
                                                                            <ItemTemplate>
                                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px;">
                                                                                    <asp:Label ID="lblDueDate" runat="server" Text='<%# Eval("DueDate") != null ? Convert.ToDateTime(Eval("DueDate")).ToString("dd-MM-yyyy") : "" %>'
                                                                                        data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("DueDate") != null ? Convert.ToDateTime(Eval("DueDate")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        
                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Status" ItemStyle-Width="20%">
                                                                            <ItemTemplate>
                                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                                    <asp:Label ID="lblCreatedBy" runat="server" Text='<%# Eval("Status") %>'
                                                                                        data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Status") %>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        
                                                                        <asp:TemplateField HeaderText="Action" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center" ItemStyle-Width="10%">
                                                                            <ItemTemplate>
                                                                                <asp:UpdatePanel runat="server" ID="upTaskActivityAction" UpdateMode="Conditional">
                                                                                    <ContentTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                                                            <asp:LinkButton CommandArgument='<%# Eval("ID")%>' AutoPostBack="true" CommandName="EditMilestone"
                                                                                            Visible='<%# Eval("StatusID").ToString() == "3" ? false : true %>'
                                                                                                ID="lnkBtnEditMileStoneDoc" runat="server" data-toggle="tooltip" data-placement="bottom" ToolTip="Edit/View Milestone Details">
                                                                                                <img src='<%# ResolveUrl("~/Images/edit_icon_new.png")%>' alt="Edit"/>
                                                                                            </asp:LinkButton>


                                                                                            <asp:LinkButton CommandArgument='<%# Eval("ID")%>' AutoPostBack="true" CommandName="DeleteMileStone"
                                                                                                OnClientClick="return confirm('Are you sure! You want to Delete this Milestone record?');" data-toggle="tooltip" data-placement="bottom"
                                                                                                ID="lnkBtnResDeleteMileStone" runat="server" ToolTip="Delete Milestone">
                                                                                                <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="Delete" />
                                                                                            </asp:LinkButton>
                                                                                        </div>
                                                                                    </ContentTemplate>
                                                                                    <Triggers>
                                                                                        <asp:PostBackTrigger ControlID="lnkBtnResDeleteMileStone" />
                                                                                    </Triggers>
                                                                                </asp:UpdatePanel>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                    <RowStyle CssClass="clsROWgrid" />
                                                                    <HeaderStyle CssClass="clsheadergrid" />
                                                                    <EmptyDataTemplate>
                                                                        No Records Found
                                                                    </EmptyDataTemplate>
                                                                </asp:GridView>
                                                            </div>

                                                              <div class="row">
                                                                <div class="col-md-12 colpadding0">
                                                                    <div class="col-md-10 colpadding0">
                                                                        <div runat="server" id="Div4" style="float: left; margin-top: 5px; color: #999">
                                                                            <p style="padding-right: 0px !Important;">
                                                                                <asp:Label ID="Label3" runat="server" Text="Showing " Visible="false"></asp:Label>
                                                                                <asp:Label ID="lblStartRecord_MileStone" Font-Bold="true" runat="server" Text=""></asp:Label>-<asp:Label ID="lblEndRecord_MileStone" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                                                of
                                                                                <asp:Label ID="lblTotalRecord_MileStone" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                                            </p>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-2 colpadding0 text-right">
                                                                        <div class="col-md-6 plr0 text-right">
                                                                            <p class="clsPageNo">Page</p>
                                                                        </div>
                                                                        <div class="col-md-6 col-sm-6 col-xs-6 pr0">
                                                                            <asp:DropDownListChosen runat="server" ID="DropDownListPageNoMilestone" AutoPostBack="true" DataPlaceHolder="No" AllowSingleDeselect="false"
                                                                                OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged" class="form-control" Width="80%" Height="30px">
                                                                            </asp:DropDownListChosen>
                                                                        </div>
                                                                    </div>
                                                                    <asp:HiddenField ID="HiddenField1" runat="server" Value="0" />
                                                                </div>
                                                            </div>
                                                        </asp:Panel>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>

                                                 <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                                     <ContentTemplate>
                                                         <asp:Panel ID="Paneladdmilestone" runat="server">

                                                             <div class="row" style="padding-top: 6px;">
                                                                 <div class="col-lg-12 col-md-12">
                                                                     <div class="col-lg-2 col-md-2">
                                                                         <label class="control-label">Title</label>
                                                                         <label style="color: #ff0000;">*</label>
                                                                     </div>
                                                                     <div class="col-lg-10 col-md-10">
                                                                         <asp:TextBox runat="server" ID="txtboxMilestonetitle" CssClass="form-control" />
                                                                     </div>
                                                                 </div>
                                                             </div>
                                                             <div class="row" style="padding-top: 6px;">
                                                                 <div class="col-lg-12 col-md-12">
                                                                     <div class="col-lg-2 col-md-2">
                                                                         <label class="control-label">Detail Description</label>
                                                                         <label style="color: #ff0000;">*</label>
                                                                     </div>
                                                                     <div class="col-lg-10 col-md-10">
                                                                         <asp:TextBox runat="server" ID="txtboxMilestonedescription" TextMode="MultiLine" CssClass="form-control" />
                                                                     </div>
                                                                 </div>
                                                             </div>

                                                             <div class="row" style="padding-top: 6px;">
                                                                 <div class="col-lg-12 col-md-12">
                                                                     <div class="col-lg-2 col-md-2">
                                                                         <label class="control-label">Department</label>
                                                                         <label style="color: #ff0000;">*</label>
                                                                     </div>
                                                                     <div class="col-lg-4 col-md-4">
                                                                         <asp:DropDownListChosen runat="server" AutoPostBack="true" ID="drpmilestoneDepartment" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                             DataPlaceHolder="Select Department" OnSelectedIndexChanged="drpmilestoneDepartment_SelectedIndexChanged" class="form-control" Width="100%" />
                                                                     </div>
                                                                     <div class="col-lg-2 col-md-2">
                                                                         <label class="control-label">Assign To</label>
                                                                         <label style="color: #ff0000;">*</label>
                                                                     </div>
                                                                     <div class="col-lg-4 col-md-4">
                                                                         <asp:DropDownListChosen runat="server" AutoPostBack="true" ID="drpmilestoneDepartmentUser" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                             DataPlaceHolder="Select User" class="form-control" Width="100%" />
                                                                     </div>
                                                                 </div>
                                                             </div>


                                                             <div class="row" style="padding-top: 6px;">
                                                                 <div class="col-lg-12 col-md-12">
                                                                   <div id="divMilestoneTypelbl" runat="server" class="col-lg-2 col-md-2">
                                                                         <label class="control-label">Type</label>
                                                                         <label style="color: #ff0000;">*</label>
                                                                     </div>
                                                                     <div id="divMilestoneType" runat="server" class="col-lg-4 col-md-4">
                                                                         <asp:RadioButtonList ID="RdoMilestoneType" AutoPostBack="true" runat="server" OnSelectedIndexChanged="RdoMilestoneType_SelectedIndexChanged" RepeatDirection="Horizontal">
                                                                             <asp:ListItem class="radio-inline" Text="Recurring" Value="1"></asp:ListItem>
                                                                             <asp:ListItem class="radio-inline" Text="One Time" Value="2" Selected="True"></asp:ListItem>
                                                                         </asp:RadioButtonList>
                                                                         
                                                                     </div>
                                                                     <div id="divfrequencylbl" runat="server" class="col-lg-2 col-md-2">
                                                                         <label class="control-label">Frequency</label>
                                                                         <label style="color: #ff0000;">*</label>
                                                                     </div>
                                                                     <div id="divfrequencydrp" runat="server" class="col-lg-4 col-md-4">
                                                                         <asp:DropDownListChosen runat="server" AutoPostBack="true" ID="DRPMilestonePeriod" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                             class="form-control" Width="100%" OnSelectedIndexChanged="DRPMilestonePeriod_SelectedIndexChanged" DataPlaceHolder="Select Period">
                                                                             <asp:ListItem Text="Select Frequency" Value="-1" Selected="True"></asp:ListItem>
                                                                             <asp:ListItem Text="Monthly" Value="M"></asp:ListItem>
                                                                             <asp:ListItem Text="Quarterly" Value="Q"></asp:ListItem>
                                                                             <asp:ListItem Text="Half-Yearly" Value="H"></asp:ListItem>
                                                                             <asp:ListItem Text="Yearly" Value="Y"></asp:ListItem>
                                                                         </asp:DropDownListChosen>
                                                                     </div>
                                                                     <div class="col-md-1 col-sm-1 col-xs-1 plr0 mt5 text-right" style="display:none;">
                                                                         <asp:Button ID="dtbutton" runat="server" OnClick="dtbutton_Click" />
                                                                     </div>
                                                                 </div>
                                                             </div>
                                                             <div>
                                                              <div id="collapseDivPaymentLog" class="panel-collapse collapse in">
                                                <div class="panel-body">
                                                    <div style="margin-bottom: 7px">
                                                        <asp:ValidationSummary ID="ValidationSummary4" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                                            ValidationGroup="NoticePopUpPaymentLogValidationGroup" EnableClientScript="true" />
                                                        <asp:CustomValidator ID="cvNoticePayment" runat="server" EnableClientScript="true"
                                                            ValidationGroup="NoticePopUpPaymentLogValidationGroup" Display="None" />
                                                    </div>
                                                    
                                                    <div class="form-group col-md-12">
                                                        <asp:UpdatePanel ID="upNoticePayment" runat="server">
                                                            <ContentTemplate>
                                                                <asp:GridView runat="server" ID="grdNoticePayment" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                                    GridLines="None" PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%" ShowFooter="true"
                                                                    OnRowCommand="grdNoticePayment_RowCommand" OnRowDataBound="grdNoticePayment_RowDataBound" PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Right"
                                                                    OnPageIndexChanging="grdNoticePayment_OnPageIndexChanging">
                                                                    <Columns>
                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr.No." ItemStyle-Width="5%" FooterStyle-Width="5%">
                                                                            <ItemTemplate>
                                                                                <%#Container.DataItemIndex+1 %>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>


                                                                          <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="From Month" ItemStyle-Width="16%" FooterStyle-Width="16%">
                                                                            <ItemTemplate>                                                                            
                                                                                <asp:Label ID="lblPaymentFromDate" runat="server" Text='<%# Eval("FromMonth") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                            <FooterTemplate>
                                                                                <div class="col-md-4 plr0 input-group date" style="width: 100%">
                                                                                    <span class="input-group-addon">
                                                                                        <span class="fa fa-calendar color-black"></span>
                                                                                    </span>
                                                                                    <asp:TextBox runat="server" placeholder="From Month" class="form-control" ID="tbxFromPaymentDate" Style="width: 100%;" />
                                                                                </div>
                                                                            </FooterTemplate>
                                                                        </asp:TemplateField>

                                                                         <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="To Month" ItemStyle-Width="16%" FooterStyle-Width="16%">
                                                                            <ItemTemplate>                                                                            
                                                                                <asp:Label ID="lblPaymentFromDate1" runat="server" Text='<%# Eval("ToMonth") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                            <FooterTemplate>
                                                                                <div class="col-md-4 plr0 input-group date" style="width: 100%">
                                                                                    <span class="input-group-addon">
                                                                                        <span class="fa fa-calendar color-black"></span>
                                                                                    </span>
                                                                                    <asp:TextBox runat="server" placeholder="To Month" class="form-control" ID="tbxToPaymentDate" Enabled="false" Style="width: 100%;height:35px;" />
                                                                                </div>
                                                                            </FooterTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Due Date" ItemStyle-Width="16%" FooterStyle-Width="16%">
                                                                            <ItemTemplate>                                                                            
                                                                                <asp:Label ID="lblPaymentDate" runat="server" Text='<%# Eval("DueDate") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                            <FooterTemplate>
                                                                                <asp:TextBox ID="tbxPaymentID" runat="server" CssClass="form-control" Style="display: none"></asp:TextBox>
                                                                                <div class="col-md-4 plr0 input-group date" style="width: 100%">
                                                                                    <span class="input-group-addon">
                                                                                        <span class="fa fa-calendar color-black"></span>
                                                                                    </span>
                                                                                    <asp:TextBox runat="server" placeholder="Due Date" class="form-control" ID="tbxPaymentDate" Style="width: 100%;" />
                                                                                </div>
                                                                                <asp:RequiredFieldValidator ID="rfvPaymentDate" ErrorMessage="Provide Due Date." runat="server"
                                                                                    ControlToValidate="tbxPaymentDate" ValidationGroup="NoticePopUpPaymentLogValidationGroup" Display="None" />
                                                                            </FooterTemplate>
                                                                        </asp:TemplateField>


                                                                        <asp:TemplateField HeaderText="Action" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="10%"
                                                                            FooterStyle-Width="10%" FooterStyle-HorizontalAlign="Left">
                                                                            <ItemTemplate>
                                                                                <asp:UpdatePanel runat="server" ID="aa1aa" UpdateMode="Conditional">
                                                                                    <ContentTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%;">
                                                                                            <asp:LinkButton CommandArgument='<%# Eval("ID") %>'
                                                                                                AutoPostBack="true" CommandName="EditPayment"
                                                                                                ID="lnkBtnEditPayment" runat="server">
                                                                                                        <img src='<%# ResolveUrl("~/Images/edit_icon_new.png")%>' alt="Edit" title="Edit" />
                                                                                            </asp:LinkButton>

                                                                                            <asp:LinkButton CommandArgument='<%# Eval("ID")%>'
                                                                                                AutoPostBack="true" CommandName="DeletePayment"
                                                                                                OnClientClick="return confirm('Are you sure! You want to Delete this record?');"
                                                                                                ID="lnkBtnDeletePayment" runat="server">
                                                                                            <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="Delete" 
                                                                                                title="Delete" />
                                                                                            </asp:LinkButton>
                                                                                            <%--"~/Images/delete_icon.png"--%>
                                                                                        </div>
                                                                                    </ContentTemplate>
                                                                                    <Triggers>
                                                                                        <asp:PostBackTrigger ControlID="lnkBtnDeletePayment" />
                                                                                    </Triggers>
                                                                                </asp:UpdatePanel>
                                                                            </ItemTemplate>
                                                                            <FooterTemplate>
                                                                                <asp:Button CssClass="btn btn-primary" ID="btnPaymentSave" runat="server" Text="Save" CausesValidation="true" ValidationGroup="NoticePopUpPaymentLogValidationGroup" OnClick="btnPaymentSave_Click"></asp:Button>
                                                                            </FooterTemplate>
                                                                        </asp:TemplateField>

                                                                    </Columns>
                                                                    <RowStyle CssClass="clsROWgrid" />
                                                                    <HeaderStyle CssClass="clsheadergrid" />
                                                                    <EmptyDataTemplate>
                                                                        No Records Found
                                                                    </EmptyDataTemplate>
                                                                </asp:GridView>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                </div>
                                            </div>
                                                             </div>
                                                           <%--  <div class="row">
                                                                 <div id="grid" style="border: none; margin-left: 10px; margin-right: 10px; margin-bottom: 2%;"></div>
                                                             </div>--%>
                                                            <%--  <div id="recurringcustomdate" runat="server" class="row" style="padding-top: 6px;">
                                                                 <div class="col-lg-12 col-md-12">
                                                                    <div class="col-lg-2 col-md-2">
                                                                         <label class="control-label">Start Date</label>
                                                                         <label style="color: #ff0000;">*</label>
                                                                     </div>
                                                                     <div class="col-lg-4 col-md-4">
                                                                         <div class="input-group date">
                                                                             <span class="input-group-addon">
                                                                                 <span class="fa fa-calendar color-black"></span>
                                                                             </span>
                                                                             <asp:TextBox ID="Tbxstartdate" runat="server" placeholder="DD-MM-YYYY" class="form-control" autocomplete="off" />
                                                                         </div>
                                                                     </div>
                                                                        <div class="col-lg-2 col-md-2">
                                                                         <label class="control-label">End Date</label>
                                                                         <label style="color: #ff0000;">*</label>
                                                                     </div>
                                                                     <div class="col-lg-4 col-md-4">
                                                                         <div class="input-group date">
                                                                             <span class="input-group-addon">
                                                                                 <span class="fa fa-calendar color-black"></span>
                                                                             </span>
                                                                             <asp:TextBox ID="Tbxenddate" runat="server" placeholder="DD-MM-YYYY" class="form-control" autocomplete="off" />
                                                                         </div>
                                                                     </div>
                                                                 </div>
                                                                 </div>--%>

                                                             <div class="row" style="padding-top: 6px;">
                                                                 <div id="divduedatelbl" runat="server" class="col-lg-12 col-md-12">
                                                                     <div class="col-lg-2 col-md-2">
                                                                         <label class="control-label">Due Date</label>
                                                                         <label style="color: #ff0000;">*</label>
                                                                     </div>
                                                                     <div id="divduedate" runat="server" class="col-lg-4 col-md-4">
                                                                         <div class="input-group date">
                                                                             <span class="input-group-addon">
                                                                                 <span class="fa fa-calendar color-black"></span>
                                                                             </span>
                                                                             <asp:TextBox ID="txbmilestoneduedate" runat="server" placeholder="DD-MM-YYYY" class="form-control" autocomplete="off" />
                                                                         </div>
                                                                     </div>
                                                                     <div class="col-lg-2 col-md-2">
                                                                         <label class="control-label">Status</label>
                                                                         <label style="color: #ff0000;">*</label>
                                                                     </div>
                                                                     <div class="col-lg-4 col-md-4">
                                                                         <asp:DropDownList ID="drpmilestonestatus" runat="server" CssClass="form-control">                                                                            
                                                                             <asp:ListItem Text="In Progress" Value="1"></asp:ListItem>
                                                                             <asp:ListItem Text="Open" Value="2"></asp:ListItem>
                                                                             <asp:ListItem Text="Completed" Value="3"></asp:ListItem>
                                                                         </asp:DropDownList>
                                                                     </div>
                                                                 </div>
                                                             </div>
                                                               <div class="row" style="padding-top: 6px;">
                                                                 <div class="col-lg-12 col-md-12">
                                                                     <div class="col-lg-2 col-md-2">
                                                                         <label class="control-label">Remark</label>
                                                                         <label style="color: #ff0000;">*</label>
                                                                     </div>
                                                                     <div class="col-lg-10 col-md-10">
                                                                         <asp:TextBox runat="server" ID="txbRemark" TextMode="MultiLine" CssClass="form-control" />
                                                                     </div>
                                                                 </div>
                                                             </div>
                                                             <div class="row" style="padding-top: 6px;">
                                                                 <div class="col-lg-12 col-md-12 col text-center">
                                                                   <%--  <div class="col-lg-4 col-md-4">
                                                                     </div>
                                                                     <div class="col-lg-2 col-md-2">--%>
                                                                         <asp:Button Text="Save" runat="server" ID="btnCreateMilestone" CssClass="btn btn-primary" OnClick="btnCreateMilestone_Click" />
                                                                   <%--  </div>
                                                                     <div class="col-lg-2 col-md-2">--%>
                                                                         <asp:Button Text="Close" runat="server" ID="btnmilestoneclosed" CssClass="btn btn-primary" OnClick="btnmilestoneclosed_Click" />
                                                                  <%--   </div>
                                                                     <div class="col-lg-4 col-md-4">
                                                                     </div>--%>
                                                                 </div>
                                                             </div>

                                                             <div class="row" runat="server" id="divmilestoneresponselog" style="padding-top: 6px;">
                                                                 <asp:UpdatePanel ID="upTaskResponseDocUpload" runat="server">
                                                                     <ContentTemplate>
                                                                         <asp:GridView runat="server" ID="grdmilestonekResponseLog" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                                             GridLines="None" PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%"
                                                                             PagerSettings-Position="Top" PagerStyle-HorizontalAlign="Right"
                                                                             OnRowCommand="grdmilestonekResponseLog_RowCommand">
                                                                             <Columns>
                                                                                 <asp:TemplateField HeaderText="Sr.No." ItemStyle-HorizontalAlign="Left" ItemStyle-Width="5%" HeaderStyle-Width="5%">
                                                                                     <ItemTemplate>
                                                                                         <%#Container.DataItemIndex+1 %>
                                                                                     </ItemTemplate>
                                                                                 </asp:TemplateField>

                                                                                 <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Status" HeaderStyle-Width="10%" ItemStyle-Width="10%">
                                                                                     <ItemTemplate>
                                                                                         <asp:Label ID="lblStatus" runat="server" Text='<%# Eval("Status") %>'
                                                                                             data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Status") %>'></asp:Label>
                                                                                     </ItemTemplate>
                                                                                 </asp:TemplateField>

                                                                                 <asp:TemplateField HeaderText="Created By" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                                                                                     <ItemTemplate>
                                                                                         <asp:Label ID="lblCreatedByName" runat="server" Text='<%# Eval("CreatedByName") %>'
                                                                                             data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("CreatedByName") %>'>
                                                                                         </asp:Label>
                                                                                     </ItemTemplate>
                                                                                 </asp:TemplateField>

                                                                                 <asp:TemplateField HeaderText="Date Created" HeaderStyle-Width="20%" ItemStyle-Width="20%">
                                                                                     <ItemTemplate>
                                                                                         <asp:Label ID="lblStatusChangedOn" runat="server" Text='<%# Eval("CreatedOn") != null ? Convert.ToDateTime(Eval("CreatedOn")).ToString("dd-MM-yyyy HH:mm:ss tt") : "" %>'
                                                                                             data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("CreatedOn") != null ? Convert.ToDateTime(Eval("CreatedOn")).ToString("dd-MM-yyyy HH:mm:ss tt") : "" %>'></asp:Label>
                                                                                     </ItemTemplate>
                                                                                 </asp:TemplateField>

                                                                                 <asp:TemplateField HeaderText="Remark" ItemStyle-Width="30%" HeaderStyle-Width="30%">
                                                                                     <ItemTemplate>
                                                                                         <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                                                             <asp:Label ID="lblComment" runat="server" Text='<%# Eval("Remark") %>'
                                                                                                 data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Remark") %>'></asp:Label>
                                                                                         </div>
                                                                                     </ItemTemplate>
                                                                                 </asp:TemplateField>

                                                                                 <asp:TemplateField HeaderText="Documents" ItemStyle-Width="10%" HeaderStyle-Width="10%">
                                                                                     <ItemTemplate>
                                                                                         <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%;">
                                                                                             <asp:Label ID="lblResDoc" runat="server" Text='<%# ShowMilestoneResponseDocCount((long)Eval("ContractID"), (long)Eval("MileStoneID"),(long)Eval("ID")) %>'> 
                                                                                             </asp:Label>
                                                                                         </div>
                                                                                     </ItemTemplate>
                                                                                 </asp:TemplateField>

                                                                                 <asp:TemplateField HeaderText="Action" ItemStyle-Width="20%" HeaderStyle-Width="20%"
                                                                                     ItemStyle-CssClass="text-right" HeaderStyle-CssClass="text-right">
                                                                                     <ItemTemplate>
                                                                                         <asp:UpdatePanel runat="server" ID="upTaskResDocument" UpdateMode="Always">
                                                                                             <ContentTemplate>
                                                                                                 <asp:LinkButton
                                                                                                     CommandArgument='<%# Eval("ID")+","+ Eval("MileStoneID")+","+ Eval("ContractID")%>' CommandName="ViewMilestoneResponseDoc"
                                                                                                     ID="lnkBtnViewmilestoneResDoc" runat="server">
                                                                                        <img src='<%# ResolveUrl("~/Images/Eye.png")%>' alt="View" title="View Documents" />
                                                                                                 </asp:LinkButton>

                                                                                                 <asp:LinkButton
                                                                                                     CommandArgument='<%# Eval("ID")+","+ Eval("MileStoneID")+","+ Eval("ContractID")%>' CommandName="DownloadMilestoneResponseDoc"
                                                                                                     ID="lnkBtnDownloadmilestoneResDoc" runat="server">
                                                                                        <img src='<%# ResolveUrl("~/Images/download_icon_new.png")%>' alt="Download" title="Download Documents" />
                                                                                                 </asp:LinkButton>

                                                                                             </ContentTemplate>
                                                                                             <Triggers>
                                                                                                 <asp:PostBackTrigger ControlID="lnkBtnDownloadmilestoneResDoc" />
                                                                                             </Triggers>
                                                                                         </asp:UpdatePanel>
                                                                                     </ItemTemplate>
                                                                                 </asp:TemplateField>

                                                                             </Columns>
                                                                             <RowStyle CssClass="clsROWgrid" />
                                                                             <HeaderStyle CssClass="clsheadergrid" />
                                                                             <EmptyDataTemplate>
                                                                                 No Records Found
                                                                             </EmptyDataTemplate>
                                                                         </asp:GridView>
                                                                     </ContentTemplate>
                                                                 </asp:UpdatePanel>
                                                             </div>

                                                         </asp:Panel>
                                                     </ContentTemplate>
                                                 </asp:UpdatePanel>

                                             </div>
                                         </div>
                                     </div>
                                 </div>
                             </div>
                         </div>

                     </asp:View>

                </asp:MultiView>
            </div>
            <%--Contract CustomFields popup--%>
            <div class="modal fade" id="divCustomFieldPopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog" style="width: 35%;">
                    <div class="modal-content">
                        <div class="modal-header">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label class="modal-header-custom">
                                Add New Custom Field</label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                            <iframe src="about:blank" id="IframeCustomflds" runat="server" frameborder="0" width="100%" height="250px"></iframe>
                        </div>
                    </div>
                </div>
            </div>

            <%--Department Popup--%>
            <div class="modal fade" id="AddDepartmentPopUp" tabindex="-1" style="overflow:hidden;" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog w35per">
                    <div class="modal-content">
                        <div class="modal-header">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label class="modal-header-custom">
                                Add New Department</label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                            <iframe src="about:blank" id="IframeDepartment" frameborder="0" runat="server" width="100%" height="300px"></iframe>
                        </div>
                    </div>
                </div>
            </div>


             <%--	Add New Quarter  Popup--%>
         <%--   <div class="modal fade" id="AddQuarterPopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog w35per">
                    <div class="modal-content">
                        <div class="modal-header">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label class="modal-header-custom">
                                Add New Details</label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                            <div>
                                <asp:TextBox ID="txbfromMonthdate" runat="server" placeholder="DD-MM-YYYY" class="form-control" autocomplete="off" />                              
                            </div>
                        </div>
                    </div>
                </div>
            </div>--%>

             <%--Contact Person Department Popup--%>
            <div class="modal fade" id="AddPDepartmentPopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog " style="width: 40%;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label class="modal-header-custom">
                                Add New Contact Person of  Department</label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="ClosePopContactPerson()">&times;</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                            <iframe src="about:blank" id="IframePDepartment" frameborder="0" runat="server" width="100%" height="500px"></iframe>
                        </div>
                    </div>
                </div>
            </div>

            <%--Act Popup--%>
            <div class="modal fade" id="AddActPopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog" style="width: 40%;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label class="modal-header-custom">
                                Add New Act</label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                            <iframe src="about:blank" id="IframeAct" frameborder="0" runat="server" width="100%" height="170px"></iframe>
                        </div>
                    </div>
                </div>
            </div>

            <%--Add New Vendor--%>
            <div class="modal fade" id="AddVendorPopUp" tabindex="-1" style="overflow: hidden;" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog w90per">
                    <div class="modal-content">
                        <div class="modal-header">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label class="modal-header-custom">
                                Add New Vendor</label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="ClosePopVendor()">&times;</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                            <iframe src="about:blank" id="IframeParty" frameborder="0" runat="server" width="100%" height="500px"></iframe>
                        </div>
                    </div>
                </div>
            </div>

            <%--Add Legal Contract category--%>
            <div class="modal fade" id="AddContractType" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label class="modal-header-custom">
                                Add New Contract Type</label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="CloseContractTypePopUp()">&times;</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                            <iframe src="about:blank" id="IframeCategoryType" frameborder="0" runat="server" width="100%" height="200px"></iframe>
                        </div>
                    </div>
                </div>
            </div>

            <%--Add Contract SubType--%>
            <div class="modal fade" id="AddContractSubTypePopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog" style="width: 35%;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label class="modal-header-custom">
                                Add New Contract Sub-Type</label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                            <iframe src="about:blank" id="Iframesubconttype" runat="server" frameborder="0" width="100%" height="250px"></iframe>
                        </div>
                    </div>
                </div>
            </div>

            <%--Add Court--%>
            <div class="modal fade" id="AddCourtsPopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog" style="width: 45%;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label class="modal-header-custom">
                                Add New Court</label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                            <iframe src="about:blank" id="IframeCourt" frameborder="0" runat="server" width="100%" height="450px"></iframe>
                        </div>
                    </div>
                </div>
            </div>

            <%--Add User--%>
            <div class="modal fade" id="AddUserPopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog" style="width: 40%;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label class="modal-header-custom">
                                Add New User</label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                            <iframe src="about:blank" id="IframeAddUser" frameborder="0" runat="server" width="100%" height="400px"></iframe>
                        </div>
                    </div>
                </div>
            </div>

            <div>
                <div class="modal fade" id="DocumentViewer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden;">
                    <div class="modal-dialog w100per">
                        <div class="modal-content">
                            <div class="modal-header">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                <label class="modal-header-custom">
                                    View Document(s)</label>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            </div>
                            <div class="modal-body" style="height: 570px;">
                                <div class="col-md-12 colpadding0">
                                    <div class="clearfix"></div>
                                    <div class="col-md-1 colpadding0">
                                        <table width="100%" style="text-align: left; margin-left: 5%;">
                                            <thead>
                                                <tr>
                                                    <td valign="top">
                                                        <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdatleMode="Conditional">
                                                            <ContentTemplate>
                                                                <asp:Repeater ID="rptDocmentVersionView" runat="server" OnItemCommand="rptDocmentVersionView_ItemCommand"
                                                                    OnItemDataBound="rptDocmentVersionView_ItemDataBound">
                                                                    <HeaderTemplate>
                                                                        <table id="tblComplianceDocumnets">
                                                                            <thead>
                                                                                <th>File Name</th>
                                                                            </thead>
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdatleMode="Conditional">
                                                                                    <ContentTemplate>
                                                                                        <asp:LinkButton CommandName="View" CommandArgument='<%# Eval("ID") %>' ID="lblDocumentVersionView"
                                                                                            runat="server" ToolTip='<%# Eval("FileName")%>' Text='<%# Eval("FileName").ToString() %>'></asp:LinkButton>
                                                                                    </ContentTemplate>
                                                                                    <Triggers>
                                                                                        <asp:AsyncPostBackTrigger ControlID="lblDocumentVersionView" />
                                                                                    </Triggers>
                                                                                </asp:UpdatePanel>
                                                                            </td>
                                                                        </tr>
                                                                    </ItemTemplate>
                                                                    <FooterTemplate>
                                                                        </table>
                                                                    </FooterTemplate>
                                                                </asp:Repeater>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:AsyncPostBackTrigger ControlID="rptDocmentVersionView" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>

                                    <div class="col-md-11 colpadding0">
                                        <asp:Label runat="server" ID="lblMessage" Style="color: red;"></asp:Label>
                                        <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; height: 550px; width: 100%;">
                                            <iframe src="about:blank" id="IFrameDocumentViewer" runat="server" width="100%" height="535px"></iframe>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <%--Contract History Popup--%>
            <div class="modal fade" id="divContractHistoryPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog p0" style="width: 100%;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label class="modal-header-custom" id="historyPopUpHeader">
                                Contract History</label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                            <iframe src="about:blank" id="IFrameContractHistory" frameborder="0" runat="server" width="100%" height="450px"></iframe>
                        </div>
                    </div>
                </div>
            </div>

            <%--Link Contract(s) Popup--%>
            <div class="modal fade" id="divLinkContractPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog p5" style="width: 80%;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label class="modal-header-custom">
                                Link Contract(s)</label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                            <asp:UpdatePanel ID="upLinkContracts" runat="server">
                                <ContentTemplate>
                                    <div style="margin-bottom: 7px">
                                        <asp:ValidationSummary ID="vsLinkContract" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                            ValidationGroup="VGLinkContracts" />
                                        <asp:CustomValidator ID="cvLinkContract" runat="server" EnableClientScript="False"
                                            ValidationGroup="VGLinkContracts" Display="None" />
                                    </div>
                                    <div class="row" style="margin: 10px">
                                        <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: #ff0000;">*</label>
                                        <label style="display: block; float: left; font-size: 13px; color: #333;">Select One or More Contract(s) and Click on Save to Link Contract(s)</label>
                                    </div>
                                    <div class="row" style="margin: 10px; max-height: 350px; overflow-y: auto;">
                                        <asp:GridView runat="server" ID="grdContractList_LinkContract" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                            AllowPaging="false" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%" DataKeyNames="ID" >
                                            <Columns>
                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr.No." ItemStyle-Width="5%">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblContractIID" runat="server" Text='<%# Eval("ID") %>' Visible="false"></asp:Label>
                                                        <%#Container.DataItemIndex+1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="5%">
                                                    <HeaderTemplate>
                                                        <asp:CheckBox ID="chkHeaderLinkContracts" runat="server" onclick="javascript:checkAll(this,'grdContractList_LinkContract')" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkRowLinkContracts" runat="server" onclick="javascript:checkUncheckRow(this)" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Contract No" ItemStyle-Width="20%">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px">
                                                            <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ContractNo") %>' ToolTip='<%# Eval("ContractNo") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Contract" ItemStyle-Width="30%">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                                            <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ContractTitle") %>' ToolTip='<%# Eval("ContractTitle") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Vendor" ItemStyle-Width="15%">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                            <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("VendorNames") %>' ToolTip='<%# Eval("VendorNames") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Status" ItemStyle-Width="10%">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                            <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("StatusName") %>' ToolTip='<%# Eval("StatusName") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Department" ItemStyle-Width="10%" Visible="false">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                            <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("DeptName") %>' ToolTip='<%# Eval("DeptName") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                            </Columns>
                                            <RowStyle CssClass="clsROWgrid" />
                                            <HeaderStyle CssClass="clsheadergrid" />
                                            <PagerSettings Visible="false" />
                                            <PagerTemplate>
                                            </PagerTemplate>
                                            <EmptyDataTemplate>
                                                No Record Found
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                    </div>
                                    <div id="divLinkContractSaveCount" class="row col-md-12 plr0" style="display: none;">
                                        <div class="col-md-5 text-left">
                                            <asp:Label runat="server" ID="lblTotalContractSelected" Text="" CssClass="control-label"></asp:Label>
                                        </div>
                                        <div class="col-md-7 text-left">
                                            <asp:Button Text="Save" runat="server" ID="btnSaveLinkContract" CssClass="btn btn-primary"
                                                OnClick="btnSaveLinkContract_Click" ValidationGroup="VGLinkContracts"></asp:Button>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
            <%--END--Link Contract(s) Popup--%>

            <%--Link Document Share Popup--%>
              <div class="modal fade" id="divOpenPermissionPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog p5" style="width: 50%;padding-top: 62px;">
            <div class="modal-content">
                <div class="modal-header">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label class="modal-header-custom">
                        Share With Users
                    </label>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body" style="width: 100%;">
                    <div class="row">
                        <div class="form-group required col-md-12">
                            <asp:ValidationSummary ID="vsPermission" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                ValidationGroup="vsPermissionValidationGroup" />
                            <asp:CustomValidator ID="vsPermissionSet" runat="server" EnableClientScript="False"
                                ValidationGroup="vsPermissionValidationGroup" Display="None" />
                        </div>
                    </div>
                      <div class="row">
                                        <div class="form-group col-md-12">
                                            <label for="lstBoxUsers" class="control-label">Select User(s)</label>
                                         </div>
                                    </div>
                    <div class="row">
                        <div class="form-group required col-md-12">
                         <%--   <label for="lstBoxUsers" class="color-lable">Users</label>   --%>                        
                                    <asp:ListBox ID="lstBoxUsers" CssClass="form-control" runat="server" SelectionMode="Multiple"></asp:ListBox>                                
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group required col-md-12">
                            <asp:Button Text="Share" ID="btnPermission" runat="server" OnClick="btnPermission_Click" CssClass="btn btn-search"></asp:Button>
                        </div>
                              <div class="form-group required col-md-12">
                            <asp:Button Text="UnShare" ID="btnUnshare" runat="server" style="margin-top: -11px;margin-left: 10px" OnClick="btnUnshare_Click" CssClass="btn btn-search"></asp:Button>
                        </div>
                    </div>
                      <div class="row"></div>
                     <div class="row">
                        <div class="form-group required col-md-12"  style="overflow-x: auto;min-height: 89px;"><%--max-height: 212px;--%>
                            <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                <ContentTemplate>
                                    <asp:Repeater ID="myRepeater" runat="server">
                                        <HeaderTemplate>
                                            <div style="font-weight: bold; color: black;">Shared with Users</div>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <div class="row">
                                                <div class="form-group required col-md-12" style="padding-left: 0px;">
                                                    <div class="form-group required col-md-4" style="padding-left: 0px;">
                                                        <asp:Label ID="myLabel" runat="server" Style="color: black; float: left" Text='<%# Eval("VisibkeUserID") %>' />
                                                    </div>
                                                    <div class="form-group required col-md-1" style="padding-left: 0px;">
                                                        <asp:ImageButton ID="LnkDeletShare" runat="server" CommandName="RemoveShare" CommandArgument='<%# Eval("UserPermissionFileId") +","+ Eval("UserId") +","+ Eval("FileType") %>' Style="float: left; padding: 0px 2px 0px 0px;" ImageUrl="~/Images/delete_icon_new.png" data-toggle="tooltip" data-placement="bottom" ToolTip="Click to UnShare File"></asp:ImageButton>                                                         
                                                    </div>
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="myRepeater" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
            <%--END--Link Document Share Popup--%>
              
              <%--executive summary Popup--%>
            <div class="modal fade" id="divexecutivesummaryPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow:hidden">
                <div class="modal-dialog p5" style="width: 80%;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label class="modal-header-custom">
                                Executive Summary</label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">

                          <div class="row">

                         <label class="control-label" style="font-size: larger;font-weight: 500;    margin-left: -178px;">Select Field</label>

                            <div class="row" style="max-height: 254px; overflow-y: scroll; padding-top: 21px;">

                                <asp:GridView runat="server" ID="grdContractField" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true"
                                    AllowSorting="true" GridLines="none" Width="100%" AllowPaging="false" AutoPostBack="true" CssClass="table" DataKeyNames="Id">
                                    <Columns>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="5%">
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="chkHeader" runat="server" onclick="javascript:checkAllTemplateField(this)" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkRow" runat="server" onclick="javascript:checkUncheckRowTemplateField(this)" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Order" Visible="false" ItemStyle-Width="7%" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTemplateFieldID" runat="server" Text='<%# Eval("Id") %>'></asp:Label>
                                                <asp:Label ID="lblTemplateFieldName" runat="server" Text='<%# Eval("ContractTemplate") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="ContractTemplate" HeaderText="Contract Field" ItemStyle-Width="53%" />
                                    </Columns>
                                    <RowStyle CssClass="clsROWgrid" />
                                    <HeaderStyle CssClass="clsheadergrid" />
                                    <PagerTemplate>
                                        <table style="display: none">
                                            <tr>
                                                <td>
                                                    <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                </td>
                                            </tr>
                                        </table>
                                    </PagerTemplate>
                                    <EmptyDataTemplate>
                                        No Record Found
                                    </EmptyDataTemplate>
                                </asp:GridView>

                            </div>
                            </div>
                            <br />

                           
                            <div class="row">
                            <label class="control-label" style="font-size: larger;font-weight: 500;">Select Section</label>
                                  <div class="row" style="max-height: 254px; overflow-y: scroll; padding-top: 21px;">
                                    <asp:GridView runat="server" ID="grdTemplateSection" AutoGenerateColumns="false" 
                                    ShowHeaderWhenEmpty="true"
                                        AllowSorting="true" GridLines="none" Width="100%" AllowPaging="false" AutoPostBack="true" CssClass="table" DataKeyNames="SectionID">
                                        <Columns>
                                            <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="5%">
                                                <HeaderTemplate>
                                                    <asp:CheckBox ID="chkHeader" runat="server" onclick="javascript:checkAllExecutive(this)" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkRow" runat="server" onclick="javascript:checkUncheckRowExecutive(this)" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Order" Visible="false" ItemStyle-Width="7%" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSectionID" runat="server" Text='<%# Eval("SectionID") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="Header" HeaderText="Section Header" ItemStyle-Width="53%" />
                                        </Columns>
                                        <RowStyle CssClass="clsROWgrid" />
                                        <HeaderStyle CssClass="clsheadergrid" />
                                        <PagerTemplate>
                                            <table style="display: none">
                                                <tr>
                                                    <td>
                                                        <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                    </td>
                                                </tr>
                                            </table>
                                        </PagerTemplate>
                                        <EmptyDataTemplate>
                                            No Record Found
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                </div>
                            </div>



                            <div class="row">
                                <div class="col-lg-12 col-md-12">
                                    <div class="col-lg-4 col-md-4">
                                    </div>
                                    <div class="col-lg-2 col-md-2">
                                        <asp:Button Text="Create Executive Summary" runat="server" ID="btnExecutiveSummery" OnClick="btnExecutiveSummery_Click" CssClass="btn btn-primary"></asp:Button>
                                    </div>
                                    <div class="col-lg-4 col-md-4">
                                    </div>
                                </div>
                            </div>



                        </div>
                    </div>
                </div>
            </div>
            <%--END--executive summary Popup--%>


            <%--Mail Document Popup--%>
            <div class="modal fade" id="divMailDocumentPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog" style="width: 50%;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label class="modal-header-custom">
                                Send Email with Document(s)</label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                            <asp:UpdatePanel ID="upMailDocument" runat="server">
                                <ContentTemplate>
                                    <div class="row">
                                        <div class="form-group required col-md-12">
                                            <asp:ValidationSummary ID="vsMailDocument" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                                ValidationGroup="mailDocumentValidationGroup" />
                                            <asp:CustomValidator ID="cvMailDocument" runat="server" EnableClientScript="False"
                                                ValidationGroup="mailDocumentValidationGroup" Display="None" />
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group required col-md-12">
                                            <label for="tbxMailTo" class="control-label">To</label>
                                            <asp:TextBox runat="server" ID="tbxMailTo" CssClass="form-control" autocomplete="off" />
                                            <asp:RequiredFieldValidator ID="rfvMailTo" ErrorMessage="Please enter Email ID of the recipient."
                                                ControlToValidate="tbxMailTo" runat="server" ValidationGroup="mailDocumentValidationGroup" Display="None" />
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group required col-md-12">
                                            <label for="tbxMailMsg" class="control-label">Message</label>
                                            <asp:TextBox runat="server" ID="tbxMailMsg" TextMode="MultiLine" CssClass="form-control" />
                                            <asp:RequiredFieldValidator ID="rfvMailMsg" ErrorMessage="Please enter message to send."
                                                ControlToValidate="tbxMailMsg" runat="server" ValidationGroup="mailDocumentValidationGroup" Display="None" />
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group required col-md-12">
                                            <asp:GridView runat="server" ID="grdMailDocumentList" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                AllowPaging="false" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%" DataKeyNames="FileID">
                                                <Columns>
                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr.No." ItemStyle-Width="5%">
                                                        <ItemTemplate>
                                                            <%#Container.DataItemIndex+1 %>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="5%">
                                                        <HeaderTemplate>
                                                            <asp:CheckBox ID="chkHeaderMailDocument" runat="server" onclick="javascript:checkAll_MailDocument(this)" />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkRowMailDocument" runat="server" onclick="javascript:checkUncheckRow_MailDocument(this)" />
                                                            <asp:Label ID="lblID" runat="server" Text='<%# Eval("FileID") %>' Visible="false"></asp:Label>
                                                            <asp:Label ID="lblFileName" runat="server" Text='<%# Eval("FileName") %>' Visible="false"></asp:Label>
                                                            <asp:Label ID="lblFilePath" runat="server" Text='<%# Eval("FilePath") %>' Visible="false"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Document Type" ItemStyle-Width="25%">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblDocType" runat="server" Text='<%# Eval("DocTypeName") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Document Name" ItemStyle-Width="60%">
                                                        <ItemTemplate>
                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                                <asp:Label ID="lblDocument" runat="server" Text='<%# Eval("FileName") %>' ToolTip='<%# Eval("FileName") %>'></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Version" ItemStyle-Width="5%" Visible="false">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblDocVersion" runat="server" Text='<%# Eval("Version") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <PagerStyle HorizontalAlign="Right" />
                                                <RowStyle CssClass="clsROWgrid" />
                                                <HeaderStyle CssClass="clsheadergrid" />
                                                <PagerTemplate>
                                                </PagerTemplate>
                                                <EmptyDataTemplate>
                                                    No Record Found
                                                </EmptyDataTemplate>
                                            </asp:GridView>
                                        </div>
                                    </div>

                                    <div id="divSendDocCount" class="row col-md-12 plr0" style="display: none;">
                                        <div class="col-md-5 text-left">
                                            <asp:Label runat="server" ID="lblTotalDocumentSelected" Text="" CssClass="control-label"></asp:Label>
                                        </div>
                                        <div class="col-md-7 text-left">
                                            <asp:Button Text="Send" runat="server" style="margin-left: -36px;margin-bottom:-53px;" ID="btnSendMail" CssClass="btn btn-primary" OnClick="btnSendMail_Click"
                                                ValidationGroup="mailDocumentValidationGroup"></asp:Button>
                                        </div>                                        
                                    </div>
                                    <asp:Button Text="Close" style="margin-left: 265px;" runat="server" ID="Button2" CssClass="btn btn-primary" data-dismiss="modal" OnClientClick="CloseMe();" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
            <%--END--Mail Document Popup--%>

            <div class="modal fade" id="AddDocumentsPopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label class="modal-header-custom">
                                Upload Document(s)</label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="CloseUploadDocumentPopup1()">&times;</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                            <iframe src="about:blank" id="IframeAddDocuments" frameborder="0" runat="server" style="width:100%;height:320px;"></iframe>
                        </div>
                    </div>
                </div>
            </div>

              <div class="modal fade" id="AddMilestonePopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label class="modal-header-custom">
                                Milestone Update</label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                            <iframe src="about:blank" id="IframeAddMilestone" frameborder="0" runat="server" style="width:100%;height:320px;"></iframe>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="divDocumentInfoPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog p5" style="width: 50%;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label class="modal-header-custom">
                                Document Details</label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                            <iframe src="about:blank" id="Iframe_DocInfo" frameborder="0" runat="server" width="100%" height="400px"></iframe>
                        </div>
                    </div>
                </div>
            </div>

                 <div class="modal fade" id="divDocumentsharePopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog p5" style="width: 82%;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label class="modal-header-custom">
                                Add Sharing Details</label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                            <iframe src="about:blank" id="Iframe_Docshare" frameborder="0" runat="server" width="100%" height="600px"></iframe>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="divRenewContractPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog p5" style="width: 50%;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label class="modal-header-custom">
                                Renew Contract</label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                            <div class="row form-group col-md-12">
                                <asp:ValidationSummary ID="vsRenewContract" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                    ValidationGroup="RenewContractValidationGroup" />
                                <asp:CustomValidator ID="cvRenewContract" runat="server" EnableClientScript="False"
                                    ValidationGroup="RenewContractValidationGroup" Display="None" />
                            </div>

                            <div class="row col-md-12 float-left">
                                <label for="txtTitle" class="control-label">Do you want to create an new contract with the same details as this contract?</label>
                            </div>

                            <div class="row col-md-12">
                                <label for="fuTaskDocUpload" class="control-label">Choose Contract Document(s) to Import</label>
                            </div>

                            <div class="row col-md-12" style="min-height: 50px; max-height: 300px; overflow-y: auto; display: none;">
                                <asp:UpdatePanel ID="upRenewContractModal" runat="server">
                                    <ContentTemplate>
                                        <asp:GridView runat="server" ID="grdContractDocument_Renew" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                            GridLines="None" AllowPaging="false" AutoPostBack="true" CssClass="table" Width="100%" ShowFooter="false">
                                            <Columns>
                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="5%">
                                                    <HeaderTemplate>
                                                        <asp:CheckBox ID="chkHeader_Import" runat="server" onclick="javascript:checkAll_Import(this)" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkRow_Import" runat="server" onclick="javascript:checkUncheckRow_Import(this)" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr.No." ItemStyle-Width="5%">
                                                    <ItemTemplate>
                                                        <%#Container.DataItemIndex+1 %>
                                                        <asp:Label ID="lblFileID" runat="server" Text='<%# Eval("FileID") %>' Visible="false"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Type" ItemStyle-Width="15%">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDocType" runat="server" Text='<%# Eval("DocTypeName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Document" ItemStyle-Width="30%">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 400px">
                                                            <asp:Label ID="lblFileName" runat="server" Text='<%# Eval("FileName") %>'
                                                                data-toggle="tooltip" data-placement="right" ToolTip='<%# Eval("FileName") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Version" ItemStyle-Width="5%">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDocVersion" runat="server" Text='<%# Eval("Version") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Uploaded By" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="15%">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                            <asp:Label ID="lblUploadedBy" runat="server" Text='<%# Eval("UploadedByName") %>'
                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("UploadedByName") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Uploaded On" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="15%">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                            <asp:Label ID="lblUploadedOn" runat="server" Text='<%# Eval("CreatedOn") != DBNull.Value ? Convert.ToDateTime(Eval("CreatedOn")).ToString("dd-MM-yyyy") : "" %>'
                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("CreatedOn") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <RowStyle CssClass="clsROWgrid" />
                                            <HeaderStyle CssClass="clsheadergrid" />
                                            <EmptyDataTemplate>
                                                No Records Found
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>

                            <div id="divDocCount_ImportRenew" class="row col-md-12 pl0" style="display: none;">
                                <div class="col-md-12 text-left">
                                    <asp:Label runat="server" ID="lblImportDocCount" Text="" CssClass="control-label"></asp:Label>
                                </div>
                            </div>

                            <div class="row col-md-12 text-center">
                                <asp:Button Text="Yes" runat="server" ID="btnRenewContract" CssClass="btn btn-primary" OnClick="btnRenewContract_Click" />
                                <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="btn btn-primary" data-dismiss="modal" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="divShowDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header bgColor-gray" style="height: 30px;">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label class="modal-header-custom col-md-6 plr0">
                                View Contract Detail(s)</label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>

                        <div class="modal-body" style="background-color: #f7f7f7;">
                            <iframe id="showdetails" src="about:blank" width="95%" height="80%" frameborder="0"></iframe>
                        </div>
                    </div>
                </div>
            </div>
               <div class="modal fade" id="DocumentPriview" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden; top: -3%;">
            <div class="modal-dialog" style="width: 100%">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" onclick="fclosedDocumentPriview();" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body" style="height: 570px;">
                        <div style="width: 100%;">
                            <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; height: 550px; width: 100%;">
                                <iframe src="about:blank" id="docPriview" runat="server" width="100%" height="535px"></iframe>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        </div>

          <div class="modal fade" id="divShowDialogTermSheet" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog" style="width:110%;height:500px;">
            <div class="modal-content">
                <div class="modal-header bgColor-gray" style="height: 43px;">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label id="lblname" runat="server" class="modal-header-custom col-md-6 plr0"> </label>
                      
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>

                <div class="modal-body" style="margin-top:-13px;background-color: #f7f7f7;">
                    <iframe id="showdetailsTermSheet" src="about:blank"  frameborder="0"></iframe>
                </div>
            </div>
        </div>
    </div>
        <div id="divForRequestToEdit" style="display: none;">
            <div class="col-md-11" style="margin-bottom: 1.5%;">
                <label class="MainWrapper" style="width: 33%">Select Management User<span style="color: red;">*</span></label>
                <input id="drpDownManagementUser" style="width: 28%" />
            </div>
            <div class="col-md-11" style="margin-bottom: 1.5%;">
                <label class="MainWrapper" style="width: 33%">Remarks<span style="color: red;">*</span></label>
                <textarea id="txtUserRemarks" placeholder="Remarks" class="k-textbox" style="width: 63%; height: 72px;"></textarea>
            </div>
            <div class="row" style="text-align: center;">
                <button id="btnsend" type="button" style="height: 29px; margin: 10px" onclick="btnSendRequest_click()" class="btn btn-primary">Send</button>
                <button id="btnClear" type="button" style="height: 29px" onclick="btnClearRequest_click()" class="btn btn-primary">Clear</button>
            </div>
        </div>
    </form>
    
</body>
</html>
