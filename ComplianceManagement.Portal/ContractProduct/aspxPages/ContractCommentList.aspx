﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ContractProduct.Master" AutoEventWireup="true" CodeBehind="ContractCommentList.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.aspxPages.ContractCommentList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">
        $(document).ready(function () {
            setactivemenu('leftworkspacemenu');
            fhead('My Workspace/ My Comments');
        });

        $('#divShowDialog').on("show", function () {
            $(this).find(".modal-body").css("max-height", height);
        });

        $('#divShowDialog').on('show.bs.modal', function () {

            //alert("called");
            $('#divShowDialog').find('.modal-body').css({
                width: 'auto', //probably not needed
                height: 'auto', //probably not needed 
                'max-height': '100%'
            });
        });

        $(window).bind("load", function () {
            $('#updateProgress').hide();
        });

        jQuery(window).load(function () {
            $('#updateProgress').hide();
        });

        function ShowDialog(ContractInstanceID, RoleID, UserID) {
           
            var modalHeight = screen.height - 150;

            if (modalHeight < 0)
                modalHeight = 200;

            $('#divShowDialog').modal('show');
            $('.modal-dialog').css('width', '95%');
            $('#showdetails').attr('width', '100%');
            $('#showdetails').attr('height', modalHeight + "px");
            $('#showdetails').attr('src', "/ContractProduct/aspxPages/ContractDetailsPage.aspx?AccessID=" + ContractInstanceID + "&AccessRoleID=" + RoleID);
            //$('#showdetails').attr('src', "/ContractProduct/aspxPages/ContractReviewePage.aspx?A=" + ContractInstanceID + "&B=10&C=" + UserID + "&D=" + RoleID);
        };

        function closeModal() {
            document.getElementById('<%= lnkBtnBindGrid.ClientID %>').click();
        }

        $(document).on("click", function (event) {
            if (event.target.id == "") {
                var idvid = $(event.target).closest('div');
                if ($(idvid).attr('id') != undefined && $(idvid).attr('id') != '' && $(idvid).attr('id').indexOf('<%= tvFilterLocation.ClientID %>') > -1) {
                    $("#divFilterLocation").show();
                } else {
                    $("#divFilterLocation").hide();
                }<%-- event.target.id != '<%= tbxFilterLocation.ClientID %>'--%>
            } else if (event.target.id != "" && event.target.id.indexOf('<%= tvFilterLocation.ClientID %>') > -1) {
                $("#divFilterLocation").show();
            } else if (event.target.id != '<%= tbxFilterLocation.ClientID %>') {
                $("#divFilterLocation").hide();
            } else if (event.target.id == '<%= tbxFilterLocation.ClientID %>') {
                $('<%= tbxFilterLocation.ClientID %>').unbind('click');

                $('<%= tbxFilterLocation.ClientID %>').click(function () {
                    $("#divFilterLocation").toggle("blind", null, 500, function () { });
                });
            }
         });
    </script>

    <style>
        .panel-heading {
            background: #ffffff;
        }

            .panel-heading .nav .active, .panel-heading .nav > li.active > a, .panel-heading .nav > li > a:hover {
                color: #1fd9e1;
                background-color: #fff;
            }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row Dashboard-white-widget mb0">
        <div class="col-md-10 colpadding0 w75per">
            <header class="panel-heading tab-bg-primary ">
                <ul class="nav nav-tabs">
                    <li class="" id="liContract" runat="server">
                        <asp:LinkButton ID="lnkContract" style="display:none;"  CausesValidation="false" runat="server" OnClick="lnkContract_Click">Contract</asp:LinkButton>
                    </li>
                    <li class="" id="liTask" runat="server">
                        <asp:LinkButton ID="lnkTask" style="display:none;"  CausesValidation="false" runat="server" OnClick="lnkTask_Click">Task</asp:LinkButton>
                    </li>
                    <li class="active" id="li1" runat="server">
                        <asp:LinkButton ID="lnkRev" style="display:none;"  CausesValidation="false" runat="server" OnClick="lnkRev_Click">My Reviews</asp:LinkButton>
                    </li>
                </ul>
            </header>
        </div>


        <div class="col-md-2 colpadding0 w25per">
           <div class="col-md-8 col-sm-8 col-xs-8 plr0 text-right">
           </div>

            
        </div>
  
        </div>
   
      <!--advance search starts-->
     <div class="row Dashboard-white-widget mb0">
                <div class="dashboard">
              

                <div class="clearfix"></div>

              <%--  <div class="modal-body">--%>
                    <div class="col-md-12 colpadding0">
                       

                        <asp:UpdatePanel ID="upDivLocation" runat="server" UpdateMode="Conditional" OnLoad="upDivLocation_Load">
                            <ContentTemplate>
                                <div class="col-md-3 pl0">
                                   <%-- <label for="tbxFilterLocation" class="filter-label">Entity/Branch/Location</label>--%>
                                    <asp:TextBox runat="server" ID="tbxFilterLocation" PlaceHolder="Click to Select" autocomplete="off" CssClass="form-control" Width="100%" />
                                    <div style="margin-left: 1px; position: absolute; z-index: 10; overflow-y: auto; height: 200px; width: 95%" id="divFilterLocation">
                                        <asp:TreeView runat="server" ID="tvFilterLocation" SelectedNodeStyle-Font-Bold="true" NodeStyle-ForeColor="#8e8e93"
                                            Style="margin-top: -15px; overflow: auto; border-left: 1px solid #c7c7cc; border-right: 1px solid #c7c7cc; border-bottom: 1px solid #c7c7cc; background-color: #ffffff; color: #8e8e93 !important;"
                                            ShowLines="true"
                                            OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged">
                                        </asp:TreeView>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>

                        <div class="col-md-3 pl0 form-group">
                           <%-- <label for="ddlDeptPage" class="filter-label">Department</label>--%>
                            <asp:DropDownListChosen runat="server" ID="ddlDeptPage" AllowSingleDeselect="false" DisableSearchThreshold="3"
                              DataPlaceHolder="Select Department"  class="form-control" Width="100%" />
                        </div>
                        

                         <div class="col-md-3 pl0 form-group" style="width: 22%;">
                             <%--<label for="ddlContractType" class="filter-label">Contract Type</label>--%>
                             <asp:DropDownListChosen runat="server" ID="ddlContractType" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                 DataPlaceHolder="Select Type" class="form-control" Width="100%">
                             </asp:DropDownListChosen>
                               </div>

                        
                         <div class="col-md-3 pl0 form-group" style="width: 28%;">
                             <asp:DropDownListChosen runat="server" ID="ddlContractStatus" AllowSingleDeselect="false" DisableSearchThreshold="5"
                               DataPlaceHolder="Select Status"  class="form-control" Width="100%">
                            </asp:DropDownListChosen>
                           <%-- <asp:DropDownListChosen runat="server" ID="ddlContractRole" AllowSingleDeselect="false" DisableSearchThreshold="5"
                               DataPlaceHolder="Select Role"  class="form-control" Width="100%">
                            </asp:DropDownListChosen>--%>
                        </div>
                        

                    </div>
             

                </div>
                    <div class="col-md-12 colpadding0">

                        <div class="col-md-3 pl0 form-group">
                            <asp:DropDownListChosen runat="server" ID="ddlVendorPage" AllowSingleDeselect="false" DisableSearchThreshold="3"
                               DataPlaceHolder="Select Vendor" class="form-control" Width="100%" />
                            
                        </div>
                        <div class="col-md-3 pl0 form-group">
                           <%-- <label for="ddlVendorPage" class="filter-label">Vendor</label>--%>
                           
                        </div>
                         <div class="col-md-2 col-sm-2 col-xs-2 plr0 text-right" style="display:none;">
                <asp:LinkButton CssClass="btn btn-primary" style="margin-right: 16px;" runat="server" ID="btnAddContract" OnClick="btnAddContract_Click"
                    data-toggle="tooltip" data-placement="bottom" ToolTip="Add a New Contract"><span class="AddNewspan1"><i class='fa fa-plus'></i></span>&nbsp;New</asp:LinkButton>
                <asp:LinkButton Text="Generate" CssClass="btn btn-primary" runat="server" ID="btnGenerateTxn" OnClick="btnGenerateTxn_Click"
                    Width="90%" Visible="false"></asp:LinkButton>
            </div>
                        <div class="col-md-4 pl0 form-group ">
                           <%-- <div class="col-md-6 ">--%>
                                <label for="ddlStatus" class="hidden-label">Filter</label>
                                <asp:LinkButton Text="Apply" CssClass="btn btn-primary" runat="server" ID="lnkBtnApplyFilter"
                                    OnClick="lnkBtnApplyFilter_Click" OnClientClick="javascript:$('#updateProgress').show()" />
                            <%--</div>--%>
                            <%--<div class="col-md-6">--%>
                                <label for="ddlStatus" class="hidden-label">Filter</label>
                                <asp:LinkButton Text="Clear" CssClass="btn btn-primary" runat="server" ID="lnkBtnClearFilter"
                                    OnClick="lnkBtnClearFilter_Click" OnClientClick="javascript:$('#updateProgress').show()" />
                           <%-- </div>--%>
                        </div>
                       
                        <div class="col-md-2 col-sm-6 col-xs-6 plr0 text-right">
                   <%-- <a class="btn btn-advanceSearch" data-toggle="modal" href="#AdvanceSearch">Advanced Search</a>--%>
                          <%-- <label for="lblfilter" class="filter-label">Filter:</label>--%>
                     <asp:TextBox DataPlaceHolder="Filter" Visible="false" runat="server" ID="tbxFilter1" Width="250px" MaxLength="50" AutoPostBack="true"
                       OnTextChanged="tbxFilter1_TextChanged" />
                        </div>
                       
                   </div>
           </div>
        <!--advance search ends--> 

    <div class="row Dashboard-white-widget">
        <div class="dashboard">
            <div class="col-md-12 colpadding0">
                <asp:ValidationSummary ID="vsContractListPage" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                    ValidationGroup="ContractListPageValidationGroup" />
                <asp:CustomValidator ID="cvErrorContractListPage" runat="server" EnableClientScript="False"
                    ValidationGroup="ContractListPageValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
            </div>

            <div class="clearfix"></div>

            <div class="col-md-12 colpadding0">
                <asp:GridView runat="server" ID="grdContractList" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                    PageSize="10" AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%" DataKeyNames="ID"
                    OnRowCommand="grdContractList_RowCommand" OnSorting="grdContractList_Sorting" OnRowCreated="grdContractList_RowCreated">
                    <Columns>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr.No." ItemStyle-Width="3%">
                            <ItemTemplate>
                                <%#Container.DataItemIndex+1 %>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Contract No" ItemStyle-Width="20%" SortExpression="ContractNo">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ContractNo") %>' ToolTip='<%# Eval("ContractNo") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Contract Title" ItemStyle-Width="20%" SortExpression="ContractTitle">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ContractTitle") %>' ToolTip='<%# Eval("ContractTitle") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                         <asp:TemplateField HeaderText="Department" ItemStyle-Width="10%" SortExpression="DeptName">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("DeptName") %>' ToolTip='<%# Eval("DeptName") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Vendor" ItemStyle-Width="15%" SortExpression="VendorNames">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("VendorNames") %>' ToolTip='<%# Eval("VendorNames") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="End Date" ItemStyle-Width="15%" ItemStyle-CssClass="text-center" SortExpression="ExpirationDate">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ExpirationDate") != null ? Convert.ToDateTime(Eval("ExpirationDate")).ToString("dd-MM-yyyy") : "null" %>'
                                        ToolTip='<%# Eval("ExpirationDate") != null ? Convert.ToDateTime(Eval("ExpirationDate")).ToString("dd-MM-yyyy") : "null" %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Entity" ItemStyle-Width="17%" Visible="false">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("BranchName") %>' ToolTip='<%# Eval("BranchName") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Department" ItemStyle-Width="10%" Visible="false">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("DeptName") %>' ToolTip='<%# Eval("DeptName") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Status" ItemStyle-Width="10%" SortExpression="StatusName">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("StatusName") %>' ToolTip='<%# Eval("StatusName") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Action" ItemStyle-Width="10%" HeaderStyle-CssClass="text-right" ItemStyle-CssClass="text-right">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkEditContract" runat="server" OnClick="lnkEditContract_Click" CommandArgument='<%# Eval("ID") %>'
                                    data-toggle="tooltip" data-placement="bottom" ToolTip="View/ Edit">                                   
                                    <img src='<%# ResolveUrl("~/Images/edit_icon_new.png")%>' alt="Edit"/>
                                </asp:LinkButton>
                                <asp:LinkButton ID="lnkDeleteContract" style="display:none;" runat="server" CommandName="DELETE_Contract" Visible="true"
                                    data-toggle="tooltip" data-placement="bottom" ToolTip="Delete" CommandArgument='<%# Eval("ID") %>'
                                    OnClientClick="return confirm('Are you certain you want to Delete this Contract? This will also delete associated Document(s), Task(s) and other related information.');">
                                     <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="Delete"/>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="clsROWgrid" />
                    <HeaderStyle CssClass="clsheadergrid" />
                    <PagerSettings Visible="false" />
                    <PagerTemplate>
                    </PagerTemplate>
                    <EmptyDataTemplate>
                        No Record Found
                    </EmptyDataTemplate>
                </asp:GridView>
            </div>
            <div class="row">
            <div class="col-md-12 colpadding0">
                <div class="col-md-10 colpadding0">
                    <div runat="server" id="DivRecordsScrum" style="color: #999">
                        <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>-
                        <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                        <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>

                    </div>
                </div>
                <div class="col-md-1 text-right colpadding0">
                    <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width:100%; float: right; height: 32px !important; margin-right:6%"
                        AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                        <asp:ListItem Text="5" />
                        <asp:ListItem Text="10" Selected="True" />
                        <asp:ListItem Text="20" />
                        <asp:ListItem Text="50" />
                    </asp:DropDownList>
                </div>
                <div class="col-md-1 text-right colpadding0">
                    <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true" DataPlaceHolder="No" AllowSingleDeselect="false"
                        OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged" class="form-control m-bot15" Width="100%" Height="32px">
                    </asp:DropDownListChosen>
                </div>
                <asp:LinkButton ID="lnkBtnBindGrid" OnClick="lnkBtnBindGrid_Click" Style="float: right; display: none;" Width="100%" runat="server">
                </asp:LinkButton>
                <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
            </div>
                </div>
        </div>
    </div>

    <div class="modal fade" id="divShowDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bgColor-gray" style="height: 30px;">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label class="modal-header-custom col-md-6 plr0">
                        Contract Detail(s)</label>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="closeModal();">&times;</button>
                </div>

                <div class="modal-body" style="background-color: #f7f7f7;">
                    <iframe id="showdetails" src="about:blank" width="95%" height="80%" frameborder="0"></iframe>
                </div>
            </div>
        </div>
    </div>   
</asp:Content>


