﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ContractMilestoneDetails.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.aspxPages.ContractMilestoneDetails" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   <title>Contract Detail</title>
    <!-- Bootstrap CSS -->
    <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- bootstrap theme -->
    <link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />
        <!-- font icon -->
    <link href="~/NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <!--external css-->
    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/contract_custom_style.css" rel="stylesheet" />
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    
    <script type="text/javascript" src="/Newjs/jquery.js"></script>
    <script type="text/javascript" src="/Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="/Newjs/bootstrap.min.js"></script>

    <link href="~/NewCSS/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
    <script src="/Newjs/bootstrap-multiselect.js" type="text/javascript"></script>

    <script src="/Newjs/bootstrap-tagsinput.js"></script>
    <link href="/NewCSS/bootstrap-tagsinput.css" rel="stylesheet" />

    <link href="/NewCSS/timeline.css" rel="stylesheet" />
    <!-- nice scroll -->
    <script type="text/javascript" src="/Newjs/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="/Newjs/jquery.nicescroll.js"></script>

    <script src="/Newjs/tag-scrolling.js" type="text/javascript"></script>
    <link href="/NewCSS/tag-scrolling.css" rel="stylesheet" />

    <script type="text/javascript">
        function OpenMilestonePopup() {
            $('#AddMilestonePopUp').modal('show');
        }
        function OpenDocviewer(file) {
            $('#DocumentViewerPopup').modal('show');
            $('#iframeDocViewer').attr('src', "/docviewer.aspx?docurl=" + file);
        }
        function closemodel() {            
            $('#AddMilestonePopUp').modal('hide');
        }
    </script>

</head>
<body style="background-color: #f7f7f7;">
    <form id="form1" runat="server">
     <asp:ScriptManager ID="smContractDetailPage" EnablePartialRendering="true" runat="server"></asp:ScriptManager>
       <div class="container" style="background-color: #f7f7f7;">

        <div class="row Dashboard-white-widget">
      <%--  AllowPaging="true"--%>
    <div class="mainDiv" style="background-color: #fff;">
        <div class="col-md-12 plr0 text-right">
           <div class="col-md-2 text-left">
               <asp:DropDownList runat="server" ID="drpdownmilestone"
                   AllowSingleDeselect="false"
                   class="form-control" Width="100%" DataPlaceHolder="Select Milestone">
               </asp:DropDownList>
           </div>
            <div class="col-md-2 text-left">
                <asp:DropDownList ID="dropmilestonestatus" runat="server" CssClass="form-control">
                    <asp:ListItem Text="Select Status" Value="-1"></asp:ListItem>
                    <asp:ListItem Text="In Progress" Value="1"></asp:ListItem>
                    <asp:ListItem Text="Open" Value="2"></asp:ListItem>
                    <asp:ListItem Text="Completed" Value="3"></asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="col-md-2 text-left">
               <asp:Button ID="btnapplymilestone" Text="Apply" CssClass="btn btn-primary" runat="server" OnClick="btnapplymilestone_Click" />
            </div>
        </div>

        <asp:GridView runat="server" ID="gridMilestone" AutoGenerateColumns="false" AllowSorting="true"
            ShowHeaderWhenEmpty="true"
            GridLines="None" PageSize="10"  AutoPostBack="true" CssClass="table" Width="100%"
            DataKeyNames="ID" OnRowCommand="gridMilestone_RowCommand"
            PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Right">
            <Columns>
                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr.No." ItemStyle-Width="5%">
                    <ItemTemplate>
                        <%#Container.DataItemIndex+1 %>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Title" ItemStyle-Wrap="true" ItemStyle-Width="15%">
                    <ItemTemplate>
                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                            <asp:Label ID="lbl21" runat="server" Text='<%# Eval("Title") %>'
                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Title") %>'></asp:Label>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Description" ItemStyle-Wrap="true" ItemStyle-Width="15%">
                    <ItemTemplate>
                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                            <asp:Label ID="lbl4" runat="server" Text='<%# Eval("Description") %>'
                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Description") %>'></asp:Label>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Department" ItemStyle-Wrap="true" ItemStyle-Width="15%">
                    <ItemTemplate>
                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                            <asp:Label ID="lbl2" runat="server" Text='<%# Eval("DeptName") %>'
                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("DeptName") %>'></asp:Label>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>

                 <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Start Date" ItemStyle-Width="10%">
                    <ItemTemplate>
                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px;">
                            <asp:Label ID="lblstartDate" runat="server" Text='<%# Eval("StartDate") != null ? Convert.ToDateTime(Eval("StartDate")).ToString("dd-MM-yyyy") : "" %>'
                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("StartDate") != null ? Convert.ToDateTime(Eval("StartDate")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="End Date" ItemStyle-Width="10%">
                    <ItemTemplate>
                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px;">
                            <asp:Label ID="lblEndDate" runat="server" Text='<%# Eval("EndDate") != null ? Convert.ToDateTime(Eval("EndDate")).ToString("dd-MM-yyyy") : "" %>'
                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("EndDate") != null ? Convert.ToDateTime(Eval("EndDate")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Due Date" ItemStyle-Width="10%">
                    <ItemTemplate>
                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px;">
                            <asp:Label ID="lblDueDate" runat="server" Text='<%# Eval("DueDate") != null ? Convert.ToDateTime(Eval("DueDate")).ToString("dd-MM-yyyy") : "" %>'
                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("DueDate") != null ? Convert.ToDateTime(Eval("DueDate")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Status" ItemStyle-Width="20%">
                    <ItemTemplate>
                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                            <asp:Label ID="lblCreatedBy" runat="server" Text='<%# Eval("Status") %>'
                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Status") %>'></asp:Label>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Action" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center" ItemStyle-Width="10%">
                    <ItemTemplate>
                        <asp:UpdatePanel runat="server" ID="upTaskActivityAction" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                    <asp:LinkButton CommandArgument='<%# Eval("ID")%>' AutoPostBack="true" CommandName="EditMilestone"
                                        ID="lnkBtnEditMileStoneDoc" Visible='<%# Eval("StatusID").ToString() == "3" ? false : true %>'
                                         runat="server" data-toggle="tooltip" data-placement="bottom" ToolTip="Edit/View Milestone Details">
                                                                                                <img src='<%# ResolveUrl("~/Images/edit_icon_new.png")%>' alt="Edit"/>
                                    </asp:LinkButton>
                                </div>
                            </ContentTemplate>
                         <%--   <Triggers>
                                <asp:PostBackTrigger ControlID="lnkBtnResDeleteMileStone" />
                            </Triggers>--%>
                        </asp:UpdatePanel>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <RowStyle CssClass="clsROWgrid" />
            <HeaderStyle CssClass="clsheadergrid" />
            <EmptyDataTemplate>
                No Records Found
            </EmptyDataTemplate>
        </asp:GridView>
    </div>
    </div>
    </div>
       <div class="modal fade" id="AddMilestonePopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content" style="width: 1021px;margin-left: -223px;">
                        <div class="modal-header">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label class="modal-header-custom">
                                Milestone Update</label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                         <asp:UpdatePanel runat="server" ID="UpdatePanel1">
                                        <ContentTemplate>
                            <div class="row">
                                <asp:ValidationSummary ID="vsTaskResponse" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                    ValidationGroup="TaskResponseValidationGroup" />
                                <asp:CustomValidator ID="cvTaskResponse" runat="server" EnableClientScript="False"
                                    ValidationGroup="TaskResponseValidationGroup" Display="None" />
                                <asp:Label ID="Labelmsg" class="alert alert-block alert-danger fade in" Style="display: none;" runat="server"></asp:Label>
                            </div>

                            <div class="row" style="padding-top: 6px;">
                                <div class="col-lg-12 col-md-12">
                                    <div class="col-lg-2 col-md-2">
                                        <label class="control-label">Title</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10">
                                        <asp:Label ID="txtboxMilestonetitle" runat="server" CssClass="text-label"></asp:Label>
                                        <%--<asp:TextBox runat="server" Enabled="false" ID="txtboxMilestonetitle" CssClass="form-control" />--%>
                                    </div>
                                </div>
                            </div>

                            <div class="row" style="padding-top: 6px;">
                                <div class="col-lg-12 col-md-12">
                                    <div class="col-lg-2 col-md-2">
                                        <label class="control-label">Description</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10">
                                        <asp:Label ID="txtboxMilestonedescription" runat="server" CssClass="text-label"></asp:Label>
                                        <%--<asp:TextBox runat="server" Enabled="false" ID="" TextMode="MultiLine" CssClass="form-control" />--%>
                                    </div>
                                </div>
                            </div>

                            <div class="row" style="padding-top: 6px;">
                                <div class="col-lg-12 col-md-12">
                                    <div class="col-lg-2 col-md-2">
                                        <label class="control-label">Department</label>
                                    </div>
                                    <div class="col-lg-4 col-md-4">
                                        <asp:Label ID="lblmilestoneDepartment" runat="server" CssClass="text-label"></asp:Label>
                                        <asp:DropDownListChosen runat="server" Visible="false" ID="drpmilestoneDepartment" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                            DataPlaceHolder="Select Department" class="form-control" Width="100%" />
                                    </div>

                                </div>
                            </div>

                            <div class="row" style="padding-top: 6px;">
                                <div class="col-lg-12 col-md-12">
                                    <div class="col-lg-2 col-md-2">
                                        <label class="control-label">User</label>
                                    </div>
                                    <div class="col-lg-4 col-md-4">
                                        <asp:Label ID="lblmilestoneUser" runat="server" CssClass="text-label"></asp:Label>
                                        <asp:DropDownListChosen runat="server" Visible="false" ID="drpmilestoneDepartmentUser" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                            DataPlaceHolder="Select User" class="form-control" Width="100%" />
                                    </div>
                                </div>
                            </div>

                            <div class="row" style="padding-top: 6px;">
                                <div class="col-lg-12 col-md-12">
                                    <div class="col-lg-2 col-md-2">
                                        <label class="control-label">Due Date</label>
                                    </div>
                                    <div class="col-lg-4 col-md-4">
                                        <asp:Label ID="txbmilestoneduedate" runat="server" CssClass="text-label"></asp:Label>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row" style="padding-top: 6px;">
                                <asp:UpdatePanel ID="upTaskResponseDocUpload" runat="server">
                                    <ContentTemplate>
                                        <asp:GridView runat="server" ID="grdmilestonekResponseLog" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                            GridLines="None" PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%"
                                            PagerSettings-Position="Top" PagerStyle-HorizontalAlign="Right"
                                            OnRowCommand="grdmilestonekResponseLog_RowCommand">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Sr.No." ItemStyle-HorizontalAlign="Left" ItemStyle-Width="5%" HeaderStyle-Width="5%">
                                                    <ItemTemplate>
                                                        <%#Container.DataItemIndex+1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Status" HeaderStyle-Width="10%" ItemStyle-Width="10%">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStatus" runat="server" Text='<%# Eval("Status") %>'
                                                            data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Status") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Created By" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCreatedByName" runat="server" Text='<%# Eval("CreatedByName") %>'
                                                            data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("CreatedByName") %>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Date Created" HeaderStyle-Width="20%" ItemStyle-Width="20%">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStatusChangedOn" runat="server" Text='<%# Eval("CreatedOn") != null ? Convert.ToDateTime(Eval("CreatedOn")).ToString("dd-MM-yyyy HH:mm:ss tt") : "" %>'
                                                            data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("CreatedOn") != null ? Convert.ToDateTime(Eval("CreatedOn")).ToString("dd-MM-yyyy HH:mm:ss tt") : "" %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Remark" ItemStyle-Width="30%" HeaderStyle-Width="30%">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                            <asp:Label ID="lblComment" runat="server" Text='<%# Eval("Remark") %>'
                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Remark") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Documents" ItemStyle-Width="10%" HeaderStyle-Width="10%">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%;">
                                                            <asp:Label ID="lblResDoc" runat="server" Text='<%# ShowMilestoneResponseDocCount((long)Eval("ContractID"), (long)Eval("MileStoneID"),(long)Eval("ID")) %>'> 
                                                            </asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Action" ItemStyle-Width="20%" HeaderStyle-Width="20%"
                                                    ItemStyle-CssClass="text-right" HeaderStyle-CssClass="text-right">
                                                    <ItemTemplate>
                                                        <asp:UpdatePanel runat="server" ID="upTaskResDocument" UpdateMode="Always">
                                                            <ContentTemplate>
                                                                <asp:LinkButton
                                                                    CommandArgument='<%# Eval("ID")+","+ Eval("MileStoneID")+","+ Eval("ContractID")%>' CommandName="ViewTaskResponseDoc"
                                                                    ID="lnkBtnViewTaskResDoc" runat="server">
                                                                                        <img src='<%# ResolveUrl("~/Images/Eye.png")%>' alt="View" title="View Documents" />
                                                                </asp:LinkButton>

                                                                <asp:LinkButton
                                                                    CommandArgument='<%# Eval("ID")+","+ Eval("MileStoneID")+","+ Eval("ContractID")%>' CommandName="DownloadTaskResponseDoc"
                                                                    ID="lnkBtnDownloadTaskResDoc" runat="server">
                                                                                        <img src='<%# ResolveUrl("~/Images/download_icon_new.png")%>' alt="Download" title="Download Documents" />
                                                                </asp:LinkButton>

                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:PostBackTrigger ControlID="lnkBtnViewTaskResDoc" />
                                                                <asp:PostBackTrigger ControlID="lnkBtnDownloadTaskResDoc" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                            </Columns>
                                            <RowStyle CssClass="clsROWgrid" />
                                            <HeaderStyle CssClass="clsheadergrid" />
                                            <EmptyDataTemplate>
                                                No Records Found
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>

                            <div class="row" style="padding-top: 6px;">
                                <div class="col-lg-12 col-md-12">
                                    <div class="col-lg-2 col-md-2">
                                        <label class="control-label">Status</label>
                                        <label style="color: #ff0000;">*</label>
                                    </div>
                                    <div class="col-lg-4 col-md-4">
                                        <asp:DropDownList ID="drpmilestonestatus" runat="server" CssClass="form-control">
                                            <%--<asp:ListItem Text="Select Status" Value="-1" Selected="True"></asp:ListItem>--%>
                                            <asp:ListItem Text="In Progress" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="Open" Value="2"></asp:ListItem>
                                            <asp:ListItem Text="Completed" Value="3"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>

                            <div class="row" style="padding-top: 6px;">
                                <div class="col-lg-12 col-md-12">
                                    <div class="col-lg-2 col-md-2">
                                        <label class="control-label">Remark</label>
                                        <label style="color: #ff0000;">*</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10">
                                        <asp:TextBox runat="server" ID="tbxremarkmilestone" TextMode="MultiLine" CssClass="form-control" />
                                         <asp:RequiredFieldValidator ID="rfvMailTo" ErrorMessage="Required Remark"
                                                ControlToValidate="tbxremarkmilestone" runat="server" ValidationGroup="TaskResponseValidationGroup" Display="None" />
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row" style="padding-top: 6px;">
                                <div class="col-lg-12 col-md-12">
                                    <div class="form-group col-md-2">
                                        <label for="tbxTaskResComment" class="control-label">Upload Document(s)</label>
                                    </div>

                                    <div class="form-group col-md-10">
                                        <asp:FileUpload ID="fuTaskResponseDocUpload" runat="server" AllowMultiple="true" Style="color: #8e8e93; margin-bottom: 15px;" />
                                    </div>
                                </div>
                            </div>

                            <div class="row" style="padding-top: 6px;">
                                <div class="col-lg-12 col-md-12">
                                    <div class="col-lg-4 col-md-4">
                                    </div>
                                    <div class="col-lg-2 col-md-2">
                                        <asp:Button Text="Save" runat="server" OnClick="btnSaveMilestone_Click" ValidationGroup="TaskResponseValidationGroup" ID="btnSaveMilestone" CssClass="btn btn-primary" />
                                    </div>
                                    <div class="col-lg-2 col-md-2">
                                    <asp:Button Text="Close" runat="server" ID="btnmilestoneclosed1" CssClass="btn btn-primary" OnClick="btnmilestoneclosed1_Click" />
                                    </div>
                                </div>
                            </div>

                             </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnSaveMilestone" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>


              <%--Document Viewer--%>                
                <div class="modal fade" id="DocumentViewerPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden;">
                    <div class="modal-dialog" style="width: 100%">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body" style="height: 570px;">
                                <div style="width: 100%;">
                                    <div style="float: left; width: 10%">
                                        <table width="100%" style="text-align: left; margin-left: 5%;">
                                            <thead>
                                                <tr>
                                                    <td valign="top">
                                                        <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdatleMode="Conditional">
                                                            <ContentTemplate>
                                                                <asp:Repeater ID="rptDocmentVersionView" runat="server" OnItemCommand="rptDocmentVersionView_ItemCommand"
                                                                    OnItemDataBound="rptDocmentVersionView_ItemDataBound">
                                                                    <HeaderTemplate>
                                                                        <table id="tblComplianceDocumnets">
                                                                            <thead>
                                                                                <th>File Name</th>
                                                                            </thead>
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdatleMode="Conditional">
                                                                                    <ContentTemplate>
                                                                                        <asp:LinkButton CommandName="View" CommandArgument='<%# Eval("ID") %>' ID="lblDocumentVersionView"
                                                                                            runat="server" ToolTip='<%# Eval("FileName")%>' Text='<%# Eval("FileName").ToString() %>'></asp:LinkButton>
                                                                                    </ContentTemplate>
                                                                                    <Triggers>
                                                                                        <asp:AsyncPostBackTrigger ControlID="lblDocumentVersionView" />
                                                                                    </Triggers>
                                                                                </asp:UpdatePanel>
                                                                            </td>
                                                                        </tr>
                                                                    </ItemTemplate>
                                                                    <FooterTemplate>
                                                                        </table>
                                                                    </FooterTemplate>
                                                                </asp:Repeater>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:AsyncPostBackTrigger ControlID="rptDocmentVersionView" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                    <div style="float: left; width: 90%">
                                        <asp:Label runat="server" ID="lblMessage" Style="color: red;"></asp:Label>
                                        <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; height: 550px; width: 100%;">
                                            <iframe src="about:blank" id="iframeDocViewer" runat="server" width="100%" height="100%"></iframe>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                    
                <%--Document Viewer-END--%>
    </form>
</body>
</html>
