﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.aspxPages
{
    public partial class ContractTaskList : System.Web.UI.Page
    {
        protected bool flag;
        protected static string TaskAssigned;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //BindTaskStatusList(true, true);
                if (AuthenticationHelper.Role != "MGMT" && AuthenticationHelper.Role != "CADMN")
                {
                    TaskAssigned = "none";
                }
                else
                {
                    TaskAssigned = "block";
                }
                if (!String.IsNullOrEmpty(Request.QueryString["Status"]))
                {
                    string queryStr = Request.QueryString["Status"].ToString().Trim();
                    if (queryStr == "nan")
                    {
                        Session.Remove("taskStatus");
                    }
                    else
                    {
                        Session["taskStatus"] = queryStr;
                        if (ddlTaskStatus.Items.FindByText(queryStr) != null)
                        {
                            ddlTaskStatus.ClearSelection();
                            ddlTaskStatus.Items.FindByText(queryStr).Selected = true;
                        }
                    }
                }

                BindGrid();
                bindPageNumber();
                ShowGridDetail();
            }
        }

        private void BindTaskStatusList(bool isForTask, bool isVisibleToUser)
        {
            try
            {
                ddlTaskStatus.DataSource = null;
                ddlTaskStatus.DataBind();
                ddlTaskStatus.ClearSelection();

                ddlTaskStatus.DataTextField = "StatusName";
                ddlTaskStatus.DataValueField = "ID";

                var statusList = ContractTaskManagement.GetStatusList_All(isForTask, isVisibleToUser);

                //List<Cont_tbl_StatusMaster> allowedStatusList = null;

                //List<int> finalStatusIDs = ContractTaskManagement.GetFinalStatusByInitialID(statusID, roleID);
                //allowedStatusList = statusList.Where(entry => finalStatusIDs.Contains((int)entry.ID)).OrderBy(entry => entry.StatusName).ToList();

                ddlTaskStatus.DataSource = statusList;
                ddlTaskStatus.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvTaskPage.IsValid = false;
                cvTaskPage.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void lnkBtn_RebindTask_Click(object sender, EventArgs e)
        {
            BindGrid();
        }

        public void BindGrid()
        {
            try
            {
                int priorityID = -1;
                int TaskAssigned = 0;
                string queryStr = string.Empty;
                string taskStatus = string.Empty;
                if (Session["taskStatus"] != null)
                {
                    queryStr = Session["taskStatus"].ToString();
                    if (ddlTaskStatus.Items.FindByText(queryStr) != null)
                    {
                        ddlTaskStatus.ClearSelection();
                        ddlTaskStatus.Items.FindByText(queryStr).Selected = true;
                    }
                }
                if (!string.IsNullOrEmpty(ddlTaskStatus.SelectedItem.Text))
                {
                    taskStatus = ddlTaskStatus.SelectedItem.Text.Trim();
                }

                if (!string.IsNullOrEmpty(ddlTaskPriority.SelectedValue))
                {
                    priorityID = Convert.ToInt32(ddlTaskPriority.SelectedValue);
                }

                var lstTaskDetails = ContractTaskManagement.GetAssignedTaskList(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, AuthenticationHelper.Role, 3, priorityID, taskStatus);

                #region ALl Task and My Task Gaurav
                if (!string.IsNullOrEmpty(ddlTaskAssigned.SelectedValue))
                {
                    TaskAssigned = Convert.ToInt32(ddlTaskAssigned.SelectedValue);
                }
                if (TaskAssigned != 0)
                {
                    lstTaskDetails = lstTaskDetails.Where(entry => entry.AssignedTaskUser.Contains(Convert.ToString(AuthenticationHelper.UserID))).ToList();
                }
                #endregion

                #region Filter Code
                if (!string.IsNullOrEmpty(tbxFilter.Text))
                {
                    lstTaskDetails = lstTaskDetails.Where(entry => entry.ContractTitle.ToUpper().Contains(tbxFilter.Text.ToUpper()) || entry.ContractNo.ToUpper().Contains(tbxFilter.Text.ToUpper()) || entry.TaskTitle.ToUpper().Contains(tbxFilter.Text.ToUpper())).ToList();
                    //if (CheckInt(tbxFilter.Text))
                    //{
                    //    int a = Convert.ToInt32(tbxFilter.Text.ToUpper());
                    //    lstTaskDetails = lstTaskDetails.Where(entry => entry.ContractID == a || entry.ContractNo.ToUpper().Contains(tbxFilter.Text.ToUpper())).ToList();
                    //    //&& entry.ContractNo.ToUpper().Contains(tbxFilter.Text.ToUpper())
                    //}
                    //else
                    //{
                    //    lstTaskDetails = lstTaskDetails.Where(entry => entry.ContractTitle.ToUpper().Contains(tbxFilter.Text.ToUpper()) || entry.ContractNo.ToUpper().Contains(tbxFilter.Text.ToUpper()) || entry.TaskTitle.ToUpper().Contains(tbxFilter.Text.ToUpper())).ToList();
                    //}
                }
                #endregion
                Session["TotalRows"] = null;
                if (lstTaskDetails.Count > 0)
                {
                    flag = true;
                    grdTaskActivity.DataSource = lstTaskDetails;
                    grdTaskActivity.DataBind();
                    Session["TotalRows"] = lstTaskDetails.Count;
                }
                else
                {
                    grdTaskActivity.DataSource = lstTaskDetails;
                    grdTaskActivity.DataBind();
                    Session["TotalRows"] = null;
                }
                lstTaskDetails.Clear();
                lstTaskDetails = null;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvTaskPage.IsValid = false;
                cvTaskPage.ErrorMessage = "Something went wrong, Please try again";
            }
        }
        protected void lnkBtnTaskResponse_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton) (sender);
                if (btn != null)
                {
                    string[] commandArgs = btn.CommandArgument.ToString().Split(new char[] { ',' });

                    long taskID = Convert.ToInt64(commandArgs[0]);
                    long contractID = Convert.ToInt64(commandArgs[1]);
                    int roleID = Convert.ToInt32(commandArgs[2]);

                    if (taskID != 0 && contractID != 0)
                    {
                        var strContractID = CryptographyManagement.Encrypt(contractID.ToString());
                        var strTaskID = CryptographyManagement.Encrypt(taskID.ToString());
                        var strUserID = CryptographyManagement.Encrypt(AuthenticationHelper.UserID.ToString());
                        var strRoleID = CryptographyManagement.Encrypt(roleID.ToString());

                        string checkSum = Util.CalculateMD5Hash(contractID.ToString() + taskID.ToString() + AuthenticationHelper.UserID.ToString() + roleID.ToString());

                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowDialog('" + strContractID + "','" + strTaskID + "','" + strUserID + "','" + strRoleID + "','" + checkSum + "');", true);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvTaskPage.IsValid = false;
                cvTaskPage.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdTaskActivity.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);

                BindGrid();
                bindPageNumber();

                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdTaskActivity.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }

                ShowGridDetail();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlTypePage_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (!String.IsNullOrEmpty(ddlTypePage.SelectedValue))
                {
                    if (ddlTypePage.SelectedValue == "1")
                        Response.Redirect("~/ContractProduct/aspxPages/ContractList.aspx", false);

                    else if (ddlTypePage.SelectedValue == "2")
                        Response.Redirect("~/ContractProduct/aspxPages/ContractTaskList.aspx", false);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lnkContract_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/ContractProduct/aspxPages/ContractList.aspx", false);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lnkBtnApplyFilter_Click(object sender, EventArgs e)
        {
            try
            {
                BindGrid();
                bindPageNumber();
                ShowGridDetail();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lnkBtnClearFilter_Click(object sender, EventArgs e)
        {
            try
            {
                ddlTaskPriority.ClearSelection();
                ddlTaskStatus.ClearSelection();
                ddlTaskAssigned.ClearSelection();
                tbxFilter.Text = string.Empty;
                lnkBtnApplyFilter_Click(sender, e);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


        #region Page Number-Bottom

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());

            grdTaskActivity.PageIndex = chkSelectedPage - 1;
            grdTaskActivity.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);

            BindGrid();
            ShowGridDetail();
        }

        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        private void ShowGridDetail()
        {
            if (Session["TotalRows"] != null)
            {
                int PageSize = 0;
                int PageNumber = 0;

                if (!string.IsNullOrEmpty(ddlPageSize.SelectedItem.Text))
                    PageSize = Convert.ToInt32(ddlPageSize.SelectedItem.Text);

                if (!string.IsNullOrEmpty(DropDownListPageNo.SelectedValue))
                    PageNumber = Convert.ToInt32(DropDownListPageNo.SelectedValue);

                var EndRecord = 0;
                var TotalRecord = 0;
                var TotalValue = PageSize * PageNumber;

                TotalRecord = Convert.ToInt32(Session["TotalRows"]);
                if (TotalRecord < TotalValue)
                {
                    EndRecord = TotalRecord;
                }
                else
                {
                    EndRecord = TotalValue;
                }

                if (TotalRecord != 0)
                    lblStartRecord.Text = Convert.ToString(PageSize * PageNumber - PageSize + 1);
                else
                    lblStartRecord.Text = "0";

                lblEndRecord.Text = Convert.ToString(EndRecord) + " ";
                lblTotalRecord.Text = TotalRecord.ToString();
            }
            else
            {
                lblStartRecord.Text = "0";
                lblEndRecord.Text = "0 ";
                lblTotalRecord.Text = "0";
            }
        }


        #endregion

        protected void grdTaskActivity_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                int priorityID = -1;

                string taskStatus = string.Empty;

                if (!string.IsNullOrEmpty(ddlTaskStatus.SelectedItem.Text))
                {
                    taskStatus = ddlTaskStatus.SelectedItem.Text.Trim();
                }

                if (!string.IsNullOrEmpty(ddlTaskPriority.SelectedValue))
                {
                    priorityID = Convert.ToInt32(ddlTaskPriority.SelectedValue);
                }

                var lstTaskDetails = ContractTaskManagement.GetAssignedTaskList(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, AuthenticationHelper.Role, 3, priorityID, taskStatus);


                string SortExpr = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                {
                    SortExpr = Convert.ToString(ViewState["SortExpression"]);
                    if (SortExpr == e.SortExpression)
                    {
                        if (direction == SortDirection.Ascending)
                        {
                            direction = SortDirection.Descending;
                        }
                        else
                        {
                            direction = SortDirection.Ascending;
                        }
                    }
                    else
                    {
                        direction = SortDirection.Ascending;
                    }
                }

                if (direction == SortDirection.Ascending)
                {
                    ViewState["Direction"] = "Ascending";
                    lstTaskDetails = lstTaskDetails.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                }
                else
                {
                    ViewState["Direction"] = "Descending";
                    lstTaskDetails = lstTaskDetails.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                }

                ViewState["SortExpression"] = e.SortExpression;

                foreach (DataControlField field in grdTaskActivity.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdTaskActivity.Columns.IndexOf(field);
                    }
                }
                Session["TotalRows"] = null;
                if (lstTaskDetails.Count > 0)
                {
                    flag = true;
                    grdTaskActivity.DataSource = lstTaskDetails;
                    grdTaskActivity.DataBind();
                    Session["TotalRows"] = lstTaskDetails.Count;
                }
                else
                {
                    grdTaskActivity.DataSource = lstTaskDetails;
                    grdTaskActivity.DataBind();
                    Session["TotalRows"] = null;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdTaskActivity_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection) ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }
        protected void lnkRev_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/ContractProduct/aspxPages/ContractTemplateList.aspx", false);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
            {
                System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
                sortImage.ImageAlign = ImageAlign.AbsMiddle;

                if (flag == true)
                {
                    if (direction == SortDirection.Ascending)
                    {
                        sortImage.ImageUrl = "../../Images/down_arrow1.png";
                        sortImage.AlternateText = "Ascending Order";
                    }
                    else
                    {
                        sortImage.ImageUrl = "../../Images/up_arrow1.png";
                        sortImage.AlternateText = "Descending Order";
                    }
                    headerRow.Cells[columnIndex].Controls.Add(sortImage);
                }
            }
        }
        #region add Filter
        private bool CheckInt(string val)
        {
            try
            {
                int i = Convert.ToInt32(val);
                return true;
            }
            catch
            {
                return false;
            }
        }
        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            BindGrid();
            bindPageNumber();
            ShowGridDetail();
        }
        #endregion

    }
}