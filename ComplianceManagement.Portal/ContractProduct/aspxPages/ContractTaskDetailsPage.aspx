﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ContractTaskDetailsPage.aspx.cs" ViewStateEncryptionMode="Always" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.aspxPages.ContractTaskDetailsPage" %>

<!DOCTYPE html>
<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="asp" %>
<%@ Register Assembly="DropDownListChosen" Namespace="DropDownListChosen" TagPrefix="cc1" %>

<html xmlns="http://www.w3.org/1999/xhtml" style="background: #f7f7f7;">
<head runat="server">
    <title>My Task Detail</title>

    <!-- Bootstrap CSS -->
    <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />

    <!-- bootstrap theme -->
    <link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />

    <!-- font icon -->
    <link href="~/NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <!--external css-->
    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/contract_custom_style.css" rel="stylesheet" />
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <%--<link href="~/NewCSS/jquery-ui.css" rel="stylesheet" type="text/css" />--%>

    <%--<script src="https://code.jquery.com/jquery-1.11.3.js"></script>--%>
    <script type="text/javascript" src="/Newjs/jquery.js"></script>
    <script type="text/javascript" src="/Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="/Newjs/bootstrap.min.js"></script>
    <%--<script type="text/javascript" src="/Newjs/jquery-1.8.3.min.js"></script>--%>

    <link href="~/NewCSS/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
    <script src="/Newjs/bootstrap-multiselect.js" type="text/javascript"></script>

      <link href="https://avacdn.azureedge.net/newcss/kendo.common1.2.min.css" rel="stylesheet" />
    <link href="https://avacdn.azureedge.net/newcss/kendo.rtl.min.css" rel="stylesheet" />
    <link href="https://avacdn.azureedge.net/newcss/kendo.silver.min.css" rel="stylesheet" />
    <link href="https://avacdn.azureedge.net/newcss/kendo.mobile.all.min.css" rel="stylesheet" />
      
    <script type="text/javascript" src="https://avacdn.azureedge.net/newjs/jszip.min.js"></script>
    <script type="text/javascript" src="https://avacdn.azureedge.net/newjs/kendo.all.min.js"></script>

     <script src="https://avacdn.azureedge.net/newjs/jquery.mentionable.js"></script>
    <link href="https://avacdn.azureedge.net/newcss/jquery.mentionable.css" rel="stylesheet" />

    <script type="text/javascript">
        $(document).ready(function () {

            $('#TxtComments').mentionable(
               '<% =KendoPath%>/Data/GetUSerDetailbySearch?customerID=<% =CustId%>', { parameterName: "search" }
            );

            GetCommentDetails();

            BindControls();

            applyCSSDate();

            if ($("#<%=tbxBranch.ClientID %>") != null) {
                $("#<%=tbxBranch.ClientID %>").unbind('click');

                $("#<%=tbxBranch.ClientID %>").click(function () {
                    $("#divBranches").toggle("blind", null, 500, function () { });
                });
            }

            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            });

            $(document).tooltip({ selector: '[data-toggle="tooltip"]' });

            hideDivBranch();
        });

        var InvalidFilesTypes = ["exe", "bat", "dll", "css", "js", "jsp",
            "php5", "pht", "asa", "cer", "asax", "swf", "xap", "aspx", "asp", "php", "reg", "rdp"];

        function CheckValidation() {
            
            if ('<% =UploadFile%>' == 'False') 
            {
                var fuSampleFile = $("#<%=fuTaskResponseDocUpload.ClientID%>").get(0).files;
                var isValidFile = true;
                $("#Labelmsg").css('display', 'none');
                if (fuSampleFile.length > 0) {
                    for (var i = 0; i < fuSampleFile.length; i++) {
                        var fileExtension = fuSampleFile[i].name.split('.').pop();
                        if (InvalidFilesTypes.indexOf(fileExtension) != -1) {
                            isValidFile = false;
                            break;
                        }
                    }
                    if (!isValidFile) {
                        $("#Labelmsg").css('display', 'block');
                        $('#Labelmsg').text("Invalid file error. System does not support uploaded file.Please upload another file..");
                    }
                    return isValidFile;

                }
                else
                {
                    $("#Labelmsg").css('display', 'block');
                    $('#Labelmsg').text("No File is uploaded.Please Select files to upload.");
                }
            }
            else
            {
                var isValidFile = true;
                return isValidFile;
            }
        }


        function InitializeRequest(sender, args) { }

        function EndRequest(sender, args) { BindControls(); }

        function BindControls() {
            var startDate = new Date();
            $(function () {
                $('input[id*=txtProposalDate]').datepicker({
                    dateFormat: 'dd-mm-yy',
                    maxDate: startDate,
                    numberOfMonths: 1,
                    changeMonth: true,
                    changeYear: true,
                });

                $('input[id*=txtAgreementDate]').datepicker({
                    dateFormat: 'dd-mm-yy',
                    maxDate: startDate,
                    numberOfMonths: 1,
                    changeMonth: true,
                    changeYear: true,
                });

                $('input[id*=txtEffectiveDate]').datepicker({
                    dateFormat: 'dd-mm-yy',
                    numberOfMonths: 1,
                    changeMonth: true,
                    changeYear: true,
                });

                $('input[id*=txtReviewDate]').datepicker({
                    dateFormat: 'dd-mm-yy',
                    numberOfMonths: 1,
                    changeMonth: true,
                    changeYear: true,
                });

                $('input[id*=txtExpirationDate]').datepicker({
                    dateFormat: 'dd-mm-yy',
                    minDate: startDate,
                    numberOfMonths: 1,
                    changeMonth: true,
                    changeYear: true,
                });

                $('input[id*=tbxTaskDueDate]').datepicker({
                    dateFormat: 'dd-mm-yy',
                    minDate: startDate,
                    numberOfMonths: 1,
                    changeMonth: true,
                    changeYear: true,
                });
            });

            $(function () {
                $('[id*=lstBoxVendor]').multiselect({
                    includeSelectAllOption: true,
                    numberDisplayed: 2,
                    buttonWidth: '100%',
                    enableCaseInsensitiveFiltering: true,
                    filterPlaceholder: 'Type to Search for Vendor..',
                    nSelectedText: ' - Vendor(s) selected',
                });
                $('[id*=ddlPaymentTerm1]').multiselect({
                    includeSelectAllOption: true,
                    numberDisplayed: 2,
                    buttonWidth: '100%',
                    enableCaseInsensitiveFiltering: true,
                    filterPlaceholder: 'Type to Search for Payment Term..',
                    nSelectedText: ' - Payment Term(s) selected',
                    enableFiltering: true,
                });
                $('[id*=lstBoxOwner]').multiselect({
                    includeSelectAllOption: true,
                    numberDisplayed: 2,
                    buttonWidth: '100%',
                    enableCaseInsensitiveFiltering: true,
                    filterPlaceholder: 'Type to Search for User..',
                    nSelectedText: ' - Owner(s) selected',
                });

                $('[id*=lstBoxApprover]').multiselect({
                    includeSelectAllOption: true,
                    numberDisplayed: 2,
                    buttonWidth: '100%',
                    enableCaseInsensitiveFiltering: true,
                    filterPlaceholder: 'Type to Search for User..',
                    nSelectedText: ' - Approver(s) selected',
                });
            });
        }

        function hideDivBranch() {
            $('#divBranches').hide("blind", null, 500, function () { });
        }

        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        });

        $('.btn-minimize').click(function () {
            var s1 = $(this).find('i');
            if ($(this).hasClass('collapsed')) {
                $(s1).removeClass('fa-chevron-down');
                $(s1).addClass('fa-chevron-up');
            } else {
                $(s1).removeClass('fa-chevron-up');
                $(s1).addClass('fa-chevron-down');
            }
        });

        function btnminimize(obj) {
            var s1 = $(obj).find('i');
            if ($(obj).hasClass('collapsed')) {

                $(s1).removeClass('fa-chevron-up');
                $(s1).addClass('fa-chevron-down');
            } else {
                $(s1).removeClass('fa-chevron-down');
                $(s1).addClass('fa-chevron-up');
            }
        }

        function OpenDocviewer(file) {
            $('#DocumentViewerPopup').modal('show');
            $('#iframeDocViewer').attr('src', "/docviewer.aspx?docurl=" + file);
        }

        function openNoticeModal() {
            $('#divAddNoticeModal').modal('show');
        }

        function reloadParentTaskListPage() {
            window.parent.reloadTaskList();
        }

        function applyCSSDate() {
            $('#<%= txtProposalDate.ClientID %>').removeClass();
            $('#<%= txtProposalDate.ClientID %>').addClass('form-control');

            $('#<%= txtAgreementDate.ClientID %>').removeClass();
            $('#<%= txtAgreementDate.ClientID %>').addClass('form-control');

            $('#<%= txtEffectiveDate.ClientID %>').removeClass();
            $('#<%= txtEffectiveDate.ClientID %>').addClass('form-control');

            $('#<%= txtReviewDate.ClientID %>').removeClass();
            $('#<%= txtReviewDate.ClientID %>').addClass('form-control');

            $('#<%= txtExpirationDate.ClientID %>').removeClass();
            $('#<%= txtExpirationDate.ClientID %>').addClass('form-control');

            $('#<%=txtNoticeTerm.ClientID %>').removeClass();
            $('#<%= txtNoticeTerm.ClientID %>').addClass('form-control');
        }

        function submitData() {
            debugger;
            var SelectedValue = new Array();
            var inps = document.getElementsByName('mentioned_id[]');
            for (var i = 0; i < inps.length; i++) {
                if (inps[i].disabled != true) {
                    var inp = inps[i];
                    SelectedValue.push(inp.value);
                }
            }

            var commet = document.getElementById("TxtComments").value;
            var ContractId = $('#hdnContractId').val();
            var TaskId = $('#hdntaskId').val();
            $.ajax({
                type: "GET",

                url: '<% =KendoPath%>/Data/PostCommentdataForContract?Comments=' + commet + '&UserId=<% =UId%>&CustId=<% =CustId%>&ContractID=' + ContractId + '&TaskID=' + TaskId,
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                crossDomain: true,
                success: function (result) {
                    GetCommentDetails();
                    $('#TxtComments').val('');
                    setTimeout(function () {
                        GetCommentDetails() // this will run after every 5 seconds
                    }, 1000);
                },
                error: function (response) {

                }
            });

        }

        function GetCommentDetails() {
            debugger;
            $.ajax({
                type: "GET",
                url: '<% =KendoPath%>/Data/GetCommentdataForContract?TaskID=' + $(hdntaskId).val() + '&searchKey=' + $(hdnContractId).val(),
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                crossDomain: true,
                success: function (result) {
                    var MainCommnetId = new Array();
                    for (var i = 0; i < result.length; i++) {
                        if (result[i].message_id == 0) {
                            MainCommnetId.push([result[i].ContractId, result[i].comment, result[i].user_id, result[i].created_at, result[i]._id, result[i].user_name]);
                        }
                    }

                    var customers = new Array();
                    for (var i = 0; i < MainCommnetId.length; i++) {
                        customers.push('<div style="font-family: Roboto;color:black;float: left;padding: 4px 10px 4px 5px;border: 1px solid #e4d8d8;width: auto;min-width: 234px;border-radius: 6px;">' + MainCommnetId[i][1] +
                            '</div></br > <div style="float: left;font-size: 10px;color:black;">' + MainCommnetId[i][5] + ',' + new Date(new Date(MainCommnetId[i][3])).toLocaleString()
                            + '<a style="font-size: 12px;margin-left: 8px;color:blue;cursor: pointer;" onclick=showDiv("' + MainCommnetId[i][4] + '");>Comments</a></div></br><div style="display:none;" id=' + MainCommnetId[i][4] + '_div><input type="text" style="border-radius: 12px;margin-right: 20px;" name=txtid id=' + MainCommnetId[i][4] + '_txt></br>&nbsp;&nbsp;&nbsp;<a style="cursor: pointer;color: blue;" onclick=submitDiv("' + MainCommnetId[i][4] + '");>submit</a>&nbsp;&nbsp;&nbsp;<a style="cursor: pointer;color: blue;" onclick=CloseDiv("' + MainCommnetId[i][4] + '");>Closed</a></div></br>');

                        for (var k = 0; k < result.length; k++) {
                            if (result[k].message_id == MainCommnetId[i][4]) {
                                customers.push('<div style="font-family: Roboto;background: #eaeaea;color:black;float: right;padding: 4px 10px 4px 5px;border: 1px solid #e4d8d8;width: auto;min-width: 234px;border-radius: 6px;">' + result[k].comment + '</div></br></br><div style="float: right;font-size: 10px;color:black;">' + result[k].user_name + ',' + new Date(new Date(result[k].created_at)).toLocaleString() + '</div></br>');
                            }
                        }
                    }
                    $("#ShowDatadiv").load();
                    $('#ShowDatadiv').html('');
                    $('#ShowDatadiv').html('<div style="min-height:45px;padding-left: 10px;padding-top: 10px;">' + customers.join('</div><div style="padding-left: 10px;padding-bottom: 18px;">') + '</div>');

                },
                error: function (response) {

                }
            });

        }

        function submitDiv(pageid) {
            var valuetext = document.getElementById(pageid + "_txt").value;

            if (valuetext != "") {


                var ContractId = $('#hdnContractId').val();
                var TaskId = $('#hdntaskId').val();

                $.ajax({
                    type: "GET",
                    url: '<% =KendoPath%>/Data/PostSubCommentdataForContract?MsgID=' + pageid + '&Comments=' + valuetext + '&UserId=<% =UId%>&CustId=<% =CustId%>&ContractID=' + ContractId + '&TaskID=' + TaskId,
                    contentType: "application/json;charset=utf-8",
                    dataType: "json",
                    crossDomain: true,
                    success: function (result) {
                        setTimeout(function () {
                            GetCommentDetails() // this will run after every 5 seconds
                        }, 1000);
                    },
                    error: function (response) {

                    }
                });
              }
              else {
                  //alert("Please Write something in comment box"); 
              }

          }


        function showDiv(pageid) {

            document.getElementById(pageid + "_div").style.display = "block";
            document.getElementById(pageid + "_txt").value = "";
        }

        function CloseDiv(pageid) {

            document.getElementById(pageid + "_div").style.display = "none";
        }

               
    </script>


    <style type="text/css">
        h4 {
            height: 100%;
            font-weight: 400;
            display: inline-block;
        }

        .clsPageNo {
            margin-top: 5%;
            margin-left: 60%;
            color: #666666;
        }

        .panel .panel-heading h2 {
             height: 100%;
             width: auto;
             display: inline-block;
             font-size: 18px;
             position: relative;
             margin: 0;
             line-height: 34px;
             font-weight: 400;
             letter-spacing: 0;
          }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" ScriptMode ="Release"></asp:ScriptManager>

        <asp:Panel ID="panelOTP" runat="server" DefaultButton="btnOTP">
            <div class="login-form">

                <asp:UpdatePanel ID="upVerifyOTP" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="col-md-12 login-form-head">
                            <p class="login-img">
                                <img src="/Images/avantil-logo.png" />
                            </p>
                        </div>

                        <div class="login-wrap" id="divOTP" runat="server" visible="false">
                            <span style="font-family: 'Roboto',san-serif; color: #555555; font-size: 13.5px;">One Time Password has been sent to
                                <br />
                                Your registered email -
                                <asp:Label ID="email" Font-Bold="true" runat="server"></asp:Label>
                                <br />
                                Your registered Phone No -
                                <asp:Label ID="mobileno" Font-Bold="true" runat="server"></asp:Label>
                                <br />
                                (Please note that the OTP is valid till
                                <asp:Label ID="Time" runat="server"></asp:Label>
                                IST)</span>

                            <div class="clearfix" style="height: 10px"></div>
                            <div class="clearfix" style="height: 10px"></div>

                            <h1 style="font-size: 15px;">Please Enter One Time Password (OTP)</h1>

                            <div class="input-group">
                                <span class="input-group-addon"></span>
                                <asp:TextBox ID="txtOTP" CssClass="form-control" runat="server" placeholder="Enter OTP" />
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtOTP" runat="server" Display="None"
                                    ErrorMessage="Only Numbers allowed in OTP" ValidationExpression="\d+" ValidationGroup="OTPValidationGroup"></asp:RegularExpressionValidator>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="None" ErrorMessage="Required OTP."
                                    ControlToValidate="txtOTP" runat="server" ValidationGroup="OTPValidationGroup" />
                            </div>

                            <asp:Label ID="lblmsgotp" runat="server" ForeColor="Red" CssClass="pull-left" Text=""></asp:Label>
                            <asp:Button ID="btnOTP" CssClass="btn btn-primary btn-lg btn-block" Text="Verify OTP"
                                OnClick="btnOTP_Click" runat="server" ValidationGroup="OTPValidationGroup"></asp:Button>

                            <div class="clearfix" style="height: 10px"></div>
                        </div>

                        <div class="col-md-12 login-form-head">
                            <asp:CustomValidator ID="cvLogin" class="alert alert-block alert-danger fade in" EnableClientScript="False" runat="server" Display="None" ValidationGroup="OTPValidationGroup" />
                            <asp:ValidationSummary ID="vsLogin" class="alert alert-block alert-danger fade in" runat="server" ValidationGroup="OTPValidationGroup" />
                        </div>

                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnOTP" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </asp:Panel>

        <asp:Panel ID="panelTaskDetail" runat="server" Visible="false">
            <div id="avatisLogo" runat="server" class="col-md-12" style="text-align: center;">
                <p class="login-img">
                    <img src="/Images/avantil-logo.png" />
                </p>
            </div>

            <div class="container" style="background-color: #f7f7f7;">
                <asp:UpdateProgress ID="updateProgress" runat="server">
                    <ProgressTemplate>
                        <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.3;">
                            <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                                AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 40%; left: 40%;" />
                        </div>
                    </ProgressTemplate>
                </asp:UpdateProgress>

                <div class="row Dashboard-white-widget">
                    <!--ContractDetail Panel Start-->
                    <div class="col-lg-12 col-md-12">
                        <div class="panel panel-default">

                            <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;" data-toggle="tooltip" data-placement="bottom" title="Click to View Contract Detail(s)">
                                <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseDivContractSummary">
                                    <a>
                                        <h4>Contract Detail(s)</h4>
                                    </a>
                                    <div class="panel-actions">
                                        <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseDivContractSummary">
                                            <i class="fa fa-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div id="collapseDivContractSummary" class="panel-collapse collapse">
                                <div class="row">
                                    <asp:ValidationSummary ID="VSContractPopup" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                        ValidationGroup="ContractPopUpValidationGroup" />
                                    <asp:CustomValidator ID="cvContractPopUp" runat="server" EnableClientScript="False"
                                        ValidationGroup="ContractPopUpValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                                </div>

                                <div class="panel-body">
                                    <asp:Panel ID="pnlContract" runat="server">
                                        <div class="container plr0">
                                            <div class="row">
                                                <div class="form-group required col-md-2 pl0">
                                                    <label for="ddlContractStatus" class="control-label">Contract Status</label>
                                                    <asp:DropDownListChosen runat="server" ID="ddlContractStatus" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                        class="form-control" Width="100%" />
                                                </div>

                                                <div class="form-group required col-md-4 pl0">
                                                    <label for="txtContractNo" class="control-label">Contract Number</label>
                                                    <asp:TextBox runat="server" ID="txtContractNo" CssClass="form-control" autocomplete="off" />
                                                </div>

                                                <div class="form-group required col-md-6 pl0">
                                                    <label for="txtTitle" class="control-label">Contract Title</label>
                                                    <asp:TextBox runat="server" ID="txtTitle" CssClass="form-control" autocomplete="off" />
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="form-group required col-md-12 pl0">
                                                    <label for="tbxDescription" class="control-label">Description</label>
                                                    <asp:TextBox runat="server" ID="tbxDescription" TextMode="MultiLine" CssClass="form-control" />
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="form-group required col-md-4 pl0">
                                                    <label for="tbxBranch" class="control-label">Entity/Location</label>
                                                    <asp:TextBox runat="server" ID="tbxBranch" CssClass="form-control" ReadOnly="true" autocomplete="off" AutoCompleteType="None" CausesValidation="true"
                                                        Style="cursor: pointer;" />

                                                    <div style="position: absolute; z-index: 10; display: none" id="divBranches">
                                                        <asp:TreeView runat="server" ID="tvBranches" BackColor="White" BorderColor="Black"
                                                            BorderWidth="1" SelectedNodeStyle-Font-Bold="true" Height="150px"
                                                            Style="overflow: auto; margin-top: -20px; border: 1px solid #c7c7cc; background-color: #ffffff; color: #8e8e93 !important;"
                                                            ShowLines="true">
                                                        </asp:TreeView>
                                                    </div>
                                                </div>

                                                <div class="form-group required col-md-4 pl0">
                                                    <label for="lstBoxVendor" class="control-label">Vendor</label>
                                                    <asp:ListBox ID="lstBoxVendor" CssClass="form-control" runat="server" SelectionMode="Multiple" Width="100%"></asp:ListBox>
                                                </div>

                                                <div class="form-group required col-md-4 pl0">
                                                    <label for="ddlDepartment" class="control-label">Department</label>
                                                    <asp:DropDownListChosen runat="server" ID="ddlDepartment" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                        DataPlaceHolder="Select Department" class="form-control" Width="100%" />
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="form-group col-md-4 pl0">
                                                    <label for="txtProposalDate" class="control-label">Proposal Date</label>
                                                    <div class="input-group date">
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-calendar color-black"></span>
                                                        </span>
                                                        <asp:TextBox ID="txtProposalDate" runat="server" placeholder="DD-MM-YYYY" class="form-control" />
                                                    </div>
                                                </div>

                                                <div class="form-group col-md-4 pl0" data-provide="datepicker">
                                                    <label for="txtAgreementDate" class="control-label">Agreement Date</label>
                                                    <div class="input-group date">
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-calendar color-black"></span>
                                                        </span>
                                                        <asp:TextBox ID="txtAgreementDate" runat="server" placeholder="DD-MM-YYYY" class="form-control" />
                                                    </div>
                                                </div>

                                                <div class="form-group required col-md-4 pl0">
                                                    <label for="txtEffectiveDate" class="control-label">Effective Date</label>
                                                    <div class="input-group date">
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-calendar color-black"></span>
                                                        </span>
                                                        <asp:TextBox ID="txtEffectiveDate" runat="server" placeholder="DD-MM-YYYY" class="form-control" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="form-group col-md-4 pl0">
                                                    <label for="txtAgreementDate" class="control-label">Review Date</label>
                                                    <div class="input-group date">
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-calendar color-black"></span>
                                                        </span>
                                                        <asp:TextBox ID="txtReviewDate" runat="server" placeholder="DD-MM-YYYY" class="form-control" />
                                                    </div>
                                                </div>

                                                <div class="form-group col-md-4 pl0 d-none">
                                                    <label for="rbContractEndType" class="control-label">&nbsp;</label>
                                                    <asp:RadioButtonList ID="rbContractEndType" runat="server" RepeatDirection="Horizontal">
                                                        <asp:ListItem class="radio-inline" Text="Expiration Date" Value="E" Selected="True"></asp:ListItem>
                                                        <asp:ListItem class="radio-inline" Text="Duration " Value="D"></asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </div>

                                                <div class="form-group col-md-4 pl0" id="divExpirationDate">
                                                    <label for="txtExpirationDate" class="control-label">Expiration Date</label>
                                                    <div class="input-group date">
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-calendar color-black"></span>
                                                        </span>
                                                        <asp:TextBox ID="txtExpirationDate" runat="server" placeholder="DD-MM-YYYY" class="form-control" />
                                                    </div>
                                                </div>

                                                <div class="form-group col-md-4 pl0 d-none" id="divDuration">
                                                    <label for="txtDuration">Duration</label>
                                                    <div class="input-group">
                                                        <asp:TextBox ID="txtDuration" runat="server" placeholder="" class="form-control col-md-8" />
                                                        <div class="input-group-btn">
                                                            <asp:DropDownList ID="ddlDuration" runat="server" CssClass="form-control col-md-4">
                                                                <asp:ListItem Text="Days" Value="D" Selected="True"></asp:ListItem>
                                                                <asp:ListItem Text="Weeks" Value="W"></asp:ListItem>
                                                                <asp:ListItem Text="Months" Value="M"></asp:ListItem>
                                                                <asp:ListItem Text="Years" Value="Y"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group col-md-4 pl0">
                                                    <label for="txtNoticeTerm" class="control-label">Notice Term</label>
                                                    <div class="input-group">
                                                        <asp:TextBox ID="txtNoticeTerm" runat="server" class="form-control col-md-8" />
                                                        <div class="input-group-btn">
                                                            <asp:DropDownList ID="ddlNoticeTerm" runat="server" CssClass="form-control col-md-4"
                                                                Style="border-bottom-right-radius: 4px; border-top-right-radius: 4px; border-left: none;">
                                                                <asp:ListItem Text="Days" Value="1" Selected="True"></asp:ListItem>
                                                                <asp:ListItem Text="Weeks" Value="2"></asp:ListItem>
                                                                <asp:ListItem Text="Months" Value="3"></asp:ListItem>
                                                                <asp:ListItem Text="Years" Value="4"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <asp:UpdatePanel ID="upContractTypeSubType" runat="server" UpdateMode="Conditional">
                                                    <ContentTemplate>

                                                        <div class="form-group required col-md-4 pl0">
                                                            <label for="ddlContractType" class="control-label">Contract Type</label>
                                                            <asp:DropDownListChosen runat="server" AutoPostBack="true" ID="ddlContractType" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                class="form-control" Width="100%" DataPlaceHolder="Select Contract Type"
                                                                OnSelectedIndexChanged="ddlContractType_SelectedIndexChanged">
                                                            </asp:DropDownListChosen>
                                                        </div>

                                                        <div class="form-group col-md-4 pl0">
                                                            <label for="ddlContractSubType" class="control-label">Contract Sub-Type</label>
                                                            <asp:DropDownListChosen runat="server" ID="ddlContractSubType" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                class="form-control" Width="100%" DataPlaceHolder="Select Contract Type">
                                                            </asp:DropDownListChosen>
                                                        </div>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>

                                                <div class="form-group required col-md-4 pl0">
                                                    <label for="lstBoxOwner" class="control-label">Contract Owner</label>
                                                    <asp:ListBox ID="lstBoxOwner" CssClass="form-control" runat="server" SelectionMode="Multiple"></asp:ListBox>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="form-group col-md-4 pl0">
                                                    <label for="tbxContractAmt" class="control-label">Contract Amount</label>
                                                    <asp:TextBox runat="server" ID="tbxContractAmt" CssClass="form-control" autocomplete="off" />
                                                </div>

                                                <div class="form-group col-md-4 pl0">
                                                    <label for="ddlPaymentTerm1" class="control-label">Payment Term</label>
                                                      <asp:ListBox ID="ddlPaymentTerm1" CssClass="form-control" runat="server" SelectionMode="Multiple" Width="100%">
                                                                </asp:ListBox>
                                                <%--    <asp:DropDownListChosen runat="server" ID="ddlPaymentTerm" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                        class="form-control" Width="100%" DataPlaceHolder="Select Court">
                                                        <asp:ListItem Text="One-Time" Value="0" Selected="True"></asp:ListItem>
                                                        <asp:ListItem Text="Daily" Value="1"></asp:ListItem>
                                                        <asp:ListItem Text="Weekly" Value="7"></asp:ListItem>
                                                        <asp:ListItem Text="Periodically" Value="15"></asp:ListItem>
                                                        <asp:ListItem Text="Monthly" Value="30"></asp:ListItem>
                                                        <asp:ListItem Text="Quarterly" Value="4"></asp:ListItem>
                                                        <asp:ListItem Text="Half-Yearly" Value="6"></asp:ListItem>
                                                        <asp:ListItem Text="Yearly" Value="12"></asp:ListItem>
                                                    </asp:DropDownListChosen>--%>
                                                </div>

                                                <div class="form-group col-md-4 pl0">
                                                    <label for="txtBoxProduct" class="control-label">Product/Items(s)</label>
                                                    <asp:TextBox ID="txtBoxProduct" CssClass="form-control" runat="server"></asp:TextBox>                                                   
                                                </div>

                                                <div class="form-group required col-md-4 d-none">
                                                    <label for="lstBoxApprover" class="control-label">Contract Approver(s)</label>
                                                    <asp:ListBox ID="lstBoxApprover" CssClass="form-control" runat="server" SelectionMode="Multiple"></asp:ListBox>
                                                   <%-- <asp:RequiredFieldValidator ID="rfvApprover" ErrorMessage="Please Select Approver"
                                                        ControlToValidate="lstBoxApprover" runat="server" ValidationGroup="ContractPopUpValidationGroup" Display="None" />--%>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="form-group col-md-12 pl0">
                                                    <asp:UpdatePanel ID="upCustomField" runat="server">
                                                        <ContentTemplate>
                                                            <asp:GridView runat="server" ID="grdCustomField" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                                GridLines="None" AllowPaging="true" AutoPostBack="true" CssClass="table" ShowFooter="true" ShowHeader="true"
                                                                PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Right" OnRowDataBound="grdCustomField_Common_RowDataBound">
                                                                <Columns>
                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%" Visible="false">
                                                                        <ItemTemplate>
                                                                            <%#Container.DataItemIndex+1 %>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField Visible="false">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblID" runat="server" Text='<%# Eval("LableID") %>'
                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("LableID") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField ItemStyle-Width="25%" FooterStyle-Width="25%"><%--HeaderText="Field"--%>
                                                                        <ItemTemplate>
                                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; margin-top: 5px;">
                                                                                <asp:Label ID="lblName" runat="server" Text='<%# Eval("Label") %>'
                                                                                    data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Label") %>'></asp:Label>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                        <FooterTemplate>
                                                                            <asp:DropDownListChosen runat="server" ID="ddlFieldName_Footer" CssClass="unique-footer-select" DataPlaceHolder="Select"
                                                                                AllowSingleDeselect="false" DisableSearchThreshold="5" Width="100%">
                                                                            </asp:DropDownListChosen>
                                                                        </FooterTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField ItemStyle-Width="1%" FooterStyle-Width="1%">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblImgAddNew" runat="server"></asp:Label>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                        <FooterTemplate>
                                                                            <img id="imgAddNewCustomField" src='<%# ResolveUrl("~/Images/add_icon_new.png")%>'
                                                                                onclick="OpenAddNewCustomFieldPopUp()" alt="Add" data-toggle="tooltip" data-placement="bottom" title="Click to Add New Custom Field" />
                                                                        </FooterTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField ItemStyle-Width="25%" FooterStyle-Width="25%" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"><%--HeaderText="Value"--%>
                                                                        <ItemTemplate>
                                                                            <asp:TextBox ID="tbxLabelValue" runat="server" CssClass="form-control" PlaceHolder="Value" Text='<%# Eval("labelValue") %>'></asp:TextBox>
                                                                        </ItemTemplate>
                                                                        <FooterTemplate>
                                                                            <asp:TextBox runat="server" AutoPostBack="true" ID="txtFieldValue_Footer" PlaceHolder="Value" CssClass="form-control" />
                                                                        </FooterTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                                <%--<RowStyle CssClass="clsROWgrid" />--%>
                                                                <HeaderStyle CssClass="clsheadergrid" />
                                                                <EmptyDataTemplate>
                                                                    No Records Found
                                                                </EmptyDataTemplate>
                                                            </asp:GridView>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--Contract Detail Panel End-->
                </div>

                <div class="row Dashboard-white-widget d-none">
                    <div class="col-lg-12 col-md-12">
                        <div class="panel panel-default">
                            <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;" data-toggle="tooltip" data-placement="bottom" title="Click to View Case Document(s)">
                                <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseDivCaseDocument">
                                    <a>
                                        <h4>Contract Document(s)</h4>
                                    </a>
                                    <div class="panel-actions">
                                        <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseDivCaseDocument">
                                            <i class="fa fa-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div id="collapseDivCaseDocument" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class=" row col-md-12 plr0">
                                        <asp:ValidationSummary ID="vsCaseDocument" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                            ValidationGroup="ContractDocumentPopUpValidationGroup" />
                                        <asp:CustomValidator ID="cvContractDocument" runat="server" EnableClientScript="False"
                                            ValidationGroup="ContractDocumentPopUpValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                                    </div>

                                    <div class="row col-md-12 plr0">
                                        <asp:UpdatePanel ID="upContractDocUploadPopup" runat="server">
                                            <ContentTemplate>
                                                <asp:GridView runat="server" ID="grdContractDocuments" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                    GridLines="None" PageSize="8" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%" ShowFooter="false"
                                                    PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Right" OnRowCommand="grdTaskContractDocuments_RowCommand">
                                                    <Columns>
                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%">
                                                            <ItemTemplate>
                                                                <%--<%#Container.DataItemIndex+1 %>--%>
                                                                <asp:Label ID="lblRowID" runat="server" Text='<%# Eval("RowID") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Type" ItemStyle-Width="10%">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDocType" runat="server" Text='<%# Eval("DocTypeName") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Document" ItemStyle-Width="40%">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 400px">
                                                                    <asp:Label ID="lblFileName" runat="server" Text='<%# Eval("FileName") %>'
                                                                        data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("FileName") %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Version" ItemStyle-Width="5%" Visible="false">
                                                            <ItemTemplate>

                                                                <asp:Label ID="lblDocVersion" runat="server" Text='<%# Eval("Version") %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Uploaded By" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="15%">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                                    <asp:Label ID="lblUploadedBy" runat="server" Text='<%# Eval("UploadedByName") %>'
                                                                        data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("UploadedByName") %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Uploaded On" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="15%">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                    <asp:Label ID="lblUploadedOn" runat="server" Text='<%# Eval("CreatedOn") != DBNull.Value ? Convert.ToDateTime(Eval("CreatedOn")).ToString("dd-MM-yyyy") : "" %>'
                                                                        data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("CreatedOn") %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Action" ItemStyle-Width="10%" HeaderStyle-CssClass="text-right" ItemStyle-CssClass="text-right">
                                                            <ItemTemplate>
                                                                <asp:UpdatePanel runat="server" ID="aa1naa" UpdateMode="Always">
                                                                    <ContentTemplate>
                                                                        <asp:LinkButton CommandArgument='<%# Eval("FileID")%>' CommandName="DownloadContDoc"
                                                                            ID="lnkBtnDownLoadCaseDoc" runat="server" data-toggle="tooltip" data-placement="bottom" title="Click to Download Document">
                                                                        <img src='<%# ResolveUrl("~/Images/download_icon_new.png")%>' alt="Download" title="Download" /> 
                                                                        </asp:LinkButton>

                                                                        <asp:LinkButton CommandArgument='<%# Eval("FileID") %>' AutoPostBack="true" CommandName="ViewContOrder"
                                                                            ID="lnkBtnViewDocCase" runat="server" data-toggle="tooltip" data-placement="bottom" title="Click to View Document">
                                                                        <img src='<%# ResolveUrl("~/Images/Eye.png")%>' alt="View" title="View Document" />
                                                                        </asp:LinkButton>

                                                                    </ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:PostBackTrigger ControlID="lnkBtnDownLoadCaseDoc" />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <RowStyle CssClass="clsROWgrid" />
                                                    <HeaderStyle CssClass="clsheadergrid" />
                                                    <EmptyDataTemplate>
                                                        No Records Found
                                                    </EmptyDataTemplate>
                                                </asp:GridView>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>

                                    <div class="row col-md-12 plr0">
                                        <div class="col-md-10 pl0">
                                            <div runat="server" id="DivRecordsScrum" style="float: left; margin-top: 5px; color: #999">
                                                <p style="padding-right: 0px !Important;">
                                                    <asp:Label ID="Label4" runat="server" Text="Showing "></asp:Label>
                                                    <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>-<asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                    of 
                                                <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="col-md-2 text-right pr0">
                                            <div class="col-md-6 plr0 text-right">
                                                <p class="clsPageNo">Page</p>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6 pr0">
                                                <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true" DataPlaceHolder="No" AllowSingleDeselect="false"
                                                    OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged" class="form-control m-bot15" Width="100%" Height="30px">
                                                </asp:DropDownListChosen>
                                            </div>
                                        </div>
                                        <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row Dashboard-white-widget">
                    <!--Task Detail Panel Start-->
                    <div class="col-lg-12 col-md-12">
                        <div class="panel panel-default">
                            <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;" data-toggle="tooltip" data-placement="bottom" title="Click to View Task Detail">
                                <div class="panel-heading" data-toggle="collapse" data-parent="#secondTabAccordion" href="#collapseDivTaskDetail">
                                    <a>
                                        <h4>Task Detail(s)</h4>
                                    </a>
                                    <div class="panel-actions">
                                        <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#secondTabAccordion" href="#collapseDivTaskDetail">
                                            <i class="fa fa-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div id="collapseDivTaskDetail" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="container plr0">
                                        <asp:Panel ID="pnlTaskDetail" runat="server">
                                            <div class="row form-group">
                                                <div class="col-md-2">
                                                    <label for="lblTaskTitle" class="control-label">Task Title</label>
                                                </div>
                                                <div class="col-md-10">
                                                    <asp:Label ID="lblTaskTitle" runat="server" CssClass="text-label"></asp:Label>
                                                </div>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col-md-2">
                                                    <label for="lblTaskDesc" class="control-label">Task Description</label>
                                                </div>
                                                <div class="col-md-10">
                                                    <asp:Label ID="lblTaskDesc" runat="server" CssClass="text-label"></asp:Label>
                                                </div>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col-md-2">
                                                    <label for="lblAssignBy" class="control-label">Assigned By</label>
                                                </div>
                                                <div class="col-md-4">
                                                    <asp:Label ID="lblAssignBy" runat="server" CssClass="text-label"></asp:Label>
                                                </div>

                                                <div class="col-md-2">
                                                    <label for="lblPriority" class="control-label">Priority</label>
                                                </div>
                                                <div class="col-md-4">
                                                    <asp:Label ID="lblPriority" runat="server" CssClass="text-label"></asp:Label>
                                                </div>                                                
                                            </div>

                                             <div class="row form-group">                                              
                                                 <div class="col-md-2">
                                                    <label for="lblTaskAssignOn" class="control-label">Task Assign On</label>
                                                </div>
                                                <div class="col-md-4">
                                                    <asp:Label ID="lblTaskAssignOn" runat="server" CssClass="text-label"></asp:Label>
                                                </div>

                                                <div class="col-md-2">
                                                    <label for="lblTaskDueDate" class="control-label">Task Due Date</label>
                                                </div>
                                                <div class="col-md-4">
                                                    <asp:Label ID="lblTaskDueDate" runat="server" CssClass="text-label"></asp:Label>
                                                </div>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col-md-2">
                                                    <label for="lblExpOutcome" class="control-label">Expected Outcome/ Specific Instruction(s)</label>
                                                </div>
                                                <div class="col-md-10">
                                                    <asp:Label ID="lblExpOutcome" runat="server" CssClass="text-label"></asp:Label>
                                                </div>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col-md-2">
                                                    <label for="lblTaskRemark" class="control-label">Comment(s)</label>
                                                </div>
                                                <div class="col-md-10">
                                                    <asp:Label ID="lblTaskRemark" runat="server" CssClass="text-label" autocomplete="off"></asp:Label>
                                                </div>
                                            </div>

                                            <div class="row col-md-12">
                                                <label for="fuTaskDocUpload" class="control-label">Shared Contract Document(s)</label>
                                            </div>

                                            <div class="row col-md-12" style="min-height: 50px; max-height: 300px; overflow-y: auto;">
                                                <asp:UpdatePanel ID="upTaskContractDocuments" runat="server">
                                                    <ContentTemplate>
                                                        <asp:GridView runat="server" ID="grdTaskContractDocuments" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                            GridLines="None" PageSize="8" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%" ShowFooter="false"
                                                            PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Right" OnRowCommand="grdTaskContractDocuments_RowCommand" >
                                                            <Columns>    
                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%">
                                                                    <ItemTemplate>
                                                                        <%#Container.DataItemIndex+1 %>
                                                                        <asp:Label ID="lblFileID" runat="server" Text='<%# Eval("FileID") %>' Visible="false"></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Type" ItemStyle-Width="15%">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblDocType" runat="server" Text='<%# Eval("DocTypeName") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Document" ItemStyle-Width="30%">
                                                                    <ItemTemplate>
                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 400px">
                                                                            <asp:Label ID="lblFileName" runat="server" Text='<%# Eval("FileName") %>'
                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("FileName") %>'></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Version" ItemStyle-Width="5%" Visible="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblDocVersion" runat="server" Text='<%# Eval("Version") %>'></asp:Label>                                                                       
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Uploaded By" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="15%">
                                                                    <ItemTemplate>
                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                                            <asp:Label ID="lblUploadedBy" runat="server" Text='<%# Eval("UploadedByName") %>'
                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("UploadedByName") %>'></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Uploaded On" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="15%">
                                                                    <ItemTemplate>
                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                            <asp:Label ID="lblUploadedOn" runat="server" Text='<%# Eval("CreatedOn") != DBNull.Value ? Convert.ToDateTime(Eval("CreatedOn")).ToString("dd-MM-yyyy") : "" %>'
                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("CreatedOn") %>'></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Action" ItemStyle-Width="15%" HeaderStyle-CssClass="text-right" ItemStyle-CssClass="text-right">
                                                                    <ItemTemplate>
                                                                        <asp:UpdatePanel runat="server" ID="aa1naa" UpdateMode="Always">
                                                                            <ContentTemplate>
                                                                                <asp:LinkButton CommandArgument='<%# Eval("FileID") %>' AutoPostBack="true" CommandName="ViewContOrder"
                                                                                    ID="lnkBtnViewTaskDoc" runat="server" data-toggle="tooltip" data-placement="left" title="Click to View Document">
                                                                                    <img src='<%# ResolveUrl("~/Images/Eye.png")%>' alt="View" />
                                                                                </asp:LinkButton>

                                                                                <asp:LinkButton CommandArgument='<%# Eval("FileID")%>' CommandName="DownloadContDoc" Visible='<%# CanDownloadDeleteDoc() %>'
                                                                                    ID="lnkBtnDownloadTaskDoc" runat="server" data-toggle="tooltip" data-placement="left" title="Click to Download Document">
                                                                                    <img src='<%# ResolveUrl("~/Images/download_icon_new.png")%>' alt="Download" /> 
                                                                                </asp:LinkButton>
                                                                            </ContentTemplate>
                                                                            <Triggers>
                                                                                <asp:PostBackTrigger ControlID="lnkBtnDownloadTaskDoc" />
                                                                            </Triggers>
                                                                        </asp:UpdatePanel>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <RowStyle CssClass="clsROWgrid" />
                                                            <HeaderStyle CssClass="clsheadergrid" />
                                                            <EmptyDataTemplate>
                                                                No Records Found
                                                            </EmptyDataTemplate>
                                                        </asp:GridView>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>

                                        </asp:Panel>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Task Detail Panel  End-->

                <!--Task-Response Panel Start-->
                <div class="row Dashboard-white-widget">
                    <div class="col-lg-12 col-md-12">
                        <div class="panel panel-default">

                            <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;" data-toggle="tooltip" data-placement="bottom" title="Click to View Response Detail">
                                <div class="panel-heading" data-toggle="collapse" data-parent="#secondTabAccordion" href="#collapseDivTaskResponse">
                                    <a>
                                        <h4>Submit Response</h4>
                                    </a>
                                    <div class="panel-actions">
                                        <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#secondTabAccordion" href="#collapseDivTaskResponse">
                                            <i class="fa fa-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div id="collapseDivTaskResponse" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <asp:UpdatePanel runat="server" ID="UpdatePanel1">
                                        <ContentTemplate>
                                            <div class="container">
                                                <div class="row">
                                                    <asp:ValidationSummary ID="vsTaskResponse" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                                        ValidationGroup="TaskResponseValidationGroup" />
                                                    <asp:CustomValidator ID="cvTaskResponse" runat="server" EnableClientScript="False"
                                                        ValidationGroup="TaskResponseValidationGroup" Display="None" />
                                                     <asp:Label ID="Labelmsg" class="alert alert-block alert-danger fade in" Style="display: none;" runat="server"></asp:Label>
                                                </div>

                                                <div class="row">
                                                     <div class="form-group required col-md-7" style="width:57%;">
                                                    <asp:UpdatePanel ID="upTaskResponseDocUpload" runat="server">
                                                        <ContentTemplate>
                                                            <asp:GridView runat="server" ID="grdTaskResponseLog" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                                GridLines="None" PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%"
                                                                PagerSettings-Position="Top" PagerStyle-HorizontalAlign="Right"
                                                                OnPageIndexChanging="grdTaskResponseLog_OnPageIndexChanging" OnRowCommand="grdTaskResponseLog_RowCommand">
                                                                <Columns>
                                                                    <asp:TemplateField HeaderText="Assign" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="13%" HeaderStyle-Width="13%">
                                                                        <ItemTemplate>
                                                                              <asp:Label ID="lblAssingn" runat="server" Text='<%# Eval("AssignToName") %>'
                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("AssignToName") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Status" HeaderStyle-Width="10%" ItemStyle-Width="10%">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblStatus" runat="server" Text='<%# Eval("StatusName") %>'
                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("StatusName") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                     <asp:TemplateField HeaderText="Created By" HeaderStyle-Width="14%" ItemStyle-Width="14%">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblCreatedByName" runat="server" Text='<%# Eval("CreatedByName") %>'
                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("CreatedByName") %>'>
                                                                            </asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="Date Created" HeaderStyle-Width="17%" ItemStyle-Width="17%">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblStatusChangedOn" runat="server" Text='<%# Eval("StatusChangeOn") != null ? Convert.ToDateTime(Eval("StatusChangeOn")).ToString("dd-MM-yyyy HH:mm:ss tt") : "" %>'
                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("StatusChangeOn") != null ? Convert.ToDateTime(Eval("StatusChangeOn")).ToString("dd-MM-yyyy HH:mm:ss tt") : "" %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <%--<asp:TemplateField HeaderText="Comment" ItemStyle-Width="30%" HeaderStyle-Width="30%" vis>
                                                                        <ItemTemplate>
                                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; ">
                                                                                <asp:Label ID="lblComment" runat="server" Text='<%# Eval("Comment") %>'
                                                                                    data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Comment") %>'></asp:Label>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>--%>

                                                                    <asp:TemplateField HeaderText="Documents" ItemStyle-Width="8%" HeaderStyle-Width="8%">
                                                                        <ItemTemplate>
                                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%;">
                                                                                <asp:Label ID="lblResDoc" runat="server" Text='<%# ShowTaskResponseDocCount((long)Eval("ContractID"), (long)Eval("TaskID"),(long)Eval("ID")) %>'>  <%--ID=TaskResponseID--%>
                                                                                </asp:Label>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="Action" ItemStyle-Width="20%" HeaderStyle-Width="20%"
                                                                        ItemStyle-CssClass="text-right" HeaderStyle-CssClass="text-right">
                                                                        <ItemTemplate>
                                                                            <asp:UpdatePanel runat="server" ID="upTaskResDocument" UpdateMode="Always">
                                                                                <ContentTemplate>
                                                                                    <asp:LinkButton
                                                                                        CommandArgument='<%# Eval("ID")+","+ Eval("TaskID")+","+ Eval("ContractID")%>' CommandName="ViewTaskResponseDoc"
                                                                                        ID="lnkBtnViewTaskResDoc" runat="server">
                                                                                        <img src='<%# ResolveUrl("~/Images/Eye.png")%>' alt="View" title="View Documents" />
                                                                                    </asp:LinkButton>

                                                                                    <asp:LinkButton
                                                                                        CommandArgument='<%# Eval("ID")+","+ Eval("TaskID")+","+ Eval("ContractID")%>' CommandName="DownloadTaskResponseDoc"
                                                                                        Visible='<%# CanDownloadDeleteDoc() %>' ID="lnkBtnDownloadTaskResDoc" runat="server">
                                                                                        <img src='<%# ResolveUrl("~/Images/download_icon_new.png")%>' alt="Download" title="Download Documents" />
                                                                                    </asp:LinkButton>

                                                                                    <asp:LinkButton CommandArgument='<%# Eval("ID")%>' Visible='<%# CanDownloadDeleteDoc() %>'
                                                                                        AutoPostBack="true" CommandName="DeleteTaskResponse"
                                                                                        OnClientClick="return confirm('Are you certain you want to delete this Response?');"
                                                                                        ID="lnkBtnDeleteTaskResponse" runat="server">
                                                                                         <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="Delete" title="Delete Response"/>
                                                                                    </asp:LinkButton>
                                                                                </ContentTemplate>
                                                                                <Triggers>
                                                                                    <asp:PostBackTrigger ControlID="lnkBtnDownloadTaskResDoc" />
                                                                                    <asp:PostBackTrigger ControlID="lnkBtnDeleteTaskResponse" />
                                                                                </Triggers>
                                                                            </asp:UpdatePanel>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                </Columns>
                                                                <RowStyle CssClass="clsROWgrid" />
                                                                <HeaderStyle CssClass="clsheadergrid" />
                                                                <EmptyDataTemplate>
                                                                    No Records Found
                                                                </EmptyDataTemplate>
                                                            </asp:GridView>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                        </div>
                                                    <div class="form-group required col-md-5"> 
                                                        <div class="panel panel-default">
                                                           <div class="panel-heading" style="padding: 4px 19px; background-color: #1fd9e1; color: white">Comments</div>
                                                           <div class="panel-body" style="padding: 1px; overflow-y: scroll;min-height: 293px; max-height: 184px;border-left-style: solid;border-bottom-style: solid;">

                                                            <div id="ShowDatadiv" style=""></div>

                                                               <div style="padding-left: 10px;">
                                   
                                                                    <textarea id="TxtComments" style="width: 250px;border-color: #ddd;float: left;border-radius: 12px;margin-top: 9px;" name="txtcmt"></textarea>
                                   
                                                                     <a onclick="submitData();" style="float:right;cursor:pointer;padding-right:70px;color: blue;margin-top: 20px;margin-bottom:101px" id="MainSubmit">Post</a>                                   
                                                               </div>
                                                           </div>
                                                         </div>
                                                     </div>
                                                </div>

                                                <asp:Panel ID="pnlTaskResponse" runat="server">
                                                    <div class="row">
                                                        <div class="form-group required col-md-2">
                                                            <label for="ddlStatus" class="control-label">Status</label>
                                                        </div>
                                                        <div class="form-group col-md-3">
                                                            <asp:DropDownListChosen runat="server" ID="ddlStatus" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                class="form-control" Width="100%" DataPlaceHolder="Select Status">
                                                            </asp:DropDownListChosen>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="form-group required col-md-2">
                                                            <label for="tbxTaskResComment" class="control-label">Comment(s)</label>
                                                        </div>
                                                        <div class="form-group col-md-10">
                                                            <asp:TextBox ID="tbxTaskResComment" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="2"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="rfvTaskResComment" ErrorMessage="Required Comment." runat="server" 
                                                                ControlToValidate="tbxTaskResComment" ValidationGroup="TaskResponseValidationGroup" Display="None" />
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="form-group col-md-2">
                                                            <label for="tbxTaskResComment" class="control-label">Upload Document(s)</label>
                                                        </div>

                                                        <div class="form-group col-md-10">
                                                            <asp:FileUpload ID="fuTaskResponseDocUpload" runat="server" AllowMultiple="true" Style="color: #8e8e93; margin-bottom: 15px;" />
                                                        </div>
                                                    </div>

                                                    <div class="row form-group text-center">
                                                        <div class="col-md-12">
                                                            <asp:Button Text="Save" runat="server" ID="btnSaveTaskResponse" CssClass="btn btn-primary" OnClick="btnSaveTaskResponse_Click" OnClientClick="if(!CheckValidation())return false;"
                                                                ValidationGroup="TaskResponseValidationGroup"></asp:Button>
                                                            <asp:Button Text="Clear" runat="server" ID="btnTaskResponseClear" CssClass="btn btn-primary" OnClick="btnClearTaskResponse_Click" />
                                                        </div>
                                                    </div>
                                                </asp:Panel>
                                            </div>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnSaveTaskResponse" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <asp:HiddenField runat="server" ID="hdnContractId" />
                <asp:HiddenField runat="server" ID="hdntaskId" />
                <!--Task-Response Panel End-->

                <!--AuditLog panel start-->
                    <div id="AuditLog" class="row Dashboard-white-widget">
                        <div class="dashboard">
                            <div class="col-lg-12 col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel panel-default" style="margin-bottom: 1px;">
                                        <div class="panel-heading">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseAuditLog">
                                                <h2>Audit Log</h2>
                                            </a>
                                            <div class="panel-actions">
                                                <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseAuditLog"><i class="fa fa-chevron-up"></i></a>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="collapseAuditLog" class="panel-collapse collapse in">
                                        <div runat="server" id="log" style="text-align: left;">
                                            <%--<fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; margin-top: 5px;">--%>
                                                <div style="margin-bottom: 7px; clear: both; margin-top: 10px">
                                                    <asp:GridView runat="server" AutoPostBack="true" ID="grdTransactionHistory" AutoGenerateColumns="false" AllowSorting="true" 
                                                        AllowPaging="true" PageSize="5" CssClass="table" GridLines="Horizontal" OnPageIndexChanging="grdTransactionHistory_OnPageIndexChanging"
                                                        BorderWidth="0px">
                                                        <Columns>
                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr.No." ItemStyle-Width="10%" FooterStyle-Width="10%">
                                                                   <ItemTemplate>
                                                                       <%#Container.DataItemIndex+1 %>
                                                                   </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Contract No." ItemStyle-Width="15%">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblDocType" runat="server" data-toggle="tooltip" ToolTip='<%# Eval("ContractNo")%>' Text='<%# Eval("ContractNo") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Contract Title" ItemStyle-Width="15%">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblDocType" runat="server" data-toggle="tooltip" ToolTip='<%# Eval("ContractTitle")%>' Text='<%# Eval("ContractTitle") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Action" ItemStyle-Width="15%">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblDocType" runat="server" data-toggle="tooltip" ToolTip='<%# Eval("Action")%>' Text='<%# Eval("Action") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Dated On" ItemStyle-Width="15%">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblDocType" runat="server" data-toggle="tooltip" ToolTip='<%# Eval("CreatedOn")%>' Text='<%# Eval("CreatedOn")%>'></asp:Label>
                                                                    </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Remark" ItemStyle-Width="15%">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblDocType" runat="server" data-toggle="tooltip" ToolTip='<%# Eval("Remark")%>' Text='<%# Eval("Remark") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <PagerStyle HorizontalAlign="Right" />
                                                        <PagerTemplate>
                                                            <table style="display: none">
                                                                <tr>
                                                                    <td>
                                                                        <asp:PlaceHolder ID="ph1" runat="server"></asp:PlaceHolder>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </PagerTemplate>
                                                    </asp:GridView>
                                                </div>
                                            <%--</fieldset> --%>                                          
                                        </div>
                                    </div>

                                    <div class="col-md-12 colpadding0">
                                    <div class="col-md-8 colpadding0">
                                        <%--<div runat="server" id="Div1" style="float: left; margin-top: 5px; color: #999">
                                            <p style="padding-right: 0px !Important;">
                                                <asp:Label ID="Label4" runat="server" Text="Showing "></asp:Label>
                                                <asp:Label ID="Label1" Font-Bold="true" runat="server" Text=""></asp:Label>- 
                                                <asp:Label ID="Label2" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                                                <asp:Label ID="Label3" Font-Bold="true" runat="server" Text=""></asp:Label>
                                            </p>
                                        </div>--%>
                                    </div>
                                    <div class="col-md-2 colpadding0">
                                        <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 37%; float: right; height:32px !important; margin-top: 2px;margin-left: 131px; position: absolute;"
                                            AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                                            <asp:ListItem Text="5" Selected="True"/>
                                            <asp:ListItem Text="10" />
                                            <asp:ListItem Text="20" />
                                            <asp:ListItem Text="50" />
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-md-2 colpadding0" style="float: right;">
                                        <div style="float: left; width: 60%">
                                            <p class="clsPageNo">Page</p>
                                        </div>
                                        <div style="float: left; width: 40%">
                                            <asp:DropDownListChosen runat="server" ID="DropDownListPageNoNew" AutoPostBack="true" DataPlaceHolder="No" AllowSingleDeselect="false"
                                                OnSelectedIndexChanged="DropDownListPageNoNew_SelectedIndexChanged" class="form-control m-bot15" Width="80%" Height="30px">
                                            </asp:DropDownListChosen>
                                        </div>
                                    </div>
                                    <asp:HiddenField ID="HiddenField1" runat="server" Value="0" />
                                </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <!--AuditLog panel end-->

                <%--Document Viewer--%>                
                <div class="modal fade" id="DocumentViewerPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden;">
                    <div class="modal-dialog" style="width: 100%">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body" style="height: 570px;">
                                <div style="width: 100%;">
                                    <div style="float: left; width: 10%">
                                        <table width="100%" style="text-align: left; margin-left: 5%;">
                                            <thead>
                                                <tr>
                                                    <td valign="top">
                                                        <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdatleMode="Conditional">
                                                            <ContentTemplate>
                                                                <asp:Repeater ID="rptDocmentVersionView" runat="server" OnItemCommand="rptDocmentVersionView_ItemCommand"
                                                                    OnItemDataBound="rptDocmentVersionView_ItemDataBound">
                                                                    <HeaderTemplate>
                                                                        <table id="tblComplianceDocumnets">
                                                                            <thead>
                                                                                <th>File Name</th>
                                                                            </thead>
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdatleMode="Conditional">
                                                                                    <ContentTemplate>
                                                                                        <asp:LinkButton CommandName="View" CommandArgument='<%# Eval("ID") %>' ID="lblDocumentVersionView"
                                                                                            runat="server" ToolTip='<%# Eval("FileName")%>' Text='<%# Eval("FileName").ToString().Substring(0,10) %>'></asp:LinkButton>
                                                                                    </ContentTemplate>
                                                                                    <Triggers>
                                                                                        <asp:AsyncPostBackTrigger ControlID="lblDocumentVersionView" />
                                                                                    </Triggers>
                                                                                </asp:UpdatePanel>
                                                                            </td>
                                                                        </tr>
                                                                    </ItemTemplate>
                                                                    <FooterTemplate>
                                                                        </table>
                                                                    </FooterTemplate>
                                                                </asp:Repeater>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:AsyncPostBackTrigger ControlID="rptDocmentVersionView" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                    <div style="float: left; width: 90%">
                                        <asp:Label runat="server" ID="lblMessage" Style="color: red;"></asp:Label>
                                        <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; height: 550px; width: 100%;">
                                            <iframe src="about:blank" id="iframeDocViewer" runat="server" width="100%" height="100%"></iframe>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                    
                <%--Document Viewer-END--%>
            </div>

        </asp:Panel>
    </form>
</body>
</html>
