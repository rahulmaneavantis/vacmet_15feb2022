﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.aspxPages
{
    public partial class ContractTaskDetailsPage : System.Web.UI.Page
    {
        public static string DocumentPath = "";
        protected static string KendoPath;
        protected static int CustId;
        protected static int UId;
        protected static string UploadFile;
        public bool ShowDownLoadIcon;
        protected string DocDownloadDeleteResult;
        protected void Page_Load(object sender, EventArgs e)
        {
            KendoPath = ConfigurationManager.AppSettings["KendoPathApp"];
            UId = Convert.ToInt32(AuthenticationHelper.UserID);
            CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            ShowDownLoadIcon = CaseManagement.CheckForClient(Convert.ToInt32(AuthenticationHelper.CustomerID), "PermissionToDowloadAdama");
            DocDownloadDeleteResult = "0";
            string customer = ConfigurationManager.AppSettings["ContractUploadTaskDoc"].ToString();
            long cID = AuthenticationHelper.CustomerID;
            List<string> CustomerList = customer.Split(',').ToList();
            UploadFile = "False";
            if (CustomerList.Count > 0)
            {
                foreach (string PList in CustomerList)
                {
                    if (PList == cID.ToString())
                    {
                        UploadFile = "True";
                        break;
                    }
                }
            }

            if (!IsPostBack)
            {
                bool validURL = false;
                if (!string.IsNullOrEmpty(Request.QueryString["A"])) //ContractID
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["B"])) //TaskID
                    {
                        if (!string.IsNullOrEmpty(Request.QueryString["C"])) //UID
                        {
                            if (!string.IsNullOrEmpty(Request.QueryString["D"])) //RID
                            {
                                //var strTaskID = Request.QueryString["TaskID"];
                                //var strContractID = Request.QueryString["CID"];
                                BindPaymentTerms();
                                
                                string strContractID = CryptographyManagement.Decrypt(Convert.ToString(Request.QueryString["A"]));
                                string strTaskID = CryptographyManagement.Decrypt(Convert.ToString(Request.QueryString["B"]));

                                string strUserID = CryptographyManagement.Decrypt(Convert.ToString(Request.QueryString["C"]));
                                string strRoleID = CryptographyManagement.Decrypt(Convert.ToString(Request.QueryString["D"]));

                                if (strContractID != "" && strTaskID != "" && strUserID != "" && strRoleID != "")
                                {
                                    long contractID = Convert.ToInt64(strContractID);
                                    long taskID = Convert.ToInt64(strTaskID);
                                    int assignedToUserID = Convert.ToInt32(strUserID);
                                    int roleID = Convert.ToInt32(strRoleID);

                                    hdnContractId.Value = strContractID;
                                    hdntaskId.Value = strTaskID;

                                    int customerID = UserManagement.GetByID(assignedToUserID).CustomerID ?? 0;

                                    ViewState["ContractInstanceID"] = contractID;
                                    ViewState["TaskID"] = taskID;

                                    ViewState["AssignedUserID"] = assignedToUserID;
                                    ViewState["RoleID"] = roleID;

                                    ViewState["CustomerID"] = customerID;

                                    if (Page.User.Identity.IsAuthenticated)
                                    {
                                        validURL = true;

                                        if (AuthenticationHelper.Role == "MGMT" || AuthenticationHelper.Role == "CADMN")
                                        {
                                            DocDownloadDeleteResult = "1";

                                            var lstTaskUsers = ContractTaskManagement.GetContractTaskUserAssignment(customerID, contractID, taskID);

                                            if (lstTaskUsers.Where(row => row.UserID == AuthenticationHelper.UserID).ToList().Count > 0)
                                                assignedToUserID = lstTaskUsers.Where(row => row.UserID == AuthenticationHelper.UserID).Select(row => row.UserID).FirstOrDefault();
                                            else
                                                assignedToUserID = lstTaskUsers.Select(row => row.UserID).FirstOrDefault();

                                            //assignedToUserID = ContractTaskManagement.GetRandomAssignedUserofTask(customerID, contractID, taskID, AuthenticationHelper.UserID);

                                            //In case of MGMT If he/she not assignedUser then Not Allowed to Save Response
                                            if (assignedToUserID != AuthenticationHelper.UserID)
                                                btnSaveTaskResponse.Enabled = false;
                                            else
                                                btnSaveTaskResponse.Enabled = true;
                                        }
                                        else
                                        {
                                            var taskDetail = ContractTaskManagement.GetTaskDetailsByUserRoleWise(contractID, taskID, assignedToUserID, roleID);

                                            if (taskDetail != null)
                                            {
                                                if (taskDetail.Permission == 1)
                                                {
                                                    DocDownloadDeleteResult = "1";
                                                }
                                            }
                                        }

                                        if ((AuthenticationHelper.UserID == assignedToUserID) || (AuthenticationHelper.Role == "MGMT" || AuthenticationHelper.Role == "CADMN"))
                                            ShowContractTaskDetails(contractID, taskID, assignedToUserID, roleID);
                                        else
                                        {
                                            cvLogin.IsValid = false;
                                            cvLogin.ErrorMessage = "You are not authorized to access this URL, or Invalid Access URL";
                                            return;
                                        }

                                    }
                                    else
                                    {
                                        var taskDetail = ContractTaskManagement.GetTaskDetailsByUserRoleWise(contractID, taskID, assignedToUserID, roleID);

                                        if (taskDetail != null)
                                        {
                                            if (taskDetail.Permission == 1)
                                            {
                                                DocDownloadDeleteResult = "1";
                                            }
                                            validURL = true;
                                            string taskVerificationMsg = VerifyTaskStatus(taskDetail); // String-Empty means Success

                                            if (!String.IsNullOrEmpty(taskVerificationMsg))
                                            {
                                                cvLogin.IsValid = false;
                                                cvLogin.ErrorMessage = taskVerificationMsg;
                                            }
                                            else
                                            {
                                                if (!string.IsNullOrEmpty(Request.QueryString["Checksum"])) //Checksum
                                                {
                                                    string receivedChecksum = Convert.ToString(Request.QueryString["Checksum"]);
                                                    string calculatedChecksum = Util.CalculateMD5Hash(contractID.ToString() + taskID.ToString() + assignedToUserID.ToString() + roleID.ToString());

                                                    //URL Parameter Checksum Validation
                                                    if (receivedChecksum.Trim().ToUpper().Equals(calculatedChecksum.Trim().ToUpper()))
                                                    {
                                                        showHideOTPPanel(true);
                                                        ShowOTP(contractID, taskID, assignedToUserID, roleID, taskDetail);
                                                    }
                                                    else
                                                        validURL = false;
                                                }
                                                else
                                                    validURL = false;
                                            }
                                        }
                                    }

                                    BindTransactions();
                                }
                            }
                        }
                    }
                }

                if(!validURL)
                {
                    cvLogin.IsValid = false;
                    cvLogin.ErrorMessage = "Invalid Access URL, Please copy and paste the Access URL into your browser's address bar.";
                    return;
                }
            }

            //Show Hide Grid Control - Enable/Disable Form Controls
            if (ViewState["taskStatus"] != null)
            {
                if (Convert.ToInt32(ViewState["taskStatus"]) == 14)
                    enableDisableTaskControls(false);
                else
                    enableDisableTaskControls(true);
            }
            else
                enableDisableTaskControls(true);
        }

        protected bool CanDownloadDeleteDoc()
        {
            try
            {
                bool result = true;
                if (ShowDownLoadIcon)
                {
                    if (DocDownloadDeleteResult != "0")
                    {
                        result = true;
                    }
                    else
                    {
                        result = false;
                    }
                }
                else
                {
                    result = true;
                }

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return false;
        }
        public void BindPaymentTerms()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            var key = "Contpayterm-" + Convert.ToString(customerID);
            var lstpaymentterm = (List<Cont_tbl_PaymentTerm>)HttpContext.Current.Cache[key];
            if (HttpContext.Current.Cache[key] == null)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    lstpaymentterm = (from row in entities.Cont_tbl_PaymentTerm
                                      where row.IsDeleted == false
                                      select row).ToList();
                }
                HttpContext.Current.Cache.Insert(key, lstpaymentterm, null, DateTime.Now.AddMinutes(1440), TimeSpan.Zero); // add it to cache
            }

            if (lstpaymentterm.Count > 0)
                lstpaymentterm = lstpaymentterm.OrderBy(row => row.Id).ToList();

            ddlPaymentTerm1.DataTextField = "PaymentTermName";
            ddlPaymentTerm1.DataValueField = "PaymentTermID";

            ddlPaymentTerm1.DataSource = lstpaymentterm;
            ddlPaymentTerm1.DataBind();
        }

        #region Contract Details

        private void BindContractStatusList(bool isForTask, bool isVisibleToUser)
        {
            try
            {
                ddlContractStatus.DataSource = null;
                ddlContractStatus.DataBind();
                ddlContractStatus.ClearSelection();

                ddlContractStatus.DataTextField = "StatusName";
                ddlContractStatus.DataValueField = "ID";

                var statusList = ContractTaskManagement.GetStatusList_All(isForTask, isVisibleToUser);

                ddlContractStatus.DataSource = statusList;
                ddlContractStatus.DataBind();                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindVendors()
        {
            int customerID = -1;            

            if (ViewState["CustomerID"] != null)
            {
                // customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                customerID = Convert.ToInt32(ViewState["CustomerID"]);

                var lstVendors = VendorDetails.GetVendors_All(customerID);

                //Drop-Down at Modal Pop-up
                lstBoxVendor.DataTextField = "VendorName";
                lstBoxVendor.DataValueField = "ID";

                lstBoxVendor.DataSource = lstVendors;
                lstBoxVendor.DataBind();

                lstBoxVendor.Items.Add(new ListItem("Add New", "0"));
            }
        }

        public void BindUsers()
        {
            try
            {
                int customerID = -1;
               
                if (ViewState["CustomerID"] != null)
                {
                    // customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                    customerID = Convert.ToInt32(ViewState["CustomerID"]);

                    var lstAllUsers = ContractUserManagement.GetAllUsers_Contract(customerID);

                    if (lstAllUsers.Count > 0)
                        lstAllUsers = lstAllUsers.Where(entry => entry.ContractRoleID != null).ToList();

                    var internalUsers = ContractUserManagement.GetRequiredUsers(lstAllUsers, 1); //1--Internal---

                    lstBoxOwner.DataValueField = "ID";
                    lstBoxOwner.DataTextField = "Name";
                    lstBoxOwner.DataSource = internalUsers;
                    lstBoxOwner.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindContractCategoryType()
        {
            try
            {
                int customerID = -1;
                if (ViewState["CustomerID"] != null)
                {
                    // customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                    customerID = Convert.ToInt32(ViewState["CustomerID"]);

                    var lstContractTypes = ContractTypeMasterManagement.GetContractTypes_All(customerID);

                    ddlContractType.DataTextField = "TypeName";
                    ddlContractType.DataValueField = "ID";

                    ddlContractType.DataSource = lstContractTypes;
                    ddlContractType.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindCustomerBranches()
        {
            try
            {
                int customerID = -1;
                if (ViewState["CustomerID"] != null)
                {
                    // customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                    customerID = Convert.ToInt32(ViewState["CustomerID"]);

                    tvBranches.Nodes.Clear();
                    NameValueHierarchy branch = null;

                    //var branchs = CustomerBranchManagement.GetAllHierarchy(customerID);
                    var branchs = CustomerBranchManagement.GetAllHierarchyManagementSatutory(customerID);
                    if (branchs.Count > 0)
                    {
                        branch = branchs[0];
                    }
                    tbxBranch.Text = "Select Entity/Location";
                    List<TreeNode> nodes = new List<TreeNode>();
                    BindBranchesHierarchy(null, branch, nodes);
                    foreach (TreeNode item in nodes)
                    {
                        tvBranches.Nodes.Add(item);
                    }

                    tvBranches.CollapseAll();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvContractPopUp.IsValid = false;
                cvContractPopUp.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp, List<TreeNode> nodes)
        {
            try
            {
                if (nvp != null)
                {
                    foreach (var item in nvp.Children)
                    {
                        TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                        BindBranchesHierarchy(node, item, nodes);
                        if (parent == null)
                        {
                            nodes.Add(node);
                        }
                        else
                        {
                            parent.ChildNodes.Add(node);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvContractPopUp.IsValid = false;
                cvContractPopUp.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        public void BindDepartments()
        {
            int customerID = -1;
            if (ViewState["CustomerID"] != null)
            {
                // customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                customerID = Convert.ToInt32(ViewState["CustomerID"]);

                var obj = CompDeptManagement.GetAllDepartmentMasterList(customerID);

                ddlDepartment.DataTextField = "Name";
                ddlDepartment.DataValueField = "ID";

                ddlDepartment.DataSource = obj;
                ddlDepartment.DataBind();
            }
        }

        public void manipulatedEmail(string Email)
        {
            // For Email Id Logic Set.
            string[] spl = Convert.ToString(Email).Split('@');
            int lenthusername = (spl[0].Length / 2);
            int mod = spl[0].Length % 2;
            if (mod > 0)
            {
                lenthusername = lenthusername - mod;
            }
            string beforemanupilatedemail = "";
            if (lenthusername == 1)
            {
                beforemanupilatedemail = spl[0].Substring(lenthusername);
            }
            else
            {
                beforemanupilatedemail = spl[0].Substring(lenthusername + 1);
            }

            string appendstar = "";
            for (int i = 0; i < beforemanupilatedemail.Length; i++) /*lenthusername*/
            {
                appendstar += "*";
            }

            string manupilatedemail = spl[0].Replace(beforemanupilatedemail, appendstar);

            string[] spl1 = spl[1].Split('.');
            int lenthatname = (spl1[0].Length / 2);
            int modat = spl1[0].Length % 2;
            if (modat > 0)
            {
                lenthatname = lenthatname - modat;
            }

            string beforatemail = "";
            if (lenthatname == 1)
            {
                beforatemail = spl1[0].Substring(lenthatname);
            }
            else
            {
                beforatemail = spl1[0].Substring(lenthatname + 1);
            }
            string appendstar1 = "";
            for (int i = 0; i < beforatemail.Length; i++) /*lenthatname*/
            {
                appendstar1 += "*";
            }

            string manupilatedatemail = spl1[0].Replace(beforatemail, appendstar1);
            string emailid = manupilatedemail;

            DateTime NewTime = DateTime.Now.AddMinutes(30);
            Time.Text = NewTime.Hour + ":" + NewTime.Minute + NewTime.ToString(" tt");
            email.Text = Convert.ToString(Email).Replace(spl[0], manupilatedemail).Replace(spl1[0], manupilatedatemail);
        }

        public void manipulatedContactNo(string contactNo)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(contactNo)))
            {
                // For Mobile Number Logic Set.
                if (contactNo.Length == 10 && contactNo.StartsWith("9") || contactNo.StartsWith("8") || contactNo.StartsWith("7"))
                {
                    string s = Convert.ToString(contactNo);
                    int l = s.Length;
                    s = s.Substring(l - 4);
                    string r = new string('*', l - 4);
                    //r = r + s;
                    mobileno.Text = r + s;
                }
                else
                {
                    mobileno.Text = "";
                    cvLogin.IsValid = false;
                    cvLogin.ErrorMessage = "You don't seem to have a correct registered mobile number with us. However OTP sent on your registered email. Please update your mobile number to use this feature in future.";
                }
            }
            else
            {
                mobileno.Text = "";
                cvLogin.IsValid = false;
                cvLogin.ErrorMessage = "You don't seem to have a correct registered mobile number with us. However OTP sent on your registered email. Please update your mobile number to use this feature in future.";
            }
        }

        public bool sendOTP(long taskID, string Email, string contactNumber)
        {
            try
            {
                bool successSendOTP = true;

                //Generate Random Number 6 Digit For OTP.
                Random random = new Random();
                int value = random.Next(1000000);
                long ContactNo;

                VerifyOTP OTPData = new VerifyOTP()
                {
                    UserId = Convert.ToInt32(taskID),
                    EmailId = Email,
                    OTP = Convert.ToInt32(value),
                    CreatedOn = DateTime.Now,
                    IsVerified = false
                };

                bool OTPresult = long.TryParse(contactNumber, out ContactNo);

                if (OTPresult)
                    OTPData.MobileNo = ContactNo;
                else
                    OTPData.MobileNo = 0;

                VerifyOTPManagement.Create(OTPData); // Insert Data in OTP Table.

                string SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"].ToString();

                try
                {
                    //Send Email on User Mail Id.
                    EmailManager.SendMail(SenderEmailAddress, new List<String>(new String[] { Email }), null, null, "OTP Verification", "Your One Time Password for Avantis login is:" + Convert.ToString(value) + "<br>" + "<br>" + "Thank you," + "<br>" + "Team Teamlease");
                }
                catch (Exception ex)
                {
                    successSendOTP = false;
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }

                if (contactNumber.Length == 10 && (contactNumber.StartsWith("9") || contactNumber.StartsWith("8") || contactNumber.StartsWith("7")))
                {
                    try
                    {
                        //Send SMS on User Mobile No.                      
                        var data = VerifyOTPManagement.GetSMSConfiguration("Avacom");
                        if (data != null)
                        {
                            SendSms.sendsmsto(contactNumber, "Your One Time Password for Avantis login is: " + Convert.ToString(value) + ".  " + "Thank you, " + "Avantis Team.", data.TEMPLATEID_DLT_TE_ID, data.authkey, data.Header_sender, data.route);
                        }
                        else
                        {
                            SendSms.sendsmsto(contactNumber, "Your One Time Password for Avantis login is: " + Convert.ToString(value) + ".  " + "Thank you, " + "Avantis Team.", "1207161856353769674");
                        }
                    }
                    catch (Exception ex)
                    {
                        successSendOTP = false;
                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                }

                return successSendOTP;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public void showHideOTPPanel(bool flag)
        {
            try
            {
                if (flag)
                {
                    panelOTP.Visible = flag;
                    avatisLogo.Visible = flag;
                    panelTaskDetail.Visible = false;
                }
                else
                {
                    panelOTP.Visible = flag;
                    avatisLogo.Visible = flag;
                    panelTaskDetail.Visible = true;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ShowOTP(long contractID, long taskID, int userID, int roleID, Cont_SP_GetTaskDetails_UserRoleWise_Result taskDetail)
        {
            try
            {
                if (taskDetail != null)
                {
                    //Session["TaskDetail"] = taskDetail;

                    #region
                    if (taskDetail.LinkCreatedOn != null)
                    {
                        //Check for Link Active or Expired
                        TimeSpan diff = DateTime.Now - Convert.ToDateTime(taskDetail.LinkCreatedOn);
                        double hours = diff.TotalHours;

                        if (hours <= 48)
                        {
                            if (taskDetail.Email != null && taskDetail.Email != "")
                            {
                                if (taskDetail.ContactNumber != null && taskDetail.ContactNumber != "")
                                {
                                    if (sendOTP(userID, taskDetail.Email, taskDetail.ContactNumber))
                                    {
                                        divOTP.Visible = true;
                                        ViewState["WrongOTP"] = 0;

                                        manipulatedEmail(taskDetail.Email);
                                        manipulatedContactNo(taskDetail.ContactNumber);
                                    }
                                    else
                                    {
                                        cvLogin.IsValid = false;
                                        cvLogin.ErrorMessage = "OTP sent failed, Please try again after some time.";
                                        return;
                                    }
                                }
                            }
                        }
                        else
                        {
                            cvLogin.IsValid = false;
                            cvLogin.ErrorMessage = "Access URL got expired, Please contact assignor to re-generate new URL.";
                            return;
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected string VerifyTaskStatus(Cont_SP_GetTaskDetails_UserRoleWise_Result taskDetail)
        {
            string returnStr = string.Empty;

            try
            {
                if (taskDetail != null)
                {
                    if (taskDetail.TaskStatusID != 14) //If Task Closed then No Response
                    {
                        if (taskDetail.TaskIsActive) //Task Active or Deleted
                        {
                            if (taskDetail.URLExpired != true) //URL Active
                            {                                
                                returnStr = string.Empty;
                            }
                            else
                            {
                                returnStr = "Access URL got expired, Please contact assignor to re-generate new URL.";
                            }
                        }
                        else
                        {
                            returnStr = "Task Deleted by Assignor. So, Access URL got expired.";
                        }
                    }
                    else
                    {
                        returnStr = "Task Closed by Assignor. So, Access URL got expired.";
                    }
                }
                else
                {
                    returnStr = "Invalid Access URL, please copy and paste the Access URL into your browser's address bar.";
                }

                return returnStr;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "Something went wrong, Please try again";
            }
        }

        protected void btnOTP_Click(object sender, EventArgs e)
        {
            try
            {
                int wrongOTPCount = 0;
                if (ViewState["AssignedUserID"] != null)
                {
                    if (!string.IsNullOrEmpty(ViewState["AssignedUserID"].ToString()))
                    {
                        int userID;
                        userID = int.Parse(ViewState["AssignedUserID"].ToString());

                        //if (VerifyOTPManagement.Exists(Convert.ToInt32(txtOTP.Text), Convert.ToInt32(ViewState["userID"].ToString())))
                        if (VerifyOTPManagement.verifyOTP(Convert.ToInt32(txtOTP.Text), Convert.ToInt32(ViewState["AssignedUserID"].ToString())))
                        {
                            bool verifiedOTP = false;

                            VerifyOTP OTPData = VerifyOTPManagement.GetByID(userID);

                            TimeSpan span = DateTime.Now.Subtract(OTPData.CreatedOn.Value);

                            if (0 <= span.Minutes && span.Minutes <= 30)
                            {
                                VerifyOTPManagement.UpdateVerifiedFlag(Convert.ToInt32(ViewState["AssignedUserID"].ToString()), Convert.ToInt32(txtOTP.Text));
                                verifiedOTP = true;
                            }
                            else
                            {
                                cvLogin.ErrorMessage = "OTP Expired.";
                                cvLogin.IsValid = false;

                                if (ViewState["WrongOTP"] != null)
                                {
                                    wrongOTPCount = Convert.ToInt32(ViewState["WrongOTP"]);
                                    ViewState["WrongOTP"] = wrongOTPCount++;
                                }

                                return;
                            }

                            if (verifiedOTP)
                            {
                                //Show Contract Task Details
                                if (ViewState["TaskID"] != null && ViewState["ContractInstanceID"] != null
                                    && ViewState["RoleID"] != null)
                                {
                                    long taskID = Convert.ToInt64(ViewState["TaskID"]);
                                    long contractID = Convert.ToInt64(ViewState["ContractInstanceID"]);
                                   
                                    int roleID = Convert.ToInt32(ViewState["RoleID"]);

                                    ShowContractTaskDetails(contractID, taskID, userID, roleID);
                                }
                            }
                            else
                            {
                                cvLogin.ErrorMessage = "OTP Expired.";
                                cvLogin.IsValid = false;
                                return;
                            }
                        }
                        else
                        {
                            if (ViewState["WrongOTP"] != null)
                            {
                                wrongOTPCount = Convert.ToInt32(ViewState["WrongOTP"]);
                                wrongOTPCount = wrongOTPCount + 1;
                                ViewState["WrongOTP"] = wrongOTPCount;
                            }

                            if (wrongOTPCount <= 3)
                            {
                                cvLogin.ErrorMessage = "Please Enter Correct OTP.";
                                cvLogin.IsValid = false;
                                return;
                            }
                            else
                            {
                                //Expire Access URL 
                                if (ViewState["TaskID"] != null)
                                {
                                    LitigationTaskManagement.ExpireTaskURL(Convert.ToInt32(ViewState["TaskID"]), userID);

                                    cvLogin.IsValid = false;
                                    cvLogin.ErrorMessage = "Access URL got expired, Please contact assignor to re-generate new URL.";
                                    return;
                                }
                                else
                                {
                                    cvLogin.IsValid = false;
                                    cvLogin.ErrorMessage = "Access URL got expired, Please contact assignor to re-generate new URL.";
                                    return;
                                }
                            }
                        }
                    }
                    else
                    {
                        cvLogin.ErrorMessage = "Please Enter Correct OTP.";
                        cvLogin.IsValid = false;
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvLogin.IsValid = false;
                cvLogin.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ShowContractTaskDetails(long contractID, long taskID, int userID, int roleID)
        {
            try
            {
                showHideOTPPanel(false);

                ShowContractDetails(contractID);
                ShowTaskDetails(contractID, taskID, userID, roleID);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ShowContractDetails(long contractID)
        {
            try
            {
                int customerID = -1;

                if (ViewState["CustomerID"] != null)
                {
                    // customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                    customerID = Convert.ToInt32(ViewState["CustomerID"]);

                    VendorDetails.BindVendors(lstBoxVendor, customerID);

                    BindCustomerBranches();
                    BindDepartments();
                    BindVendors();
                    BindUsers();
                    BindContractCategoryType();

                    BindContractStatusList(false, true);

                    clearContractControls();
                    enableDisableContractSummaryTabControls(false);

                    if (contractID != 0)
                    {
                        var contractRecord = ContractManagement.GetContractDetailsByContractID(customerID, contractID);                        
                        //var contractRecord = ContractManagement.GetContractByID(contractID);

                        if (contractRecord != null)
                        {
                            ddlContractStatus.ClearSelection();

                            if (ddlContractStatus.Items.FindByValue(contractRecord.ContractStatusID.ToString()) != null)
                                ddlContractStatus.Items.FindByValue(contractRecord.ContractStatusID.ToString()).Selected = true;

                            txtContractNo.Text = contractRecord.ContractNo;
                            txtTitle.Text = contractRecord.ContractTitle;
                            tbxDescription.Text = contractRecord.ContractDetailDesc;

                            if (contractRecord.CustomerBranchID != 0)
                            {
                                foreach (TreeNode node in tvBranches.Nodes)
                                {
                                    if (node.Value == contractRecord.CustomerBranchID.ToString())
                                    {
                                        node.Selected = true;
                                    }
                                    foreach (TreeNode item1 in node.ChildNodes)
                                    {
                                        if (item1.Value == contractRecord.CustomerBranchID.ToString())
                                            item1.Selected = true;
                                    }
                                }
                            }

                            tvBranches_SelectedNodeChanged(null, null);

                            #region Vendor

                            var lstVendorMapping = VendorDetails.GetVendorMapping(contractID);

                            if (lstBoxVendor.Items.Count > 0)
                            {
                                lstBoxVendor.ClearSelection();
                            }

                            if (lstVendorMapping.Count > 0)
                            {
                                foreach (var VendorID in lstVendorMapping)
                                {
                                    if (lstBoxVendor.Items.FindByValue(VendorID.ToString()) != null)
                                        lstBoxVendor.Items.FindByValue(VendorID.ToString()).Selected = true;
                                }
                            }

                            #endregion

                            ddlDepartment.ClearSelection();

                            if (ddlDepartment.Items.FindByValue(contractRecord.DepartmentID.ToString()) != null)
                                ddlDepartment.SelectedValue = contractRecord.DepartmentID.ToString();

                            if (contractRecord.ProposalDate != null)
                                txtProposalDate.Text = Convert.ToDateTime(contractRecord.ProposalDate).ToString("dd-MM-yyyy");
                            else
                                txtProposalDate.Text = string.Empty;

                            if (contractRecord.AgreementDate != null)
                                txtAgreementDate.Text = Convert.ToDateTime(contractRecord.AgreementDate).ToString("dd-MM-yyyy");
                            else
                                txtAgreementDate.Text = string.Empty;

                            if (contractRecord.EffectiveDate != null)
                                txtEffectiveDate.Text = Convert.ToDateTime(contractRecord.EffectiveDate).ToString("dd-MM-yyyy");
                            else
                                txtEffectiveDate.Text = string.Empty;

                            if (contractRecord.ReviewDate != null)
                                txtReviewDate.Text = Convert.ToDateTime(contractRecord.ReviewDate).ToString("dd-MM-yyyy");
                            else
                                txtReviewDate.Text = string.Empty;

                            if (contractRecord.ExpirationDate != null)
                                txtExpirationDate.Text = Convert.ToDateTime(contractRecord.ExpirationDate).ToString("dd-MM-yyyy");
                            else
                                txtExpirationDate.Text = string.Empty;

                            if (contractRecord.ExpirationDate != null)
                                txtNoticeTerm.Text = Convert.ToString(contractRecord.NoticeTermNumber);
                            else
                                txtNoticeTerm.Text = string.Empty;

                            if (ddlNoticeTerm.Items.FindByValue(contractRecord.NoticeTermType.ToString()) != null)
                                ddlNoticeTerm.SelectedValue = contractRecord.NoticeTermType.ToString();

                            ddlContractType.ClearSelection();

                            if (ddlContractType.Items.FindByValue(contractRecord.ContractTypeID.ToString()) != null)
                                ddlContractType.SelectedValue = contractRecord.ContractTypeID.ToString();

                            ddlContractType_SelectedIndexChanged(null, null);

                            if (contractRecord.ContractSubTypeID != null)
                            {
                                ddlContractSubType.ClearSelection();

                                if (ddlContractSubType.Items.FindByValue(contractRecord.ContractSubTypeID.ToString()) != null)
                                    ddlContractSubType.SelectedValue = contractRecord.ContractSubTypeID.ToString();
                            }

                            #region Owner && Approvers                            

                            var lstContractUserAssignment = ContractManagement.GetContractUserAssignment_All(contractID);

                            if (lstContractUserAssignment.Count > 0)
                            {
                                lstBoxOwner.ClearSelection();
                                lstBoxApprover.ClearSelection();

                                foreach (var eachAssignmentRecord in lstContractUserAssignment)
                                {
                                    if (eachAssignmentRecord.RoleID == 3)
                                    {
                                        if (lstBoxOwner.Items.FindByValue(eachAssignmentRecord.UserID.ToString()) != null)
                                            lstBoxOwner.Items.FindByValue(eachAssignmentRecord.UserID.ToString()).Selected = true;
                                    }
                                    else if (eachAssignmentRecord.RoleID == 6)
                                    {
                                        if (lstBoxApprover.Items.FindByValue(eachAssignmentRecord.UserID.ToString()) != null)
                                            lstBoxApprover.Items.FindByValue(eachAssignmentRecord.UserID.ToString()).Selected = true;
                                    }
                                }
                            }

                            //var lstAssignedUserIDs = ContractManagement.GetAssignedUsersByRoleID(contractInstanceID, 3); //3--Owner

                            //if (lstBoxOwner.Items.Count > 0)
                            //{
                            //    lstBoxOwner.ClearSelection();
                            //}

                            //if (lstAssignedUserIDs.Count > 0)
                            //{
                            //    foreach (var userID in lstAssignedUserIDs)
                            //    {
                            //        if (lstBoxOwner.Items.FindByValue(userID.ToString()) != null)
                            //            lstBoxOwner.Items.FindByValue(userID.ToString()).Selected = true;
                            //    }
                            //}                          

                            #endregion

                            if (!string.IsNullOrEmpty(tbxContractAmt.Text))
                                contractRecord.ContractAmt = Convert.ToDecimal(tbxContractAmt.Text);
                            else
                                contractRecord.ContractAmt = 0;

                            #region Payment Term

                            var lstpaymenttermMapping = ContractManagement.GetPaymentTermMapping(contractID);

                            if (ddlPaymentTerm1.Items.Count > 0)
                            {
                                ddlPaymentTerm1.ClearSelection();
                            }

                            if (lstpaymenttermMapping.Count > 0)
                            {
                                foreach (var paymenttermID in lstpaymenttermMapping)
                                {
                                    if (ddlPaymentTerm1.Items.FindByValue(paymenttermID.ToString()) != null)
                                        ddlPaymentTerm1.Items.FindByValue(paymenttermID.ToString()).Selected = true;
                                }
                            }

                            #endregion
                            //if (contractRecord.PaymentTermID != null)
                            //{
                            //    ddlPaymentTerm.ClearSelection();

                            //    if (ddlPaymentTerm.Items.FindByValue(contractRecord.PaymentTermID.ToString()) != null)
                            //        ddlPaymentTerm.SelectedValue = contractRecord.PaymentTermID.ToString();
                            //}

                            if (!string.IsNullOrEmpty(txtBoxProduct.Text))
                                contractRecord.ProductItems = Convert.ToString(txtBoxProduct.Text.Trim());
                            
                        }

                        //BindContractDocuments_Paging();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void clearContractControls()
        {
            try
            {
                txtContractNo.Text = "";
                txtTitle.Text = "";
                tbxDescription.Text = "";

                tbxBranch.Text = "";
                tbxBranch.Text = "Select Entity/Location";
                lstBoxVendor.ClearSelection();
                ddlDepartment.ClearSelection();

                txtProposalDate.Text = "";
                txtAgreementDate.Text = "";
                txtEffectiveDate.Text = "";

                txtReviewDate.Text = "";
                txtExpirationDate.Text = "";
                txtNoticeTerm.Text = "";

                ddlContractType.ClearSelection();
                ddlContractSubType.ClearSelection();
                lstBoxOwner.ClearSelection();

                tbxContractAmt.Text = "";
                ddlPaymentTerm1.ClearSelection();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void enableDisableContractSummaryTabControls(bool flag)
        {
            try
            {
                ddlContractStatus.Enabled = flag;

                txtContractNo.Enabled = flag;
                txtTitle.Enabled = flag;
                tbxDescription.Enabled = flag;

                tbxBranch.Enabled = flag;
                lstBoxVendor.Enabled = flag;
                ddlDepartment.Enabled = flag;

                txtProposalDate.Enabled = flag;
                txtAgreementDate.Enabled = flag;
                txtEffectiveDate.Enabled = flag;

                txtReviewDate.Enabled = flag;
                txtExpirationDate.Enabled = flag;

                txtNoticeTerm.Enabled = flag;
                ddlNoticeTerm.Enabled = flag;

                tbxContractAmt.Enabled = flag;
                ddlPaymentTerm1.Enabled = flag;

                lstBoxVendor.Enabled = flag;
                ddlDepartment.Enabled = flag;

                ddlContractType.Enabled = flag;
                ddlContractSubType.Enabled = flag;
                lstBoxOwner.Enabled = flag;
                ddlPaymentTerm1.Enabled = flag;
                txtBoxProduct.Enabled = flag;

                if (flag)
                {
                    ddlDepartment.Attributes.Remove("disabled");
                    ddlNoticeTerm.Attributes.Remove("disabled");
                    ddlContractType.Attributes.Remove("disabled");
                    ddlContractSubType.Attributes.Remove("disabled");
                    ddlPaymentTerm1.Attributes.Remove("disabled");
                    lstBoxOwner.Attributes.Remove("disabled");
                    lstBoxVendor.Attributes.Remove("disabled");
                }
                else
                {
                    ddlDepartment.Attributes.Add("disabled", "disabled");
                    ddlNoticeTerm.Attributes.Add("disabled", "disabled");
                    ddlContractType.Attributes.Add("disabled", "disabled");
                    ddlContractSubType.Attributes.Add("disabled", "disabled");
                    lstBoxOwner.Attributes.Add("disabled", "disabled");
                    lstBoxVendor.Attributes.Add("disabled", "disabled");
                    ddlPaymentTerm1.Attributes.Add("disabled", "disabled");
                }

                if (grdCustomField != null)
                {
                    grdCustomField.Enabled = flag;
                    HideShowGridColumns(grdCustomField, "Action", flag);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void HideShowGridColumns(GridView gridView, string headerTextToMatch, bool flag)
        {
            try
            {
                if (gridView == null)
                {
                    return;
                }

                // Loop through all of the columns in the grid.
                for (int i = 0; i < gridView.Columns.Count; i++)
                {
                    String headerText = gridView.Columns[i].HeaderText;

                    //Show Hide Columns with Specific headerText
                    if (headerText == headerTextToMatch)
                        gridView.Columns[i].Visible = flag;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlContractType_SelectedIndexChanged(object sender, EventArgs e)
        {
            int customerID = -1;
            
            if (ViewState["CustomerID"] != null)
            {
                // customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                customerID = Convert.ToInt32(ViewState["CustomerID"]);

                bool bindSuccess = false;

                if (ddlContractType.SelectedValue != null && ddlContractType.SelectedValue != "0" && ddlContractType.SelectedValue != "")
                {
                    ViewState["ddlCustomFieldFilled"] = null;
                    ViewState["dataTableCustomFields"] = null;

                    ViewState["CustomefieldCount"] = null;

                    ViewState["ContractTypeIDUpdated"] = "false";

                    var lstContractSubTypes = ContractTypeMasterManagement.GetContractSubType_All(customerID, Convert.ToInt64(ddlContractType.SelectedValue));

                    if (lstContractSubTypes.Count > 0)
                    {
                        ddlContractSubType.DataTextField = "SubTypeName";
                        ddlContractSubType.DataValueField = "ContractSubTypeID";

                        ddlContractSubType.DataSource = lstContractSubTypes;
                        ddlContractSubType.DataBind();
                        ddlContractSubType.Items.Add(new ListItem("Add New", "0"));

                        bindSuccess = true;
                    }
                    Session["ContractTypeID"] = ddlContractType.SelectedValue;
                    BindCustomFields(grdCustomField, grdCustomField);
                }

                if (!bindSuccess)
                {
                    if (ddlContractSubType.Items.Count > 0)
                        ddlContractSubType.Items.Clear();

                    if (ddlContractType.SelectedValue != null && ddlContractType.SelectedValue != "0" && ddlContractType.SelectedValue != "")
                        ddlContractSubType.Items.Add(new ListItem("Add New", "0"));
                }

                upContractTypeSubType.Update();

                //ScriptManager.RegisterStartupScript(this, this.GetType(), "changeContractType", "ddlContractTypeChange(); ddlCustomFieldChange();", true);
            }
        }

        protected void tvBranches_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxBranch.Text = tvBranches.SelectedNode != null ? tvBranches.SelectedNode.Text : "Select Entity/Location";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "hideDivTree", "hideDivBranch();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvContractPopUp.IsValid = false;
                cvContractPopUp.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        private void BindCustomFields(GridView gridViewCustomField, GridView gridViewCustomField_History)
        {
            try
            {
                int customerID = -1;

                if (ViewState["CustomerID"] != null)
                {
                    // customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                    customerID = Convert.ToInt32(ViewState["CustomerID"]);

                    int contractInstanceID = 0;
                    if (!string.IsNullOrEmpty(Convert.ToString(ViewState["ContractInstanceID"])))
                    {
                        contractInstanceID = Convert.ToInt32(ViewState["ContractInstanceID"]);
                    }
                    if (ddlContractType.SelectedValue != null && ddlContractType.SelectedValue != "0")  //&& CaseInstanceID != 0
                    {
                        List<Cont_SP_GetCustomFieldsValues_Result> lstCustomFields = new List<Cont_SP_GetCustomFieldsValues_Result>();

                        if (contractInstanceID != 0)
                        {
                            lstCustomFields = ContractManagement.GetCustomsFieldsContractWise_All(customerID, contractInstanceID, Convert.ToInt64(ddlContractType.SelectedValue));

                            bool historyFlag = false;
                            if (ViewState["FlagHistory"] != null)
                            {
                                historyFlag = Convert.ToBoolean(Convert.ToInt32(ViewState["FlagHistory"]));
                            }

                            if (lstCustomFields != null && lstCustomFields.Count > 0)
                            {
                                ViewState["CustomefieldCount"] = lstCustomFields.Count;

                                if (!historyFlag)
                                {
                                    gridViewCustomField.DataSource = lstCustomFields;
                                    gridViewCustomField.DataBind();

                                    //lblAddNewGround.Visible = true;

                                    gridViewCustomField.Visible = true;
                                    gridViewCustomField_History.Visible = false;
                                }
                                else if (historyFlag)
                                {
                                    gridViewCustomField_History.DataSource = lstCustomFields;
                                    gridViewCustomField_History.DataBind();

                                    //lblAddNewGround.Visible = false;

                                    gridViewCustomField.Visible = false;
                                    gridViewCustomField_History.Visible = true;
                                }
                            }
                            else
                            {
                                Cont_SP_GetCustomFieldsValues_Result obj = new Cont_SP_GetCustomFieldsValues_Result(); //initialize empty class that may contain properties
                                lstCustomFields.Add(obj); //Add empty object to list

                                if (!historyFlag)
                                {
                                    gridViewCustomField.DataSource = lstCustomFields; /*Assign datasource to create one row with default values for the class you have*//*Assign datasource to create one row with default values for the class you have*/
                                    gridViewCustomField.DataBind(); //Bind that empty source     

                                    //To Hide row
                                    gridViewCustomField.Rows[0].Visible = false;
                                    gridViewCustomField.Rows[0].Controls.Clear();

                                    //lblAddNewGround.Visible = true;

                                    gridViewCustomField.Visible = true;
                                    gridViewCustomField_History.Visible = false;
                                }
                                else if (historyFlag)
                                {
                                    gridViewCustomField_History.DataSource = lstCustomFields;
                                    gridViewCustomField_History.DataBind();

                                    //To Hide row
                                    gridViewCustomField_History.Rows[0].Visible = false;
                                    gridViewCustomField_History.Rows[0].Controls.Clear();

                                    //lblAddNewGround.Visible = false;

                                    gridViewCustomField.Visible = false;
                                    gridViewCustomField_History.Visible = true;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindCustomFieldDropDown(DropDownList ddlCustomField, GridView gridViewCustomField)
        {
            try
            {
                int customerID = -1;
                if (ViewState["CustomerID"] != null)
                {
                    // customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                    customerID = Convert.ToInt32(ViewState["CustomerID"]);

                    if (ddlContractType.SelectedValue != null && ddlContractType.SelectedValue != "" && ddlContractType.SelectedValue != "0")
                    {
                        var customFields = ContractManagement.GetCustomsFieldsByContractType(customerID, Convert.ToInt32(ddlContractType.SelectedValue));

                        if (customFields.Count > 0)
                        {
                            //lblAddNewGround.Visible = true;
                            gridViewCustomField.Visible = true;

                            if (ddlCustomField.Items.Count > 0)
                                ddlCustomField.Items.Clear();

                            ddlCustomField.DataTextField = "Label";
                            ddlCustomField.DataValueField = "ID";

                            ddlCustomField.DataSource = customFields;
                            ddlCustomField.DataBind();

                            ddlCustomField.Items.Add(new ListItem("Add New", "0"));

                            ViewState["ddlCustomFieldFilled"] = "1";
                        }
                        else
                        {
                            //lblAddNewGround.Visible = false;
                            gridViewCustomField.Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdCustomField_Common_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                GridView gridView = (GridView)sender;

                if (gridView != null)
                {
                    if (e.Row.RowType == DataControlRowType.DataRow)
                    {
                        TextBox tbxLabelValue = (TextBox)e.Row.FindControl("tbxLabelValue");

                        if (!string.IsNullOrEmpty(Request.QueryString["HistoryFlag"]))
                        {
                            tbxLabelValue.Enabled = false;
                        }

                        //Hide Delete in Case of Total or Row with 0 ID
                        Label lblID = (Label)e.Row.FindControl("lblID");
                        LinkButton lnkBtnDeleteCustomField = (LinkButton)e.Row.FindControl("lnkBtnDeleteCustomField");

                        if (lblID != null && lnkBtnDeleteCustomField != null)
                        {
                            if (lblID.Text != "" && lblID.Text != "0")
                            {
                                e.Row.Enabled = true;
                                lnkBtnDeleteCustomField.Visible = true;
                            }
                            else
                            {
                                e.Row.Enabled = false;
                                lnkBtnDeleteCustomField.Visible = false;
                            }
                        }
                    }

                    if (e.Row.RowType == DataControlRowType.Footer)
                    {
                        DropDownList ddlFieldName_Footer = (DropDownList)e.Row.FindControl("ddlFieldName_Footer");

                        if (ddlFieldName_Footer != null)
                        {
                            BindCustomFieldDropDown(ddlFieldName_Footer, gridView);

                            foreach (GridViewRow gvr in gridView.Rows)
                            {
                                Label lblID = (Label)gvr.FindControl("lblID");

                                if (lblID != null)
                                {
                                    if (lblID.Text != "")
                                    {
                                        if (ddlFieldName_Footer.Items.FindByValue(lblID.Text) != null)
                                            ddlFieldName_Footer.Items.Remove(ddlFieldName_Footer.Items.FindByValue(lblID.Text));
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        #endregion

        #region Contract Document
        public void BindContractDocuments_Paging()
        {
            try
            {
                int customerID = -1;

                if (ViewState["ContractInstanceID"] != null && ViewState["CustomerID"] != null)
                {
                    customerID = Convert.ToInt32(ViewState["CustomerID"]);
                    long ContractInstanceID = Convert.ToInt64(ViewState["ContractInstanceID"]);

                    int PageNumber = 1;
                    if (!(string.IsNullOrEmpty(DropDownListPageNo.SelectedValue)))
                    {
                        PageNumber = Convert.ToInt32(DropDownListPageNo.SelectedValue);
                    }

                    List<Cont_SP_GetContractDocuments_Paging_Result> lstContDocs = new List<Cont_SP_GetContractDocuments_Paging_Result>();

                    lstContDocs = ContractDocumentManagement.GetContractDocuments_Paging(customerID, ContractInstanceID, Convert.ToInt32(grdContractDocuments.PageSize), PageNumber);

                    grdContractDocuments.DataSource = lstContDocs;
                    grdContractDocuments.DataBind();

                    int Totalcnt = 0;
                    if (lstContDocs.Count > 0)
                    {
                        foreach (var item in lstContDocs)
                        {
                            Totalcnt = Convert.ToInt32(item.TotalRowCount);
                            break;
                        }
                    }

                    Session["TotalRows"] = Totalcnt;
                    lblStartRecord.Text = DropDownListPageNo.SelectedValue;
                    bindPageNumber();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvContractDocument.IsValid = false;
                cvContractDocument.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                //Select Which Grid to Loop based on Selected Category/Type
                DropDownListChosen.DropDownListChosen ddlPageNo = null;

                ddlPageNo = DropDownListPageNo;

                if (ddlPageNo.Items.Count > 0)
                {
                    ddlPageNo.Items.Clear();
                }

                ddlPageNo.DataTextField = "ID";
                ddlPageNo.DataValueField = "ID";

                ddlPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    ddlPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    ddlPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    ddlPageNo.Items.Add("0");
                    ddlPageNo.SelectedValue = ("0").ToString();
                }

                SetShowingRecords();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                int totalPages = 0;
                int pageSize = 0;
                TotalRows.Value = Session["TotalRows"].ToString();

                pageSize = Convert.ToInt32(grdContractDocuments.PageSize);

                if (pageSize != 0)
                {
                    totalPages = Convert.ToInt32(TotalRows.Value) / pageSize;

                    // total page item to be displyed
                    int pageItemRemain = Convert.ToInt32(TotalRows.Value) % pageSize;

                    // remaing no of pages
                    if (pageItemRemain > 0)// set total No of pages
                    {
                        totalPages = totalPages + 1;
                    }
                    else
                    {
                        totalPages = totalPages + 0;
                    }
                }

                return totalPages;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        private void SetShowingRecords()
        {
            try
            {
                int PageSize = 0;
                int PageNumber = 0;

                int EndRecord = 0;
                int TotalRecord = 0;
                int TotalValue = 0;


                PageSize = grdContractDocuments.PageSize;
                PageNumber = Convert.ToInt32(DropDownListPageNo.SelectedValue);


                TotalValue = PageSize * PageNumber;

                if (Session["TotalRows"] != null)
                {
                    TotalRecord = Convert.ToInt32(Session["TotalRows"]);
                    if (TotalRecord < TotalValue)
                    {
                        EndRecord = TotalRecord;
                    }
                    else
                    {
                        EndRecord = TotalValue;
                    }
                }

                lblStartRecord.Text = Convert.ToString(PageSize * PageNumber - PageSize + 1);
                lblEndRecord.Text = Convert.ToString(EndRecord) + " ";
                lblTotalRecord.Text = TotalRecord.ToString();



            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DropDownListChosen.DropDownListChosen ddlSender = sender as DropDownListChosen.DropDownListChosen;

                if (ddlSender != null)
                {
                    int chkSelectedPage = Convert.ToInt32(ddlSender.SelectedItem.ToString());

                    if (ddlSender.ID == "DropDownListPageNo")
                    {
                        ViewState["PageNumberFlagID"] = 1;

                        grdContractDocuments.PageIndex = chkSelectedPage - 1;
                        BindContractDocuments_Paging();
                    }

                    SetShowingRecords();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void rptDocmentVersionView_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (ViewState["AssignedUserID"] != null)
                {
                    int userID = Convert.ToInt32(ViewState["AssignedUserID"]);
                    // var AllinOneDocumentList=null;
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                    if (e.CommandName.Equals("View"))
                    {
                        if (e.CommandArgument != null)
                        {
                            long fileID = Convert.ToInt64(e.CommandArgument);
                            if (fileID != 0)
                                ViewContractDocument(fileID, userID);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                // cvDuplicateEntry.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        protected void rptDocmentVersionView_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);

                LinkButton lblDocumentVersionView = (LinkButton)e.Item.FindControl("lblDocumentVersionView");
                scriptManager.RegisterAsyncPostBackControl(lblDocumentVersionView);
            }
        }

        protected void grdTaskContractDocuments_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandArgument != null)
                {
                    if (e.CommandName.Equals("DownloadContDoc"))
                    {
                        DownloadContractDocument(Convert.ToInt32(e.CommandArgument));
                    }
                    else if (e.CommandName.Equals("ViewContOrder"))
                    {
                        if (ViewState["AssignedUserID"] != null)
                        {
                            int userID = Convert.ToInt32(ViewState["AssignedUserID"]);

                            rptDocmentVersionView.DataSource = null;
                            rptDocmentVersionView.DataBind();

                            ViewContractDocument(Convert.ToInt64(e.CommandArgument), userID);

                            //var fileRecord = ContractDocumentManagement.GetContractDocumentByID(Convert.ToInt64(e.CommandArgument));

                            //if (fileRecord != null)
                            //{
                            //    string filePath = Path.Combine(Server.MapPath(fileRecord.FilePath), fileRecord.FileKey + Path.GetExtension(fileRecord.FileName));
                            //    if (fileRecord.FilePath != null && File.Exists(filePath))
                            //    {
                            //        string Folder = "~/TempFiles";
                            //        string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                            //        string DateFolder = Folder + "/" + File;

                            //        string extension = System.IO.Path.GetExtension(filePath);

                            //        Directory.CreateDirectory(Server.MapPath(DateFolder));

                            //        if (!Directory.Exists(DateFolder))
                            //        {
                            //            Directory.CreateDirectory(Server.MapPath(DateFolder));
                            //        }

                            //        string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

                            //        string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                            //        string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                            //        string FileName = DateFolder + "/" + User + "" + extension;

                            //        FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                            //        BinaryWriter bw = new BinaryWriter(fs);
                            //        bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                            //        bw.Close();
                            //        DocumentPath = FileName;
                            //        DocumentPath = DocumentPath.Substring(2, DocumentPath.Length - 2);
                            //        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenDocviewer('" + DocumentPath + "');", true);
                            //        lblMessage.Text = "";
                            //    }
                            //    else
                            //    {
                            //        lblMessage.Text = "There is no file to preview";
                            //        //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReview();", true);
                            //    }
                            //}
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvContractDocument.IsValid = false;
                cvContractDocument.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        public void DownloadContractDocument(long contFileID)
        {
            try
            {
                if (ViewState["CustomerID"] != null && ViewState["AssignedUserID"] != null)
                {
                    int customerID = -1;
                    customerID = Convert.ToInt32(ViewState["CustomerID"]);

                    int userID= Convert.ToInt32(ViewState["AssignedUserID"]);
                    
                    var file = ContractDocumentManagement.GetContractDocumentByID(contFileID);

                    if (file != null)
                    {
                        if (file.FilePath != null)
                        {
                            string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                            if (filePath != null && File.Exists(filePath))
                            {
                                Response.Buffer = true;
                                Response.Clear();
                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.ContentType = "application/octet-stream";
                                Response.AddHeader("content-disposition", "attachment; filename=" + DocumentManagement.MakeValidFileName(file.FileName));
                                if (file.EnType == "M")
                                {
                                    Response.BinaryWrite(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                                }
                                else
                                {
                                    Response.BinaryWrite(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                                }
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                                ContractManagement.CreateAuditLog("CT", Convert.ToInt32(ViewState["ContractInstanceID"]), "Cont_tbl_FileData", "Download", customerID, userID, "Contract Document Downloaded", true, Convert.ToInt32(ViewState["ContractInstanceID"]));
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvContractDocument.IsValid = false;
                cvContractDocument.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        public void ViewContractDocument(long contFileID, int userID)
        {
            try
            {
                var fileRecord = ContractDocumentManagement.GetContractDocumentByID(contFileID);

                if (fileRecord != null)
                {
                    string filePath = Path.Combine(Server.MapPath(fileRecord.FilePath), fileRecord.FileKey + Path.GetExtension(fileRecord.FileName));

                    if (fileRecord.FilePath != null && File.Exists(filePath))
                    {
                        string Folder = "~/TempFiles";
                        string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                        string DateFolder = Folder + "/" + File;

                        string extension = System.IO.Path.GetExtension(filePath);

                        Directory.CreateDirectory(Server.MapPath(DateFolder));

                        if (!Directory.Exists(DateFolder))
                        {
                            Directory.CreateDirectory(Server.MapPath(DateFolder));
                        }

                        string customerID = Convert.ToString(UserManagement.GetByID(userID).CustomerID ?? 0); 
                        
                        string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                        string User = userID + "" + customerID + "" + FileDate;

                        string FileName = DateFolder + "/" + User + "" + extension;

                        FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                        BinaryWriter bw = new BinaryWriter(fs);
                        if (fileRecord.EnType == "M")
                        {
                            bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                        }
                        else
                        {
                            bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                        }
                        bw.Close();
                        DocumentPath = FileName;
                        DocumentPath = DocumentPath.Substring(2, DocumentPath.Length - 2);

                        lblMessage.Text = "";

                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenDocviewer('" + DocumentPath + "');", true);
                    }
                    else
                    {
                        lblMessage.Text = "There is no file to preview";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected bool UploadDocuments(long contractID, long docTypeID, HttpFileCollection fileCollection, string fileUploadControlName, long? taskID, long? taskResponseID, string fileTags)
        {
            bool uploadSuccess = false;
            try
            {
                #region Upload Document

                int customerID = -1;
                int userID = -1;

                if (ViewState["CustomerID"] != null)
                {
                    customerID = Convert.ToInt32(ViewState["CustomerID"]);
                    userID = Convert.ToInt32(ViewState["AssignedUserID"]);
                    //customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                    string directoryPath = string.Empty;
                    string fileName = string.Empty;

                    Cont_tbl_FileData objContDoc = new Cont_tbl_FileData()
                    {
                        ContractID = contractID,
                        DocTypeID = docTypeID,

                        TaskID = taskID,
                        TaskResponseID = taskResponseID,

                        CustomerID = customerID,
                        CreatedBy = userID,
                        CreatedOn = DateTime.Now,
                        IsDeleted = false,
                    };

                    if (fileCollection.Count > 0)
                    {
                        List<KeyValuePair<string, Byte[]>> fileList = new List<KeyValuePair<string, Byte[]>>();

                        if (contractID > 0)
                        {
                            for (int i = 0; i < fileCollection.Count; i++)
                            {
                                HttpPostedFile uploadedFile = fileCollection[i];

                                if (uploadedFile.ContentLength > 0)
                                {
                                    string[] keys1 = fileCollection.Keys[i].Split('$');

                                    if (keys1[keys1.Count() - 1].Equals(fileUploadControlName))
                                    {
                                        fileName = uploadedFile.FileName;
                                    }

                                    objContDoc.FileName = fileName;

                                    //Get Document Version
                                    var contractDocVersion = ContractDocumentManagement.ExistsContractDocumentReturnVersion(objContDoc);

                                    contractDocVersion++;
                                    objContDoc.Version = contractDocVersion + ".0";

                                    if (taskID == null && taskResponseID == null)
                                        directoryPath = Server.MapPath("~/ContractDocuments/" + customerID + "/" + Convert.ToInt32(contractID) + "/" + docTypeID + "/" + objContDoc.Version);
                                    else if (taskID != null && taskResponseID == null)
                                        directoryPath = Server.MapPath("~/ContractDocuments/" + customerID + "/" + Convert.ToInt32(contractID) + "/" + taskID + "/" + docTypeID + "/" + objContDoc.Version);
                                    else if (taskID != null && taskResponseID != null)
                                        directoryPath = Server.MapPath("~/ContractDocuments/" + customerID + "/" + Convert.ToInt32(contractID) + "/" + taskID + "/" + taskResponseID + "/" + docTypeID + "/" + objContDoc.Version);

                                    if (!Directory.Exists(directoryPath))
                                        Directory.CreateDirectory(directoryPath);

                                    Guid fileKey1 = Guid.NewGuid();
                                    string finalPath1 = Path.Combine(directoryPath, fileKey1 + Path.GetExtension(uploadedFile.FileName));
                                    Stream fs = uploadedFile.InputStream;
                                    BinaryReader br = new BinaryReader(fs);
                                    Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                                    fileList.Add(new KeyValuePair<string, Byte[]>(finalPath1, bytes));

                                    objContDoc.FilePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                    objContDoc.FileKey = fileKey1.ToString();
                                    objContDoc.VersionDate = DateTime.Now;
                                    objContDoc.CreatedOn = DateTime.Now;
                                    objContDoc.FileSize = uploadedFile.ContentLength;
                                    DocumentManagement.Contract_SaveDocFiles(fileList);
                                    long newFileID = ContractDocumentManagement.CreateContractDocumentMapping(objContDoc);

                                    uploadSuccess = true;

                                    //if (string.IsNullOrEmpty(fileTags))
                                    //    fileTags = fileName;

                                    if (newFileID > 0 & !string.IsNullOrEmpty(fileTags))
                                    {
                                        string[] arrFileTags = fileTags.Trim().Split(',');

                                        if (arrFileTags.Length > 0)
                                        {
                                            List<Cont_tbl_FileDataTagsMapping> lstFileTagMapping = new List<Cont_tbl_FileDataTagsMapping>();

                                            for (int j = 0; j < arrFileTags.Length; j++)
                                            {
                                                Cont_tbl_FileDataTagsMapping objFileTagMapping = new Cont_tbl_FileDataTagsMapping()
                                                {
                                                    FileID = newFileID,
                                                    FileTag = arrFileTags[j].Trim(),
                                                    IsActive = true,
                                                    CreatedBy = userID,
                                                    CreatedOn = DateTime.Now,
                                                    UpdatedBy = userID,
                                                    UpdatedOn = DateTime.Now,
                                                };

                                                lstFileTagMapping.Add(objFileTagMapping);
                                            }

                                            if (lstFileTagMapping.Count > 0)
                                            {
                                                ContractDocumentManagement.CreateUpdate_FileTagsMapping(lstFileTagMapping);
                                            }
                                        }
                                    }

                                    fileList.Clear();
                                }

                            }//End For Each
                        }
                    }
                }

                #endregion

                return uploadSuccess;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return uploadSuccess;
            }
        }
        #endregion

        public void BindTaskResponses(long contractID, long taskID, int userID, int roleID)
        {
            try
            {
                List <long?> StatusID = new List<long?>() { 12,15};
                Nullable<long> Status = 13;
                List<Cont_SP_GetTaskResponses_All_Result> lstTaskResponses = new List<Cont_SP_GetTaskResponses_All_Result>();

                lstTaskResponses = ContractTaskManagement.GetTaskResponses(contractID, taskID);

                lstTaskResponses = lstTaskResponses.OrderBy(row => row.StatusChangeOn).ToList();

                if ((lstTaskResponses.Where(row =>row.StatusID==Status).ToList()).Count > 0)
                {
                    ddlStatus.Enabled = false;
                    tbxTaskResComment.Enabled = false;
                    fuTaskResponseDocUpload.Enabled = false;
                    btnSaveTaskResponse.Enabled = false;
                }
                else if ((lstTaskResponses.Where(row => StatusID.Contains(row.StatusID)).ToList()).Count > 0)
                {
                    if (lstTaskResponses.Count > 0)
                    {
                        foreach(var item in lstTaskResponses)
                        {
                            if (item.UserID == userID)
                            {
                                ddlStatus.Enabled = true;
                                tbxTaskResComment.Enabled = true;
                                fuTaskResponseDocUpload.Enabled = true;
                                btnSaveTaskResponse.Enabled = true;
                            }
                            else
                            {
                                ddlStatus.Enabled = false;
                                tbxTaskResComment.Enabled = false;
                                fuTaskResponseDocUpload.Enabled = false;
                                btnSaveTaskResponse.Enabled = false;
                            }
                        }
                    }
                }
                if (lstTaskResponses != null && lstTaskResponses.Count > 0)
                {
                    lstTaskResponses = lstTaskResponses.Where(row => row.ID != 0 && row.RoleID == roleID)
                        .OrderBy(row => row.UserID).ThenByDescending(entry => entry.StatusChangeOn).ToList();
                }
               
                grdTaskResponseLog.DataSource = lstTaskResponses;
                grdTaskResponseLog.DataBind();

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvTaskResponse.IsValid = false;
                cvTaskResponse.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        //public void BindTaskResponses(int taskID)
        //{
        //    try
        //    {
        //        List<tbl_TaskResponse> lstTaskResponses = new List<tbl_TaskResponse>();

        //        lstTaskResponses = LitigationTaskManagement.GetTaskResponseDetails(taskID);

        //        if (lstTaskResponses != null && lstTaskResponses.Count > 0)
        //        {
        //            lstTaskResponses = lstTaskResponses.OrderByDescending(entry => entry.UpdatedOn).ThenByDescending(entry => entry.CreatedOn).ToList();
        //        }

        //        grdTaskResponseLog.DataSource = lstTaskResponses;
        //        grdTaskResponseLog.DataBind();
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvTaskResponse.IsValid = false;
        //        cvTaskResponse.ErrorMessage = "Server Error Occured. Please try again.";
        //    }
        //}

        public void clearTaskResponseControls()
        {
            try
            {
                ddlStatus.ClearSelection();
                tbxTaskResComment.Text = "";
                fuTaskResponseDocUpload.Attributes.Clear();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void enableDisableTaskControls(bool flag)
        {
            try
            {
                pnlTaskDetail.Enabled = flag;
                pnlTaskResponse.Enabled = flag;

                //Hide Action Delete Button
                for (int count = 0; count < grdTaskResponseLog.Rows.Count; count++)
                {
                    LinkButton lnkBtnDeleteTaskResponse = (LinkButton)grdTaskResponseLog.Rows[count].FindControl("lnkBtnDeleteTaskResponse");
                    if (lnkBtnDeleteTaskResponse != null)
                    {
                        if (ViewState["taskStatus"] != null)
                        {
                            if (Convert.ToInt32(ViewState["taskStatus"]) == 14) //Closed
                                lnkBtnDeleteTaskResponse.Visible = false;
                            else if (ShowDownLoadIcon)
                            {
                                if (DocDownloadDeleteResult != "0")
                                {
                                    lnkBtnDeleteTaskResponse.Visible = true;
                                }
                                else
                                {
                                    lnkBtnDeleteTaskResponse.Visible = false;
                                }
                            }
                            else
                                lnkBtnDeleteTaskResponse.Visible = true;
                        }
                    }
                }                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnSaveTaskResponse_Click(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["CustomerID"] != null && ViewState["ContractInstanceID"] != null && ViewState["TaskID"] != null && ViewState["AssignedUserID"] != null && ViewState["RoleID"] != null)
                {
                    int customerID = -1;
                    customerID = Convert.ToInt32(ViewState["CustomerID"]);

                    bool validateData = false;
                    bool saveSuccess = false;
                    bool isBlankFile = false;
                    long newResponseID = 0;
                    long taskID = 0;
                    long contractID = 0;
                    contractID = Convert.ToInt64(ViewState["ContractInstanceID"]);
                    taskID = Convert.ToInt64(ViewState["TaskID"]);

                    int roleID = 4;
                    if (ViewState["TaskType"] != null)
                    {
                        if (Convert.ToInt32(ViewState["TaskType"]) != 1)
                            roleID = Convert.ToInt32(ViewState["TaskType"]);
                    }

                    int userID = 0;
                    //userID = AuthenticationHelper.UserID;
                    userID = Convert.ToInt32(ViewState["AssignedUserID"]);

                    HttpFileCollection fileCollection = Request.Files;
                    if (fileCollection.Count > 0)
                    {
                        string[] InvalidFileTypes = { "exe", "bat", "dll", "css", "js", "jsp", "php5", "pht", "asa", "cer", "asax", "swf", "xap", "aspx", "asp", "php", "reg", "rdp", };
                        for (int i = 0; i < fileCollection.Count; i++)
                        {
                            HttpPostedFile uploadfile = null;
                            uploadfile = fileCollection[i];
                            int filelength = uploadfile.ContentLength;
                            string fileName = Path.GetFileName(uploadfile.FileName);
                            string ext = System.IO.Path.GetExtension(uploadfile.FileName);
                            if (!string.IsNullOrEmpty(fileName))
                            {
                                if (filelength == 0)
                                {
                                    isBlankFile = true;
                                    break;
                                }
                                else if (ext == "")
                                {
                                    isBlankFile = true;
                                    break;
                                }
                                else
                                {
                                    if (ext != "")
                                    {
                                        for (int j = 0; j < InvalidFileTypes.Length; j++)
                                        {
                                            if (ext == "." + InvalidFileTypes[j])
                                            {
                                                isBlankFile = true;
                                                break;
                                            }
                                        }
                                    }

                                }
                            }
                        }
                    }

                    if (contractID != 0 && taskID != 0 && userID != 0 && roleID != 0)
                    {
                        if (ddlStatus.SelectedValue != "" && ddlStatus.SelectedValue != "-1" && ddlStatus.SelectedValue != "0")
                        {
                            if (tbxTaskResComment.Text != "")
                            {
                                if (isBlankFile == false)
                                {
                                    validateData = true;
                                 
                                }
                                else
                                {
                                    cvTaskResponse.IsValid = false;
                                    cvTaskResponse.ErrorMessage = "Invalid file error. System does not support uploaded file.Please upload another file.";
                                    return;
                                }
                            }
                            else
                            {
                                cvTaskResponse.IsValid = false;
                                cvTaskResponse.ErrorMessage = "Required Comment";
                                return;
                            }
                        }
                        else
                        {
                            cvTaskResponse.IsValid = false;
                            cvTaskResponse.ErrorMessage = "Select Status";
                            return;
                        }
                    }

                    if (validateData)
                    {
                        Cont_tbl_TaskTransaction newRecord = new Cont_tbl_TaskTransaction()
                        {
                            ContractID = contractID,
                            TaskID = taskID,
                            StatusID = Convert.ToInt32(ddlStatus.SelectedValue),
                            Comment = tbxTaskResComment.Text.Trim(),
                            UserID = userID,
                            RoleID = roleID,
                            StatusChangeOn = DateTime.Now,
                            IsActive = true,
                            CreatedBy = userID,
                            CreatedOn = DateTime.Now,
                        };

                        newResponseID = ContractTaskManagement.CreateTaskTransactionLog(newRecord);

                        if (newResponseID > 0)
                            saveSuccess = true;
                    }

                    if (saveSuccess)
                    {
                        //Save Notice Response Uploaded Documents
                        #region Upload Document

                        if (fuTaskResponseDocUpload.HasFiles)
                        {
                            UploadDocuments(contractID, 0, Request.Files, "fuTaskResponseDocUpload", taskID, newResponseID, "");
                            //saveSuccess = UploadDocuments(contractID, 0, Request.Files, "fuTaskResponseDocUpload", taskID, newResponseID, "");
                        }

                        #endregion                        

                        if (saveSuccess)
                        {
                            //Update Contract Status
                            if (ViewState["TaskType"] != null)
                            {
                                int taskType = Convert.ToInt32(ViewState["TaskType"]);
                                string statusName = "Submitted";

                                int newStatusID = 0;

                                if (taskType == 4) //Review Task
                                {
                                    newStatusID = 3; //Review Completed
                                    roleID = 4;
                                }
                                else if (taskType == 6) //Approval Task
                                {
                                    newStatusID = 5;  //Approval Completed 
                                    roleID = 6;
                                }

                                if (newStatusID != 0)
                                    saveSuccess = ContractTaskManagement.UpdateContractStatus(customerID, contractID, taskID, statusName, newStatusID);
                                
                                clearTaskResponseControls();

                                cvTaskResponse.IsValid = false;
                                cvTaskResponse.ErrorMessage = "Task Response Saved Successfully.";
                                vsTaskResponse.CssClass = "alert alert-success";

                                #region Sent Email to Contract Owner and Task Creater

                                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                {
                                  
                                    var query1 = (from row in entities.Cont_tbl_TaskTransaction
                                                 where row.ContractID == contractID && row.TaskID == taskID
                                                 select row).FirstOrDefault();

                                    var query = (from row in entities.Cont_tbl_ContractInstance
                                                 where row.ID == contractID
                                                 select row).FirstOrDefault();


                                    string emailTemplate = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_Contract_Assign_Owner;
                                    string accessURL = string.Empty;
                                    URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(AuthenticationHelper.CustomerID));
                                    if (Urloutput != null)
                                    {
                                        accessURL = Urloutput.URL;
                                    }
                                    else
                                    {
                                        accessURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                                    }
                                    //string accessURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                                    string customerName = CustomerManagement.CustomerGetByIDName(customerID);
                                    var userquery = UserManagement.GetByID(userID);
                                    var userquery1 = UserManagement.GetByID(query1.CreatedBy);
                                    string username = string.Empty;
                                    string senderEmail = string.Empty;
                                    string senderEmail1 = string.Empty;
                                    if (userquery != null)
                                    {
                                        username = string.Format("{0} {1}", userquery.FirstName, userquery.LastName);

                                        senderEmail = userquery.Email;
                                        senderEmail1 = userquery1.Email;

                                    }
                                    string message = emailTemplate
                                                    .Replace("@User", username)
                                                    .Replace("@ContractNo", query.ContractNo)
                                                    .Replace("@Title", query1.Comment)
                                                    .Replace("@AccessURL", accessURL)
                                                    .Replace("@From", "Team " + customerName)
                                                    .Replace("@PortalURL", accessURL);

                                    EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(new String[] { senderEmail, senderEmail1 }), null, null, "Contract Notification-Task Status", message);

                                }
                                #endregion
                            }
                        }

                        //Re-Bind Responses Log Details
                        BindTaskResponses(contractID, taskID, userID, roleID);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnClearTaskResponse_Click(object sender, EventArgs e)
        {
            try
            {
                clearTaskResponseControls();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdTaskResponseLog_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (ViewState["ContractInstanceID"] != null && ViewState["TaskID"] != null
                    && ViewState["AssignedUserID"] != null && ViewState["RoleID"] != null)
                {
                    long contractID = Convert.ToInt64(ViewState["ContractInstanceID"]);
                    long taskID = Convert.ToInt64(ViewState["TaskID"]);
                    int userID = Convert.ToInt32(ViewState["AssignedUserID"]);
                    int roleID = Convert.ToInt32(ViewState["RoleID"]);

                    grdTaskResponseLog.PageIndex = e.NewPageIndex;
                    BindTaskResponses(contractID, taskID, userID, roleID);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdTaskResponseLog_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandArgument != null)
                {
                    if (e.CommandName.Equals("DownloadTaskResponseDoc"))
                    {
                        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                        long taskResponseID = Convert.ToInt64(commandArgs[0]);
                        long taskID = Convert.ToInt64(commandArgs[1]);
                        long contractID = Convert.ToInt64(commandArgs[2]);

                        if (taskResponseID != 0 && taskID != 0 && contractID != 0)
                        {
                            var lstTaskResponseDocument = ContractTaskManagement.GetTaskResponseDocuments(contractID, taskID, taskResponseID);

                            if (lstTaskResponseDocument.Count > 0)
                            {
                                using (ZipFile responseDocZip = new ZipFile())
                                {
                                    int i = 0;
                                    foreach (var file in lstTaskResponseDocument)
                                    {
                                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            //string[] filename = file.Name.Split('.');
                                            //string str = filename[0] + i + "." + filename[1];

                                            int idx = file.FileName.LastIndexOf('.');
                                            string str = file.FileName.Substring(0, idx) + "_" + file.Version + "." + file.FileName.Substring(idx + 1);

                                            if (!responseDocZip.ContainsEntry(file.DocTypeID + "/" + str))
                                            {
                                                if (file.EnType == "M")
                                                {
                                                    responseDocZip.AddEntry(file.DocTypeID + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                else
                                                {
                                                    responseDocZip.AddEntry(file.DocTypeID + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                            }
                                            i++;
                                        }
                                    }

                                    var zipMs = new MemoryStream();
                                    responseDocZip.Save(zipMs);
                                    zipMs.Position = 0;
                                    byte[] Filedata = zipMs.ToArray();
                                    Response.Buffer = true;
                                    Response.ClearContent();
                                    Response.ClearHeaders();
                                    Response.Clear();
                                    Response.ContentType = "application/zip";
                                    Response.AddHeader("content-disposition", "attachment; filename=TaskResponseDocuments-" + DateTime.Now.ToString("dd-MM-yyyy") + ".zip");
                                    Response.BinaryWrite(Filedata);
                                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                                }
                            }
                            else
                            {
                                cvTaskResponse.IsValid = false;
                                cvTaskResponse.ErrorMessage = "No Document Available for Download.";
                                return;
                            }
                        }
                    }
                    else if (e.CommandName.Equals("ViewTaskResponseDoc"))
                    {
                        if (ViewState["AssignedUserID"] != null)
                        {  
                            int userID = Convert.ToInt32(ViewState["AssignedUserID"]);                            

                            string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                        
                        long taskResponseID = Convert.ToInt64(commandArgs[0]);
                        long taskID = Convert.ToInt64(commandArgs[1]);
                        long contractID = Convert.ToInt64(commandArgs[2]);

                            if (taskResponseID != 0 && taskID != 0 && contractID != 0)
                            {
                                var lstTaskDocument = ContractTaskManagement.GetTaskResponseDocuments(contractID, taskID, taskResponseID);

                                if (lstTaskDocument != null)
                                {
                                    List<Cont_tbl_FileData> entitiesData = lstTaskDocument.Where(entry => entry.Version != null).ToList();

                                    if (lstTaskDocument.Where(entry => entry.Version == null).ToList().Count > 0)
                                    {
                                        Cont_tbl_FileData entityData = new Cont_tbl_FileData();
                                        entityData.Version = "1.0";
                                        entityData.ContractID = contractID;
                                        entityData.TaskID = taskID;
                                        entityData.TaskResponseID = taskResponseID;
                                        entitiesData.Add(entityData);
                                    }

                                    if (entitiesData.Count > 0)
                                    {
                                        rptDocmentVersionView.DataSource = entitiesData.OrderBy(entry => entry.Version);
                                        rptDocmentVersionView.DataBind();

                                        ViewContractDocument(entitiesData[0].ID, userID);

                                        //foreach (var file in entitiesData)
                                        //{
                                        //    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                        //    if (file.FilePath != null && File.Exists(filePath))
                                        //    {
                                        //        string Folder = "~/TempFiles";
                                        //        string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                        //        string DateFolder = Folder + "/" + File;

                                        //        string extension = System.IO.Path.GetExtension(filePath);

                                        //        Directory.CreateDirectory(Server.MapPath(DateFolder));

                                        //        if (!Directory.Exists(DateFolder))
                                        //        {
                                        //            Directory.CreateDirectory(Server.MapPath(DateFolder));
                                        //        }

                                        //        string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

                                        //        string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                        //        string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                        //        string FileName = DateFolder + "/" + User + "" + extension;

                                        //        FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                        //        BinaryWriter bw = new BinaryWriter(fs);
                                        //        bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        //        bw.Close();
                                        //        DocumentPath = FileName;

                                        //        DocumentPath = DocumentPath.Substring(2, DocumentPath.Length - 2);

                                        //        lblMessage.Text = "";

                                        //        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenDocviewer('" + DocumentPath + "');", true);
                                        //    }
                                        //    else
                                        //    {
                                        //        lblMessage.Text = "There is no document available to preview";
                                        //    }
                                        //    break;
                                        //}
                                        //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);
                                    }
                                }
                            }
                        }
                    }
                    else if (e.CommandName.Equals("DeleteTaskResponse"))
                    {
                        //Bind Task Responses
                        if (ViewState["ContractInstanceID"] != null && ViewState["TaskID"] != null && ViewState["AssignedUserID"] != null && ViewState["RoleID"] != null)
                        {
                            long contractID = Convert.ToInt64(ViewState["ContractInstanceID"]);
                            long taskID = Convert.ToInt64(ViewState["TaskID"]);
                            int userID = Convert.ToInt32(ViewState["AssignedUserID"]);
                            int roleID = Convert.ToInt32(ViewState["RoleID"]);

                            long taskResponseID = Convert.ToInt64(e.CommandArgument);

                            bool deleteSuccess = false;

                            if (taskResponseID != 0)
                            {
                                //Delete Response with Documents
                                deleteSuccess = ContractTaskManagement.DeleteTaskTransactionLog(contractID, taskID, taskResponseID, userID);
                            }

                            if (deleteSuccess)
                            {
                                BindTaskResponses(contractID, taskID, userID, roleID);

                                cvTaskResponse.IsValid = false;
                                cvTaskResponse.ErrorMessage = "Response Deleted Successfully.";
                            }
                            else
                            {
                                cvTaskResponse.IsValid = false;
                                cvTaskResponse.ErrorMessage = "Something went wrong, Please try again";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvTaskResponse.IsValid = false;
                cvTaskResponse.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void grdTaskResponseLog_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            LinkButton lnkBtnDownloadTaskResDoc = (LinkButton)e.Row.FindControl("lnkBtnDownloadTaskResDoc");

            if (lnkBtnDownloadTaskResDoc != null)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lnkBtnDownloadTaskResDoc);
            }

            LinkButton lnkBtnDeleteTaskResponse = (LinkButton)e.Row.FindControl("lnkBtnDeleteTaskResponse");
            if (lnkBtnDeleteTaskResponse != null)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lnkBtnDeleteTaskResponse);

                if (ViewState["taskStatus"] != null)
                {
                    if (Convert.ToInt32(ViewState["taskStatus"]) == 14) //Closed
                        lnkBtnDeleteTaskResponse.Visible = false;
                    else
                        lnkBtnDeleteTaskResponse.Visible = true;
                }
            }
        }

        public string ShowTaskResponseDocCount(long contractID, long taskID, long taskResponseID)
        {
            try
            {
                var docCount = ContractTaskManagement.GetTaskResponseDocuments(contractID, taskID, taskResponseID).Count;

                if (docCount == 0)
                    return "No Documents";
                else if (docCount == 1)
                    return "1 File";
                else if (docCount > 1)
                    return docCount + " Files";
                else
                    return "";
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }

        public string ShowTaskDocCount(long contractID, long taskID)
        {
            try
            {
                string returnStr = string.Empty;
                int customerID = -1;
                
                if (ViewState["CustomerID"] != null)
                {
                    if (taskID != 0 && contractID != 0)
                    {
                        var lstDocs = ContractTaskManagement.GetTaskDocuments(customerID, contractID, 0, taskID, null); //0--DocTypeID--Task

                        int docCount = lstDocs.Count;

                        lstDocs.Clear(); lstDocs = null;

                        if (docCount == 0)
                            returnStr= "No Document";
                        else if (docCount == 1)
                            returnStr= "1 Document";
                        else if (docCount > 1)
                            returnStr= docCount + " Documents";
                    }
                }

                return returnStr;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }

        public void BindTaskDocuments_All(GridView grdtoBindDocuments, long contractID, long taskID)
        {
            try
            {
                if (ViewState["CustomerID"] != null)
                {
                    int customerID = Convert.ToInt32(ViewState["CustomerID"]);

                    if (contractID != 0 && taskID != 0)
                    {
                        var lstTaskDocs = ContractTaskManagement.GetTaskDocuments_All(customerID, contractID, taskID);

                        grdtoBindDocuments.DataSource = lstTaskDocs;
                        grdtoBindDocuments.DataBind();

                        lstTaskDocs.Clear();
                        lstTaskDocs = null;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void ShowTaskDetails(long contractID, long taskID, int userID, int roleID)
        {
            try
            {
                var taskDetail = ContractTaskManagement.GetTaskDetailsByUserRoleWise(contractID, taskID, userID, roleID);
                //var taskDetail = ContractTaskManagement.GetTaskDetailsByTaskID(contractID, taskID); //, int customerID

                if (taskDetail != null)
                {
                    ViewState["TaskType"] = taskDetail.TaskType;

                    if (taskDetail.TaskType == 6)
                        roleID = 6;

                    ViewState["RoleID"] = roleID;

                    lblTaskTitle.Text = taskDetail.TaskTitle;
                    lblTaskAssignOn.Text = taskDetail.AssignOn.ToString("dd-MM-yyyy");
                    //ViewState["TaskAssignOn"] = taskDetail.CreatedOn.ToString("dd-MM-yyyy");
                    ViewState["TaskTitle"] = taskDetail.TaskTitle;
                    ViewState["TaskInstanceID"] = taskDetail.TaskID;
                    lblTaskDueDate.Text = taskDetail.DueDate.ToString("dd-MM-yyyy");
                    lblTaskDesc.Text = taskDetail.TaskDesc;
                    lblTaskRemark.Text = taskDetail.Remark;

                    lblAssignBy.Text = ContractUserManagement.GetUserNameByUserID(taskDetail.CreatedBy);

                    if (taskDetail.PriorityID != 0)
                    {
                        if (taskDetail.PriorityID == 1)
                            lblPriority.Text = "High";
                        else if (taskDetail.PriorityID == 2)
                            lblPriority.Text = "Medium";
                        else if (taskDetail.PriorityID == 3)
                            lblPriority.Text = "Low";
                    }

                    BindTaskDocuments_All(grdTaskContractDocuments, contractID, taskID);

                    if (taskDetail.TaskStatusID != null)
                        ViewState["taskStatus"] = Convert.ToInt64(taskDetail.TaskStatusID);
                                      
                    BindTaskStatusList(true, false);

                    //Bind Task Responses
                    if (ViewState["AssignedUserID"] != null)
                        BindTaskResponses(contractID, taskID, Convert.ToInt32(ViewState["AssignedUserID"]), roleID);


                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindTaskStatusList(bool isForTask, bool isVisibleToUser)
        {
            try
            {
                ddlStatus.DataSource = null;
                ddlStatus.DataBind();
                ddlStatus.ClearSelection();

                ddlStatus.DataTextField = "StatusName";
                ddlStatus.DataValueField = "ID";

                var statusList = ContractTaskManagement.GetStatusList_All(isForTask, isVisibleToUser);

                //List<Cont_tbl_StatusMaster> allowedStatusList = null;

                //List<int> finalStatusIDs = ContractTaskManagement.GetFinalStatusByInitialID(statusID, roleID);
                //allowedStatusList = statusList.Where(entry => finalStatusIDs.Contains((int)entry.ID)).OrderBy(entry => entry.StatusName).ToList();

                ddlStatus.DataSource = statusList;
                ddlStatus.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvTaskResponse.IsValid = false;
                cvTaskResponse.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void btnDownloadTaskDoc_Click(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["TaskID"] != null && ViewState["NoticeCaseInstanceID"] != null)
                {
                    long taskID = Convert.ToInt32(ViewState["TaskID"]);
                    long noticeCaseInstanceID = Convert.ToInt32(ViewState["NoticeCaseInstanceID"]);

                    var lstTaskDocument = LitigationTaskManagement.GetTaskDocuments(taskID, noticeCaseInstanceID);

                    if (lstTaskDocument.Count > 0)
                    {
                        using (ZipFile responseDocZip = new ZipFile())
                        {
                            int i = 0;
                            foreach (var file in lstTaskDocument)
                            {
                                string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                if (file.FilePath != null && File.Exists(filePath))
                                {
                                    //string[] filename = file.Name.Split('.');
                                    //string str = filename[0] + i + "." + filename[1];

                                    int idx = file.FileName.LastIndexOf('.');
                                    string str = file.FileName.Substring(0, idx) + "_" + file.Version + "." + file.FileName.Substring(idx + 1);

                                    if (!responseDocZip.ContainsEntry(file.CreatedByText + "/" + str))
                                    {
                                        if (file.EnType == "M")
                                        {
                                            responseDocZip.AddEntry(file.CreatedByText + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        else
                                        {
                                            responseDocZip.AddEntry(file.CreatedByText + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                    }
                                    i++;
                                }
                            }

                            var zipMs = new MemoryStream();
                            responseDocZip.Save(zipMs);
                            zipMs.Position = 0;
                            byte[] Filedata = zipMs.ToArray();
                            Response.Buffer = true;
                            Response.ClearContent();
                            Response.ClearHeaders();
                            Response.Clear();
                            Response.ContentType = "application/zip";
                            Response.AddHeader("content-disposition", "attachment; filename=TaskDocuments-" + DateTime.Now.ToString("dd-MM-yyyy") + ".zip");
                            Response.BinaryWrite(Filedata);
                            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                        }
                    }
                    else
                    {
                        cvTaskResponse.IsValid = false;
                        cvTaskResponse.ErrorMessage = "No Document Available for Download.";
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindTransactions()
        {
            try
            {
                Session["TotalRows"] = null;
                if (ViewState["ContractInstanceID"] != null)
                {
                    int customerID = -1;
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                    //long contractID = Convert.ToInt32(ViewState["contractID"]);
                    long contractID = 0;
                    contractID = Convert.ToInt64(ViewState["ContractInstanceID"]);
                    int contractInstanceID = Convert.ToInt32(ViewState["ContractInstanceID"]);
                    DateTime assigndate = Convert.ToDateTime(ViewState["TaskAssignOn"]);
                    string tasktitle = Convert.ToString(ViewState["TaskTitle"]);
                    int TaskID = Convert.ToInt32(ViewState["TaskInstanceID"]);
                    //var lstAuditLogs = ContractManagement.GetContractAuditLogs_All(customerID, contractID);
                    var lstAuditLogs = ContractManagement.GetTaskAuditLogs_All(customerID, contractID, assigndate,tasktitle, TaskID);
                    //var lstAuditLogs = ContractManagement.GetContractDetailsByContractID(customerID, contractID);

                    if (lstAuditLogs.Count > 0)
                    {
                        grdTransactionHistory.DataSource = lstAuditLogs;
                        grdTransactionHistory.DataBind();
                        Session["TotalRows"] = lstAuditLogs.Count;
                    }
                }
                else
                {
                    grdTransactionHistory.DataSource = null;
                    grdTransactionHistory.DataBind();
                    Session["TotalRows"] = null;
                }
                bindPageNumberNew();
                ShowGridDetail();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvContractPopUp.IsValid = false;
                cvContractPopUp.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdTransactionHistory_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (ViewState["ContractInstanceID"] != null)
                {
                    grdTransactionHistory.PageIndex = e.NewPageIndex;

                    BindTransactions();
                }
                //grdTransactionHistory.PageIndex = e.NewPageIndex;
                //BindTransactions(Convert.ToInt32(ViewState["ContractInstanceID"]));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvContractPopUp.IsValid = false;
                cvContractPopUp.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdTransactionHistory.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);

                BindTransactions();
                //bindPageNumberNew();

                int count = Convert.ToInt32(GetTotalPagesCountNew());
                if (count > 0)
                {
                    int gridindex = grdTransactionHistory.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNoNew.SelectedValue = (chkcindition).ToString();
                }

                ShowGridDetail();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void ShowGridDetail()
        {
            if (Session["TotalRows"] != null)
            {
                int PageSize = 0;
                int PageNumber = 0;

                if (!string.IsNullOrEmpty(ddlPageSize.SelectedItem.Text))
                    PageSize = Convert.ToInt32(ddlPageSize.SelectedItem.Text);

                if (!string.IsNullOrEmpty(DropDownListPageNoNew.SelectedValue))
                    PageNumber = Convert.ToInt32(DropDownListPageNoNew.SelectedValue);

                var EndRecord = 0;
                var TotalRecord = 0;
                var TotalValue = PageSize * PageNumber;

                TotalRecord = Convert.ToInt32(Session["TotalRows"]);
                if (TotalRecord < TotalValue)
                {
                    EndRecord = TotalRecord;
                }
                else
                {
                    EndRecord = TotalValue;
                }

                if (TotalRecord != 0)
                    lblStartRecord.Text = Convert.ToString(PageSize * PageNumber - PageSize + 1);
                else
                    lblStartRecord.Text = "0";

                lblEndRecord.Text = Convert.ToString(EndRecord) + " ";
                lblTotalRecord.Text = TotalRecord.ToString();
            }
            else
            {
                lblStartRecord.Text = "0";
                lblEndRecord.Text = "0 ";
                lblTotalRecord.Text = "0";
            }
        }

        protected void DropDownListPageNoNew_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNoNew.SelectedItem.ToString());

            grdTransactionHistory.PageIndex = chkSelectedPage - 1;
            grdTransactionHistory.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);

            BindTransactions();
            ShowGridDetail();
        }

        private void bindPageNumberNew()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCountNew());

                if (DropDownListPageNoNew.Items.Count > 0)
                {
                    DropDownListPageNoNew.Items.Clear();
                }

                DropDownListPageNoNew.DataTextField = "ID";
                DropDownListPageNoNew.DataValueField = "ID";

                DropDownListPageNoNew.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNoNew.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNoNew.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNoNew.Items.Add("0");
                    DropDownListPageNoNew.SelectedValue = ("0").ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private int GetTotalPagesCountNew()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }


    }
}