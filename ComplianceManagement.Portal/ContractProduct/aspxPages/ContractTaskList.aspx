﻿<%@ Page Title="My Task(s) :: My Workspace" Language="C#" MasterPageFile="~/ContractProduct.Master" AutoEventWireup="true" CodeBehind="ContractTaskList.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.aspxPages.ContractTaskList" %>

<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">
        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);
        window.onunload = function () { };

        $(document).ready(function () {           
            setactivemenu('leftworkspacemenu');           
            fhead('My Workspace/ Task');           
        });

        function ShowDialog(contractID, taskID, userID, roleID, checkSum) {
            
            var modalHeight = screen.height - 200;

            if (modalHeight < 0)
                modalHeight = screen.height;

            //alert("Screen Height-" + screen.height + "\n Modal Height-" + modalHeight);

            $('#divShowDialog').modal('show');
            $('.modal-dialog').css('width', '95%');            
            $('#showdetails').attr('width', '100%');
            $('#showdetails').attr('height', modalHeight + "px");
            $('#showdetails').attr('src', "/ContractProduct/aspxPages/ContractTaskDetailsPage.aspx?A=" + contractID + "&B=" + taskID + "&C=" + userID + "&D=" + roleID + "&Checksum=" + checkSum);
        };

        function reloadTaskList() {
            //window.parent.location.reload(true); self.close();
            $('#divShowDialog').modal('hide');
            document.getElementById('<%= lnkBtn_RebindTask.ClientID %>').click();
            //window.location.href = "/ContractProduct/aspxPages/ContractTaskList.aspx";
        }        
    </script>

    <style>
        .panel-heading {
            background: #ffffff;
        }

            .panel-heading .nav .active, .panel-heading .nav > li.active > a, .panel-heading .nav > li > a:hover {
                color: #1fd9e1;
                background-color: #fff;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row Dashboard-white-widget">
        <div class="dashboard">

            <div class="col-md-12 colpadding0">
                <asp:ValidationSummary ID="vsTaskPage" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                    ValidationGroup="TaskPageValidationGroup" />
                <asp:CustomValidator ID="cvTaskPage" runat="server" EnableClientScript="False"
                    ValidationGroup="TaskPageValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
            </div>

            <div class="col-md-12 colpadding0">
                <div class="col-md-10 colpadding0">
                    <header class="panel-heading tab-bg-primary ">
                        <ul class="nav nav-tabs">
                            <li class="" id="liContract" runat="server">
                                <asp:LinkButton ID="lnkContract" style="display:none;" CausesValidation="false" runat="server" OnClick="lnkContract_Click">Contract</asp:LinkButton>
                            </li>
                            <li class="active" id="liTask" runat="server">
                                <asp:LinkButton ID="lnkTask" style="display:none;"  CausesValidation="false" runat="server">Task</asp:LinkButton>
                            </li>
                            <li class="" id="li1" runat="server">
                        <asp:LinkButton ID="lnkRev" CausesValidation="false" style="display:none;" runat="server" OnClick="lnkRev_Click">My Reviews</asp:LinkButton>
                    </li>
                        </ul>
                    </header>
                </div>
                <%--<div class="col-md-2 colpadding0 text-right">
                     <a class="btn btn-advanceSearch" data-toggle="modal" href="#AdvanceSearch">Advanced Search</a>
                </div>--%>
            </div>
             <div class="clearfix"></div>
            <!--advance search start-->
             <div class="col-md-12 colpadding0">
                        
                       <div class="col-md-2 colpadding0 entrycount" style="margin-top: 5px; width: 20%; margin-right: 10px;">
                            <%--<label for="ddlTaskPriority" class="filter-label">Priority</label>--%>
                            <asp:DropDownListChosen runat="server" ID="ddlTaskPriority" AllowSingleDeselect="false" DisableSearchThreshold="5"
                              DataPlaceHolder="Select Priority"  CssClass="form-control" Width="100%">

                               <asp:ListItem Text="Select Priority" Value="0" ></asp:ListItem>
                                <asp:ListItem Text="High" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Medium" Value="2"></asp:ListItem>
                                <asp:ListItem Text="Low" Value="3"></asp:ListItem>
                            </asp:DropDownListChosen>
                        </div>

                      <div class="col-md-2 colpadding0 entrycount" style="margin-top: 5px; width: 20%; margin-right: 10px;">
                            <%--<label for="ddlTaskStatus" class="filter-label">Status</label>--%>
                            <asp:DropDownListChosen runat="server" ID="ddlTaskStatus" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                DataPlaceHolder="Select Status"  class="form-control" Width="100%">
                                <asp:ListItem Text="Select Status" Value="0" ></asp:ListItem>
                                <asp:ListItem Text="Upcoming" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Submitted for Review" Value="2"></asp:ListItem>
                                <asp:ListItem Text="Submitted for Approval" Value="3"></asp:ListItem>
                                <asp:ListItem Text="Reviewed/Approved" Value="4"></asp:ListItem>
                                <asp:ListItem Text="Overdue" Value="5"></asp:ListItem>
                                <asp:ListItem Text="Closed" Value="6"></asp:ListItem>
                            </asp:DropDownListChosen>
                        </div>

                      <div class="col-md-2 colpadding0 entrycount" style="margin-top: 5px; width: 20%; margin-right: 10px;display:<%=TaskAssigned%>;">
                            <asp:DropDownListChosen runat="server" ID="ddlTaskAssigned" AllowSingleDeselect="false" DisableSearchThreshold="5"
                              DataPlaceHolder="All Task"  CssClass="form-control" Width="100%">
                               <asp:ListItem Text="All Task" Selected="True" Value="0"></asp:ListItem>
                               <asp:ListItem Text="My Task" Value="1" ></asp:ListItem>
                            </asp:DropDownListChosen>
                        </div>

                 <div class="col-md-2 colpadding0" style="margin-top: 5px; width: 20%; margin-right: 5px;">
                     <asp:TextBox runat="server" OnTextChanged="tbxFilter_TextChanged" ID="tbxFilter" Width="100%" AutoPostBack="true" placeholder="Type to Search" CssClass="form-control" />
                 </div>
                 <div class="col-md-2 colpadding0 " style="margin-top: 5px;margin-left: -15px;width: 17%;margin-right: 5px;">
                    
                     <asp:LinkButton ID="lnkBtn_RebindTask" OnClick="lnkBtn_RebindTask_Click" Style="display: none;" runat="server" />
                      <label for="ddlStatus" class="hidden-label">Filter</label>
                         <asp:LinkButton Text="Apply" CssClass="btn btn-primary" runat="server" ID="lnkBtnApplyFilter"
                             OnClick="lnkBtnApplyFilter_Click" OnClientClick="javascript:$('#updateProgress').show()" />

                         <label for="ddlStatus" class="hidden-label">Filter</label>
                         <asp:LinkButton Text="Clear" CssClass="btn btn-primary" runat="server" ID="lnkBtnClearFilter"
                             OnClick="lnkBtnClearFilter_Click" OnClientClick="javascript:$('#updateProgress').show()" />
                             
                   
                   
                 
             </div>
     </div>

             <div class="col-md-12 colpadding0">
                <div class="col-md-3 pl0 form-group d-none">
                    <label for="ddlTypePage" class="filter-label">Move to</label>
                    <asp:DropDownListChosen runat="server" ID="ddlTypePage" AllowSingleDeselect="false" DisableSearchThreshold="5" AutoPostBack="true"
                        DataPlaceHolder="Select Type" class="form-control" Width="100%" OnSelectedIndexChanged="ddlTypePage_SelectedIndexChanged">
                        <asp:ListItem Text="Task" Value="2"></asp:ListItem>
                        <asp:ListItem Text="Contract" Value="1"></asp:ListItem>
                    </asp:DropDownListChosen>
                </div>
            </div>
             <!--advance search end-->
            <div class="clearfix"></div>
                      
            <div class="col-md-12 colpadding0">
                <asp:GridView runat="server" ID="grdTaskActivity" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                    GridLines="None" PageSize="10" AllowPaging="true" CssClass="table" Width="100%" DataKeyNames="TaskID" OnSorting="grdTaskActivity_Sorting" OnRowCreated="grdTaskActivity_RowCreated">
                    <Columns>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr.No." ItemStyle-Width="5%">
                            <ItemTemplate>
                                <%#Container.DataItemIndex+1 %>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Type" ItemStyle-Width="10%">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px;">
                                    <asp:Label ID="lblTaskType" runat="server" Text='<%# com.VirtuosoITech.ComplianceManagement.Business.Contract.ContractTaskManagement.ShowTaskType((int)Eval("TaskType")) %>'
                                        data-toggle="tooltip" data-placement="bottom" ToolTip='<%# com.VirtuosoITech.ComplianceManagement.Business.Contract.ContractTaskManagement.ShowTaskType((int)Eval("TaskType")) %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                         <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Contract Title" ItemStyle-Width="10%" SortExpression="ContractTitle">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 110px">
                                    <asp:Label ID="lblContractTitle" runat="server" Text='<%# Eval("ContractTitle") %>'
                                        data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("ContractTitle") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                         <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Task Title" ItemStyle-Width="15%" SortExpression="TaskTitle">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px">
                                    <asp:Label ID="lblTask" runat="server" Text='<%# Eval("TaskTitle") %>' Width="100%"
                                        data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("TaskTitle") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>


                           <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Initiated By" ItemStyle-Width="10%" SortExpression="Initiated By">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px">
                                   <asp:Label runat="server" Text='<%# com.VirtuosoITech.ComplianceManagement.Business.UserManagement.GetUserName(Convert.ToInt32(Eval("CreatedBy"))).ToString() %>' Width="100%"
                                        data-toggle="tooltip" data-placement="bottom"></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                           <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Assinged To" ItemStyle-Width="10%" SortExpression="Assinged To">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px">
                                    <asp:Label runat="server" Text='<%# com.VirtuosoITech.ComplianceManagement.Business.UserManagement.showAssignedUser((string)Eval("AssignedTaskUser")) %>' Width="100%"
                                        data-toggle="tooltip" data-placement="bottom" ToolTip='<%#com.VirtuosoITech.ComplianceManagement.Business.UserManagement.showAssignedUser((string)Eval("AssignedTaskUser")) %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Priority" ItemStyle-Width="10%" SortExpression="Priority">
                            <ItemTemplate>
                                <asp:Label ID="lblTaskPriority" runat="server" Text='<%# Eval("Priority") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Assign On" ItemStyle-Width="10%" SortExpression="AssignOn" Visible="false">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px;">
                                    <asp:Label ID="lblAssignOn" runat="server" Text='<%# Eval("AssignOn") != null ? Convert.ToDateTime(Eval("AssignOn")).ToString("dd-MM-yyyy") : "" %>'
                                        data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("AssignOn") != null ? Convert.ToDateTime(Eval("AssignOn")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Due Date" ItemStyle-Width="10%" SortExpression="DueDate">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px;">
                                    <asp:Label ID="lblDueOn" runat="server" Text='<%# Eval("DueDate") != null ? Convert.ToDateTime(Eval("DueDate")).ToString("dd-MM-yyyy") : "" %>'
                                        data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("DueDate") != null ? Convert.ToDateTime(Eval("DueDate")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Status" ItemStyle-Width="10%">
                            <ItemTemplate>
                                <asp:Label ID="lblTaskStatus" runat="server" Text='<%# Eval("StatusName") %>' ToolTip='<%# Eval("StatusName") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Action" HeaderStyle-CssClass="text-right" ItemStyle-CssClass="text-right" ItemStyle-Width="5%">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 50px;">
                                    <asp:LinkButton CommandArgument='<%# Eval("TaskID")+","+Eval("ContractID")+","+Eval("RoleID")%>'
                                        AutoPostBack="true" OnClick="lnkBtnTaskResponse_Click" data-placement="left"
                                        ID="lnkBtnTaskResponse" runat="server" ToolTip="View Task Detail(s)/Submit Response" data-toggle="tooltip">
                                       <img src='<%# ResolveUrl("~/Images/edit_icon_new.png")%>' alt="View"/>
                                    </asp:LinkButton>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="clsROWgrid" />
                    <HeaderStyle CssClass="clsheadergrid" />
                    <PagerTemplate></PagerTemplate>
                    <EmptyDataTemplate>
                        No Records Found.
                    </EmptyDataTemplate>
                </asp:GridView>
            </div>

            <div class="col-md-12 colpadding0">
                <div class="col-md-8 colpadding0">
                    <div runat="server" id="DivRecordsScrum" style="float: left; margin-top: 5px; color: #999">
                        <p style="padding-right: 0px !Important;">
                            <%--<asp:Label ID="Label4" runat="server" Text="Showing "></asp:Label>--%>
                            <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>- 
                            <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                            <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                        </p>
                    </div>
                </div>
                <div class="col-md-2 colpadding0">
                    <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 37%; float: right; height:32px !important"
                        AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                        <asp:ListItem Text="5" />
                        <asp:ListItem Text="10" Selected="True" />
                        <asp:ListItem Text="20" />
                        <asp:ListItem Text="50" />
                    </asp:DropDownList>
                </div>
                <div class="col-md-2 colpadding0" style="float: right;">
                    <div style="float: left; width: 60%">
                        <p class="clsPageNo">Page</p>
                    </div>
                    <div style="float: left; width: 40%">
                        <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true" DataPlaceHolder="No" AllowSingleDeselect="false"
                            OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged" class="form-control m-bot15" Width="100%" Height="30px">
                        </asp:DropDownListChosen>
                    </div>
                </div>
                <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
            </div>
        </div>
        <%--</section>--%>
    </div>

    <div class="modal fade" id="divShowDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="background-color: #f7f7f7; height: 30px;">
                    <button type="button" class="close" onclick="javascript:reloadTaskList();" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>

                <div class="modal-body" style="background-color: #f7f7f7;">
                    <iframe id="showdetails" src="about:blank" frameborder="0"></iframe>
                </div>
            </div>
        </div>
    </div>

     
    <div class="modal fade" id="AdvanceSearch" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 500px">
            <div class="modal-content" style="height: 150px">

                <div class="modal-header">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label class="modal-header-custom">
                        Filter(s)</label>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>

                <div class="clearfix"></div>

                <div class="modal-body" style="width: 100%;">
                   
                </div>

            </div>
        </div>
    </div>
    

</asp:Content>
