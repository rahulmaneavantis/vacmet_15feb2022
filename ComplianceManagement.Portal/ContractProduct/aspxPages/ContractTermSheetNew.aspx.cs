﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Ionic.Zip;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.aspxPages
{
    public partial class ContractTermSheetNew : System.Web.UI.Page
    {
        public int CIid;
        protected static string KendoPath;
        protected static int customerID;
        protected static int userid;
        protected static int conInitiid;
        public static string DocumentPath = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                KendoPath = ConfigurationManager.AppSettings["KendoPathApp"];
                ViewState["PageLink"] = "ContractLink";
                btnAddContract_Click(sender, e);
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                userid = Convert.ToInt32(AuthenticationHelper.UserID);
                var branchList = CustomerBranchManagement.GetAllContractEntitiesAssignedHierarchyManagementSatutory(customerID, userid, AuthenticationHelper.Role);
                string Flag = Convert.ToString(Request.QueryString["Flag"]);
                var InitiatorID = Request.QueryString["contractInitiatorID"];
                ViewState["ContractTermID"] = InitiatorID;
                conInitiid = Convert.ToInt32(InitiatorID);
                //BindCustomerBranches(tvFilterLocation, tbxFilterLocation, branchList);
                BindCustomerBranches();
                BindDepartment();
                
                BindVendors();
                BindUsers();
                if (Flag == "Save")
                {
                    ViewState["Mode"] = 0;
                    liContractRequestor.Visible = false;
                    liAuditLog.Visible = false;
                }
                else if(Flag == "Update")
                {
                    ViewState["Mode"] = 1;
                    Getdetails(Convert.ToInt32(InitiatorID));
                    GetContractRequestdetails(Convert.ToInt32(InitiatorID));
                    GetAuditLogData(Convert.ToInt32(InitiatorID));
                    BindTempDocumentData(Convert.ToInt64(AuthenticationHelper.UserID), Convert.ToInt64(AuthenticationHelper.CustomerID), Convert.ToInt32(InitiatorID));

                }
                else
                {
                    Getdetails(Convert.ToInt32(InitiatorID));
                    GetContractRequestdetails(Convert.ToInt32(InitiatorID));
                    GetAuditLogData(Convert.ToInt32(InitiatorID));
                    btnSave.Enabled = false;
                    btndraft.Enabled = false;
                }
                string ContractNo = Getcontractno(Convert.ToInt32(conInitiid));
                string conno = Existscontract(ContractNo);
                if (conno != null)
                {
                    btnCreateContract.Enabled = false;
                }
                else
                {
                    btnCreateContract.Enabled = true;
                }
                if(AuthenticationHelper.Role== "EXCT")
                {
                    btnCreateContract.Enabled = false;
                }
                //BindLocationFilter();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "hideDivTree", "hideDivBranch();", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "MyPostBackScript", "FetchUSerDetail();", true);

            }
        }
        public void Getdetails(int InitiatorID)
        {

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.tbl_Contract_TermSheet
                            where row.ID == InitiatorID
                            select row).SingleOrDefault();

                txtcontractno.Text = data.Contract_no;
                txtscopeofservice.Text = data.Scope_of_service;
                txtterm.Text = data.Term;
                txtservicefees.Text = data.ServiceFees.ToString();
                ddltermpayment.SelectedValue = data.PaymentTermid.ToString();
                txtdetails1.Text = data.Taxation_addDetails;
                txtdetails2.Text = data.SharedData_adddetails;
                txtdetails3.Text = data.Confidetialdata1_details;
                txtspocdesignation.Text = data.SPOC_Designation;
                ddlEntity.SelectedValue = data.EntityTypeId.ToString();
                tbxobjective.Text = data.Contract_objective;
                txtrisk.Text = data.Define_Risk;
                txtconrisk.Text = data.Consequence_defineRisk;
                txtmitigationaction.Text = data.Risk_MitigationAction;
                txtclauses.Text = data.Contract_Clauses;
                txtescprocess.Text = data.Escalation_process;
                txttermplan1.Text = data.Exit_Termination_plan;
                txttermplan2.Text = data.Termination_plan;
                txtauditappproval.Text = data.Audit_approval;
                txtdetails4.Text = data.IsEGIC_Additionaldetails;
                txtdetails5.Text = data.IsIntellectualshared_details;

                if(data.departmentid != -1)
                {
                    ddldepartment.SelectedValue = data.departmentid.ToString();
                }
                if(data.IsTaxation_instruction == false)
                {
                    ddltaxation.SelectedValue = "false";
                    divadddetails1.Visible = false;
                }
                else
                {
                    ddltaxation.SelectedValue = "true";
                    divadddetails1.Visible = true;
                }
                if(data.IsSharedData == false)
                {
                    ddlshared.SelectedValue = "false";
                    divadddetails2.Visible = false;
                }
                else
                {
                    ddlshared.SelectedValue = "true";
                    divadddetails2.Visible = true;
                }
                if(data.IsConfidentialdata1 == false)
                {
                    ddlcondata1.SelectedValue = "false";
                    divdetails.Visible = false;
                }
                else
                {
                    ddlcondata1.SelectedValue = "true";
                    divdetails.Visible = true;
                }
                if (data.IsConfidentialdata2 == false)
                {
                    ddlconfdata2.SelectedValue = "false";
                }
                else
                {
                    ddlconfdata2.SelectedValue = "true";
                }
                if(data.IsRelatedParty ==false)
                {
                    ddlrelparty.SelectedValue = "false";
                }
                else
                {
                    ddlrelparty.SelectedValue = "true";
                }
                if (data.IsOutsourcing_contract == false)
                {
                    ddloutscon.SelectedValue = "false";
                }
                else
                {
                    ddloutscon.SelectedValue = "true";
                }
                if (data.IsRisk_assessment == false)
                {
                    ddlrisk1.SelectedValue = "false";
                }
                else
                {
                    ddlrisk1.SelectedValue = "true";
                }
                if (data.IsIntellectualshared == false)
                {
                    ddlarrange.SelectedValue = "false";
                    divadddetails5.Visible = false;
                }
                else
                {
                    ddlarrange.SelectedValue = "true";
                    divadddetails5.Visible = true;
                }
                
                if (data.VendorId.ToString() != "" && data.VendorId.ToString() != "-1")
                {
                    ddlvendor.SelectedValue = data.VendorId.ToString();
                }
                if (data.SPOC_Nameid.ToString() != "" && data.SPOC_Nameid.ToString() != "-1")
                {
                    ddlSPOCName.SelectedValue = data.SPOC_Nameid.ToString();
                }
                if (data.Attentionreq.ToString() != "" && data.Attentionreq.ToString() != "-1")
                {
                    ddlattention.SelectedValue = data.Attentionreq.ToString();
                }
               
                if (data.TerminationNoticeperiodId.ToString() != "")
                {
                    DropDownListChosen2.SelectedValue = data.TerminationNoticeperiodId.ToString();
                }
                if (data.IsEGIC == false)
                {
                    ddlEGIC.SelectedValue = "false";
                    divadddetails4.Visible = false;
                }
                else
                {
                    ddlEGIC.SelectedValue = "true";
                    divadddetails4.Visible = true;
                }
                if (data.IPR_Rightsid.ToString() != "")
                {
                    DropDownListChosen1.SelectedValue = data.IPR_Rightsid.ToString();
                }
                
                if (data.RequestTo.ToString() != "")
                {
                    ddlrequestto.SelectedValue = data.RequestTo.ToString();
                }
                if(data.Startdate != null)
                {
                    txtstartdate.Text = Convert.ToDateTime(data.Startdate).ToString("dd-MM-yyyy");
                }
                if(data.Enddate != null)
                {
                    txtenddate.Text = Convert.ToDateTime(data.Enddate).ToString("dd-MM-yyyy");
                }
                if(data.todate != null)
                {
                    txtdate.Text = Convert.ToDateTime(data.todate).ToString("dd-MM-yyyy");
                }
                if (data.Vendor_Address != "" && data.Vendor_Address != null)
                {
                    txtvendoraddress.Text = data.Vendor_Address;
                }
                if (data.branchid != 0)
                {
                    foreach (TreeNode node in tvFilterLocation.Nodes)
                    {
                        if (node.Value == data.branchid.ToString())
                        {
                            node.Selected = true;
                        }
                        foreach (TreeNode item1 in node.ChildNodes)
                        {
                            if (item1.Value == data.branchid.ToString())
                                item1.Selected = true;
                        }
                    }
                }
                if(data.Approver != null && data.Approver != "")
                {
                    ddlApproveruser.SelectedValue = data.Approver;
                }
                tvFilterLocation_SelectedNodeChanged(null, null);

                

            }

        }
        
        public void GetContractRequestdetails(int InitiatorID)
        {

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.SP_GetContractTermSheetDetails()
                            where row.ID == InitiatorID
                            select row).SingleOrDefault();
                if(data.DepartmentName!="" && data.DepartmentName!="-1" && data.DepartmentName!=null)
                {
                    lbldept.Text = data.DepartmentName.ToString();
                }
                if (data.IsContract_Summary != "" && data.IsContract_Summary != "-1" && data.IsContract_Summary!=null)
                {
                    lblcontsummmary.Text = data.IsContract_Summary.ToString();
                }
                if (data.VendorName != "" && data.VendorName != "-1" && data.VendorName!=null)
                {
                    lblnameofvendor.Text = data.VendorName.ToString();
                }
                if (data.Scope_of_service != "" && data.Scope_of_service!=null)
                {
                    lblscopeofservice.Text = data.Scope_of_service;
                }
                if (data.Startdate.ToString() != "" && data.Startdate!=null)
                {
                    lblstartdate.Text = Convert.ToDateTime(data.Startdate).ToString("dd-MM-yyyy");
                }
                if (data.Enddate.ToString() != "" && data.Enddate != null)
                {
                    lblenddate.Text = Convert.ToDateTime(data.Enddate).ToString("dd-MM-yyyy");
                }
                if (data.Term != "" && data.Term != null)
                {
                    lblterm.Text = data.Term;
                }
                if (data.ServiceFees.ToString() != "" && data.ServiceFees != null)
                {
                    lblserfees.Text = data.ServiceFees.ToString();
                }
                if (data.Contract_no != "" && data.Contract_no != null)
                {
                    lblcontractno.Text = data.Contract_no;
                }
                if (data.PaymentTerm != "" && data.PaymentTerm != null)
                {
                    lblTermsPayment.Text = data.PaymentTerm.ToString();
                }
                if (data.Vendor_Address != "" && data.Vendor_Address != null)
                {
                    lblvaddress.Text = data.Vendor_Address;
                }
                if (data.TaxationInstruction != "" && data.TaxationInstruction !=null)
                {
                    lbltaxationinstru.Text = data.TaxationInstruction.ToString();
                }
                if (data.Taxation_addDetails != "" && data.Taxation_addDetails !=null)
                {
                    lbltaxadddetails.Text = data.Taxation_addDetails;
                }
                if (data.IsSharedData.ToString() != "" && data.IsSharedData != null)
                {
                    lblshareddata.Text = data.IsSharedData.ToString();
                }
                if (data.SharedData_adddetails != "" && data.SharedData_adddetails!=null)
                {
                    lblsharedadditionaldetails.Text = data.SharedData_adddetails;
                }
               
                if (data.Confidetialdata1_details != "" && data.Confidetialdata1_details != null)
                {
                    lblconaddtidetail.Text = data.Confidetialdata1_details;
                }
                if (data.IsConfidentialdata2.ToString() != "" &&  data.IsConfidentialdata2!=null)
                {
                    lblcondata2.Text = data.IsConfidentialdata2.ToString();
                }
                if (data.IsRelatedParty.ToString() != "" && data.IsRelatedParty!=null)
                {
                    lblparty.Text = data.IsRelatedParty.ToString();
                }
                if (data.IsOutsourcing_contract.ToString() != "" && data.IsOutsourcing_contract!=null)
                {
                    lbloutsourcingcont.Text = data.IsOutsourcing_contract.ToString();
                }
                if (data.SPOC_Designation != "" && data.SPOC_Designation!=null)
                {
                    lblspocdesign.Text = data.SPOC_Designation.ToString();
                }
                if (data.EntityType != "" && data.EntityType != null)
                {
                    lblentitytype.Text = data.EntityType.ToString();
                }
                if (data.Contract_objective != "" && data.Contract_objective !=  null)
                {
                    lblconobjective.Text = data.Contract_objective;
                }
                if (data.todate.ToString() != "" && data.todate.ToString() != null)
                {
                    lbldate.Text = Convert.ToDateTime(data.todate).ToString("dd-MM-yyyy");
                }
                if (data.todate.ToString() != "" && data.todate.ToString() != null)
                {
                    //if(data.UpdatedOn.ToString() !="" && data.UpdatedOn !=null)
                    //{
                    //    DateTime newDate = Convert.ToDateTime(data.UpdatedOn).AddDays(3);
                    //    lblTAT.Text = newDate.ToString("dd-MM-yyyy");
                    //}
                    //else
                    if (data.CreatedOn.ToString() != "" && data.CreatedOn != null)
                    {
                        DateTime newDate = Convert.ToDateTime(data.CreatedOn).AddDays(3);
                        lblTAT.Text = newDate.ToString("dd-MM-yyyy");
                    }
                    
                }
                if (data.Define_Risk != "" && data.Define_Risk != null)
                {
                    lbldefinerisk.Text = data.Define_Risk;
                }
                if (data.Consequence_defineRisk != "" && data.Consequence_defineRisk != null)
                {
                    lblconrisk.Text = data.Consequence_defineRisk;
                }
                if (data.Risk_MitigationAction != "" && data.Risk_MitigationAction != null)
                {
                    lblriskmitigation.Text = data.Risk_MitigationAction;
                }
                if (data.Contract_Clauses != "" && data.Contract_Clauses != null)
                {
                    lblconclauses.Text = data.Contract_Clauses;
                }
                if (data.Escalation_process != "" && data.Escalation_process != null)
                {
                    lblescprocess.Text = data.Escalation_process;
                }
                if (data.Termination_plan != "" && data.Termination_plan != null)
                {
                    lblothertermplam.Text = data.Termination_plan;
                }
                if (data.TerminitationNoticeperiod != "" && data.TerminitationNoticeperiod != null)
                {
                    lbltermnoticeperiod.Text = data.TerminitationNoticeperiod;
                }
                if (data.Audit_approval != "" && data.Audit_approval != null)
                {
                    lblauitapproval.Text = data.Audit_approval;
                }

                if (data.IsRisk_assessment != "" && data.IsRisk_assessment != null)
                {
                    lblriskassessment.Text = data.IsRisk_assessment.ToString();
                }
                if (data.IsEGIC != "" && data.IsEGIC != null)
                {
                    lblEGIC.Text = data.IsEGIC.ToString();
                }
                if (data.IsEGIC_Additionaldetails != "" && data.IsEGIC_Additionaldetails != null)
                {
                    lblEGICdetails.Text = data.IsEGIC_Additionaldetails;
                }
                if (data.IsIntellectualshared_details != "" && data.IsIntellectualshared != null)
                {
                    lbladddetails.Text = data.IsIntellectualshared_details;
                }
                if (data.IsConfidentialdata1 != "" && data.IsConfidentialdata1 != null)
                {
                    lblcondata1.Text = data.IsConfidentialdata1.ToString();

                }
                if (data.SPOC_Name != "" && data.SPOC_Name != null)
                {
                    lblspocname.Text = data.SPOC_Name;
                }
                if (data.AttentionReq != "" && data.AttentionReq != null && data.AttentionReq != "-1")
                {
                    lblattenreq.Text = data.AttentionReq.ToString();
                }

                if (data.IPR_Rights.ToString() != "" && data.IPR_Rights != null)
                {
                    lblIPRRights.Text = data.IPR_Rights.ToString();
                }
                if (data.IsIntellectualshared.ToString() != "" && data.IsIntellectualshared != null)
                {
                    lblinteproperty.Text = data.IsIntellectualshared.ToString();
                }
                if (data.CustomerBranch != "" && data.CustomerBranch != null)
                {
                    lbllocation.Text = data.CustomerBranch;
                }
                if (data.Requestor != "" && data.Requestor != null)
                {
                    lblreviewer.Text = data.Requestor;
                }
                if(data.Approver !="-1")
                {
                    lblapprover.Text = data.Approver;
                }
            }

        }

        public void GetAuditLogData(int InitiatorID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.Cont_tbl_TermSheetPostComment
                            where row.ContractID == InitiatorID
                            select row).ToList();

                gvContractAuditLog.DataSource = data;
                gvContractAuditLog.DataBind();
            }
        }
        protected void TabContract_Click(object sender, EventArgs e)
        {
            ViewState["PageLink"] = "ContractLink";
            liContractSummary.Attributes.Add("class", "active");
            liContractRequestor.Attributes.Add("class", "");

            liAuditLog.Attributes.Add("class", "");

            MainView.ActiveViewIndex = 0;


            long customerID = -1;
            customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;

       }

        protected void btnAddContract_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["Mode"] = 0;
                TabContract_Click(sender, e);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilternew", "$(\"#divFilterLocationnew\").hide(\"blind\", null, 500, function () { });", true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        //private void BindCustomerBranches(TreeView treetoBind, TextBox treeTxtBox, List<NameValueHierarchy> branchList)
        //{
        //    try
        //    {
        //        treetoBind.Nodes.Clear();

        //        NameValueHierarchy branch = null;

        //        if (branchList.Count > 0)
        //        {
        //            branch = branchList[0];
        //        }

        //        treeTxtBox.Text = "Select Entity/Branch/Location";

        //        List<TreeNode> nodes = new List<TreeNode>();

        //        BindBranchesHierarchy(null, branch, nodes);

        //        foreach (TreeNode item in nodes)
        //        {
        //            treetoBind.Nodes.Add(item);
        //        }

        //        treetoBind.CollapseAll();
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvContractterm.IsValid = false;
        //        cvContractterm.ErrorMessage = "Something went wrong, Please try again.";
        //    }
        //}
        private void BindCustomerBranches()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                tvFilterLocation.Nodes.Clear();
                NameValueHierarchy branch = null;

                //var branchs = CustomerBranchManagement.GetAllHierarchy(customerID);
                //var branchs = CustomerBranchManagement.GetAllHierarchyManagementSatutory(customerID);
                List<NameValueHierarchy> branchs;
                //string key = "LocationHierarchy" + AuthenticationHelper.CustomerID;
                //if (CacheHelper.Exists(key))
                //{
                //    CacheHelper.Get<List<NameValueHierarchy>>(key, out branchs);
                //}
                //else
                //{
                //branchs = CustomerBranchManagement.GetAllContractEntitiesAssignedHierarchyManagementSatutory(customerID,AuthenticationHelper.UserID,AuthenticationHelper.Role);
                //CacheHelper.Set<List<NameValueHierarchy>>(key, branchs);
                //}
                branchs = CustomerBranchManagement.GetAllHierarchyManagementSatutory(customerID);
                if (branchs.Count > 0)
                {
                    branch = branchs[0];
                }

                //tbxBranch.Text = "Select Entity/Location";
                tbxFilterLocation.Text = "Click to Select";

                List<TreeNode> nodes = new List<TreeNode>();
                BindBranchesHierarchy(null, branch, nodes);
                foreach (TreeNode item in nodes)
                {
                    tvFilterLocation.Nodes.Add(item);
                }

                tvFilterLocation.CollapseAll();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvContractterm.IsValid = false;
                cvContractterm.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }
        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp, List<TreeNode> nodes)
        {
            try
            {
                if (nvp != null)
                {
                    foreach (var item in nvp.Children)
                    {
                        TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                        BindBranchesHierarchy(node, item, nodes);
                        if (parent == null)
                        {
                            nodes.Add(node);
                        }
                        else
                        {
                            parent.ChildNodes.Add(node);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvContractterm.IsValid = false;
                cvContractterm.ErrorMessage = "Something went wrong, Please try again.";
            }
        }

        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxFilterLocation.Text = tvFilterLocation.SelectedNode != null ? tvFilterLocation.SelectedNode.Text : "Select Entity/Branch/Location";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "hideDivTree", "hideDivBranch();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void BindDepartment()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var users = GetAllDepartment(customerID);

            ddldepartment.DataValueField = "ID";
            ddldepartment.DataTextField = "Name";
            ddldepartment.DataSource = users;
            ddldepartment.DataBind();

          
            ddldepartment.Items.Insert(0, new ListItem("Select Department", "-1"));

        }
        public void BindUsers()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            int CacheClearTime = Convert.ToInt32(ConfigurationManager.AppSettings["CacheClearMasterData"]);
            var key = "Contusers-" + Convert.ToString(customerID);
            var lstAllUsers = (List<User>)HttpContext.Current.Cache[key];
            if (HttpContext.Current.Cache[key] == null)
            {
                lstAllUsers = ContractUserManagement.GetAllUsers_Contract(customerID);
                HttpContext.Current.Cache.Insert(key, lstAllUsers, null, DateTime.Now.AddMinutes(CacheClearTime), TimeSpan.Zero); // add it to cache
            }

            if (lstAllUsers.Count > 0)
                lstAllUsers = lstAllUsers.Where(entry => entry.ContractRoleID != null).ToList();

            var internalUsers = ContractUserManagement.GetRequiredUsers(lstAllUsers, 1); //1--Internal---

            //ddlApprover.DataValueField = "ID";
            //ddlApprover.DataTextField = "Name";
            //ddlApprover.DataSource = internalUsers;
            //ddlApprover.DataBind();

            lstAllUsers.Clear();
            lstAllUsers = null;

            var allUsers = ContractUserManagement.GetAllUsersContract(customerID);

            ddlSPOCName.DataValueField = "ID";
            ddlSPOCName.DataTextField = "Name";
            ddlSPOCName.DataSource = allUsers;
            ddlSPOCName.DataBind();

            var data = ContractUserManagement.GetAllMGMTContract(customerID);
            

            ddlrequestto.DataValueField = "ID";
            ddlrequestto.DataTextField = "Name";
            ddlrequestto.DataSource = data;
            ddlrequestto.DataBind();

            ddlrequestto.Items.Insert(0, new ListItem("Select User", "-1"));
            ddlSPOCName.Items.Insert(0, new ListItem("Select User", "-1"));
        }
        public static List<object> GetAllDepartment(int customerid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Departments
                             where row.IsDeleted == false
                             && row.CustomerID == customerid
                             select new { row.ID, row.Name });

                return query.ToList<object>();
            }
        }
        protected void upDivLocation_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        //private void BindLocationFilter()
        //{
        //    try
        //    {

        //        int customerID = -1;
        //        //if (AuthenticationHelper.Role == "CADMN")
        //        //{
        //        customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
        //        //}
        //        var bracnhes = CustomerBranchManagement.GetAllHierarchy(customerID);

        //        //TreeNode node = new TreeNode("< All >", "-1");
        //        //node.Selected = true;
        //        //tvFilterLocation.Nodes.Add(node);

        //        foreach (var item in bracnhes)
        //        {
        //            TreeNode node = new TreeNode(item.Name, item.ID.ToString());
        //            node.SelectAction = TreeNodeSelectAction.Expand;
        //            BindBranchesHierarchy(node, item);
        //            //tvFilterLocationnew.Nodes.Add(node);
        //        }

        //        //tvFilterLocationnew.CollapseAll();


        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //}

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp)
        {
            try
            {
                foreach (var item in nvp.Children)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    BindBranchesHierarchy(node, item);
                    parent.ChildNodes.Add(node);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnlocation_Click(object sender, EventArgs e)
        {
            try
            {
                //BindComplianceEntityInstances();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnClear1_Click(object sender, EventArgs e)
        {

            for (int i = 0; i < this.tvFilterLocation.Nodes.Count; i++)
            {
                ChkBoxClear(this.tvFilterLocation.Nodes[i]);
            }
        }

        protected void ChkBoxClear(TreeNode node)
        {

            if (node.Checked) // && node.ChildNodes.Count == 0 if (node.Checked)
            {
                node.Checked = false;
            }
            foreach (TreeNode tn in node.ChildNodes)
            {

                if (tn.Checked)//&& tn.ChildNodes.Count == 0)//  && tn.ChildNodes.Count == 0if (tn.Checked)              
                {
                    tn.Checked = false;
                }

                if (tn.ChildNodes.Count != 0)
                {
                    for (int i = 0; i < tn.ChildNodes.Count; i++)
                    {
                        ChkBoxClear(tn.ChildNodes[i]);
                    }
                }
            }
        }
        //protected void upComplianceTypeList_Load(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        //ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilternew", string.Format("initializeJQueryUI('{0}', 'divFilterLocationnew');", tbxFilterLocationnew.ClientID), true);
        //        ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilternew", "$(\"#divFilterLocationnew\").hide(\"blind\", null, 500, function () { });", true);
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        //cvDuplicateEntry.IsValid = false;
        //        //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
        //    }
        //}
       
       
        public void BindVendors()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            var lstVendors = VendorDetails.GetVendors_All(customerID);

            ddlvendor.DataTextField = "VendorName";
            ddlvendor.DataValueField = "ID";

            ddlvendor.DataSource = lstVendors;
            ddlvendor.DataBind();

            ddlvendor.Items.Insert(0, new ListItem("Select Vendor", "-1"));
        }
        protected void btnclear_Click(object sender, EventArgs e)
        {

        }

        protected void ddltaxation_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddltaxation.SelectedValue == "false")
            {
                divadddetails1.Visible = false;
            }
            else
            {
                divadddetails1.Visible = true;
            }

        }

        protected void ddlshared_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlshared.SelectedValue == "false")
            {
                divadddetails2.Visible = false;
            }
            else
            {
                divadddetails2.Visible = true;
            }
        }

        protected void ddlcondata1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlcondata1.SelectedValue == "false")
            {
                divdetails.Visible = false;
            }
            else
            {
                divdetails.Visible = true;
            }
        }

        protected void ddlEGIC_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlEGIC.SelectedValue == "false")
            {
                divadddetails6.Visible = false;
            }
            else
            {
                divadddetails6.Visible = true;
            }
        }

        protected void ddlarrange_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlarrange.SelectedValue == "false")
            {
                divadddetails5.Visible = false;
            }
            else
            {
                divadddetails5.Visible = true;
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                bool saveSuccess = false;
                Boolean chkSubIndustryFlag = false;
                if (tvFilterLocation.SelectedNode != null && tvFilterLocation.SelectedNode.Text != "Select Entity/Branch/Location")
                {
                    chkSubIndustryFlag = true;
                }
                if (chkSubIndustryFlag == false)
                {
                    cvContractterm.IsValid = false;
                    cvContractterm.ErrorMessage = "Please select at least one Location.";
                    ValidationSummary11.CssClass = "alert alert-danger";
                }
                else
                {
                    tbl_Contract_TermSheet tbl = new tbl_Contract_TermSheet()
                    {
                        IsContract_Summary = Convert.ToBoolean(ddlcontractsummmary.SelectedValue),
                        Contract_no = txtcontractno.Text,
                        VendorId = Convert.ToInt32(ddlvendor.SelectedValue),
                        Vendor_Address = txtvendoraddress.Text,
                        Scope_of_service = txtscopeofservice.Text,
                        //tbl.Startdate = Convert.ToDateTime(txtstartdate.Text);
                        Startdate = DateTimeExtensions.GetDate(txtstartdate.Text),
                        Enddate = DateTimeExtensions.GetDate(txtenddate.Text),
                        Term = txtterm.Text,
                        ServiceFees = Convert.ToInt32(txtservicefees.Text),
                        PaymentTermid = Convert.ToInt32(ddltermpayment.SelectedValue),
                        IsTaxation_instruction = Convert.ToBoolean(ddltaxation.SelectedValue),
                        Taxation_addDetails = txtdetails1.Text,
                        IsSharedData = Convert.ToBoolean(ddlshared.SelectedValue),
                        SharedData_adddetails = txtdetails2.Text,
                        IsConfidentialdata1 = Convert.ToBoolean(ddlcondata1.SelectedValue),
                        Confidetialdata1_details = txtdetails3.Text,
                        IsConfidentialdata2 = Convert.ToBoolean(ddlconfdata2.SelectedValue),
                        IsRelatedParty = Convert.ToBoolean(ddlrelparty.SelectedValue),
                        IsOutsourcing_contract = Convert.ToBoolean(ddloutscon.SelectedValue),
                        branchid = Convert.ToInt32(tvFilterLocation.SelectedValue),
                        departmentid = Convert.ToInt32(ddldepartment.SelectedValue),
                        //tbl.SPOC_Nameid = Convert.ToInt32(ddlSPOCName.SelectedValue);
                        SPOC_Designation = txtspocdesignation.Text,
                        EntityTypeId = Convert.ToInt32(ddlEntity.SelectedValue),
                        //Attentionreq = Convert.ToInt32(ddlattention.SelectedValue),
                        Contract_objective = tbxobjective.Text,
                        //IsRisk_assessment = Convert.ToBoolean(ddlrisk1.SelectedValue),
                        todate = DateTimeExtensions.GetDate(txtdate.Text),
                        Define_Risk = txtrisk.Text,
                        Consequence_defineRisk = txtconrisk.Text,
                        Risk_MitigationAction = txtmitigationaction.Text,
                        Contract_Clauses = txtclauses.Text,
                        Escalation_process = txtescprocess.Text,
                        Exit_Termination_plan = txttermplan1.Text,
                        //TerminationNoticeperiodId = Convert.ToInt32(DropDownListChosen2.SelectedValue),
                        Termination_plan = txttermplan2.Text,
                        Audit_approval = txtauditappproval.Text,
                        //IsEGIC = Convert.ToBoolean(ddlEGIC.SelectedValue),
                        IsEGIC_Additionaldetails = txtdetails4.Text,
                        //IPR_Rightsid = Convert.ToInt32(DropDownListChosen1.SelectedValue),
                        //IsIntellectualshared = Convert.ToBoolean(ddlarrange.SelectedValue),
                        IsIntellectualshared_details = txtdetails5.Text,
                        RequestTo = Convert.ToInt32(ddlrequestto.SelectedValue),
                    };
                    if (ddlSPOCName.SelectedValue != "" && ddlSPOCName.SelectedValue != "-1")
                    {
                        tbl.SPOC_Nameid = Convert.ToInt32(ddlSPOCName.SelectedValue);
                    }
                    if (ddlattention.SelectedValue != "" && ddlattention.SelectedValue != "-1")
                    {
                        tbl.Attentionreq = Convert.ToInt32(ddlattention.SelectedValue);
                    }
                    if (ddlrisk1.SelectedValue != "")
                    {
                        tbl.IsRisk_assessment = Convert.ToBoolean(ddlrisk1.SelectedValue);
                    }
                    if (DropDownListChosen2.SelectedValue != "")
                    {
                        tbl.TerminationNoticeperiodId = Convert.ToInt32(DropDownListChosen2.SelectedValue);
                    }
                    if (ddlEGIC.SelectedValue != "")
                    {
                        tbl.IsEGIC = Convert.ToBoolean(ddlEGIC.SelectedValue);
                    }
                    if (DropDownListChosen1.SelectedValue != "")
                    {
                        tbl.IPR_Rightsid = Convert.ToInt32(DropDownListChosen1.SelectedValue);
                    }
                    if (ddlarrange.SelectedValue != "")
                    {
                        tbl.IsIntellectualshared = Convert.ToBoolean(ddlarrange.SelectedValue);
                    }
                    if(ddlApproveruser.SelectedValue != "-1")
                    {
                        tbl.Approver = ddlApproveruser.SelectedValue;
                    }

                    tbl.CreatedBy = AuthenticationHelper.UserID;
                    tbl.CreatedOn = System.DateTime.Now;
                    tbl.Isdeleted = false;



                    if ((int)ViewState["Mode"] == 0)
                    {
                        Boolean result = ExistscontractTermSheet(tbl.Contract_no);
                        if (result==true)
                        {
                            tbl.ID = Convert.ToInt32(ViewState["ContractTermID"]);
                            saveSuccess = UpdateContractTermSheet(tbl);
                            UploadDocuments(tbl.ID, tbl.Contract_no);
                            BindTempDocumentData(Convert.ToInt64(AuthenticationHelper.UserID), Convert.ToInt64(AuthenticationHelper.CustomerID), Convert.ToInt32(tbl.ID));
                            cvContractterm.IsValid = false;
                            cvContractterm.ErrorMessage = "Contract term sheet send successfully.";
                            ValidationSummary11.CssClass = "alert alert-success";
                        }
                        else
                        {
                            CreateTermSheet(tbl);
                            UploadDocuments(tbl.ID, tbl.Contract_no);
                            BindTempDocumentData(Convert.ToInt64(AuthenticationHelper.UserID), Convert.ToInt64(AuthenticationHelper.CustomerID), Convert.ToInt32(tbl.ID));
                            cvContractterm.IsValid = false;
                            cvContractterm.ErrorMessage = "Contract term sheet send successfully.";
                            ValidationSummary11.CssClass = "alert alert-success";
                        }
                        

                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "scrollUpPageScript", "scrollUpPage();", true);

                    }
                    else
                    {
                        tbl.ID = Convert.ToInt32(ViewState["ContractTermID"]);
                        saveSuccess = UpdateContractTermSheet(tbl);
                        UploadDocuments(tbl.ID, tbl.Contract_no);
                        cvContractterm.IsValid = false;
                        cvContractterm.ErrorMessage = "Contract term sheet send successfully.";
                        ValidationSummary11.CssClass = "alert alert-success";
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "scrollUpPageScript", "scrollUpPage();", true);

                    }
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
               
            }
           

        }
        public static bool UpdateContractTermSheet(tbl_Contract_TermSheet objRecord)
        {
            try
            {
                using (ComplianceDBEntities Entities = new ComplianceDBEntities())
                {
                    var QueryResult = (from row in Entities.tbl_Contract_TermSheet
                                       where row.ID == objRecord.ID
                                       select row).FirstOrDefault();

                    if (QueryResult != null)
                    {
                        QueryResult.IsContract_Summary = objRecord.IsContract_Summary;
                        QueryResult.Contract_no = objRecord.Contract_no;
                        QueryResult.VendorId = objRecord.VendorId;
                        QueryResult.Vendor_Address = objRecord.Vendor_Address;
                        QueryResult.Scope_of_service = objRecord.Scope_of_service;
                        //tbl.Startdate = Convert.ToDateTime(txtstartdate.Text);
                        QueryResult.Startdate = objRecord.Startdate;
                        QueryResult.Enddate = objRecord.Enddate;
                        QueryResult.Term = objRecord.Term;
                        QueryResult.ServiceFees = objRecord.ServiceFees;
                        QueryResult.PaymentTermid = objRecord.PaymentTermid;
                        QueryResult.IsTaxation_instruction = objRecord.IsTaxation_instruction;
                        QueryResult.Taxation_addDetails = objRecord.Taxation_addDetails;
                        QueryResult.IsSharedData = objRecord.IsSharedData;
                        QueryResult.SharedData_adddetails = objRecord.SharedData_adddetails;
                        QueryResult.IsConfidentialdata1 = objRecord.IsConfidentialdata1;
                        QueryResult.Confidetialdata1_details = objRecord.Confidetialdata1_details;
                        QueryResult.IsConfidentialdata2 = objRecord.IsConfidentialdata2;
                        QueryResult.IsRelatedParty = objRecord.IsRelatedParty;
                        QueryResult.IsOutsourcing_contract = objRecord.IsOutsourcing_contract;
                        QueryResult.branchid = objRecord.branchid;
                        QueryResult.departmentid = objRecord.departmentid;
                        //tbl.SPOC_Nameid = Convert.ToInt32(ddlSPOCName.SelectedValue);
                        QueryResult.SPOC_Designation = objRecord.SPOC_Designation;
                        QueryResult.EntityTypeId = objRecord.EntityTypeId;
                        //Attentionreq = Convert.ToInt32(ddlattention.SelectedValue),
                        QueryResult.Contract_objective = objRecord.Contract_objective;
                        //IsRisk_assessment = Convert.ToBoolean(ddlrisk1.SelectedValue),
                        QueryResult.todate = objRecord.todate;
                        QueryResult.Define_Risk = objRecord.Define_Risk;
                        QueryResult.Consequence_defineRisk = objRecord.Consequence_defineRisk;
                        QueryResult.Risk_MitigationAction = objRecord.Risk_MitigationAction;
                        QueryResult.Contract_Clauses = objRecord.Contract_Clauses;
                        QueryResult.Escalation_process = objRecord.Escalation_process;
                        QueryResult.Exit_Termination_plan = objRecord.Exit_Termination_plan;
                        //TerminationNoticeperiodId = Convert.ToInt32(DropDownListChosen2.SelectedValue),
                        QueryResult.Termination_plan = objRecord.Termination_plan;
                        QueryResult.Audit_approval = objRecord.Audit_approval;
                        //IsEGIC = Convert.ToBoolean(ddlEGIC.SelectedValue),
                        QueryResult.IsEGIC_Additionaldetails = objRecord.IsEGIC_Additionaldetails;
                        //IPR_Rightsid = Convert.ToInt32(DropDownListChosen1.SelectedValue),
                        //IsIntellectualshared = Convert.ToBoolean(ddlarrange.SelectedValue),
                        QueryResult.IsIntellectualshared_details = objRecord.IsIntellectualshared_details;
                        QueryResult.SPOC_Nameid = objRecord.SPOC_Nameid;
                        QueryResult.Attentionreq = objRecord.Attentionreq;
                        QueryResult.IsRisk_assessment = objRecord.IsRisk_assessment;
                        QueryResult.TerminationNoticeperiodId = objRecord.TerminationNoticeperiodId;
                        QueryResult.IsEGIC = objRecord.IsEGIC;
                        QueryResult.IPR_Rightsid = objRecord.IPR_Rightsid;
                        QueryResult.IsIntellectualshared = objRecord.IsIntellectualshared;
                        QueryResult.RequestTo = objRecord.RequestTo;
                        QueryResult.CreatedBy = AuthenticationHelper.UserID;
                        //QueryResult.UpdatedOn = System.DateTime.Now;
                        QueryResult.Approver = objRecord.Approver;
                        Entities.SaveChanges();

                    }
                }

                return true;

            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }
        public static void CreateTermSheet(tbl_Contract_TermSheet objEntitiesAssignment)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.tbl_Contract_TermSheet.Add(objEntitiesAssignment);
                entities.SaveChanges();
            }
        }

        protected void lnkContractRequestor_Click(object sender, EventArgs e)
        {
            ViewState["PageLink"] = "ContractLink";
            liContractSummary.Attributes.Add("class", "");
            liContractRequestor.Attributes.Add("class", "active");
            liAuditLog.Attributes.Add("class", "");

            MainView.ActiveViewIndex = 1;

        }

        protected void lnkAuditLog_Click(object sender, EventArgs e)
        {
            ViewState["PageLink"] = "ContractLink";
            liContractSummary.Attributes.Add("class", "");
            liContractRequestor.Attributes.Add("class", "");
            liAuditLog.Attributes.Add("class", "active");

            MainView.ActiveViewIndex = 2;
        }

        protected void rptDocmentVersionView_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                // var AllinOneDocumentList=null;
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                if (e.CommandName.Equals("View"))
                {
                    if (e.CommandArgument != null)
                    {
                        long fileID = Convert.ToInt64(e.CommandArgument);
                        if (fileID != 0)
                            ViewContractDocument(fileID);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                // cvDuplicateEntry.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }
        public bool ViewContractDocument(long contFileID)
        {
            bool viewSuccess = false;
            try
            {
                var file = ContractDocumentManagement.GetContractTSheetDocumentByID(contFileID);

                if (file != null)
                {
                    string filePath = Path.Combine(Server.MapPath(file.DocPath), Path.GetExtension(file.DocName));

                    if (file.DocPath != null && File.Exists(filePath))
                    {
                        string Folder = "~/TempFiles";
                        string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                        string DateFolder = Folder + "/" + File;

                        string extension = System.IO.Path.GetExtension(filePath);

                        Directory.CreateDirectory(Server.MapPath(DateFolder));

                        if (!Directory.Exists(DateFolder))
                        {
                            Directory.CreateDirectory(Server.MapPath(DateFolder));
                        }

                        string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

                        string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                        string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                        string FileName = DateFolder + "/" + User + "" + extension;

                        FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                        BinaryWriter bw = new BinaryWriter(fs);
                        //if (file.EnType == "M")
                        //{
                            bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                        //}
                        //else
                        //{
                           // bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                        //}
                        bw.Close();
                        DocumentPath = FileName;

                        DocumentPath = DocumentPath.Substring(2, DocumentPath.Length - 2);

                        lblMessage.Text = "";


                    }

                }

                return viewSuccess;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return viewSuccess;
            }
        }
        protected void rptDocmentVersionView_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);

                LinkButton lblDocumentVersionView = (LinkButton)e.Item.FindControl("lblDocumentVersionView");
                scriptManager.RegisterAsyncPostBackControl(lblDocumentVersionView);
            }
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            try
            {
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("AuditLogDetail");
                    DataTable ExcelData = null;
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        if (ViewState["ContractTermID"] != null)
                        {
                             long contractInitiatorID = Convert.ToInt64(ViewState["ContractTermID"]);

                            if (contractInitiatorID != 0)
                            {
                                var lstAuditLogs = GetContractAuditLogs_All(contractInitiatorID);

                                var cname = CustomerManagement.CustomerGetByIDName(Convert.ToInt32(AuthenticationHelper.CustomerID));
                                DataTable table = lstAuditLogs.ToDataTable();

                                DataView view = new System.Data.DataView(table);
                                //List<Cont_SP_AuditLogs_All_Result> cb = new List<Cont_SP_AuditLogs_All_Result>();

                                ExcelData = view.ToTable("Selected", false, "FromUserName", "Comment", "CreatedOn");

                                ExcelData.Columns.Add("SNo", typeof(int)).SetOrdinal(0);

                                int rowCount = 0;
                                foreach (DataRow item in ExcelData.Rows)
                                {
                                    item["SNo"] = ++rowCount;
                                }

                                exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                                exWorkSheet.Cells["A1"].Value = "Customer Name:";

                                exWorkSheet.Cells["B1"].Merge = true;
                                exWorkSheet.Cells["B1"].Value = cname;

                                exWorkSheet.Cells["A2"].Style.Font.Bold = true;
                                exWorkSheet.Cells["A2"].Value = "Report Name:";

                                //exWorkSheet.Cells["B2:C2"].Merge = true;
                                exWorkSheet.Cells["B2"].Value = "Audit Log Report";

                                exWorkSheet.Cells["A3"].Style.Font.Bold = true;
                                exWorkSheet.Cells["A3"].Value = "Report Generated On:";

                                //exWorkSheet.Cells["B3:C3"].Merge = true;
                                exWorkSheet.Cells["B3"].Value = DateTime.Today.Date.ToString("dd-MMM-yyyy ");

                                exWorkSheet.Cells["A4"].LoadFromDataTable(ExcelData, true);
                                exWorkSheet.Cells["A4"].Style.Font.Bold = true;
                                exWorkSheet.Cells["A4"].Style.Font.Size = 12;
                                exWorkSheet.Cells["A4"].Value = "Sr.No.";
                                exWorkSheet.Cells["A4"].AutoFitColumns(20);

                                exWorkSheet.Cells["B4"].Style.Font.Bold = true;
                                exWorkSheet.Cells["B4"].Style.Font.Size = 12;
                                exWorkSheet.Cells["B4"].Value = "Created By";
                                exWorkSheet.Cells["B4"].AutoFitColumns(40);
                                 
                                exWorkSheet.Cells["C4"].Style.Font.Bold = true;
                                exWorkSheet.Cells["C4"].Style.Font.Size = 12;
                                exWorkSheet.Cells["C4"].Value = "Action taken";
                                exWorkSheet.Cells["C4"].AutoFitColumns(40);

                                exWorkSheet.Cells["D4"].Style.Font.Bold = true;
                                exWorkSheet.Cells["D4"].Style.Font.Size = 12;
                                exWorkSheet.Cells["D4"].Value = "Created On";
                                exWorkSheet.Cells["D4"].AutoFitColumns(25);
                              
                            }
                        }
                    }
                    using (ExcelRange col = exWorkSheet.Cells[1, 1, 4 + ExcelData.Rows.Count, 4])
                    {
                        col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;

                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                    }
                    using (ExcelRange col = exWorkSheet.Cells[1, 2, 4 + ExcelData.Rows.Count, 4])
                    {
                        col[1, 2, 4 + ExcelData.Rows.Count, 4].Style.Numberformat.Format = "dd/MMM/yyyy hh:mm:ss";
                    }

                    Byte[] fileBytes = exportPackge.GetAsByteArray();
                    Response.ClearContent();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=AuditLogDetails.xlsx");
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.ms-excel";
                    StringWriter sw = new StringWriter();
                    Response.BinaryWrite(fileBytes);
                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public static List<Cont_tbl_TermSheetPostComment> GetContractAuditLogs_All(long contractID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var lstAuditLogs = (from row in entities.Cont_tbl_TermSheetPostComment
                                    where row.ContractID==contractID
                                    select row).OrderBy(entry => entry.CreatedOn).ToList();
                return lstAuditLogs.ToList();
            }
        }

        protected void btnCreateContract_Click(object sender, EventArgs e)
        {
            long ConTermID = Convert.ToInt64(ViewState["ContractTermID"]);
            string ContractInstanceID = Getcontractno(Convert.ToInt32(ConTermID));
            //long ContractInstanceID=0;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowDialog(" + ConTermID + ");", true);
            
        }
        public static string Getcontractno(int contractInitiatorID)
        {
            using (ComplianceDBEntities entities=new ComplianceDBEntities())
            {
                var data = (from row in entities.tbl_Contract_TermSheet
                            where row.ID == contractInitiatorID
                            select row.Contract_no).FirstOrDefault();
                return data;
            }
        }
        public static string Existscontract(string contractno)
        {
            
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.Cont_tbl_ContractInstance
                            where row.ContractNo == contractno
                            select row.ContractNo).FirstOrDefault();
                return data;
            }
        }
        public static Boolean ExistscontractTermSheet(string contractno)
        {

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                Boolean result=false;
                var data = (from row in entities.tbl_Contract_TermSheet
                            where row.Contract_no== contractno
                            select row).FirstOrDefault();
                if(data!=null)
                {
                    result = true;
                        
                }
                return result;
            }
        }
        protected void btndraft_Click(object sender, EventArgs e)
        {
            //bool saveSuccess = false;
            bool saveSuccess = false;
            tbl_Contract_TermSheet tbl = new tbl_Contract_TermSheet()
            {
                IsContract_Summary = Convert.ToBoolean(ddlcontractsummmary.SelectedValue),
                IsSharedData = Convert.ToBoolean(ddlshared.SelectedValue),
                IsConfidentialdata1 = Convert.ToBoolean(ddlcondata1.SelectedValue),
                IsConfidentialdata2 = Convert.ToBoolean(ddlconfdata2.SelectedValue),
                IsRelatedParty = Convert.ToBoolean(ddlrelparty.SelectedValue),
                IsOutsourcing_contract = Convert.ToBoolean(ddloutscon.SelectedValue),
                IsEGIC_Additionaldetails = txtdetails4.Text,
                IsIntellectualshared_details = txtdetails5.Text,
                
            };
            if (!String.IsNullOrEmpty(txtcontractno.Text.Trim()))
            {
                tbl.Contract_no = txtcontractno.Text;
            }
            if (ddlvendor.SelectedValue != "" && ddlvendor.SelectedValue != "-1")
            {
                tbl.VendorId = Convert.ToInt32(ddlvendor.SelectedValue);
            }
            if (!String.IsNullOrEmpty(txtvendoraddress.Text.Trim()))
            {
                tbl.Vendor_Address = txtvendoraddress.Text;
            }
            if (!String.IsNullOrEmpty(txtscopeofservice.Text.Trim()))
            {
                tbl.Scope_of_service = txtscopeofservice.Text;
            }
            if (!String.IsNullOrEmpty(txtstartdate.Text.Trim()))
            {
                tbl.Startdate = DateTimeExtensions.GetDate(txtstartdate.Text);
            }
            if (!String.IsNullOrEmpty(txtenddate.Text.Trim()))
            {
                tbl.Enddate = DateTimeExtensions.GetDate(txtenddate.Text);
            }
            if (!String.IsNullOrEmpty(txtterm.Text.Trim()))
            {
                tbl.Term = txtterm.Text;
            }
            if (!String.IsNullOrEmpty(txtservicefees.Text.Trim()))
            {
                tbl.ServiceFees = Convert.ToInt32(txtservicefees.Text);
            }
            if (ddltermpayment.SelectedValue != "" && ddltermpayment.SelectedValue != "-1")
            {
                tbl.PaymentTermid = Convert.ToInt32(ddltermpayment.SelectedValue);
            }
            if(ddltaxation.SelectedValue!="" && ddltaxation.SelectedValue!="-1")
            {
                tbl.IsTaxation_instruction = Convert.ToBoolean(ddltaxation.SelectedValue);
            }
            if(!String.IsNullOrEmpty(txtdetails1.Text.Trim()))
            {
                tbl.Taxation_addDetails = txtdetails1.Text;
            }
            if(!String.IsNullOrEmpty(txtdetails2.Text.Trim()))
            {
                tbl.SharedData_adddetails = txtdetails2.Text;
            }
            if (!String.IsNullOrEmpty(txtdetails3.Text.Trim()))
            {
                tbl.Confidetialdata1_details = txtdetails3.Text;
            }
            if(tvFilterLocation.SelectedNode != null && tvFilterLocation.SelectedNode.Text != "Select Entity/Branch/Location")
            {
                tbl.branchid = Convert.ToInt32(tvFilterLocation.SelectedValue);
            }
            if(ddldepartment.SelectedValue!="" && ddldepartment.SelectedValue!="-1")
            {
                tbl.departmentid = Convert.ToInt32(ddldepartment.SelectedValue);
            }
            if(!String.IsNullOrEmpty(txtspocdesignation.Text.Trim()))
            {
                tbl.SPOC_Designation = txtspocdesignation.Text;
            }
            if(ddlEntity.SelectedValue!="" && ddlEntity.SelectedValue!="-1")
            {
                tbl.EntityTypeId = Convert.ToInt32(ddlEntity.SelectedValue);
            }
            if(!String.IsNullOrEmpty(tbxobjective.Text.Trim()))
            {
                tbl.Contract_objective = tbxobjective.Text;
            }
            if (!String.IsNullOrEmpty(txtdate.Text.Trim()))
            {
                tbl.todate = DateTimeExtensions.GetDate(txtdate.Text);
            }
            if (!String.IsNullOrEmpty(txtrisk.Text.Trim()))
            {
                tbl.Define_Risk = txtrisk.Text;
            }
            if (!String.IsNullOrEmpty(txtconrisk.Text.Trim()))
            {
                tbl.Consequence_defineRisk = txtconrisk.Text;
            }
            if (!String.IsNullOrEmpty(txtmitigationaction.Text.Trim()))
            {
                tbl.Risk_MitigationAction = txtmitigationaction.Text;
            }
            if (!String.IsNullOrEmpty(txtclauses.Text.Trim()))
            {
                tbl.Contract_Clauses = txtclauses.Text;
            }
            if (!String.IsNullOrEmpty(txtescprocess.Text.Trim()))
            {
                tbl.Escalation_process = txtescprocess.Text;
            }
            if (!String.IsNullOrEmpty(txttermplan1.Text.Trim()))
            {
                tbl.Exit_Termination_plan = txttermplan1.Text;
            }
            if (!String.IsNullOrEmpty(txttermplan2.Text.Trim()))
            {
                tbl.Termination_plan = txttermplan2.Text;
            }
            if (!String.IsNullOrEmpty(txttermplan2.Text.Trim()))
            {
                tbl.Audit_approval = txtauditappproval.Text;
            }
               
            if (ddlSPOCName.SelectedValue != "" && ddlSPOCName.SelectedValue != "-1")
            {
                tbl.SPOC_Nameid = Convert.ToInt32(ddlSPOCName.SelectedValue);
            }
            if (ddlattention.SelectedValue != "" && ddlattention.SelectedValue != "-1")
            {
                tbl.Attentionreq = Convert.ToInt32(ddlattention.SelectedValue);
            }
            if (ddlrisk1.SelectedValue != "")
            {
                tbl.IsRisk_assessment = Convert.ToBoolean(ddlrisk1.SelectedValue);
            }
            if (DropDownListChosen2.SelectedValue != "")
            {
                tbl.TerminationNoticeperiodId = Convert.ToInt32(DropDownListChosen2.SelectedValue);
            }
            if (ddlEGIC.SelectedValue != "")
            {
                tbl.IsEGIC = Convert.ToBoolean(ddlEGIC.SelectedValue);
            }
            if (DropDownListChosen1.SelectedValue != "")
            {
                tbl.IPR_Rightsid = Convert.ToInt32(DropDownListChosen1.SelectedValue);
            }
            if (ddlarrange.SelectedValue != "")
            {
                tbl.IsIntellectualshared = Convert.ToBoolean(ddlarrange.SelectedValue);
            }
            if (ddlrequestto.SelectedValue != "" && ddlrequestto.SelectedValue != "-1")
            {
                tbl.RequestTo = Convert.ToInt32(ddlrequestto.SelectedValue);
            }
            if (ddlApproveruser.SelectedValue != "" && ddlApproveruser.SelectedValue != "-1")
            {
                tbl.Approver = ddlApproveruser.SelectedItem.Text;
            }
               
            tbl.CreatedBy = AuthenticationHelper.UserID;
            tbl.CreatedOn = System.DateTime.Now;
            tbl.Isdeleted = false;

            if ((int)ViewState["Mode"] == 0)
            {
                CreateTermSheet(tbl);
                UploadDocuments(tbl.ID, tbl.Contract_no);
                cvContractterm.IsValid = false;
                cvContractterm.ErrorMessage = "Contract term sheet saved successfully.";
                ValidationSummary11.CssClass = "alert alert-success";
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "scrollUpPageScript", "scrollUpPage();", true);

            }
            else
            {
                tbl.ID = Convert.ToInt32(ViewState["ContractTermID"]);
                saveSuccess = UpdateContractTermSheet(tbl);
                UploadDocuments(tbl.ID, tbl.Contract_no);
                cvContractterm.IsValid = false;
                cvContractterm.ErrorMessage = "Contract term sheet Updated successfully.";
                ValidationSummary11.CssClass = "alert alert-success";
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "scrollUpPageScript", "scrollUpPage();", true);

            }
        }

        protected void upContractAuditLog_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvContractterm.IsValid = false;
                cvContractterm.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void UploadDocuments(int termsheetid,string contractno)
        {
            try
            {
                if ((int)ViewState["Mode"] == 1)
                {
                    //tbl_ContractTermSheetFileData tbl = ContractManagement.GetContractTermSheetDocumentId(termsheetid, contractno);

                    //if (tbl.ID != 0 && tbl.ID != null)
                    //{
                    //ContractManagement.DeleteContractTermDocument(tbl.ID);
                    //}
                }

                if (ViewState["ContractTermID"] != null)
                {
                    bool saveSuccess = false;
                    int conTermID = termsheetid;
                   
                    if (conTermID > 0)
                    {
                        #region Upload Document

                        if (fuSampleFile.HasFiles)
                        {
                            tbl_ContractTermSheetFileData objDoc = new tbl_ContractTermSheetFileData()
                            {
                                TermSheetNo = conTermID,
                                ContractNo = contractno,
                                CreatedBy = AuthenticationHelper.UserID,
                                CreatedByText = AuthenticationHelper.User,
                                IsDeleted = false,

                            };

                            HttpFileCollection fileCollection = Request.Files;

                            if (fileCollection.Count > 0)
                            {
                                List<KeyValuePair<string, Byte[]>> fileList = new List<KeyValuePair<string, Byte[]>>();

                                int customerID = -1;
                                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                                string directoryPath = "";
                                String fileName = "";

                                if (conTermID > 0)
                                {
                                    for (int i = 0; i < fileCollection.Count; i++)
                                    {
                                        HttpPostedFile uploadedFile = fileCollection[i];

                                        if (uploadedFile.ContentLength > 0)
                                        {
                                            string[] keys1 = fileCollection.Keys[i].Split('$');

                                            if (keys1[keys1.Count() - 1].Equals("fuSampleFile"))
                                            {
                                                fileName = uploadedFile.FileName;
                                            }

                                            objDoc.FileName = fileName;

                                            directoryPath = Server.MapPath("~/ContractTermSheetDocuments/" + customerID + "/" + contractno);

                                            if (!Directory.Exists(directoryPath))
                                                Directory.CreateDirectory(directoryPath);

                                            Guid fileKey1 = Guid.NewGuid();
                                            string finalPath1 = Path.Combine(directoryPath, fileKey1 + Path.GetExtension(uploadedFile.FileName));
                                            Stream fs = uploadedFile.InputStream;
                                            BinaryReader br = new BinaryReader(fs);
                                            Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                                            fileList.Add(new KeyValuePair<string, Byte[]>(finalPath1, bytes));

                                            objDoc.FilePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                            objDoc.FileKey = fileKey1.ToString();
                                            //objDoc.VersionDate = DateTime.Now;
                                            objDoc.CreatedOn = DateTime.Now;
                                            objDoc.FileSize = uploadedFile.ContentLength;
                                            TermSheet_SaveDocFiles(fileList);
                                            saveSuccess = CreateDocumentMapping(objDoc);

                                            fileList.Clear();
                                        }

                                    }//End For Each  

                                    if (saveSuccess)
                                    {
                                        BindTempDocumentData(Convert.ToInt64(AuthenticationHelper.UserID), Convert.ToInt64(AuthenticationHelper.CustomerID), Convert.ToInt32(conTermID));

                                    }
                                }
                            }

                            if (saveSuccess)
                            {
                                cvContractterm.IsValid = false;
                                cvContractterm.ErrorMessage = "Document(s) Uploaded Successfully";
                                ValidationSummary11.CssClass = "alert alert-success";
                            }
                            else
                            {
                                cvContractterm.IsValid = false;
                                cvContractterm.ErrorMessage = "Something went wrong, during document upload, Please try again";
                                ValidationSummary11.CssClass = "alert alert-danger";
                            }
                        }
                        else
                        {
                            cvContractterm.IsValid = false;
                            cvContractterm.ErrorMessage = "No document selected to upload";
                            ValidationSummary11.CssClass = "alert alert-danger";
                        }

                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
           
        }
       
        public static void TermSheet_SaveDocFiles(List<KeyValuePair<string, Byte[]>> filesList)
        {
            if (filesList != null)
            {
                foreach (var file in filesList)
                {
                    using (BinaryWriter Writer = new BinaryWriter(File.OpenWrite(file.Key)))
                    {
                        Writer.Write(CryptographyHandler.AESEncrypt(file.Value));
                        Writer.Flush();
                        Writer.Close();
                    }
                }
            }
        }
        public static bool CreateDocumentMapping(tbl_ContractTermSheetFileData newDoc)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    newDoc.EnType = "A";
                    entities.tbl_ContractTermSheetFileData.Add(newDoc);
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                 return false;
            }
        }

        public void BindTempDocumentData(long UserID, long CustID,long coninid)
        {
            try
            {
                grdDocument.DataSource = null;
                grdDocument.DataBind();

                if (coninid != null  && coninid > 0)
                {
                    //long ContID = Convert.ToInt64(ViewState["ContractTermID"]);
                    var DocData = DocumentManagement.GetContractTermSheetDocumentData(UserID, CustID, coninid);
                    if (DocData.Count > 0)
                    {
                        grdDocument.Visible = true;
                        grdDocument.DataSource = DocData;
                        grdDocument.DataBind();
                    }
                    else
                    {
                        grdDocument.Visible = false;
                    }
                }
               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvContractterm.IsValid = false;
                cvContractterm.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdDocument_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Delete Document"))
                {
                    long FileID = -1;
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    FileID = Convert.ToInt64(commandArgs[0]);
                    if (FileID > 0)
                    {
                        //if (ViewState["ContractTermID"] != "" && ViewState["ContractTermID"] != "-1" && Convert.ToInt32(ViewState["ContractTermID"]) > 0)
                        //{
                           ContractManagement.DeleteContractTermDocument(FileID);
                           BindTempDocumentData(Convert.ToInt64(AuthenticationHelper.UserID), Convert.ToInt64(AuthenticationHelper.CustomerID), Convert.ToInt32(ViewState["ContractTermID"]));
                           ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Document deleted successfully.')", true);
                            
                        //}

                       
                    }
                }
                else if (e.CommandName.Equals("Download Document"))
                {
                    long FileID = -1;
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    FileID = Convert.ToInt64(commandArgs[0]);
                    if (FileID > 0)
                    {
                        //if (ViewState["ContractTermID"] != "" && ViewState["ContractTermID"] != "-1" && Convert.ToInt32(ViewState["ContractTermID"]) > 0)
                        //{
                            using (ZipFile ComplianceZip = new ZipFile())
                            {
                                tbl_ContractTermSheetFileData file = ContractManagement.GetContractTermSheetDocument(FileID);
                                if (file != null)
                                {
                                    Response.Buffer = true;
                                    Response.ClearContent();
                                    Response.ClearHeaders();
                                    Response.Clear();
                                    Response.ContentType = "application/octet-stream";
                                    Response.AddHeader("content-disposition", "attachment; filename= " + file.FileName);
                                    Response.TransmitFile(Server.MapPath(file.FilePath));
                                    Response.Flush();
                                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                                }
                            }
                            BindTempDocumentData(Convert.ToInt64(AuthenticationHelper.User), Convert.ToInt64(AuthenticationHelper.CustomerID), Convert.ToInt32(ViewState["ContractTermID"]));
                        //}
                        
                    }
                }
                else if (e.CommandName.Equals("View Document"))
                {
                    

                    var AllinOneDocumentList = ContractDocumentManagement.GetContractTermSheetDocumentByID(Convert.ToInt64(e.CommandArgument));

                    if (AllinOneDocumentList != null)
                    {
                        DocumentPath = string.Empty;

                        string filePath = Path.Combine(Server.MapPath(AllinOneDocumentList.FilePath), AllinOneDocumentList.FileKey + Path.GetExtension(AllinOneDocumentList.FileName));
                        if (AllinOneDocumentList.FilePath != null && File.Exists(filePath))
                        {
                            string fileExtension = System.IO.Path.GetExtension(filePath);

                            if (fileExtension.ToUpper() == ".ZIP" || fileExtension.ToUpper() == ".7Z" || fileExtension.ToUpper() == ".RAR")
                            {
                                lblMessage.Text = "";
                                lblMessage.Text = "Compressed file(s) can not be preview, Please try to download file(s)";
                            }
                            else
                            {
                                string Folder = "~/TempFiles";
                                string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                                string DateFolder = Folder + "/" + File;
                                string extension = System.IO.Path.GetExtension(filePath);
                                Directory.CreateDirectory(Server.MapPath(DateFolder));
                                if (!Directory.Exists(DateFolder))
                                {
                                    Directory.CreateDirectory(Server.MapPath(DateFolder));
                                }
                                string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);
                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;
                                string FileName = DateFolder + "/" + User + "" + extension;
                                FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                BinaryWriter bw = new BinaryWriter(fs);
                                if (AllinOneDocumentList.EnType == "M")
                                {
                                    bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                }
                                else
                                {
                                    bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                }
                                bw.Close();
                                DocumentPath = FileName;
                                DocumentPath = DocumentPath.Substring(2, DocumentPath.Length - 2);
                            }

                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenDocviewer('" + DocumentPath + "');", true);
                        }
                        else
                        {
                            lblMessage.Text = "There is no file to preview";
                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenDocviewer('" + DocumentPath + "');", true);
                        }
                    }
                }
                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fCalltreeCollapsed();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvContractterm.IsValid = false;
                cvContractterm.ErrorMessage = "Server Error Occured. Please try again."; ;
            }
        }

        protected void grdDocument_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

    }
}