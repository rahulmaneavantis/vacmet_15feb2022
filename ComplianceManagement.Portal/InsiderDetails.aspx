﻿<%@ Page Title="" Language="C#" MasterPageFile="~/InsiderTrade.Master" AutoEventWireup="true" CodeBehind="InsiderDetails.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.InsiderDetails" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     

    <link href="NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />
        
   
    <script type="text/javascript" src="Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="Newjs/jszip.min.js"></script>

    <style type="text/css">
    .k-calendar td.k-state-focused.k-state-selected{
        background-color:blue;
    }
    .k-i-arrow-60-down{
        margin-top:5px;
    }
    .k-i-calendar{
        margin-top:5px;
    }
    .k-grid-search {
        float:left !important;
    }
    .k-grid{
        padding:0;
    }
    .customer-photo {
        display: inline-block;
        width: 32px;
        height: 32px;
        border-radius: 50%;
        background-size: 32px 35px;
        background-position: center center;
        vertical-align: middle;
        line-height: 32px;
        box-shadow: inset 0 0 1px #999, inset 0 0 10px rgba(0,0,0,.2);
        margin-left: 5px;
        cursor:pointer;
    }

    .customer-name {
        display: inline-block;
        vertical-align: middle;
        line-height: 32px;
        padding-left: 3px;
    }
    .k-i-clock {
        margin-top:8px;
    }
</style>
    <style>
        body{
        color:#666;
        font-weight:300;
    }
        .form-group.required .control-label:after { 
   content:"*";
   color:red;
}
        html .k-grid tr:hover {
    background: #E4F7FB;
}
         .k-state-focused.k-state-selected{
              background: transparent;
              color:#666;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="col-sm-12">
        <div class="col-sm-12" style="padding:0">
            <div class="toolbar">  
                    <label class="control-label" style="font-weight:450;padding-top: 5px;" for="sel1">Select Company: &nbsp; </label>
                    <input id="dropdownlistComplianceType" data-placeholder="Type" style="width:172px;">
                
                    <button  style="float:right;display:none"  id="button1" type="button" class="btn btn-primary"  onclick="window.open('InsiderHistoricalData.aspx', '_self');" >Historical Data</button> 
                    <button  style="float:right;margin-right:1%;"  id="button" type="button" class="btn btn-primary"  onclick="OpenAdvanceSearch(event)" >Add New</button> 
            </div>
        </div>
        <div class="col-sm-12" style="margin-bottom:2%;" >
            
        </div>

        <div class="col-sm-12" id="grid"></div>
    </div>

   


     
    <div id="divAdvanceSearchModel" style="padding-top: 5px;z-index: 999;display:none;">
        <div  class="col-sm-12" style="padding:0">
            <form  class="col-sm-12 form-inline" method="POST" id="fcontact"  >
                <div class="col-sm-12" style="padding:0;padding-top:10px;">
                    <div class="col-sm-6  required " style="padding:0">
                        <label class="control-label col-sm-4" style="font-weight:450;padding-top: 5px;" for="sel1">Select Company: &nbsp; </label>
                        <input id="dropdownlistComplianceType1" style="width:40%" data-placeholder="Company" >
                    </div>
                    <div class="col-sm-6  required " style="padding:0">
                        <label class="control-label col-sm-4" style="font-weight:450;padding-top: 5px;" for="firstName">Name:</label>
                        <input  id="dropdownName" style="width:40%" data-placeholder="Name" />
                    </div>
                    <div class="col-sm-6" style="padding:0;margin-top:5px;">
                        <label class=" control-label col-sm-4" style="font-weight:450;padding-top: 5px;" for="sel1">Select Type of UPSI: &nbsp; </label>
                        <input  id="dropdownUPSI" style="width:40%" data-placeholder="UPSI Type" >
                    </div>
                    <div class="col-sm-6" style="padding:0;margin-top:5px;">
                        <label class="control-label col-sm-4" style="font-weight:450;padding-top: 5px;" for="firstName">Information Type:</label>
                        <span class="col-sm-8 k-widget k-textbox" style="width:40%" ><input id="informationtype" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="" autocomplete="off" ></span>
                    </div>
                    <div class="col-sm-6" style="padding:0;margin-top:5px;">
                        <label class="col-sm-4 control-label" style="font-weight:450;padding-top: 5px;" for="sel2">Date of Information Shared: </label>
                        <input id="datepicker" style="width:40%" value="10/10/2020" title="datepicker"  />
                    </div>
                    <div class="col-sm-6" style="padding:0;margin-top:5px;">
                        <label class="col-sm-4 control-label" style="font-weight:450;padding-top: 5px;" for="sel2">Time of Information Shared: </label>
                        <input id="timepicker" style="width:40%" value="10:00" title="timepicker"  />
                    </div>
               
                    <div class="col-sm-6" style="padding:0;margin-top:5px;">
                        <label class="col-sm-4 control-label" style="font-weight:450;padding-top: 5px;" for="sel2">Information Shared:&nbsp; </label>
                        <input id="dropdownShared" style="width:40%" data-placeholder="SharedWith" >
                    </div>
                    <div class="col-sm-12" style="padding:0;margin-top:5px;display:inline-block" id="internalType">
                        <div class="col-sm-6" style="padding:0">
                            <label class="col-sm-4 control-label" style="font-weight:450;padding-top: 5px;" for="firstName">Shared With:</label>
                            <input id="dropdownCName" style="width:40%" data-placeholder="Name">
                        </div>
                    </div>
                    <div class="col-sm-12" style="padding:0;display:inline-block" id="internalOtherType">
                        <div class="col-sm-6" style="padding:0;margin-top:5px;">
                            <label class="col-sm-4 control-label" style="font-weight:450;padding-top: 5px;" for="firstName">Name:</label>
                            <span class="col-sm-8 k-widget k-textbox" style="width:40%"><input id="othname" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="Name" autocomplete="off" ></span>
                        </div>
                        <div class="col-sm-6" style="padding:0;margin-top:5px;">
                            <label class="col-sm-4 control-label" style="font-weight:450;padding-top: 5px;" for="firstName">Designation:</label>
                            <span class="col-sm-8 k-widget k-textbox" style="width:40%"><input id="othdesignation" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="Designation" autocomplete="off" ></span>
                        </div>
                        <div class="col-sm-6" style="padding:0;margin-top:5px;">
                            <label class="col-sm-4 control-label" style="font-weight:450;padding-top: 5px;" for="firstName">ID Type:</label>
                            <input id="internalidtype" style="width:40%;padding:0" data-placeholder="PAN" >
                        </div>
                        <div class="col-sm-6" id="internalname" style="padding:0;margin-top:5px;display:none;">
                            <label class="col-sm-4 control-label" style="font-weight:450;padding-top: 5px;" for="firstName">ID Name:</label>
                            <span class="col-sm-8 k-widget k-textbox" style="width:40%"><input id="internalothidname" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="ID Name" autocomplete="off" ></span>
                        </div>
                        <div class="col-sm-6" id="internalcode" style="padding:0;margin-top:5px;display:none;">
                            <label class="col-sm-4 control-label" style="font-weight:450;padding-top: 5px;" for="firstName">ID Code:</label>
                            <span class="col-sm-8 k-widget k-textbox" style="width:40%"><input id="internalothidcode" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="ID Code" autocomplete="off" ></span>
                        </div>
                        <div class="col-sm-6" id="internalpan" style="padding:0;margin-top:5px;">
                            <label class="col-sm-4 control-label" style="font-weight:450;padding-top: 5px;" for="firstName">PAN:</label>
                            <span class="col-sm-8 k-widget k-textbox" style="width:40%"><input id="othpan" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="PAN" autocomplete="off" ></span>
                        </div>
                        <div class="col-sm-6" style="padding:0;margin-top:5px;">
                            <label class="col-sm-4 control-label" style="font-weight:450;padding-top: 5px;" for="firstName">Department:</label>
                            <span class="col-sm-8 k-widget k-textbox" style="width:40%"><input id="othdepartment" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="Department" autocomplete="off" ></span>
                        </div>
                        <div class="col-sm-6" style="padding:0;margin-top:5px;">
                            <label class="col-sm-4 control-label" style="font-weight:450;padding-top: 5px;" for="firstName">Relation:</label>
                            <span class="col-sm-8 k-widget k-textbox" style="width:40%"><input id="othrelation" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="Relation" autocomplete="off" ></span>
                        </div>
                    </div>
                    <div class="col-sm-12" style="padding:0;display:none" id="auditorType">
                        <div class="col-sm-6" style="padding:0;margin-top:5px;">
                            <label class="col-sm-4 control-label" style="font-weight:450;padding-top: 5px;" for="sel1">Select FirmName: &nbsp; </label>
                            <input  id="dropdownFirmName" style="width:40%" data-placeholder="Auditor" >
                        </div>
                        <div class="col-sm-6" style="padding:0;margin-top:5px;">
                            <label class="col-sm-4 control-label" style="font-weight:450;padding-top: 5px;" for="sel1">Select Auditor: &nbsp; </label>
                            <input id="dropdownAuditor" style="width:40%" data-placeholder="Auditor" >
                        </div>
                        <div class="col-sm-6" style="padding:0;margin-top:5px;">
                            <label class="col-sm-4 control-label" style="font-weight:450;padding-top: 5px;" for="firstName">Auditor Type:</label>
                            <span class="col-sm-8 k-widget k-textbox" style="width:40%" ><input id="dropdownauditorrelation" style="width: 100%;background-color: #0000001c" data-role="textbox" aria-disabled="false" class="k-input" data-placeholder="Company"  autocomplete="off" disabled ></span>
                            <%--<input id="dropdownauditorrelation" style="width:40%" data-placeholder="Company" disabled >--%>
                        </div>
                        <div class="col-sm-6" id="dothauditorname" style="padding:0;margin-top:5px;">
                            <label class="col-sm-4 control-label" style="font-weight:450;padding-top: 5px;" for="firstName">Auditor Name:</label>
                            <span class="col-sm-8 k-widget k-textbox" style="width:40%"><input id="othauditorname" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="Name" autocomplete="off" ></span>
                        </div>
                        <div id="auddesignation" class="col-sm-6" style="padding:0;margin-top:5px;">
                            <label class="col-sm-4 control-label" style="font-weight:450;padding-top: 5px;" for="firstName">Designation:</label>
                            <span class="col-sm-8 k-widget k-textbox" style="width:40%"><input id="auddesignationinp" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="Designation" autocomplete="off" ></span>
                        </div>
                        <div class="col-sm-6" id="dootherauditoridtype" style="padding:0;margin-top:5px;">
                            <label class="col-sm-4 control-label" style="font-weight:450;padding-top: 5px;" for="firstName">ID Type:</label>
                            <input id="auditoridtype" style="width:40%;padding:0" data-placeholder="PAN" >
                        </div>
                        <div class="col-sm-6" id="oauditorname" style="padding:0;margin-top:5px;display:none;">
                            <label class="col-sm-4 control-label" style="font-weight:450;padding-top: 5px;" for="firstName">ID Name:</label>
                            <span class="col-sm-8 k-widget k-textbox" style="width:40%"><input id="auditorothidname" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="ID Name" autocomplete="off" ></span>
                        </div>
                        <div class="col-sm-6" id="oauditorcode" style="padding:0;margin-top:5px;display:none;">
                            <label class="col-sm-4 control-label" style="font-weight:450;padding-top: 5px;" for="firstName">ID Code:</label>
                            <span class="col-sm-8 k-widget k-textbox" style="width:40%"><input id="auditorothidcode" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="ID Code" autocomplete="off" ></span>
                        </div>
                        <div class="col-sm-6" id="oauditorpan" style="padding:0;margin-top:5px;">
                            <label class="col-sm-4 control-label" style="font-weight:450;padding-top: 5px;" for="firstName">PAN:</label>
                            <span class="col-sm-8 k-widget k-textbox" style="width:40%"><input id="othauditorpan" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="PAN" autocomplete="off" ></span>
                        </div>
                    </div>
                    <div class="col-sm-12" style="padding:0;display:none;" id="otherType">
                        <div class="col-sm-6" style="padding:0;margin-top:5px;">
                            <label class="col-sm-4 control-label" style="font-weight:450;padding-top: 5px;" for="firstName">Company:</label>
                            <input id="dropdownlistCompany" style="width:40%" data-placeholder="Type" >
                        </div>
                        <div class="col-sm-6" style="padding:0;margin-top:5px;">
                            <label class="col-sm-4 control-label" style="font-weight:450;padding-top: 5px;" for="firstName">Shared With:</label>
                            <span class="col-sm-8 k-widget k-textbox" style="width:40%" ><input id="oconcernedname" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="Shared With" autocomplete="off" ></span>
                        </div>
                        <div class="col-sm-6" style="padding:0;margin-top:5px;">
                            <label class="col-sm-4 control-label" style="font-weight:450;padding-top: 5px;" for="firstName">Designation:</label>
                            <span class="col-sm-8 k-widget k-textbox" style="width:40%"><input id="odesignation" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="Designation" autocomplete="off" ></span>
                        </div>
                        <div class="col-sm-6" style="padding:0;margin-top:5px;">
                            <label class="col-sm-4 control-label" style="font-weight:450;padding-top: 5px;" for="firstName">ID Type:</label>
                            <input id="otheridtype" style="width:40%;padding:0" data-placeholder="PAN" >
                        </div>
                        <div class="col-sm-6" id="othername" style="padding:0;margin-top:5px;display:none;">
                            <label class="col-sm-4 control-label" style="font-weight:450;padding-top: 5px;" for="firstName">ID Name:</label>
                            <span class="col-sm-8 k-widget k-textbox" style="width:40%"><input id="otheridname" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="ID Name" autocomplete="off" ></span>
                        </div>
                        <div class="col-sm-6" id="othercode" style="padding:0;margin-top:5px;display:none;">
                            <label class="col-sm-4 control-label" style="font-weight:450;padding-top: 5px;" for="firstName">ID Code:</label>
                            <span class="col-sm-8 k-widget k-textbox" style="width:40%"><input id="otheridcode" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="ID Code" autocomplete="off" ></span>
                        </div>
                        <div class="col-sm-6" id="otherpan" style="padding:0;margin-top:5px;">
                            <label class="col-sm-4 control-label" style="font-weight:450;padding-top: 5px;" for="firstName">PAN:</label>
                            <span class="col-sm-8 k-widget k-textbox" style="width:40%" ><input id="opan" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="PAN" autocomplete="off" ></span>
                        </div>
                    </div>
                    <div class="col-sm-12" style="padding:0;">
                        <div id="reasondiv" class="col-sm-6" style="padding:0;margin-top:5px;">
                            <label class="col-sm-4 control-label" style="font-weight:450;padding-top: 5px;" for="firstName">Reason:</label>
                            <span class="col-sm-8 k-widget k-textbox" style="width:40%" > <textarea id="reason" style="width: 100%;" required data-required-msg="Please enter a text." data-max-msg="Enter value between 1 and 200" ></textarea></span>
                        </div>
                    </div>
                 </div>
                 <button id="saveBtn" style="margin-top:86px; type="submit" onclick="saveDetails(event)" class="btn btn-primary"  disabled>Save</button>
              </form>
              <label class="control-label" id="checkA" style="margin-left:15px;margin-top:-60px;font-weight:450;"><input type="checkbox" id="showcheckbox" value="1" onclick="showBtn()"> I confirm that details furnished above are true and correct to the best of my knowledge</label>
            </div>
        </div>

        <script type="text/javascript">
            $(document).ready(function () {
                

                var xx = $('#dropdownlistComplianceType').val();
               // alert(xx)
               
                $("#grid").kendoGrid({
                    
                    dataSource: {
                        transport: {
                            read: '<%=ConfigurationManager.AppSettings["insiderapi"]%>getdetails/?user_id=' +<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID %>+'&customer_id=' +<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID %>+'&fcompanyid=' + xx
                        },
                        schema: {
                            data: function (response) {
                                return response.data;
                            },
                            total: function (response) {
                                return response.total;
                            },
                            model: {
                                fields: {
                                    name: { type: "string" },
                                    upsitype: { type: "string" },
                                    InformationSharedDate: { type: "date" },
                                    InformationSharedTime: { type: "string" },
                                    ConcernedName: { type: "string" },
                                    FirmName: { type: "string" },
                                    ConcernedDesignation: { type: "string" },
                                    EmployeeID: { type: "string" },
                                    IdType: { type: "string" },
                                    Reason: { type: "string" },
                                }
                            }
                        },
                        pageSize: 7    
                    },
                    sortable: {
                        mode: "single",
                        allowUnsort: false
                    },
                    //height: 550,
                    //toolbar: ["search"],
                    filterable: true,
                    pageable: {
                        refresh: true,
                        pageSizes: true,
                        buttonCount: 5
                    },
                    columns: [{
                        template: "<div class='customer-name'>#: name #</div>",
                        field: "name",
                        title: "Name",
                        //locked: true,
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        },
                        width: 140
                    }, {
                            field: "upsitype",
                            title: "UPSI Type",
                           // locked: true,
                            filterable: {
                                multi: true,
                                extra: false,
                                search: true,
                                operators: {
                                    string: {
                                        eq: "Is equal to",
                                        neq: "Is not equal to",
                                        contains: "Contains"
                                    }
                                }
                            },
                            width: 140
                    }, {
                            field: "InformationType",
                            title: "Information Type",
                            filterable: false,
                            width: 140
                    }, {

                        field: "InformationSharedDate",
                            title: "Shared Date",
                            format: "{0:dd/MM/yyyy}" ,
                            filterable: false,
                        width: 150

                    }, {

                             field: "InformationSharedTime",
                             title: "Shared Time",
                             //format: "{0:dd/MM/yyyy}" ,
                             filterable: false,
                             width: 140

                         },{

                            field: "ConcernedName",
                            title: "Shared With",
                            filterable: false,
                            width: 150

                        }, {

                            field: "IdType",
                            title: "ID Name",
                            filterable: false,
                            width: 150

                        }
                        , {

                            field: "EmployeeID",
                            title: "ID Code",
                            filterable: false,
                            width: 150

                        }, {

                            field: "ConcernedDesignation",
                            title: "Designation",
                            filterable: false,
                            width: 150

                        }, {

                            field: "ConcernedDepartment",
                            title: "Department",
                            filterable: false,
                            width: 150

                        }, {

                            field: "FirmName",
                            title: "Company/Firm",
                            filterable: false,
                            width: 150

                        }, {

                            field: "ConcernedRelation",
                            title: "Relation",
                            filterable: false,
                            width: 150

                        },{

                            field: "Reason",
                            title: "Remark",
                            filterable: false,
                            width: 150

                        }]
                });


            });
        </script>


    <script type="text/javascript">
        function setAuditorRelation(cid, firm) {

            $.ajax({
                type: 'post',
                url: "<%=ConfigurationManager.AppSettings["insiderapi"]%>getfirmpcspecific/?customer_id=" +<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID %>,
                    data: { "company": cid, "firm": firm },
                    success: function (result) {
                        //alert(JSON.stringify(result[0].AuditorType));

                        if (result.length > 0) {
                            $("#dropdownauditorrelation").val(result[0].AuditorType);
                           // var xxdropdownlist1 = $("#dropdownauditorrelation");
                            //var xxdropdownlist1 = $("#dropdownauditorrelation").data("kendoTextBox")
                           // xxdropdownlist1.value(result[0].AuditorType);
                            
                           // xxdropdownlist1.enable(false);

                        }


                    },
                    error: function (e, t) { alert('something went wrong'); }
                });
        };

        function openPoPup() {
            $('#videoModal1').modal('show');
        }
    </script>
    
    <script type="text/javascript">
        $(document).ready(function () {
            

            // create DatePicker from input HTML element
            $("#datepicker").kendoDatePicker();
            var todayDate = new Date();
            //$('#datepicker').data("kendoDatePicker").value(todayDate);
            $('#datepicker').kendoDatePicker({
                value: todayDate,
                max: todayDate,
                format: "dd/MM/yyyy"
            }).data('kendoDatePicker');
            $("#datepicker").closest("span.k-datepicker").width('40%');
            $("#timepicker").kendoTimePicker({
                dateInput: true,
                format:"HH:mm",
                timeFormat: "HH:mm",
            });
            $("#pagetype").text("My Insider Details");
            $("#dropdownlistComplianceType").kendoDropDownList({
                dataTextField: "Name",
                dataValueField: "ID",

                dataSource: {
                    transport: {
                        read: {
                            url: "<%=ConfigurationManager.AppSettings["insiderapi"]%>getcompanyall/?customer_id=<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID %>",
                        }
                    }
                },
               
                change: function (e) {
                    var value = this.value();
                   // alert(value)
                    var dataSource = new kendo.data.DataSource({
                        
                            transport: {
                            read: '<%=ConfigurationManager.AppSettings["insiderapi"]%>getdetails/?customer_id=' +<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID %>+"&user_id=" +<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID %>+'&fcompanyid='+this.value()
                        },
                        schema: {
                            data: function (response) {
                                return response.data;
                            },
                            total: function (response) {
                                return response.total;
                            },
                            model: {
                                fields: {
                                    name: { type: "string" },
                                    upsitype: { type: "string" },
                                    InformationSharedDate: { type: "date" },
                                    ConcernedName: { type: "string" },
                                    FirmName: { type: "string" },
                                    ConcernedDesignation: { type: "string" },
                                    EmployeeID: { type: "string" },
                                    Reason: { type: "string" },
                                    IdType: { type: "string" },
                                }
                            }
                        },
                        pageSize: 7
                            
                        
                    });
                    var grid = $("#grid").data("kendoGrid");
                    grid.setDataSource(dataSource);
                    // Use the value of the widget
                }
            });
            $("#dropdownlistComplianceType").data('kendoDropDownList').select(0);
            $("#dropdownlistComplianceType1").kendoDropDownList({
                dataTextField: "Name",
                dataValueField: "ID",
                dataSource: {
                    transport: {
                        read: {
                            url: "<%=ConfigurationManager.AppSettings["insiderapi"]%>getcompany/?customer_id=<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID %>",
                        }
                    }
                },
                dataBound: function (e) {
                    loadfirm();
                }
            });
            

            $("#dropdownUPSI").kendoDropDownList({
                dataTextField: "UPSIName",
                dataValueField: "ID",
                dataSource: {
                    transport: {
                        read: {
                            url: "<%=ConfigurationManager.AppSettings["insiderapi"]%>getupsi/?customer_id=<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID %>",
                        }
                    }
                }
            });
            $("#dropdownShared").kendoDropDownList({
                placeholder: "",
                dataTextField: "text",
                dataValueField: "value",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                dataSource: [
                    { text: "Shared Internally", value: "1" },
                    { text: "Auditor", value: "2" },
                    { text: "Other", value: "3" }
                    ],
                index: 0,
                change: function (e) {
                    $('#checkA').css('margin-left', '15px');
                    $('#checkA').css('margin-top', '-60px');
                    if (e.sender.value() == 2) {
                        loadfirm()
                        $('#internalType').hide();
                        $('#otherType').hide();
                        $('#internalOtherType').hide();
                        $('#saveBtn').css('margin-left', '0');
                        $('#saveBtn').css('margin-top', '86px');
                        $('#checkA').css('margin-left', '15px');
                        $('#checkA').css('margin-top', '-60px');
                        /*
                        $('#reasondiv').css('margin-top', 14);
                        $('#reasondiv').css('margin-left', '-61%');
                        $('#saveBtn').css('margin-top', 220);
                        $('#saveBtn').css('margin-left', '-61.4%');
                        */
                        $('#auditorType').show();
                        
                    } else if (e.sender.value() == 3) {
                        $('#auditorType').hide();
                        $('#internalType').hide();

                        $('#internalOtherType').hide();
                        /*
                        $('#reasondiv').css('margin-top', '-30px');
                        $('#reasondiv').css('margin-left', 0);
                        $('#saveBtn').css('margin-top', 50);
                        $('#saveBtn').css('margin-left', '1.4%');
                        */
                        $('#saveBtn').css('margin-left', '0');
                        $('#saveBtn').css('margin-top', '86px');
                        $('#checkA').css('margin-left', '15px');
                        $('#checkA').css('margin-top', '-60px');
                        $('#otherType').show();
                    } else {
                        $('#auditorType').hide();
                        $('#otherType').hide();
                        /*
                        $('#reasondiv').css('margin-top', 0);
                        $('#reasondiv').css('margin-left', 0);
                        $('#saveBtn').css('margin-left', '-61.4%');
                        $('#saveBtn').css('margin-top', 243);
                        */
                        $('#saveBtn').css('margin-left', '0');
                        $('#saveBtn').css('margin-top', '86px');
                        $('#checkA').css('margin-left', '15px');
                        $('#checkA').css('margin-top', '-60px');
                        $('#internalType').show();

                        $('#internalOtherType').show();
                    }
                    
                }
                
            });
            
            $("#dropdownName").kendoDropDownList({
                dataTextField: "Name",
                dataValueField: "ID",
                dataSource: {
                    transport: {
                        read: {
                            url: "<%=ConfigurationManager.AppSettings["insiderapi"]%>getuserp/?customer_id=<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID %>",
                        }
                    }
                }
            });

            $("#dropdownCName").kendoDropDownList({
                dataTextField: "Name",
                dataValueField: "ID",
                dataSource: {
                    transport: {
                        read: {
                            url: "<%=ConfigurationManager.AppSettings["insiderapi"]%>getuserpother/?customer_id=<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID %>",
                        }
                    }
                },
                change: function (e) {
                    if (e.sender.value() == -1) {
                        $('#internalOtherType').show();
                    } else{
                        $('#internalOtherType').hide();
                    }
                }
            });
            $("#dropdownlistCompany").kendoDropDownList({
                dataTextField: "Name",
                dataValueField: "ID",
                dataSource: {
                    transport: {
                        read: {
                            url: "<%=ConfigurationManager.AppSettings["insiderapi"]%>getcompp/?customer_id=<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID %>",
                        }
                    }
                }
            });
            var dropdownlist1 = $("#dropdownName").data("kendoDropDownList");
            dropdownlist1.value(<% =com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID %>);
            <%    if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.Role != "HDCS") {  %>
            dropdownlist1.enable(false);
            

            <% } %>
            /*
            $("#dropdownlistComplianceType").kendoDropDownList({
                placeholder: "Select Company",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                dataTextField: "ProductName",
                dataValueField: "ProductID",
                dataSource: {
                    transport: {
                        read: {
                            dataType: "jsonp",
                            url: "https://demos.telerik.com/kendo-ui/service/Products",
                        }
                    }
                }
                index: 0,
                change: function (e) {
                    DataBindDaynamicKendoGriddMain();
                }
            });
            */
            
        });
        function DataBindDaynamicKendoGriddMain() {
            alert('coming3');
        }
    </script>
    
    <script>

    function OpenAdvanceSearch(e) {

            var myWindowAdv = $("#divAdvanceSearchModel");

            function onClose() {

            }

            myWindowAdv.kendoWindow({
                width: "85%",
                height: "68%",
                title: "Add New ",
                visible: false,
                actions: [
                    //"Pin",
                    //"Minimize",
                    "Maximize",
                    "Close"
                ],
                close: onClose
            });
             $("#divAdvanceSearchModel").data("kendoWindow").wrapper.addClass("myKendoCustomClass");

            myWindowAdv.data("kendoWindow").center().open();
            e.preventDefault();
            return false;
        }

        function OpenAdvanceSearchFilter(e) {
            $('#divAdvanceSearchFilterModel').modal('show');
            e.preventDefault();
            return false;
        }
        </script>

    <script>
        function loadfirm() {
            var comid = $('#dropdownlistComplianceType1').val();
            //alert('comid',comid)
            
            $("#dropdownFirmName").kendoDropDownList({
                dataTextField: "FirmName",
                dataValueField: "FirmName",
                dataSource: {
                    transport: {
                        read: {
                            url: "<%=ConfigurationManager.AppSettings["insiderapi"]%>getfirmpc/?customer_id=" +<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID %>+"&company=" + comid,
                        }
                    }
                },
                dataBound: function (e) {
                    loadauditor();
                    
                },
                change: function (e) {
                    //alert(e.sender.value());
                    var adropdownlist1 = $("#dropdownAuditor").data("kendoDropDownList");
                    var xdataSource = new kendo.data.DataSource({
                        transport: {
                            read: "<%=ConfigurationManager.AppSettings["insiderapi"]%>getauditordetailfirm/?customer_id=<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID %>" + "&firm=" + e.sender.value(),
                        }
                    });
                    adropdownlist1.setDataSource(xdataSource);
                    var comid = e.sender.value();
                    var comid1 = $('#dropdownlistComplianceType1').val();
                    setAuditorRelation(comid1, comid);
                    /*
                    if (e.sender.value() == -1) {
                    }
                    */
                }
            });

        }
    </script>
    <script>
        function loadauditor() {
            var comid = $('#dropdownFirmName').val();

            $("#dropdownAuditor").kendoDropDownList({
                dataTextField: "AuditorName",
                dataValueField: "ID",
                dataSource: {
                    transport: {
                        read: {
                            url: "<%=ConfigurationManager.AppSettings["insiderapi"]%>getauditordetailfirm/?customer_id=<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID %>" + "&firm=" + comid,
                        }
                    }
                },
                change: function (e) {
                    var xxdropdownlist12 = $("#dropdownauditorrelation");
                    // xxdropdownlist12.enable(true);
                    if (e.sender.value() == -1) {
                       // alert(e.sender.value())
                        $('#saveBtn').css('margin-top', '86px');
                        $('#saveBtn').css('margin-left', '0');

                        // $('#dothauditorname').show();
                        // $('#dothauditorpan').show();
                        //$('#auddesignation').css("visibility", "visible");
                        $('#auddesignation').show();
                        $('#dootherauditoridtype').show();
                        $('#oauditorpan').show();
                        //$('#oauditorname').show();
                        //$('#oauditorcode').show();

                        $('#dothauditorname').show();
                        $('#dothauditorpan').show();

                    } else {
                        //alert(JSON.stringify(e.sender.value());
                        //alert(e.sender.value())

                        $('#saveBtn').css('margin-top', '86px');
                        $('#saveBtn').css('margin-left', '0');
                        $('#checkA').css('margin-left', '15px');
                        //$('#auddesignation').css("visibility", "hidden");
                        $('#auddesignation').hide();
                        $('#dootherauditoridtype').hide();
                        $('#oauditorpan').hide();
                        $('#oauditorname').hide();
                        $('#oauditorcode').hide();
                        $('#dothauditorname').hide();
                        $('#dothauditorpan').hide();
                        var comid1 = $('#dropdownlistComplianceType1').val();
                        
                        //setAuditorRelation(comid1,comid);
                    }
                }
            });
            //var comid1 = $('#dropdownlistComplianceType1').val();
            //setAuditorRelation(comid1, comid);
        }
    </script>
    <script>
    function saveDetails(e){
        e.preventDefault();
        //confirm("Are you sure want to add details?");

        var user_id = $("#dropdownName").val();
        var customer_id = <%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID %>;
        var detailcreated_id =<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID %>;
        var upsitype_id = $("#dropdownUPSI").val();
        var information_type = $('#informationtype').val();
        var information_shareddate = $('#datepicker').val();
        var information_sharedtime = $('#timepicker').val();
        console.log("information_sharedtime", information_sharedtime);
        var sharedwithtype = $('#dropdownShared').val();
        var concernedname = '';
        var oconcernedname = '';
        concernedname = $('#concernedname').val();
        var auditorid = $('#dropdownAuditor').val();
        var firmname = $('#dropdownlistCompany').val();
        oconcernedname = $('#oconcernedname').val();
        var concerneddesignation = $('#designation').val();
        var oconcerneddesignation = $('#odesignation').val();
        var concerneddepartment = '';
        var xflag = '';
        var othrelation = '';
        var idType = '';
        var afirmname = '';
        if (sharedwithtype == 1) {
            concernedname = $('#dropdownCName').val();

            var employeeid = $('#pan').val();
            if (concernedname == '-1') {
                concernedname = $('#othname').val();
                concerneddesignation = $('#othdesignation').val();
                concerneddepartment = $('#othdepartment').val();
                xflag = 'other';
                othrelation = $('#othrelation').val();
                idType = $('#internalidtype').val();
                if (idType == 'PAN') {
                    employeeid = $('#othpan').val();
                } else {
                    idType = $('#internalothidname').val();
                    employeeid = $('#internalothidcode').val();
                }
            }
        } else if (sharedwithtype == 2) {
            concernedname = $('#dropdownAuditor').val();
            othrelation = $('#dropdownauditorrelation').val();
            var employeeid = '';
            if (concernedname == '-1') {
                xflag = 'other';
                concernedname = $('#othauditorname').val();
                othrelation = $('#dropdownauditorrelation').val();
                idType = $('#auditoridtype').val();
                concerneddesignation = $('#auddesignationinp').val();
                afirmname = $('#dropdownFirmName').val();
                if (idType == 'PAN') {
                    employeeid = $('#othauditorpan').val();
                } else {
                    idType = $('#auditorothidname').val();
                    employeeid = $('#auditorothidcode').val();
                }
            }
            
        } else {
                idType = $('#otheridtype').val();
                if (idType == 'PAN') {
                    employeeid = $('#opan').val();
                } else {
                    idType = $('#otheridname').val();
                    employeeid = $('#otheridcode').val();
                }
                
        }
        var fcompany = $('#dropdownlistComplianceType1').val();
        var reason = $('#reason').val();
        var created_by = "<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User %>";


        $.ajax({
            type: 'post',
            url: '<%=ConfigurationManager.AppSettings["insiderapi"]%>createdetail/',
            data: { "fcompany": fcompany, "user_id": user_id, "detailcreated_id": detailcreated_id, "customer_id": customer_id, "upsitype_id": upsitype_id, "information_type": information_type, "information_shareddate": information_shareddate, "information_sharedtime": information_sharedtime, "sharedwithtype": sharedwithtype, "concernedname": concernedname, "auditorid": auditorid, "firmname": firmname, "oconcernedname": oconcernedname, "concerneddesignation": concerneddesignation, "oconcerneddesignation": oconcerneddesignation, "employeeid": employeeid, "reason": reason, "created_by": created_by, "department": concerneddepartment, "xflag": xflag, "othrelation": othrelation,"idtype":idType, "afirmname":afirmname  },
            success: function (result) {
               // alert(result.data);
                location.reload(); 

            },
            error: function (e, t) { alert('something went wrong'); }
        });
        
        return false;
    }
    </script>

   <!-- <script>
        $("#dropdownauditorrelation").kendoDropDownList({
            placeholder: "",
            dataTextField: "text",
            dataValueField: "value",
            checkboxes: true,
            checkAll: true,
            autoClose: true,
            dataSource: [
                { text: "Statutory Auditor", value: "Statutory Auditor" },
                { text: "Secretarial Audiotor", value: "Secretarial Audiotor" },
                { text: "Cost Auditor", value: "Cost Auditor" },
                { text: "Internal Auditor", value: "Internal Auditor" }
            ],
        });
    </script> -->
    <script>
        $("#internalidtype").kendoDropDownList({
            placeholder: "",
            dataTextField: "text",
            dataValueField: "value",
            checkboxes: true,
            checkAll: true,
            autoClose: true,
            dataSource: [
                { text: "PAN", value: "PAN" },
                { text: "Other", value: "Other" }
                
            ],
            change: function (e) {
                if (e.sender.value() == "PAN") {
                    $('#internalname').hide();
                    $('#internalcode').hide();
                    $('#internalpan').show();
                } else {
                    $('#internalpan').hide();
                    $('#internalname').show();
                    $('#internalcode').show();
                }
            }
        });
    </script>
    <script>
        $("#otheridtype").kendoDropDownList({
            placeholder: "",
            dataTextField: "text",
            dataValueField: "value",
            autoClose: true,
            dataSource: [
                { text: "PAN", value: "PAN" },
                { text: "Other", value: "Other" }

            ],
            change: function (e) {
                if (e.sender.value() == "PAN") {
                    $('#othername').hide();
                    $('#othercode').hide();
                    $('#saveBtn').css('margin-top', '86px');
                    $('#saveBtn').css('margin-left', '0');
                    $('#checkA').css('margin-left', '15px');
                    $('#otherpan').show();

                } else {
                    $('#saveBtn').css('margin-top', '38px');
                    $('#saveBtn').css('margin-left', '44%');
                    $('#checkA').css('margin-left', '-47%');
                    $('#otherpan').hide();
                    $('#othername').show();
                    $('#othercode').show();
                }
            }
        });
    </script>
    <script>
        $("#auditoridtype").kendoDropDownList({
            placeholder: "",
            dataTextField: "text",
            dataValueField: "value",
            autoClose: true,
            dataSource: [
                { text: "PAN", value: "PAN" },
                { text: "Other", value: "Other" }

            ],
            change: function (e) {
                if (e.sender.value() == "PAN") {
                    $('#oauditorname').hide();
                    $('#oauditorcode').hide();
                    $('#saveBtn').css('margin-top', '86px');
                    $('#saveBtn').css('margin-left', '0');

                    $('#checkA').css('margin-left', '15px');
                    
                    $('#oauditorpan').show();
                } else {
                    $('#oauditorpan').hide();
                    $('#saveBtn').css('margin-top', '38px');
                    $('#saveBtn').css('margin-left', '44%');
                    $('#checkA').css('margin-left', '-47%');
                    $('#oauditorname').show();
                    $('#oauditorcode').show();
                }
            }
        });
    </script>

<!--<script type="text/javascript">
    function setAuditorRelation(cid,firm) {
        $.ajax({
            type: 'post',
            url: "<%=ConfigurationManager.AppSettings["insiderapi"]%>getfirmpcspecific/?customer_id=" +<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID %>+,
            data: { "company": cid,"firm":firm },
             success: function (result) {
                 //alert(JSON.stringify(result));
                 
                 if (result.length > 0) {
                     var xxdropdownlist1 = $("#dropdownauditorrelation").data("kendoDropDownList");
                     xxdropdownlist1.value(result[0].AuditorType);
                     xxdropdownlist1.enable(false);

                 }
                 

             },
             error: function (e, t) { alert('something went wrong'); }
         });
    }
</script> -->
 <script>
     function showBtn() {
         if ($("#showcheckbox").is(':checked')) {
             $("#saveBtn").removeAttr("disabled");
         } else {
             $("#saveBtn").attr("disabled", true);
         }
     }
 </script>
     <script>
         $(document).ready(function () {
             var customer_id = <%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID %>;
             console.log(customer_id)
            $.ajax({
                type: 'Get',
                url: "<%=ConfigurationManager.AppSettings["insiderapi"]%>getuseroleid/?customer_id=" +<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID %>,


                success: function (result) {
                    if (result["status"] == "success") {
                        $("#button1").css("display", "block");
                    }
                    else {
                        $("#button1").css("display", "none");
                    }
                },
                error: function (e, t) { alert('something went wrong'); }
            });

            return false;
         })

     </script>
</asp:Content>
