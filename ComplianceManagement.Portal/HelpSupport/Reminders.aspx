﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
  

    <%--  <link href="NewCSS/helpcss/all.css" rel="stylesheet" />
    <link href="NewCSS/helpcss/bootstrap.min.css" rel="stylesheet" />
    <script src="Newjs/Helpjs/jquery-1.11.3.min.js"></script>
    <script src="Newjs/Helpjs/bootstrap.min.js"></script>--%>
     <link href="/NewCSS/helpcss/all.css" rel="stylesheet" />
    <link href="/NewCSS/helpcss/bootstrap.min.css" rel="stylesheet" />
    <script src="/Newjs/Helpjs/jquery-1.11.3.min.js"></script>
    <script src="/Newjs/Helpjs/bootstrap.min.js"></script>
     <style>
    /* Set height of the grid so .sidenav can be 100% (adjust if needed) */
    .row.content {height: 600px;
                  /*margin:125px;*/
    }
    
    /* Set gray background color and 100% height */
    .sidenav {
      background-color:#F5F2F4;
      height: 1500px;
      
    }
    .MainContainer {
     
      height: 1500px;
      border:groove;
      border-color:#f1f3f4;
      
    }
      .img1{
        border:ridge;
        border-color:lightgrey;
    }
   
    
   
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        *height: auto;
        *padding: 15px;
      
      }
      .row.content {height: auto;} 
    }
  </style>
    
    <style>
       
  .container {
  /*padding: 16px;*/
  background-color: white;
  margin:10px;
  /*margin-right:50px;
  margin-top:50px;*/
  height:1400px;
  width:1170px;
 

  
}
 body {
  font-family: Arial, Helvetica, sans-serif;
   /*background-color:#1fd9e1;*/
  
}

* {
  box-sizing: border-box;
}

    </style>

 
  <style>

	.panel-group .panel {
		border-radius: 5px;
		border-color: #EEEEEE;
        padding:0;
	}

	.panel-default > .panel-heading {
		/*color: #fff;*/
		/*background-color: #346767;*/
		border-color: #EEEEEE;
	}

	.panel-title {
		font-size: 14px;
	}

	.panel-title > a {
		display: block;
		padding: 15px;
		text-decoration: none;
	}

	.short-full {
		float: right;
		color: #fff;
	}

	.panel-default > .panel-heading + .panel-collapse > .panel-body {
		border: solid 1px #EEEEEE;
        /*background-color: #B7FFB7;*/
        
	}

</style>   
    <style>
table, th, td {
  border: 1px solid black;
}
</style>
</head>
<body >
    <form id="form1" runat="server">
      <div class="container">
      
      <div class="row content"> 

        <div class="col-sm-3 sidenav">
                     
                   <ul class="nav nav-pills nav-stacked"  >
                             <li style="font-size:18px"><a href="../NewHelp.aspx" style="color:#333"><b>Standard Dashboard</b></a>
                          <li style="font-size:14px" ><a href="../NewHelp.aspx" style="color:#333" ><i class='fas fa-desktop'></i>&nbsp;&nbsp;&nbsp;<b>My Dashboard</b></a>
                            <ul style="line-height:25px;font-size:13px">
                                <li ><a href="../HelpSupport/PerformerSummary.aspx" style="color:#333">Performer Summary</a></li>
                                <li><a href="../HelpSupport/PerformerTaskSummary.aspx" style="color:#333">Performer Task Summary</a></li>
                                <li><a href="../HelpSupport/PerformerLocation.aspx" style="color:#333">Performer Location</a></li>
                                <li> <a href="../HelpSupport/ReviewerSummary.aspx" style="color:#333">Reviewer Summary</a></li>
                                <li> <a href="../HelpSupport/ReviewerTaskSummary.aspx" style="color:#333">Reviewer Task Summary</a></li>
                                <li> <a href="../HelpSupport/ReviewerLocation.aspx" style="color:#333">Reviewer Location</a></li>
                                <li><a href="../HelpSupport/EventOwnerSummary.aspx" style="color:#333">Event Owner Summary</a></li>
                                <li><a href="../HelpSupport/ComplianceCalender.aspx" style="color:#333">My Compliance Calender</a></li>
                                <li><a href="../HelpSupport/DailyUpdates.aspx" style="color:#333">Daily Updates</a></li>
                                <li><a href="../HelpSupport/NewsLetter.aspx" style="color:#333">Newsletter</a></li>
                            </ul>
                        </li>
                        <li style="font-size:14px" ><a href="../HelpSupport/Compliance.aspx" style="color:#333;"><i class='fas fa-briefcase'></i>&nbsp;&nbsp;&nbsp;<b>My Workspace</b></a>
                            <ul style="line-height:25px;font-size:13px">
                              <li><a href="../HelpSupport/Compliance.aspx" style="color:#333">Compliance</a></li>
                              <li><a href="../HelpSupport/License.aspx" style="color:#333">License</a></li>
                            </ul>
                        </li>
                  
                  
                        <li style="font-size:14px" ><a href="../HelpSupport/MyReports.aspx" style="color:#333" > <i class='far fa-file-alt'></i>&nbsp;&nbsp;&nbsp;<b>My Reports</b> </a></li>
                        <li style="font-size:14px"><a href="../HelpSupport/MyDocuments.aspx" style="color:#333"><i class='far fa-file-alt'></i>&nbsp;&nbsp;&nbsp;<b>My Documents</b></a></li>
                        <li style="font-size:14px"><a href="../HelpSupport/Reminders.aspx" style="color:#333"><i class='far fa-clock'></i>&nbsp;&nbsp;&nbsp;<b>My Reminders</b></a></li>
                        <li style="font-size:14px"><a href="../HelpSupport/Escalation.aspx" style="color:#333"><i class="far fa-compass"></i>&nbsp;&nbsp;&nbsp;<b>My Escalation</b></a></li>

                     </ul>
           </div>

          <div class="col-sm-9 MainContainer">


              <h3>My reminders</h3>



              <p>
                  You can set and view your reminders. Following is the process,
              </p>
              <ul>
                  <li style="list-style-type: decimal">Click on My Reminders in menu, you will be redirected to page that holds list of previous reminders.</li>
                  <br />
                  <img class="img1" style="width: 700px; margin-left: 20px; height: 350px" src="../ImagesHelpCenter/Performer_My%20Reminders_1_Screenshot.png" /><br />
                  <br />
                  <li style="list-style-type: decimal">To Add new reminder, click on Add New button enter details and save.</li>
                  <br />
                  <img class="img1" style="width: 700px; margin-left: 20px; height: 450px" src="../ImagesHelpCenter/Performer_My%20Reminders_2_Screenshot.png" />

              </ul>
         
           <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
              <div class="panel panel-default">
		    	<div class="panel-heading" role="tab" id="headingOne">
				<h4 class="panel-title">
					<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
						<i class="short-full glyphicon glyphicon-plus"></i>
						<b>System generated reminder</b>
					</a>
				</h4>
			</div>
			 <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
				<div class="panel-body" style="align-content:center">
                <p>You will receive system generated reminders in following frequency,<br />
               
               </p>

                    <table style="border:2px;width:680px;height:300px">
                        <tr style="background-color:lightgray;text-align:center">
                            <th>Frequency <br />of Compliance</th>
                            <th>First <br />reminder(before)</th>
                            <th>Second <br />reminder(before)</th>
                             <th>Third <br />reminder(before)</th>
                             <th>Fourth <br /> reminder(before)</th>
                        </tr>
                        <tr style="text-align:center">
                             <td>Monthly</td>
                             <td>15 days</td>
                             <td>7 days</td>
                             <td>2 days</td>
                             <td>NA</td>
                        </tr>
                         <tr style="text-align:center">
                             <td>Quarterly</td>
                             <td>30 days</td>
                             <td>15 days</td>
                             <td>2 days</td>
                             <td>NA</td>
                        </tr>
                         <tr style="text-align:center">
                             <td>Four Monthly</td>
                             <td>30 days</td>
                             <td>15 days</td>
                             <td>2 days</td>
                             <td>NA</td>
                        </tr>
                        <tr style="text-align:center">
                             <td>Half Yearly</td>
                             <td>60days</td>
                             <td>30 days</td>
                             <td>15 days</td>
                             <td>2 days</td>
                        </tr>
                        <tr style="text-align:center">
                             <td>Annual</td>
                             <td>60days</td>
                             <td>30 days</td>
                             <td>15 days</td>
                             <td>2 days</td>
                        </tr>
                         <tr style="text-align:center">
                             <td>Two Yearly</td>
                             <td>60days</td>
                             <td>30 days</td>
                             <td>15 days</td>
                             <td>2 days</td>
                        </tr>
                         <tr style="text-align:center">
                             <td>Seven Yearly</td>
                             <td>60days</td>
                             <td>30 days</td>
                             <td>15 days</td>
                             <td>2 days</td>
                        </tr>
                    </table>
                    <br /><br />
                     </div>
                </div>
         </div>
      </div>
          </div>
      </div>
      </div>
    </form>
     

<script>

	function toggleIcon(e) {
        $(e.target)
            .prev('.panel-heading')
            .find(".short-full")
            .toggleClass('glyphicon-plus glyphicon-minus');
    }
    $('.panel-group').on('hidden.bs.collapse', toggleIcon);
    $('.panel-group').on('shown.bs.collapse', toggleIcon);
</script>
</body>
</html>
