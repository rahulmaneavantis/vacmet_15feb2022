﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using Ionic.Zip;
using System.Collections;
using System.Configuration;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;

namespace com.VirtuosoITech.ComplianceManagement.Portal
{
    public partial class StandardReportDocviewer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
                {
                    string filepath = Request.QueryString["ID"].ToString();

                    if (!string.IsNullOrEmpty(Request.QueryString["Id"]))
                    {
                        bool GetResult = false;
                        string url = string.Empty;
                        string fileName = string.Empty;
                        long UID = -1;
                        int ID = Convert.ToInt32(Request.QueryString["Id"]);

                        report_generate_log Reports = GetDetailReports(ID);

                        if (Reports != null)
                        {
                            if (Reports.pdf_name.Equals("DetailReport"))
                            {
                                List<sp_Get_Reportgeneratelog_Result> getDetail = new List<sp_Get_Reportgeneratelog_Result>();
                                getDetail = GetDetailReportNew(Convert.ToInt32(Reports.user_id), Convert.ToDateTime(Reports.created_at), Reports.pdf_name, Reports.ReportType);
                                if (getDetail.Count > 0)
                                {
                                    if (getDetail.Count > 1)
                                    {
                                        string userpath = null;
                                        using (ZipFile ComplianceZip = new ZipFile())
                                        {
                                            foreach (var item in getDetail)
                                            {
                                                url = item.location.ToString();
                                                fileName = item.report_name;
                                                UID = Convert.ToInt64(item.user_id);
                                                if (!string.IsNullOrEmpty(url) && !string.IsNullOrEmpty(fileName))
                                                {
                                                    GetResult = DownloadFileAndSaveDocuments(url, fileName, UID, out userpath);
                                                    if (GetResult)
                                                    {
                                                        doccontrol.Document = userpath;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (getDetail.Count > 0)
                                        {
                                            url = getDetail[0].location.ToString();
                                            fileName = getDetail[0].pdf_name + ".xlsx";
                                            UID = Convert.ToInt64(getDetail[0].user_id);
                                        }
                                        if (!string.IsNullOrEmpty(url) && !string.IsNullOrEmpty(fileName))
                                        {
                                            #region updated change 
                                            string userpath = null;
                                            GetResult = DownloadFileAndSaveDocuments(url, fileName, UID, out userpath);
                                            if (GetResult)
                                            {
                                                doccontrol.Document = userpath;
                                            }
                                            #endregion
                                            
                                        }
                                    }
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "NoDcoumentAlert();", true);
                                }
                            }
                            else
                            {
                                if (Reports != null)
                                {
                                    if (Reports.pdf_name.Equals("DetailReport"))
                                    {
                                        url = Reports.location.ToString();
                                        fileName = Reports.pdf_name + ".xlsx";
                                        UID = Convert.ToInt64(Reports.user_id);
                                    }
                                    else
                                    {
                                        url = Reports.location.ToString();
                                        fileName = Reports.pdf_name + ".pdf";
                                        UID = Convert.ToInt64(Reports.user_id);
                                    }
                                }
                                if (!string.IsNullOrEmpty(url) && !string.IsNullOrEmpty(fileName))
                                {
                                      
                                    if (!string.IsNullOrEmpty(url) && !string.IsNullOrEmpty(fileName))
                                    {
                                        #region updated change 
                                        string userpath = null;
                                        GetResult = DownloadFileAndSaveDocuments(url, fileName, UID, out userpath);
                                        if (GetResult)
                                        {

                                            doccontrol.Document = userpath;
                                            
                                        }
                                        else
                                        {
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "NoDcoumentAlert();", true);
                                        }
                                        #endregion
                                        
                                    }
                                    else
                                    {
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "NoDcoumentAlert();", true);
                                    }
                                }
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "NoDcoumentAlert();", true);
                        }
                    }

                }
            }
        }

        public static bool DownloadFileRahul(string urlAddress, string fileSavePath, long userID)
        {
            try
            {
                string azure_ContainerName = "avareport/ReportFile/" + userID;
                string filetoDownload = Path.GetFileName(urlAddress);
                string storageAccount_connectionString = ConfigurationManager.AppSettings["StandardReportstorageconnectionstring"];

                CloudStorageAccount mycloudStorageAccount = CloudStorageAccount.Parse(storageAccount_connectionString);
                CloudBlobClient blobClient = mycloudStorageAccount.CreateCloudBlobClient();

                CloudBlobContainer container = blobClient.GetContainerReference(azure_ContainerName);
                CloudBlockBlob cloudBlockBlob = container.GetBlockBlobReference(filetoDownload);

                cloudBlockBlob.DownloadToFile(fileSavePath, FileMode.OpenOrCreate);

                return true;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }

             
        }
        public static bool DownloadFileAndSaveDocuments(string downloadFilePath, string fileName, long userid, out string downloadedFilePath)
        {
            bool downloadSuccess = false;
            downloadedFilePath = string.Empty;

            try
            {
                string directoryPath = string.Empty;
                string DocAPIURlwithPATH = string.Empty;
                 directoryPath = ConfigurationManager.AppSettings["standardReportURL"].ToString() + "\\" + userid + "\\" + Convert.ToString(DateTime.Now.ToString("ddMMyy")) + "\\" + Convert.ToString(DateTime.Now.ToString("HHmmss")) + DateTime.Now.ToString("ffff");
                //directoryPath = "D:\\testnew\\test";

                if (!Directory.Exists(directoryPath))
                    Directory.CreateDirectory(directoryPath);

                directoryPath = directoryPath + @"\" + fileName;

                DocAPIURlwithPATH = downloadFilePath;
                if (!string.IsNullOrEmpty(directoryPath))
                {
                    downloadSuccess = DownloadFileRahul(DocAPIURlwithPATH, directoryPath, userid);
                    if (downloadSuccess)
                        downloadedFilePath = directoryPath;
                }
                return downloadSuccess;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return downloadSuccess;
            }
        }

        public static report_generate_log GetDetailReports(int ID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.report_generate_log
                            where row.id == ID
                            select row).FirstOrDefault();
                return data;
            }
        }

        public static bool DownloadFile(string url, string fileName)
        {
            //its not change
            //Create a stream for the file
            Stream stream = null;

            //This controls how many bytes to read at a time and send to the client
            int bytesToRead = 10000;

            // Buffer to read bytes in chunk size specified above
            byte[] buffer = new Byte[bytesToRead];

            // The number of bytes read
            try
            {
                //Create a WebRequest to get the file
                HttpWebRequest fileReq = (HttpWebRequest)HttpWebRequest.Create(url);

                //Create a response for this request
                HttpWebResponse fileResp = (HttpWebResponse)fileReq.GetResponse();

                if (fileReq.ContentLength > 0)
                    fileResp.ContentLength = fileReq.ContentLength;

                //Get the Stream returned from the response
                stream = fileResp.GetResponseStream();

                // prepare the response to the client. resp is the client Response
                var resp = HttpContext.Current.Response;

                //Indicate the type of data being sent
                resp.ContentType = "application/octet-stream";

                //Name the file 
                resp.AddHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
                resp.AddHeader("Content-Length", fileResp.ContentLength.ToString());

                int length;
                do
                {
                    // Verify that the client is connected.
                    if (resp.IsClientConnected)
                    {
                        // Read data into the buffer.
                        length = stream.Read(buffer, 0, bytesToRead);

                        // and write it out to the response's output stream
                        resp.OutputStream.Write(buffer, 0, length);

                        // Flush the data
                        resp.Flush();

                        //Clear the buffer
                        buffer = new Byte[bytesToRead];
                    }
                    else
                    {
                        // cancel the download if client has disconnected
                        length = -1;
                    }
                } while (length > 0); //Repeat until no data is read
            }
            finally
            {
                if (stream != null)
                {
                    //Close the input stream
                    stream.Close();
                }
            }
            return false;
        }

        public static List<sp_Get_Reportgeneratelog_Result> GetDetailReportNew(int UID, DateTime checkDate, string PDFName, string Flag)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                string getDate = checkDate.ToString("yyyy-MM-dd");

                var data = entities.sp_Get_Reportgeneratelog(UID, PDFName, getDate, Flag).ToList();

                return data;
            }
        }


    }
}