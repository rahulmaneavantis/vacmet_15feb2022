﻿<%@ Page Title="Customer List" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true"
    CodeBehind="CustomerList.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Customers.CustomerList" %>

<%@ Register Src="~/Controls/CustomerDetailsControl.ascx" TagName="CustomerDetailsControl"
    TagPrefix="vit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    </style>


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
      <div style="float: right; margin-top: 1px; margin-right: 8px;">
        <asp:LinkButton runat="server" ID="lbtnExportExcel" Visible="true" OnClick="lbtnExportExcel_Click" ><img src="../Images/excel.png" alt="Export to Excel"
        title="Export to Excel" width="30px" height="30px"/></asp:LinkButton>
    </div>
    <asp:UpdatePanel ID="upCustomerList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <table width="100%">
                <tr>
                    <td align="right" class="pagefilter" style="width: 15px; height: 25px;">
                        <div id="divFilter" runat="server">
                            Filter :
                        <asp:TextBox runat="server" ID="tbxFilter" Width="250px" MaxLength="50" AutoPostBack="true"
                            OnTextChanged="tbxFilter_TextChanged" />
                        </div>
                    </td>
                    <td class="newlink" align="right">
                        <asp:LinkButton Text="Add New" runat="server" ID="btnAddCustomer" OnClick="btnAddCustomer_Click" />
                    </td>
                </tr>
            </table>
            <div style="margin-bottom: 2px">
                <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                    ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
            </div>
            <asp:GridView runat="server" ID="grdCustomer" AutoGenerateColumns="false" GridLines="Vertical"
                BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true" OnRowCreated="grdCustomer_RowCreated"
                CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="12" Width="100%" OnSorting="grdCustomer_Sorting"
                Font-Size="12px" DataKeyNames="ID" OnRowCommand="grdCustomer_RowCommand" OnPageIndexChanging="grdCustomer_PageIndexChanging"
                OnPreRender="grdCustomer_OnPreRender">
                <Columns>
                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="60px">
                        <HeaderTemplate>
                            <asp:CheckBox ID="chkCompliancesHeader" Text="All" runat="server" onclick="javascript:SelectheaderCheckboxes(this)" />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkCustomer" runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Name" HeaderText="Name" HeaderStyle-Height="20px" ItemStyle-Height="25px" SortExpression="Name" />
                    <asp:BoundField DataField="BuyerName" HeaderText="Buyer Name" SortExpression="BuyerName" />
                    <asp:BoundField DataField="BuyerContactNumber" HeaderText="Buyer Contact" ItemStyle-HorizontalAlign="Center" SortExpression="BuyerContactNumber" />
                    <asp:BoundField DataField="BuyerEmail" HeaderText="Buyer Email" SortExpression="BuyerEmail" />
                    <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="Status" />
                    <asp:TemplateField HeaderText="Start Date" ItemStyle-HorizontalAlign="Center" SortExpression="StartDate">
                        <ItemTemplate>
                            <%# Eval("StartDate") != null ? ((DateTime)Eval("StartDate")).ToString("dd-MMM-yyyy")  : " "%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="End Date" ItemStyle-HorizontalAlign="Center" SortExpression="EndDate">
                        <ItemTemplate>
                            <%# Eval("EndDate") != null ? ((DateTime)Eval("EndDate")).ToString("dd-MMM-yyyy") : " "%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="DiskSpace" HeaderText="Disk Space" SortExpression="DiskSpace" />
                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Sub Entity">
                        <ItemTemplate>
                            <asp:LinkButton runat="server" CommandName="VIEW_COMPANIES" CommandArgument='<%# Eval("ID") %>'>sub-entities</asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-Width="60px" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:LinkButton ID="LinkButton1" runat="server" CommandName="EDIT_CUSTOMER" CommandArgument='<%# Eval("ID") %>'><img src="../Images/edit_icon.png" alt="Edit Customer" title="Edit Customer" /></asp:LinkButton>
                            <asp:LinkButton ID="LinkButton2" runat="server" CommandName="DELETE_CUSTOMER" CommandArgument='<%# Eval("ID") %>'
                                OnClientClick="return confirm('This will also delete all the sub-entities associated with customer. Are you certain you want to delete this customer entry?');"><img src="../Images/delete_icon.png" alt="Disable Customer" title="Disable Customer" /></asp:LinkButton>
                        </ItemTemplate>
                        <HeaderTemplate>
                        </HeaderTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <FooterStyle BackColor="#CCCC99" />
                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                <PagerSettings Position="Top" />
                <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                <AlternatingRowStyle BackColor="#E6EFF7" />
                <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                <EmptyDataTemplate>
                    No Records Found.
                </EmptyDataTemplate>
            </asp:GridView>
            <div style="margin-bottom: 7px; margin-left: 10px; margin-top: 10px; margin-right: 20px; text-align: right; float: right">
                <asp:Button Text="Notify Software Update" runat="server" ID="btnSendNotification" Width="100%" ToolTip="Click here to send server down notifications." OnClick="btnSendNotification_Click"
                    CssClass="button" ValidationGroup="ModifyAsignmentValidationGroup" CausesValidation="false" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div id="divNotificationTime">
        <asp:UpdatePanel ID="upServerDownTimeDetails" runat="server" UpdateMode="Conditional" OnLoad="upServerDownTimeDetails_Load">
            <ContentTemplate>
                <div style="margin: 5px">
                    <div style="margin-bottom: 4px">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="vdsummary" ValidationGroup="ComplianceCategoryValidationGroup1" />
                        <asp:CustomValidator ID="CustomValidator1" runat="server" EnableClientScript="False"
                            ValidationGroup="ComplianceCategoryValidationGroup1" Display="None" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Date</label>
                        <asp:TextBox runat="server" ID="txtDate" Style="height: 16px; width: 200px;" ReadOnly="true" MaxLength="200" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Date can not be empty." ControlToValidate="txtDate"
                            runat="server" ValidationGroup="ComplianceCategoryValidationGroup1" Display="None" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Time
                        </label>
                        <asp:DropDownList runat="server" ID="ddlStartTime"></asp:DropDownList>
                        <asp:CompareValidator ID="CompareValidator1" ErrorMessage="Please select start time." ControlToValidate="ddlStartTime"
                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceCategoryValidationGroup1"
                            Display="None" />
                        To
                        <asp:DropDownList runat="server" ID="ddlEndTime"></asp:DropDownList>
                        <asp:CompareValidator ID="CompareValidator2" ErrorMessage="Please select end time." ControlToValidate="ddlEndTime"
                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceCategoryValidationGroup1"
                            Display="None" />
                    </div>
                    <div style="margin-bottom: 7px; float: right; margin-right: 66px; margin-top: 10px;">
                        <asp:Button Text="Send" runat="server" ID="btnSend" CssClass="button" OnClick="btnSend_click"
                            ValidationGroup="ComplianceCategoryValidationGroup1" />
                        <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="button" OnClientClick="$('#divNotificationTime').dialog('close');" />
                    </div>
                </div>
            </ContentTemplate>
             <Triggers>
            <asp:PostBackTrigger ControlID="lbtnExportExcel" />
        </Triggers>
        </asp:UpdatePanel>
    </div>

    <script type="text/javascript">

        $(function () {
            $('#divNotificationTime').dialog({
                height: 300,
                width: 500,
                autoOpen: false,
                showOn: true,
                draggable: true,
                title: "Software Update Details",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });
        });



        function initializeDowntimeDatePicker(date) {
            var startDate = new Date();
            $('#<%= txtDate.ClientID %>').datepicker({
                dateFormat: 'dd-mm-yy',
                numberOfMonths: 1,
                minDate: startDate
            });

            if (date != null) {
                $("#<%= txtDate.ClientID %>").datepicker("option", "defaultDate", date);
            }
        }

        function SelectheaderCheckboxes(headerchk) {
            var rolecolumn;

            var chkheaderid = headerchk.id.split("_");

            var gvcheck = document.getElementById("<%=grdCustomer.ClientID %>");
            var i;

            if (headerchk.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[0].getElementsByTagName("INPUT")[0].checked = headerchk.checked;
                }
            }

            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[0].getElementsByTagName("INPUT")[0].checked = headerchk.checked;
                }
            }
        }

        function Selectchildcheckboxes(header) {
            var i;
            var count = 0;
            var rolecolumn;
            var gvcheck = document.getElementById("<%=grdCustomer.ClientID %>");
            var headerchk = document.getElementById(header);
            var chkheaderid = header.split("_");

            var rowcount = gvcheck.rows.length;

            for (i = 1; i < gvcheck.rows.length - 1; i++) {
                if (gvcheck.rows[i + 1].cells[0].getElementsByTagName("INPUT")[0].checked) {
                    count++;
                }
            }

            if (count == gvcheck.rows.length - 2) {
                headerchk.checked = true;
            }
            else {
                headerchk.checked = false;
            }
        }

    </script>

    <vit:CustomerDetailsControl runat="server" ID="udcInputForm" />
</asp:Content>
