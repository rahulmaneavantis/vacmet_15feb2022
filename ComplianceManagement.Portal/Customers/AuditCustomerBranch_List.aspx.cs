﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Logger;
using System.Reflection;
using System.Configuration;
using System.Collections.Generic;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using OfficeOpenXml;
using System.Web;
using System.IO;
using OfficeOpenXml.Style;
using System.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataLitigation;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Customers
{
    public partial class AuditCustomerBranch_List : System.Web.UI.Page
    {
        protected tbl_PageAuthorizationMaster authpage;
        protected int pageid = 1;
        protected string IsAuditManager = string.Empty;
        // public static List<int> Verticallist = new List<int>();
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.ProductApplicableLogin == "L")
                this.MasterPageFile = "~/LitigationMaster.Master";
            else if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.ProductApplicableLogin == "T")
                this.MasterPageFile = "~/ContractProduct.Master";
            else if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.ProductApplicableLogin == "S")
                this.MasterPageFile = "~/LicenseManagement.Master";
            else
                this.MasterPageFile = "~/AuditTool.Master";
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            IsAuditManager = UserManagementRisk.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).IsAuditHeadOrMgr;
            if (Cache.Get("Page_authorizeddata") != null)
            {
                var Records = (List<tbl_PageAuthorizationMaster>)Cache.Get("Page_authorizeddata");
                if (Records.Count > 0)
                {
                    var query = (from row in Records
                                 where row.PageID == pageid
                                 select row).FirstOrDefault();
                    authpage = query;
                }
            }
            //CustomValidator1.CssClass = "alert alert-block alert-danger fade in";
            SubUnitValidationSummary.CssClass= "alert alert-danger";
            ValidationSummary1.CssClass = "alert alert-danger";
            valcustomerbranch.CssClass = "alert alert-danger";
            if (!IsPostBack)
            {
                try
                {
                    ViewState["ParentID"] = null;
                    BindCustomerBranches();
                    BindCustomerStatus();
                    BindIndustry();
                    BindStates();
                    if (!(AuthenticationHelper.Role.Equals("SADMN") || AuthenticationHelper.Role.Equals("IMPT")))
                    {
                        //  btnAddCustomerBranch.Visible = false;
                    }
                    BindLegalEntityType();
                    BindCompanyTypeType();
                    bindPageNumber();
                    hideAddControls();
                   
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";


                }
            }
          
        }

        public void hideAddControls()
        {
            try
            {
                if (authpage != null)
                {
                    if (authpage.Addval == false)
                    {
                        btnAddCustomerBranch.Visible = false;
                    }
                    else
                    {
                        btnAddCustomerBranch.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                DropDownListPageNo.Items.Clear();

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DropDownListPageNo.SelectedItem.ToString() != "")
            {
                int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
                grdCustomerBranch.PageIndex = chkSelectedPage - 1;
                grdCustomerBranch.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                BindCustomerBranches();
            }
        }
        private void BindLegalRelationShips(bool parent)
        {
            try
            {
                ddlLegalRelationShip.DataTextField = "Name";
                ddlLegalRelationShip.DataValueField = "ID";

                var legalRelationShips = Enumerations.GetAll<LegalRelationship>();
                if (!parent)
                {
                    legalRelationShips.RemoveAt(0);
                }
                ddlLegalRelationShip.DataSource = legalRelationShips;
                ddlLegalRelationShip.DataBind();

                ddlLegalRelationShip.Items.Insert(0, new ListItem(" Select ", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindBranchTypes()
        {
            try
            {
                ddlType.DataSource = null;
                ddlType.DataBind();
                ddlType.ClearSelection();

                ddlType.DataTextField = "Name";
                ddlType.DataValueField = "ID";

                var dataSource = UserManagementRisk.GetSubEntityList();
                ddlType.DataSource = dataSource;

                ddlType.DataBind();
                ddlType.Items.Insert(0, new ListItem(" Select ", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindIndustry()
        {
            try
            {
                ddlIndustry.DataTextField = "Name";
                ddlIndustry.DataValueField = "ID";

                ddlIndustry.DataSource = UserManagementRisk.GetAllIndustry();
                ddlIndustry.DataBind();
                ddlIndustry.Items.Insert(0, new ListItem(" Select ", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void dlBreadcrumb_ItemCommand(object source, DataListCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "ITEM_CLICKED")
                {
                    if (e.Item.ItemIndex == 0)
                    {
                        ViewState["ParentID"] = null;
                    }
                    else
                    {
                        ViewState["ParentID"] = e.CommandArgument.ToString();
                    }
                    BindCustomerBranches();
                    upCustomerBranchList.Update();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static List<SP_CustomerBranchView_Risk_Result> GetAll(int customerID, long parentID, string filter)
        {
            //LogLibrary.WriteErrorLog("CID :"+ customerID+ "parentID:" + parentID + "filter:" + filter);
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                try
                {
                    //var customerBranches = (from row in entities.CustomerBranchView_Risk
                    //                        where row.IsDeleted == false && row.CustomerID == customerID
                    //                        select row).ToList();

                    var customerBranches = (from row in entities.SP_CustomerBranchView_Risk(customerID)
                                            where row.IsDeleted == false
                                            select row).ToList();

                    if (parentID != -1)
                    {
                        customerBranches = customerBranches.Where(entry => entry.ParentID == parentID).ToList();
                        
                    }
                    else
                    {
                        customerBranches = customerBranches.Where(entry => entry.ParentID == null).ToList();
                    }

                    if (!string.IsNullOrEmpty(filter))
                    {
                        customerBranches = customerBranches.Where(entry => entry.Name.Contains(filter) || entry.TypeName.Contains(filter) || entry.ContactPerson.Contains(filter) || entry.EmailID.Contains(filter) || entry.Landline.Contains(filter)).ToList();
                    }

                    return customerBranches.ToList();

                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    return null;
                }

            }
        }
        private void BindCustomerBranches()
        {
            try
            {
                long parentID = -1;
                ddlLegalRelationShipOrStatus.Items.Clear();
                if (ViewState["ParentID"] != null)
                {                    
                    try
                    {
                        //LogLibrary.WriteErrorLog("Start TryParse");
                        long.TryParse(ViewState["ParentID"].ToString(), out parentID);
                        //LogLibrary.WriteErrorLog("End TryParse");

                    }
                    catch (Exception ex)
                    {
                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }

                    try
                    {
                        //LogLibrary.WriteErrorLog("Start BindLegalRelationShipOrStatus");
                        BindLegalRelationShipOrStatus(Convert.ToInt32(ViewState["IndustryId"].ToString()));
                        //LogLibrary.WriteErrorLog("End BindLegalRelationShipOrStatus");
                    }
                    catch (Exception ex)
                    {
                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);                        
                    }                    
                }
                else
                {
                    ViewState["IndustryId"] = null;
                }
                try
                {
                    //LogLibrary.WriteErrorLog("Start GetHierarchy");
                    dlBreadcrumb.DataSource = CustomerBranchManagement.GetHierarchy(Convert.ToInt32(AuthenticationHelper.CustomerID), parentID);
                    dlBreadcrumb.DataBind();
                    //LogLibrary.WriteErrorLog("END GetHierarchy");
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
                try
                {
                    //LogLibrary.WriteErrorLog(Convert.ToString( AuthenticationHelper.CustomerID));
                    //LogLibrary.WriteErrorLog("Start CustomerBranchList");
                    var aaa = Convert.ToInt32(AuthenticationHelper.CustomerID);

                    //LogLibrary.WriteErrorLog("CID :" + aaa + "parentID:" + parentID + "filter:" + tbxFilter.Text);
                    var CustomerBranchList = GetAll(aaa, parentID, tbxFilter.Text);
                    grdCustomerBranch.DataSource = CustomerBranchList;
                    Session["TotalRows"] = CustomerBranchList.Count;
                    grdCustomerBranch.DataBind();

                    //LogLibrary.WriteErrorLog("END CustomerBranchList");
                }               
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
               
                upCustomerBranchList.Update();

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                divLegalRelationship.Visible = divIndustry.Visible= ddlType.SelectedValue == "1";
                string industryId = string.Empty;

                if (ViewState["IndustryId"] != null)
                {
                    if (Convert.ToInt32(ViewState["IndustryId"]) !=0 )
                    {
                        BindLegalRelationShipOrStatus(Convert.ToInt32(ViewState["IndustryId"].ToString()));
                        ddlIndustry.SelectedValue = ViewState["IndustryId"].ToString();
                    }
                   
                }
                if (!divIndustry.Visible)
                {
                    ddlIndustry.SelectedIndex = -1;
                    if (ViewState["IndustryId"] == null)
                        BindLegalRelationShipOrStatus(0);
                }
                if (!divLegalRelationship.Visible)
                {
                    ddlLegalRelationShip.SelectedIndex = -1;
                }
                if (ddlType.SelectedValue == "1")
                {
                    divLegalEntityType.Visible = true;
                    divCompanyType.Visible = true;
                   
                }
                else
                {
                    divLegalEntityType.Visible = false;
                    divCompanyType.Visible = false;
                   
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlIndustry_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindLegalRelationShipOrStatus(Convert.ToInt32(ddlIndustry.SelectedValue));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindLegalRelationShipOrStatus(int industryId)
        {
            try
            {
                ddlLegalRelationShipOrStatus.DataSource = null;
                ddlLegalRelationShipOrStatus.DataBind();
                ddlLegalRelationShipOrStatus.ClearSelection();

                ddlLegalRelationShipOrStatus.DataTextField = "Name";
                ddlLegalRelationShipOrStatus.DataValueField = "ID";


                ddlLegalRelationShipOrStatus.DataSource = CustomerBranchManagement.GetAllLegalStatus(industryId, ddlType.SelectedValue.Length > 0 ? Convert.ToInt32(ddlType.SelectedValue) : -1);
                ddlLegalRelationShipOrStatus.DataBind();

                ddlLegalRelationShipOrStatus.Items.Insert(0, new ListItem(" Select ", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindCities()
        {
            try
            {
                ddlCity.DataSource = null;
                ddlCity.DataBind();
                ddlCity.ClearSelection();

                ddlCity.DataTextField = "Name";
                ddlCity.DataValueField = "ID";

                if (!string.IsNullOrEmpty(ddlState.SelectedValue))
                {
                    ddlCity.DataSource = AddressManagement.GetAllCitiesByState(Convert.ToInt32(ddlState.SelectedValue));
                    ddlCity.DataBind();
                }                
                ddlCity.Items.Insert(0, new ListItem(" Other ", "0"));
                ddlCity.Items.Insert(0, new ListItem(" Select ", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindStates()
        {
            try
            {
                ddlState.DataTextField = "Name";
                ddlState.DataValueField = "ID";
                ddlState.DataSource = AddressManagement.GetAllStates();
                ddlState.DataBind();

                ddlState.Items.Insert(0, new ListItem(" Select ", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindCities();
            ddlCity.SelectedValue = "-1";
        }

        protected void ddlCity_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlCity.SelectedValue == "0" && divOther.Visible == false)
                {
                    divOther.Visible = true;
                    tbxOther.Text = string.Empty;
                }
                else if (ddlCity.SelectedValue != "0")
                {
                    divOther.Visible = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdCustomerBranch_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int customerBranchID = Convert.ToInt32(e.CommandArgument);

                if (e.CommandName.Equals("EDIT_CUSTOMER_BRANCH"))
                {
                    ViewState["Mode"] = 1;
                    ViewState["CustomerBranchID"] = customerBranchID;

                    CustomerBranch customerBranch = CustomerBranchManagement.GetByID(customerBranchID);
                    if (customerBranch != null)
                    {
                        PopulateInputForm();
                        if (!string.IsNullOrEmpty(customerBranch.Name))
                        {
                            tbxName.Text = customerBranch.Name;
                        }

                        if (!string.IsNullOrEmpty(Convert.ToString(customerBranch.Type)))
                        {
                            if (customerBranch.Type != -1)
                            {
                                ddlType.SelectedValue = customerBranch.Type.ToString();
                                ddlType_SelectedIndexChanged(null, null);
                            }
                        }

                        if (!string.IsNullOrEmpty(Convert.ToString(customerBranch.LegalRelationShipID)))
                        {
                            if (customerBranch.LegalRelationShipID != -1 && customerBranch.LegalRelationShipID != 0)
                            {
                                ddlLegalRelationShip.SelectedValue = (customerBranch.LegalRelationShipID ?? -1).ToString();
                            }
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(customerBranch.LegalEntityTypeID)))
                        {
                            if (customerBranch.LegalEntityTypeID != -1 && customerBranch.LegalEntityTypeID != 0)
                            {
                                ddlLegalEntityType.SelectedValue = (customerBranch.LegalEntityTypeID ?? -1).ToString();
                            }
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(customerBranch.ComType)))
                        {
                            if (customerBranch.ComType != 0 && customerBranch.ComType != -1)
                            {
                                ddlCompanyType.SelectedValue = (customerBranch.ComType).ToString();
                            }
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(customerBranch.Industry)))
                        {
                            if (customerBranch.Industry != -1)
                            {
                                ddlIndustry.SelectedValue = (customerBranch.Industry).ToString();
                                ddlIndustry_SelectedIndexChanged(null, null);

                                if (customerBranch.Industry == -1 || string.IsNullOrEmpty(customerBranch.Industry.ToString()))
                                {
                                    int industryID = -1;
                                    if (ViewState["IndustryId"] == null)
                                    {
                                        if (customerBranch.ParentID != null)//Added by Rahul on 9 FEB 2016
                                        {
                                            industryID = Convert.ToInt32(CustomerBranchManagement.GetByID(Convert.ToInt32(customerBranch.ParentID)).Industry);
                                        }
                                    }
                                    else
                                    {
                                        industryID = Convert.ToInt32(ViewState["IndustryId"]);
                                    }
                                    BindLegalRelationShipOrStatus(industryID);
                                }
                            }
                        }

                        if (!string.IsNullOrEmpty(Convert.ToString(customerBranch.LegalRelationShipOrStatus)))
                        {
                            if (customerBranch.LegalRelationShipOrStatus != -1)
                            {
                                ddlLegalRelationShipOrStatus.SelectedValue = customerBranch.LegalRelationShipOrStatus != 0 ? customerBranch.LegalRelationShipOrStatus.ToString() : "-1";
                            }
                        }


                        tbxAddressLine1.Text = customerBranch.AddressLine1;
                        tbxAddressLine2.Text = customerBranch.AddressLine2;
                        if (!string.IsNullOrEmpty(Convert.ToString(customerBranch.StateID)))
                        {
                            if (customerBranch.StateID != -1)
                            {
                                ddlState.SelectedValue = customerBranch.StateID.ToString();
                                ddlState_SelectedIndexChanged(null, null);
                            }
                        }

                        if (!string.IsNullOrEmpty(Convert.ToString(customerBranch.CityID)))
                        {
                            if (customerBranch.CityID != -1)
                            {
                                ddlCity.SelectedValue = customerBranch.CityID.ToString();
                                ddlCity_SelectedIndexChanged(null, null);
                            }
                        }


                        tbxOther.Text = customerBranch.Others;
                        tbxPinCode.Text = customerBranch.PinCode;

                        tbxContactPerson.Text = customerBranch.ContactPerson;
                        tbxLandline.Text = customerBranch.Landline;
                        tbxMobile.Text = customerBranch.Mobile;
                        tbxEmail.Text = customerBranch.EmailID;
                        if (!string.IsNullOrEmpty(Convert.ToString(customerBranch.Status)))
                        {
                            if (customerBranch.Status != -1)
                            {
                                ddlCustomerStatus.SelectedValue = Convert.ToString(customerBranch.Status);
                            }
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(customerBranch.AuditPR)))
                        {
                            if (customerBranch.AuditPR == false)
                            {
                                ddlPersonResponsibleApplicable.SelectedValue = Convert.ToString(0);
                            }
                            else
                            {
                                ddlPersonResponsibleApplicable.SelectedValue = Convert.ToString(1);
                            }

                        }


                        ddlIndustry.Enabled = false;
                        ddlType.Enabled = false;
                        ddlLegalRelationShipOrStatus.Enabled = false;
                        ddlLegalRelationShip.Enabled = false;
                        upCustomerBranches.Update();
                    }
                }
                else if (e.CommandName.Equals("DELETE_CUSTOMER_BRANCH"))
                {                    
                    CustomerBranchManagement.Delete(customerBranchID);
                    CustomerBranchManagementRisk.Delete(customerBranchID);
                    UserManagementRisk.DeleteBranchVertical(customerBranchID);
                    BindCustomerBranches();
                    bindPageNumber();
                    //GetPageDisplaySummary();
                    upCustomerBranches.Update();
                }
                else if (e.CommandName.Equals("VIEW_CHILDREN"))
                {
                    try
                    {                        
                        ViewState["ParentID"] = customerBranchID;
                        if (ViewState["IndustryId"] == null)
                        {
                            ViewState["IndustryId"] = CustomerBranchManagement.GetIndustryIDByCustomerId(customerBranchID);
                        }

                        BindCustomerBranches();
                        bindPageNumber();
                        //GetPageDisplaySummary();
                        upCustomerBranches.Update();
                    }
                    catch (Exception ex)
                    {
                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdCustomerBranch_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdCustomerBranch.PageIndex = e.NewPageIndex;
                BindCustomerBranches();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnAddCustomerBranch_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["Mode"] = 0;

                ddlIndustry.Enabled = true;
                ddlType.Enabled = true;
                ddlLegalRelationShipOrStatus.Enabled = true;
                ddlLegalRelationShip.Enabled = true;

                tbxName.Text = tbxAddressLine1.Text = tbxAddressLine2.Text = tbxOther.Text = tbxPinCode.Text = tbxContactPerson.Text = tbxLandline.Text = tbxMobile.Text = tbxEmail.Text = string.Empty;
                PopulateInputForm();

                ddlIndustry.SelectedValue = "-1";
                ddlType.SelectedValue = "-1";
                ddlType_SelectedIndexChanged(null, null);

                ddlCompanyType.SelectedValue = "-1";

                ddlState.SelectedValue = "-1";
                ddlState_SelectedIndexChanged(null, null);
                ddlCity_SelectedIndexChanged(null, null);

                ddlCustomerStatus.SelectedIndex = -1;
                txtsubunittype.Text = string.Empty;
                tbxSubunitType.Text = string.Empty;
                upCustomerBranches.Update();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "javascript:fopenpopup()", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void PopulateInputForm()
        {
            try
            {
                BindBranchTypes();
                BindLegalRelationShips(ViewState["ParentID"] == null);
                divParent.Visible = ViewState["ParentID"] != null;

                litCustomer.Text = CustomerManagement.GetByID(Convert.ToInt32(AuthenticationHelper.CustomerID)).Name;

                if (ViewState["ParentID"] != null)
                {
                    litParent.Text = CustomerBranchManagement.GetByID(Convert.ToInt32(ViewState["ParentID"])).Name;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                grdCustomerBranch.PageIndex = 0;
                BindCustomerBranches();
                bindPageNumber();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int resultData = 0;
                bool resultchk = false;

                if (!string.IsNullOrEmpty(ddlType.SelectedValue))
                {
                    if (ddlType.SelectedValue == "1")
                    {
                        if (ddlCompanyType.SelectedValue == "-1")
                        {
                            cvDuplicateEntry.ErrorMessage = "Please select Type.";
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.CssClass = "alert alert-danger";
                            return;
                        }
                    }
                }
                else
                {
                    cvDuplicateEntry.ErrorMessage = "Please select Type.";
                    cvDuplicateEntry.IsValid = false;
                    return;
                }

                int comType = 0;
                if (!string.IsNullOrEmpty(ddlCompanyType.SelectedValue))
                {
                    if (ddlCompanyType.SelectedValue.ToString() == "-1")
                    {
                        comType = 0;
                    }
                    else
                    {
                        comType = Convert.ToInt32(ddlCompanyType.SelectedValue);
                    }
                }

                #region Compliance Save And Update

                CustomerBranch customerBranch = new CustomerBranch()
                {
                    Name = tbxName.Text,
                    Type = Convert.ToByte(ddlType.SelectedValue),
                    ComType = Convert.ToByte(comType),
                    AddressLine1 = tbxAddressLine1.Text,
                    AddressLine2 = tbxAddressLine2.Text,
                    StateID = Convert.ToInt32(ddlState.SelectedValue),
                    CityID = Convert.ToInt32(ddlCity.SelectedValue),
                    Others = tbxOther.Text,
                    PinCode = tbxPinCode.Text,
                    Industry = Convert.ToInt32(ddlIndustry.SelectedValue),
                    ContactPerson = tbxContactPerson.Text,
                    Landline = tbxLandline.Text,
                    Mobile = tbxMobile.Text,
                    EmailID = tbxEmail.Text,
                    CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID),
                    ParentID = ViewState["ParentID"] == null ? (int?)null : Convert.ToInt32(ViewState["ParentID"]),
                    Status = Convert.ToInt32(ddlCustomerStatus.SelectedValue) ,
                    CreatedBy=AuthenticationHelper.UserID,
                    CreatedFrom=3    
                };
              

                if (ddlPersonResponsibleApplicable.SelectedItem.Text == "Yes")
                {
                    customerBranch.AuditPR = true;
                }
                if (ddlType.SelectedValue == "1")
                {
                    if (!string.IsNullOrEmpty(ddlLegalRelationShip.SelectedValue))
                    {
                        customerBranch.LegalRelationShipID = Convert.ToInt32(ddlLegalRelationShip.SelectedValue);
                    }
                    if (!string.IsNullOrEmpty(ddlLegalEntityType.SelectedValue))
                    {
                        customerBranch.LegalEntityTypeID = Convert.ToInt32(ddlLegalEntityType.SelectedValue);
                    }
                }
                else
                {
                    customerBranch.LegalRelationShipID = null;
                    customerBranch.LegalEntityTypeID = null;
                }

                if (!string.IsNullOrEmpty(ddlLegalRelationShipOrStatus.SelectedValue))
                {
                    if (ddlLegalRelationShipOrStatus.SelectedValue != "-1")
                        customerBranch.LegalRelationShipOrStatus = Convert.ToByte(ddlLegalRelationShipOrStatus.SelectedValue);
                    else
                        customerBranch.LegalRelationShipOrStatus = 0;
                }
                else
                {
                    customerBranch.LegalRelationShipOrStatus = 0;
                }

                if ((int)ViewState["Mode"] == 1)
                {
                    customerBranch.ID = Convert.ToInt32(ViewState["CustomerBranchID"]);
                }

                if ((int)ViewState["Mode"] == 0)/////////***************For Save*********************//
                {
                    if (CustomerBranchManagement.Exists(customerBranch, Convert.ToInt32(AuthenticationHelper.CustomerID)))
                    {
                        cvDuplicateEntry.ErrorMessage = "Customer branch name already exists.";
                        cvDuplicateEntry.IsValid = false;
                        return;
                    }
                    try
                    {
                        //CustomerBranchManagement.Create(customerBranch);
                        resultData = CustomerBranchManagement.Create(customerBranch);
                        if (resultData > 0)
                        {
                            // Added by SACHIN 28 April 2016
                            string ReplyEmailAddressName = "Avantis";
                            string accessURL = string.Empty;
                            URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(AuthenticationHelper.CustomerID));
                            if (Urloutput != null)
                            {
                                accessURL = Urloutput.URL;
                            }
                            else
                            {
                                accessURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                            }
                            string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EMailTemplate_NewCustomerBranchCreaded
                                            .Replace("@NewCustomer", litCustomer.Text)
                                            .Replace("@BranchName", tbxName.Text)
                                            .Replace("@LoginUser", AuthenticationHelper.User)
                                            .Replace("@PortalURL", Convert.ToString(accessURL))
                                            .Replace("@From", ReplyEmailAddressName)
                                            .Replace("@URL", Convert.ToString(accessURL));
                                        

                            //string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EMailTemplate_NewCustomerBranchCreaded
                            //                    .Replace("@NewCustomer", litCustomer.Text)
                            //                    .Replace("@BranchName", tbxName.Text)
                            //                    .Replace("@LoginUser", AuthenticationHelper.User)
                            //                    .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                            //                    .Replace("@From", ReplyEmailAddressName)
                            //                    .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                            //                ;

                            string SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"].ToString();
                            string CustomerCreatedEmail = ConfigurationManager.AppSettings["CustomerCreatedEmail"].ToString();
                            EmailManager.SendMail(SenderEmailAddress, new List<String>(new String[] { CustomerCreatedEmail }), null, null, "AVACOM customer branch added.", message);
                        }
                    }
                    catch (Exception ex)
                    {
                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                        CustomerBranchManagement.deleteCustomerBranch(resultData);
                        resultData = 0;
                        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                        cvDuplicateEntry.IsValid = false;
                        return;
                    }
                }
                else if ((int)ViewState["Mode"] == 1)/////////////////**************For Update****************///////////////
                {
                    //CustomerBranchManagement.Update(customerBranch);
                    resultData = CustomerBranchManagement.UpdateCustBranch(customerBranch);
                }

                #endregion

                if (resultData > 0)
                {
                    #region Audit Save And Update
                    com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_CustomerBranch customerBranch1 = new com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_CustomerBranch()
                    {
                        Name = tbxName.Text,
                        Type = Convert.ToByte(ddlType.SelectedValue),
                        ComType = Convert.ToByte(comType),
                        AddressLine1 = tbxAddressLine1.Text,
                        AddressLine2 = tbxAddressLine2.Text,
                        StateID = Convert.ToInt32(ddlState.SelectedValue),
                        CityID = Convert.ToInt32(ddlCity.SelectedValue),
                        Others = tbxOther.Text,
                        PinCode = tbxPinCode.Text,
                        Industry = Convert.ToInt32(ddlIndustry.SelectedValue),
                        ContactPerson = tbxContactPerson.Text,
                        Landline = tbxLandline.Text,
                        Mobile = tbxMobile.Text,
                        EmailID = tbxEmail.Text,
                        CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID),
                        ParentID = ViewState["ParentID"] == null ? (int?)null : Convert.ToInt32(ViewState["ParentID"]),
                        Status = Convert.ToInt32(ddlCustomerStatus.SelectedValue) ,
                        CreatedBy=AuthenticationHelper.UserID,
                        CreatedFrom=3
                    };
                    if (ddlPersonResponsibleApplicable.SelectedItem.Text == "Yes")
                    {
                        customerBranch1.AuditPR = true;
                    }
                    if (ddlType.SelectedValue == "1")
                    {
                        if (!string.IsNullOrEmpty(ddlLegalRelationShip.SelectedValue))
                        {
                            customerBranch1.LegalRelationShipID = Convert.ToInt32(ddlLegalRelationShip.SelectedValue);
                        }
                        if (!string.IsNullOrEmpty(ddlLegalEntityType.SelectedValue))
                        {
                            customerBranch1.LegalEntityTypeID = Convert.ToInt32(ddlLegalEntityType.SelectedValue);
                        }
                    }
                    else
                    {
                        customerBranch1.LegalRelationShipID = null;
                        customerBranch1.LegalEntityTypeID = null;
                    }

                    if (!string.IsNullOrEmpty(ddlLegalRelationShipOrStatus.SelectedValue))
                    {
                        if (ddlLegalRelationShipOrStatus.SelectedValue != "-1")
                            customerBranch1.LegalRelationShipOrStatus = Convert.ToByte(ddlLegalRelationShipOrStatus.SelectedValue);
                        else
                            customerBranch1.LegalRelationShipOrStatus = 0;
                    }
                    else
                    {
                        customerBranch1.LegalRelationShipOrStatus = 0;
                    }

                    if ((int)ViewState["Mode"] == 1)
                    {
                        customerBranch1.ID = Convert.ToInt32(ViewState["CustomerBranchID"]);
                    }

                    // var customerID = 0;
                    // customerID = UserManagement.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;

                    if ((int)ViewState["Mode"] == 0)
                    {
                        if (CustomerBranchManagement.Exists1(customerBranch1, Convert.ToInt32(AuthenticationHelper.CustomerID)))
                        {
                            cvDuplicateEntry.ErrorMessage = "Branch with same name already exists.";
                            cvDuplicateEntry.IsValid = false;
                            return;
                        }

                        resultchk = CustomerBranchManagement.Create1(customerBranch1);
                        if (resultchk == false)
                        {
                            CustomerBranchManagement.deleteCustomerBranch(resultData);
                        }
                        cvDuplicateEntry.ErrorMessage = "Branch Added Successfully.";
                        cvDuplicateEntry.IsValid = false;
                    }
                    else if ((int)ViewState["Mode"] == 1)
                    {
                        CustomerBranchManagement.Update1(customerBranch1);
                        cvDuplicateEntry.ErrorMessage = "Branch Updated Successfully.";
                        cvDuplicateEntry.IsValid = false;
                        // ddlVerticalBranch.ClearSelection();
                    }
                    #endregion
                }
                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divCustomerBranchesDialog\").dialog('close')", true);
                BindCustomerBranches();
                //GetPageDisplaySummary();
                bindPageNumber();
                upCustomerBranches.Update();

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void upCustomerBranches_Load(object sender, EventArgs e)
        {
        }
        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void grdCustomerBranch_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {

                long parentID = -1;
                if (ViewState["ParentID"] != null)
                {
                    long.TryParse(ViewState["ParentID"].ToString(), out parentID);
                }

                var customerBranchList = CustomerBranchManagement.GetAll(Convert.ToInt32(AuthenticationHelper.CustomerID), parentID, tbxFilter.Text);
                if (direction == SortDirection.Ascending)
                {
                    customerBranchList = customerBranchList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                }
                else
                {
                    customerBranchList = customerBranchList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                }


                foreach (DataControlField field in grdCustomerBranch.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdCustomerBranch.Columns.IndexOf(field);
                    }
                }

                grdCustomerBranch.DataSource = customerBranchList;
                grdCustomerBranch.DataBind();


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public void BindCustomerStatus()
        {
            try
            {
                ddlCustomerStatus.DataTextField = "Name";
                ddlCustomerStatus.DataValueField = "ID";

                ddlCustomerStatus.DataSource = Enumerations.GetAll<CustomerStatus>();
                ddlCustomerStatus.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        //added by Manisha
        private void BindLegalEntityType()
        {
            try
            {

                ddlLegalEntityType.DataTextField = "EntityTypeName";
                ddlLegalEntityType.DataValueField = "ID";
                ddlLegalEntityType.DataSource = UserManagementRisk.LegalentityAllData();
                ddlLegalEntityType.DataBind();
                ddlLegalEntityType.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindCompanyTypeType()
        {
            try
            {
                ddlCompanyType.DataTextField = "Name";
                ddlCompanyType.DataValueField = "ID";
                ddlCompanyType.DataSource = CustomerBranchManagement.GetAllComanyTypeCustomerBranch();

                ddlCompanyType.DataBind();
                ddlCompanyType.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }


        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdCustomerBranch.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                BindCustomerBranches();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdCustomerBranch.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
            }
            catch (Exception ex)
            {
                // ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }
        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        protected void btnSaveSubUnitType_Click(object sender, EventArgs e)
        {

            using (AuditControlEntities entities=new AuditControlEntities())
            {
                mst_NodeType mstnode = new mst_NodeType()
                {
                    Name = tbxSubunitType.Text,
                    isDelete = false,

                };
            
                
            bool CheckExistSubUnitType = ExistSubUnitType(tbxSubunitType.Text);
            if (!CheckExistSubUnitType)
            {
                AddSubUnitType(mstnode);
                SubUnitValidator.IsValid = false;
                SubUnitValidator.ErrorMessage = "Record save successfully.";
                SubUnitValidationSummary.CssClass = "alert alert-success";
                tbxSubunitType.Text = string.Empty;
            }
            else
            {
                SubUnitValidator.IsValid = false;
                SubUnitValidator.ErrorMessage = "Unit Type with same name already exists.";
                SubUnitValidationSummary.CssClass = "alert alert-danger";
               tbxSubunitType.Text = string.Empty;
            }
               
          }
            BindBranchTypes();
            UpdatePanelUnittype.Update();

        }
       
        public void AddSubUnitType(mst_NodeType msttype)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                entities.mst_NodeType.Add(msttype);
                entities.SaveChanges();
            }
        }
        public void AddLegalUnitType(mst_LegalEntityType msttype)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                entities.mst_LegalEntityType.Add(msttype);
                entities.SaveChanges();
            }
        }
        public static bool ExistLegalunitType(string legalUnittype)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var data = (from row in entities.mst_LegalEntityType
                            where row.EntityTypeName.Trim().ToUpper().Equals(legalUnittype.Trim().ToUpper())
                            select row).FirstOrDefault(); 
                if (data != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public static bool ExistSubUnitType(string subUnittype)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var data = (from row in entities.mst_NodeType
                            where row.Name.Trim().ToUpper().Equals(subUnittype.Trim().ToUpper())
                            && row.isDelete == false
                            select row).FirstOrDefault();
                if(data != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
      
        protected void lnkShowAddNewSubUnitModal_Click(object sender, ImageClickEventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "javascript:OpenAddNewSubUnitPopup()", true);
            tbxSubunitType.Text = string.Empty;
            UpdatePanelUnittype.Update();

        }

        protected void btnsType_Click(object sender, EventArgs e)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                mst_LegalEntityType mstnode = new mst_LegalEntityType()
                {
                    EntityTypeName = txtsubunittype.Text,
                    CustomerID = AuthenticationHelper.CustomerID,

                };


                bool CheckExistUnitType = ExistLegalunitType(txtsubunittype.Text);
                if (!CheckExistUnitType)
                {
                    AddLegalUnitType(mstnode);
                    CustomValidator1.IsValid = false;
                    CustomValidator1.ErrorMessage = "Record Saved Successfully.";
                    ValidationSummary1.CssClass = "alert alert-success";
                    txtsubunittype.Text = string.Empty;
                }
                else
                {
                    CustomValidator1.IsValid = false;
                    CustomValidator1.ErrorMessage = "Sub Unit type with same name already exists.";
                    ValidationSummary1.CssClass = "alert alert-danger";
                    txtsubunittype.Text = string.Empty;
                }
                
            }
            BindLegalEntityType();
            UpdatePanel1.Update();
        }

        protected void lnkSUnitType_Click(object sender, ImageClickEventArgs e)
        {
           ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialogNew", "javascript:OpenAddNewUnitPopup()", true);
            txtsubunittype.Text = string.Empty;
            UpdatePanel1.Update();
        }

        protected void UpdatePanelUnittype_Load(object sender, EventArgs e)
        {

        }

        protected void UpdatePanel1_Load(object sender, EventArgs e)
        {

        }

        protected void lnkExportCustomerBranchlist_Click(object sender, EventArgs e)
        {
            using (ExcelPackage exportPackge = new ExcelPackage())
            {
                try
                {
                    int CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    String FileName = String.Empty;
                    ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("BranchList");
                    var listOfBranch = UserManagementRisk.GetCustomerBranchReportList(CustomerId);
                    DataTable ExcelData = null;
                    DataView view = new System.Data.DataView((listOfBranch as List<SP_CustomerBranchReport_Result>).ToDataTable());
                    ExcelData = view.ToTable("Selected", false, "ParentBranch", "ChildBranch", "EntityType", "Type", "Industry", "AddressLine1", "AddressLine2", "StateName", "CityName", "PinCode", "ContactPerson", "LandLine", "Mobile", "EmailID", "LegalEntityType", "CreatedOn");
                    string CustomerName = CustomerManagement.CustomerGetByIDName(Convert.ToInt32(CustomerId));

                    FileName = "Customer Brach List";
                    foreach (DataRow item in ExcelData.Rows)
                    {
                        string BranchName = string.Empty;
                        if (!string.IsNullOrEmpty(Convert.ToString(item["ParentBranch"])))
                        {
                            BranchName = Convert.ToString(item["ParentBranch"]);
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(Convert.ToString(item["ChildBranch"])))
                            {
                                BranchName = Convert.ToString(item["ChildBranch"]);
                            }
                        }
                        if (!string.IsNullOrEmpty(BranchName))
                        {
                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                            {
                                var result = (from row in entities.CustomerBranches
                                              join row1 in entities.CompanyTypes
                                              on row.ComType equals row1.ID
                                              where row.Name.ToLower() == BranchName.ToLower()
                                              select row1.Name).FirstOrDefault();
                                if (result != null)
                                {
                                    item["Type"] = result;
                                }
                            }
                        }
                    }
                    exWorkSheet.Cells["A1"].LoadFromDataTable(ExcelData, true);

                    exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["A1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["A1"].Value = "Parent";
                    exWorkSheet.Cells["A1"].AutoFitColumns(30);
                    //exWorkSheet.Cells["A1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));

                    exWorkSheet.Cells["B1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["B1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["B1"].Value = "Location/Branch";
                    exWorkSheet.Cells["B1"].AutoFitColumns(30);
                    //exWorkSheet.Cells["B1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));

                    exWorkSheet.Cells["C1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["C1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["C1"].Value = "Sub  Entity Type";
                    exWorkSheet.Cells["C1"].AutoFitColumns(30);
                    //exWorkSheet.Cells["C1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));

                    exWorkSheet.Cells["D1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["D1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["D1"].Value = "Type";
                    exWorkSheet.Cells["D1"].AutoFitColumns(10);
                    // exWorkSheet.Cells["D1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));

                    exWorkSheet.Cells["E1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["E1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["E1"].Value = "Industry";
                    exWorkSheet.Cells["E1"].AutoFitColumns(20);
                    // exWorkSheet.Cells["E1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));

                    exWorkSheet.Cells["F1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["F1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["F1"].Value = "Address";
                    exWorkSheet.Cells["F1"].AutoFitColumns(50);
                    // exWorkSheet.Cells["F1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));

                    exWorkSheet.Cells["G1"].Value = "Address 2";
                    exWorkSheet.Cells["G1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["G1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["G1"].AutoFitColumns(50);
                    // exWorkSheet.Cells["G1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));

                    exWorkSheet.Cells["H1"].Value = "State";
                    exWorkSheet.Cells["H1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["H1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["H1"].AutoFitColumns(20);
                    //  exWorkSheet.Cells["H1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));

                    exWorkSheet.Cells["I1"].Value = "City";
                    exWorkSheet.Cells["I1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["I1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["I1"].AutoFitColumns(20);
                    //  exWorkSheet.Cells["I1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));

                    exWorkSheet.Cells["J1"].Value = "Pincode";
                    exWorkSheet.Cells["J1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["J1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["J1"].AutoFitColumns(20);
                    //  exWorkSheet.Cells["J1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));

                    exWorkSheet.Cells["K1"].Value = "Contact Person";
                    exWorkSheet.Cells["K1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["K1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["K1"].AutoFitColumns(10);
                    //  exWorkSheet.Cells["K1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));

                    exWorkSheet.Cells["L1"].Value = "Landline No.";
                    exWorkSheet.Cells["L1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["L1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["L1"].AutoFitColumns(20);
                    //  exWorkSheet.Cells["L1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));

                    exWorkSheet.Cells["M1"].Value = "Mobile No.";
                    exWorkSheet.Cells["M1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["M1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["M1"].AutoFitColumns(20);
                    //  exWorkSheet.Cells["M1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));

                    exWorkSheet.Cells["N1"].Value = "Email";
                    exWorkSheet.Cells["N1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["N1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["N1"].AutoFitColumns(30);
                    //  exWorkSheet.Cells["N1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));

                    exWorkSheet.Cells["O1"].Value = "Legal Entity Type";
                    exWorkSheet.Cells["O1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["O1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["O1"].AutoFitColumns(20);
                    //  exWorkSheet.Cells["O1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));

                    exWorkSheet.Cells["P1"].Value = "Created On";
                    exWorkSheet.Cells["P1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["P1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["P1"].AutoFitColumns(20);
                    //  exWorkSheet.Cells["J1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));


                    using (ExcelRange col = exWorkSheet.Cells[1, 1, 1 + ExcelData.Rows.Count, 16])
                    {
                        col.Style.WrapText = true;
                        col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                        // Assign borders
                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    }

                    Byte[] fileBytes = exportPackge.GetAsByteArray();
                    Response.ClearContent();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=Customer BranchList.xlsx");
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.ms-excel";
                    StringWriter sw = new StringWriter();
                    Response.BinaryWrite(fileBytes);
                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                }
            }
        }
    }
}