﻿<%@ Page Title="Customer Entities" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true"
    EnableEventValidation="false" CodeBehind="AuditCustomerBranch_List.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Customers.AuditCustomerBranch_List" %>

<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="asp" %>
<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls"  TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }

       
    </style>
     <style type="text/css">
        .dd_chk_select {
            
            height: 81px;
            height: 3px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc!important;        
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family:Roboto sans-serif !important;
            margin-bottom: 0px !important;
        }
         .chosen-results {  max-height: 75px !important
         }
        .chosen-container-single .chosen-single{background:none !important;border:1px solid #c7c7cc   !important;height: 32px  !important;padding: 3px 0px 0px 14px !important; font-family:'Roboto', sans-serif !important; }
        .EDIT_CUSTOMER_BRANCH a:hover{
            background-color:blue;
        }
         .table {
             margin-bottom: 0px;
         }
    </style> 

    <script type="text/javascript">
        function fopenpopup() {
            $('#divCustomerBranchesDialog').modal('show');
        };

        function OpenAddNewSubUnitPopup() {
            $('#AddSubUnitPopUp').modal('show');
           
        };
      
        function CloseWin() {
            $('#Newaddremider').modal('show');
        };

        function ClosePopSubUnit() {
            $('#AddSubUnitPopUp').modal('hide');

        }
        function OpenAddNewUnitPopup() {
            $('#AddUnitPopUp').modal('show');

        };

        function ClosePopUnit() {
            $('#AddUnitPopUp').modal('hide');

        }
      

    </script>
   
   

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">   
    <asp:UpdatePanel ID="upCustomerBranchList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel">                 
                        <div style="margin-bottom: 4px"/> 
                              <div class="col-md-12 colpadding0" style="margin-bottom: 5px;">
                                  <div style="text-align:left;" >
                                    <asp:DataList runat="server" ID="dlBreadcrumb" OnItemCommand="dlBreadcrumb_ItemCommand"
                                        RepeatLayout="Flow" RepeatDirection="Horizontal">
                                        <ItemTemplate>
                                            <asp:LinkButton Text='<%# Eval("Name") %>' CommandArgument='<%# Eval("ID") %>' CommandName="ITEM_CLICKED"
                                                runat="server" Style="text-decoration: none; color: #666;" />
                                        </ItemTemplate>
                                        <ItemStyle Font-Size="12" ForeColor="#666" Font-Underline="true" />
                                        <SeparatorStyle Font-Size="12" />
                                        <SeparatorTemplate>
                                            <span style="color: #666;">&gt;</span>
                                        </SeparatorTemplate>
                                    </asp:DataList>
                                  </div>
                                </div>

                            <div class="col-md-2 colpadding0 entrycount" style="margin-top: 5px;">
                                <div class="col-md-6 colpadding0">
                                <p style="color: #999; margin-top: 5px;">Show </p>
                                </div>
                                <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px; float: left"
                                    AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" >                        
                                    <asp:ListItem Text="5" Selected="True" />
                                    <asp:ListItem Text="10" />
                                    <asp:ListItem Text="20" />
                                    <asp:ListItem Text="50" />
                                </asp:DropDownList>
                            </div>                           
                            <div class="col-md-3 colpadding0 entrycount" id="divFilter" runat="server" style="margin-top: 5px;">
                                <div class="col-md-2 colpadding0" >
                                    <p style="color: #999; margin-top: 5px;">Filter</p>
                                </div>
                                <asp:TextBox runat="server" ID="tbxFilter" class="form-control" Style="Width:150px" MaxLength="50" AutoPostBack="true"
                                OnTextChanged="tbxFilter_TextChanged" />
                            </div>
                            <div style="text-align:right">
                                 <asp:LinkButton Text="Add New" CssClass="btn btn-primary" runat="server" OnClientClick="fopenpopup()" ID="btnAddCustomerBranch" OnClick="btnAddCustomerBranch_Click" />
                                <% if (IsAuditManager == "AH")%>
                                <% { %>
                                <asp:LinkButton Text="Export to Excel" CssClass="btn btn-primary" AutoPostBack="false" runat="server" ID="lnkExportCustomerBranchlist" OnClick="lnkExportCustomerBranchlist_Click" />
                                <% } %>
                            </div> 
                     
                            <div style="margin-bottom: 4px;"> 
                                &nbsp
                                <asp:GridView runat="server" ID="grdCustomerBranch" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%" 
                                OnSorting="grdCustomerBranch_Sorting" DataKeyNames="ID" OnRowCommand="grdCustomerBranch_RowCommand" OnPageIndexChanging="grdCustomerBranch_PageIndexChanging" >
                                <Columns>
                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex+1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="Name" HeaderText="Name" />
                                    <asp:BoundField DataField="TypeName" HeaderText="Type Name" />
                                    <asp:BoundField DataField="ContactPerson" HeaderText="Contact Person" />
                                    <asp:BoundField DataField="EmailID" HeaderText="Email" />                      
                                    <asp:BoundField DataField="Industry" HeaderText="Industry" Visible="false" SortExpression="Industry" />
                                    <asp:TemplateField HeaderText="Created On">
                                        <ItemTemplate>
                                            <%# ((DateTime)Eval("CreatedOn")).ToString("dd-MMM-yy HH:mm")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Sub Unit">
                                        <ItemTemplate>
                                            <asp:LinkButton runat="server" CommandName="VIEW_CHILDREN" CommandArgument='<%# Eval("ID") %>'>sub-units</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ItemStyle-HorizontalAlign="Justify" HeaderText="Action">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkButton1"   runat="server"   ToolTip="Edit Sub-units"  CommandName="EDIT_CUSTOMER_BRANCH" OnClientClick="fopenpopup()" CommandArgument='<%# Eval("ID") %>'><img src="../Images/edit_icon_new.png" data-toggle="tooltip" data-placement="top" alt="Edit Sub-unit"  title="Edit Sub-unit"/></asp:LinkButton>
                                            <asp:LinkButton ID="LinkButton2" runat="server" ToolTip="Delete Sub-units" CommandName="DELETE_CUSTOMER_BRANCH" CommandArgument='<%# Eval("ID") %>' style="display:none"
                                            OnClientClick="return confirm('This will also delete all the sub-units associated with current entity. Are you certain you want to delete this entity?');"><img src="../Images/delete_icon_new.png" data-toggle="tooltip" data-placement="top" alt="Delete Sub-unit" title="Delete Sub-unit" /></asp:LinkButton>
                                        </ItemTemplate>                       
                                    </asp:TemplateField>
                                </Columns>
                                <RowStyle CssClass="clsROWgrid" />
                                <HeaderStyle CssClass="clsheadergrid" /> 
                                <HeaderStyle BackColor="#ECF0F1" />
                                <PagerSettings Visible="false" />             
                                <PagerTemplate>
                                <%--<table style="display: none">
                                    <tr>
                                        <td>
                                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                        </td>
                                    </tr>
                                </table>--%>
                                </PagerTemplate>
                                <EmptyDataTemplate>
                                    No Record Found
                                </EmptyDataTemplate>
                                </asp:GridView>
                                  <div style="float: right;">
                                 <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true"
                      class="form-control m-bot15"  Width="120%" Height="30px"  OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">                                   
                  </asp:DropDownListChosen>  
                                      </div>
                            </div>
                            <div class="col-md-12 colpadding0">
                            <div class="col-md-5 colpadding0">
                                <div class="table-Selecteddownload">
                                    <div class="table-Selecteddownload-text">
                                        <p><asp:Label runat="server" ID="lblTotalSelected" Text="" style="color: #999; margin-right: 10px;"></asp:Label></p>
                                    </div>                                   
                                </div>
                            </div>
                            <div class="col-md-6 colpadding0"  style="float:right;">
                                <div class="table-paging" style="margin-bottom: 10px;">
                                    <%--<asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick ="lBPrevious_Click"/>--%>                                  
                                    <div class="table-paging-text" style="float:right;">
                                        <p>
                                            Page
                                            <%--<asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/--%>
                                            <%--<asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>--%>
                                        </p>
                                    </div>
                                    <%--<asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick ="lBNext_Click" />--%>                                   
                                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                </div>
                            </div>
                        </div>
                       </section>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="lnkExportCustomerBranchlist" />
        </Triggers>
    </asp:UpdatePanel>

    <div class="modal fade" id="divCustomerBranchesDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content" style="width: 650px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                  
                        <asp:UpdatePanel ID="upCustomerBranches" runat="server" UpdateMode="Conditional" OnLoad="upCustomerBranches_Load">
                            <ContentTemplate>
                                <div>
                                    <div style="margin-bottom: 7px">
                                        <asp:ValidationSummary id="valcustomerbranch" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                            ValidationGroup="CustomerBranchValidationGroup" />
                                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                            ValidationGroup="CustomerBranchValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                                    </div>
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                            Customer</label>
                                        <span style="color: black; font-size: 13px; margin-left: 1%;">
                                            <asp:Literal ID="litCustomer" runat="server" /></span>
                                    </div>
                                    <div id="divParent" runat="server" style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                            Parent</label>
                                         <span style="color: black; font-size: 13px; margin-left: 1%;">
                                               <asp:Literal ID="litParent" runat="server" />
                                    </div>
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                            Name</label>
                                        <asp:TextBox runat="server" ID="tbxName" CssClass="form-control" autocomplete="off" Style="width: 300px;" MaxLength="500" />
                                        <asp:RequiredFieldValidator ErrorMessage="Please Enter Name." ControlToValidate="tbxName"
                                            runat="server" ValidationGroup="CustomerBranchValidationGroup" Display="None" />
                                        <asp:RegularExpressionValidator Display="None" runat="server" ValidationGroup="CustomerBranchValidationGroup"
                                            ErrorMessage="Please enter a valid name." ControlToValidate="tbxName" ValidationExpression="[a-zA-Z0-9\s()\/@#.,-]*$"></asp:RegularExpressionValidator>                                      
                                    </div>
                                    <%--<div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                            Vertical Branch</label>
                                        <asp:DropDownCheckBoxes ID="ddlVerticalBranch" runat="server" AutoPostBack="true" Visible="true" CssClass="form-control m-bot15"
                                            AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True" Style="padding: 0px; margin: 0px; width: 300px; height:50px; ">
                                            <Style SelectBoxWidth="300" DropDownBoxBoxWidth="300" DropDownBoxBoxHeight="130" />
                                            <Texts SelectBoxCaption="Select Vertical Branch" />
                                        </asp:DropDownCheckBoxes>
                                    </div>--%>
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                             Unit Type</label>
                                        <asp:DropDownList runat="server" ID="ddlType" Style="padding: 0px; margin: 0px; width: 300px;"
                                            CssClass="form-control m-bot15" AutoPostBack="true" OnSelectedIndexChanged="ddlType_SelectedIndexChanged" />
                                        <asp:CompareValidator ErrorMessage="Please select  Unit Type." ControlToValidate="ddlType"
                                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="CustomerBranchValidationGroup"
                                            Display="None" />
                                          <div style="margin-top: -29px;margin-left: 523px;">
                                               <%--<asp:ImageButton ID="lnkShowAddNewVendorModal" runat="server" ImageUrl="/Images/add_icon_new.png" OnClientClick="OpenAddNewVendorPopup(); " 
                                                 data-toggle="tooltip" data-placement="bottom" ToolTip="Add New Sub Unit Type" />--%>
                                            <asp:ImageButton ID="lnkShowAddNewVendorModal" runat="server" ImageUrl="/Images/add_icon_new.png" OnClick="lnkShowAddNewSubUnitModal_Click"
                                                 data-toggle="tooltip" data-placement="bottom" ToolTip="Add New Unit Type" />
                                          </div>
                                      </div>
                                   

                                    <div runat="server" id="divCompanyType" style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label id="lblType" style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                            Type</label>
                                        <asp:DropDownList runat="server" ID="ddlCompanyType" Style="padding: 0px; margin: 0px; width: 300px;"
                                            CssClass="form-control m-bot15" />
                                        <%--     <asp:CompareValidator ErrorMessage="Please select Type." ControlToValidate="ddlCompanyType"
                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="CustomerBranchValidationGroup"
                            Display="None" />--%>
                                    </div>
                                    <div runat="server" id="divLegalEntityType" style="margin-bottom: 7px" visible="false">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label id="lblLegalEntityType" style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                          Sub Unit Type</label>
                                        <asp:DropDownList runat="server" ID="ddlLegalEntityType" Style="padding: 0px; margin: 0px; width: 300px;"
                                            CssClass="form-control m-bot15" />
                                      <div style="margin-top: -29px;margin-left: 523px;">
                                         <asp:ImageButton ID="lnkSUnitType" runat="server" ImageUrl="/Images/add_icon_new.png" OnClick="lnkSUnitType_Click"
                                                 data-toggle="tooltip" data-placement="bottom" ToolTip="Add New Sub Unit Type" />
                                      </div>
                                        
                                        <%--        <asp:CompareValidator ErrorMessage="Please select Unit Type." ControlToValidate="ddlLegalEntityType"
                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="CustomerBranchValidationGroup"
                            Display="None" />--%>
                                    </div>
                                    <div runat="server" id="divLegalRelationship" style="margin-bottom: 7px" visible="false">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                            Legal Relationship</label>
                                        <asp:DropDownList runat="server" ID="ddlLegalRelationShip" Style="padding: 0px; margin: 0px; width: 300px;"
                                            CssClass="form-control m-bot15" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="Please select legal relationship."
                                            ControlToValidate="ddlLegalRelationShip" runat="server" ValidationGroup="CustomerBranchValidationGroup"
                                            Display="None" />
                                    </div>
                                    <div runat="server" id="divIndustry" style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                            Industry</label>
                                        <asp:DropDownList runat="server" ID="ddlIndustry" Style="padding: 0px; margin: 0px; width: 300px;"
                                            CssClass="form-control m-bot15" AutoPostBack="true" OnSelectedIndexChanged="ddlIndustry_SelectedIndexChanged" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Please select Industry."
                                            ControlToValidate="ddlIndustry" runat="server" ValidationGroup="CustomerBranchValidationGroup"
                                            Display="None" />
                                    </div>
                                    <div style="margin-bottom: 7px" runat="server" id="divlegalstatus" visible="false">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label runat="server" id="lblLegalRelationShipOrStatus" style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                            Legal Status</label>
                                        <asp:DropDownList runat="server" ID="ddlLegalRelationShipOrStatus" Style="padding: 0px; margin: 0px; width: 300px;"
                                            CssClass="form-control m-bot15" />
                                     </div>
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                            Address Line 1</label>
                                        <asp:TextBox runat="server" ID="tbxAddressLine1" class="form-control" Style="width: 300px;"
                                            MaxLength="150" />
                                        <asp:RequiredFieldValidator ErrorMessage="Please Enter Address Line 1." ControlToValidate="tbxAddressLine1"
                                            runat="server" ValidationGroup="CustomerBranchValidationGroup" Display="None" />
                                    </div>
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                            Unit Details</label>
                                        <asp:TextBox runat="server" ID="tbxAddressLine2" class="form-control" Style="width: 300px;"
                                            MaxLength="150" />
                                    </div>
                                    <div runat="server" id="divState" style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                            State</label>
                                        <asp:DropDownList runat="server" ID="ddlState" Style="padding: 0px; margin: 0px; width: 300px;"
                                            CssClass="form-control m-bot15" AutoPostBack="true" OnSelectedIndexChanged="ddlState_SelectedIndexChanged" />
                                        <asp:CompareValidator ErrorMessage="Please select State." ControlToValidate="ddlState"
                                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="CustomerBranchValidationGroup"
                                            Display="None" />
                                    </div>
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                            City</label>
                                        <asp:DropDownList runat="server" ID="ddlCity" Style="padding: 0px; margin: 0px; width: 300px;"
                                            CssClass="form-control m-bot15" AutoPostBack="true" OnSelectedIndexChanged="ddlCity_SelectedIndexChanged" />
                                        <asp:CompareValidator ErrorMessage="Please select City." ControlToValidate="ddlCity"
                                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="CustomerBranchValidationGroup"
                                            Display="None" />
                                    </div>
                                    <div runat="server" id="divOther" style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                            Others</label>
                                        <asp:TextBox runat="server" ID="tbxOther" class="form-control" Style="width: 300px;" MaxLength="50" />
                                        <asp:RequiredFieldValidator ErrorMessage="Other City Name can not be empty." ControlToValidate="tbxOther"
                                            runat="server" ValidationGroup="CustomerBranchValidationGroup" Display="None" />
                                    </div>
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                            Pin Code</label>
                                        <asp:TextBox runat="server" ID="tbxPinCode" class="form-control" Style="width: 300px;" MaxLength="6" />
                                        <%--   <asp:RequiredFieldValidator ErrorMessage="Pin Code can not be empty." ControlToValidate="tbxPinCode"
                            runat="server" ValidationGroup="CustomerBranchValidationGroup" Display="None" />
                        <asp:RegularExpressionValidator Display="None" runat="server" ValidationGroup="CustomerBranchValidationGroup"
                            ErrorMessage="Please enter a valid pin code." ControlToValidate="tbxPinCode"
                            ValidationExpression="^[0-9]{6}$"></asp:RegularExpressionValidator>--%>
                                    </div>
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                            Contact Person</label>
                                        <asp:TextBox runat="server" ID="tbxContactPerson" class="form-control" Style="width: 300px;"
                                            MaxLength="200" />
                                        <asp:RequiredFieldValidator ErrorMessage="Please Enter Contact Person Name."
                                            ControlToValidate="tbxContactPerson" runat="server" ValidationGroup="CustomerBranchValidationGroup"
                                            Display="None" />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" Display="None" runat="server"
                                            ValidationGroup="CustomerBranchValidationGroup" ErrorMessage="Please enter a valid contact person name."
                                            ControlToValidate="tbxContactPerson" ValidationExpression="^[a-zA-Z_ .-]*$"></asp:RegularExpressionValidator>
                                    </div>
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                            Landline No.</label>
                                        <asp:TextBox runat="server" ID="tbxLandline" class="form-control" Style="width: 300px;"
                                            MaxLength="15" />
                                        <%--      <asp:RequiredFieldValidator ErrorMessage="Landline Number can not be empty." ControlToValidate="tbxLandline"
                            runat="server" ValidationGroup="CustomerBranchValidationGroup" Display="None" />--%>
                                        <%--  <asp:RegularExpressionValidator Display="None" runat="server" ValidationGroup="CustomerBranchValidationGroup"
                            ErrorMessage="Please enter a valid landline number." ControlToValidate="tbxLandline"
                            ValidationExpression="^[\+\d]+(?:[\d-.\s()]*)$"></asp:RegularExpressionValidator>--%>
                                    </div>
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                            Mobile No.</label>
                                        <asp:TextBox runat="server" ID="tbxMobile" class="form-control" Style="width: 300px;" MaxLength="15" />
                                        <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Mobile Number can not be empty."
                            ControlToValidate="tbxMobile" runat="server" ValidationGroup="CustomerBranchValidationGroup"
                            Display="None" />--%>
                                        <%--  <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="None" runat="server"
                            ValidationGroup="CustomerBranchValidationGroup" ErrorMessage="Please enter a valid mobile number."
                            ControlToValidate="tbxMobile" ValidationExpression="^[\+\d]+(?:[\d-.\s()]*)$"></asp:RegularExpressionValidator>--%>
                                    </div>
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                            Email</label>
                                        <asp:TextBox runat="server" ID="tbxEmail" class="form-control" Style="width: 300px;" MaxLength="200" />
                                        <asp:RequiredFieldValidator ErrorMessage="Please Enter Email-ID." ControlToValidate="tbxEmail"
                                            runat="server" ValidationGroup="CustomerBranchValidationGroup" Display="None" />
                                        <asp:RegularExpressionValidator Display="None" runat="server" ValidationGroup="CustomerBranchValidationGroup"
                                            ErrorMessage="Please enter a valid email." ControlToValidate="tbxEmail" ValidationExpression="^([\w-_]+\.)*[\w-_]+@([\w-_]+\.)*[\w-_]+\.[\w-_]+$"></asp:RegularExpressionValidator>
                                    </div>
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                            Status</label>
                                        <asp:DropDownList runat="server" ID="ddlCustomerStatus" Style="padding: 0px; margin: 0px; width: 300px;"
                                            CssClass="form-control m-bot15" />
                                    </div>
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                            Person Responsible Applicable</label>
                                        <asp:DropDownList runat="server" ID="ddlPersonResponsibleApplicable" Style="padding: 0px; margin: 0px; width: 300px;"
                                            CssClass="form-control m-bot15">
                                            <asp:ListItem Value="1" Text="Yes"></asp:ListItem>
                                            <asp:ListItem Value="0" Text="No"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div style="margin-bottom: 7px; margin-left: 200px; margin-top: 25px;">
                                        <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="btn btn-primary"
                                            ValidationGroup="CustomerBranchValidationGroup" />
                                        <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="btn btn-primary" data-dismiss="modal" />
                                    </div>
                                </div>
                                <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 10px;">
                                    <p style="color: red;"><strong>Note:</strong> (*) fields are compulsory</p>
                                </div>
                                <div class="clearfix" style="height: 50px">
                                </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                   
                </div>
            </div>
        </div>
    </div>
      <%--Add New  Unit--%>
           <%-- <div class="modal fade" id="AddSubUnitPopUp" tabindex="-1" style="overflow: hidden;width: 520px;margin-left: 327px;margin-top: 168px;" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">--%>
    <div class="modal fade" id="AddSubUnitPopUp" tabindex="-1" style="overflow: hidden;margin-left: 51px;margin-top: 168px;" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog w90per">
                    <div class="modal-content">
                        <div class="modal-header">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label class="modal-header-custom">
                                Add New Unit Type</label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="ClosePopSubUnit()">&times;</button>
                        </div>
                        <%--<div class="modal-body" style="width: 100%;">
                            <iframe src="about:blank" id="IframeSubUnit" frameborder="0" runat="server" width="100%" height="500px"></iframe>
                        </div>--%>
                     <div class="modal-body" style="width: 100%;">
                        <asp:UpdatePanel ID="UpdatePanelUnittype" runat="server" UpdateMode="Conditional" OnLoad="UpdatePanelUnittype_Load">
                            <ContentTemplate>
                                <div style="margin-bottom: 7px">
                                    <asp:ValidationSummary ID="SubUnitValidationSummary" runat="server" Display="none" class="alert alert-block alert-danger fade in" ValidationGroup="SubUnitAddPopupValidationGroup" />
                                    <asp:CustomValidator ID="SubUnitValidator" runat="server" EnableClientScript="False"
                                        ValidationGroup="SubUnitAddPopupValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                                </div>
                                <div style="margin-bottom: 7px; margin-top: 20px; align-content: center">
                                    <label style="width: 3%; display: block; float: left; margin-top: 5px; font-size: 13px; color: red;">*</label>
                                    <label style="width: 27%; display: block; float: left; margin-top: 5px; font-size: 13px; color: #333;">
                                         Unit Type</label>
                                    <asp:TextBox runat="server" ID="tbxSubunitType" CssClass="form-control" Style="width: 68%;" MaxLength="100" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Unit Type can not be empty." ControlToValidate="tbxSubunitType"
                                        runat="server" ValidationGroup="SubUnitAddPopupValidationGroup" Display="None" />
                                </div>

                                <div style="margin-bottom: 7px; margin-left: 140px; margin-top: 10px">
                                    <div style="margin-bottom: 7px; text-align: center">
                                        <asp:Button Text="Save" runat="server" ID="btnSaveSubUnitType"  CssClass="btn btn-primary" OnClick="btnSaveSubUnitType_Click" ValidationGroup="SubUnitAddPopupValidationGroup" />
                                        <asp:Button Text="Close" runat="server" ID="btnCancelCityPopUp" CssClass="btn btn-primary" data-dismiss="modal" OnClientClick="CloseMeCity();" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <p style="color: red;"><strong>Note:</strong> (*) fields are Compulsory</p>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    </div>
                </div>
            </div>

     <%--Add New Sub Unit--%>
           <%-- <div class="modal fade" id="AddSubUnitPopUp" tabindex="-1" style="overflow: hidden;width: 520px;margin-left: 327px;margin-top: 168px;" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">--%>
    <div class="modal fade" id="AddUnitPopUp" tabindex="-1" style="overflow: hidden;width: 520px;margin-left: 327px;margin-top: 168px;" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog w90per">
                    <div class="modal-content">
                        <div class="modal-header">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label class="modal-header-custom">
                                Add Sub New Unit Type</label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="ClosePopUnit()">&times;</button>
                        </div>
                        <%--<div class="modal-body" style="width: 100%;">
                            <iframe src="about:blank" id="IframeSubUnit" frameborder="0" runat="server" width="100%" height="500px"></iframe>
                        </div>--%>
                     <div class="modal-body" style="width: 100%;">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional" OnLoad="UpdatePanel1_Load">
                            <ContentTemplate>
                                <div style="margin-bottom: 7px">
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" Display="none" class="alert alert-block alert-danger fade in" ValidationGroup="SubAddPopupValidationGroup" />
                                    <asp:CustomValidator ID="CustomValidator1" runat="server" EnableClientScript="False"
                                        ValidationGroup="SubAddPopupValidationGroup" Display="none" class="alert alert-block alert-danger fade in"/>
                                </div>
                                <div style="margin-bottom: 7px; margin-top: 20px; align-content: center">
                                    <label style="width: 3%; display: block; float: left; margin-top: 5px; font-size: 13px; color: red;">*</label>
                                    <label style="width: 27%; display: block; float: left; margin-top: 5px; font-size: 13px; color: #333;">
                                        Sub Unit Type</label>
                                    <asp:TextBox runat="server" ID="txtsubunittype" CssClass="form-control" Style="width: 68%;" MaxLength="100" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ErrorMessage="Sub Unit can not be empty." ControlToValidate="txtsubunittype"
                                        runat="server" ValidationGroup="SubAddPopupValidationGroup" Display="None" />
                                </div>

                                <div style="margin-bottom: 7px; margin-left: 140px; margin-top: 10px">
                                    <div style="margin-bottom: 7px; text-align: center">
                                        <asp:Button Text="Save" runat="server" ID="btnsType"  CssClass="btn btn-primary" OnClick="btnsType_Click" ValidationGroup="SubAddPopupValidationGroup" />
                                        <asp:Button Text="Close" runat="server" ID="btnsclose" CssClass="btn btn-primary" data-dismiss="modal" OnClientClick="ClosePopUnit()();" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <p style="color: red;"><strong>Note:</strong> (*) fields are Compulsory</p>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    </div>
                </div>
            </div>

    <script type="text/javascript">
        function initializeRadioButtonsList(controlID) {
            $(controlID).buttonset();
        }
        $(document).ready(function () {
            setactivemenu('Unit Master');
            fhead('Unit Master');
        });
    </script>
</asp:Content>
