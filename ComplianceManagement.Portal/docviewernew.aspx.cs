﻿using Microsoft.IdentityModel.Protocols;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal
{
    public partial class docviewernew : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["docurl"]))
                {
                    string filepath = Request.QueryString["docurl"].ToString();
                    if (filepath != "undefined")
                    {
                        if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                        {

                            doccontrol.Document = ConfigurationManager.AppSettings["DriveUrl"] + "/" + Convert.ToString(Request.QueryString["docurl"]);
                        }
                        else
                        {
                            doccontrol.Document = Server.MapPath(Convert.ToString(Request.QueryString["docurl"]));
                        }
                    }
                    else
                    {
                        //lblMessage.Text = "Sorry File not find";
                    }
                }
            }
        }
    }
}