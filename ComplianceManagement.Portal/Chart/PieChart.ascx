﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PieChart.ascx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Chart.PieChart" %>
<style type="text/css">
    
    #div_<%= ClientID %>_Tooltip
    {
        position: absolute;
        width: 200px;
        height: auto;
        padding: 10px;
        background-color: white;
        -webkit-border-radius: 10px;
        -moz-border-radius: 10px;
        border-radius: 10px;
        -webkit-box-shadow: 4px 4px 10px rgba(0, 0, 0, 0.4);
        -moz-box-shadow: 4px 4px 10px rgba(0, 0, 0, 0.4);
        box-shadow: 4px 4px 10px rgba(0, 0, 0, 0.4);
        pointer-events: none;
    }
    
    #div_<%= ClientID %>_Tooltip.hidden
    {
        display: none;
    }
    
    #div_<%= ClientID %>_Tooltip p
    {
        margin: 0;
        font-family: sans-serif;
        font-size: 16px;
        line-height: 20px;
    }
</style>
<div id="div_<%= ClientID %>_Chart">
</div>
<div id="div_<%= ClientID %>_Tooltip" class="hidden">
    <p>
        <strong><span id="name">Label Heading</span></strong></p>
    <p>
        <span id="value">100</span></p>
    <% if (DoPostBack)
       { %>
            <asp:Button ID="btnPostback" runat="server" Style="display: none" OnClick="btnPostback_Click" />
            <asp:HiddenField ID="hdnSelectedID" runat="server" />
    <% } %>
</div>
<script type="text/javascript">
    function <%= ClientID %>_Initialize(dataset) {
       var w = <%= Width %>;
        var h = <%= Height %>;
        var outerRadius = w / 2;
        var innerRadius = 0;
        var arc = d3.svg.arc()
							.innerRadius(innerRadius)
							.outerRadius(outerRadius);

        var pie = d3.layout.pie().value(function (d) {
            return d.Count;
        });

        //Easy colors accessible via a 10-step ordinal scale
        var color = d3.scale.category10();

        //Create SVG element
        var svg = d3.select("#div_<%= ClientID %>_Chart").html('')
						.append("svg")
						.attr("width", w)
						.attr("height", h);

        if (dataset.length == 0)
        {
            svg.append("text")
			    .attr("transform", "translate(" + (w / 2) + "," + (h / 2) + ")")
			    .attr("text-anchor", "middle")
				.attr("fill", "black")
			    .text('No data available');

            return;
        }
        //Set up groups
        var arcs = svg.selectAll("g.arc")
						  .data(pie(dataset))
						  .enter()

						  .append("g")
						  .attr("class", "arc")
						  .attr("transform", "translate(" + outerRadius + "," + outerRadius + ")");

        //Draw arc paths
        arcs.append("path")
			    .attr("fill", function (d, i) { return color(i); })
			    .attr("d", arc)
				.attr("stroke", "white")
                <% if (DoPostBack) { %>
				.attr("style", "cursor: hand")
                <% } %>
				.attr('opacity', '1')
                .attr ('title', function (d, i) { return dataset[i].Name + ': ' + dataset[i].Count; })
				.on('mouseover', function (d, i) {
				    d3.select(this)
					.attr('opacity', '1');
				    //Get this bar's x/y values, then augment for the tooltip

				})
				.on('mouseout', function (d) {
				    d3.select(this)
					.attr('opacity', '1');
				})
                .on('click', function (d, i) {
                      <% if (DoPostBack)
                       { %>
                            $('#<%= hdnSelectedID.ClientID %>').val(i);
                            $('#<%= btnPostback.ClientID %>').click();
                    <% } %>				   
				});

               
        //Labels
        arcs.append("text")
			    .attr("transform", function (d) {
			        return "translate(" + arc.centroid(d) + ")";
			    })
			    .attr("text-anchor", "middle")
				.attr("fill", "white")
			    .text(function (d) {
			        return d.value;
			    });

            $('path').tooltip({
                track: true
            });
    }
</script>
