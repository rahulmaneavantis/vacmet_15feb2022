﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using com.VirtuosoITech.ComplianceManagement.Business.Data;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Chart
{
    public partial class BarChart : System.Web.UI.UserControl
    {
        public int Height { get; set; }
        public int Width { get; set; }
        public List<DataItem> DataSource { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            if (DataSource == null)
            {
                DataSource = new List<DataItem>();
            }
            string dataSouce = DataSource.ToJSON();
            ScriptManager.RegisterStartupScript(this, this.GetType(), ClientID + "_Initialize", string.Format("{0}_Initialize({1});", ClientID, dataSouce), true);
        }
    }

}