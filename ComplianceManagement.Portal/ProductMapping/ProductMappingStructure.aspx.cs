﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using com.VirtuosoITech.ComplianceManagement.Portal.Users;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.ProductMapping
{
    public partial class ProductMappingStructure : System.Web.UI.Page
    {
        protected string Approveruser_Roles;
        protected List<Int32> roles;
        public static string LogoName;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {     
                    int CustID = -1;
                LogoName = null;
                if (AuthenticationHelper.CustomerID == 0)
                {
                    CustID = Convert.ToInt32(Session["CustomerID_new"]);
                }
                else
                {
                    CustID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                Customer objCust = UserManagement.GetCustomerforLogo(CustID);                
                if (objCust != null)
                {
                    if (objCust.LogoPath != null)
                    {
                        var CustomerLogo = objCust.LogoPath;
                        LogoName = CustomerLogo.Replace("~", "..");
                    }
                }
                if (!IsPostBack)
                {
                    long customerID = -1;
                    //customerID = Common.AuthenticationHelper.CustomerID;
                    int userID = Common.AuthenticationHelper.UserID;
                    User loggedInUser = UserManagement.GetByID(userID);
                    mst_User loggedInmstUser = UserManagementRisk.GetByID(userID);

                    if (loggedInUser.CustomerID != null)
                        customerID = Convert.ToInt32(loggedInUser.CustomerID);                    
                    else
                        customerID = Common.AuthenticationHelper.CustomerID;

                    List<long> ProductMappingDetails = new List<long>();
                    ProductMappingDetails = UserManagement.GetByProductIDList(Convert.ToInt32(customerID));

                    //var ProductMappingDetails = UserManagement.GetByProductIDList(Convert.ToInt32(customerID));
                    if (ProductMappingDetails.Count > 3)
                    {
                        dvbtnCompliance.Attributes["style"] = "width: 30%; margin-top:3%; margin-left:3%;";
                        dvbtnLitigation.Attributes["style"] = "width: 30%; margin-top:3%; margin-left:3%;";
                        dvbtnContract.Attributes["style"] = "width: 30%; margin-top:3%; margin-left:3%;";
                        dvbtnLicense.Attributes["style"] = "width: 30%; margin-top:3%; margin-left:3%;";
                        dvbtnVendor.Attributes["style"] = "width: 30%; margin-top:3%; margin-left:3%;";
                        dvbtnHRProduct.Attributes["style"] = "width: 30%; margin-top:3%; margin-left:3%;";
                        dvbtnSecretarial.Attributes["style"] = "width: 30%; margin-top:3%; margin-left:3%;";
                        dvbtnInsiderTrading.Attributes["style"] = "width: 30%; margin-top:3%; margin-left:3%;";
                        dvbtnVendorAuditNew.Attributes["style"] = "width: 30%; margin-top:3%; margin-left:3%;";
                        dvbtnInternalControl.Attributes["style"] = "width: 30%; margin-top:3%; margin-left:3%;";
                        dvbtnAudit.Attributes["style"] = "width: 30%; margin-top:3%; margin-left:3%;";
                    }
                    if (ProductMappingDetails.Contains(1))
                    {
                        dvbtnCompliance.Visible = true;
                        btnCompliance.Visible = true;
                    }
                    else
                    {
                        dvbtnCompliance.Attributes["style"] = "display:none;";
                        btnCompliance.Visible = false;
                    }
                    bool showLitigationProduct = false;
                    if (ProductMappingDetails.Contains(2))
                    {
                        if (loggedInUser != null)
                        {
                            if (loggedInUser.LitigationRoleID != null)
                            {
                                showLitigationProduct = true;
                            }
                        }
                    }
                    if (showLitigationProduct)
                    {
                        dvbtnLitigation.Visible = true;
                        btnLitigation.Visible = true;
                    }
                    else
                    {
                        dvbtnLitigation.Attributes["style"] = "display:none;";
                        btnLitigation.Visible = false;
                    }
                    if (ProductMappingDetails.Contains(3))
                    {
                        if (loggedInmstUser.RoleID != -1)
                        {
                            dvbtnInternalControl.Visible = true;
                            btnInternalControl.Visible = true;
                        }
                        else
                        {
                            dvbtnInternalControl.Attributes["style"] = "display:none;";
                            btnInternalControl.Visible = false;
                        }
                    }
                    else
                    {
                        dvbtnInternalControl.Attributes["style"] = "display:none;";
                        btnInternalControl.Visible = false;
                    }
                    if (ProductMappingDetails.Contains(4))
                    {
                        if (loggedInmstUser.RoleID != -1)
                        {
                            dvbtnAudit.Visible = true;
                            btnAudit.Visible = true;
                        }
                        else
                        {
                            dvbtnAudit.Attributes["style"] = "display:none;";
                            btnAudit.Visible = false;
                        }
                    }
                    else
                    {
                        dvbtnAudit.Attributes["style"] = "display:none;";
                        btnAudit.Visible = false;
                    }
                    bool showContractProduct = false;
                    if (ProductMappingDetails.Contains(5))
                    {
                        if (loggedInUser != null)
                        {
                            if (loggedInUser.ContractRoleID != null)
                            {
                                showContractProduct = true;
                            }
                        }
                    }
                    if (showContractProduct)
                    {
                        dvbtnContract.Visible = true;
                        btnContract.Visible = true;
                    }
                    else
                    {
                        dvbtnContract.Attributes["style"] = "display:none;";
                        btnContract.Visible = false;
                    }
                    bool showLicenseManagement = false;
                    if (ProductMappingDetails.Contains(6))
                    {
                        if (loggedInUser != null)
                        {
                            if (loggedInUser.LicenseRoleID != null)
                            {
                                showLicenseManagement = true;
                            }
                        }
                    }
                    if (showLicenseManagement)
                    {
                        dvbtnLicense.Visible = true;
                        btnLicense.Visible = true;
                    }
                    else
                    {
                        dvbtnLicense.Attributes["style"] = "display:none;";
                        btnLicense.Visible = false;
                    }


                    bool showVendorAuditManagement = false;
                    if (ProductMappingDetails.Contains(11))
                    {
                        if (loggedInUser != null)
                        {
                            if (loggedInUser.VendorAuditRoleID != null)
                            {
                                showVendorAuditManagement = true;
                            }
                        }
                    }
                    if (showVendorAuditManagement)
                    {
                        dvbtnVendorAuditNew.Visible = true;
                        btnVendor.Visible = true;
                    }
                    else
                    {
                        dvbtnVendorAuditNew.Attributes["style"] = "display:none;";
                        btnVendorAudit.Visible = false;
                    }


                    bool showVendorManagement = false;
                    if (ProductMappingDetails.Contains(7))
                    {
                        if (loggedInUser != null)
                        {
                            if (loggedInUser.VendorRoleID != null)
                            {
                                showVendorManagement = true;
                            }
                        }
                    }
                    if (showVendorManagement)
                    {
                        dvbtnVendor.Visible = true;
                        btnVendor.Visible = true;
                    }
                    else
                    {
                        dvbtnVendor.Attributes["style"] = "display:none;";
                        btnVendor.Visible = false;
                    }

                    bool showSecretarialProduct = false;
                    if (ProductMappingDetails.Contains(8))
                    {
                        if (loggedInUser != null)
                        {
                            if (loggedInUser.SecretarialRoleID != null)
                            {
                                showSecretarialProduct = true;
                            }
                        }
                    }
                    if (showSecretarialProduct)
                    {
                        dvbtnSecretarial.Visible = true;
                        btnSecretarial.Visible = true;
                    }
                    else
                    {
                        dvbtnSecretarial.Attributes["style"] = "display:none;";
                        btnSecretarial.Visible = false;
                    }




                    //---------------HR
                    bool showHRProduct = false;
                    if (ProductMappingDetails.Contains(9))
                    {
                        if (loggedInUser != null)
                        {
                            if (loggedInUser.HRRoleID != null) 
                            {
                                showHRProduct = true;
                            }
                        }
                    }
                    if (showHRProduct)
                    {
                        dvbtnHRProduct.Visible = true;
                        btnHRProduct.Visible = true;
                    }
                    else
                    {
                        dvbtnHRProduct.Attributes["style"] = "display:none;";
                        btnHRProduct.Visible = false;
                    }

                    //---------------Insider Trading
                    bool showInsiderTrading = false;
                    if (ProductMappingDetails.Contains(10))
                    {
                        if (loggedInUser != null)
                        {
                            if (loggedInUser.InsiderRoleID != null)
                            {
                                showInsiderTrading = true;
                            }
                        }
                    }
                    if (showInsiderTrading)
                    {
                        dvbtnInsiderTrading.Visible = true;
                        btnInsiderTrading.Visible = true;
                    }
                    else
                    {
                        dvbtnInsiderTrading.Attributes["style"] = "display:none;";
                        btnInsiderTrading.Visible = false;
                    }

                    cid.Value = Convert.ToString(Common.AuthenticationHelper.CustomerID);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void ProcessAuthenticationInformation_Compliance(User user)
        {
            try
            {
                string name = string.Format("{0} {1}", user.FirstName, user.LastName);
                string role = RoleManagement.GetByID(user.RoleID).Code;
                int checkInternalapplicable = 0;
                int checkTaskapplicable = 0;
                int checkVerticalapplicable = 0;
                int checkLabelApplicable = 0;
                bool IsPaymentCustomer = false;

                int complianceProdType = 0;

                if (user.CustomerID == null)
                {
                    checkInternalapplicable = 2;
                    checkTaskapplicable = 2;
                    checkVerticalapplicable = 2;
                    checkLabelApplicable = 2;
                    complianceProdType = 0;
                }
                else
                {
                    var IsInternalComplianceApplicable = Common.AuthenticationHelper.IComplilanceApplicable;
                    var IsTaskApplicable = Common.AuthenticationHelper.TaskApplicable;
                    var Isverticlapplicable = Common.AuthenticationHelper.IsVerticalApplicable;
                    var IsLabelApplicable = Common.AuthenticationHelper.IsLabelApplicable;
                    var valCompProdType = Common.AuthenticationHelper.ComplianceProductType;

                    checkTaskapplicable = Convert.ToInt32(IsTaskApplicable);
                    checkInternalapplicable = Convert.ToInt32(IsInternalComplianceApplicable);
                    checkVerticalapplicable = Convert.ToInt32(Isverticlapplicable);
                    checkLabelApplicable = Convert.ToInt32(IsLabelApplicable);
                    complianceProdType = Convert.ToInt32(valCompProdType);
                }
                FormsAuthenticationRedirect_Compliance(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                if (user.ChangPasswordDate != null)
                {
                    DateTime LastPasswordChangedDate = Convert.ToDateTime(user.ChangPasswordDate);
                    DateTime currentDate = DateTime.Now;
                    LastPasswordChangedDate = LastPasswordChangedDate != null ? LastPasswordChangedDate : DateTime.Now;
                    int noDays = Convert.ToInt32(ConfigurationManager.AppSettings["ChangedPasswordDays"]);
                    int dateDifference = Convert.ToInt32((currentDate - LastPasswordChangedDate).TotalDays);
                    if (dateDifference == noDays || dateDifference > noDays)
                    {
                        Session["ChangePassword"] = true;
                        Response.Redirect("~/Account/ChangePassword.aspx", false);
                    }
                    else
                    {
                        Session["ChangePassword"] = false;
                    }
                }
                else
                {
                    Session["ChangePassword"] = true;
                    Response.Redirect("~/Account/ChangePassword.aspx", false);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void ProcessAuthenticationInformation_Audit(mst_User user) //IFC
        {
            try
            {
                string name = string.Format("{0} {1}", user.FirstName, user.LastName);
                string role = RoleManagementRisk.GetByID(user.RoleID).Code;
                int checkInternalapplicable = 0;
                int checkTaskapplicable = 0;
                int checkVerticalapplicable = 0;
                int checkLabelApplicable = 0;
                bool IsPaymentCustomer = false;
                int complianceProdType = 0;
                if (user.CustomerID == null)
                {
                    checkInternalapplicable = 2;
                    checkTaskapplicable = 2;
                    checkVerticalapplicable = 2;
                    checkLabelApplicable = 2;
                }
                else
                {
                    var IsInternalComplianceApplicable = Common.AuthenticationHelper.IComplilanceApplicable;
                    var IsTaskApplicable = Common.AuthenticationHelper.TaskApplicable;
                    var Isverticlapplicable = Common.AuthenticationHelper.IsVerticalApplicable;
                    var IsLabelApplicable = Common.AuthenticationHelper.IsLabelApplicable;
                    var valComProdType = Common.AuthenticationHelper.ComplianceProductType;

                    checkTaskapplicable = Convert.ToInt32(IsTaskApplicable);
                    checkInternalapplicable = Convert.ToInt32(IsInternalComplianceApplicable);
                    checkVerticalapplicable = Convert.ToInt32(Isverticlapplicable);
                    checkLabelApplicable = Convert.ToInt32(IsLabelApplicable);
                    complianceProdType = Convert.ToInt32(valComProdType);
                }
                FormsAuthenticationRedirect_IFC(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                if (user.ChangPasswordDate != null)
                {
                    DateTime LastPasswordChangedDate = Convert.ToDateTime(user.ChangPasswordDate);
                    DateTime currentDate = DateTime.Now;
                    LastPasswordChangedDate = LastPasswordChangedDate != null ? LastPasswordChangedDate : DateTime.Now;
                    int noDays = Convert.ToInt32(ConfigurationManager.AppSettings["ChangedPasswordDays"]);
                    int dateDifference = Convert.ToInt32((currentDate - LastPasswordChangedDate).TotalDays);
                    if (dateDifference == noDays || dateDifference > noDays)
                    {
                        Session["ChangePassword"] = true;
                        Response.Redirect("~/Account/ChangePassword.aspx", false);
                    }
                    else
                    {
                        Session["ChangePassword"] = false;
                    }
                }
                else
                {
                    Session["ChangePassword"] = true;
                    Response.Redirect("~/Account/ChangePassword.aspx", false);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void ProcessAuthenticationInformation_InternalControl(mst_User user)
        {
            try
            {
                string name = string.Format("{0} {1}", user.FirstName, user.LastName);
                string role = RoleManagementRisk.GetByID(user.RoleID).Code;
                int checkInternalapplicable = 0;
                int checkTaskapplicable = 0;
                int checkVerticalapplicable = 0;
                int checkLabelApplicable = 0;
                bool IsPaymentCustomer = false;
                int complianceProdType = 0;
                if (user.CustomerID == null)
                {
                    checkInternalapplicable = 2;
                    checkTaskapplicable = 2;
                    checkVerticalapplicable = 2;
                    checkLabelApplicable = 2;
                    complianceProdType = 0;
                }
                else
                {
                    var IsInternalComplianceApplicable = Common.AuthenticationHelper.IComplilanceApplicable;
                    var IsTaskApplicable = Common.AuthenticationHelper.TaskApplicable;
                    var Isverticlapplicable = Common.AuthenticationHelper.IsVerticalApplicable;
                    var IsLabelApplicable = Common.AuthenticationHelper.IsLabelApplicable;
                    var valComProdType = Common.AuthenticationHelper.ComplianceProductType;

                    checkTaskapplicable = Convert.ToInt32(IsTaskApplicable);
                    checkInternalapplicable = Convert.ToInt32(IsInternalComplianceApplicable);
                    checkVerticalapplicable = Convert.ToInt32(Isverticlapplicable);
                    checkLabelApplicable = Convert.ToInt32(IsLabelApplicable);                    
                    complianceProdType = Convert.ToInt32(valComProdType);
                }
                FormsAuthenticationRedirect_ARS(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                if (user.ChangPasswordDate != null)
                {
                    DateTime LastPasswordChangedDate = Convert.ToDateTime(user.ChangPasswordDate);
                    DateTime currentDate = DateTime.Now;
                    LastPasswordChangedDate = LastPasswordChangedDate != null ? LastPasswordChangedDate : DateTime.Now;
                    int noDays = Convert.ToInt32(ConfigurationManager.AppSettings["ChangedPasswordDays"]);
                    int dateDifference = Convert.ToInt32((currentDate - LastPasswordChangedDate).TotalDays);
                    if (dateDifference == noDays || dateDifference > noDays)
                    {
                        Session["ChangePassword"] = true;
                        Response.Redirect("~/Account/ChangePassword.aspx", false);
                    }
                    else
                    {
                        Session["ChangePassword"] = false;
                    }
                }
                else
                {
                    Session["ChangePassword"] = true;
                    Response.Redirect("~/Account/ChangePassword.aspx", false);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void ProcessAuthenticationInformation_Litigation(User user)
        {
            try
            {
                string name = string.Format("{0} {1}", user.FirstName, user.LastName);
                string role = string.Empty;
                if (user.LitigationRoleID != null)
                    role = RoleManagement.GetByID(Convert.ToInt32(user.LitigationRoleID)).Code;
                else
                    role = RoleManagement.GetByID(user.RoleID).Code;
                int checkInternalapplicable = 0;
                int checkTaskapplicable = 0;
                int checkVerticalapplicable = 0;
                int checkLabelApplicable = 0;
                bool IsPaymentCustomer = false;
                int complianceProdType = 0;
                if (user.CustomerID == null)
                {
                    checkInternalapplicable = 2;
                    checkTaskapplicable = 2;
                    checkVerticalapplicable = 2;
                    checkLabelApplicable = 2;
                    complianceProdType = 0;
                }
                else
                {
                    var IsInternalComplianceApplicable = Common.AuthenticationHelper.IComplilanceApplicable;
                    var IsTaskApplicable = Common.AuthenticationHelper.TaskApplicable;
                    var Isverticlapplicable = Common.AuthenticationHelper.IsVerticalApplicable;
                    var IsLabelApplicable = Common.AuthenticationHelper.IsLabelApplicable;
                    var valComProdType = Common.AuthenticationHelper.ComplianceProductType;

                    checkTaskapplicable = Convert.ToInt32(IsTaskApplicable);
                    checkInternalapplicable = Convert.ToInt32(IsInternalComplianceApplicable);
                    checkVerticalapplicable = Convert.ToInt32(Isverticlapplicable);
                    checkLabelApplicable = Convert.ToInt32(IsLabelApplicable);
                    complianceProdType = Convert.ToInt32(valComProdType);
                }
                FormsAuthenticationRedirect_Litigation(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                if (user.ChangPasswordDate != null)
                {
                    DateTime LastPasswordChangedDate = Convert.ToDateTime(user.ChangPasswordDate);
                    DateTime currentDate = DateTime.Now;
                    LastPasswordChangedDate = LastPasswordChangedDate != null ? LastPasswordChangedDate : DateTime.Now;
                    int noDays = Convert.ToInt32(ConfigurationManager.AppSettings["ChangedPasswordDays"]);
                    int dateDifference = Convert.ToInt32((currentDate - LastPasswordChangedDate).TotalDays);
                    if (dateDifference == noDays || dateDifference > noDays)
                    {
                        Session["ChangePassword"] = true;
                        Response.Redirect("~/Account/ChangePassword.aspx", false);
                    }
                    else
                    {
                        Session["ChangePassword"] = false;
                    }
                }
                else
                {
                    Session["ChangePassword"] = true;
                    Response.Redirect("~/Account/ChangePassword.aspx", false);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void ProcessAuthenticationInformation_Contract(User user)
        {
            try
            {
                string name = string.Format("{0} {1}", user.FirstName, user.LastName);
                string role = string.Empty;
                if (user.ContractRoleID != null)
                    role = RoleManagement.GetByID(Convert.ToInt32(user.ContractRoleID)).Code;
                else
                    role = RoleManagement.GetByID(user.RoleID).Code;
                int checkInternalapplicable = 0;
                int checkTaskapplicable = 0;
                int checkVerticalapplicable = 0;
                int checkLabelApplicable = 0;
                bool IsPaymentCustomer = false;
                int complianceProdType = 0;
                if (user.CustomerID == null)
                {
                    checkInternalapplicable = 2;
                    checkTaskapplicable = 2;
                    checkVerticalapplicable = 2;
                    checkLabelApplicable = 2;
                    complianceProdType = 0;
                }
                else
                {
                    var IsInternalComplianceApplicable = Common.AuthenticationHelper.IComplilanceApplicable;
                    var IsTaskApplicable = Common.AuthenticationHelper.TaskApplicable;
                    var Isverticlapplicable = Common.AuthenticationHelper.IsVerticalApplicable;
                    var IsLabelApplicable = Common.AuthenticationHelper.IsLabelApplicable;
                    var valComProdType = Common.AuthenticationHelper.ComplianceProductType;

                    checkTaskapplicable = Convert.ToInt32(IsTaskApplicable);
                    checkInternalapplicable = Convert.ToInt32(IsInternalComplianceApplicable);
                    checkVerticalapplicable = Convert.ToInt32(Isverticlapplicable);
                    checkLabelApplicable = Convert.ToInt32(IsLabelApplicable);
                    complianceProdType = Convert.ToInt32(valComProdType);
                }
                FormsAuthenticationRedirect_Contract(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                if (user.ChangPasswordDate != null)
                {
                    DateTime LastPasswordChangedDate = Convert.ToDateTime(user.ChangPasswordDate);
                    DateTime currentDate = DateTime.Now;
                    LastPasswordChangedDate = LastPasswordChangedDate != null ? LastPasswordChangedDate : DateTime.Now;
                    int noDays = Convert.ToInt32(ConfigurationManager.AppSettings["ChangedPasswordDays"]);
                    int dateDifference = Convert.ToInt32((currentDate - LastPasswordChangedDate).TotalDays);
                    if (dateDifference == noDays || dateDifference > noDays)
                    {
                        Session["ChangePassword"] = true;
                        Response.Redirect("~/Account/ChangePassword.aspx", false);
                    }
                    else
                    {
                        Session["ChangePassword"] = false;
                    }
                }
                else
                {
                    Session["ChangePassword"] = true;
                    Response.Redirect("~/Account/ChangePassword.aspx", false);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void ProcessAuthenticationInformation_License(User user)
        {
            try
            {
                string name = string.Format("{0} {1}", user.FirstName, user.LastName);
                string role = string.Empty;
                if (user.LicenseRoleID != null)
                    role = RoleManagement.GetByID(Convert.ToInt32(user.LicenseRoleID)).Code;
                else
                    role = RoleManagement.GetByID(user.RoleID).Code;

                int checkInternalapplicable = 0;
                int checkTaskapplicable = 0;
                int checkVerticalapplicable = 0;
                int checkLabelApplicable = 0;
                bool IsPaymentCustomer = false;
                int complianceProdType = 0;
                if (user.CustomerID == null)
                {
                    checkInternalapplicable = 2;
                    checkTaskapplicable = 2;
                    checkVerticalapplicable = 2;
                    checkLabelApplicable = 2;
                    complianceProdType = 0;
                }
                else
                {
                    var IsInternalComplianceApplicable = Common.AuthenticationHelper.IComplilanceApplicable;
                    var IsTaskApplicable = Common.AuthenticationHelper.TaskApplicable;
                    var Isverticlapplicable = Common.AuthenticationHelper.IsVerticalApplicable;
                    var IsLabelApplicable = Common.AuthenticationHelper.IsLabelApplicable;
                    var valComProdType = Common.AuthenticationHelper.ComplianceProductType;

                    checkTaskapplicable = Convert.ToInt32(IsTaskApplicable);
                    checkInternalapplicable = Convert.ToInt32(IsInternalComplianceApplicable);
                    checkVerticalapplicable = Convert.ToInt32(Isverticlapplicable);
                    checkLabelApplicable = Convert.ToInt32(IsLabelApplicable);
                    complianceProdType = Convert.ToInt32(valComProdType);
                }
                FormsAuthenticationRedirect_License(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                if (user.ChangPasswordDate != null)
                {
                    DateTime LastPasswordChangedDate = Convert.ToDateTime(user.ChangPasswordDate);
                    DateTime currentDate = DateTime.Now;
                    LastPasswordChangedDate = LastPasswordChangedDate != null ? LastPasswordChangedDate : DateTime.Now;
                    int noDays = Convert.ToInt32(ConfigurationManager.AppSettings["ChangedPasswordDays"]);
                    int dateDifference = Convert.ToInt32((currentDate - LastPasswordChangedDate).TotalDays);
                    if (dateDifference == noDays || dateDifference > noDays)
                    {
                        Session["ChangePassword"] = true;
                        Response.Redirect("~/Account/ChangePassword.aspx", false);
                    }
                    else
                    {
                        Session["ChangePassword"] = false;
                    }
                }
                else
                {
                    Session["ChangePassword"] = true;
                    Response.Redirect("~/Account/ChangePassword.aspx", false);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void ProcessAuthenticationInformation_VendorAuditNew(User user)
        {
            try
            {
                string name = string.Format("{0} {1}", user.FirstName, user.LastName);
                string role = string.Empty;
                if (user.VendorAuditRoleID != null)
                    role = RoleManagement.GetByID(Convert.ToInt32(user.VendorAuditRoleID)).Code;
                else
                    role = RoleManagement.GetByID(user.RoleID).Code;

                int checkInternalapplicable = 0;
                int checkTaskapplicable = 0;
                int checkVerticalapplicable = 0;
                int checkLabelApplicable = 0;
                bool IsPaymentCustomer = false;
                int complianceProdType = 0;
                if (user.CustomerID == null)
                {
                    checkInternalapplicable = 2;
                    checkTaskapplicable = 2;
                    checkVerticalapplicable = 2;
                    checkLabelApplicable = 2;
                    complianceProdType = 0;
                }
                else
                {
                    var IsInternalComplianceApplicable = Common.AuthenticationHelper.IComplilanceApplicable;
                    var IsTaskApplicable = Common.AuthenticationHelper.TaskApplicable;
                    var Isverticlapplicable = Common.AuthenticationHelper.IsVerticalApplicable;
                    var IsLabelApplicable = Common.AuthenticationHelper.IsLabelApplicable;
                    var valComProdType = Common.AuthenticationHelper.ComplianceProductType;

                    checkTaskapplicable = Convert.ToInt32(IsTaskApplicable);
                    checkInternalapplicable = Convert.ToInt32(IsInternalComplianceApplicable);
                    checkVerticalapplicable = Convert.ToInt32(Isverticlapplicable);
                    checkLabelApplicable = Convert.ToInt32(IsLabelApplicable);
                    complianceProdType = Convert.ToInt32(valComProdType);
                }
                FormsAuthenticationRedirect_RLCSVendorAuditNew(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                if (user.ChangPasswordDate != null)
                {
                    DateTime LastPasswordChangedDate = Convert.ToDateTime(user.ChangPasswordDate);
                    DateTime currentDate = DateTime.Now;
                    LastPasswordChangedDate = LastPasswordChangedDate != null ? LastPasswordChangedDate : DateTime.Now;
                    int noDays = Convert.ToInt32(ConfigurationManager.AppSettings["ChangedPasswordDays"]);
                    int dateDifference = Convert.ToInt32((currentDate - LastPasswordChangedDate).TotalDays);
                    if (dateDifference == noDays || dateDifference > noDays)
                    {
                        Session["ChangePassword"] = true;
                        Response.Redirect("~/Account/ChangePassword.aspx", false);
                    }
                    else
                    {
                        Session["ChangePassword"] = false;
                    }
                }
                else
                {
                    Session["ChangePassword"] = true;
                    Response.Redirect("~/Account/ChangePassword.aspx", false);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void ProcessAuthenticationInformation_Vendor(User user)
        {
            try
            {
                string name = string.Format("{0} {1}", user.FirstName, user.LastName);
                string role = string.Empty;
                if (user.VendorRoleID != null)
                    role = RoleManagement.GetByID(Convert.ToInt32(user.VendorRoleID)).Code;
                else
                    role = RoleManagement.GetByID(user.RoleID).Code;

                int checkInternalapplicable = 0;
                int checkTaskapplicable = 0;
                int checkVerticalapplicable = 0;
                int checkLabelApplicable = 0;
                bool IsPaymentCustomer = false;
                int complianceProdType = 0;
                if (user.CustomerID == null)
                {
                    checkInternalapplicable = 2;
                    checkTaskapplicable = 2;
                    checkVerticalapplicable = 2;
                    checkLabelApplicable = 2;
                    complianceProdType = 0;
                }
                else
                {
                    var IsInternalComplianceApplicable = Common.AuthenticationHelper.IComplilanceApplicable;
                    var IsTaskApplicable = Common.AuthenticationHelper.TaskApplicable;
                    var Isverticlapplicable = Common.AuthenticationHelper.IsVerticalApplicable;
                    var IsLabelApplicable = Common.AuthenticationHelper.IsLabelApplicable;
                    var valComProdType = Common.AuthenticationHelper.ComplianceProductType;

                    checkTaskapplicable = Convert.ToInt32(IsTaskApplicable);
                    checkInternalapplicable = Convert.ToInt32(IsInternalComplianceApplicable);
                    checkVerticalapplicable = Convert.ToInt32(Isverticlapplicable);
                    checkLabelApplicable = Convert.ToInt32(IsLabelApplicable);
                    complianceProdType = Convert.ToInt32(valComProdType);
                }
                FormsAuthenticationRedirect_RLCSVendor(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                if (user.ChangPasswordDate != null)
                {
                    DateTime LastPasswordChangedDate = Convert.ToDateTime(user.ChangPasswordDate);
                    DateTime currentDate = DateTime.Now;
                    LastPasswordChangedDate = LastPasswordChangedDate != null ? LastPasswordChangedDate : DateTime.Now;
                    int noDays = Convert.ToInt32(ConfigurationManager.AppSettings["ChangedPasswordDays"]);
                    int dateDifference = Convert.ToInt32((currentDate - LastPasswordChangedDate).TotalDays);
                    if (dateDifference == noDays || dateDifference > noDays)
                    {
                        Session["ChangePassword"] = true;
                        Response.Redirect("~/Account/ChangePassword.aspx", false);
                    }
                    else
                    {
                        Session["ChangePassword"] = false;
                    }
                }
                else
                {
                    Session["ChangePassword"] = true;
                    Response.Redirect("~/Account/ChangePassword.aspx", false);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void ProcessAuthenticationInformation_HRProduct(User user)
        {
            try
            {
                string name = string.Format("{0} {1}", user.FirstName, user.LastName);
                string role = RoleManagement.GetByID((int)user.HRRoleID).Code; 
                int checkInternalapplicable = 0;
                int checkTaskapplicable = 0;
                int checkVerticalapplicable = 0;
                int checkLabelApplicable = 0;
                bool IsPaymentCustomer = false;

                int complianceProdType = 0;

                if (user.CustomerID == null)
                {
                    checkInternalapplicable = 2;
                    checkTaskapplicable = 2;
                    checkVerticalapplicable = 2;
                    checkLabelApplicable = 2;
                    complianceProdType = 0;
                }
                else
                {
                    var IsInternalComplianceApplicable = Common.AuthenticationHelper.IComplilanceApplicable;
                    var IsTaskApplicable = Common.AuthenticationHelper.TaskApplicable;
                    var Isverticlapplicable = Common.AuthenticationHelper.IsVerticalApplicable;
                    var IsLabelApplicable = Common.AuthenticationHelper.IsLabelApplicable;
                    var valCompProdType = Common.AuthenticationHelper.ComplianceProductType;

                    checkTaskapplicable = Convert.ToInt32(IsTaskApplicable);
                    checkInternalapplicable = Convert.ToInt32(IsInternalComplianceApplicable);
                    checkVerticalapplicable = Convert.ToInt32(Isverticlapplicable);
                    checkLabelApplicable = Convert.ToInt32(IsLabelApplicable);
                    complianceProdType = Convert.ToInt32(valCompProdType);
                }
                FormsAuthenticationRedirect_HRCompliance(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                if (user.ChangPasswordDate != null)
                {
                    DateTime LastPasswordChangedDate = Convert.ToDateTime(user.ChangPasswordDate);
                    DateTime currentDate = DateTime.Now;
                    LastPasswordChangedDate = LastPasswordChangedDate != null ? LastPasswordChangedDate : DateTime.Now;
                    int noDays = Convert.ToInt32(ConfigurationManager.AppSettings["ChangedPasswordDays"]);
                    int dateDifference = Convert.ToInt32((currentDate - LastPasswordChangedDate).TotalDays);
                    if (dateDifference == noDays || dateDifference > noDays)
                    {
                        Session["ChangePassword"] = true;
                        Response.Redirect("~/Account/ChangePassword.aspx", false);
                    }
                    else
                    {
                        Session["ChangePassword"] = false;
                    }
                }
                else
                {
                    Session["ChangePassword"] = true;
                    Response.Redirect("~/Account/ChangePassword.aspx", false);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void ProcessAuthenticationInformation_Secretarial(User user)
        {
            try
            {
                string name = string.Format("{0} {1}", user.FirstName, user.LastName);
                string role = string.Empty;

                if (user.SecretarialRoleID != null)
                    role = RoleManagement.GetByID(Convert.ToInt32(user.SecretarialRoleID)).Code;
                else
                    role = RoleManagement.GetByID(user.RoleID).Code;

                int checkInternalapplicable = 0;
                int checkTaskapplicable = 0;
                int checkVerticalapplicable = 0;
                int checkLabelApplicable = 0;
                bool IsPaymentCustomer = false;
                int complianceProdType = 0;
                if (user.CustomerID == null)
                {
                    checkInternalapplicable = 2;
                    checkTaskapplicable = 2;
                    checkVerticalapplicable = 2;
                    checkLabelApplicable = 2;
                    complianceProdType = 0;
                }
                else
                {
                    var IsInternalComplianceApplicable = Common.AuthenticationHelper.IComplilanceApplicable;
                    var IsTaskApplicable = Common.AuthenticationHelper.TaskApplicable;
                    var Isverticlapplicable = Common.AuthenticationHelper.IsVerticalApplicable;
                    var IsLabelApplicable = Common.AuthenticationHelper.IsLabelApplicable;
                    var valComProdType = Common.AuthenticationHelper.ComplianceProductType;

                    checkTaskapplicable = Convert.ToInt32(IsTaskApplicable);
                    checkInternalapplicable = Convert.ToInt32(IsInternalComplianceApplicable);
                    checkVerticalapplicable = Convert.ToInt32(Isverticlapplicable);
                    checkLabelApplicable = Convert.ToInt32(IsLabelApplicable);
                    complianceProdType = Convert.ToInt32(valComProdType);
                }

                FormsAuthenticationRedirect_Secretarial(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                
                if (user.ChangPasswordDate != null)
                {
                    DateTime LastPasswordChangedDate = Convert.ToDateTime(user.ChangPasswordDate);
                    DateTime currentDate = DateTime.Now;
                    LastPasswordChangedDate = LastPasswordChangedDate != null ? LastPasswordChangedDate : DateTime.Now;
                    int noDays = Convert.ToInt32(ConfigurationManager.AppSettings["ChangedPasswordDays"]);
                    int dateDifference = Convert.ToInt32((currentDate - LastPasswordChangedDate).TotalDays);
                    if (dateDifference == noDays || dateDifference > noDays)
                    {
                        Session["ChangePassword"] = true;
                        Response.Redirect("~/Account/ChangePassword.aspx", false);
                    }
                    else
                    {
                        Session["ChangePassword"] = false;
                    }
                }
                else
                {
                    Session["ChangePassword"] = true;
                    Response.Redirect("~/Account/ChangePassword.aspx", false);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnCompliance_Click(object sender, EventArgs e)
        {
            long customerID = -1;
            customerID = Common.AuthenticationHelper.CustomerID;
            var ProductMappingDetails = UserManagement.GetByProductIDList(Convert.ToInt32(customerID));
            if (ProductMappingDetails.Contains(1))
            {
                int userID = Common.AuthenticationHelper.UserID;
                ProcessAuthenticationInformation_Compliance(UserManagement.GetByID(userID));
            }
            else
            {
                lblErrMsg.Text = "You don't have permission to Access. ";
            }
        }
        protected void btnLitigation_Click(object sender, EventArgs e)
        {
            long customerID = -1;
            customerID = Common.AuthenticationHelper.CustomerID;
            var ProductMappingDetails = UserManagement.GetByProductIDList(Convert.ToInt32(customerID));
            if (ProductMappingDetails.Contains(2))
            {
                int userID = Common.AuthenticationHelper.UserID;
                ProcessAuthenticationInformation_Litigation(UserManagement.GetByID(userID));
            }
            else
            {
                lblErrMsg.Text = "You don't have permission to Access. ";
            }
        }
        protected void btnContract_Click(object sender, EventArgs e)
        {
            long customerID = -1;
            customerID = Common.AuthenticationHelper.CustomerID;
            var ProductMappingDetails = UserManagement.GetByProductIDList(Convert.ToInt32(customerID));
            if (ProductMappingDetails.Contains(5))
            {
                int userID = Common.AuthenticationHelper.UserID;
                ProcessAuthenticationInformation_Contract(UserManagement.GetByID(userID));
            }
            else
            {
                lblErrMsg.Text = "You don't have permission to Access. ";
            }
        }
        protected void btnLicense_Click(object sender, EventArgs e)
        {
            long customerID = -1;
            customerID = Common.AuthenticationHelper.CustomerID;
            var ProductMappingDetails = UserManagement.GetByProductIDList(Convert.ToInt32(customerID));
            if (ProductMappingDetails.Contains(6))
            {
                int userID = Common.AuthenticationHelper.UserID;
                ProcessAuthenticationInformation_License(UserManagement.GetByID(userID));
            }
            else
            {
                lblErrMsg.Text = "You don't have permission to Access. ";
            }
        }
        protected void btnVendor_Click(object sender, EventArgs e)
        {
            long customerID = -1;
            customerID = Common.AuthenticationHelper.CustomerID;
            var ProductMappingDetails = UserManagement.GetByProductIDList(Convert.ToInt32(customerID));
            if (ProductMappingDetails.Contains(7))
            {
                int userID = Common.AuthenticationHelper.UserID;
                ProcessAuthenticationInformation_Vendor(UserManagement.GetByID(userID));
            }
            else
            {
                lblErrMsg.Text = "You don't have permission to Access. ";
            }
        }
        protected void btnInternalControl_Click(object sender, EventArgs e)
        {
            long customerID = -1;
            customerID = Common.AuthenticationHelper.CustomerID;
            var ProductMappingDetails = UserManagement.GetByProductIDList(Convert.ToInt32(customerID));
            if (ProductMappingDetails.Contains(3))
            {
                int userID = Common.AuthenticationHelper.UserID;
                ProcessAuthenticationInformation_Audit(UserManagementRisk.GetByID(Convert.ToInt32(userID)));
            }
            else
            {
                lblErrMsg.Text = "You don't have permission to Access. ";
            }
        }
        protected void btnAudit_Click(object sender, EventArgs e)
        {
            long customerID = -1;
            customerID = Common.AuthenticationHelper.CustomerID;
            var ProductMappingDetails = UserManagement.GetByProductIDList(Convert.ToInt32(customerID));
            if (ProductMappingDetails.Contains(4))
            {
                int userID = Common.AuthenticationHelper.UserID;
                ProcessAuthenticationInformation_InternalControl(UserManagementRisk.GetByID(Convert.ToInt32(userID)));
            }
            else
            {
                lblErrMsg.Text = "You don't have permission to Access. ";
            }
        }
        protected void btnHRProduct_Click(object sender, EventArgs e)
        {
            long customerID = -1;
            customerID = Common.AuthenticationHelper.CustomerID;
            var ProductMappingDetails = UserManagement.GetByProductIDList(Convert.ToInt32(customerID));
            if (ProductMappingDetails.Contains(9))
            {
                int userID = Common.AuthenticationHelper.UserID;
                ProcessAuthenticationInformation_HRProduct(UserManagement.GetByID(userID));
            }
            else
            {
                lblErrMsg.Text = "You don't have permission to Access. ";
            }
        }

        protected void btnSecretarial_Click(object sender, EventArgs e)
        {
            long customerID = -1;
            customerID = Common.AuthenticationHelper.CustomerID;
            var ProductMappingDetails = UserManagement.GetByProductIDList(Convert.ToInt32(customerID));
            if (ProductMappingDetails.Contains(8))
            {
                int userID = Common.AuthenticationHelper.UserID;
                ProcessAuthenticationInformation_Secretarial(UserManagement.GetByID(userID));
            }
            else
            {
                lblErrMsg.Text = "You don't have permission to Access. ";
            }
        }

        public bool ReAuthenticate_UserNew()
        {
            bool authSuccess = false;
            try
            {

                string[] userDetails = HttpContext.Current.User.Identity.Name.Split(';');
                if (userDetails.Length >= 11)
                {
                    string UserID = userDetails[0];
                    string Role = userDetails[1];
                    string User = userDetails[2];
                    string IComplilanceApplicable = userDetails[3];
                    string ProductApplicableLogin = userDetails[4];
                    string CustomerID = userDetails[5]; ;
                    string TaskApplicable = userDetails[6];
                    string ProfileID = userDetails[7];
                    string AuthKey = userDetails[8];
                    string IsVerticalApplicable = userDetails[9];
                    string IsPaymentCustomer = userDetails[10];
                    string IsLabelApplicable = userDetails[11];
                    string ComplianceProductType = userDetails[12];

                    FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12}",
                      UserID, Role, User, IComplilanceApplicable, ProductApplicableLogin, CustomerID, TaskApplicable, ProfileID, AuthKey, IsVerticalApplicable,
                      IsPaymentCustomer, IsLabelApplicable, ComplianceProductType), false);                    
                    if (Role.Equals("MGMT"))
                    {
                        HttpContext.Current.Response.Redirect("~/Management/MangementDashboard", false);
                    }
                    else if (Role.Equals("CADMN"))
                    {
                        HttpContext.Current.Response.Redirect("~/Common/Dashboard.aspx", false);
                    }
                    else if (Role.Equals("EXCT"))
                    {
                        HttpContext.Current.Response.Redirect("~/Common/Dashboard.aspx", false);
                    }
                    else
                    {
                        HttpContext.Current.Response.Redirect("~/Common/Dashboard.aspx", false);
                    }
                    authSuccess = true;
                }
                return authSuccess;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return authSuccess;
            }
        }
        public void FormsAuthenticationRedirect_Compliance(User user, string role, string name, int checkInternalapplicable, int checkTaskapplicable ,int IsVerticlApplicable, bool IsPaymentCustomer, int checkLabelApplicable, int complianceProdType)
        {
            roles = CustomerBranchManagement.GetAssignedroleid(Convert.ToInt32(user.ID));
            Session["User_comp_Roles"] = roles;
            if (roles.Contains(6))
            {
                Approveruser_Roles = "APPR";
            }
            else
            {
                Approveruser_Roles = "";
            }

            string userProfileID = string.Empty;
            string userProfileID_Encrypted = string.Empty;
            string authkey = string.Empty;
            
            //if (user.RoleID >= 14 && user.RoleID <= 18 && complianceProdType < 2)
            if (complianceProdType == 1 || complianceProdType == 3)
            {
                userProfileID = RLCSManagement.GetProfileIDByUserID(user.ID);

                string TLConnectKey = ConfigurationManager.AppSettings["TLConnect_Encrypt_Decrypt_Key"];
                string TLConnectAPIUrl = ConfigurationManager.AppSettings["TLConnect_API_URL"];

                if (!string.IsNullOrEmpty(TLConnectAPIUrl) && !string.IsNullOrEmpty(userProfileID))
                {
                    try
                    {
                        authkey = RLCSManagement.GetAuthKeyByProfileID(TLConnectAPIUrl, userProfileID, TLConnectKey);
                    }
                    catch (Exception ex)
                    {
                        userProfileID = string.Empty;
                        authkey = string.Empty;
                    }
                }
            }
            else
                userProfileID = user.ID.ToString();

            if (complianceProdType == 3 && role.StartsWith("H"))
            {
                var lstMgrAssignedBranch = CustomerBranchManagement.GetMGMTAssignedBranch(Convert.ToInt32(user.ID));
                if (lstMgrAssignedBranch.Count > 0)
                    role = "MGMT";
                else if (roles.Count > 0)
                    role = "EXCT";
            }

            FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), Convert.ToBoolean(Session["RM"]));
            FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12}", user.ID, role, name, checkInternalapplicable, "C", user.CustomerID, checkTaskapplicable, userProfileID, authkey, IsVerticlApplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType), false);

            TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
            Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);

            if (complianceProdType == 1)// TLConnect Customer
            {
                if (role.Equals("HMGMT") || role.Equals("LSPOC") || role.Equals("HMGR") || role.Equals("HAPPR") || role.Equals("HEXCT"))
                {
                    HttpContext.Current.Response.Redirect("~/RLCS/RLCS_HRMDashboardNew.aspx", false);
                }
                else if (role.Equals("HVADM") || role.Equals("HVAUD"))
                {
                    HttpContext.Current.Response.Redirect("~/RLCSVendorAudit/RLCSUploadChecklist.aspx", false);
                }
                else if (role.Equals("VAUDT"))
                {
                    HttpContext.Current.Response.Redirect("~/VenderAudit/aspxpages/VendorAuditDashboard.aspx", false);
                }
            }
            else if (complianceProdType == 2) //HRPlus Compliance Product User
            {
                if (role.Equals("HMGMT") || role.Equals("HMGR") || role.Equals("HEXCT"))
                {
                    HttpContext.Current.Response.Redirect("~/RLCS/HRPlus_DFMSPOCDashboard.aspx", false);
                }
                else if (role.Equals("SPADM") || role.Equals("DADMN"))
                {
                    HttpContext.Current.Response.Redirect("~/RLCS/HRPlus_DFMSPOCDashboard.aspx", false);
                }
                else if (role.Equals("CADMN"))
                {
                    HttpContext.Current.Response.Redirect("~/RLCS/HRPlus_DFMDashboard.aspx", false);
                }
            }
            else
            {
                if (role.Equals("MGMT") || role.Equals("AUDT"))
                {
                    HttpContext.Current.Response.Redirect("~/Management/MangementDashboard", false);
                }
                else if (role.Equals("IMPT"))
                {
                    HttpContext.Current.Response.Redirect("~/Common/CompanyStructure.aspx", false);
                }
                else if (role.Equals("CADMN"))
                {
                    if (roles.Count == 0)
                        HttpContext.Current.Response.Redirect("~/Common/ComplianceDashboard.aspx", false);
                    else
                        HttpContext.Current.Response.Redirect("~/Common/Dashboard.aspx", false);
                }
                else if (role.Equals("HVADM") || role.Equals("HVAUD"))
                {
                    HttpContext.Current.Response.Redirect("~/RLCSVendorAudit/RLCSUploadChecklist.aspx", false);
                }
                else if (role.Equals("VAUDT"))
                {
                    HttpContext.Current.Response.Redirect("~/VenderAudit/aspxpages/VendorAuditDashboard.aspx", false);
                }
                else if (role.Equals("EXCT"))
                {
                    if (user.IsHead == true)
                    {
                        HttpContext.Current.Response.Redirect("~/Management/MgmtDashboardDeptHead.aspx", false);
                    }
                    else if (Approveruser_Roles.Contains("APPR") && (roles.Contains(3) || roles.Contains(4)))
                    {
                        if (user.IsHead == true)
                        {
                            HttpContext.Current.Response.Redirect("~/Management/MgmtDashboardDeptHead.aspx", false);
                        }
                        else
                        {
                            HttpContext.Current.Response.Redirect("~/Common/Dashboard.aspx", false);
                        }
                    }
                    else if (Approveruser_Roles.Contains("APPR"))
                    {
                        HttpContext.Current.Response.Redirect("~/Management/ManagementDashboardNew.aspx", false);
                    }
                    else
                    {
                        HttpContext.Current.Response.Redirect("~/Common/Dashboard.aspx", false);
                    }
                }                                
                else if (role.Equals("HMGMT") || role.Equals("LSPOC") || role.Equals("HMGR") || role.Equals("HAPPR") || role.Equals("HEXCT")) //AVACOM+TL User
                {
                    //HMGMT
                    if (role.Equals("HMGMT"))
                    {
                        var lstMgrAssignedBranch = CustomerBranchManagement.GetMGMTAssignedBranch(Convert.ToInt32(user.ID));

                        if (lstMgrAssignedBranch.Count > 0)
                            HttpContext.Current.Response.Redirect("~/Management/MangementDashboard", false);
                        else if (roles.Count > 0)
                            HttpContext.Current.Response.Redirect("~/Common/Dashboard.aspx", false);
                        else
                            HttpContext.Current.Response.Redirect("~/RLCS/RLCS_HRMDashboardNew.aspx", false);
                    }
                    else if (role.Equals("LSPOC") || role.Equals("HMGR") || role.Equals("HAPPR") || role.Equals("HEXCT"))
                    {
                        if (roles.Count > 0)
                            HttpContext.Current.Response.Redirect("~/Common/Dashboard.aspx", false);
                        else
                            HttpContext.Current.Response.Redirect("~/RLCS/RLCS_HRMDashboardNew.aspx", false);
                    }
                }
                else
                {
                    if (user.IsHead == true)
                    {
                        HttpContext.Current.Response.Redirect("~/Management/MgmtDashboardDeptHead.aspx", false);
                    }
                    else
                    {
                        HttpContext.Current.Response.Redirect("~/Common/Dashboard.aspx", false);
                    }
                }
            }
        }
        public void FormsAuthenticationRedirect_Litigation(User user, string role, string name, int checkInternalapplicable, int checkTaskapplicable, int IsVerticlApplicable, bool IsPaymentCustomer, int checkLabelApplicable, int complianceProdType)
        {
            FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), Convert.ToBoolean(Session["RM"]));
            FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12}", user.ID, role, name, checkInternalapplicable, "L", user.CustomerID, checkTaskapplicable, string.Empty, string.Empty, IsVerticlApplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType), false);
            TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
            Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);
            if (role.Equals("MGMT"))
            {
                HttpContext.Current.Response.Redirect("~/Litigation/Dashboard/LitigationManagementDashboard.aspx", false);
            }
            else if (role.Equals("CADMN"))
            {
                HttpContext.Current.Response.Redirect("~/Litigation/Dashboard/LitigationManagementDashboard.aspx", false);
            }
            else
            {
                HttpContext.Current.Response.Redirect("~/Litigation/Dashboard/LitigationDashboard.aspx", false);
            }
        }
        public void FormsAuthenticationRedirect_IFC(mst_User user, string role, string name, int checkInternalapplicable, int checkTaskapplicable, int IsVerticlApplicable, bool IsPaymentCustomer, int checkLabelApplicable, int complianceProductType)
        {
            FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), Convert.ToBoolean(Session["RM"]));
            FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12}", user.ID, role, name, checkInternalapplicable, "A", user.CustomerID, checkTaskapplicable, string.Empty, string.Empty, IsVerticlApplicable, IsPaymentCustomer, checkLabelApplicable, complianceProductType), false);
            TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
            Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);
            if (role.Equals("MGMT"))
            {
                HttpContext.Current.Response.Redirect("~/Management/ICFRManagementDashboard.aspx", false);
            }
            else if (role.Equals("HVADM") || role.Equals("HVAUD"))
            {
                HttpContext.Current.Response.Redirect("~/RLCSVendorAudit/RLCSUploadChecklist.aspx", false);
            }
            else if (role.Equals("IMPT"))
            {
                HttpContext.Current.Response.Redirect("~/Common/CompanyStructure.aspx", false);
            }
            else
            {
                string AuditHeadOrManager = "";
                AuditHeadOrManager = CustomerManagementRisk.GetAuditHeadOrManagerid(Convert.ToInt32(user.ID));
                if (AuditHeadOrManager == "AM" || AuditHeadOrManager == "AH")
                {
                    HttpContext.Current.Response.Redirect("~/RiskManagement/Common/IFCAuditManagerDashboard.aspx", false);
                }
                else
                {
                    HttpContext.Current.Response.Redirect("~/RiskManagement/Common/AuditDashboard.aspx", false);
                }
            }
        }
        public void FormsAuthenticationRedirect_ARS(mst_User user, string role, string name, int checkInternalapplicable, int checkTaskapplicable, int IsVerticlApplicable, bool IsPaymentCustomer, int checkLabelApplicable, int complianceProductType)
        {
            FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), Convert.ToBoolean(Session["RM"]));
            FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12}", user.ID, role, name, checkInternalapplicable, "I", user.CustomerID, checkTaskapplicable, string.Empty, string.Empty, IsVerticlApplicable, IsPaymentCustomer, checkLabelApplicable, complianceProductType), false);
            TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
            Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);
            bool DepartmentHead = CustomerManagementRisk.GetDepartMentHeadId(Convert.ToInt32(user.ID));
            if (DepartmentHead)
            {
                HttpContext.Current.Response.Redirect("~/RiskManagement/Common/DepartmentHeadDashboard.aspx", false);
            }
            else if (role.Equals("MGMT"))
            {
                HttpContext.Current.Response.Redirect("~/Management/AuditManagementDashboard.aspx", false);
            }
            else if (role.Equals("IMPT"))
            {
                HttpContext.Current.Response.Redirect("~/Common/CompanyStructure.aspx", false);
            }
            else if (role.Equals("HVADM") || role.Equals("HVAUD"))
            {
                HttpContext.Current.Response.Redirect("~/RLCSVendorAudit/RLCSUploadChecklist.aspx", false);
            }
            else if (role.Equals("CADMN"))
            {
                HttpContext.Current.Response.Redirect("~/RiskManagement/Common/admindashboard.aspx", false);
            }
            else
            {
                string AuditHeadOrManager = "";
                var PersonResp = CustomerManagementRisk.GetInternalPersonResponsibleid(Convert.ToInt32(user.ID));
                AuditHeadOrManager = CustomerManagementRisk.GetAuditHeadOrManagerid(Convert.ToInt32(user.ID));
                if (AuditHeadOrManager == "AM" || AuditHeadOrManager == "AH")
                {
                    HttpContext.Current.Response.Redirect("~/RiskManagement/Common/AuditManagerDashboard.aspx", false);
                }
                else if (PersonResp != 0)  // Person Responsible
                {
                    HttpContext.Current.Response.Redirect("~/RiskManagement/Common/PersonResponsibleDashboard.aspx", false);
                }
                else
                {
                    HttpContext.Current.Response.Redirect("~/RiskManagement/Common/InternalControlDashboard.aspx", false);
                }
            }
        }
        public void FormsAuthenticationRedirect_Contract(User user, string role, string name, int checkInternalapplicable, int checkTaskapplicable, int IsVerticlApplicable, bool IsPaymentCustomer, int checkLabelApplicable, int complianceProductType)
        {
            FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), Convert.ToBoolean(Session["RM"]));
            FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12}", user.ID, role, name, checkInternalapplicable, 'T', user.CustomerID, checkTaskapplicable, string.Empty, string.Empty, IsVerticlApplicable, IsPaymentCustomer, checkLabelApplicable, complianceProductType), false);
            TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
            Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);
           Boolean ShowContractInitiator = CaseManagement.CheckForClientNew(Convert.ToInt32(AuthenticationHelper.CustomerID), "ShowContractInitiator");

            if (role.Equals("MGMT"))
            {
                HttpContext.Current.Response.Redirect("~/ContractProduct/Dashboard/ContractMangDashboard.aspx", false);
            }
            else if (role.Equals("CADMN"))
            {
                HttpContext.Current.Response.Redirect("~/ContractProduct/Dashboard/ContractMangDashboard.aspx", false);
            }
            else if(ShowContractInitiator==true && role.Equals("EXCT"))
            {
                HttpContext.Current.Response.Redirect("~/ContractProduct/aspxPages/ContractTermSheetDetails.aspx", false);
            }
            else
            {
                HttpContext.Current.Response.Redirect("~/ContractProduct/Dashboard/ContractDashboard.aspx", false);
            }
        }
        public void FormsAuthenticationRedirect_License(User user, string role, string name, int checkInternalapplicable, int checkTaskapplicable, int IsVerticlApplicable, bool IsPaymentCustomer, int checkLabelApplicable, int complianceProductType)
        {
            FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), Convert.ToBoolean(Session["RM"]));
            FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12}", user.ID, role, name, checkInternalapplicable, 'S', user.CustomerID, checkTaskapplicable, string.Empty, string.Empty, IsVerticlApplicable, IsPaymentCustomer, checkLabelApplicable, complianceProductType), false);
            TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);

            Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);
            if (role.Equals("MGMT"))
            {
                HttpContext.Current.Response.Redirect("~/LicenseManagement/Dashboard/LicenseMangDashboard.aspx", false);
            }
            else if (role.Equals("HVADM") || role.Equals("HVAUD"))
            {
                HttpContext.Current.Response.Redirect("~/RLCSVendorAudit/RLCSUploadChecklist.aspx", false);
            }
            else if (role.Equals("CADMN"))
            {
                HttpContext.Current.Response.Redirect("~/LicenseManagement/Dashboard/LicenseMangDashboard.aspx", false);
            }
            else
            {
                HttpContext.Current.Response.Redirect("~/LicenseManagement/Dashboard/LicenseDashboard.aspx", false);
            }
        }
        public void FormsAuthenticationRedirect_RLCSVendorAuditNew(User user, string role, string name, int checkInternalapplicable, int checkTaskapplicable, int IsVerticlApplicable, bool IsPaymentCustomer, int checkLabelApplicable, int complianceProductType)
        {
            if (user.VendorAuditRoleID != null && user.VendorAuditRoleID != -1)
            {
                var vrole = RoleManagement.GetByID((int)user.VendorAuditRoleID).Code;

                FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), Convert.ToBoolean(Session["RM"]));
                FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12}", user.ID, vrole, name, checkInternalapplicable, 'S', user.CustomerID, checkTaskapplicable, string.Empty, string.Empty, IsVerticlApplicable, IsPaymentCustomer, checkLabelApplicable, complianceProductType), false);
                TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
                Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);
                if (vrole.Equals("VCAUD") || vrole.Equals("VCCON"))
                {
                    HttpContext.Current.Response.Redirect("~/VendorAuditManagement/VendorAudit/ContractorDashboard", false);
                }
                if (vrole.Equals("CADMN") || vrole.Equals("VCCAD"))
                {
                    HttpContext.Current.Response.Redirect("~/VendorAuditManagement/VendorAudit/Entities", false);
                }
                if (vrole.Equals("VCMNG") || vrole.Equals("VCPH") || vrole.Equals("VCPD"))
                {
                    HttpContext.Current.Response.Redirect("~/VendorAuditManagement/VendorAudit/ManagementDashboard", false);
                }
            }
            else
            {
                FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), Convert.ToBoolean(Session["RM"]));
                FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11}", user.ID, role, name, checkInternalapplicable, 'S', user.CustomerID, checkTaskapplicable, string.Empty, string.Empty, IsVerticlApplicable, IsPaymentCustomer, checkLabelApplicable), false);
                TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
                Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);
                if (role.Equals("CADMN"))
                {
                    HttpContext.Current.Response.Redirect("~/VendorAuditManagement/VendorAudit/Entities", false);
                }
            }
        }

        public void FormsAuthenticationRedirect_RLCSVendor(User user, string role, string name, int checkInternalapplicable, int checkTaskapplicable, int IsVerticlApplicable, bool IsPaymentCustomer, int checkLabelApplicable, int complianceProductType)
        {
            if (user.VendorRoleID != null && user.VendorRoleID != -1)
            {
                var vrole = RoleManagement.GetByID((int)user.VendorRoleID).Code;

                FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), Convert.ToBoolean(Session["RM"]));
                FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12}", user.ID, vrole, name, checkInternalapplicable, 'S', user.CustomerID, checkTaskapplicable, string.Empty, string.Empty, IsVerticlApplicable, IsPaymentCustomer, checkLabelApplicable, complianceProductType), false);
                TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
                Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);
                if (vrole.Equals("HVADM"))
                {
                    HttpContext.Current.Response.Redirect("~/RLCSVendorAudit/RLCSUploadChecklist.aspx", false);
                }
                else if (vrole.Equals("HVEND") || vrole.Equals("HVAUD"))
                {
                    HttpContext.Current.Response.Redirect("~/RLCSVendorAudit/RLCSVendorAuditDashboard.aspx", false);
                }
            }
            else
            {
                FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), Convert.ToBoolean(Session["RM"]));
                FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11}", user.ID, role, name, checkInternalapplicable, 'S', user.CustomerID, checkTaskapplicable, string.Empty, string.Empty, IsVerticlApplicable, IsPaymentCustomer, checkLabelApplicable), false);
                TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
                Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);
                if (role.Equals("HVADM"))
                {
                    HttpContext.Current.Response.Redirect("~/RLCSVendorAudit/RLCSUploadChecklist.aspx", false);
                }
                else if (role.Equals("HVEND") || role.Equals("HVAUD"))
                {
                    HttpContext.Current.Response.Redirect("~/RLCSVendorAudit/RLCSVendorAuditDashboard.aspx", false);
                }
            }
        }
        public void FormsAuthenticationRedirect_RLCSCompliance(int customerID, RLCS_User_Mapping user, string role, string name, int checkInternalapplicable, int checkTaskapplicable, int IsVerticlApplicable, bool IsPaymentCustomer, int checkLabelApplicable, int complianceProdType)
        {
            if (customerID != -1)
            {
                int roleidfromrole = UserManagement.GetRLCSRoleID(user.AVACOM_UserRole);

                roles = CustomerBranchManagement.GetAssignedroleid(Convert.ToInt32(user.AVACOM_UserID));

                var lstMgrAssignedBranch = CustomerBranchManagement.GetMGMTAssignedBranch(Convert.ToInt32(user.AVACOM_UserID));

                Session["User_comp_Roles"] = roles;
                if (roles.Contains(6))
                {
                    Approveruser_Roles = "APPR";
                }
                else
                {
                    Approveruser_Roles = "";
                }


                string userProfileID = string.Empty;
                string userProfileID_Encrypted = string.Empty;
                string authkey = string.Empty;

                if (roleidfromrole >= 14 && (complianceProdType == 1 || complianceProdType == 3))
                {
                    string TLConnectKey = ConfigurationManager.AppSettings["TLConnect_Encrypt_Decrypt_Key"];
                    string TLConnectAPIUrl = ConfigurationManager.AppSettings["TLConnect_API_URL"];

                    userProfileID = RLCSManagement.GetProfileIDByUserID(user.UserID, user.CustomerID);

                    if (!string.IsNullOrEmpty(TLConnectAPIUrl) && !string.IsNullOrEmpty(userProfileID))
                    {
                        try
                        {
                            authkey = RLCSManagement.GetAuthKeyByProfileID(TLConnectAPIUrl, userProfileID, TLConnectKey);
                        }
                        catch (Exception ex)
                        {
                            //userProfileID = string.Empty;
                            authkey = string.Empty;
                        }
                    }
                }
                else
                {
                    if (user.AVACOM_UserID != null)
                        userProfileID = user.AVACOM_UserID.ToString();
                }

                if (complianceProdType == 3 && role.StartsWith("H"))
                {
                    if (lstMgrAssignedBranch.Count > 0)
                        role = "MGMT";
                    else if (roles.Count > 0)
                        role = "EXCT";
                }

                FormsAuthentication.RedirectFromLoginPage(user.AVACOM_UserID.ToString(), Convert.ToBoolean(Session["RM"]));
                FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12}", user.AVACOM_UserID, role, name, checkInternalapplicable, "C", customerID, checkTaskapplicable, userProfileID, authkey, IsVerticlApplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType), false);
                TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);

                if (complianceProdType == 1)// TLConnect Customer
                {
                    if (role.Equals("HMGMT") || role.Equals("LSPOC") || role.Equals("HMGR") || role.Equals("HAPPR"))
                    {
                        HttpContext.Current.Response.Redirect("~/RLCS/RLCS_HRMDashboardNew.aspx", false);
                    }
                    else if (role.Equals("HEXCT") || role.Equals("DADMN")) 
                    {
                        HttpContext.Current.Response.Redirect("~/RLCS/HRPlus_DFMSPOCDashboard.aspx", false);
                    }
                    else if (role.Equals("HVADM") || role.Equals("HVAUD"))
                    {
                        HttpContext.Current.Response.Redirect("~/RLCSVendorAudit/RLCSUploadChecklist.aspx", false);
                    }
                    else if (role.Equals("VAUDT"))
                    {
                        HttpContext.Current.Response.Redirect("~/VenderAudit/aspxpages/VendorAuditDashboard.aspx", false);
                    }
                }
                else if (complianceProdType == 2)//HRPlus Compliance Product User
                {
                    if (role.Equals("HMGMT") || role.Equals("HMGR") || role.Equals("HEXCT"))
                    {
                        HttpContext.Current.Response.Redirect("~/RLCS/HRPlus_DFMSPOCDashboard.aspx", false);
                    }
                    else if (role.Equals("SPADM") || role.Equals("DADMN"))
                    {
                        HttpContext.Current.Response.Redirect("~/RLCS/HRPlus_DFMSPOCDashboard.aspx", false);
                    }
                    else if (role.Equals("CADMN"))
                    {
                        HttpContext.Current.Response.Redirect("~/RLCS/HRPlus_DFMDashboard.aspx", false);
                    }
                }
                else
                {
                    if (role.Equals("MGMT") || role.Equals("AUDT"))
                    {
                        HttpContext.Current.Response.Redirect("~/Management/MangementDashboard", false);
                    }
                    else if (role.Equals("IMPT"))
                    {
                        HttpContext.Current.Response.Redirect("~/Common/CompanyStructure.aspx", false);
                    }
                    else if (role.Equals("CADMN"))
                    {
                        if (roles.Count == 0)
                            HttpContext.Current.Response.Redirect("~/Common/ComplianceDashboard.aspx", false);
                        else
                            HttpContext.Current.Response.Redirect("~/Common/Dashboard.aspx", false);
                    }
                    else if (role.Equals("EXCT"))
                    {
                        HttpContext.Current.Response.Redirect("~/Common/Dashboard.aspx", false);
                    }
                    else if (Approveruser_Roles.Contains("APPR") && (roles.Contains(3) || roles.Contains(4)))
                    {
                        HttpContext.Current.Response.Redirect("~/Common/Dashboard.aspx", false);
                    }
                    else if (Approveruser_Roles.Contains("APPR"))
                    {
                        HttpContext.Current.Response.Redirect("~/Management/MangementDashboard", false);
                    }
                    else if (role.Equals("HVADM"))
                    {
                        HttpContext.Current.Response.Redirect("~/RLCSVendorAudit/RLCSUploadChecklist.aspx", false);
                    }
                    else if (role.Equals("HMGMT") || role.Equals("LSPOC") || role.Equals("HMGR") || role.Equals("HAPPR") || role.Equals("HEXCT")) //AVACOM+TL User
                    {
                        //HMGMT
                        if (role.Equals("HMGMT"))
                        {
                            if (lstMgrAssignedBranch.Count > 0)
                                HttpContext.Current.Response.Redirect("~/Management/MangementDashboard", false);
                            else if (roles.Count > 0)
                                HttpContext.Current.Response.Redirect("~/Common/Dashboard.aspx", false);
                            else
                                HttpContext.Current.Response.Redirect("~/RLCS/RLCS_HRMDashboardNew.aspx", false);
                        }
                        else if (role.Equals("LSPOC") || role.Equals("HMGR") || role.Equals("HAPPR") || role.Equals("HEXCT"))
                        {
                            if (roles.Count > 0)
                                HttpContext.Current.Response.Redirect("~/Common/Dashboard.aspx", false);
                            else
                                HttpContext.Current.Response.Redirect("~/RLCS/RLCS_HRMDashboardNew.aspx", false);
                        }
                    }
                    else
                    {
                        HttpContext.Current.Response.Redirect("~/Common/Dashboard.aspx", false);
                    }
                }
            }
        }
        public void FormsAuthenticationRedirect_RLCSCompliance(int customerID, RLCS_User_Mapping user, string role, string name, int checkInternalapplicable, int checkTaskapplicable, string profileID, string authKey, int IsVerticlApplicable, bool IsPaymentCustomer, int checkLabelApplicable, int complianceProdType)
        {
            if (customerID != -1)
            {
                int roleidfromrole = UserManagement.GetRLCSRoleID(user.AVACOM_UserRole);
                roles = CustomerBranchManagement.GetAssignedroleid(Convert.ToInt32(user.AVACOM_UserID));
                var lstMgrAssignedBranch = CustomerBranchManagement.GetMGMTAssignedBranch(Convert.ToInt32(user.AVACOM_UserID));
                Session["User_comp_Roles"] = roles;
                if (roles.Contains(6))
                {
                    Approveruser_Roles = "APPR";
                }
                else
                {
                    Approveruser_Roles = "";
                }
                string userProfileID = profileID;
                string authkey = authKey;

                if (String.IsNullOrEmpty(userProfileID) || string.IsNullOrEmpty(authKey))
                {
                    if (roleidfromrole >= 14 && complianceProdType < 2)
                    {
                        string TLConnectKey = ConfigurationManager.AppSettings["TLConnect_Encrypt_Decrypt_Key"];
                        string TLConnectAPIUrl = ConfigurationManager.AppSettings["TLConnect_API_URL"];

                        //userProfileID = RLCSManagement.GetProfileIDByUserID((long)user.AVACOM_UserID);
                        userProfileID = RLCSManagement.GetProfileIDByUserID(user.UserID, user.CustomerID);

                        if (!string.IsNullOrEmpty(TLConnectAPIUrl) && !string.IsNullOrEmpty(userProfileID))
                        {
                            try
                            {
                                authkey = RLCSManagement.GetAuthKeyByProfileID(TLConnectAPIUrl, userProfileID, TLConnectKey);
                            }
                            catch (Exception ex)
                            {
                                //userProfileID = string.Empty;
                                authkey = string.Empty;
                            }
                        }
                    }
                }

                FormsAuthentication.RedirectFromLoginPage(user.AVACOM_UserID.ToString(), Convert.ToBoolean(Session["RM"]));
                FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12}", user.AVACOM_UserID, role, name, checkInternalapplicable, "C", customerID, checkTaskapplicable, userProfileID, authkey, IsVerticlApplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType), false);
                TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);

                if (complianceProdType == 1)// TLConnect Customer
                {

                    if (role.Equals("HMGMT") || role.Equals("LSPOC") || role.Equals("HMGR") || role.Equals("HAPPR") || role.Equals("HEXCT"))
                    {
                        HttpContext.Current.Response.Redirect("~/RLCS/RLCS_HRMDashboardNew.aspx", false);
                    }
                    else if (role.Equals("HVADM") || role.Equals("HVAUD"))
                    {
                        HttpContext.Current.Response.Redirect("~/RLCSVendorAudit/RLCSUploadChecklist.aspx", false);
                    }
                    else if (role.Equals("VAUDT"))
                    {
                        HttpContext.Current.Response.Redirect("~/VenderAudit/aspxpages/VendorAuditDashboard.aspx", false);
                    }
                }
                else if (complianceProdType == 2)//HRPlus Compliance Product User
                {
                    if (role.Equals("HMGMT") || role.Equals("HMGR") || role.Equals("HEXCT"))
                    {
                        HttpContext.Current.Response.Redirect("~/RLCS/HRPlus_DFMSPOCDashboard.aspx", false);
                    }
                    else if (role.Equals("SPADM") || role.Equals("DADMN"))
                    {
                        HttpContext.Current.Response.Redirect("~/RLCS/HRPlus_DFMSPOCDashboard.aspx", false);
                    }
                    else if (role.Equals("CADMN"))
                    {
                        HttpContext.Current.Response.Redirect("~/RLCS/HRPlus_DFMDashboard.aspx", false);
                    }
                }
                else
                {
                    if (role.Equals("MGMT") || role.Equals("AUDT"))
                    {
                        HttpContext.Current.Response.Redirect("~/Management/MangementDashboard", false);
                    }
                    else if (role.Equals("IMPT"))
                    {
                        HttpContext.Current.Response.Redirect("~/Common/CompanyStructure.aspx", false);
                    }
                    else if (role.Equals("CADMN"))
                    {
                        if (roles.Count == 0)
                            HttpContext.Current.Response.Redirect("~/Common/ComplianceDashboard.aspx", false);
                        else
                            HttpContext.Current.Response.Redirect("~/Common/Dashboard.aspx", false);
                    }
                    else if (role.Equals("EXCT"))
                    {
                        HttpContext.Current.Response.Redirect("~/Common/Dashboard.aspx", false);
                    }
                    else if (Approveruser_Roles.Contains("APPR") && (roles.Contains(3) || roles.Contains(4)))
                    {
                        HttpContext.Current.Response.Redirect("~/Common/Dashboard.aspx", false);
                    }
                    else if (Approveruser_Roles.Contains("APPR"))
                    {
                        HttpContext.Current.Response.Redirect("~/Management/MangementDashboard", false);
                    }
                    else if (role.Equals("HVADM"))
                    {
                        HttpContext.Current.Response.Redirect("~/RLCSVendorAudit/RLCSUploadChecklist.aspx", false);
                    }
                    else if (role.Equals("HMGMT") || role.Equals("LSPOC") || role.Equals("HMGR") || role.Equals("HAPPR") || role.Equals("HEXCT")) //AVACOM+TL User
                    {
                        //HMGMT
                        if (role.Equals("HMGMT"))
                        {
                            if (lstMgrAssignedBranch.Count > 0)
                                HttpContext.Current.Response.Redirect("~/Management/MangementDashboard", false);
                            else if (roles.Count > 0)
                                HttpContext.Current.Response.Redirect("~/Common/Dashboard.aspx", false);
                            else
                                HttpContext.Current.Response.Redirect("~/RLCS/RLCS_HRMDashboardNew.aspx", false);
                        }
                        else if (role.Equals("LSPOC") || role.Equals("HMGR") || role.Equals("HAPPR") || role.Equals("HEXCT"))
                        {
                            if (roles.Count > 0)
                                HttpContext.Current.Response.Redirect("~/Common/Dashboard.aspx", false);
                            else
                                HttpContext.Current.Response.Redirect("~/RLCS/RLCS_HRMDashboardNew.aspx", false);
                        }
                    }
                    else
                    {
                        HttpContext.Current.Response.Redirect("~/RLCS/RLCS_HRMDashboardNew.aspx", false);
                    }
                }
            }
        }
        public void FormsAuthenticationRedirect_HRCompliance(User user, string role, string name, int checkInternalapplicable, int checkTaskapplicable, int IsVerticlApplicable, bool IsPaymentCustomer, int checkLabelApplicable, int complianceProdType)
        {
            roles = CustomerBranchManagement.GetAssignedroleid(Convert.ToInt32(user.ID));
            Session["User_comp_Roles"] = roles;
            if (roles.Contains(6))
            {
                Approveruser_Roles = "APPR";
            }
            else
            {
                Approveruser_Roles = "";
            }

            string userProfileID = string.Empty;
            string userProfileID_Encrypted = string.Empty;
            string authkey = string.Empty;
            
            //if (user.RoleID >= 14 && user.RoleID <= 18 && complianceProdType < 2)
            if (complianceProdType == 1 || complianceProdType == 3)
            {
                userProfileID = RLCSManagement.GetProfileIDByUserID(user.ID);

                string TLConnectKey = ConfigurationManager.AppSettings["TLConnect_Encrypt_Decrypt_Key"];
                string TLConnectAPIUrl = ConfigurationManager.AppSettings["TLConnect_API_URL"];

                if (!string.IsNullOrEmpty(TLConnectAPIUrl) && !string.IsNullOrEmpty(userProfileID))
                {
                    try
                    {
                        authkey = RLCSManagement.GetAuthKeyByProfileID(TLConnectAPIUrl, userProfileID, TLConnectKey);
                    }
                    catch (Exception ex)
                    {
                        userProfileID = string.Empty;
                        authkey = string.Empty;
                    }
                }
            }
            else
                userProfileID = user.ID.ToString();

            if (complianceProdType == 3 && role.StartsWith("H"))
            {
                var lstMgrAssignedBranch = CustomerBranchManagement.GetMGMTAssignedBranch(Convert.ToInt32(user.ID));
                if (lstMgrAssignedBranch.Count > 0)
                    role = "MGMT";
                else if (roles.Count > 0)
                    role = "EXCT";
            }

            FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), Convert.ToBoolean(Session["RM"]));
            FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12}", user.ID, role, name, checkInternalapplicable, "C", user.CustomerID, checkTaskapplicable, userProfileID, authkey, IsVerticlApplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType), false);

            TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
            Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);

            if (complianceProdType == 1)// TLConnect Customer
            {
                if (role.Equals("HMGMT") || role.Equals("LSPOC") || role.Equals("HMGR") || role.Equals("HAPPR") || role.Equals("HEXCT"))
                {
                    HttpContext.Current.Response.Redirect("~/RLCS/RLCS_HRMDashboardNew.aspx", false);
                }
                else if (role.Equals("HVADM") || role.Equals("HVAUD"))
                {
                    HttpContext.Current.Response.Redirect("~/RLCSVendorAudit/RLCSUploadChecklist.aspx", false);
                }
                else if (role.Equals("VAUDT"))
                {
                    HttpContext.Current.Response.Redirect("~/VenderAudit/aspxpages/VendorAuditDashboard.aspx", false);
                }
            }
            else if (complianceProdType == 2 || complianceProdType == 4) //HRPlus Compliance Product User
            {
                if (role.Equals("HMGMT") || role.Equals("HMGR") || role.Equals("HEXCT"))
                {
                    HttpContext.Current.Response.Redirect("~/RLCS/HRPlus_DFMSPOCDashboard.aspx", false);
                }
                else if (role.Equals("SPADM") || role.Equals("DADMN"))
                {
                    HttpContext.Current.Response.Redirect("~/RLCS/HRPlus_DFMSPOCDashboard.aspx", false);
                }
                else if (role.Equals("CADMN"))
                {
                    HttpContext.Current.Response.Redirect("~/RLCS/HRPlus_DFMDashboard.aspx", false);
                }
            }
            else
            {
                if (role.Equals("MGMT") || role.Equals("AUDT"))
                {
                    HttpContext.Current.Response.Redirect("~/Management/MangementDashboard", false);
                }
                else if (role.Equals("IMPT"))
                {
                    HttpContext.Current.Response.Redirect("~/Common/CompanyStructure.aspx", false);
                }
                else if (role.Equals("CADMN"))
                {
                    if (roles.Count == 0)
                        HttpContext.Current.Response.Redirect("~/Common/ComplianceDashboard.aspx", false);
                    else
                        HttpContext.Current.Response.Redirect("~/Common/Dashboard.aspx", false);
                }
                else if (role.Equals("HVADM") || role.Equals("HVAUD"))
                {
                    HttpContext.Current.Response.Redirect("~/RLCSVendorAudit/RLCSUploadChecklist.aspx", false);
                }
                else if (role.Equals("VAUDT"))
                {
                    HttpContext.Current.Response.Redirect("~/VenderAudit/aspxpages/VendorAuditDashboard.aspx", false);
                }
                else if (role.Equals("EXCT"))
                {
                    if (user.IsHead == true)
                    {
                        HttpContext.Current.Response.Redirect("~/Management/MgmtDashboardDeptHead.aspx", false);
                    }
                    else if (Approveruser_Roles.Contains("APPR") && (roles.Contains(3) || roles.Contains(4)))
                    {
                        if (user.IsHead == true)
                        {
                            HttpContext.Current.Response.Redirect("~/Management/MgmtDashboardDeptHead.aspx", false);
                        }
                        else
                        {
                            HttpContext.Current.Response.Redirect("~/Common/Dashboard.aspx", false);
                        }
                    }
                    else if (Approveruser_Roles.Contains("APPR"))
                    {
                        HttpContext.Current.Response.Redirect("~/Management/ManagementDashboardNew.aspx", false);
                    }
                    else
                    {
                        HttpContext.Current.Response.Redirect("~/Common/Dashboard.aspx", false);
                    }
                }                                
                else if (role.Equals("HMGMT") || role.Equals("LSPOC") || role.Equals("HMGR") || role.Equals("HAPPR") || role.Equals("HEXCT")) //AVACOM+TL User
                {
                    //HMGMT
                    if (role.Equals("HMGMT"))
                    {
                        var lstMgrAssignedBranch = CustomerBranchManagement.GetMGMTAssignedBranch(Convert.ToInt32(user.ID));

                        if (lstMgrAssignedBranch.Count > 0)
                            HttpContext.Current.Response.Redirect("~/Management/MangementDashboard", false);
                        else if (roles.Count > 0)
                            HttpContext.Current.Response.Redirect("~/Common/Dashboard.aspx", false);
                        else
                            HttpContext.Current.Response.Redirect("~/RLCS/RLCS_HRMDashboardNew.aspx", false);
                    }
                    else if (role.Equals("LSPOC") || role.Equals("HMGR") || role.Equals("HAPPR") || role.Equals("HEXCT"))
                    {
                        if (roles.Count > 0)
                            HttpContext.Current.Response.Redirect("~/Common/Dashboard.aspx", false);
                        else
                            HttpContext.Current.Response.Redirect("~/RLCS/RLCS_HRMDashboardNew.aspx", false);
                    }
                }
                else
                {
                    if (user.IsHead == true)
                    {
                        HttpContext.Current.Response.Redirect("~/Management/MgmtDashboardDeptHead.aspx", false);
                    }
                    else
                    {
                        HttpContext.Current.Response.Redirect("~/Common/Dashboard.aspx", false);
                    }
                }
            }
        }

        //private void FormsAuthenticationRedirect_Secreterial(User user, string role, string name, int checkInternalapplicable, int checkTaskapplicable)
        public void FormsAuthenticationRedirect_Secretarial(User user, string role, string name, int checkInternalapplicable, int checkTaskapplicable, int IsVerticlApplicable, bool IsPaymentCustomer, int checkLabelApplicable, int complianceProductType)
        {
            if (user.SecretarialRoleID != null)
            {
                string secretarialRole = RoleManagementRisk.GetByID(Convert.ToInt32(user.SecretarialRoleID)).Code;

                FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), Convert.ToBoolean(Session["RM"]));
                FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12}", user.ID, secretarialRole, name, checkInternalapplicable, "S", user.CustomerID, checkTaskapplicable, string.Empty, string.Empty, IsVerticlApplicable, IsPaymentCustomer, checkLabelApplicable, complianceProductType), false);
                //FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8}", user.ID, role, name, checkInternalapplicable, "L", user.CustomerID, checkTaskapplicable, string.Empty, string.Empty), false);
                TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
                Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);
                //if (role.Equals("MGMT"))
                //{
                //    HttpContext.Current.Response.Redirect("~/Litigation/Dashboard/LitigationManagementDashboard.aspx", false);
                //}
                //else if (role.Equals("CADMN"))
                //{
                //    HttpContext.Current.Response.Redirect("~/Litigation/Dashboard/LitigationManagementDashboard.aspx", false);
                //}
                //else
                //{
                //    HttpContext.Current.Response.Redirect("~/BM_Management/EntityMaster/GetEntityDtls", false);
                //    //HttpContext.Current.Response.Redirect("~/BM_Management/infofDirectors/GetInfoDirectorsData", false);
                //}

                //HttpContext.Current.Response.Redirect("~/BM_Management/EntityMaster/GetEntityDtls", false);

                if (secretarialRole.Equals("DRCTR"))
                {
                    HttpContext.Current.Response.Redirect("~/BM_Management/Dashboard/Dashboard", false);
                }
                else if (secretarialRole.Equals("HDCS") || secretarialRole.Equals("CS"))
                {
                    HttpContext.Current.Response.Redirect("~/BM_Management/Dashboard/", false);
                }
                else
                {
                    HttpContext.Current.Response.Redirect("~/BM_Management/Dashboard/", false);
                }
            }
            else
            {
                HttpContext.Current.Response.Redirect("~/ProductMapping/ProductMappingStructure.aspx", false);
            }
        }
        public bool ReAuthenticate_User(int customerID, string role = "", string prodApplLogin = "")
        {
            bool authSuccess = false;
            try
            {
                if (customerID != 0)
                {
                    string[] userDetails = HttpContext.Current.User.Identity.Name.Split(';');

                    if (userDetails.Length >= 9)
                    {
                        string UserID = userDetails[0];
                        string Role = userDetails[1];

                        if (!role.Equals(""))
                            Role = role;

                        string User = userDetails[2];
                        string IComplilanceApplicable = userDetails[3];
                        string ProductApplicableLogin = userDetails[4];

                        if (!prodApplLogin.Equals(""))
                            ProductApplicableLogin = prodApplLogin;

                        string CustomerID = customerID.ToString();
                        string TaskApplicable = userDetails[6];
                        string ProfileID = userDetails[7];
                        string AuthKey = userDetails[8];
                        string IsVerticalApplicable = userDetails[9];
                        string IsPaymentCustomer = userDetails[10];
                        string IsLabelApplicable = userDetails[11];
                        string ComplianceProductType = userDetails[12];

                        FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12}",
                          UserID, Role, User, IComplilanceApplicable, ProductApplicableLogin, CustomerID, TaskApplicable, ProfileID, AuthKey, IsVerticalApplicable, IsPaymentCustomer, IsLabelApplicable, ComplianceProductType), false);

                        authSuccess = true;
                    }
                }

                return authSuccess;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return authSuccess;
            }
        }

        private void ProcessAuthenticationInformation_InsiderTrading(User user)
        {
            try
            {
                string name = string.Format("{0} {1}", user.FirstName, user.LastName);
                string role = RoleManagement.GetByID((int)user.InsiderRoleID).Code;
                int checkInternalapplicable = 0;
                int checkTaskapplicable = 0;
                int checkVerticalapplicable = 0;
                int checkLabelApplicable = 0;
                bool IsPaymentCustomer = false;

                int complianceProdType = 0;

                if (user.CustomerID == null)
                {
                    checkInternalapplicable = 2;
                    checkTaskapplicable = 2;
                    checkVerticalapplicable = 2;
                    checkLabelApplicable = 2;
                    complianceProdType = 0;
                }
                else
                {
                    var IsInternalComplianceApplicable = Common.AuthenticationHelper.IComplilanceApplicable;
                    var IsTaskApplicable = Common.AuthenticationHelper.TaskApplicable;
                    var Isverticlapplicable = Common.AuthenticationHelper.IsVerticalApplicable;
                    var IsLabelApplicable = Common.AuthenticationHelper.IsLabelApplicable;
                    var valCompProdType = Common.AuthenticationHelper.ComplianceProductType;

                    checkTaskapplicable = Convert.ToInt32(IsTaskApplicable);
                    checkInternalapplicable = Convert.ToInt32(IsInternalComplianceApplicable);
                    checkVerticalapplicable = Convert.ToInt32(Isverticlapplicable);
                    checkLabelApplicable = Convert.ToInt32(IsLabelApplicable);
                    complianceProdType = Convert.ToInt32(valCompProdType);
                }

                FormsAuthenticationRedirect_InsiderTrading(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                if (user.ChangPasswordDate != null)
                {
                    DateTime LastPasswordChangedDate = Convert.ToDateTime(user.ChangPasswordDate);
                    DateTime currentDate = DateTime.Now;
                    LastPasswordChangedDate = LastPasswordChangedDate != null ? LastPasswordChangedDate : DateTime.Now;
                    int noDays = Convert.ToInt32(ConfigurationManager.AppSettings["ChangedPasswordDays"]);
                    int dateDifference = Convert.ToInt32((currentDate - LastPasswordChangedDate).TotalDays);
                    if (dateDifference == noDays || dateDifference > noDays)
                    {
                        Session["ChangePassword"] = true;
                        Response.Redirect("~/Account/ChangePassword.aspx", false);
                    }
                    else
                    {
                        Session["ChangePassword"] = false;
                    }
                }
                else
                {
                    Session["ChangePassword"] = true;
                    Response.Redirect("~/Account/ChangePassword.aspx", false);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void FormsAuthenticationRedirect_InsiderTrading(User user, string role, string name, int checkInternalapplicable, int checkTaskapplicable, int IsVerticlApplicable, bool IsPaymentCustomer, int checkLabelApplicable, int complianceProductType)
        {
            FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), Convert.ToBoolean(Session["RM"]));
            FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12}", user.ID, role, name, checkInternalapplicable, "IT", user.CustomerID, checkTaskapplicable, string.Empty, string.Empty, IsVerticlApplicable, IsPaymentCustomer, checkLabelApplicable, complianceProductType), false);
            //FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8}", user.ID, role, name, checkInternalapplicable, "L", user.CustomerID, checkTaskapplicable, string.Empty, string.Empty), false);
            TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
            Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);

            
                HttpContext.Current.Response.Redirect("~/Insiderdetails.aspx", false);
            
        }
        protected void btnInsiderTrading_Click(object sender, EventArgs e)
        {
            long customerID = -1;
            customerID = Common.AuthenticationHelper.CustomerID;
            var ProductMappingDetails = UserManagement.GetByProductIDList(Convert.ToInt32(customerID));
            if (ProductMappingDetails.Contains(10))
            {
                int userID = Common.AuthenticationHelper.UserID;
                ProcessAuthenticationInformation_InsiderTrading(UserManagement.GetByID(userID));
            }
            else
            {
                lblErrMsg.Text = "You don't have permission to Access. ";
            }
        }

        protected void btnVendorAudit_Click(object sender, EventArgs e)
        {
            long customerID = -1;
            customerID = Common.AuthenticationHelper.CustomerID;
            var ProductMappingDetails = UserManagement.GetByProductIDList(Convert.ToInt32(customerID));
            if (ProductMappingDetails.Contains(11))
            {
                int userID = Common.AuthenticationHelper.UserID;
                ProcessAuthenticationInformation_VendorAuditNew(UserManagement.GetByID(userID));
            }
            else
            {
                lblErrMsg.Text = "You don't have permission to Access. ";
            }
        }
    }
}