﻿using BM_ManegmentServices.Services.Compliance;
using BM_ManegmentServices.Services.DocumentManagenemt;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using GleamTech.DocumentUltimate.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas_Secretarial
{
    public partial class Doc_ViewerByScheduleOnId : System.Web.UI.Page
    {
        FileData_Service objIFileData_Service = new FileData_Service();
        Compliance_Service objICompliance_Service = new Compliance_Service();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                long meetingID;
                if (!string.IsNullOrEmpty(Request.QueryString["meetingId"]) && !string.IsNullOrEmpty(Request.QueryString["event"]))
                {
                    if (!long.TryParse(Request.QueryString["meetingId"], out meetingID))
                    {
                        meetingID = 0;
                    }

                    if(meetingID > 0)
                    {
                        BindFileDataList(meetingID, Request.QueryString["event"]);
                    }
                }
                else if (!string.IsNullOrEmpty(Request.QueryString["historicalId"]) && !string.IsNullOrEmpty(Request.QueryString["doc"]))
                {
                    if (!long.TryParse(Request.QueryString["historicalId"], out meetingID))
                    {
                        meetingID = 0;
                    }

                    if (meetingID > 0)
                    {
                        BindFileDataList(meetingID, Request.QueryString["doc"]);
                    }
                }
            }
        }

        protected void rptComplianceVersionView_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                LinkButton lblDocumentVersionView = (LinkButton)e.Item.FindControl("lblDocumentVersionView");
                scriptManager.RegisterAsyncPostBackControl(lblDocumentVersionView);
            }
        }

        protected void rptComplianceVersionView_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                if (e.CommandName.Equals("View"))
                {
                    string[] commandArg = e.CommandArgument.ToString().Split(',');
                    //List<GetComplianceDocumentsView> CMPDocuments = DocumentManagement.GetFileDataView(Convert.ToInt32(commandArgs[0]), Convert.ToInt32(commandArgs[2])).ToList();
                    long fileID;
                    if (!long.TryParse(commandArgs[2], out fileID))
                    {
                        fileID = 0;
                    }
                    //Session["ScheduleOnID"] = commandArg[0];

                    if (fileID > 0)
                    {
                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReview('" + fileID + "');", true);
                    }
                    else
                    {
                        lblMessage.Text = "There is no file to preview";
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void BindFileDataList(long meetingId, string eventName)
        {
            var scheduleOnID = objICompliance_Service.GetScheduleOnIDForMeetingEvent(meetingId, eventName);
            BindFileDataList(scheduleOnID);
        }

        protected void BindFileDataList(long scheduleOnID)
        {
            var result = objICompliance_Service.FileDataListByScheduleOnID(scheduleOnID);
            rptComplianceVersionView.DataSource = result;
            rptComplianceVersionView.DataBind();

            if(result != null)
            {
                if(result.Count > 0)
                {
                    var fileId = result.First().FileID;
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReview('" + fileId + "');", true);
                }
            }
        }

        protected void BindHistoricalFileList(long meetingId, string eventName)
        {
            var result = objIFileData_Service.FileDataListByHistoricalID(meetingId, eventName);
            rptComplianceVersionView.DataSource = result;
            rptComplianceVersionView.DataBind();

            if (result != null)
            {
                if (result.Count > 0)
                {
                    var fileId = result.First().FileID;
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReview('" + fileId + "');", true);
                }
            }
        }
    }
}