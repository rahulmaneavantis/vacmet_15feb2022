﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.AppCode.Controllers;
using com.VirtuosoITech.ComplianceManagement.Portal.AppCode.Models;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class EntityBranchDetails : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            vsEntityBranchPage.CssClass = "alert alert-danger";
            int customerBranchID = 0;
            int parentID = 0;

            if (!IsPostBack)
            {
                try
                {
                    ViewState["CustomerID"] = AuthenticationHelper.CustomerID;

                    BindCustomerStatus();
                    BindLegalRelationShips(false);
                    BindIndustry();
                    BindStates();

                    BindLegalEntityType();
                    BindCompanyTypeType();

                    BindBranchTypes();

                    if (!string.IsNullOrEmpty(Request.QueryString["BranchID"]) && !string.IsNullOrEmpty(Request.QueryString["ParentID"]))
                    {
                        customerBranchID = Convert.ToInt32(Request.QueryString["BranchID"]);
                        ViewState["CustomerBranchID"] = customerBranchID;

                        parentID = Convert.ToInt32(Request.QueryString["ParentID"]);
                        ViewState["ParentID"] = parentID;

                        if (customerBranchID > 0)
                        {
                            EditBranch(customerBranchID);
                        }
                        else if (customerBranchID == 0)
                        {
                            AddBranch(parentID);                            
                        }
                    }

                    divLegalEntityType.Visible = false;
                    divLegalRelationship.Visible = false;

                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvEntityBranchPage.IsValid = false;
                    cvEntityBranchPage.ErrorMessage = "Something went wrong, Please try again";
                }
            }
        }

        public void AddBranch(int parentID)
        {
            try
            {
                ViewState["Mode"] = 0;

                if (parentID != null)
                    parentName.Text = CustomerBranchManagement.GetByID(Convert.ToInt32(parentID)).Name;
                else
                    parentName.Text = string.Empty;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEntityBranchPage.IsValid = false;
                cvEntityBranchPage.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        public void EditBranch(int CustomerBranchID)
        {
            try
            {
                ViewState["Mode"] = 1;
                CustomerBranch customerBranch = CustomerBranchManagement.GetByID(CustomerBranchID);

                if (customerBranch != null)
                {
                    if (customerBranch.ParentID != null)
                        parentName.Text = CustomerBranchManagement.GetByID(Convert.ToInt32(customerBranch.ParentID)).Name;
                    else
                        parentName.Text = string.Empty;

                    if (!string.IsNullOrEmpty(customerBranch.Name))
                    {
                        tbxName.Text = customerBranch.Name;
                        tbxName.Enabled = false;
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(customerBranch.Type)))
                    {
                        if (customerBranch.Type != -1)
                        {
                            ddlType.SelectedValue = customerBranch.Type.ToString();
                            ddlType_SelectedIndexChanged(null, null);
                        }
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(customerBranch.LegalRelationShipID)))
                    {
                        if (customerBranch.LegalRelationShipID != -1 && customerBranch.LegalRelationShipID != 0)
                        {
                            ddlLegalRelationShip.SelectedValue = (customerBranch.LegalRelationShipID ?? -1).ToString();
                        }
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(customerBranch.LegalEntityTypeID)))
                    {
                        if (customerBranch.LegalEntityTypeID != -1 && customerBranch.LegalEntityTypeID != 0)
                        {
                            ddlLegalEntityType.SelectedValue = (customerBranch.LegalEntityTypeID ?? -1).ToString();
                        }
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(customerBranch.ComType)))
                    {
                        if (customerBranch.ComType != 0 && customerBranch.ComType != -1)
                        {
                            ddlCompanyType.SelectedValue = (customerBranch.ComType).ToString();
                        }
                    }

                    tbxAddressLine1.Text = customerBranch.AddressLine1;
                    tbxAddressLine2.Text = customerBranch.AddressLine2;

                    if (!string.IsNullOrEmpty(Convert.ToString(customerBranch.StateID)))
                    {
                        if (customerBranch.StateID != -1)
                        {
                            ddlState.SelectedValue = customerBranch.StateID.ToString();
                            ddlState_SelectedIndexChanged(null, null);
                        }
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(customerBranch.CityID)))
                    {
                        if (customerBranch.CityID != -1)
                        {
                            ddlCity.SelectedValue = customerBranch.CityID.ToString();
                            ddlCity_SelectedIndexChanged(null, null);
                        }
                    }

                    tbxOther.Text = customerBranch.Others;
                    tbxPinCode.Text = customerBranch.PinCode;
                    tbxContactPerson.Text = customerBranch.ContactPerson;
                    tbxLandline.Text = customerBranch.Landline;
                    tbxMobile.Text = customerBranch.Mobile;
                    tbxEmail.Text = customerBranch.EmailID;

                    if (!string.IsNullOrEmpty(Convert.ToString(customerBranch.Status)))
                    {
                        if (customerBranch.Status != -1)
                        {
                            ddlCustomerStatus.SelectedValue = Convert.ToString(customerBranch.Status);
                        }
                    }
                }

                //ddlIndustry.Enabled = false;
                //ddlType.Enabled = false;
                //ddlLegalRelationShipOrStatus.Enabled = false;
                //ddlLegalRelationShip.Enabled = false;

                if (ViewState["Mode"] != null)
                {
                    if ((int)ViewState["Mode"] == 1)
                    {
                        btnSave.Enabled = true;
                    }
                }

                upCustomerBranches.Update();                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEntityBranchPage.IsValid = false;
                cvEntityBranchPage.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        private void BindLegalRelationShips(bool parent)
        {
            try
            {
                ddlLegalRelationShip.DataTextField = "Name";
                ddlLegalRelationShip.DataValueField = "ID";

                var legalRelationShips = Enumerations.GetAll<LegalRelationship>();
                if (!parent)
                {
                    legalRelationShips.RemoveAt(0);
                }
                ddlLegalRelationShip.DataSource = legalRelationShips;
                ddlLegalRelationShip.DataBind();

                ddlLegalRelationShip.Items.Insert(0, new ListItem("Select", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEntityBranchPage.IsValid = false;
                cvEntityBranchPage.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        private void BindBranchTypes()
        {
            try
            {
                ddlType.DataSource = null;
                ddlType.DataBind();
                ddlType.ClearSelection();

                ddlType.DataTextField = "Name";
                ddlType.DataValueField = "ID";

                var dataSource = CustomerBranchManagement.GetAllNodeTypes();
                dataSource = dataSource.Where(entry => entry.ID != 1).ToList();

                ddlType.DataSource = dataSource;

                ddlType.DataBind();
                ddlType.Items.Insert(0, new ListItem("Other", "0"));
                ddlType.Items.Insert(0, new ListItem("Select", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEntityBranchPage.IsValid = false;
                cvEntityBranchPage.ErrorMessage = "Something went wrong, Please try again";
            }
        }
                
        private void BindIndustry()
        {
            try
            {
                rptIndustry.DataSource = CustomerBranchManagement.GetAllIndustry();
                rptIndustry.DataBind();

                foreach (RepeaterItem aItem in rptIndustry.Items)
                {
                    CheckBox chkIndustry = (CheckBox)aItem.FindControl("chkIndustry");

                    if (!chkIndustry.Checked)
                    {
                        //chkIndustry.Checked = true;
                    }
                }

                //CheckBox IndustrySelectAll = (CheckBox)rptIndustry.Controls[0].Controls[0].FindControl("IndustrySelectAll");
                //IndustrySelectAll.Checked = true;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindCities()
        {
            try
            {
                ddlCity.Items.Clear();

                if (ddlState.SelectedValue != "" || !string.IsNullOrEmpty(ddlState.SelectedValue) || ddlState.SelectedValue != "-1")
                {
                    //ddlCity.DataTextField = "Name";
                    //ddlCity.DataValueField = "ID";

                    //ddlCity.DataSource = AddressManagement.GetAllCitiesByState(Convert.ToInt32(ddlState.SelectedValue));
                    //ddlCity.DataBind();

                    ddlCity.DataTextField = "Name";
                    ddlCity.DataValueField = "ID";

                    ddlCity.DataSource = RLCS_Master_Management.FillLocationCityByStateID(Convert.ToInt32(ddlState.SelectedValue));
                    ddlCity.DataBind();

                    ddlCity.Items.Insert(0, new ListItem("Other", "0"));
                    ddlCity.Items.Insert(0, new ListItem("Select", "-1"));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEntityBranchPage.IsValid = false;
                cvEntityBranchPage.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        private void BindStates()
        {
            try
            {
                ddlState.DataTextField = "Name";
                ddlState.DataValueField = "ID";

                //ddlState.DataSource = AddressManagement.GetAllStates();
                //ddlState.DataBind();

                ddlState.DataSource = RLCS_Master_Management.FillStates();
                ddlState.DataBind();

                ddlState.Items.Insert(0, new ListItem("Select", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEntityBranchPage.IsValid = false;
                cvEntityBranchPage.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindCities();
            ddlCity.SelectedValue = "-1";
        }

        protected void ddlCity_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlCity.SelectedValue == "0" && divOther.Visible == false)
                {
                    divOther.Visible = true;
                    tbxOther.Text = string.Empty;
                }
                else if (ddlCity.SelectedValue != "0" && string.IsNullOrEmpty(tbxPinCode.Text.Trim()))
                {
                    if (ddlCity.SelectedItem.Text.Contains("-"))
                    {
                        string[] city = ddlCity.SelectedItem.Text.Split('-');

                        if (city != null)
                        {
                            if (city.Length == 2)
                                tbxPinCode.Text = city[1];
                        }
                    }

                    divOther.Visible = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEntityBranchPage.IsValid = false;
                cvEntityBranchPage.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        public void BindCustomerStatus()
        {
            try
            {
                ddlCustomerStatus.DataTextField = "Name";
                ddlCustomerStatus.DataValueField = "ID";

                ddlCustomerStatus.DataSource = Enumerations.GetAll<CustomerStatus>();
                ddlCustomerStatus.DataBind();

                if (ddlCustomerStatus.Items.FindByText("Active") != null)
                    ddlCustomerStatus.Items.FindByText("Active").Selected = true;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEntityBranchPage.IsValid = false;
                cvEntityBranchPage.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        private void BindLegalEntityType()
        {
            try
            {
                ddlLegalEntityType.DataTextField = "EntityTypeName";
                ddlLegalEntityType.DataValueField = "ID";
                ddlLegalEntityType.DataSource = CustomerBranchManagement.GetAllLegalEntityType();
                ddlLegalEntityType.DataBind();
                //ddlLegalEntityType.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEntityBranchPage.IsValid = false;
                cvEntityBranchPage.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        private void BindCompanyTypeType()
        {
            try
            {
                ddlCompanyType.DataTextField = "Name";
                ddlCompanyType.DataValueField = "ID";
                ddlCompanyType.DataSource = CustomerBranchManagement.GetAllComanyTypeCustomerBranch();
                ddlCompanyType.DataBind();
                ddlCompanyType.Items.Insert(0, new ListItem("Select", "-1"));
                ddlCompanyType.SelectedValue = "-1";
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEntityBranchPage.IsValid = false;
                cvEntityBranchPage.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void ddlType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                divLegalRelationship.Visible = ddlType.SelectedValue == "1";
                string industryId = string.Empty;

                if (!divLegalRelationship.Visible)
                {
                    ddlLegalRelationShip.SelectedIndex = -1;
                }

                if (ddlType.SelectedValue == "1")
                {
                    divLegalEntityType.Visible = true;
                    divCompanyType.Visible = true;
                }
                else
                {
                    divLegalEntityType.Visible = false;
                    divCompanyType.Visible = false;
                }


                if (ddlType.SelectedValue == "0" && divOtherType.Visible == false)
                {
                    divOtherType.Visible = true;
                    txtOtherType.Text = string.Empty;
                }
                else if (ddlCity.SelectedValue != "0")
                {
                    divOtherType.Visible = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEntityBranchPage.IsValid = false;
                cvEntityBranchPage.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void upCustomerBranches_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeDatePicker();", true);

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeIndustryList", string.Format("initializeJQueryUI('{0}', 'dvIndustry');", txtIndustry.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideIndustryList", "$(\"#dvIndustry\").hide(\"blind\", null, 5, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEntityBranchPage.IsValid = false;
                cvEntityBranchPage.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["Mode"] != null)
                {
                    bool saveSuccess = false;
                    int branchType = 0;

                    if (!string.IsNullOrEmpty(ddlType.SelectedValue))
                    {
                        if (ddlType.SelectedValue == "1")
                        {
                            if (ddlCompanyType.SelectedValue == "-1")
                            {
                                cvEntityBranchPage.ErrorMessage = "Please select Type";
                                cvEntityBranchPage.IsValid = false;
                                return;
                            }
                        }
                        else if (ddlType.SelectedValue == "0")
                        {
                            if (!string.IsNullOrEmpty(txtOtherType.Text))
                            {
                                branchType = CustomerBranchManagement.CreateBranchType(txtOtherType.Text);

                                if (branchType == -1)
                                {
                                    cvEntityBranchPage.ErrorMessage = "Other Type-" + txtOtherType.Text.Trim() + "already exists, Please Select from Branch Type";
                                    cvEntityBranchPage.IsValid = false;
                                    return;
                                }
                                else if (branchType == 0)
                                {
                                    cvEntityBranchPage.ErrorMessage = "Not able to create Other Type-" + txtOtherType.Text.Trim() + ", check the text and try again";
                                    cvEntityBranchPage.IsValid = false;
                                    return;
                                }
                            }
                            else
                            {
                                cvEntityBranchPage.ErrorMessage = "Please Specify(Other Type)";
                                cvEntityBranchPage.IsValid = false;
                                return;
                            }
                        }
                        else
                            branchType = Convert.ToInt32(ddlType.SelectedValue);
                    }
                    else
                    {
                        cvEntityBranchPage.ErrorMessage = "Please Select Branch Type";
                        cvEntityBranchPage.IsValid = false;
                        return;
                    }

                    int comType = 0;
                    if (!string.IsNullOrEmpty(ddlCompanyType.SelectedValue))
                    {
                        if (ddlCompanyType.SelectedValue.ToString() == "-1")
                        {
                            comType = 0;
                        }
                        else
                        {
                            comType = Convert.ToInt32(ddlCompanyType.SelectedValue);
                        }
                    }

                    #region Compliance-Branch
                    CustomerBranch customerBranch = new CustomerBranch()
                    {
                        Name = tbxName.Text.Trim(),
                        Type = Convert.ToByte(branchType),
                        ComType = Convert.ToByte(comType),
                        AddressLine1 = tbxAddressLine1.Text,
                        AddressLine2 = tbxAddressLine2.Text,
                        StateID = Convert.ToInt32(ddlState.SelectedValue),
                        CityID = Convert.ToInt32(ddlCity.SelectedValue),
                        Others = tbxOther.Text,
                        PinCode = tbxPinCode.Text,
                        //Industry = Convert.ToInt32(ddlIndustry.SelectedValue),
                        ContactPerson = tbxContactPerson.Text,
                        Landline = tbxLandline.Text,
                        Mobile = tbxMobile.Text,
                        EmailID = tbxEmail.Text,
                        CustomerID = Convert.ToInt32(ViewState["CustomerID"]),
                        ParentID = ViewState["ParentID"] == null ? (int?)null : Convert.ToInt32(ViewState["ParentID"]),
                        Status = Convert.ToInt32(ddlCustomerStatus.SelectedValue)
                    };

                    if (ddlType.SelectedValue == "1")
                    {
                        if (!string.IsNullOrEmpty(ddlLegalRelationShip.SelectedValue))
                        {
                            customerBranch.LegalRelationShipID = Convert.ToInt32(ddlLegalRelationShip.SelectedValue);
                        }
                        if (!string.IsNullOrEmpty(ddlLegalEntityType.SelectedValue))
                        {
                            customerBranch.LegalEntityTypeID = Convert.ToInt32(ddlLegalEntityType.SelectedValue);
                        }
                    }
                    else
                    {
                        customerBranch.LegalRelationShipID = null;
                        customerBranch.LegalEntityTypeID = null;
                    }

                    #endregion

                    #region Audit-Branch
                    com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_CustomerBranch customerBranch1 = new com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_CustomerBranch()
                    {
                        Name = tbxName.Text.Trim(),
                        Type = Convert.ToByte(branchType),
                        ComType = Convert.ToByte(comType),
                        AddressLine1 = tbxAddressLine1.Text,
                        AddressLine2 = tbxAddressLine2.Text,
                        StateID = Convert.ToInt32(ddlState.SelectedValue),
                        CityID = Convert.ToInt32(ddlCity.SelectedValue),
                        Others = tbxOther.Text,
                        PinCode = tbxPinCode.Text,
                        //Industry = Convert.ToInt32(ddlIndustry.SelectedValue),
                        ContactPerson = tbxContactPerson.Text,
                        Landline = tbxLandline.Text,
                        Mobile = tbxMobile.Text,
                        EmailID = tbxEmail.Text,
                        CustomerID = Convert.ToInt32(ViewState["CustomerID"]),
                        ParentID = ViewState["ParentID"] == null ? (int?)null : Convert.ToInt32(ViewState["ParentID"]),
                        Status = Convert.ToInt32(ddlCustomerStatus.SelectedValue)
                    };

                    if (ddlType.SelectedValue == "1")
                    {
                        if (!string.IsNullOrEmpty(ddlLegalRelationShip.SelectedValue))
                        {
                            customerBranch1.LegalRelationShipID = Convert.ToInt32(ddlLegalRelationShip.SelectedValue);
                        }
                        if (!string.IsNullOrEmpty(ddlLegalEntityType.SelectedValue))
                        {
                            customerBranch1.LegalEntityTypeID = Convert.ToInt32(ddlLegalEntityType.SelectedValue);
                        }
                    }
                    else
                    {
                        customerBranch1.LegalRelationShipID = null;
                        customerBranch1.LegalEntityTypeID = null;
                    }
                    #endregion

                    if ((int)ViewState["Mode"] == 1)
                    {
                        customerBranch.ID = Convert.ToInt32(ViewState["CustomerBranchID"]);
                        customerBranch1.ID = Convert.ToInt32(ViewState["CustomerBranchID"]);
                    }

                    if (CustomerBranchManagement.Exists(customerBranch, Convert.ToInt32(ViewState["CustomerID"])))
                    {
                        cvEntityBranchPage.ErrorMessage = "Branch with same name already exists";
                        cvEntityBranchPage.IsValid = false;
                        return;
                    }

                    if (CustomerBranchManagement.Exists1(customerBranch1, Convert.ToInt32(ViewState["CustomerID"])))
                    {
                        cvEntityBranchPage.ErrorMessage = "Branch with same name already exists";
                        cvEntityBranchPage.IsValid = false;
                        return;
                    }

                    


                    if ((int)ViewState["Mode"] == 0)
                    {
                        int newBranchID = CustomerBranchManagement.Create(customerBranch);

                        if (newBranchID > 0)
                        {
                            saveSuccess = CustomerBranchManagement.Create1(customerBranch1);
                        }
                    }
                    else if ((int)ViewState["Mode"] == 1)
                    {
                        bool updateSuccess= CustomerBranchManagement.Update(customerBranch);

                        if (updateSuccess)
                            saveSuccess = CustomerBranchManagement.Update1(customerBranch1);
                    }

                    if (saveSuccess)
                    {
                        cvEntityBranchPage.ErrorMessage = "Details Save Successfully";
                        cvEntityBranchPage.IsValid = false;
                        vsEntityBranchPage.CssClass = "alert alert-success";
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "hide&showscript2", "Hideshow();", true);
                    }
                    else
                    {
                        cvEntityBranchPage.ErrorMessage = "Something went wrong, Please try again";
                        cvEntityBranchPage.IsValid = false;
                    }

                    upCustomerBranches.Update();                    
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEntityBranchPage.IsValid = false;
                cvEntityBranchPage.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
    }
}