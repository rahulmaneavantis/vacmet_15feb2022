﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddEditEntityWiseConfigurations.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Areas_Secretarial.AddEditEntityWiseConfigurations" %>

<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
   
    <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />    
    <link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />   
    <link href="~/NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />
       
    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/contract_custom_style.css" rel="stylesheet" />
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
   
    <script type="text/javascript" src="/Newjs/jquery.js"></script>
    <script type="text/javascript" src="/Newjs/bootstrap.min.js"></script>

    <link href="~/Content/2018.3.1017/css/kendo.common.min.css" rel="stylesheet" />
    <link href="~/Content/2018.3.1017/css/kendo.default.min.css" rel="stylesheet" />
    
    <link href="~/Content/css/Kendouicss.css" rel="stylesheet" />   
    <link href="~/Content/css/Layoutcss.css" rel="stylesheet" />        

    <script type="text/javascript" src="/Content/2018.3.1017/js/kendo.all.min.js"></script>

    <style>
        .m-10 {
            /*margin-left: 10px;*/
            padding-left: 24px;
        }

        .pl0 {
            padding-left: 0px;
        }

        .required .control-label:after {
            content: "*";
            color: red;
            margin-left: 1px;
        }

        .btn-primary {
            font-weight: 500;
            background-color: #0090d6;
        }
    </style>
    <script>
        $(document).ready(function (){
            BindGrid();
           
            $("#btnSaveSetting").click(function (e) {
                Save_Settings(e);
            });
        });
                
        function ClosePopupWindowAfterSave() {
            setTimeout(function () {
                if (window.parent.$("#divAddEditCustomerDialog") != null && window.parent.$("#divAddEditCustomerDialog") != undefined)
                    window.parent.$("#divAddEditCustomerDialog").data("kendoWindow").close();
            }, 3000);
        }       

        function gridConfiguration_RowDataBound(e) {            
            $(".numeric").kendoNumericTextBox({
                min: 0,
                format: "n0",
            });
            $("#gridConfiguration tr.k-alt").removeClass("k-alt");
        }

        function BindGrid() {

            kendo.ui.progress($("#divGridParent"), true);
            
            var sharedDataSource = new kendo.data.DataSource({
                transport: {
                    read: {
                        url: '/api/CustomerMasterAPI/GetEntityConfigurationCustomerWise?custID='+<%= selectedCustomerID%>,
                        dataType: "json",
                    }
                },
                schema: {
                    model: { id: "EntityTypeID" },
                    data: function (response) {
                        if (response != null && response != undefined) {
                            return response;
                        }
                    },
                    total: function (response) {
                        if (response != null && response != undefined) {
                            if (response.length > 0) {

                            }
                            return response.length;
                        }
                    }
                },

                pageSize: 10
            });

            sharedDataSource.read();

            $("#gridConfiguration").kendoGrid({
                dataSource: sharedDataSource,
                autoBind: false,
                columnMenu: {
                    columns: false,
                },
                filterable: true,
                sortable: true,
                scrollable: true,
                pageable: {
                    pageSizes: true
                },
                //change: onRowSelect,
                dataBound: gridConfiguration_RowDataBound,
                columns: [      
                    { field: "EntityTypeID", title: "EntityTypeID", hidden: true },
                    { field: "EntityTypeName", title: "Type", width: "80%", attributes: { "class": "showTooltip", style: 'white-space: nowrap;' }, sortable: true, },
                    { field: "AllowedValue", title: 'Max Limit', template: kendo.template($("#valueTemplate").html()) , width: "20%" },                    
                ],
                editable: false,
            });

            kendo.ui.progress($("#divGridParent"), false);
        }

        function Save_Settings(e) {
            debugger;
            e.preventDefault();
            kendo.ui.progress($("#gridConfiguration"), true);
            var saveSuccess = false;

            var grid = $("#gridConfiguration").data("kendoGrid");

            if ($('#hdnCustID').val() != null && $('#hdnCustID').val() != undefined) {
                var gridDataSource = grid.dataSource.view();
                var gridData = grid._data;
                if (gridData.length > 0) {
                    var objJson = [];

                    for (var i = 0; i < gridData.length; i++) {
                        _obj = {}
                        var currentUid = gridDataSource[i].uid;
                        var currenRow = grid.table.find("tr[data-uid='" + currentUid + "']");

                        var textBox=$(currenRow).find("input.k-formatted-value:input");

                        if(textBox!=null && textBox!=undefined){
                            if(textBox.length>0){
                                _obj["CustID"] = $('#hdnCustID').val();
                                _obj["EntityTypeID"] = gridData[i].EntityTypeID;                        
                                _obj["Value"] = textBox[0].value;
                                _obj["UserID"] = <%= loggedInUserID%>;

                                objJson.push(_obj);
                            }
                        }
                    }

                    if (objJson.length > 0) {
                        var uri = '/api/CustomerMasterAPI/SaveSettings'
                        $.ajax({
                            //async: true,
                            type: 'Post',
                            url: uri,
                            data: JSON.stringify(objJson),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",                                                     
                            success: function (result) {  
                                debugger;
                                saveSuccess = true;
                                showValidationSummary('success','Details Save Successfully');                                
                                //kendo.alert("Details Save Successfully");
                                //alert(JSON.stringify('Data Save Successfully'));
                                //$("#divAlert").html("Data save successfully"); 
                                kendo.ui.progress($("#gridConfiguration"), false);  
                                ClosePopupWindowAfterSave();
                            },
                            error: function (e, t) {
                                debugger;
                                kendo.ui.progress($("#gridConfiguration"), false);
                                showValidationSummary('error','Something wents wrong, Please try again');       
                                //alert("Error-" + e.error.toString());
                            }
                        });
                    }

                    if (saveSuccess) {
                        kendo.ui.progress($("#gridConfiguration"), false);
                    }
                }
            }

            return true;
        }

        function showValidationSummary(type,message){
            var className='alert-success';
            
            if(type==='success')
                className='alert-success';
            else if(type==='error')
                className='alert-danger';

            var alertClass='alert '+className+' alert-dismissible fade show';

            $("#divValidationSummary").empty();
            $("#divValidationSummary").append('<div class="'+alertClass+'" role="alert">'+message+'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
            $("#divValidationSummary").removeClass("hidden");
        }
    </script>

    <script id="valueTemplate" type="text/x-kendo-tmpl"> 
        <input class="numeric" type="number" value="#= AllowedValue #" step="1" />
      </script>
</head>
<body style="background: white;">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:HiddenField ID="hdnCustID" runat="server" />

        <div id="divValidationSummary" class="hidden"></div>

       <div class="row colpadding0 mt10">
            <div id="divGridParent" class="col-md-12 colpadding0">
                <div id="gridConfiguration"></div>
            </div>

           <div class="row col-md-12 colpadding0 mt10 text-center">
               <button id="btnSaveSetting" class="btn btn-primary">Save & Close</button>
           </div>
        </div>

    </form>
</body>
</html>
