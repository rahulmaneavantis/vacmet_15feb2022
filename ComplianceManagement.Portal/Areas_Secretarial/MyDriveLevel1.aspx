﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MyDriveLevel1.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Areas_Secretarial.MyDriveLevel1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <!-- Bootstrap CSS -->
    <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- bootstrap theme -->
    <link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />
        <!-- font icon -->
    <link href="~/NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <!--external css-->
    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />    
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
   
    <script type="text/javascript" src="/Newjs/jquery.js"></script>
    <script type="text/javascript" src="/Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="/Newjs/bootstrap.min.js"></script>   

    <link href="~/NewCSS/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
    <script src="/Newjs/bootstrap-multiselect.js" type="text/javascript"></script>
       
    <!-- nice scroll -->
    <script type="text/javascript" src="/Newjs/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="/Newjs/jquery.nicescroll.js"></script>
    <link href="../NewCSS/Document_Drive_Style.css" rel="stylesheet" />  

    <script type="text/javascript" src="/Newjs/kendo.all.min.js"></script>

     <script type="text/javascript">
         $(document).ready(function () {
             $(document).tooltip({ selector: '[data-toggle="tooltip"]' });

            FetchUSerDetail();
            $("button.multiselect").on("click", function () {
                $(this).parent().addClass("open");
            });
           
            $(".notification-row > ul > .dropdown").click(function () { $('.notification-row > ul > .dropdown').addClass('open') });

            if ($(window.parent.document).find("#divParentIframe").children().first().hasClass("k-loading-mask")) {                
                $(window.parent.document).find("#divParentIframe").children().first().hide();
            }

            $(document).kendoTooltip({
                filter: ".sharedrive",
                content: function (e) {
                    return "Share";
                },
                show: PositionTooltip
            });

            $(document).kendoTooltip({
                filter: ".deletedrive",
                content: function (e) {
                    return "Delete";
                },
                show: PositionTooltip
            });            
         });

         function PositionTooltip(e) {
             var position = e.sender.options.position;
             if (position == "bottom") {
                 e.sender.popup.element.css("margin-top", "10px");
             } else if (position == "top") {
                 e.sender.popup.element.css("margin-bottom", "10px");
             }
         }

        function OpenNewPermissionPopup() {
            FetchUSerDetail();
            $('#divOpenPermissionPopup').modal('show');
        }

        function fopenpopup() {
            $('#divOpenNewFolderPopup').modal('show');
        }

        function fclosepopup() {
            $('#divOpenNewFolderPopup').modal('hide');
        }

        function lnkNewTest() {
            $('#lnkDropdown1').addClass('open');
            debugger;
        }

        function FetchUSerDetail() {
            $('[id*=lstBoxUser]').multiselect({
                includeSelectAllOption: true,
                numberDisplayed: 2,
                buttonWidth: '100%',
                enableCaseInsensitiveFiltering: true,
                filterPlaceholder: 'Type to Search for User..',
                nSelectedText: ' - Owner(s) selected',
            });
        }
    </script>

     <script type="text/javascript">
         $(document).ready(function () {
         $('.clsROWgrid').click(function () {
             debugger;
            setInterval(function () { }, 100);

            var owner = $(this).find('.ownerdoc');
            if ($(this).css('background-color') == '#f8f8f8' || $(this).css('background-color') == 'rgb(248, 248, 248)') {
                var fclick = $(this).find('.foldertogo');
                if ($(this).find('.foldertogo').attr('id') != null && $(this).find('.foldertogo').attr('id') != undefined) {
                    document.getElementById($(fclick).attr('id')).click();
                }
            } else {
                $('.deletedrive').hide();
                $('.sharedrive').hide();
                if ($(owner).text() == 'me') {
                    $('.deletedrive').show();
                    $('.sharedrive').show();
                }
                $('.clsROWgrid').css('background-color', 'white');
                $('.clsROWgrid').removeClass('selectedDocs');
                $('.deleteandsharediv').show();
                $(this).css('background-color', '#f8f8f8');
                $(this).addClass('selectedDocs');
            }
        });
        $('.deletedrive').on('click', function (event) {
            var deletedrive = $('.selectedDocs').find('.deletef');
            var r = confirm('You sure you want to delete this folder, as all sub-folders and files related to this folder also get deleted?')
            if (r == true) {
                document.getElementById($(deletedrive).attr('id')).click();
            }
            event.stopPropagation();
        });
        $('.sharedrive').on('click', function (event) {
            var shareddrive = $('.selectedDocs').find('.sharef');
            document.getElementById($(shareddrive).attr('id')).click();
            event.stopPropagation();
        });

        
            debugger;
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_endRequest(function () {
                $('.clsROWgrid').click(function () {
                    debugger;
                    var owner = $(this).find('.ownerdoc');
                    if ($(this).css('background-color') == '#f8f8f8' || $(this).css('background-color') == 'rgb(248, 248, 248)') {
                        var fclick = $(this).find('.foldertogo');
                        if ($(this).find('.foldertogo').attr('id') != null && $(this).find('.foldertogo').attr('id') != undefined) {
                            document.getElementById($(fclick).attr('id')).click();
                        }
                    } else {
                        $('.deletedrive').hide();
                        $('.sharedrive').hide();
                        if ($(owner).text() == 'me') {
                            $('.deletedrive').show();
                            $('.sharedrive').show();                            
                        }
                        $('.clsROWgrid').css('background-color', 'white');
                        $('.clsROWgrid').removeClass('selectedDocs');
                        $('.deleteandsharediv').show();
                        $(this).css('background-color', '#f8f8f8');
                        $(this).addClass('selectedDocs');
                    }
                });
                $('.deletedrive').on('click', function (event) {
                    var deletedrive = $('.selectedDocs').find('.deletef');
                    var r = confirm('You sure you want to delete this folder, as all sub-folders and files related to this folder also get deleted?')
                    if (r == true) {
                        document.getElementById($(deletedrive).attr('id')).click();
                    }
                    event.stopPropagation();
                });
                $('.sharedrive').on('click', function (event) {
                    var shareddrive = $('.selectedDocs').find('.sharef');
                    document.getElementById($(shareddrive).attr('id')).click();
                    event.stopPropagation();
                });
            });

            
        });

         function thirty_pc() {
             debugger;
             $("#divParentIframe").css({ 'height': ($(window).height()) + 'px' });
             $("#divParentIframe").css({ 'max-height': ($(window).height()) + 'px' });
         }         

         //$(document).ready(function () {
         //    thirty_pc();
         //    $(window).bind('resize', thirty_pc);
         //});

    </script>

    <style type="text/css">

        body {           
            background: #f8f9fa;
        }

        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }

        .folder-size {
            height: 18px;
            width: 18px;
            float: left;
            margin-right: 15px;
        }

        .table {
            width: 100%;
            border-collapse: collapse;
            border: 1px solid #D5D5D5;
        }

        .table > tbody > tr > th {
            font-weight: bold;
        }

        .clsROWgrid {
            cursor: pointer;
        }

        .clsheadergrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: bottom !important;
            border-bottom: 2px solid #dddddd !important;
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
            text-align: left;
            background: #E9EAEA;
            border: 1px solid #DDDDDD;
            box-sizing: border-box;
        }

        .rowVerticalCenter {
            display: flex; /* make the row a flex container */
            align-items: center;
        }

        .hoverCircle:hover {
            -moz-box-shadow: 0 0 0 12px #E9EAEA;
            -webkit-box-shadow: 0 0 0 12px #E9EAEA;
            box-shadow: 0 0 0 12px #E9EAEA;
            -moz-transition: -moz-box-shadow .3s;
            -webkit-transition: -webkit-box-shadow .3s;
            transition: box-shadow .3s;
            border-radius: 50%;
            background-color: #E9EAEA;
        }

        .master-Headre {
            height: 80px;
            position: relative;
            background: #FFFFFF;
            border: 1px solid #DDDDDD;
            box-sizing: border-box;
            box-shadow: 0px 3px 4px rgba(0, 0, 0, 0.1);
            border-radius: 4px;
        }

        .vertical-center {
    margin: 0;
    position: absolute;
    top: 50%;
    -ms-transform: translateY(-50%);
    transform: translateY(-50%);
}
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div>
            <asp:UpdatePanel ID="upPromotorList" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="row Dashboard-white-widget">
                        <div class="dashboard">
                            <div class="col-lg-12 col-md-12">
                                <div style="clear: both; height: 15px;"></div>

                                <div class="row col-lg-12 col-md-12 colpadding0 rowVerticalCenter">
                                    <div class="col-md-6 col-md-4 colpadding0">
                                        <div class="input-group">
                                            <asp:TextBox runat="server" ID="tbxFilter" CssClass="form-control" MaxLength="50"
                                                PlaceHolder="Type to Search Files, folders(e.g. tags, process and subprocess)" AutoPostBack="true" OnTextChanged="tbxFilter_TextChanged" />
                                            <div class="input-group-btn">
                                                <button class="btn btn-primary" type="submit" style="height: 34px">
                                                    <i class="fa fa-search"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-5 text-right">
                                        <div class="col-md-12 colpadding0 deleteandsharediv " style="display: none; float: left;">
                                            <img class="sharedrive hoverCircle" src="../Areas/BM_Management/img/share.png" style="cursor: pointer; margin-right: 15px"  />
                                            <img class="deletedrive hoverCircle" src="../Areas/BM_Management/img/Delete.png" style="cursor: pointer;" />
                                        </div>
                                    </div>

                                    <div class="col-md-1 colpadding0 text-right">
                                        <li id="lnkDropdown1" class="dropdown hidden" style="float: right; list-style: none;">
                                            <button class="dropdown-toggle" onclick="lnkNewTest();" style="width: 95px; height: 42px; background-image: url(/images/Addnewicon.png); background-color: white; border: none;" type="button" id="menu1" data-toggle="dropdown">
                                            </button>
                                            <ul class="dropdown-menu" role="menu" aria-labelledby="menu1" style="margin: 0px -35px 0;">
                                                <li role="presentation">
                                                    
                                                </li>
                                            </ul>
                                        </li>
                                        
                                        <li class="dropdown">
                                            <%--<a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                                <span class="customer-photo1 textimage1" id="userIcon1" style="background-color: rgb(33, 153, 153); color: rgb(255, 255, 255); text-align: center; line-height: 202%;">H</span>

                                                <b class="caret"></b>
                                            </a>--%>

                                            <button class="dropdown-toggle" onclick="lnkNewTest();" style="width: 95px; height: 42px; background-image: url(/images/Addnewicon.png); background-color: white; border: none;" type="button" id="menu1" data-toggle="dropdown">
                                            </button>

                                            <ul class="dropdown-menu" style="margin: 0px -65px 0;">
                                                <li role="presentation">
                                                    <a role="menuitem" tabindex="-1">
                                                        <asp:LinkButton ID="lnkAddNewFolder" runat="server"
                                                            OnClientClick="fopenpopup()" OnClick="lnkAddNewFolder_Click">New Folder
                                                        </asp:LinkButton>
                                                    </a>                                                    
                                                </li>
                                            </ul>
                                        </li>
                                    </div>
                                </div>

                                <div class="row col-lg-12 col-md-12 colpadding0">
                                    <asp:GridView runat="server" ID="grdFolderDetail" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true" OnRowCommand="grdFolderDetail_RowCommand"
                                        PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%" DataKeyNames="ID">
                                        <Columns>

                                            <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" Visible="false">
                                                <ItemTemplate>
                                                    <%#Container.DataItemIndex+1 %>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Name" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%">
                                                        <asp:LinkButton ID="LinkButton1" runat="server" ToolTip="Double Click to view Sub-Folders" data-toggle="tooltip"
                                                            CssClass="foldertogo" CommandName="Goto_Subfolder"
                                                            CommandArgument='<%# Eval("ID") %>'>
                                                            <img src='<%# ResolveUrl("/Areas/BM_Management/img/Folder_Grey.png")%>' class="folder-size" alt="Edit Details" title="Edit Details" />
                                                        </asp:LinkButton>
                                                        <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Name") %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Owner">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%">
                                                        <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" CssClass="ownerdoc" Text='<%# ShowUserName(Convert.ToString(Eval("Createdby"))) %>' ></asp:Label> <%--ToolTip='<%# ShowUserName(Convert.ToString(Eval("Createdby"))) %>'--%>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Last Modified">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%">
                                                        <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("UpdatedOn") %>'></asp:Label>  <%--ToolTip='<%# Eval("UpdatedOn") %>'--%>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField ItemStyle-Width="1%">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="LnkShare" runat="server" CssClass="sharef" Style="float: left; padding: 0px 2px 0px 0px; display: none;" ImageUrl="~/Images/reset_password_new.png" CommandArgument='<%# Eval("ID") +","+ Eval("Type") %>' CommandName="ShareFile" data-toggle="tooltip" data-placement="bottom" ToolTip="Click to Share File"></asp:ImageButton>
                                                    <asp:ImageButton ID="lnkDeleteUpload" runat="server"
                                                        Visible="true" ImageUrl="~/Images/delete_icon_new.png" Style="display: none;" CssClass="deletef"
                                                        data-toggle="tooltip" data-placement="bottom" CommandName="DeleteDoc"
                                                        ToolTip="Delete" CommandArgument='<%# Eval("ID") +","+ Eval("Type") %>'></asp:ImageButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <RowStyle CssClass="clsROWgrid" />
                                        <HeaderStyle CssClass="clsheadergrid" />
                                        <PagerSettings Visible="false" />
                                        <PagerTemplate>
                                        </PagerTemplate>
                                        <EmptyDataTemplate>
                                            No Record Found
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                </div>

                                <div class="col-md-12 colpadding0">
                                    <div class="col-md-2 colpadding0">
                                        <div class="col-md-2 colpadding0" style="margin-right: 10px;">
                                            <p style="color: #999; margin-top: 5px; margin-right: 5px;">Show </p>
                                        </div>
                                        <div class="col-md-6 colpadding0">
                                            <asp:DropDownListChosen runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px; float: left"
                                                AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" AllowSingleDeselect="false">
                                                <asp:ListItem Text="5" Selected="True" />
                                                <asp:ListItem Text="10" />
                                                <asp:ListItem Text="20" />
                                                <asp:ListItem Text="50" />
                                            </asp:DropDownListChosen>
                                        </div>
                                    </div>
                                    <div class="col-md-8 colpadding0"></div>
                                    <div class="col-md-2 colpadding0">
                                        <div class="col-md-6"></div>
                                       
                                        <div class="col-md-3 colpadding0 table-paging-text" style="width: 25%;">
                                            <p>
                                                Page                                       
                                            </p>
                                        </div>
                                        <div class="col-md-3 colpadding0">
                                            <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true" AllowSingleDeselect="false"
                                                class="form-control m-bot15" Width="100%" Height="30px" OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">
                                            </asp:DropDownListChosen>
                                        </div>
                                    </div>
                                </div>

                                <div class=" col-md-12 colpadding0 entrycount" style="margin-top: 5px;">
                                    <div class="col-md-6 colpadding0" style="float: right">
                                        <div class="table-paging" style="margin-bottom: 10px;">
                                            <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                        </div>
                                    </div>
                                    <div class="col-md-6 colpadding0 entrycount" style="margin-top: 5px;">
                                        <div class="col-md-5 colpadding0">
                                            <div class="table-Selecteddownload">
                                                <div class="table-Selecteddownload-text">
                                                    <p>
                                                        <asp:Label runat="server" ID="lblTotalSelected" Text="" Style="color: #999; margin-right: 10px;"></asp:Label>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>

            <div class="modal fade" id="divOpenNewFolderPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog p5" style="width: 40%;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label class="modal-header-custom">
                                New Folder</label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                            <asp:UpdatePanel ID="upMailDocument" runat="server">
                                <ContentTemplate>
                                    <div class="row form-group ">
                                        <div class="col-md-12">
                                            <asp:ValidationSummary ID="FolderValidation" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                                ValidationGroup="vsFolderDocumentValidationGroup" />
                                            <asp:CustomValidator ID="cvMailDocument" runat="server" EnableClientScript="False"
                                                ValidationGroup="vsFolderDocumentValidationGroup" Display="None" />
                                        </div>
                                    </div>
                                    <div class="row form-group ">
                                        <div class="col-md-12">
                                            <asp:TextBox runat="server" ID="txtFolderName" CssClass="form-control" />
                                            <asp:RequiredFieldValidator ID="Foldermsg" ErrorMessage="Required Name"
                                                ControlToValidate="txtFolderName" runat="server" ValidationGroup="vsFolderDocumentValidationGroup" Display="None" />
                                        </div>
                                    </div>
                                    <div class="row form-group ">
                                        <div class="col-md-12 text-center">
                                            <asp:Button Text="Create" runat="server" ID="btnCreateFolder" CssClass="btn btn-primary"
                                                OnClick="btnCreate_Click" ValidationGroup="vsFolderDocumentValidationGroup"></asp:Button>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="divOpenPermissionPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog p5" style="width: 50%;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label class="modal-header-custom">
                                Share With Others
                            </label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                            <div class="row">
                                <div class="form-group required col-md-12">
                                    <asp:ValidationSummary ID="vsPermission" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                        ValidationGroup="vsPermissionValidationGroup" />
                                    <asp:CustomValidator ID="vsPermissionSet" runat="server" EnableClientScript="False"
                                        ValidationGroup="vsPermissionValidationGroup" Display="None" />
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group required col-md-12" style="display: none;">
                                    <label for="ddlPermission1" class="color-lable">Permission</label>
                                    <asp:DropDownListChosen runat="server" ID="ddlPermission1" DataPlaceHolder="Select Permission"
                                        AllowSingleDeselect="false" DisableSearchThreshold="5" CssClass="form-control" Width="100%">
                                        <asp:ListItem Text="View" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="Write" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="View & Write" Value="3"></asp:ListItem>
                                    </asp:DropDownListChosen>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group required col-md-12">
                                    <label for="lstBoxUser" class="color-lable">People</label>
                                    <asp:ListBox ID="lstBoxUser" CssClass="form-control" runat="server" SelectionMode="Multiple"></asp:ListBox>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group required col-md-12">
                                    <asp:Button Text="Done" ID="btnPermission" runat="server" OnClick="btnPermission_Click" CssClass="btn btn-primary"></asp:Button>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group required col-md-12">
                                    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                        <ContentTemplate>
                                            <asp:Repeater ID="myRepeater" runat="server" OnItemCommand="myRepeater_ItemCommand">
                                                <HeaderTemplate>
                                                    <div style="font-weight: bold; color: black">Share with Users</div>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <div class="row">
                                                        <div class="form-group required col-md-12" style="padding-left: 0px;">
                                                            <div class="form-group required col-md-3" style="padding-left: 0px;">
                                                                <asp:Label ID="myLabel" runat="server" Style="color: black; float: left" Text='<%# Eval("UserName") %>' />
                                                            </div>
                                                            <div class="form-group required col-md-1" style="padding-left: 0px;">
                                                                <asp:ImageButton ID="LnkDeletShare" runat="server" CommandName="RemoveShare" CommandArgument='<%# Eval("UserPermissionFileId") +","+ Eval("UserId") +","+ Eval("FileType") %>' Style="float: left; padding: 0px 2px 0px 0px;" ImageUrl="~/Images/delete_icon_new.png" data-toggle="tooltip" data-placement="bottom" ToolTip="Click to UnShare File"></asp:ImageButton>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="myRepeater" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
