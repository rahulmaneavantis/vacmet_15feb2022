﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EntityBranchDetails.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.EntityBranchDetails" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <!-- Bootstrap CSS -->
    <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap theme -->
    <link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <!-- font icon -->
    <link href="~/NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <!--external css-->
    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/contract_custom_style.css" rel="stylesheet" />
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="/Newjs/jquery.js"></script>
    <script type="text/javascript" src="/Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="/Newjs/bootstrap.min.js"></script>

    <style>
        .form-control {
            margin-bottom: 8px;
            height: 31px;
            font-family: sans-serif;
        }

        .m-10 {
            /*margin-left: 10px;*/
            padding-left: 24px;
        }

        #loaderImage {
            position: fixed;
            margin-left: 40%;
            margin-top: 25%;
            width: 50px;
            height: 50px;
            background-image: url("../NewCSS/Default/loading-image.gif");
        }
    </style>

    <script>
        $(document).ready(function () {
            $("body").tooltip({ selector: '[data-toggle=tooltip]' });
            initializeDatePicker();

            $(window.parent.document).find("#BodyContent_updateProgress").hide();

            $('#btnSave').click(function () {

            });

            if ($(window.parent.document).find("#divBranch").children().first().hasClass("k-loading-mask"))
                $(window.parent.document).find("#divBranch").children().first().hide();

            if ($(window.parent.document).find("#IframeMyCompliances").children().first().hasClass("k-loading-mask"))
                $(window.parent.document).find("#divCompliancesShow").children().first().hide();
        });       

        function chkDecimal() {
            var val = $("#txtBonusP").val();
            var checkForPercentage = /^\d{1,2}\.\d{1,2}$|^\d{1,3}$/;
            if (!checkForPercentage.test(val)) {
                alert("Enter Bonus Percentage only in Decimal or Integer");
                $("#txtBonusP").val("");
            }
        }

        function myShow() {
            $(window.parent.document).find("#BodyContent_updateProgress").show();
        }

        function initializeDatePicker() {

        }

    </script>

    <script type="text/javascript">
        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }

        function Hideshow() {
            $('#btnSave').addClass('disabled');
        }

        function DisableBtn(e) {
            var ValidationError = $('#vsEntityBranchPage ul').text();

            if (ValidationError.length == 0) {
                $("#loader").show();
                $('#btnSave').addClass('disabled');
            }
        }

        function hideLoader() {
            debugger;
            $("#loader").hide();
            $('#btnSave').removeClass('disabled');
        }

        function showLoader() {
            debugger;
            $("#loader").show();
            $('#btnSave').addClass('disabled');
        }
    </script>
</head>
<body style="background: white;">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div id="loader" style="display: none;">
            <div id="loaderImage">
            </div>
        </div>

        <asp:UpdatePanel ID="upCustomerBranches" runat="server" UpdateMode="Conditional" OnLoad="upCustomerBranches_Load">
            <ContentTemplate>

                <div style="margin: 5px">
                    <div class="row form-group">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <asp:ValidationSummary ID="vsEntityBranchPage" runat="server" CssClass="alert alert-block alert-danger alert-dismissable" ValidationGroup="CustomerBranchValidationGroup" />
                            <asp:CustomValidator ID="cvEntityBranchPage" runat="server" EnableClientScript="False" ValidationGroup="CustomerBranchValidationGroup" Display="None" />
                        </div>
                    </div>

                    <div id="divParent" runat="server" class="row form-group">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <label class="control-label">Entity/Company- </label>                            
                            <asp:Label id="parentName" runat="server" class="control-label"></asp:Label>
                        </div>                        
                    </div>

                    <div class="row form-group">
                        <div class="col-xs-4 col-sm-4 col-md-4 required">
                            <label class="control-label">Name<span style="color: red; margin-left: 7px; font-size: 13px;">*</span></label>
                            <asp:TextBox runat="server" ID="tbxName" MaxLength="500" autoComplete="nope" CssClass="form-control" />
                            <asp:RequiredFieldValidator ErrorMessage="Required Branch/Location Name, can not be empty" ControlToValidate="tbxName"
                                runat="server" ValidationGroup="CustomerBranchValidationGroup" Display="None" />
                            <asp:RegularExpressionValidator Display="None" runat="server" ValidationGroup="CustomerBranchValidationGroup"
                                ErrorMessage="Please enter a valid Branch/Location Name" ControlToValidate="tbxName" ValidationExpression="[a-zA-Z0-9\s()+\/@&#.,-_]*$">
                            </asp:RegularExpressionValidator>
                        </div>

                        <div class="col-xs-2 col-sm-2 col-md-2">
                            <label class="control-label">Status<span style="color: red; margin-left: 7px; font-size: 13px;">*</span></label>
                            <asp:DropDownListChosen runat="server" ID="ddlCustomerStatus" CssClass="form-control" Width="100%" AllowSingleDeselect="false" />
                        </div>

                        <div class="col-xs-2 col-sm-2 col-md-2">
                            <label class="control-label">Branch Type<span style="color: red; margin-left: 7px; font-size: 13px;">*</span></label>
                            <asp:DropDownListChosen runat="server" ID="ddlType" Width="100%" AllowSingleDeselect="false"
                                CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlType_SelectedIndexChanged" />
                            <asp:CompareValidator ErrorMessage="Please Select Branch Type" ControlToValidate="ddlType"
                                runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="CustomerBranchValidationGroup" Display="None" />
                        </div>

                         <div id="divOtherType" runat="server" class="col-xs-4 col-sm-4 col-md-4">
                            <label class="control-label">Specify(Other Type)<span style="color: red; margin-left: 7px; font-size: 13px;">*</span></label>
                            <asp:TextBox runat="server" ID="txtOtherType" MaxLength="500" autoComplete="nope" CssClass="form-control" />
                            <asp:RequiredFieldValidator ErrorMessage="Required Other Type, can not be empty" ControlToValidate="txtOtherType"
                                runat="server" ValidationGroup="CustomerBranchValidationGroup" Display="None" />
                        </div>
                    </div>

                    <div class="row" runat="server" id="divCompanyType" visible="false">
                        <div class="col-xs-4 col-sm-4 col-md-4">
                            <label id="lblType" class="control-label">Type<span style="color: red; margin-left: 7px; font-size: 13px;">*</span></label>
                            <asp:DropDownListChosen runat="server" ID="ddlCompanyType" Width="100%" AllowSingleDeselect="false" CssClass="form-control" />
                        </div>
                    </div>

                    <div id="divLegalEntityType" runat="server" style="margin-bottom: 7px" visible="false">
                        <label id="lblLegalEntityType" class="control-label">Legal Entity Type<span style="color: red; margin-left: 7px; font-size: 13px;">&nbsp;</span></label>
                        <asp:DropDownList runat="server" ID="ddlLegalEntityType" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                            CssClass="txtbox" />
                    </div>

                    <div runat="server" id="divLegalRelationship" style="margin-bottom: 7px" visible="false">
                        <label class="control-label">Legal Relationship<span style="color: red; margin-left: 7px; font-size: 13px;">&nbsp;</span></label>
                        <asp:DropDownList runat="server" ID="ddlLegalRelationShip" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                            CssClass="txtbox" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="Please select legal relationship."
                            ControlToValidate="ddlLegalRelationShip" runat="server" ValidationGroup="CustomerBranchValidationGroup"
                            Display="None" />
                    </div>

                    <div id="divIndustry" runat="server" class="hidden">
                        <label class="control-label">Industry<span style="color: red; margin-left: 7px; font-size: 13px;">*</span></label>
                        <asp:TextBox runat="server" ID="txtIndustry" CssClass="form-control" />
                        <div style="position: absolute; z-index: 50; overflow-y: auto; background: white; border: 1px solid gray; height: 200px;" id="dvIndustry">
                            <asp:Repeater ID="rptIndustry" runat="server">
                                <HeaderTemplate>
                                    <table class="detailstable FadeOutOnEdit" id="RepeaterTable">
                                        <tr>
                                            <td style="width: 100px;">
                                                <asp:CheckBox ID="IndustrySelectAll" Text="Select All" runat="server" onclick="checkAll(this)" /></td>
                                            <td style="width: 282px;">
                                                <asp:Button runat="server" ID="btnRepeater" Text="Ok" Style="float: left" /></td>
                                            <%--OnClick="btnRefresh_Click" --%>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td style="width: 20px;">
                                            <asp:CheckBox ID="chkIndustry" runat="server" onclick="UncheckHeader();" /></td>
                                        <td style="width: 200px;">
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 250px; padding-bottom: 5px;">
                                                <asp:Label ID="lblIndustryID" runat="server" Visible="false" Text='<%# Eval("ID")%>' ToolTip='<%# Eval("ID") %>'></asp:Label>
                                                <asp:Label ID="lblIndustryName" runat="server" Text='<%# Eval("Name")%>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                            </div>
                                        </td>

                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>

                            </asp:Repeater>
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-xs-4 col-sm-4 col-md-4">
                            <label class="control-label">Contact Person<span style="color: red; margin-left: 7px; font-size: 13px;">*</span></label>
                            <asp:TextBox runat="server" ID="tbxContactPerson" autoComplete="off" CssClass="form-control"
                                MaxLength="200" />
                            <asp:RequiredFieldValidator ErrorMessage="Required Contact Person, can not be empty"
                                ControlToValidate="tbxContactPerson" runat="server" ValidationGroup="CustomerBranchValidationGroup"
                                Display="None" />
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" Display="None" runat="server"
                                ValidationGroup="CustomerBranchValidationGroup" ErrorMessage="Please enter a valid Contact Person Name (i.e. it should only contains alphabets)"
                                ControlToValidate="tbxContactPerson" ValidationExpression="^[a-zA-Z_ .-]*$"></asp:RegularExpressionValidator>
                        </div>

                        <div class="col-xs-4 col-sm-4 col-md-4">
                            <label class="control-label">Contact Number<span style="color: red; margin-left: 7px; font-size: 13px;">*</span></label>
                            <asp:TextBox runat="server" ID="tbxLandline" CssClass="form-control" autoComplete="off" MaxLength="10" />
                            <asp:RequiredFieldValidator ErrorMessage="Required Contact Number, can not be empty"
                                ControlToValidate="tbxLandline" runat="server" ValidationGroup="CustomerBranchValidationGroup"
                                Display="None" />
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" Display="None"
                                ValidationGroup="CustomerBranchValidationGroup" ErrorMessage="Please enter a valid Contact Number(i.e. it should only contains numbers)"
                                ControlToValidate="tbxLandline" ValidationExpression="(\+\d{1,3}[- ]?)?\d{10}$"></asp:RegularExpressionValidator>
                        </div>

                        <div class="col-xs-4 col-sm-4 col-md-4 d-none">
                            <label class="control-label">Mobile No.</label>
                            <asp:TextBox runat="server" ID="tbxMobile" CssClass="form-control" MaxLength="15" autoComplete="off" />
                        </div>

                        <div class="col-xs-4 col-sm-4 col-md-4">
                            <label class="control-label">Email<span style="color: red; margin-left: 7px; font-size: 13px;">*</span></label>
                            <asp:TextBox runat="server" ID="tbxEmail" CssClass="form-control" MaxLength="200" autoComplete="off" />
                            <asp:RequiredFieldValidator ErrorMessage="Required Email, can not be empty" ControlToValidate="tbxEmail"
                                runat="server" ValidationGroup="CustomerBranchValidationGroup" Display="None" />
                            <asp:RegularExpressionValidator Display="None" runat="server" ValidationGroup="CustomerBranchValidationGroup"
                                ErrorMessage="Please enter a valid email" ControlToValidate="tbxEmail" ValidationExpression="^([\w-_]+\.)*[\w-_]+@([\w-_]+\.)*[\w-_]+\.[\w-_]+$"></asp:RegularExpressionValidator>
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-xs-4 col-sm-4 col-md-4">
                            <label class="control-label">Address Line 1<span style="color: red; margin-left: 7px; font-size: 13px;">*</span></label>
                            <asp:TextBox runat="server" ID="tbxAddressLine1" CssClass="form-control" autocomplete="off" MaxLength="150" />
                            <asp:RequiredFieldValidator ErrorMessage="Required Address, can not be empty" ControlToValidate="tbxAddressLine1"
                                runat="server" ValidationGroup="CustomerBranchValidationGroup" Display="None" />
                        </div>

                        <div class="col-xs-4 col-sm-4 col-md-4">
                            <label class="control-label">Address Line 2<span style="color: red; margin-left: 7px; font-size: 13px;">&nbsp;</span></label>
                            <asp:TextBox runat="server" ID="tbxAddressLine2" CssClass="form-control" autoComplete="off" MaxLength="150" />
                        </div>

                        <div id="divState" runat="server" class="col-xs-4 col-sm-4 col-md-4">
                            <label class="control-label">State<span style="color: red; margin-left: 7px; font-size: 13px;">*</span></label>
                            <asp:DropDownListChosen runat="server" ID="ddlState" Width="100%" AllowSingleDeselect="false"
                                CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlState_SelectedIndexChanged" />
                            <asp:CompareValidator ErrorMessage="Please Select State" ControlToValidate="ddlState"
                                runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="CustomerBranchValidationGroup"
                                Display="None" />
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-xs-4 col-sm-4 col-md-4">
                            <label class="control-label">City<span style="color: red; margin-left: 7px; font-size: 13px;">*</span></label>
                            <asp:DropDownListChosen runat="server" ID="ddlCity" Width="100%" AllowSingleDeselect="false"
                                CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlCity_SelectedIndexChanged" />
                            <asp:CompareValidator ErrorMessage="Please select City." ControlToValidate="ddlCity"
                                runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="CustomerBranchValidationGroup"
                                Display="None" />
                        </div>

                        <div id="divOther" runat="server" class="col-xs-4 col-sm-4 col-md-4">
                            <label class="control-label">Other(City Name)<span style="color: red; margin-left: 7px; font-size: 13px;">*</span></label>
                            <asp:TextBox runat="server" ID="tbxOther" MaxLength="50" autoComplete="nope" CssClass="form-control" />
                            <asp:RequiredFieldValidator ErrorMessage="Required Other(City Name), can not be empty" ControlToValidate="tbxOther"
                                runat="server" ValidationGroup="CustomerBranchValidationGroup" Display="None" />
                        </div>

                        <div class="col-xs-4 col-sm-4 col-md-4">
                            <label class="control-label">Pin Code<span style="color: red; margin-left: 7px; font-size: 13px;">*</span></label>
                            <asp:TextBox runat="server" ID="tbxPinCode" MaxLength="6" autoComplete="nope" CssClass="form-control" />
                        </div>
                    </div>

                    <div class="row col-xs-12 col-sm-12 col-md-12 colpadding0 text-center">
                        <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="btn btn-primary" ValidationGroup="CustomerBranchValidationGroup" />
                    </div>

                    <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 10px;">
                        <p style="color: red;"><strong>Note:</strong> (*) fields are compulsory</p>
                    </div>
                </div>

            </ContentTemplate>
        </asp:UpdatePanel>

    </form>
</body>
</html>
