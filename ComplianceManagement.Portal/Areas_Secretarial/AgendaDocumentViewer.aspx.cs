﻿using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Kendo.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BM_ManegmentServices.Services.Meetings;
using GleamTech.DocumentUltimate.Web;
using BM_ManegmentServices.Services.DocumentManagenemt;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas_Secretarial
{
    public partial class AgendaDocumentViewer : System.Web.UI.Page
    {
        public IDocument_Service objIDocument_Service { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["meetingId"] != null)
                {
                    long meetingId = Convert.ToInt64(Request.QueryString["meetingId"]);
                    long assignmentId = 0;
                    if (Request.QueryString["assignmentId"] != null)
                    {
                        if (!long.TryParse(Request.QueryString["assignmentId"], out assignmentId))
                        {
                            assignmentId = 0;
                        }
                    }
                    if (meetingId > 0)
                    {
                        loadDocument(meetingId, assignmentId);
                    }
                }
            }
        }

        protected void loadDocument(long meetingId, long assignmentId)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int userId = Convert.ToInt32(AuthenticationHelper.UserID);

            objIDocument_Service = new Document_Service(new AgendaMinutesReviewService(), new DocumentSetting_Service(), new FileData_Service());
            //var bytes = objIDocument_Service.GenerateAgendaDocument(meetingId, assignmentId, userId, customerId);
            //var guid = Guid.NewGuid();
            //DocumentInfo di = new DocumentInfo(guid.ToString(), "Agenda_" + guid.ToString() + ".docx", null);
            //doccontrol.DocumentSource = new DocumentSource(di, bytes);

            var fileName = objIDocument_Service.GenerateAgendaDocument1(meetingId, assignmentId, userId, customerId, false);
            doccontrol.Document = fileName;
        }
    }
}