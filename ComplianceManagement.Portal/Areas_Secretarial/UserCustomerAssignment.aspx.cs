﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business;
using System.Globalization;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using BM_ManegmentServices.Data;
using BM_ManegmentServices.Services;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas_Secretarial
{
    public partial class UserCustomerAssignment : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            vsUserCustomerMapping.CssClass = "alert alert-danger";
            if (!IsPostBack)
            {
                BindCustomers();

                long recordID = 0;
                if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
                {
                    recordID = Convert.ToInt64(Request.QueryString["ID"]);
                }

                if (recordID != 0)
                {
                    EditRecord(recordID);
                }
                else
                {
                    ddlCustomerPopup_SelectedIndexChanged(null, null);
                }
            }
        }

        private void BindCustomers()
        {
            try
            {
                int customerID = -1;
                int serviceProviderID = -1;
                int distributorID = -1;

                if (AuthenticationHelper.Role == "CADMN")
                {
                    //customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "SPADM")
                {
                    serviceProviderID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "DADMN")
                {
                    distributorID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }

                var customerList = SecretarialManagement.GetAll_MyCustomers(customerID, serviceProviderID, distributorID, false);
               
                #region Pop-Up DropDown

                ddlCustomerPopup.DataTextField = "Name";
                ddlCustomerPopup.DataValueField = "ID";

                ddlCustomerPopup.DataSource = customerList;
                ddlCustomerPopup.DataBind();

                #endregion
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUCMPopup.IsValid = false;
                cvUCMPopup.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void ddlCustomerPopup_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                lstBoxUser.ClearSelection();
                ddlMgrPopup.ClearSelection();

                BindUsers_Popup();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUCMPopup.IsValid = false;
                cvUCMPopup.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        private void BindUsers_Popup()
        {
            try
            {
                int customerID = -1;
                int distID = -1;
                int serviceProviderID = -1;

                if (!string.IsNullOrEmpty(ddlCustomerPopup.SelectedValue))
                {
                    customerID = Convert.ToInt32(ddlCustomerPopup.SelectedValue);
                }

                var lstUsers = SecretarialManagement.GetAll_Users(customerID, serviceProviderID, distID);

                BindUserToDropDown(lstUsers, lstBoxUser);
                //BindUserToDropDown(lstUsers, ddlUserPopup);
                BindUserToDropDown(lstUsers, ddlMgrPopup, "CSMGR");
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUCMPopup.IsValid = false;
                cvUCMPopup.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        private void BindUserToDropDown(List<BM_SP_GetAllUser_ServiceProviderDistributor_Result> lstUsers, DropDownList ddltoBind, string roleCode = "")
        {
            try
            {
                if (lstUsers.Count > 0 && !string.IsNullOrEmpty(roleCode))
                    lstUsers = lstUsers.Where(row => row.RoleCode == roleCode).ToList();

                List<object> users = new List<object>();
                users = (from row in lstUsers
                         select new { ID = row.ID, Name = row.FirstName + " " + row.LastName, RoleCode = row.RoleCode }).Distinct().OrderBy(row => row.Name).ToList<object>();

                ddltoBind.Items.Clear();

                ddltoBind.DataTextField = "Name";
                ddltoBind.DataValueField = "ID";

                users.Insert(0, new { ID = -1, Name = "Select" });

                ddltoBind.DataSource = users;
                ddltoBind.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUCMPopup.IsValid = false;
                cvUCMPopup.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        private void BindUserToDropDown(List<BM_SP_GetAllUser_ServiceProviderDistributor_Result> lstUsers, ListBox lstBoxtoBind, string roleCode = "")
        {
            try
            {
                if (lstUsers.Count > 0 && !string.IsNullOrEmpty(roleCode))
                    lstUsers = lstUsers.Where(row => row.RoleCode == roleCode).ToList();

                List<object> users = new List<object>();
                users = (from row in lstUsers
                         select new { ID = row.ID, Name = row.FirstName + " " + row.LastName, RoleCode = row.RoleCode }).Distinct().OrderBy(row => row.Name).ToList<object>();

                lstBoxtoBind.Items.Clear();

                lstBoxtoBind.DataTextField = "Name";
                lstBoxtoBind.DataValueField = "ID";

                users.Insert(0, new { ID = -1, Name = "Select" });

                lstBoxtoBind.DataSource = users;
                lstBoxtoBind.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUCMPopup.IsValid = false;
                cvUCMPopup.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                bool saveSuccess = false;

                if (!string.IsNullOrEmpty(ddlCustomerPopup.SelectedValue) && ddlCustomerPopup.SelectedValue != "-1")
                {                    
                    int selectedUsersCount = 0;
                    foreach (ListItem eachUser in lstBoxUser.Items)
                    {
                        if (eachUser.Selected)
                            selectedUsersCount++;
                    }

                    int customerID = Convert.ToInt32(ddlCustomerPopup.SelectedValue);
                                        
                    if (selectedUsersCount > 0)                       
                    {
                        List<UserCustomerMapping> lstUserCustmerMapping = new List<UserCustomerMapping>();

                        if (lstBoxUser.Items.Count > 0)
                        {
                            foreach (ListItem eachUser in lstBoxUser.Items)
                            {
                                if (eachUser.Selected)
                                {
                                    if (Convert.ToInt32(eachUser.Value) != 0)
                                    {
                                        UserCustomerMapping objUserCustMapping = new UserCustomerMapping()
                                        {
                                            ProductID = 8,
                                            UserID = Convert.ToInt32(eachUser.Value),
                                            CustomerID = customerID,                                          
                                            IsActive = true
                                        };

                                        if (!string.IsNullOrEmpty(ddlMgrPopup.SelectedValue) && ddlMgrPopup.SelectedValue != "-1")
                                            objUserCustMapping.MgrID = Convert.ToInt32(ddlMgrPopup.SelectedValue);

                                        lstUserCustmerMapping.Add(objUserCustMapping);
                                    }
                                }
                            }
                        }

                        if (lstUserCustmerMapping.Count > 0)
                        {
                            saveSuccess = UserCustomerMappingManagement.DeActivate_UserCustomerMapping_Multiple(customerID, 8);

                            if (saveSuccess)
                                saveSuccess = UserCustomerMappingManagement.CreateUpdate_UserCustomerMapping_Multiple(lstUserCustmerMapping);
                        }
                        
                        if (saveSuccess)
                        {
                            lstBoxUser.ClearSelection();
                            ddlCustomerPopup.ClearSelection();
                            ddlMgrPopup.ClearSelection();

                            cvUCMPopup.IsValid = false;
                            cvUCMPopup.ErrorMessage = "Details Save Successfully";
                            vsUserCustomerMapping.CssClass = "alert alert-success";
                        }
                        else
                        {
                            cvUCMPopup.IsValid = false;
                            cvUCMPopup.ErrorMessage = "Something went wrong, please try again";
                        }                       
                    }
                    else
                    {
                        cvUCMPopup.IsValid = false;
                        cvUCMPopup.ErrorMessage = "Select One or more User to Assign";
                    }
                }
                else
                {
                    cvUCMPopup.IsValid = false;
                    cvUCMPopup.ErrorMessage = "Select Customer to Assign";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUCMPopup.IsValid = false;
                cvUCMPopup.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        private void EditRecord(long recordID)
        {
            var objRecord = UserCustomerMappingManagement.GetRecord_UserCustomerMapping(recordID);

            if (objRecord != null)
            {
                if (ddlCustomerPopup.Items.FindByValue(objRecord.CustomerID.ToString()) != null)
                {
                    ddlCustomerPopup.ClearSelection();
                    ddlCustomerPopup.Items.FindByValue(objRecord.CustomerID.ToString()).Selected = true;

                    ddlCustomerPopup_SelectedIndexChanged(null, null);
                }

                #region Assigned User

                int customerID = Convert.ToInt32(ddlCustomerPopup.SelectedValue);

                var lstUsers = UserCustomerMappingManagement.GetRecord_Users(customerID, 8);

                if (lstBoxUser.Items.Count > 0)
                {
                    lstBoxUser.ClearSelection();
                }

                if (lstUsers.Count > 0)
                {
                    foreach (var eachUserID in lstUsers)
                    {
                        if (lstBoxUser.Items.FindByValue(eachUserID.ToString()) != null)
                            lstBoxUser.Items.FindByValue(eachUserID.ToString()).Selected = true;
                    }
                }

                //if (objRecord.UserID != null)
                //{
                //    if (lstBoxUser.Items.FindByValue(objRecord.UserID.ToString()) != null)
                //    {
                //        lstBoxUser.Items.FindByValue(objRecord.UserID.ToString()).Selected = true;
                //    }
                //}

                #endregion

                if (objRecord.MgrID != null)
                {
                    if (ddlMgrPopup.Items.FindByValue(objRecord.MgrID.ToString()) != null)
                    {
                        ddlMgrPopup.ClearSelection();
                        ddlMgrPopup.Items.FindByValue(objRecord.MgrID.ToString()).Selected = true;
                    }
                }

                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenModal", "$(\"#divAssignEntitiesDialogpopup\").modal('show');", true);
            }
        }
    }
}