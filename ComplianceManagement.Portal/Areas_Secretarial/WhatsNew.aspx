﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WhatsNew.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.WhatsNew" %>

<!DOCTYPE html>
<%@ Register TagPrefix="GleamTech" Namespace="GleamTech.DocumentUltimate.Web" Assembly="GleamTech.DocumentUltimate" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
   
</head>
<body>
    <form id="ddlwhatsnew" runat="server">
    <div>
        <asp:Label ID="lblWhatsnew" runat="server"></asp:Label>
        <GleamTech:DocumentViewer runat="server" Width="100%" ID="docwhatsnew" ToolbarVisible="false" FullViewport="false" searchControl="false" SidePaneVisible="false" Height="500px"
            DownloadAsPdfEnabled="false" DisableHeaderIncludes="false" />
    </div>   
    </form>
</body>
</html>
