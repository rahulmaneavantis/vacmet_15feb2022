﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Globalization;
using System.Configuration;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using System.Text;
using Newtonsoft.Json;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using System.IO;
using BM_ManegmentServices.Services;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas_Secretarial
{
    public partial class AddEditCustomer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ValidationSummary1.CssClass = "alert alert-danger";

            if (!IsPostBack)
            {
                ViewState["CorpID"] = null;

                BindCustomerStatus();
                BindLocationType();
                BindServiceProvider();
                BindDistributors();
                
                int customerID = 0;
                if (!string.IsNullOrEmpty(Request.QueryString["CustomerID"]))
                {
                    customerID = Convert.ToInt32(Request.QueryString["CustomerID"]);
                }

                if (customerID != 0)
                {
                    EditCustomerInformation(customerID);
                }
                else if (customerID == 0)
                {
                    ViewState["Mode"] = 0;
                }

                //Allow Access to Create Sub-Distributor
                int loggedInUserCustomerID = 0;

                if (AuthenticationHelper.Role == "IMPT")
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["DistID"]))
                    {
                        loggedInUserCustomerID = Convert.ToInt32(Request.QueryString["DistID"]);
                    }
                    //loggedInUserCustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else
                    loggedInUserCustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                var custRecord = CustomerManagement.GetByID(loggedInUserCustomerID);

                if (custRecord != null)
                {
                    //FixedPrice or Payment
                    if (ViewState["Mode"] != null)
                    {
                        if ((int)ViewState["Mode"] == 0)
                        {
                            bool isPaymentModeCust = false;

                            if (isPaymentModeCust)
                            {
                                if (rblPayment.Items.FindByValue("1") != null)
                                {
                                    rblPayment.ClearSelection();
                                    rblPayment.Items.FindByValue("1").Selected = true;
                                }
                            }
                        }
                    }
                }

                if (loggedInUserCustomerID == customerID && AuthenticationHelper.Role != "IMPT")
                {
                    rblSubDistributor.Enabled = false;
                    divSubDist.Visible = false;
                }
                else
                {
                    rblSubDistributor.Enabled = true;
                    divSubDist.Visible = true;
                }

                rblCustomerDistributor_CheckedChanged(null, null);

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", "initializeDatePicker(null);", true);
            }
        }
                
        public void BindLocationType()
        {
            try
            {
                ddlLocationType.DataTextField = "Name";
                ddlLocationType.DataValueField = "ID";
                ddlLocationType.Items.Clear();
                ddlLocationType.DataSource = ProcessManagement.FillLocationType();
                ddlLocationType.DataBind();
                ddlLocationType.Items.Insert(0, new ListItem("< All >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void BindServiceProvider()
        {
            try
            {
                int serviceProviderID = 95;
                if (AuthenticationHelper.Role == "SPADM")
                {
                    serviceProviderID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }

                ddlSPName.DataTextField = "Name";
                ddlSPName.DataValueField = "ID";
                ddlSPName.Items.Clear();
                ddlSPName.DataSource = CustomerManagement.FillServiceProvider(serviceProviderID);
                ddlSPName.DataBind();

                if (ddlSPName.Items.FindByValue(serviceProviderID.ToString()) != null)
                    ddlSPName.Items.FindByValue(serviceProviderID.ToString()).Selected = true;
                else
                    ddlSPName.Items.Insert(0, new ListItem("Select", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void BindDistributors()
        {
            try
            {
                int distbutorID = 95; //95=Avantis
                if (AuthenticationHelper.Role == "SPADM")
                {
                    distbutorID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "DADMN")
                {
                    distbutorID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "CADMN")
                {
                    distbutorID = RLCS_Master_Management.Get_DistributorID(Convert.ToInt32(AuthenticationHelper.CustomerID));
                }
                else if (AuthenticationHelper.Role == "IMPT")
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["DistID"]))
                    {
                        distbutorID = Convert.ToInt32(Request.QueryString["DistID"]);
                    }
                }

                ddlDistributor.DataTextField = "Name";
                ddlDistributor.DataValueField = "ID";
                ddlDistributor.Items.Clear();

                ddlDistributor.DataSource = CustomerManagement.Fill_ServiceProviders_Distributors(1, -1, distbutorID);
                //ddlDistributor.DataSource = CustomerManagement.FillServiceProvider(distbutorID);
                ddlDistributor.DataBind();

                if (ddlDistributor.Items.FindByValue(distbutorID.ToString()) != null)
                    ddlDistributor.Items.FindByValue(distbutorID.ToString()).Selected = true;
                else
                    ddlDistributor.Items.Insert(0, new ListItem("Select", "-1"));

                if (ddlDistributor.Items.Count == 1 && ddlDistributor.SelectedValue != "-1")
                    divDistributor.Visible = false;
                else
                    divDistributor.Visible = true;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void EditCustomerInformation(int customerID)
        {
            try
            {
                lblErrorMassage.Text = "";
                ViewState["Mode"] = 1;
                ViewState["CustomerID"] = customerID;

                Customer customer = CustomerManagement.GetByID(customerID);
                tbxName.Text = customer.Name;
                tbxAddress.Text = customer.Address;
                tbxBuyerName.Text = customer.BuyerName;
                tbxBuyerContactNo.Text = customer.BuyerContactNumber;
                tbxBuyerEmail.Text = customer.BuyerEmail;
                txtStartDate.Text = customer.StartDate != null ? customer.StartDate.Value.ToString("dd-MM-yyyy") : " ";
                txtEndDate.Text = customer.EndDate != null ? customer.EndDate.Value.ToString("dd-MM-yyyy") : " ";
                txtDiskSpace.Text = customer.DiskSpace;

                ImgLogo.ImageUrl = string.IsNullOrEmpty(customer.LogoPath) ? "" : customer.LogoPath;
                imgDiv.Visible = string.IsNullOrEmpty(customer.LogoPath) ? false : true;

                if (customer.LocationType != null)
                {
                    ddlLocationType.SelectedValue = Convert.ToString(customer.LocationType);
                }
                if (customer.IComplianceApplicable != null)
                {
                    if (ddlComplianceApplicable.Items.FindByValue(Convert.ToString(customer.IComplianceApplicable)) != null)
                    {
                        ddlComplianceApplicable.ClearSelection();
                        ddlComplianceApplicable.Items.FindByValue(Convert.ToString(customer.IComplianceApplicable)).Selected = true;
                    }
                }
                if (customer.TaskApplicable != null)
                {
                    if (ddlTaskApplicable.Items.FindByValue(Convert.ToString(customer.TaskApplicable)) != null)
                    {
                        ddlTaskApplicable.ClearSelection();
                        ddlTaskApplicable.Items.FindByValue(Convert.ToString(customer.TaskApplicable)).Selected = true;
                    }
                }
                if (customer.Status != null)
                {
                    if (ddlCustomerStatus.Items.FindByValue(Convert.ToString(customer.Status)) != null)
                    {
                        ddlCustomerStatus.ClearSelection();
                        ddlCustomerStatus.Items.FindByValue(Convert.ToString(customer.Status)).Selected = true;
                    }
                }

                if (customer.IsServiceProvider == true)
                {
                    chkSp.Checked = true;
                    //showHideServiceProvider();
                    customer.ServiceProviderID = null;
                }
                else
                {
                    BindServiceProvider();
                    chkSp.Checked = false;
                    //showHideServiceProvider();
                    if (ddlSPName.Items.FindByValue(Convert.ToString(customer.ServiceProviderID)) != null)
                    {
                        ddlSPName.ClearSelection();
                        ddlSPName.Items.FindByValue(Convert.ToString(customer.ServiceProviderID)).Selected = true;
                    }
                }

                rblCustomerDistributor.ClearSelection();
                ViewState["Type"] = customer.IsDistributor;
                if (customer.IsDistributor == true)
                {
                    if (rblCustomerDistributor.Items.FindByValue("D") != null)
                        rblCustomerDistributor.Items.FindByValue("D").Selected = true;
                }
                else
                {
                    if (rblCustomerDistributor.Items.FindByValue("C") != null)
                        rblCustomerDistributor.Items.FindByValue("C").Selected = true;
                }

                if (customer.CanCreateSubDist != null)
                {
                    rblSubDistributor.ClearSelection();
                    if (customer.CanCreateSubDist == true)
                    {
                        if (rblSubDistributor.Items.FindByValue("1") != null)
                            rblSubDistributor.Items.FindByValue("1").Selected = true;
                    }
                    else
                    {
                        if (rblSubDistributor.Items.FindByValue("0") != null)
                            rblSubDistributor.Items.FindByValue("0").Selected = true;
                    }
                }

                rblCustomerDistributor_CheckedChanged(null, null);

                if (customer.IsPayment != null)
                {
                    rblPayment.ClearSelection();
                    if (customer.IsPayment == true)
                    {
                        if (rblPayment.Items.FindByValue("1") != null)
                        {
                            rblPayment.ClearSelection();
                            rblPayment.Items.FindByValue("1").Selected = true;
                        }
                    }
                    else
                    {
                        if (rblPayment.Items.FindByValue("0") != null)
                        {
                            rblPayment.ClearSelection();
                            rblPayment.Items.FindByValue("0").Selected = true;
                        }
                    }
                }

                txtPAN.Text = txtTAN.Text = string.Empty;
                ddlCountry.ClearSelection();
                ddlState.ClearSelection();
                ddlCity.ClearSelection();

                upCustomers.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void AddCustomer()
        {
            try
            {
                lblErrorMassage.Text = string.Empty;
                ViewState["Mode"] = 0;
                chkSp.Checked = false;

                tbxName.Text = tbxAddress.Text = tbxBuyerName.Text = tbxBuyerContactNo.Text = tbxBuyerEmail.Text = txtStartDate.Text = txtEndDate.Text = txtDiskSpace.Text = txtPAN.Text = txtTAN.Text = string.Empty;

                ddlCustomerStatus.SelectedIndex = -1;

                ddlCountry.ClearSelection();
                ddlState.ClearSelection();
                ddlCity.ClearSelection();

                imgDiv.Visible = false;

                //ddlCountry.Items.Clear();
                //ddlState.Items.Clear();
                //ddlCity.Items.Clear();

                upCustomers.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            List<string> errorMessage = new List<string>();
            try
            {
                if (tbxName.Text.Trim().Equals(""))
                {
                    errorMessage.Add("Please Enter Name");
                }
                //if (tbxAddress.Text.Trim().Equals(""))
                //{
                //    errorMessage.Add("Please Enter Address");
                //}
                if (tbxBuyerContactNo.Text.Trim().Equals(""))
                {
                    errorMessage.Add("Please Enter Contact Number");
                }
                if (tbxBuyerEmail.Text.Trim().Equals(""))
                {
                    errorMessage.Add("Please Enter Email");
                }

                if (errorMessage.Count == 0)
                {
                    int avacom_CustomerID = 0;
                    bool saveSuccess = false;

                    Customer customer = new Customer();
                    customer.Name = tbxName.Text;
                    customer.Address = tbxAddress.Text;
                    customer.BuyerName = tbxBuyerName.Text;
                    customer.BuyerContactNumber = tbxBuyerContactNo.Text;
                    customer.BuyerEmail = tbxBuyerEmail.Text;

                    string strtdate = Request[txtStartDate.UniqueID].ToString().Trim();
                    string enddate = Request[txtEndDate.UniqueID].ToString().Trim();

                    if (strtdate != "" && enddate != "")
                    {
                        customer.StartDate = DateTime.ParseExact(strtdate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        customer.EndDate = DateTime.ParseExact(enddate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    }

                    customer.DiskSpace = txtDiskSpace.Text;

                    if (ViewState["Mode"] != null)
                    {
                        if ((int)ViewState["Mode"] == 1)
                        {
                            customer.ID = Convert.ToInt32(ViewState["CustomerID"]);
                            avacom_CustomerID = Convert.ToInt32(ViewState["CustomerID"]);
                        }
                    }

                    if (CustomerManagement.Exists(customer))
                    {
                        cvDuplicateEntry.ErrorMessage = "Customer name already exists.";
                        cvDuplicateEntry.IsValid = false;
                        return;
                    }

                    if (chkSp.Checked == false)
                    {
                        if (string.IsNullOrEmpty(ddlSPName.SelectedValue) || ddlSPName.SelectedValue == "-1") //if (ddlSPName.SelectedIndex == 0)
                        {
                            //cvDuplicateEntry.ErrorMessage = "Please Select Service Provider Name.";
                            //cvDuplicateEntry.IsValid = false;
                            //return;
                        }
                    }

                    customer.LocationType = Convert.ToInt32(ddlLocationType.SelectedValue);
                    customer.Status = Convert.ToInt32(ddlCustomerStatus.SelectedValue);
                    customer.IComplianceApplicable = Convert.ToInt32(ddlComplianceApplicable.SelectedValue);
                    customer.TaskApplicable = Convert.ToInt32(ddlTaskApplicable.SelectedValue);

                    //Service Provider
                    customer.IsServiceProvider = chkSp.Checked;
                    //if(Convert.ToInt32(ddlSPName.SelectedValue)==-1)
                    //{
                    //    customer.ServiceProviderID = null;
                    //}
                    //else
                    //{
                    //    customer.ServiceProviderID = Convert.ToInt32(ddlSPName.SelectedValue);
                    //}

                    //Commented on 07082020
                    //if (chkSp.Checked == true)
                    //{
                    //    customer.IsServiceProvider = true;
                    //    customer.ServiceProviderID = null;
                    //}
                    //else
                    //{
                    //    customer.IsServiceProvider = false;
                    //    customer.ServiceProviderID = Convert.ToInt32(ddlSPName.SelectedValue);
                    //}

                    if (!string.IsNullOrEmpty(rblCustomerDistributor.SelectedValue))
                    {
                        if (rblCustomerDistributor.SelectedValue.Trim().ToUpper().Equals("C"))
                        {
                            customer.IsDistributor = false;
                        }
                        else if (rblCustomerDistributor.SelectedValue.Trim().ToUpper().Equals("D"))
                        {
                            customer.IsDistributor = true;
                        }
                    }

                    if (AuthenticationHelper.CustomerID != null)
                        customer.ParentID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                    //if (!string.IsNullOrEmpty(ddlDistributor.SelectedValue) && ddlDistributor.SelectedValue != "-1")
                    //{
                    //    customer.ParentID = Convert.ToInt32(ddlDistributor.SelectedValue);
                    //}

                    if (!string.IsNullOrEmpty(rblSubDistributor.SelectedValue))
                    {
                        customer.CanCreateSubDist = Convert.ToBoolean(Convert.ToInt32(rblSubDistributor.SelectedValue));
                    }

                    if (!string.IsNullOrEmpty(rblPayment.SelectedValue))
                    {
                        customer.IsPayment = Convert.ToBoolean(Convert.ToInt32(rblPayment.SelectedValue));
                    }

                    customer.IsServiceProvider = customer.IsDistributor;
                    customer.ServiceProviderID = customer.ParentID;

                    //ComplianceProductType 0--Compliance, 1 -HR Only Compliance(HR Compliance RLCS Generated) 2 -HR+Compliance(HR Compliance RLCS Generated)  
                    customer.ComplianceProductType = 0;

                    com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_Customer mstCustomer = new com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_Customer();
                    mstCustomer.Name = tbxName.Text;
                    mstCustomer.Address = tbxAddress.Text;
                    mstCustomer.BuyerName = tbxBuyerName.Text;
                    mstCustomer.BuyerContactNumber = tbxBuyerContactNo.Text;
                    mstCustomer.BuyerEmail = tbxBuyerEmail.Text;

                    if (strtdate != "" && enddate != "")
                    {
                        mstCustomer.StartDate = DateTime.ParseExact(strtdate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        mstCustomer.EndDate = DateTime.ParseExact(enddate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    }

                    if ((int)ViewState["Mode"] == 1)
                    {
                        mstCustomer.ID = Convert.ToInt32(ViewState["CustomerID"]);
                    }

                    if (CustomerManagementRisk.Exists(mstCustomer))
                    {
                        cvDuplicateEntry.ErrorMessage = "Customer name already exists.";
                        cvDuplicateEntry.IsValid = false;
                        return;
                    }

                    mstCustomer.LocationType = Convert.ToInt32(ddlLocationType.SelectedValue);
                    mstCustomer.Status = Convert.ToInt32(ddlCustomerStatus.SelectedValue);
                    mstCustomer.IComplianceApplicable = Convert.ToInt32(ddlComplianceApplicable.SelectedValue);
                    mstCustomer.TaskApplicable = Convert.ToInt32(ddlTaskApplicable.SelectedValue);

                    //Service Provider
                    mstCustomer.IsServiceProvider = customer.IsServiceProvider;
                    mstCustomer.ServiceProviderID = customer.ServiceProviderID;

                    //Distributor
                    mstCustomer.IsDistributor = customer.IsDistributor;
                    mstCustomer.ParentID = customer.ParentID;
                    mstCustomer.CanCreateSubDist = customer.CanCreateSubDist;
                    mstCustomer.IsPayment = customer.IsPayment;

                    //ComplianceProductType
                    mstCustomer.ComplianceProductType = customer.ComplianceProductType;

                    if ((int)ViewState["Mode"] == 0)
                    {
                        bool resultDataChk = false;
                        int resultData = 0;

                        resultData = CustomerManagement.Create(customer);
                        if (resultData > 0)
                        {
                            resultDataChk = CustomerManagementRisk.Create(mstCustomer);
                            if (resultDataChk == false)
                            {
                                CustomerManagement.deleteCustReset(resultData);
                            }
                            else
                            {
                                avacom_CustomerID = resultData;
                                saveSuccess = true;
                            }
                        }

                        try
                        {
                            string portalurl = string.Empty;
                            URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(AuthenticationHelper.CustomerID));
                            if (Urloutput != null)
                            {
                                portalurl = Urloutput.URL;
                            }
                            else
                            {
                                portalurl = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                            }

                            string ReplyEmailAddressName = "Avantis";
                            string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EMailTemplate_NewCustomerCreaded
                                              .Replace("@NewCustomer", customer.Name)
                                              .Replace("@LoginUser", AuthenticationHelper.User)
                                              .Replace("@PortalURL", Convert.ToString(portalurl))
                                              .Replace("@From", ReplyEmailAddressName)
                                              .Replace("@URL", Convert.ToString(portalurl));

                            //string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EMailTemplate_NewCustomerCreaded
                            //                    .Replace("@NewCustomer", customer.Name)
                            //                    .Replace("@LoginUser", AuthenticationHelper.User)
                            //                    .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                            //                    .Replace("@From", ReplyEmailAddressName)
                            //                    .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]));

                            string SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"].ToString();
                            string CustomerCreatedEmail = ConfigurationManager.AppSettings["CustomerCreatedEmail"].ToString();
                            //EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(new String[] { user.Email }), null, null, "AVACOM Email ID Changed.", message);
                            EmailManager.SendMail(SenderEmailAddress, new List<String>(new String[] { CustomerCreatedEmail }), null, null, "AVACOM customer account created.", message);
                        }
                        catch (Exception ex)
                        {
                            LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                        }
                    }
                    else if ((int)ViewState["Mode"] == 1)
                    {
                        CustomerManagement.Update(customer);
                        CustomerManagementRisk.Update(mstCustomer);//added by rahul on 18 April 2016
                        saveSuccess = true;
                    }

                    if (saveSuccess)
                    {
                        bool accesstoDirector = false;
                        if (chkBoxAccessDirector.Checked)
                            accesstoDirector = chkBoxAccessDirector.Checked;

                        bool defaultVirtualMeeting = false;
                        if (chkBoxDefaultVirtualMeeting.Checked)
                            defaultVirtualMeeting = chkBoxDefaultVirtualMeeting.Checked;

                        SecretarialManagement.SetCustomerConfiguration(avacom_CustomerID, AuthenticationHelper.UserID, accesstoDirector, defaultVirtualMeeting, false);

                        com.VirtuosoITech.ComplianceManagement.Business.Data.ProductMapping productmapping = new com.VirtuosoITech.ComplianceManagement.Business.Data.ProductMapping()
                        {
                            CustomerID = avacom_CustomerID,
                            ProductID = 8, //Secretarial
                            IsActive = false,
                            CreatedBy = 1,
                            CreatedOn = DateTime.Now
                        };

                        ProductMapping_Risk productmappingrisk = new ProductMapping_Risk()
                        {
                            CustomerID = avacom_CustomerID,
                            ProductID = 8, //Secretarial
                            IsActive = false,
                            CreatedBy = 1,
                            CreatedOn = DateTime.Now
                        };

                        RLCS_Master_Management.CreateUpdate_ProductMapping_ComplianceDB(productmapping);
                        RLCS_Master_Management.CreateUpdate_ProductMapping_AuditDB(productmappingrisk);

                        //Upload Customer Logo
                        if (fuCustLogoUpload.HasFile)
                        {
                            string fileName = Path.GetFileName(fuCustLogoUpload.PostedFile.FileName);
                            string FolderPath = "~/CustomerLogos/" + avacom_CustomerID + "/" + customer.Name;

                            string filepath = FolderPath + "/" + fileName;

                            if (!Directory.Exists(MapPath(FolderPath)))
                            {
                                Directory.CreateDirectory(Server.MapPath(FolderPath));
                            }

                            if (!File.Exists(MapPath(filepath)))
                            {
                                fuCustLogoUpload.PostedFile.SaveAs(Server.MapPath(filepath));
                            }

                            var LogoPath = filepath;
                            customer.LogoPath = LogoPath;

                            CustomerManagement.UpdateCustomerPhoto(Convert.ToInt32(avacom_CustomerID), filepath);
                            CustomerManagementRisk.UpdateCustomerPhoto(Convert.ToInt32(avacom_CustomerID), filepath);
                        }

                        if (customer.ServiceProviderID != null && customer.ParentID != null)
                        {
                            //ServiceProviderCustomerLimit obj = new ServiceProviderCustomerLimit();
                            //obj.ServiceProviderID = Convert.ToInt32(customer.ServiceProviderID);
                            //obj.DistributorID = Convert.ToInt32(customer.ParentID);
                            //obj.CustomerLimit = 25;
                            //obj.UserLimit = 5;
                            //ICIAManagement.CreateServiceProviderCustomerLimit(obj);
                        }
                    }

                    if (saveSuccess)
                    {
                        EditCustomerInformation(avacom_CustomerID);

                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Details Save Successfully";
                        ValidationSummary1.CssClass = "alert alert-success";
                        //added by tejashri for close popup window after save record..
                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "script", "ClosePopupWindowAfterSave();", true);
                        //added by tejashri for close popup window after save record..
                    }
                }
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    foreach (var item in errorMessage)
                    {
                        cvDuplicateEntry.ErrorMessage = item;
                    }
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured, Please try again.";
            }
        }

        protected void upCustomers_Load(object sender, EventArgs e)
        {
            try
            {

                DateTime date = DateTime.MinValue;
                if (DateTime.TryParseExact(txtStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date) || DateTime.TryParseExact(txtEndDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                }
                else
                {
                    //ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", "initializeDatePicker(null);", true);
                    ScriptManager.RegisterStartupScript(upCustomers, upCustomers.GetType(), "InitializeDatePicker", "initializeDatePicker(null);", true);
                }
                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "InitializeCombobox", "initializeCombobox();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        /// <summary>
        /// This method for binding customer status.
        /// </summary>
        public void BindCustomerStatus()
        {
            try
            {
                ddlCustomerStatus.DataTextField = "Name";
                ddlCustomerStatus.DataValueField = "ID";

                ddlCustomerStatus.DataSource = Enumerations.GetAll<CustomerStatus>();
                ddlCustomerStatus.DataBind();

                if (ddlCustomerStatus.Items.FindByText("Suspended") != null)
                    ddlCustomerStatus.Items.Remove(ddlCustomerStatus.Items.FindByText("Suspended"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rblCustomerDistributor_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(rblCustomerDistributor.SelectedValue))
                {
                    if (rblCustomerDistributor.SelectedValue == "D")
                    {
                        divSubDist.Visible = true;

                        if (AuthenticationHelper.Role == "SPADM" || AuthenticationHelper.Role == "IMPT")
                            divPayment.Visible = true;
                        else
                            divPayment.Visible = false;
                    }
                    else if (rblCustomerDistributor.SelectedValue == "C")
                    {
                        divSubDist.Visible = false;
                        divPayment.Visible = false;

                        if (rblSubDistributor.Items.FindByValue("0") != null)
                        {
                            rblSubDistributor.ClearSelection();
                            rblSubDistributor.Items.FindByValue("0").Selected = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindCountry()
        {
            try
            {
                ddlCountry.Items.Clear();

                ddlCountry.DataTextField = "Name";
                ddlCountry.DataValueField = "ID";

                ddlCountry.DataSource = RLCS_Master_Management.FillCountry();
                ddlCountry.DataBind();

                if (ddlCountry.Items.FindByText("India") != null)
                {
                    ddlCountry.ClearSelection();
                    ddlCountry.Items.FindByText("India").Selected = true;

                    ddlCountry_SelectedIndexChanged(null, null);
                }

                //ddlCountry.Items.Insert(0, new ListItem("<Select Country>", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindState();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindState()
        {
            try
            {
                if (ddlCountry.SelectedValue == "001")
                {
                    ddlState.Items.Clear();

                    ddlState.DataTextField = "Name";
                    ddlState.DataValueField = "ID";

                    ddlState.DataSource = RLCS_Master_Management.FillStates();
                    ddlState.DataBind();

                    //ddlState.Items.Insert(0, new ListItem("<Select State>", "-1"));
                }
                else
                {
                    ddlState.Items.Clear();
                    //ddlState.Items.Insert(0, new ListItem("<Select State>", "-1"));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindCity();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindCity()
        {
            try
            {
                if (ddlState.SelectedValue != "" || !string.IsNullOrEmpty(ddlState.SelectedValue) || ddlState.SelectedValue != "-1")
                {
                    ddlCity.Items.Clear();

                    ddlCity.DataTextField = "Name";
                    ddlCity.DataValueField = "ID";
                    if (!String.IsNullOrEmpty(ddlState.SelectedValue))
                    {
                        ddlCity.DataSource = RLCS_Master_Management.FillLocationCityByStateID(Convert.ToInt32(ddlState.SelectedValue));

                    }
                    ddlCity.DataBind();

                    //ddlCity.Items.Insert(0, new ListItem("<Select City>", "-1"));
                }
                else
                {
                    ddlCity.Items.Clear();
                    //ddlCity.Items.Insert(0, new ListItem("<Select City>", "-1"));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

    }
}