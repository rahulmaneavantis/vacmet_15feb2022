﻿using BM_ManegmentServices;
using BM_ManegmentServices.Services.DocumentManagenemt;
using BM_ManegmentServices.Services.Meetings;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using GleamTech.DocumentUltimate.Web;
using Syncfusion.DocIO;
using Syncfusion.DocIO.DLS;
using Syncfusion.DocToPDFConverter;
using Syncfusion.Pdf;
using Syncfusion.Pdf.Parsing;
using Syncfusion.OfficeChart;
//using Syncfusion.OfficeChartToImageConverter;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Windows.Documents.Flow.FormatProviders.Html;
using Telerik.Windows.Documents.Flow.Model;
using System.IO;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas_Secretarial
{
    public partial class AgendaDocumentViewerSyncfusion : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["meetingId"] != null)
                {
                    long meetingId = Convert.ToInt64(Request.QueryString["meetingId"]);
                    long assignmentId = 0;
                    var generateNew = false;
                    if (Request.QueryString["assignmentId"] != null)
                    {
                        if (!long.TryParse(Request.QueryString["assignmentId"], out assignmentId))
                        {
                            assignmentId = 0;
                        }

                        if(Request.QueryString["mode"] != null)
                        {
                            if(Request.QueryString["mode"] == "Preview")
                            {
                                generateNew = true;
                            }
                        }
                    }
                    if (meetingId > 0)
                    {
                        LoadAgendaDocument(meetingId, assignmentId, generateNew);
                    }
                }
                else if (Request.QueryString["minutesMeetingId"] != null)
                {
                    long minutesMeetingId = Convert.ToInt64(Request.QueryString["minutesMeetingId"]);
                    long assignmentId = 0;
                    var generateNew = false;
                    if (Request.QueryString["assignmentId"] != null)
                    {
                        if (!long.TryParse(Request.QueryString["assignmentId"], out assignmentId))
                        {
                            assignmentId = 0;
                        }

                        if (Request.QueryString["mode"] != null)
                        {
                            if (Request.QueryString["mode"] == "Preview")
                            {
                                generateNew = true;
                            }
                        }
                    }
                    if (minutesMeetingId > 0)
                    {
                        LoadMinutesDocument(minutesMeetingId, assignmentId, generateNew);
                    }
                }
            }
        }

        public void LoadAgendaDocument(long meetingId, long? assignmentId, bool generateNew)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int userId = Convert.ToInt32(AuthenticationHelper.UserID);

            var objIDocument_Service = new Document_Service(new AgendaMinutesReviewService(), new DocumentSetting_Service(), new FileData_Service());
            var fileName = objIDocument_Service.GenerateAgendaDocument1(meetingId, assignmentId, userId, customerId, generateNew);
            doccontrol.Document = fileName;
        }

        public void LoadMinutesDocument(long minutesMeetingId, long? assignmentId, bool generateNew)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int userId = Convert.ToInt32(AuthenticationHelper.UserID);

            var objIDocument_Service = new Document_Service(new AgendaMinutesReviewService(), new DocumentSetting_Service(), new FileData_Service());
            var fileName = objIDocument_Service.GetMinutesDocumentFile(minutesMeetingId, assignmentId, userId, customerId, generateNew, true, string.Empty);
            doccontrol.Document = fileName;
        }

        public void CheckAllAgendaData(int m)
        {
            var convertToKendoHtml = false;
            WordDocument doc = new WordDocument();
            doc.EnsureMinimal();

            string agendaFormatId = "", ResolutionId = "", MiApproveId = "", MiDisApproveId = "", MiDifferId = "";

            WParagraph para = doc.LastParagraph;
            IAgendaMinutesReviewService objIAgendaMinutesReviewService = new AgendaMinutesReviewService();
            var items = objIAgendaMinutesReviewService.GetAgenda(m);
            foreach (var item in items)
            {
                var SrNo = item.AgendaID;

                doc.AddSection();

                para = doc.LastSection.AddParagraph() as WParagraph;
                para.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Right;
                WTextRange text = para.AppendText("Agenda Id. " + SrNo) as WTextRange;
                text.CharacterFormat.UnderlineStyle = UnderlineStyle.Single;
                text.CharacterFormat.Bold = true;

                para = doc.LastSection.AddParagraph() as WParagraph;


                #region Convert html format in XHTML 1.0
                var htmlstring = (string.IsNullOrEmpty(item.AgendaFormatHeading) ? "" : item.AgendaFormatHeading.ToUpper());
                htmlstring = htmlstring.Replace("<B>", "<b>");
                htmlstring = htmlstring.Replace("</B>", "</b>");
                #endregion

                bool isValidHtml = doc.LastSection.Body.IsValidXHTML(htmlstring, XHTMLValidationType.None);
                if (isValidHtml)
                {
                    //section.Body.InsertXHTML(htmlstring);
                    para.AppendHTML(htmlstring);
                    para.ParagraphFormat.Borders.Bottom.BorderType = Syncfusion.DocIO.DLS.BorderStyle.Single;
                    //newPara.ParagraphFormat.Borders.Bottom.LineWidth = 1;
                    para.ApplyStyle(BuiltinStyle.Heading2);
                }

                htmlstring = item.AgendaFormat;
                HtmlFormatProvider htmlProvider = new HtmlFormatProvider();
                RadFlowDocument htmlDocument = new RadFlowDocument();


                if (!string.IsNullOrEmpty(htmlstring))
                {
                    htmlProvider.Import(htmlstring);
                    if (convertToKendoHtml)
                    {
                        htmlstring = htmlProvider.Export(htmlDocument);
                    }

                    try
                    {
                        isValidHtml = doc.LastSection.Body.IsValidXHTML(htmlstring, XHTMLValidationType.None);
                        if (isValidHtml)
                        {
                            para = doc.LastSection.AddParagraph() as WParagraph;
                            para.AppendHTML(htmlstring);
                        }
                        else
                        {
                            agendaFormatId += agendaFormatId + SrNo + ",";
                        }

                        //, ResolutionId = "", MiApproveId = "", MiDisApproveId = "", MiDifferId = "";
                    }
                    catch (Exception ex)
                    {
                        agendaFormatId += agendaFormatId + SrNo + ",";
                    }
                }


                #region Resolution
                htmlstring = item.ResolutionFormat;
                if (!string.IsNullOrEmpty(htmlstring))
                {
                    if (convertToKendoHtml)
                    {
                        htmlProvider = new HtmlFormatProvider();
                        htmlDocument = htmlProvider.Import(htmlstring);
                        htmlstring = htmlProvider.Export(htmlDocument);
                    }

                    try
                    {
                        isValidHtml = doc.LastSection.Body.IsValidXHTML(htmlstring, XHTMLValidationType.None);
                        if (isValidHtml)
                        {
                            para = doc.LastSection.AddParagraph() as WParagraph;
                            para.AppendHTML(htmlstring);
                        }
                        else
                        {
                            ResolutionId += ResolutionId + SrNo + ",";
                        }
                    }
                    catch (Exception ex)
                    {
                        ResolutionId += ResolutionId + SrNo + ",";
                    }
                }

                #endregion

                #region MinutesApproveFormat

                htmlstring = item.MinutesApproveFormat;
                if (!string.IsNullOrEmpty(htmlstring))
                {
                    if (convertToKendoHtml)
                    {
                        htmlProvider = new HtmlFormatProvider();
                        htmlDocument = htmlProvider.Import(htmlstring);
                        htmlstring = htmlProvider.Export(htmlDocument);
                    }

                    try
                    {
                        isValidHtml = doc.LastSection.Body.IsValidXHTML(htmlstring, XHTMLValidationType.None);
                        if (isValidHtml)
                        {
                            para = doc.LastSection.AddParagraph() as WParagraph;
                            para.AppendHTML(htmlstring);
                        }
                        else
                        {
                            MiApproveId += MiApproveId + SrNo + ",";
                        }
                    }
                    catch (Exception ex)
                    {
                        MiApproveId += MiApproveId + SrNo + ",";
                    }
                }
                #endregion

                #region MinutesDisApproveFormat
                htmlstring = item.MinutesDisApproveFormat;

                if (!string.IsNullOrEmpty(htmlstring))
                {
                    if (convertToKendoHtml)
                    {
                        htmlProvider = new HtmlFormatProvider();
                        htmlDocument = htmlProvider.Import(htmlstring);
                        htmlstring = htmlProvider.Export(htmlDocument);
                    }

                    try
                    {
                        isValidHtml = doc.LastSection.Body.IsValidXHTML(htmlstring, XHTMLValidationType.None);
                        if (isValidHtml)
                        {
                            para = doc.LastSection.AddParagraph() as WParagraph;
                            para.AppendHTML(htmlstring);
                        }
                        else
                        {
                            MiDisApproveId += MiDisApproveId + SrNo + ",";
                        }
                    }
                    catch (Exception ex)
                    {
                        MiDisApproveId += MiDisApproveId + SrNo + ",";
                    }
                }
                #endregion

                #region MinutesDeferFormat
                htmlstring = item.MinutesDeferFormat;


                if (!string.IsNullOrEmpty(htmlstring))
                {
                    if (convertToKendoHtml)
                    {
                        htmlProvider = new HtmlFormatProvider();
                        htmlDocument = htmlProvider.Import(htmlstring);
                        htmlstring = htmlProvider.Export(htmlDocument);
                    }

                    try
                    {
                        isValidHtml = doc.LastSection.Body.IsValidXHTML(htmlstring, XHTMLValidationType.None);
                        if (isValidHtml)
                        {
                            para = doc.LastSection.AddParagraph() as WParagraph;
                            para.AppendHTML(htmlstring);
                        }
                        else
                        {
                            MiDifferId += MiDifferId + SrNo + ",";
                        }
                    }
                    catch (Exception ex)
                    {
                        MiDifferId += MiDifferId + SrNo + ",";
                    }
                }
                #endregion

            }


            using (System.IO.StreamWriter sw = System.IO.File.AppendText(Server.MapPath("~\\Temp\\AgendaItemlog.txt")))
            {
                //Response.Write( + ": " + Request.Form[keys[i]] + "<br>");
                sw.WriteLine("Agenda Item test on :" + DateTime.Now.ToString("dd MMM yyyy hh:mm:ss tt"));

                sw.WriteLine("Agenda Format (" + agendaFormatId + ")");
                sw.WriteLine("Agenda ResolutionId (" + ResolutionId + ")");
                sw.WriteLine("Agenda MiApproveId (" + MiApproveId + ")");
                sw.WriteLine("Agenda MiDisApproveId (" + MiDisApproveId + ")");
                sw.WriteLine("Agenda MiDifferId (" + MiDifferId + ")");
                sw.WriteLine("-------------------------------");
                sw.Close();
            }


            doc.Save("AgendaItemsList.docx", FormatType.Docx, Response, HttpContentDisposition.Attachment);

        }
    }
}