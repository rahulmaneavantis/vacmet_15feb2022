﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Doc_ViewerByHistoricalId.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Areas_Secretarial.Doc_ViewerByHistoricalId" %>

<!DOCTYPE html>
<%@ Register TagPrefix="GleamTech" Namespace="GleamTech.DocumentUltimate.Web" Assembly="GleamTech.DocumentUltimate" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

     <script type="text/javascript">

        function ShowViewDocument() {
            $('#divViewDocument').modal('show');
            return true;
        };

         function fopendocfileReview(file) {
             debugger;
             var url = "../../Areas_Secretarial/doc_viewer.aspx";
            $('#divViewDocument').modal('show');
            $('#docViewerStatutory').attr('src', "../../Areas_Secretarial/doc_viewer.aspx?agendaDocUrl=" + file);
        }

        function ViewInternalDocument() {
            $('#divViewInternalDocument').modal('show');
            return true;
        };

        function fopendocfileReviewInternal(file) {
            $('#divViewInternalDocument').modal('show');
            $('#docViewerInternal').attr('src', "doc_viewer.aspx?agendaDocUrl=" + file);
        }
    </script>
</head>

<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div>
            <div id="divViewDocument" style="width: 95%; background: #fff;">
                <div style="float: left; width: 10%">
                    <table width="100%" style="text-align: left; margin-left: 25%;">
                        <thead>
                            <tr>
                                <td valign="top">
                                    <asp:UpdatePanel ID="upComplianceDetails1" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:Repeater ID="rptComplianceVersionView" runat="server" OnItemCommand="rptComplianceVersionView_ItemCommand"
                                                OnItemDataBound="rptComplianceVersionView_ItemDataBound">
                                                <HeaderTemplate>
                                                    <table id="tblComplianceDocumnets">
                                                        <thead>
                                                            <th>Versions</th>
                                                        </thead>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td>
                                                            <input class="hidden" type="button" onclick="fopendocfileReview(<%# Eval("FileID")%>);" />
                                                            <a href="#" role="button" onclick="fopendocfileReview(<%# Eval("FileID")%>);" title='<%# Eval("FileName")%>' ><%# Eval("Version") +" "+ Eval("FileName").ToString().Substring(0,4) %></a>
                                                        </td>
                                                        <td class="hidden">
                                                            <asp:LinkButton CommandName="View" CommandArgument='<%# Eval("FileID") + ","+ Eval("Version")+ ","+ Eval("FileID") %>' ID="lblDocumentVersionView"
                                                                runat="server" ToolTip='<%# Eval("FileName")%>' Text='<%# Eval("Version") +" "+ Eval("FileName").ToString().Substring(0,4) %>'></asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </table>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="rptComplianceVersionView" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                        </thead>
                    </table>
                </div>
                <div style="float: left; width: 90%">
                    <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdatleMode="Conditional">
                        <ContentTemplate>
                            <asp:Label runat="server" ID="lblMessage" Style="color: red;"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; height: 530px; width: 100%;">
                        <iframe src="about:blank" id="docViewerStatutory" runat="server" width="100%" height="510px"></iframe>
                    </fieldset>
                </div>
            </div>


        </div>
    </form>
</body>
</html>