﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas_Secretarial
{
    public partial class MyDriveLevel2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["FolderID"]))
                {
                    int FolderID = Convert.ToInt32(Request.QueryString["FolderID"]);
                    ViewState["ExistFolderID"] = FolderID;
                    ViewState["FixExistFolderID"] = FolderID;
                    if (!string.IsNullOrEmpty(Request.QueryString["FilterSearch"]))
                    {
                        tbxFilter.Text = Convert.ToString(Request.QueryString["FilterSearch"]);
                    }
                }
                BindFolderData();
                bindPageNumber();

                BindUsers();
                BindListShare();

            }
        }

        private void BindListShare()
        {
            List<ShareDetail> ShareList = new List<ShareDetail>();

            myRepeater.DataSource = ShareList;
            myRepeater.DataBind();
        }

        public static List<User> GetAllUsers(int customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Users
                             where row.IsDeleted == false
                             && row.CustomerID == customerID
                             select row).ToList();

                return query.ToList();
            }
        }

        private void BindUsers()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
                var lstAllUsers = GetAllUsers(customerID);
                var lstUsers = (from row in lstAllUsers
                                select new { ID = row.ID, Name = row.FirstName + " " + row.LastName }).OrderBy(entry => entry.Name).ToList<object>();

                lstBoxUser.Items.Clear();
                lstBoxUser.DataValueField = "ID";
                lstBoxUser.DataTextField = "Name";
                lstBoxUser.DataSource = lstUsers;
                lstBoxUser.DataBind();

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {

                grdFolderDetail.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                //Reload the Grid
                BindFolderData();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdFolderDetail.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvMailDocument.IsValid = false;
                cvMailDocument.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindFolderData()
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    long customerID = -1;
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                    string Searchfilter = string.Empty;
                    Searchfilter = tbxFilter.Text.ToString();

                    long CurrentFID = Convert.ToInt64(ViewState["ExistFolderID"]);
                    var FolderList = entities.SP_GetFolderFileDataPermissionNew(Convert.ToInt64(AuthenticationHelper.UserID), CurrentFID, customerID, Searchfilter).ToList();
                    grdFolderDetail.DataSource = FolderList.OrderByDescending(entry => entry.Type).ToList();
                    grdFolderDetail.DataBind();
                    Session["TotalDocShareListRows"] = null;
                    Session["TotalDocShareListRows"] = Convert.ToInt32(FolderList.Count);
                    //upPromotorList.Update();
                    Session["TotalRows"] = Convert.ToInt32(FolderList.Count);



                    //bind list name 
                    List<LinkAllFilterData> objlnk = new List<LinkAllFilterData>();
                    long CurrentFIDNew = Convert.ToInt64(ViewState["ExistFolderID"]);
                    int parentID = 0;
                    do
                    {
                        var FolderDetail = (from row in entities.Mst_FolderMaster
                                            where row.IsDeleted == false && row.CustomerID == customerID
                                            && row.ID == CurrentFID
                                            select row).FirstOrDefault();

                        if (FolderDetail != null)
                        {
                            objlnk.Add(new LinkAllFilterData { ID = Convert.ToInt64(FolderDetail.ID), Name = FolderDetail.Name.ToString() });
                            if (FolderDetail.ParentID != null)
                            {
                                parentID = Convert.ToInt32(FolderDetail.ParentID);
                                CurrentFID = parentID;
                            }
                            else
                            {
                                CurrentFID = 0;
                            }
                        }

                    } while (CurrentFID > 0);
                    objlnk.Reverse();
                    dlBreadcrumb.DataSource = objlnk.ToList();
                    dlBreadcrumb.DataBind();
                    upPromotorList.Update();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void dlBreadcrumb_ItemCommand(object source, DataListCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "ITEM_CLICKED")
                {
                    if (e.Item.ItemIndex == 0)
                    {
                        ViewState["ExistFolderID"] = Convert.ToInt64(ViewState["FixExistFolderID"]);

                        ViewState["ExistFolderID"] = e.CommandArgument.ToString();
                        BindFolderData();
                    }
                    else
                    {
                        ViewState["ExistFolderID"] = e.CommandArgument.ToString();
                        BindFolderData();
                    }

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void btnPermission_Click(object sender, EventArgs e)
        {
            try
            {
                ComplianceDBEntities entities = new ComplianceDBEntities();
                long UserID = Convert.ToInt64(AuthenticationHelper.UserID);
                bool IsReadFile = false;
                bool IsWriteFile = false;
                bool saveSuccess = false;
                List<long> lstUserMapping = new List<long>();
                List<long> lstUserRemoveMapping = new List<long>();
                long customerID = -1;
                customerID = Convert.ToInt64(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                if (ViewState["ExistFileIDShare"] != null)
                {
                    long ExistFileID = Convert.ToInt64(ViewState["ExistFileIDShare"]);
                    if (ViewState["Fileorfolder"].ToString().ToLower() == "file")
                    {

                        if (ExistFileID > 0)
                        {

                            #region Userlist
                            foreach (ListItem eachUser in lstBoxUser.Items)
                            {
                                if (eachUser.Selected)
                                {
                                    if (Convert.ToInt32(eachUser.Value) > 0)
                                    {
                                        lstUserMapping.Add(Convert.ToInt64(eachUser.Value));
                                    }
                                }
                                else
                                {
                                    if (Convert.ToInt32(eachUser.Value) > 0)
                                    {
                                        lstUserRemoveMapping.Add(Convert.ToInt64(eachUser.Value));
                                    }

                                }
                            }
                            #endregion

                            #region Check Permission
                            if (ddlPermission1.SelectedValue != null && Convert.ToInt32(ddlPermission1.SelectedValue) > 0)
                            {
                                if (Convert.ToInt32(ddlPermission1.SelectedValue) == 1)
                                {
                                    IsReadFile = true;
                                    IsWriteFile = false;
                                }
                                if (Convert.ToInt32(ddlPermission1.SelectedValue) == 2)
                                {
                                    IsReadFile = false;
                                    IsWriteFile = true;
                                }
                                if (Convert.ToInt32(ddlPermission1.SelectedValue) == 3)
                                {
                                    IsReadFile = true;
                                    IsWriteFile = true;
                                }
                            }
                            #endregion

                            #region share file
                            UserFolderPermission objFldPersmission = (from row in entities.UserFolderPermissions
                                                                      where row.IsDeleted == false && row.ID == ExistFileID
                                                                            && row.CustomerID == customerID
                                                                      select row).FirstOrDefault();

                            if (objFldPersmission != null)
                            {
                                long FileID = Convert.ToInt64(objFldPersmission.FileID);

                                if (objFldPersmission.CreatedBy == UserID)
                                {
                                    var fetchFileDetail = (from row in entities.FolderFileDatas
                                                           where row.IsDeleted == false && row.ID == FileID
                                                                 && row.CustomerID == customerID
                                                           select row).FirstOrDefault();
                                    if (fetchFileDetail != null)
                                    {
                                        List<UserFolderPermission> lstUSerMapping_ToSave = new List<UserFolderPermission>();

                                        lstUserMapping.ForEach(EachUSer =>
                                        {

                                            UserFolderPermission updatedPermission = (from row in entities.UserFolderPermissions
                                                                                      where row.CustomerID == customerID
                                                                                      && row.FileID == FileID && row.UserID == EachUSer
                                                                                      select row).FirstOrDefault();
                                            if (updatedPermission != null)
                                            {
                                                updatedPermission.IsDeleted = false;
                                                updatedPermission.UpdatedBy = Convert.ToInt64(AuthenticationHelper.UserID);
                                                updatedPermission.UpdatedOn = DateTime.Now;
                                                entities.SaveChanges();
                                            }
                                            else
                                            {
                                                UserFolderPermission _UserMappingRecord = new UserFolderPermission()
                                                {
                                                    FolderID = Convert.ToInt64(fetchFileDetail.FolderId),
                                                    FileID = Convert.ToInt64(fetchFileDetail.ID),
                                                    CreatedBy = Convert.ToInt64(AuthenticationHelper.UserID),
                                                    CreatedOn = DateTime.Now,
                                                    IsRead = IsReadFile,
                                                    IsWrite = IsWriteFile,
                                                    IsDeleted = false,
                                                    UserID = Convert.ToInt64(EachUSer),
                                                    CustomerID = Convert.ToInt64(customerID),
                                                };
                                                lstUSerMapping_ToSave.Add(_UserMappingRecord);
                                            }
                                        });


                                        if (lstUSerMapping_ToSave.Count > 0)
                                        {
                                            lstUSerMapping_ToSave.ForEach(eachRecord =>
                                            {
                                                saveSuccess = false;
                                                entities.UserFolderPermissions.Add(eachRecord);
                                                entities.SaveChanges();
                                                saveSuccess = true;
                                            });
                                        }

                                        if (saveSuccess)
                                        {
                                            // saveSuccess = true;
                                        }
                                    }
                                }
                            }
                            #endregion
                        }
                    }
                    else
                    {
                        #region Userlist
                        foreach (ListItem eachUser in lstBoxUser.Items)
                        {
                            if (eachUser.Selected)
                            {
                                if (Convert.ToInt32(eachUser.Value) > 0)
                                {
                                    lstUserMapping.Add(Convert.ToInt64(eachUser.Value));
                                }
                            }
                            else
                            {
                                if (Convert.ToInt32(eachUser.Value) > 0)
                                {
                                    lstUserRemoveMapping.Add(Convert.ToInt64(eachUser.Value));
                                }

                            }
                        }
                        #endregion

                        #region Permission
                        if (ddlPermission1.SelectedValue != null && Convert.ToInt32(ddlPermission1.SelectedValue) > 0)
                        {
                            if (Convert.ToInt32(ddlPermission1.SelectedValue) == 1)
                            {
                                IsReadFile = true;
                                IsWriteFile = false;
                            }
                            if (Convert.ToInt32(ddlPermission1.SelectedValue) == 2)
                            {
                                IsReadFile = false;
                                IsWriteFile = true;
                            }
                            if (Convert.ToInt32(ddlPermission1.SelectedValue) == 3)
                            {
                                IsReadFile = true;
                                IsWriteFile = true;
                            }
                        }
                        #endregion

                        #region Folder
                        saveSuccess = false;
                        long CurrentFolderID = Convert.ToInt64(ViewState["ExistFolderID"]);
                        long CreatedUserID = (from row in entities.Mst_FolderMaster
                                              where row.ID == ExistFileID && row.IsDeleted == false && row.ParentID == CurrentFolderID
                                              && row.CreatedBy == UserID && row.CustomerID == customerID
                                              select (long)row.CreatedBy).FirstOrDefault();
                        if (CreatedUserID > 0)
                        {
                            List<long> AllFolderIDs = (from row in entities.sp_AllFolder((int)ExistFileID)
                                                       select (long)row).ToList();
                            List<UserFolderPermission> lstUSerMapping_ToSaveFolder = new List<UserFolderPermission>();
                            foreach (var itemf in AllFolderIDs)
                            {
                                long assignFolderId = Convert.ToInt64(itemf);

                                lstUserMapping.ForEach(EachUSer =>
                                {
                                    UserFolderPermission CheckExistFileAssigne = (from row in entities.UserFolderPermissions
                                                                                  where row.FolderID == assignFolderId && row.IsDeleted == false
                                                                                  && row.UserID == EachUSer && row.CustomerID == customerID
                                                                                  select row).FirstOrDefault();

                                    if (CheckExistFileAssigne != null)
                                    {

                                    }
                                    else
                                    {
                                        UserFolderPermission _UserMappingRecord = new UserFolderPermission()
                                        {
                                            FolderID = Convert.ToInt64(assignFolderId),
                                            FileID = null,
                                            CreatedBy = Convert.ToInt64(CreatedUserID),
                                            CreatedOn = DateTime.Now,
                                            IsRead = IsReadFile,
                                            IsWrite = IsWriteFile,
                                            IsDeleted = false,
                                            UserID = Convert.ToInt64(EachUSer),
                                            CustomerID = Convert.ToInt64(customerID),
                                        };
                                        lstUSerMapping_ToSaveFolder.Add(_UserMappingRecord);
                                    }
                                });
                            }
                            if (lstUSerMapping_ToSaveFolder.Count > 0)
                            {
                                lstUSerMapping_ToSaveFolder.ForEach(eachRecord =>
                                {
                                    saveSuccess = false;
                                    entities.UserFolderPermissions.Add(eachRecord);
                                    entities.SaveChanges();
                                    saveSuccess = true;
                                });
                            }
                        }
                        #endregion

                        #region File

                        List<long> FileIDs = (from row in entities.sp_AllfilesofFolder((int)ExistFileID)
                                              select (long)row).ToList();

                        List<UserFolderPermission> lstUSerMapping_ToSave = new List<UserFolderPermission>();
                        foreach (var item in FileIDs)
                        {
                            long FileIds = Convert.ToInt64(item);

                            UserFolderPermission uploadedFileIds = (from row in entities.UserFolderPermissions
                                                                    where row.FileID == FileIds && row.IsDeleted == false
                                                                    && row.UserID == UserID && row.CustomerID == customerID
                                                                    select row).FirstOrDefault();
                            if (uploadedFileIds != null)
                            {
                                lstUserMapping.ForEach(EachUSer =>
                                {
                                    UserFolderPermission CheckExistFileAssigne = (from row in entities.UserFolderPermissions
                                                                                  where row.FileID == FileIds && row.IsDeleted == false
                                                                                  && row.UserID == EachUSer && row.CustomerID == customerID
                                                                                  select row).FirstOrDefault();

                                    if (CheckExistFileAssigne != null)
                                    {

                                    }
                                    else
                                    {
                                        UserFolderPermission _UserMappingRecord = new UserFolderPermission()
                                        {
                                            FolderID = Convert.ToInt64(uploadedFileIds.FolderID),
                                            FileID = Convert.ToInt64(FileIds),
                                            CreatedBy = Convert.ToInt64(uploadedFileIds.CreatedBy),
                                            CreatedOn = DateTime.Now,
                                            IsRead = IsReadFile,
                                            IsWrite = IsWriteFile,
                                            IsDeleted = false,
                                            UserID = Convert.ToInt64(EachUSer),
                                            CustomerID = Convert.ToInt64(customerID),
                                        };
                                        lstUSerMapping_ToSave.Add(_UserMappingRecord);
                                    }
                                });
                            }
                        }
                        if (lstUSerMapping_ToSave.Count > 0)
                        {
                            lstUSerMapping_ToSave.ForEach(eachRecord =>
                            {
                                saveSuccess = false;
                                entities.UserFolderPermissions.Add(eachRecord);
                                entities.SaveChanges();
                                saveSuccess = true;
                            });
                        }

                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void btnUploadFile_Click(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["ExistFolderID"] != null)
                {
                    bool saveSuccess = false;
                    long ContractInstanceID = Convert.ToInt64(ViewState["ExistFolderID"]);

                    if (ContractInstanceID > 0)
                    {
                        #region Upload Document

                        if (ContractFileUpload.HasFiles)
                        {
                            saveSuccess = uploadDocuments(Request.Files);//, "DriveFileUpload", null, null, txtDocTags.Text.Trim());

                            if (saveSuccess)
                            {
                                vsDocumentValid.IsValid = false;
                                vsDocumentValid.ErrorMessage = "Document(s) uploaded successfully";
                            }
                            else
                            {
                                vsDocumentValid.IsValid = false;
                                vsDocumentValid.ErrorMessage = "Something went wrong, during document upload, Please try again";
                            }
                        }
                        else
                        {

                            vsDocumentValid.IsValid = false;
                            vsDocumentValid.ErrorMessage = "No document selected to upload";
                        }

                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvMailDocument.IsValid = false;
                cvMailDocument.ErrorMessage = "Something went wrong, Please try again";
            }
        }
        protected bool uploadDocuments(HttpFileCollection fileCollection)
        {
            bool uploadSuccess = false;
            try
            {

                for (int i = 0; i < fileCollection.Count; i++)
                {
                    HttpPostedFile uploadedFile = fileCollection[i];

                    string FileName = uploadedFile.FileName.ToString();
                }
                #region Upload Document
                ComplianceDBEntities entities = new ComplianceDBEntities();

                long customerID = -1;
                customerID = Convert.ToInt64(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                List<string> objstring = new List<string>();
                string directoryPath = string.Empty;
                string fileName = string.Empty;

                FolderFileData objFD = new FolderFileData()
                {
                    FolderId = Convert.ToInt64(ViewState["ExistFolderID"]),
                    CustomerID = customerID,
                    IsDeleted = false,
                    CreatedBy = Convert.ToInt64(AuthenticationHelper.UserID),
                    CreatedOn = DateTime.Now,
                    UpdatedBy = Convert.ToInt64(AuthenticationHelper.UserID),
                    UpdatedOn = DateTime.Now,
                    //Doc_Header = TxtDocHeader.Text.ToString(),
                    //Doc_Description = TxtDescription.Text.ToString(),
                    //Others = TxtOthers.Text.ToString(),
                    //Process = TxtProcess.Text.ToString(),
                    //SubProcess = TxtSubProcess.Text.ToString(),
                    //Vertical = TxtVertical.Text.ToString(),
                };

                long CurrentFID = Convert.ToInt64(ViewState["ExistFolderID"]);
                int parentID = 0;
                do
                {
                    var FolderDetail = (from row in entities.Mst_FolderMaster
                                        where row.IsDeleted == false && row.CustomerID == customerID
                                        && row.ID == CurrentFID
                                        select row).FirstOrDefault();

                    if (FolderDetail != null)
                    {
                        objstring.Add(FolderDetail.Name.ToString());
                        if (FolderDetail.ParentID != null)
                        {
                            parentID = Convert.ToInt32(FolderDetail.ParentID);
                            CurrentFID = parentID;
                        }
                        else
                        {
                            CurrentFID = 0;
                        }
                    }

                } while (CurrentFID > 0);

                string FinalFolderPath = "~/DriveFiles/" + customerID + "/";

                for (int i = (objstring.Count - 1); i >= 0; i--)
                {
                    FinalFolderPath = FinalFolderPath + objstring[i] + "/";
                }

                string UserNameFolder = txtFolderName.Text.ToString();

                string CreateNewFolder = FinalFolderPath + UserNameFolder;


                if (fileCollection.Count > 0)
                {
                    List<KeyValuePair<string, Byte[]>> fileList = new List<KeyValuePair<string, Byte[]>>();

                    if (Convert.ToInt32(Convert.ToInt64(ViewState["ExistFolderID"])) > 0)
                    {
                        for (int i = 0; i < fileCollection.Count; i++)
                        {
                            HttpPostedFile uploadedFile = fileCollection[i];

                            if (10485760 > Convert.ToInt64(fileCollection[i].ContentLength))
                            {
                                long Filesize = Convert.ToInt64(fileCollection[i].ContentLength) / 1024;
                                if (uploadedFile.ContentLength > 0)
                                {
                                    string[] keys1 = fileCollection.Keys[i].Split('$');

                                    if (keys1[keys1.Count() - 1].Equals("DriveFileUpload"))
                                    {
                                        fileName = uploadedFile.FileName;
                                    }

                                    //Get Document Version
                                    //var contractDocVersion = ContractDocumentManagement.ExistsContractDocumentReturnVersion(objContDoc);
                                    var contractDocVersion = 1.0;
                                    //contractDocVersion++;
                                    //objContDoc.Version = contractDocVersion + ".0";

                                    if (!Directory.Exists(Server.MapPath(CreateNewFolder)))
                                        Directory.CreateDirectory(Server.MapPath(CreateNewFolder));

                                    Guid fileKey1 = Guid.NewGuid();
                                    string finalPath1 = Path.Combine(Server.MapPath(CreateNewFolder), fileKey1 + Path.GetExtension(uploadedFile.FileName));
                                    Stream fs = uploadedFile.InputStream;
                                    BinaryReader br = new BinaryReader(fs);
                                    Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                                    fileList.Add(new KeyValuePair<string, Byte[]>(finalPath1, bytes));

                                    objFD.Version = "1.0";
                                    objFD.Name = uploadedFile.FileName.ToString();
                                    objFD.FilePath = (Server.MapPath(CreateNewFolder).Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/")).ToString();
                                    objFD.FileKey = fileKey1.ToString();
                                    objFD.VersionDate = DateTime.Now;
                                    objFD.FileSize = Filesize;

                                    DocumentManagement.SaveDocFiles(fileList);
                                    int UpdatedID = -1;
                                    uploadSuccess = true;
                                    if (uploadSuccess)
                                    {
                                        entities.FolderFileDatas.Add(objFD);
                                        entities.SaveChanges();

                                        UpdatedID = Convert.ToInt32(objFD.ID);
                                        if (UpdatedID <= 0)
                                        {
                                            uploadSuccess = false;
                                        }
                                    }
                                    if (uploadSuccess)
                                    {
                                        UserFolderPermission objFP = new UserFolderPermission()
                                        {
                                            FolderID = Convert.ToInt64(ViewState["ExistFolderID"]),
                                            FileID = Convert.ToInt64(UpdatedID),
                                            CreatedBy = Convert.ToInt64(AuthenticationHelper.UserID),
                                            CreatedOn = DateTime.Now,
                                            IsDeleted = false,
                                            UserID = Convert.ToInt64(AuthenticationHelper.UserID),
                                            CustomerID = customerID
                                        };
                                        if (ddlPermission.SelectedValue != null && Convert.ToInt32(ddlPermission.SelectedValue) > 0)
                                        {
                                            if (Convert.ToInt32(ddlPermission.SelectedValue) == 1)
                                            {
                                                objFP.IsRead = true;
                                                objFP.IsWrite = false;
                                            }
                                            if (Convert.ToInt32(ddlPermission.SelectedValue) == 2)
                                            {
                                                objFP.IsRead = false;
                                                objFP.IsWrite = true;
                                            }
                                            if (Convert.ToInt32(ddlPermission.SelectedValue) == 3)
                                            {
                                                objFP.IsRead = true;
                                                objFP.IsWrite = true;
                                            }
                                        }
                                        entities.UserFolderPermissions.Add(objFP);
                                        entities.SaveChanges();
                                        uploadSuccess = true;
                                    }
                                    if (uploadSuccess)
                                    {
                                        vsDocumentValid.IsValid = false;
                                        vsDocumentValid.ErrorMessage = "File Upload Successfully";
                                        //BindFolderData();
                                        //bindPageNumber();
                                        //int count = Convert.ToInt32(GetTotalPagesCount());
                                        //if (count > 0)
                                        //{
                                        //    int gridindex = grdFolderDetail.PageIndex;
                                        //    string chkcindition = (gridindex + 1).ToString();
                                        //    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                                        //}
                                    }
                                    else
                                    {
                                        vsDocumentValid.IsValid = false;
                                        vsDocumentValid.ErrorMessage = "Something went wrong, Please try again";
                                    }
                                    fileList.Clear();
                                }
                            }
                            else
                            {
                                vsDocumentValid.IsValid = false;
                                vsDocumentValid.ErrorMessage = "File Size Should be less than 10MB.";
                            }
                        }//End For Each

                        BindFolderData();
                        bindPageNumber();
                        int count = Convert.ToInt32(GetTotalPagesCount());
                        if (count > 0)
                        {
                            int gridindex = grdFolderDetail.PageIndex;
                            string chkcindition = (gridindex + 1).ToString();
                            DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                        }
                    }
                }

                #endregion

                return uploadSuccess;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return uploadSuccess;
            }
        }
        protected void btnCreate_Click(object sender, EventArgs e)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    List<string> objstring = new List<string>();
                    long customerID = -1;
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                    long CurrentFID = Convert.ToInt64(ViewState["ExistFolderID"]);
                    int parentID = 0;
                    do
                    {
                        var FolderDetail = (from row in entities.Mst_FolderMaster
                                            where row.IsDeleted == false && row.CustomerID == customerID
                                            && row.ID == CurrentFID
                                            select row).FirstOrDefault();

                        if (FolderDetail != null)
                        {
                            objstring.Add(FolderDetail.Name.ToString());
                            if (FolderDetail.ParentID != null)
                            {
                                parentID = Convert.ToInt32(FolderDetail.ParentID);
                                CurrentFID = parentID;
                            }
                            else
                            {
                                CurrentFID = 0;
                            }
                        }

                    } while (CurrentFID > 0);

                    string FinalFolderPath = "~/DriveFiles/" + customerID + "/";

                    for (int i = (objstring.Count - 1); i >= 0; i--)
                    {
                        FinalFolderPath = FinalFolderPath + objstring[i] + "/";
                    }

                    string UserNameFolder = txtFolderName.Text.ToString();

                    string CreateNewFolder = FinalFolderPath + UserNameFolder;

                    if (!Directory.Exists(Server.MapPath(CreateNewFolder)))
                    {
                        Directory.CreateDirectory(Server.MapPath(CreateNewFolder));
                        long CreatedFID = -1;

                        Mst_FolderMaster ObjFolder = new Mst_FolderMaster();
                        ObjFolder.Name = UserNameFolder.ToString();
                        ObjFolder.CustomerID = customerID;
                        ObjFolder.ParentID = Convert.ToInt64(ViewState["ExistFolderID"]);
                        ObjFolder.CreatedBy = Convert.ToInt64(AuthenticationHelper.UserID);
                        ObjFolder.CreatedOn = DateTime.Now;
                        ObjFolder.IsDeleted = false;
                        ObjFolder.UpdatedBy = Convert.ToInt64(AuthenticationHelper.UserID);
                        ObjFolder.UpdatedOn = DateTime.Now;
                        entities.Mst_FolderMaster.Add(ObjFolder);
                        entities.SaveChanges();
                        CreatedFID = Convert.ToInt64(ObjFolder.ID);

                        if (CreatedFID > 0)
                        {
                            UserFolderPermission objFP = new UserFolderPermission()
                            {
                                FolderID = Convert.ToInt64(CreatedFID),
                                FileID = null,
                                CreatedBy = Convert.ToInt64(AuthenticationHelper.UserID),
                                CreatedOn = DateTime.Now,
                                IsDeleted = false,
                                UserID = Convert.ToInt64(AuthenticationHelper.UserID),
                                CustomerID = customerID
                            };
                            entities.UserFolderPermissions.Add(objFP);
                            entities.SaveChanges();
                        }

                        cvMailDocument.IsValid = false;
                        cvMailDocument.ErrorMessage = "Folder Create Succesfully";

                        txtFolderName.Text = "";

                        BindFolderData();
                        bindPageNumber();
                        int count = Convert.ToInt32(GetTotalPagesCount());
                        if (count > 0)
                        {
                            int gridindex = grdFolderDetail.PageIndex;
                            string chkcindition = (gridindex + 1).ToString();
                            DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                        }
                    }
                    else
                    {
                        cvMailDocument.IsValid = false;
                        cvMailDocument.ErrorMessage = "Folder Name already Exists";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvMailDocument.IsValid = false;
                cvMailDocument.ErrorMessage = "Something went wrong, Please try again";
            }
        }
        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalDocShareListRows"].ToString();
                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);
                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdFolderDetail.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                //Reload the Grid
                BindFolderData();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdFolderDetail.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvMailDocument.IsValid = false;
                cvMailDocument.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdFolderDetail.PageIndex = chkSelectedPage - 1;
            grdFolderDetail.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            BindFolderData();
        }
        private void bindSelecedUser(long ExistPermissionFileId)
        {
            ComplianceDBEntities entities = new ComplianceDBEntities();
            long UserID = Convert.ToInt64(AuthenticationHelper.UserID);
            List<ShareDetail> ShareList = new List<ShareDetail>();
            ShareList.Clear();
            long customerID = -1;
            customerID = Convert.ToInt64(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            if (ViewState["Fileorfolder"].ToString().ToLower() == "file")
            {
                UserFolderPermission ExistFileId = (from row in entities.UserFolderPermissions
                                                    where row.ID == ExistPermissionFileId && row.IsDeleted == false
                                                    && row.CreatedBy == UserID && row.CustomerID == customerID
                                                    select row).FirstOrDefault();
                if (ExistFileId != null)
                {
                    long FileId = Convert.ToInt64(ExistFileId.FileID);


                    var PermissionFiledetails = (from row in entities.UserFolderPermissions
                                                 where row.FileID == FileId && row.IsDeleted == false
                                                 && row.UserID != UserID && row.CustomerID == customerID
                                                 select row).ToList();
                    int Cust_Id = Convert.ToInt32(customerID);
                    foreach (var item in PermissionFiledetails)
                    {
                        var lstUsers = (from row in entities.UserCustomerViews
                                        where row.UserID == item.UserID && row.CustomerId == Cust_Id
                                        select row).FirstOrDefault();

                        if (lstUsers != null)
                        {
                            ShareList.Add(new ShareDetail() { UserId = Convert.ToInt32(lstUsers.UserID), UserName = lstUsers.UserName.ToString(), UserPermissionFileId = Convert.ToInt64(item.ID), FileType = "File" });
                        }
                    }
                }
            }
            else
            {
                List<long> FinaluserList = new List<long>();
                List<long> UserIds = new List<long>();
                List<long> FileIDs = new List<long>();
                List<long> FinalOutputList = new List<long>();
                bool flagFileExist = false;

                #region FileShare
                List<long> AllFileIDs = (from row in entities.sp_AllfilesofFolder((int)ExistPermissionFileId)
                                         select (long)row).ToList();
                if (AllFileIDs.Count > 0)
                {

                    foreach (var item in AllFileIDs)
                    {
                        long CheckExistFilePermisssion = Convert.ToInt64(item);
                        var uploadedFileIds = (from row in entities.UserFolderPermissions
                                               where row.FileID == CheckExistFilePermisssion && row.IsDeleted == false
                                               && row.UserID == UserID && row.CustomerID == customerID
                                               select row).ToList();
                        if (uploadedFileIds.Count > 0)
                        {
                            flagFileExist = true;
                            FileIDs.Add(CheckExistFilePermisssion);
                        }
                    }

                    foreach (var item in FileIDs)
                    {
                        long ExistFileID = Convert.ToInt64(item);
                        var uploadedFileIds = (from row in entities.UserFolderPermissions
                                               where row.FileID == ExistFileID && row.IsDeleted == false
                                               && row.UserID != UserID && row.CustomerID == customerID
                                               select row).ToList();
                        foreach (var items in uploadedFileIds)
                        {
                            UserIds.Add(Convert.ToInt64(items.UserID));
                        }
                    }
                    if (UserIds.Count > 0)
                    {
                        UserIds = UserIds.Distinct().ToList();
                    }

                    int Count = Convert.ToInt32(FileIDs.Count);
                    foreach (var ids in UserIds)
                    {
                        long userIdCheck = Convert.ToInt64(ids);
                        int flag = 0;
                        foreach (var flds in FileIDs)
                        {
                            long fileids = Convert.ToInt64(flds);
                            UserFolderPermission uploadedFileIds = (from row in entities.UserFolderPermissions
                                                                    where row.FileID == fileids && row.IsDeleted == false
                                                                    && row.UserID == userIdCheck && row.CustomerID == customerID
                                                                    select row).FirstOrDefault();
                            if (uploadedFileIds != null)
                            {
                                flag = flag + 1;
                            }
                        }
                        if (flag == Count)
                        {
                            FinaluserList.Add(Convert.ToInt64(userIdCheck));
                        }
                    }
                }
                #endregion

                #region folder
                if (flagFileExist == true)
                {
                    long CreatedUserID = (from row in entities.Mst_FolderMaster
                                          where row.ID == ExistPermissionFileId && row.IsDeleted == false
                                          && row.CreatedBy == UserID && row.CustomerID == customerID
                                          select (long)row.CreatedBy).FirstOrDefault();
                    if (CreatedUserID > 0)
                    {
                        List<long> AllFolderIDs = (from row in entities.sp_AllFolder((int)ExistPermissionFileId)
                                                   select (long)row).ToList();
                        if (AllFolderIDs.Count > 0)
                        {
                            int CountFolderIds = Convert.ToInt32(AllFolderIDs.Count);
                            foreach (var itemUserid in FinaluserList)//ckeck user 
                            {
                                long userIdCheck = Convert.ToInt64(itemUserid);
                                int flagFolder = 0;
                                foreach (var itemFolderid in AllFolderIDs)//check folder
                                {
                                    long folderids = Convert.ToInt64(itemFolderid);
                                    UserFolderPermission uploadedFileIds = (from row in entities.UserFolderPermissions
                                                                            where row.FolderID == folderids && row.IsDeleted == false
                                                                            && row.UserID == userIdCheck && row.CustomerID == customerID
                                                                            select row).FirstOrDefault();
                                    if (uploadedFileIds != null)
                                    {
                                        flagFolder = flagFolder + 1;
                                    }
                                }
                                if (flagFolder == CountFolderIds)
                                {
                                    FinalOutputList.Add(Convert.ToInt64(userIdCheck));
                                }
                            }
                        }
                    }
                    int Cust_Id = Convert.ToInt32(customerID);
                    foreach (var item in FinalOutputList)
                    {

                        var lstUsers = (from row in entities.UserCustomerViews
                                        where row.UserID == item && row.CustomerId == Cust_Id
                                        select row).FirstOrDefault();

                        if (lstUsers != null)
                        {
                            ShareList.Add(new ShareDetail() { UserId = Convert.ToInt32(lstUsers.UserID), UserName = lstUsers.UserName.ToString(), UserPermissionFileId = Convert.ToInt64(ExistPermissionFileId), FileType = "Folder" });
                        }
                    }
                }
                else
                {
                    List<long> FinaluserOutputList = new List<long>();
                    List<long> UserFolderIDs = new List<long>();

                    long CreatedUserID = (from row in entities.Mst_FolderMaster
                                          where row.ID == ExistPermissionFileId && row.IsDeleted == false
                                          && row.CreatedBy == UserID && row.CustomerID == customerID
                                          select (long)row.CreatedBy).FirstOrDefault();
                    if (CreatedUserID > 0)
                    {
                        List<long> AllFolderIDs = (from row in entities.sp_AllFolder((int)ExistPermissionFileId)
                                                   select (long)row).ToList();
                        if (AllFolderIDs.Count > 0)
                        {
                            foreach (var item in AllFolderIDs)
                            {
                                long CheckExistFolderPermisssion = Convert.ToInt64(item);
                                var uploadedFOlderUserIds = (from row in entities.UserFolderPermissions
                                                             where row.FolderID == CheckExistFolderPermisssion && row.IsDeleted == false
                                                             && row.CustomerID == customerID && row.FileID == null && row.UserID != UserID
                                                             select row).ToList();
                                foreach (var itemFld in uploadedFOlderUserIds)
                                {
                                    long UIdFloder = Convert.ToInt64(itemFld.UserID);
                                    UserFolderIDs.Add(UIdFloder);
                                }
                            }
                            if (UserFolderIDs.Count > 0)
                            {
                                UserFolderIDs = UserFolderIDs.Distinct().ToList();
                            }

                            int CountFLD = Convert.ToInt32(AllFolderIDs.Count);
                            foreach (var ids in UserFolderIDs)
                            {
                                long userIdCheck = Convert.ToInt64(ids);
                                int flag = 0;
                                foreach (var flds in AllFolderIDs)
                                {
                                    long fileids = Convert.ToInt64(flds);
                                    UserFolderPermission uploadedFileIds = (from row in entities.UserFolderPermissions
                                                                            where row.FolderID == fileids && row.IsDeleted == false
                                                                            && row.UserID == userIdCheck && row.CustomerID == customerID
                                                                            select row).FirstOrDefault();
                                    if (uploadedFileIds != null)
                                    {
                                        flag = flag + 1;
                                    }
                                }
                                if (flag == CountFLD)
                                {
                                    FinaluserOutputList.Add(Convert.ToInt64(userIdCheck));
                                }
                            }
                            int Cust_Id = Convert.ToInt32(customerID);
                            foreach (var item in FinaluserOutputList)
                            {
                                var lstUsers = (from row in entities.UserCustomerViews
                                                where row.UserID == item && row.CustomerId == Cust_Id
                                                select row).FirstOrDefault();

                                if (lstUsers != null)
                                {
                                    ShareList.Add(new ShareDetail() { UserId = Convert.ToInt32(lstUsers.UserID), UserName = lstUsers.UserName.ToString(), UserPermissionFileId = Convert.ToInt64(ExistPermissionFileId), FileType = "Folder" });
                                }
                            }
                        }
                    }
                }
                #endregion
                //List<long> FinaluserList = new List<long>();
                //List<long> UserIds = new List<long>();
                //List<long> FileIDs = new List<long>();
                //List<long> AllFileIDs = (from row in entities.sp_AllfilesofFolder((int) ExistPermissionFileId)
                //                         select (long) row).ToList();

                //foreach (var item in AllFileIDs)
                //{
                //    long CheckExistFilePermisssion = Convert.ToInt64(item);
                //    var uploadedFileIds = (from row in entities.UserFolderPermissions
                //                           where row.FileID == CheckExistFilePermisssion && row.IsDeleted == false
                //                           && row.UserID == UserID && row.CustomerID == customerID
                //                           select row).ToList();
                //    if (uploadedFileIds.Count > 0)
                //    {
                //        FileIDs.Add(CheckExistFilePermisssion);
                //    }
                //}

                //foreach (var item in FileIDs)
                //{
                //    long ExistFileID = Convert.ToInt64(item);
                //    var uploadedFileIds = (from row in entities.UserFolderPermissions
                //                           where row.FileID == ExistFileID && row.IsDeleted == false
                //                           && row.UserID != UserID && row.CustomerID == customerID
                //                           select row).ToList();
                //    foreach (var items in uploadedFileIds)
                //    {
                //        UserIds.Add(Convert.ToInt64(items.UserID));
                //    }
                //}
                //if (UserIds.Count > 0)
                //{
                //    UserIds = UserIds.Distinct().ToList();
                //}

                //int Count = Convert.ToInt32(FileIDs.Count);
                //foreach (var ids in UserIds)
                //{
                //    long userIdCheck = Convert.ToInt64(ids);
                //    int flag = 0;
                //    foreach (var flds in FileIDs)
                //    {
                //        long fileids = Convert.ToInt64(flds);
                //        UserFolderPermission uploadedFileIds = (from row in entities.UserFolderPermissions
                //                                                where row.FileID == fileids && row.IsDeleted == false
                //                                                && row.UserID == userIdCheck && row.CustomerID == customerID
                //                                                select row).FirstOrDefault();
                //        if (uploadedFileIds != null)
                //        {
                //            flag = flag + 1;
                //        }
                //    }
                //    if (flag == Count)
                //    {
                //        FinaluserList.Add(Convert.ToInt64(userIdCheck));
                //    }
                //}

                //foreach (var item in FinaluserList)
                //{
                //    var lstUsers = (from row in entities.UserCustomerViews
                //                    where row.UserID == item
                //                    select row).FirstOrDefault();

                //    if (lstUsers != null)
                //    {
                //        ShareList.Add(new ShareDetail() { UserId = Convert.ToInt32(lstUsers.UserID), UserName = lstUsers.UserName.ToString(), UserPermissionFileId = Convert.ToInt64(ExistPermissionFileId), FileType = "Folder" });
                //    }
                //}
            }
            myRepeater.DataSource = ShareList;
            myRepeater.DataBind();

        }
        protected void grdFolderDetail_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Goto_Subfolder"))
                {
                    long FolderID = Convert.ToInt64(e.CommandArgument);
                    ViewState["ExistFolderID"] = FolderID;
                    BindFolderData();
                }
                else if (e.CommandName.Equals("ShareFile"))
                {
                    string[] str = e.CommandArgument.ToString().Split(',');
                    long FileID = Convert.ToInt64(str[0]);
                    ViewState["ExistFileIDShare"] = FileID;
                    ViewState["Fileorfolder"] = str[1];
                    bindSelecedUser(FileID);
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenNewPermissionPopup();", true);
                }
                else if (e.CommandName.Equals("ViewDoc"))
                {
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    long FileID = Convert.ToInt64(commandArgs[0]);
                    string Type = commandArgs[1];
                    ViewState["ExistFileIDShare"] = FileID;
                    ViewState["ExistFileType"] = Type;

                    if (!(string.IsNullOrEmpty(Type)))
                    {
                        if (Type.Equals("File"))
                        {
                            var file = ComplianceManagement.Business.ComplianceManagement.GetDocumentByID(FileID);
                            string CompDocReviewPath = "";
                            if (file != null)
                            {
                                if (file.FilePath != null)
                                {
                                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        string Folder = "~/TempFiles";
                                        string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                                        string DateFolder = Folder + "/" + File;
                                        string extension = System.IO.Path.GetExtension(filePath);
                                        Directory.CreateDirectory(Server.MapPath(DateFolder));
                                        if (!Directory.Exists(DateFolder))
                                        {
                                            Directory.CreateDirectory(Server.MapPath(DateFolder));
                                        }
                                        string customerID = Convert.ToString(Convert.ToInt32(AuthenticationHelper.CustomerID));
                                        string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                        string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;
                                        string FileName = DateFolder + "/" + User + "" + extension;
                                        FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                        BinaryWriter bw = new BinaryWriter(fs);
                                        bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        bw.Close();

                                        CompDocReviewPath = FileName;
                                        CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReview('" + CompDocReviewPath + "');", true);
                                        lblMessage.Text = "";
                                        //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenDocumentViewPopup();", true);
                                    }
                                    else
                                    {
                                        lblMessage.Text = "There is no file to preview";
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);

                                    }
                                }
                            }
                        }
                    }
                }
                else if (e.CommandName.Equals("InfoDoc"))
                {
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    long FileID = Convert.ToInt64(commandArgs[0]);
                    string Type = commandArgs[1];

                    ViewState["ExistFileIDShare"] = FileID;
                    ViewState["ExistFileType"] = Type;

                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenNewInfoPopup();", true);

                    BindInformationData();
                }
                else if (e.CommandName.Equals("DownloadDoc"))
                {
                    bool downloadSuccess = DownloadDocument(Convert.ToInt64(e.CommandArgument));

                    if (!downloadSuccess)
                    {
                        //cvContractDocument.ErrorMessage = "Something went wrong, Please try again";
                    }
                }
                else if (e.CommandName.Equals("DeleteDoc"))
                {
                    bool success = false;
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    long FileID = Convert.ToInt64(commandArgs[0]);
                    string Type = commandArgs[1];
                    if (Type.Equals("File"))
                    {
                        success = DeleteFileDocument(FileID);
                    }
                    else
                    {
                        success = DeleteFolderDocument(FileID);
                    }
                    if (success)
                    {
                        BindFolderData();
                        bindPageNumber();
                        int count = Convert.ToInt32(GetTotalPagesCount());
                        if (count > 0)
                        {
                            int gridindex = grdFolderDetail.PageIndex;
                            string chkcindition = (gridindex + 1).ToString();
                            DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                        }
                        //cvContractDocument.ErrorMessage = "Something went wrong, Please try again";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public bool DeleteFolderDocument(long FolderPermissionId)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
                long UserID = Convert.ToInt64(AuthenticationHelper.UserID);
                string UserNameFolder = string.Empty;
                bool result = false;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    #region Folder Deleted 
                    Mst_FolderMaster folderpermisionCheck = (from row in entities.Mst_FolderMaster
                                                             where row.ID == FolderPermissionId && row.IsDeleted == false
                                                             && row.CustomerID == customerID && row.CreatedBy == UserID
                                                             select row).FirstOrDefault();
                    if (folderpermisionCheck != null)
                    {
                        List<long> AllFolderIDs = (from row in entities.sp_AllFolder((int)FolderPermissionId)
                                                   select (long)row).ToList();
                        if (AllFolderIDs.Count > 0)
                        {
                            foreach (var item in AllFolderIDs)
                            {
                                var uploadedFileIds = (from row in entities.UserFolderPermissions
                                                       where row.FolderID == item && row.IsDeleted == false
                                                       && row.CreatedBy == UserID && row.FileID == null
                                                       select row).ToList();
                                foreach (var itemdelFolder in uploadedFileIds)
                                {
                                    long updatepermissionid = Convert.ToInt64(itemdelFolder.ID);
                                    UserFolderPermission unsharedoc = (from row in entities.UserFolderPermissions
                                                                       where row.FolderID == item && row.IsDeleted == false
                                                                       && row.ID == updatepermissionid && row.FileID == null
                                                                       && row.CreatedBy == UserID
                                                                       select row).FirstOrDefault();
                                    if (unsharedoc != null)
                                    {
                                        unsharedoc.IsDeleted = true;
                                        entities.SaveChanges();
                                    }
                                }
                            }
                        }
                    }
                    #endregion

                    #region file deleted inside folder
                    List<long> AllFileIDs = (from row in entities.sp_AllfilesofFolder((int)FolderPermissionId)
                                             select (long)row).ToList();

                    foreach (var item in AllFileIDs)
                    {
                        long CheckExistFilePermisssion = Convert.ToInt64(item);
                        var uploadedFileIds = (from row in entities.UserFolderPermissions
                                               where row.FileID == CheckExistFilePermisssion && row.IsDeleted == false
                                               && row.CreatedBy == UserID
                                               select row).ToList();
                        foreach (var itemx in uploadedFileIds)
                        {
                            long updatepermissionid = Convert.ToInt64(itemx.ID);
                            UserFolderPermission unsharedoc = (from row in entities.UserFolderPermissions
                                                               where row.FileID == CheckExistFilePermisssion && row.IsDeleted == false
                                                               && row.ID == updatepermissionid
                                                               && row.CreatedBy == UserID
                                                               select row).FirstOrDefault();
                            if (unsharedoc != null)
                            {
                                unsharedoc.IsDeleted = true;
                                entities.SaveChanges();
                            }
                        }
                    }
                    #endregion

                    Mst_FolderMaster folderpermision = (from row in entities.Mst_FolderMaster
                                                        where row.ID == FolderPermissionId && row.IsDeleted == false
                                                        && row.CustomerID == customerID
                                                        select row).FirstOrDefault();
                    if (folderpermision != null)
                    {
                        if (folderpermision.CreatedBy == UserID)
                        {
                            folderpermision.IsDeleted = true;
                            entities.SaveChanges();
                            UserNameFolder = folderpermision.Name;
                            result = true;


                            if (result)
                            {
                                string FinalFolderPath = "~/DriveFiles/" + customerID + "/";

                                string DeleteFolder = FinalFolderPath + UserNameFolder;

                                string date = DateTime.Now.ToString("ddd MM.dd.yyyy");
                                string time = DateTime.Now.ToString("HH.mm tt");
                                string newname = Server.MapPath(DeleteFolder + "_Delete_" + date + "_" + time);

                                if (Directory.Exists(Server.MapPath(DeleteFolder)))
                                {
                                    Directory.Move(Server.MapPath(DeleteFolder), newname);
                                }
                            }
                        }
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }
        //public bool DeleteFolderDocument(long FolderPermissionId)
        //{
        //    try
        //    {
        //        long UserID = Convert.ToInt64(AuthenticationHelper.UserID);
        //        long customerID = -1;
        //        customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
        //        string UserNameFolder = string.Empty;
        //        bool result = false;
        //        using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //        {

        //            List<long> AllFileIDs = (from row in entities.sp_AllfilesofFolder((int) FolderPermissionId)
        //                                     select (long) row).ToList();

        //            foreach (var item in AllFileIDs)
        //            {
        //                long CheckExistFilePermisssion = Convert.ToInt64(item);
        //                var uploadedFileIds = (from row in entities.UserFolderPermissions
        //                                       where row.FileID == CheckExistFilePermisssion && row.IsDeleted == false
        //                                       && row.CreatedBy == UserID
        //                                       select row).ToList();
        //                foreach (var itemx in uploadedFileIds)
        //                {
        //                    long updatepermissionid = Convert.ToInt64(itemx.ID);
        //                    UserFolderPermission unsharedoc = (from row in entities.UserFolderPermissions
        //                                                       where row.FileID == CheckExistFilePermisssion && row.IsDeleted == false
        //                                                       && row.ID == updatepermissionid
        //                                                       && row.CreatedBy == UserID
        //                                                       select row).FirstOrDefault();
        //                    if (unsharedoc != null)
        //                    {
        //                        unsharedoc.IsDeleted = true;
        //                        entities.SaveChanges();
        //                    }
        //                }
        //            }


        //            Mst_FolderMaster folderpermision = (from row in entities.Mst_FolderMaster
        //                                                where row.ID == FolderPermissionId && row.IsDeleted == false
        //                                                  && row.CustomerID == customerID 
        //                                                select row).FirstOrDefault();
        //            if (folderpermision != null)
        //            {
        //                if (folderpermision.CreatedBy == UserID)
        //                {
        //                    folderpermision.IsDeleted = true;
        //                    entities.SaveChanges();
        //                    UserNameFolder = folderpermision.Name;
        //                    result = true;

        //                    if (result)
        //                    {
        //                        List<string> objstring = new List<string>();
        //                        long CurrentFID = Convert.ToInt64(ViewState["ExistFolderID"]);
        //                        int parentID = 0;
        //                        do
        //                        {
        //                            var FolderDetail = (from row in entities.Mst_FolderMaster
        //                                                where row.IsDeleted == false && row.CustomerID == customerID
        //                                                && row.ID == CurrentFID
        //                                                select row).FirstOrDefault();

        //                            if (FolderDetail != null)
        //                            {
        //                                objstring.Add(FolderDetail.Name.ToString());
        //                                if (FolderDetail.ParentID != null)
        //                                {
        //                                    parentID = Convert.ToInt32(FolderDetail.ParentID);
        //                                    CurrentFID = parentID;
        //                                }
        //                                else
        //                                {
        //                                    CurrentFID = 0;
        //                                }
        //                            }

        //                        } while (CurrentFID > 0);

        //                        string FinalFolderPath = "~/DriveFiles/" + customerID + "/";

        //                        for (int i = (objstring.Count - 1); i >= 0; i--)
        //                        {
        //                            FinalFolderPath = FinalFolderPath + objstring[i] + "/";
        //                        }
        //                        string DeleteFolder = FinalFolderPath + UserNameFolder;

        //                        string date = DateTime.Now.ToString("ddd MM.dd.yyyy");
        //                        string time = DateTime.Now.ToString("HH.mm tt");
        //                        string newname = Server.MapPath(DeleteFolder + "_Delete_" + date + "_" + time);

        //                        if (Directory.Exists(Server.MapPath(DeleteFolder)))
        //                        {
        //                            Directory.Move(Server.MapPath(DeleteFolder), newname);
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //        return result;
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        return false;
        //    }
        //}
        public bool DeleteFileDocument(long FilePermissionId)
        {
            try
            {
                long UserID = Convert.ToInt64(AuthenticationHelper.UserID);
                bool result = false;
                int customerID = -1;
                customerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    UserFolderPermission filepermision = (from row in entities.UserFolderPermissions
                                                          where row.ID == FilePermissionId && row.IsDeleted == false
                                                          && row.CustomerID == customerID
                                                          select row).FirstOrDefault();
                    if (filepermision != null)
                    {
                        if (filepermision.CreatedBy == UserID)
                        {
                            long existFileId = Convert.ToInt64(filepermision.FileID);
                            var DeletedDatas = (from row in entities.UserFolderPermissions
                                                where row.FileID == existFileId && row.IsDeleted == false
                                                 && row.CustomerID == customerID
                                                select row).ToList();

                            foreach (var item in DeletedDatas)
                            {
                                long ids = Convert.ToInt64(item.ID);
                                UserFolderPermission deleteids = (from row in entities.UserFolderPermissions
                                                                  where row.ID == ids && row.IsDeleted == false
                                                                   && row.CustomerID == customerID
                                                                  select row).FirstOrDefault();
                                if (deleteids != null)
                                {
                                    deleteids.IsDeleted = true;
                                    entities.SaveChanges();
                                    result = true;
                                }
                            }
                        }
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }
        public string ShowUserName(string CreatedById)
        {
            try
            {
                ComplianceDBEntities entities = new ComplianceDBEntities();
                int customerID = -1;
                customerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);

                long getUserID = Convert.ToInt64(CreatedById);

                if (getUserID > 0)
                {
                    if (Convert.ToInt64(AuthenticationHelper.UserID) == getUserID)
                    {
                        return "me";
                    }
                    else
                    {
                        var UserObj = (from row in entities.UserCustomerViews
                                       where row.UserID == getUserID && row.CustomerId == customerID
                                       select row).FirstOrDefault();
                        if (UserObj != null)
                        {
                            return UserObj.UserName;
                        }
                        return string.Empty;
                    }
                }
                else
                    return string.Empty;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return string.Empty;
            }
        }
        protected void grdFolderDetail_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ComplianceDBEntities entities = new ComplianceDBEntities();

                long customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                long C_UserId = Convert.ToInt64(AuthenticationHelper.UserID);

                GridView gridView = (GridView)sender;

                if (gridView != null)
                {
                    if (e.Row.RowType == DataControlRowType.DataRow)
                    {
                        TextBox tbxTypeValue = (TextBox)e.Row.FindControl("tbxTypeValue");
                        ImageButton LnkShare = (ImageButton)e.Row.FindControl("LnkShare");
                        ImageButton lnkViewDoc = (ImageButton)e.Row.FindControl("lnkViewDoc");
                        if (tbxTypeValue.Text != null)
                        {
                            if (tbxTypeValue.Text.Equals("File"))
                            {
                                TextBox tbxCreatedByValue = (TextBox)e.Row.FindControl("tbxCreatedByValue");
                                if (tbxCreatedByValue.Text != null)
                                {
                                    long createdBy = Convert.ToInt64(tbxCreatedByValue.Text);

                                    if (LnkShare != null)
                                    {
                                        if (createdBy == C_UserId)
                                        {
                                            LnkShare.Visible = true;
                                        }
                                        else
                                        {
                                            LnkShare.Visible = false;
                                        }
                                    }
                                }


                                TextBox tbxIDFile = (TextBox)e.Row.FindControl("tbxIDFile");

                                if (tbxIDFile.Text != null)
                                {
                                    long FID = Convert.ToInt64(tbxIDFile.Text);
                                    long CFID = Convert.ToInt64(ViewState["ExistFolderID"]);

                                    UserFolderPermission objPermissionFile = (from row in entities.UserFolderPermissions
                                                                              where row.ID == FID && row.FolderID == CFID
                                                                                   && row.CustomerID == customerID && row.UserID == C_UserId
                                                                                   && row.IsDeleted == false
                                                                              select row).FirstOrDefault();
                                    if (objPermissionFile != null)
                                    {
                                        if (objPermissionFile.IsRead == true || objPermissionFile.IsWrite == true)
                                        {
                                            lnkViewDoc.Visible = true;
                                        }
                                    }
                                    else
                                    {
                                        lnkViewDoc.Visible = false;
                                    }
                                }
                                else
                                {
                                    lnkViewDoc.Visible = false;
                                }
                                //Show File Image Based on Extension
                                Label lblName = (Label)e.Row.FindControl("lblName");
                                ImageButton ImgfileType = (ImageButton)e.Row.FindControl("ImgfileType");
                                if (lblName != null && ImgfileType != null)
                                {
                                    if (!string.IsNullOrEmpty(lblName.Text))
                                    {
                                        string fileExtension = Path.GetExtension(lblName.Text);

                                        if (!string.IsNullOrEmpty(fileExtension))
                                        {
                                            string[] imageFilesTypes = { ".bmp", ".jpg", ".jpe", ".jpeg", ".gif", ".tif", ".tiff", ".png" };

                                            if (fileExtension.Equals(".pdf", StringComparison.OrdinalIgnoreCase))
                                            {
                                                ImgfileType.ImageUrl = "~/Images/pdfdoc.png";
                                            }
                                            else if (fileExtension.Equals(".doc", StringComparison.OrdinalIgnoreCase) || fileExtension.Equals(".docx", StringComparison.OrdinalIgnoreCase))
                                            {
                                                ImgfileType.ImageUrl = "~/Images/WordDoc.png";
                                            }
                                            else if (fileExtension.Equals(".ppt", StringComparison.OrdinalIgnoreCase) || fileExtension.Equals(".pptx", StringComparison.OrdinalIgnoreCase))
                                            {
                                                ImgfileType.ImageUrl = "~/Images/ppt.png";
                                            }
                                            else if (fileExtension.Equals(".xls", StringComparison.OrdinalIgnoreCase) || fileExtension.Equals(".xlsx", StringComparison.OrdinalIgnoreCase))
                                            {
                                                ImgfileType.ImageUrl = "~/Images/exceldoc.png";
                                            }
                                            else if (imageFilesTypes.Contains(fileExtension))
                                            {
                                                ImgfileType.ImageUrl = "~/Images/image.png";
                                            }
                                            else
                                            {
                                                ImgfileType.ImageUrl = "~/Images/File.png";
                                            }
                                        }
                                    }
                                }

                            }
                            else
                            {
                                //LnkShare.Visible = false;
                                lnkViewDoc.Visible = false;
                            }
                        }
                        else
                        {
                            //LnkShare.Visible = false;
                            lnkViewDoc.Visible = false;
                        }


                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public bool DownloadDocument(long DocFileID)
        {
            bool downloadSuccess = false;
            try
            {
                var file = ComplianceManagement.Business.ComplianceManagement.GetDocumentByID(DocFileID);

                if (file != null)
                {
                    if (file.FilePath != null)
                    {
                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));

                        if (filePath != null && File.Exists(filePath))
                        {
                            Response.Buffer = true;
                            Response.Clear();
                            Response.ClearContent();
                            Response.ClearHeaders();
                            Response.ContentType = "application/octet-stream";
                            Response.AddHeader("content-disposition", "attachment; filename=" + DocumentManagement.MakeValidFileName(file.Name));
                            Response.BinaryWrite(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                            downloadSuccess = true;
                        }
                    }
                }

                return downloadSuccess;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return downloadSuccess;
            }
        }
        protected void lnkMyDrive_Click(object sender, EventArgs e)
        {
            string Filter = tbxFilter.Text.ToString();
            Response.Redirect("~/Areas_Secretarial/MyDriveLevel1.aspx?FilterSearch=" + Filter);
        }

        private void BindInformationData()
        {
            try
            {
                long FilePermissionId = Convert.ToInt64(ViewState["ExistFileIDShare"]);
                string TypeFile = ViewState["ExistFileType"].ToString();


                if (!(string.IsNullOrEmpty(TypeFile)))
                {
                    if (TypeFile.Equals("File"))
                    {
                        var fileInfo = ComplianceManagement.Business.ComplianceManagement.GetDocumentByID(FilePermissionId);

                        if (fileInfo != null)
                        {
                            long FileID = Convert.ToInt64(fileInfo.ID);
                            if (FileID > 0)
                            {
                                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                {
                                    //btnUpdateInfo.Visible = true;
                                    //TxtOthers.Visible = true;
                                    //TxtVertical.Visible = true;
                                    //TxtSubProcess.Visible = true;
                                    //TxtProcess.Visible = true;
                                    //TxtDocHeader.Visible = true;
                                    //TxtDescription.Visible = true;
                                    //LblDescription.Visible = true;
                                    //LblHeader.Visible = true;
                                    //lblOthers.Visible = true;
                                    //lblProcess.Visible = true;
                                    //lblSubProcess.Visible = true;
                                    //lblVertical.Visible = true;

                                    FolderFileData filepermision = (from row in entities.FolderFileDatas
                                                                    where row.ID == FileID && row.IsDeleted == false
                                                                    select row).FirstOrDefault();
                                    if (filepermision != null)
                                    {
                                        TxtDocName.Text = filepermision.Name.ToString();
                                        TxtDocType.Text = Path.GetExtension(filepermision.Name.ToString()).Replace(".", "");

                                        if (!string.IsNullOrEmpty(filepermision.Doc_Header))
                                            TxtDocHeader.Text = filepermision.Doc_Header.ToString();

                                        if (!string.IsNullOrEmpty(filepermision.Doc_Description))
                                            TxtDescription.Text = filepermision.Doc_Description.ToString();

                                        if (!string.IsNullOrEmpty(filepermision.Others))
                                            TxtOthers.Text = filepermision.Others.ToString();

                                        if (!string.IsNullOrEmpty(filepermision.Process))
                                            TxtProcess.Text = filepermision.Process.ToString();

                                        if (!string.IsNullOrEmpty(filepermision.SubProcess))
                                            TxtSubProcess.Text = filepermision.SubProcess.ToString();

                                        if (!string.IsNullOrEmpty(filepermision.Vertical))
                                            TxtVertical.Text = filepermision.Vertical.ToString();

                                        if (!string.IsNullOrEmpty(filepermision.CreatedBy.ToString()))
                                            TxtDocCreatedby.Text = ShowUserName(filepermision.CreatedBy.ToString());

                                        if (!string.IsNullOrEmpty(filepermision.CreatedOn.ToString()))
                                            TxtDocCreatedon.Text = filepermision.CreatedOn.ToString();

                                        if (!string.IsNullOrEmpty(filepermision.UpdatedOn.ToString()))
                                            TxtDocModifiedDate.Text = filepermision.UpdatedOn.ToString();

                                        if (!string.IsNullOrEmpty(filepermision.FileCreatedDate.ToString()))
                                            TxtBxFileCreated.Text = filepermision.FileCreatedDate.ToString();

                                        if (!string.IsNullOrEmpty(filepermision.Location))
                                            TxtLocation.Text = filepermision.Location.ToString();


                                        var objtag = (from row in entities.FileDataTagsMappings
                                                      where row.FileID == FileID && row.IsActive == true
                                                      select row).ToList();
                                        string collectTag = string.Empty;
                                        int i = 0;
                                        foreach (var item in objtag)
                                        {
                                            if (i == 0)
                                            {
                                                collectTag = item.FileTag.ToString();
                                            }
                                            else
                                            {
                                                collectTag = collectTag + "," + item.FileTag.ToString();
                                            }
                                            i = 1;
                                        }

                                        txtDocTags.Text = collectTag;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        //using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        //{
                        //    btnUpdateInfo.Visible = false;
                        //    TxtOthers.Visible = false;
                        //    TxtVertical.Visible = false;
                        //    TxtSubProcess.Visible = false;
                        //    TxtProcess.Visible = false;
                        //    TxtDocHeader.Visible = false;
                        //    TxtDescription.Visible = false;

                        //    LblDescription.Visible = false;
                        //    LblHeader.Visible = false;
                        //    lblOthers.Visible = false;
                        //    lblProcess.Visible = false;
                        //    lblSubProcess.Visible = false;
                        //    lblVertical.Visible = false;



                        //    Mst_FolderMaster folderpermision = (from row in entities.Mst_FolderMaster
                        //                                        where row.ID == FilePermissionId && row.IsDeleted == false
                        //                                        select row).FirstOrDefault();
                        //    if (folderpermision != null)
                        //    {
                        //        TxtDocName.Text = folderpermision.Name.ToString();
                        //        TxtDocType.Text = "Google Drive Folder";
                        //        TxtDocCreatedby.Text = ShowUserName(folderpermision.CreatedBy.ToString());
                        //        TxtDocCreatedon.Text = folderpermision.CreatedOn.ToString();
                        //        TxtDocModifiedDate.Text = folderpermision.UpdatedOn.ToString();


                        //        TxtDocHeader.Text = "";
                        //        TxtDescription.Text = "";
                        //        TxtOthers.Text = "";
                        //        TxtProcess.Text = "";
                        //        TxtSubProcess.Text = "";
                        //        TxtVertical.Text = "";
                        //    }
                        //}
                    }

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnUpdateInfo_Click(object sender, EventArgs e)
        {
            try
            {
                long FilePermissionId = Convert.ToInt64(ViewState["ExistFileIDShare"]);
                string TypeFile = ViewState["ExistFileType"].ToString();
                int customerID = -1;
                customerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);

                var fileInfo = ComplianceManagement.Business.ComplianceManagement.GetDocumentByID(FilePermissionId);

                if (fileInfo != null)
                {
                    long FileID = Convert.ToInt64(fileInfo.ID);
                    if (FileID > 0)
                    {
                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        {
                            FolderFileData filepermision = (from row in entities.FolderFileDatas
                                                            where row.ID == FileID && row.IsDeleted == false
                                                             && row.CustomerID == customerID
                                                            select row).FirstOrDefault();
                            if (filepermision != null)
                            {
                                filepermision.Doc_Header = TxtDocHeader.Text.ToString();
                                filepermision.Doc_Description = TxtDescription.Text.ToString();
                                filepermision.Others = TxtOthers.Text.ToString();
                                filepermision.Process = TxtProcess.Text.ToString();
                                filepermision.SubProcess = TxtSubProcess.Text.ToString();
                                filepermision.Vertical = TxtVertical.Text.ToString();
                                filepermision.UpdatedBy = Convert.ToInt64(AuthenticationHelper.UserID);
                                filepermision.UpdatedOn = DateTime.Now;
                                if (!(string.IsNullOrEmpty(TxtBxFileCreated.Text)))
                                {
                                    filepermision.FileCreatedDate = Convert.ToDateTime(TxtBxFileCreated.Text.ToString());
                                }
                                filepermision.Location = TxtLocation.Text.ToString();
                                entities.SaveChanges();


                                //Add at time of Upload(aspx.cs)
                                if (!(string.IsNullOrEmpty(txtDocTags.Text)))
                                {
                                    string[] arrFileTags = txtDocTags.Text.Trim().Split(',');

                                    if (arrFileTags.Length > 0)
                                    {
                                        List<FileDataTagsMapping> lstFileTagMapping = new List<FileDataTagsMapping>();

                                        for (int j = 0; j < arrFileTags.Length; j++)
                                        {
                                            FileDataTagsMapping objFileTagMapping = new FileDataTagsMapping()
                                            {
                                                FileID = FileID,
                                                FileTag = arrFileTags[j].Trim(),
                                                IsActive = true,
                                                CreatedBy = AuthenticationHelper.UserID,
                                                CreatedOn = DateTime.Now,
                                                UpdatedBy = AuthenticationHelper.UserID,
                                                UpdatedOn = DateTime.Now,
                                            };

                                            lstFileTagMapping.Add(objFileTagMapping);
                                        }

                                        if (lstFileTagMapping.Count > 0)
                                        {
                                            ComplianceManagement.Business.ComplianceManagement.CreateUpdate_FileTagsMapping(lstFileTagMapping, FileID);
                                            //ContractDocumentManagement.CreateUpdate_FileTagsMapping(lstFileTagMapping, FileID);
                                        }
                                    }
                                }


                                InfoValidator.IsValid = false;
                                InfoValidator.ErrorMessage = "Information Uploaded Successfully";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                InfoValidator.IsValid = false;
                InfoValidator.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        public string ShowFileType(string Name, string Type)
        {
            try
            {
                string Anstype = string.Empty;
                if (!string.IsNullOrEmpty(Name) && !string.IsNullOrEmpty(Type))
                {
                    if (Type.Equals("File"))
                    {
                        Anstype = Path.GetExtension(Name.ToString()).Replace(".", "");
                        return Anstype;
                    }
                    else
                    {
                        return Anstype;
                    }
                }
                else
                    return Anstype;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return string.Empty;
            }
        }
        public string ShowFileSize(long FilePermissionId, string Type)
        {
            try
            {
                ComplianceDBEntities entities = new ComplianceDBEntities();
                long customerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
                long UserID = Convert.ToInt64(AuthenticationHelper.UserID);

                string Anstype = string.Empty;
                if (FilePermissionId > 0 && !string.IsNullOrEmpty(Type))
                {
                    if (Type.Equals("File"))
                    {
                        UserFolderPermission objFileData = (from row in entities.UserFolderPermissions
                                                            where row.CustomerID == customerID && row.ID == FilePermissionId
                                                            && row.IsDeleted == false
                                                            select row).FirstOrDefault();
                        if (objFileData != null)
                        {
                            long FileID = Convert.ToInt64(objFileData.FileID);

                            FolderFileData FileSizeData = (from row in entities.FolderFileDatas
                                                           where row.CustomerID == customerID && row.ID == FileID
                                                           && row.IsDeleted == false
                                                           select row).FirstOrDefault();
                            if (FileSizeData != null)
                            {
                                if (!string.IsNullOrEmpty(FileSizeData.FileSize.ToString()))
                                {
                                    Anstype = Convert.ToString(FileSizeData.FileSize) + " KB";
                                }
                            }
                            return Anstype;
                        }
                        else
                            return Anstype = "-";
                    }
                    else
                    {
                        return Anstype = "-";
                    }
                }
                else
                    return Anstype = "-";

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return string.Empty;
            }
        }
        protected void myRepeater_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "RemoveShare")
            {
                string Type = string.Empty;
                string[] str = e.CommandArgument.ToString().Split(',');
                long fileUploadPermission = Convert.ToInt64(str[0]);
                long USerIDAssign = Convert.ToInt64(str[1]);
                Type = Convert.ToString(str[2]);
                int customerID = -1;
                customerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
                long UserID = Convert.ToInt64(AuthenticationHelper.UserID);

                if (fileUploadPermission > 0)
                {
                    ComplianceDBEntities entities = new ComplianceDBEntities();
                    if (Type.Equals("File"))
                    {
                        UserFolderPermission objFldPersmission = (from row in entities.UserFolderPermissions
                                                                  where row.IsDeleted == false && row.ID == fileUploadPermission
                                                                        && row.CustomerID == customerID && row.UserID == USerIDAssign
                                                                        && row.CreatedBy == UserID
                                                                  select row).FirstOrDefault();
                        if (objFldPersmission != null)
                        {
                            objFldPersmission.IsDeleted = true;
                            entities.SaveChanges();
                        }
                    }
                    else//folder
                    {
                        #region File
                        List<long> AllFileIDs = (from row in entities.sp_AllfilesofFolder((int)fileUploadPermission)
                                                 select (long)row).ToList();

                        foreach (var item in AllFileIDs)
                        {
                            long CheckExistFilePermisssion = Convert.ToInt64(item);
                            var uploadedFileIds = (from row in entities.UserFolderPermissions
                                                   where row.FileID == CheckExistFilePermisssion && row.IsDeleted == false
                                                   && row.UserID == USerIDAssign && row.CreatedBy == UserID && row.CustomerID == customerID
                                                   select row).ToList();
                            foreach (var itemx in uploadedFileIds)
                            {
                                long updatepermissionid = Convert.ToInt64(itemx.ID);
                                UserFolderPermission unsharedoc = (from row in entities.UserFolderPermissions
                                                                   where row.FileID == CheckExistFilePermisssion && row.IsDeleted == false
                                                                   && row.ID == updatepermissionid && row.UserID == USerIDAssign && row.CustomerID == customerID
                                                                   && row.CreatedBy == UserID
                                                                   select row).FirstOrDefault();
                                if (unsharedoc != null)
                                {
                                    unsharedoc.IsDeleted = true;
                                    entities.SaveChanges();
                                }
                            }
                        }
                        #endregion

                        #region Folder
                        List<long> AllFolderIDs = (from row in entities.sp_AllFolder((int)fileUploadPermission)
                                                   select (long)row).ToList();

                        foreach (var item in AllFolderIDs)
                        {
                            long CheckExistFilePermisssion = Convert.ToInt64(item);
                            var uploadedFolderIds = (from row in entities.UserFolderPermissions
                                                     where row.FolderID == CheckExistFilePermisssion && row.IsDeleted == false && row.FileID == null
                                                     && row.UserID == USerIDAssign && row.CreatedBy == UserID && row.CustomerID == customerID
                                                     select row).ToList();
                            foreach (var itemx in uploadedFolderIds)
                            {
                                long updatepermissionid = Convert.ToInt64(itemx.ID);
                                UserFolderPermission unsharedoc = (from row in entities.UserFolderPermissions
                                                                   where row.FolderID == CheckExistFilePermisssion && row.IsDeleted == false && row.FileID == null
                                                                   && row.ID == updatepermissionid && row.UserID == USerIDAssign && row.CustomerID == customerID
                                                                   && row.CreatedBy == UserID
                                                                   select row).FirstOrDefault();
                                if (unsharedoc != null)
                                {
                                    unsharedoc.IsDeleted = true;
                                    entities.SaveChanges();
                                }
                            }
                        }
                        #endregion
                        //List<long> AllFileIDs = (from row in entities.sp_AllfilesofFolder((int) fileUploadPermission)
                        //                         select (long) row).ToList();

                        //foreach (var item in AllFileIDs)
                        //{
                        //    long CheckExistFilePermisssion = Convert.ToInt64(item);
                        //    var uploadedFileIds = (from row in entities.UserFolderPermissions
                        //                           where row.FileID == CheckExistFilePermisssion && row.IsDeleted == false
                        //                           && row.UserID == USerIDAssign && row.CreatedBy == UserID
                        //                           select row).ToList();
                        //    foreach (var itemx in uploadedFileIds)
                        //    {
                        //        long updatepermissionid = Convert.ToInt64(itemx.ID);
                        //        UserFolderPermission unsharedoc = (from row in entities.UserFolderPermissions
                        //                                        where row.FileID == CheckExistFilePermisssion && row.IsDeleted == false
                        //                                        && row.ID == updatepermissionid && row.UserID == USerIDAssign 
                        //                                        && row.CreatedBy == UserID
                        //                                        select row).FirstOrDefault();
                        //        if (unsharedoc != null)
                        //        {
                        //            unsharedoc.IsDeleted = true;
                        //            entities.SaveChanges();
                        //        }
                        //    }
                        //}                        
                    }
                }
            }
        }
    }
}