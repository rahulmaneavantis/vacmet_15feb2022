﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddEditComplience.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.AreaSecreterial.AddEditComplience" ValidateRequest="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <title></title>
    <%--<link href="../NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
    --%>
    <!-- bootstrap theme -->
    <%--<link href="../NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <script src="../Scripts/jquery-1.10.2.js"></script>
    <script src="../Scripts/jquery-ui-1.9.2.custom.min.js"></script>

    <link href="../Content/css/ComplienceStyleSeet.css" rel="stylesheet" />
    <link href="../Content/css/bootstrap-tagsinput.css" rel="stylesheet" />

    <script src="../Scripts/jquery.nicescroll.js"></script>
    <script src="../Scripts/tagging.js"></script>
    <script src="../Scripts/bootstrap.min.js"></script>
    <link href="../NewCSS/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />

    <script src="../Scripts/bootstrap-multiselect.js"></script>--%>

    <link href="../NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap theme -->
    <link href="../NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <!--external css-->
    <!-- font icon -->
    <link href="../NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="../NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="../NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <link href="../NewCSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="../NewCSS/bs_leftnavi.css" rel="stylesheet" type="text/css" />
    <link href="../Content/css/bootstrap-tagsinput.css" rel="stylesheet" />
    <script type="text/javascript" src="../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="../Newjs/bs_leftnavi.js"></script>
    <script src="../Scripts/tagging.js"></script>
    <link href="../NewCSS/tag-scrolling.css" rel="stylesheet" />
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD50uFhROVysfZducuIgGl1ltBV3Hx-Ig8"></script>

    <link href="../NewCSS/litigation_custom_style.css" rel="stylesheet" />

    <script>
        function ResolveUrl() {

            $('input[data-role="tagsinput"]').tagsinput({
            });
        }
    </script>
    <style>
        .chzn-container-multi .chzn-choices {
            height: 120px !important;
            overflow-y: auto;
        }
    </style>

    <style type="text/css">
        .custom-combobox {
            position: relative;
            display: inline-block;
        }

        .custom-combobox-toggle {
            position: absolute;
            top: 0;
            bottom: 0;
            margin-left: -1px;
            padding: 0;
            /* support: IE7 */
            *height: 1.7em;
            *top: 0.1em;
        }

        .custom-combobox-input {
            margin: 0;
            padding: 0.3em;
            width: 350px;
        }

        .ccmitemlist {
            top: auto !important;
            left: auto !important;
            position: absolute !important;
            overflow: hidden !important;
            display: block !important;
        }

        #CMPMenuBar a.level1 {
            height: 22px;
            width: 145px !important;
            text-decoration: none;
            border-style: none;
        }

        fieldset {
            display: block;
            margin-inline-start: 2px;
            margin-inline-end: 2px;
            padding-block-start: 0.35em;
            padding-inline-start: 0.75em;
            padding-inline-end: 0.75em;
            padding-block-end: 0.625em;
            min-inline-size: min-content;
            border-width: 1px;
            border-style: groove;
            border-color: rgba(128, 128, 128, 0.56);
            border-radius: 11px;
            /*border-color: threedface;
    border-image: initial;*/
        }
    </style>

    <script type="text/javascript">

        function initializeCombobox() {

            <%--  $("#<%= ddlComplianceType.ClientID %>").combobox();
            $("#<%= ddlNatureOfCompliance.ClientID %>").combobox();--%>
            <%--$("#<%= ddlFrequency.ClientID %>").combobox();--%>
            <%--   $("#<%= ddlRiskType.ClientID %>").combobox();--%>
            <%--  $("#<%= ddlDueDate.ClientID %>").combobox();
            $("#<%= ddlNonComplianceType.ClientID %>").combobox();
            $("#<%= ddlPerDayMonth.ClientID %>").combobox();--%>
            <%--  $("#<%= ddlEvents.ClientID %>").combobox();--%>
            <%-- $("#<%= ddlChklstType.ClientID %>").combobox();--%>
           <%-- $("#<%= ddlComplianceSubType.ClientID %>").combobox();
            $("#<%= ddlWeekDueDay.ClientID %>").combobox();--%>
        }

        function initializeJQueryUI(textBoxID, divID) {
            //$("#" + textBoxID).unbind('click');

            //$("#" + textBoxID).click(function () {
            //    $("#" + divID).toggle("blind", null, 500, function () { });
            //});
        }

        function checkAll(cb) {
            var ctrls = document.getElementsByTagName('input');
            for (var i = 0; i < ctrls.length; i++) {
                var cbox = ctrls[i];
                if (cbox.type == "checkbox" && cbox.id.indexOf("chkIndustry") > -1) {
                    cbox.checked = cb.checked;
                }
            }
        }

        function UncheckHeader() {
            var rowCheckBox = $("#RepeaterTable input[id*='chkIndustry']");
            var rowCheckBoxSelected = $("#RepeaterTable input[id*='chkIndustry']:checked");
            var rowCheckBoxHeader = $("#RepeaterTable input[id*='IndustrySelectAll']");
            if (rowCheckBox.length == rowCheckBoxSelected.length) {
                rowCheckBoxHeader[0].checked = true;
            } else {

                rowCheckBoxHeader[0].checked = false;
            }
        }

        function checkAllET(cb) {
            var ctrls = document.getElementsByTagName('input');
            for (var i = 0; i < ctrls.length; i++) {
                var cbox = ctrls[i];
                if (cbox.type == "checkbox" && cbox.id.indexOf("chkEntityType") > -1) {
                    cbox.checked = cb.checked;
                }
            }
        }

        function UncheckHeaderET() {
            var rowCheckBox = $("#RepeaterTable input[id*='chkEntityType']");
            var rowCheckBoxSelected = $("#RepeaterTable input[id*='chkEntityType']:checked");
            var rowCheckBoxHeader = $("#RepeaterTable input[id*='EntityTypeSelectAll']");
            if (rowCheckBox.length == rowCheckBoxSelected.length) {
                rowCheckBoxHeader[0].checked = true;
            } else {

                rowCheckBoxHeader[0].checked = false;
            }
        }

        function initializeDatePicker(date) {
          
            var startDate = new Date();
            $("#<%= tbxOnetimeduedate.ClientID %>").datepicker({
                dateFormat: 'dd-mm-yy',
                defaultDate: startDate,
                numberOfMonths: 1,
                minDate: startDate,
                onClose: function (startDate) {
                    $("#<%= tbxOnetimeduedate.ClientID %>").datepicker("option", "minDate", startDate);
                }
            });
            if (date != null) {
                $("#<%= tbxOnetimeduedate.ClientID %>").datepicker("option", "defaultDate", date);

            }
        }
        function initializeDatePicker1(date)
        {
            var startDate1 = new Date();
                $("#<%= txtStartDate.ClientID %>").datepicker({
                dateFormat: 'dd-mm-yy',
                defaultDate: startDate1,
                numberOfMonths: 1,
                minDate: startDate1,
                onClose: function (startDate1) {
                    $("#<%= txtStartDate.ClientID %>").datepicker("option", "minDate", startDate1);
                }
                });
                if (date != null) {
                    $("#<%= txtStartDate.ClientID %>").datepicker("option", "defaultDate", date);

                }
            }

    </script>


    <style type="text/css">
        .bootstrap-tagsinput {
            width: 390px;
        }

        [class="bootstrap-tagsinput"] input {
            width: 390px;
        }

        .label {
            width: auto;
            color: black;
        }

        tr.spaceUnder > td {
            padding-bottom: 1em;
        }

        .bootstrap-tagsinput .tag {
            margin-top: 3px;
            color: black;
        }
    </style>

    <script type="text/javascript">
        $(function () {
            $(document).tooltip();
        });
      

        function setDate() {
            $(".StartDate").datepicker();
        }

        var validFilesTypes = ["exe", "bat", "dll"];
        function ValidateFile() {

            var label = document.getElementById("<%=Label2.ClientID%>");
            var fuSampleFile = $("#<%=fuSampleFile.ClientID%>").get(0).files;
            var isValidFile = true;

            for (var i = 0; i < fuSampleFile.length; i++) {
                var fileExtension = fuSampleFile[i].name.split('.').pop();
                if (validFilesTypes.indexOf(fileExtension) != -1) {
                    isValidFile = false;
                    break;
                }
            }

            if (!isValidFile) {
                label.style.color = "red";
                //label.innerHTML = "Invalid file uploded. .exe,.zip,.bat formats not supported.";
                label.innerHTML = "Invalid file uploded. .exe,.bat formats not supported.";
            }
            return isValidFile;
        }

    </script>

    <style type="text/css">
        .label {
            display: inline-block;
            font-weight: normal;
            font-size: 12px;
        }

        .ui-tooltip {
            max-width: 700px;
            font-weight: normal;
            font-size: 12px;
        }
    </style>

  
    <script type="text/javascript">
        $(document).ready(function () {
         
            window.parent.OnRefresh();
        })
    </script>

    <style>
        .k-window-content {
            overflow: hidden;
        }

        .mt-30 {
            margin-top: 15px;
        }
    </style>

  </head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" />
        <div class="container"  style="background-color: white">
            <div class="row">
    <div class="col-md-12">
   <div class="alert alert-success alert-dismissible" runat="server" id="divsuccess" visible="false">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <label runat="server" id="lblsuccess"></label>
  </div>
    </div>
</div>

        <div class="row">
    <div class="col-md-12">
   <div class="alert alert-danger alert-dismissible" runat="server" visible="false" id="diverror" >
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <label runat="server" id="lblerror"></label>
  </div>
    </div>
</div>
        <div class="row colpadding0">
            <div class="col-md-12 colpadding0">
                <div id="divComplianceDetailsDialog">
                    <asp:UpdatePanel ID="upComplianceDetails" runat="server" UpdateMode="Conditional" OnLoad="upComplianceDetails_Load">
                        <ContentTemplate>
                            <div style="margin: 5px 5px 80px;">

                                    <div class="col-md-12">
   <%--<div class="alert alert-success alert-dismissible" runat="server" id="div1">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>--%>
     <asp:ValidationSummary runat="server" CssClass="alert alert-danger alert-dismissible"
                                        ValidationGroup="ComplianceValidationGroup" />
                                    <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                        ValidationGroup="ComplianceValidationGroup" Display="None" />
                                    <asp:Label runat="server" ID="lblErrorMassage" ForeColor="Red"></asp:Label>
  <%--</div>
    </div>--%>

                                <div style="margin-bottom: 4px">
                                   
                                </div>

                                <div class="row colpadding0 mt-30">
                                    <div class="form-group col-md-12 colpadding0">
                                        <div class="col-md-3 colpadding0">
                                            <span style="color: red">*</span><label>
                                                Act Name</label>
                                        </div>
                                        <div class="col-md-8 colpadding0">
                                            <asp:DropDownListChosen runat="server" ID="ddlAct" AllowSingleDeselect="false" DisableSearchThreshold="5" AutoPostBack="true"
                                                 class="form-control" Width="76%">
                                            </asp:DropDownListChosen>
                                            <asp:CompareValidator ID="CompareValidator1" ErrorMessage="Please select Act Name."
                                                ControlToValidate="ddlAct" runat="server" ValueToCompare="-1" Operator="NotEqual"
                                                ValidationGroup="ComplianceValidationGroup" Display="None" />
                                        </div>
                                    </div>
                                </div>

                                <div class="row colpadding0 mt-30">
                                    <div class="col-md-12 colpadding0 form-group">
                                        <div class="col-md-3 colpadding0">
                                            <span style="color: red">*</span><label>Section(s) / Rule(s)</label>
                                        </div>
                                        <div class="col-md-8 colpadding0">
                                            <asp:TextBox runat="server" ID="tbxSections" CssClass="form-control" Width="75%" />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator11" ErrorMessage="Section(s) / Rule(s) can not be empty."
                                                ControlToValidate="tbxSections" runat="server" ValidationGroup="ComplianceValidationGroup"
                                                Display="None" />
                                        </div>
                                    </div>
                                </div>

                                <div class="row colpadding0 mt-30">
                                    <div class="col-md-12 colpadding0 form-group">
                                        <div class="col-md-3 colpadding0">
                                            <span style="color: red">*</span><label>Short Description</label>
                                        </div>

                                        <div class="col-md-8 colpadding0">
                                            <asp:TextBox runat="server" ID="txtShortDescription" TextMode="MultiLine" MaxLength="100" CssClass="form-control" Width="75%" />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator14" ErrorMessage="Short Description can not be empty."
                                                ControlToValidate="txtShortDescription" runat="server" ValidationGroup="ComplianceValidationGroup"
                                                Display="None" />
                                        </div>
                                    </div>
                                </div>

                                <div class="row colpadding0 mt-30">
                                    <div class="col-md-12 colpadding0 form-group">
                                        <div class="col-md-3 colpadding0">
                                            <span style="color: red">*</span>
                                            <label>Short Form</label>
                                        </div>
                                        <div class="col-md-8 colpadding0">
                                            <asp:TextBox runat="server" ID="txtShortForm" TextMode="MultiLine" CssClass="form-control" Width="75%" />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator16" ErrorMessage="Description can not be empty."
                                                ControlToValidate="tbxDescription" runat="server" ValidationGroup="ComplianceValidationGroup"
                                                Display="None" />
                                        </div>
                                    </div>
                                </div>

                                <div class="row colpadding0 mt-30">
                                    <div class="col-md-12 colpadding0 form-group">
                                        <div class="col-md-3 colpadding0">
                                            <span style="color: red">*</span>
                                            <label>Detailed Description</label>
                                        </div>
                                        <div class="col-md-8 colpadding0">
                                            <asp:TextBox runat="server" ID="tbxDescription" TextMode="MultiLine" CssClass="form-control" Width="75%" />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator15" ErrorMessage="Description can not be empty."
                                                ControlToValidate="tbxDescription" runat="server" ValidationGroup="ComplianceValidationGroup"
                                                Display="None" />
                                        </div>
                                    </div>
                                </div>

                                <div class="row colpadding0 mt-30">
                                    <div class="col-md-12 colpadding0 form-group">
                                        <div class="col-md-3 colpadding0">
                                            <span style="color: red">*</span>
                                            <label>Start Date</label>
                                        </div>
                                        <div class="col-md-8 colpadding0">

                                            <div class="input-group date" style="width: 35%">
                                                <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar" style="padding: 3px !important;"></span>
                                                </span>

                                                <asp:TextBox runat="server" ID="txtStartDate" AutoPostBack="true" CssClass="form-control txtbox" autocomplete="off" />
                                            </div>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator18" ErrorMessage="Start Date can not be empty."
                                                ControlToValidate="txtStartDate" runat="server" ValidationGroup="ComplianceValidationGroup"
                                                Display="None" />
                                        </div>
                                    </div>
                                </div>

                                <div class="colpadding0 row mt-30">
                                    <div class="col-md-12 colpadding0 form-group">
                                        <div class="col-md-3 colpadding0">
                                            <span style="color: red">*</span>
                                            <label>Compliance Tag</label>
                                        </div>
                                        <div class="col-md-8 colpadding0">
                                            <asp:TextBox runat="server" ID="txtCompliancetag" ClientIDMode="Static" data-role="tagsinput"
                                                CssClass="form-control txtbox" Width="75%" autocomplete="off" Rows="1" />
                                        </div>
                                    </div>
                                </div>

                                <div class="row colpadding0 mt-30">
                                    <div class="col-md-12 colpadding0 form-group" runat="server" visible="false">
                                        <div class="col-md-3 colpadding0">
                                            <span style="color: red">*</span>
                                            <label>
                                                Industry
                                            </label>
                                        </div>
                                        <div class="col-md-8 colpadding0">
                                            <asp:TextBox runat="server" ID="txtIndustry" CssClass="form-control txtbox" Width="75%" />
                                        </div>

                                        <div style="margin-left: 196px; top: 35px; width: 52%; position: absolute; z-index: 50; overflow-y: auto; background: white; border: 1px solid gray; height: 200px;" id="dvIndustry" class="form-control">

                                            <asp:Repeater ID="rptIndustry" runat="server">
                                                <HeaderTemplate>
                                                    <table class="detailstable FadeOutOnEdit" id="RepeaterTable">
                                                        <tr>
                                                            <td style="width: 100px;">
                                                                <asp:CheckBox ID="IndustrySelectAll" Text="Select All" runat="server" onclick="checkAll(this)" /></td>
                                                            <td style="width: 282px;">
                                                                <asp:Button runat="server" ID="btnRepeater" Text="Ok" Style="float: left" OnClick="btnRefresh_Click" /></td>
                                                            <%--OnClick="btnRefresh_Click" --%>
                                                        </tr>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td style="width: 20px;">
                                                            <asp:CheckBox ID="chkIndustry" runat="server" onclick="UncheckHeader();" /></td>
                                                        <td style="width: 200px;">
                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 250px; padding-bottom: 5px;">
                                                                <asp:Label ID="lblIndustryID" runat="server" Visible="false" Text='<%# Eval("ID")%>' ToolTip='<%# Eval("ID") %>'></asp:Label>
                                                                <asp:Label ID="lblIndustryName" runat="server" Text='<%# Eval("Name")%>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                                            </div>
                                                        </td>

                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </table>
                                                </FooterTemplate>

                                            </asp:Repeater>

                                        </div>
                                    </div>
                                </div>

                                <div class="row colpadding0 mt-30">
                                    <div class="col-md-12 colpadding0 form-group" runat="server" visible="false">
                                        <div class="col-md-3 colpadding0">
                                            <span style="color: red">*</span><label>
                                                Legal Entity Type
                                            </label>
                                        </div>
                                        <div class="col-md-8 colpadding0">
                                            <asp:TextBox runat="server" ID="txtEntityType" CssClass="form-control txtbox" Width="75%" />
                                            <div style="margin-left: 200px; position: absolute; z-index: 50; overflow-y: auto; background: white; border: 1px solid gray; height: 200px;" id="dvEntityType">

                                                <asp:Repeater ID="rptEntityType" runat="server">
                                                    <HeaderTemplate>
                                                        <table class="detailstable FadeOutOnEdit" id="RepeaterTable">
                                                            <tr>
                                                                <td style="width: 100px;">
                                                                    <asp:CheckBox ID="EntityTypeSelectAll" Text="Select All" runat="server" onclick="checkAllET(this)" /></td>
                                                                <td style="width: 282px;">
                                                                    <asp:Button runat="server" ID="btnRepeaterEntityType" Text="Ok" Style="float: left" OnClick="btnRefresh_Click" /></td>
                                                                <%--OnClick="btnRefresh_Click" --%>
                                                            </tr>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td style="width: 20px;">
                                                                <asp:CheckBox ID="chkEntityType" runat="server" onclick="UncheckHeaderET();" /></td>
                                                            <td style="width: 200px;">
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 250px; padding-bottom: 5px;">
                                                                    <asp:Label ID="lblEntityTypeID" runat="server" Visible="false" Text='<%# Eval("ID")%>' ToolTip='<%# Eval("ID") %>'></asp:Label>
                                                                    <asp:Label ID="lblEntityTypeName" runat="server" Text='<%# Eval("EntityTypeName")%>' ToolTip='<%# Eval("EntityTypeName") %>'></asp:Label>
                                                                </div>
                                                            </td>

                                                        </tr>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        </table>
                                                    </FooterTemplate>

                                                </asp:Repeater>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row colpadding0 mt-30">
                                    <div class="col-md-12 colpadding0 form-group">
                                        <div class="col-md-3 colpadding0">
                                            <span style="color: red">*</span><label>
                                                Compliance Type</label>
                                        </div>
                                        <div class="col-md-8 colpadding0">
                                            <asp:DropDownListChosen runat="server" ID="ddlComplianceType" AllowSingleDeselect="false" DisableSearchThreshold="5" AutoPostBack="true"
                                                DataPlaceHolder="Select Type" class="form-control" Width="76%" OnSelectedIndexChanged="ddlComplianceType_SelectedIndexChanged">
                                                <asp:ListItem Text="Function based" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="Checklist" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="Time Based" Value="3"></asp:ListItem>
                                            </asp:DropDownListChosen>
                                            <%-- <asp:DropDownList runat="server" ID="ddlComplianceType" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                        CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlComplianceType_SelectedIndexChanged">
                                        <asp:ListItem Text="Function based" Value="0" />
                                        <asp:ListItem Text="Checklist" Value="1" />
                                        <asp:ListItem Text="Time Based" Value="2" />
                                    </asp:DropDownList>--%>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator12" ErrorMessage="Please Select Compliance Type."
                                                ControlToValidate="ddlComplianceType" runat="server" ValidationGroup="ComplianceValidationGroup"
                                                Display="None" />
                                        </div>
                                    </div>
                                </div>

                                <div class="row colpadding0 mt-30">
                                    <div runat="server" id="divNatureOfCompliance" class="col-md-12 colpadding0 form-group">
                                        <div class="col-md-3 colpadding0">
                                            <span style="color: red">*</span><label>
                                                Nature Of Compliance</label>
                                        </div>

                                        <div class="col-md-8 colpadding0">
                                            <asp:DropDownListChosen runat="server" ID="ddlNatureOfCompliance" AllowSingleDeselect="false" DisableSearchThreshold="5" AutoPostBack="true"
                                                class="form-control" Width="76%" OnSelectedIndexChanged="ddlNatureOfCompliance_SelectedIndexChanged">
                                            </asp:DropDownListChosen>
                                            <%--  <asp:DropDownList runat="server" ID="ddlNatureOfCompliance" OnSelectedIndexChange="ddlNatureOfCompliance_SelectedIndexChanged" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                        CssClass="txtbox">
                                       
                                    </asp:DropDownList>--%>
                                            <asp:CompareValidator ErrorMessage="Please select Nature of Compliance." ControlToValidate="ddlNatureOfCompliance"
                                                runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceValidationGroup"
                                                Display="None" />
                                        </div>
                                    </div>
                                </div>

                                <div class="row colpadding0 mt-30">
                                    <div runat="server" id="divComplianceSubType" class="col-md-12 colpadding0 form-group">
                                        <div class="col-md-3 colpadding0">
                                            <span>&nbsp;</span>
                                            <label>
                                                Nature Of Compliance Sub Type</label>
                                        </div>
                                        <div class="col-md-8 colpadding0">
                                            <asp:DropDownListChosen runat="server" ID="ddlComplianceSubType" AllowSingleDeselect="false" DisableSearchThreshold="5" AutoPostBack="true"
                                               class="form-control" Width="76%">
                                            </asp:DropDownListChosen>
                                            <%--<asp:DropDownList runat="server" ID="ddlComplianceSubType" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                        CssClass="txtbox">
                                    </asp:DropDownList>--%>
                                        </div>
                                    </div>
                                </div>

                                <div class="row colpadding0 mt-30">
                                    <div id="divTimebasedTypes" runat="server" visible="false" class="col-md-12 colpadding0 form-group">
                                        <div class="col-md-3 colpadding0">
                                            <span style="color: red">*</span><label>
                                                Timely Based Type</label>
                                        </div>
                                        <div class="col-md-8 colpadding0">
                                            <asp:RadioButtonList ID="rbTimebasedTypes" runat="server" RepeatDirection="Horizontal" AutoPostBack="true"
                                                OnSelectedIndexChanged="rbTimebasedTypes_SelectedIndexChanged">
                                                <asp:ListItem Text="Fixed Gap" Value="0" Selected="True" />
                                                <asp:ListItem Text="Periodically Based" Value="1" />
                                            </asp:RadioButtonList>
                                        </div>
                                    </div>
                                </div>

                                <div class="row colpadding0 mt-30">
                                    <div id="divOneTime" runat="server" visible="false" class="col-md-12 colpadding0 form-group">
                                        <div class="col-md-3 colpadding0">
                                            <span style="color: red">*</span><label>
                                                One Time Date</label>
                                        </div>
                                        <div class="col-md-8 colpadding0">
                                            <%--  <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar" style="padding: 3px !important;"></span>
                                            </span>
                                        <asp:TextBox runat="server" ID="tbxOnetimeduedate" AutoPostBack="true" CssClass="form-control txtbox" Width="75%" />--%>
                                            <div class="input-group date" style="width: 35%">
                                                <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar" style="padding: 3px !important;"></span>
                                                </span>

                                                <asp:TextBox runat="server" ID="tbxOnetimeduedate" AutoPostBack="true" CssClass="form-control txtbox" autocomplete="off" />
                                            </div>
                                            <asp:RequiredFieldValidator ID="rfvOnetimeduedate" ErrorMessage="Please enter One time Due Date."
                                                ControlToValidate="tbxOnetimeduedate" runat="server" ValidationGroup="ComplianceValidationGroup"
                                                Display="None" />
                                        </div>

                                    </div>
                                </div>

                                <div class="row colpadding0 mt-30">
                                    <div class="col-md-12 colpadding0 form-group" style="display: none">
                                        <div style="margin-bottom: 15px" id="divActionable" runat="server">
                                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                            <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                                Compliance Actionable/Informational
                                            </label>
                                            <asp:RadioButton ID="rdoComplianceVisible" AutoPostBack="true" Checked="true" OnCheckedChanged="rdoComplianceVisible_CheckedChanged" Text="Actionable" Font-Bold="true" GroupName="radioComplianceVisible" runat="server" />
                                            <asp:RadioButton ID="rdoNotComplianceVisible" AutoPostBack="true" Text="Informational" OnCheckedChanged="rdoComplianceVisible_CheckedChanged" Font-Bold="true" GroupName="radioComplianceVisible" runat="server" />
                                        </div>
                                    </div>
                                </div>

                                <div class="row colpadding0 mt-30">
                                    <div class="col-md-12 colpadding0 form-group">
                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <div class="row colpadding0">
                                                    <div runat="server" id="divFunctionBased" class="row col-md-12 colpadding0 form-group">
                                                        <div id="divNonEvents" runat="server" class="row col-md-12 colpadding0 form-group">

                                                            <div class="row col-md-12 colpadding0 form-group mt-30" id="divFrequency" runat="server">
                                                                <div class="col-md-3 colpadding0">
                                                                    <span style="color: red;">*</span>
                                                                    <label>
                                                                        Frequency</label>
                                                                </div>
                                                                <div class="col-md-8 colpadding0">
                                                                    <%--<asp:DropDownList runat="server" ID="ddlFrequency" OnSelectedIndexChanged="ddlFrequency_SelectedIndexChanged" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                                            CssClass="txtbox">
                                                        </asp:DropDownList>--%>
                                                                    <asp:DropDownListChosen runat="server" ID="ddlFrequency" AllowSingleDeselect="false" DisableSearchThreshold="5" AutoPostBack="true"
                                                                         class="form-control" Width="76%" OnSelectedIndexChanged="ddlFrequency_SelectedIndexChanged">
                                                                    </asp:DropDownListChosen>
                                                                    <asp:CompareValidator ErrorMessage="Please select Frequency." ControlToValidate="ddlFrequency" ID="cvfrequency"
                                                                        runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceValidationGroup"
                                                                        Display="None" />
                                                                </div>
                                                            </div>

                                                            <div id="vivDueDate" runat="server" class="row col-md-12 colpadding0 form-group mt-30">
                                                                <div class="col-md-3 colpadding0">
                                                                    <span style="color: red;">&nbsp;</span>
                                                                    <label>
                                                                        Due Date</label>
                                                                </div>
                                                                <div class="col-md-8 colpadding0">
                                                                    <asp:DropDownListChosen runat="server" ID="ddlDueDate" AllowSingleDeselect="false" DisableSearchThreshold="5" AutoPostBack="true"
                                                                        DataPlaceHolder="Select" class="form-control" Width="20%">
                                                                    </asp:DropDownListChosen>
                                                                    <%-- <asp:DropDownList runat="server" ID="ddlDueDate" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                                            CssClass="txtbox">
                                                        </asp:DropDownList>--%>
                                                                    <asp:RequiredFieldValidator ID="vaDueDate" ErrorMessage="Please Select Due Date."
                                                                        ControlToValidate="ddlDueDate" runat="server" ValidationGroup="ComplianceValidationGroup"
                                                                        Display="None" Enabled="false" />
                                                                </div>
                                                            </div>

                                                            <div class="row col-md-12 colpadding0 form-group mt-30" id="vivWeekDueDays" runat="server" visible="false">
                                                                <div class="col-md-3 colpadding0">
                                                                    <span style="color: red;">*</span>
                                                                    <label>
                                                                        Week Due Day</label>
                                                                </div>
                                                                <div class="col-md-8 colpadding0">
                                                                    <asp:DropDownListChosen runat="server" ID="ddlWeekDueDay" AllowSingleDeselect="false" DisableSearchThreshold="5" AutoPostBack="true"
                                                                        DataPlaceHolder="Select" class="form-control" Width="99%">
                                                                    </asp:DropDownListChosen>
                                                                    <%--<asp:DropDownList runat="server" ID="ddlWeekDueDay" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                                            CssClass="txtbox">
                                                        </asp:DropDownList>--%>
                                                                    <asp:CompareValidator ErrorMessage="Please Select Week Due Day." ControlToValidate="ddlWeekDueDay" ID="vaDueWeekDay"
                                                                        runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceValidationGroup"
                                                                        Display="None" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row col-md-12 colpadding0 form-group mt-30" id="divNonComplianceType" runat="server">
                                                    <div class="col-md-3 colpadding0">
                                                        <span style="color: red;">&nbsp;</span>
                                                        <label>
                                                            Non Compliance Type</label>
                                                    </div>
                                                    <div class="col-md-8 colpadding0">

                                                        <asp:DropDownListChosen runat="server" ID="ddlNonComplianceType" AllowSingleDeselect="false" DisableSearchThreshold="5" AutoPostBack="true"
                                                            DataPlaceHolder="Select Type" class="form-control" Width="76%" OnSelectedIndexChanged="ddlNonComplianceType_SelectedIndexChanged">
                                                            <asp:ListItem Text="<Select>" Value="-1"></asp:ListItem>
                                                            <asp:ListItem Text="Both" Value="2"></asp:ListItem>
                                                            <asp:ListItem Text="Monetary" Value="0"></asp:ListItem>
                                                            <asp:ListItem Text="Non-Monetary" Value="1"></asp:ListItem>
                                                        </asp:DropDownListChosen>
                                                    </div>
                                                </div>

                                                <div class="row colpadding0 mt-30">
                                                    <div runat="server" id="divMonetary" class="col-md-12 colpadding0">
                                                        <div class="col-md-12 colpadding0 form-group">
                                                            <fieldset class="col">
                                                                <legend>
                                                                    <label style="font-size: 17px">Monetory</label></legend>

                                                                <div class="row colpadding0 mt-30">
                                                                    <div class="col-md-12 colpadding0 form-group">
                                                                        <div class="col-md-3 colpadding0">
                                                                            <span style="color: red;">*</span>
                                                                            <label>
                                                                                Fixed Minimum</label>
                                                                        </div>
                                                                        <div class="col-md-8 colpadding0">
                                                                            <asp:TextBox runat="server" ID="tbxFixedMinimum"
                                                                                MaxFixedMinimum="4" CssClass="form-control" Width="76%" />
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Fixed minimum can not be empty."
                                                                                ControlToValidate="tbxFixedMinimum" runat="server" ValidationGroup="ComplianceValidationGroup"
                                                                                Display="None" />
                                                                            <asp:CompareValidator ID="CompareValidator2" ErrorMessage="Fixed minimum is a not valid number."
                                                                                ControlToValidate="tbxFixedMinimum" Operator="DataTypeCheck" Type="Double" runat="server"
                                                                                Display="None" ValidationGroup="ComplianceValidationGroup" />
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="row colpadding0 mt-30">
                                                                    <div class="col-md-12 colpadding0 form-group">
                                                                        <div class="col-md-3 colpadding0">
                                                                            <span style="color: red;">*</span>
                                                                            <label>
                                                                                Fixed Maximum</label>
                                                                        </div>
                                                                        <div class="col-md-8 colpadding0">
                                                                            <asp:TextBox runat="server" ID="tbxFixedMaximum" MaxFixedMaximum="4" CssClass="form-control" Width="76%" />
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="Fixed maximum can not be empty."
                                                                                ControlToValidate="tbxFixedMaximum" runat="server" ValidationGroup="ComplianceValidationGroup"
                                                                                Display="None" />
                                                                            <asp:CompareValidator ID="CompareValidator3" ErrorMessage="Fixed maximum is a not valid number."
                                                                                ControlToValidate="tbxFixedMaximum" Operator="DataTypeCheck" Type="Double" runat="server"
                                                                                Display="None" ValidationGroup="ComplianceValidationGroup" />
                                                                            <asp:CompareValidator ID="CompareValidatormaximum" runat="server" ErrorMessage="Fixed maximum should be greater than fixed minimum." Type="Double"
                                                                                ControlToCompare="tbxFixedMinimum" ControlToValidate="tbxFixedMaximum" Display="None" Operator="GreaterThanEqual" ValidationGroup="ComplianceValidationGroup" />
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="row colpadding0 mt-30">
                                                                    <div class="col-md-12 colpadding0 form-group">
                                                                        <div class="col-md-3 colpadding0">
                                                                            <label>&nbsp;</label>
                                                                        </div>
                                                                        <div class="col-md-8 colpadding0">

                                                                            <asp:DropDownListChosen runat="server" ID="ddlPerDayMonth" AllowSingleDeselect="false" DisableSearchThreshold="5" AutoPostBack="true"
                                                                                class="form-control" Width="76%">
                                                                                <asp:ListItem Text="<Select>" Value="-1"></asp:ListItem>
                                                                                <asp:ListItem Text="Day" Value="0"></asp:ListItem>
                                                                                <asp:ListItem Text="Month" Value="1"></asp:ListItem>

                                                                            </asp:DropDownListChosen>
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Please select Day or Month."
                                                                                InitialValue="-1" ControlToValidate="ddlPerDayMonth" runat="server" ValidationGroup="ComplianceValidationGroup"
                                                                                Display="None" />
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="row colpadding0 mt-30">
                                                                    <div class="col-md-12 colpadding0 form-group">
                                                                        <div class="col-md-3 colpadding0">
                                                                            <span style="color: red;">*</span>
                                                                            <label>
                                                                                Variable Amount Rs.</label>
                                                                        </div>

                                                                        <div class="col-md-8 colpadding0">
                                                                            <asp:TextBox runat="server" ID="tbxVariableAmountPerDay" Width="76%" CssClass="form-control"
                                                                                MaxVariableAmountPerDay="4" />
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ErrorMessage="Variable amount can not be empty."
                                                                                ControlToValidate="tbxVariableAmountPerDay" runat="server" ValidationGroup="ComplianceValidationGroup"
                                                                                Display="None" />
                                                                            <asp:CompareValidator ID="CompareValidator4" ErrorMessage="Variable amount is a not valid number."
                                                                                ControlToValidate="tbxVariableAmountPerDay" Operator="DataTypeCheck" Type="Double"
                                                                                runat="server" Display="None" ValidationGroup="ComplianceValidationGroup" />
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="row colpadding0 mt-30">
                                                                    <div class="col-md-12 colpadding0 form-group">
                                                                        <div class="col-md-3 colpadding0">
                                                                            <span style="color: red;">*</span>
                                                                            <label>
                                                                                Variable Amount (Max)</label>
                                                                        </div>

                                                                        <div class="col-md-8 colpadding0">
                                                                            <asp:TextBox runat="server" ID="tbxVariableAmountPerDayMax" CssClass="form-control" Width="76%"
                                                                                MaxVariableAmountPerDayMax="4" />
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ErrorMessage="Variable amount (Max) can not be empty."
                                                                                ControlToValidate="tbxVariableAmountPerDayMax" runat="server" ValidationGroup="ComplianceValidationGroup"
                                                                                Display="None" />
                                                                            <asp:CompareValidator ID="CompareValidator5" ErrorMessage="Variable amount (max) is a not valid number."
                                                                                ControlToValidate="tbxVariableAmountPerDayMax" Operator="DataTypeCheck" Type="Double"
                                                                                runat="server" Display="None" ValidationGroup="ComplianceValidationGroup" />
                                                                            <asp:CompareValidator ID="CompareValidator12" runat="server" ErrorMessage="Variable Amount (Max) should be greater than Variable Amount Rs." Type="Double"
                                                                                ControlToCompare="tbxVariableAmountPerDay" ControlToValidate="tbxVariableAmountPerDayMax" Display="None" Operator="GreaterThanEqual" ValidationGroup="ComplianceValidationGroup" />
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="row colpadding0 mt-30">
                                                                    <div class="col-md-12 colpadding0 form-group">
                                                                        <div class="col-md-3 colpadding0">
                                                                            <span style="color: red;">*</span>
                                                                            <label>
                                                                                Variable Amount (%)</label>
                                                                        </div>
                                                                        <div class="col-md-8 colpadding0">
                                                                            <asp:TextBox runat="server" ID="tbxVariableAmountPercent" CssClass="form-control" Width="76%"
                                                                                MaxVariableAmountPercent="4" />
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ErrorMessage="Variable amount (%) can not be empty."
                                                                                ControlToValidate="tbxVariableAmountPercent" runat="server" ValidationGroup="ComplianceValidationGroup"
                                                                                Display="None" />
                                                                            <asp:CompareValidator ID="CompareValidator6" ErrorMessage="Variable amount (%) is a not valid number."
                                                                                ControlToValidate="tbxVariableAmountPercent" Operator="DataTypeCheck" Type="Double"
                                                                                runat="server" Display="None" ValidationGroup="ComplianceValidationGroup" />
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="row colpadding0 mt-30">
                                                                    <div class="col-md-12 colpadding0 form-group">
                                                                        <div class="col-md-3 colpadding0">
                                                                            <span style="color: red;">*</span>

                                                                            <label>
                                                                                Variable Amount (% Max)</label>
                                                                        </div>
                                                                        <div class="col-md-8 colpadding0">
                                                                            <asp:TextBox runat="server" ID="tbxVariableAmountPercentMaximum" Width="76%" CssClass="form-control"
                                                                                MaxVariableAmountPercentMaximum="4" />
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ErrorMessage="Variable amount (% max) can not be empty."
                                                                                ControlToValidate="tbxVariableAmountPercentMaximum" runat="server" ValidationGroup="ComplianceValidationGroup"
                                                                                Display="None" />
                                                                            <asp:CompareValidator ID="CompareValidator7" ErrorMessage="Variable amount (% max) is a not valid number."
                                                                                ControlToValidate="tbxVariableAmountPercentMaximum" Operator="DataTypeCheck"
                                                                                Type="Double" runat="server" Display="None" ValidationGroup="ComplianceValidationGroup" />
                                                                            <asp:CompareValidator ID="CompareValidator13" runat="server" ErrorMessage="Variable Amount (% Max) should be greater than Variable Amount Variable Amount (%)." Type="Double"
                                                                                ControlToCompare="tbxVariableAmountPerDay" ControlToValidate="tbxVariableAmountPerDayMax" Display="None" Operator="GreaterThanEqual" ValidationGroup="ComplianceValidationGroup" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </fieldset>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div>

                                                    <div runat="server" id="divNonMonetary" class="row colpadding0 mt-30">
                                                        <div class="col-md-12 colpadding0 form-group">
                                                            <fieldset class="col">
                                                                <legend>
                                                                    <label style="font-size: 17px">Non-Monetory</label></legend>

                                                                <div class="row colpadding0 mt-30">
                                                                    <div class="col-md-12 colpadding0 form-group">
                                                                        <div class="col-md-3 colpadding0">
                                                                            <span>&nbsp;</span>
                                                                            <label>
                                                                                Imprisonment</label>
                                                                        </div>

                                                                        <div class="col-md-8 colpadding0">
                                                                            <asp:CheckBox runat="server" ID="chbImprisonment" CssClass="txtbox" AutoPostBack="true"
                                                                                OnCheckedChanged="chbImprisonment_CheckedChanged" />
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div id="divImprisonmentDetails" runat="server" visible="false">
                                                                    <div class="row colpadding0 mt-30">
                                                                        <div class="col-md-12 colpadding0 form-group">
                                                                            <div class="col-md-3 colpadding0">
                                                                                <span>&nbsp;</span>
                                                                                <label>
                                                                                    Designation</label>
                                                                            </div>
                                                                            <div class="col-md-8 colpadding0">
                                                                                <asp:TextBox runat="server" ID="tbxDesignation" CssClass="form-control" Width="76%"
                                                                                    MaxDesignation="4" />
                                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ErrorMessage="Designation can not be empty."
                                                                                    ControlToValidate="tbxDesignation" runat="server" ValidationGroup="ComplianceValidationGroup"
                                                                                    Display="None" />
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="row colpadding0 mt-30">
                                                                        <div class="col-md-12 colpadding0 form-group">
                                                                            <div class="col-md-3 colpadding0">
                                                                                <span>&nbsp;</span><label>
                                                                                    Minimum Years</label>
                                                                            </div>
                                                                            <div class="col-md-8 colpadding0">
                                                                                <asp:TextBox runat="server" ID="tbxMinimumYears" CssClass="form-control" Width="76%"
                                                                                    MaxMinimumYears="4" />
                                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator9" ErrorMessage="Minimum years can not be empty."
                                                                                    ControlToValidate="tbxMinimumYears" runat="server" ValidationGroup="ComplianceValidationGroup"
                                                                                    Display="None" />
                                                                                <asp:CompareValidator ID="CompareValidator9" ErrorMessage="Minimum years is a not valid number."
                                                                                    ControlToValidate="tbxMinimumYears" Operator="DataTypeCheck" Type="Integer" runat="server"
                                                                                    Display="None" ValidationGroup="ComplianceValidationGroup" />
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="row colpadding0 mt-30">
                                                                        <div class="col-md-12 colpadding0 form-group">
                                                                            <div class="col-md-3 colpadding0">
                                                                                <span>&nbsp;</span><label>
                                                                                    Maximum Years</label>
                                                                            </div>
                                                                            <div class="col-md-8 colpadding0">
                                                                                <asp:TextBox runat="server" ID="tbxMaximumYears" CssClass="form-control" Width="76%"
                                                                                    MaxMaximumYears="4" />
                                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator10" ErrorMessage="Maximum years can not be empty."
                                                                                    ControlToValidate="tbxMaximumYears" runat="server" ValidationGroup="ComplianceValidationGroup"
                                                                                    Display="None" />
                                                                                <asp:CompareValidator ID="CompareValidator10" ErrorMessage="Maximum years is not a valid number."
                                                                                    ControlToValidate="tbxMaximumYears" Operator="DataTypeCheck" Type="Integer" runat="server"
                                                                                    Display="None" ValidationGroup="ComplianceValidationGroup" />
                                                                                <asp:CompareValidator ID="CompareValidator11" runat="server" ErrorMessage="Maximum year should be greater than minimum year." Type="Double"
                                                                                    ControlToCompare="tbxMinimumYears" ControlToValidate="tbxMaximumYears" Display="None" Operator="GreaterThanEqual" ValidationGroup="ComplianceValidationGroup" />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="row colpadding0 mt-30">
                                                                    <div class="col-md-12 colpadding0 form-group">
                                                                        <div class="col-md-3 colpadding0">
                                                                            <span>&nbsp;</span>
                                                                            <label>
                                                                                Others</label>
                                                                        </div>
                                                                        <div class="col-md-8 colpadding0">
                                                                            <asp:TextBox runat="server" ID="tbxNonComplianceEffects" Width="76%" CssClass="form-control" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </fieldset>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row colpadding0 mt-30">
                                                    <div class="col-md-12 colpadding0 form-group">
                                                        <div class="col-md-3 colpadding0">
                                                            <span>&nbsp;</span>
                                                            <label>
                                                                Penalty Description</label>
                                                        </div>
                                                        <div class="col-md-8 colpadding0">
                                                            <asp:TextBox runat="server" ID="txtPenaltyDescription" TextMode="MultiLine" CssClass="form-control txtbox" Width="75%" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row colpadding0 mt-30">
                                                    <div class="col-md-12 colpadding0 form-group">
                                                        <div class="col-md-3 colpadding0">
                                                            <span>&nbsp;</span>
                                                            <label>
                                                                Reference Material Text</label>
                                                        </div>
                                                        <div class="col-md-8 colpadding0">
                                                            <asp:TextBox runat="server" ID="txtReferenceMaterial" TextMode="MultiLine" CssClass="form-control txtbox" Width="75%" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row colpadding0 mt-30">
                                                    <div class="col-md-12 colpadding0 form-group" id="divReminderType" runat="server" visible="false">
                                                        <span>&nbsp;</span>
                                                        <div class="col-md-3 colpadding0">
                                                            <label>
                                                                Reminder Type</label>
                                                        </div>
                                                        <div class="col-md-8 colpadding0">
                                                            <asp:RadioButtonList ID="rbReminderType" runat="server" RepeatDirection="Horizontal" AutoPostBack="true"
                                                                OnSelectedIndexChanged="rbReminderType_SelectedIndexChanged">
                                                                <asp:ListItem Text="Standard" Value="0" Selected="True" />
                                                                <asp:ListItem Text="Custom" Value="1" />
                                                            </asp:RadioButtonList>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row colpadding0 mt-30">
                                                    <div class="col-md-12 colpadding0 form-group" id="divForCustome" runat="server" visible="false">
                                                        <div style="float: left; width: 30%; overflow: hidden; margin-left: 200px;">
                                                            <label>
                                                                Before(In Days)
                                                            </label>
                                                            <asp:TextBox runat="server" ID="txtReminderBefore" MaxLength="4" Width="80px" />
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtReminderBefore"
                                                                runat="server" ErrorMessage="Only Numbers allowed" Display="None" ValidationExpression="^-?[0-9]\d*(\.\d+)?$" ValidationGroup="ComplianceValidationGroup" />
                                                            <asp:RequiredFieldValidator ID="rfvforcustomeReminder" ErrorMessage="Please enter reminder before value."
                                                                ControlToValidate="txtReminderBefore" runat="server" ValidationGroup="ComplianceValidationGroup"
                                                                Display="None" Enabled="false" />
                                                        </div>
                                                        <div style="overflow: hidden;">
                                                            <label>
                                                                Gap(In Days)
                                                            </label>
                                                            <asp:TextBox runat="server" ID="txtReminderGap" MaxLength="4" Width="80px" />
                                                        </div>

                                                    </div>
                                                </div>

                                                <div class="row colpadding0 mt-30">
                                                    <div class="col-md-12 colpadding0 form-group">
                                                        <div class="col-md-3 colpadding0">
                                                            <span style="color: red;">*</span>
                                                            <label>
                                                                Risk Type</label>
                                                        </div>
                                                        <div class="col-md-8 colpadding0">
                                                            <asp:DropDownListChosen runat="server" ID="ddlRiskType" AllowSingleDeselect="false" DisableSearchThreshold="5" AutoPostBack="true"
                                                                DataPlaceHolder="Select Type" class="form-control" Width="76%">
                                                                <asp:ListItem Text="< Select >" Value="-1"></asp:ListItem>
                                                                <asp:ListItem Text="High" Value="0"></asp:ListItem>
                                                                <asp:ListItem Text="Low" Value="2"></asp:ListItem>
                                                                <asp:ListItem Text="Medium" Value="1"></asp:ListItem>
                                                            </asp:DropDownListChosen>
                                                            <%--   <asp:DropDownList runat="server" ID="ddlRiskType" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                                    CssClass="txtbox">
                                                    <asp:ListItem Text="< Select >" Value="-1" />
                                                    <asp:ListItem Text="High" Value="0" />
                                                    <asp:ListItem Text="Low" Value="2" />
                                                    <asp:ListItem Text="Medium" Value="1" />

                                                </asp:DropDownList>--%>
                                                            <asp:CompareValidator ID="CompareValidator8" ErrorMessage="Please select Risk Type."
                                                                ControlToValidate="ddlRiskType" runat="server" ValueToCompare="-1" Operator="NotEqual"
                                                                ValidationGroup="ComplianceValidationGroup" Display="None" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row colpadding0 mt-30">
                                                    <div class="col-md-12 colpadding0 form-group">
                                                        <div id="divChecklist" runat="server">
                                                            <div class="col-md-3 colpadding0">
                                                                <span>&nbsp;</span><label>
                                                                    Checklist Type</label>
                                                            </div>
                                                            <div class="col-md-8 colpadding0">
                                                                <asp:DropDownListChosen runat="server" ID="ddlChklstType" AllowSingleDeselect="false" DisableSearchThreshold="5" AutoPostBack="true"
                                                                    DataPlaceHolder="Select Type" class="form-control" Width="76%" OnSelectedIndexChanged="ddlChklstType_SelectedIndexChanged">
                                                                    <asp:ListItem Text="One Time based Checklist" Value="0"></asp:ListItem>
                                                                    <asp:ListItem Text="Function based Checklist" Value="1"></asp:ListItem>
                                                                    <asp:ListItem Text="Time Based Checklist" Value="2"></asp:ListItem>
                                                                </asp:DropDownListChosen>

                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator13" ErrorMessage="Please Select Compliance Type."
                                                                    ControlToValidate="ddlComplianceType" runat="server" ValidationGroup="ComplianceValidationGroup"
                                                                    Display="None" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row colpadding0 mt-30">
                                                    <div style="margin-bottom: 7px" class="col-md-12 colpadding0 form-group">
                                                        <%--  <label style="width: 10px;  display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Event Based</label>--%>
                                                        <asp:CheckBox runat="server" Visible="false" ID="chkEventBased" CssClass="txtbox" OnCheckedChanged="chkEventBased_CheckedChanged" AutoPostBack="true" />
                                                    </div>
                                                </div>

                                                <div class="row colpadding0 mt-30">
                                                    <div id="divEvent" runat="server" class="col-md-12 colpadding0 form-group" visible="false">
                                                        <div class="row col-md-12 colpadding0 form-group">
                                                            <div class="col-md-3 colpadding0">
                                                                <span>&nbsp;</span><label>Events</label>
                                                            </div>
                                                            <div class="col-md-8 colpadding0">
                                                                <asp:DropDownListChosen runat="server" ID="ddlEvents" AllowSingleDeselect="false" DisableSearchThreshold="5" AutoPostBack="true"
                                                                    DataPlaceHolder="Select" class="form-control" Width="99%" OnSelectedIndexChanged="ddlEvents_SelectedIndexChanged">
                                                                </asp:DropDownListChosen>

                                                                <%-- <asp:DropDownList runat="server" ID="ddlEvents" AutoPostBack="true" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                            CssClass="txtbox" OnSelectedIndexChanged="ddlEvents_SelectedIndexChanged">
                                        </asp:DropDownList>--%>

                                                                <asp:CompareValidator ErrorMessage="Please Select Event." ControlToValidate="ddlEvents" ID="rfEvents"
                                                                    runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceValidationGroup"
                                                                    Display="None" />
                                                            </div>
                                                        </div>

                                                        <div class="row col-md-12 colpadding0 form-group mt-30" runat="server" id="divSubEventmode" visible="false">
                                                            <div class="col-md-3 colpadding0">
                                                                <span>&nbsp;</span>
                                                                <label>Sub Event</label>
                                                            </div>
                                                            <div class="col-md-8 colpadding0">
                                                                <asp:TextBox runat="server" ID="tbxSubEvent" CssClass="form-control txtbox" Width="75%" Height="21px" Text="< Select Sub Event >" />
                                                                <div style="margin-left: 200px; position: absolute; z-index: 10" id="divSubevent">
                                                                    <asp:TreeView runat="server" ID="tvSubEvent" BackColor="White" BorderColor="Black"
                                                                        BorderWidth="1" Height="200px" Width="394px"
                                                                        Style="overflow: auto" ShowLines="true" OnSelectedNodeChanged="tvSubEvent_SelectedNodeChanged">
                                                                    </asp:TreeView>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row col-md-12 colpadding0 form-group mt-30" id="divEventComplianceType" runat="server">
                                                            <div class="col-md-3 colpadding0">
                                                                <span>&nbsp;</span>
                                                                <label>
                                                                    Event Compliance Type</label>
                                                            </div>
                                                            <div class="col-md-8 colpadding0">
                                                                <asp:RadioButtonList ID="rbEventComplianceType" runat="server" RepeatDirection="Horizontal">
                                                                    <asp:ListItem Text="Mandatory" Value="0" Selected="True" />
                                                                    <asp:ListItem Text="Optional" Value="1" />
                                                                </asp:RadioButtonList>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row colpadding0">
                                                    <div class="col-md-12 colpadding0 form-group" id="divComplianceDueDays" runat="server">
                                                        <div class="col-md-3 colpadding0">
                                                            <span>&nbsp;</span>
                                                            <label>
                                                                Compliance Due(Day)</label>
                                                        </div>
                                                        <div class="col-md-8 colpadding0">

                                                            <asp:TextBox runat="server" ID="txtEventDueDate" MaxLength="4" CssClass="form-control txtbox" Width="20%" />
                                                            <asp:RegularExpressionValidator ID="rgexEventDueDate" ControlToValidate="txtEventDueDate"
                                                                runat="server" ErrorMessage="Only Numbers allowed" Display="None" ValidationExpression="^-?[0-9]\d*(\.\d+)?$" ValidationGroup="ComplianceValidationGroup" />
                                                            <asp:RequiredFieldValidator ID="rfvEventDue" ErrorMessage="Please enter compliance Due(Day)"
                                                                ControlToValidate="txtEventDueDate" runat="server" ValidationGroup="ComplianceValidationGroup"
                                                                Display="None" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>

                                        <asp:UpdatePanel ID="upFileUploadPanel" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <div class="row colpadding0 mt-30">
                                                    <div id="dvUploadDoc" runat="server" class="col-md-12 form-group colpadding0">
                                                        <div class="col-md-3 colpadding0">
                                                            <span style="color: red;">&nbsp;</span>
                                                            <label>
                                                                Upload Document</label>
                                                        </div>
                                                        <div class="col-md-8 colpadding0">
                                                            <asp:CheckBox runat="server" ID="chkDocument" CssClass="txtbox" OnCheckedChanged="chkDocument_CheckedChanged" AutoPostBack="true" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row colpadding0 mt-30">
                                                    <div id="dvReqForms" runat="server" class="col-md-12 colpadding0 form-group">
                                                        <div class="col-md-3 colpadding0">
                                                            <span>&nbsp;</span>
                                                            <label>
                                                                Required Forms</label>
                                                        </div>
                                                        <div class="col-md-8 colpadding0">
                                                            <asp:TextBox runat="server" ID="tbxRequiredForms" CssClass="form-control txtbox" Width="76%" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row colpadding0 mt-30">
                                                    <div class="col-md-12 colpadding0 form-group" id="Div4" runat="server">
                                                        <div class="col-md-3 colpadding0">
                                                            <span>&nbsp;</span>
                                                            <label>Sample Form Link</label>
                                                        </div>
                                                        <div class="col-md-8 colpadding0">
                                                            <asp:TextBox runat="server" ID="txtSampleFormLink" CssClass="form-control txtbox" Width="75%" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row colpadding0 mt-30">
                                                    <div class="col-md-12 colpadding0 form-group" id="dvSampleForm" runat="server">
                                                        <div class="col-md-3 colpadding0">
                                                            <span>&nbsp;</span>
                                                            <label>
                                                                Sample Form</label>
                                                        </div>
                                                        <div class="col-md-8 colpadding0">
                                                            <%-- <asp:Label runat="server" ID="lblSampleForm" CssClass="txtbox" />--%>
                                                            <asp:FileUpload AllowMultiple="true" runat="server" ID="fuSampleFile" />
                                                            <asp:HiddenField runat="server" ID="hdnFile" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row colpadding0 mt-30">
                                                    <div style="margin-bottom: 7px" id="divSampleForm" runat="server" class="col-md-12 colpadding0 form-group">
                                                        <asp:Panel ID="Panel2" Width="100%" Height="100px" ScrollBars="Vertical" runat="server">
                                                            <asp:GridView runat="server" ID="grdSampleForm" AutoGenerateColumns="false" GridLines="Vertical"
                                                                BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true"
                                                                CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="10" Width="100%"
                                                                Font-Size="12px" DataKeyNames="ID" OnRowCommand="grdSampleForm_RowCommand">
                                                                <Columns>
                                                                    <asp:BoundField DataField="ID" HeaderText="ID" ItemStyle-Width="50px" SortExpression="ID" />
                                                                    <asp:TemplateField HeaderText="Form Name" ItemStyle-Height="20px" HeaderStyle-Height="20px" ItemStyle-Width="100px">
                                                                        <ItemTemplate>
                                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                                                                <asp:Label runat="server" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Action" ItemStyle-Width="140px" ItemStyle-HorizontalAlign="Center">
                                                                        <ItemTemplate>
                                                                            <asp:UpdatePanel ID="upFileUploadPanel" runat="server">
                                                                                <ContentTemplate>
                                                                                    <asp:LinkButton ID="lbkDownload" runat="server" CommandName="DownloadSampleForm" CommandArgument='<%# Eval("ID") + "," + Eval("ComplianceID") %>'><img src="../Images/downloaddoc.png" alt="Download"/></asp:LinkButton>
                                                                                    <asp:LinkButton ID="lbkDelete" runat="server" CommandName="DeleleSampleForm" CommandArgument='<%# Eval("ID") + "," + Eval("ComplianceID") %>'
                                                                                        OnClientClick="return confirm('Are you sure you want to delete this Sampleform?');"><img src="../Images/delete_icon.png" alt="Delete Form"/></asp:LinkButton>
                                                                                    <asp:LinkButton ID="lbkView" runat="server" OnClientClick="fopendocfileReview()" CommandArgument='<%# Eval("ID") + "," + Eval("ComplianceID") %>'><img src="../Images/package_icon.png" alt="View Form"/></asp:LinkButton>
                                                                                </ContentTemplate>
                                                                                <Triggers>
                                                                                    <asp:PostBackTrigger ControlID="lbkDownload" />
                                                                                    <asp:AsyncPostBackTrigger ControlID="lbkDelete" />
                                                                                </Triggers>
                                                                            </asp:UpdatePanel>
                                                                        </ItemTemplate>
                                                                        <HeaderTemplate>
                                                                        </HeaderTemplate>
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                                <FooterStyle BackColor="#CCCC99" />
                                                                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                                                                <PagerSettings Position="Top" />
                                                                <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                                                                <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                                                <AlternatingRowStyle BackColor="#E6EFF7" />
                                                                <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                                                                <EmptyDataTemplate>
                                                                    No Records Found.
                                                                </EmptyDataTemplate>
                                                            </asp:GridView>
                                                        </asp:Panel>
                                                    </div>
                                                </div>
                                                <div style="margin-bottom: 7px; margin-left: 210px" id="Div3" runat="server">
                                                    <asp:Label ID="Label2" runat="server"></asp:Label>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>

                                        <div class="row colpadding0 mt-30">
                                            <div class="col-md-12 colpadding0 text-center">
                                            <label>&nbsp;</label>
                                            <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" OnClientClick="if (!ValidateFile()) return false;" CssClass="button btn btn-primary"
                                                ValidationGroup="ComplianceValidationGroup" />
                                            <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="button btn btn-primary" OnClientClick="$('#divComplianceDetailsDialog').dialog('close');" />
                                        </div>
                                            </div>
                                        <div style="margin-bottom: 7px; margin-left: 10px; margin-top: 30px;">
                                            <p style="color: red;"><strong>Note:</strong> (*) Fields Are Compulsary</p>
                                            <%--<label style="width: 208px; display: block; float: left; font-size: 13px; color: red;">Note :: (*) Fields Are Compulsary</label>--%>
                                        </div>
                                    </div>
                                </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="btnSave" />
                        </Triggers>
                    </asp:UpdatePanel>
                    <asp:HiddenField ID="saveopo" runat="server" Value="false" />
                </div>
            </div>
        </div>
            </div>
    </form>
</body>
</html>
