﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AgendaDocumentViewerSyncfusion.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Areas_Secretarial.AgendaDocumentViewerSyncfusion" %>

<!DOCTYPE html>
<%@ Register TagPrefix="GleamTech" Namespace="GleamTech.DocumentUltimate.Web" Assembly="GleamTech.DocumentUltimate" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    
    <!---->

 <%--    <link href="../NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../NewCSS/bootstrap-theme.css" rel="stylesheet" />

    <link href="../NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <!-- Custom styles -->
    <link href="../NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="../NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="../Newjs/jquery.js"></script>

    <script type="text/javascript" src="../Newjs/jquery-ui-1.9.2.custom.min.js"></script>

    <script src="../Scripts/modernizr-2.8.3.js"></script>
    <link href="../NewCSS/SimpleIcons.css" rel="stylesheet" />

    <script src="../Content/2018.3.1017/js/kendo.all.min.js"></script>
    <script src="../Content/2018.3.1017/js/kendo.aspnetmvc.min.js"></script>

    <script src="../Scripts/jquery.validate.js"></script>
    <script src="../Scripts/jquery.validate.unobtrusive.min.js"></script>
    <link href="../Content/css/Kendouicss.css" rel="stylesheet" />--%>

</head>
<body style="overflow-y:hidden;">
    <form id="form1" runat="server">
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
        <table style="width:100%">
            <tr>
                <td style="width:20%; vertical-align:top; display:none;">
                    <asp:Label ID="lblFileName" runat="server" Text="" ></asp:Label>
                </td>
                <td style="width:90%">
                    <div>
                        <GleamTech:DocumentViewer runat="server" Width="100%" ID="doccontrol" FullViewport="false" searchControl="true" SidePaneVisible="true" Height="560px"
                            DownloadAsPdfEnabled="true" DisableHeaderIncludes="false" ClientLoad="documentViewerLoad" ToolbarVisible="true" />
                    </div>   
                </td>
            </tr>
        </table>
    </form>
</body>
</html>

<script type="text/javascript">
    function documentViewerLoad(sender, e) {
        debugger;
        var documentViewer = sender; //sender parameter will be the DocumentViewer instance

        documentViewer.setZoomLevel(1); // Set zoom to 100%
        documentViewer.fitWidth(); // Default behaviour

        window.parent.myfunction();

        //setTimeout(documentViewerLoadNew, 3000);
    }


    //function documentViewerLoadNew() {
    //    $("#ui-id-2").parent().addClass("ui-state-default ui-corner-top ui-tabs-active ui-state-active");
    //    $("#ui-id-2").trigger("click");
    //}
    //$(document).ready(function () {
    //});
</script>

