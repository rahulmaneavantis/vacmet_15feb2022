﻿using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas_Secretarial
{
    public partial class RegisterDocViewer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["docurl"]))
                {
                    string Filename = Request.QueryString["docurl"].ToString();
                    if (Filename != "undefined")
                    {
                        int CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                        doccontrol.Document = Server.MapPath("~/Temp/"+ CustomerId +"/"+ Filename);
                       
                    }
                }
            }
        }
    }
}