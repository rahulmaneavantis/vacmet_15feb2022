﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="externalogin.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.externalogin" %>
 

<!DOCTYPE html>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc2" %>
<%@ Register Assembly="GoogleReCaptcha" Namespace="GoogleReCaptcha" TagPrefix="cc1" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="AVANTIS - Products that simplify" />
    <meta name="author" content="AVANTIS - Development Team" />
    
    <title>Login :: AVANTIS - Products that simplify</title>  
    <!-- Bootstrap CSS -->
     <link href="https://avacdn.azureedge.net/newcss/bootstrap.min.css" rel="stylesheet" />
    <!-- bootstrap theme -->
    <link href="https://avacdn.azureedge.net/newcss/bootstrap-theme.css" rel="stylesheet" />             
    <!--external css-->
    <!-- font icon -->
    <link href="https://avacdn.azureedge.net/newcss/elegant-icons-style.css" rel="stylesheet" />
    <link href="https://avacdn.azureedge.net/newcss/font-awesome.css" rel="stylesheet" />
    <!-- Custom styles -->
    <link href="https://avacdn.azureedge.net/newcss/style.css" rel="stylesheet" />
    <link href="https://avacdn.azureedge.net/newcss/style-responsive.css" rel="stylesheet" />
    <style type="text/css">
        .otpdiv > span > label {
            border: 0px;
            color: black;
            /* margin-bottom: 5px; */
            margin-left: -17px;
            margin-top: 2px;
            position: absolute;
            width: 111px;

        }
      .otpdiv > span  {
            width: 233px;
            height: 26px;
            margin: auto;
            position: relative;
            /* background: #fff; */
            /* border: 1px solid #2e2e2e; */
            border-radius: 2px;
            -webkit-border-radius: 2px;
            -moz-border-radius: 2px;
        }
        .otpdiv > span > input {
            width: 80px;
            height: 26px;
            margin: auto;
            position: relative;           
            border-radius: 2px;
            -webkit-border-radius: 2px;
            -moz-border-radius: 2px;
            margin-left: -9px;
        }
    </style>
    <script type="text/javascript">
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-92029518-1', 'auto');
  ga('send', 'pageview');

  function settracknew(e, t, n, r) {
      try {
          ga('send', 'event', e, t, n + "#" + r)
      } catch (t) { } return !0
  }

  function settracknewnonInteraction(e, t, n, r) {
      ga('send', 'event', e, t, n, 0, { 'nonInteraction': 1 })
  }

  settracknewnonInteraction("Login", 'Login', "googlelogin", "0");
  
</script>
   
</head>
<body>
    <div class="container">
        <%--<form runat="server" class="login-form" name="login" id="loginForm" autocomplete="off">--%>
            <form runat="server" class="login-form" name="login">
        
                        <div class="col-md-12 login-form-head">
                            <p class="login-img">  <a href="https://teamleaseregtech.com">
                                <img src="Images/TeamLease1.png" /></a>
                            </p>
                        </div>

                        <div class="login-wrap">
                            <div id="divLogin" class="row" runat="server">
                               
                                                   
                            <asp:CustomValidator ID="cvLogin" class="alert alert-block alert-danger fade in" EnableClientScript="False" runat="server" Display="None" />
                           
                                <div runat="server" id="message" style="font-size: 21px;color:black"></div>
                           
                             </div>

                        <div style="clear: both; height: 10px;"></div>
                        <a href="/login.aspx"> Go back to login page </a>
                            </div>
              
        </form>
    </div>
    <div class="clearfix" style="height: 10px"></div>
    <!--js-->
 
    <script src='https://www.google.com/recaptcha/api.js'></script>       
    <script type="text/javascript" src="https://avacdn.azureedge.net/newjs/jquery.js"></script>
    <script type='text/javascript' src="https://avacdn.azureedge.net/newjs/bootstrap.min.js"></script>   
<%--    <script src="js/jquery-ui-1.10.4.min.js"></script>--%>

  <script type="text/javascript">
     
        $(function () {
            $("#chkOTP").click(function () {
                if ($(this).is(":checked")) {
                    $("#dvCaptcha").hide();
                    $("#dvOr").hide();
                } else {
                    $("#dvCaptcha").show();
                    $("#dvOr").show();
                }
            });
        });
        $(document).tooltip({ selector: '[data-toggle="tooltip"]' });
    </script>
</body>
</html>
