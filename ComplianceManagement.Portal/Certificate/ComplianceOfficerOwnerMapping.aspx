﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="ComplianceOfficerEventMapping.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Certificate.ComplianceOfficerOwnerMapping" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   <style type="text/css">
       .custom-combobox-input {
    margin: 0;
    padding: 0.3em;
    width: 310px;
}
   </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
     <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
     <asp:UpdatePanel ID="upcomList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <table width="100%">
                <tr>
                    <td align="right">
                          <asp:DropDownList runat="server" ID="ddlCustomerFilter" Style="padding: 0px; margin: 0px; height: 22px; width: 350px;"
                            CssClass="txtbox" OnSelectedIndexChanged="ddlCustomerFilter_SelectedIndexChanged"  AutoPostBack="true"/>
                    </td>
                    <td align="right" style="width: 20%">
                       
                    </td>
                    <td align="right" style="width: 25%">
                     
                    </td>
                    <td class="newlink" align="right">
                        <asp:LinkButton Text="Add New" runat="server" ID="btnAddAct" OnClick="btnAddOwner_Click"  />
                    </td>
                </tr>
            </table>
            <asp:GridView runat="server" ID="grdComplianceOfficerMapping" AutoGenerateColumns="false" GridLines="Vertical" 
                BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true"
                CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="13" Width="100%" OnPageIndexChanging="grdComplianceOfficerMapping_PageIndexChanging"
                Font-Size="12px" DataKeyNames="ID" OnRowCommand="grdComplianceOfficerMapping_RowCommand">
                <Columns>
                   <%-- <asp:BoundField DataField="ID" HeaderText="Act ID" ItemStyle-VerticalAlign="Top" ItemStyle-Width="10px" />--%>
                      <asp:TemplateField HeaderText="Sr.No">
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex+1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="Customer Name" ItemStyle-Height="25px" HeaderStyle-Height="20px" SortExpression="Name">
                        <ItemTemplate>
                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px">
                                <asp:Label runat="server" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Officer Name" ItemStyle-Height="25px" HeaderStyle-Height="20px" SortExpression="UserName">
                        <ItemTemplate>
                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                <asp:Label runat="server" Text='<%# Eval("UserName") %>' ToolTip='<%# Eval("UserName") %>'></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                   
                               <asp:TemplateField ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:LinkButton ID="lbtEdit" runat="server" CommandName="EDIT_OfficerOwner" CommandArgument='<%# Eval("ID") %>'><img src="../Images/edit_icon.png" alt="Edit Owner" title="Edit Owner" /></asp:LinkButton>
                            <asp:LinkButton ID="lbtDelete" runat="server" CommandName="DELETE_OfficerOwner" CommandArgument='<%# Eval("ID") %>'
                                OnClientClick="return confirm('Are you certain you want to delete this Owner?');"><img src="../Images/delete_icon.png" alt="Delete Owner" title="Delete Owner" /></asp:LinkButton>

                        </ItemTemplate>
                        <HeaderTemplate>
                        </HeaderTemplate>
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                </Columns>
                <FooterStyle BackColor="#CCCC99" />
                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                <PagerSettings Position="Top" />
                <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                <AlternatingRowStyle BackColor="#E6EFF7" />
                <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                <EmptyDataTemplate>
                    No Records Found.
                </EmptyDataTemplate>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>

     <div id="divOfficerdetailsDialog" style="height: auto;">
        <asp:UpdatePanel ID="upcom" runat="server" UpdateMode="Conditional" OnLoad="upCom_Load">
            <ContentTemplate>
                <div style="margin: 5px">
                    <div style="margin-bottom: 4px">
                        <asp:ValidationSummary runat="server" CssClass="vdsummary"
                            ValidationGroup="ComValidationGroup" />
                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                            ValidationGroup="ComValidationGroup" Display="None" />
                    </div>

                    <div style="margin-bottom: 7px" runat="server" visible="false" id="divcustomer">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                          Customer
                        </label>
                        <asp:DropDownList runat="server" ID="ddlCustomer" Style="padding: 0px; margin: 0px; height: 22px; width: 350px;"
                            CssClass="txtbox" OnSelectedIndexChanged="ddlCustomer_SelectedIndexChanged" AutoPostBack="true" />
                        <asp:CompareValidator ID="CompareValidator1" ErrorMessage="Please select Customer." ControlToValidate="ddlCustomer"
                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComValidationGroup"
                            Display="None" />
                    </div>

                   

                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                          Compliance Officer
                        </label>
                        <asp:DropDownList runat="server" ID="ddlComplianceOfficer" Style="padding: 0px; margin: 0px; height: 22px; width: 350px;"
                            CssClass="txtbox" OnSelectedIndexChanged="ddlComplianceOfficer_SelectedIndexChanged" AutoPostBack="true" />
                        <asp:CompareValidator ID="CompareValidator3" ErrorMessage="Please select Compliance Officer." ControlToValidate="ddlComplianceOfficer"
                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComValidationGroup"
                            Display="None" />
                    </div>

                   
                   
                   



                    <%--     Compliance Owner  --%>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Compliance Owner 
                        </label>
                        <asp:TextBox runat="server" ID="txtComplianceOwner" Style="padding: 0px; margin: 0px; height: 22px; width: 350px;"
                            CssClass="txtbox" />
                        <div style="width: 56.5%; margin-left: 160px; position: absolute; z-index: 50; overflow-y: auto; background: white; border: 1px solid gray; height: 100px;" id="dvOwner">
                            <asp:Repeater ID="rptComplianceOwner" runat="server">
                                <HeaderTemplate>
                                    <table class="detailstable FadeOutOnEdit" id="RepeaterTable">
                                        <tr>
                                            <td style="width: 100px">
                                                <asp:CheckBox ID="OwnerSelectAll" Text="Select All" runat="server" onclick="checkOwnerAll(this)" /></td>
                                            <td style="width: 282px;">
                                                <asp:Button runat="server" ID="btnRepeaterOwner" Text="Ok" Style="float: left" onclick="btnRepeaterOwner_Click" /></td>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td style="width: 20px">
                                            <asp:CheckBox ID="chkCompowner" runat="server" onclick="UncheckOwnerHeader();" /></td>
                                        <td style="width: 200px;">
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 250px; padding-bottom: 5px;">
                                                <asp:Label ID="lblOwnerID" runat="server" Visible="false" Text='<%# Eval("ID")%>' ToolTip='<%# Eval("ID") %>'></asp:Label>
                                                <asp:Label ID="lblOwnerName" runat="server" Text='<%# Eval("Name")%>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                            </div>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                    </div>

                 

                    <div id="DivSave" style="margin-bottom: 7px; margin-top: 4%; margin-left: 152px;" runat="server">
                        <asp:Button Text="Save" runat="server" ID="btnSave" OnClientClick="if (!ValidateFile()) return false;"  CssClass="button" ValidationGroup="ComValidationGroup" OnClick="btnSave_Click" />
                        <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="button" onclick="btnCancel_Click"/>
                    </div>

                    <div style="margin-bottom: 7px; margin-left: 10px; margin-top: 10px;">

                        <p style="color: red;"><strong>Note:</strong> (*) Fields Are Compulsary</p>

                        <%--   <label style="width: 208px; display: block; float: left; font-size: 13px; color: red;">Note :: (*) Fields Are Compulsary</label>--%>
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnSave" />
            </Triggers>
        </asp:UpdatePanel>
        <asp:HiddenField ID="saveopo" runat="server" Value="false" />
    </div>

      <script type="text/javascript">
        $(function () {
            $('#divOfficerdetailsDialog').dialog({
                width: 600,
                height:300,
                autoOpen: false,
                draggable: true,
                title: "Compliance Officer Owner Mapping",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });
            if (document.getElementById('BodyContent_saveopo').value == "true") {
                newfun();
                document.getElementById('BodyContent_saveopo').value = "false";
            }
            else {
                $("#divOfficerdetailsDialog").dialog('close');
            }
        });

        function newfun() {
            $("#divOfficerdetailsDialog").dialog('open');

        }
        function initializeCombobox() {
            $("#<%= ddlComplianceOfficer.ClientID %>").combobox();
             $("#<%= ddlCustomer.ClientID %>").combobox();
           

        }

       

       
      
        function checkOwnerAll(cb) {
            var ctrls = document.getElementsByTagName('input');
            for (var i = 0; i < ctrls.length; i++) {
                var cbox = ctrls[i];
                if (cbox.type == "checkbox" && cbox.id.indexOf("chkCompowner") > -1) {
                    cbox.checked = cb.checked;
                }
            }
        }
       
        function UncheckOwnerHeader() {
            var rowCheckBox = $("#RepeaterTable input[id*='chkCompowner']");
            var rowCheckBoxSelected = $("#RepeaterTable input[id*='chkCompowner']:checked");
            var rowCheckBoxHeader = $("#RepeaterTable input[id*='OwnerSelectAll']");
            if (rowCheckBox.length == rowCheckBoxSelected.length) {
                rowCheckBoxHeader[0].checked = true;
            } else {

                rowCheckBoxHeader[0].checked = false;
            }
        }

       

       
     
        function initializeJQueryUI1(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }
    </script>
</asp:Content>
