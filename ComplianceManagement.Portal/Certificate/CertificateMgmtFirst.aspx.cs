﻿using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Certificate
{
    public partial class CertificateMgmtFirst : System.Web.UI.Page
    {
        protected static string Path;
        protected static string FlagIsApp;
        protected static int CustId;
        protected static int UId;
        protected void Page_Load(object sender, EventArgs e)
        {
            Path = ConfigurationManager.AppSettings["KendoPathApp"];
            CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            UId = Convert.ToInt32(AuthenticationHelper.UserID);
            FlagIsApp = Convert.ToString(AuthenticationHelper.Role);
        }
        protected void btnCreateCertificate_Click(object sender, EventArgs e)
        {
            try
            {
                string val = HttpUtility.UrlDecode(datastringNew.Text.ToString());
                RadDisplayControl.Content = HttpUtility.UrlDecode(datastringNew.Text.ToString());

                RadDisplayControl.TrackChangesSettings.Author = "Performer";
                RadDisplayControl.TrackChangesSettings.CanAcceptTrackChanges = true;
                RadDisplayControl.TrackChangesSettings.UserCssId = "reU9";
                RadDisplayControl.TrackChangesSettings.CanAcceptTrackChanges = false;
                RadDisplayControl.EnableTrackChanges = false;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}