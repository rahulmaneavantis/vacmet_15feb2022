﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CertificateMgmtFirstDashboard.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Certificate.CertificateMgmtFirstDashboard" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jszip.min.js"></script>

    <style type="text/css">
        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            background: white;
            border: none;
        }

        .k-textbox .k-icon {
            top: 50%;
            margin: -7px 5px 0px;
            position: absolute;
        }

        .k-active-filter, .k-state-active, .k-state-active:hover {
            background-color: #E9EAEA;
            border-color: #a6a6ad;
            color: #535b6a;
        }

        .div.k-grid-footer, div.k-grid-header {
            border-top-width: 1px;
            margin-right: 0px;
        }

        .k-grid-footer-wrap, .k-grid-header-wrap {
            position: relative;
            width: 100%;
            overflow: hidden;
            border-style: solid;
            border-width: 0 1px 0 0;
            zoom: 1;
        }

         .k-grid-content {
            max-height: 180px !important;
        }

            .k-grid-content k-auto-scrollable {
                height: auto;
            }

        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 2.0em;
            border-bottom-width: 1px;
            background-color: white;
            border-width: 0 1px 1px 0px;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
            background-color: white;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: white;
        }

        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 6px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: white;
        }

        html .k-grid tr.k-alt:hover {
            background: white;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
            margin-right: 2px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
            text-align: center !important;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: white;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 0px 1px 0px;
            text-align: center;
        }

        .k-grid-pager {
            margin-top: -1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-i-arrow-60-down, .k-i-arrow-60-left, .k-i-arrow-60-right, .k-i-arrow-60-up {
            cursor: pointer;
            margin-top: 6px;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            /* border-width: 0px 0px 1px 0px;*/
            background: #E9EAEA;
            font-weight: bold;
            margin-right: 18px;
            font-family: 'Roboto',sans-serif;
            font-size: 15px;
            color: rgba(0, 0, 0, 0.5);
            height: 16px;
            vertical-align: middle;
        }

        .k-dropdown-wrap.k-state-default {
            background-color: white;
        }

        .k-popup.k-calendar-container, .k-popup.k-list-container {
            background-color: white;
        }

        .k-list > .k-state-focused.k-state-selected, .k-listview > .k-state-focused.k-state-selected, .k-state-focused.k-state-selected, td.k-state-focused.k-state-selected {
            -webkit-box-shadow: inset 0 0 3px 1px #14699f;
            box-shadow: none;
        }

        label.k-label {
            font-family: roboto,sans-serif !important;
            color: #515967;
            /* font-stretch: 100%; */
            font-style: normal;
            font-weight: 400;
            min-width: 362px;
            white-space: pre-wrap;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: flex;
            margin-bottom: 0px;
        }

        .k-state-default > .k-select {
            border-color: #ceced2;
            margin-top: 0px;
        }

        .k-grid-norecords {
            width: 100%;
            height: 100%;
            text-align: center;
            margin-top: 2%;
        }

        .k-grouping-header {
            font-style: italic;
        }

        .k-grid-toolbar {
            background: white;
            border: none;
            margin: 5px;
        }

        .k-grid table {
            width: 100.5%;
        }

        .k-grid-header-wrap.k-auto-scrollable {
            width: 100% !important;
        }

        .k-multiselect-wrap .k-input {
            display: inherit !important;
        }
    </style>

    <script type="text/javascript">
        $(document).ready(function () {
            Bindgrid();
        });

        var dataSource = new kendo.data.DataSource({
            transport: {
                read: {
                    url: '<%=Path%>Data/ComplianceCertificateMgmtFirst?CustId=<% =CustId%>&UId=<% =UId%>', 
                },
            },
            pageSize: 4,
            schema: {
                model: {
                    fields: {
                        DueDate: { type: "date" },
                    }
                }
            }
        });

        function Bindgrid() {
            var grid = $('#grid').data("kendoGrid");
            if (grid != undefined || grid != null)
                $('#grid').empty();


            var grid = $("#grid").kendoGrid({
                dataSource: dataSource,
                excel: {
                    allPages: true,
                },
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                height: 180,
                sortable: false,
                // groupable: true,
                filterable: false,
                columnMenu: false,
                //pageable: true,
                reorderable: false,
                resizable: false,
                multi: true,
                selectable: false,
                dataBound: function () {
                    for (var i = 0; i < this.columns.length; i++) {
                        this.autoWidth;
                    }
                },
                columns: [
                    {
                        field: "Formonth", title: 'Periods',
                        attributes: {
                            style: 'white-space: nowrap;text-align:center;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "20%"
                    },
                    //{
                    //    field: "SubmissionStatusPer", title: 'Submission Status %', template: '#=SubmissionStatusPer#' + '<span class="k-icon k-i-percent"></span>',
                    //    attributes: {
                    //        style: 'white-space: nowrap;text-align:center;'

                    //    }, filterable: {
                    //        multi: true,
                    //        extra: false,
                    //        search: true,
                    //        operators: {
                    //            string: {
                    //                eq: "Is equal to",
                    //                neq: "Is not equal to",
                    //                contains: "Contains"
                    //            }
                    //        }
                    //    }, width: "30%"
                    //},
                    //{
                    //    field: "DueDate", title: 'Due Date',
                    //    type: "date",
                    //    format: "{0:dd-MMM-yyyy}",
                    //    //template: "#= kendo.toString(kendo.parseDate(ScheduledOn, 'yyyy-MM-dd'), 'dd-MMM-yyyy') #",
                    //    filterable: {
                    //        multi: true,
                    //        extra: false,
                    //        search: true,
                    //        operators: {
                    //            string: {
                    //                type: "date",
                    //                format: "{0:dd-MMM-yyyy}",
                    //                eq: "Is equal to",
                    //                neq: "Is not equal to",
                    //                contains: "Contains"
                    //            }
                    //        }
                    //    }, width: "30%"
                    //},
                    {
                        title: "Status as on Certificate Date",
                        attributes: {
                            style: 'white-space: nowrap; text-align: center;'
                        },
                        columns: [
                            {
                                field: "ComplianceStatusClosedTimelyPer", title: 'Completed %', template: '#=ComplianceStatusClosedTimelyPer#' + '<span class="k-icon k-i-percent"></span>',
                                attributes: {
                                    style: 'white-space: nowrap;text-align:center;'

                                }, filterable: {
                                    multi: true,
                                    extra: false,
                                    search: true,
                                    operators: {
                                        string: {
                                            eq: "Is equal to",
                                            neq: "Is not equal to",
                                            contains: "Contains"
                                        }
                                    }
                                }, width: "20%"
                            }, 
                            {
                                field: "ComplianceStatusOpenPer", title: 'Overdue %', template: '#=ComplianceStatusOpenPer#' + '<span class="k-icon k-i-percent"></span>',
                                attributes: {
                                    style: 'white-space: nowrap;text-align:center;'

                                }, filterable: {
                                    multi: true,
                                    extra: false,
                                    search: true,
                                    operators: {
                                        string: {
                                            eq: "Is equal to",
                                            neq: "Is not equal to",
                                            contains: "Contains"
                                        }
                                    }
                                }, width: "20%"
                            }]

                    },
                    {
                        title: "Status as on Current Date",
                        attributes: {
                            style: 'white-space: nowrap; text-align: center;'
                        },
                        columns: [
                            {
                                field: "AsOnStatusClosedTimelyPer", title: 'Completed %', template: '#=AsOnStatusClosedTimelyPer#' + '<span class="k-icon k-i-percent"></span>',
                                attributes: {
                                    style: 'white-space: nowrap;'

                                }, filterable: {
                                    multi: true,
                                    extra: false,
                                    search: true,
                                    operators: {
                                        string: {
                                            eq: "Is equal to",
                                            neq: "Is not equal to",
                                            contains: "Contains"
                                        }
                                    }
                                }, width: "20%",
                                attributes: {
                                    "class": "table-cell",
                                    style: "text-align: center; font-size: 14px"
                                }
                            },
                            {
                                field: "AsOnStatusOpenPer", title: 'Overdue %', template: '#=AsOnStatusOpenPer#' + '<span class="k-icon k-i-percent"></span>',
                                attributes: {
                                    style: 'white-space: nowrap;'

                                }, filterable: {
                                    multi: true,
                                    extra: false,
                                    search: true,
                                    operators: {
                                        string: {
                                            eq: "Is equal to",
                                            neq: "Is not equal to",
                                            contains: "Contains"
                                        }
                                    }
                                }, width: "20%",
                                attributes: {
                                    "class": "table-cell",
                                    style: "text-align: center; font-size: 14px"
                                }
                            }]
                    },
                ]

            });

            $(document).on("click", "#grid tbody tr .ob-overviewMain", function (e) {
                var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));

                window.location.href = "../Certificate/CertificateMgmtSecond.aspx?SID=" + item.CerScheduleonID + "&Period=" + item.Formonth;
                return true;
            });

        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
           <asp:ScriptManager ID="Isdf" runat="server"></asp:ScriptManager>
        <div class="row">
            <div id="grid" style="margin: 4px;"></div>
        </div>
    </form>
</body>
</html>
