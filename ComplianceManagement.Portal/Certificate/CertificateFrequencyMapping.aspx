﻿<%@ Page Title="Certificate Configration" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" ValidateRequest="false"
    CodeBehind="CertificateFrequencyMapping.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Certificate.CertificateFrequencyMapping" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../NewCSS/ComplienceStyleSeet.css" rel="stylesheet" />
    <script src="../Newjs/tagging.js" type="text/javascript"></script>
    <link href="../NewCSS/bootstrap-tagsinput.css" rel="stylesheet" />
    <link href="../NewCSS/bootstrap.min.css" rel="stylesheet" />
    <link href="../NewCSS/bootstrap-theme.css" rel="stylesheet" />
    <script type="text/javascript">
        function initializeCombobox() {
        }
    </script>
    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }

        .ui-state-active {
            border: 1px solid #fad42e !important;
            background: #fbec88 url(images/ui-bg_flat_55_fbec88_40x100.png) 50% 50% repeat-x !important;
            color: #363636 !important;
        }

        .ui-datepicker-div {
            top: 482.453px !important;
        }

        label {
            margin-bottom: 0px !important;
        }

        input.custom-combobox-input.ui-widget.ui-widget-content.ui-state-default.ui-corner-left.ui-autocomplete-input {
            width: 470px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upActList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <table width="100%">
                <tr>
                    <td align="right">
                      
                    </td>
                    <td align="right" style="width: 20%">
                   
                    </td>
                    <td align="right" style="width: 25%">Filter :
                        <asp:TextBox runat="server" ID="tbxFilter" Width="250px" MaxLength="50" AutoPostBack="true"
                            OnTextChanged="tbxFilter_TextChanged" />
                    </td>
                    <td class="newlink" align="right">
                        <asp:LinkButton Text="Add New" runat="server" ID="btnAddAct" OnClick="btnAddAct_Click" />
                    </td>
                </tr>
            </table>
            <asp:GridView runat="server" ID="grdCertificateFrequency" AutoGenerateColumns="false" GridLines="Vertical"
                BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true"
                CellPadding="4" ForeColor="Black" OnRowCommand="grdCertificateFrequency_RowCommand" OnRowEditing="grdCertificateFrequency_RowEditing"  AllowPaging="False" PageSize="13" Width="100%"
                Font-Size="12px" DataKeyNames="ID">
                <Columns>
                    <asp:TemplateField HeaderText="Customer Name" ItemStyle-Height="25px" HeaderStyle-Height="20px">
                        <ItemTemplate>
                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 900px">
                                <asp:Label runat="server" Text='<%# Eval("CustomerName") %>' ToolTip='<%# Eval("CustomerName") %>'></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Certificate Frequency" ItemStyle-Height="25px" HeaderStyle-Height="20px">
                        <ItemTemplate>
                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                <asp:Label runat="server" Text='<%# Eval("CertificateFrequency") %>' ToolTip='<%# Eval("CertificateFrequency") %>'></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                   <asp:TemplateField HeaderText="Due Date" ItemStyle-Height="25px" HeaderStyle-Height="20px">
                        <ItemTemplate>
                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                <asp:Label runat="server" Text='<%# Eval("DueDate") %>' ToolTip='<%# Eval("DueDate") %>'></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                     
                    <asp:TemplateField HeaderText="Start Date" ItemStyle-Height="25px" HeaderStyle-Height="20px">
                        <ItemTemplate>
                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                <asp:Label runat="server" Text='<%# Convert.ToDateTime(Eval("StartDate")).ToString("dd-MMM-yyyy") %>' ToolTip='<%# Convert.ToDateTime(Eval("StartDate")).ToString("dd-MMM-yyyy") %>'></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                   
                     <asp:TemplateField ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:LinkButton ID="lbtEdit" runat="server" CommandName="EDIT" CommandArgument='<%# Eval("ID") %>'><img src="../Images/edit_icon.png" alt="Edit Configration" title="Edit Configration" /></asp:LinkButton>
                            <asp:LinkButton ID="lbtDelete" runat="server" CommandName="DELETE" CommandArgument='<%# Eval("ID") %>'
                                OnClientClick="return confirm('Are you certain you want to delete this Configration?');"><img src="../Images/delete_icon.png" alt="Delete Configration" title="Delete Configration" /></asp:LinkButton>

                        </ItemTemplate>
                        <HeaderTemplate>
                        </HeaderTemplate>
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                </Columns>
                <FooterStyle BackColor="#CCCC99" />
                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                <PagerSettings Position="Top" />
                <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                <AlternatingRowStyle BackColor="#E6EFF7" />
                <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                <EmptyDataTemplate>
                    No Records Found.
                </EmptyDataTemplate>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div id="divActDialog" style="height: auto;">
        <asp:UpdatePanel ID="upAct" runat="server" UpdateMode="Conditional" OnLoad="upAct_Load">
            <ContentTemplate>
                <div style="margin: 5px">
                    <div style="margin-bottom: 4px">
                        <asp:ValidationSummary runat="server" CssClass="vdsummary"
                            ValidationGroup="ActValidationGroup" />
                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                            ValidationGroup="ActValidationGroup" Display="None" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 250px; display: block; float: left; font-size: 13px; color: #333;">
                            Customer</label>
                        <asp:DropDownList runat="server" ID="ddlCustomer" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;"
                            CssClass="txtbox" AutoPostBack="true">
                        </asp:DropDownList>
                        <asp:CompareValidator ID="CompareValidator5" ErrorMessage="Please select Customer"
                            ControlToValidate="ddlCustomer" runat="server" ValueToCompare="-1" Operator="NotEqual"
                            ValidationGroup="ActValidationGroup" Display="None" />
                    </div>

                    <div runat="server" id="div1" style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 250px; display: block; float: left; font-size: 13px; color: #333;">
                            Start Date</label>
                        <asp:TextBox runat="server" ID="tbxStartDate" Style="padding: 0px; margin: 0px; height: 30px; width: 250px;" CssClass="StartDate" />
                        <asp:RequiredFieldValidator ID="rfvStartDate" ErrorMessage="Please Select Start Date."
                            ControlToValidate="tbxStartDate" runat="server" ValidationGroup="ActValidationGroup"
                            Display="None" />
                    </div>

                   <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 250px; display: block; float: left; font-size: 13px; color: #333;">
                            Frequency</label>
                        <asp:DropDownList runat="server" ID="ddlFrequency" OnSelectedIndexChanged="ddlFrequency_SelectedIndexChanged" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;"
                            CssClass="txtbox" AutoPostBack="true">
                        </asp:DropDownList>
                        <asp:CompareValidator ID="CompareValidator1" ErrorMessage="Please select Frequency"
                            ControlToValidate="ddlFrequency" runat="server" ValueToCompare="-1" Operator="NotEqual"
                            ValidationGroup="ActValidationGroup" Display="None" />
                    </div>
                    <div id="divFinancial" runat="server" visible="false" style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 250px; display: block; float: left; font-size: 13px; color: #333;">
                            Is Financial Year </label>
                        <asp:RadioButtonList ID="rbFinancial" runat="server" RepeatDirection="Horizontal" AutoPostBack="true">
                            <asp:ListItem Text="Financial Year" Value="1" Selected="True" />
                            <asp:ListItem Text="Calendar Year" Value="0" />
                        </asp:RadioButtonList>
                    </div>
                    <div style="margin-bottom: 7px" id="vivDueDate" runat="server">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 250px; display: block; float: left; font-size: 13px; color: #333;">
                            Due Date</label>
                        <asp:DropDownList runat="server" ID="ddlDueDate" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                            CssClass="txtbox">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="vaDueDate" ErrorMessage="Please Select Due Date."
                            ControlToValidate="ddlDueDate" runat="server" ValidationGroup="ActValidationGroup"
                            Display="None" Enabled="false" />
                    </div>
                    <div style="margin-bottom: 29px" id="Div3" runat="server">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 250px; display: block; float: left; font-size: 13px; color: #333;">
                            Is grace period two applicable</label>
                        <asp:CheckBox ID="ChkGracePeriodTwo" runat="server" OnCheckedChanged="ChkGracePeriodTwo_CheckedChanged" AutoPostBack="true" />
                    </div>
                    <div style="margin-bottom: 14px" id="Div2" runat="server">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 250px; display: block; float: left; font-size: 13px; color: #333;">
                            Auto Submit Grace Period One</label>
                        <asp:DropDownList runat="server" ID="ddlGracePeriod" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                            CssClass="txtbox">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Please Select Grace Period."
                            ControlToValidate="ddlGracePeriod" runat="server" ValidationGroup="ActValidationGroup"
                            Display="None" Enabled="false" />
                    </div>
                    <div style="margin-bottom: 7px" id="DivGracePeriodTwo" runat="server">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 250px; display: block; float: left; font-size: 13px; color: #333;">
                            Auto Submit Grace Period Two</label>
                        <asp:DropDownList runat="server" ID="ddlGracePeriodTwo" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                            CssClass="txtbox">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Please Select Grace Period."
                            ControlToValidate="ddlGracePeriodTwo" runat="server" ValidationGroup="ActValidationGroup"
                            Display="None" Enabled="false" />
                    </div>
                    
                    <div id="DivSave" style="margin-bottom: 7px; margin-top: 4%; margin-left: 152px;" runat="server">
                        <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="button" ValidationGroup="ActValidationGroup" />
                        <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="button" OnClick="btnCancel_Click" />
                    </div>
                    <div style="margin-bottom: 7px; margin-left: 10px; margin-top: 10px;">
                        <p style="color: red;"><strong>Note:</strong> (*) Fields Are Compulsary</p>
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnSave" />
            </Triggers>
        </asp:UpdatePanel>
        <asp:HiddenField ID="saveopo" runat="server" Value="false" />
    </div>

    <script type="text/javascript">
        $(function () {
            $('#divActDialog').dialog({
                width: 900,
                autoOpen: false,
                draggable: true,
                title: "Certificate Configration",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });
            if (document.getElementById('BodyContent_saveopo').value == "true") {
                newfun();
                document.getElementById('BodyContent_saveopo').value = "false";
            }
            else {
                $("#divActDialog").dialog('close');
            }
        });

        function newfun() {
            $("#divActDialog").dialog('open');

        }
        function initializeCombobox() {
            $("#<%= ddlCustomer.ClientID %>").combobox();
          <%--  $("#<%= ddlFrequency.ClientID %>").combobox();
            $("#<%= ddlDueDate.ClientID %>").combobox();--%>
        }
     
        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);
        window.onunload = function () { null };

        function initializeDatePicker(todayDate) {

            var startDate = new Date();
            var maxyr = new Date().getFullYear().toString();
            maxyr = parseInt(maxyr) + 100;

            $(".StartDate").datepicker({
  
                dateFormat: 'dd-mm-yy',
                numberOfMonths: 1,
                changeYear: true,
                yearRange: '1900:' + maxyr.toString()
                ,
                beforeShow: function (input, inst) {

                    var date = $("#<%= tbxStartDate.ClientID %>").val();

                    if (date.toString().trim() != '') { 
                        setTimeout(function () {
                            inst.dpDiv.find('a.ui-state-highlight').removeClass('ui-state-highlight');
                        }, 100);
                    }
                     
                    $(".StartDate").datepicker('setDate', date);

                },
                onClose: function (dateText, inst) {

                    if (dateText != null) {
                        $("#<%= tbxStartDate.ClientID %>").val(dateText);
                }
                }
            });

        $("html").on("mouseenter", ".ui-datepicker-trigger", function () {
            $(this).attr('title', 'Select Start Date');
        });


        $('.monthYearPicker').datepicker({
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            dateFormat: 'M yy'
        }).focus(function () {
            var thisCalendar = $(this);
            $('.ui-datepicker-calendar').detach();
            $('.ui-datepicker-close').click(function () {
                var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
                var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
                thisCalendar.datepicker('setDate', new Date(year, month, 1));
            });
        });
    }
    </script>

</asp:Content>
