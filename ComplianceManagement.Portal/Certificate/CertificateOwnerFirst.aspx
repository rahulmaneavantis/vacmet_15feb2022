﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NewCompliance.Master" AutoEventWireup="true" CodeBehind="CertificateOwnerFirst.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Certificate.CertificateOwnerFirst" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
    <link href="../../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jszip.min.js"></script>

    <style type="text/css">
        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            background-color: white;
            border: none;
        }

        .k-textbox .k-icon {
            top: 50%;
            margin: -7px 5px 0px;
            position: absolute;
        }

        .k-active-filter, .k-state-active, .k-state-active:hover {
            background-color: #E9EAEA;
            border-color: #a6a6ad;
            color: #535b6a;
        }

        .div.k-grid-footer, div.k-grid-header {
            border-top-width: 1px;
            margin-right: 0px;
        }

        .k-grid-footer-wrap, .k-grid-header-wrap {
            position: relative;
            width: 100%;
            overflow: hidden;
            border-style: solid;
            border-width: 0 1px 0 0;
            zoom: 1;
        }

        .k-grid-content {
            min-height: 400px !important;
        }

        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 2.0em;
            border-bottom-width: 1px;
            background-color: white;
            border-width: 0 1px 1px 0px;
        }

            .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
                background-color: #1fd9e1;
                background-image: none;
                background-color: white;
            }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: white;
        }

        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 6px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: white;
        }

        html .k-grid tr.k-alt:hover {
            background: white;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
            margin-right: 2px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
            text-align: center;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: white;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 0px 1px 0px;
            text-align: center;
        }

        .k-grid-pager {
            margin-top: -1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-i-arrow-60-down, .k-i-arrow-60-left, .k-i-arrow-60-right, .k-i-arrow-60-up {
            cursor: pointer;
            margin-top: 6px;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            /* border-width: 0px 0px 1px 0px;*/
            background: #E9EAEA;
            font-weight: bold;
            margin-right: 18px;
            font-family: 'Roboto',sans-serif;
            font-size: 15px;
            color: rgba(0, 0, 0, 0.5);
            height: 16px;
            vertical-align: middle;
        }

        .k-dropdown-wrap.k-state-default {
            background-color: white;
        }

        .k-popup.k-calendar-container, .k-popup.k-list-container {
            background-color: white;
        }

        .k-list > .k-state-focused.k-state-selected, .k-listview > .k-state-focused.k-state-selected, .k-state-focused.k-state-selected, td.k-state-focused.k-state-selected {
            -webkit-box-shadow: inset 0 0 3px 1px #14699f;
            box-shadow: none;
        }

        label.k-label {
            font-family: roboto,sans-serif !important;
            color: #515967;
            /* font-stretch: 100%; */
            font-style: normal;
            font-weight: 400;
            min-width: 362px;
            white-space: pre-wrap;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: flex;
            margin-bottom: 0px;
        }

        .k-state-default > .k-select {
            border-color: #ceced2;
            margin-top: 0px;
        }

        .k-grid-norecords {
            width: 100%;
            height: 100%;
            text-align: center;
            margin-top: 2%;
        }

        .k-grouping-header {
            font-style: italic;
        }

        .k-grid-toolbar {
            background: white;
            border: none;
            margin: 5px;
        }

        .k-grid table {
            width: 100.5%;
        }

        .k-grid-header-wrap.k-auto-scrollable {
            width: 100% !important;
        }

        .k-multiselect-wrap .k-input {
            display: inherit !important;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-button.k-button-icon .k-icon:before, .k-grid-filter .k-icon:before, .k-header .k-icon:before {
            text-indent: 0;
            content: "\e006";
        }
    </style>

    <script type="text/javascript">
        $(document).ready(function () {
            setactivemenu('leftmastermenu');
            fhead('Compliance Certificate Owner');
            BindFilters();
            Bindgrid();

        });

        function OnClientModeChange(editor, args) {

            var mode = editor.get_mode();
            switch (mode) {
                case 1:
                    //alert( "We are in Design mode");
                    //do something
                    break;
                case 2:
                    //alert("We are in HTML mode");
                    break;
                case 4:
                    setTimeout(function () {
                        var tool = editor.getToolByName("Print");
                        tool.setState(0);
                    }, 0);
                    //alert( "We are in Preview mode");
                    //do something
                    break;
            }
        }

        function BindFilters() {

            $("#Startdatepicker").kendoDatePicker({
                format: "dd-MMM-yyyy",
                change: FilterAllMain
            });

            $("#Lastdatepicker").kendoDatePicker({
                format: "dd-MMM-yyyy",
                change: FilterAllMain
            });

            $("#dropdownlistPeriod").kendoDropDownTree({
                placeholder: "Period",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "Name",
                dataValueField: "ID",
                change: function () {
                    fCreateStoryBoard('dropdownlistPeriod', 'filtersstoryboard', 'Period')
                    FilterAllMain();
                },
                //dataSource: [
                //    { text: "Jan21", value: "Jan21" },
                //    { text: "High", value: "High" },
                //    { text: "Medium", value: "Medium" },                    
                //]
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<%=Path%>Data/ComplianceCertificateOwnerPeriodList?CustId=<% =CustId%>&UId=<% =UId%>',
                        }

                    },
                }
            });

            $("#dropdownlistSubmissionStatus").kendoDropDownTree({
                placeholder: "Submission Status",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",

                change: function () {
                    fCreateStoryBoard('dropdownlistSubmissionStatus', 'filtersstoryStatus', 'Status')
                    FilterAllMain();
                },
                dataSource: [
                    { text: "Auto Submitted", value: "Auto Submitted" },
                    { text: "Auto Generated", value: "Auto Generated" },
                    { text: "Submitted", value: "Submitted" },
                    { text: "Not Submitted", value: "Not Submitted" },
                ]
            });
        }

        function fCreateStoryBoard(Id, div, filtername) {

            var IdKReset = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('id');
            $('#' + div).html('');
            $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');
            $('#' + div).css('display', 'block');

            if (div == 'filtersstoryboard') {
                $('#' + div).append('Period&nbsp;&nbsp;&nbsp;:');//Dashboard
            }

            if (div == 'filtersstoryStatus') {
                $('#' + div).append('Submission Status&nbsp;&nbsp;&nbsp;:');
            }

            for (var i = 0; i < $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length; i++) {
                var button = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button')[i];
                $(button).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('placeholder', 'Shree');
                var buttontest = $($(button).find('span')[0]).text();
                if (buttontest.length > 10) {
                    buttontest = buttontest.substring(0, 10).concat("...");
                }
                $('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="background-color:#EBEBEB;height: 20px;Color:Gray;border-radius:10px;margin-left:5px;margin-bottom: 4px;"><span unselectable="on" title="' + $($(button).find('span')[0]).text() + '">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory(this)" title="Clear" aria-label="Clear" class="k-select" style="padding-left: 6px;"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory(this)"  class="k-icon k-i-close" title="Clear" aria-label="Clear" style="font-size: 12px;"></span></span></li>');
            }

            if ($($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) {
                $('#' + div).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');

            }

        }

        function ClearAllFilterMain(e) {

            $("#dropdownlistPeriod").data("kendoDropDownTree").value([]);
            $("#dropdownlistSubmissionStatus").data("kendoDropDownTree").value([]);
            $("#Startdatepicker").data("kendoDatePicker").value(null);
            $("#Lastdatepicker").data("kendoDatePicker").value(null);
            $("#grid").data("kendoGrid").dataSource.filter({});
            e.preventDefault();
        }

        function fcloseStory(obj) {

            var DataId = $(obj).attr('data-Id');
            var dataKId = $(obj).attr('data-K-Id');
            var seq = $(obj).attr('data-seq');
            var deepspan = $('#' + DataId + ' > li > span > span.k-i-close')[seq];
            $(deepspan).trigger('click');
            var upperli = $('#' + dataKId);
            $(upperli).remove();

            //for rebind if any pending filter is present (Main Grid)
            fCreateStoryBoard('dropdownlistPeriod', 'filtersstoryboard', 'Period')
            fCreateStoryBoard('dropdownlistSubmissionStatus', 'filtersstoryStatus', 'Status')

        };

        function FilterAllMain() {

            var SubmissionStatusdetails = $("#dropdownlistSubmissionStatus").data("kendoDropDownTree").value();
            var Perioddetails = $("#dropdownlistPeriod").data("kendoDropDownTree").value();

            var finalSelectedfilter = { logic: "and", filters: [] };
            if (SubmissionStatusdetails.length > 0
                || ($("#Startdatepicker").val() != null && $("#Startdatepicker").val() != "")
                || ($("#Lastdatepicker").val() != null && $("#Lastdatepicker").val() != "")
                || Perioddetails.length > 0) {

                if (SubmissionStatusdetails.length > 0) {
                    var LocationFilter = { logic: "or", filters: [] };

                    $.each(SubmissionStatusdetails, function (i, v) {
                        LocationFilter.filters.push({
                            field: "SubmittedFlag", operator: "eq", value: v
                        });
                    });

                    finalSelectedfilter.filters.push(LocationFilter);
                }

                if (Perioddetails.length > 0) {
                    var LocationFilter = { logic: "or", filters: [] };

                    $.each(Perioddetails, function (i, v) {
                        LocationFilter.filters.push({
                            field: "Formonth", operator: "eq", value: v
                        });
                    });

                    finalSelectedfilter.filters.push(LocationFilter);
                }

                if ($("#Startdatepicker").val() != null && $("#Startdatepicker").val() != "") {
                    var DateFilter = { logic: "or", filters: [] };
                    DateFilter.filters.push({
                        field: "startdate", operator: "gte", value: kendo.parseDate($("#Startdatepicker").val(), 'dd-MMM-yyyy')
                    });
                    finalSelectedfilter.filters.push(DateFilter);
                }

                if ($("#Lastdatepicker").val() != null && $("#Lastdatepicker").val() != "") {
                    var DateFilter = { logic: "or", filters: [] };
                    DateFilter.filters.push({
                        field: "startdate", operator: "lte", value: kendo.parseDate($("#Lastdatepicker").val(), 'dd-MMM-yyyy')
                    });
                    finalSelectedfilter.filters.push(DateFilter);
                }

                if (finalSelectedfilter.filters.length > 0) {
                    var dataSource = $("#grid").data("kendoGrid").dataSource;
                    dataSource.filter(finalSelectedfilter);
                }
                else {
                    $("#grid").data("kendoGrid").dataSource.filter({});
                }

            }
            else {
                $("#grid").data("kendoGrid").dataSource.filter({});
            }
        }

        var dataSource = new kendo.data.DataSource({
            transport: {
                read: {
                    url: '<%=Path%>Data/ComplianceCertificateOwnerFirst?CustId=<% =CustId%>&UId=<% =UId%>',
                },
            },
            pageSize: 10,
            schema: {
                model: {
                    fields: {
                        startdate: { type: "date" },
                    }
                }
            },
        });

        function Bindgrid() {
            var grid = $('#grid').data("kendoGrid");
            if (grid != undefined || grid != null)
                $('#grid').empty();


            var grid = $("#grid").kendoGrid({
                dataSource: dataSource,
                excel: {
                    allPages: true,
                },
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                sortable: true,
                filterable: true,
                columnMenu: true,
                pageable: true,
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                pageable: {
                    numeric: true,
                    pageSizes: ['All', 5, 10, 20],
                    pageSize: 10,
                    buttonCount: 3,
                },
                dataBound: OnGridDataBound,
                columns: [
                    {
                        field: "Formonth", title: 'Periods',
                        //headerAttributes: {
                        //    "class": "table-header-cell",
                        //    style: "text-align: left;"
                        //},
                        attributes: {
                            style: 'white-space: nowrap;text-align: center;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "20%"
                    },
                    {
                        field: "SubmittedFlag", title: 'Submission Status',
                        //headerAttributes: {
                        //    "class": "table-header-cell",
                        //    style: "text-align: left;important"
                        //},
                        attributes: {
                            style: 'white-space: nowrap;text-align: center;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "30%"
                    },
                    {
                        field: "startdate", title: 'Due Date',
                        hidden: true,
                        type: "date",
                        format: "{0:dd-MMM-yyyy}",
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    type: "date",
                                    format: "{0:dd-MMM-yyyy}",
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "20%"
                    },
                    {
                        title: "Status as on Certificate Date",
                        attributes: {
                            style: 'white-space: nowrap; text-align: center;'
                        },
                        columns: [
                            {
                                field: "ComplianceStatusClosedTimelyPer", title: 'Completed %', template: '#=ComplianceStatusClosedTimelyPer#' + '<span class="k-icon k-i-percent"></span>',
                                attributes: {
                                    style: 'white-space: nowrap;'

                                }, filterable: {
                                    multi: true,
                                    extra: false,
                                    search: true,
                                    operators: {
                                        string: {
                                            eq: "Is equal to",
                                            neq: "Is not equal to",
                                            contains: "Contains"
                                        }
                                    }
                                }, width: "20%",
                                attributes: {
                                    "class": "table-cell",
                                    style: "text-align: center; font-size: 14px"
                                }
                            },                            
                            {
                                field: "ComplianceStatusOpenPer", title: 'Overdue %', template: '#=ComplianceStatusOpenPer#' + '<span class="k-icon k-i-percent"></span>',
                                attributes: {
                                    style: 'white-space: nowrap;'

                                }, filterable: {
                                    multi: true,
                                    extra: false,
                                    search: true,
                                    operators: {
                                        string: {
                                            eq: "Is equal to",
                                            neq: "Is not equal to",
                                            contains: "Contains"
                                        }
                                    }
                                }, width: "20%",
                                attributes: {
                                    "class": "table-cell",
                                    style: "text-align: center; font-size: 14px"
                                }
                            }]
                    },

                    {
                        title: "Status as on Current Date",
                        attributes: {
                            style: 'white-space: nowrap; text-align: center;'
                        },
                        columns: [
                            {
                                field: "AsOnStatusClosedTimelyPer", title: 'Completed %', template: '#=AsOnStatusClosedTimelyPer#' + '<span class="k-icon k-i-percent"></span>',
                                attributes: {
                                    style: 'white-space: nowrap;'

                                }, filterable: {
                                    multi: true,
                                    extra: false,
                                    search: true,
                                    operators: {
                                        string: {
                                            eq: "Is equal to",
                                            neq: "Is not equal to",
                                            contains: "Contains"
                                        }
                                    }
                                }, width: "20%",
                                attributes: {
                                    "class": "table-cell",
                                    style: "text-align: center; font-size: 14px"
                                }
                            },
                            {
                                field: "AsOnStatusOpenPer", title: 'Overdue %', template: '#=AsOnStatusOpenPer#' + '<span class="k-icon k-i-percent"></span>',
                                attributes: {
                                    style: 'white-space: nowrap;'

                                }, filterable: {
                                    multi: true,
                                    extra: false,
                                    search: true,
                                    operators: {
                                        string: {
                                            eq: "Is equal to",
                                            neq: "Is not equal to",
                                            contains: "Contains"
                                        }
                                    }
                                }, width: "20%",
                                attributes: {
                                    "class": "table-cell",
                                    style: "text-align: center; font-size: 14px"
                                }
                            }]


                    },
                    {
                        command: [
                            { name: "edit2", text: "", iconClass: "k-icon k-i-eye", className: "ob-overviewMain" },
                            { name: "edit1", text: "", iconClass: "k-icon k-i-hyperlink-open", className: "ob-overview" }
                        ], title: "Action", lock: true, width: "20%;", headerAttributes: {
                            style: "border-right: solid 1px ;text-align: center;"
                        }
                    }
                ]

            });

            $(document).on("click", "#grid tbody tr .ob-overviewMain", function (e) {
                var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));

                window.location.href = "../Certificate/CertificateOwnerSecond.aspx?SID=" + item.CerScheduleonID + "&Period=" + item.Formonth + "&Flag=" + item.SubmittedFlag + '&StartDateDetail=&EndDateDetail=';
                return true;
            });

            $(document).on("click", "#grid tbody tr .ob-overview", function (e) {
                var PathName = document.getElementById('Path').value;
                var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
                $.ajax({
                    type: "Post",
                    url: '' + PathName + '//ExportReport/CertificatePreview',
                    data: {
                        //UserId: item.OfficerUserID,
                        //CustomerID: item.CustomerID,
                        UserId: <% =UId%>,
                        CustomerID:<% =CustId%>,
                        SID: item.CerScheduleonID,
                        Period: item.Formonth,
                        Flag: item.SubmittedFlag,
                    },
                    success: function (response) {
                        //if (response.result.length != 0) {
                        $("#<%=datastringNew.ClientID%>").val(encodeURIComponent(response.result[0].Content));
                        DownloadPDf = response.result[0].FilenamePDF;
                        DownloadExcel = response.result[0].FilenameExcel;
                        $("#ContentPlaceHolder1_btnCreateCertificate").click();
                        OpenPopupCCCustomized(this);
                        //}
                        //else {
                        //    alert("Error Occured while previewing");
                        //}
                    }
                });
                return true;
            });

            $("#grid").kendoTooltip({
                filter: ".k-grid-edit1",
                content: function (e) {
                    return "Preview";
                }
            });
            $("#grid").kendoTooltip({
                filter: ".k-grid-edit2",
                content: function (e) {
                    return "View";
                }
            });

            function OnGridDataBound(e) {
                for (var i = 0; i < this.columns.length; i++) {
                    this.autoWidth;
                }
                var grid = $("#grid").data("kendoGrid");
                var gridData = grid.dataSource.view();
                for (var i = 0; i < gridData.length; i++) {
                     
                    if (gridData[i].SubmittedFlag == "Submitted" || gridData[i].SubmittedFlag == "Auto Submitted" || gridData[i].SubmittedFlag == "Auto Generated") {

                    }
                    else {
                        var currentUid = gridData[i].uid;
                        var currentRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                        var previewButton = $(currentRow).find(".ob-overview");
                        previewButton.hide();
                    }
                }
            }
        }

        var DownloadPDf = "";
        var DownloadExcel = "";

        function OpenPopupCCCustomized(e) {
            $("#MoreReports").kendoWindow({
                width: "90%",
                height: "90%",
                title: "Preview",
                visible: false,
                // modal:true,
                refresh: true,
                pinned: true,
                actions: [
                    "Close"
                ]

            }).data("kendoWindow").open().center();
            e.preventDefault();

            var windowWidget = $("#MoreReports").data("kendoWindow");

            kendo.ui.progress(windowWidget.element, true);

            settimeout(function () { kendo.ui.progress(windowWidget.element, false); }, 5000)
        }

        function btnDownloadPDF_click() {
            var PathName = document.getElementById('Path').value;
            window.location.href = '' + PathName + '/ExportReport/GetFile?userpath=' + DownloadPDf + '';
        }
        function btnDownloadExcel_click() {
            var PathName = document.getElementById('Path').value;
            window.location.href = '' + PathName + '/ExportReport/GetFile?userpath=' + DownloadExcel + '';
        }

    </script>
    <script type="text/javascript">

        Telerik.Web.UI.Editor.CommandList["CustomPrint"] = function (commandName, editor, args) {

            var printIframe = document.createElement("IFRAME");
            document.body.appendChild(printIframe);
            var printDocument = printIframe.contentWindow.document;
            printDocument.designMode = "on";
            printDocument.open();
            var currentLocation = document.location.href;
            currentLocation = currentLocation.substring(0, currentLocation.lastIndexOf("/") + 1);
            printDocument.write("<html><head></head><body>" + editor.get_html() + "</body></html>");
            printDocument.close();
            try {
                if (document.all) {
                    var oLink = printDocument.createElement("link");
                    oLink.setAttribute("href", currentLocation + "PrintWithStyles.css", 0);
                    oLink.setAttribute("type", "text/css");
                    oLink.setAttribute("rel", "stylesheet", 0);
                    printDocument.getElementsByTagName("head")[0].appendChild(oLink);
                    printDocument.execCommand("Print");
                }
                else {
                    printDocument.body.innerHTML = "<link rel='stylesheet' type='text/css' href='" + currentLocation + "PrintWithStyles.css'></link>" + printDocument.body.innerHTML;
                    printIframe.contentWindow.print();
                }
            }
            catch (ex) {
            }
            document.body.removeChild(printIframe);
        };
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div>
        <input id="Path" type="hidden" value="<% =Path%>" />
    </div>
    <div class="col-md-12" style="margin: 0.4%; margin-left: -1%;">
        <input id="dropdownlistPeriod" style="width: 20%; margin-right: 5px;" />
        <input id="dropdownlistSubmissionStatus" style="width: 20%; margin-right: 5px;" />
        <input id="Startdatepicker" placeholder="Start Date" style="width: 15%; margin-right: 5px; display:none;" />
        <input id="Lastdatepicker" placeholder="End Date" style="width: 15%; display:none;" />
        <button id="ClearfilterMain" class="k-button" style="margin-right: -2%; float: right;" onclick="ClearAllFilterMain(event)"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear Filter</button>
    </div>

    <div class="row" style="display: none; padding-bottom: 4px; font-size: 12px; font-weight: bold; color: black; margin-left: 4px;" id="filtersstoryboard">&nbsp;</div>
    <div class="row" style="display: none; padding-bottom: 4px; font-size: 12px; font-weight: bold; color: black; margin-left: 4px;" id="filtersstoryStatus">&nbsp;</div>

    <div class="row">
        <div id="grid" style="margin: 4px;"></div>
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                <asp:Button ID="btnCreateCertificate" runat="server" Style="display: none" OnClick="btnCreateCertificate_Click" />
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnCreateCertificate" />
            </Triggers>
        </asp:UpdatePanel>
        <asp:TextBox ID="datastringNew" Style="display: none" runat="server"></asp:TextBox>
    </div>
    <div id="MoreReports" style="display: none">
        <button type="button" id="btnDownloadPDF" class="k-button" style="display: none;" onclick="btnDownloadPDF_click();">Export PDF</button>
        <button type="button" id="btnDownloadExcel" class="k-button" style="float: right; margin-right: 4%; margin-top: 1.3%; display: none;" onclick="btnDownloadExcel_click();">Download</button>
        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
            <ContentTemplate>
                <div class="demo-containers" style="margin-top: 10px; margin-left: 90px;">
                    <div class="demo-container">
                        <telerik:RadEditor RenderMode="Lightweight" runat="server" ID="RadDisplayControl" SkinID="DefaultSetOfTools"
                            Height="535px" Width="100%" Style="margin-left: -41px" OnClientModeChange="OnClientModeChange" EditModes="Preview">
                            <ImageManager ViewPaths="~/Editor/images/UserDir/Marketing,~/Editor/images/UserDir/PublicRelations"
                                UploadPaths="~/Editor/images/UserDir/Marketing,~/Editor/images/UserDir/PublicRelations"
                                DeletePaths="~/Editor/images/UserDir/Marketing,~/Editor/images/UserDir/PublicRelations"
                                EnableAsyncUpload="true"></ImageManager>
                            <Tools>
                                <telerik:EditorToolGroup>
                                    <telerik:EditorTool Name="print" />
                                </telerik:EditorToolGroup>
                            </Tools>
                        </telerik:RadEditor>
                    </div>
                </div>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <telerik:RadAjaxLoadingPanel runat="server" ID="RadAjaxLoadingPanel1">
                        </telerik:RadAjaxLoadingPanel>
                        <telerik:RadAjaxManager runat="server" ID="RadAjaxManager1">
                            <AjaxSettings>
                                <telerik:AjaxSetting AjaxControlID="CheckBoxListEditMode">
                                    <UpdatedControls>
                                        <telerik:AjaxUpdatedControl ControlID="RadEditor1" LoadingPanelID="RadAjaxLoadingPanel1" />
                                        <telerik:AjaxUpdatedControl ControlID="CheckBoxListEditMode" />
                                    </UpdatedControls>
                                </telerik:AjaxSetting>
                                <telerik:AjaxSetting AjaxControlID="RadioButtonToolsFile">
                                    <UpdatedControls>
                                        <telerik:AjaxUpdatedControl ControlID="RadEditor1" LoadingPanelID="RadAjaxLoadingPanel1" />
                                        <telerik:AjaxUpdatedControl ControlID="RadioButtonToolsFile" />
                                    </UpdatedControls>
                                </telerik:AjaxSetting>
                                <telerik:AjaxSetting AjaxControlID="CheckBoxListModules">
                                    <UpdatedControls>
                                        <telerik:AjaxUpdatedControl ControlID="RadEditor1" LoadingPanelID="RadAjaxLoadingPanel1" />
                                        <telerik:AjaxUpdatedControl ControlID="CheckBoxListModules" />
                                    </UpdatedControls>
                                </telerik:AjaxSetting>
                                <telerik:AjaxSetting AjaxControlID="RadioButtonListEnabled">
                                    <UpdatedControls>
                                        <telerik:AjaxUpdatedControl ControlID="RadEditor1" LoadingPanelID="RadAjaxLoadingPanel1" />
                                        <telerik:AjaxUpdatedControl ControlID="RadioButtonListEnabled" />
                                    </UpdatedControls>
                                </telerik:AjaxSetting>
                                <telerik:AjaxSetting AjaxControlID="NewLineModeButtonList">
                                    <UpdatedControls>
                                        <telerik:AjaxUpdatedControl ControlID="RadEditor1" LoadingPanelID="RadAjaxLoadingPanel1" />
                                        <telerik:AjaxUpdatedControl ControlID="NewLineModeButtonList" />
                                    </UpdatedControls>
                                </telerik:AjaxSetting>
                                <telerik:AjaxSetting AjaxControlID="CheckBoxListModules">
                                    <UpdatedControls>
                                        <telerik:AjaxUpdatedControl ControlID="RadEditor1" LoadingPanelID="RadAjaxLoadingPanel1" />
                                        <telerik:AjaxUpdatedControl ControlID="CheckBoxListModules" />
                                    </UpdatedControls>
                                </telerik:AjaxSetting>
                            </AjaxSettings>
                        </telerik:RadAjaxManager>
                    </ContentTemplate>
                </asp:UpdatePanel>

            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
