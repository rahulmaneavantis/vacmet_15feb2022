﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.CertificateData;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Logger;
using System.Reflection;
using System.Linq;
using System.Web.Security;
using System.Web;
using System.Configuration;
using System.IO;
using System.Globalization;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Certificate
{
    public partial class CertificateFrequencyMapping : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindCustomersList();
                BindFrequency();
                BindGrdCertificateFrequncy();
                BindDueDates();
                BindGracePeriod();
                BindGracePeriodTwo();
            }
        }

        private void BindFrequency()
        {
            try
            {
                var FrequencyDetails = ComplianceCertificate.GetCertificateFrequency();
                 
                ddlFrequency.DataTextField = "Frequency";
                ddlFrequency.DataValueField = "ID";
                ddlFrequency.DataSource = FrequencyDetails;
                ddlFrequency.DataBind();
                ddlFrequency.Items.Insert(0, new ListItem("< Select Frequency>", "-1"));
                ddlFrequency.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindDueDates()
        {
            try
            {
                for (int count = 1; count < 32; count++)
                {
                    ddlDueDate.Items.Add(new ListItem() { Text = count.ToString(), Value = count.ToString() });
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindGracePeriod()
        {
            try
            {
                for (int count = 0; count < 32; count++)
                {
                    ddlGracePeriod.Items.Add(new ListItem() { Text = count.ToString(), Value = count.ToString() });
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindGracePeriodTwo()
        {
            try
            {
                for (int count = 0; count < 32; count++)
                {
                    ddlGracePeriodTwo.Items.Add(new ListItem() { Text = count.ToString(), Value = count.ToString() });
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindCustomersList()
        {
            try
            {
                int userID = Convert.ToInt32(AuthenticationHelper.UserID);
                var details = Assigncustomer.GetAllCustomer(userID);
                ddlCustomer.DataTextField = "Name";
                ddlCustomer.DataValueField = "ID";
                ddlCustomer.DataSource = details;
                ddlCustomer.DataBind();
                ddlCustomer.Items.Insert(0, new ListItem("< Select Customer >", "-1"));
                ddlCustomer.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
  
       

        protected void btnAddAct_Click(object sender, EventArgs e)
        {
            try
            {
                ddlCustomer.Enabled = true;
                ddlDueDate.Enabled = true;
                ddlFrequency.Enabled = true;
                tbxStartDate.Enabled = true;
                ViewState["Mode"] = 0;
                btnSave.Enabled = true;
                tbxStartDate.Text = string.Empty;
                saveopo.Value = "false";
                ddlCustomer.SelectedValue = "-1";
                ddlFrequency.SelectedValue = "-1";
                ChkGracePeriodTwo.Checked = false; 
                ChkGracePeriodTwo_CheckedChanged(sender, e);
                upAct.Update();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "initializeCombobox(); $(\"#divActDialog\").dialog('open')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                grdCertificateFrequency.PageIndex = 0;
                BindGrdCertificateFrequncy();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
         private void BindGrdCertificateFrequncy()
        {
            
            try
            {
                int UserID = AuthenticationHelper.UserID;
                var Customerlist = ComplianceCertificate.GetFrequencyMappedCustomerDeatil(UserID);
                if (!string.IsNullOrEmpty(tbxFilter.Text))
                {
                    if (CheckInt(tbxFilter.Text))
                    {
                        int a = Convert.ToInt32(tbxFilter.Text.ToUpper());
                        Customerlist = Customerlist.Where(entry => entry.ID == a).ToList();
                    }
                    else
                    {
                        Customerlist = Customerlist.Where(entry => entry.CertificateFrequency.ToUpper().Contains(tbxFilter.Text.ToUpper()) || entry.CustomerName.ToUpper().Contains(tbxFilter.Text.ToUpper())).ToList();
                    }
                }
                grdCertificateFrequency.DataSource = Customerlist;
                grdCertificateFrequency.DataBind();
                upActList.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
private bool CheckInt(string val)
{
    try
    {
        int i = Convert.ToInt32(val);
        return true;
    }
    catch
    {
        return false;
    }
}
protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int CustomerID = -1;
                int FrequencyID = -1;

                if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                {
                    if (ddlCustomer.SelectedValue != "-1")
                    {
                        CustomerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                    }
                }

                if (!string.IsNullOrEmpty(ddlFrequency.SelectedValue))
                {
                    if (ddlFrequency.SelectedValue != "-1")
                    {
                        FrequencyID = Convert.ToInt32(ddlFrequency.SelectedValue);
                    }
                }

                int gracePeriodTwo=0;
                if(ChkGracePeriodTwo.Checked== true)
                {
                    gracePeriodTwo = Convert.ToInt32(ddlGracePeriodTwo.SelectedValue);
                }

                Certificate_CustomerFrequencyMapping cer = new Certificate_CustomerFrequencyMapping()
                {
                    CustomerID = Convert.ToInt32(ddlCustomer.SelectedValue),
                    Cer_FrequencyID = Convert.ToInt32(ddlFrequency.SelectedValue),
                    DueDate = Convert.ToInt32(ddlDueDate.SelectedValue),
                    IsSecondGraceApplicable= ChkGracePeriodTwo.Checked,
                    GracePeriod= Convert.ToInt32(ddlGracePeriod.SelectedValue),
                    GracePeriodTwo = gracePeriodTwo,
                    CreatedBy = AuthenticationHelper.UserID,
                    CreatedOn = DateTime.Now,
                    IsFinancialYear = Convert.ToInt32(rbFinancial.SelectedValue),
                };

                if (!string.IsNullOrEmpty(tbxStartDate.Text))
                    cer.StartDate = DateTime.ParseExact(Convert.ToString(tbxStartDate.Text).Trim(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
 

                if ((int)ViewState["Mode"] == 1)
                {
                    cer.ID = Convert.ToInt32(ViewState["CerID"]);
                }

                bool isExists = ComplianceCertificate.Exists(cer);

                if ((int)ViewState["Mode"] == 0)
                {
                    if (isExists)
                    {
                        saveopo.Value = "true";
                        cvDuplicateEntry.ErrorMessage = "Certificate Freqancy already exists for Customer.";
                        cvDuplicateEntry.IsValid = false;
                    }
                    else
                    {
                        #region Add                 
                        int _objCerID = ComplianceCertificate.Create(cer);

                        if (isExists == false && _objCerID > 0)
                        {
                            ComplianceCertificate.CreateCertificateScheduleOn(cer);
                            saveopo.Value = "true";
                            cvDuplicateEntry.ErrorMessage = "Record Saved Sucessfully.";
                            cvDuplicateEntry.IsValid = false;
                        }
                        #endregion
                    }
                }
                else if ((int)ViewState["Mode"] == 1)
                {
                    #region Edit
                    saveopo.Value = "true";
                    int CerID = ComplianceCertificate.Update(cer);
                    cvDuplicateEntry.ErrorMessage = "Record Updated Sucessfully.";
                    cvDuplicateEntry.IsValid = false;

                    #endregion
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divActDialog\").dialog('close')", true);
            BindGrdCertificateFrequncy();
        }
 
        protected void upAct_Load(object sender, EventArgs e)
        {
            try
            {
                DateTime date = DateTime.Now;
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
   
        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }
 
         protected void btnRefreshnew_Click(object sender, EventArgs e)
        {
            saveopo.Value = "true";
        }

        protected void ddlFrequency_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                divFinancial.Visible = false;
                if (ddlFrequency.SelectedValue == "3" || ddlFrequency.SelectedValue == "4" || ddlFrequency.SelectedValue == "5" )
                {
                    divFinancial.Visible = true;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdCertificateFrequency_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("EDIT"))
                { 
                    int actID = Convert.ToInt32(e.CommandArgument);
                    ViewState["Mode"] = 1;
                    ViewState["CerID"] = actID;
                 
                    Certificate_CustomerFrequencyMapping act = ComplianceCertificate.GetCustomerFrequencyMappingID(actID);
 

                    ddlCustomer.SelectedValue = act.CustomerID.ToString();
                    ddlDueDate.SelectedValue = act.DueDate.ToString();
                    ddlGracePeriod.SelectedValue = act.GracePeriod.ToString();
                    ddlFrequency.SelectedValue = act.Cer_FrequencyID.ToString();


                    if (act.IsSecondGraceApplicable == true)
                    {
                        ChkGracePeriodTwo.Checked = true;
                        DivGracePeriodTwo.Visible = true;
                        ddlGracePeriodTwo.SelectedValue = act.GracePeriodTwo.ToString();
                    }

                    ChkGracePeriodTwo_CheckedChanged(sender,e);

                    if (act.StartDate != null)
                    {
                        tbxStartDate.Text = Convert.ToDateTime(act.StartDate).ToString("dd-MM-yyyy");
                    }

                    ddlCustomer.Enabled = false;
                    ddlDueDate.Enabled = false;
                    ddlFrequency.Enabled = false;
                    tbxStartDate.Enabled = false;

                    if (ddlFrequency.SelectedValue == "3" || ddlFrequency.SelectedValue == "4" || ddlFrequency.SelectedValue == "5")
                    {
                        divFinancial.Visible = true;
                        rbFinancial.SelectedValue = Convert.ToString(act.IsFinancialYear);
                    }
                   
                    upAct.Update();
                    DateTime date = DateTime.Now;
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "initializeCombobox(); $(\"#divActDialog\").dialog('open')", true);
                }
                else if (e.CommandName.Equals("DELETE"))
                {
                    int actID = Convert.ToInt32(e.CommandArgument);
                    bool isComplianceExists = ComplianceCertificate.ExistsTransaction(actID);
                    if (!isComplianceExists)
                    {
                        int UserID = AuthenticationHelper.UserID;
                        ComplianceCertificate.DeleteFrequencyConfigration(actID, UserID);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Unable to delete certificate configration, since configration are exists for the transaction');", true);
                    }                     
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdCertificateFrequency_RowEditing(object sender, GridViewEditEventArgs e)
        {

        }

        protected void ChkGracePeriodTwo_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                DivGracePeriodTwo.Visible = false;
                if (ChkGracePeriodTwo.Checked == true)
                {
                    DivGracePeriodTwo.Visible = true;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
    }
}