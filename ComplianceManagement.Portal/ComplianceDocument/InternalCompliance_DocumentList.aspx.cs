﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using Ionic.Zip;
using System.IO;
using System.Collections;
using System.Globalization;
using System.Configuration;

namespace com.VirtuosoITech.ComplianceManagement.Portal.ComplianceDocument
{
    public partial class InternalCompliance_DocumentList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindComplianceDocument(DateTime.Now, DateTime.Now);
                BindTypes();
                BindCategories();
                //BindActList();
                BindCompliances();
            }

        }
        protected void upDownloadList_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
                DateTime date = DateTime.MinValue;
                if (DateTime.TryParseExact(txtStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date) || DateTime.TryParseExact(txtEndDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", "initializeDatePicker(null);", true);
                }

                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeActList", string.Format("initializeJQueryUI('{0}', 'dvActList');", txtactList.ClientID), true);
                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideActList", "$(\"#dvActList\").hide(\"blind\", null, 5, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindComplianceDocument(DateTime startdate, DateTime enddate, int pageIndex = 0)
        {
            try
            {
                
                List<InternalComplianceDocumentsView> DocumentList = new List<InternalComplianceDocumentsView>();
                CmpInstancesSaveCheckedValues();
                List<long> complianceIds = new List<long>();
                int userID = AuthenticationHelper.UserID;
                int? customerID = -1;
                if (userID > 0 && AuthenticationHelper.Role != "SADMN")
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID;
                }                
                DocumentList = DocumentManagement.GetAllInternalCompliance(customerID, AuthenticationHelper.Role, AuthenticationHelper.UserID);
                List<ComplianceAssignment> roles = new List<ComplianceAssignment>();

                if (ViewState["checkedCompliancesInstances"] != null)
                {
                    foreach (var gvrow in (ArrayList)ViewState["checkedCompliancesInstances"])
                    {
                        complianceIds.Add(Convert.ToInt32(gvrow));
                    }                    
                    DocumentList = DocumentList.Where(entry => complianceIds.Contains((long)entry.InternalComplianceID)).ToList();
                }

                if (startdate.Date != DateTime.Now.Date && enddate.Date != DateTime.Now.Date)
                {
                    DocumentList = DocumentList.Where(entry => entry.InternalScheduledOn >= startdate && entry.InternalScheduledOn <= enddate).ToList();
                }              
                grdIComplianceDocument.PageIndex = pageIndex;
                grdIComplianceDocument.DataSource = DocumentList;
                grdIComplianceDocument.DataBind();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divDownloadList\").dialog('close');", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdIComplianceDocument_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Download")
                {

                    string[] commandArg = e.CommandArgument.ToString().Split(',');
                    //List<GetComplianceDocumentsView> CMPDocuments = Business.ComplianceManagement.GetDocumnets(Convert.ToInt64(commandArg[0]), Convert.ToInt64(commandArg[1]));
                    List<GetInternalComplianceDocumentsView> CMPDocuments = Business.InternalComplianceManagement.GetInternalDocumnets(Convert.ToInt64(commandArg[0]), Convert.ToInt64(commandArg[1]));
                    Session["ScheduleOnID"] = commandArg[0];
                    Session["TransactionID"] = commandArg[1];

                    if (CMPDocuments != null)
                    {
                        //List<GetComplianceDocumentsView> entitiesData = CMPDocuments.Where(entry => entry.Version != null).GroupBy(entry => entry.Version).Select(en => en.FirstOrDefault()).ToList();
                        List<GetInternalComplianceDocumentsView> entitiesData = CMPDocuments.Where(entry => entry.Version != null).GroupBy(entry => entry.Version).Select(en => en.FirstOrDefault()).ToList();
                        if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                        {
                            GetInternalComplianceDocumentsView entityData = new GetInternalComplianceDocumentsView();
                            entityData.Version = "1.0";
                            entityData.InternalComplianceScheduledOnID = Convert.ToInt64(commandArg[0]);
                            entitiesData.Add(entityData);
                        }

                        rptIComplianceVersion.DataSource = entitiesData.OrderBy(entry => entry.Version);
                        rptIComplianceVersion.DataBind();

                        rptIComplianceDocumnets.DataSource = null;
                        rptIComplianceDocumnets.DataBind();

                        rptWorkingFiles.DataSource = null;
                        rptWorkingFiles.DataBind();

                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "OpendivDownloadDocument", "$(\"#divDownloadDocument\").dialog('open');", true);
                    }

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
               
            }
        }

        protected void grdIComplianceDocument_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    //LinkButton lblDownLoadfile = (LinkButton)e.Row.FindControl("lblDownLoadfile");
                    //var scriptManager = ScriptManager.GetCurrent(this.Page);
                    //if (scriptManager != null)
                    //{
                    //    scriptManager.RegisterPostBackControl(lblDownLoadfile);                        
                    //}

                    CheckBox headerchk = (CheckBox)grdIComplianceDocument.HeaderRow.FindControl("chkICompliancesHeader");
                    CheckBox childchk = (CheckBox)e.Row.FindControl("chkICompliances");
                    childchk.Attributes.Add("onclick", "javascript:Selectchildcheckboxes('" + headerchk.ClientID + "','grdIComplianceDocument')");
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnSetFilter_Click(object sender, EventArgs e)
        {
            try
            {
                ddlIComplinceCatagory.SelectedIndex = -1;
                ddlFilterIComplianceType.SelectedIndex = -1;
                ViewState["checkedCompliancesInstances"] = null;
                BindCompliances();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divDownloadList\").dialog('open')", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        // Zip all files from folder
        protected void btnDownload_Click(object sender, EventArgs e)
        {
            try
            {

                using (ZipFile ComplianceZip = new ZipFile())
                {
                    CmpSaveCheckedValues();

                    if (ViewState["checkedCompliances"] != null)
                    {
                        foreach (var gvrow in (ArrayList)ViewState["checkedCompliances"])
                        {

                            int ScheduledOnID = Convert.ToInt32(gvrow);
                            if (ScheduledOnID != 0)
                            {
                                //var ComplianceData = DocumentManagement.GetForMonth(ScheduledOnID);InternalComplianceGetForMonth
                                var ComplianceData = DocumentManagement.InternalComplianceGetForMonth(ScheduledOnID);
                                //List<GetComplianceDocumentsView> fileData = DocumentManagement.GetFileData1(ScheduledOnID);
                                List<GetInternalComplianceDocumentsView> fileData = DocumentManagement.GetFileDataInternal(ScheduledOnID);
                                //craeted subdirectory
                                //string directoryName = fileData[0].ID + "_" + fileData[0].Branch + "_" + fileData[0].ScheduledOn.Value.ToString("dd-MM-yyyy");
                                string directoryName = ComplianceData.ShortDescription + "/" + ComplianceData.ForMonth;
                                int i = 0;
                                foreach (var file in fileData)
                                {
                                    string version = string.IsNullOrEmpty(file.Version) ? "1.0" : file.Version;
                                    var dictionary = ComplianceZip.EntryFileNames.Where(x => x.Contains(directoryName + "/" + version)).FirstOrDefault();
                                    if (dictionary == null)
                                        ComplianceZip.AddDirectoryByName(directoryName + "/" + version);
                                    string filePath = string.Empty;
                                    //Change by rahul on 21 JAN 2017 for Azure Drive or Local Drive
                                    if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                    {
                                        filePath = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.FileName));
                                    }
                                    else
                                    {
                                        filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                    }
                                        
                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        string ext = Path.GetExtension(file.FileName);
                                        string[] filename = file.FileName.Split('.');
                                        //string str = filename[0] + i + "." + filename[1];
                                        string str = filename[0] + i + "." + ext;
                                        if (file.EnType == "M")
                                        {
                                            ComplianceZip.AddEntry(directoryName + "/" + version + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        else
                                        {
                                            ComplianceZip.AddEntry(directoryName + "/" + version + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        i++;
                                    }
                                }
                            }
                        }

                        var zipMs = new MemoryStream();
                        ComplianceZip.Save(zipMs);
                        zipMs.Position = zipMs.Length;


                        //byte[] data = zipMs.ToArray();
                        //Response.BinaryWrite(data);
                        //Response.BinaryWrite(@"C:\Users\devlp1\AppData\Local\Temp\document.txt", File.WriteAllBytes(data));

                        byte[] data = zipMs.ToArray();
                        //File.WriteAllBytes(@"C:\Users\devlp1\AppData\Local\Temp\Compliance\document.zip", data);
                        //Response.BinaryWrite(data);
                        Response.Buffer = true;
                        Response.ClearContent();
                        Response.ClearHeaders();
                        Response.Clear();
                        Response.ContentType = "application/zip";
                        Response.AddHeader("content-disposition", "attachment; filename=InternalComplianceDocument.zip");
                        Response.BinaryWrite(data);
                        Response.Flush();
                        Response.End();

                    }
                }
                //BindComplianceDocument(DateTime.Now,DateTime.Now);
            }
            catch (Exception)
            {
                throw;
                //LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }

        protected void grdIComplianceDocument_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            CmpSaveCheckedValues();
            BindComplianceDocument(DateTime.Now, DateTime.Now, pageIndex: e.NewPageIndex);
            CmpPopulateCheckedValues();
        }

        private void CmpPopulateCheckedValues()
        {
            ArrayList complianceDetails = (ArrayList)ViewState["checkedCompliances"];

            if (complianceDetails != null && complianceDetails.Count > 0)
            {
                foreach (GridViewRow gvrow in grdIComplianceDocument.Rows)
                {
                    LinkButton lblDownLoadfile = (LinkButton)gvrow.FindControl("lblDownLoadfile");
                    int index = Convert.ToInt32(lblDownLoadfile.CommandArgument.ToString().Split(',')[0]);
                    if (complianceDetails.Contains(index))
                    {
                        CheckBox myCheckBox = (CheckBox)gvrow.FindControl("chkICompliances");
                        myCheckBox.Checked = true;
                    }
                }
            }
        }

        private void CmpSaveCheckedValues()
        {
            ArrayList userdetails = new ArrayList();
            int index = -1;
            foreach (GridViewRow gvrow in grdIComplianceDocument.Rows)
            {
                LinkButton lblDownLoadfile = (LinkButton)gvrow.FindControl("lblDownLoadfile");
                index = Convert.ToInt32(lblDownLoadfile.CommandArgument.ToString().Split(',')[0]);
                bool result = ((CheckBox)gvrow.FindControl("chkICompliances")).Checked;

                // Check in the Session
                if (ViewState["checkedCompliances"] != null)
                    userdetails = (ArrayList)ViewState["checkedCompliances"];
                if (result)
                {
                    if (!userdetails.Contains(index))
                        userdetails.Add(index);
                }
                else
                    userdetails.Remove(index);
            }
            if (userdetails != null && userdetails.Count > 0)
                ViewState["checkedCompliances"] = userdetails;
        }

        private void CmpInstancesPopulateCheckedValues()
        {
            ArrayList complianceDetails = (ArrayList)ViewState["checkedCompliancesInstances"];

            if (complianceDetails != null && complianceDetails.Count > 0)
            {
                foreach (GridViewRow gvrow in grdIComplianceInstances.Rows)
                {

                    int index = Convert.ToInt32(grdIComplianceInstances.DataKeys[gvrow.RowIndex].Value);
                    if (complianceDetails.Contains(index))
                    {
                        CheckBox myCheckBox = (CheckBox)gvrow.FindControl("chkFilterICompliances");
                        myCheckBox.Checked = true;
                    }
                }
            }
        }

        private void CmpInstancesSaveCheckedValues()
        {
            ArrayList userdetails = new ArrayList();
            int index = -1;
            foreach (GridViewRow gvrow in grdIComplianceInstances.Rows)
            {

                index = Convert.ToInt32(grdIComplianceInstances.DataKeys[gvrow.RowIndex].Value);
                bool result = ((CheckBox)gvrow.FindControl("chkFilterICompliances")).Checked;

                // Check in the Session
                if (ViewState["checkedCompliancesInstances"] != null)
                    userdetails = (ArrayList)ViewState["checkedCompliancesInstances"];
                if (result)
                {
                    if (!userdetails.Contains(index))
                        userdetails.Add(index);
                }
                else
                    userdetails.Remove(index);
            }
            if (userdetails != null && userdetails.Count > 0)
                ViewState["checkedCompliancesInstances"] = userdetails;
        }

        /// <summary>
        /// this function is bind catagory of fiter catagory drop down.
        /// </summary>
        private void BindTypes()
        {
            try
            {
                ddlFilterIComplianceType.DataTextField = "Name";
                ddlFilterIComplianceType.DataValueField = "ID";

                //ddlFilterIComplianceType.DataSource = ComplianceTypeManagement.GetAll();
                ddlFilterIComplianceType.DataSource = ComplianceTypeManagement.GetAllInternalCompliancesWC();
                ddlFilterIComplianceType.DataBind();

                ddlFilterIComplianceType.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        /// <summary>
        /// this function is bind catagory for filter drodown.
        /// </summary>
        private void BindCategories()
        {
            try
            {
                ddlIComplinceCatagory.DataTextField = "Name";
                ddlIComplinceCatagory.DataValueField = "ID";
                int customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);

                //ddlIComplinceCatagory.DataSource = ComplianceCategoryManagement.GetAll();
                ddlIComplinceCatagory.DataSource = ComplianceCategoryManagement.GetAllInternalCompliancesCategories(customerid);
                ddlIComplinceCatagory.DataBind();

                ddlIComplinceCatagory.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        /// <summary>
        /// this function is bind all acts.
        /// </summary>
        //private void BindActList()
        //{
        //    try
        //    {
        //        int complianceTypeID = Convert.ToInt32(ddlFilterIComplianceType.SelectedValue);
        //        int complianceCatagoryID = Convert.ToInt32(ddlComplinceCatagory.SelectedValue);

        //        List<ActView> ActList = ActManagement.GetAll(complianceCatagoryID, complianceTypeID);
        //        rptActList.DataSource = ActList;
        //        rptActList.DataBind();

        //        if (complianceCatagoryID != -1 || complianceTypeID != -1)
        //        {

        //            foreach (RepeaterItem aItem in rptActList.Items)
        //            {
        //                CheckBox chkAct = (CheckBox)aItem.FindControl("chkAct");

        //                if (!chkAct.Checked)
        //                {
        //                    chkAct.Checked = true;
        //                }
        //            }
        //            CheckBox actSelectAll = (CheckBox)rptActList.Controls[0].Controls[0].FindControl("actSelectAll");
        //            actSelectAll.Checked = true;
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //}

        private void BindCompliances(int pageIndex = 0)
        {
            try
            {

                int complianceCatagoryID = Convert.ToInt32(ddlIComplinceCatagory.SelectedValue);
                int complianceTypeID = Convert.ToInt32(ddlFilterIComplianceType.SelectedValue);

                //List<int> actIds = new List<int>();
                //foreach (RepeaterItem aItem in rptActList.Items)
                //{
                //    CheckBox chkAct = (CheckBox)aItem.FindControl("chkAct");
                //    if (chkAct.Checked)
                //    {
                //        actIds.Add(Convert.ToInt32(((Label)aItem.FindControl("lblActID")).Text.Trim()));
                //    }
                //}

                //if (complianceTypeID != -1 || complianceCatagoryID != -1 || actIds.Count != 0)
                //{
                grdIComplianceInstances.PageIndex = pageIndex;
                //grdIComplianceInstances.DataSource = Business.ComplianceManagement.GetByType(complianceTypeID, complianceCatagoryID, false, -1, actIds);
                grdIComplianceInstances.DataSource = Business.InternalComplianceManagement.GetByType(complianceTypeID, complianceCatagoryID, false, -1);
                grdIComplianceInstances.DataBind();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", "initializeDatePicker(null);", true);
                //}

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdIComplianceInstances_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            CmpInstancesSaveCheckedValues();
            BindCompliances(pageIndex: e.NewPageIndex);
            CmpInstancesPopulateCheckedValues();
        }

        protected void grdIComplianceInstances_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    CheckBox headerchk = (CheckBox)grdIComplianceInstances.HeaderRow.FindControl("chkFilterICompliancesHeader");
                    CheckBox childchk = (CheckBox)e.Row.FindControl("chkFilterICompliances");
                    childchk.Attributes.Add("onclick", "javascript:Selectchildcheckboxes('" + headerchk.ClientID + "','grdIComplianceInstances')");
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlFilterIComplianceType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //BindActList();
                BindCompliances();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlIComplinceCatagory_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //BindActList();
                BindCompliances();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            BindCompliances();
        }

        protected void btnApply_Click(object sender, EventArgs e)
        {
            try
            {
                string strtdate = Request[txtStartDate.UniqueID].ToString().Trim();
                string enddate = Request[txtEndDate.UniqueID].ToString().Trim();

                if (strtdate != "" && enddate != "")
                {
                    DateTime startdate1 = DateTime.ParseExact(strtdate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    DateTime enddate1 = DateTime.ParseExact(enddate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    BindComplianceDocument(startdate1, enddate1);
                }
                else
                {
                    BindComplianceDocument(DateTime.Now, DateTime.Now);
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        //Download module popup
        protected void rptIComplianceVersion_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                //List<GetComplianceDocumentsView> ComplianceFileData = new List<GetComplianceDocumentsView>();GetInternalComplianceDocumentsView
                //List<GetComplianceDocumentsView> ComplianceDocument = new List<GetComplianceDocumentsView>();
                List<GetInternalComplianceDocumentsView> ComplianceFileData = new List<GetInternalComplianceDocumentsView>();
                List<GetInternalComplianceDocumentsView> ComplianceDocument = new List<GetInternalComplianceDocumentsView>();
                //ComplianceDocument = DocumentManagement.GetFileData1(Convert.ToInt32(commandArgs[0])).ToList();
                ComplianceDocument = DocumentManagement.GetFileDataInternal(Convert.ToInt32(commandArgs[0])).ToList();

                if (commandArgs[1].Equals("1.0"))
                {
                    ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == commandArgs[1]).ToList();
                    if (ComplianceFileData.Count <= 0)
                    {
                        ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == null).ToList();
                    }
                }
                else
                {
                    ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == commandArgs[1]).ToList();
                }

                if (e.CommandName.Equals("version"))
                {
                    if (e.CommandName.Equals("version"))
                    {
                        rptIComplianceDocumnets.DataSource = ComplianceFileData.Where(entry => entry.FileType == 1).ToList();
                        rptIComplianceDocumnets.DataBind();

                        rptWorkingFiles.DataSource = ComplianceFileData.Where(entry => entry.FileType == 2).ToList();
                        rptWorkingFiles.DataBind();
                        //List<GetComplianceDocumentsView> CMPDocuments = Business.ComplianceManagement.GetDocumnets(Convert.ToInt64(Session["ScheduleOnID"]), Convert.ToInt64(Session["TransactionID"]));
                        List<GetInternalComplianceDocumentsView> CMPDocuments = Business.InternalComplianceManagement.GetInternalDocumnets(Convert.ToInt64(Session["ScheduleOnID"]), Convert.ToInt64(Session["TransactionID"]));
                        if (CMPDocuments != null)
                        {
                            //List<GetComplianceDocumentsView> entitiesData = CMPDocuments.Where(entry => entry.Version != null).GroupBy(entry => entry.Version).Select(en => en.FirstOrDefault()).ToList();
                            List<GetInternalComplianceDocumentsView> entitiesData = CMPDocuments.Where(entry => entry.Version != null).GroupBy(entry => entry.Version).Select(en => en.FirstOrDefault()).ToList();
                            if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                            {
                                GetInternalComplianceDocumentsView entityData = new GetInternalComplianceDocumentsView();
                                entityData.Version = "1.0";
                                entityData.InternalComplianceScheduledOnID = Convert.ToInt64(Session["ScheduleOnID"]);
                                entitiesData.Add(entityData);
                            }
                            rptIComplianceVersion.DataSource = entitiesData.OrderBy(entry => entry.Version);
                            rptIComplianceVersion.DataBind();
                        }

                        UpdatePanel1.Update();
                    }
                }
                else if (e.CommandName.Equals("Download"))
                {
                    using (ZipFile ComplianceZip = new ZipFile())
                    {

                        //var ComplianceData = DocumentManagement.GetForMonth(Convert.ToInt32(commandArgs[0]));
                        var ComplianceData = DocumentManagement.InternalComplianceGetForMonth(Convert.ToInt32(commandArgs[0]));
                        ComplianceZip.AddDirectoryByName(ComplianceData.ForMonth + "/" + commandArgs[1]);

                        if (ComplianceFileData.Count > 0)
                        {
                            int i = 0;
                            foreach (var file in ComplianceFileData)
                            {
                                string filePath = string.Empty;
                                //Change by rahul on 21 JAN 2017 for Azure Drive or Local Drive
                                if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                {
                                    filePath = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.FileName));
                                }
                                else
                                {
                                    filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                }
                                if (file.FilePath != null && File.Exists(filePath))
                                {
                                    string ext = Path.GetExtension(file.FileName);
                                    string[] filename = file.FileName.Split('.');
                                    //string str = filename[0] + i + "." + filename[1];
                                    string str = filename[0] + i + "." + ext;
                                    if (file.EnType == "M")
                                    {
                                        ComplianceZip.AddEntry(ComplianceData.ForMonth + "/" + commandArgs[1] + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    else
                                    {
                                        ComplianceZip.AddEntry(ComplianceData.ForMonth + "/" + commandArgs[1] + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    i++;
                                }
                            }
                        }

                        var zipMs = new MemoryStream();
                        ComplianceZip.Save(zipMs);
                        zipMs.Position = 0;
                        byte[] data = zipMs.ToArray();

                        Response.Buffer = true;

                        Response.ClearContent();
                        Response.ClearHeaders();
                        Response.Clear();
                        Response.ContentType = "application/zip";
                        Response.AddHeader("content-disposition", "attachment; filename=InternalComplianceDocument_" + commandArgs[1] + ".zip");
                        Response.BinaryWrite(data);
                        Response.Flush();
                        Response.End();
                    }
                }


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rptIComplianceVersion_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnComplinceVersionDoc");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);

                LinkButton lblDocumentVersion = (LinkButton)e.Item.FindControl("lblDocumentVersion");
                scriptManager.RegisterAsyncPostBackControl(lblDocumentVersion);

            }
        }

        protected void rptIComplianceDocumnets_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {

                DownloadFile(Convert.ToInt32(e.CommandArgument));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rptIComplianceDocumnets_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnIComplianceDocumnets");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);
            }
        }

        protected void rptWorkingFiles_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            DownloadFile(Convert.ToInt32(e.CommandArgument));
        }

        protected void rptWorkingFiles_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnWorkingFiles");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);
            }
        }

        public void DownloadFile(int fileId)
        {
            try
            {
                var file = Business.ComplianceManagement.GetFile(fileId);

                if (file.FilePath != null)
                {                    
                    string filePath = string.Empty;
                    //Change by rahul on 21 JAN 2017 for Azure Drive or Local Drive
                    if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                    {
                        filePath = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.Name));
                    }
                    else
                    {
                        filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
                    }

                    if (filePath != null && File.Exists(filePath))
                    {
                        Response.Buffer = true;
                        Response.Clear();
                        Response.ClearContent();
                        Response.ContentType = "application/octet-stream";
                        Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                        if (file.EnType == "M")
                        {
                            Response.BinaryWrite(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                        }
                        else {
                            Response.BinaryWrite(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                        }
                        Response.Flush(); // send it to the client to download
                        Response.End();

                    }
                }


            }
            catch (Exception)
            {
                //LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                throw;
            }
        }
       
    }
}
    
