﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NewCompliance.Master" AutoEventWireup="true" CodeBehind="ComplianceDocumentListNew1.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.ComplianceDocument.ComplianceDocumentListNew1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.4.0/jszip.min.js"></script>

    <style type="text/css">
        input[type=checkbox], input[type=radio] {
            margin: 4px 4px 0;
            line-height: normal;
        }

        .div.k-grid-footer, div.k-grid-header {
            border-top-width: 1px;
            margin-right: -2px;
        }

        .k-grid-footer-wrap, .k-grid-header-wrap {
            position: relative;
            width: 100%;
            overflow: hidden;
            border-style: solid;
            border-width: 0 1px 0 0;
            zoom: 1;
        }

        .k-grid-content {
            min-height: 380px !important;
        }

        #grid1 .k-grid-content {
            min-height: 350px !important;
        }

        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 2.0em;
            border-bottom-width: 1px;
            background-color: white;
            border-width: 0 1px 1px 0px;
            font-size:12px;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
            background-color: white;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: white;
        }

        #grid .k-grid-toolbar {
            background: white;
        }

        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 6px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: white;
        }

        html .k-grid tr.k-alt:hover {
            background: white;
        }

        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            background: white;
            border: none;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
            margin-right: 2px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: white;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 0px 1px 0px;
            text-align: center;
        }

        .k-grid-pager {
            margin-top: -1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-i-arrow-60-down, .k-i-arrow-60-left, .k-i-arrow-60-right, .k-i-arrow-60-up {
            cursor: pointer;
            margin-top: 6px;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            border-width: 0px 0px 1px 0px;
            background: #E9EAEA;
            font-weight: bold;
            margin-right: 18px;
            font-family: 'Roboto',sans-serif;
            font-size: 15px;
            color: rgba(0, 0, 0, 0.5);
            height: 20px;
            vertical-align: middle;
        }

        .k-dropdown-wrap.k-state-default {
            background-color: white;
        }

        .k-popup.k-calendar-container, .k-popup.k-list-container {
            background-color: white;
        }

        .k-grid-header th.k-state-focused, .k-list > .k-state-focused, .k-listview > .k-state-focused, .k-state-focused, td.k-state-focused {
            -webkit-box-shadow: inset 0 0 3px 1px white;
            box-shadow: inset 0 0 3px 1px white;
        }

        label.k-label {
            font-family: roboto,sans-serif !important;
            color: #515967;
            /* font-stretch: 100%; */
            font-style: normal;
            font-weight: 400;
            min-width: 362px;
            white-space: pre-wrap;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: flex;
            margin-bottom: 0px;
        }

        .k-state-default > .k-select {
            border-color: #ceced2;
            margin-top: 0px;
        }

        .k-grid-norecords {
            width: 100%;
            height: 100%;
            text-align: center;
            margin-top: 2%;
        }

        #grid .k-grouping-header {
            font-style: italic;
            margin-top: -7px;
            background-color: white;
        }

        .k-grouping-header {
            font-style: italic;
            background-color: white;
        }

        .k-grid-toolbar {
            background: white;
            border: none;
        }

        .k-grid table {
            width: 100.5%;
            font-size:12px;
        }
       
        .k-active-filter, .k-state-active, .k-state-active:hover {
            background-color: #E9EAEA;
            border-color: #a6a6ad;
            color: #535b6a;
        }

        .k-multiselect-wrap .k-input {
            /*padding-top:6px;*/
            display: inherit !important;
        }
    </style>

    <title></title>
    <script type="text/x-kendo-template" id="template"> 
    </script>
    <script id="fileTemplate" type="text/x-kendo-template">
                 
           
            <div class='file-wrapper'> 
            #=GetFileExtType(FileName)# #=FileName # 
           <%-- <span class='k-icon k-i-file-txt k-i-txt'></span>#=FileName# --%>

    </script>

    <script id="fileExtensionTemplate" type="text/x-kendo-template">
      
            #=FileName.split('.').pop() # 
         
    </script>

    <script type="text/javascript">

        function GetFileExtType(value) {

            if (value.split('.').pop() == "pdf" || value.split('.').pop() == "PDF" || value.split('.').pop() == "Pdf") {
                return "<span class='k-icon k-i-file-pdf k-i-pdf'></span>";
            }
            else if (value.split('.').pop() == "doc" || value.split('.').pop() == "docx" || value.split('.').pop() == "DOC" || value.split('.').pop() == "DOCX") {
                return "<span class='k-icon k-i-file-word k-i-file-doc k-i-word k-i-doc'></span>";
            }

            else if (value.split('.').pop() == "xls" || value.split('.').pop() == "xlsx" || value.split('.').pop() == "XLS" || value.split('.').pop() == "XLSX") {
                return "<span class='k-icon k-i-file-excel k-i-file-xls k-i-excel k-i-xls'></span>";
            }
            else if (value.split('.').pop() == "ppt" || value.split('.').pop() == "pptx" || value.split('.').pop() == "PPT" || value.split('.').pop() == "PPTX") {
                return "<span class='k-icon k-i-file-ppt k-i-ppt'></span>";
            }
            else if (value.split('.').pop() == "msg" || value.split('.').pop() == "MSG") {
                return "<span class='k-icon k-i-email k-i-envelop k-i-letter'></span>";
            }
            else if (value.split('.').pop() == "txt") {
                return "<span class='k-icon k-i-file-txt k-i-txt'></span>";
            }
            else if (value.split('.').pop() == "jpg" || value.split('.').pop() == "JPG" || value.split('.').pop() == "jpeg" || value.split('.').pop() == "JPEG" || value.split('.').pop() == "png" || value.split('.').pop() == "PNG" || value.split('.').pop() == "tif" || value.split('.').pop() == "TIF" || value.split('.').pop() == "tiff" || value.split('.').pop() == "TIFF" || value.split('.').pop() == "bmp" || value.split('.').pop() == "BMP" || value.split('.').pop() == "gif" || value.split('.').pop() == "GIF") {
                return "<span class='k-icon k-i-image k-i-photo'></span>";
            }
            else {
                return "";
            }
        }

        function fclosebtn(tbn) {
            $('#' + tbn).css('display', 'none');
            $('#' + tbn).html('');
        }

        $(document).ready(function () {

            fhead('My Documents / Compliance Documents');
            setactivemenu('ComplianceDocumentList');
            fmaters();

            BindgridFilters();
            Bindgrid();

            Bindpopupfilters();
            Bindpopupgrid();

            $("#txtSearchComplianceID").on('input', function (e) {
                FilterAllMain();
            });
            $("#txtSearchComplianceID1").on('input', function (e) {
                FilterAllAdvancedSearch();
            });

           <%if (Falg == "AUD")%>
           <%{%>

            $('#Startdatepicker').val('<% =SDate%>');
            $('#Lastdatepicker').val('<% =LDate%>');

            $("#Startdatepicker").attr("readonly", true);
            $("#Lastdatepicker").attr("readonly", true);
            $("#dropdownPastData").attr("readonly", true);
            $("#dropdownFY").attr("readonly", true);

            $('#dropdownPastData').val('All');
            $('#dropdownlistTypePastdata').val('All');
           <%}%>

            $("#Startdatepicker").kendoDatePicker({
                format: "dd-MM-yyyy",
                change: DateFilterCustom
            });

            $("#Lastdatepicker").kendoDatePicker({
                format: "dd-MM-yyyy",
                change: DateFilterCustom
            });

            $(document).on("click", "#chkAll", function (e) {

                if ($('input[id=chkAll]').prop('checked')) {

                    $('input[name="sel_chkbx"]').each(function (i, e) {
                        e.click();
                    });
                }
                else {

                    $('input[name="sel_chkbx"]').attr("checked", false);
                }
                if (($('input[name="sel_chkbx"]:checked').length) == 0) {
                    $('#dvbtndownloadDocument').css('display', 'none');
                }
                else {
                    $('#dvbtndownloadDocument').css('display', 'block');
                }
                return true;
            });

            $(document).on("click", "#chkAllMain", function (e) {

                if ($('input[id=chkAllMain]').prop('checked')) {
                    $('input[name="sel_chkbxMain"]').each(function (i, e) {
                        e.click();
                    });

                    // $('input[name="sel_chkbxMain"]').attr("checked", true);
                }
                else {

                    $('input[name="sel_chkbxMain"]').attr("checked", false);

                    //$('input[name="sel_chkbxMain"]').each(function (i, e) {
                    //    //e.prop("checked", false);
                    //    e.attr("checked", false);
                    //});
                }
                if (($('input[name="sel_chkbxMain"]:checked').length) == 0) {
                    $('#dvbtndownloadDocumentMain').css('display', 'none');
                }
                else {
                    $('#dvbtndownloadDocumentMain').css('display', 'block');
                }
                return true;
            });

            $(document).on("click", "#sel_chkbx", function (e) {
                if (($('input[name="sel_chkbx"]:checked').length) == 0) {
                    $('#dvbtndownloadDocument').css('display', 'none');
                }
                else {
                    $('#dvbtndownloadDocument').css('display', 'block');
                }
                return true;
            });

            $(document).on("click", "#sel_chkbxMain", function (e) {
                if (($('input[name="sel_chkbxMain"]:checked').length) == 0) {
                    $('#dvbtndownloadDocumentMain').css('display', 'none');
                }
                else {
                    $('#dvbtndownloadDocumentMain').css('display', 'block');
                }
                return true;
            });

        });

        function DateFilterCustom() {

            $('input[id=chkAll]').prop('checked', false);
            $('#dvbtndownloadDocument').css('display', 'none');
            $("#dropdownPastData").data("kendoDropDownList").select(4);
            var setStartDate = $("#Startdatepicker").val();
            var setEndDate = $("#Lastdatepicker").val();
            if (setStartDate != null && setStartDate != "") {
                $("#Startdatepicker").data("kendoDatePicker").value(setStartDate);
            }
            if (setEndDate != null && setEndDate != "") {
                $("#Lastdatepicker").data("kendoDatePicker").value(setEndDate);
            }
            FilterAllAdvancedSearch();
        }

        function Bindgrid() {

            var grid = $("#grid").kendoGrid({

                dataSource: {

                    transport: {
                        read: {
                            url: '<% =Path%>data/KendoMyDocument?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=<% =Falg%>&MonthId=<% =IsMonthID%>&StatusFlag=-1&FY=',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>data/KendoMyDocument?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=<% =Falg%>&MonthId=<% =IsMonthID%>&StatusFlag=-1&FY="

                    },
                    pageSize: 10,
                    schema: {
                        model: {
                            id: "ID",

                            fields: {
                                ComplianceID: { type: "string", },
                                ScheduledOn: { type: "date", },
                            }
                        },

                    },

                },
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                toolbar: kendo.template($("#template").html()),
                //height: 513,
                sortable: true,
                groupable: true,
                filterable: true,

                columnMenu: true,
                pageable: true,
                pageable: {
                    numeric: true,
                    pageSizes: [5, 10, 20, 'All'],
                    pageSize: 10,
                    buttonCount: 3,
                },
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                sort: onSorting1,
                page: onPaging1,
                columns: [
                    {
                        //field: "ID", title: " ",
                        template: "<input name='sel_chkbxMain' id='sel_chkbxMain' type='checkbox' value=#=ScheduledOnID# >",
                        filterable: false, sortable: false,
                        headerTemplate: "<input type='checkbox' id='chkAllMain' />",
                        width: "5%;"//, lock: true
                    },
                    { hidden: true, field: "RiskCategory", title: "Risk", filterable: { multi: true, extra: false, search: true, }, width: "20%" },
                    { hidden: true, field: "CustomerBranchID", title: "Branch ID", filterable: { multi: true, extra: false, search: true, }, width: "20%" },
                    {
                        field: "Branch", title: 'Location',
                        width: "16.7%;",
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: { multi: true, extra: false, search: true, }


                    },
                    { hidden: true, field: "ComplianceID", title: "Compliance ID", filterable: { multi: true, extra: false, search: true, }, width: "20%" },

                    {
                        field: "ShortDescription", title: 'Compliance',
                        width: "30%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: 'PerformerName', title: 'Performer',
                        width: "15%",
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: 'ReviewerName', title: 'Reviewer',
                        hidden: true,
                        width: "10%",
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        hidden: true,
                        field: "EventName", title: 'Event Name',
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "20%"
                    },
                    {
                        hidden: true,
                        field: "EventNature", title: 'Event Nature',
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "20%"
                    },
                    {
                        field: "ScheduledOn", title: 'Due Date',
                        type: "date",
                        format: "{0:dd-MMM-yyyy}",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    type: "date",
                                    format: "{0:dd-MM-yyyy}",
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            },
                        }, width: "15%"
                    },
                    {
                        field: "ForMonth", title: 'Period', filterable: { multi: true, extra: false, search: true, }, width: "14%"
                    },
                    {
                        field: "Status", title: 'Status',
                        //   width: 130,
                        attributes: {
                            style: 'white-space: nowrap '
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "15%"
                    },
                    {
                        command: [
                            {
                                name: "edit", text: "", iconClass: "k-icon k-i-hyperlink-open", className: "ob-edit",
                                visible: function () {
                                    if (<% =UploadDocumentLink%> == 1) {
                                        return false;
                                    }
                                    else {
                                        return true;
                                    }
                                    //return dataItem.ISLink === false
                                }
                            },
                            {
                                name: "edit1", text: "", iconClass: "k-icon k-i-download", className: "ob-download",
                              <%--  visible: function () {
                                     if (<% =UploadDocumentLink%> == 1) {
                                         return false;
                                    }
                                     else {
                                         return true;
                                    }
                                    //return dataItem.ISLink === false
                                }--%>
                            },
                            //{
                            //    text: "Click Here", className: "ob-click",
                            //    visible: function (dataItem) {
                            //        return dataItem.ISLink === true
                            //    }
                            //},
                            { name: "edit2", text: "", iconClass: "k-icon k-i-eye", className: "ob-overview" }
                        ], title: "Action", lock: true, width: 150
                    }
                ]
            });

            function onSorting1(arg) {
                settracknew('Compliance Document', 'Sorting', arg.sort.field, '');
            }

            function onPaging1(arg) {
                settracknew('Compliance Document', 'Paging', arg.page, '');
            }

            $("#grid").kendoTooltip({
                filter: ".k-grid-edit",
                content: function (e) {
                    return "View";
                }
            });

            $("#grid").kendoTooltip({
                filter: ".k-grid-edit1",
                content: function (e) {
                    return "Download";
                }
            });

            $("#grid").kendoTooltip({
                filter: ".k-grid-edit2",
                content: function (e) {
                    return "Overview";
                }
            });

            $("#grid").kendoTooltip({
                filter: "td[role=gridcell]", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");

            $('#grid').kendoTooltip({
                filter: "th[role=columnheader]",  //what element
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                },
                beforeShow: function (e) {
                    var target = e.target;

                    var isTextBiggerThanElement = findOutSize(target);
                    if (!isTextBiggerThanElement) {
                        e.preventDefault();
                    }
                },
                position: 'down'
            }).data("kendoTooltip");

            $('#grid').kendoTooltip({
                filter: "th[data-role=droptarget]",  //what element
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                },
                beforeShow: function (e) {
                    var target = e.target;

                    var isTextBiggerThanElement = findOutSize(target);
                    if (!isTextBiggerThanElement) {
                        e.preventDefault();
                    }
                },
                position: 'down'
            }).data("kendoTooltip");

            $('#grid').kendoTooltip({
                filter: "td[colspan=7]",  //what element
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                },
                position: 'down'
            }).data("kendoTooltip");

            $('#grid').kendoTooltip({
                filter: "td[colspan=8]",  //what element
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                },
                position: 'down'
            }).data("kendoTooltip");

            $('#grid').kendoTooltip({
                filter: "td[colspan=9]",  //what element
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                },
                position: 'down'
            }).data("kendoTooltip");

            $('#grid').kendoTooltip({
                filter: "td[colspan=10]",  //what element
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                },
                position: 'down'
            }).data("kendoTooltip");

            $('#grid').kendoTooltip({
                filter: "td[colspan=11]",  //what element
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                },
                position: 'down'
            }).data("kendoTooltip");

            $('#grid').kendoTooltip({
                filter: "td[colspan=12]",  //what element
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                },
                position: 'down'
            }).data("kendoTooltip");

            $('#grid').kendoTooltip({
                filter: "td[colspan=13]",  //what element
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "" && content != "  ") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                },
                position: 'down'
            }).data("kendoTooltip");

            $("#grid tbody").on("click", "tr", function (e) {
                var rowElement = this;
                var row = $(rowElement);
                var grid = $("#grid").getKendoGrid();
                if (row.hasClass("k-state-selected")) {
                    var selected = grid.select();
                    selected = $.grep(selected, function (x) {
                        var itemToRemove = grid.dataItem(row);
                        var currentItem = grid.dataItem(x);
                        return itemToRemove.ID != currentItem.ID
                    })
                    grid.clearSelection();
                    grid.select(selected);
                    //e.stopPropagation();
                } else {
                    grid.select(row)
                    //e.stopPropagation();
                }
            });

            $(document).on("click", "#grid tbody tr .ob-edit", function (e) {
                var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
                OpenDocumentOverviewpupMain(item.ScheduledOnID, item.ID)
                return true;
            });

            $(document).on("click", "#grid tbody tr .ob-download", function (e) {
                var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
                OpenDownloadOverviewpup(item.ScheduledOnID, item.ID)
                return true;
            });

            $(document).on("click", "#grid tbody tr .ob-overview", function (e) {
                var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
                OpenOverViewpupMain(item.ScheduledOnID, item.ComplianceInstanceID);
                return true;
            });

            $(document).on("click", "#grid tbody tr .ob-delete", function (e) {
                var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));

                e.preventDefault();

                return true;
            });
        }

        function Bindpopupgrid() {
            $(".k-grid1-content tbody[role='rowgroup'] tr[role='row'] td:first-child").prepend('<span class="k-icon k-i-filter"</span>');

            var grid1 = $("#grid1").kendoGrid({
                dataSource: {
                    //type: "odata",
                    transport: {
                        read: {
                            url: '<% =Path%>data/KendoMyDocument?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=<% =Falg%>&MonthId=<% =IsMonthID%>&StatusFlag=-1&FY=',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>data/KendoMyDocument?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=<% =Falg%>&MonthId=<% =IsMonthID%>&StatusFlag=-1&FY="
                    },
                    pageSize: 10,
                    schema: {
                        model: {
                            id: "ID",

                            fields: {
                                ComplianceID: { type: "string", },
                                ScheduledOn: { type: "date" }
                            }
                        },

                    },
                },
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                //height: 513,
                sortable: true,
                groupable: true,
                filterable: true,
                columnMenu: true,
                pageable: true,
                pageable: {
                    numeric: true,
                    pageSizes: [5, 10, 20, 'All'],
                    pageSize: 10,
                    buttonCount: 3,
                },
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                sort: onSorting,
                page: onPaging,
                columns: [
                    {
                        template: "<input name='sel_chkbx' id='sel_chkbx' type='checkbox' value=#=ScheduledOnID# >",
                        filterable: false, sortable: false,
                        headerTemplate: "<input type='checkbox' id='chkAll' />",
                        width: "5%;"//, lock: true
                    },
                    { hidden: true, field: "RiskCategory", title: "Risk", filterable: { multi: true, extra: false, search: true }, width: "20%" },
                    { hidden: true, field: "CustomerBranchID", title: "Branch ID", filterable: { multi: true, extra: false, search: true, }, width: "20%" },
                    {
                        hidden: true, field: "FileName", title: "Document Name",
                        template: kendo.template($('#fileTemplate').html()), width: "29.7%", filterable: { multi: true, extra: false, search: true, }, width: "20%"
                    },
                    { hidden: true, field: "Version", title: "Version", filterable: { multi: true, extra: false, search: true, }, width: "20%" },
                    {
                        field: "Branch", title: 'Location',
                        width: "16.7%;",
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "ShortDescription", title: 'Compliance',
                        width: "43.7%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        hidden: true,
                        field: "EventName", title: 'Event Name',
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "20%"
                    },
                    {
                        hidden: true,
                        field: "EventNature", title: 'Event Nature',
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "20%"
                    },
                    {
                        field: "ScheduledOn", title: 'Due Date',
                        type: "date",
                        format: "{0:dd-MMM-yyyy}",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    type: "date",
                                    format: "{0:dd-MM-yyyy}",
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            },
                        }, width: "12%"
                    },
                    {
                        field: 'PerformerName', title: 'Performer',
                        width: "15%",
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: 'ReviewerName', title: 'Reviewer',
                        hidden: true,
                        width: "10%",
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "ForMonth", title: 'Period', filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "14%"
                    },
                    {
                        field: "Status", title: 'Status',
                        //   width: 130,
                        attributes: {
                            style: 'white-space: nowrap '
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "20%"
                    },

                    {
                        hidden: true, field: "VersionDate", title: "Uploaded Date", type: "date",
                        template: "#= kendo.toString(kendo.parseDate(VersionDate, 'yyyy-MM-dd'), 'dd-MMM-yyyy') #",
                        filterable: { multi: true, extra: false, search: true, }, width: "20%"
                    },
                    {
                        hidden: true, field: "FileName", title: "Type",
                        template: kendo.template($('#fileExtensionTemplate').html()), filterable: { multi: true, extra: false, search: true, }, width: "20%"
                    },
                    //{ hidden: true, field: "", title: "Type" },
                    { hidden: true, field: "", title: "Uploaded By", filterable: { multi: true, extra: false, search: true, }, width: "20%" },
                    { hidden: true, field: "", title: "Size", filterable: { multi: true, extra: false, search: true, }, width: "20%" },
                    { hidden: true, field: "ActID", title: "", filterable: { multi: true, extra: false, search: true, }, width: "20%" },
                    { hidden: true, field: "UserID", title: "User ID", filterable: { multi: true, extra: false, search: true, }, width: "20%" },
                    { hidden: true, field: "ComplianceID", title: "Compliance ID", filterable: { multi: true, extra: false, search: true, }, width: "20%" },
                    {
                        command: [
                            {
                                name: "edit5", text: "", iconClass: "k-icon k-i-hyperlink-open", className: "ob-edit",
                                visible: function () {
                                    if (<% =UploadDocumentLink%> == 1) {
                                        return false;
                                    }
                                    else {
                                        return true;
                                    }
                                    //return dataItem.ISLink === false
                                }
                            },
                            {
                                name: "edit6", text: "", iconClass: "k-icon k-i-download", className: "ob-download",
                               <%-- visible: function () {
                                     if (<% =UploadDocumentLink%> == 1) {
                                         return false;
                                    }
                                     else {
                                         return true;
                                    }
                                    //return dataItem.ISLink === false
                                }--%>
                            },
                            //{
                            //    text: "Click Here", className: "ob-click",
                            //    visible: function (dataItem) {
                            //        return dataItem.ISLink === true
                            //    }
                            //},
                            { name: "edit2", text: "", iconClass: "k-icon k-i-eye", className: "ob-overviewMain" }
                        ], title: "Action", lock: true, width: 150
                    },
                ]
            });

            function onSorting(arg) {
                settracknew('Compliance Document', 'Sorting', arg.sort.field, '');
            }

            function onPaging(arg) {
                settracknew('Compliance Document', 'Paging', arg.page, '');
            }

            $("#grid1").kendoTooltip({
                filter: ".k-grid-edit2",
                content: function (e) {
                    return "Overview";
                }
            });

            $("#grid1").kendoTooltip({
                filter: "td[role=gridcell]", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");

            $('#grid1').kendoTooltip({
                filter: "th[role=columnheader]",  //what element
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                },
                beforeShow: function (e) {
                    var target = e.target;

                    var isTextBiggerThanElement = findOutSize(target);
                    if (!isTextBiggerThanElement) {
                        e.preventDefault();
                    }
                },
                position: 'down'
            }).data("kendoTooltip");

            $('#grid1').kendoTooltip({
                filter: "th[data-role=droptarget]",  //what element
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                },
                beforeShow: function (e) {
                    var target = e.target;

                    var isTextBiggerThanElement = findOutSize(target);
                    if (!isTextBiggerThanElement) {
                        e.preventDefault();
                    }
                },
                position: 'down'
            }).data("kendoTooltip");

            $('#grid1').kendoTooltip({
                filter: "td[colspan=7]",  //what element
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                },
                position: 'down'
            }).data("kendoTooltip");

            $('#grid1').kendoTooltip({
                filter: "td[colspan=8]",  //what element
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                },
                position: 'down'
            }).data("kendoTooltip");

            $('#grid1').kendoTooltip({
                filter: "td[colspan=9]",  //what element
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                },
                position: 'down'
            }).data("kendoTooltip");

            $('#grid1').kendoTooltip({
                filter: "td[colspan=10]",  //what element
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                },
                position: 'down'
            }).data("kendoTooltip");

            $('#grid1').kendoTooltip({
                filter: "td[colspan=11]",  //what element
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                },
                position: 'down'
            }).data("kendoTooltip");

            $('#grid1').kendoTooltip({
                filter: "td[colspan=12]",  //what element
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                },
                position: 'down'
            }).data("kendoTooltip");

            $('#grid1').kendoTooltip({
                filter: "td[colspan=13]",  //what element
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "" && content != "  ") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                },
                position: 'down'
            }).data("kendoTooltip");


            $(document).on("click", "#grid1 tbody tr .ob-overviewMain", function (e) {
                var item = $("#grid1").data("kendoGrid").dataItem($(this).closest("tr"));
                //myWindowAdv.close();
                // $("#divAdvanceSearchModel").close();
                OpenOverViewpup(item.ScheduledOnID, item.ComplianceInstanceID);
                return true;
            });

            $(document).on("click", "#grid1 tbody tr .ob-download", function (e) {
                var item = $("#grid1").data("kendoGrid").dataItem($(this).closest("tr"));
                OpenDownloadOverviewpupAdvanceSearch(item.ScheduledOnID, item.ID)
                return true;
            });

            $(document).on("click", "#grid1 tbody tr .ob-edit", function (e) {
                var item = $("#grid1").data("kendoGrid").dataItem($(this).closest("tr"));
                OpenDocumentOverviewpup(item.ScheduledOnID, item.ID)
                return true;
            });
        }

        function BindgridFilters() {
            $("#dropdownlistRisk").kendoDropDownTree({
                placeholder: "Risk",
                checkboxes: true,
                checkAll: true,
                autoClose: false,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",

                change: function () {
                    settracknew('Compliance Document', 'Filtering', 'Risk', '')
                    FilterAllMain();
                    fCreateStoryBoard('dropdownlistRisk', 'filterrisk', 'risk');

                    $('input[id=chkAllMain]').prop('checked', false);
                    $('#dvbtndownloadDocumentMain').css('display', 'none');
                },
                dataSource: [
                    { text: "Critical", value: "3" },
                    { text: "High", value: "0" },
                    { text: "Medium", value: "1" },
                    { text: "Low", value: "2" }
                ]
            });

            $("#dropdownlistStatus").kendoDropDownTree({
                placeholder: "Status",
                checkboxes: true,
                checkAll: true,
                autoClose: false,
                autoWidth: true,
                checkAllTemplate: "Select All",
                change: function (e) {
                    settracknew('Compliance Document', 'Filtering', 'Status', '')

                    FilterAllMain();
                    fCreateStoryBoard('dropdownlistStatus', 'filterstatus', 'status')

                    $('input[id=chkAllMain]').prop('checked', false);
                    $('#dvbtndownloadDocumentMain').css('display', 'none');
                },
                dataSource: [
                    { text: "Closed-Delayed", value: "Closed-Delayed" },
                    { text: "Closed-Timely", value: "Closed-Timely" },
                    { text: "Rejected", value: "Rejected" },
                    <%if (IsNotCompiled == true)%>
                    <%{%>
                    { text: "Not Complied", value: "Not Complied" },
                    <%}%>
                    { text: "In Progress", value: "In Progress" },
                    { text: "Pending For Review", value: "Pending For Review" },
                    { text: "Interim Rejected", value: "Interim Rejected" },
                    { text: "Interim Review Approved", value: "Interim Review Approved" },
                    { text: "Submitted For Interim Review", value: "Submitted For Interim Review" }
                ]
            });

            $("#dropdowntree").kendoDropDownTree({
                placeholder: "Entity/Sub-Entity/Location",
                checkboxes: {
                    checkChildren: true
                },
                //checkboxes: true,
                checkAll: true,
                autoClose: false,
                autoWidth: true,
                checkAllTemplate: "Select All",
                dataTextField: "Name",
                dataValueField: "ID",
                //optionLabel: "All",
                change: function (e) {
                    settracknew('Compliance Document', 'Filtering', 'Entity/Sub-Entity/Location', '')
                    FilterAllMain();
                    fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc');

                    $('input[id=chkAllMain]').prop('checked', false);
                    $('#dvbtndownloadDocumentMain').css('display', 'none');
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Falg%>&IsStatutoryInternal=S',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                           //read: "<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Falg%>&IsStatutoryInternal=S"
                    },
                    schema: {
                        data: function (response) {
                            return response[0].locationList;
                        },
                        model: {
                            children: "Children"
                        }
                    }
                }
            });

            $("#dropdownlistComplianceType").kendoDropDownList({
                placeholder: "Compliance Type",
                dataTextField: "text",
                dataValueField: "value",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                dataSource: [
                    { text: "Statutory", value: "-1" },
                    { text: "Internal", value: "0" },
                    { text: "Event Based", value: "1" },
                    { text: "Statutory CheckList", value: "2" },
                    { text: "Internal CheckList", value: "3" },
                    { text: "Event Based CheckList", value: "4" }
                ],
                index: 0,
                change: function (e) {

                    DataBindDaynamicKendoGriddMain();

                    settracknew('Compliance Document', 'Filtering', 'Compliance Type', '')
                }
            });

            $("#dropdownlistTypePastdata").kendoDropDownList({
                placeholder: "Period",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    DataBindDaynamicKendoGriddMain();
                    settracknew('Compliance Document', 'Filtering', 'Period', '')
                },
                index: 1,
                dataSource: [
                    { text: "Last Month", value: "1" },
                    { text: "Last Three Months", value: "3" },
                    { text: "Last Six Months", value: "6" },
                    { text: "Last Year", value: "12" },
                    { text: "All Period", value: "All" }
                ]
            });
            var evalEventName = 0;

            if ($("#dropdownEventName").val() != '') {
                evalEventName = $("#dropdownEventName").val()
            }

            $("#dropdownEventName").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: true,
                dataTextField: "EventName",
                dataValueField: "eventid",
                optionLabel: "Select Event Name",
                change: function (e) {
                    settracknew('Compliance Document', 'Filtering', 'EventName', '')
                    var values = this.value();

                    if (values != "" && values != null) {
                        FilterAllMain();
                        if ($("#dropdownEventName").val() != '') {
                            evalEventName = $("#dropdownEventName").val()
                        }
                        var dataSource1 = new kendo.data.DataSource({
                            transport: {
                                read: {
                                    url: '<% =Path%>data/KendoGetEventNature?UId=<% =UId%>&CId=<% =CustId%>&EventID=' + evalEventName + '&RoleId=<% =RoleID%>',
                                    dataType: "json",
                                    beforeSend: function (request) {
                                        request.setRequestHeader('Authorization', '<% =Authorization%>');
                                    },
                                }
                                //read: '<% =Path%>data/KendoGetEventNature?UId=<% =UId%>&CId=<% =CustId%>&EventID=' + evalEventName + '&RoleId=<% =RoleID%>'
                            },
                        });
                        dataSource1.read();
                        $("#dropdownEventNature").data("kendoDropDownList").setDataSource(dataSource1);
                    }
                    else {
                        ClearAllFilterMain();
                    }
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>data/KendoGetEventName?UId=<% =UId%>&CId=<% =CustId%>&RoleId=<% =RoleID%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                    }
                }
            });

            $("#dropdownEventNature").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: true,
                dataTextField: "EventNature",
                dataValueField: "EventScheduleOnid",
                optionLabel: "Select Event Nature",
                change: function (e) {
                    settracknew('Compliance Document', 'Filtering', 'EventNature', '')

                    var values = this.value();
                    if (values != "" && values != null) {
                        FilterAllMain();
                    }
                    else {
                        ClearAllFilterMain();
                    }
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>data/KendoGetEventNature?UId=<% =UId%>&CId=<% =CustId%>&EventID=' + evalEventName + '&RoleId=<% =RoleID%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: '<% =Path%>data/KendoGetEventNature?UId=<% =UId%>&CId=<% =CustId%>&EventID=' + evalEventName + '&RoleId=<% =RoleID%>'
                    }
                }
            });
        }

        function Bindpopupfilters() {
            $("#dropdownComplianceSubType").kendoDropDownTree({
                placeholder: "Compliance Sub Type",
                checkboxes: true,
                checkAll: true,
                autoClose: false,
                dataTextField: "Name",
                dataValueField: "ID",
                //optionLabel: "All",
                change: function (e) {
                    settracknew('Compliance Document', 'Filtering', 'Compliance Sub Type', '')
                    fCreateStoryBoard('dropdownComplianceSubType', 'filterCompSubType', 'CompSubType');
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/BindComplianceSubTypeList?UId=<% =UserId%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Data/BindComplianceSubTypeList?UId=<% =UserId%>"
                    }
                }
            });

            $("#dropdownPastData").kendoDropDownList({
                placeholder: "Period",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    DataBindDaynamicKendoGrid();
                    settracknew('Compliance Document', 'Filtering', 'Period', '')
                },
                index: 1,
                dataSource: [
                    { text: "Last Month", value: "1" },
                    { text: "Last Three Months", value: "3" },
                    { text: "Last Six Months", value: "6" },
                    { text: "Last Year", value: "12" },
                    { text: "All Period", value: "All" }
                ]
            });

            var evalEventName1 = 0;

            if ($("#dropdownEventName1").val() != '') {
                evalEventName1 = $("#dropdownEventName1").val()
            }

            $("#dropdownEventName1").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: true,
                dataTextField: "EventName",
                dataValueField: "eventid",
                optionLabel: "Select Event Name",
                change: function (e) {
                    settracknew('Compliance Document', 'Filtering', 'EventName', '')

                    var values = this.value();

                    if (values != "" && values != null) {
                        FilterAllAdvancedSearch();
                        if ($("#dropdownEventName1").val() != '') {
                            evalEventName1 = $("#dropdownEventName1").val()
                        }
                        var dataSource1 = new kendo.data.DataSource({
                            transport: {
                                read: {
                                    url: '<% =Path%>data/KendoGetEventNature?UId=<% =UId%>&CId=<% =CustId%>&EventID=' + evalEventName1 + '&RoleId=<% =RoleID%>',
                                    dataType: "json",
                                    beforeSend: function (request) {
                                        request.setRequestHeader('Authorization', '<% =Authorization%>');
                                    },
                                }
                                //read: '<% =Path%>data/KendoGetEventNature?UId=<% =UId%>&CId=<% =CustId%>&EventID=' + evalEventName1 + '&RoleId=<% =RoleID%>'
                            },
                        });
                        dataSource1.read();
                        $("#dropdownEventNature1").data("kendoDropDownList").setDataSource(dataSource1);
                    }
                    else {
                        ClearAllFilter();
                    }
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>data/KendoGetEventName?UId=<% =UId%>&CId=<% =CustId%>&RoleId=<% =RoleID%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read:"<% =Path%>data/KendoGetEventName?UId=<% =UId%>&CId=<% =CustId%>&RoleId=<% =RoleID%>"
                    }
                }
            });

            $("#dropdownEventNature1").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: true,
                dataTextField: "EventNature",
                dataValueField: "EventScheduleOnid",
                optionLabel: "Select Event Nature",
                change: function (e) {

                    var values = this.value();
                    if (values != "" && values != null) {
                        FilterAllAdvancedSearch();
                    }
                    else {
                        ClearAllFilter();
                    }
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>data/KendoGetEventNature?UId=<% =UId%>&CId=<% =CustId%>&EventID=' + evalEventName1 + '&RoleId=<% =RoleID%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: '<% =Path%>data/KendoGetEventNature?UId=<% =UId%>&CId=<% =CustId%>&EventID=' + evalEventName1 + '&RoleId=<% =RoleID%>'
                    }
                }
            });

            $("#dropdowntree1").kendoDropDownTree({
                placeholder: "Entity/Sub-Entity/Location",
                checkboxes: {
                    checkChildren: true
                },
                //checkboxes: true,
                checkAll: true,
                autoClose: false,
                dataTextField: "Name",
                dataValueField: "ID",
                //optionLabel: "All",
                change: function (e) {

                    FilterAllAdvancedSearch();
                    settracknew('Compliance Document', 'Filtering', 'Entity/Sub-Entity/Location', '')
                    fCreateStoryBoard('dropdowntree1', 'filtersstoryboard1', 'loc1');

                    $('input[id=chkAll]').prop('checked', false);
                    $('#dvbtndownloadDocument').css('display', 'none');
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Falg%>&IsStatutoryInternal=S',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Falg%>&IsStatutoryInternal=S"
                    },
                    schema: {
                        data: function (response) {
                            return response[0].locationList;
                        },
                        model: {
                            children: "Children"
                        }
                    }
                }
            });

            $("#dropdownlistRisk1").kendoDropDownTree({
                placeholder: "Risk",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",

                change: function () {
                    settracknew('Compliance Document', 'Filtering', 'Risk', '')
                    FilterAllAdvancedSearch();
                    fCreateStoryBoard('dropdownlistRisk1', 'filterrisk1', 'risk1');
                    $('input[id=chkAll]').prop('checked', false);
                    $('#dvbtndownloadDocument').css('display', 'none');
                },
                dataSource: [
                    { text: "Critical", value: "3" },
                    { text: "High", value: "0" },
                    { text: "Medium", value: "1" },
                    { text: "Low", value: "2" }
                ]
            });

            $("#dropdownFY").kendoDropDownList({
                autoWidth: true,
                dataTextField: "FinancialYear",
                dataValueField: "FinancialYear",
                optionLabel: "Financial Year",
                change: function () {
                    DataBindDaynamicKendoGrid();
                    settracknew('Compliance Document', 'Filtering', 'dropdownFY', '')
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/GetFYDetail',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                    }
                }
            });

            $("#dropdownUser").kendoDropDownTree({
                placeholder: "User",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "FullName",
                dataValueField: "UID",

                change: function () {
                    settracknew('Compliance Document', 'Filtering', 'User', '')
                    FilterAllAdvancedSearch();
                    fCreateStoryBoard('dropdownUser', 'filterUser', 'user');

                    $('input[id=chkAll]').prop('checked', false);
                    $('#dvbtndownloadDocument').css('display', 'none');

                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/KendoUserDocumentListNew?UserId=<% =UId%>&CustId=<% =CustId%>&Flag=<% =Falg%>&status=-1',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Data/KendoUserList?UserId=<% =UId%>&CustId=<% =CustId%>&Flag=<% =Falg%>"
                    },
                }
            });

            $("#dropdownlistStatus1").kendoDropDownTree({
                placeholder: "Status",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                autoWidth: true,
                checkAllTemplate: "Select All",
                change: function (e) {

                    FilterAllAdvancedSearch();
                    settracknew('Compliance Document', 'Filtering', 'Status', '');
                    fCreateStoryBoard('dropdownlistStatus1', 'filterstatus1', 'status1');

                    $('input[id=chkAll]').prop('checked', false);
                    $('#dvbtndownloadDocument').css('display', 'none');
                },
                dataSource: [
                    { text: "Closed-Delayed", value: "Closed-Delayed" },
                    { text: "Closed-Timely", value: "Closed-Timely" },
                    { text: "Rejected", value: "Rejected" },
                    <%if (IsNotCompiled == true)%>
                    <%{%>
                    { text: "Not Complied", value: "Not Complied" },
                    <%}%>
                    { text: "In Progress", value: "In Progress" },
                    { text: "Pending For Review", value: "Pending For Review" },
                    { text: "Interim Rejected", value: "Interim Rejected" },
                    { text: "Interim Review Approved", value: "Interim Review Approved" },
                    { text: "Submitted For Interim Review", value: "Submitted For Interim Review" }
                ]
            });

            $("#dropdownlistComplianceType1").kendoDropDownList({

                autoWidth: true,

                dataTextField: "text",
                dataValueField: "value",
                index: 0,
                change: function (e) {
                    DataBindDaynamicKendoGrid();
                    settracknew('Compliance Document', 'Filtering', 'dropdownlistComplianceType1', '')
                },
                dataSource: [
                    { text: "Statutory", value: "-1" },
                    { text: "Internal", value: "0" },
                    { text: "Event Based", value: "1" },
                    { text: "Statutory CheckList", value: "2" },
                    { text: "Internal CheckList", value: "3" },
                    { text: "Event Based CheckList", value: "4" }
                ]
            });

            $("#dropdownACT").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: false,
                dataTextField: "Name",
                dataValueField: "ID",
                optionLabel: "Select Act",
                change: function (e) {
                    settracknew('Compliance Document', 'Filtering', 'dropdownACT', '')
                    FilterAllAdvancedSearch();
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/BindActList?UId=<% =UId%>&CId=<% =CustId%>&Flag=<% =Falg%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Data/BindActList?UId=<% =UId%>&CId=<% =CustId%>&Flag=<% =Falg%>"
                    }
                }
            });

             <%if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsLabelApplicable == 1)%><%{%>
            $("#dropdownSequence").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: false,
                dataTextField: "Name",
                dataValueField: "ID",
                optionLabel: "Label",
                change: function (e) {
                    FilterAllAdvancedSearch();
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/GetSequenceDetail?Flag=S&CustomerID=<% =CustId%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: '<% =Path%>Data/GetSequenceDetail?Flag=S&CustomerID=<% =CustId%>',
                    }
                }
                //, dataBound: function (e) {
                //    e.sender.list.width("900");
                //}
            });
             <%}%>
        }

        function DataBindDaynamicKendoGriddMain() {

            $('input[id=chkAllMain]').prop('checked', false);
            $('#dvdropdownEventNature').css('display', 'none');
            $('#dvdropdownEventName').css('display', 'none');
            $("#dropdowntree").data("kendoDropDownTree").value([]);
            $("#dropdownlistRisk").data("kendoDropDownTree").value([]);
            $("#dropdownlistStatus").data("kendoDropDownTree").value([]);
            $('#dvbtndownloadDocumentMain').css('display', 'none');


            $("#grid").data('kendoGrid').dataSource.data([]);

                    <%if (Falg == "AUD")%>
            <%{%>
            var dataSource = new kendo.data.DataSource({
                transport: {
                    read: {
                        url: '<% =Path%>data/KendoMyDocument?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=<% =Falg%>&MonthId=<% =IsMonthID%>&StatusFlag=' + $("#dropdownlistComplianceType").val() + '&FY=',
                        dataType: "json",
                        beforeSend: function (request) {
                            request.setRequestHeader('Authorization', '<% =Authorization%>');
                        },
                    }
                    //read: '<% =Path%>data/KendoMyDocument?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=<% =Falg%>&MonthId=<% =IsMonthID%>&StatusFlag=' + $("#dropdownlistComplianceType").val() + '&FY='
                },
                pageSize: 10,
            });
            var grid = $('#grid').data("kendoGrid");
            dataSource.read();
            grid.setDataSource(dataSource);
                    <%}%>
            <%else %>
            <%{%>                                          
            var dataSource = new kendo.data.DataSource({
                transport: {
                    read: {
                        url: '<% =Path%>data/KendoMyDocument?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=<% =Falg%>&MonthId=' + $("#dropdownlistTypePastdata").val() + '&StatusFlag=' + $("#dropdownlistComplianceType").val() + '&FY=',
                        dataType: "json",
                        beforeSend: function (request) {
                            request.setRequestHeader('Authorization', '<% =Authorization%>');
                        },
                    }
                    //read: '<% =Path%>data/KendoMyDocument?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=<% =Falg%>&MonthId=' + $("#dropdownlistTypePastdata").val() + '&StatusFlag=' + $("#dropdownlistComplianceType").val() + '&FY='
                },
                schema: {
                    model: {
                        fields: {
                            ScheduledOn: { type: "date" },
                            ComplianceID: { type: "string" },
                        }
                    }
                },
                pageSize: 10,
            });
            var grid = $('#grid').data("kendoGrid");
            dataSource.read();
            grid.setDataSource(dataSource);
                    <%}%>


            if ($("#dropdownlistComplianceType").val() == 0 || $("#dropdownlistComplianceType").val() == 3)//Internal and Internal Checklist
            {
                var dataSource12 = new kendo.data.HierarchicalDataSource({
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Falg%>&IsStatutoryInternal=I',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Falg%>&IsStatutoryInternal=I"
                    },
                    schema: {
                        data: function (response) {
                            return response[0].locationList;
                        },
                        model: {
                            children: "Children"
                        }
                    }
                });
                dataSource12.read();
                $("#dropdowntree").data("kendoDropDownTree").setDataSource(dataSource12);
            }
            else {
                var dataSource12 = new kendo.data.HierarchicalDataSource({
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Falg%>&IsStatutoryInternal=S',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Falg%>&IsStatutoryInternal=S"
                    },
                    schema: {
                        data: function (response) {
                            return response[0].locationList;
                        },
                        model: {
                            children: "Children"
                        }
                    }
                });
                dataSource12.read();
                $("#dropdowntree").data("kendoDropDownTree").setDataSource(dataSource12);
            }


            $("#dropdownEventName").data("kendoDropDownList").select(0);
            $("#dropdownEventNature").data("kendoDropDownList").select(0);
            if ($("#dropdownlistComplianceType").val() == 1 || $("#dropdownlistComplianceType").val() == 4) {
                $('#dvdropdownEventNature').css('display', 'block');
                $('#dvdropdownEventName').css('display', 'block');

                $("#grid").data("kendoGrid").showColumn(5);
                $("#grid").data("kendoGrid").showColumn(6);
                $("#grid").data("kendoGrid").showColumn(3);
                $("#grid").data("kendoGrid").showColumn(8);
                //$("#grid").data("kendoGrid").hideColumn(3);
                //$("#grid").data("kendoGrid").hideColumn(8);
            }
            else {
                $("#grid").data("kendoGrid").showColumn(8);
                $("#grid").data("kendoGrid").showColumn(3);
                $("#grid").data("kendoGrid").showColumn(5);
                $("#grid").data("kendoGrid").showColumn(6);
                //$("#grid").data("kendoGrid").hideColumn(5);
                //$("#grid").data("kendoGrid").hideColumn(6);
            }
        }

        function DataBindDaynamicKendoGrid() {

            $("#grid1").data('kendoGrid').dataSource.data([]);

            <%if (RoleFlag == 1)%>
            <%{%>
            $("#dropdownUser").data("kendoDropDownTree").value([]);
                    <%}%>
            $('input[id=chkAll]').prop('checked', false);

            $("#grid1").data("kendoGrid").dataSource.filter({});

            $("#dropdowntree1").data("kendoDropDownTree").value([]);
            // $("#dropdownACT").data("kendoDropDownTree").value([]);
            $("#dropdownlistRisk1").data("kendoDropDownTree").value([]);
            //$("#dropdownUser").data("kendoDropDownTree").value([]);
            $("#dropdownlistStatus1").data("kendoDropDownTree").value([]);
            $("#Startdatepicker").data("kendoDatePicker").value(null);
            $("#Lastdatepicker").data("kendoDatePicker").value(null);
            $('#dvbtndownloadDocument').css('display', 'none');

            $("#dropdownACT").data("kendoDropDownList").enable(true);
            var dataSourceUser = new kendo.data.HierarchicalDataSource({
                severFiltering: true,
                transport: {
                    read: {
                        url: '<% =Path%>Data/KendoUserDocumentListNew?UserId=<% =UId%>&CustId=<% =CustId%>&Flag=<% =Falg%>&status=' + $("#dropdownlistComplianceType1").val(),
                        dataType: "json",
                        beforeSend: function (request) {
                            request.setRequestHeader('Authorization', '<% =Authorization%>');
                        },
                    }
                }
            });
            dataSourceUser.read();
            $("#dropdownUser").data("kendoDropDownTree").setDataSource(dataSourceUser);
            if ($("#dropdownlistComplianceType1").val() == 0 || $("#dropdownlistComplianceType1").val() == 3)//Internal and Internal Checklist
            {
                $("#dropdownACT").data("kendoDropDownList").enable(false);

                var dataSource12 = new kendo.data.HierarchicalDataSource({
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Falg%>&IsStatutoryInternal=I',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Falg%>&IsStatutoryInternal=I"
                    },
                    schema: {
                        data: function (response) {
                            return response[0].locationList;
                        },
                        model: {
                            children: "Children"
                        }
                    }
                });
                dataSource12.read();
                $("#dropdowntree1").data("kendoDropDownTree").setDataSource(dataSource12);
            }
            else {
                var dataSource12 = new kendo.data.HierarchicalDataSource({
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Falg%>&IsStatutoryInternal=S',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Falg%>&IsStatutoryInternal=S"
                    },
                    schema: {
                        data: function (response) {
                            return response[0].locationList;
                        },
                        model: {
                            children: "Children"
                        }
                    }
                });
                dataSource12.read();
                $("#dropdowntree1").data("kendoDropDownTree").setDataSource(dataSource12);
            }

            if ($("#dropdownlistComplianceType1").val() == 1 || $("#dropdownlistComplianceType1").val() == 4)//event based and event based checklist
            {
                $('#dvdropdownEventNature1').css('display', 'block');
                $('#dvdropdownEventName1').css('display', 'block');

                $("#grid1").data("kendoGrid").showColumn(7);//Event Name
                $("#grid1").data("kendoGrid").showColumn(8);//Event Nature

                $("#grid1").data("kendoGrid").hideColumn(5);//Branch
                $("#grid1").data("kendoGrid").hideColumn(10);//ForMonth
            }
            else {
                $('#dvdropdownEventNature1').css('display', 'none');
                $('#dvdropdownEventName1').css('display', 'none');

                $("#grid1").data("kendoGrid").hideColumn(7);//Event Name
                $("#grid1").data("kendoGrid").hideColumn(8);//Event Nature

                $("#grid1").data("kendoGrid").showColumn(5);//Branch
                $("#grid1").data("kendoGrid").showColumn(10);//ForMonth
            }

            <%if (Falg == "AUD")
        {%>
            if ($("#dropdownFY").val() == "") {
                var dataSource = new kendo.data.DataSource({
                    transport: {
                        read: {
                            url: '<% =Path%>data/KendoMyDocument?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=<% =Falg%>&MonthId=<% =IsMonthID%>&StatusFlag=' + $("#dropdownlistComplianceType1").val() + '&FY=' + $("#dropdownFY").val() + '',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: '<% =Path%>data/KendoMyDocument?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=<% =Falg%>&MonthId=<% =IsMonthID%>&StatusFlag=' + $("#dropdownlistComplianceType1").val() + '&FY=' + $("#dropdownFY").val() + ''
                    },
                    schema: {
                        model: {
                            fields: {
                                ScheduledOn: { type: "date" },
                                ComplianceID: { type: "string" },
                            }
                        }
                    },
                    pageSize: 10,
                    filterable: true,
                });
                var grid = $('#grid1').data("kendoGrid");
                dataSource.read();
                grid.setDataSource(dataSource);
            }
            else {

                var dataSource = new kendo.data.DataSource({
                    transport: {
                        read: {
                            url: '<% =Path%>data/KendoMyDocument?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=<% =Falg%>&MonthId=<% =IsMonthID%>&StatusFlag=' + $("#dropdownlistComplianceType1").val() + '&FY=' + $("#dropdownFY").val() + '',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: '<% =Path%>data/KendoMyDocument?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=<% =Falg%>&MonthId=<% =IsMonthID%>&StatusFlag=' + $("#dropdownlistComplianceType1").val() + '&FY=' + $("#dropdownFY").val() + ''
                    },
                    schema: {
                        model: {
                            fields: {
                                ScheduledOn: { type: "date" },
                                ComplianceID: { type: "string" },
                            }
                        }
                    },
                    pageSize: 10,
                });
                var grid = $('#grid1').data("kendoGrid");
                dataSource.read();
                grid.setDataSource(dataSource);
            }
                    <%}%>
            <%else %>
            <%{%>                                          
            if ($("#dropdownFY").val() == "") {
                var dataSource = new kendo.data.DataSource({
                    transport: {
                        read: {
                            url: '<% =Path%>data/KendoMyDocument?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=<% =Falg%>&MonthId=' + $("#dropdownPastData").val() + '&StatusFlag=' + $("#dropdownlistComplianceType1").val() + '&FY=' + $("#dropdownFY").val() + '',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: '<% =Path%>data/KendoMyDocument?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=<% =Falg%>&MonthId=' + $("#dropdownPastData").val() + '&StatusFlag=' + $("#dropdownlistComplianceType1").val() + '&FY=' + $("#dropdownFY").val() + ''
                    },
                    schema: {
                        model: {
                            fields: {
                                ScheduledOn: { type: "date" },
                                ComplianceID: { type: "string" },
                            }
                        }
                    },
                    pageSize: 10,
                    filterable: true,
                });
                var grid = $('#grid1').data("kendoGrid");
                dataSource.read();
                grid.setDataSource(dataSource);
            }
            else {

                var dataSource = new kendo.data.DataSource({
                    transport: {
                        read: {
                            url: '<% =Path%>data/KendoMyDocument?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=<% =Falg%>&MonthId=All&StatusFlag=' + $("#dropdownlistComplianceType1").val() + '&FY=' + $("#dropdownFY").val() + '',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: '<% =Path%>data/KendoMyDocument?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=<% =Falg%>&MonthId=All&StatusFlag=' + $("#dropdownlistComplianceType1").val() + '&FY=' + $("#dropdownFY").val() + ''
                    },
                    schema: {
                        model: {
                            fields: {
                                ScheduledOn: { type: "date" },
                                ComplianceID: { type: "string" },
                            }
                        }
                    },
                    pageSize: 10,
                });
                var grid = $('#grid1').data("kendoGrid");
                dataSource.read();
                grid.setDataSource(dataSource);
            }
                    <%}%>

            if ($("#dropdownlistComplianceType1").val() == 0 || $("#dropdownlistComplianceType1").val() == 3) {
                var dataSourceSequence = new kendo.data.DataSource({
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/GetSequenceDetail?Flag=I&CustomerID=<% =CustId%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: '<% =Path%>Data/GetSequenceDetail?Flag=I&CustomerID=<% =CustId%>'
                    }
                });
                dataSourceSequence.read();
                  <%if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsLabelApplicable == 1)%><%{%>
                $("#dropdownSequence").data("kendoDropDownList").setDataSource(dataSourceSequence);
                  <%}%>
            }
            else {
                var dataSourceSequence = new kendo.data.DataSource({
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/GetSequenceDetail?Flag=S&CustomerID=<% =CustId%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: '<% =Path%>Data/GetSequenceDetail?Flag=S&CustomerID=<% =CustId%>'
                    }
                });
                dataSourceSequence.read();
                 <%if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsLabelApplicable == 1)%><%{%>
                $("#dropdownSequence").data("kendoDropDownList").setDataSource(dataSourceSequence);
                 <%}%>
            }
        }

        function selectedDocument(e) {

            if (($('input[name="sel_chkbx"]:checked').length) == 0) {

                return;
            }
            var checkboxlist = [];
            $('input[name="sel_chkbx"]').each(function (i, e) {
                if ($(e).is(':checked')) {
                    checkboxlist.push(e.value);
                }
            });
            console.log(checkboxlist.join(","));


            $('#downloadfile').attr('src', "../ComplianceDocument/DownloadDoc.aspx?ComplianceScheduleID=" + checkboxlist.join(",") + "&IsFlag=" + $("#dropdownlistComplianceType1").val());
            return false;
        }

        function selectedDocumentMain(e) {

            e.preventDefault();
            if (($('input[name="sel_chkbxMain"]:checked').length) == 0) {

                return;
            }
            var checkboxlist = [];
            $('input[name="sel_chkbxMain"]').each(function (i, e) {
                if ($(e).is(':checked')) {
                    checkboxlist.push(e.value);
                }
            });
            console.log(checkboxlist.join(","));



            $('#downloadfile').attr('src', "../ComplianceDocument/DownloadDoc.aspx?ComplianceScheduleID=" + checkboxlist.join(",") + "&IsFlag=" + $("#dropdownlistComplianceType").val());
            return false;
        }

        function FilterAllMain() {

            var locationsdetails = [];
            if ($("#dropdowntree").data("kendoDropDownTree") != undefined) {
                locationsdetails = $("#dropdowntree").data("kendoDropDownTree")._values;
            }

            //Status details
            var Statusdetails = [];
            if ($("#dropdownlistStatus").data("kendoDropDownTree") != undefined) {
                Statusdetails = $("#dropdownlistStatus").data("kendoDropDownTree")._values;
            }

            //risk Details
            var Riskdetails = [];
            if ($("#dropdownlistRisk").data("kendoDropDownTree") != undefined) {
                Riskdetails = $("#dropdownlistRisk").data("kendoDropDownTree")._values;
            }

            var finalSelectedfilter = { logic: "and", filters: [] };

            if ($("#dropdowntree").data("kendoDropDownTree")._values.length > 0
                || $("#dropdownlistStatus").data("kendoDropDownTree")._values.length > 0
                || $("#dropdownlistRisk").data("kendoDropDownTree")._values.length > 0
                || $("#txtSearchComplianceID").val() != ""
                || ($("#dropdownEventName").val() != 0 && $("#dropdownEventName").val() != -1 && $("#dropdownEventName").val() != "")
                || ($("#dropdownEventNature").val() != 0 && $("#dropdownEventNature").val() != -1 && $("#dropdownEventNature").val() != "")) {
                if (locationsdetails.length > 0) {
                    var LocationFilter = { logic: "or", filters: [] };

                    $.each(locationsdetails, function (i, v) {
                        LocationFilter.filters.push({
                            field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                        });
                    });

                    finalSelectedfilter.filters.push(LocationFilter);
                }
                if (Statusdetails.length > 0) {
                    var StatusFilter = { logic: "or", filters: [] };

                    $.each(Statusdetails, function (i, v) {
                        StatusFilter.filters.push({
                            field: "Status", operator: "eq", value: v
                        });
                    });

                    finalSelectedfilter.filters.push(StatusFilter);
                }
                if (Riskdetails.length > 0) {
                    var RiskFilter = { logic: "or", filters: [] };
                    $.each(Riskdetails, function (i, v) {
                        RiskFilter.filters.push({
                            field: "Risk", operator: "eq", value: parseInt(v)
                        });
                    });
                    finalSelectedfilter.filters.push(RiskFilter);
                }

                if (($("#dropdownEventName").val() != 0 && $("#dropdownEventName").val() != -1 && $("#dropdownEventName").val() != "")) {
                    var EYFilter = { logic: "or", filters: [] };

                    EYFilter.filters.push({
                        field: "EventID", operator: "eq", value: parseInt($("#dropdownEventName").val())
                    });

                    finalSelectedfilter.filters.push(EYFilter);
                }
                if (($("#dropdownEventNature").val() != 0 && $("#dropdownEventNature").val() != -1 && $("#dropdownEventNature").val() != "")) {
                    var EYFilter = { logic: "or", filters: [] };

                    EYFilter.filters.push({
                        field: "EventScheduleOnID", operator: "eq", value: parseInt($("#dropdownEventNature").val())
                    });

                    finalSelectedfilter.filters.push(EYFilter);
                }
                if ($("#txtSearchComplianceID").val() != "") {
                    var RiskFilter = { logic: "or", filters: [] };
                    RiskFilter.filters.push({
                        field: "ComplianceID", operator: "contains", value: $("#txtSearchComplianceID").val()
                    });
                    finalSelectedfilter.filters.push(RiskFilter);
                }
                if (finalSelectedfilter.filters.length > 0) {
                    var dataSource = $("#grid").data("kendoGrid").dataSource;
                    dataSource.filter(finalSelectedfilter);
                }
                else {
                    $("#grid").data("kendoGrid").dataSource.filter({});
                }
            }
            else {
                $("#grid").data("kendoGrid").dataSource.filter({});
            }

        }

        function FilterAllAdvancedSearch() {

            //location details
            var locationsdetails = [];
            if ($("#dropdowntree1").data("kendoDropDownTree") != undefined) {
                locationsdetails = $("#dropdowntree1").data("kendoDropDownTree")._values;
            }

            //Status details
            var Statusdetails = [];
            if ($("#dropdownlistStatus1").data("kendoDropDownTree") != undefined) {
                Statusdetails = $("#dropdownlistStatus1").data("kendoDropDownTree")._values;
            }

            //risk Details
            var Riskdetails = [];
            if ($("#dropdownlistRisk1").data("kendoDropDownTree") != undefined) {
                Riskdetails = $("#dropdownlistRisk1").data("kendoDropDownTree")._values;
            }


            //user Details 
            var userdetails = [];
                  <%if (RoleFlag == 1)%>
            <%{%>
            if ($("#dropdownUser").data("kendoDropDownTree") != undefined) {
                userdetails = $("#dropdownUser").data("kendoDropDownTree")._values;
            }
                <%}%>

            //datefilter
            var datedetails = [];
            if ($("#Startdatepicker").val() != null && $("#Startdatepicker").val() != "") {
                datedetails.push({
                    field: "ScheduledOn", operator: "gte", value: kendo.parseDate($("#Startdatepicker").val(), 'MM/dd/yyyy')
                });
            }
            if ($("#Lastdatepicker").val() != null && $("#Lastdatepicker").val() != "") {
                datedetails.push({
                    field: "ScheduledOn", operator: "lte", value: kendo.parseDate($("#Lastdatepicker").val(), 'MM/dd/yyyy')
                });
            }

            var finalSelectedfilter = { logic: "and", filters: [] };
            //six
            if (locationsdetails.length > 0
                || Statusdetails.length > 0
                || Riskdetails.length > 0
                || userdetails.length > 0
                || ($("#dropdownACT").val() != "" && $("#dropdownACT").val() != null && $("#dropdownACT").val() != undefined)
                || ($("#dropdownSequence").val() != undefined && $("#dropdownSequence").val() != null && $("#dropdownSequence").val() != "")
                || datedetails.length > 0
                || $("#txtSearchComplianceID1").val() != ""
                || ($("#dropdownEventName1").val() != 0 && $("#dropdownEventName1").val() != -1 && $("#dropdownEventName1").val() != "")
                || ($("#dropdownEventNature1").val() != 0 && $("#dropdownEventNature1").val() != -1 && $("#dropdownEventNature1").val() != "")) {
                if ($("#dropdownSequence").val() != undefined && $("#dropdownSequence").val() != null && $("#dropdownSequence").val() != "") {
                    var SeqFilter = { logic: "or", filters: [] };
                    SeqFilter.filters.push({
                        field: "SequenceID", operator: "eq", value: $("#dropdownSequence").val()
                    });
                    finalSelectedfilter.filters.push(SeqFilter);
                }
                if (locationsdetails.length > 0) {
                    var LocationFilter = { logic: "or", filters: [] };

                    $.each(locationsdetails, function (i, v) {
                        LocationFilter.filters.push({
                            field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                        });
                    });

                    finalSelectedfilter.filters.push(LocationFilter);
                }
                if (Statusdetails.length > 0) {
                    var StatusFilter = { logic: "or", filters: [] };

                    $.each(Statusdetails, function (i, v) {
                        StatusFilter.filters.push({
                            field: "Status", operator: "eq", value: v
                        });
                    });

                    finalSelectedfilter.filters.push(StatusFilter);
                }
                if (Riskdetails.length > 0) {
                    var RiskFilter = { logic: "or", filters: [] };
                    $.each(Riskdetails, function (i, v) {
                        RiskFilter.filters.push({
                            field: "Risk", operator: "eq", value: parseInt(v)
                        });
                    });
                    finalSelectedfilter.filters.push(RiskFilter);
                }
                if (userdetails.length > 0) {
                    var UserFilter = { logic: "or", filters: [] };

                    $.each(userdetails, function (i, v) {
                        UserFilter.filters.push({
                            field: "PerformerID", operator: "eq", value: parseInt(v)
                        });
                    });

                    $.each(userdetails, function (i, v) {
                        UserFilter.filters.push({
                            field: "ReviewerID", operator: "eq", value: parseInt(v)
                        });
                    });
                    $.each(userdetails, function (i, v) {
                        UserFilter.filters.push({
                            field: "ApproverID", operator: "eq", value: parseInt(v)
                        });
                    });

                    finalSelectedfilter.filters.push(UserFilter);
                }
                if ($("#dropdownACT").val() != "" && $("#dropdownACT").val() != null && $("#dropdownACT").val() != undefined) {
                    var ActFilter = { logic: "or", filters: [] };
                    ActFilter.filters.push({
                        field: "ActID", operator: "eq", value: parseInt($("#dropdownACT").val())
                    });
                    finalSelectedfilter.filters.push(ActFilter);
                }
                if ($("#Startdatepicker").val() != null && $("#Startdatepicker").val() != "") {

                    var DateFilter = { logic: "or", filters: [] };

                    if ($("#Startdatepicker").val() != null && $("#Startdatepicker").val() != "") {
                        DateFilter.filters.push({
                            field: "ScheduledOn", operator: "gte", value: kendo.parseDate($("#Startdatepicker").val(), 'dd-MM-yyyy')
                        });
                    }
                    finalSelectedfilter.filters.push(DateFilter);
                }
                if ($("#Lastdatepicker").val() != null && $("#Lastdatepicker").val() != "") {

                    var DateFilter = { logic: "or", filters: [] };

                    if ($("#Lastdatepicker").val() != null && $("#Lastdatepicker").val() != "") {
                        DateFilter.filters.push({
                            field: "ScheduledOn", operator: "lte", value: kendo.parseDate($("#Lastdatepicker").val(), 'dd-MM-yyyy')
                        });
                    }
                    finalSelectedfilter.filters.push(DateFilter);
                }
                if ($("#txtSearchComplianceID1").val() != "") {
                    var RiskFilter = { logic: "or", filters: [] };
                    RiskFilter.filters.push({
                        field: "ComplianceID", operator: "contains", value: $("#txtSearchComplianceID1").val()
                    });
                    finalSelectedfilter.filters.push(RiskFilter);
                }
                if (($("#dropdownEventName1").val() != 0 && $("#dropdownEventName1").val() != -1 && $("#dropdownEventName1").val() != "")) {
                    var EYFilter = { logic: "or", filters: [] };

                    EYFilter.filters.push({
                        field: "EventID", operator: "eq", value: parseInt($("#dropdownEventName1").val())
                    });

                    finalSelectedfilter.filters.push(EYFilter);
                }
                if (($("#dropdownEventNature1").val() != 0 && $("#dropdownEventNature1").val() != -1 && $("#dropdownEventNature1").val() != "")) {
                    var EYFilter = { logic: "or", filters: [] };

                    EYFilter.filters.push({
                        field: "EventScheduleOnID", operator: "eq", value: parseInt($("#dropdownEventNature1").val())
                    });

                    finalSelectedfilter.filters.push(EYFilter);
                }
                if (finalSelectedfilter.filters.length > 0) {
                    var dataSource = $("#grid1").data("kendoGrid").dataSource;
                    dataSource.filter(finalSelectedfilter);
                }
                else {
                    $("#grid1").data("kendoGrid").dataSource.filter({});
                }
            }
            else {
                $("#grid1").data("kendoGrid").dataSource.filter({});
            }
        }

        function ClearAllFilterMain(e) {
            $("#dropdowntree").data("kendoDropDownTree").value([]);
            $("#dropdownlistRisk").data("kendoDropDownTree").value([]);
            $("#dropdownlistStatus").data("kendoDropDownTree").value([]);
            $('#dvbtndownloadDocumentMain').css('display', 'none');
            $("#dropdownEventName").data("kendoDropDownList").select(0);
            $("#dropdownEventNature").data("kendoDropDownList").select(0);
            $("#txtSearchComplianceID").val('');
            $("#dropdownlistTypePastdata").data("kendoDropDownList").select(1);
            $("#grid").data("kendoGrid").dataSource.filter({});
            $('input[id=chkAllMain]').prop('checked', false);
            e.preventDefault();
        }

        function ClearAllFilter(e) {

            $("#dropdownEventName1").data("kendoDropDownList").select(0);
            $("#dropdownEventNature1").data("kendoDropDownList").select(0);
            $("#dropdownACT").data("kendoDropDownList").select(0);
            $("#dropdowntree1").data("kendoDropDownTree").value([]);
            $("#dropdownlistRisk1").data("kendoDropDownTree").value([]);
            <%if (RoleFlag == 1){%>
            $("#dropdownUser").data("kendoDropDownTree").value([]);
            <%}%>
            $("#dropdownlistStatus1").data("kendoDropDownTree").value([]);
            $("#Startdatepicker").data("kendoDatePicker").value(null);
            $("#Lastdatepicker").data("kendoDatePicker").value(null);
            $("#txtSearchComplianceID1").val('');
            $('#dvbtndownloadDocument').css('display', 'none');
            $("#dropdownPastData").data("kendoDropDownList").select(1);
            $("#dropdownFY").data("kendoDropDownList").select(0);
            $("#grid1").data("kendoGrid").dataSource.filter({});
            $('input[id=chkAll]').prop('checked', false);
            e.preventDefault();
        }

        function fcloseStory(obj) {

            var DataId = $(obj).attr('data-Id');
            var dataKId = $(obj).attr('data-K-Id');
            var seq = $(obj).attr('data-seq');
            var deepspan = $('#' + DataId + ' > li > span > span.k-i-close')[seq];
            $(deepspan).trigger('click');
            var upperli = $('#' + dataKId);
            $(upperli).remove();

            fCreateStoryBoard('dropdownlistRisk', 'filterrisk', 'risk');
            fCreateStoryBoard('dropdownlistStatus', 'filterstatus', 'status')
            fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc')
            fCreateStoryBoard('dropdownComplianceSubType', 'filterCompSubType', 'CompSubType');
            fCreateStoryBoard('dropdowntree1', 'filtersstoryboard1', 'loc1');
            fCreateStoryBoard('dropdownlistRisk1', 'filterrisk1', 'risk1');
            fCreateStoryBoard('dropdownUser', 'filterUser', 'user');
            fCreateStoryBoard('dropdownlistStatus1', 'filterstatus1', 'status1');
        };

        function fCreateStoryBoard(Id, div, filtername) {

            var IdKReset = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('id');
            $('#' + div).html('');
            $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');
            $('#' + div).css('display', 'block');

            if (div == 'filtersstoryboard') {
                $('#' + div).append('Location&nbsp;&nbsp;&nbsp;:');//Dashboard
            }
            if (div == 'filtersstoryboard1') {
                $('#' + div).append('Location&nbsp;&nbsp;&nbsp;:');//Dashboard
            }
            else if (div == 'filterrisk') {
                $('#' + div).append('Risk&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');//Dashboard
            }
            else if (div == 'filterrisk1') {
                $('#' + div).append('Risk&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');//Dashboard
            }
            else if (div == 'filterstatus') {
                $('#' + div).append('Status&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');//Dashboard
            }
            else if (div == 'filterstatus1') {
                $('#' + div).append('Status&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');//Dashboard
            }
            else if (div == 'filterCompSubType') {
                $('#' + div).append('SubType&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
            }
            else if (div == 'filterUser') {
                $('#' + div).append('User&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
            }

            for (var i = 0; i < $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length; i++) {
                var button = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button')[i];
                $(button).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('placeholder', 'Shree');
                var buttontest = $($(button).find('span')[0]).text();
                //if (buttontest.length > 10) {
                //    buttontest = buttontest.substring(0, 10).concat("...");
                //}
                $('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="background-color:#EBEBEB;height: 20px;Color:Gray;border-radius:10px;margin-left:5px;margin-bottom: 4px;"><span unselectable="on" title="' + $($(button).find('span')[0]).text() + '">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory(this)" title="Clear" aria-label="Clear" class="k-select" style="padding-left: 6px;"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory(this)"  class="k-icon k-i-close" title="Clear" aria-label="Clear" style="font-size: 12px;"></span></span></li>');
            }

            if ($($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) {
                $('#' + div).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');

            }

        }

        function OpenAdvanceSearch(e) {
     
            $("#divAdvanceSearchModel").kendoWindow({
                width: "98%",
                height: "97%",
                modal: true,
                title: "Advanced Search",
                visible: false,
                pinned: true,
                actions: [
                    "Close"
                ], close: ClearFilters
            }).data("kendoWindow").center().open();
            e.preventDefault();
        }

        function ClearFilters() {
            $('#dropdowntree1').data('kendoDropDownTree').value([]);
            $('#dropdownlistStatus1').data('kendoDropDownTree').value([]);
            $('#dropdownlistRisk1').data('kendoDropDownTree').value([]);
            $("#dropdownEventName1").data("kendoDropDownList").select(0);
            $("#dropdownEventNature1").data("kendoDropDownList").select(0);
            $("#dropdownACT").data("kendoDropDownList").select(0);
            <%if (RoleFlag == 1)%>
            <%{%>
            $('#dropdownUser').data('kendoDropDownTree').value([]);
            <%}%>
            $("#Startdatepicker").data("kendoDatePicker").value(null);
            $("#Lastdatepicker").data("kendoDatePicker").value(null);
            $('#filterStartDate').html('');
            $('#filterLastDate').html('');
            $("#txtSearchComplianceID1").val('');
            $('#filterStartDate').css('display', 'none');
            $('#filterLastDate').css('display', 'none');
            $('#dvbtndownloadDocument').css('display', 'none');

            $("#grid1").data("kendoGrid").dataSource.filter({});
        }

        function ChangeView() {
            $('#grid1').css('display', 'block');

            $("#grid1").data("kendoGrid").showColumn(0);//Id
            $("#grid1").data("kendoGrid").hideColumn(1);//Risk
            $("#grid1").data("kendoGrid").hideColumn(2);//CId
            $("#grid1").data("kendoGrid").hideColumn(3);//FileName
            $("#grid1").data("kendoGrid").hideColumn(4);//V
            $("#grid1").data("kendoGrid").showColumn(5);//Branch
            $("#grid1").data("kendoGrid").showColumn(6);//SD
            $("#grid1").data("kendoGrid").hideColumn(7);//Event Name
            $("#grid1").data("kendoGrid").hideColumn(8);//Event Nature
            $("#grid1").data("kendoGrid").showColumn(9);//Scheduleon
            $("#grid1").data("kendoGrid").showColumn(10);//ForMonth
            $("#grid1").data("kendoGrid").showColumn(11);//Status
            $("#grid1").data("kendoGrid").hideColumn(12);//VD
            $("#grid1").data("kendoGrid").hideColumn(13);//type
            $("#grid1").data("kendoGrid").hideColumn(14);//Uploaded Date
            $("#grid1").data("kendoGrid").hideColumn(15);//Size

            $("#grid1").data("kendoGrid").hideColumn(16);//Size

            if ($("#dropdownlistComplianceType1").val() == 1)//event based
            {
                $("#grid1").data("kendoGrid").showColumn(7);//Event Name
                $("#grid1").data("kendoGrid").showColumn(8);//Event Nature

                $("#grid1").data("kendoGrid").hideColumn(5);//Branch
                $("#grid1").data("kendoGrid").hideColumn(10);//ForMonth
            }

        }

        function ChangeListView() {
            $('#grid1').css('display', 'block');
            //  $('#grid2').css('display', 'none');
            $("#grid1").data("kendoGrid").showColumn(0);//Id
            $("#grid1").data("kendoGrid").hideColumn(1);//Risk
            $("#grid1").data("kendoGrid").hideColumn(2);//CId
            $("#grid1").data("kendoGrid").showColumn(3);//FileName
            $("#grid1").data("kendoGrid").showColumn(4);//V
            $("#grid1").data("kendoGrid").hideColumn(5);//Branch
            $("#grid1").data("kendoGrid").hideColumn(6);//SD
            $("#grid1").data("kendoGrid").hideColumn(7);//Scheduleon
            $("#grid1").data("kendoGrid").hideColumn(8);//ForMonth
            $("#grid1").data("kendoGrid").hideColumn(9);//Status
            $("#grid1").data("kendoGrid").showColumn(10);//VD
            $("#grid1").data("kendoGrid").hideColumn(11);//type
            $("#grid1").data("kendoGrid").showColumn(12);//Uploaded Date
            $("#grid1").data("kendoGrid").showColumn(13);//Size
            $("#grid1").data("kendoGrid").hideColumn(15);//Size

            $("#grid1").data("kendoGrid").hideColumn(7);//Event Name
            $("#grid1").data("kendoGrid").hideColumn(8);//Event Nature
        }

        function ChangeAuditQView() {
            $('#grid1').css('display', 'none');
            //  $('#grid2').css('display', 'block');            
        }

        function exportReport() {
            $("#grid").getKendoGrid().saveAsExcel();
            return false;
        };

        function OpenOverViewpup(scheduledonid, instanceid) {
            settracknew('Compliance Document', 'Action', 'Overview', '');
            $('#divOverView1').modal('show');
            $('#OverViews1').attr('width', '1250px');
            $('#OverViews1').attr('height', '600px');
            $('.modal-dialog').css('width', '1306px');

            if ($("#dropdownlistComplianceType").val() == 0 || $("#dropdownlistComplianceType").val() == 3) {

                $('#OverViews1').attr('src', "../Common/ComplianceOverviewInternal.aspx?ComplianceScheduleID=" + scheduledonid + "&ComplainceInstatnceID=" + instanceid);
            }
            else {
                $('#OverViews1').attr('src', "../Common/ComplianceOverview.aspx?ComplianceScheduleID=" + scheduledonid + "&ComplainceInstatnceID=" + instanceid);
            }
        }

        function OpenDocumentOverviewpup(scheduledonid, transactionid) {
            settracknew('Compliance Document', 'Action', 'View', '')
            $('#divOverView').modal('show');
            $('#OverViews').attr('width', '1150px');
            $('#OverViews').attr('height', '600px');
            $('.modal-dialog').css('width', '1200px');
            $('#OverViews').attr('src', "../Common/DocumentOverview.aspx?ComplianceScheduleID=" + scheduledonid + "&ComplainceTransactionID=" + transactionid + "&ISStatutoryflag=" + $("#dropdownlistComplianceType").val());
        }

        function OpenOverViewpupMain(scheduledonid, instanceid) {
            settracknew('Compliance Document', 'Action', 'Overview', '');
            $('#divOverViewMain1').modal('show');
            $('#OverViewsMain1').attr('width', '1250px');
            $('#OverViews1Main1').attr('height', '600px');
            $('.modal-dialog').css('width', '1306px');

            if ($("#dropdownlistComplianceType").val() == 0 || $("#dropdownlistComplianceType").val() == 3) {

                $('#OverViewsMain1').attr('src', "../Common/ComplianceOverviewInternal.aspx?ComplianceScheduleID=" + scheduledonid + "&ComplainceInstatnceID=" + instanceid);
            }
            else {
                $('#OverViewsMain1').attr('src', "../Common/ComplianceOverview.aspx?ComplianceScheduleID=" + scheduledonid + "&ComplainceInstatnceID=" + instanceid);
            }
        }

        function OpenDocumentOverviewpupMain(scheduledonid, transactionid) {
            settracknew('Compliance Document', 'Action', 'View', '')
            $('#divOverViewMain').modal('show');
            $('#OverViewsMain').attr('width', '1150px');
            $('#OverViewsMain').attr('height', '600px');
            $('.modal-dialog').css('width', '1200px');
            $('#OverViewsMain').attr('src', "../Common/DocumentOverview.aspx?ComplianceScheduleID=" + scheduledonid + "&ComplainceTransactionID=" + transactionid + "&ISStatutoryflag=" + $("#dropdownlistComplianceType").val());
        }

        function OpenDownloadOverviewpup(scheduledonid, transactionid) {

            //done
            settracknew('Compliance Document', 'Action', 'Download', '')
            $('#divDownloadView').modal('show');
            $('#DownloadViews').attr('width', '401px');
            //$('#DownloadViews').attr('height', 'auto');
            $('#DownloadViews').attr('height', '200px');
            $('.modal-dialog').css('width', '466px');
            //$('.modal-dialog').css('height', '207px');           
            $('#DownloadViews').attr('src', "../Common/DownloadOverview.aspx?ComplianceScheduleID=" + scheduledonid + "&ComplainceTransactionID=" + transactionid + "&ISStatutoryflag=" + $("#dropdownlistComplianceType").val());

        }

        function OpenDownloadOverviewpupAdvanceSearch(scheduledonid, transactionid) {

            //done
            settracknew('Compliance Document', 'Action', 'Download', '')
            $('#divDownloadView1').modal('show');
            $('#DownloadViews1').attr('width', '401px');
            $('#DownloadViews1').attr('height', '200px');
            $('.modal-dialog').css('width', '466px');
            $('#DownloadViews1').attr('src', "../Common/DownloadOverview.aspx?ComplianceScheduleID=" + scheduledonid + "&ComplainceTransactionID=" + transactionid + "&ISStatutoryflag=" + $("#dropdownlistComplianceType").val());
        }

        $("#newModelClose").on("click", function () {
            myWindow3.close();
        });

        function CloseClearPopup() {
            $('#OverViews1').attr('src', "../Common/blank.html");
        }

        function CloseClearOV() {
            $('#OverViews').attr('src', "../Common/blank.html");
        }

        function CloseClearDV() {
            $('#DownloadViews').attr('src', "../Common/blank.html");
        }
        function CloseClearDVAS() {
            $('#DownloadViews1').attr('src', "../Common/blank.html");
        }

        function CloseClearPopupMain() {
            $('#OverViewsMain').attr('src', "../Common/blank.html");
        }

        function CloseClearOVMain() {
            $('#OverViewsMain1').attr('src', "../Common/blank.html");
        }

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>

        <div class="row" style="padding-bottom: 4px; margin-left: 1%">
            <input id="dropdowntree" placeholder="Entity/Sub-Entity/Location" style="width: 25%; margin-right: 8px;" />
            <input id="dropdownlistComplianceType" placeholder="Type" style="width: 15%; margin-right: 8px;" />
            <input id="dropdownlistRisk" placeholder="Risk" style="width: 10%; margin-right: 8px;" />
            <input id="dropdownlistStatus" placeholder="Status" style="width: 16%; margin-right: 8px;" />
            <input id="dropdownlistTypePastdata" placeholder="Status" style="width: 14.6%;margin-right: 8px;" />
            <input id="txtSearchComplianceID" class="k-textbox" onkeydown="return (event.keyCode!=13);" placeholder="Compliance ID" style="width: 12%;" />
        </div>
        <div class="row" style="padding-bottom: 10px; margin-left: 1%; margin-top: 5px;">
            <button type="button" id="ClearfilterMain" style="float: right; margin-right: 2.1%; height: 30px;" onclick="ClearAllFilterMain(event)"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear</button>
            <button type="button" id="AdavanceSearch" style="height: 30px; float: right; margin-right: 12px" onclick="OpenAdvanceSearch(event)"><span class="k-icon k-i-filter" onclick="javascript:return false;"></span>Advanced Search</button>
            <button id="dvbtndownloadDocumentMain" style="margin-right: 1%;height: 30px;float: right;display: none;" onclick="selectedDocumentMain(event)">Download</button>
       
            <div class="col-md-2" style="width: 26.4%;padding-left: 0%;">
                <div id="dvdropdownEventName" style="display: none;">
                    <input id="dropdownEventName" placeholder="Event Name" style="width: 100%;" />
                </div>
            </div>
            <div class="col-md-2" style="width: 16.3%;padding-left: 0px;margin-left: -3px;">
                <div id="dvdropdownEventNature" style="display: none;">
                    <input id="dropdownEventNature" placeholder="Event Nature" style="width: 100%;" />
                </div>
            </div>
        </div>


        <div class="row" style="display: none; margin-left: 1%; padding-bottom: 4px; font-size: 12px; font-weight: bold; color: black;" id="filtersstoryboard">&nbsp;</div>
        <div class="row" style="display: none; margin-left: 1%; padding-bottom: 4px; font-size: 12px; font-weight: bold; color: black;" id="filtertype">&nbsp;</div>
        <div class="row" style="display: none; margin-left: 1%; padding-bottom: 4px; font-size: 12px; font-weight: bold; color: black;" id="filterrisk">&nbsp;</div>
        <div class="row" style="display: none; margin-left: 1%; padding-bottom: 4px; font-size: 12px; font-weight: bold; color: black;" id="filterstatus">&nbsp;</div>
        <div id="grid" style="margin-left: 10px; margin-right: 23px; margin-bottom: 2%;"></div>
    </div>
    <div>
        <div class="modal fade" id="divDownloadView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
            <div class="modal-dialog" style="width: 360px;">
                <div class="modal-content" style="width: 100%; height: 100%">
                    <div class="modal-header" style="border-bottom: none;">
                        <button type="button" class="close" data-dismiss="modal" onclick="CloseClearDV();" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">
                        <iframe id="DownloadViews" src="about:blank" width="300px" height="350px" frameborder="0"></iframe>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="divOverViewMain" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
            <div class="modal-dialog" style="width: 1150px; z-index: 9999">
                <div class="modal-content" style="width: 100%;">
                    <div class="modal-header" style="border-bottom: none;">
                        <button type="button" class="close" data-dismiss="modal" onclick="CloseClearPopupMain();" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">
                        <iframe id="OverViewsMain" src="about:blank" width="1200px" height="100%" frameborder="0"></iframe>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="divOverViewMain1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
            <div class="modal-dialog" style="width: 1150px;">
                <div class="modal-content" style="width: 100%; height: 600px">
                    <div class="modal-header" style="border-bottom: none;">
                        <button type="button" class="close" data-dismiss="modal" onclick="CloseClearOVMain();" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body" style="height: 550px">
                        <iframe id="OverViewsMain1" src="about:blank" width="1200px" height="100%" frameborder="0"></iframe>
                    </div>
                </div>
            </div>
        </div>

        <div id="divAdvanceSearchModel" style="display: none">

            <div class="row">
                <div class="col-md-10" style="padding-left: 0px; padding-bottom: 5px;">
                    <button id="primaryTextButton1" onclick="ChangeView()">Grid View</button>
                    <button id="primaryTextButton" onclick="ChangeListView()">List View</button>
                    <%-- <button id="primaryTextButton2" onclick="ChangeAuditQView()">IKEA Audit</button>--%>
                </div>
            </div>

            <div class="row" style="margin-left: -9px; padding-bottom: 5px; width: 99%;">
                <div class="col-md-12 colpadding0">
                    <div class="col-md-2" id="dvdropdowntree1" style="width: 20%; padding-left: 9px;">
                        <input id="dropdowntree1" data-placeholder="Entity/Sub-Entity/Location" style="width: 100%;margin-right:8px;" />
                    </div>
                    <div class="col-md-2" id="dvdropdownFY" style="width: 11%; padding-left: 0px;">
                        <input id="dropdownFY" data-placeholder="Finance Year" style="width: 102%;" />
                    </div>
                    <div class="col-md-2" id="dvdropdownlistStatus1" style="width: 11%; padding-left: 4px;">
                        <input id="dropdownlistStatus1" data-placeholder="Status" style="width: 100%;" />
                    </div>

                    <div class="col-md-2" id="dvdropdownlistRisk1" style="width: 10%; padding-left: 0px;">
                        <input id="dropdownlistRisk1" data-placeholder="Risk" style="width: 100%;" />
                    </div>
                    <div class="col-md-2" id="dvStartdatepicker" style="width: 10%; padding-left: 0px;">
                        <input id="Startdatepicker" placeholder="Start Date" cssclass="clsROWgrid" title="startdatepicker" style="width: 100%;" />
                    </div>
                    <div class="col-md-2" id="dvLastdatepicker" style="width: 10%; padding-left: 0px;">
                        <input id="Lastdatepicker" placeholder="End Date" title="enddatepicker" style="width: 100%;" />
                    </div>
                    <div class="col-md-2" id="dvdropdownACT" style="width: 18%; padding-left: 0px;">
                        <input id="dropdownACT" placeholder="Act" style="width: 114%;" />
                    </div>
                </div>
            </div>

            <div class="row" style="margin-left: -9px;width: 99%;">
                <div class="col-md-12 colpadding0">
                    <div class="col-md-2" id="dvdropdownComplianceSubType" style="width: 20%; padding-left: 9px;">
                        <input id="dropdownPastData" style="width: 100%;" />
                    </div>
                
                    <div class="col-md-2" id="dvdropdownlistComplianceType1" style="width: 11%; padding-left: 0px;">
                        <input id="dropdownlistComplianceType1" data-placeholder="Type" style="width: 100%;" />
                    </div>
                    <div class="col-md-2" style="width: 11%; padding-left: 4px;">
                        <input id="SearchTag" type="text" style="width: 100%;" class="k-textbox" placeholder="Document Tag" />
                    </div>
                       <div class="col-md-2" style="width: 10%; padding-left: 0px;">
                        <input id="txtSearchComplianceID1" class="k-textbox" placeholder="Compliance ID" style="width: 100%;" />
                    </div>
                    <div class="col-md-2" style="width: 10%; padding-left: 0px;" id="dvdropdownUser">
                        <%if (RoleFlag == 1)%>
                        <%{%>
                        <input id="dropdownUser" placeholder="User" style="width: 100%;" />
                        <%}%>
                    </div>
                      <div class="col-md-2" style="width: 16.5%;padding-left: 0px;">
                        <%if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsLabelApplicable == 1)%><%{%>
                        <input id="dropdownSequence" style="width: 100%;" />
                        <%}%>
                    </div>
                     <div class="col-md-2" style="width: 11.5%;padding-left: 0px;float:right;">
                        <button type="button" id="Clearfilter" style="height: 24px;" onclick="ClearAllFilter(event)"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear</button>
                       <button id="dvbtndownloadDocument" style="height: 24px; display: none;float:right;" onclick="selectedDocument(event)">Download</button>
                    </div>
                </div>
            </div>

            <div class="row" style="padding-bottom: 5px;padding-top: 5px;  width: 99%;">
                <div class="col-md-12 colpadding0">
                        <div class="col-md-2" style="width: 19.9%;padding-left: 1px;">
                        <div id="dvdropdownEventName1" style="display: none;">
                            <input id="dropdownEventName1" data-placeholder="Event Name" style="width: 100%;" />
                        </div>
                    </div>
                    <div class="col-md-2" style="width: 23.5%;padding-left: 0px;">
                        <div id="dvdropdownEventNature1" style="display: none;">
                            <input id="dropdownEventNature1" data-placeholder="Event Nature" style="width: 100%;" />
                        </div>
                    </div>
                </div>
            </div>

            <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a; font-weight: bold;" id="filterCompType">&nbsp;</div>
            <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a; font-weight: bold;" id="filterCategory">&nbsp;</div>
            <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a; font-weight: bold;" id="filterAct">&nbsp;</div>
            <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a; font-weight: bold;" id="filterCompSubType">&nbsp;</div>
            <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a; font-weight: bold;" id="filterStartDate">&nbsp;</div>
            <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a; font-weight: bold;" id="filterLastDate">&nbsp;</div>
            <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a; font-weight: bold;" id="filtersstoryboard1">&nbsp;</div>
            <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a; font-weight: bold;" id="filtertype1">&nbsp;</div>
            <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a; font-weight: bold;" id="filterrisk1">&nbsp;</div>

            <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a; font-weight: bold;" id="filterpstData1">&nbsp;</div>
            <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a; font-weight: bold;" id="filterUser">&nbsp;</div>
            <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a; font-weight: bold;" id="filterFY">&nbsp;</div>
            <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a; font-weight: bold;" id="filterstatus1">&nbsp;</div>


            <div id="grid1" style="width: 98.6%;"></div>
            <div class="modal fade" id="divOverView1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                <div class="modal-dialog" style="width: 1150px; z-index: 9999">
                    <div class="modal-content" style="width: 100%;">
                        <div class="modal-header" style="border-bottom: none;">
                            <button type="button" class="close" data-dismiss="modal" onclick="CloseClearPopup();" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body">
                            <iframe id="OverViews1" src="about:blank" width="1200px" height="100%" frameborder="0"></iframe>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="divOverView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                <div class="modal-dialog" style="width: 1150px;">
                    <div class="modal-content" style="width: 100%;">
                        <div class="modal-header" style="border-bottom: none;">
                            <button type="button" class="close" data-dismiss="modal" onclick="CloseClearOV();" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body">
                            <iframe id="OverViews" src="about:blank" width="1200px" height="100%" frameborder="0"></iframe>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="divDownloadView1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                <div class="modal-dialog" style="width: 360px;">
                    <div class="modal-content" style="width: 100%; height: 100%">
                        <div class="modal-header" style="border-bottom: none;">
                            <button type="button" class="close" data-dismiss="modal" onclick="CloseClearDVAS();" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body">
                            <iframe id="DownloadViews1" src="about:blank" width="300px" height="350px" frameborder="0"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <iframe id="downloadfile" src="about:blank" width="0" height="0"></iframe>
    </div>

</asp:Content>
