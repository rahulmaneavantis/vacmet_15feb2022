﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.ComplianceDocument
{
    public partial class HRDownloadDoc : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString["ComplianceScheduleID"]))
                {
                    string[] commandArg = Request.QueryString["ComplianceScheduleID"].ToString().Split(',');

                    string[] Statutory = Request.QueryString["IsFlag"].ToString().Split(',');

                    using (ZipFile ComplianceZip = new ZipFile())
                    {
                        foreach (var gvrow in commandArg)
                        {
                            string[] parameters= gvrow.Split('/');
                            
                            int IsStatutory = Convert.ToInt32(parameters[1]);                            
                            int ScheduledOnID = Convert.ToInt32(parameters[0]); //295386;// Convert.ToInt32(gvrow);

                            if (ScheduledOnID != 0)
                            {
                                if (IsStatutory == -1 || IsStatutory == 1) //Stautory 
                                {
                                    var ComplianceData = DocumentManagement.GetForMonth(ScheduledOnID);
                                    List<GetComplianceDocumentsView> fileData = DocumentManagement.GetFileData1(ScheduledOnID);

                                    //craeted subdirectory
                                    //string directoryName = fileData[0].ID + "_" + fileData[0].Branch + "_" + fileData[0].ScheduledOn.Value.ToString("dd-MM-yyyy");
                                    //string directoryName = ComplianceData.ShortDescription + "/" + ComplianceData.ForMonth;
                                    if (fileData != null)
                                    {
                                        var CustomerBranch = CustomerBranchManagement.GetByID(ComplianceData.CustomerBranchID).Name;
                                        string directoryName = CustomerBranch + "/" + ComplianceData.ShortDescription + "/" + ComplianceData.ForMonth;
                                        int i = 0;
                                        foreach (var file in fileData)
                                        {
                                            string version = string.IsNullOrEmpty(file.Version) ? "1.0" : file.Version;
                                            var dictionary = ComplianceZip.EntryFileNames.Where(x => x.Contains(directoryName + "/" + version)).FirstOrDefault();
                                            if (dictionary == null)
                                                ComplianceZip.AddDirectoryByName(directoryName + "/" + version);
                                            string filePath = string.Empty;
                                            //Change by rahul on 21 JAN 2017 for Azure Drive or Local Drive
                                            if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                            {
                                                filePath = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.FileName));
                                            }
                                            else
                                            {
                                                filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                            }

                                            if (file.FilePath != null && File.Exists(filePath))
                                            {
                                                string ext = Path.GetExtension(file.FileName);
                                                string[] filename = file.FileName.Split('.');
                                                //string str = filename[0] + i + "." + filename[1];
                                                string str = filename[0] + i + "." + ext;
                                                if (file.EnType == "M")
                                                {
                                                    ComplianceZip.AddEntry(directoryName + "/" + version + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                else
                                                {
                                                    ComplianceZip.AddEntry(directoryName + "/" + version + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                i++;
                                            }
                                        }
                                    }
                                }
                                else if (IsStatutory == 0) //Internal 
                                {
                                    var ComplianceData = DocumentManagement.GetForMonthInternal(ScheduledOnID);
                                    List<GetInternalComplianceDocumentsView> fileData = DocumentManagement.GetInternalFileData1(ScheduledOnID);

                                    if (fileData != null)
                                    {
                                        //craeted subdirectory
                                        //string directoryName = fileData[0].ID + "_" + fileData[0].Branch + "_" + fileData[0].ScheduledOn.Value.ToString("dd-MM-yyyy");
                                        //string directoryName = ComplianceData.ShortDescription + "/" + ComplianceData.ForMonth;
                                        var CustomerBranch = CustomerBranchManagement.GetByID(ComplianceData.CustomerBranchID).Name;
                                        string directoryName = CustomerBranch + "/" + ComplianceData.ShortDescription + "/" + ComplianceData.ForMonth;
                                        int i = 0;
                                        foreach (var file in fileData)
                                        {
                                            string version = string.IsNullOrEmpty(file.Version) ? "1.0" : file.Version;
                                            var dictionary = ComplianceZip.EntryFileNames.Where(x => x.Contains(directoryName + "/" + version)).FirstOrDefault();
                                            if (dictionary == null)
                                                ComplianceZip.AddDirectoryByName(directoryName + "/" + version);
                                            string filePath = string.Empty;
                                            //Change by rahul on 21 JAN 2017 for Azure Drive or Local Drive
                                            if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                            {
                                                filePath = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.FileName));
                                            }
                                            else
                                            {
                                                filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                            }

                                            if (file.FilePath != null && File.Exists(filePath))
                                            {
                                                string ext = Path.GetExtension(file.FileName);
                                                string[] filename = file.FileName.Split('.');
                                                //string str = filename[0] + i + "." + filename[1];
                                                string str = filename[0] + i + "." + ext;
                                                if (file.EnType == "M")
                                                {
                                                    ComplianceZip.AddEntry(directoryName + "/" + version + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                else
                                                {
                                                    ComplianceZip.AddEntry(directoryName + "/" + version + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                i++;
                                            }
                                        }
                                    }
                                }
                                else if (IsStatutory == 2 || IsStatutory == 4) //Stautory Checklist or Event Based Checklist
                                {
                                    var ComplianceData = DocumentManagement.GetForMonthChecklist(ScheduledOnID);
                                    List<GetComplianceDocumentsView> fileData = DocumentManagement.GetFileData1(ScheduledOnID);

                                    if (fileData != null)
                                    {
                                        var CustomerBranch = CustomerBranchManagement.GetByID(ComplianceData.CustomerBranchID).Name;
                                        string directoryName = CustomerBranch + "/" + ComplianceData.ShortDescription + "/" + ComplianceData.ForMonth;
                                        int i = 0;
                                        foreach (var file in fileData)
                                        {
                                            string version = string.IsNullOrEmpty(file.Version) ? "1.0" : file.Version;
                                            var dictionary = ComplianceZip.EntryFileNames.Where(x => x.Contains(directoryName + "/" + version)).FirstOrDefault();
                                            if (dictionary == null)
                                                ComplianceZip.AddDirectoryByName(directoryName + "/" + version);
                                            string filePath = string.Empty;
                                            //Change by rahul on 21 JAN 2017 for Azure Drive or Local Drive
                                            if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                            {
                                                filePath = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.FileName));
                                            }
                                            else
                                            {
                                                filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                            }

                                            if (file.FilePath != null && File.Exists(filePath))
                                            {
                                                string ext = Path.GetExtension(file.FileName);
                                                string[] filename = file.FileName.Split('.');
                                                //string str = filename[0] + i + "." + filename[1];
                                                string str = filename[0] + i + "." + ext;
                                                if (file.EnType == "M")
                                                {
                                                    ComplianceZip.AddEntry(directoryName + "/" + version + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                else
                                                {
                                                    ComplianceZip.AddEntry(directoryName + "/" + version + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                i++;
                                            }
                                        }
                                    }
                                }
                                else if (IsStatutory == 3) //Internal CheckList
                                {
                                    var ComplianceData = DocumentManagement.InternalComplianceCheckListGetForMonth(ScheduledOnID);
                                    List<GetInternalComplianceDocumentsView> fileData = DocumentManagement.GetInternalFileData1(ScheduledOnID);

                                    if (fileData != null)
                                    {
                                        var CustomerBranch = CustomerBranchManagement.GetByID(ComplianceData.CustomerBranchID).Name;
                                        string directoryName = CustomerBranch + "/" + ComplianceData.ShortDescription + "/" + ComplianceData.ForMonth;

                                        int i = 0;
                                        foreach (var file in fileData)
                                        {
                                            string version = string.IsNullOrEmpty(file.Version) ? "1.0" : file.Version;
                                            var dictionary = ComplianceZip.EntryFileNames.Where(x => x.Contains(directoryName + "/" + version)).FirstOrDefault();
                                            if (dictionary == null)
                                                ComplianceZip.AddDirectoryByName(directoryName + "/" + version);
                                            string filePath = string.Empty;
                                            //Change by rahul on 21 JAN 2017 for Azure Drive or Local Drive
                                            if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                            {
                                                filePath = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.FileName));
                                            }
                                            else
                                            {
                                                filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                            }

                                            if (file.FilePath != null && File.Exists(filePath))
                                            {
                                                string ext = Path.GetExtension(file.FileName);
                                                string[] filename = file.FileName.Split('.');
                                                //string str = filename[0] + i + "." + filename[1];
                                                string str = filename[0] + i + "." + ext;
                                                if (file.EnType == "M")
                                                {
                                                    ComplianceZip.AddEntry(directoryName + "/" + version + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                else
                                                {
                                                    ComplianceZip.AddEntry(directoryName + "/" + version + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                i++;
                                            }
                                        }
                                    }
                                }

                            }
                        
                        }
                        var zipMs = new MemoryStream();
                        ComplianceZip.Save(zipMs);
                        zipMs.Position = zipMs.Length;

                        byte[] data = zipMs.ToArray();

                        Response.Buffer = true;
                        Response.ClearContent();
                        Response.ClearHeaders();
                        Response.Clear();
                        Response.ContentType = "application/zip";
                        Response.AddHeader("content-disposition", "attachment; filename=ComplianceDocument.zip");
                        Response.BinaryWrite(data);
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest();

                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

            //long ScheduledOnID = 89867;
            //long transactionID = 91291;
            //List<GetComplianceDocumentsView> CMPDocuments = Business.ComplianceManagement.GetDocumnets(ScheduledOnID, transactionID);

            //if (CMPDocuments != null)
            //{
            //    List<GetComplianceDocumentsView> entitiesData = CMPDocuments.Where(entry => entry.Version != null).GroupBy(entry => entry.Version).Select(en => en.FirstOrDefault()).ToList();
            //    if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
            //    {
            //        GetComplianceDocumentsView entityData = new GetComplianceDocumentsView();
            //        entityData.Version = "1.0";
            //        entityData.ScheduledOnID = Convert.ToInt64(ScheduledOnID);
            //        entitiesData.Add(entityData);
            //    }


            //    using (ZipFile ComplianceZip = new ZipFile())
            //    {
            //        //  string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

            //        List<GetComplianceDocumentsView> ComplianceFileData = new List<GetComplianceDocumentsView>();
            //        List<GetComplianceDocumentsView> ComplianceDocument = new List<GetComplianceDocumentsView>();

            //        ComplianceDocument = DocumentManagement.GetFileData1(Convert.ToInt32(ScheduledOnID)).ToList();
            //        ComplianceFileData = ComplianceDocument;

            //        // var ComplianceData = DocumentManagement.GetForMonth(Convert.ToInt32(ScheduledOnID));
            //        ComplianceZip.AddDirectoryByName("Jan 17" + "/" + transactionID);

            //        if (ComplianceFileData.Count > 0)
            //        {
            //            int i = 0;
            //            foreach (var file in ComplianceFileData)
            //            {
            //                string filePath = string.Empty;
            //                //Change by rahul on 21 JAN 2017 for Azure Drive or Local Drive
            //                if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
            //                {
            //                    filePath = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.FileName));
            //                }
            //                else
            //                {
            //                    filePath = Path.Combine(System.Web.HttpContext.Current.Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
            //                }
            //                if (file.FilePath != null && File.Exists(filePath))
            //                {
            //                    string[] filename = file.FileName.Split('.');
            //                    string str = filename[0] + i + "." + filename[1];
            //                    ComplianceZip.AddEntry("Jan 17" + "/" + transactionID + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
            //                    i++;
            //                }
            //            }
            //        }

            //        var zipMs = new MemoryStream();
            //        ComplianceZip.Save(zipMs);
            //        zipMs.Position = 0;
            //        byte[] data = zipMs.ToArray();

            //        HttpContext.Current.Response.Buffer = true;
            //        HttpContext.Current.Response.ClearContent();
            //        HttpContext.Current.Response.ClearHeaders();
            //        HttpContext.Current.Response.Clear();
            //        HttpContext.Current.Response.ContentType = "application/zip";
            //        HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=ComplianceDocument_" + transactionID + ".zip");
            //        HttpContext.Current.Response.BinaryWrite(data);
            //        HttpContext.Current.Response.Flush();
            //        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
            //        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
            //        HttpContext.Current.ApplicationInstance.CompleteRequest();
            //    }

            //}

        }
    }
}