﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NewCompliance.Master" AutoEventWireup="true" CodeBehind="ActDocument.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.ComplianceDocument.ActDocument" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />
    <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.4.0/jszip.min.js"></script>

    <style type="text/css">
        .k-list > .k-state-focused.k-state-selected, .k-listview > .k-state-focused.k-state-selected, .k-state-focused.k-state-selected, td.k-state-focused.k-state-selected {
            -webkit-box-shadow: inset 0 0 3px 1px rgba(0, 0, 0, 0.5);
            box-shadow: inset 0 0 3px 1px rgba(0, 0, 0, 0.5);
        }

        input[type=checkbox], input[type=radio] {
            margin: 4px 6px 0;
            margin-top: 1px\9;
            line-height: normal;
        }

        .div.k-grid-footer, div.k-grid-header {
            border-top-width: 1px;
            margin-right: -2px;
        }

        .k-grid-footer-wrap, .k-grid-header-wrap {
            position: relative;
            width: 100%;
            overflow: hidden;
            border-style: solid;
            border-width: 0 1px 0 0;
            zoom: 1;
        }

        .k-grid-content {
            min-height: 390px !important;
        }

        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 2.0em;
            border-bottom-width: 1px;
            background-color: white;
            border-width: 0 1px 1px 0px;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
            background-color: white;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: white;
        }

        #grid .k-grid-toolbar {
            background: white;
        }

        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 6px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: white;
        }

        html .k-grid tr.k-alt:hover {
            background: white;
        }

        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            background: white;
            border: none;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
            margin-right: 2px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-active-filter, .k-state-active, .k-state-active:hover {
            background-color: #E9EAEA;
            border-color: #a6a6ad;
            color: #535b6a;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: white;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 0px 1px 0px;
            text-align: center;
        }

        .k-grid-pager {
            margin-top: -1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-i-arrow-60-down, .k-i-arrow-60-left, .k-i-arrow-60-right, .k-i-arrow-60-up {
            cursor: pointer;
            margin-top: 6px;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            border-width: 0px 0px 1px 0px;
            background: #E9EAEA;
            font-weight: bold;
            margin-right: 18px;
            font-family: 'Roboto',sans-serif;
            font-size: 15px;
            color: rgba(0, 0, 0, 0.5);
            height: 20px;
            vertical-align: middle;
        }

        .k-dropdown-wrap.k-state-default {
            background-color: white;
        }

        .k-popup.k-calendar-container, .k-popup.k-list-container {
            background-color: white;
        }

        .k-grid-header th.k-state-focused, .k-list > .k-state-focused, .k-listview > .k-state-focused, .k-state-focused, td.k-state-focused {
            -webkit-box-shadow: inset 0 0 3px 1px white;
            box-shadow: inset 0 0 3px 1px white;
        }

        label.k-label {
            font-family: roboto,sans-serif !important;
            color: #515967;
            /* font-stretch: 100%; */
            font-style: normal;
            font-weight: 400;
            min-width: 362px;
            white-space: pre-wrap;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: flex;
            margin-bottom: 0px;
        }

        .k-state-default > .k-select {
            border-color: #ceced2;
            margin-top: 0px;
        }

        .k-grid-norecords {
            width: 100%;
            height: 100%;
            text-align: center;
            margin-top: 2%;
        }

        .k-grouping-header {
            font-style: italic;
            background-color: white;
        }

        .k-grid-toolbar, .k-grouping-header {
            margin: 0;
            padding: .22em .2em .28em;
            cursor: default;
        }

        .k-grid-toolbar {
            background: white;
            border: none;
        }

        .k-grid table {
            width: 100.5%;
        }
    </style>
    <title></title>
    <script id="templateTooltip" type="text/x-kendo-template">
                <div>
                <div> #:value ? value : "N/A" #</div>
                </div>
    </script>
    <script type="text/x-kendo-template" id="template"> 
    <div class=row style="padding-bottom: 4px; display:none;">
            <div class="toolbar">               
                <input id="dropdowntree" data-placeholder="Entity/Sub-Entity/Location" style="width:242px;">            
                <input id="dropdownlistComplianceType" data-placeholder="Type">                  
                <input id="dropdownlistRisk" data-placeholder="Risk">                  
                <input id="dropdownlistStatus" data-placeholder="Status">
                <input id="dropdownlistTypePastdata" data-placeholder="Status">                
                <button id="AdavanceSearch" style="height: 23px;" onclick="OpenAdvanceSearch(event)"><span class="k-icon k-i-filter" onclick="javascript:return false;"></span>Advanced Search</button>
            </div>
    </div> 
           
         <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-2" style="width: 17%;;">
                        </div>
                        <div class="col-md-2" style="width: 15%;">
                            <div id="dvdropdownEventName" style="display:none;"><input id="dropdownEventName" data-placeholder="Event Name" style="width:175px;"></div>          
                        </div>
                        <div class="col-md-2" style="width: 15%;padding-left: 0px;">
                           <div id="dvdropdownEventNature" style="display:none;"><input id="dropdownEventNature" data-placeholder="Event Nature"></div>
                        </div>
                        <div class="col-md-2" style="width: 3%;">
                        </div>
                        <div class="col-md-1" style="width: 37%;padding-left: 22px;">                             
                             <button id="ClearfilterMain" style="float: right; margin-left: 1%;display:none;" onclick="ClearAllFilterMain(event)"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear Filter</button>                                         
                             <button id="dvbtndownloadDocumentMain" style="float: right;display:none;" onclick="selectedDocumentMain(event)">Download</button>                                                                         
                        </div>

                    </div>
                </div>
                             
       
            <div class=row style="padding-bottom: 4px;font-size: 12px;display:none;" Id="filtersstoryboard">&nbsp;</div>
            <div class=row style="padding-bottom: 4px;font-size: 12px;display:none;" Id="filtertype">&nbsp;</div>
            <div class=row style="padding-bottom: 4px;font-size: 12px;display:none;" Id="filterrisk">&nbsp;</div>
            <div class=row style="padding-bottom: 4px;font-size: 12px;display:none;" Id="filterstatus">&nbsp;</div>

    </script>

    <script id="fileTemplate" type="text/x-kendo-template">
            <span class='k-progress'></span>
            <div class='file-wrapper'> 
            #=GetFileExtType(FileName)# #=FileName # 
    </script>

    <script id="fileExtensionTemplate" type="text/x-kendo-template">
      
            #=FileName.split('.').pop() # 
         
    </script>

    <script type="text/javascript">

        function GetFileExtType(value) {

            if (value.split('.').pop() == "pdf" || value.split('.').pop() == "PDF" || value.split('.').pop() == "Pdf") {
                return "<span class='k-icon k-i-file-pdf k-i-pdf'></span>";
            }
            else if (value.split('.').pop() == "doc" || value.split('.').pop() == "docx" || value.split('.').pop() == "DOC" || value.split('.').pop() == "DOCX") {
                return "<span class='k-icon k-i-file-word k-i-file-doc k-i-word k-i-doc'></span>";
            }

            else if (value.split('.').pop() == "xls" || value.split('.').pop() == "xlsx" || value.split('.').pop() == "XLS" || value.split('.').pop() == "XLSX") {
                return "<span class='k-icon k-i-file-excel k-i-file-xls k-i-excel k-i-xls'></span>";
            }
            else if (value.split('.').pop() == "ppt" || value.split('.').pop() == "pptx" || value.split('.').pop() == "PPT" || value.split('.').pop() == "PPTX") {
                return "<span class='k-icon k-i-file-ppt k-i-ppt'></span>";
            }
            else if (value.split('.').pop() == "msg" || value.split('.').pop() == "MSG") {
                return "<span class='k-icon k-i-email k-i-envelop k-i-letter'></span>";
            }
            else if (value.split('.').pop() == "txt") {
                return "<span class='k-icon k-i-file-txt k-i-txt'></span>";
            }
            else if (value.split('.').pop() == "jpg" || value.split('.').pop() == "JPG" || value.split('.').pop() == "jpeg" || value.split('.').pop() == "JPEG" || value.split('.').pop() == "png" || value.split('.').pop() == "PNG" || value.split('.').pop() == "tif" || value.split('.').pop() == "TIF" || value.split('.').pop() == "tiff" || value.split('.').pop() == "TIFF" || value.split('.').pop() == "bmp" || value.split('.').pop() == "BMP" || value.split('.').pop() == "gif" || value.split('.').pop() == "GIF") {
                return "<span class='k-icon k-i-image k-i-photo'></span>";
            }
            else {
                return "";
            }
        }

        function fclosebtn(tbn) {
            $('#' + tbn).css('display', 'none');
            $('#' + tbn).html('');
        }

        $(document).ready(function () {

            <%if (Falg == "AUD")%>
           <%{%>

            $('#Startdatepicker').val('<% =SDate%>');
            $('#Lastdatepicker').val('<% =LDate%>');

            $("#Startdatepicker").attr("readonly", true);
            $("#Lastdatepicker").attr("readonly", true);
            $("#dropdownPastData").attr("readonly", true);
            $("#dropdownFY").attr("readonly", true);

            $('#dropdownPastData').val('All');
            $('#dropdownlistTypePastdata').val('All');
           <%}%>

            var myWindowAdv = $("#divAdvanceSearchModel");

            function onClose() {
            }

            myWindowAdv.kendoWindow({
                width: "95%",
                height: "95%",
                title: "Advanced Search",
                visible: false,
                actions: [
                    "Close"
                ],
                close: onClose
            });

            $("#Startdatepicker").kendoDatePicker({
                change: onChange
            });
            function onChange() {
                settracknew('Act Document', 'Filtering', 'Start Date', '');
                $('#filterStartDate').css('display', 'none');
                $('#Clearfilter').css('display', 'none');
                $('#filterStartDate').html('');
                if (kendo.toString(this.value(), 'd') != null) {
                    $('#filterStartDate').css('display', 'block');
                    $('#Clearfilter').css('display', 'block');
                    $('#filterStartDate').append('Start Date:&nbsp;');
                    $('#filterStartDate').append('<span class="k-button" style="background-color:#1fd9e1; height: 20px;Color:white;">' + kendo.toString(this.value(), 'dd MMM yyyy') + '<span class="k-icon k-i-close" onclick="fclosebtn(\'filterStartDate\')"></span></span>');

                }
                DateFilterCustom();
            }

            function DateFilterCustom() {

                $('input[id=chkAll]').prop('checked', false);
                $('#dvbtndownloadDocument').css('display', 'none');

                FilterAllAdvancedSearch();
            }

            $("#Lastdatepicker").kendoDatePicker({
                change: onChange1
            });
            function onChange1() {
                settracknew('Act Document', 'Filtering', 'Last date', '');
                $('#filterLastDate').html('');
                if (kendo.toString(this.value(), 'd') != null) {
                    $('#filterLastDate').css('display', 'block');
                    $('#Clearfilter').css('display', 'block');
                    $('#filterLastDate').append('End Date&nbsp;&nbsp;:&nbsp;');

                    $('#filterLastDate').append('<span class="k-button" style="background-color:white; height: 20px;Color:white;">' + kendo.toString(this.value(), 'dd MMM yyyy') + '<span class="k-icon k-i-close" onclick="fclosebtn(\'filterStartDate\')"></span></span>');
                }
                DateFilterCustom();
            }

            $(".k-grid1-content tbody[role='rowgroup'] tr[role='row'] td:first-child").prepend('<span class="k-icon k-i-filter"</span>');

            var grid1 = $("#grid1").kendoGrid({
                dataSource: {
                    transport: {
                        read: {
                            url: '<% =Path%>data/KendoMyActDocument?UserId=<% =UId%>&RoleID=<% =RoleID%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                    },
                    pageSize: 10,
                },
                sortable: true,
                groupable: true,
                filterable: true,
                columnMenu: true,
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                change: onChange,
                sort: onSorting,
                page: onPaging,
                pageable: {
                    numeric: true,
                    pageSizes: ['All', 5, 10, 20],
                    pageSize: 10,
                    buttonCount: 3,
                },
                dataBinding: function () {
                    var total = this.dataSource._pristineTotal;
                    if (this.dataSource.pageSize() == undefined) {
                        this.dataSource.pageSize(total);
                    }
                },
                columns: [
                    {
                        field: "ActID", title: 'Act ID',
                        width: "5%;",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "ActName", title: 'Act Name',
                        width: "35%;",
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            extra: false,
                            multi: true,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "ComplianceTypeName", title: 'Type Name',
                        width: "14%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            extra: false,
                            multi: true,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "ComplianceCategoryName", title: 'Category Name',
                        width: "25%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        command: [
                            { name: "edit5", text: "", iconClass: "k-icon k-i-eye", className: "ob-edit" },
                            { name: "edit6", text: "", iconClass: "k-icon k-i-download", className: "ob-download" },
                        ], title: "Action", lock: true,width:"100",
                        headerAttributes: {
                            style: "text-align: center;"
                        },
                    }
                ]
            });
            function onSorting(arg) {
                settracknew('Act Document', 'Sorting', arg.sort.field, '');

            }
            function onPaging(arg) {
                settracknew('Act Document', 'Paging', arg.page, '');
            }
            $("#grid").kendoTooltip({
                filter: "th",
                content: function (e) {
                    var target = e.target; // element for which the tooltip is shown 
                    return $(target).text();
                }
            });
            $("#grid1").kendoTooltip({
                filter: ".k-grid-edit5",
                content: function (e) {
                    return "View";
                }
            });

            $("#grid1").kendoTooltip({
                filter: ".k-grid-edit6",
                width: 100,
                content: function (e) {
                    return "Download";
                }
            });

            //function definition
            $("#grid1").kendoTooltip({
                filter: "td", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 100em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");
            var grid = $("#grid").kendoGrid({

                dataSource: {

                    transport: {

                        read: {
                            url: '<% =Path%>data/KendoMyActDocument?UserId=<% =UId%>&RoleID=<% =RoleID%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                    },
                    pageSize: 10,
                },

                toolbar: kendo.template($("#template").html()),
                sortable: true,
                groupable: true,
                filterable: true,
                columnMenu: true,
                pageable: {
                    numeric: true,
                    pageSizes: ['All', 5, 10, 20],
                    pageSize: 10,
                    buttonCount: 3,
                },
                dataBinding: function () {
                    var total = this.dataSource._pristineTotal;
                    if (this.dataSource.pageSize() == undefined) {
                        this.dataSource.pageSize(total);
                    }
                },
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                sort: onSorting,
                page: onPaging,
                dataBound: function() {
                    for (var i = 0; i < this.columns.length; i++) {
                        this.autoWidth;
                    }
                },
                columns: [
                    {
                        field: "ActID", title: 'Act ID',
                        width: "10%;",
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            extra: false,
                            multi: true,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "ActName", title: 'Act Name',
                        width: "40%;",
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            extra: false,
                            multi: true,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "ComplianceTypeName", title: 'Type Name',
                        width: "15%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "ComplianceCategoryName", title: 'Category Name',
                        width: "20%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        command: [
                            { name: "edit", text: "", iconClass: "k-icon k-i-eye", className: "ob-edit" },
                            { name: "edit1", text: "", iconClass: "k-icon k-i-download", className: "ob-download" },
                        ], title: "Action", lock: true,width: "10%", 
                        headerAttributes: {
                            style: "text-align: center;"
                        },
                    }
                ]
            });
            function onSorting(arg) {
                settracknew('Act Document', 'Sorting', arg.sort.field, '');

            }
            function onPaging(arg) {
                settracknew('Act Document', 'Paging', arg.page, '');
            }

            $("#grid").kendoTooltip({
                filter: ".k-grid-edit",
                content: function (e) {
                    return "View";
                }
            });

            $("#grid").kendoTooltip({
                filter: ".k-grid-edit1",
                content: function (e) {
                    return "Download";
                }
            });

            $("#grid").kendoTooltip({
                filter: "td", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");


            $("#dropdownlistComplianceType").kendoDropDownList({
                placeholder: "Compliance Type",
                dataTextField: "text",
                dataValueField: "value",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                dataSource: [
                    { text: "Statutory", value: "-1" },
                    { text: "Internal", value: "0" },
                    { text: "Event Based", value: "1" },
                    { text: "Statutory CheckList", value: "2" },
                    { text: "Internal CheckList", value: "3" },
                    { text: "Event Based CheckList", value: "4" }
                ],
                index: 0,
                change: function (e) {
                    DataBindDaynamicKendoGriddMain();
                    settracknew('Act Document', 'Filtering', 'Compliance Type', '');
                }
            });

            function DataBindDaynamicKendoGriddMain() {
                $('input[id=chkAllMain]').prop('checked', false);
                $('#dvdropdownEventNature').css('display', 'none');
                $('#dvdropdownEventName').css('display', 'none');
                $("#dropdowntree").data("kendoDropDownTree").value([]);
                $("#dropdownlistRisk").data("kendoDropDownTree").value([]);
                $("#dropdownlistStatus").data("kendoDropDownTree").value([]);
                $('#ClearfilterMain').css('display', 'none');
                $('#dvbtndownloadDocumentMain').css('display', 'none');
                $("#grid").data('kendoGrid').dataSource.data([]);

                var dataSource = new kendo.data.DataSource({
                    transport: {
                        read: {
                            url: '<% =Path%>data/KendoMyActDocument?UserId=<% =UId%>&RoleID=<% =RoleID%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>data/KendoMyActDocument?UserId=<% =UId%>&RoleID=<% =RoleID%>"
                    },
                    pageSize: 10,
                });
                var grid = $('#grid').data("kendoGrid");
                dataSource.read();
                grid.setDataSource(dataSource);


                $("#dropdownEventName").data("kendoDropDownList").select(0);
                $("#dropdownEventNature").data("kendoDropDownList").select(0);
                if ($("#dropdownlistComplianceType").val() == 1 || $("#dropdownlistComplianceType").val() == 4) {
                    $('#dvdropdownEventNature').css('display', 'block');
                    $('#dvdropdownEventName').css('display', 'block');

                    $("#grid").data("kendoGrid").showColumn(5);
                    $("#grid").data("kendoGrid").showColumn(6);
                    $("#grid").data("kendoGrid").hideColumn(3);
                    $("#grid").data("kendoGrid").hideColumn(8);
                }
                else {
                    $("#grid").data("kendoGrid").showColumn(8);
                    $("#grid").data("kendoGrid").showColumn(3);
                    $("#grid").data("kendoGrid").hideColumn(5);
                    $("#grid").data("kendoGrid").hideColumn(6);
                }
            }

            $("#dropdownlistRisk").kendoDropDownTree({
                placeholder: "Risk",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",

                change: function () {
                    settracknew('Act Document', 'Filtering', 'Risk', '');
                    FilterAllMain();
                    fCreateStoryBoard('dropdownlistRisk', 'filterrisk', 'risk');

                    $('input[id=chkAllMain]').prop('checked', false);
                    $('#dvbtndownloadDocumentMain').css('display', 'none');
                },
                dataSource: [
                    { text: "High", value: "0" },
                    { text: "Medium", value: "1" },
                    { text: "Low", value: "2" }
                ]
            });

            $("#dropdownlistStatus").kendoDropDownTree({
                placeholder: "Status",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                autoWidth: true,
                checkAllTemplate: "Select All",
                change: function (e) {
                    settracknew('Act Document', 'Filtering', 'Status', '');
                    FilterAllMain();
                    fCreateStoryBoard('dropdownlistStatus', 'filterstatus', 'status')

                    $('input[id=chkAllMain]').prop('checked', false);
                    $('#dvbtndownloadDocumentMain').css('display', 'none');
                },
                dataSource: [
                    { text: "Closed-Delayed", value: "Closed-Delayed" },
                    { text: "Closed-Timely", value: "Closed-Timely" },
                    { text: "Rejected", value: "Rejected" },
                    { text: "Pending For Review", value: "Pending For Review" },
                    { text: "Interim Rejected", value: "Interim Rejected" },
                    { text: "Interim Review Approved", value: "Interim Review Approved" },
                    { text: "Submitted For Interim Review", value: "Submitted For Interim Review" }
                ]
            });

            $("#dropdownPastData").kendoDropDownList({
                placeholder: "Period",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    DataBindDaynamicKendoGrid();
                    settracknew('Act Document', 'Filtering', 'Period', '');
                },
                index: 1,
                dataSource: [
                    { text: "Last Month", value: "1" },
                    { text: "Last Three Months", value: "3" },
                    { text: "Last Six Months", value: "6" },
                    { text: "Last Year", value: "12" },
                    { text: "All", value: "All" }
                ]
            });


            function DataBindDaynamicKendoGrid() {

                $("#grid1").data('kendoGrid').dataSource.data([]);

                $("#dropdownUser").data("kendoDropDownTree").value([]);

                $('input[id=chkAll]').prop('checked', false);

                $("#grid1").data("kendoGrid").dataSource.filter({});

                $("#dropdowntree1").data("kendoDropDownTree").value([]);
                $("#dropdownlistRisk1").data("kendoDropDownTree").value([]);
                $("#dropdownlistStatus1").data("kendoDropDownTree").value([]);
                $("#Startdatepicker").data("kendoDatePicker").value(null);
                $("#Lastdatepicker").data("kendoDatePicker").value(null);
                $('#filterStartDate').html('');
                $('#filterLastDate').html('');
                $('#filterStartDate').css('display', 'none');
                $('#filterLastDate').css('display', 'none');
                $('#Clearfilter').css('display', 'none');
                $('#dvbtndownloadDocument').css('display', 'none');

                $("#dvdropdownACT").css('display', 'block');

                if ($("#dropdownlistComplianceType1").val() == 1 || $("#dropdownlistComplianceType1").val() == 4)//event based and event based checklist
                {
                    $('#dvdropdownEventNature1').css('display', 'block');
                    $('#dvdropdownEventName1').css('display', 'block');

                    $("#grid1").data("kendoGrid").showColumn(7);//Event Name
                    $("#grid1").data("kendoGrid").showColumn(8);//Event Nature

                    $("#grid1").data("kendoGrid").hideColumn(5);//Branch
                    $("#grid1").data("kendoGrid").hideColumn(10);//ForMonth
                }
                else {
                    $('#dvdropdownEventNature1').css('display', 'none');
                    $('#dvdropdownEventName1').css('display', 'none');

                    $("#grid1").data("kendoGrid").hideColumn(7);//Event Name
                    $("#grid1").data("kendoGrid").hideColumn(8);//Event Nature

                    $("#grid1").data("kendoGrid").showColumn(5);//Branch
                    $("#grid1").data("kendoGrid").showColumn(10);//ForMonth
                }

                var dataSource = new kendo.data.DataSource({
                    transport: {
                        read: {
                            url: '<% =Path%>data/KendoMyActDocument?UserId=<% =UId%>&RoleID=<% =RoleID%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>data/KendoMyActDocument?UserId=<% =UId%>&RoleID=<% =RoleID%>"
                    },
                    pageSize: 10,
                    filterable: true,
                });
                var grid = $('#grid1').data("kendoGrid");
                dataSource.read();
                grid.setDataSource(dataSource);
            }
            $("#dropdownlistTypePastdata").kendoDropDownList({
                placeholder: "Period",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    DataBindDaynamicKendoGriddMain();
                    settracknew('Act Document', 'Filtering', 'Period', '');
                },
                index: 1,
                dataSource: [
                    { text: "Last Month", value: "1" },
                    { text: "Last Three Months", value: "3" },
                    { text: "Last Six Months", value: "6" },
                    { text: "Last Year", value: "12" },
                    { text: "All", value: "All" }
                ]
            });

            function FilterAllMain() {
                var list1 = $("#dropdowntree").data("kendoDropDownTree")._values;
                var locationsdetails = [];
                $.each(list1, function (i, v) {
                    locationsdetails.push({
                        field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                    });
                });

                var list2 = $("#dropdownlistStatus").data("kendoDropDownTree")._values;
                var Statusdetails = [];
                $.each(list2, function (i, v) {
                    Statusdetails.push({
                        field: "Status", operator: "eq", value: v
                    });
                });

                var Riskdetails = [];
                var list3 = $("#dropdownlistRisk").data("kendoDropDownTree")._values;
                $.each(list3, function (i, v) {
                    Riskdetails.push({
                        field: "Risk", operator: "eq", value: parseInt(v)
                    });
                });


                var dataSource = $("#grid").data("kendoGrid").dataSource;

                if ($("#dropdowntree").data("kendoDropDownTree")._values.length > 0
                    && $("#dropdownlistStatus").data("kendoDropDownTree")._values.length > 0
                    && $("#dropdownlistRisk").data("kendoDropDownTree")._values.length > 0) {

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: Riskdetails
                            },
                            {
                                logic: "or",
                                filters: Statusdetails
                            },
                            {
                                logic: "or",
                                filters: locationsdetails
                            }
                        ]
                    });
                }

                else if ($("#dropdowntree").data("kendoDropDownTree")._values.length > 0
                    && $("#dropdownlistStatus").data("kendoDropDownTree")._values.length > 0) {

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: Statusdetails
                            },
                            {
                                logic: "or",
                                filters: locationsdetails
                            }
                        ]
                    });
                }

                else if ($("#dropdowntree").data("kendoDropDownTree")._values.length > 0
                    && $("#dropdownlistRisk").data("kendoDropDownTree")._values.length > 0) {

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: Riskdetails
                            },
                            {
                                logic: "or",
                                filters: locationsdetails
                            }
                        ]
                    });
                }

                else if ($("#dropdownlistStatus").data("kendoDropDownTree")._values.length > 0
                    && $("#dropdownlistRisk").data("kendoDropDownTree")._values.length > 0) {

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: Riskdetails
                            },
                            {
                                logic: "or",
                                filters: Statusdetails
                            }
                        ]
                    });
                }

                else if ($("#dropdowntree").data("kendoDropDownTree")._values.length > 0) {

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: locationsdetails
                            }
                        ]
                    });
                }

                else if ($("#dropdownlistStatus").data("kendoDropDownTree")._values.length > 0) {

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: Statusdetails
                            }
                        ]
                    });
                }

                else if ($("#dropdownlistRisk").data("kendoDropDownTree")._values.length > 0) {

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: Riskdetails
                            }
                        ]
                    });
                }

                else {
                    $("#grid").data("kendoGrid").dataSource.filter({});
                }
            }

            function FilterAllAdvancedSearch() {
                //location details
                var list1 = $("#dropdowntree1").data("kendoDropDownTree")._values;
                var locationsdetails = [];
                $.each(list1, function (i, v) {
                    locationsdetails.push({
                        field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                    });
                });

                var list2 = $("#dropdownlistStatus1").data("kendoDropDownTree")._values;
                var Statusdetails = [];
                $.each(list2, function (i, v) {
                    Statusdetails.push({
                        field: "Status", operator: "eq", value: v
                    });
                });

                var Riskdetails = [];
                var list3 = $("#dropdownlistRisk1").data("kendoDropDownTree")._values;
                $.each(list3, function (i, v) {
                    Riskdetails.push({
                        field: "Risk", operator: "eq", value: parseInt(v)
                    });
                });

                var userdetails = [];
                var list4 = $("#dropdownUser").data("kendoDropDownTree")._values;
                $.each(list4, function (i, v) {
                    userdetails.push({
                        field: "PerformerID", operator: "eq", value: parseInt(v)
                    });
                });

                var Actdetails = [];
                if ($("#dropdownACT").val() != "" && $("#dropdownACT").val() != null && $("#dropdownACT").val() != undefined) {
                    Actdetails.push({
                        field: "ActID", operator: "eq", value: parseInt($("#dropdownACT").val())
                    });
                }

                var datedetails = [];
                if ($("#Startdatepicker").val() != null && $("#Startdatepicker").val() != "") {
                    datedetails.push({
                        field: "ScheduledOn", operator: "gt", value: kendo.parseDate($("#Startdatepicker").val(), 'MM/dd/yyyy')
                    });
                }
                if ($("#Lastdatepicker").val() != null && $("#Lastdatepicker").val() != "") {
                    datedetails.push({
                        field: "ScheduledOn", operator: "lte", value: kendo.parseDate($("#Lastdatepicker").val(), 'MM/dd/yyyy')
                    });
                }

                debugger;
                var dataSource = $("#grid1").data("kendoGrid").dataSource;
                if (locationsdetails.length > 0
                    && Statusdetails.length > 0
                    && Riskdetails.length > 0
                    && userdetails.length > 0
                    && Actdetails.length > 0
                    && datedetails.length > 0) {

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: locationsdetails
                            },
                            {
                                logic: "or",
                                filters: Statusdetails
                            },
                            {
                                logic: "or",
                                filters: Riskdetails
                            },
                            {
                                logic: "or",
                                filters: userdetails
                            },
                            {
                                logic: "or",
                                filters: Actdetails
                            },
                            {
                                logic: "and",
                                filters: datedetails
                            }
                        ]
                    });
                }
                else if (locationsdetails.length > 0
                    && Statusdetails.length > 0
                    && Riskdetails.length > 0
                    && userdetails.length > 0
                    && Actdetails.length > 0) {

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: locationsdetails
                            },
                            {
                                logic: "or",
                                filters: Statusdetails
                            },
                            {
                                logic: "or",
                                filters: Riskdetails
                            },
                            {
                                logic: "or",
                                filters: userdetails
                            },
                            {
                                logic: "or",
                                filters: Actdetails
                            }
                        ]
                    });
                }

                else if (locationsdetails.length > 0
                    && Statusdetails.length > 0
                    && userdetails.length > 0
                    && Actdetails.length > 0
                    && datedetails.length > 0) {

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: locationsdetails
                            },
                            {
                                logic: "or",
                                filters: Statusdetails
                            },
                            {
                                logic: "or",
                                filters: userdetails
                            },
                            {
                                logic: "or",
                                filters: Actdetails
                            },
                            {
                                logic: "and",
                                filters: datedetails
                            }
                        ]
                    });
                }

                else if (locationsdetails.length > 0
                    && Statusdetails.length > 0
                    && Riskdetails.length > 0
                    && Actdetails.length > 0
                    && datedetails.length > 0) {

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: locationsdetails
                            },
                            {
                                logic: "or",
                                filters: Statusdetails
                            },
                            {
                                logic: "or",
                                filters: Riskdetails
                            },
                            {
                                logic: "or",
                                filters: Actdetails
                            },
                            {
                                logic: "and",
                                filters: datedetails
                            }
                        ]
                    });
                }
                else if (locationsdetails.length > 0
                    && Riskdetails.length > 0
                    && userdetails.length > 0
                    && Actdetails.length > 0
                    && datedetails.length > 0) {

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: locationsdetails
                            },
                            {
                                logic: "or",
                                filters: Riskdetails
                            },
                            {
                                logic: "or",
                                filters: userdetails
                            },
                            {
                                logic: "or",
                                filters: Actdetails
                            },
                            {
                                logic: "and",
                                filters: datedetails
                            }
                        ]
                    });
                }
                else if (Statusdetails.length > 0
                    && Riskdetails.length > 0
                    && userdetails.length > 0
                    && Actdetails.length > 0
                    && datedetails.length > 0) {

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: Statusdetails
                            },
                            {
                                logic: "or",
                                filters: Riskdetails
                            },
                            {
                                logic: "or",
                                filters: userdetails
                            },
                            {
                                logic: "or",
                                filters: Actdetails
                            },
                            {
                                logic: "and",
                                filters: datedetails
                            }
                        ]
                    });
                }
                else if (locationsdetails.length > 0
                    && Statusdetails.length > 0
                    && Riskdetails.length > 0
                    && userdetails.length > 0) {

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: locationsdetails
                            },
                            {
                                logic: "or",
                                filters: Statusdetails
                            },
                            {
                                logic: "or",
                                filters: Riskdetails
                            },
                            {
                                logic: "or",
                                filters: userdetails
                            }
                        ]
                    });
                }
                else if (locationsdetails.length > 0
                    && Riskdetails.length > 0
                    && Actdetails.length > 0
                    && datedetails.length > 0) {

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: locationsdetails
                            },
                            {
                                logic: "or",
                                filters: Riskdetails
                            },
                            {
                                logic: "or",
                                filters: Actdetails
                            },
                            {
                                logic: "and",
                                filters: datedetails
                            }
                        ]
                    });
                }

                else if (locationsdetails.length > 0
                    && Riskdetails.length > 0
                    && userdetails.length > 0
                    && datedetails.length > 0) {

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: locationsdetails
                            },
                            {
                                logic: "or",
                                filters: Riskdetails
                            },
                            {
                                logic: "or",
                                filters: userdetails
                            },
                            {
                                logic: "and",
                                filters: datedetails
                            }
                        ]
                    });
                }

                else if (Statusdetails.length > 0
                    && Riskdetails.length > 0
                    && userdetails.length > 0
                    && Actdetails.length > 0) {

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: Statusdetails
                            },
                            {
                                logic: "or",
                                filters: Riskdetails
                            },
                            {
                                logic: "or",
                                filters: userdetails
                            },
                            {
                                logic: "or",
                                filters: Actdetails
                            }
                        ]
                    });
                }

                else if (Statusdetails.length > 0
                    && Riskdetails.length > 0
                    && Actdetails.length > 0
                    && datedetails.length > 0) {

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: Statusdetails
                            },
                            {
                                logic: "or",
                                filters: Riskdetails
                            },
                            {
                                logic: "or",
                                filters: Actdetails
                            },
                            {
                                logic: "and",
                                filters: datedetails
                            }
                        ]
                    });
                }

                else if (locationsdetails.length > 0
                    && Statusdetails.length > 0
                    && userdetails.length > 0
                    && Actdetails.length > 0) {

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: locationsdetails
                            },
                            {
                                logic: "or",
                                filters: Statusdetails
                            },
                            {
                                logic: "or",
                                filters: userdetails
                            },
                            {
                                logic: "or",
                                filters: Actdetails
                            }
                        ]
                    });
                }

                else if (locationsdetails.length > 0
                    && Statusdetails.length > 0
                    && Riskdetails.length > 0
                    && datedetails.length > 0) {

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: locationsdetails
                            },
                            {
                                logic: "or",
                                filters: Statusdetails
                            },
                            {
                                logic: "or",
                                filters: Riskdetails
                            },
                            {
                                logic: "and",
                                filters: datedetails
                            }
                        ]
                    });
                }
                else if (locationsdetails.length > 0
                    && Statusdetails.length > 0
                    && Actdetails.length > 0
                    && datedetails.length > 0) {

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: locationsdetails
                            },
                            {
                                logic: "or",
                                filters: Statusdetails
                            },
                            {
                                logic: "or",
                                filters: Actdetails
                            },
                            {
                                logic: "and",
                                filters: datedetails
                            }
                        ]
                    });
                }
                else if (locationsdetails.length > 0
                    && Riskdetails.length > 0
                    && userdetails.length > 0
                    && Actdetails.length > 0) {

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: locationsdetails
                            },
                            {
                                logic: "or",
                                filters: Riskdetails
                            },
                            {
                                logic: "or",
                                filters: userdetails
                            },
                            {
                                logic: "or",
                                filters: Actdetails
                            }
                        ]
                    });
                }
                else if (locationsdetails.length > 0
                    && userdetails.length > 0
                    && Actdetails.length > 0
                    && datedetails.length > 0) {

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: locationsdetails
                            },
                            {
                                logic: "or",
                                filters: userdetails
                            },
                            {
                                logic: "or",
                                filters: Actdetails
                            },
                            {
                                logic: "and",
                                filters: datedetails
                            }
                        ]
                    });
                }
                else if (Statusdetails.length > 0
                    && userdetails.length > 0
                    && Actdetails.length > 0
                    && datedetails.length > 0) {

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: Statusdetails
                            },
                            {
                                logic: "or",
                                filters: userdetails
                            },
                            {
                                logic: "or",
                                filters: Actdetails
                            },
                            {
                                logic: "and",
                                filters: datedetails
                            }
                        ]
                    });
                }
                else if (Riskdetails.length > 0
                    && userdetails.length > 0
                    && Actdetails.length > 0
                    && datedetails.length > 0) {

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: Riskdetails
                            },
                            {
                                logic: "or",
                                filters: userdetails
                            },
                            {
                                logic: "or",
                                filters: Actdetails
                            },
                            {
                                logic: "and",
                                filters: datedetails
                            }
                        ]
                    });
                }
                else if (locationsdetails.length > 0
                    && Statusdetails.length > 0
                    && Riskdetails.length > 0) {

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: locationsdetails
                            },
                            {
                                logic: "or",
                                filters: Statusdetails
                            },
                            {
                                logic: "or",
                                filters: Riskdetails
                            }
                        ]
                    });
                }
                else if (locationsdetails.length > 0
                    && Statusdetails.length > 0
                    && userdetails.length > 0) {

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: locationsdetails
                            },
                            {
                                logic: "or",
                                filters: Statusdetails
                            },
                            {
                                logic: "or",
                                filters: userdetails
                            }
                        ]
                    });
                }
                else if (locationsdetails.length > 0
                    && Statusdetails.length > 0
                    && Actdetails.length > 0) {

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: locationsdetails
                            },
                            {
                                logic: "or",
                                filters: Statusdetails
                            },
                            {
                                logic: "or",
                                filters: Actdetails
                            }
                        ]
                    });
                }
                else if (locationsdetails.length > 0
                    && Statusdetails.length > 0
                    && datedetails.length > 0) {

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: locationsdetails
                            },
                            {
                                logic: "or",
                                filters: Statusdetails
                            },
                            {
                                logic: "and",
                                filters: datedetails
                            }
                        ]
                    });
                }
                else if (locationsdetails.length > 0
                    && Riskdetails.length > 0
                    && userdetails.length > 0) {

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: locationsdetails
                            },
                            {
                                logic: "or",
                                filters: Riskdetails
                            },
                            {
                                logic: "or",
                                filters: userdetails
                            }
                        ]
                    });
                }
                else if (locationsdetails.length > 0
                    && Riskdetails.length > 0
                    && Actdetails.length > 0) {
                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: locationsdetails
                            },
                            {
                                logic: "or",
                                filters: Riskdetails
                            },
                            {
                                logic: "or",
                                filters: Actdetails
                            }
                        ]
                    });
                }
                else if (locationsdetails.length > 0
                    && Riskdetails.length > 0
                    && datedetails.length > 0) {

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: locationsdetails
                            },
                            {
                                logic: "or",
                                filters: Riskdetails
                            },
                            {
                                logic: "and",
                                filters: datedetails
                            }
                        ]
                    });
                }
                else if (locationsdetails.length > 0
                    && userdetails.length > 0
                    && Actdetails.length > 0) {

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: locationsdetails
                            },
                            {
                                logic: "or",
                                filters: userdetails
                            },
                            {
                                logic: "or",
                                filters: Actdetails
                            }
                        ]
                    });
                }
                else if (locationsdetails.length > 0
                    && Actdetails.length > 0
                    && datedetails.length > 0) {

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: locationsdetails
                            },
                            {
                                logic: "or",
                                filters: Actdetails
                            },
                            {
                                logic: "and",
                                filters: datedetails
                            }
                        ]
                    });
                }
                else if (locationsdetails.length > 0
                    && userdetails.length > 0
                    && datedetails.length > 0) {

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: locationsdetails
                            },
                            {
                                logic: "or",
                                filters: userdetails
                            },
                            {
                                logic: "and",
                                filters: datedetails
                            }
                        ]
                    });
                }
                else if (Statusdetails.length > 0
                    && Actdetails.length > 0
                    && datedetails.length > 0) {

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: Statusdetails
                            },
                            {
                                logic: "or",
                                filters: Actdetails
                            },
                            {
                                logic: "and",
                                filters: datedetails
                            }
                        ]
                    });
                }
                else if (Statusdetails.length > 0
                    && Riskdetails.length > 0
                    && userdetails.length > 0) {

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: Statusdetails
                            },
                            {
                                logic: "or",
                                filters: Riskdetails
                            },
                            {
                                logic: "or",
                                filters: userdetails
                            }
                        ]
                    });
                }
                else if (Statusdetails.length > 0
                    && userdetails.length > 0
                    && Actdetails.length > 0) {

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: Statusdetails
                            },
                            {
                                logic: "or",
                                filters: userdetails
                            },
                            {
                                logic: "or",
                                filters: Actdetails
                            }
                        ]
                    });
                }
                else if (Riskdetails.length > 0
                    && Actdetails.length > 0
                    && datedetails.length > 0) {

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: Riskdetails
                            },
                            {
                                logic: "or",
                                filters: Actdetails
                            },
                            {
                                logic: "and",
                                filters: datedetails
                            }
                        ]
                    });
                }
                else if (Statusdetails.length > 0
                    && userdetails.length > 0
                    && datedetails.length > 0) {

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: Statusdetails
                            },
                            {
                                logic: "or",
                                filters: userdetails
                            },
                            {
                                logic: "and",
                                filters: datedetails
                            }
                        ]
                    });
                }
                else if (Statusdetails.length > 0
                    && Riskdetails.length > 0
                    && Actdetails.length > 0) {

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: Statusdetails
                            },
                            {
                                logic: "or",
                                filters: Riskdetails
                            },
                            {
                                logic: "or",
                                filters: Actdetails
                            },
                        ]
                    });
                }
                else if (Statusdetails.length > 0
                    && Riskdetails.length > 0
                    && datedetails.length > 0) {

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: Statusdetails
                            },
                            {
                                logic: "or",
                                filters: Riskdetails
                            },
                            {
                                logic: "and",
                                filters: datedetails
                            }
                        ]
                    });
                }
                else if (Riskdetails.length > 0
                    && userdetails.length > 0
                    && Actdetails.length > 0) {

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: Riskdetails
                            },
                            {
                                logic: "or",
                                filters: userdetails
                            },
                            {
                                logic: "or",
                                filters: Actdetails
                            }
                        ]
                    });
                }
                else if (Riskdetails.length > 0
                    && userdetails.length > 0
                    && datedetails.length > 0) {

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: Riskdetails
                            },
                            {
                                logic: "or",
                                filters: userdetails
                            },
                            {
                                logic: "and",
                                filters: datedetails
                            }
                        ]
                    });
                }
                else if (userdetails.length > 0
                    && Actdetails.length > 0
                    && datedetails.length > 0) {

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: userdetails
                            },
                            {
                                logic: "and",
                                filters: Actdetails
                            },
                            {
                                logic: "or",
                                filters: datedetails
                            }
                        ]
                    });
                }
                else if (locationsdetails.length > 0
                    && Statusdetails.length > 0) {

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: locationsdetails
                            },
                            {
                                logic: "or",
                                filters: Statusdetails
                            }
                        ]
                    });
                }
                else if (locationsdetails.length > 0
                    && Riskdetails.length > 0) {

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: locationsdetails
                            },
                            {
                                logic: "or",
                                filters: Riskdetails
                            }
                        ]
                    });
                }
                else if (locationsdetails.length > 0
                    && userdetails.length > 0) {

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: locationsdetails
                            },
                            {
                                logic: "or",
                                filters: userdetails
                            }
                        ]
                    });
                }

                else if (locationsdetails.length > 0
                    && Actdetails.length > 0) {

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: locationsdetails
                            },
                            {
                                logic: "or",
                                filters: Actdetails
                            }
                        ]
                    });
                }
                else if (Statusdetails.length > 0
                    && Riskdetails.length > 0) {

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: Statusdetails
                            },
                            {
                                logic: "or",
                                filters: Riskdetails
                            }
                        ]
                    });
                }
                else if (Statusdetails.length > 0
                    && userdetails.length > 0) {

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: Statusdetails
                            },
                            {
                                logic: "or",
                                filters: userdetails
                            }
                        ]
                    });
                }

                else if (Statusdetails.length > 0
                    && Actdetails.length > 0) {
                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: Statusdetails
                            },
                            {
                                logic: "or",
                                filters: Actdetails
                            }
                        ]
                    });
                }
                else if (Statusdetails.length > 0
                    && datedetails.length > 0) {

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: Statusdetails
                            },
                            {
                                logic: "and",
                                filters: datedetails
                            }
                        ]
                    });
                }
                else if (Riskdetails.length > 0
                    && userdetails.length > 0) {

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: Riskdetails
                            },
                            {
                                logic: "or",
                                filters: userdetails
                            }
                        ]
                    });
                }
                else if (Riskdetails.length > 0
                    && Actdetails.length > 0) {

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: Riskdetails
                            },
                            {
                                logic: "or",
                                filters: Actdetails
                            }
                        ]
                    });
                }
                else if (userdetails.length > 0
                    && Actdetails.length > 0) {
                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: userdetails
                            },
                            {
                                logic: "or",
                                filters: Actdetails
                            }
                        ]
                    });
                }
                else if (locationsdetails.length > 0
                    && datedetails.length > 0) {

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: locationsdetails
                            },
                            {
                                logic: "and",
                                filters: datedetails
                            }
                        ]
                    });
                }
                else if (userdetails.length > 0
                    && datedetails.length > 0) {
                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: userdetails
                            },
                            {
                                logic: "and",
                                filters: datedetails
                            }
                        ]
                    });
                }
                else if (Riskdetails.length > 0
                    && datedetails.length > 0) {

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: Riskdetails
                            },
                            {
                                logic: "and",
                                filters: datedetails
                            }
                        ]
                    });
                }
                else if (Actdetails.length > 0
                    && datedetails.length > 0) {
                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: Actdetails
                            },
                            {
                                logic: "and",
                                filters: datedetails
                            }
                        ]
                    });
                }
                else if (Actdetails.length > 0) {
                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: Actdetails
                            }
                        ]
                    });
                }
                else if (datedetails.length > 0) {

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "and",
                                filters: datedetails
                            }
                        ]
                    });
                }

                else if (locationsdetails.length > 0) {
                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: locationsdetails
                            }
                        ]
                    });
                }
                else if (userdetails.length > 0) {

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: userdetails
                            }
                        ]
                    });
                }
                else if (Riskdetails.length > 0) {

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: Riskdetails
                            }
                        ]
                    });
                }

                else if (Statusdetails.length > 0) {

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: Statusdetails
                            }
                        ]
                    });
                }
                else {
                    $("#grid1").data("kendoGrid").dataSource.filter({});
                }
            }
            $("#dropdowntree1").kendoDropDownTree({
                placeholder: "Entity/Sub-Entity/Location",
                checkboxes: {
                    checkChildren: true
                },
                checkAll: true,
                autoClose: false,
                dataTextField: "Name",
                dataValueField: "ID",
                change: function (e) {
                    settracknew('Act Document', 'Filtering', 'Location', '');
                    FilterAllAdvancedSearch();
                    fCreateStoryBoard('dropdowntree1', 'filtersstoryboard1', 'loc1')
                    $('input[id=chkAll]').prop('checked', false);
                    $('#dvbtndownloadDocument').css('display', 'none');
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Falg%>&IsStatutoryInternal=S',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Falg%>&IsStatutoryInternal=S"
                    },
                    schema: {
                        data: function (response) {
                            return response[0].locationList;
                        },
                        model: {
                            children: "Children"
                        }
                    }
                }
            });

            $("#grid tbody").on("click", "tr", function (e) {
                var rowElement = this;
                var row = $(rowElement);
                var grid = $("#grid").getKendoGrid();
                if (row.hasClass("k-state-selected")) {
                    var selected = grid.select();
                    selected = $.grep(selected, function (x) {
                        var itemToRemove = grid.dataItem(row);
                        var currentItem = grid.dataItem(x);
                        return itemToRemove.ID != currentItem.ID
                    })
                    grid.clearSelection();
                    grid.select(selected);
                } else {
                    grid.select(row)
                }
            });

            $(document).on("click", "#grid tbody tr .ob-edit", function (e) {
                var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
                OpenDocumentOverviewpup(item.ActID)
                return true;
            });

            $(document).on("click", "#grid tbody tr .ob-download", function (e) {
                var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
                OpenDownloadOverviewpup(item.ActID)
                return true;
            });

            $(document).on("click", "#grid tbody tr .ob-delete", function (e) {
                var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
                e.preventDefault();
                return true;
            });

            $(document).on("click", "#grid1 tbody tr .ob-download", function (e) {
                var item = $("#grid1").data("kendoGrid").dataItem($(this).closest("tr"));
                OpenDownloadOverviewpup(item.ActID)
                return true;
            });

            $(document).on("click", "#chkAll", function (e) {
                if ($('input[id=chkAll]').prop('checked')) {

                    $('input[name="sel_chkbx"]').each(function (i, e) {
                        e.click();
                    });
                }
                else {
                    $('input[name="sel_chkbx"]').attr("checked", false);
                }
                if (($('input[name="sel_chkbx"]:checked').length) == 0) {
                    $('#dvbtndownloadDocument').css('display', 'none');
                }
                else {
                    $('#dvbtndownloadDocument').css('display', 'block');
                }
                return true;
            });

            $(document).on("click", "#chkAllMain", function (e) {
                if ($('input[id=chkAllMain]').prop('checked')) {
                    $('input[name="sel_chkbxMain"]').each(function (i, e) {
                        e.click();
                    });
                }
                else {
                    $('input[name="sel_chkbxMain"]').attr("checked", false);
                }
                if (($('input[name="sel_chkbxMain"]:checked').length) == 0) {
                    $('#dvbtndownloadDocumentMain').css('display', 'none');
                }
                else {
                    $('#dvbtndownloadDocumentMain').css('display', 'block');
                }
                return true;
            });

            $(document).on("click", "#sel_chkbx", function (e) {
                if (($('input[name="sel_chkbx"]:checked').length) == 0) {
                    $('#dvbtndownloadDocument').css('display', 'none');
                }
                else {
                    $('#dvbtndownloadDocument').css('display', 'block');
                }
                return true;
            });

            $(document).on("click", "#sel_chkbxMain", function (e) {
                if (($('input[name="sel_chkbxMain"]:checked').length) == 0) {
                    $('#dvbtndownloadDocumentMain').css('display', 'none');
                }
                else {
                    $('#dvbtndownloadDocumentMain').css('display', 'block');
                }
                return true;
            });
        });

        function selectedDocument(e) {
            if (($('input[name="sel_chkbx"]:checked').length) == 0) {
                return;
            }
            var checkboxlist = [];
            $('input[name="sel_chkbx"]').each(function (i, e) {
                if ($(e).is(':checked')) {
                    checkboxlist.push(e.value);
                }
            });
            console.log(checkboxlist.join(","));
            $('#downloadfile').attr('src', "../ComplianceDocument/DownloadDoc.aspx?ComplianceScheduleID=" + checkboxlist.join(",") + "&IsFlag=" + $("#dropdownlistComplianceType1").val());
            return false;
        }
        function selectedDocumentMain(e) {

            e.preventDefault();
            if (($('input[name="sel_chkbxMain"]:checked').length) == 0) {

                return;
            }
            var checkboxlist = [];
            $('input[name="sel_chkbxMain"]').each(function (i, e) {
                if ($(e).is(':checked')) {
                    checkboxlist.push(e.value);
                }
            });
            console.log(checkboxlist.join(","));
            $('#downloadfile').attr('src', "../ComplianceDocument/DownloadDoc.aspx?ComplianceScheduleID=" + checkboxlist.join(",") + "&IsFlag=" + $("#dropdownlistComplianceType").val());
            return false;
        }

        function ClearAllFilterMain(e) {
            $("#dropdowntree").data("kendoDropDownTree").value([]);
            $("#dropdownlistRisk").data("kendoDropDownTree").value([]);
            $("#dropdownlistStatus").data("kendoDropDownTree").value([]);
            $('#ClearfilterMain').css('display', 'none');

            $('#dvbtndownloadDocumentMain').css('display', 'none');

            $("#dropdownEventName").data("kendoDropDownList").select(0);
            $("#dropdownEventNature").data("kendoDropDownList").select(0);

            $("#grid").data("kendoGrid").dataSource.filter({});

            $('input[id=chkAllMain]').prop('checked', false);
            e.preventDefault();
        }

        function ClearAllFilter(e) {
            $("#dropdownEventName1").data("kendoDropDownList").select(0);
            $("#dropdownEventNature1").data("kendoDropDownList").select(0);
            $("#dropdownACT").data("kendoDropDownList").select(0);
            $("#dropdowntree1").data("kendoDropDownTree").value([]);
            $("#dropdownlistRisk1").data("kendoDropDownTree").value([]);
            $("#dropdownUser").data("kendoDropDownTree").value([]);
            $("#dropdownlistStatus1").data("kendoDropDownTree").value([]);
            $("#Startdatepicker").data("kendoDatePicker").value(null);
            $("#Lastdatepicker").data("kendoDatePicker").value(null);
            $('#filterStartDate').html('');
            $('#filterLastDate').html('');
            $('#filterStartDate').css('display', 'none');
            $('#filterLastDate').css('display', 'none');
            $('#Clearfilter').css('display', 'none');

            $('#dvbtndownloadDocument').css('display', 'none');

            $("#grid1").data("kendoGrid").dataSource.filter({});


            $('input[id=chkAll]').prop('checked', false);
            e.preventDefault();
        }

        function fcloseStory(obj) {

            var DataId = $(obj).attr('data-Id');
            var dataKId = $(obj).attr('data-K-Id');
            var seq = $(obj).attr('data-seq');
            var deepspan = $('#' + DataId + ' > li > span > span.k-i-close')[seq];
            $(deepspan).trigger('click');
            var upperli = $('#' + dataKId);
            $(upperli).remove();

            fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc');
            fCreateStoryBoard('dropdownlistRisk', 'filterrisk', 'risk');
            fCreateStoryBoard('dropdownlistStatus', 'filterstatus', 'status');
            fCreateStoryBoard('dropdowntree1', 'filtersstoryboard1', 'loc1');
            fCreateStoryBoard('dropdownlistStatus1', 'filterstatus1', 'status1');
            fCreateStoryBoard('dropdownlistRisk1', 'filterrisk1', 'risk1');
            fCreateStoryBoard('dropdownUser', 'filterUser', 'user');

            CheckFilterClearorNot();
            CheckFilterClearorNotMain();
        };

        function CheckFilterClearorNotMain() {
            if (($($($('#dropdowntree').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
                ($($($('#dropdownlistRisk').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
                ($($($('#dropdownlistStatus').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0)) {
                $('#ClearfilterMain').css('display', 'none');
            }
        }

        function CheckFilterClearorNot() {
            if (($($($('#dropdowntree1').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
                ($($($('#dropdownlistRisk1').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
                ($($($('#dropdownUser').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
                ($($($('#dropdownlistStatus1').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0)) {
                $('#Clearfilter').css('display', 'none');
            }
        }

        function fCreateStoryBoard(Id, div, filtername) {

            var IdKReset = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('id');
            $('#' + div).html('');
            $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '28px');
            $('#' + div).css('display', 'block');

            if (div == 'filtersstoryboard') {
                $('#' + div).append('Location&nbsp;&nbsp;&nbsp;:');//Dashboard
                $('#ClearfilterMain').css('display', 'block');
            }
            else if (div == 'filtertype') {
                $('#' + div).append('Type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');//Dashboard               
            }
            else if (div == 'filterrisk') {
                $('#' + div).append('Risk&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');//Dashboard
                $('#ClearfilterMain').css('display', 'block');
            }
            else if (div == 'filterstatus') {
                $('#' + div).append('Status&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');//Dashboard
                $('#ClearfilterMain').css('display', 'block');
            }
            else if (div == 'filterpstData1') {
                $('#' + div).append('Time&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filterCategory') {
                $('#' + div).append('Category&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filterAct') {
                $('#' + div).append('ACT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filterCompSubType') {
                $('#' + div).append('SubType&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filterCompType') {
                $('#' + div).append('type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filtersstoryboard1') {
                $('#' + div).append('Location&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filtertype1') {
                $('#' + div).append('Type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filterrisk1') {
                $('#' + div).append('Risk&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filterFY') {
                $('#' + div).append('FY&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filterUser') {
                $('#' + div).append('User&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filterstatus1') {
                $('#' + div).append('Status&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }

            for (var i = 0; i < $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length; i++) {
                var button = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button')[i];
                $(button).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('placeholder', 'Shree');
                var buttontest = $($(button).find('span')[0]).text();
                if (buttontest.length > 10) {
                    buttontest = buttontest.substring(0, 10).concat("...");
                }
                $('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="background-color:white; height: 20px;Color:white;"><span unselectable="on">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory(this)" title="delete" aria-label="delete" class="k-select"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory(this)"  class="k-icon k-i-close"></span></span></li>');
            }

            if ($($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) {
                $('#' + div).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');

            }
            CheckFilterClearorNot();
            CheckFilterClearorNotMain();
        }

        function OpenAdvanceSearch(e) {

            var myWindowAdv = $("#divAdvanceSearchModel");

            function onClose() {

            }

            myWindowAdv.kendoWindow({
                width: "85%",
                height: "85%",
                title: "Advanced Search",
                visible: false,
                actions: [
                    // "Maximize",
                    "Close"
                ],

                close: onClose
            });

            $("#divAdvanceSearchModel").data("kendoWindow").wrapper.addClass("myKendoCustomClass");

            myWindowAdv.data("kendoWindow").center().open();
            e.preventDefault();
            return false;
        }

        function OpenAdvanceSearchFilter(e) {
            $('#divAdvanceSearchFilterModel').modal('show');
            e.preventDefault();
            return false;
        }

        function ChangeView() {
            $('#grid1').css('display', 'block');
            $("#grid1").data("kendoGrid").showColumn(0);//Id
            $("#grid1").data("kendoGrid").hideColumn(1);//Risk
            $("#grid1").data("kendoGrid").hideColumn(2);//CId
            $("#grid1").data("kendoGrid").hideColumn(3);//FileName
            $("#grid1").data("kendoGrid").hideColumn(4);//V
            $("#grid1").data("kendoGrid").showColumn(5);//Branch
            $("#grid1").data("kendoGrid").showColumn(6);//SD
            $("#grid1").data("kendoGrid").hideColumn(7);//Event Name
            $("#grid1").data("kendoGrid").hideColumn(8);//Event Nature
            $("#grid1").data("kendoGrid").showColumn(9);//Scheduleon
            $("#grid1").data("kendoGrid").showColumn(10);//ForMonth
            $("#grid1").data("kendoGrid").showColumn(11);//Status
            $("#grid1").data("kendoGrid").hideColumn(12);//VD
            $("#grid1").data("kendoGrid").hideColumn(13);//type
            $("#grid1").data("kendoGrid").hideColumn(14);//Uploaded Date
            $("#grid1").data("kendoGrid").hideColumn(15);//Size
            $("#grid1").data("kendoGrid").hideColumn(16);//Size

            if ($("#dropdownlistComplianceType1").val() == 1)//event based
            {
                $("#grid1").data("kendoGrid").showColumn(7);//Event Name
                $("#grid1").data("kendoGrid").showColumn(8);//Event Nature

                $("#grid1").data("kendoGrid").hideColumn(5);//Branch
                $("#grid1").data("kendoGrid").hideColumn(10);//ForMonth
            }
        }

        function ChangeListView() {
            $('#grid1').css('display', 'block');
            //  $('#grid2').css('display', 'none');
            $("#grid1").data("kendoGrid").showColumn(0);//ActID
            $("#grid1").data("kendoGrid").showColumn(1);//ActName
            $("#grid1").data("kendoGrid").hideColumn(2);//Description
            $("#grid1").data("kendoGrid").showColumn(3);//ComplianceTypeName
            $("#grid1").data("kendoGrid").hideColumn(4);//ComplianceCategoryId
            $("#grid1").data("kendoGrid").showColumn(5);//ComplianceCategoryName
            $("#grid1").data("kendoGrid").hideColumn(6);//StateName

        }

        function ChangeAuditQView() {
            $('#grid1').css('display', 'none');
        }

        function exportReport() {
            $("#grid").getKendoGrid().saveAsExcel();
            return false;
        };


        function OpenDownloadOverviewpup(ActID) {
            settracknew('Act Document', 'Action', 'Download', '')
            $('#divDownloadView').modal('show');
            $('#DownloadViews').attr('width', '600px');
            $('.modal-dialog').css('width', '650px');
            $('#DownloadViews').attr('src', "../Common/DownloadActOverview.aspx?ActID=" + ActID);
        }

        function OpenDocumentOverviewpup(ActID) {
            settracknew('Act Document', 'Action', 'Overview', '')

            $('#divOverView').modal('show');
            $('#OverViews').attr('width', '1150px');
            $('#OverViews').attr('height', '600px');
            $('.modal-dialog').css('width', '1200px');
            $('#OverViews').attr('src', "../Common/ActOverview.aspx?ActID=" + ActID);
        }

        $("#newModelClose").on("click", function () {
            myWindow3.close();
        });

        function CloseClearPopup() {
            $('#OverViews1').attr('src', "../Common/blank.html");
        }

        function CloseClearOV() {
            $('#OverViews').attr('src', "../Common/blank.html");
        }

        function CloseClearDV() {
            $('#DownloadViews').attr('src', "../Common/blank.html");
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="example">
        <div id="grid"></div>
        <div>

            <div class="modal fade" id="divDownloadView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                <div class="modal-dialog" style="width: 650px;">
                    <div class="modal-content" style="width: 100%; height: 100%">
                        <div class="modal-header" style="border-bottom: none;">
                            <button type="button" class="close" data-dismiss="modal" onclick="CloseClearDV();" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body">
                            <iframe id="DownloadViews" src="about:blank" width="500px" height="350px" frameborder="0"></iframe>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="divOverView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                <div class="modal-dialog" style="width: 1150px;">
                    <div class="modal-content" style="width: 100%;">
                        <div class="modal-header" style="border-bottom: none;">
                            <button type="button" class="close" data-dismiss="modal" onclick="CloseClearOV();" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body">
                            <iframe id="OverViews" src="about:blank" width="1200px" height="100%" frameborder="0"></iframe>
                        </div>
                    </div>
                </div>
            </div>


            <div id="divAdvanceSearchModel" style="padding-top: 5px;">

                <div class="row">
                    <div class="col-md-10" style="padding-left: 0px; padding-bottom: 4px;">
                        <button id="primaryTextButton1" onclick="ChangeView()">Grid View</button>
                        <button id="primaryTextButton" onclick="ChangeListView()">List View</button>
                        <%-- <button id="primaryTextButton2" onclick="ChangeAuditQView()">IKEA Audit</button>--%>
                    </div>
                </div>
                <div class="row" style="margin-left: -9px;">
                    <div class="col-md-12 colpadding0">
                        <div class="col-md-2" id="dvdropdowntree1" style="width: 20%; padding-left: 9px;">
                            <input id="dropdowntree1" data-placeholder="Entity/Sub-Entity/Location" style="width: 100%;" />
                        </div>
                        <div class="col-md-2" id="dvdropdownFY" style="width: 15%; padding-left: 0px;">
                            <input id="dropdownFY" data-placeholder="Finance Year" style="width: 102%;" />
                        </div>
                        <div class="col-md-2" id="dvdropdownUser" style="width: 13%; padding-left: 4px;">
                            <input id="dropdownlistStatus1" data-placeholder="Status" style="width: 100%;" />
                        </div>

                        <div class="col-md-2" id="dvdropdownlistRisk1" style="width: 15%; padding-left: 0px;">
                            <input id="dropdownlistRisk1" data-placeholder="Risk" style="width: 100%;" />
                        </div>
                        <div class="col-md-2" id="dvStartdatepicker" style="width: 15%; padding-left: 0px;">
                            <input id="Startdatepicker" placeholder="Start Date" cssclass="clsROWgrid" title="startdatepicker" style="width: 100%;" />
                        </div>
                        <div class="col-md-2" id="dvLastdatepicker" style="width: 13%; padding-left: 0px;">
                            <input id="Lastdatepicker" placeholder="End Date" title="enddatepicker" style="width: 115%;" />
                        </div>
                    </div>
                </div>

                <div class="row" style="margin-left: -9px; margin-top: 7px; margin-bottom: 5px;">
                    <div class="col-md-12 colpadding0">
                        <div class="col-md-2" id="dvdropdownComplianceSubType" style="width: 20%; padding-left: 9px;">
                            <input id="dropdownPastData" style="width: 100%;" />
                        </div>
                        <div class="col-md-2" id="dvdropdownlistComplianceType1" style="width: 15.3%; padding-left: 0px;">
                            <input id="dropdownlistComplianceType1" data-placeholder="Type" style="width: 100%;" />
                        </div>
                        <div class="col-md-2" style="width: 13%; padding-left: 0px;">
                            <input id="SearchTag" type="text" style="width: 100%;" class="k-textbox" placeholder="Document Tag" />
                        </div>
                        <div class="col-md-4" id="dvdropdownACT" style="width: 31.3%; padding-left: 0px;">
                            <input id="dropdownACT" data-placeholder="Act" style="width: 100%;" />
                        </div>
                        <div class="col-md-2" style="width: 13.4%; padding-left: 0px;" id="dvdropdownlistStatus1">

                            <input id="dropdownUser" data-placeholder="User" style="width: 112%;" />
                        </div>

                    </div>
                </div>


                <div class="row" style="padding-bottom: 5px;">
                    <div class="col-md-12">
                        <div class="col-md-2" style="width: 16.6%;">
                        </div>
                        <div class="col-md-2" style="width: 14.3%;">
                            <div id="dvdropdownEventName1" style="display: none;">
                                <input id="dropdownEventName1" data-placeholder="Event Name" style="width: 196px;">
                            </div>
                        </div>
                        <div class="col-md-2" style="width: 10%;">
                            <div id="dvdropdownEventNature1" style="display: none;">
                                <input id="dropdownEventNature1" data-placeholder="Event Nature" style="width: 166px;">
                            </div>
                        </div>
                        <div class="col-md-2" style="width: 3%;">
                        </div>
                        <div class="col-md-1" style="width: 37%; padding-left: 105px;">
                            <button id="Clearfilter" style="float: right; margin-left: 1%; display: none;" onclick="ClearAllFilter(event)"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear Filter</button>
                            <button id="dvbtndownloadDocument" style="float: right; display: none;" onclick="selectedDocument(event)">Download</button>
                        </div>
                    </div>
                </div>



                <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a;" id="filterCompType">&nbsp;</div>
                <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a;" id="filterCategory">&nbsp;</div>
                <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a;" id="filterAct">&nbsp;</div>
                <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a;" id="filterCompSubType">&nbsp;</div>
                <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a;" id="filterStartDate">&nbsp;</div>
                <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a;" id="filterLastDate">&nbsp;</div>
                <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a;" id="filtersstoryboard1">&nbsp;</div>
                <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a;" id="filtertype1">&nbsp;</div>
                <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a;" id="filterrisk1">&nbsp;</div>

                <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a;" id="filterpstData1">&nbsp;</div>
                <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a;" id="filterUser">&nbsp;</div>
                <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a;" id="filterFY">&nbsp;</div>
                <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a;" id="filterstatus1">&nbsp;</div>


                <div id="grid1"></div>
            </div>
            </div>
        </div>
            <script type="text/javascript">
                $(document).ready(function () {

                    fhead('My Documents / Act Documents');
                    setactivemenu('ComplianceDocumentList');
                    fmaters()
                });
            </script>
</asp:Content>
