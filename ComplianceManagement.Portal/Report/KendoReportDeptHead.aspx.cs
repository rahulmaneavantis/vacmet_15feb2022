﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Web;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Report
{
    public partial class KendoReportDeptHead : System.Web.UI.Page
    {
        protected static int UID;
        protected static int CustId;
        protected static string CName;
        protected static string DiagraphPath;
        protected static string UserIdDiagraph;
        protected static string PassIdDiagraph;
        protected static string UserRole;
        protected static string Path;
        protected static int StatusFlag;
        protected static string IsApprover;
        protected static string Authorization;
        protected void Page_Load(object sender, EventArgs e)
        {
            int CacheClearTime = Convert.ToInt32(ConfigurationManager.AppSettings["CacheClearTimeToken"]);
            string CacheName = "CacheGetTokenData_" + Convert.ToString(AuthenticationHelper.UserID) + "_" + Convert.ToString(AuthenticationHelper.CustomerID);
            Authorization = (string)HttpContext.Current.Cache[CacheName];
            if (Authorization == null)
            {
                Authorization = Business.ComplianceManagement.getToken(Convert.ToString(AuthenticationHelper.UserID));
                HttpContext.Current.Cache.Insert(CacheName, Authorization, null, DateTime.Now.AddMinutes(CacheClearTime), System.Web.Caching.Cache.NoSlidingExpiration); // add it to cache
            }
            if (!IsPostBack)
            {
                Path = ConfigurationManager.AppSettings["KendoPathApp"];
                StatusFlag = -1;
                IsApprover = "False";

                UID = Convert.ToInt32(AuthenticationHelper.UserID);
                CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                CName = Convert.ToString(RoleManagement.GetCustNameByID(CustId));

                DiagraphPath = Convert.ToString(ConfigurationManager.AppSettings["KendoGraphPath"]);
                UserIdDiagraph = Convert.ToString(ConfigurationManager.AppSettings["KendoGraphUserID"]);
                PassIdDiagraph = Convert.ToString(ConfigurationManager.AppSettings["KendoGraphPass"]);

                UserRole = Convert.ToString(AuthenticationHelper.Role);

                var ComprehensiveReport = GetDetailReports(UID, "ComprehensiveReport");
                if (ComprehensiveReport.Count <= 3)
                {
                    lbgotoReport.Visible = false;
                }

                var CategoryReport = GetDetailReports(UID, "CategoryReport");
                if (CategoryReport.Count <= 3)
                {
                    LinkButton3.Visible = false;
                }

                var LocationReport = GetDetailReports(UID, "LocationReport");
                if (LocationReport.Count <= 3)
                {
                    LinkButton1.Visible = false;
                }

                var UserReport = GetDetailReports(UID, "UserReport");
                if (UserReport.Count <= 3)
                {
                    LinkButton2.Visible = false;
                }

                var RiskReport = GetDetailReports(UID, "RiskReport");
                if (RiskReport.Count <= 3)
                {
                    LinkButton4.Visible = false;
                }

                var Detaildeport = GetDetailReports(UID, "DetailReport");
                if (Detaildeport.Count <= 3)
                {
                    LinkButton5.Visible = false;
                }
            }
        }

        public static List<report_generate_log> GetDetailReports(int UID, string PDFName)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.report_generate_log
                            where row.user_id == UID
                            && row.pdf_name.Equals(PDFName)
                            select row).ToList();
                return data;
            }
        }

        protected string GetDate(DateTime? Createdate)
        {
            try
            {
                string result = "";

                if (!string.IsNullOrEmpty(Createdate.ToString()))
                {
                    DateTime checkDate = Convert.ToDateTime(Createdate);

                    result = "Week of " + String.Format("{0:dd MMM yyyy}", checkDate);
                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }

        }
    }
}