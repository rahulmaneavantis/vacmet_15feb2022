﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NewCompliance.Master" AutoEventWireup="true" 
    CodeBehind="KendoReportNew_Old.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Report.KendoReportNew_Old" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
  
        <link href="../NewCSS/kendo.common.min.css" rel="stylesheet" />
        <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
        <link href="../NewCSS/kendo.silver.min.css" rel="stylesheet" />
        <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

        <%--<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.2.620/styles/kendo.common.min.css" />
        <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.2.620/styles/kendo.rtl.min.css" />
        <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.2.620/styles/kendo.silver.min.css" />
        <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.2.620/styles/kendo.mobile.all.min.css" />--%>
      
<%--        <script type="text/javascript" src="https://www.amcharts.com/lib/3/ammap.js"></script>
        <script type="text/javascript" src="https://www.amcharts.com/lib/3/themes/light.js"></script>
        <script type="text/javascript" src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
 
        <script type="text/javascript" src="https://kendo.cdn.telerik.com/2018.2.620/js/kendo.all.min.js"></script>--%>

       <script type="text/javascript" src="../js/telerikReportViewer.kendo-12.1.18.620.js"></script>
        <script type="text/javascript" src="../js/telerikReportViewer-12.1.18.620.js"></script>
    <script type="text/javascript">
            $(document).ready(function() {

                fhead('My Reports / Standard Report');
                setactivemenu('Kendoreport');
                fmaters1();

                //$("#sidebar2").kendoSortable({
                //    filter: ">div",
                //    cursor: "move",
                //    connectWith: "#sidebar4,#sidebar2",
                //    placeholder: placeholder,
                //    hint: hint
                //});
                    
                
                //$("#sidebar4").kendoSortable({
                //    filter: ">div",
                //    cursor: "move",
                //    connectWith: "#sidebar2,#sidebar4",
                //    placeholder: placeholder,
                //    hint: hint
                //});
                    
                //$("#sidebar3").kendoSortable({
                //    filter: ">div",
                //    cursor: "move",
                //    placeholder: placeholder,
                //    hint: hint
                //});
                //$("#mainsummery").kendoSortable({
                //    filter: ">div",
                //    cursor: "move",
                //    placeholder: placeholder,
                //    hint: hint
                //});

                //exapand
                $(".panel-wrap").on("click", "span.k-i-sort-desc-sm", function(e) {
                    var contentElement = $(e.target).closest(".widget").find(">div");
                    $(e.target)
                        .removeClass("k-i-sort-desc-sm")
                        .addClass("k-i-sort-asc-sm");

                    kendo.fx(contentElement).expand("vertical").stop().play();
                });

                //collapse
                $(".panel-wrap").on("click", "span.k-i-sort-asc-sm", function(e) {
                    var contentElement = $(e.target).closest(".widget").find(">div");
                    $(e.target)
                        .removeClass("k-i-sort-asc-sm")
                        .addClass("k-i-sort-desc-sm");

                    kendo.fx(contentElement).expand("vertical").stop().reverse();
                });
            });

            function placeholder(element) {
                return element.clone().addClass("placeholder");
            }

            function hint(element) {
                return element.clone().addClass("hint")
                            .height(element.height())
                            .width(element.width());
            }
        </script>

        <style>

           
           
           
            .m-t{
                margin-top:38px;
            }
            .mr10 {
                margin-left: 15px;
            }

            .ml5 {
                margin-left: 10px;
            }

            .map {
            }

            .columnchart {
            }

            #example, #example1 {
                -webkit-user-select: none;
                -moz-user-select: none;
                -ms-user-select: none;
                user-select: none;
            }
       .dash-head {
                width: 970px;
                height: 80px;
                /*background: url('../content/web/sortable/dashboard-head.png') no-repeat 50% 50% #222222;*/
            }

            .panel-wrap {
                display: table;
                /* width: 968px;
                  background-color: #f5f5f5;
                    border: 1px solid #e5e5e5;*/
            }

            /*#sidebar2 #sidebar3 {
                display: table-cell;
                
                vertical-align: top;
            }*/



            .widget.placeholder {
                opacity: 0.4;
                border: 1px dashed #a6a6a6;
            }

            /* WIDGETS */
            .widget {
              
                padding: 0;
                background-color: #ffffff;
             
                border-radius: 3px;
                cursor: move;
            }

                .widget:hover {
                    background-color: #fcfcfc;
                    border-color: #cccccc;
                }

                .widget div {
                    /*padding: 10px;
                    min-height: 50px;*/
                }

                .widget h3 {
                    margin-top: 0px;
                    margin-bottom: 0px;
                    font-size: 23px;
                    padding: 8px 10px;
                    /*text-transform: uppercase;*/
                    border-bottom: 1px solid #e7e7e7;
                }

                    .widget h3 span {
                        float: right;
                    }

                        .widget h3 span:hover {
                            cursor: pointer;
                            background-color: #e7e7e7;
                            border-radius: 20px;
                        }

            /* PROFILE */
            .profile-photo {
                /*width: 80px;
                    height: 80px;*/
                /*margin: 10px auto;*/
                border-radius: 100px;
                border: 1px solid #e7e7e7;
                background: url('../content/web/Customers/ISLAT.jpg') no-repeat 50% 50%;
            }

            #profile div {
                /*text-align: center;*/
            }

            #profile #profile1 h4 {
                /*width: auto;
                    margin: 0 0 5px;
                    font-size: 1.2em;*/
                color: #1f97f7;
            }

            #profile p {
                /*margin: 0 0 10px;*/
            }



            /* BLOGS & NEWS */
            /*#blogs,
            #news,#profile1,#teammates1,#teammates2,#teammates3,#teammates4,#teammates5 {
                margin-bottom:15px;
              margin-right:15px;
            }*/


            #teammates teammates1 teammates2 h4,
            #blogs h4,
            #news h4 {
                width: auto;
                font-size: 1.4em;
                color: #1f97f7;
                font-weight: normal;
            }

            .blog-info {
                font-size: .9em;
                color: #787878;
            }

            /*#sidebar2 #sidebar3 #blogs h4 {
                font-size: 1em;
            }

            #sidebar2 #sidebar3 #blogs p {
                display: none;
            }

            #sidebar2 #blogs .blog-info {
                display: block;
            }*/

            /*#sidebar4 {
               
            }*/

            #mainsummery #news h4 {
                font-size: 1.2em;
                line-height: 1.4em;
                height: 40px;
            }

                #mainsummery #news h4 span {
                    display: block;
                    float: left;
                    width: 100px;
                    height: 40px;
                    color: #000;
                }

            #teammates1, #teammates2, #teammates3, #teammates4, #teammates5, #teammates6 {
                margin-left: 15px;
                margin-bottom: 15px;
            }
            /* TEAMMATES */
            .team-mate:after {
                content: ".";
                display: block;
                height: 0;
                line-height: 0;
                clear: both;
                visibility: hidden;
            }
            /*#teammates1, #teammates2,  #teammates5{
                 margin-right:15px;
                 margin-bottom:15px;
             }*/
            #teammates #teammates1 #teammates2 #teammates3 #teammates4 #teammates5 .team-mate h4 {
                font-size: 1.4em;
                font-weight: normal;
                /*margin-top: 12px;*/
            }

            .team-mate p {
            }

            .team-mate img {
                float: left;
                margin: 0 15px 0 0;
                border: 1px solid #e7e7e7;
                border-radius: 60px;
            }

            .hint {
                width: 250px;
                height: 100px;
                overflow: hidden;
            }

                .hint > h3 {
                    padding-left: 20px;
                }

            .form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
                cursor: not-allowed;
                background-color: #ffffff;
                color: #000000;
            }

            .select_Date {
                width: 100%;
                margin-right: 0;
            }

            .icongcalender {
                color: #666;
            }

            .m10 {
                margin-left: 10px;
            }

            .k-icon-15 {
                font-size: 15px; /* Sets icon size to 32px */
            }
            .Dashboard-white-widget {
                background: none;
            }

            .pointimg {
                width: 31.5%;
                cursor: pointer;
                padding: 8px;
                border-radius: 12px;
            }
            .imgheader {
                 margin-top: 6px;
                 color:#0487c4;
                 /*font-size: 30px;*/
            }
            .keyFeature {
                  margin-top: 10px;
                  font-size: 18px;
                  padding-bottom: 10px;
                  padding-left:10px;
                  color:black;
                      /*border-top: 1px solid #e7e7e7;*/
            }
          
            .set_bullet {
                 list-style: disc;
            }

            #teammates121, #teammates2, #teammates3, #teammates412, #teammates5 {
                -webkit-box-shadow: 0px 3px 2px 0px rgba(142,143,153,1);
                -moz-box-shadow: 0px 3px 2px 0px rgba(142,143,153,1);
                box-shadow: 0px 3px 2px 0px rgba(142,143,153,1);
            }

            .imgbackshowd {
                -webkit-box-shadow: 1px 2px 6px 2px rgba(142,143,153,0.5);
    -moz-box-shadow: 1px 2px 6px 2px rgba(142,143,153,0.5);
    box-shadow: 1px 2px 6px 2px rgba(142,143,153,0.5);
    padding-left: -11px;
    /* border: 2px solid #e2dcd4; */
    /* border-color: #e2dcd4; */
    /* border-width: thin; */
    /* -webkit-box-shadow: 1px 4px 6px 3px rgba(142,143,153,0.6); */
    -moz-box-shadow: 1px 4px 6px 3px rgba(142,143,153,0.6);
    /* box-shadow: 1px 4px 6px 3px rgba(142,143,153,0.6); */
            }

        </style>
  <script type="text/javascript">
      $(document).ready(function () {
            report();
            locationreport();
            userreport();
            categoryreport();
            Summaryreport();
      });
        


      //Risk
      $(".close-button").click(function () {
          $("#windowriskpopup").close();
      });
      function OpenstatupWindow() {    
          
          $('#windowriskpopup').show();
          var myWindowAdv = $("#windowriskpopup");
          function onClose() {           
              myWindowAdv.data("kendoWindow").hide();
          }
          myWindowAdv.kendoWindow({
              width: "87%",
              height: "87%",             
              title: "Overall Risk Report",
              visible: false,
              actions: [
                  "Pin",
                  "Close"
              ],
              close: function() {               
              },
          });
          //report();
           myWindowAdv.data("kendoWindow").center().open();

      }

      function report() {
         
       
          $("#windowriskpopup").telerik_ReportViewer({

            
                 serviceUrl: "<% =DiagraphPath%>",              
                   	reportServer: {
						url: "<% =DiagraphPath%>",
						username: "<% =UserIdDiagraph%>",
						password: "<% =PassIdDiagraph%>"
					},
                    reportSource: {
                        report: "Samples/FFRiskSummaryReport280918",
                        parameters: { Customerid:<% =CustId%>,UserID:<% =UID%>,CustomerName:"<% =CName%>",Flag:"<% =UserRole%>" }
                        //parameters: { Customerid:<% =CustId%>, UserID:<% =UID%> }
                    },
                    viewMode: telerikReportViewer.ViewModes.INTERACTIVE,
                    scaleMode: telerikReportViewer.ScaleModes.SPECIFIC,
                    scale: 1.0,
					enableAccessibility: true,
                    ready: function () {
                        //this.refreshReport();
                    },
                });
      }


     // location

       function OpenstatupLocationWindow() {
          $('#windowlocationpopup').show();
          var myWindowAdv = $("#windowlocationpopup");
          function onClose() {

          }
          myWindowAdv.kendoWindow({
              width: "87%",
              height: "87%",             
              title: "Overall Location Report",
              visible: false,
              actions: [
                  "Pin",
                  "Close"
              ],
              close: onClose
          });
          //locationreport();
          myWindowAdv.data("kendoWindow").center().open();

          return false;
      }

      function locationreport() {        
           $("#windowlocationpopup").telerik_ReportViewer({
                  serviceUrl: "<% =DiagraphPath%>",              
                   	reportServer: {
						url: "<% =DiagraphPath%>",
						username: "<% =UserIdDiagraph%>",
						password: "<% =PassIdDiagraph%>"
					},
                    
               reportSource: {                        
                   report: "Samples/MLReport280918",
                  parameters: { Customerid:<% =CustId%>,UserID:<% =UID%>,CustomerName:"<% =CName%>",Flag:"<% =UserRole%>" }
                   //parameters: { Customerid:5,UserID:38 }

               },
                    
                    viewMode: telerikReportViewer.ViewModes.INTERACTIVE,
                    scaleMode: telerikReportViewer.ScaleModes.SPECIFIC,
                    scale: 1.0,
					enableAccessibility: true,
                    ready: function () {
                        //this.refreshReport();
                    },
                });
      }


      //User

        function OpenstatupUserWindow() {
          $('#windowuserpopup').show();
          var myWindowAdv = $("#windowuserpopup");
          function onClose() {

          }
          myWindowAdv.kendoWindow({
              width: "87%",
              height: "87%",             
              title: "Overall User Report",
              visible: false,
              actions: [
                  "Pin",
                  "Close"
              ],
              close: onClose
          });
          //userreport();
          myWindowAdv.data("kendoWindow").center().open();

          return false;
      }

      function userreport() {        
           $("#windowuserpopup").telerik_ReportViewer({
                  serviceUrl: "<% =DiagraphPath%>",              
                   	reportServer: {
						url: "<% =DiagraphPath%>",
						username: "<% =UserIdDiagraph%>",
						password: "<% =PassIdDiagraph%>"
					},
                    
               reportSource: {                        
                   report: "Samples/UserReport280918",
                    parameters: { Customerid:<% =CustId%>,UserID:<% =UID%>,CustomerName:"<% =CName%>",Flag:"<% =UserRole%>" }
               },
                    
                    viewMode: telerikReportViewer.ViewModes.INTERACTIVE,
                    scaleMode: telerikReportViewer.ScaleModes.SPECIFIC,
                    scale: 1.0,
					enableAccessibility: true,
                    ready: function () {
                        //this.refreshReport();
                    },
                });
      }


      //Category
      
       function OpenstatupCategoryWindow() {
          $('#windowCategorypopup').show();
          var myWindowAdv = $("#windowCategorypopup");
          function onClose() {

          }
          myWindowAdv.kendoWindow({
              width: "87%",
              height: "87%",             
              title: "Overall Category Report",
              visible: false,
              actions: [
                  "Pin",
                  "Close"
              ],
              close: onClose
          });
          //categoryreport();
          myWindowAdv.data("kendoWindow").center().open();

          return false;
      }

      function categoryreport() {        
           $("#windowCategorypopup").telerik_ReportViewer({
                serviceUrl: "<% =DiagraphPath%>",              
                   	reportServer: {
						url: "<% =DiagraphPath%>",
						username: "<% =UserIdDiagraph%>",
						password: "<% =PassIdDiagraph%>"
					},
                    
               reportSource: {                        
                   report: "Samples/FinalCategorytReport280918",
                    parameters: { Customerid:<% =CustId%>,UserID:<% =UID%>,CustomerName:"<% =CName%>",Flag:"<% =UserRole%>" }
               },
                    
                    viewMode: telerikReportViewer.ViewModes.INTERACTIVE,
                    scaleMode: telerikReportViewer.ScaleModes.SPECIFIC,
                    scale: 1.0,
					enableAccessibility: true,
                    ready: function () {
                        //this.refreshReport();
                    },
                });
      }

      //windowSummarypopup
        function OpenstatupSummaryWindow() {
          $('#windowSummarypopup').show();
          var myWindowAdv = $("#windowSummarypopup");
          function onClose() {

          }
          myWindowAdv.kendoWindow({
              width: "87%",
              height: "87%",             
              title: "Overall Summary Report",
              visible: false,
              actions: [
                  "Pin",
                  "Close"
              ],
              close: onClose
          });
          //Summaryreport();
          myWindowAdv.data("kendoWindow").center().open();

          return false;
      }

      function Summaryreport() {        
          $("#windowSummarypopup").telerik_ReportViewer({
                 serviceUrl: "<% =DiagraphPath%>",              
                   	reportServer: {
						url: "<% =DiagraphPath%>",
						username: "<% =UserIdDiagraph%>",
						password: "<% =PassIdDiagraph%>"
					},
                         
               reportSource: {                        
                   report: "Samples/ComprehensiveReport",
                  parameters: { Customerid:<% =CustId%>,UserID:<% =UID%>,CustomerName:"<% =CName%>",Flag:"<% =UserRole%>" }
               },
                    
                    viewMode: telerikReportViewer.ViewModes.INTERACTIVE,
                    scaleMode: telerikReportViewer.ScaleModes.SPECIFIC,
                    scale: 1.0,
					enableAccessibility: true,
                    ready: function () {
                        //this.refreshReport();
                    },
                });
      }

  </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div style="float:left;width:100%">
       
                    <div id="teammates121" onclick="OpenstatupSummaryWindow();"  class="widget col-md-4 colpadding0 pointimg" style="margin-bottom: 31px;height: 402px;">
                       
                        <div>
                             <h3 class="imgheader" style="font-family: MetricWeb-Regular, 'Arial Bold', Gadget, sans-serif;"><b>Overall Summary Report</b></h3>
                            <%--<img src="../Images/RiskNew.jpg"/>--%>
                            <img src="../Images/RiskNew.jpg" class="imgbackshowd" style="margin-left: 3px;">
                           <%-- <h3 class="imgheader" style="font-family: MetricWeb-Regular, 'Arial Bold', Gadget, sans-serif;"><b>Status By Risk</b></h3>--%>
                            <div class="keyFeature">Description</div>
                           <div style="color:black;padding-left: 10px;">
                                This dynamic report shows the overall summary of the status of your compliances across locations, categories, users, risk profiles, along with details of overdue compliances
                            </div>

                           <%--  <ul style="padding-left: 28px;color: black;">                            
                                 <li class="set_bullet">Risk summary by status</li>
                                 <li class="set_bullet">Risk summary by location</li>
                                 <li class="set_bullet">Risk summary by category</li>
                             </ul>--%>
                        </div>
                    </div>
         
                    <div id="teammates2" style="width: 353px;height: 402px;" onclick="return OpenstatupLocationWindow()" class="widget col-md-4 colpadding0 pointimg">
                     
                        <div style="">
                            
                             <h3 class="imgheader" style="font-family: MetricWeb-Regular, 'Arial Bold', Gadget, sans-serif;"><b>Location Summary Report</b></h3>
                             <%--<img src="../Images/locationwiseNew.jpg"/>  --%>
                            <img src="../Images/locationwiseNew.jpg" class="imgbackshowd">
                            <div class="keyFeature">Description</div>
                            <div style="color:black;padding-left: 10px;">
                               View the status of your compliances segregated for each of your locations across categories, risk profiles, and users, along with details of ageing of overdue compliances by location
                            </div>
                             <%--<ul style="padding-left: 28px;color: black;">
                              
                                 <li class="set_bullet">Location summary by status</li>
                                 <li class="set_bullet">Location summary of compliances</li>
                                 <li class="set_bullet">Ageing summary of overdue compliances</li>
                             </ul>   --%>                            
                        </div>
                    </div>

                     <div id="teammates3" onclick="return OpenstatupUserWindow()" class="widget col-md-4 colpadding0 pointimg">
                    
                          <div style="padding-left: 4px;">
                        
                                <h3 class="imgheader" style="font-family: MetricWeb-Regular, 'Arial Bold', Gadget, sans-serif;"><b>User Summary Report</b></h3>
                                <img src="../Images/UserWiseNew.jpg" class="imgbackshowd"/>  
                              <div class="keyFeature">Description</div>
                              <div style="color:black;padding-left: 10px;">
                                This report shows the status of your compliances managed by each one of your performers and reviewers across categories, risk profiles, and locations, along with details of ageing of overdue compliances
                                </div>
                              <%--   <ul style="padding-left: 28px;color: black;">
                            
                                 <li class="set_bullet">Performer overdue ageing</li>
                                 <li class="set_bullet">Reviewer overdue ageing</li>
                                 <li class="set_bullet">Ageing summary of overdue compliances</li>
                             </ul>--%>
                            
                        </div>
                    </div>
         
        </div>
     
                  <div style="float:left;width:100%">
 
                   <div id="teammates412" onclick="return OpenstatupCategoryWindow()"  class="widget col-md-4 colpadding0 pointimg" style="float:left;">
                     
                       <div style="padding-left: 1px;">
                           
                             <h3 class="imgheader" style="font-family: MetricWeb-Regular, 'Arial Bold', Gadget, sans-serif;"><b>Category Summary Report</b></h3>
                            <img src="../Images/categorywiseNew.jpg" style="margin-left: -3px;" class="imgbackshowd"/>   
                           <div class="keyFeature">Description</div>
                            <div style="color:black;padding-left: 10px;">
                                View the status of your compliances segregated by category across locations, risk profiles, and users, along with details of ageing of overdue compliances by category  
                                </div>
                            <%--<ul style="padding-left: 28px;color: black;">
                               
                                 <li class="set_bullet">Overall category by status</li>
                                 <li class="set_bullet">Overall category by location</li>
                                 <li class="set_bullet">Overall category by category</li>
                             </ul>--%>

                      
                        </div>
                    </div>
                    
                   <div id="teammates5" class="widget col-md-4 colpadding0 pointimg" style="float:left;">
                    
                        <div style="padding-left: 1px;">
                             
                           
                            <h3 class="imgheader" style="font-family: MetricWeb-Regular, 'Arial Bold', Gadget, sans-serif;"><b>Risk Summary Report</b></h3>
                             <img src="../Images/SummaryNew.jpg" style="margin-left: -3px;" class="imgbackshowd" onclick="return OpenstatupWindow()" /> 
                            <div class="keyFeature">Description</div>  
                             <div style="color:black;padding-left: 10px;">
                                This dynamic report shows the status of your compliances segregated by risk profiles across locations, categories, and users, along with details of ageing of overdue compliances by risk
                                </div>
                           <%-- <ul style="padding-left: 28px;color: black;">
                             
                                 <li class="set_bullet">Overall summary by status</li>
                                 <li class="set_bullet">Overall summary by location</li>
                                 <li class="set_bullet">Overall summary by category</li>
                             </ul>--%>
                       
                        </div>
                   
                       </div>
    


                </div>
         
        <%--  Risk--%>
        <div id="windowriskpopup">         
        </div>

        <%--Location--%>
        <div id="windowlocationpopup">
        </div>

      <%--User--%>
        <div id="windowuserpopup">
        </div>

     <%--Category--%>
        <div id="windowCategorypopup">
        </div>
    
    
     <%--Summary--%>
        <div id="windowSummarypopup">
        </div>

</asp:Content>
