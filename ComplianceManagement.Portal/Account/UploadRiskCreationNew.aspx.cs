﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DropDownListChosen;
using System.Data;
using Ionic.Zip;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Account
{
    public partial class UploadRiskCreationNew : System.Web.UI.Page
    {
        public List<long> locationList = new List<long>();
        public List<long> Branchlist = new List<long>();
        bool sucess = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                lblMessage.Text = string.Empty;
                LblErormessage.Text = string.Empty;
                locationList.Clear();
                ViewState["Flagvalue"] = null;
                Tab1_Click1(sender, e);
                try
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 0, function () { });", true);
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }

            }
        }
        private void BindLocationFilter()
        {
            try
            {
                long customerID = -1;
                customerID = Common.AuthenticationHelper.CustomerID;
                tvFilterLocation.Nodes.Clear();
                var bracnhes = CustomerBranchManagement.GetAllHierarchySatutory(customerID);
                TreeNode node = new TreeNode();
                foreach (var item in bracnhes)
                {
                    node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    CustomerBranchManagementRisk.BindBranchesHierarchy(node, item);
                    tvFilterLocation.Nodes.Add(node);
                }
                tvFilterLocation.CollapseAll();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void BindLegalEntityData()
        {
            long customerID = -1;
            customerID = Common.AuthenticationHelper.CustomerID;
            int UserID = Common.AuthenticationHelper.UserID;
            ddlLegalEntity.DataTextField = "Name";
            ddlLegalEntity.DataValueField = "ID";
            ddlLegalEntity.Items.Clear();
            string AuditHeadOrManager = "";
            AuditHeadOrManager = CustomerManagementRisk.GetAuditHeadOrManagerid(Convert.ToInt32(UserID));
            if (AuditHeadOrManager == "AM" || AuditHeadOrManager == "AH")
            {
                ddlLegalEntity.DataSource = AuditKickOff_NewDetails.FillLegalEntityDataAuditManager(customerID, UserID);
            }
            //ddlLegalEntity.DataSource = AuditKickOff_NewDetails.FillLegalEntityData(customerID);
            ddlLegalEntity.DataBind();
            ddlLegalEntity.Items.Insert(0, new ListItem("Unit", "-1"));
        }
        public void BindSubEntityData(DropDownList DRP, int ParentId)
        {
            long customerID = -1;
            customerID = Common.AuthenticationHelper.CustomerID;
            DRP.DataTextField = "Name";
            DRP.DataValueField = "ID";
            DRP.Items.Clear();
            DRP.DataSource = AuditKickOff_NewDetails.FillSubEntityData(ParentId, customerID);
            DRP.DataBind();
            DRP.Items.Insert(0, new ListItem("Sub Unit", "-1"));
        }
        private void BindddlVerticalBranch()
        {
            if (ddlVerticalBranch.SelectedValue != null)
            {
                long customerID = -1;
                customerID = Common.AuthenticationHelper.CustomerID;
                ddlVerticalBranch.DataTextField = "Name";
                ddlVerticalBranch.DataValueField = "ID";
                ddlVerticalBranch.DataSource = UserManagementRisk.GetVerticalBranchList(customerID);
                ddlVerticalBranch.DataBind();
            }
        }
        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 0, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void upComplianceDetails_Load(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 0, function () { });", true);
        }

        public void BindVertical()
        {
            try
            {
                long customerID = -1;
                customerID = Common.AuthenticationHelper.CustomerID;

                ddlVertical.DataTextField = "VerticalName";
                ddlVertical.DataValueField = "ID";
                ddlVertical.Items.Clear();
                ddlVertical.DataSource = UserManagementRisk.GetAllVertical(customerID);
                ddlVertical.DataBind();
                ddlVertical.Items.Insert(0, new ListItem("Select Vertical", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        #region  Import Risk Register and Risk Control Matrix 
        protected void RetrieveNodes(TreeNode node)
        {
            if (node.Checked && node.ChildNodes.Count == 0) // && node.ChildNodes.Count == 0 if (node.Checked)
            {
                if (!locationList.Contains(Convert.ToInt32(node.Value)))
                    locationList.Add(Convert.ToInt32(node.Value));
            }

            foreach (TreeNode tn in node.ChildNodes)
            {
                if (tn.Checked && tn.ChildNodes.Count == 0)//  && tn.ChildNodes.Count == 0if (tn.Checked)
                {
                    if (!locationList.Contains(Convert.ToInt32(tn.Value)))
                        locationList.Add(Convert.ToInt32(tn.Value));
                }

                if (tn.ChildNodes.Count != 0)
                {
                    for (int i = 0; i < tn.ChildNodes.Count; i++)
                    {
                        RetrieveNodes(tn.ChildNodes[i]);
                    }
                }
            }
        }
        protected void btnUploadFile_Click(object sender, EventArgs e)
        {

            if (MasterFileUpload.HasFile)
            {
                try
                {
                    string filename = Path.GetFileName(MasterFileUpload.FileName);
                    MasterFileUpload.SaveAs(Server.MapPath("~/Uploaded/") + filename.Trim());
                    FileInfo excelfile = new FileInfo(Server.MapPath("~/Uploaded/") + filename.Trim());
                    if (excelfile != null)
                    {
                        using (ExcelPackage xlWorkbook = new ExcelPackage(excelfile))
                        {
                            if (rdoRCMUpload.Checked)
                            {
                                bool flag = RiskTransactionSheetsExitsts(xlWorkbook, "RiskControlMatrixCreation");

                                if (flag == true)
                                {
                                    //List<long> locationList = new List<long>();
                                    locationList.Clear();
                                    for (int i = 0; i < this.tvFilterLocation.Nodes.Count; i++)
                                    {
                                        RetrieveNodes(this.tvFilterLocation.Nodes[i]);
                                    }
                                    //foreach (ListItem item in ddlBranchList.Items)
                                    //{
                                    //    if(item.Selected)                                        
                                    //        locationList.Add(Convert.ToInt32(item.Value));                                        
                                    //}
                                    if (locationList.Count > 0)
                                    {
                                        ProcessRiskCategoryDataNew(xlWorkbook, locationList);
                                    }
                                    else
                                    {
                                        cvDuplicateEntry.IsValid = false;
                                        cvDuplicateEntry.ErrorMessage = "Please Select Branch.";
                                    }
                                }
                                else
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "No Data Found in Excel Document or Sheet Name must be 'RiskControlMatrixCreation'.";
                                }
                            }
                            else if (rdoSubProcess.Checked)
                            {
                                bool flag = ProcessSheetsExitsts(xlWorkbook, "ProcessSubProcess");
                                if (flag == true)
                                {
                                    ProcessData(xlWorkbook);
                                }
                                else
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "No Data Found in Excel Document or Sheet Name must be 'ProcessSubProcess'.";
                                }
                            }
                            else if (rdoRCMUpdate.Checked)
                            {
                                bool flag = RCMUpdateSheetExitsts(xlWorkbook, "RiskControlMatrixUpdate");
                                if (flag == true)
                                {
                                    ProcessRiskControlMatrixUpdate(xlWorkbook);
                                }
                                else
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "No Data Found in Excel Document or Sheet Name must be 'RiskControlMatrixUpdate'.";
                                }
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Please select type of data wants to be upload.";
                            }
                            if (sucess == true)
                            {
                                locationList.Clear();
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Data uploaded successfully.";
                            }
                        }
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Error uploading file. Please try again.";
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                }
            }
        }
        private bool RCMUpdateSheetExitsts(ExcelPackage xlWorkbook, string data)
        {
            try
            {
                bool flag = false;
                foreach (ExcelWorksheet sheet in xlWorkbook.Workbook.Worksheets)
                {
                    if (data.Equals("RiskControlMatrixUpdate"))
                    {
                        if (sheet.Name.Trim().Equals("RiskControlMatrixUpdate"))
                        {
                            flag = true;
                            break;//added by Manisha
                        }
                        else
                        {
                            flag = false;
                            break;
                        }
                    }

                }
                return flag;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                return false;
            }
        }
        private bool RiskTransactionSheetsExitsts(ExcelPackage xlWorkbook, string data)
        {
            try
            {
                bool flag = false;
                foreach (ExcelWorksheet sheet in xlWorkbook.Workbook.Worksheets)
                {
                    if (data.Equals("RiskControlMatrixCreation"))
                    {
                        if (sheet.Name.Trim().Equals("RiskControlMatrixCreation") || sheet.Name.Trim().Equals("RiskControlMatrixCreation") || sheet.Name.Trim().Equals("RiskControlMatrixCreation"))
                        {
                            flag = true;
                            break;//added by Manisha
                        }
                        else
                        {
                            flag = false;
                            break;
                        }
                    }


                }
                return flag;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }
        protected void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                lblMessage.Text = "";
                MasterFileUpload = null;
                //lblMessage1.Text = "";
                //MasterFileUpload1 = null;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static int GetRiskControlRatingID(string Flag, string Name)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var transactionsQuery = (from row in entities.mst_Risk_ControlRating
                                         where row.Name.ToUpper() == Name.ToUpper() && row.IsActive == false
                                         && row.IsRiskControl == Flag
                                         select row.Value).FirstOrDefault();
                return transactionsQuery;
            }
        }
        public static void Insertmst_Activity(mst_Activity mstactivity)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                entities.mst_Activity.Add(mstactivity);
                entities.SaveChanges();

            }
        }
        private void ProcessRiskControlMatrixUpdate(ExcelPackage xlWorkbook)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(Common.AuthenticationHelper.CustomerID);
                List<RiskActivityTransaction> riskactivitytransactionlist = new List<RiskActivityTransaction>();
                List<RiskActivityTransaction> riskactivitytransactionlist1 = new List<RiskActivityTransaction>();

                List<RiskCategoryCreation> riskcategorycreationlist = new List<RiskCategoryCreation>();
                List<RiskCategoryCreation> riskcategorycreationlist1 = new List<RiskCategoryCreation>();

                List<RiskActivityToBeDoneMapping> RiskATBDM = new List<RiskActivityToBeDoneMapping>();
                List<RiskActivityToBeDoneMapping> RiskATBDM1 = new List<RiskActivityToBeDoneMapping>();

                List<StepChecklistMapping> SCMRecords = new List<StepChecklistMapping>();

                List<string> errorMessage = new List<string>();
                ExcelWorksheet xlWorksheetvalidation = xlWorkbook.Workbook.Worksheets["RiskControlMatrixUpdate"];
                int xlvalrow2 = xlWorksheetvalidation.Dimension.End.Row;
                if (xlWorksheetvalidation != null)
                {
                    #region Validations  

                    string valControlNo = string.Empty;
                    string valactivitydescription = string.Empty;
                    string valControlObjective = string.Empty;
                    string valControlDescription = string.Empty;
                    string valMControlDescription = string.Empty;
                    string valProcessOwner = string.Empty;
                    string valProcessOwnerEmail = string.Empty;
                    string valControlOwner = string.Empty;
                    string valControlOwnerEmail = string.Empty;
                    string valERPsystem = string.Empty;
                    string valFraudRiskControl = string.Empty;
                    string valRiskRating = string.Empty;
                    string valAuditMethodology = string.Empty;
                    string valAuditStep = string.Empty;

                    int valbranchid = -1;
                    int valverticalid = -1;
                    int valprocessid = -1;
                    int valsubprocessid = -1;
                    int valriskid = -1;
                    int valratid = -1;
                    int valatbdid = -1;
                    int valasmid = -1;

                    for (int rowNum = 2; rowNum <= xlvalrow2; rowNum++)
                    {
                        valControlNo = string.Empty;
                        valactivitydescription = string.Empty;
                        valControlObjective = string.Empty;
                        valControlDescription = string.Empty;
                        valMControlDescription = string.Empty;
                        valProcessOwner = string.Empty;
                        valProcessOwnerEmail = string.Empty;
                        valControlOwner = string.Empty;
                        valControlOwnerEmail = string.Empty;
                        valERPsystem = string.Empty;
                        valFraudRiskControl = string.Empty;
                        valRiskRating = string.Empty;
                        valAuditMethodology = string.Empty;
                        valAuditStep = string.Empty;

                        valbranchid = -1;
                        valverticalid = -1;
                        valprocessid = -1;
                        valsubprocessid = -1;
                        valriskid = -1;
                        valratid = -1;
                        valatbdid = -1;
                        valasmid = -1;

                        #region 6 objective_ref
                        if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 6].Text.ToString().Trim()))
                        {
                            valControlNo = xlWorksheetvalidation.Cells[rowNum, 6].Text.Trim();
                        }
                        if (string.IsNullOrEmpty(valControlNo))
                        {
                            errorMessage.Add("Required Objective Ref.  at row number-" + rowNum);
                        }
                        #endregion

                        #region  7 Risk Description
                        if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 7].Text.ToString().Trim()))
                        {
                            valactivitydescription = xlWorksheetvalidation.Cells[rowNum, 7].Text.Trim();
                        }
                        if (string.IsNullOrEmpty(valactivitydescription))
                        {
                            errorMessage.Add("Required Risk/Activity Description at row number-" + rowNum);
                        }
                        #endregion

                        #region  8 Control Objective
                        if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 8].Text.ToString().Trim()))
                        {
                            valControlObjective = xlWorksheetvalidation.Cells[rowNum, 8].Text.Trim();
                        }
                        if (string.IsNullOrEmpty(valControlObjective))
                        {
                            errorMessage.Add("Required Control Objective at row number-" + rowNum);
                        }
                        #endregion

                        #region 9 Control Description
                        if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 9].Text.ToString().Trim()))
                        {
                            valControlDescription = xlWorksheetvalidation.Cells[rowNum, 9].Text.Trim();
                        }
                        if (string.IsNullOrEmpty(valControlDescription))
                        {
                            errorMessage.Add("Required Control Description at row number-" + rowNum);
                        }
                        #endregion

                        #region C 11 Process Owner  12 Process Owner Email                        
                        if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 11].Text.ToString().Trim()))
                        {
                            valProcessOwner = xlWorksheetvalidation.Cells[rowNum, 11].Text.Trim();
                        }
                        if (string.IsNullOrEmpty(valProcessOwner))
                        {
                            errorMessage.Add("Required Process Owner Name at row number-" + rowNum);
                        }
                        if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 12].Text.ToString().Trim()))
                        {
                            valProcessOwnerEmail = xlWorksheetvalidation.Cells[rowNum, 12].Text.Trim();
                        }
                        if (string.IsNullOrEmpty(valProcessOwnerEmail))
                        {
                            errorMessage.Add("Required Process Owner Email at row number-" + rowNum);
                        }
                        int personresponsibleIDcheck = -1;
                        if (!String.IsNullOrEmpty(valProcessOwner) && !String.IsNullOrEmpty(valProcessOwnerEmail))
                        {
                            personresponsibleIDcheck = RiskCategoryManagement.GetUserIDByName(valProcessOwner, customerID, valProcessOwnerEmail);
                        }
                        if (personresponsibleIDcheck == 0 || personresponsibleIDcheck == -1)
                        {
                            errorMessage.Add("Process Owner Name/Email not defined in the system at row number-" + rowNum);
                        }
                        #endregion

                        #region  13 Control Owner	14 Control Owner Email                        
                        if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 13].Text.ToString().Trim()))
                        {
                            valControlOwner = xlWorksheetvalidation.Cells[rowNum, 13].Text.Trim();
                        }
                        if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 14].Text.ToString().Trim()))
                        {
                            valControlOwnerEmail = xlWorksheetvalidation.Cells[rowNum, 14].Text.Trim();
                        }
                        if (!String.IsNullOrEmpty(valControlOwner) && !String.IsNullOrEmpty(valControlOwnerEmail))
                        {
                            int ControlownerIDcheck = -1;
                            ControlownerIDcheck = RiskCategoryManagement.GetUserIDByName(valControlOwner, customerID, valControlOwnerEmail);
                            if (ControlownerIDcheck == 0 || ControlownerIDcheck == -1)
                            {
                                errorMessage.Add("Control Owner Name/Email not defined in the system at row number-" + rowNum);
                            }
                        }
                        #endregion

                        #region 15 Application system supporting the transaction (ERP System)                    
                        if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 15].Text.ToString().Trim()))
                        {
                            valERPsystem = xlWorksheetvalidation.Cells[rowNum, 15].Text.Trim();
                        }
                        if (string.IsNullOrEmpty(valERPsystem))
                        {
                            errorMessage.Add("Required Application system supporting the transaction at row number-" + rowNum);
                        }
                        #endregion

                        #region 16  Fraud Risk Control	
                        if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 16].Text.ToString().Trim()))
                        {
                            valFraudRiskControl = xlWorksheetvalidation.Cells[rowNum, 16].Text.Trim();
                        }
                        if (string.IsNullOrEmpty(valFraudRiskControl))
                        {
                            errorMessage.Add("Required Fraud Risk Control at row number-" + rowNum);
                        }
                        #endregion

                        #region 18 Risk Rating
                        if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 18].Text.ToString().Trim()))
                        {
                            valRiskRating = xlWorksheetvalidation.Cells[rowNum, 18].Text.Trim();
                        }
                        if (string.IsNullOrEmpty(valRiskRating))
                        {
                            errorMessage.Add("Required Risk Rating at row number-" + rowNum);
                        }
                        else
                        {
                            int RiskRating = -1;
                            RiskRating = GetRiskControlRatingID("R", Convert.ToString(valRiskRating).Trim());
                            if (RiskRating == 0 || RiskRating == -1)
                            {
                                errorMessage.Add("Risk Rating not defined in the system at row number-" + rowNum);
                            }
                        }
                        #endregion

                        #region  19 Sampling methodology/Audit Methodology (Audit Objective)	
                        if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 19].Text.ToString().Trim()))
                        {
                            valAuditMethodology = xlWorksheetvalidation.Cells[rowNum, 19].Text.Trim();
                        }
                        if (string.IsNullOrEmpty(valAuditMethodology))
                        {
                            errorMessage.Add("Required Sampling methodology/Audit Methodology at row number-" + rowNum);
                        }
                        #endregion

                        #region  20 Audit Steps		
                        if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 20].Text.ToString().Trim()))
                        {
                            valAuditStep = xlWorksheetvalidation.Cells[rowNum, 20].Text.Trim();
                        }
                        if (string.IsNullOrEmpty(valAuditStep))
                        {
                            errorMessage.Add("Required Audit Steps at row number-" + rowNum);
                        }
                        #endregion

                        #region 23 BID
                        if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 23].Text.ToString().Trim()))
                        {
                            valbranchid = Convert.ToInt32(xlWorksheetvalidation.Cells[rowNum, 23].Text);
                        }
                        if (valbranchid == 0 || valbranchid == -1)
                        {
                            errorMessage.Add("BID not defined in the system or BID can not be empty at row number-" + rowNum);
                        }
                        #endregion

                        #region 24 VID
                        if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 24].Text.ToString().Trim()))
                        {
                            valverticalid = Convert.ToInt32(xlWorksheetvalidation.Cells[rowNum, 24].Text);
                        }
                        if (valverticalid == 0 || valverticalid == -1)
                        {
                            errorMessage.Add("VID not defined in the system or VID can not be empty at row number-" + rowNum);
                        }
                        #endregion

                        #region 25 PID
                        if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 25].Text.ToString().Trim()))
                        {
                            valprocessid = Convert.ToInt32(xlWorksheetvalidation.Cells[rowNum, 25].Text);
                        }
                        if (valprocessid == 0 || valprocessid == -1)
                        {
                            errorMessage.Add("PID not defined in the system or PID can not be empty at row number-" + rowNum);
                        }
                        #endregion

                        #region 26 SPID
                        if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 26].Text.ToString().Trim()))
                        {
                            valsubprocessid = Convert.ToInt32(xlWorksheetvalidation.Cells[rowNum, 26].Text);
                        }
                        if (valsubprocessid == 0 || valsubprocessid == -1)
                        {
                            errorMessage.Add("SPID not defined in the system or SPID can not be empty at row number-" + rowNum);
                        }
                        #endregion

                        #region 27 RiskID
                        if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 27].Text.ToString().Trim()))
                        {
                            valriskid = Convert.ToInt32(xlWorksheetvalidation.Cells[rowNum, 27].Text);
                        }
                        if (valriskid == 0 || valriskid == -1)
                        {
                            errorMessage.Add("RiskID not defined in the system or RiskID can not be empty at row number-" + rowNum);
                        }
                        #endregion

                        #region 28 RATID
                        if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 28].Text.ToString().Trim()))
                        {
                            valratid = Convert.ToInt32(xlWorksheetvalidation.Cells[rowNum, 28].Text);
                        }
                        if (valratid == 0 || valratid == -1)
                        {
                            errorMessage.Add("RiskID not defined in the system or RiskID can not be empty at row number-" + rowNum);
                        }
                        #endregion

                        #region 29 ATBDId
                        if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 29].Text.ToString().Trim()))
                        {
                            valatbdid = Convert.ToInt32(xlWorksheetvalidation.Cells[rowNum, 29].Text);
                        }
                        if (valatbdid == 0 || valatbdid == -1)
                        {
                            errorMessage.Add("ATBDId not defined in the system or ATBDId can not be empty at row number-" + rowNum);
                        }
                        #endregion

                        #region 30 ASMID
                        if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 30].Text.ToString().Trim()))
                        {
                            valasmid = Convert.ToInt32(xlWorksheetvalidation.Cells[rowNum, 30].Text);
                        }
                        if (valasmid == 0 || valasmid == -1)
                        {
                            errorMessage.Add("ASMID not defined in the system or ASMID can not be empty at row number-" + rowNum);
                        }
                        #endregion
                    }
                    #endregion

                    if (errorMessage.Count > 0)
                    {
                        ErrorMessages(errorMessage);
                        sucess = false;
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 0, function () { });", true);
                    }
                    else
                    {
                        ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["RiskControlMatrixUpdate"];
                        int xlrow2 = xlWorksheet.Dimension.End.Row;
                        #region Save Data in RiskCategoryCreation/RiskActivityTransaction

                        string ControlNo = string.Empty;
                        string ActivityDescription = string.Empty;
                        string ControlObjective = string.Empty;

                        string ControlDescription = string.Empty;
                        string MControlDescription = string.Empty;
                        string ERPsystem = string.Empty;
                        string FRC = string.Empty;
                        string TestStrategy = string.Empty;

                        string AuditObjective = string.Empty;
                        string AuditStep = string.Empty;
                        string Analysistobeperformed = string.Empty;
                        string PreRequisiteList = string.Empty;

                        int personresponsibleID = 0;
                        int ControlownerID = 0;
                        int riskid = -1;
                        int riskactivityid = -1;
                        int atbdid = -1;
                        int asmid = -1;
                        long auditStepMasterID = -1;

                        for (int i = 2; i <= xlrow2; i++)
                        {
                            personresponsibleID = 0;
                            ControlownerID = 0;
                            riskid = -1;
                            riskactivityid = -1;
                            atbdid = -1;
                            asmid = -1;

                            ControlNo = string.Empty;
                            ActivityDescription = string.Empty;
                            ControlObjective = string.Empty;
                            ControlDescription = string.Empty;
                            MControlDescription = string.Empty;
                            ERPsystem = string.Empty;
                            FRC = string.Empty;
                            TestStrategy = string.Empty;
                            AuditObjective = string.Empty;
                            AuditStep = string.Empty;
                            Analysistobeperformed = string.Empty;
                            PreRequisiteList = string.Empty;

                            //27 RiskID
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 27].Text.ToString().Trim()))
                            {
                                riskid = Convert.ToInt32(xlWorksheet.Cells[i, 27].Text);
                            }
                            //28 RiskActivityID
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 28].Text.ToString().Trim()))
                            {
                                riskactivityid = Convert.ToInt32(xlWorksheet.Cells[i, 28].Text);
                            }
                            //29 atbdid
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 29].Text.ToString().Trim()))
                            {
                                atbdid = Convert.ToInt32(xlWorksheet.Cells[i, 29].Text);
                            }
                            //30 asmid
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 30].Text.ToString().Trim()))
                            {
                                asmid = Convert.ToInt32(xlWorksheet.Cells[i, 30].Text);
                            }
                            if ((riskid != 0 || riskid != -1) && (riskactivityid != 0 || riskactivityid != -1) && (atbdid != 0 || atbdid != -1))
                            {
                                //6 ControlNo                   
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 6].Text.ToString().Trim()))
                                {
                                    ControlNo = Convert.ToString(xlWorksheet.Cells[i, 6].Text).Trim();
                                }
                                //7 Risk Description
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 7].Text.ToString().Trim()))
                                {
                                    ActivityDescription = Convert.ToString(xlWorksheet.Cells[i, 7].Text).Trim();
                                }
                                //8 Control Objective
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 8].Text.ToString().Trim()))
                                {
                                    ControlObjective = Convert.ToString(xlWorksheet.Cells[i, 8].Text).Trim();
                                }
                                //9 Control Description
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 9].Text.ToString().Trim()))
                                {
                                    ControlDescription = Convert.ToString(xlWorksheet.Cells[i, 9].Text).Trim();
                                }
                                //10 Mitigating Control Description
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 10].Text.ToString().Trim()))
                                {
                                    MControlDescription = Convert.ToString(xlWorksheet.Cells[i, 10].Text).Trim();
                                }
                                //11 Person Responsible                              
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 11].Text.ToString().Trim()))
                                {   //12 Email
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 12].Text.ToString().Trim()))
                                    {
                                        personresponsibleID = RiskCategoryManagement.GetUserIDByName(xlWorksheet.Cells[i, 11].Text.ToString().Trim(), customerID, xlWorksheet.Cells[i, 12].Text.ToString().Trim());
                                    }
                                }
                                //13 Control OwnerID                          
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 13].Text.ToString().Trim()))
                                {   //14 Email
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 14].Text.ToString().Trim()))
                                    {
                                        ControlownerID = RiskCategoryManagement.GetUserIDByName(xlWorksheet.Cells[i, 13].Text.ToString().Trim(), customerID, xlWorksheet.Cells[i, 14].Text.ToString().Trim());
                                    }
                                }
                                //15 Application system supporting the transaction 
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 15].Text.ToString().Trim().Trim()))
                                {
                                    ERPsystem = Convert.ToString(xlWorksheet.Cells[i, 15].Text.ToString().Trim());
                                }
                                //16 Fraud risk indicator
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 16].Text.ToString().Trim().Trim()))
                                {
                                    FRC = Convert.ToString(xlWorksheet.Cells[i, 16].Text.ToString().Trim());
                                }
                                //17 Test Strategy  
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 17].Text.ToString().Trim().Trim()))
                                {
                                    TestStrategy = Convert.ToString(xlWorksheet.Cells[i, 17].Text.ToString().Trim());
                                }
                                //18 Risk Rating
                                int RiskRatingcheck = -1;
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 18].Text.ToString().Trim()))
                                {
                                    RiskRatingcheck = GetRiskControlRatingID("R", Convert.ToString(xlWorksheet.Cells[i, 18].Text).Trim());
                                }
                                //19 Audit Methodology  
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 19].Text.ToString().Trim().Trim()))
                                {
                                    AuditObjective = Convert.ToString(xlWorksheet.Cells[i, 19].Text.ToString().Trim());
                                }
                                //20 Audit Methodology  
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 20].Text.ToString().Trim().Trim()))
                                {
                                    AuditStep = Convert.ToString(xlWorksheet.Cells[i, 20].Text.ToString().Trim());
                                }
                                //21 Analyis To Be Performed  
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 21].Text.ToString().Trim().Trim()))
                                {
                                    Analysistobeperformed = Convert.ToString(xlWorksheet.Cells[i, 21].Text.ToString().Trim());
                                }

                                if (RiskCategoryManagement.ExistsRCCByID(riskid))
                                {
                                    RiskCategoryCreation riskcategorycreation = new RiskCategoryCreation()
                                    {
                                        Id = riskid,
                                        ControlNo = ControlNo,
                                        ActivityDescription = ActivityDescription,
                                        ControlObjective = ControlObjective,
                                    };
                                    riskcategorycreationlist.Add(riskcategorycreation);
                                }
                                if (RiskCategoryManagement.ExistsRATByID(riskactivityid, riskid))
                                {
                                    RiskActivityTransaction riskactivitytransaction = new RiskActivityTransaction()
                                    {
                                        Id = riskactivityid,
                                        RiskCreationId = riskid,
                                        ControlDescription = ControlDescription,
                                        MControlDescription = MControlDescription,
                                        PersonResponsible = personresponsibleID,
                                        ControlOwner = ControlownerID,
                                        ERPsystem = ERPsystem,
                                        FRC = FRC,
                                        TestStrategy = TestStrategy,
                                        RiskRating = RiskRatingcheck,
                                    };
                                    riskactivitytransactionlist.Add(riskactivitytransaction);
                                }

                                if (RiskCategoryManagement.ExistsRATBDMByID(atbdid))
                                {
                                    auditStepMasterID = RiskCategoryManagement.GetAuditStepMasterID(AuditStep);
                                    if (auditStepMasterID == 0)
                                    {
                                        AuditStepMaster newASM = new AuditStepMaster()
                                        {
                                            AuditStep = AuditStep
                                        };

                                        if (RiskCategoryManagement.CreateAuditStepMaster(newASM))
                                            auditStepMasterID = newASM.ID;
                                    }
                                    if (auditStepMasterID != 0)
                                    {
                                        RiskActivityToBeDoneMapping RATBDM = new RiskActivityToBeDoneMapping()
                                        {
                                            ID = atbdid,
                                            AuditStepMasterID = auditStepMasterID,
                                            AuditObjective = AuditObjective,
                                            ActivityTobeDone = AuditStep,
                                            Analyis_To_Be_Performed = Analysistobeperformed,
                                            Rating = 1
                                        };
                                        RiskATBDM.Add(RATBDM);
                                    }
                                    #region  22 Pre-Requisite List 
                                    //if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 22].Text.ToString().Trim()))
                                    //{
                                    //    string prerequisite = xlWorksheet.Cells[i, 22].Text.ToString().Trim();
                                    //    string[] split = prerequisite.Split('|');
                                    //    if (split.Length > 0)
                                    //    {
                                    //        List<int> prerequisiteID = new List<int>();
                                    //        {
                                    //            string prerequisites = xlWorksheet.Cells[i, 31].Text.ToString().Trim();
                                    //            string[] splitint = prerequisites.Split('|');
                                    //            if (splitint.Length > 0)
                                    //            {
                                    //                for (int rs = 0; rs < splitint.Length; rs++)
                                    //                {
                                    //                    if (!string.IsNullOrEmpty(splitint[rs].ToString().Trim()))
                                    //                    {
                                    //                        prerequisiteID.Add(Convert.ToInt32(splitint[rs].ToString().Trim()));
                                    //                    }
                                    //                }
                                    //            }
                                    //        }

                                    //        string prerequisiteName = string.Empty;
                                    //        for (int rs = 0; rs < split.Length; rs++)
                                    //        {
                                    //            int SCMID = -1;
                                    //            if (rs + 1 <= prerequisiteID.Count)
                                    //            {
                                    //                SCMID = prerequisiteID[rs];
                                    //            }
                                    //            prerequisiteName = Convert.ToString(split[rs].ToString().Trim());
                                    //            if (!String.IsNullOrEmpty(prerequisiteName))
                                    //            {
                                    //                StepChecklistMapping SCM = new StepChecklistMapping();
                                    //                if (SCMID != -1)
                                    //                {
                                    //                    SCM.ID = SCMID;
                                    //                }
                                    //                SCM.ATBDID = atbdid;
                                    //                SCM.ChecklistDocument = prerequisiteName;
                                    //                SCM.IsActive = true;
                                    //                SCMRecords.Add(SCM);
                                    //            }
                                    //        }
                                    //        prerequisiteID.Clear();
                                    //    }
                                    //}
                                    //if (SCMRecords.Count > 0)
                                    //{
                                    //    sucess = RiskCategoryManagement.CreateStepChecklistMapping(SCMRecords);
                                    //    SCMRecords.Clear();
                                    //}
                                    #endregion
                                }
                            }
                        }

                        riskcategorycreationlist1 = riskcategorycreationlist.Where(entry => entry.ProcessId == 0 && entry.SubProcessId == 0).Distinct().ToList();
                        riskactivitytransactionlist1 = riskactivitytransactionlist.Where(entry => entry.ProcessId == 0 && entry.RiskCreationId == 0).ToList();
                        RiskATBDM1 = RiskATBDM.Where(entry => entry.ID == 0).ToList();

                        if (riskcategorycreationlist1.Count == 0 && riskactivitytransactionlist1.Count == 0 && RiskATBDM1.Count == 0)
                        {
                            //Update RCC and RAT and RATBD
                            sucess = BulkRCCUpdateNew(riskcategorycreationlist);
                            sucess = BulkRATUpdateNew(riskactivitytransactionlist);
                            sucess = BulkRATBDMUpdateNew(RiskATBDM);
                        }
                        #endregion

                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static bool BulkRCCUpdateNew(List<RiskCategoryCreation> ListRCCData)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    int Count = 0;
                    ListRCCData.ForEach(EachRCC =>
                    {
                        RiskCategoryCreation RCCtoUpdate = (from row in entities.RiskCategoryCreations
                                                            where row.Id == EachRCC.Id
                                                            select row).FirstOrDefault();
                        if (RCCtoUpdate != null)
                        {
                            RCCtoUpdate.ControlNo = EachRCC.ControlNo;
                            RCCtoUpdate.ActivityDescription = EachRCC.ActivityDescription;
                            RCCtoUpdate.ControlObjective = EachRCC.ControlObjective;
                            Count++;
                        }
                        if (Count >= 100)
                        {
                            entities.SaveChanges();
                            Count = 0;
                        }
                    });
                    entities.SaveChanges();
                    return true;
                }

            }
            catch (Exception)
            {
                return false;
            }

        }
        public static bool BulkRATUpdateNew(List<RiskActivityTransaction> ListRATData)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    int Count = 0;
                    ListRATData.ForEach(EachRAT =>
                    {
                        RiskActivityTransaction RATtoUpdate = (from row in entities.RiskActivityTransactions
                                                               where row.Id == EachRAT.Id
                                                               && row.RiskCreationId == EachRAT.RiskCreationId
                                                               select row).FirstOrDefault();

                        if (RATtoUpdate != null)
                        {
                            RATtoUpdate.ControlDescription = EachRAT.ControlDescription;
                            RATtoUpdate.MControlDescription = EachRAT.MControlDescription;
                            RATtoUpdate.ControlOwner = EachRAT.ControlOwner;
                            RATtoUpdate.PersonResponsible = EachRAT.PersonResponsible;
                            RATtoUpdate.TestStrategy = EachRAT.TestStrategy;
                            RATtoUpdate.RiskRating = EachRAT.RiskRating;
                            RATtoUpdate.FRC = EachRAT.FRC;
                            RATtoUpdate.ERPsystem = EachRAT.ERPsystem;
                            Count++;
                        }

                        if (Count > 100)
                        {
                            entities.SaveChanges();
                            Count = 0;
                        }
                    });

                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool BulkRATBDMUpdateNew(List<RiskActivityToBeDoneMapping> ListRATBD)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    int Count = 0;
                    ListRATBD.ForEach(EachStep =>
                    {
                        RiskActivityToBeDoneMapping RATBDToUpdate = (from row in entities.RiskActivityToBeDoneMappings
                                                                     where row.ID == EachStep.ID
                                                                     select row).FirstOrDefault();

                        if (RATBDToUpdate != null)
                        {
                            RATBDToUpdate.AuditStepMasterID = EachStep.AuditStepMasterID;
                            RATBDToUpdate.AuditObjective = EachStep.AuditObjective;
                            RATBDToUpdate.ActivityTobeDone = EachStep.ActivityTobeDone;
                            RATBDToUpdate.Analyis_To_Be_Performed = EachStep.Analyis_To_Be_Performed;
                            RATBDToUpdate.Rating = EachStep.Rating;
                            Count++;
                        }

                        if (Count >= 100)
                        {
                            entities.SaveChanges();
                            Count = 0;
                        }
                    });

                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }


        protected bool CheckDate(String date)
        {
            try
            {
                DateTime dt = DateTime.Parse(date);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public static string GetSafeTagName(string tag)
        {
            tag = tag.Replace("'", "`")
            .Replace('"', '`')
            .Replace('‐', '_');

            //.Replace("&", "and")
            //.Replace(",", ":")
            //.Replace(@"\", "/"); //Do not allow escaped characters from user
            tag = Regex.Replace(tag, @"\s+", " "); //multiple spaces with single spaces
            return tag;
        }
        public static bool CreateRiskActivityTransaction(RiskActivityTransaction RAT)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    entities.Database.CommandTimeout = 300;
                    entities.RiskActivityTransactions.Add(RAT);
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public static bool CreateRiskCategoryCreation(RiskCategoryCreation RCC)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    entities.Database.CommandTimeout = 300;
                    entities.RiskCategoryCreations.Add(RCC);
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool PrerequsiteExistsNew(StepChecklistMapping objmaster)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.StepChecklistMappings
                             where
                                row.ChecklistDocument.ToUpper().Trim() == objmaster.ChecklistDocument.ToUpper().Trim()
                                && row.ATBDID == objmaster.ATBDID
                                && row.IsActive == objmaster.IsActive
                             select row).FirstOrDefault();

                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public static bool ExistsNew(mst_RCMMaster objmaster)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_RCMMaster
                             where
                                row.ObjectiveRef.ToUpper().Trim() == objmaster.ObjectiveRef.ToUpper().Trim()
                                && row.Processid == objmaster.Processid
                                && row.SubProcessid == objmaster.SubProcessid
                                && row.ActivityId == objmaster.ActivityId
                                && row.RiskDesc.ToUpper().Trim() == objmaster.RiskDesc.ToUpper().Trim()
                                && row.ControlObjective.ToUpper().Trim() == objmaster.ControlObjective.ToUpper().Trim()
                                && row.Controldescription.ToUpper().Trim() == objmaster.Controldescription.ToUpper().Trim()
                                && row.RiskCategory.ToUpper().Trim() == objmaster.RiskCategory.ToUpper().Trim()
                                && row.FinancialAssertion.ToUpper().Trim() == objmaster.FinancialAssertion.ToUpper().Trim()
                                && row.MCDescription.ToUpper().Trim() == objmaster.MCDescription.ToUpper().Trim()
                                && row.ProcessOwnerID == objmaster.ProcessOwnerID
                                && row.ControlOwnerID == objmaster.ControlOwnerID
                                && row.EffectiveDate == objmaster.EffectiveDate
                                && row.KC1 == objmaster.KC1
                                && row.KC2 == objmaster.KC2
                                && row.PrimarySecondary == objmaster.PrimarySecondary
                                && row.PreventiveDetective == objmaster.PreventiveDetective
                                && row.AutomatedManual == objmaster.AutomatedManual
                                && row.Frequency == objmaster.Frequency
                                && row.IPE.ToUpper().Trim() == objmaster.IPE.ToUpper().Trim()
                                && row.ERPSystem.ToUpper().Trim() == objmaster.ERPSystem.ToUpper().Trim()
                                && row.FraudRiskIndecator.ToUpper().Trim() == objmaster.FraudRiskIndecator.ToUpper().Trim()
                                && row.UniqueReferred.ToUpper().Trim() == objmaster.UniqueReferred.ToUpper().Trim()
                                && row.TestStrategy.ToUpper().Trim() == objmaster.TestStrategy.ToUpper().Trim()
                                && row.DocumentsExamined.ToUpper().Trim() == objmaster.DocumentsExamined.ToUpper().Trim()
                                && row.Riskrating == objmaster.Riskrating
                                && row.ControlRating == objmaster.ControlRating
                                && row.AuditObjective.ToUpper().Trim() == objmaster.AuditObjective.ToUpper().Trim()
                                && row.AuditStep.ToUpper().Trim() == objmaster.AuditStep.ToUpper().Trim()
                                && row.AnalysistobePerformed.ToUpper().Trim() == objmaster.AnalysistobePerformed.ToUpper().Trim()
                                && row.PreRequisiteList.ToUpper().Trim() == objmaster.PreRequisiteList.ToUpper().Trim()
                                && row.BranchList.ToUpper().Trim() == objmaster.BranchList.ToUpper().Trim()
                                && row.VerticalList.ToUpper().Trim() == objmaster.VerticalList.ToUpper().Trim()
                             select row).FirstOrDefault();

                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        #region Assertions
        public static bool AssertionExists(string CategoryName)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_Assertions
                             where row.Name.Equals(CategoryName)
                             select row);
                return query.Select(entry => true).SingleOrDefault();
            }
        }
        public static void AssertionCreate(mst_Assertions Data)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                entities.mst_Assertions.Add(Data);
                entities.SaveChanges();

            }
        }
        public static bool AssertionsMappingExists(long Assid, long Riskid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.AssertionsMappings
                             where row.RiskCategoryCreationId == Riskid && row.AssertionId == Assid
                             select row).FirstOrDefault();
                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        #endregion
        public void ErrorMessages(List<string> emsg)
        {
            string finalErrMsg = string.Empty;

            finalErrMsg += "<ol type='1'>";

            if (emsg.Count > 0)
            {
                emsg.ForEach(eachErrMsg =>
                {
                    finalErrMsg += "<li>" + eachErrMsg + "</li>";
                });

                finalErrMsg += "</ol>";
            }

            cvDuplicateEntry.IsValid = false;
            cvDuplicateEntry.ErrorMessage = finalErrMsg;
        }

        private int GetDimensionRows(ExcelWorksheet sheet)
        {
            var startRow = sheet.Dimension.Start.Row;
            var endRow = sheet.Dimension.End.Row;
            return endRow - startRow;
        }
        private void ProcessRiskCategoryDataNew(ExcelPackage xlWorkbook, List<long> LocationList)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    ExcelWorksheet xlWorksheetvalidation = xlWorkbook.Workbook.Worksheets["RiskControlMatrixCreation"];
                    List<string> errorMessage = new List<string>();
                    if (xlWorksheetvalidation != null)
                    {
                        #region xlWorksheetvalidation  != null
                        if (GetDimensionRows(xlWorksheetvalidation) != 0)
                        {
                            int xlrow2 = xlWorksheetvalidation.Dimension.End.Row;
                            List<int> Verticallist = new List<int>();
                            Verticallist.Clear();
                            int checkVerticalapplicable = 0;
                            int CustomerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
                            var cdetails = CustomerManagement.GetByID(Convert.ToInt32(CustomerID));
                            var IsVerticlApplicable = cdetails.VerticalApplicable;
                            if (IsVerticlApplicable != null)
                            {
                                if (IsVerticlApplicable != -1)
                                {
                                    checkVerticalapplicable = Convert.ToInt32(IsVerticlApplicable);
                                }
                            }
                            if (checkVerticalapplicable == 0)
                            {
                                mst_Vertical objVerti = new mst_Vertical()
                                {
                                    VerticalName = "NA",
                                    CustomerID = CustomerID
                                };
                                if (!UserManagementRisk.VerticalNameExist(objVerti))
                                {
                                    UserManagementRisk.CreateVerticalName(objVerti);
                                    Verticallist.Add(objVerti.ID);
                                }
                                else
                                {
                                    int verticalid = UserManagementRisk.VerticalgetBycustomerid(CustomerID);
                                    Verticallist.Add(verticalid);
                                }
                            }
                            else
                            {
                                for (int i = 0; i < ddlVerticalBranch.Items.Count; i++)
                                {
                                    if (ddlVerticalBranch.Items[i].Selected)
                                    {
                                        Verticallist.Add(Convert.ToInt32(ddlVerticalBranch.Items[i].Value));
                                    }
                                }
                                if (Verticallist.Count == 0)
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Please select vertical or vertical not defined in the system";
                                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 0, function () { });", true);

                                    return;
                                }
                            }

                            #region Validations

                            string valobjective_ref = string.Empty;
                            string valProcessName = string.Empty;
                            string valSubProcessName = string.Empty;
                            string valActivity = string.Empty;
                            string valactivitydescription = string.Empty;
                            string valControlObjective = string.Empty;
                            string valControlDescription = string.Empty;
                            string valProcessOwner = string.Empty;
                            string valProcessOwnerEmail = string.Empty;
                            string valControlOwner = string.Empty;
                            string valControlOwnerEmail = string.Empty;
                            string valstringeffectivedate = string.Empty;
                            string valPreventiveControl = string.Empty;
                            string valAutomatedControl = string.Empty;
                            string valFrequency = string.Empty;
                            string valERPsystem = string.Empty;
                            string valFraudRiskControl = string.Empty;
                            string valRiskRating = string.Empty;
                            string valControlRating = string.Empty;
                            string valAuditMethodology = string.Empty;
                            string valAuditStep = string.Empty;
                            string valAssertions = string.Empty;
                            int valprocessid = -1;
                            int valsubprocessid = -1;
                            int valactivityid = -1;
                            string CheckRCMType = string.Empty;


                            for (int rowNum = 2; rowNum <= xlrow2; rowNum++)
                            {
                                valobjective_ref = string.Empty;
                                valProcessName = string.Empty;
                                valSubProcessName = string.Empty;
                                valActivity = string.Empty;
                                valactivitydescription = string.Empty;
                                valControlObjective = string.Empty;
                                valControlDescription = string.Empty;
                                valProcessOwner = string.Empty;
                                valProcessOwnerEmail = string.Empty;
                                valControlOwner = string.Empty;
                                valControlOwnerEmail = string.Empty;
                                valstringeffectivedate = string.Empty;
                                valPreventiveControl = string.Empty;
                                valAutomatedControl = string.Empty;
                                valFrequency = string.Empty;
                                valERPsystem = string.Empty;
                                valFraudRiskControl = string.Empty;
                                valRiskRating = string.Empty;
                                valControlRating = string.Empty;
                                valAuditMethodology = string.Empty;
                                valAuditStep = string.Empty;
                                CheckRCMType = string.Empty;

                                valprocessid = -1;
                                valsubprocessid = -1;
                                valactivityid = -1;

                                //37 Check ARS/IFC/Both
                                if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 37].Text.ToString().Trim()))
                                {
                                    if (xlWorksheetvalidation.Cells[rowNum, 37].Text.ToString().ToUpper().Trim() == "IFC")
                                    {
                                        CheckRCMType = "IFC";
                                    }
                                    else if (xlWorksheetvalidation.Cells[rowNum, 37].Text.ToString().ToUpper().Trim() == "ARS")
                                    {
                                        CheckRCMType = "ARS";
                                    }
                                    else if (xlWorksheetvalidation.Cells[rowNum, 37].Text.ToString().ToUpper().Trim() == "BOTH")
                                    {
                                        CheckRCMType = "BOTH";
                                    }
                                    else
                                    {
                                        errorMessage.Add("Required Valid RCM Type.  at row number-" + rowNum);
                                    }
                                }
                                else
                                {
                                    errorMessage.Add("Required Valid RCM Type.  at row number-" + rowNum);
                                }

                                #region C 1 objective_ref
                                if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 1].Text.ToString().Trim()))
                                {
                                    valobjective_ref = xlWorksheetvalidation.Cells[rowNum, 1].Text.Trim();
                                }
                                if (string.IsNullOrEmpty(valobjective_ref))
                                {
                                    errorMessage.Add("Required Objective Ref.  at row number-" + rowNum);
                                }
                                #endregion

                                #region C 2 ProcessName
                                if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 2].Text.ToString().Trim()))
                                {
                                    valProcessName = xlWorksheetvalidation.Cells[rowNum, 2].Text.Trim();
                                }
                                if (string.IsNullOrEmpty(valProcessName))
                                {
                                    errorMessage.Add("Required ProcessName  at row number-" + rowNum);
                                }
                                else
                                {
                                    if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 2].Text.ToString().Trim()))
                                    {
                                        valprocessid = ProcessManagement.GetProcessIDByName(Regex.Replace(xlWorksheetvalidation.Cells[rowNum, 2].Text.ToString().Trim(), @"\t|\n|\r", ""), CustomerID);
                                    }
                                    if (valprocessid == 0 || valprocessid == -1)
                                    {
                                        errorMessage.Add("Process Name not defined in the system at row number-" + rowNum);
                                    }
                                }
                                #endregion

                                #region C 3 SubProcessName
                                if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 3].Text.ToString().Trim()))
                                {
                                    valSubProcessName = xlWorksheetvalidation.Cells[rowNum, 3].Text.Trim();
                                }
                                if (string.IsNullOrEmpty(valSubProcessName))
                                {
                                    errorMessage.Add("Required SubProcessName  at row number-" + rowNum);
                                }
                                else
                                {
                                    if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 3].Text.ToString().Trim()) && valprocessid != -1 && valprocessid != 0)
                                    {
                                        valsubprocessid = ProcessManagement.GetSubProcessIDByName(Regex.Replace(xlWorksheetvalidation.Cells[rowNum, 3].Text.ToString().Trim(), @"\t|\n|\r", ""), valprocessid);
                                        if (valsubprocessid == 0 || valsubprocessid == -1)
                                        {
                                            errorMessage.Add("SubProcessName not defined in the system at row number-" + rowNum);
                                        }
                                    }

                                }
                                #endregion

                                #region C 4 Activity
                                if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 4].Text.ToString().Trim()))
                                {
                                    valActivity = xlWorksheetvalidation.Cells[rowNum, 4].Text.Trim();
                                    if (valprocessid != -1 && valprocessid != 0 && valsubprocessid != -1 && valsubprocessid != 0)
                                    {
                                        valactivityid = ProcessManagement.GetActivityBYName(valprocessid, valsubprocessid, Regex.Replace(valActivity, @"\t|\n|\r", ""));
                                        if (valactivityid == 0 || valactivityid == -1)
                                        {
                                            errorMessage.Add("Activity Name not defined in the system at row number-" + rowNum);
                                        }
                                    }
                                }
                                else
                                {
                                    if (valprocessid != -1 && valprocessid != 0 && valsubprocessid != -1 && valsubprocessid != 0)
                                    {
                                        if (!(ProcessManagement.ExistsActivity(valprocessid, valsubprocessid, "NA")))
                                        {
                                            mst_Activity mstactivity = new mst_Activity();
                                            mstactivity.Name = "NA";
                                            mstactivity.ProcessId = valprocessid;
                                            mstactivity.SubProcessId = valsubprocessid;
                                            mstactivity.CreatedBy = Common.AuthenticationHelper.UserID;
                                            mstactivity.IsDeleted = false;
                                            mstactivity.CreatedOn = DateTime.Now;
                                            Insertmst_Activity(mstactivity);
                                        }
                                    }
                                }
                                #endregion

                                #region  C 5 Risk Description
                                if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 5].Text.ToString().Trim()))
                                {
                                    valactivitydescription = xlWorksheetvalidation.Cells[rowNum, 5].Text.Trim();
                                }
                                if (string.IsNullOrEmpty(valactivitydescription))
                                {
                                    errorMessage.Add("Required Risk/Activity Description at row number-" + rowNum);
                                }
                                #endregion

                                #region  C 6 Control Objective
                                if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 6].Text.ToString().Trim()))
                                {
                                    valControlObjective = xlWorksheetvalidation.Cells[rowNum, 6].Text.Trim();
                                }
                                if (string.IsNullOrEmpty(valControlObjective))
                                {
                                    errorMessage.Add("Required Control Objective at row number-" + rowNum);
                                }
                                #endregion

                                #region  C 7 Control Description
                                if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 7].Text.ToString().Trim()))
                                {
                                    valControlDescription = xlWorksheetvalidation.Cells[rowNum, 7].Text.Trim();
                                }
                                if (string.IsNullOrEmpty(valControlDescription))
                                {
                                    errorMessage.Add("Required Control Description at row number-" + rowNum);
                                }
                                #endregion

                                #region C 8 Risk Category Mapping add to List 
                                if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 8].Text.ToString().Trim()))
                                {
                                    string risk = xlWorksheetvalidation.Cells[rowNum, 8].Text.ToString().Trim();
                                    string[] split = risk.Split(',');
                                    if (split.Length > 0)
                                    {
                                        string riskcategoryid = "";
                                        for (int rs = 0; rs < split.Length; rs++)
                                        {
                                            riskcategoryid = Convert.ToString(RiskCategoryManagement.GetriskcategoryIDByName(split[rs].ToString().Trim()));
                                            if (riskcategoryid == "0" || riskcategoryid == "-1")
                                            {
                                                errorMessage.Add("Risk Category Name not defined in the system at row number-" + rowNum);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    if (!ProcessManagement.CategoryExists("NA"))
                                    {
                                        mst_Riskcategory CategoryData = new mst_Riskcategory()
                                        {
                                            Name = "NA",
                                            Description = null,
                                            IsDeleted = false,
                                            IsICRRisk = "I",
                                        };
                                        ProcessManagement.CategoryCreate(CategoryData);
                                    }
                                }
                                #endregion

                                #region C 9 Financial statement assertion 

                                if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 9].Text.ToString().Trim()))
                                {
                                    valAssertions = xlWorksheetvalidation.Cells[rowNum, 9].Text.Trim();
                                }
                                if (string.IsNullOrEmpty(valAssertions))
                                {
                                    errorMessage.Add("Required Financial statement assertion at row number-" + rowNum);
                                }
                                else
                                {
                                    if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 9].Text.ToString().Trim()))
                                    {
                                        string Assertions = xlWorksheetvalidation.Cells[rowNum, 9].Text.ToString().Trim();
                                        string[] splitAssertions = Assertions.Split(',');
                                        if (splitAssertions.Length > 0)
                                        {
                                            string Assertionsmappigid = "";
                                            for (int rs = 0; rs < splitAssertions.Length; rs++)
                                            {
                                                Assertionsmappigid = Convert.ToString(RiskCategoryManagement.GetAssertionsIdByName(splitAssertions[rs].ToString().Trim()));
                                                if (Assertionsmappigid == "0" || Assertionsmappigid == "-1")
                                                {
                                                    if (splitAssertions[rs].ToString().Trim().Length > 50)
                                                    {
                                                        errorMessage.Add("Check Length of Financial statement assertion at row number-" + rowNum);
                                                    }
                                                    else
                                                    {
                                                        if (!AssertionExists(splitAssertions[rs].ToString().Trim()))
                                                        {
                                                            mst_Assertions AssertionData = new mst_Assertions()
                                                            {
                                                                Name = splitAssertions[rs].ToString().Trim(),
                                                            };
                                                            AssertionCreate(AssertionData);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                #endregion

                                #region C 11 Process Owner  12 Process Owner Email                        
                                if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 11].Text.ToString().Trim()))
                                {
                                    valProcessOwner = xlWorksheetvalidation.Cells[rowNum, 11].Text.Trim();
                                }
                                if (string.IsNullOrEmpty(valProcessOwner))
                                {
                                    errorMessage.Add("Required Process Owner Name at row number-" + rowNum);
                                }
                                if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 12].Text.ToString().Trim()))
                                {
                                    valProcessOwnerEmail = xlWorksheetvalidation.Cells[rowNum, 12].Text.Trim();
                                }
                                if (string.IsNullOrEmpty(valProcessOwnerEmail))
                                {
                                    errorMessage.Add("Required Process Owner Email at row number-" + rowNum);
                                }
                                int personresponsibleIDcheck = -1;
                                if (!String.IsNullOrEmpty(valProcessOwner) && !String.IsNullOrEmpty(valProcessOwnerEmail))
                                {
                                    personresponsibleIDcheck = RiskCategoryManagement.GetUserIDByName(valProcessOwner, CustomerID, valProcessOwnerEmail);
                                }
                                if (personresponsibleIDcheck == 0 || personresponsibleIDcheck == -1)
                                {
                                    errorMessage.Add("Process Owner Name/Email not defined in the system at row number-" + rowNum);
                                }
                                #endregion

                                #region  13 Control Owner	14 Control Owner Email                        
                                if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 13].Text.ToString().Trim()))
                                {
                                    valControlOwner = xlWorksheetvalidation.Cells[rowNum, 13].Text.Trim();
                                }
                                if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 14].Text.ToString().Trim()))
                                {
                                    valControlOwnerEmail = xlWorksheetvalidation.Cells[rowNum, 14].Text.Trim();
                                }
                                if (!String.IsNullOrEmpty(valControlOwner) && !String.IsNullOrEmpty(valControlOwnerEmail))
                                {
                                    int ControlownerIDcheck = -1;
                                    ControlownerIDcheck = RiskCategoryManagement.GetUserIDByName(valControlOwner, CustomerID, valControlOwnerEmail);
                                    if (ControlownerIDcheck == 0 || ControlownerIDcheck == -1)
                                    {
                                        errorMessage.Add("Control Owner Name/Email not defined in the system at row number-" + rowNum);
                                    }
                                }
                                #endregion

                                #region 15 Effective Date                        
                                if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 15].Text.ToString().Trim()))
                                {
                                    valstringeffectivedate = Convert.ToString(xlWorksheetvalidation.Cells[rowNum, 15].Text).Trim();
                                    try
                                    {
                                        bool check = CheckDate(valstringeffectivedate);
                                        if (check)
                                        {
                                            sucess = true;
                                        }
                                        else
                                        {
                                            errorMessage.Add("Please Check the Effective Date at row number-" + rowNum);
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        errorMessage.Add("Please Check the Effective Date at row number-" + rowNum);
                                    }
                                }
                                #endregion

                                #region 16 KC-1 Multiple risks addressed
                                int KC1check = 1;
                                if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 16].Text.ToString().Trim()))
                                {
                                    if (xlWorksheetvalidation.Cells[rowNum, 16].Text.ToString().ToUpper().Trim() == "YES")
                                    {
                                        KC1check = 1;
                                    }
                                    else if (xlWorksheetvalidation.Cells[rowNum, 16].Text.ToString().ToUpper().Trim() == "NO")
                                    {
                                        KC1check = 2;
                                    }
                                    if (KC1check == 0 || KC1check == -1)
                                    {
                                        errorMessage.Add("KC-1 Multiple risks addressed not defined in the system at row number-" + rowNum);
                                    }
                                }
                                #endregion

                                #region 17 Key(KC-2 Criticality of control)
                                
                                int KeyIDcheck = -1;
                                if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 17].Text.ToString().Trim()))
                                {
                                    if (xlWorksheetvalidation.Cells[rowNum, 17].Text.ToString().ToUpper().Trim() == "YES")
                                    {
                                        KeyIDcheck = 1;
                                    }
                                    else if (xlWorksheetvalidation.Cells[rowNum, 17].Text.ToString().ToUpper().Trim() == "NO")
                                    {
                                        KeyIDcheck = 2;
                                    }
                                    else
                                    {
                                        if (xlWorksheetvalidation.Cells[rowNum, 17].Text.ToString().ToUpper().Trim() == "NA")
                                        {
                                            if (CheckRCMType != "ARS")
                                            {
                                                errorMessage.Add("KC-2 Criticality of control not be NA while RCM Type IFC/Both-" + rowNum);
                                            }
                                            else
                                            {
                                                KeyIDcheck = 3;
                                            }
                                        }
                                    }
                                    if (KeyIDcheck == 0 || KeyIDcheck == -1)
                                    {
                                        errorMessage.Add("KC-2 Criticality of control not defined in the system at row number-" + rowNum);
                                    }
                                }
                                else
                                {
                                    errorMessage.Add("Required KC-2 Criticality of control at row number-" + rowNum);
                                }
                                #endregion

                                #region 18 Primary / Secondary
                                int PrimarySecondaryIDcheck = -1;
                                if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 18].Text.ToString().Trim()))
                                {
                                    PrimarySecondaryIDcheck = RiskCategoryManagement.GetPrimarySecondaryIdByName(Regex.Replace(xlWorksheetvalidation.Cells[rowNum, 18].Text.ToString().Trim(), @"\t|\n|\r", ""));
                                    if (PrimarySecondaryIDcheck == 0 || PrimarySecondaryIDcheck == -1)
                                    {
                                        errorMessage.Add("Primary / Secondary not defined in the system at row number-" + rowNum);
                                    }
                                }
                                #endregion

                                #region C 19 Preventive Control                   
                                if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 19].Text.ToString().Trim()))
                                {
                                    valPreventiveControl = xlWorksheetvalidation.Cells[rowNum, 19].Text.Trim();
                                }
                                if (string.IsNullOrEmpty(valPreventiveControl))
                                {
                                    errorMessage.Add("Required Preventive / Detective / Deterrent)  at row number-" + rowNum);
                                }
                                else
                                {
                                    int PrevationControlIDcheck = -1;
                                    PrevationControlIDcheck = RiskCategoryManagement.GetPrevationControlIdByName(Regex.Replace(valPreventiveControl, @"\t|\n|\r", ""));

                                    if (PrevationControlIDcheck == 0 || PrevationControlIDcheck == -1)
                                    {
                                        errorMessage.Add("Preventive / Detective / Deterrent) Control not defined in the system at row number-" + rowNum);
                                    }
                                }
                                #endregion

                                #region C 20 Automated Control
                                if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 20].Text.ToString().Trim()))
                                {
                                    valAutomatedControl = xlWorksheetvalidation.Cells[rowNum, 20].Text.Trim();
                                }
                                if (string.IsNullOrEmpty(valAutomatedControl))
                                {
                                    errorMessage.Add("Required Automated / Manual  at row number-" + rowNum);
                                }
                                else
                                {
                                    int AutomatedControlIDcheck = -1;
                                    AutomatedControlIDcheck = RiskCategoryManagement.GetAutomatedControlIDByName(Regex.Replace(valAutomatedControl, @"\t|\n|\r", ""));
                                    if (AutomatedControlIDcheck == 0 || AutomatedControlIDcheck == -1)
                                    {
                                        errorMessage.Add("Automated/Manual Control not defined in the system at row number-" + rowNum);
                                    }
                                }
                                #endregion

                                #region C 21 Frequency
                                if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 21].Text.ToString().Trim()))
                                {
                                    valFrequency = xlWorksheetvalidation.Cells[rowNum, 21].Text.Trim();
                                }
                                if (string.IsNullOrEmpty(valFrequency))
                                {
                                    errorMessage.Add("Required Frquency at row number-" + rowNum);
                                }
                                else
                                {
                                    int Frequencycheck = -1;
                                    Frequencycheck = RiskCategoryManagement.GetFrequencyIDByName(Regex.Replace(valFrequency, @"\t|\n|\r", ""));
                                    if (Frequencycheck == 0 || Frequencycheck == -1)
                                    {
                                        errorMessage.Add("Frquency Name not defined in the system at row number-" + rowNum);
                                    }
                                }
                                #endregion

                                #region C 26 Application system supporting the transaction (ERP System	)                    
                                if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 26].Text.ToString().Trim()))
                                {
                                    valERPsystem = xlWorksheetvalidation.Cells[rowNum, 26].Text.Trim();
                                }
                                if (string.IsNullOrEmpty(valERPsystem))
                                {
                                    errorMessage.Add("Required Application system supporting the transaction at row number-" + rowNum);
                                }
                                #endregion

                                #region C 27 Fraud Risk Control	
                                if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 27].Text.ToString().Trim()))
                                {
                                    valFraudRiskControl = xlWorksheetvalidation.Cells[rowNum, 27].Text.Trim();
                                }
                                if (string.IsNullOrEmpty(valFraudRiskControl))
                                {
                                    errorMessage.Add("Required Fraud Risk Control at row number-" + rowNum);
                                }
                                #endregion

                                #region C 31 Risk Rating
                                if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 31].Text.ToString().Trim()))
                                {
                                    valRiskRating = xlWorksheetvalidation.Cells[rowNum, 31].Text.Trim();
                                }
                                if (string.IsNullOrEmpty(valRiskRating))
                                {
                                    errorMessage.Add("Required Risk Rating at row number-" + rowNum);
                                }
                                else
                                {
                                    int RiskRating = -1;
                                    RiskRating = GetRiskControlRatingID("R", Convert.ToString(valRiskRating).Trim());
                                    if (RiskRating == 0 || RiskRating == -1)
                                    {
                                        errorMessage.Add("Risk Rating not defined in the system at row number-" + rowNum);
                                    }
                                }
                                #endregion

                                #region 32 Control Rating                   
                                if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 32].Text.ToString().Trim()))
                                {
                                    valControlRating = xlWorksheetvalidation.Cells[rowNum, 32].Text.Trim();

                                    int ControlRating = -1;
                                    ControlRating = GetRiskControlRatingID("C", Convert.ToString(valControlRating).Trim());
                                    if (ControlRating == 0 || ControlRating == -1)
                                    {
                                        errorMessage.Add("Control Rating not defined in the system at row number-" + rowNum);
                                    }
                                }
                                #endregion

                                #region  C 33 Sampling methodology/Audit Methodology (Audit Objective)	
                                if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 33].Text.ToString().Trim()))
                                {
                                    valAuditMethodology = xlWorksheetvalidation.Cells[rowNum, 33].Text.Trim();
                                }
                                if (string.IsNullOrEmpty(valAuditMethodology))
                                {
                                    errorMessage.Add("Required Sampling methodology/Audit Methodology at row number-" + rowNum);
                                }
                                #endregion

                                #region  C 34 Audit Steps		
                                if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 34].Text.ToString().Trim()))
                                {
                                    valAuditStep = xlWorksheetvalidation.Cells[rowNum, 34].Text.Trim();
                                }
                                if (string.IsNullOrEmpty(valAuditStep))
                                {
                                    errorMessage.Add("Required Audit Steps at row number-" + rowNum);
                                }
                                #endregion

                            }
                            #endregion

                            if (errorMessage.Count > 0)
                            {
                                ErrorMessages(errorMessage);
                                sucess = false;
                                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 0, function () { });", true);
                            }
                            else
                            {
                                #region New Table
                                ExcelWorksheet xlWorksheetMaster = xlWorkbook.Workbook.Worksheets["RiskControlMatrixCreation"];
                                int xlrowtemptable = xlWorksheetMaster.Dimension.End.Row;

                                List<mst_RCMMaster> MasterRCMList = new List<mst_RCMMaster>();
                                for (int temprow = 2; temprow <= xlrowtemptable; temprow++)
                                {
                                    string ControlNo = string.Empty;
                                    string ActivityDescription = string.Empty;
                                    string ControlObjective = string.Empty;
                                    string ControlDescription = string.Empty;
                                    string MControlDescription = string.Empty;
                                    DateTime? effectivedate = null;
                                    string GapDescription = string.Empty;
                                    string Recommendations = string.Empty;
                                    string ActionRemediationplan = string.Empty;
                                    string IPE = string.Empty;
                                    string ERPsystem = string.Empty;
                                    string FRC = string.Empty;
                                    string UniqueReferred = string.Empty;
                                    string TestStrategy = string.Empty;
                                    string DocumentsExamined = string.Empty;
                                    string finalriskcategoryid = string.Empty;
                                    string finalAssertionsmappigid = string.Empty;
                                    string AuditObjective = string.Empty;
                                    string AuditStep = string.Empty;
                                    string Analysistobeperformed = string.Empty;
                                    string PreRequisiteList = string.Empty;
                                    string RCMTypeCheck = string.Empty;

                                    int processid = -1;
                                    int subprocessid = -1;
                                    int activityid = -1;
                                    int personresponsibleID = 0;
                                    int ControlownerID = 0;
                                    int PrevationControlID = 0;
                                    int PrimarySecondaryID = 0;
                                    int AutomatedControlID = 0;
                                    int Frequency = 0;
                                    int KeyID = 0;
                                    int KCONEID = 0;

                                    //1 Objective Ref.
                                    if (!String.IsNullOrEmpty(xlWorksheetMaster.Cells[temprow, 1].Text.ToString().Trim()))
                                    {
                                        ControlNo = Convert.ToString(xlWorksheetMaster.Cells[temprow, 1].Text).Trim();
                                    }
                                    //2 Process 
                                    if (!String.IsNullOrEmpty(xlWorksheetMaster.Cells[temprow, 2].Text.ToString().Trim()))
                                    {
                                        processid = ProcessManagement.GetProcessIDByName(Regex.Replace(xlWorksheetMaster.Cells[temprow, 2].Text.ToString().Trim(), @"\t|\n|\r", ""), CustomerID);
                                    }
                                    //3 Sub Process                    
                                    if (!String.IsNullOrEmpty(xlWorksheetMaster.Cells[temprow, 3].Text.ToString().Trim()))
                                    {
                                        subprocessid = ProcessManagement.GetSubProcessIDByName(Regex.Replace(xlWorksheetMaster.Cells[temprow, 3].Text.ToString().Trim(), @"\t|\n|\r", ""), processid);
                                    }
                                    //4 Activity
                                    if (!String.IsNullOrEmpty(xlWorksheetMaster.Cells[temprow, 4].Text.ToString().Trim()))
                                    {
                                        activityid = ProcessManagement.GetActivityBYName(processid, subprocessid, Regex.Replace(xlWorksheetMaster.Cells[temprow, 4].Text.ToString().Trim(), @"\t|\n|\r", ""));
                                    }
                                    else
                                    {
                                        activityid = ProcessManagement.GetActivityBYName(processid, subprocessid, "NA");
                                    }
                                    //5 Risk Description
                                    if (!String.IsNullOrEmpty(xlWorksheetMaster.Cells[temprow, 5].Text.ToString().Trim()))
                                    {
                                        ActivityDescription = Convert.ToString(GetSafeTagName(xlWorksheetMaster.Cells[temprow, 5].Text.ToString().Trim())).Trim();
                                    }
                                    //6 Control Objective
                                    if (!String.IsNullOrEmpty(xlWorksheetMaster.Cells[temprow, 6].Text.ToString().Trim()))
                                    {
                                        ControlObjective = Convert.ToString(GetSafeTagName(xlWorksheetMaster.Cells[temprow, 6].Text.ToString().Trim())).Trim();
                                    }
                                    //7 Control Description
                                    if (!String.IsNullOrEmpty(xlWorksheetMaster.Cells[temprow, 7].Text.ToString().Trim()))
                                    {
                                        ControlDescription = Convert.ToString(xlWorksheetMaster.Cells[temprow, 7].Text).Trim();
                                    }
                                    #region 8 Risk Category Mapping add to List 
                                    if (!String.IsNullOrEmpty(xlWorksheetMaster.Cells[temprow, 8].Text.ToString().Trim()))
                                    {
                                        string risk = xlWorksheetMaster.Cells[temprow, 8].Text.ToString().Trim();
                                        string[] split = risk.Split(',');
                                        if (split.Length > 0)
                                        {

                                            string riskcategoryid = "";
                                            for (int rs = 0; rs < split.Length; rs++)
                                            {
                                                riskcategoryid = Convert.ToString(RiskCategoryManagement.GetriskcategoryIDByName(split[rs].ToString().Trim()));
                                                if (split.Length > 1)
                                                {
                                                    finalriskcategoryid += riskcategoryid + ",";
                                                }
                                                else
                                                {
                                                    finalriskcategoryid += riskcategoryid;
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        finalriskcategoryid += Convert.ToString(RiskCategoryManagement.GetriskcategoryIDByName("NA"));
                                    }
                                    #endregion

                                    #region 9 Assertions Mapping 
                                    if (!String.IsNullOrEmpty(xlWorksheetMaster.Cells[temprow, 9].Text.ToString().Trim()))
                                    {
                                        string Assertions = xlWorksheetMaster.Cells[temprow, 9].Text.ToString().Trim(',');
                                        string[] splitAssertions = Assertions.Split(',');
                                        if (splitAssertions.Length > 0)
                                        {

                                            string Assertionsmappigid = "";
                                            for (int rs = 0; rs < splitAssertions.Length; rs++)
                                            {
                                                Assertionsmappigid = Convert.ToString(RiskCategoryManagement.GetAssertionsIdByName(splitAssertions[rs].ToString().Trim()));
                                                if (splitAssertions.Length > 1)
                                                {
                                                    finalAssertionsmappigid += Assertionsmappigid + ",";
                                                }
                                                else
                                                {
                                                    finalAssertionsmappigid += Assertionsmappigid;
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        finalAssertionsmappigid += Convert.ToString(RiskCategoryManagement.GetAssertionsIdByName("NA"));
                                    }
                                    #endregion

                                    //10 Mitigating Control Description
                                    if (!String.IsNullOrEmpty(xlWorksheetMaster.Cells[temprow, 10].Text.ToString().Trim()))
                                    {
                                        MControlDescription = Convert.ToString(xlWorksheetMaster.Cells[temprow, 10].Text).Trim();
                                    }
                                    //11 Process Owner
                                    if (!String.IsNullOrEmpty(xlWorksheetMaster.Cells[temprow, 11].Text.ToString().Trim()))
                                    {   //12 Process Owner Email
                                        if (!String.IsNullOrEmpty(xlWorksheetMaster.Cells[temprow, 12].Text.ToString().Trim()))
                                        {
                                            personresponsibleID = RiskCategoryManagement.GetUserIDByName(xlWorksheetMaster.Cells[temprow, 11].Text.ToString().Trim(), CustomerID, xlWorksheetMaster.Cells[temprow, 12].Text.ToString().Trim());
                                        }
                                    }
                                    //13 Control Owner
                                    if (!String.IsNullOrEmpty(xlWorksheetMaster.Cells[temprow, 13].Text.ToString().Trim()))
                                    {   //14 Control Ownerr Email
                                        if (!String.IsNullOrEmpty(xlWorksheetMaster.Cells[temprow, 14].Text.ToString().Trim()))
                                        {
                                            ControlownerID = RiskCategoryManagement.GetUserIDByName(xlWorksheetMaster.Cells[temprow, 13].Text.ToString().Trim(), CustomerID, xlWorksheetMaster.Cells[temprow, 14].Text.ToString().Trim());
                                        }
                                    }
                                    //15 Effective Date	                                                  
                                    if (!String.IsNullOrEmpty(xlWorksheetMaster.Cells[temprow, 15].Text.ToString().Trim()))
                                    {
                                        string c = Convert.ToString(xlWorksheetMaster.Cells[temprow, 15].Text).Trim();
                                        DateTime? aaaa = CleanDateField(c);
                                        DateTime dt1 = Convert.ToDateTime(aaaa);
                                        effectivedate = GetDate(dt1.ToString("dd/MM/yyyy"));
                                    }
                                    //16 KC-1 Multiple risks addressed
                                    if (!String.IsNullOrEmpty(xlWorksheetMaster.Cells[temprow, 16].Text.ToString().Trim()))
                                    {
                                        if (xlWorksheetMaster.Cells[temprow, 16].Text.ToString().ToUpper().Trim() == "YES")
                                        {
                                            KCONEID = 1;
                                        }
                                        else if (xlWorksheetMaster.Cells[temprow, 16].Text.ToString().ToUpper().Trim() == "NO")
                                        {
                                            KCONEID = 2;
                                        }
                                    }
                                    //17 Key(KC-2 Criticality of control)
                                    if (!String.IsNullOrEmpty(xlWorksheetMaster.Cells[temprow, 17].Text.ToString().Trim()))
                                    {
                                        if (xlWorksheetMaster.Cells[temprow, 17].Text.ToString().ToUpper().Trim() == "YES")
                                        {
                                            KeyID = 1;
                                        }
                                        else if (xlWorksheetMaster.Cells[temprow, 17].Text.ToString().ToUpper().Trim() == "NO")
                                        {
                                            KeyID = 2;
                                        }
                                        else if (xlWorksheetMaster.Cells[temprow, 17].Text.ToString().ToUpper().Trim() == "NA")
                                        {
                                            KeyID = 0;
                                        }
                                    }
                                    //18 Primary / Secondary
                                    if (!String.IsNullOrEmpty(xlWorksheetMaster.Cells[temprow, 18].Text.ToString().Trim()))
                                    {
                                        PrimarySecondaryID = RiskCategoryManagement.GetPrimarySecondaryIdByName(Regex.Replace(xlWorksheetMaster.Cells[temprow, 18].Text.ToString().Trim(), @"\t|\n|\r", ""));
                                    }
                                    //19 Preventive Control
                                    if (!String.IsNullOrEmpty(xlWorksheetMaster.Cells[temprow, 19].Text.ToString().Trim()))
                                    {
                                        PrevationControlID = RiskCategoryManagement.GetPrevationControlIdByName(Regex.Replace(xlWorksheetMaster.Cells[temprow, 19].Text.ToString().Trim(), @"\t|\n|\r", ""));
                                    }
                                    //20 Automated Control
                                    if (!String.IsNullOrEmpty(xlWorksheetMaster.Cells[temprow, 20].Text.ToString().Trim()))
                                    {
                                        AutomatedControlID = RiskCategoryManagement.GetAutomatedControlIDByName(Regex.Replace(xlWorksheetMaster.Cells[temprow, 20].Text.ToString().Trim(), @"\t|\n|\r", ""));
                                    }
                                    //21 Frequency
                                    if (!String.IsNullOrEmpty(xlWorksheetMaster.Cells[temprow, 21].Text.ToString().Trim()))
                                    {
                                        Frequency = RiskCategoryManagement.GetFrequencyIDByName(xlWorksheetMaster.Cells[temprow, 21].Text.ToString().Trim());
                                    }
                                    //22 Gap Description
                                    if (!String.IsNullOrEmpty(xlWorksheetMaster.Cells[temprow, 22].Text.ToString().Trim()))
                                    {
                                        GapDescription = Convert.ToString(Regex.Replace(xlWorksheetMaster.Cells[temprow, 22].Text.ToString().Trim(), @"\t|\n|\r", ""));
                                    }
                                    //23 Recommendations
                                    if (!String.IsNullOrEmpty(xlWorksheetMaster.Cells[temprow, 23].Text.ToString().Trim()))
                                    {
                                        Recommendations = Convert.ToString(xlWorksheetMaster.Cells[temprow, 23].Text.ToString().Trim());
                                    }
                                    //24 Action Remediation Plan
                                    if (!String.IsNullOrEmpty(xlWorksheetMaster.Cells[temprow, 24].Text.ToString().Trim()))
                                    {
                                        ActionRemediationplan = Convert.ToString(xlWorksheetMaster.Cells[temprow, 24].Text.ToString().Trim());
                                    }
                                    //25 Information Produced by Entity
                                    if (!String.IsNullOrEmpty(xlWorksheetMaster.Cells[temprow, 25].Text.ToString().Trim()))
                                    {
                                        IPE = Convert.ToString(xlWorksheetMaster.Cells[temprow, 25].Text.ToString().Trim());
                                    }
                                    //26 ERP System	
                                    if (!String.IsNullOrEmpty(xlWorksheetMaster.Cells[temprow, 26].Text.ToString().Trim()))
                                    {
                                        ERPsystem = Convert.ToString(xlWorksheetMaster.Cells[temprow, 26].Text.ToString().Trim());
                                    }
                                    //27 Fraud Risk Control	
                                    if (!String.IsNullOrEmpty(xlWorksheetMaster.Cells[temprow, 27].Text.ToString().Trim()))
                                    {
                                        FRC = Convert.ToString(xlWorksheetMaster.Cells[temprow, 27].Text.ToString().Trim());
                                    }
                                    //28 Unique Referred
                                    if (!String.IsNullOrEmpty(xlWorksheetMaster.Cells[temprow, 28].Text.ToString().Trim().Trim()))
                                    {
                                        UniqueReferred = Convert.ToString(xlWorksheetMaster.Cells[temprow, 28].Text.ToString().Trim());
                                    }
                                    //29 Test Strategy
                                    if (!String.IsNullOrEmpty(xlWorksheetMaster.Cells[temprow, 29].Text.ToString().Trim().Trim()))
                                    {
                                        TestStrategy = Convert.ToString(xlWorksheetMaster.Cells[temprow, 29].Text.ToString().Trim());
                                    }
                                    //30 Documents Examined
                                    if (!String.IsNullOrEmpty(xlWorksheetMaster.Cells[temprow, 30].Text.ToString().Trim()))
                                    {
                                        DocumentsExamined = Convert.ToString(xlWorksheetMaster.Cells[temprow, 30].Text.ToString().Trim());
                                    }
                                    //31 RiskRating
                                    int RiskRating = -1;
                                    if (!String.IsNullOrEmpty(xlWorksheetMaster.Cells[temprow, 31].Text.ToString().Trim()))
                                    {
                                        RiskRating = GetRiskControlRatingID("R", Convert.ToString(xlWorksheetMaster.Cells[temprow, 31].Text).Trim());
                                    }
                                    //32 ControlRating
                                    int ControlRating = -1;
                                    if (!String.IsNullOrEmpty(xlWorksheetMaster.Cells[temprow, 32].Text.ToString().Trim()))
                                    {
                                        ControlRating = GetRiskControlRatingID("C", Convert.ToString(xlWorksheetMaster.Cells[temprow, 32].Text).Trim());
                                    }
                                    //33 AuditObjective
                                    if (!String.IsNullOrEmpty(xlWorksheetMaster.Cells[temprow, 33].Text.ToString().Trim()))
                                    {
                                        AuditObjective = Convert.ToString(GetSafeTagName(xlWorksheetMaster.Cells[temprow, 33].Text.ToString().Trim())).Trim();
                                    }
                                    //34 AuditStep
                                    if (!String.IsNullOrEmpty(xlWorksheetMaster.Cells[temprow, 34].Text.ToString().Trim()))
                                    {
                                        AuditStep = Convert.ToString(GetSafeTagName(xlWorksheetMaster.Cells[temprow, 34].Text.ToString().Trim())).Trim();
                                    }
                                    //35 Analysistobeperformed
                                    if (!String.IsNullOrEmpty(xlWorksheetMaster.Cells[temprow, 35].Text.ToString().Trim()))
                                    {
                                        Analysistobeperformed = Convert.ToString(GetSafeTagName(xlWorksheetMaster.Cells[temprow, 35].Text.ToString().Trim())).Trim();
                                    }
                                    //36 PreRequisiteList
                                    if (!String.IsNullOrEmpty(xlWorksheetMaster.Cells[temprow, 36].Text.ToString().Trim()))
                                    {
                                        PreRequisiteList = Convert.ToString(GetSafeTagName(xlWorksheetMaster.Cells[temprow, 36].Text.ToString().Trim())).Trim();
                                    }

                                    //37 Check ARS/IFC/Both
                                    if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[temprow, 37].Text.ToString().Trim()))
                                    {
                                        if (xlWorksheetvalidation.Cells[temprow, 37].Text.ToString().ToUpper().Trim() == "IFC")
                                        {
                                            RCMTypeCheck = "IFC";
                                        }
                                        else if (xlWorksheetvalidation.Cells[temprow, 37].Text.ToString().ToUpper().Trim() == "ARS")
                                        {
                                            RCMTypeCheck = "ARS";
                                        }
                                        else if (xlWorksheetvalidation.Cells[temprow, 37].Text.ToString().ToUpper().Trim() == "BOTH")
                                        {
                                            RCMTypeCheck = "BOTH";
                                        }
                                    }

                                    mst_RCMMaster mstrcmmaster = new mst_RCMMaster();
                                    mstrcmmaster.ObjectiveRef = ControlNo;
                                    mstrcmmaster.Processid = processid;
                                    mstrcmmaster.SubProcessid = subprocessid;
                                    mstrcmmaster.ActivityId = activityid;
                                    mstrcmmaster.RiskDesc = ActivityDescription;
                                    mstrcmmaster.ControlObjective = ControlObjective;
                                    mstrcmmaster.Controldescription = ControlDescription;
                                    mstrcmmaster.RiskCategory = finalriskcategoryid.Trim(',');
                                    mstrcmmaster.FinancialAssertion = finalAssertionsmappigid.Trim(',');
                                    mstrcmmaster.MCDescription = MControlDescription;
                                    mstrcmmaster.ProcessOwnerID = personresponsibleID;
                                    mstrcmmaster.ControlOwnerID = ControlownerID;
                                    mstrcmmaster.EffectiveDate = effectivedate;
                                    mstrcmmaster.KC1 = KeyID;
                                    mstrcmmaster.KC2 = KCONEID;
                                    mstrcmmaster.PrimarySecondary = PrimarySecondaryID;
                                    mstrcmmaster.PreventiveDetective = PrevationControlID;
                                    mstrcmmaster.AutomatedManual = AutomatedControlID;
                                    mstrcmmaster.Frequency = Frequency;
                                    mstrcmmaster.GapDescription = GapDescription;
                                    mstrcmmaster.ProposedRecommendations = Recommendations;
                                    mstrcmmaster.ActionRemediationPlan = ActionRemediationplan;
                                    mstrcmmaster.IPE = IPE;
                                    mstrcmmaster.ERPSystem = ERPsystem;
                                    mstrcmmaster.FraudRiskIndecator = FRC;
                                    mstrcmmaster.UniqueReferred = UniqueReferred;
                                    mstrcmmaster.TestStrategy = TestStrategy;
                                    mstrcmmaster.DocumentsExamined = DocumentsExamined;
                                    mstrcmmaster.Riskrating = RiskRating;
                                    mstrcmmaster.ControlRating = ControlRating;
                                    mstrcmmaster.AuditObjective = AuditObjective;
                                    mstrcmmaster.AuditStep = AuditStep;
                                    mstrcmmaster.AnalysistobePerformed = Analysistobeperformed;
                                    mstrcmmaster.PreRequisiteList = PreRequisiteList;
                                    mstrcmmaster.IsProcessed = false;
                                    mstrcmmaster.IsDeleted = false;
                                    mstrcmmaster.Createdon = DateTime.Now;
                                    mstrcmmaster.CreatedBy = Portal.Common.AuthenticationHelper.UserID;
                                    mstrcmmaster.BranchList = string.Join(",", LocationList.ToArray());
                                    mstrcmmaster.VerticalList = string.Join(",", Verticallist.ToArray());
                                    mstrcmmaster.RCMType = RCMTypeCheck;

                                    if (!(ExistsNew(mstrcmmaster)))
                                    {
                                        MasterRCMList.Add(mstrcmmaster);
                                    }
                                }
                                sucess = RiskCategoryManagement.CreateBulkExcelRCMMaster(MasterRCMList);
                                #endregion

                                var transactionsQuery = (from row in entities.mst_RCMMaster
                                                         where row.IsProcessed == false
                                                         && row.IsDeleted == false
                                                         select row).ToList();

                                foreach (var item in transactionsQuery)
                                {
                                    #region Master Region  
                                    List<long> masterbranchlist = new List<long>();
                                    List<long> masterverticalist = new List<long>();
                                    List<long> masterriskcategory = new List<long>();
                                    List<long> masterasseration = new List<long>();
                                    if (!string.IsNullOrEmpty(item.BranchList))
                                    {
                                        masterbranchlist.Clear();
                                        masterbranchlist = item.BranchList.Trim(',').Split(',').Select(long.Parse).Distinct().ToList();
                                    }
                                    if (!string.IsNullOrEmpty(item.VerticalList))
                                    {
                                        masterverticalist.Clear();
                                        masterverticalist = item.VerticalList.Trim(',').Split(',').Select(long.Parse).Distinct().ToList();
                                    }
                                    if (!string.IsNullOrEmpty(item.RiskCategory))
                                    {
                                        masterriskcategory.Clear();
                                        masterriskcategory = item.RiskCategory.Trim(',').Split(',').Select(long.Parse).Distinct().ToList();
                                    }
                                    if (!string.IsNullOrEmpty(item.FinancialAssertion))
                                    {
                                        masterasseration.Clear();
                                        masterasseration = item.FinancialAssertion.Trim(',').Split(',').Select(long.Parse).Distinct().ToList();
                                    }
                                    foreach (var Branchitem in masterbranchlist)
                                    {
                                        if (!(RiskCategoryManagement.ExistsNew(item.ObjectiveRef, Convert.ToInt32(item.Processid), (long)item.SubProcessid,
                                            CustomerID, (int)Branchitem, Convert.ToString(item.RiskDesc), Convert.ToString(item.ControlObjective))))
                                        {
                                            #region risk not Exists
                                            RiskCategoryCreation riskcategorycreation = new RiskCategoryCreation()
                                            {
                                                ControlNo = item.ObjectiveRef,
                                                ProcessId = (long)item.Processid,
                                                SubProcessId = item.SubProcessid,
                                                ActivityDescription = item.RiskDesc,
                                                ControlObjective = item.ControlObjective,
                                                IsDeleted = false,
                                                LocationType = -1,
                                                IsInternalAudit = "N",
                                                CustomerId = CustomerID,
                                                CustomerBranchId = Branchitem,
                                            };
                                            sucess = CreateRiskCategoryCreation(riskcategorycreation);

                                            foreach (var riskcategoryitem in masterriskcategory)
                                            {
                                                RiskCategoryMapping riskcategorymapping = new RiskCategoryMapping()
                                                {
                                                    RiskCategoryCreationId = riskcategorycreation.Id,
                                                    RiskCategoryId = Convert.ToInt32(riskcategoryitem),
                                                    ProcessId = item.Processid,
                                                    SubProcessId = item.SubProcessid,
                                                    IsActive = true,
                                                };
                                                RiskCategoryManagement.CreateRiskCategoryMapping(riskcategorymapping);
                                            }
                                            foreach (var asserationitem in masterasseration)
                                            {
                                                AssertionsMapping assertionsmapping = new AssertionsMapping()
                                                {
                                                    RiskCategoryCreationId = riskcategorycreation.Id,
                                                    AssertionId = Convert.ToInt32(asserationitem),
                                                    ProcessId = item.Processid,
                                                    SubProcessId = item.SubProcessid,
                                                    IsActive = true,
                                                };
                                                RiskCategoryManagement.CreateAssertionsMapping(assertionsmapping);
                                            }

                                            foreach (var verticalitem in masterverticalist)
                                            {
                                                BranchVertical BranchVerticals = new BranchVertical();
                                                BranchVerticals.Branch = (int)Branchitem;
                                                BranchVerticals.CustomerID = CustomerID;
                                                BranchVerticals.VerticalID = (int)verticalitem;
                                                BranchVerticals.IsActive = true;
                                                if (!UserManagementRisk.VerticalBrachExist(CustomerID, (int)Branchitem, (int)verticalitem))
                                                {
                                                    UserManagementRisk.CreateVerticalBranchlist(BranchVerticals);
                                                }

                                                long auditStepMasterID = -1;
                                                if (!(RiskCategoryManagement.Exists((long)item.Processid, (long)item.SubProcessid, riskcategorycreation.Id,
                                                    Convert.ToInt32(item.Frequency), Branchitem, verticalitem, (long)item.ActivityId, item.Controldescription)))
                                                {
                                                    #region Control not Exists                                           
                                                    RiskActivityTransaction riskactivitytransaction = new RiskActivityTransaction()
                                                    {
                                                        CustomerBranchId = Branchitem,
                                                        VerticalsId = verticalitem,
                                                        ProcessId = (long)item.Processid,
                                                        SubProcessId = (long)item.SubProcessid,
                                                        RiskCreationId = riskcategorycreation.Id,
                                                        ControlDescription = item.Controldescription,
                                                        MControlDescription = item.MCDescription,
                                                        PersonResponsible = (long)item.ProcessOwnerID,
                                                        EffectiveDate = item.EffectiveDate,
                                                        Key_Value = (long)item.KC1,
                                                        PrevationControl = (long)item.PreventiveDetective,
                                                        AutomatedControl = (long)item.AutomatedManual,
                                                        Frequency = (long)item.Frequency,
                                                        IsDeleted = false,
                                                        GapDescription = item.GapDescription,
                                                        Recommendations = item.ProposedRecommendations,
                                                        ActionRemediationplan = item.ActionRemediationPlan,
                                                        IPE = item.IPE,
                                                        ERPsystem = item.ERPSystem,
                                                        FRC = item.FraudRiskIndecator,
                                                        UniqueReferred = item.UniqueReferred,
                                                        TestStrategy = item.TestStrategy,
                                                        RiskRating = (int)item.Riskrating,
                                                        ControlRating = (int)item.ControlRating,
                                                        ProcessScore = 0,
                                                        ActivityID = item.ActivityId,
                                                        KC2 = item.KC2,
                                                        Primary_Secondary = (long)item.PrimarySecondary,
                                                        ControlOwner = (long)item.ControlOwnerID,
                                                        ISICFR_Close = true,
                                                        RCMType=item.RCMType,
                                                        DocumentsExamined=item.DocumentsExamined
                                                    };
                                                    sucess = CreateRiskActivityTransaction(riskactivitytransaction);

                                                    if (!(RiskCategoryManagement.RiskActivityToBeDoneMappingExists(riskcategorycreation.Id, riskactivitytransaction.Id,
                                                        Branchitem, (int)verticalitem, (int)item.Processid, (int)item.SubProcessid, item.AuditStep)))
                                                    {
                                                        auditStepMasterID = RiskCategoryManagement.GetAuditStepMasterID(item.AuditStep);
                                                        if (auditStepMasterID == 0)
                                                        {
                                                            AuditStepMaster newASM = new AuditStepMaster()
                                                            {
                                                                AuditStep = item.AuditStep
                                                            };

                                                            if (RiskCategoryManagement.CreateAuditStepMaster(newASM))
                                                                auditStepMasterID = newASM.ID;
                                                        }

                                                        if (auditStepMasterID != 0)
                                                        {
                                                            RiskActivityToBeDoneMapping RATBDM = new RiskActivityToBeDoneMapping()
                                                            {
                                                                AuditStepMasterID = auditStepMasterID,
                                                                RiskCategoryCreationId = riskcategorycreation.Id,
                                                                RiskActivityId = riskactivitytransaction.Id,
                                                                CustomerBranchID = Branchitem,
                                                                VerticalID = verticalitem,
                                                                ProcessId = (long)item.Processid,
                                                                SubProcessId = item.SubProcessid,
                                                                AuditObjective = item.AuditObjective,
                                                                ActivityTobeDone = item.AuditStep,
                                                                Analyis_To_Be_Performed = item.AnalysistobePerformed,
                                                                Rating = 1,
                                                                IsActive = true,
                                                                RCMType=item.RCMType
                                                            };
                                                            sucess = RiskCategoryManagement.CreateRiskActivityTBDMapping(RATBDM);

                                                            #region   PreRequisite
                                                            if (!String.IsNullOrEmpty(item.PreRequisiteList))
                                                            {
                                                                List<StepChecklistMapping> SCMRecords = new List<StepChecklistMapping>();
                                                                string prerequisite = item.PreRequisiteList;
                                                                string[] split = prerequisite.Split('|');
                                                                if (split.Length > 0)
                                                                {
                                                                    string prerequisiteName = string.Empty;
                                                                    for (int rs = 0; rs < split.Length; rs++)
                                                                    {
                                                                        prerequisiteName = Convert.ToString(split[rs].ToString().Trim());
                                                                        if (!String.IsNullOrEmpty(prerequisiteName))
                                                                        {
                                                                            StepChecklistMapping SCM = new StepChecklistMapping();
                                                                            SCM.ATBDID = RATBDM.ID;
                                                                            SCM.ChecklistDocument = prerequisiteName;
                                                                            SCM.IsActive = true;
                                                                            SCMRecords.Add(SCM);
                                                                        }
                                                                    }

                                                                    if (SCMRecords.Count > 0)
                                                                    {
                                                                        sucess = RiskCategoryManagement.CreateStepChecklistMapping(SCMRecords);
                                                                        SCMRecords.Clear();
                                                                    }
                                                                }
                                                            }

                                                            #endregion
                                                        }
                                                    }
                                                    else
                                                    {
                                                        sucess = RiskCategoryManagement.RiskActivityToBeDoneMappingUpdate(riskcategorycreation.Id, riskactivitytransaction.Id, Branchitem, (int)verticalitem, (int)item.Processid, (int)item.SubProcessid, item.AuditStep, item.AuditObjective);
                                                    }
                                                    #endregion
                                                }
                                                else
                                                {
                                                    #region Control Exists
                                                    int controlactivityid = RiskCategoryManagement.GetControlCreationIdNew((long)item.Processid, (int)item.SubProcessid, riskcategorycreation.Id, item.Controldescription, (int)item.Frequency, (int)Branchitem, verticalitem, (long)item.ActivityId);
                                                    if (!(RiskCategoryManagement.RiskActivityToBeDoneMappingExists(riskcategorycreation.Id, controlactivityid,
                                                        Branchitem, (int)verticalitem, (int)item.Processid, (int)item.SubProcessid, item.AuditStep)))
                                                    {
                                                        auditStepMasterID = RiskCategoryManagement.GetAuditStepMasterID(item.AuditStep);
                                                        if (auditStepMasterID == 0)
                                                        {
                                                            AuditStepMaster newASM = new AuditStepMaster()
                                                            {
                                                                AuditStep = item.AuditStep
                                                            };

                                                            if (RiskCategoryManagement.CreateAuditStepMaster(newASM))
                                                                auditStepMasterID = newASM.ID;
                                                        }

                                                        if (auditStepMasterID != 0)
                                                        {
                                                            RiskActivityToBeDoneMapping RATBDM = new RiskActivityToBeDoneMapping()
                                                            {
                                                                AuditStepMasterID = auditStepMasterID,
                                                                RiskCategoryCreationId = riskcategorycreation.Id,
                                                                RiskActivityId = controlactivityid,
                                                                CustomerBranchID = Branchitem,
                                                                VerticalID = verticalitem,
                                                                ProcessId = (long)item.Processid,
                                                                SubProcessId = item.SubProcessid,
                                                                AuditObjective = item.AuditObjective,
                                                                ActivityTobeDone = item.AuditStep,
                                                                Analyis_To_Be_Performed = item.AnalysistobePerformed,
                                                                Rating = 1,
                                                                IsActive = true,
                                                                RCMType=item.RCMType
                                                            };
                                                            sucess = RiskCategoryManagement.CreateRiskActivityTBDMapping(RATBDM);

                                                            #region   PreRequisite
                                                            if (!String.IsNullOrEmpty(item.PreRequisiteList))
                                                            {
                                                                List<StepChecklistMapping> SCMRecords = new List<StepChecklistMapping>();
                                                                string prerequisite = item.PreRequisiteList;
                                                                string[] split = prerequisite.Split('|');
                                                                if (split.Length > 0)
                                                                {
                                                                    string prerequisiteName = string.Empty;
                                                                    for (int rs = 0; rs < split.Length; rs++)
                                                                    {
                                                                        prerequisiteName = Convert.ToString(split[rs].ToString().Trim());
                                                                        if (!String.IsNullOrEmpty(prerequisiteName))
                                                                        {
                                                                            StepChecklistMapping SCM = new StepChecklistMapping();
                                                                            SCM.ATBDID = RATBDM.ID;
                                                                            SCM.ChecklistDocument = prerequisiteName;
                                                                            SCM.IsActive = true;
                                                                            SCMRecords.Add(SCM);
                                                                        }
                                                                    }

                                                                    if (SCMRecords.Count > 0)
                                                                    {
                                                                        sucess = RiskCategoryManagement.CreateStepChecklistMapping(SCMRecords);
                                                                        SCMRecords.Clear();
                                                                    }
                                                                }
                                                            }
                                                            #endregion
                                                        }
                                                    }
                                                    else
                                                    {
                                                        sucess = RiskCategoryManagement.RiskActivityToBeDoneMappingUpdate(riskcategorycreation.Id, controlactivityid, Branchitem, (int)verticalitem, (int)item.Processid, (int)item.SubProcessid, item.AuditStep, item.AuditObjective);
                                                        int atbdid = RiskCategoryManagement.GetATBDID(riskcategorycreation.Id, controlactivityid, Branchitem, (int)verticalitem, (int)item.Processid, (int)item.SubProcessid, item.AuditStep);
                                                        #region   PreRequisite
                                                        if (!String.IsNullOrEmpty(item.PreRequisiteList))
                                                        {
                                                            List<StepChecklistMapping> SCMRecords = new List<StepChecklistMapping>();
                                                            string prerequisite = item.PreRequisiteList;
                                                            string[] split = prerequisite.Split('|');
                                                            if (split.Length > 0)
                                                            {
                                                                string prerequisiteName = string.Empty;
                                                                for (int rs = 0; rs < split.Length; rs++)
                                                                {
                                                                    prerequisiteName = Convert.ToString(split[rs].ToString().Trim());
                                                                    if (!String.IsNullOrEmpty(prerequisiteName))
                                                                    {
                                                                        StepChecklistMapping SCM = new StepChecklistMapping();
                                                                        SCM.ATBDID = atbdid;
                                                                        SCM.ChecklistDocument = prerequisiteName;
                                                                        SCM.IsActive = true;
                                                                        if (!(PrerequsiteExistsNew(SCM)))
                                                                        {
                                                                            SCMRecords.Add(SCM);
                                                                        }
                                                                    }
                                                                }
                                                                if (SCMRecords.Count > 0)
                                                                {
                                                                    sucess = RiskCategoryManagement.CreateStepChecklistMapping(SCMRecords);
                                                                    SCMRecords.Clear();
                                                                }
                                                            }
                                                        }
                                                        #endregion
                                                    }
                                                    #endregion
                                                }

                                                #region AuditAssignment
                                                if (!(RiskCategoryManagement.Exists_Internal_AuditAssignment(Branchitem, verticalitem, CustomerID)))
                                                {
                                                    Internal_AuditAssignment InA = new Internal_AuditAssignment();
                                                    InA.AssignedTo = "I";
                                                    InA.VerticalID = (int)verticalitem;
                                                    InA.CustomerBranchid = Branchitem;
                                                    InA.ExternalAuditorId = -1;
                                                    InA.CreatedBy = Common.AuthenticationHelper.UserID;
                                                    InA.CreatedOn = DateTime.Now.Date;
                                                    InA.IsActive = false;
                                                    InA.CustomerId = (int)Common.AuthenticationHelper.CustomerID;
                                                    entities.Internal_AuditAssignment.Add(InA);
                                                    entities.SaveChanges();
                                                }
                                                #endregion
                                            }
                                            #endregion
                                        }
                                        else
                                        {
                                            #region risk Exists                            
                                            int riskcreationId = RiskCategoryManagement.GetRiskCreationIdNew(item.RiskDesc, item.ControlObjective, Convert.ToInt32(item.Processid), CustomerID, (int)Branchitem, (int)item.SubProcessid, item.ObjectiveRef);
                                            foreach (var riskcategoryitem in masterriskcategory)
                                            {
                                                RiskCategoryMapping riskcategorymapping = new RiskCategoryMapping()
                                                {
                                                    RiskCategoryCreationId = riskcreationId,
                                                    RiskCategoryId = Convert.ToInt32(riskcategoryitem),
                                                    ProcessId = item.Processid,
                                                    SubProcessId = item.SubProcessid,
                                                    IsActive = true,
                                                };
                                                if (!RiskCategoryManagement.RiskCategoryMappingExists(Convert.ToInt64(riskcategoryitem), riskcreationId))
                                                {
                                                    RiskCategoryManagement.CreateRiskCategoryMapping(riskcategorymapping);
                                                }
                                            }
                                            foreach (var asserationitem in masterasseration)
                                            {
                                                AssertionsMapping assertionsmapping = new AssertionsMapping()
                                                {
                                                    RiskCategoryCreationId = riskcreationId,
                                                    AssertionId = Convert.ToInt32(asserationitem),
                                                    ProcessId = item.Processid,
                                                    SubProcessId = item.SubProcessid,
                                                    IsActive = true,
                                                };
                                                if (!AssertionsMappingExists(Convert.ToInt64(asserationitem), riskcreationId))
                                                {
                                                    RiskCategoryManagement.CreateAssertionsMapping(assertionsmapping);
                                                }
                                            }
                                            foreach (var verticalitem in masterverticalist)
                                            {
                                                long auditStepMasterID = -1;
                                                if (!(RiskCategoryManagement.Exists((long)item.Processid, (long)item.SubProcessid, riskcreationId,
                                                    Convert.ToInt32(item.Frequency), Branchitem, verticalitem, (long)item.ActivityId, item.Controldescription)))
                                                {
                                                    #region Control not Exists
                                                    //RiskActivityTransaction riskactivitytransaction = new RiskActivityTransaction();
                                                    //riskactivitytransaction.CustomerBranchId = Branchitem;
                                                    //riskactivitytransaction.VerticalsId = verticalitem;
                                                    //riskactivitytransaction.ProcessId = (long)item.Processid;
                                                    //riskactivitytransaction.SubProcessId = (long)item.SubProcessid;
                                                    //riskactivitytransaction.RiskCreationId = riskcreationId;
                                                    //riskactivitytransaction.ControlDescription = item.Controldescription;
                                                    //riskactivitytransaction.MControlDescription = item.MCDescription;
                                                    //riskactivitytransaction.PersonResponsible = (long)item.ProcessOwnerID;
                                                    //riskactivitytransaction.EffectiveDate = item.EffectiveDate.Value.Date;
                                                    //riskactivitytransaction.Key_Value = (long)item.KC1;
                                                    //riskactivitytransaction.PrevationControl = (long)item.PreventiveDetective;
                                                    //riskactivitytransaction.AutomatedControl = (long)item.AutomatedManual;
                                                    //riskactivitytransaction.Frequency = (long)item.Frequency;
                                                    //riskactivitytransaction.IsDeleted = false;
                                                    //riskactivitytransaction.GapDescription = item.GapDescription;
                                                    //riskactivitytransaction.Recommendations = item.ProposedRecommendations;
                                                    //riskactivitytransaction.ActionRemediationplan = item.ActionRemediationPlan;
                                                    //riskactivitytransaction.IPE = item.IPE;
                                                    //riskactivitytransaction.ERPsystem = item.ERPSystem;
                                                    //riskactivitytransaction.FRC = item.FraudRiskIndecator;
                                                    //riskactivitytransaction.UniqueReferred = item.UniqueReferred;
                                                    //riskactivitytransaction.TestStrategy = item.TestStrategy;
                                                    //riskactivitytransaction.RiskRating = (int)item.Riskrating;
                                                    //riskactivitytransaction.ControlRating = (int)item.ControlRating;
                                                    //riskactivitytransaction.ProcessScore = 0;
                                                    //riskactivitytransaction.ActivityID = item.ActivityId;
                                                    //riskactivitytransaction.KC2 = item.KC2;
                                                    //riskactivitytransaction.Primary_Secondary = (long)item.PrimarySecondary;
                                                    //riskactivitytransaction.ControlOwner = (long)item.ControlOwnerID;
                                                    //riskactivitytransaction.ISICFR_Close = true;
                                                    //entities.RiskActivityTransactions.Add(riskactivitytransaction);
                                                    //entities.SaveChanges();

                                                    RiskActivityTransaction riskactivitytransaction = new RiskActivityTransaction()
                                                    {
                                                        CustomerBranchId = Branchitem,
                                                        VerticalsId = verticalitem,
                                                        ProcessId = (long)item.Processid,
                                                        SubProcessId = (long)item.SubProcessid,
                                                        RiskCreationId = riskcreationId,
                                                        ControlDescription = item.Controldescription,
                                                        MControlDescription = item.MCDescription,
                                                        PersonResponsible = (long)item.ProcessOwnerID,
                                                        EffectiveDate = item.EffectiveDate,
                                                        Key_Value = (long)item.KC1,
                                                        PrevationControl = (long)item.PreventiveDetective,
                                                        AutomatedControl = (long)item.AutomatedManual,
                                                        Frequency = (long)item.Frequency,
                                                        IsDeleted = false,
                                                        GapDescription = item.GapDescription,
                                                        Recommendations = item.ProposedRecommendations,
                                                        ActionRemediationplan = item.ActionRemediationPlan,
                                                        IPE = item.IPE,
                                                        ERPsystem = item.ERPSystem,
                                                        FRC = item.FraudRiskIndecator,
                                                        UniqueReferred = item.UniqueReferred,
                                                        TestStrategy = item.TestStrategy,
                                                        RiskRating = (int)item.Riskrating,
                                                        ControlRating = (int)item.ControlRating,
                                                        ProcessScore = 0,
                                                        ActivityID = item.ActivityId,
                                                        KC2 = item.KC2,
                                                        Primary_Secondary = (long)item.PrimarySecondary,
                                                        ControlOwner = (long)item.ControlOwnerID,
                                                        ISICFR_Close = true,
                                                        RCMType=item.RCMType,
                                                        DocumentsExamined=item.DocumentsExamined,
                                                    };
                                                    sucess = CreateRiskActivityTransaction(riskactivitytransaction);

                                                    if (!(RiskCategoryManagement.RiskActivityToBeDoneMappingExists(riskcreationId, riskactivitytransaction.Id,
                                                        Branchitem, (int)verticalitem, (int)item.Processid, (int)item.SubProcessid, item.AuditStep)))
                                                    {
                                                        auditStepMasterID = RiskCategoryManagement.GetAuditStepMasterID(item.AuditStep);
                                                        if (auditStepMasterID == 0)
                                                        {
                                                            AuditStepMaster newASM = new AuditStepMaster()
                                                            {
                                                                AuditStep = item.AuditStep
                                                            };

                                                            if (RiskCategoryManagement.CreateAuditStepMaster(newASM))
                                                                auditStepMasterID = newASM.ID;
                                                        }

                                                        if (auditStepMasterID != 0)
                                                        {
                                                            RiskActivityToBeDoneMapping RATBDM = new RiskActivityToBeDoneMapping()
                                                            {
                                                                AuditStepMasterID = auditStepMasterID,
                                                                RiskCategoryCreationId = riskcreationId,
                                                                RiskActivityId = riskactivitytransaction.Id,
                                                                CustomerBranchID = Branchitem,
                                                                VerticalID = verticalitem,
                                                                ProcessId = (long)item.Processid,
                                                                SubProcessId = item.SubProcessid,
                                                                AuditObjective = item.AuditObjective,
                                                                ActivityTobeDone = item.AuditStep,
                                                                Analyis_To_Be_Performed = item.AnalysistobePerformed,
                                                                Rating = 1,
                                                                IsActive = true,
                                                                RCMType=item.RCMType
                                                            };
                                                            sucess = RiskCategoryManagement.CreateRiskActivityTBDMapping(RATBDM);

                                                            #region   PreRequisite
                                                            if (!String.IsNullOrEmpty(item.PreRequisiteList))
                                                            {
                                                                List<StepChecklistMapping> SCMRecords = new List<StepChecklistMapping>();
                                                                string prerequisite = item.PreRequisiteList;
                                                                string[] split = prerequisite.Split('|');
                                                                if (split.Length > 0)
                                                                {
                                                                    string prerequisiteName = string.Empty;
                                                                    for (int rs = 0; rs < split.Length; rs++)
                                                                    {
                                                                        prerequisiteName = Convert.ToString(split[rs].ToString().Trim());
                                                                        if (!String.IsNullOrEmpty(prerequisiteName))
                                                                        {
                                                                            StepChecklistMapping SCM = new StepChecklistMapping();
                                                                            SCM.ATBDID = RATBDM.ID;
                                                                            SCM.ChecklistDocument = prerequisiteName;
                                                                            SCM.IsActive = true;
                                                                            SCMRecords.Add(SCM);
                                                                        }
                                                                    }

                                                                    if (SCMRecords.Count > 0)
                                                                    {
                                                                        sucess = RiskCategoryManagement.CreateStepChecklistMapping(SCMRecords);
                                                                        SCMRecords.Clear();
                                                                    }
                                                                }
                                                            }

                                                            #endregion
                                                        }
                                                    }
                                                    else
                                                    {
                                                        sucess = RiskCategoryManagement.RiskActivityToBeDoneMappingUpdate(riskcreationId, riskactivitytransaction.Id, Branchitem, (int)verticalitem, (int)item.Processid, (int)item.SubProcessid, item.AuditStep, item.AuditObjective);
                                                    }
                                                    #endregion
                                                }
                                                else
                                                {
                                                    #region Control Exists
                                                    int controlactivityid = RiskCategoryManagement.GetControlCreationIdNew((long)item.Processid, (int)item.SubProcessid, riskcreationId, item.Controldescription, (int)item.Frequency, (int)Branchitem, verticalitem, (long)item.ActivityId);

                                                    if (!(RiskCategoryManagement.RiskActivityToBeDoneMappingExists(riskcreationId, controlactivityid,
                                                        Branchitem, (int)verticalitem, (int)item.Processid, (int)item.SubProcessid, item.AuditStep)))
                                                    {
                                                        auditStepMasterID = RiskCategoryManagement.GetAuditStepMasterID(item.AuditStep);
                                                        if (auditStepMasterID == 0)
                                                        {
                                                            AuditStepMaster newASM = new AuditStepMaster()
                                                            {
                                                                AuditStep = item.AuditStep
                                                            };

                                                            if (RiskCategoryManagement.CreateAuditStepMaster(newASM))
                                                                auditStepMasterID = newASM.ID;
                                                        }

                                                        if (auditStepMasterID != 0)
                                                        {
                                                            RiskActivityToBeDoneMapping RATBDM = new RiskActivityToBeDoneMapping()
                                                            {
                                                                AuditStepMasterID = auditStepMasterID,
                                                                RiskCategoryCreationId = riskcreationId,
                                                                RiskActivityId = controlactivityid,
                                                                CustomerBranchID = Branchitem,
                                                                VerticalID = verticalitem,
                                                                ProcessId = (long)item.Processid,
                                                                SubProcessId = item.SubProcessid,
                                                                AuditObjective = item.AuditObjective,
                                                                ActivityTobeDone = item.AuditStep,
                                                                Analyis_To_Be_Performed = item.AnalysistobePerformed,
                                                                Rating = 1,
                                                                IsActive = true,
                                                                RCMType=item.RCMType
                                                            };
                                                            sucess = RiskCategoryManagement.CreateRiskActivityTBDMapping(RATBDM);

                                                            #region   PreRequisite
                                                            if (!String.IsNullOrEmpty(item.PreRequisiteList))
                                                            {
                                                                List<StepChecklistMapping> SCMRecords = new List<StepChecklistMapping>();
                                                                string prerequisite = item.PreRequisiteList;
                                                                string[] split = prerequisite.Split('|');
                                                                if (split.Length > 0)
                                                                {
                                                                    string prerequisiteName = string.Empty;
                                                                    for (int rs = 0; rs < split.Length; rs++)
                                                                    {
                                                                        prerequisiteName = Convert.ToString(split[rs].ToString().Trim());
                                                                        if (!String.IsNullOrEmpty(prerequisiteName))
                                                                        {
                                                                            StepChecklistMapping SCM = new StepChecklistMapping();
                                                                            SCM.ATBDID = RATBDM.ID;
                                                                            SCM.ChecklistDocument = prerequisiteName;
                                                                            SCM.IsActive = true;
                                                                            SCMRecords.Add(SCM);
                                                                        }
                                                                    }

                                                                    if (SCMRecords.Count > 0)
                                                                    {
                                                                        sucess = RiskCategoryManagement.CreateStepChecklistMapping(SCMRecords);
                                                                        SCMRecords.Clear();
                                                                    }
                                                                }
                                                            }
                                                            #endregion
                                                        }
                                                    }
                                                    else
                                                    {
                                                        sucess = RiskCategoryManagement.RiskActivityToBeDoneMappingUpdate(riskcreationId, controlactivityid, Branchitem, (int)verticalitem, (int)item.Processid, (int)item.SubProcessid, item.AuditStep, item.AuditObjective);
                                                        int atbdid = RiskCategoryManagement.GetATBDID(riskcreationId, controlactivityid, Branchitem, (int)verticalitem, (int)item.Processid, (int)item.SubProcessid, item.AuditStep);
                                                        #region   PreRequisite
                                                        if (!String.IsNullOrEmpty(item.PreRequisiteList))
                                                        {
                                                            List<StepChecklistMapping> SCMRecords = new List<StepChecklistMapping>();
                                                            string prerequisite = item.PreRequisiteList;
                                                            string[] split = prerequisite.Split('|');
                                                            if (split.Length > 0)
                                                            {
                                                                string prerequisiteName = string.Empty;
                                                                for (int rs = 0; rs < split.Length; rs++)
                                                                {
                                                                    prerequisiteName = Convert.ToString(split[rs].ToString().Trim());
                                                                    if (!String.IsNullOrEmpty(prerequisiteName))
                                                                    {
                                                                        StepChecklistMapping SCM = new StepChecklistMapping();
                                                                        SCM.ATBDID = atbdid;
                                                                        SCM.ChecklistDocument = prerequisiteName;
                                                                        SCM.IsActive = true;
                                                                        if (!(PrerequsiteExistsNew(SCM)))
                                                                        {
                                                                            SCMRecords.Add(SCM);
                                                                        }
                                                                    }
                                                                }
                                                                if (SCMRecords.Count > 0)
                                                                {
                                                                    sucess = RiskCategoryManagement.CreateStepChecklistMapping(SCMRecords);
                                                                    SCMRecords.Clear();
                                                                }
                                                            }
                                                        }
                                                        #endregion
                                                    }
                                                    #endregion
                                                }
                                            }
                                            #endregion
                                        }
                                    }
                                    #endregion

                                    RCMMasterUpdate(item.ID);
                                }
                            }
                        }
                        else
                        {
                            errorMessage.Add("Sheet cannot be empty");
                            ErrorMessages(errorMessage);
                            ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 0, function () { });", true);
                            sucess = false;
                        }
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                sucess = false;
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static bool RCMMasterUpdate(long rcmID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_RCMMaster
                             where row.ID == rcmID
                             select row).FirstOrDefault();


                if (query != null)
                {
                    query.IsProcessed = true;
                    entities.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public DateTime GetDate(string date)
        {
            string date1 = "";
            if (date.Contains("/"))
            {
                date1 = date.Trim().Substring(date.IndexOf("/") + 4, 4) + "-" + date.Substring(date.IndexOf("/") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains("-"))
            {
                date1 = date.Substring(date.IndexOf("-") + 4, 4) + "-" + date.Substring(date.IndexOf("-") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains(" "))
            {
                date1 = date.Substring(date.IndexOf(" ") + 4, 4) + "-" + date.Substring(date.IndexOf(" ") + 1, 2) + "-" + date.Substring(0, 2);
            }
            return Convert.ToDateTime(date1);
        }
        public static DateTime? CleanDateField(string DateField)
        {
            // Convert the text to DateTime and return the value or null
            DateTime? CleanDate = new DateTime();
            int intDate;
            bool DateIsInt = int.TryParse(DateField, out intDate);
            if (DateIsInt)
            {
                // If this is a serial date, convert it
                CleanDate = DateTime.FromOADate(intDate);
            }
            else if (DateField.Length != 0 && DateField != "1/1/0001 12:00:00 AM" &&
                DateField != "1/1/1753 12:00:00 AM")
            {
                // Convert from a General format
                CleanDate = (Convert.ToDateTime(DateField));
            }
            else
            {
                // Date is blank
                CleanDate = null;
            }
            return CleanDate;
        }
        #endregion

        #region Export  Risk Control Matrix
        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
               server control at run time. */
        }
        public List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchy(int customerID, int customerbranchid)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_CustomerBranch
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }
            return hierarchy;
        }
        public void LoadSubEntities(int customerid, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp, bool isClient, AuditControlEntities entities)
        {
            IQueryable<mst_CustomerBranch> query = (from row in entities.mst_CustomerBranch
                                                    where row.IsDeleted == false && row.CustomerID == customerid
                                                     && row.ParentID == nvp.ID
                                                    select row);
            var subEntities = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }
        public void BindProcess()
        {
            long customerID = -1;
            customerID = Common.AuthenticationHelper.CustomerID;

            ddlFilterProcess.DataTextField = "Name";
            ddlFilterProcess.DataValueField = "ID";
            ddlFilterProcess.Items.Clear();
            ddlFilterProcess.DataSource = ProcessManagement.GetAllProcess(customerID);
            ddlFilterProcess.DataBind();
            ddlFilterProcess.Items.Insert(0, new ListItem("Select Process", "-1"));
        }
        private void BindSubProcess(long Processid, string flag)
        {
            try
            {
                if (flag == "P")
                {
                    ddlFilterSubProcess.Items.Clear();
                    ddlFilterSubProcess.DataTextField = "Name";
                    ddlFilterSubProcess.DataValueField = "Id";
                    ddlFilterSubProcess.DataSource = ProcessManagement.FillSubProcess(Processid, flag);
                    ddlFilterSubProcess.DataBind();
                    ddlFilterSubProcess.Items.Insert(0, new ListItem("Sub Process", "-1"));
                }
                else
                {
                    ddlFilterSubProcess.Items.Clear();
                    ddlFilterSubProcess.DataTextField = "Name";
                    ddlFilterSubProcess.DataValueField = "Id";
                    ddlFilterSubProcess.DataSource = ProcessManagement.FillSubProcess(Processid, flag);
                    ddlFilterSubProcess.DataBind();
                    ddlFilterSubProcess.Items.Insert(0, new ListItem("Sub Process", "-1"));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlLegalEntity_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity1, Convert.ToInt32(ddlLegalEntity.SelectedValue));
                }
            }
            else
            {
                if (ddlSubEntity1.Items.Count > 0)
                    ddlSubEntity1.Items.Clear();

                if (ddlSubEntity2.Items.Count > 0)
                    ddlSubEntity2.Items.Clear();

                if (ddlSubEntity3.Items.Count > 0)
                    ddlSubEntity3.Items.Clear();

                if (ddlFilterLocation.Items.Count > 0)
                    ddlFilterLocation.Items.Clear();
            }
        }
        protected void ddlSubEntity1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity2, Convert.ToInt32(ddlSubEntity1.SelectedValue));
                }
            }
            else
            {
                if (ddlSubEntity2.Items.Count > 0)
                    ddlSubEntity2.ClearSelection();

                if (ddlSubEntity3.Items.Count > 0)
                    ddlSubEntity3.ClearSelection();

                if (ddlFilterLocation.Items.Count > 0)
                    ddlFilterLocation.ClearSelection();
            }
        }
        protected void ddlSubEntity2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity3, Convert.ToInt32(ddlSubEntity2.SelectedValue));
                }
            }
            else
            {
                if (ddlSubEntity3.Items.Count > 0)
                    ddlSubEntity3.ClearSelection();

                if (ddlFilterLocation.Items.Count > 0)
                    ddlFilterLocation.ClearSelection();
            }
        }
        protected void ddlSubEntity3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlFilterLocation, Convert.ToInt32(ddlSubEntity3.SelectedValue));
                }
            }
            else
            {
                if (ddlFilterLocation.Items.Count > 0)
                    ddlFilterLocation.ClearSelection();
            }
        }
        protected void ddlFilterLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
        }
        protected void ddlFilterProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlFilterProcess.SelectedValue))
            {
                if (ddlFilterProcess.SelectedValue != "-1")
                {
                    BindSubProcess(Convert.ToInt32(ddlFilterProcess.SelectedValue), "P");
                }
                else
                {
                    if (ddlFilterSubProcess.Items.Count > 0)
                        ddlFilterSubProcess.Items.Clear();
                }
            }
            else
            {
                if (ddlFilterSubProcess.Items.Count > 0)
                    ddlFilterSubProcess.Items.Clear();
            }
        }
        protected void ddlFilterSubProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private List<sp_AuditStepExport_Result> GetDataExportUpdate(string Flag)
        {
            int CustomerBranchId = -1;
            int processid = -1;
            int subProcessid = -1;
            int verticalid = -1;
            int customerID = -1;
            customerID = Convert.ToInt32(Common.AuthenticationHelper.CustomerID);
            List<sp_AuditStepExport_Result> AuditStepList = new List<sp_AuditStepExport_Result>();
            if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
            {
                if (ddlFilterLocation.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlFilterProcess.SelectedValue))
            {
                if (ddlFilterProcess.SelectedValue != "-1")
                {
                    processid = Convert.ToInt32(ddlFilterProcess.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlFilterSubProcess.SelectedValue))
            {
                if (ddlFilterSubProcess.SelectedValue != "-1")
                {
                    subProcessid = Convert.ToInt32(ddlFilterSubProcess.SelectedValue);
                }
            }
            if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 0)
            {
                int vid = UserManagementRisk.VerticalgetBycustomerid(Portal.Common.AuthenticationHelper.CustomerID);
                if (vid != -1)
                {
                    verticalid = vid;
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(ddlVertical.SelectedValue))
                {
                    if (ddlVertical.SelectedValue != "-1")
                    {
                        verticalid = Convert.ToInt32(ddlVertical.SelectedValue);
                    }
                }
            }
            Branchlist.Clear();
            GetAllHierarchy(customerID, CustomerBranchId);

            if (Flag == "L")
            {
                AuditStepList = GetRCMAndAuditStepExportForUpdate(customerID, Branchlist, processid, subProcessid, verticalid, "", "P");
            }
            else if (Flag == "P")
            {
                AuditStepList = GetRCMAndAuditStepExportForUpdate(customerID, Branchlist, processid, subProcessid, verticalid, "", "P");
            }

            return AuditStepList;
        }
        public static List<sp_AuditStepExport_Result> GetRCMAndAuditStepExportForUpdate(long Customerid, List<long> BranchList, int ProcessId, int subProcessID, int verticalid, string filter, string Flag)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                entities.Database.CommandTimeout = 300;
                List<sp_AuditStepExport_Result> riskcategorycreations = new List<sp_AuditStepExport_Result>();

                if (Flag == "P")
                {
                    #region Process and Sub Process

                    if (BranchList.Count > 0 && ProcessId != -1)
                    {
                        riskcategorycreations = (from row in entities.sp_AuditStepExport((int)Customerid)
                                                 where row.CustomerID == Customerid
                                                  && BranchList.Contains((long)row.BranchId)
                                                 && row.ProcessId == ProcessId
                                                 orderby row.RiskCreationId
                                                 select row).ToList();
                    }
                    else if (BranchList.Count > 0)
                    {
                        riskcategorycreations = (from row in entities.sp_AuditStepExport((int)Customerid)
                                                 where row.CustomerID == Customerid
                                                  && BranchList.Contains((long)row.BranchId)
                                                 orderby row.RiskCreationId
                                                 select row).ToList();
                    }
                    else if (ProcessId != -1)
                    {
                        riskcategorycreations = (from row in entities.sp_AuditStepExport((int)Customerid)
                                                 where row.CustomerID == Customerid
                                                 && row.ProcessId == ProcessId
                                                 orderby row.RiskCreationId
                                                 select row).ToList();
                    }
                    else
                    {
                        riskcategorycreations = (from row in entities.sp_AuditStepExport((int)Customerid)
                                                 where row.CustomerID == Customerid
                                                 orderby row.RiskCreationId
                                                 select row).ToList();
                    }
                    #endregion
                }
                else
                {
                    #region Non Process and sub Process
                    if (BranchList.Count > 0 && ProcessId != -1)
                    {
                        riskcategorycreations = (from row in entities.sp_AuditStepExport((int)Customerid)
                                                 where row.CustomerID == Customerid
                                                  && BranchList.Contains((long)row.BranchId)
                                                 && row.ProcessId == ProcessId
                                                 orderby row.RiskCreationId
                                                 select row).ToList();
                    }
                    else if (BranchList.Count > 0)
                    {
                        riskcategorycreations = (from row in entities.sp_AuditStepExport((int)Customerid)
                                                 where row.CustomerID == Customerid
                                                  && BranchList.Contains((long)row.BranchId)
                                                 orderby row.RiskCreationId
                                                 select row).ToList();
                    }
                    else if (ProcessId != -1)
                    {
                        riskcategorycreations = (from row in entities.sp_AuditStepExport((int)Customerid)
                                                 where row.CustomerID == Customerid
                                                 && row.ProcessId == ProcessId
                                                 orderby row.RiskCreationId
                                                 select row).ToList();
                    }
                    else
                    {
                        riskcategorycreations = (from row in entities.sp_AuditStepExport((int)Customerid)
                                                 where row.CustomerID == Customerid
                                                 orderby row.RiskCreationId
                                                 select row).ToList();
                    }
                    #endregion
                }

                if (verticalid != -1)
                    riskcategorycreations = riskcategorycreations.Where(entry => entry.VerticalId == verticalid).ToList();

                if (subProcessID != -1)
                    riskcategorycreations = riskcategorycreations.Where(entry => entry.SubProcessId == subProcessID).ToList();

                if (filter != "")
                    riskcategorycreations = riskcategorycreations
                                             .Where(entry => entry.ControlNo.Contains(filter)
                                             || entry.BranchName.Contains(filter)
                                             || entry.VerticalName.Contains(filter)
                                             || entry.ActivityTobeDone.Contains(filter)).ToList();

                if (riskcategorycreations.Count > 0)
                {
                    riskcategorycreations = riskcategorycreations.OrderBy(entry => entry.ActivityTobeDone)
                                                               //.ThenBy(entry => entry.VerticalName)
                                                               .ThenBy(entry => entry.ProcessName)
                                                               .ThenBy(entry => entry.SubProcessName)
                                                               .ToList();

                }

                return riskcategorycreations;
            }
        }
        #endregion
        private void ProcessData(ExcelPackage xlWorkbook)
        {
            try
            {
                ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["ProcessSubProcess"];
                int customerID = -1;
                customerID = Convert.ToInt32(Common.AuthenticationHelper.CustomerID);

                if (xlWorksheet != null)
                {
                    int count = 1;
                    int processid = -1;
                    string processName = string.Empty;
                    int xlrow2 = xlWorksheet.Dimension.End.Row;
                    List<mst_Subprocess> mstSubprocesslist = new List<mst_Subprocess>();
                    List<mst_Subprocess> mstSubprocesslist1 = new List<mst_Subprocess>();
                    List<ActivityDetails> ActivityDetailList = new List<ActivityDetails>();
                    List<ActivityDetails> ActivityDetailList1 = new List<ActivityDetails>();
                    List<mst_Activity> mstActivitylist = new List<mst_Activity>();

                    for (int i = 2; i <= xlrow2; i++)
                    {
                        count = count + 1;
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 1].Text.ToString()))
                        {
                            processid = ProcessManagement.GetProcessIDByName(xlWorksheet.Cells[i, 1].Text.ToString(), customerID);
                            processName = Convert.ToString(xlWorksheet.Cells[i, 1].Text.ToString().Trim());
                        }
                        if (processid == 0 || processid == -1)
                        {
                            sucess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Correct the Process Name at row number - " + count + " or Process not Defined in the System.";
                            break;
                        }
                        else
                        {
                            sucess = true;
                        }
                        string subprocessname = "";
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 2].Text.ToString().Trim()))
                        {
                            subprocessname = Convert.ToString(xlWorksheet.Cells[i, 2].Text.ToString().Trim());
                        }
                        if (string.IsNullOrEmpty(subprocessname))
                        {
                            sucess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Correct the Sub Process Name at row number - " + count + " or Sub Process not Defined in the System.";
                            break;
                        }
                        else
                        {
                            sucess = true;
                        }
                        string activityname = "";
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 3].Text.ToString().Trim()))
                        {
                            activityname = Convert.ToString(xlWorksheet.Cells[i, 3].Text.ToString().Trim());
                        }
                        if (!(ProcessManagement.ExistsProcess(processid, Convert.ToString(Regex.Replace(subprocessname.Trim(), @"\t|\n|\r", "")))))
                        {
                            if (!mstSubprocesslist.Any(x => x.ProcessId == processid && x.Name.ToUpper().Trim() == Regex.Replace(subprocessname.Trim(), @"\t|\n|\r", "").ToUpper().Trim()))
                            {
                                mst_Subprocess mstSubprocess = new mst_Subprocess();
                                mstSubprocess.Name = Regex.Replace(subprocessname.Trim(), @"\t|\n|\r", "");
                                mstSubprocess.ProcessId = processid;
                                mstSubprocess.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                mstSubprocess.IsDeleted = false;
                                mstSubprocess.CreatedOn = DateTime.Now;
                                mstSubprocess.IsProcessNonProcess = "P";
                                mstSubprocess.Scores = 0;
                                mstSubprocesslist.Add(mstSubprocess);
                            }
                        }//sub Process exists end

                        if (!string.IsNullOrEmpty(activityname))
                        {
                            ActivityDetails activity_details = new ActivityDetails();
                            activity_details.ProcessName = Regex.Replace(processName.Trim(), @"\t|\n|\r", "");
                            activity_details.ProcessId = processid;
                            activity_details.SubProcessName = Regex.Replace(subprocessname.Trim(), @"\t|\n|\r", "");
                            activity_details.ActivityName = Regex.Replace(activityname.Trim(), @"\t|\n|\r", "");
                            ActivityDetailList.Add(activity_details);
                        }

                    }
                    mstSubprocesslist1 = mstSubprocesslist.Where(entry => entry.ProcessId == 0).ToList();
                    ActivityDetailList1 = ActivityDetailList.Where(entry => entry.ProcessId == 0).ToList();
                    if (mstSubprocesslist1.Count == 0)
                    {
                        if (processid != 0)
                        {
                            if (processid != -1)
                            {
                                sucess = ProcessManagement.CreateExcelProcess(mstSubprocesslist);
                                if (sucess)
                                {
                                    if (ActivityDetailList1.Count == 0)
                                    {
                                        if (ActivityDetailList.Count > 0)
                                        {
                                            long subprocessid = -1;
                                            ActivityDetailList.ForEach(entry =>
                                            {
                                                subprocessid = ProcessManagement.GetSubProcessBYName(entry.ProcessId, entry.SubProcessName);
                                                if (subprocessid != -1)
                                                {
                                                    if (!(ProcessManagement.ExistsActivity(entry.ProcessId, subprocessid, Regex.Replace(entry.ActivityName.Trim(), @"\t|\n|\r", ""))))
                                                    {
                                                        if (!mstActivitylist.Any(x => x.ProcessId == entry.ProcessId && x.SubProcessId == subprocessid && x.Name.ToUpper().Trim() == Regex.Replace(entry.ActivityName.Trim(), @"\t|\n|\r", "").ToUpper().Trim()))
                                                        {
                                                            mst_Activity mstactivity = new mst_Activity();
                                                            mstactivity.Name = entry.ActivityName;
                                                            mstactivity.ProcessId = entry.ProcessId;
                                                            mstactivity.SubProcessId = subprocessid;
                                                            mstactivity.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                            mstactivity.IsDeleted = false;
                                                            mstactivity.CreatedOn = DateTime.Now;
                                                            mstActivitylist.Add(mstactivity);
                                                        }
                                                    }
                                                }
                                            });
                                            sucess = ProcessManagement.CreateExcelActivity(mstActivitylist);
                                            sucess = true;
                                        }
                                    }
                                }
                                else
                                {
                                    sucess = false;
                                }
                            }
                        }
                    }
                    else
                    {
                        sucess = false;
                    }
                }
            }
            catch (Exception ex)
            {
                sucess = false;
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private bool ProcessSheetsExitsts(ExcelPackage xlWorkbook, string data)
        {
            try
            {
                bool flag = false;
                foreach (ExcelWorksheet sheet in xlWorkbook.Workbook.Worksheets)
                {
                    if (data.Equals("ProcessSubProcess"))
                    {
                        if (sheet.Name.Trim().Equals("ProcessSubProcess") || sheet.Name.Trim().Equals("ProcessSubProcess") || sheet.Name.Trim().Equals("ProcessSubProcess"))
                        {
                            flag = true;
                            break;//added by Manisha
                        }
                        else
                        {
                            flag = false;
                            break;
                        }
                    }
                }
                return flag;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }
        protected void rdoCompliance_CheckedChanged(object sender, EventArgs e)
        {
            if (rdoRCMUpload.Checked == true)
                MasterFileUpload.Visible = true;
            BindLocationFilter();

            Divbranchlist.Style.Add("display", "block");
            BindddlVerticalBranch();
        }
        protected void rdoSubProcess_CheckedChanged(object sender, EventArgs e)
        {
            if (rdoSubProcess.Checked == true)
                MasterFileUpload.Visible = true;
            Divbranchlist.Style.Add("display", "none");
        }
        protected void txtFilter_TextChanged(object sender, EventArgs e)
        {
        }
        protected void Tab1_Click1(object sender, EventArgs e)
        {
            try
            {
                performerdocuments.Visible = true;
                reviewerdocuments.Visible = false;
                liNotDone.Attributes.Add("class", "active");
                liSubmitted.Attributes.Add("class", "");
                performerdocuments.Attributes.Remove("class");
                performerdocuments.Attributes.Add("class", "tab-pane active");
                reviewerdocuments.Attributes.Remove("class");
                reviewerdocuments.Attributes.Add("class", "tab-pane");
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void sampleForm_Click(object sender, EventArgs e)
        {
            WebClient req = new WebClient();
            HttpResponse response = HttpContext.Current.Response;
            string filePath = Server.MapPath("~/AuditSampleDocument/RiskControlMatrix_Audit Steps_Sample.xlsx");
            response.Clear();
            response.ClearContent();
            response.ClearHeaders();
            response.Buffer = true;
            response.AddHeader("Content-Disposition", "attachment;filename=RiskControlMatrix_Audit Steps_Sample.xlsx");
            byte[] data = req.DownloadData(filePath);
            Response.ContentType = "application/vnd.ms-excel";
            StringWriter sw = new StringWriter();
            response.BinaryWrite(data);
            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.Flush();
            Response.End();
        }
        protected void Tab2_Click1(object sender, EventArgs e)
        {
            try
            {

                BindLegalEntityData();
                BindVertical();
                BindProcess();
                txtFilter.Text = "";
                performerdocuments.Visible = false;
                reviewerdocuments.Visible = true;
                liNotDone.Attributes.Add("class", "");
                liSubmitted.Attributes.Add("class", "active");
                performerdocuments.Attributes.Remove("class");
                performerdocuments.Attributes.Add("class", "tab-pane");
                reviewerdocuments.Attributes.Remove("class");
                reviewerdocuments.Attributes.Add("class", "tab-pane active");
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void lbtnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("RiskControlMatrixUpdate");
                    DataTable ExcelData = null;
                    var AuditStepList = GetDataExportUpdate("P");
                    DataView view = new System.Data.DataView(AuditStepList.ToDataTable());
                    //DataView view = new System.Data.DataView((DataTable)Session["grdFilterRCMData"]);                    

                    ExcelData = view.ToTable("Selected", false, "BranchName", "VerticalName", "ProcessName",
                    "SubProcessName", "ActivityName", "ControlNo", "ActivityDescription", "ControlObjective",
                    "ControlDescription", "MControlDescription", "PersonResponsible", "PersonResponsibleEmail",
                    "ControlOwnerName", "ControlOwnerEmail", "ERPsystem", "FRC", "TestStrategy", "RiskRating", "AuditObjective",
                    "ActivityTobeDone", "Analyis_To_Be_Performed", "Pre_Requisite_List", "BranchId",
                    "VerticalId", "ProcessId", "SubProcessId", "RiskCreationId", "RiskActivityID", "ATBDId",
                    "AuditStepMasterID", "checklistdocid","RCMType");

                    exWorkSheet.Cells["A1"].LoadFromDataTable(ExcelData, true);

                    exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["A1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["A1"].Value = "Branch";
                    exWorkSheet.Cells["A1"].AutoFitColumns(20);

                    exWorkSheet.Cells["B1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["B1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["B1"].Value = "Vertical";
                    exWorkSheet.Cells["B1"].AutoFitColumns(20);

                    exWorkSheet.Cells["C1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["C1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["C1"].Value = "Process";
                    exWorkSheet.Cells["C1"].AutoFitColumns(30);

                    exWorkSheet.Cells["D1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["D1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["D1"].Value = "Sub Process";
                    exWorkSheet.Cells["D1"].AutoFitColumns(30);

                    exWorkSheet.Cells["E1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["E1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["E1"].Value = "Activity";
                    exWorkSheet.Cells["E1"].AutoFitColumns(30);

                    exWorkSheet.Cells["F1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["F1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["F1"].Value = "ControlNo";
                    exWorkSheet.Cells["F1"].AutoFitColumns(15);

                    exWorkSheet.Cells["G1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["G1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["G1"].Value = "Risk Description";
                    exWorkSheet.Cells["G1"].AutoFitColumns(50);

                    exWorkSheet.Cells["H1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["H1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["H1"].Value = "Control Objective";
                    exWorkSheet.Cells["H1"].AutoFitColumns(50);

                    exWorkSheet.Cells["I1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["I1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["I1"].Value = "Control Description";
                    exWorkSheet.Cells["I1"].AutoFitColumns(50);

                    exWorkSheet.Cells["J1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["J1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["J1"].Value = "Mitigating Control Description";
                    exWorkSheet.Cells["J1"].AutoFitColumns(30);

                    exWorkSheet.Cells["K1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["K1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["K1"].Value = "Person Responsible";
                    exWorkSheet.Cells["K1"].AutoFitColumns(20);

                    exWorkSheet.Cells["L1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["L1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["L1"].Value = "Email";
                    exWorkSheet.Cells["L1"].AutoFitColumns(30);

                    exWorkSheet.Cells["M1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["M1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["M1"].Value = "Control Owner";
                    exWorkSheet.Cells["M1"].AutoFitColumns(20);

                    exWorkSheet.Cells["N1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["N1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["N1"].Value = "Email";
                    exWorkSheet.Cells["N1"].AutoFitColumns(30);

                    exWorkSheet.Cells["O1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["O1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["O1"].Value = "Application system supporting the transaction";
                    exWorkSheet.Cells["O1"].AutoFitColumns(30);

                    exWorkSheet.Cells["P1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["P1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["P1"].Value = "Fraud risk indicator";
                    exWorkSheet.Cells["P1"].AutoFitColumns(10);

                    exWorkSheet.Cells["Q1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["Q1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["Q1"].Value = "Test Strategy";
                    exWorkSheet.Cells["Q1"].AutoFitColumns(30);

                    exWorkSheet.Cells["R1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["R1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["R1"].Value = "Risk Rating";
                    exWorkSheet.Cells["R1"].AutoFitColumns(10);

                    exWorkSheet.Cells["S1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["S1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["S1"].Value = "Audit Methodology";
                    exWorkSheet.Cells["S1"].AutoFitColumns(50);

                    exWorkSheet.Cells["T1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["T1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["T1"].Value = "Audit Steps";
                    exWorkSheet.Cells["T1"].AutoFitColumns(50);

                    exWorkSheet.Cells["U1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["U1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["U1"].Value = "Analyis To Be Performed";
                    exWorkSheet.Cells["U1"].AutoFitColumns(50);

                    exWorkSheet.Cells["V1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["V1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["V1"].Value = "Pre-Requisite List";
                    exWorkSheet.Cells["V1"].AutoFitColumns(50);

                    exWorkSheet.Cells["W1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["W1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["W1"].Value = "BID";
                    exWorkSheet.Cells["W1"].AutoFitColumns(10);

                    exWorkSheet.Cells["X1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["X1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["X1"].Value = "VID";
                    exWorkSheet.Cells["X1"].AutoFitColumns(5);

                    exWorkSheet.Cells["Y1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["Y1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["Y1"].Value = "PID";
                    exWorkSheet.Cells["Y1"].AutoFitColumns(5);

                    exWorkSheet.Cells["Z1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["Z1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["Z1"].Value = "SPID";
                    exWorkSheet.Cells["Z1"].AutoFitColumns(5);

                    exWorkSheet.Cells["AA1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["AA1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["AA1"].Value = "RiskID";
                    exWorkSheet.Cells["AA1"].AutoFitColumns(5);

                    exWorkSheet.Cells["AB1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["AB1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["AB1"].Value = "RATID";
                    exWorkSheet.Cells["AB1"].AutoFitColumns(5);

                    exWorkSheet.Cells["AC1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["AC1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["AC1"].Value = "ATBDId";
                    exWorkSheet.Cells["AC1"].AutoFitColumns(5);

                    exWorkSheet.Cells["AD1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["AD1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["AD1"].Value = "ASMID";
                    exWorkSheet.Cells["AD1"].AutoFitColumns(5);

                    exWorkSheet.Cells["AE1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["AE1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["AE1"].Value = "CDOID";
                    exWorkSheet.Cells["AE1"].AutoFitColumns(15);

                    exWorkSheet.Cells["AF1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["AF1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["AF1"].Value = "RCMType";
                    exWorkSheet.Cells["AF1"].AutoFitColumns(15);

                    using (ExcelRange col = exWorkSheet.Cells[1, 1, 1 + ExcelData.Rows.Count, 32])
                    {
                        col.Style.WrapText = true;
                        col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                        //Assign borders
                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    }

                    Byte[] fileBytes = exportPackge.GetAsByteArray();
                    Response.ClearContent();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=RiskControlMatrixExportForUpdate.xlsx");
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.ms-excel";
                    StringWriter sw = new StringWriter();
                    Response.BinaryWrite(fileBytes);
                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                throw;
            }
        }
    }
}