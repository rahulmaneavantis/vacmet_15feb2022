﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="UploadUserBranch.aspx.cs" 
    Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Account.UploadUserBranch" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .col-sm-6 {
            height: 64px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">

     <asp:UpdateProgress ID="updateProgress" runat="server" AssociatedUpdatePanelID="upUploadUtility">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.3;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 30%; left: 40%;" />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

     <asp:UpdatePanel ID="upUploadUtility" runat="server">
        <ContentTemplate>
              <asp:Panel ID="vdpanel" runat="server" ScrollBars="Auto">
                        <asp:ValidationSummary ID="vsUploadUtility" runat="server" 
                            class="alert alert-block alert-danger fade in" DisplayMode="BulletList" ValidationGroup="uploadUtilityValidationGroup" ForeColor="red"  />
                        <asp:CustomValidator ID="cvUploadUtilityPage" runat="server" EnableClientScript="False"
                            ValidationGroup="uploadUtilityValidationGroup" Display="None" Enabled="true" ShowSummary="true" />
                    </asp:Panel>

            <table align="right" >
                <tr>
                    <td>
                           <asp:LinkButton ID="lnkSampleFormat" runat="server" class="newlink" data-placement="bottom" data-toggle="tooltip" Font-Underline="True" OnClick="lnkSampleFormat_Click" Text="Sample Format(Excel)" ToolTip="Download Sample Excel Document Format for User/ Customer Branch Upload"></asp:LinkButton>
                            
                    </td>
                </tr>
            </table>
         

         

         <table align="center" style="width: 400px; height: 180px">
             <tr>
                 <td>
                   <label style="width: 110px; display: block; float: left; font-size: 13px; color:black; margin-bottom: -5px;">
                                Select Customer :</label>
                 </td>
                 <td>
                     <asp:DropDownList runat="server" ID="ddlCustomerList" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                    CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlCustomerList_SelectedIndexChanged" />
                 </td>
             </tr>
               
           
                 <tr>
                     <td>
                         <asp:RadioButton ID="rdoBtn_User" runat="server" AutoPostBack="True" GroupName="uploadContentGroup" OnCheckedChanged="rdoBtn_CheckedChanged" Style="display: block; font-size: 13px; color: #333;" Text="Users" />
                     </td>
                     <td>
                         <asp:RadioButton ID="rdoBtn_Branch" runat="server" AutoPostBack="True" GroupName="uploadContentGroup" OnCheckedChanged="rdoBtn_CheckedChanged" Style="display: block; font-size: 13px; color: #333;" Text="Branches" />
                     </td>
                 </tr>

            
            
           <tr>
               <td>Upload File:</td>
                    <td>
                        <asp:FileUpload ID="MasterFileUpload" runat="server" Style="display: block; font-size: 13px; color: #333;" />
                                <asp:RequiredFieldValidator ID="rfvFileUpload" ErrorMessage="Please Select File" ControlToValidate="MasterFileUpload"
                                    runat="server" Display="None" ValidationGroup="uploadUtilityValidationGroup" />
                    </td>
               </tr>
             <caption>
                 <br />
                 <tr>
                     <td>
                         <asp:Button ID="btnUploadFile" runat="server" OnClick="btnUploadFile_Click" Style="margin-left: 75px;" Text="Upload" ValidationGroup="oplValidationGroup" />
                     </td>
                     
                 </tr>
             </caption>
                                
          </table>
           
 </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnUploadFile" />
           <asp:PostBackTrigger ControlID="lnkSampleFormat" />
        </Triggers>
    </asp:UpdatePanel>
   
   

</asp:Content>
