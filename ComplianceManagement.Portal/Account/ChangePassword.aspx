﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ChangePassword.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Account.ChangePassword" %>

<%@ Register Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" TagPrefix="ajax" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Teamlease - Products that simplify" />
    <meta name="author" content="Teamlease - Development Team" />

    <title>Change Password :: Teamlease - Products that simplify</title>

    <!-- Bootstrap CSS -->
    <link href="../NewCSS/bootstrap.min.css" rel="stylesheet" />

    <link href="../NewCSS/bootstrap.min.css" rel="stylesheet" />
    <!-- bootstrap theme -->
    <link href="../NewCSS/bootstrap-theme.css" rel="stylesheet" />
    <!--external css-->
    <!-- font icon -->
    <link href="../NewCSS/elegant-icons-style.css" rel="stylesheet" />
    
    <!-- Custom styles -->
    <link href="../NewCSS/style.css" rel="stylesheet" />
    <link href="../NewCSS/style-responsive.css" rel="stylesheet" />


    <script src="../Newjs/jquery-1.12.4.min.js"></script>

    <script src="../Newjs/passwordStrength.js"></script>
      <script type="text/javascript" src="../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../Newjs/jquery-ui-1.9.2.custom.min.js"></script>


    <style>
  body { background-color:#333; font-family:'Roboto';}
  h1 { color:#fff;}
  .container { margin:150px auto; max-width:400px;}
.mt-10 { margin-top: 20px; }
.password-box { margin:20px auto
}
.password-progress {
  /*height: 12px;*/
  /*margin-top: 10px;*/
  overflow: hidden;
  background-color: #f5f5f5;
  border-radius: 4px;
  -webkit-box-shadow: inset 0 1px 2px rgba(0,0,0,.1);
  box-shadow: inset 0 1px 2px rgba(0,0,0,.1);
}

.progress-bar {
  float: left;
  height: 100%;
  background-color: #337ab7;
  -webkit-box-shadow: inset 0 -1px 0 rgba(0,0,0,.15);
  box-shadow: inset 0 -1px 0 rgba(0,0,0,.15);
  -webkit-transition: width .6s ease;
  -o-transition: width .6s ease;
  transition: width .6s ease;
}

.bg-red {
  background: #E74C3C;
  border: 1px solid #E74C3C;
}

.bg-orange {
  background: #F39C12;
  border: 1px solid #F39C12;
}

.bg-green {
  background: #1ABB9C;
  border: 1px solid #1ABB9C;
}
</style>

    <style type="text/css">
        .VeryPoorStrength {
            background: Red;
            color: White;
            font-weight: bold;
        }

        .WeakStrength {
            background: Gray;
            color: White;
            font-weight: bold;
        }

        .AverageStrength {
            background: orange;
            color: black;
            font-weight: bold;
        }

        .GoodStrength {
            background: blue;
            color: White;
            font-weight: bold;
        }

        .ExcellentStrength {
            background: Green;
            color: White;
            font-weight: bold;
        }

        .BarBorder {
            width: 400px;
            height: 1px;
        }

        .BarBorder1 {
            margin-left: 400px;
            margin-bottom: 1px;
        }
    </style>
     <script type="text/javascript">
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-92029518-1', 'auto');
        ga('send', 'pageview');

        function settracknew(e, t, n, r) {
            try {
                ga('send', 'event', e, t, n + "#" + r)
            } catch (t) { } return !0
        }

        function settracknewnonInteraction(e, t, n, r) {
            ga('send', 'event', e, t, n, 0, { 'nonInteraction': 1 })
        }
     
    </script>
</head>


<body>
    <form class="login-form" runat="server" name="login" id="Form1" autocomplete="off">
        <asp:ScriptManager ID="ScriptManager1" runat="server" />
        <div class="col-md-12 login-form-head">
            <p class="login-img">
                <img id="imgc" src="../Images/avantil-logo.png" />
            </p>
        </div>
        <div runat="server" id="divMasterQuestions">
            <asp:UpdatePanel ID="upChangePassword" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="login-wrap">
                        <h1>Change Password</h1>
                        <div class="clearfix" style="height: 10px"></div>
                        <div class="input-group" style="width: 100%">
                            <asp:TextBox runat="server" ID="txtOldPassword" class="form-control" TabIndex="2" placeholder="Old Password" TextMode="Password" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="Old Password can not be empty." ControlToValidate="txtOldPassword"
                                runat="server" Display="None" />
                        </div>
                        <div  class="input-group password-box" style="width: 100%">
                                 <asp:TextBox runat="server" ID="txtNewPassword" name="txtNewPassword" placeholder="New Password" class="form-control" TextMode="Password" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ErrorMessage="New Password can not be empty." ControlToValidate="txtNewPassword"
                                    runat="server" Display="None" />
                            <%--   <asp:RegularExpressionValidator runat="server" ID="RegularExpressionValidator3" Display = "None" ControlToValidate = "txtNewPassword"
                                   ErrorMessage="Password length must be between 7 to 10 characters" ValidationExpression="^[a-zA-Z0-9\s]{8,10}$" >
                            </asp:RegularExpressionValidator> --%>
                               <%--<asp:RegularExpressionValidator runat="server" ID="RegularExpressionValidator3" Display = "None" ControlToValidate = "txtNewPassword" ValidationExpression = "^.*(?=.{8,})(?=.*\d)(?=.*[a-zA-Z])(?=.*[0-1]).*$" ErrorMessage="Password must be minimum 8 alphanumeric character long">
                            </asp:RegularExpressionValidator> --%>
                           
                        </div>
                        <div class="input-group" style="width: 100%">
                            <asp:TextBox runat="server" ID="txtConfirmPassword" class="form-control" placeholder="Confirm Password" TextMode="Password" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Confirm Password can not be empty." ControlToValidate="txtConfirmPassword"
                                runat="server" Display="None" />
                            <asp:CompareValidator ID="CompareValidator1" runat="server"  ErrorMessage="New Passwords and Confirm Password do not match."
                                ControlToCompare="txtNewPassword" ControlToValidate="txtConfirmPassword" Display="None" />

                    
                        </div>
                        <div class="clearfix"></div>
                        <%--<div class="input-group" style="width: 100%">
                            <ajax:PasswordStrength ID="PasswordStrength1" runat="server" TargetControlID="txtConfirmPassword"
                                StrengthIndicatorType="BarIndicator" Enabled="true"
                                HelpStatusLabelID="lblhelp1" PreferredPasswordLength="8"
                                MinimumNumericCharacters="1" MinimumSymbolCharacters="1"
                                BarBorderCssClass="BarBorder"
                                TextStrengthDescriptionStyles="VeryPoorStrength;WeakStrength;AverageStrength;GoodStrength;ExcellentStrength"></ajax:PasswordStrength>
                        </div>--%>
                        <div class="clearfix"></div>
                        <div class="row">
                            <div class="col-md-6">
                                <asp:Button ID="btnSavePassword" CssClass="btn btn-primary btn-lg btn-block" runat="server" Text="Submit"
                                    CausesValidation="true" OnClick="btnSavePassword_Click" />
                            </div>
                            <div class="col-md-6">
                                <asp:Button ID="btnClear" CssClass="btn btn-primary btn-lg btn-block" runat="server" Text="Clear"
                                    CausesValidation="false" OnClick="btnClear_Click" />
                            </div>
                        </div>
                        <div class="clearfix" style="height: 15px"></div>
                        <p>
                           
                     <asp:LinkButton ID="lnklogin" CausesValidation="false" runat="server" Text="Click here to go back"  OnClientClick="back();"></asp:LinkButton>
                        </p>
                        <%-- <asp:Label ID="lblmsg" class="alert alert-block alert-danger fade in" runat="server"></asp:Label>--%>
                        <asp:ValidationSummary ID="validationSummary2" class="alert alert-block alert-danger fade in" runat="server" />
                        <asp:CustomValidator ID="CustomValidator1" runat="server" class="alert alert-block alert-danger fade in" Display="None" EnableClientScript="False"></asp:CustomValidator>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </form>
</body>

<script>
    $(document).ready(function () {
        // $.tester.addRules({
        //     uppercase: {
        //         rule: /[A-Z]+/,
        //         method: true
        //     }
        // });

        var obj = $('#password').passwordStrength(
            // {gradeClass: {
            //     bad: 'bg-blue',
            //     pass: 'bg-blue',
            //     good: 'bg-blue'
            // }}
        );
        var obj2 = $('#txtNewPassword').passwordStrength();

        $('#destroy').click(function () {
            obj.destroy();
        });
        $('#reset').click(function () {
            obj2.reset();
        });
     
    });
    function back() {
        window.history.back();
    };
    $(document).ready(function () {

        if ($('#cid').val() == '914') {
            $('#imgc').attr('src', '../Images/zomatologo.jpg');
        }
        else if ('<%=LogoName%>' != null && '<%=LogoName%>' != '') {
               
                  $('#imgc').attr('src', '<%=LogoName%>');
              }
              else {
                  $('#imgc').attr('src', '../Images/avantisLogo.png');

              }

          });
</script>
 
</html>
