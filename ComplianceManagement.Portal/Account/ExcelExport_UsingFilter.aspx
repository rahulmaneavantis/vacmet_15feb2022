﻿<%@ Page Title="Act List" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true"
    CodeBehind="ExcelExport_UsingFilter.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Account.ExcelExport_UsingFilter" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upActList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
           
            <table width="100%">
                <tr>
                    <td align="right" style="width: 20%">
                        <asp:DropDownList runat="server" ID="ddlfilterAct" Style="padding: 0px; margin: 0px; height: 22px; width: 220px;"
                            CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlfilterAct_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td align="right" style="width: 20%">
                        <asp:DropDownList runat="server" ID="ddlFilterIndustry" Style="padding: 0px; margin: 0px; height: 22px; width: 220px;"
                            CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlFilterIndustry_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td align="right" style="width: 20%">
                       
                         <asp:DropDownList runat="server" ID="ddlLegalEntityType" Style="padding: 0px; margin: 0px; height: 22px; width: 220px;"
                            CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlLegalEntityType_SelectedIndexChanged">
                        </asp:DropDownList>
                        
                    </td>
                    <td align="right" style="width: 20%">Filter :

                         <asp:TextBox runat="server" ID="tbxFilter" Width="250px" MaxLength="50" AutoPostBack="true"
                            OnTextChanged="tbxFilter_TextChanged" />
                    </td>
                     <td align="right" style="width: 20%">
                         <asp:Button ID="btnExportExcel" runat="server" Text="Export" OnClick="btnExportExcel_Click" />
                         </td>

                </tr>
            </table>
            <div id="divComp" runat="server">


                <asp:GridView runat="server" ID="grdAct" AutoGenerateColumns="false" 
                     AllowSorting="true"
                    CellPadding="5"  AllowPaging="True" PageSize="13" Width="100%"
                    OnPageIndexChanging="grdAct_PageIndexChanging">
                    <Columns>
                        <asp:BoundField HeaderText="Act Name" DataField="ActName" />
                        <asp:BoundField HeaderText="Section (S) / Rule (S)" DataField="Sections" />
                        <asp:BoundField HeaderText="Compliance Type" DataField="ComplianceType" />
                        <asp:BoundField HeaderText="Timely based type" DataField="SubComplianceType" />
                        <asp:BoundField HeaderText="Short Description" DataField="ShortDescription" />
                        <asp:BoundField HeaderText="Detailed Description" DataField="Description" />
                        <asp:BoundField HeaderText="Nature Of Compliance" DataField="NatureOfCompliance" />
                        <asp:BoundField HeaderText="Upload Document" DataField="UploadDocument" />
                        <asp:BoundField HeaderText="Required Forms" DataField="RequiredForms" />
                        <asp:BoundField HeaderText="Sample Form" DataField="sampleform" />
                        <asp:BoundField HeaderText="Event based" DataField="iseventbased" />
                        <asp:BoundField HeaderText="Event name" DataField="EventName" />
                        <asp:BoundField HeaderText="Event compliance type" DataField="EventComplianceType" />
                        <asp:BoundField HeaderText="Sub Event name" DataField="SubEventName" />
                        <asp:BoundField HeaderText="Compliance due (in days)" DataField="DueDate" />
                        <asp:BoundField HeaderText="Frequency" DataField="Frequency" />
                        <asp:BoundField HeaderText="DueDate" DataField="DueDate" />
                        <asp:BoundField HeaderText="Special Due Date - For Month" DataField="spdate" />
                        <asp:BoundField HeaderText="Special Due Date - Date" DataField="spdate" />
                        <asp:BoundField HeaderText="Reminder type" DataField="ReminderType" />
                        <asp:BoundField HeaderText="Before (in days)" DataField="ReminderBefore" />
                        <asp:BoundField HeaderText="Gap (in days)" DataField="ReminderGap" />
                        <asp:BoundField HeaderText="Risk Type" DataField="RiskType" />
                        <asp:BoundField HeaderText="Non Compliance Type" DataField="NonComplianceType" />
                        <asp:BoundField HeaderText="Fixed Minimum" DataField="FixedMinimum" />
                        <asp:BoundField HeaderText="Fixed Maximum" DataField="FixedMaximum" />
                        <asp:BoundField HeaderText="Day/Month" DataField="VariableAmountPerMonth" />
                        <asp:BoundField HeaderText="Variable Amount Rs." DataField="VariableAmountPerDay" />
                        <asp:BoundField HeaderText="Variable Amount (Max)" DataField="VariableAmountPerDayMax" />
                        <asp:BoundField HeaderText="Variable Amount (%)" DataField="VariableAmountPercent" />
                        <asp:BoundField HeaderText="Variable Amount (% Max)" DataField="VariableAmountPercentMax" />
                        <asp:BoundField HeaderText="Imprisonment" DataField="Imprisonment" />
                        <asp:BoundField HeaderText="Designation" DataField="Designation" />
                        <asp:BoundField HeaderText="Minimum Years" DataField="MinimumYears" />
                        <asp:BoundField HeaderText="Maximum Years" DataField="MaximumYears" />
                        <asp:BoundField HeaderText="Others" DataField="Others" />
                        <asp:BoundField HeaderText="CalFlag" DataField="calflag" />                                            
                    </Columns>
                   
                    <HeaderStyle BackColor="#df5015" Font-Bold="true" ForeColor="White" />
                    <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                    <EmptyDataTemplate>
                        No Records Found.
                    </EmptyDataTemplate>
                </asp:GridView>

            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnExportExcel" />
        </Triggers>
    </asp:UpdatePanel>
    <div id="divActDialog">
        <asp:UpdatePanel ID="upAct" runat="server" UpdateMode="Conditional" >
            <ContentTemplate>
                <div style="margin: 5px">
                    <div style="margin-bottom: 4px">
                        <asp:ValidationSummary runat="server" CssClass="vdsummary"
                            ValidationGroup="ActValidationGroup" />
                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                            ValidationGroup="ActValidationGroup" Display="None" />
                    </div>

                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
