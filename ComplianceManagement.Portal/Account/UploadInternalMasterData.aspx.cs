﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using OfficeOpenXml;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Data;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Account
{
    public partial class UploadInternalMasterData : System.Web.UI.Page
    {
        DataTable dtCompliance = new DataTable();
        bool suucess = false;
        bool sucessvalidation = false;
        bool suucessSave = false;
        bool CheckListsuucess = false;
        bool CheckListsuucessSave = false;
        static int? customerID;
        public static int UserID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                UserID = AuthenticationHelper.UserID;
                if (AuthenticationHelper.Role == "IMPT")
                {
                    BindCustomers(UserID);                    
                    customerdiv.Visible = true;
                    checklistcustomer.Visible = true;
                }
                else
                {
                    customerdiv.Visible = false;
                    checklistcustomer.Visible = false;
                }
                    lblMessage.Text = string.Empty;
                    LblErormessage.Text = string.Empty; 
                    Tab1_Click(sender, e);  
            }
        }

        private void BindCustomers(int UserID)
        {
            try
            {

                var data= Assigncustomer.GetAllCustomer(UserID);
                ddlCustomer.DataTextField = "Name";
                ddlCustomer.DataValueField = "ID";

                ddlCustomer.DataSource = data;
                ddlCustomer.DataBind();
                ddlCustomer.Items.Insert(0, new ListItem("Select Customer", "-1"));


                ddlchecklistCustomer.DataTextField = "Name";
                ddlchecklistCustomer.DataValueField = "ID";

                ddlchecklistCustomer.DataSource = data;
                ddlchecklistCustomer.DataBind();

                ddlchecklistCustomer.Items.Insert(0, new ListItem("Select Customer", "-1"));

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


      

        #region Add Compliance
        protected void btnUploadFile_Click(object sender, EventArgs e)
        {
            if (MasterFileUpload.HasFile)
            {
                try
                {
                    string filename = Path.GetFileName(MasterFileUpload.FileName);
                    MasterFileUpload.SaveAs(Server.MapPath("~/Uploaded/") + filename.Trim());

                    FileInfo excelfile = new FileInfo(Server.MapPath("~/Uploaded/") + filename.Trim());
                    if (excelfile != null)
                    {
                        using (ExcelPackage xlWorkbook = new ExcelPackage(excelfile))
                        {
                            bool flag = InternalComplianceSheetsExitsts(xlWorkbook, "InternalCompliances");
                            if (flag == true)
                            {
                                ValidateComplianceDataNew(xlWorkbook);
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Please correct the sheet name.";                                
                            }
                            if (suucess == true)
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Excelsheet data verified successfully.";
                                btnSave.Enabled = true;
                            }
                        }
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Error uploading file. Please try again.";

                    }


                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";

                }
            }
        }


        private void ValidateComplianceDataNew(ExcelPackage xlWorkbook)
        {
            try
            {
                long cid = -1;
                if (AuthenticationHelper.Role.Contains("IMPT"))
                {
                    if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                        cid = Convert.ToInt64(ddlCustomer.SelectedValue);
                }
                else if (AuthenticationHelper.Role.Contains("CADMN"))
                {
                    cid = Convert.ToInt64(AuthenticationHelper.CustomerID);
                }
                else
                {
                    cid = Convert.ToInt64(AuthenticationHelper.CustomerID);
                }

                if (cid != -1)
                {
                    string ComplianceTypevalue = string.Empty;
                    string CategoryTypevalue = string.Empty;
                    List<string> errorMessage = new List<string>();
                    ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["InternalCompliances"];
                    if (xlWorksheet != null)
                    {
                        int xlrow2 = xlWorksheet.Dimension.End.Row;
                        #region Validation                           
                        for (int i = 2; i <= xlrow2; i++)
                        {
                            #region 1 Compliance Type Name
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 1].Text.ToString().Trim()))
                            {
                                try
                                {
                                    ComplianceTypevalue = Convert.ToString(xlWorksheet.Cells[i, 1].Text);

                                    int data = Convert.ToInt32(ComplianceTypevalue.Length);
                                    if (data >= 100)
                                    {
                                        errorMessage.Add("Please Check Compliance Type Name At -" + i + "Max length of Compliance Type Name is 100 characters");
                                    }
                                }
                                catch (Exception ex)
                                {
                                    errorMessage.Add(" Exception At  - " + i + " Compliance Type Name should not blank");
                                }
                            }
                            else
                            {
                                errorMessage.Add(" Record No - At  - " + i + " should not blank");
                            }
                            #endregion

                            #region 2  Compliance Category Name
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 2].Text.ToString().Trim()))
                            {
                                try
                                {
                                    CategoryTypevalue = Convert.ToString(xlWorksheet.Cells[i, 2].Text);

                                    int data = Convert.ToInt32(CategoryTypevalue.Length);
                                    if (data >= 100)
                                    {
                                        errorMessage.Add("Please Check Compliance Category Name At -" + i + "Max length of Compliance Category Name is 100 characters");
                                    }
                                }
                                catch (Exception ex)
                                {
                                    errorMessage.Add(" Exception At  - " + i + " Compliance Category Name should not blank");
                                }
                            }
                            else
                            {
                                errorMessage.Add(" Record No - At  - " + i + " should not blank");
                            }
                            #endregion

                            #region 3 Compliance Type

                            if ((Convert.ToString(xlWorksheet.Cells[i, 3].Value) == "Functionbased"))
                            {


                            }
                            else if (String.IsNullOrEmpty(xlWorksheet.Cells[i, 3].Text.ToString().Trim()))
                            {

                            }
                            else
                            {
                                errorMessage.Add("Record No -" + i + "Please enter correct Compliance Type or should not blank");
                            }
                          
                            #endregion

                            #region 4 Timely based type
                            if (Convert.ToString(xlWorksheet.Cells[i, 3].Value).Trim().Equals("Timebased"))
                            {


                            }
                            #endregion

                            #region 5 Short Description
                            if (String.IsNullOrEmpty(xlWorksheet.Cells[i, 5].Text.ToString().Trim()))
                            {
                                errorMessage.Add("Record No -" + i + "  Compliance ShortDescription should not blank");
                            }
                            #endregion

                            #region 6 Upload Document
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 6].Text.ToString().Trim()))
                            {
                                List<string> isud = new List<string>();
                                isud.Add("YES");
                                isud.Add("NO");
                                var isupdoc = Convert.ToString(xlWorksheet.Cells[i, 6].Value).Trim().ToUpper();

                                if (!isud.Contains(isupdoc))
                                {
                                    errorMessage.Add("Record No -" + i + " Upload Document should be (Yes, No)");
                                }

                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 3].Text.ToString().Trim()))
                                {
                                    if ((Convert.ToString(xlWorksheet.Cells[i, 3].Value) == "Functionbased"))
                                    {
                                        if (isupdoc == "NO")
                                        {
                                            errorMessage.Add("Record No -" + i + " Upload Document should be Yes because Compliance Type is Functionbased");
                                        }
                                    }
                                }
                            }
                            else
                            {
                                errorMessage.Add("Record No -" + i + " Upload Document should not blank. Instead Use (Yes , No)");
                            }

                            #endregion

                            #region 7 Required Forms   


                            #endregion

                            #region 8 9 Compliance Occourrence /Frequency
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 8].Text.ToString().Trim()))
                            {
                                if ((Convert.ToString(xlWorksheet.Cells[i, 8].Value) == "Recurring") && Convert.ToString(xlWorksheet.Cells[i, 3].Value).Trim().Equals("Functionbased"))
                                {
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 9].Text.ToString().Trim()))
                                    {
                                        List<string> fr = new List<string>();
                                        fr.Add("ANNUAL");
                                        fr.Add("HALFYEARLY");
                                        fr.Add("QUARTERLY");
                                        fr.Add("FOURMONTHLY");
                                        fr.Add("TWOYEARLY");
                                        fr.Add("SEVENYEARLY");
                                        fr.Add("MONTHLY");
                                        fr.Add("DAILY");
                                        fr.Add("WEEKLY");
                                        var freq = Convert.ToString(xlWorksheet.Cells[i, 9].Value).Trim().ToUpper();
                                        if (!fr.Contains(freq))
                                        {
                                            errorMessage.Add("Record No -" + i + "  Please enter correct Frequency");
                                        }
                                    }
                                    else
                                    {
                                        errorMessage.Add("Record No -" + i + " Frequency should not blank.");
                                    }
                                }
                                else if ((Convert.ToString(xlWorksheet.Cells[i, 8].Value) == "One Time"))
                                {
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 9].Text.ToString().Trim()))
                                    {
                                        errorMessage.Add("Record No -" + i + "  Please Remove Frequency or check Compliance Occourrence type ");
                                    }
                                }
                                else
                                {
                                    errorMessage.Add("Record No -" + i + " Please Correct Compliance Occourrence As Per Sample Format.");
                                }
                            }
                            else
                            {
                                errorMessage.Add("Record No -" + i + " Compliance Occourrence should not blank.");
                            }
                            #endregion

                            #region 10 Due Date
                            try
                            {
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 10].Text.ToString().Trim()))
                                {
                                    bool checkNumber = IsValidNumber(Convert.ToString(xlWorksheet.Cells[i, 10].Value));
                                    if (!checkNumber)
                                    {
                                        errorMessage.Add("Record No -" + i + "  Please Enter only numbers in Due Date");
                                    }
                                    else
                                    {
                                        var val = Convert.ToInt32(xlWorksheet.Cells[i, 10].Value);

                                        if (val > 31)
                                        {
                                            errorMessage.Add("Record No -" + i + "  Please correct Due Date");
                                        }
                                    }
                                }
                                else
                                {

                                    if ((Convert.ToString(xlWorksheet.Cells[i, 3].Value) == "Functionbased"))
                                    {
                                        var freq = Convert.ToString(xlWorksheet.Cells[i, 9].Value).Trim().ToUpper();
                                        if (freq != "DAILY" && freq != "WEEKLY")
                                        {
                                            errorMessage.Add("Record No -" + i + " Due Date should not blank.");
                                        }
                                    }

                                }

                                if (Convert.ToString(xlWorksheet.Cells[i, 3].Value).Trim().Equals("Timebased"))
                                {
                                    if (Convert.ToString(xlWorksheet.Cells[i, 4].Value).Trim().Equals("FixedGap"))
                                    {
                                        bool checkNumber = IsValidNumber(Convert.ToString(xlWorksheet.Cells[i, 10].Value));
                                        if (!checkNumber)
                                        {
                                            errorMessage.Add("Record No -" + i + "  Please Enter only numbers in Due Date");
                                        }
                                        else
                                        {
                                            var val = Convert.ToInt32(xlWorksheet.Cells[i, 10].Value);

                                            if (val > 31)
                                            {
                                                errorMessage.Add("Record No -" + i + "  Please correct Due Date");
                                            }
                                        }
                                    }
                                }

                            }
                            catch (Exception ex)
                            {

                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Due Date .";
                                LoggerMessage.InsertLog(ex, "Due Date ", MethodBase.GetCurrentMethod().Name);
                            }
                          
                            #endregion

                            #region 11 12  Special Due Date - For Month / Special Due Date -Date                               
                            #endregion

                            #region 13  Reminder type                              
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 13].Text.ToString().Trim()))
                            {
                                List<string> isud = new List<string>();
                                isud.Add("Custom");
                                isud.Add("Standard");
                                var isupdoc = Convert.ToString(xlWorksheet.Cells[i, 13].Value).Trim();

                                if (!isud.Contains(isupdoc))
                                {
                                    errorMessage.Add("Record No -" + i + " Reminder Type should be (Custom, Standard)");
                                }
                            }
                            else
                            {
                                errorMessage.Add("Record No -" + i + "  Reminder Type should not blank it should be (Standard,Custom)");
                            }
                            #endregion

                            #region 14 Before (in days)
                            if (Convert.ToString(xlWorksheet.Cells[i, 13].Value).Trim().Equals("Custom"))
                            {
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 14].Text.ToString().Trim()))
                                {
                                    bool checkNumber = IsValidNumber(Convert.ToString(xlWorksheet.Cells[i, 14].Value));
                                    if (!checkNumber)
                                    {
                                        errorMessage.Add("Record No -" + i + " Please Enter only numbers in Before (in days)");
                                    }
                                }
                                else
                                {
                                    errorMessage.Add("Record No -" + i + "  Before (in days) can not blank");
                                }
                            }
                            #endregion

                            #region 15 Gap (in days)
                            if (Convert.ToString(xlWorksheet.Cells[i, 13].Value).Trim().Equals("Custom"))
                            {
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 15].Text.ToString().Trim()))
                                {
                                    bool checkNumber = IsValidNumber(Convert.ToString(xlWorksheet.Cells[i, 15].Value));
                                    if (!checkNumber)
                                    {
                                        errorMessage.Add("Record No -" + i + " Please Enter only numbers in Gap (in days)");
                                    }
                                }
                                else
                                {
                                    errorMessage.Add("Record No -" + i + "  Gap (in days) should not blank");
                                }
                            }
                            #endregion

                            #region 16  Risk Type
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 16].Text.ToString().Trim()))
                            {
                                List<string> rt = new List<string>();
                                rt.Add("HIGH");
                                rt.Add("MEDIUM");
                                rt.Add("LOW");
                                rt.Add("CRITICAL");
                                var risk = Convert.ToString(xlWorksheet.Cells[i, 16].Value).Trim().ToUpper();
                                if (!rt.Contains(risk))
                                {
                                    errorMessage.Add("Record No -" + i + "  Risk Type should be (High, Low, Medium,Critical)");
                                }
                            }
                            else
                            {
                                errorMessage.Add("Record No -" + i + " Risk Type should not blank");
                            }
                            #endregion

                            #region 17 OneTime Date

                            if (Convert.ToString(xlWorksheet.Cells[i, 8].Value).Trim().Equals("One Time"))
                            {
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 17].Text.ToString().Trim()))
                                {
                                    try
                                    {
                                        bool check = CheckDate(Convert.ToString(xlWorksheet.Cells[i, 17].Text).Trim());
                                        if (!check)
                                        {
                                            errorMessage.Add("Record No -" + i + "  Please Check OneTime Date or Date should be in DD-MMM-YYYY Format");
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                    }
                                }
                                else
                                {
                                    errorMessage.Add("Record No -" + i + "   OneTime Date should not blank");
                                }                                
                            }
                            #endregion

                            #region 18 CalFlag
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 18].Text.ToString().Trim()))
                            {
                                List<string> cf = new List<string>();
                                cf.Add("Y");
                                cf.Add("N");
                                var stype = Convert.ToString(xlWorksheet.Cells[i, 18].Value).Trim().ToUpper();
                                if (!cf.Contains(stype))
                                {
                                    errorMessage.Add("Record No -" + i + " calflag should be (Y, N)");
                                }
                            }
                            else
                            {
                                errorMessage.Add("Record No -" + i + " calflag should not blank");
                            }
                            #endregion

                            #region 19 IsDocumentRequired
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 19].Text.ToString().Trim()))
                            {
                                List<string> isdr = new List<string>();
                                isdr.Add("YES");
                                isdr.Add("NO");
                                var isdocreq = Convert.ToString(xlWorksheet.Cells[i, 19].Value).Trim().ToUpper();
                                if (!isdr.Contains(isdocreq))
                                {
                                    errorMessage.Add("Record No -" + i + " IsDocumentRequired should be (Yes, No)");
                                }
                            }
                            else
                            {
                                errorMessage.Add("Record No -" + i + " IsDocumentRequired should not blank('Yes'/'No')");
                            }
                            #endregion

                            #region 20 IDetailed Description
                            if (String.IsNullOrEmpty(xlWorksheet.Cells[i, 20].Text.ToString().Trim()))
                            {
                                errorMessage.Add("Record No -" + i + "  Detailed Description should not blank");
                            }
                            #endregion

                            #region 21 Short Form
                            if (String.IsNullOrEmpty(xlWorksheet.Cells[i, 21].Text.ToString().Trim()))
                            {
                                errorMessage.Add("Record No -" + i + " Short Form should not blank");
                            }
                            #endregion

                            #region 22 Schedule Type
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 3].Text.ToString().Trim()))
                            {
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 22].Text.ToString().Trim()))
                                {
                                    List<string> st = new List<string>();
                                    st.Add("AFTER");
                                    st.Add("IN BETWEEN");
                                    st.Add("BEFORE");
                                    var stype = Convert.ToString(xlWorksheet.Cells[i, 22].Value).Trim().ToUpper();
                                    if (!st.Contains(stype))
                                    {
                                        errorMessage.Add("Record No -" + i + "  Schedule Type should be (After, In Between, Before)");
                                    }
                                }
                                else
                                {
                                    errorMessage.Add("Record No -" + i + " Schedule Type should not blank");
                                }
                            }
                            #endregion

                            #region 23 Is EventBased
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 3].Text.ToString().Trim()))
                            {
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 23].Text.ToString().Trim()))
                                {
                                    List<string> SCT = new List<string>();
                                    SCT.Add("YES");
                                    SCT.Add("NO");
                                    var stype = Convert.ToString(xlWorksheet.Cells[i, 23].Value).Trim().ToUpper();
                                    if (!SCT.Contains(stype))
                                    {
                                        errorMessage.Add("Record No -" + i + " Is EventBased should be (YES, NO)");
                                    }
                                }
                                else
                                {
                                    errorMessage.Add("Record No -" + i + " Is EventBased should not blank");
                                }
                            }
                            #endregion

                            #region 24 Week Due Day For Weekly Frequency
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 3].Text.ToString().Trim()))
                            {

                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 9].Text.ToString().Trim()))
                                { 
                                    var freq = Convert.ToString(xlWorksheet.Cells[i, 9].Value).Trim().ToUpper();
                                    if (freq == "WEEKLY")
                                    {
                                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 24].Text.ToString().Trim()))
                                        {
                                            var weekDay = Convert.ToString(xlWorksheet.Cells[i, 24].Value).Trim().ToUpper();                                          
                                            bool IsValidWeekDay = false;
                                            switch (weekDay)
                                            {
                                                case "SUNDAY":
                                                    IsValidWeekDay = true;
                                                    break;
                                                case "MONDAY":
                                                    IsValidWeekDay = true;
                                                    break;
                                                case "TUESDAY":
                                                    IsValidWeekDay = true;
                                                    break;
                                                case "WEDNESDAY":
                                                    IsValidWeekDay = true;
                                                    break;
                                                case "THURSDAY":
                                                    IsValidWeekDay = true;
                                                    break;
                                                case "FRIDAY":
                                                    IsValidWeekDay = true;
                                                    break;
                                                case "SATURDAY":
                                                    IsValidWeekDay = true;
                                                    break;
                                            }
                                            if (IsValidWeekDay == false)
                                            {
                                                errorMessage.Add("Record No -" + i + "  Please enter correct Due Week Day");
                                            }
                                        }
                                    }
                                }                                 
                            }
                            #endregion
                        }

                        #endregion

                        if (errorMessage.Count > 0)
                        {
                            showErrorMessages(errorMessage, cvDuplicateEntry);
                            sucessvalidation = false;
                        }
                        else
                        {
                            try
                            {
                                createDataTable();

                                #region Grid Display Code                                
                                for (int k = 2; k <= xlrow2; k++)
                                {

                                    DataRow drCompliance = dtCompliance.NewRow();
                                    com.VirtuosoITech.ComplianceManagement.Business.Data.InternalCompliance complianceData = new com.VirtuosoITech.ComplianceManagement.Business.Data.InternalCompliance();

                                    complianceData.CustomerID = customerID;
                                    drCompliance["CustomerID"] = customerID;

                                    drCompliance["IComplianceTypeID"] = Convert.ToString(xlWorksheet.Cells[k, 1].Value);
                                    drCompliance["IComplianceCategoryID"] = Convert.ToString(xlWorksheet.Cells[k, 2].Value);

                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[k, 8].Text.ToString().Trim()))
                                    {
                                        if ((xlWorksheet.Cells[k, 8].Value.ToString()) == "Recurring")
                                        {
                                            drCompliance["IComplianceType"] = Convert.ToString(xlWorksheet.Cells[k, 3].Value);
                                        }
                                    }
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[k, 3].Text.ToString().Trim()))
                                    {
                                        if (Convert.ToString(xlWorksheet.Cells[k, 3].Value).Trim().Equals("Timebased"))
                                        {
                                            drCompliance["ISubComplianceType"] = Convert.ToString(xlWorksheet.Cells[k, 4].Value);
                                        }
                                    }
                                    drCompliance["IShortDescription"] = Convert.ToString(xlWorksheet.Cells[k, 5].Value);
                                    complianceData.IShortDescription = Convert.ToString(xlWorksheet.Cells[k, 5].Value);

                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[k, 6].Text.ToString().Trim()))
                                    {
                                        if (Convert.ToString(xlWorksheet.Cells[k, 6].Value).Trim().Equals("Yes") || Convert.ToString(xlWorksheet.Cells[k, 6].Value).Trim().Equals("No"))
                                        {
                                            drCompliance["IUploadDocument"] = Convert.ToString(xlWorksheet.Cells[k, 6].Value);
                                        }
                                    }
                                    drCompliance["IShortForm"] = Convert.ToString(xlWorksheet.Cells[k, 21].Value);
                                    complianceData.IShortForm = Convert.ToString(xlWorksheet.Cells[k, 21].Value);

                                    if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[k, 7].Value)))
                                    {
                                        drCompliance["IRequiredFrom"] = Convert.ToString(xlWorksheet.Cells[k, 7].Value);
                                    }
                                    
                                    drCompliance["IComplianceOccurrence"] = Convert.ToString(xlWorksheet.Cells[k, 8].Value);

                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[k, 3].Text.ToString().Trim()))
                                    {
                                        if (Convert.ToString(xlWorksheet.Cells[k, 3].Value).Trim().Equals("Functionbased"))
                                        {
                                            drCompliance["IFrequency"] = Convert.ToString(xlWorksheet.Cells[k, 9].Value);
                                            drCompliance["IDueDate"] = Convert.ToString(xlWorksheet.Cells[k, 10].Value);
                                        }
                                    }


                                    drCompliance["specilMonth"] = Convert.ToString(xlWorksheet.Cells[k, 11].Value);
                                    drCompliance["specialDay"] = Convert.ToString(xlWorksheet.Cells[k, 12].Value);
                                    drCompliance["IReminderType"] = Convert.ToString(xlWorksheet.Cells[k, 13].Value);
                                    drCompliance["IReminderBefore"] = Convert.ToString(xlWorksheet.Cells[k, 14].Value);
                                    drCompliance["IReminderGap"] = Convert.ToString(xlWorksheet.Cells[k, 15].Value);
                                    drCompliance["IRiskType"] = Convert.ToString(xlWorksheet.Cells[k, 16].Value);

                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[k, 17].Text.ToString().Trim()))
                                    {
                                        drCompliance["IOneTimeDate"] = Convert.ToString(xlWorksheet.Cells[k, 17].Text);
                                    }

                                    drCompliance["calflag"] = Convert.ToString(xlWorksheet.Cells[k, 18].Value);
                                    drCompliance["IsDocumentRequired"] = Convert.ToString(xlWorksheet.Cells[k, 19].Value);
                                    drCompliance["IDetailedDescription"] = Convert.ToString(xlWorksheet.Cells[k, 20].Value);
                                    drCompliance["IShortForm"] = Convert.ToString(xlWorksheet.Cells[k, 21].Value);
                                    drCompliance["ScheduleType"] = Convert.ToString(xlWorksheet.Cells[k, 22].Value);
                                    drCompliance["IsEventBased"] = Convert.ToString(xlWorksheet.Cells[k, 23].Value);
                                    drCompliance["DueWeekDay"] = Convert.ToString(xlWorksheet.Cells[k, 24].Value);
                                    dtCompliance.Rows.Add(drCompliance);

                                    grdCompliance.DataSource = dtCompliance;
                                    grdCompliance.DataBind();
                                }
                                #endregion

                                sucessvalidation = true;
                                suucess = true;
                            }
                            catch (Exception ex)
                            {
                                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                sucessvalidation = false;
                                suucess = false;
                            }                           
                        }                        
                    }
                }
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Please select customer.";
                }               
            }
            catch (Exception ex)
            {

                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void showErrorMessages(List<string> lstErrMsgs, CustomValidator cvtoShowErrorMsg)
        {
            //string finalErrMsg = string.Join("<br/>", emsg.ToArray());

            string finalErrMsg = string.Empty;

            finalErrMsg += "<ol type='1'>";

            if (lstErrMsgs.Count > 0)
            {
                lstErrMsgs.ForEach(eachErrMsg =>
                {
                    finalErrMsg += "<li>" + eachErrMsg + "</li>";
                });

                finalErrMsg += "</ol>";
            }
            cvtoShowErrorMsg.IsValid = false;
            cvtoShowErrorMsg.ErrorMessage = finalErrMsg;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "scrollUpPageScript", "scrollUpPage();", true);
        }
        private void ValidateComplianceData(ExcelPackage xlWorkbook)
        {
            try
            {
                ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["InternalCompliances"];
                int xlrow = xlWorksheet.Dimension.End.Row;
                int filledrow = 0;
                for (int i = 1; i <= xlrow; i++)
                {
                    if ((xlWorksheet.Cells[i, 1].Value) != null)
                    {
                        filledrow = filledrow + 1;
                    }
                }
                createDataTable();
                for (int i = 2; i <= filledrow; i++)
                {
                    if (((xlWorksheet.Cells[i, 5].Value) == null) && (Convert.ToString(xlWorksheet.Cells[i, 5].Value) == string.Empty))
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + "  <br />  Compliance ShortDescription can not blank";
                        return;
                    }

                    DataRow drCompliance = dtCompliance.NewRow();
                    com.VirtuosoITech.ComplianceManagement.Business.Data.InternalCompliance complianceData = new com.VirtuosoITech.ComplianceManagement.Business.Data.InternalCompliance();
                    if (((xlWorksheet.Cells[i, 1].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 1].Value) != string.Empty))
                    {


                        string ComplianceTypevalue = Convert.ToString(xlWorksheet.Cells[i, 1].Value);
                        int data = Convert.ToInt32(ComplianceTypevalue.Length);
                        if (data >= 100)
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 5].Value) + " <br />  Max length of Compliance Type is 100 characters";
                            return;
                        }
                        else
                        {
                            drCompliance["IComplianceTypeID"] = Convert.ToString(xlWorksheet.Cells[i, 1].Value);
                        }

                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 5].Value) + " <br />  Compliance type can not blank";
                        return;
                    }

                    if (((xlWorksheet.Cells[i, 2].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 2].Value) != string.Empty))
                    {
                        string CategoryTypevalue = Convert.ToString(xlWorksheet.Cells[i, 2].Value);
                        int data = Convert.ToInt32(CategoryTypevalue.Length);
                        if (data >= 100)
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 5].Value) + " <br /> Max length of category type is 100 characters";
                            return;
                        }
                        else
                        {
                            drCompliance["IComplianceCategoryID"] = Convert.ToString(xlWorksheet.Cells[i, 2].Value);
                        }
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 5].Value) + " <br />  Compliance Category can not blank";
                        return;
                    }

                    if ((Convert.ToString(xlWorksheet.Cells[i, 3].Value) == "Functionbased") || (Convert.ToString(xlWorksheet.Cells[i, 3].Value) != string.Empty))
                    {

                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 5].Value) + " <br />  Please enter correct Compliance Type";
                        return;
                    }

                    if ((Convert.ToString(xlWorksheet.Cells[i, 8].Value) == "Recurring") || (Convert.ToString(xlWorksheet.Cells[i, 8].Value) == "One Time"))
                    {
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 5].Value) + " <br />  Please enter correct Compliance Occourrence";
                        return;
                    }

                    if ((Convert.ToString(xlWorksheet.Cells[i, 9].Value) == string.Empty) || (Convert.ToString(xlWorksheet.Cells[i, 9].Value) == "Annual") || (Convert.ToString(xlWorksheet.Cells[i, 9].Value) == "HalfYearly")
                      || (Convert.ToString(xlWorksheet.Cells[i, 9].Value) == "Quarterly") || (Convert.ToString(xlWorksheet.Cells[i, 9].Value) == "FourMonthly") || (Convert.ToString(xlWorksheet.Cells[i, 9].Value) == "TwoYearly")
                      || (Convert.ToString(xlWorksheet.Cells[i, 9].Value) == "SevenYearly") || (Convert.ToString(xlWorksheet.Cells[i, 9].Value) == "Monthly"))
                    {
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 5].Value) + " <br />  Please enter correct Frequency";
                        return;
                    }

                    if ((Convert.ToString(xlWorksheet.Cells[i, 13].Value) == "Standard") || (Convert.ToString(xlWorksheet.Cells[i, 13].Value) == "Custom"))
                    {
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 5].Value) + " <br />  Please enter correct Reminder type";
                        return;
                    }

                    if ((Convert.ToString(xlWorksheet.Cells[i, 16].Value) == "High") || (Convert.ToString(xlWorksheet.Cells[i, 16].Value) == "Medium") || (Convert.ToString(xlWorksheet.Cells[i, 16].Value) == "Low") || (Convert.ToString(xlWorksheet.Cells[i, 16].Value) == "Critical") || (Convert.ToString(xlWorksheet.Cells[i, 16].Value) != string.Empty)) 
                    {
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 5].Value) + " <br />  Please enter correct Risk Type";
                        return;
                    }

                    complianceData.CustomerID = customerID;// UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID;//  Convert.ToInt32(ddlCustomer.SelectedValue);
                    drCompliance["CustomerID"] = customerID;//  Convert.ToInt32(ddlCustomer.SelectedValue);
                                                            //     drCompliance["CustomerID"] = Convert.ToInt32(ddlCustomer.SelectedValue);

                    if ((xlWorksheet.Cells[i, 8].Value.ToString()) == "Recurring")
                    {
                        if (((xlWorksheet.Cells[i, 3].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 3].Value) != string.Empty))
                        {
                            drCompliance["IComplianceType"] = Convert.ToString(xlWorksheet.Cells[i, 3].Value);
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 5].Value) + " <br />  Compliance Type can not blank(Function based,Checklist,Time Based)";
                            return;
                        }
                    }

                    drCompliance["IShortDescription"] = Convert.ToString(xlWorksheet.Cells[i, 5].Value);
                    complianceData.IShortDescription = Convert.ToString(xlWorksheet.Cells[i, 5].Value);

                    drCompliance["IShortForm"] = Convert.ToString(xlWorksheet.Cells[i, 21].Value);
                    complianceData.IShortForm = Convert.ToString(xlWorksheet.Cells[i, 21].Value);

                    //if (((xlWorksheet.Cells[i, 3].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 3].Value) != string.Empty))
                    //{
                    //    drCompliance["IComplianceType"] = Convert.ToString(xlWorksheet.Cells[i, 3].Value);
                    //}
                    //else
                    //{
                    //    cvDuplicateEntry.IsValid = false;
                    //    cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 5].Value) + " <br />  Compliance Type can not blank(Function based,Checklist,Time Based)";
                    //    return;
                    //}

                    if (((xlWorksheet.Cells[i, 6].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 6].Value) != string.Empty) && (Convert.ToString(xlWorksheet.Cells[i, 6].Value) != "YES") || (Convert.ToString(xlWorksheet.Cells[i, 6].Value) != "NO"))
                    {
                        if (Convert.ToString(xlWorksheet.Cells[i, 6].Value).Trim().Equals("Yes"))
                        {
                            drCompliance["IUploadDocument"] = Convert.ToString(xlWorksheet.Cells[i, 7].Value);

                        }
                        else
                        {
                            complianceData.IUploadDocument = false;
                        }
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 5].Value) + " <br />  Upload Document can not blank(Yes,No)";
                        return;
                    }

                    drCompliance["IRequiredFrom"] = Convert.ToString(xlWorksheet.Cells[i, 7].Value);

                    if (((xlWorksheet.Cells[i, 8].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 8].Value) != string.Empty) && (Convert.ToString(xlWorksheet.Cells[i, 8].Value) != "One Time") || (Convert.ToString(xlWorksheet.Cells[i, 8].Value) != "Recurring"))
                    {
                        drCompliance["IComplianceOccurrence"] = Convert.ToString(xlWorksheet.Cells[i, 8].Value);
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 5].Value) + " <br />  ComplianceOccurrence can not blank";
                        return;
                    }

                    //if (((xlWorksheet.Cells[i,16].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 16].Value) != string.Empty) && (Convert.ToString(xlWorksheet.Cells[i, 16].Value) != "High") || (Convert.ToString(xlWorksheet.Cells[i, 16].Value) != "Low") || (Convert.ToString(xlWorksheet.Cells[i, 16].Value) != "Medium"))
                    if (((xlWorksheet.Cells[i, 16].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 16].Value) != string.Empty) && (Convert.ToString(xlWorksheet.Cells[i, 16].Value).Equals("High")) || (Convert.ToString(xlWorksheet.Cells[i, 16].Value).Equals("Medium")) || (Convert.ToString(xlWorksheet.Cells[i, 16].Value).Equals("Low")) || (Convert.ToString(xlWorksheet.Cells[i, 16].Value).Equals("Critical")))
                    {
                        drCompliance["IRiskType"] = Convert.ToString(xlWorksheet.Cells[i, 16].Value);
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 5].Value) + " <br />  Risk Type can not blank(High,Low,Medium,Critical)";
                        return;
                    }

                    if (Convert.ToString(xlWorksheet.Cells[i, 3].Value).Trim().Equals("Functionbased"))
                    {

                        if (((xlWorksheet.Cells[i, 9].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 9].Value) != string.Empty))
                        {

                            drCompliance["IFrequency"] = Convert.ToString(xlWorksheet.Cells[i, 9].Value);
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 5].Value) + " <br />  Frequency can not blank";
                            return;
                        }

                        if (((xlWorksheet.Cells[i, 10].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 10].Value) != string.Empty))
                        {
                            bool checkNumber = IsValidNumber(Convert.ToString(xlWorksheet.Cells[i, 10].Value));
                            if (!checkNumber)
                            {
                                //  errorMessage.Add("Please Enter only numbers in Contract Amount at row-" + a);
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 5].Value) + " <br />  Please Enter only numbers in Due Date";
                                return;
                            }
                            else
                            {
                                drCompliance["IDueDate"] = Convert.ToString(xlWorksheet.Cells[i, 10].Value);
                            }

                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 5].Value) + " <br />  DueDate can not blank";
                            return;
                        }

                        //if ((xlWorksheet.Cells[i, 11].Value != null && xlWorksheet.Cells[i, 11].Value != null) && (xlWorksheet.Cells[i, 12].Value.ToString().Trim() != "" && xlWorksheet.Cells[i, 12].Value.ToString().Trim() != ""))
                        //{

                        drCompliance["specilMonth"] = Convert.ToString(xlWorksheet.Cells[i, 11].Value);
                        drCompliance["specialDay"] = Convert.ToString(xlWorksheet.Cells[i, 12].Value);
                        //}
                        //else
                        //{
                        //    cvDuplicateEntry.IsValid = false;
                        //    cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 5].Value) + " <br />  SpecilMonth and SpecialDay can not blank";
                        //    return;
                        //}


                        if (((xlWorksheet.Cells[i, 18].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 18].Value) != string.Empty))
                        {
                            drCompliance["calflag"] = Convert.ToString(xlWorksheet.Cells[i, 18].Value);
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 5].Value) + " <br />  calflag can not blank";
                            return;
                        }
                    }

                    if (Convert.ToString(xlWorksheet.Cells[i, 3].Value).Trim().Equals("Timebased"))
                    {

                        if (((xlWorksheet.Cells[i, 4].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 4].Value) != string.Empty))
                        {
                            drCompliance["ISubComplianceType"] = Convert.ToString(xlWorksheet.Cells[i, 4].Value);
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 5].Value) + " <br />  SubComplianceType can not blank";
                            return;
                        }
                        if (Convert.ToString(xlWorksheet.Cells[i, 4].Value).Trim().Equals("FixedGap"))
                        {
                            if (((xlWorksheet.Cells[i, 4].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 4].Value) != string.Empty))
                            {
                                drCompliance["IDueDate"] = Convert.ToString(xlWorksheet.Cells[i, 10].Value);
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 5].Value) + " <br />  DueDate can not blank";
                                return;
                            }
                        }
                    }

                    if (((xlWorksheet.Cells[i, 13].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 13].Value) != string.Empty))
                    {
                        drCompliance["IReminderType"] = Convert.ToString(xlWorksheet.Cells[i, 13].Value);
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 5].Value) + " <br />  ReminderType can not blank(Standard,Custom)";
                        return;
                    }
                    if ((Convert.ToString(xlWorksheet.Cells[i, 22].Value) == "After") || (Convert.ToString(xlWorksheet.Cells[i, 22].Value) == "In Between") || (Convert.ToString(xlWorksheet.Cells[i, 22].Value) == "Before"))
                    {
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 5].Value) + " <br />  Please enter correct Schedule type";
                        return;
                    }
                    if (Convert.ToString(xlWorksheet.Cells[i, 13].Value).Trim().Equals("Standard"))
                    {
                    }
                    else
                    {
                        if (((xlWorksheet.Cells[i, 17].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 17].Value) != string.Empty))
                        {
                            drCompliance["IOneTimeDate"] = Convert.ToString(xlWorksheet.Cells[i, 17].Text);
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 5].Value) + " <br />  OneTime Date can not blank";
                            return;
                        }

                        if (((xlWorksheet.Cells[i, 14].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 14].Value) != string.Empty))
                        {
                            bool checkNumber = IsValidNumber(Convert.ToString(xlWorksheet.Cells[i, 10].Value));
                            if (!checkNumber)
                            {
                                //  errorMessage.Add("Please Enter only numbers in Contract Amount at row-" + a);
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 5].Value) + " <br />  Please Enter only numbers in Due Date";
                                return;
                            }
                            else
                            {
                                drCompliance["IReminderBefore"] = Convert.ToString(xlWorksheet.Cells[i, 14].Value);
                            }
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 5].Value) + " <br />  ReminderBefore can not blank";
                            return;
                        }
                        if (((xlWorksheet.Cells[i, 13].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 13].Value) != string.Empty))
                        {
                            bool checkNumber = IsValidNumber(Convert.ToString(xlWorksheet.Cells[i, 10].Value));
                            if (!checkNumber)
                            {
                                //  errorMessage.Add("Please Enter only numbers in Contract Amount at row-" + a);
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 5].Value) + " <br />  Please Enter only numbers in Due Date";
                                return;
                            }
                            else
                            {
                                drCompliance["IReminderGap"] = Convert.ToString(xlWorksheet.Cells[i, 15].Value);
                            }
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 5].Value) + " <br />  ReminderGap can not blank";
                            return;
                        }
                    }

                    if (((xlWorksheet.Cells[i, 19].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 19].Value) != string.Empty))
                    {
                        drCompliance["IsDocumentRequired"] = Convert.ToString(xlWorksheet.Cells[i, 19].Value);
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 5].Value) + " <br />  IsDocumentRequired not blank('Yes'/'No')";
                        return;
                    }

                    if (((xlWorksheet.Cells[i, 20].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 20].Value) != string.Empty))
                    {
                        drCompliance["IDetailedDescription"] = Convert.ToString(xlWorksheet.Cells[i, 20].Value);
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 5].Value) + " <br />  Detailed Description can not blank";
                        return;
                    }
                    if (((xlWorksheet.Cells[i, 21].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 21].Value) != string.Empty))
                    {
                        drCompliance["IShortForm"] = Convert.ToString(xlWorksheet.Cells[i, 21].Value);
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 5].Value) + " <br />  Short Form can not blank";
                        return;
                    }
                    if (((xlWorksheet.Cells[i, 22].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 22].Value) != string.Empty))
                    {
                        drCompliance["ScheduleType"] = Convert.ToString(xlWorksheet.Cells[i, 22].Value);
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 5].Value) + " <br />  Schedule Type can not blank";
                        return;
                    }

                    dtCompliance.Rows.Add(drCompliance);

                    grdCompliance.DataSource = dtCompliance;
                    grdCompliance.DataBind();
                }
                suucess = true;
            }
            catch (Exception ex)
            {
                suucess = false;
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void ProcessComplianceData()
        {
            try
            {
                List<com.VirtuosoITech.ComplianceManagement.Business.Data.InternalCompliance> complianceList = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.InternalCompliance>();
                if (AuthenticationHelper.Role == "IMPT")
                {
                    customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                }
                else
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }   
                for (int i = 0; i <= grdCompliance.Rows.Count-1; i++)
                {
                    com.VirtuosoITech.ComplianceManagement.Business.Data.InternalCompliance complianceData = new com.VirtuosoITech.ComplianceManagement.Business.Data.InternalCompliance();
                    if (((grdCompliance.Rows[i].Cells[0].Text) != null) && (Convert.ToString(grdCompliance.Rows[i].Cells[0].Text) != string.Empty))
                    {
                        try
                        {
                            List<InternalComplianceType> complianceTypeList = new List<InternalComplianceType>();
                            if (!(ComplianceTypeManagement.ExistsInternalComplianceType(Convert.ToString(grdCompliance.Rows[i].Cells[0].Text), customerID)))
                            {
                                InternalComplianceType complianceType = new InternalComplianceType();
                                complianceType.Name = Convert.ToString(grdCompliance.Rows[i].Cells[0].Text);
                                complianceType.CustomerID = customerID; //Convert.ToInt32(ddlCustomer.SelectedValue);
                                complianceType.IsDeleted = false;
                                complianceTypeList.Add(complianceType);
                                ComplianceTypeManagement.Create(complianceTypeList);
                                complianceData.IComplianceTypeID = ActManagement.GetInternalComplianceTypeIDByName(Convert.ToString(grdCompliance.Rows[i].Cells[0].Text), customerID);
                            }
                            else
                            {
                                complianceData.IComplianceTypeID = ActManagement.GetInternalComplianceTypeIDByName(Convert.ToString(grdCompliance.Rows[i].Cells[0].Text), customerID);
                            }
                        }
                        catch (Exception ex)
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Server Error Occured. InternalComplianceType .";
                            LoggerMessage.InsertLog(ex, "InternalComplianceType ", MethodBase.GetCurrentMethod().Name);
                        }

                        try
                        {
                            List<InternalCompliancesCategory> complianceCategoryList = new List<InternalCompliancesCategory>();
                            if (!(ComplianceCategoryManagement.ExistsInternalComplianceCategory(Convert.ToString(grdCompliance.Rows[i].Cells[1].Text), customerID)))
                            {
                                InternalCompliancesCategory Category = new InternalCompliancesCategory();
                                Category.Name = Convert.ToString(grdCompliance.Rows[i].Cells[1].Text);
                                Category.CustomerID = customerID;
                                Category.IsDeleted = false;
                                complianceCategoryList.Add(Category);
                                ComplianceCategoryManagement.CreateInternalCompliancesCategory(complianceCategoryList);
                                complianceData.IComplianceCategoryID = ActManagement.GetInternalComplianceCategoryIDByName(Convert.ToString(grdCompliance.Rows[i].Cells[1].Text), customerID);
                            }
                            else
                            {
                                complianceData.IComplianceCategoryID = ActManagement.GetInternalComplianceCategoryIDByName(Convert.ToString(grdCompliance.Rows[i].Cells[1].Text), customerID);
                            }
                        }
                        catch (Exception ex)
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Server Error Occured. InternalCompliancesCategory .";
                            LoggerMessage.InsertLog(ex, "InternalCompliancesCategory ", MethodBase.GetCurrentMethod().Name);
                        }
                      

                        complianceData.CustomerID = customerID;// Convert.ToInt32(ddlCustomer.SelectedValue);

                        try
                        {
                            if (Convert.ToString(grdCompliance.Rows[i].Cells[2].Text).Trim().Equals("Functionbased"))
                            {
                                complianceData.IComplianceType = 0;
                            }
                            else if (Convert.ToString(grdCompliance.Rows[i].Cells[2].Text).Trim().Equals("Timebased"))
                            {
                                complianceData.IComplianceType = 2;
                            }
                            else
                            {
                                complianceData.IComplianceType = 0;
                            }
                        }
                        catch (Exception ex)
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Server Error Occured. IComplianceType .";
                            LoggerMessage.InsertLog(ex, "IComplianceType ", MethodBase.GetCurrentMethod().Name);
                        }
                       

                        complianceData.IShortDescription = Convert.ToString(grdCompliance.Rows[i].Cells[4].Text);

                        try
                        {
                            string IUploadDocument = Convert.ToString(grdCompliance.Rows[i].Cells[5].Text).Trim().ToUpper();
                            if (IUploadDocument == "YES")
                            {
                                complianceData.IUploadDocument = true;
                            }
                            else
                            {
                                complianceData.IUploadDocument = false;
                            }
                        }
                        catch (Exception ex)
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Server Error Occured. IUploadDocument .";
                            LoggerMessage.InsertLog(ex, "IUploadDocument  ", MethodBase.GetCurrentMethod().Name);
                        }

                        if (!string.IsNullOrEmpty(Convert.ToString(grdCompliance.Rows[i].Cells[6].Text)))
                        {
                            var aa= Convert.ToString(grdCompliance.Rows[i].Cells[6].Text);
                            if (aa != "&nbsp;")
                            {
                                complianceData.IRequiredFrom = aa;
                            }                            
                        }
                        else
                        {
                            complianceData.IRequiredFrom = null;
                        }
                        try
                        {
                            if (Convert.ToString(grdCompliance.Rows[i].Cells[13].Text).Trim().Equals("High"))
                            {
                                complianceData.IRiskType = 0;
                            }
                            else if (Convert.ToString(grdCompliance.Rows[i].Cells[13].Text).Trim().Equals("Medium"))
                            {
                                complianceData.IRiskType = 1;
                            }
                            else if (Convert.ToString(grdCompliance.Rows[i].Cells[13].Text).Trim().Equals("Low"))
                            {
                                complianceData.IRiskType = 2;
                            }
                            else
                            {
                                complianceData.IRiskType = 3;
                            }
                        }
                        catch (Exception ex)
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Server Error Occured. IRiskType .";
                            LoggerMessage.InsertLog(ex, "IRiskType  ", MethodBase.GetCurrentMethod().Name);
                        }

                        try
                        {
                            if (Convert.ToString(grdCompliance.Rows[i].Cells[2].Text).Trim().Equals("Functionbased"))
                            {
                                complianceData.IFrequency = Convert.ToByte(Enumerations.GetEnumByName<Frequency>(Convert.ToString(grdCompliance.Rows[i].Cells[8].Text)));
                                if (grdCompliance.Rows[i].Cells[9].Text != null && grdCompliance.Rows[i].Cells[9].Text.ToString().Trim() != "")
                                {
                                    if (complianceData.IFrequency != 7 && complianceData.IFrequency != 8)
                                    {
                                        complianceData.IDueDate = Convert.ToInt32(grdCompliance.Rows[i].Cells[9].Text);
                                    }
                                }
                                int step = 1;
                                switch ((Frequency)Convert.ToByte(Enumerations.GetEnumByName<Frequency>(Convert.ToString(grdCompliance.Rows[i].Cells[8].Text))))
                                {
                                    case Frequency.Monthly:
                                        step = 1;
                                        break;
                                    case Frequency.Quarterly:
                                        step = 3;
                                        break;
                                    case Frequency.FourMonthly:
                                        step = 4;
                                        break;
                                    case Frequency.HalfYearly:
                                        step = 6;
                                        break;
                                    case Frequency.Annual:
                                        step = 12;
                                        break;
                                    default:
                                        step = 12;
                                        break;
                                }
                                int specilMonth = 0;
                                int specialDay = 0;
                                if ((grdCompliance.Rows[i].Cells[11].Text != null && grdCompliance.Rows[i].Cells[11].Text != "&nbsp;" && grdCompliance.Rows[i].Cells[12].Text != null) && (grdCompliance.Rows[i].Cells[11].Text.ToString().Trim() != "" && grdCompliance.Rows[i].Cells[12].Text != "&nbsp;" && grdCompliance.Rows[i].Cells[12].Text.ToString().Trim() != ""))
                                {
                                    specilMonth = Convert.ToInt32(grdCompliance.Rows[i].Cells[11].Text);
                                    specialDay = Convert.ToInt32(grdCompliance.Rows[i].Cells[12].Text);
                                }
                                List<InternalComplianceSchedule> complianceSchedule = new List<InternalComplianceSchedule>();
                                int SpecialMonth;
                                string calflag = Convert.ToString(grdCompliance.Rows[i].Cells[15].Text);
                                var frequency = complianceData.IFrequency;
                                if (frequency != 7 && frequency != 8)
                                {
                                    if (calflag == "Y" && complianceData.IFrequency != 1 && complianceData.IFrequency != 0)
                                    {
                                        for (int month = 4; month <= 12; month += step)
                                        {
                                            InternalComplianceSchedule complianceShedule = new InternalComplianceSchedule();
                                            complianceShedule.ForMonth = month;

                                            SpecialMonth = month;
                                            if (complianceData.IFrequency == 0 && SpecialMonth == 12)
                                            {
                                                SpecialMonth = 1;
                                            }
                                            else if (complianceData.IFrequency == 1 && SpecialMonth == 10)
                                            {
                                                SpecialMonth = 1;
                                            }
                                            else if (complianceData.IFrequency == 2 && SpecialMonth == 10)
                                            {
                                                SpecialMonth = 4;
                                            }
                                            else if (complianceData.IFrequency == 2 && SpecialMonth == 4)
                                            {
                                                SpecialMonth = 10;
                                            }
                                            else if (complianceData.IFrequency == 4 && SpecialMonth == 4)
                                            {
                                                SpecialMonth = 8;
                                            }
                                            else if (complianceData.IFrequency == 4 && SpecialMonth == 8)
                                            {
                                                SpecialMonth = 12;
                                            }
                                            else if (complianceData.IFrequency == 4 && SpecialMonth == 12)
                                            {
                                                SpecialMonth = 4;
                                            }
                                            else if (complianceData.IFrequency == 3 && SpecialMonth == 1)
                                            {
                                                SpecialMonth = 4;
                                            }
                                            else if (complianceData.IFrequency == 3 && SpecialMonth == 4)
                                            {
                                                SpecialMonth = 4;
                                            }
                                            else if ((complianceData.IFrequency == 5 || complianceData.IFrequency == 6) && SpecialMonth == 1)
                                            {
                                                SpecialMonth = 1;
                                            }
                                            else
                                            {
                                                SpecialMonth = SpecialMonth + step;
                                            }

                                            //int lastdate = DateTime.DaysInMonth(DateTime.Now.Year, SpecialMonth);
                                            //if (Convert.ToInt32(compliance.IDueDate.Value.ToString("D2")) > lastdate)
                                            //{
                                            //    complianceShedule.SpecialDate = lastdate.ToString() + SpecialMonth.ToString("D2");
                                            //}
                                            //else
                                            //{
                                            //    complianceShedule.SpecialDate = compliance.IDueDate.Value.ToString("D2") + SpecialMonth.ToString("D2");
                                            //}

                                            int lastdate = DateTime.DaysInMonth(DateTime.Now.Year, SpecialMonth);
                                            if (complianceData.IDueDate > lastdate)
                                            {
                                                complianceShedule.SpecialDate = lastdate.ToString() + SpecialMonth.ToString("D2");
                                            }
                                            else
                                            {
                                                if (grdCompliance.Rows[i].Cells[9].Text != null && grdCompliance.Rows[i].Cells[9].Text.ToString().Trim() != "")
                                                {
                                                    complianceShedule.SpecialDate = Convert.ToInt32(grdCompliance.Rows[i].Cells[9].Text).ToString("D2") + SpecialMonth.ToString("D2");
                                                }
                                                else
                                                {
                                                    complianceShedule.SpecialDate = Convert.ToInt32(grdCompliance.Rows[i].Cells[11].Text).ToString("D2") + Convert.ToInt32(grdCompliance.Rows[i].Cells[10].Text).ToString("D2");
                                                }
                                            }

                                            if (specilMonth == month)
                                            {
                                                complianceShedule.SpecialDate = specialDay.ToString("D2") + month.ToString("D2");
                                                complianceData.InternalComplianceSchedules.Add(complianceShedule);
                                            }
                                            else
                                            {
                                                complianceData.InternalComplianceSchedules.Add(complianceShedule);
                                            }
                                        }
                                    }//Financial Year close
                                    else
                                    {
                                        for (int month = 1; month <= 12; month += step)
                                        {

                                            InternalComplianceSchedule complianceShedule = new InternalComplianceSchedule();
                                            complianceShedule.ForMonth = month;

                                            SpecialMonth = month;
                                            if (complianceData.IFrequency == 0 && SpecialMonth == 12)
                                            {
                                                SpecialMonth = 1;
                                            }
                                            else if (complianceData.IFrequency == 1 && SpecialMonth == 10)
                                            {
                                                SpecialMonth = 1;
                                            }
                                            else if (complianceData.IFrequency == 2 && SpecialMonth == 7)
                                            {
                                                SpecialMonth = 1;
                                            }
                                            else if (complianceData.IFrequency == 4 && SpecialMonth == 9)
                                            {
                                                SpecialMonth = 1;
                                            }
                                            else if ((complianceData.IFrequency == 3 || complianceData.IFrequency == 5 || complianceData.IFrequency == 6) && SpecialMonth == 1)
                                            {
                                                SpecialMonth = 1;
                                            }
                                            else
                                            {
                                                SpecialMonth = SpecialMonth + step;
                                            }
                                            int lastdate = DateTime.DaysInMonth(DateTime.Now.Year, month);
                                            if (complianceData.IDueDate > lastdate)
                                            {
                                                complianceShedule.SpecialDate = lastdate.ToString() + SpecialMonth.ToString("D2");
                                            }
                                            else
                                            {

                                                if (grdCompliance.Rows[i].Cells[9].Text != null && grdCompliance.Rows[i].Cells[9].Text.ToString().Trim() != "")
                                                {
                                                    if (Convert.ToInt32(grdCompliance.Rows[i].Cells[9].Text) < 0)
                                                    {
                                                        //string negativevalue = Convert.ToString(xlWorksheet.Cells[i, 17].Value).Trim('-');                                               
                                                        //complianceShedule.SpecialDate = Convert.ToInt32(negativevalue).ToString("D2") + SpecialMonth.ToString("D2");
                                                    }
                                                    else
                                                    {
                                                        complianceShedule.SpecialDate = Convert.ToInt32(grdCompliance.Rows[i].Cells[9].Text).ToString("D2") + SpecialMonth.ToString("D2");
                                                    }
                                                }
                                                else
                                                {
                                                    if (Convert.ToInt32(grdCompliance.Rows[i].Cells[9].Text) < 0)
                                                    {
                                                        //string negativevalue = Convert.ToString(xlWorksheet.Cells[i, 17].Value).Trim('-');                                               
                                                        //complianceShedule.SpecialDate = Convert.ToInt32(negativevalue).ToString("D2") + SpecialMonth.ToString("D2");
                                                    }
                                                    else
                                                    {
                                                        complianceShedule.SpecialDate = Convert.ToInt32(grdCompliance.Rows[i].Cells[11].Text).ToString("D2") + Convert.ToInt32(grdCompliance.Rows[i].Cells[10].Text).ToString("D2");
                                                    }
                                                }
                                            }

                                            if (specilMonth == month)
                                            {
                                                if (Convert.ToInt32(grdCompliance.Rows[i].Cells[9].Text) < 0)
                                                {
                                                }
                                                else
                                                {
                                                    complianceShedule.SpecialDate = specialDay.ToString("D2") + month.ToString("D2");
                                                    complianceData.InternalComplianceSchedules.Add(complianceShedule);
                                                }
                                            }
                                            else
                                            {
                                                if (Convert.ToInt32(grdCompliance.Rows[i].Cells[9].Text) < 0)
                                                {
                                                }
                                                else
                                                {
                                                    complianceData.InternalComplianceSchedules.Add(complianceShedule);
                                                }
                                            }
                                        }
                                    }//Calender Year Close
                                }
                                else
                                {
                                    if (Convert.ToString(grdCompliance.Rows[i].Cells[3].Text).Trim().Equals("FixedGap"))
                                    {
                                        complianceData.ISubComplianceType = 0;//fixed gap
                                        complianceData.IDueDate = Convert.ToInt32(grdCompliance.Rows[i].Cells[9].Text);
                                    }
                                    else
                                    {
                                    }
                                }
                            }
                            if (Convert.ToString(grdCompliance.Rows[i].Cells[2].Text).Trim().Equals("Timebased"))
                            {
                                if (Convert.ToString(grdCompliance.Rows[i].Cells[3].Text).Trim().Equals("FixedGap"))
                                {
                                    complianceData.ISubComplianceType = 0;//fixed gap
                                    complianceData.IDueDate = Convert.ToInt32(grdCompliance.Rows[i].Cells[9].Text);
                                }
                                else
                                {
                                    complianceData.ISubComplianceType = 1;//periodically
                                    int step = 1;
                                    complianceData.IFrequency = Convert.ToByte(Enumerations.GetEnumByName<Frequency>(Convert.ToString(grdCompliance.Rows[i].Cells[8].Text)));
                                    switch ((Frequency)Convert.ToByte(Enumerations.GetEnumByName<Frequency>(Convert.ToString(grdCompliance.Rows[i].Cells[8].Text))))
                                    {
                                        case Frequency.Monthly:
                                            step = 1;
                                            break;
                                        case Frequency.Quarterly:
                                            step = 3;
                                            break;
                                        case Frequency.FourMonthly:
                                            step = 4;
                                            break;
                                        case Frequency.HalfYearly:
                                            step = 6;
                                            break;
                                        case Frequency.Annual:
                                            step = 12;
                                            break;
                                        default:
                                            step = 12;
                                            break;
                                    }
                                    //int SpecialMonth;
                                    List<InternalComplianceSchedule> complianceSchedule = new List<InternalComplianceSchedule>();
                                    for (int month = 1; month <= 12; month += step)
                                    {
                                        InternalComplianceSchedule complianceShedule = new InternalComplianceSchedule();
                                        complianceShedule.ForMonth = month;
                                        //}

                                        //if (subcomplincetype == 1 || subcomplincetype == 2)
                                        //{
                                        int monthCopy = month;
                                        if (monthCopy == 1)
                                        {
                                            monthCopy = 12;
                                            complianceShedule.ForMonth = monthCopy;
                                        }
                                        else
                                        {
                                            monthCopy = month - 1;
                                            complianceShedule.ForMonth = monthCopy;
                                        }

                                        //if (compliance.Frequency == 3 || compliance.Frequency == 5 || compliance.Frequency == 6)//Annual,TwoYearly,SevenYearly
                                        if (Convert.ToString(grdCompliance.Rows[i].Cells[8].Text).Trim().Equals("Annual") || Convert.ToString(grdCompliance.Rows[i].Cells[8].Text).Trim().Equals("TwoYearly") || Convert.ToString(grdCompliance.Rows[i].Cells[8].Text).Trim().Equals("SevenYearly"))
                                        {
                                            monthCopy = 3;
                                            complianceShedule.ForMonth = monthCopy;
                                        }

                                        int lastdateOfPeriodic = DateTime.DaysInMonth(DateTime.Now.Year, monthCopy);
                                        complianceShedule.SpecialDate = lastdateOfPeriodic.ToString() + monthCopy.ToString("D2");

                                        //complianceData.ComplianceSchedules.Add(complianceShedule);
                                        complianceData.InternalComplianceSchedules.Add(complianceShedule);

                                        //}
                                    }

                                }
                            }
                            if (Convert.ToString(grdCompliance.Rows[i].Cells[10].Text).Trim().Equals("Standard"))
                            {
                                complianceData.IReminderType = 0;//standard
                            }
                            else
                            {
                                complianceData.IReminderType = 1;//custom
                                complianceData.IOneTimeDate = Convert.ToDateTime(grdCompliance.Rows[i].Cells[14].Text);
                                complianceData.IReminderBefore = Convert.ToInt32(grdCompliance.Rows[i].Cells[11].Text);
                                complianceData.IReminderGap = Convert.ToInt32(grdCompliance.Rows[i].Cells[12].Text);
                            }
                        }
                        catch (Exception ex)
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Server Error Occured. Compliance Schedule .";
                            LoggerMessage.InsertLog(ex, "Compliance Schedule  ", MethodBase.GetCurrentMethod().Name);
                        }
                       

                        complianceData.CreatedOn = DateTime.Now;
                        complianceData.UpdatedOn = DateTime.Now;
                        complianceData.IsDeleted = false;

                        try
                        {
                            if (Convert.ToString(grdCompliance.Rows[i].Cells[7].Text).Trim().Equals("Recurring"))
                            {
                                complianceData.IComplianceOccurrence = 1;
                            }
                            else
                            {
                                complianceData.IComplianceOccurrence = 0;
                            }
                        }
                        catch (Exception ex)
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Server Error Occured. IComplianceOccurrence.";
                            LoggerMessage.InsertLog(ex, "IComplianceOccurrence  ", MethodBase.GetCurrentMethod().Name);
                        }

                        try
                        {
                            string IsDocumentRequired = Convert.ToString(grdCompliance.Rows[i].Cells[16].Text).Trim().ToUpper();
                            if (IsDocumentRequired == "YES")
                            {
                                complianceData.IsDocumentRequired = true;
                            }
                            else
                            {
                                complianceData.IsDocumentRequired = false;
                            }
                        }
                        catch (Exception ex)
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Server Error Occured. IsDocumentRequired.";
                            LoggerMessage.InsertLog(ex, "IsDocumentRequired  ", MethodBase.GetCurrentMethod().Name);
                        }
                      
                        complianceData.IDetailedDescription = Convert.ToString(grdCompliance.Rows[i].Cells[17].Text);
                        complianceData.IShortForm = Convert.ToString(grdCompliance.Rows[i].Cells[18].Text);

                        try
                        {
                            string schtype = Convert.ToString(grdCompliance.Rows[i].Cells[19].Text);
                            if (schtype == "Before")
                            {
                                complianceData.ScheduleType = 2;
                            }
                            else if (schtype == "In Between")
                            {
                                complianceData.ScheduleType = 1;
                            }
                            else if (schtype == "After")
                            {
                                complianceData.ScheduleType = 0;
                            }
                            else
                            {
                                complianceData.ScheduleType = null;
                            }
                        }
                        catch (Exception ex)
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Server Error Occured. ScheduleType.";
                            LoggerMessage.InsertLog(ex, "ScheduleType  ", MethodBase.GetCurrentMethod().Name);
                        }


                        try
                        {
                            string IsEvent = Convert.ToString(grdCompliance.Rows[i].Cells[20].Text).Trim().ToUpper();
                            if (IsEvent == "YES")
                            {
                                complianceData.EventFlag = true;
                            }
                            else
                            {
                                complianceData.EventFlag = null;
                            }
                        }
                        catch (Exception ex)
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Server Error Occured. EventFlag.";
                            LoggerMessage.InsertLog(ex, "EventFlag  ", MethodBase.GetCurrentMethod().Name);
                        }

                        try
                        {
                            if (complianceData.IFrequency == 8)
                            {
                                string weekDay = Convert.ToString(grdCompliance.Rows[i].Cells[21].Text).Trim().ToUpper();
                                byte? WeekDay = 1;
                                switch (weekDay)
                                {
                                    case "SUNDAY":
                                        WeekDay = 0;
                                        break;
                                    case "MONDAY":
                                        WeekDay = 1;
                                        break;
                                    case "TUESDAY":
                                        WeekDay = 2;
                                        break;
                                    case "WEDNESDAY":
                                        WeekDay = 3;
                                        break;
                                    case "THURSDAY":
                                        WeekDay = 4;
                                        break;
                                    case "FRIDAY":
                                        WeekDay = 5;
                                        break;
                                    case "SATURDAY":
                                        WeekDay = 6;
                                        break;
                                }

                                complianceData.DueWeekDay = WeekDay;
                            }
                        }
                        catch (Exception ex)
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Server Error Occured. EventFlag.";
                            LoggerMessage.InsertLog(ex, "DueWeekDay  ", MethodBase.GetCurrentMethod().Name);
                        }

                        complianceData.CreatedBy = AuthenticationHelper.UserID;
                        complianceData.UpdatedBy = AuthenticationHelper.UserID;
                    }
                    complianceList.Add(complianceData);
                }
                Business.ComplianceManagement.CreateInternal(complianceList, customerID);
                suucessSave = true;
            }
            catch (Exception ex)
            {
                suucessSave = false;
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ProcessComplianceData();
                btnSave.Enabled = false;

                if (suucessSave == true)
                {
                    grdCompliance.DataSource = null;
                    grdCompliance.DataBind();
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Data save successfully.";
                    btnSave.Enabled = true;
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        #endregion


        #region Commn
        private bool InternalComplianceSheetsExitsts(ExcelPackage xlWorkbook, string data)
        {
            try
            {
                bool flag = false;
                foreach (ExcelWorksheet sheet in xlWorkbook.Workbook.Worksheets)
                {
                    if (data.Equals("InternalCompliances"))
                    {
                        if (!sheet.Name.Equals("InternalChecklist"))
                        {
                            if (sheet.Name.Trim().Equals("Act") || sheet.Name.Trim().Equals("Compliance Categories") || sheet.Name.Trim().Equals("Compliance Types") || sheet.Name.Trim().Equals("Compliances") || sheet.Name.Trim().Equals("InternalCompliances"))
                            {
                                flag = true;
                                break;
                            }
                            else
                            {
                                flag = false;
                                break;
                            }
                        }
                    }
                    if (data.Equals("InternalChecklist"))
                    {
                        if (sheet.Name.Trim().Equals("Act") || sheet.Name.Trim().Equals("Compliance Categories") || sheet.Name.Trim().Equals("Compliance Types") || sheet.Name.Trim().Equals("Checklist") || sheet.Name.Trim().Equals("InternalChecklist"))
                        {
                            flag = true;
                            break;
                        }
                        else
                        {
                            flag = false;
                            break;
                        }
                    }

                }
                return flag;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }
        private void createDataTable()
        {
            dtCompliance = new DataTable();
            dtCompliance.Columns.Add("CustomerID");
            dtCompliance.Columns.Add("IComplianceCategoryID");
            dtCompliance.Columns.Add("IComplianceTypeID");
            dtCompliance.Columns.Add("IShortDescription");
            dtCompliance.Columns.Add("IComplianceType");
            dtCompliance.Columns.Add("IUploadDocument");
            dtCompliance.Columns.Add("IRequiredFrom");
            dtCompliance.Columns.Add("IFrequency");
            dtCompliance.Columns.Add("IDueDate");
            dtCompliance.Columns.Add("IReminderType");
            dtCompliance.Columns.Add("IRiskType");
            dtCompliance.Columns.Add("IReminderBefore");
            dtCompliance.Columns.Add("IReminderGap");
            dtCompliance.Columns.Add("ISubComplianceType");
            dtCompliance.Columns.Add("calflag");
            dtCompliance.Columns.Add("specilMonth");
            dtCompliance.Columns.Add("specialDay");
            //dtCompliance.Columns.Add("IsDeleted");
            //dtCompliance.Columns.Add("CreatedOn");
            //dtCompliance.Columns.Add("CreatedBy");
            //dtCompliance.Columns.Add("UpdatedOn");
            //dtCompliance.Columns.Add("UpdatedBy");
            dtCompliance.Columns.Add("IComplianceOccurrence");
            dtCompliance.Columns.Add("IOneTimeDate");
            dtCompliance.Columns.Add("CheckListTypeID");
            dtCompliance.Columns.Add("EffectiveDate");
            dtCompliance.Columns.Add("IsDocumentRequired");
            dtCompliance.Columns.Add("IDetailedDescription");
            dtCompliance.Columns.Add("IShortForm");
            dtCompliance.Columns.Add("ScheduleType");
            dtCompliance.Columns.Add("IsEventBased");
            dtCompliance.Columns.Add("DueWeekDay");
        }

        public static bool IsValidNumber(string Number)
        {
            int num;
            bool isNum = Int32.TryParse(Number, out num);

            return isNum;
        }
        #endregion

        #region CheckList Upload
        private void ProcessCheckListData()
        {
            try
            {
                List<Business.Data.InternalCompliance> complianceList = new List<Business.Data.InternalCompliance>();
                if (AuthenticationHelper.Role == "IMPT")
                {
                    customerID = Convert.ToInt32(ddlchecklistCustomer.SelectedValue);
                }
                else
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                for (int i = 0; i <= grdCheckList.Rows.Count - 1; i++)
                {
                    com.VirtuosoITech.ComplianceManagement.Business.Data.InternalCompliance complianceData = new com.VirtuosoITech.ComplianceManagement.Business.Data.InternalCompliance();
                    if (((grdCheckList.Rows[i].Cells[0].Text) != null) && (Convert.ToString(grdCheckList.Rows[i].Cells[0].Text) != string.Empty))
                    {
                        try
                        {
                            List<InternalComplianceType> complianceTypeList = new List<InternalComplianceType>();
                            if (!(ComplianceTypeManagement.ExistsInternalComplianceType(Convert.ToString(grdCheckList.Rows[i].Cells[0].Text), customerID)))
                            {
                                InternalComplianceType complianceType = new InternalComplianceType();
                                complianceType.Name = Convert.ToString(grdCheckList.Rows[i].Cells[0].Text);
                                complianceType.CustomerID = customerID;
                                complianceType.IsDeleted = false;
                                complianceTypeList.Add(complianceType);
                                ComplianceTypeManagement.Create(complianceTypeList);
                                complianceData.IComplianceTypeID = ActManagement.GetInternalComplianceTypeIDByName(Convert.ToString(grdCheckList.Rows[i].Cells[0].Text), customerID);
                            }
                            else
                            {
                                complianceData.IComplianceTypeID = ActManagement.GetInternalComplianceTypeIDByName(Convert.ToString(grdCheckList.Rows[i].Cells[0].Text), customerID);
                            }
                        }
                        catch (Exception ex)
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Server Error Occured. InternalComplianceType .";
                            LoggerMessage.InsertLog(ex, "InternalComplianceType ", MethodBase.GetCurrentMethod().Name);
                        }

                        try
                        {
                            List<InternalCompliancesCategory> complianceCategoryList = new List<InternalCompliancesCategory>();
                            if (!(ComplianceCategoryManagement.ExistsInternalComplianceCategory(Convert.ToString(grdCheckList.Rows[i].Cells[1].Text), customerID)))
                            {
                                InternalCompliancesCategory Category = new InternalCompliancesCategory();
                                Category.Name = Convert.ToString(grdCheckList.Rows[i].Cells[1].Text);
                                Category.CustomerID = customerID;
                                Category.IsDeleted = false;
                                complianceCategoryList.Add(Category);
                                ComplianceCategoryManagement.CreateInternalCompliancesCategory(complianceCategoryList);
                                complianceData.IComplianceCategoryID = ActManagement.GetInternalComplianceCategoryIDByName(Convert.ToString(grdCheckList.Rows[i].Cells[1].Text), customerID);
                            }
                            else
                            {
                                complianceData.IComplianceCategoryID = ActManagement.GetInternalComplianceCategoryIDByName(Convert.ToString(grdCheckList.Rows[i].Cells[1].Text), customerID);
                            }
                        }
                        catch (Exception ex )
                        {

                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Server Error Occured. InternalCompliancesCategory .";
                            LoggerMessage.InsertLog(ex, "InternalCompliancesCategory ", MethodBase.GetCurrentMethod().Name);
                        }
                       

                        complianceData.CustomerID = customerID;

                        try
                        {
                            if (Convert.ToString(grdCheckList.Rows[i].Cells[2].Text).Trim().Equals("Function based Checklist"))
                            {
                                complianceData.IComplianceType = 0;
                            }
                            else if (Convert.ToString(grdCheckList.Rows[i].Cells[2].Text).Trim().Equals("Time Based Checklist"))
                            {
                                complianceData.IComplianceType = 2;
                            }
                            else
                            {
                                complianceData.IComplianceType = 1;
                            }
                        }
                        catch (Exception ex)
                        {

                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Server Error Occured. IComplianceType .";
                            LoggerMessage.InsertLog(ex, "IComplianceType ", MethodBase.GetCurrentMethod().Name);
                        }
                       

                        complianceData.IShortDescription = Convert.ToString(grdCheckList.Rows[i].Cells[4].Text);


                        try
                        {

                            string IUploadDocument = Convert.ToString(grdCheckList.Rows[i].Cells[5].Text).Trim().ToUpper();
                            if (IUploadDocument == "YES")
                            {
                                complianceData.IUploadDocument = true;
                            }
                            else
                            {
                                complianceData.IUploadDocument = false;
                            }                            
                        }
                        catch (Exception ex)
                        {

                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Server Error Occured. IUploadDocument .";
                            LoggerMessage.InsertLog(ex, "IUploadDocument ", MethodBase.GetCurrentMethod().Name);
                        }


                        try
                        {
                            if (!string.IsNullOrEmpty(Convert.ToString(grdCheckList.Rows[i].Cells[6].Text)))
                            {
                                var aa = Convert.ToString(grdCheckList.Rows[i].Cells[6].Text);
                                if (aa != "&nbsp;")
                                {
                                    complianceData.IRequiredFrom = aa;
                                }
                            }
                            else
                            {
                                complianceData.IRequiredFrom = null;
                            }
                        }
                        catch (Exception ex)
                        {

                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Server Error Occured. IRequiredFrom .";
                            LoggerMessage.InsertLog(ex, "IRequiredFrom ", MethodBase.GetCurrentMethod().Name);
                        }


                        try
                        {
                            if (Convert.ToString(grdCheckList.Rows[i].Cells[15].Text).Trim().Equals("High"))
                            {
                                complianceData.IRiskType = 0;
                            }
                            else if (Convert.ToString(grdCheckList.Rows[i].Cells[15].Text).Trim().Equals("Medium"))
                            {
                                complianceData.IRiskType = 1;
                            }
                            else if (Convert.ToString(grdCheckList.Rows[i].Cells[15].Text).Trim().Equals("Low"))
                            {
                                complianceData.IRiskType = 2;
                            }
                            else
                            {
                                complianceData.IRiskType = 3;
                            }
                        }
                        catch (Exception ex)
                        {

                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Server Error Occured. IRiskType .";
                            LoggerMessage.InsertLog(ex, "IRiskType ", MethodBase.GetCurrentMethod().Name);
                        }

                        try
                        {
                            if (Convert.ToString(grdCheckList.Rows[i].Cells[17].Text).Trim().Equals("Function based Checklist"))
                            {
                                #region Function based Checklist
                                complianceData.IFrequency = Convert.ToByte(Enumerations.GetEnumByName<Frequency>(Convert.ToString(grdCheckList.Rows[i].Cells[8].Text)));
                                if (grdCheckList.Rows[i].Cells[9].Text != null && grdCheckList.Rows[i].Cells[9].Text.ToString().Trim() != "")
                                {
                                    if (complianceData.IFrequency != 7 && complianceData.IFrequency != 8)
                                    {
                                        complianceData.IDueDate = Convert.ToInt32(grdCheckList.Rows[i].Cells[9].Text);
                                    }
                                }
                                int step = 1;
                                switch ((Frequency)Convert.ToByte(Enumerations.GetEnumByName<Frequency>(Convert.ToString(grdCheckList.Rows[i].Cells[8].Text))))
                                {
                                    case Frequency.Monthly:
                                        step = 1;
                                        break;
                                    case Frequency.Quarterly:
                                        step = 3;
                                        break;
                                    case Frequency.FourMonthly:
                                        step = 4;
                                        break;
                                    case Frequency.HalfYearly:
                                        step = 6;
                                        break;
                                    case Frequency.Annual:
                                        step = 12;
                                        break;
                                    default:
                                        step = 12;
                                        break;
                                }
                                int specilMonth = 0;
                                int specialDay = 0;
                                if ((grdCheckList.Rows[i].Cells[10].Text != null && grdCheckList.Rows[i].Cells[10].Text != "&nbsp;" && grdCheckList.Rows[i].Cells[10].Text != null) && (grdCheckList.Rows[i].Cells[11].Text.ToString().Trim() != "" && grdCheckList.Rows[i].Cells[11].Text != "&nbsp;" && grdCheckList.Rows[i].Cells[11].Text.ToString().Trim() != ""))
                                {
                                    specilMonth = Convert.ToInt32(grdCheckList.Rows[i].Cells[10].Text);
                                    specialDay = Convert.ToInt32(grdCheckList.Rows[i].Cells[11].Text);
                                }
                                List<InternalComplianceSchedule> complianceSchedule = new List<InternalComplianceSchedule>();

                                int SpecialMonth;
                                #region Financial Year And Calender
                                string calflag = Convert.ToString(grdCheckList.Rows[i].Cells[18].Text);
                                var frequency = complianceData.IFrequency;
                                if (frequency != 7 && frequency != 8)
                                {
                                    if (calflag == "Y")
                                    {
                                        for (int month = 4; month <= 12; month += step)
                                        {
                                            InternalComplianceSchedule complianceShedule = new InternalComplianceSchedule();
                                            complianceShedule.ForMonth = month;

                                            SpecialMonth = month;
                                            if (complianceData.IFrequency == 0 && SpecialMonth == 12)
                                            {
                                                SpecialMonth = 1;
                                            }
                                            else if (complianceData.IFrequency == 1 && SpecialMonth == 10)
                                            {
                                                SpecialMonth = 1;
                                            }
                                            else if (complianceData.IFrequency == 2 && SpecialMonth == 10)
                                            {
                                                SpecialMonth = 4;
                                            }
                                            else if (complianceData.IFrequency == 2 && SpecialMonth == 4)
                                            {
                                                SpecialMonth = 10;
                                            }
                                            else if (complianceData.IFrequency == 4 && SpecialMonth == 4)
                                            {
                                                SpecialMonth = 8;
                                            }
                                            else if (complianceData.IFrequency == 4 && SpecialMonth == 8)
                                            {
                                                SpecialMonth = 12;
                                            }
                                            else if (complianceData.IFrequency == 4 && SpecialMonth == 12)
                                            {
                                                SpecialMonth = 4;
                                            }
                                            else if (complianceData.IFrequency == 3 && SpecialMonth == 1)
                                            {
                                                SpecialMonth = 4;
                                            }
                                            else if (complianceData.IFrequency == 3 && SpecialMonth == 4)
                                            {
                                                SpecialMonth = 4;
                                            }
                                            else if ((complianceData.IFrequency == 5 || complianceData.IFrequency == 6) && SpecialMonth == 1)
                                            {
                                                SpecialMonth = 1;
                                            }
                                            else
                                            {
                                                SpecialMonth = SpecialMonth + step;
                                            }
                                            int lastdate = DateTime.DaysInMonth(DateTime.Now.Year, SpecialMonth);
                                            if (complianceData.IDueDate > lastdate)
                                            {
                                                complianceShedule.SpecialDate = lastdate.ToString() + SpecialMonth.ToString("D2");
                                            }
                                            else
                                            {
                                                if (grdCheckList.Rows[i].Cells[9].Text != null && grdCheckList.Rows[i].Cells[9].Text.ToString().Trim() != "")
                                                {
                                                    complianceShedule.SpecialDate = Convert.ToInt32(grdCheckList.Rows[i].Cells[9].Text).ToString("D2") + SpecialMonth.ToString("D2");
                                                }
                                                else
                                                {
                                                    complianceShedule.SpecialDate = Convert.ToInt32(grdCheckList.Rows[i].Cells[11].Text).ToString("D2") + Convert.ToInt32(grdCheckList.Rows[i].Cells[10].Text).ToString("D2");
                                                }
                                            }

                                            if (specilMonth == month)
                                            {
                                                complianceShedule.SpecialDate = specialDay.ToString("D2") + month.ToString("D2");
                                                complianceData.InternalComplianceSchedules.Add(complianceShedule);
                                            }
                                            else
                                            {
                                                complianceData.InternalComplianceSchedules.Add(complianceShedule);
                                            }
                                        }
                                    }//Financial Year close
                                    else
                                    {
                                        for (int month = 1; month <= 12; month += step)
                                        {

                                            InternalComplianceSchedule complianceShedule = new InternalComplianceSchedule();
                                            complianceShedule.ForMonth = month;

                                            SpecialMonth = month;
                                            if (complianceData.IFrequency == 0 && SpecialMonth == 12)
                                            {
                                                SpecialMonth = 1;
                                            }
                                            else if (complianceData.IFrequency == 1 && SpecialMonth == 10)
                                            {
                                                SpecialMonth = 1;
                                            }
                                            else if (complianceData.IFrequency == 2 && SpecialMonth == 7)
                                            {
                                                SpecialMonth = 1;
                                            }
                                            else if (complianceData.IFrequency == 4 && SpecialMonth == 9)
                                            {
                                                SpecialMonth = 1;
                                            }
                                            else if ((complianceData.IFrequency == 3 || complianceData.IFrequency == 5 || complianceData.IFrequency == 6) && SpecialMonth == 1)
                                            {
                                                SpecialMonth = 1;
                                            }
                                            else
                                            {
                                                SpecialMonth = SpecialMonth + step;
                                            }
                                            int lastdate = DateTime.DaysInMonth(DateTime.Now.Year, SpecialMonth);
                                            if (complianceData.IDueDate > lastdate)
                                            {
                                                complianceShedule.SpecialDate = lastdate.ToString() + SpecialMonth.ToString("D2");
                                            }
                                            else
                                            {

                                                if (grdCheckList.Rows[i].Cells[9].Text != null && grdCheckList.Rows[i].Cells[9].Text.ToString().Trim() != "")
                                                {
                                                    if (Convert.ToInt32(grdCheckList.Rows[i].Cells[9].Text) < 0)
                                                    {
                                                        //string negativevalue = Convert.ToString(xlWorksheet.Cells[i, 17].Value).Trim('-');                                               
                                                        //complianceShedule.SpecialDate = Convert.ToInt32(negativevalue).ToString("D2") + SpecialMonth.ToString("D2");
                                                    }
                                                    else
                                                    {
                                                        complianceShedule.SpecialDate = Convert.ToInt32(grdCheckList.Rows[i].Cells[9].Text).ToString("D2") + SpecialMonth.ToString("D2");
                                                    }
                                                }
                                                else
                                                {
                                                    if (Convert.ToInt32(grdCheckList.Rows[i].Cells[9].Text) < 0)
                                                    {
                                                        //string negativevalue = Convert.ToString(xlWorksheet.Cells[i, 17].Value).Trim('-');                                               
                                                        //complianceShedule.SpecialDate = Convert.ToInt32(negativevalue).ToString("D2") + SpecialMonth.ToString("D2");
                                                    }
                                                    else
                                                    {
                                                        complianceShedule.SpecialDate = Convert.ToInt32(grdCheckList.Rows[i].Cells[11].Text).ToString("D2") + Convert.ToInt32(grdCheckList.Rows[i].Cells[10].Text).ToString("D2");
                                                    }
                                                }
                                            }

                                            if (specilMonth == month)
                                            {
                                                if (Convert.ToInt32(grdCheckList.Rows[i].Cells[9].Text) < 0)
                                                {
                                                }
                                                else
                                                {
                                                    complianceShedule.SpecialDate = specialDay.ToString("D2") + month.ToString("D2");
                                                    complianceData.InternalComplianceSchedules.Add(complianceShedule);
                                                }
                                            }
                                            else
                                            {
                                                if (Convert.ToInt32(grdCheckList.Rows[i].Cells[9].Text) < 0)
                                                {
                                                }
                                                else
                                                {
                                                    complianceData.InternalComplianceSchedules.Add(complianceShedule);
                                                }
                                            }
                                        }
                                    }//Calender Year Close
                                }
                                #endregion

                                #endregion
                            }
                            #region Time Based
                            if (Convert.ToString(grdCheckList.Rows[i].Cells[17].Text).Trim().Equals("Time Based Checklist"))
                            {
                                if (Convert.ToString(grdCheckList.Rows[i].Cells[3].Text).Trim().Equals("FixedGap"))
                                {
                                    complianceData.ISubComplianceType = 0;//fixed gap
                                    complianceData.IDueDate = Convert.ToInt32(grdCheckList.Rows[i].Cells[9].Text);
                                }
                                else
                                {
                                    complianceData.IDueDate = Convert.ToInt32(grdCheckList.Rows[i].Cells[9].Text);
                                    complianceData.ISubComplianceType = 1;//periodically
                                    int step = 1;
                                    complianceData.IFrequency = Convert.ToByte(Enumerations.GetEnumByName<Frequency>(Convert.ToString(grdCheckList.Rows[i].Cells[8].Text)));
                                    switch ((Frequency)Convert.ToByte(Enumerations.GetEnumByName<Frequency>(Convert.ToString(grdCheckList.Rows[i].Cells[8].Text))))
                                    {
                                        case Frequency.Monthly:
                                            step = 1;
                                            break;
                                        case Frequency.Quarterly:
                                            step = 3;
                                            break;
                                        case Frequency.FourMonthly:
                                            step = 4;
                                            break;
                                        case Frequency.HalfYearly:
                                            step = 6;
                                            break;
                                        case Frequency.Annual:
                                            step = 12;
                                            break;
                                        default:
                                            step = 12;
                                            break;
                                    }
                                    List<InternalComplianceSchedule> complianceSchedule = new List<InternalComplianceSchedule>();
                                    for (int month = 1; month <= 12; month += step)
                                    {
                                        InternalComplianceSchedule complianceShedule = new InternalComplianceSchedule();
                                        complianceShedule.ForMonth = month;

                                        int monthCopy = month;
                                        if (monthCopy == 1)
                                        {
                                            monthCopy = 12;
                                            complianceShedule.ForMonth = monthCopy;
                                        }
                                        else
                                        {
                                            monthCopy = month - 1;
                                            complianceShedule.ForMonth = monthCopy;
                                        }
                                        if (Convert.ToString(grdCheckList.Rows[i].Cells[8].Text).Trim().Equals("Annual") || Convert.ToString(grdCheckList.Rows[i].Cells[8].Text).Trim().Equals("TwoYearly") || Convert.ToString(grdCheckList.Rows[i].Cells[8].Text).Trim().Equals("SevenYearly"))
                                        {
                                            monthCopy = 3;
                                            complianceShedule.ForMonth = monthCopy;
                                        }

                                        int lastdateOfPeriodic = DateTime.DaysInMonth(DateTime.Now.Year, monthCopy);
                                        complianceShedule.SpecialDate = lastdateOfPeriodic.ToString() + monthCopy.ToString("D2");

                                        complianceData.InternalComplianceSchedules.Add(complianceShedule);
                                    }

                                }
                            }
                            #endregion reminder  
                        }
                        catch (Exception ex)
                        {

                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Server Error Occured. Compliance Schedule .";
                            LoggerMessage.InsertLog(ex, "Compliance Schedule ", MethodBase.GetCurrentMethod().Name);
                        }


                        try
                        {
                            if (Convert.ToString(grdCheckList.Rows[i].Cells[12].Text).Trim().Equals("Standard"))
                            {
                                complianceData.IReminderType = 0;//standard
                            }
                            else
                            {
                                complianceData.IReminderType = 1;//custom
                                complianceData.IReminderBefore = Convert.ToInt32(grdCheckList.Rows[i].Cells[13].Text);
                                complianceData.IReminderGap = Convert.ToInt32(grdCheckList.Rows[i].Cells[14].Text);
                            }
                        }
                        catch (Exception ex)
                        {

                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Server Error Occured. IReminderType .";
                            LoggerMessage.InsertLog(ex, "IReminderType ", MethodBase.GetCurrentMethod().Name);
                        }

                        try
                        {
                            if (Convert.ToString(grdCheckList.Rows[i].Cells[17].Text).Trim().Equals("Function based Checklist"))
                            {
                                complianceData.CheckListTypeID = 1;
                            }
                            else if (Convert.ToString(grdCheckList.Rows[i].Cells[17].Text).Trim().Equals("Time Based Checklist"))
                            {
                                complianceData.CheckListTypeID = 2;
                            }
                            else
                            {
                                complianceData.CheckListTypeID = 0;
                            }
                        }
                        catch (Exception ex)
                        {

                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Server Error Occured. CheckListTypeID .";
                            LoggerMessage.InsertLog(ex, "CheckListTypeID ", MethodBase.GetCurrentMethod().Name);
                        }
                       

                       
                        complianceData.IComplianceOccurrence = 1;
                        

                        if (Convert.ToString(grdCheckList.Rows[i].Cells[17].Text).Trim().Equals("One Time based Checklist"))
                        {
                            complianceData.IOneTimeDate = Convert.ToDateTime(grdCheckList.Rows[i].Cells[16].Text);
                            complianceData.IComplianceOccurrence = 0;
                        }


                        complianceData.IDetailedDescription = Convert.ToString(grdCheckList.Rows[i].Cells[19].Text);

                        complianceData.IShortForm = Convert.ToString(grdCheckList.Rows[i].Cells[20].Text);

                        try
                        {
                            string schtype = Convert.ToString(grdCheckList.Rows[i].Cells[21].Text);
                            if (schtype == "Before")
                            {
                                complianceData.ScheduleType = 2;
                            }
                            else if (schtype == "In Between")
                            {
                                complianceData.ScheduleType = 1;
                            }
                            else if (schtype == "After")
                            {
                                complianceData.ScheduleType = 0;
                            }
                            else
                            {
                                complianceData.ScheduleType = null;
                            }
                        }
                        catch (Exception ex)
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Server Error Occured. ScheduleType.";
                            LoggerMessage.InsertLog(ex, "ScheduleType  ", MethodBase.GetCurrentMethod().Name);
                        }


                        try
                        {
                            string IsEvent = Convert.ToString(grdCheckList.Rows[i].Cells[22].Text).Trim().ToUpper();
                            if (IsEvent == "YES")
                            {
                                complianceData.EventFlag = true;
                            }
                            else
                            {
                                complianceData.EventFlag = null;
                            }
                        }
                        catch (Exception ex)
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Server Error Occured. EventFlag.";
                            LoggerMessage.InsertLog(ex, "EventFlag  ", MethodBase.GetCurrentMethod().Name);
                        }

                        try
                        {
                            if (complianceData.IFrequency == 8)
                            {
                                string weekDay = Convert.ToString(grdCheckList.Rows[i].Cells[23].Text).Trim().ToUpper();
                                byte? WeekDay = 1;
                                switch (weekDay)
                                {
                                    case "SUNDAY":
                                        WeekDay = 0;
                                        break;
                                    case "MONDAY":
                                        WeekDay = 1;
                                        break;
                                    case "TUESDAY":
                                        WeekDay = 2;
                                        break;
                                    case "WEDNESDAY":
                                        WeekDay = 3;
                                        break;
                                    case "THURSDAY":
                                        WeekDay = 4;
                                        break;
                                    case "FRIDAY":
                                        WeekDay = 5;
                                        break;
                                    case "SATURDAY":
                                        WeekDay = 6;
                                        break;
                                }

                                complianceData.DueWeekDay = WeekDay;
                            }
                        }
                        catch (Exception ex)
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Server Error Occured. EventFlag.";
                            LoggerMessage.InsertLog(ex, "DueWeekDay  ", MethodBase.GetCurrentMethod().Name);
                        }
                        complianceData.CreatedOn = DateTime.Now;
                        complianceData.UpdatedOn = DateTime.Now;
                        complianceData.IsDeleted = false;
                        complianceData.CreatedBy = AuthenticationHelper.UserID;
                        complianceData.UpdatedBy = AuthenticationHelper.UserID;
                                      

                    }
                    complianceList.Add(complianceData);
                }
                Business.ComplianceManagement.CreateInternal(complianceList, customerID);
                CheckListsuucessSave = true;
            }
            catch (Exception ex)
            {
                CheckListsuucessSave = false;
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void ValidateCheckListData(ExcelPackage xlWorkbook)
        {
            try
            {
                ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["InternalChecklist"];
                int xlrow = xlWorksheet.Dimension.End.Row;
                int filledrow = 0;
                for (int i = 1; i <= xlrow; i++)
                {
                    if ((xlWorksheet.Cells[i, 1].Value) != null)
                    {
                        filledrow = filledrow + 1;
                    }
                }
                createDataTable();
                for (int i = 2; i <= filledrow; i++)
                {
                    if (((xlWorksheet.Cells[i, 6].Value) == null) && (Convert.ToString(xlWorksheet.Cells[i, 6].Value) == string.Empty))
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + "  <br />  Compliance ShortDescription can not blank";
                        return;
                    }

                    DataRow drCompliance = dtCompliance.NewRow();
                    com.VirtuosoITech.ComplianceManagement.Business.Data.InternalCompliance complianceData = new com.VirtuosoITech.ComplianceManagement.Business.Data.InternalCompliance();
                    if (((xlWorksheet.Cells[i, 1].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 1].Value) != string.Empty))
                    {
                        string complianceTypeValue = Convert.ToString(xlWorksheet.Cells[i, 1].Value);
                        int data = complianceTypeValue.Length;
                        if (data >= 100)
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 5].Value) + " <br />  Max length of category type is 100 characters";
                            return;
                        }
                        else
                        {
                            drCompliance["IComplianceTypeID"] = Convert.ToString(xlWorksheet.Cells[i, 1].Value);
                        }
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 6].Value) + " <br />  Compliance Type can not blank";
                        return;
                    }
                    if (((xlWorksheet.Cells[i, 2].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 2].Value) != string.Empty))
                    {

                        string categoryTypeValue = Convert.ToString(xlWorksheet.Cells[i, 1].Value);
                        int data = categoryTypeValue.Length;
                        if (data >= 100)
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 5].Value) + " <br />  Max length of Category Type is 100 characters";
                            return;
                        }
                        else
                        {
                            drCompliance["IComplianceCategoryID"] = Convert.ToString(xlWorksheet.Cells[i, 2].Value);

                        }
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 6].Value) + " <br />  Compliance Category can not blank";
                        return;
                    }

                    if ((Convert.ToString(xlWorksheet.Cells[i, 3].Value) == "Checklist"))
                    {
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 5].Value) + " <br />  Please enter correct Compliance Type";
                        return;
                    }

                    if ((Convert.ToString(xlWorksheet.Cells[i, 4].Value) == string.Empty) || (Convert.ToString(xlWorksheet.Cells[i, 4].Value) == "FixedGap") || (Convert.ToString(xlWorksheet.Cells[i, 4].Value) == "PeriodicallyBased"))
                    {
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 5].Value) + " <br />  Please enter correct Timely based compliance";
                        return;
                    }

                    if ((Convert.ToString(xlWorksheet.Cells[i, 5].Value) == "Function based Checklist") || (Convert.ToString(xlWorksheet.Cells[i, 5].Value) == "One Time based Checklist") || (Convert.ToString(xlWorksheet.Cells[i, 5].Value) == "Time Based Checklist"))
                    {
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 5].Value) + " <br />  Please enter correct CheckListType";
                        return;
                    }

                    if ((Convert.ToString(xlWorksheet.Cells[i, 11].Value) == string.Empty) || (Convert.ToString(xlWorksheet.Cells[i, 11].Value) == "Annual") || (Convert.ToString(xlWorksheet.Cells[i, 11].Value) == "HalfYearly")
                      || (Convert.ToString(xlWorksheet.Cells[i, 11].Value) == "Quarterly") || (Convert.ToString(xlWorksheet.Cells[i, 11].Value) == "FourMonthly") || (Convert.ToString(xlWorksheet.Cells[i, 11].Value) == "TwoYearly")
                      || (Convert.ToString(xlWorksheet.Cells[i, 11].Value) == "SevenYearly") || (Convert.ToString(xlWorksheet.Cells[i, 11].Value) == "Monthly")
                      || (Convert.ToString(xlWorksheet.Cells[i, 11].Value) == "Daily") || (Convert.ToString(xlWorksheet.Cells[i, 11].Value) == "Weekly"))
                    {
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 5].Value) + " <br />  Please enter correct Frequency";
                        return;
                    }

                    if ((Convert.ToString(xlWorksheet.Cells[i, 15].Value) == "Standard") || (Convert.ToString(xlWorksheet.Cells[i, 15].Value) == "Custom"))
                    {
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 5].Value) + " <br />  Please enter correct Reminder type";
                        return;
                    }

                    if ((Convert.ToString(xlWorksheet.Cells[i, 18].Value) == "High") || (Convert.ToString(xlWorksheet.Cells[i, 18].Value) == "Medium") || (Convert.ToString(xlWorksheet.Cells[i, 18].Value) == "Low")|| (Convert.ToString(xlWorksheet.Cells[i, 18].Value) == "Critical"))
                    {
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 5].Value) + " <br />  Please enter correct Risk Type";
                        return;
                    }




                    complianceData.CustomerID = customerID;// Convert.ToInt32(ddlchecklistCustomer.SelectedValue);
                    drCompliance["CustomerID"] = customerID;// Convert.ToInt32(ddlchecklistCustomer.SelectedValue);

                    if (((xlWorksheet.Cells[i, 3].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 3].Value) != string.Empty))
                    {
                        drCompliance["IComplianceType"] = Convert.ToString(xlWorksheet.Cells[i, 3].Value);
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 6].Value) + " <br />  Compliance Type can not blank(Checklist)";
                        return;
                    }

                    drCompliance["IShortDescription"] = Convert.ToString(xlWorksheet.Cells[i, 6].Value);
                    complianceData.IShortDescription = Convert.ToString(xlWorksheet.Cells[i, 6].Value);

                    drCompliance["IShortForm"] = Convert.ToString(xlWorksheet.Cells[i, 21].Value);
                    complianceData.IShortForm = Convert.ToString(xlWorksheet.Cells[i, 21].Value);

                    drCompliance["ScheduleType"] = Convert.ToString(xlWorksheet.Cells[i, 22].Value);
                    complianceData.ScheduleType = Convert.ToInt32(xlWorksheet.Cells[i, 22].Value);


                    if (((xlWorksheet.Cells[i, 8].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 8].Value) != string.Empty))
                    {
                        drCompliance["IUploadDocument"] = Convert.ToString(xlWorksheet.Cells[i, 8].Value);
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 6].Value) + " <br />  Upload Document can not blank(Yes,No)";
                        return;
                    }

                    drCompliance["IRequiredFrom"] = Convert.ToString(xlWorksheet.Cells[i, 9].Value);

                    if (((xlWorksheet.Cells[i, 18].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 18].Value) != string.Empty))
                    {

                        drCompliance["IRiskType"] = Convert.ToString(xlWorksheet.Cells[i, 18].Value);
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 6].Value) + " <br />  Risk Type can not blank(High,Low,Medium,Critical)";
                        return;
                    }

                    if (Convert.ToString(xlWorksheet.Cells[i, 5].Value).Trim().Equals("Function based Checklist"))
                    {
                        if (((xlWorksheet.Cells[i, 11].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 11].Value) != string.Empty))
                        {

                            drCompliance["IFrequency"] = Convert.ToString(xlWorksheet.Cells[i, 11].Value);
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 6].Value) + " <br />  Frequency can not blank";
                            return;
                        }

                        if (((xlWorksheet.Cells[i, 12].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 12].Value) != string.Empty))
                        {

                            drCompliance["IDueDate"] = Convert.ToString(xlWorksheet.Cells[i, 12].Value);
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 6].Value) + " <br />  DueDate can not blank";
                            return;
                        }

                        //if ((xlWorksheet.Cells[i, 11].Value != null && xlWorksheet.Cells[i, 11].Value != null) && (xlWorksheet.Cells[i, 12].Value.ToString().Trim() != "" && xlWorksheet.Cells[i, 12].Value.ToString().Trim() != ""))
                        //{

                        drCompliance["specilMonth"] = Convert.ToString(xlWorksheet.Cells[i, 13].Value);
                        drCompliance["specialDay"] = Convert.ToString(xlWorksheet.Cells[i, 14].Value);
                        //}
                        //else
                        //{
                        //    cvDuplicateEntry.IsValid = false;
                        //    cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 5].Value) + " <br />  SpecilMonth and SpecialDay can not blank";
                        //    return;
                        //}

                        if (((xlWorksheet.Cells[i, 19].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 19].Value) != string.Empty))
                        {
                            drCompliance["calflag"] = Convert.ToString(xlWorksheet.Cells[i, 19].Value);
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 6].Value) + " <br />  calflag can not blank";
                            return;
                        }
                    }

                    if (Convert.ToString(xlWorksheet.Cells[i, 5].Value).Trim().Equals("Time Based Checklist"))
                    {
                        if (Convert.ToString(xlWorksheet.Cells[i, 4].Value).Trim().Equals("FixedGap"))
                        {
                            if (((xlWorksheet.Cells[i, 4].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 4].Value) != string.Empty))
                            {
                                drCompliance["ISubComplianceType"] = Convert.ToString(xlWorksheet.Cells[i, 4].Value);
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 5].Value) + " <br />  SubComplianceType can not blank";
                                return;
                            }

                            if (((xlWorksheet.Cells[i, 4].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 4].Value) != string.Empty))
                            {
                                drCompliance["IDueDate"] = Convert.ToString(xlWorksheet.Cells[i, 10].Value);
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 5].Value) + " <br />  DueDate can not blank";
                                return;
                            }
                        }
                        else
                        {
                            if (((xlWorksheet.Cells[i, 4].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 4].Value) != string.Empty))
                            {
                                drCompliance["ISubComplianceType"] = Convert.ToString(xlWorksheet.Cells[i, 4].Value);
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 5].Value) + " <br />  SubComplianceType can not blank";
                                return;
                            }

                            if (((xlWorksheet.Cells[i, 19].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 19].Value) != string.Empty))
                            {
                                drCompliance["calflag"] = Convert.ToString(xlWorksheet.Cells[i, 19].Value);
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 6].Value) + " <br />  calflag can not blank";
                                return;
                            }

                            if (((xlWorksheet.Cells[i, 11].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 11].Value) != string.Empty))
                            {

                                drCompliance["IFrequency"] = Convert.ToString(xlWorksheet.Cells[i, 11].Value);
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 6].Value) + " <br />  Frequency can not blank";
                                return;
                            }
                        }
                    }

                    if (((xlWorksheet.Cells[i, 15].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 15].Value) != string.Empty))
                    {
                        drCompliance["IReminderType"] = Convert.ToString(xlWorksheet.Cells[i, 15].Value);
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 6].Value) + " <br />  ReminderType can not blank(Standard,Custom)";
                        return;
                    }

                    if (Convert.ToString(xlWorksheet.Cells[i, 15].Value).Trim().Equals("Standard"))
                    {
                    }
                    else
                    {

                        if (((xlWorksheet.Cells[i, 16].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 16].Value) != string.Empty))
                        {
                            drCompliance["IReminderBefore"] = Convert.ToString(xlWorksheet.Cells[i, 16].Value);
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 6].Value) + " <br />  ReminderBefore can not blank";
                            return;
                        }
                        if (((xlWorksheet.Cells[i, 17].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 17].Value) != string.Empty))
                        {
                            drCompliance["IReminderGap"] = Convert.ToString(xlWorksheet.Cells[i, 17].Value);
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 6].Value) + " <br />  ReminderGap can not blank";
                            return;
                        }
                    }


                    if (((xlWorksheet.Cells[i, 5].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 5].Value) != string.Empty))
                    {
                        drCompliance["CheckListTypeID"] = Convert.ToString(xlWorksheet.Cells[i, 5].Value);
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 6].Value) + " <br />  CheckListType can not blank";
                        return;
                    }

                    if (Convert.ToString(xlWorksheet.Cells[i, 5].Value).Trim().Equals("One Time based Checklist"))
                    {
                        if (((xlWorksheet.Cells[i, 7].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 7].Value) != string.Empty))
                        {
                            drCompliance["IOneTimeDate"] = Convert.ToString(xlWorksheet.Cells[i, 7].Text);
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 6].Value) + " <br />  OneTime Date can not blank";
                            return;
                        }
                    }

                    if (((xlWorksheet.Cells[i, 20].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 20].Value) != string.Empty))
                    {
                        drCompliance["IDetailedDescription"] = Convert.ToString(xlWorksheet.Cells[i, 20].Value);
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 6].Value) + " <br />  Detailed Description can not blank";
                        return;
                    }

                    if (((xlWorksheet.Cells[i, 21].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 21].Value) != string.Empty))
                    {
                        drCompliance["IShortForm"] = Convert.ToString(xlWorksheet.Cells[i, 21].Value);
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 6].Value) + " <br />  Short Form can not blank";
                        return;
                    }
                    if (((xlWorksheet.Cells[i, 22].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 22].Value) != string.Empty))
                    {
                        drCompliance["ScheduleType"] = Convert.ToString(xlWorksheet.Cells[i, 22].Value);
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 6].Value) + " <br />  Short Form can not blank";
                        return;
                    }

                    complianceData.CreatedOn = DateTime.Now;
                    complianceData.IsDeleted = false;

                    dtCompliance.Rows.Add(drCompliance);

                    grdCheckList.DataSource = dtCompliance;
                    grdCheckList.DataBind();
                }
                CheckListsuucess = true;
            }
            catch (Exception ex)
            {
                CheckListsuucess = false;
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected bool CheckDate(String date)
        {
            try
            {
                DateTime dt = DateTime.Parse(date);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public static DateTime? CleanDateField(string DateField)
        {
            // Convert the text to DateTime and return the value or null
            DateTime? CleanDate = new DateTime();
            int intDate;
            bool DateIsInt = int.TryParse(DateField, out intDate);
            if (DateIsInt)
            {
                // If this is a serial date, convert it
                CleanDate = DateTime.FromOADate(intDate);
            }
            else if (DateField.Length != 0 && DateField != "1/1/0001 12:00:00 AM" &&
                DateField != "1/1/1753 12:00:00 AM")
            {
                // Convert from a General format
                CleanDate = (Convert.ToDateTime(DateField));
            }
            else
            {
                // Date is blank
                CleanDate = null;
            }
            return CleanDate;
        }
        public DateTime GetDate(string date)
        {
            string date1 = "";
            if (date.Contains("/"))
            {
                date1 = date.Trim().Substring(date.IndexOf("/") + 4, 4) + "-" + date.Substring(date.IndexOf("/") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains("-"))
            {
                date1 = date.Substring(date.IndexOf("-") + 4, 4) + "-" + date.Substring(date.IndexOf("-") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains(" "))
            {
                date1 = date.Substring(date.IndexOf(" ") + 4, 4) + "-" + date.Substring(date.IndexOf(" ") + 1, 2) + "-" + date.Substring(0, 2);
            }
            return Convert.ToDateTime(date1);
        }
        private void ValidateCheckListDataNew(ExcelPackage xlWorkbook)
        {
            try
            {
                long cid = -1;
                if (AuthenticationHelper.Role.Contains("IMPT"))
                {
                    if (!string.IsNullOrEmpty(ddlchecklistCustomer.SelectedValue))
                        cid = Convert.ToInt64(ddlchecklistCustomer.SelectedValue);
                }
                else if (AuthenticationHelper.Role.Contains("CADMN"))
                {
                    cid = Convert.ToInt64(AuthenticationHelper.CustomerID);
                }
                else
                {
                    cid = Convert.ToInt64(AuthenticationHelper.CustomerID);
                }
                if (cid != -1)
                {
                    string ComplianceTypevalue = string.Empty;
                    string CategoryTypevalue = string.Empty;
                    List<string> errorMessage = new List<string>();
                    ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["InternalChecklist"];
                    if (xlWorksheet != null)
                    {
                        int xlrow2 = xlWorksheet.Dimension.End.Row;

                        #region Validation                           
                        for (int i = 2; i <= xlrow2; i++)
                        {
                            #region 1 Compliance Type Name
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 1].Text.ToString().Trim()))
                            {
                                try
                                {
                                    ComplianceTypevalue = Convert.ToString(xlWorksheet.Cells[i, 1].Text);

                                    int data = Convert.ToInt32(ComplianceTypevalue.Length);
                                    if (data >= 100)
                                    {
                                        errorMessage.Add("Please Check Compliance Type Name At -" + i + "Max length of Compliance Type Name is 100 characters");
                                    }
                                }
                                catch (Exception ex)
                                {
                                    errorMessage.Add(" Exception At  - " + i + " Compliance Type Name should not blank");
                                }
                            }
                            else
                            {
                                errorMessage.Add(" Record No - At  - " + i + " should not blank");
                            }
                            #endregion

                            #region 2  Compliance Category Name
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 2].Text.ToString().Trim()))
                            {
                                try
                                {
                                    CategoryTypevalue = Convert.ToString(xlWorksheet.Cells[i, 2].Text);

                                    int data = Convert.ToInt32(CategoryTypevalue.Length);
                                    if (data >= 100)
                                    {
                                        errorMessage.Add("Please Check Compliance Category Name At -" + i + "Max length of Compliance Category Name is 100 characters");
                                    }
                                }
                                catch (Exception ex)
                                {
                                    errorMessage.Add(" Exception At  - " + i + " Compliance Category Name should not blank");
                                }
                            }
                            else
                            {
                                errorMessage.Add(" Record No - At  - " + i + " should not blank");
                            }
                            #endregion

                            #region 3 Compliance Type
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 3].Text.ToString().Trim()))
                            {
                                if (!(Convert.ToString(xlWorksheet.Cells[i, 3].Value) == "Checklist"))
                                {
                                    errorMessage.Add("Record No -" + i + "Please enter correct Compliance Type");
                                }
                            }
                            else
                            {
                                errorMessage.Add("Record No -" + i + "Compliance Type should not blank");
                            }
                            #endregion

                            #region 4 Timely based compliance
                            if ((Convert.ToString(xlWorksheet.Cells[i, 4].Value) == string.Empty) || (Convert.ToString(xlWorksheet.Cells[i, 4].Value) == "FixedGap") || (Convert.ToString(xlWorksheet.Cells[i, 4].Value) == "PeriodicallyBased"))
                            {

                            }
                            else
                            {
                                errorMessage.Add("Record No -" + i + "  Please enter correct Timely based compliance");
                            }
                            #endregion

                            #region 5 CheckListType
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 5].Text.ToString().Trim()))
                            {
                                if ((Convert.ToString(xlWorksheet.Cells[i, 5].Value) == "Function based Checklist") || (Convert.ToString(xlWorksheet.Cells[i, 5].Value) == "One Time based Checklist") || (Convert.ToString(xlWorksheet.Cells[i, 5].Value) == "Time Based Checklist"))
                                {
                                }
                                else
                                {
                                    errorMessage.Add("Record No -" + i + " Please enter correct CheckListType");
                                }
                            }
                            else
                            {
                                errorMessage.Add("Record No -" + i + "  CheckListType should not blank");
                            }
                            #endregion

                            #region 6 Short Description
                            if (String.IsNullOrEmpty(xlWorksheet.Cells[i, 6].Text.ToString().Trim()))
                            {
                                errorMessage.Add("Record No -" + i + "  Compliance ShortDescription should not blank");
                            }
                            #endregion

                            #region 7 OneTime Date
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 5].Text.ToString().Trim()))
                            {
                                if ((Convert.ToString(xlWorksheet.Cells[i, 5].Value) == "One Time based Checklist"))
                                {
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 7].Text.ToString().Trim()))
                                    {


                                        try
                                        {
                                            bool check = CheckDate(Convert.ToString(xlWorksheet.Cells[i, 7].Text).Trim());
                                            if (!check)
                                            {                                               
                                                errorMessage.Add("Record No -" + i + "  Please Check OneTime Date or Date should be in DD-MMM-YYYY Format");
                                            }                                           
                                        }
                                        catch (Exception ex)
                                        {
                                        }

                                    }
                                    else
                                    {
                                        errorMessage.Add("Record No -" + i + "   OneTime Date should not blank");
                                    }
                                    
                                }
                            }
                            #endregion

                            #region 8 Upload Document
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 8].Text.ToString().Trim()))
                            {
                                List<string> isud = new List<string>();
                                isud.Add("YES");
                                isud.Add("NO");
                                var isupdoc = Convert.ToString(xlWorksheet.Cells[i, 8].Value).Trim().ToUpper();

                                if (!isud.Contains(isupdoc))
                                {
                                    errorMessage.Add("Record No -" + i + " Upload Document should be (Yes, No)");
                                }                               
                            }
                            else
                            {
                                errorMessage.Add("Record No -" + i + " Upload Document should not blank. Instead Use (Yes , No)");
                            }

                            #endregion

                            #region 9 Required Forms                             
                            #endregion

                            #region 10 Compliance due (in days)
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 5].Text.ToString().Trim()))
                            {
                                if (Convert.ToString(xlWorksheet.Cells[i, 5].Value).Trim().Equals("Time Based Checklist"))
                                {
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 4].Text.ToString().Trim()))
                                    {
                                        if (Convert.ToString(xlWorksheet.Cells[i, 4].Value).Trim().Equals("FixedGap"))
                                        {
                                            if (String.IsNullOrEmpty(xlWorksheet.Cells[i, 10].Text.ToString().Trim()))
                                            {
                                                errorMessage.Add("Record No -" + i + "   Due Date can not blank");
                                            }
                                        }                                       
                                    }
                                    else
                                    {
                                        errorMessage.Add("Record No -" + i + " Timely based compliance should not blank. Instead Use (FixedGap , PeriodicallyBased)" +
                                            "or Checklist type is Time Based Checklist");
                                    }
                                    
                                }
                            }
                            #endregion

                            #region 11 Frequency
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 5].Text.ToString().Trim()))
                            {
                                if (Convert.ToString(xlWorksheet.Cells[i, 5].Value).Trim().Equals("Function based Checklist"))
                                {
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 11].Text.ToString().Trim()))
                                    {
                                        List<string> fr = new List<string>();
                                        fr.Add("ANNUAL");
                                        fr.Add("HALFYEARLY");
                                        fr.Add("QUARTERLY");
                                        fr.Add("FOURMONTHLY");
                                        fr.Add("TWOYEARLY");
                                        fr.Add("SEVENYEARLY");
                                        fr.Add("MONTHLY");
                                        fr.Add("DAILY");
                                        fr.Add("WEEKLY");
                                        var freq = Convert.ToString(xlWorksheet.Cells[i, 11].Value).Trim().ToUpper();
                                        if (!fr.Contains(freq))
                                        {
                                            errorMessage.Add("Record No -" + i + "  Please enter correct Frequency");
                                        }
                                    }
                                    else
                                    {
                                        errorMessage.Add("Record No -" + i + " Frequency should not blank.");
                                    }
                                }
                                else if (Convert.ToString(xlWorksheet.Cells[i, 5].Value).Trim().Equals("Time Based Checklist"))
                                {
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 4].Text.ToString().Trim()))
                                    {
                                        if (Convert.ToString(xlWorksheet.Cells[i, 4].Value).Trim().Equals("PeriodicallyBased"))
                                        {
                                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 11].Text.ToString().Trim()))
                                            {
                                                List<string> fr = new List<string>();
                                                fr.Add("ANNUAL");
                                                fr.Add("HALFYEARLY");
                                                fr.Add("QUARTERLY");
                                                fr.Add("FOURMONTHLY");
                                                fr.Add("TWOYEARLY");
                                                fr.Add("SEVENYEARLY");
                                                fr.Add("MONTHLY");
                                                fr.Add("DAILY");
                                                fr.Add("WEEKLY");
                                                var freq = Convert.ToString(xlWorksheet.Cells[i, 11].Value).Trim().ToUpper();
                                                if (!fr.Contains(freq))
                                                {
                                                    errorMessage.Add("Record No -" + i + "  Please enter correct Frequency");
                                                }
                                            }
                                            else
                                            {
                                                errorMessage.Add("Record No -" + i + " Frequency should not blank.");
                                            }
                                        }
                                    }
                                    else
                                    {
                                        errorMessage.Add("Record No -" + i + " Timely based compliance should not blank. Instead Use (FixedGap , PeriodicallyBased)" +
                                            "or Checklist type is Time Based Checklist");
                                    }
                                }
                                else if ((Convert.ToString(xlWorksheet.Cells[i, 5].Value) == "One Time based Checklist"))
                                {
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 11].Text.ToString().Trim()))
                                    {
                                        errorMessage.Add("Record No -" + i + "  Please Remove Frequency or check CheckListType ");
                                    }
                                }
                            }
                            #endregion

                            #region 12 Due Date

                            try
                            {
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 5].Text.ToString().Trim()))
                                {
                                    if (Convert.ToString(xlWorksheet.Cells[i, 5].Value).Trim().Equals("Function based Checklist"))
                                    {
                                        var freq = Convert.ToString(xlWorksheet.Cells[i, 11].Value).Trim().ToUpper();
                                        if (freq != "DAILY" && freq != "WEEKLY")
                                        {
                                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 12].Text.ToString().Trim()))
                                            {
                                                bool checkNumber = IsValidNumber(Convert.ToString(xlWorksheet.Cells[i, 12].Value));
                                                if (!checkNumber)
                                                {
                                                    errorMessage.Add("Record No -" + i + "  Please Enter only numbers in Due Date");
                                                }
                                                else
                                                {
                                                    var val = Convert.ToInt32(xlWorksheet.Cells[i, 12].Value);

                                                    if (val > 31)
                                                    {
                                                        errorMessage.Add("Record No -" + i + "  Please correct Due Date");
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                errorMessage.Add("Record No -" + i + " Due Date should not blank.");
                                            }
                                        }
                                       
                                    }
                                    else if (Convert.ToString(xlWorksheet.Cells[i, 5].Value).Trim().Equals("Time Based Checklist"))
                                    {
                                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 4].Text.ToString().Trim()))
                                        {
                                            if (Convert.ToString(xlWorksheet.Cells[i, 4].Value).Trim().Equals("PeriodicallyBased"))
                                            {
                                                var freq = Convert.ToString(xlWorksheet.Cells[i, 11].Value).Trim().ToUpper();
                                                if (freq != "DAILY" && freq != "WEEKLY")
                                                {
                                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 12].Text.ToString().Trim()))
                                                    {
                                                        bool checkNumber = IsValidNumber(Convert.ToString(xlWorksheet.Cells[i, 12].Value));
                                                        if (!checkNumber)
                                                        {
                                                            errorMessage.Add("Record No -" + i + "  Please Enter only numbers in Due Date");
                                                        }
                                                        else
                                                        {
                                                            var val = Convert.ToInt32(xlWorksheet.Cells[i, 12].Value);

                                                            if (val > 31)
                                                            {
                                                                errorMessage.Add("Record No -" + i + "  Please correct Due Date");
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        errorMessage.Add("Record No -" + i + " Due Date should not blank.");
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            errorMessage.Add("Record No -" + i + " Timely based compliance should not blank. Instead Use (FixedGap , PeriodicallyBased)" +
                                                "or Checklist type is Time Based Checklist");
                                        }
                                    }
                                    else if ((Convert.ToString(xlWorksheet.Cells[i, 5].Value) == "One Time based Checklist"))
                                    {
                                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 12].Text.ToString().Trim()))
                                        {
                                            errorMessage.Add("Record No -" + i + "  Please Remove Due Date or check CheckListType ");
                                        }
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Due Date .";
                                LoggerMessage.InsertLog(ex, "Due Date ", MethodBase.GetCurrentMethod().Name);
                            }
                                                       
                            #endregion

                            #region 13 14  Special Due Date - For Month / Special Due Date -Date                               
                            #endregion

                            #region 15  Reminder type  
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 15].Text.ToString().Trim()))
                            {
                                List<string> isud = new List<string>();
                                isud.Add("Custom");
                                isud.Add("Standard");
                                var isupdoc = Convert.ToString(xlWorksheet.Cells[i, 15].Value).Trim();

                                if (!isud.Contains(isupdoc))
                                {
                                    errorMessage.Add("Record No -" + i + " Reminder Type should be (Custom, Standard)");
                                }
                            }
                            else
                            {
                                errorMessage.Add("Record No -" + i + "  Reminder Type should not blank it should be (Standard,Custom)");
                            }
                            #endregion

                            #region 16 Before (in days)
                            if (Convert.ToString(xlWorksheet.Cells[i, 15].Value).Trim().Equals("Custom"))
                            {
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 16].Text.ToString().Trim()))
                                {
                                    bool checkNumber = IsValidNumber(Convert.ToString(xlWorksheet.Cells[i, 16].Value));
                                    if (!checkNumber)
                                    {
                                        errorMessage.Add("Record No -" + i + " Please Enter only numbers in Before (in days)");
                                    }
                                }
                                else
                                {
                                    errorMessage.Add("Record No -" + i + "  Before (in days) should not blank");
                                }
                            }
                            #endregion

                            #region 17 Gap (in days)
                            if (Convert.ToString(xlWorksheet.Cells[i, 15].Value).Trim().Equals("Custom"))
                            {
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 17].Text.ToString().Trim()))
                                {
                                    bool checkNumber = IsValidNumber(Convert.ToString(xlWorksheet.Cells[i, 17].Value));
                                    if (!checkNumber)
                                    {
                                        errorMessage.Add("Record No -" + i + " Please Enter only numbers in Gap (in days)");
                                    }
                                }
                                else
                                {
                                    errorMessage.Add("Record No -" + i + "  Gap (in days) should not blank");
                                }
                            }
                            #endregion

                            #region 18  Risk Type
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 18].Text.ToString().Trim()))
                            {
                                List<string> rt = new List<string>();
                                rt.Add("HIGH");
                                rt.Add("MEDIUM");
                                rt.Add("LOW");
                                rt.Add("CRITICAL");
                                var risk = Convert.ToString(xlWorksheet.Cells[i, 18].Value).Trim().ToUpper();
                                if (!rt.Contains(risk))
                                {
                                    errorMessage.Add("Record No -" + i + "  Risk Type should be (High, Low, Medium,Critical)");
                                }
                            }
                            else
                            {
                                errorMessage.Add("Record No -" + i + " Risk Type should not blank");
                            }
                            #endregion

                            #region 19 CalFlag                        
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 5].Text.ToString().Trim()))
                            {
                                if (Convert.ToString(xlWorksheet.Cells[i, 5].Value).Trim().Equals("Function based Checklist"))
                                {
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 19].Text.ToString().Trim()))
                                    {
                                        List<string> cf = new List<string>();
                                        cf.Add("Y");
                                        cf.Add("N");
                                        var stype = Convert.ToString(xlWorksheet.Cells[i, 19].Value).Trim().ToUpper();
                                        if (!cf.Contains(stype))
                                        {
                                            errorMessage.Add("Record No -" + i + " calflag should be (Y, N)");
                                        }
                                    }
                                    else
                                    {
                                        errorMessage.Add("Record No -" + i + " calflag should not blank");
                                    }                                                                     
                                }
                                else if (Convert.ToString(xlWorksheet.Cells[i, 5].Value).Trim().Equals("Time Based Checklist"))
                                {
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 4].Text.ToString().Trim()))
                                    {
                                        if (Convert.ToString(xlWorksheet.Cells[i, 4].Value).Trim().Equals("PeriodicallyBased"))
                                        {
                                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 19].Text.ToString().Trim()))
                                            {
                                                List<string> cf = new List<string>();
                                                cf.Add("Y");
                                                cf.Add("N");
                                                var stype = Convert.ToString(xlWorksheet.Cells[i, 19].Value).Trim().ToUpper();
                                                if (!cf.Contains(stype))
                                                {
                                                    errorMessage.Add("Record No -" + i + " calflag should be (Y, N)");
                                                }
                                            }
                                            else
                                            {
                                                errorMessage.Add("Record No -" + i + " calflag should not blank");
                                            }                                           
                                        }
                                    }
                                    else
                                    {
                                        errorMessage.Add("Record No -" + i + " Timely based compliance should not blank. Instead Use (FixedGap , PeriodicallyBased)" +
                                            "or Checklist type is Time Based Checklist");
                                    }
                                }
                                else if ((Convert.ToString(xlWorksheet.Cells[i, 5].Value) == "One Time based Checklist"))
                                {
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 19].Text.ToString().Trim()))
                                    {
                                        errorMessage.Add("Record No -" + i + "  Please Remove calflag or check CheckListType ");
                                    }
                                }
                            }
                            #endregion                            

                            #region 20 IDetailed Description
                            if (String.IsNullOrEmpty(xlWorksheet.Cells[i, 20].Text.ToString().Trim()))
                            {
                                errorMessage.Add("Record No -" + i + "  Detailed Description should not blank");
                            }
                            #endregion

                            #region 21 Short Form
                            if (String.IsNullOrEmpty(xlWorksheet.Cells[i, 21].Text.ToString().Trim()))
                            {
                                errorMessage.Add("Record No -" + i + " Short Form should not blank");
                            }
                            #endregion

                            #region 22 Schedule Type
                            if (!(Convert.ToString(xlWorksheet.Cells[i, 5].Value) == "One Time based Checklist"))
                            {
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 22].Text.ToString().Trim()))
                                {
                                    List<string> st = new List<string>();
                                    st.Add("AFTER");
                                    st.Add("IN BETWEEN");
                                    st.Add("BEFORE");
                                    var stype = Convert.ToString(xlWorksheet.Cells[i, 22].Value).Trim().ToUpper();
                                    if (!st.Contains(stype))
                                    {
                                        errorMessage.Add("Record No -" + i + "  Schedule Type should be (After, In Between, Before)");
                                    }
                                }
                                else
                                {
                                    errorMessage.Add("Record No -" + i + " Schedule Type should not blank");
                                }
                            }
                            #endregion

                            #region 23 Is EventBased
                            if (!(Convert.ToString(xlWorksheet.Cells[i, 5].Value) == "One Time based Checklist"))
                            {
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 23].Text.ToString().Trim()))
                                {
                                    List<string> SCT = new List<string>();
                                    SCT.Add("YES");
                                    SCT.Add("NO");
                                    var stype = Convert.ToString(xlWorksheet.Cells[i, 23].Value).Trim().ToUpper();
                                    if (!SCT.Contains(stype))
                                    {
                                        errorMessage.Add("Record No -" + i + " Is EventBased should be (YES, NO)");
                                    }
                                }
                                else
                                {
                                    errorMessage.Add("Record No -" + i + " Is EventBased should not blank");
                                }
                            }
                            #endregion

                            #region 24 Week Due Day For Weekly Frequency
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 5].Text.ToString().Trim()))
                            {

                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 11].Text.ToString().Trim()))
                                {
                                    var freq = Convert.ToString(xlWorksheet.Cells[i, 11].Value).Trim().ToUpper();
                                    if (freq == "WEEKLY")
                                    {
                                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 24].Text.ToString().Trim()))
                                        {
                                            var weekDay = Convert.ToString(xlWorksheet.Cells[i, 24].Value).Trim().ToUpper();
                                            bool IsValidWeekDay = false;
                                            switch (weekDay)
                                            {
                                                case "SUNDAY":
                                                    IsValidWeekDay = true;
                                                    break;
                                                case "MONDAY":
                                                    IsValidWeekDay = true;
                                                    break;
                                                case "TUESDAY":
                                                    IsValidWeekDay = true;
                                                    break;
                                                case "WEDNESDAY":
                                                    IsValidWeekDay = true;
                                                    break;
                                                case "THURSDAY":
                                                    IsValidWeekDay = true;
                                                    break;
                                                case "FRIDAY":
                                                    IsValidWeekDay = true;
                                                    break;
                                                case "SATURDAY":
                                                    IsValidWeekDay = true;
                                                    break;
                                            }
                                            if (IsValidWeekDay == false)
                                            {
                                                errorMessage.Add("Record No -" + i + "  Please enter correct Due Week Day");
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion
                        }

                        #endregion



                        if (errorMessage.Count > 0)
                        {
                            showErrorMessages(errorMessage, cvDuplicateEntry);
                            sucessvalidation = false;
                        }
                        else
                        {
                            try
                            {
                                createDataTable();
                                #region Grid Display Code 
                                for (int k = 2; k <= xlrow2; k++)
                                {
                                    DataRow drCompliance = dtCompliance.NewRow();
                                    com.VirtuosoITech.ComplianceManagement.Business.Data.InternalCompliance complianceData = new com.VirtuosoITech.ComplianceManagement.Business.Data.InternalCompliance();

                                    complianceData.CustomerID = customerID;
                                    drCompliance["CustomerID"] = customerID;

                                    drCompliance["IComplianceTypeID"] = Convert.ToString(xlWorksheet.Cells[k, 1].Value);
                                    drCompliance["IComplianceCategoryID"] = Convert.ToString(xlWorksheet.Cells[k, 2].Value);
                                    drCompliance["IComplianceType"] = Convert.ToString(xlWorksheet.Cells[k, 3].Value);

                                    if (Convert.ToString(xlWorksheet.Cells[k, 5].Value).Trim().Equals("Time Based Checklist"))
                                    {
                                        if (Convert.ToString(xlWorksheet.Cells[k, 4].Value).Trim().Equals("FixedGap"))
                                        {
                                            drCompliance["ISubComplianceType"] = Convert.ToString(xlWorksheet.Cells[k, 4].Value);
                                            drCompliance["IDueDate"] = Convert.ToString(xlWorksheet.Cells[k, 10].Value);
                                        }
                                        else if (Convert.ToString(xlWorksheet.Cells[k, 4].Value).Trim().Equals("PeriodicallyBased"))
                                        {
                                            drCompliance["ISubComplianceType"] = Convert.ToString(xlWorksheet.Cells[k, 4].Value);
                                            drCompliance["calflag"] = Convert.ToString(xlWorksheet.Cells[k, 19].Value);
                                            drCompliance["IFrequency"] = Convert.ToString(xlWorksheet.Cells[k, 11].Value);
                                            drCompliance["IDueDate"] = Convert.ToString(xlWorksheet.Cells[k, 12].Value);
                                        }
                                    }
                                    else if (Convert.ToString(xlWorksheet.Cells[k, 5].Value).Trim().Equals("Function based Checklist"))
                                    {
                                        drCompliance["IDueDate"] = Convert.ToString(xlWorksheet.Cells[k, 12].Value);
                                        drCompliance["IFrequency"] = Convert.ToString(xlWorksheet.Cells[k, 11].Value);
                                        drCompliance["calflag"] = Convert.ToString(xlWorksheet.Cells[k, 19].Value);
                                    }


                                    drCompliance["CheckListTypeID"] = Convert.ToString(xlWorksheet.Cells[k, 5].Value);
                                    drCompliance["IShortDescription"] = Convert.ToString(xlWorksheet.Cells[k, 6].Value);
                                    complianceData.IShortDescription = Convert.ToString(xlWorksheet.Cells[k, 6].Value);


                                    if (Convert.ToString(xlWorksheet.Cells[k, 5].Value).Trim().Equals("One Time based Checklist"))
                                    {
                                        drCompliance["IOneTimeDate"] = Convert.ToString(xlWorksheet.Cells[k, 7].Text);
                                    }


                                    drCompliance["IUploadDocument"] = Convert.ToString(xlWorksheet.Cells[k, 8].Value);
                                    drCompliance["IRequiredFrom"] = Convert.ToString(xlWorksheet.Cells[k, 9].Value);
                                    drCompliance["specilMonth"] = Convert.ToString(xlWorksheet.Cells[k, 13].Value);
                                    drCompliance["specialDay"] = Convert.ToString(xlWorksheet.Cells[k, 14].Value);
                                    drCompliance["IReminderType"] = Convert.ToString(xlWorksheet.Cells[k, 15].Value);
                                    drCompliance["IReminderBefore"] = Convert.ToString(xlWorksheet.Cells[k, 16].Value);
                                    drCompliance["IReminderGap"] = Convert.ToString(xlWorksheet.Cells[k, 17].Value);
                                    drCompliance["IRiskType"] = Convert.ToString(xlWorksheet.Cells[k, 18].Value);
                                    drCompliance["IDetailedDescription"] = Convert.ToString(xlWorksheet.Cells[k, 20].Value);
                                    drCompliance["IShortForm"] = Convert.ToString(xlWorksheet.Cells[k, 21].Value);
                                    complianceData.IShortForm = Convert.ToString(xlWorksheet.Cells[k, 21].Value);
                                    drCompliance["ScheduleType"] = Convert.ToString(xlWorksheet.Cells[k, 22].Value);
                                    drCompliance["IsEventBased"] = Convert.ToString(xlWorksheet.Cells[k, 23].Value);
                                    drCompliance["DueWeekDay"] = Convert.ToString(xlWorksheet.Cells[k, 24].Value);

                                    complianceData.CreatedOn = DateTime.Now;
                                    complianceData.IsDeleted = false;

                                    dtCompliance.Rows.Add(drCompliance);

                                    grdCheckList.DataSource = dtCompliance;
                                    grdCheckList.DataBind();
                                }
                                #endregion
                                sucessvalidation = true;
                                CheckListsuucess = true;
                            }
                            catch (Exception ex)
                            {
                                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                sucessvalidation = false;
                                CheckListsuucess = false;
                            }
                        }                             
                    }
                }
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Please select customer.";
                }                              
            }
            catch (Exception ex)
            {
                CheckListsuucess = false;
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnChecklistUploadFile_Click(object sender, EventArgs e)
        {
            if (MasterCheckListFileUpload.HasFile)
            {
                try
                {
                    string filename = Path.GetFileName(MasterCheckListFileUpload.FileName);
                    MasterCheckListFileUpload.SaveAs(Server.MapPath("~/Uploaded/") + filename.Trim());

                    FileInfo excelfile = new FileInfo(Server.MapPath("~/Uploaded/") + filename.Trim());
                    if (excelfile != null)
                    {
                        using (ExcelPackage xlWorkbook = new ExcelPackage(excelfile))
                        {
                            bool flag = InternalComplianceSheetsExitsts(xlWorkbook, "InternalChecklist");
                            if (flag == true)
                            {
                                ValidateCheckListDataNew(xlWorkbook);
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Please correct the sheet name.";
                            }
                            if (CheckListsuucess == true)
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Excelsheet data verified successfully.";
                                btnCheckListSave.Enabled = true;
                            }
                        }
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Error uploading file. Please try again.";

                    }


                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";

                }
            }
        }

        protected void btnCheckListSave_Click(object sender, EventArgs e)
        {
            try
            {
                ProcessCheckListData();
                btnCheckListSave.Enabled = false;

                if (CheckListsuucessSave == true)
                {
                    grdCheckList.DataSource = null;
                    grdCheckList.DataBind();
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Data save successfully.";
                    btnCheckListSave.Enabled = true;
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        #endregion

        #region Top  TAB
        protected void Tab1_Click(object sender, EventArgs e)
        {
            Tab1.CssClass = "Clicked";
            Tab3.CssClass = "Initial";
            MainView.ActiveViewIndex = 0;
        }

        protected void Tab3_Click(object sender, EventArgs e)
        {
            Tab1.CssClass = "Initial";
            Tab3.CssClass = "Clicked";
            MainView.ActiveViewIndex = 1;
        }
        protected void btnExcelFormat_Click(object sender, EventArgs e)
        {
            string FileName = "Internal Compliance Upload Format.xlsx";

            System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
            response.ClearContent();
            response.Clear();
            response.ContentType = "image/jpeg";
            response.AddHeader("Content-Disposition", "attachment; filename=" + FileName + ";");
            response.TransmitFile(Server.MapPath("~/ExcelFormat/Internal Compliance Upload Format.xlsx"));
            response.Flush();
            response.End();
        }

        #endregion
    }
  
}