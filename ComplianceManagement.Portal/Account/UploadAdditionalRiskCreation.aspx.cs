﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Account
{
    public partial class UploadAdditionalRiskCreation : System.Web.UI.Page
    {
        bool suucess = false;
        protected static List<long> Branchlist = new List<long>();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                lblMessage.Text = string.Empty;
                LblErormessage.Text = string.Empty;

                liRCM.Attributes.Add("class", "");
                lnkAuditchecklist.Attributes.Add("class", "active");

                Tab1_Click(sender, e);                          
            }
        }

        public static List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchy(long customerID, int customerbranchid)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_CustomerBranch
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }
            return hierarchy;
        }

        public static void LoadSubEntities(long customerid, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp, bool isClient, AuditControlEntities entities)
        {


            IQueryable<mst_CustomerBranch> query = (from row in entities.mst_CustomerBranch
                                                    where row.IsDeleted == false && row.CustomerID == customerid
                                                     && row.ParentID == nvp.ID
                                                    select row);
            var subEntities = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }

        public void BindLegalEntityData()
        {
            long customerID = -1;            
            customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;

            ddlLegalEntity.DataTextField = "Name";
            ddlLegalEntity.DataValueField = "ID";
            ddlLegalEntity.Items.Clear();
            ddlLegalEntity.DataSource = AuditKickOff_NewDetails.FillLegalEntityData(customerID);
            ddlLegalEntity.DataBind();
            ddlLegalEntity.Items.Insert(0, new ListItem("Unit", "-1"));
        }

        public void BindSubEntityData(DropDownList DRP, int ParentId)
        {
            long customerID = -1;            
            customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;
            DRP.DataTextField = "Name";
            DRP.DataValueField = "ID";
            DRP.Items.Clear();
            DRP.DataSource = AuditKickOff_NewDetails.FillSubEntityData(ParentId, customerID);
            DRP.DataBind();
            DRP.Items.Insert(0, new ListItem("Sub Unit", "-1"));
        }

        protected void ddlLegalEntity_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlLegalEntity.SelectedValue != "-1")
            {                
                BindSubEntityData(ddlSubEntity1, Convert.ToInt32(ddlLegalEntity.SelectedValue));                
            }
            else
            {
                if (ddlSubEntity1.Items.Count > 0)
                    ddlSubEntity1.Items.Clear();

                if (ddlSubEntity2.Items.Count > 0)
                    ddlSubEntity2.Items.Clear();

                if (ddlSubEntity3.Items.Count > 0)
                    ddlSubEntity3.Items.Clear();

                if (ddlFilterLocation.Items.Count > 0)
                    ddlFilterLocation.Items.Clear();
            }
        }

        protected void ddlSubEntity1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity1.SelectedValue != "-1")
            {                
                BindSubEntityData(ddlSubEntity2, Convert.ToInt32(ddlSubEntity1.SelectedValue));                               
            }
            else
            {
                if (ddlSubEntity2.Items.Count > 0)
                    ddlSubEntity2.ClearSelection();

                if (ddlSubEntity3.Items.Count > 0)
                    ddlSubEntity3.ClearSelection();

                if (ddlFilterLocation.Items.Count > 0)
                    ddlFilterLocation.ClearSelection();
            }
        }

        protected void ddlSubEntity2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity2.SelectedValue != "-1")
            {                
                BindSubEntityData(ddlSubEntity3, Convert.ToInt32(ddlSubEntity2.SelectedValue));                             
            }
            else
            {
                if (ddlSubEntity3.Items.Count > 0)
                    ddlSubEntity3.ClearSelection();

                if (ddlFilterLocation.Items.Count > 0)
                    ddlFilterLocation.ClearSelection();
            }
        }

        protected void ddlSubEntity3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity3.SelectedValue != "-1")
            {                
                BindSubEntityData(ddlFilterLocation, Convert.ToInt32(ddlSubEntity3.SelectedValue));                             
            }
            else
            {
                if (ddlFilterLocation.Items.Count > 0)
                    ddlFilterLocation.ClearSelection();
            }

        }

        protected void ddlFilterLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        protected void ddlProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlProcess.SelectedValue))
            {
                if (ddlProcess.SelectedValue != "-1")
                {                    
                    BindSubProcess(Convert.ToInt32(ddlProcess.SelectedValue), "P");
                    //BindDataExport("P");
                }
                else
                {
                    ddlSubProcess.Items.Clear();
                }
            }
            else
            {
                ddlSubProcess.Items.Clear();
            }
        }        
        protected void upComplianceDetails_Load(object sender, EventArgs e)
        {
            try
            {
                //ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "Script", "initializeCombobox();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
               
        protected void btnUploadFile_Click(object sender, EventArgs e)
        {
            if (MasterFileUpload.HasFile)
            {
                try
                {
                    string filename = Path.GetFileName(MasterFileUpload.FileName);
                    MasterFileUpload.SaveAs(Server.MapPath("~/Uploaded/") + filename.Trim());
                    FileInfo excelfile = new FileInfo(Server.MapPath("~/Uploaded/") + filename.Trim());

                    if (excelfile != null)
                    {
                        using (ExcelPackage xlWorkbook = new ExcelPackage(excelfile))
                        {
                            if (rdoCompliance.Checked)
                            {
                                bool flag = RiskTransactionSheetsExitsts(xlWorkbook, "AuditStepExportCreate");
                                if (flag == true)
                                {
                                    ProcessAuditStepCreate(xlWorkbook);
                                }
                                else
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "No Data Found in Excel Document or Sheet Name must be 'AuditStepExportCreate'.";                                   
                                }
                            }
                            else if (rdoAuditStepUpdate.Checked)
                            {
                                bool flag = UpdateAuditStepSheetsExitsts(xlWorkbook, "AuditStepExportForUpdate");
                                if (flag == true)
                                {
                                    ProcessAuditStepUpdate(xlWorkbook);
                                }
                                else
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "No Data Found in Excel Document or Sheet Name must be 'AuditStepExportForUpdate'.";
                                }
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Please select type of data wants to be upload.";
                            }

                            if (suucess)
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ForeColor = Color.Green;
                                cvDuplicateEntry.ErrorMessage = "Data Uploaded Successfully.";
                            }                            
                        }
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Error uploading file. Please try again.";
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                }
            }
        }    

        public DateTime GetDate(string date)
        {
            string date1 = "";
            if (date.Contains("/"))
            {
                date1 = date.Trim().Substring(date.IndexOf("/") + 4, 4) + "-" + date.Substring(date.IndexOf("/") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains("-"))
            {
                date1 = date.Substring(date.IndexOf("-") + 4, 4) + "-" + date.Substring(date.IndexOf("-") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains(" "))
            {
                date1 = date.Substring(date.IndexOf(" ") + 4, 4) + "-" + date.Substring(date.IndexOf(" ") + 1, 2) + "-" + date.Substring(0, 2);
            }
            return Convert.ToDateTime(date1);
        }

        public static DateTime? CleanDateField(string DateField)
        {
            // Convert the text to DateTime and return the value or null
            DateTime? CleanDate = new DateTime();
            int intDate;
            bool DateIsInt = int.TryParse(DateField, out intDate);
            if (DateIsInt)
            {
                // If this is a serial date, convert it
                CleanDate = DateTime.FromOADate(intDate);
            }
            else if (DateField.Length != 0 && DateField != "1/1/0001 12:00:00 AM" &&
                DateField != "1/1/1753 12:00:00 AM")
            {
                // Convert from a General format
                CleanDate = (Convert.ToDateTime(DateField));
            }
            else
            {
                // Date is blank
                CleanDate = null;
            }
            return CleanDate;
        }       
     
        private bool RiskTransactionSheetsExitsts(ExcelPackage xlWorkbook, string data)
        {
            try
            {
                bool flag = false;
                foreach (ExcelWorksheet sheet in xlWorkbook.Workbook.Worksheets)
                {
                    if (data.Equals("AuditStepExportCreate"))
                    {
                        if (sheet.Name.Trim().Equals("AuditStepExportCreate") || sheet.Name.Trim().Equals("AuditStepExportCreate") || sheet.Name.Trim().Equals("AuditStepExportCreate"))
                        {
                            flag = true;
                            break;//added by Manisha
                        }
                        else
                        {
                            flag = false;
                            break;
                        }
                    }


                }
                return flag;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        private bool UpdateAuditStepSheetsExitsts(ExcelPackage xlWorkbook, string data)
        {
            try
            {
                bool flag = false;
                foreach (ExcelWorksheet sheet in xlWorkbook.Workbook.Worksheets)
                {
                    if (data.Equals("AuditStepExportForUpdate"))
                    {
                        if (sheet.Name.Trim().Equals("AuditStepExportForUpdate"))
                        {
                            flag = true;
                            break;
                        }
                        else
                        {
                            flag = false;
                            break;
                        }
                    }
                }
                return flag;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        private void ProcessAuditStepCreate(ExcelPackage xlWorkbook)
        {
            try
            {
                ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["AuditStepExportCreate"];
                if (xlWorksheet != null)
                {
                    int count = 0;
                    int xlrow2 = xlWorksheet.Dimension.End.Row;
                    //List<RiskActivityToBeDoneMapping> RiskATBDM = new List<RiskActivityToBeDoneMapping>();
                    //List<RiskActivityToBeDoneMapping> RiskATBDM1 = new List<RiskActivityToBeDoneMapping>();                
                    List<StepChecklistMapping> SCMRecords = new List<StepChecklistMapping>();
                    
                    long customerID = -1;                   
                    customerID = Common.AuthenticationHelper.CustomerID;
                    List<Mst_Process> ProcessRecords = new List<Mst_Process>();
                    List<long> BranchList = new List<long>();
                    List<mst_Subprocess> SubProcessRecords = new List<mst_Subprocess>();

                    var ProcessList = HttpContext.Current.Cache["ProcessListCustomer"];
                    var SubProcessList = HttpContext.Current.Cache["SubProcessProcessListCustomer"];

                    if (ProcessList == null)
                    {
                        using (AuditControlEntities entities = new AuditControlEntities())
                        {
                            ProcessRecords = (from row in entities.Mst_Process
                                       where row.CustomerID == customerID
                                       select row).ToList();

                            HttpContext.Current.Cache.Insert("ProcessListCustomer", ProcessRecords); // add it to cache
                        }
                    }

                    ProcessRecords = (List<Mst_Process>) Cache["ProcessListCustomer"];

                    if (SubProcessList == null)
                    {
                        using (AuditControlEntities entities = new AuditControlEntities())
                        {
                            SubProcessRecords = (from row in entities.mst_Subprocess
                                                 select row).ToList();

                            HttpContext.Current.Cache.Insert("SubProcessProcessListCustomer", SubProcessRecords); // add it to cache
                        }
                    }

                    SubProcessRecords = (List<mst_Subprocess>) Cache["SubProcessProcessListCustomer"];

                    int riskcreationId = 0;
                    int RiskActivityID = 0;
                    int custBranchID = -1;
                    int verticalID = -1;
                    int processid = -1;
                    int subprocessid = -1;
                    string activitytobedone = "";
                    string auditObj = "";
                    //string rating = "";
                    //int ratingid = -1;
                    long auditStepMasterID = -1;
                    string analyis_to_be_performed = string.Empty;
                    using (AuditControlEntities entities = new AuditControlEntities())
                    {
                        BranchList = (from row in entities.mst_CustomerBranch
                                      where row.CustomerID == customerID
                                      && row.IsDeleted == false
                                      && row.Status == 1
                                      select (long)row.ID).ToList();
                    }
                    for (int i = 2; i <= xlrow2; i++)
                    {
                        count = count + 1;

                        riskcreationId = 0;
                        RiskActivityID = 0;
                        processid = 0;
                        subprocessid = 0;
                        activitytobedone = ""; //AuditStep
                        auditObj = "";
                        //rating = "";
                        //ratingid = 0;

                        custBranchID = 0;
                        verticalID = 0;
                        auditStepMasterID = 0;

                        //13 RiskID
                        if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 13].Text))
                        {
                            riskcreationId = Convert.ToInt32(xlWorksheet.Cells[i, 13].Value.ToString());
                        }
                        if (riskcreationId == 0)
                        {
                            suucess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Check the Risk ID at row - " + (count + 1) + " or Risk ID can not be left blank.";
                            break;
                        }

                        //14 RiskActivityID
                        if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 14].Text))
                        {
                            RiskActivityID = Convert.ToInt32(xlWorksheet.Cells[i, 14].Value.ToString());
                        }
                        if (RiskActivityID == 0)
                        {
                            suucess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Check the RiskActivityID at row - " + (count + 1) + " or RiskActivityID can not be left blank.";
                            break;
                        }

                        //15 CustomerBranchID
                        if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 15].Text))
                        {                           
                            custBranchID = Convert.ToInt32(xlWorksheet.Cells[i, 15].Value.ToString());

                            if (!BranchList.Contains(custBranchID))
                            {
                                suucess = false;
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Please Correct the BID at row - " + (count + 1) + " or BID not Defined in the System.";
                                break;
                            }                           
                        }
                        if (custBranchID == 0)
                        {
                            suucess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Check the BID at row - " + (count + 1) + " or BID can not be left blank.";
                            break;
                        }

                        //16 VerticalID
                        if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 16].Text))
                        {
                            verticalID = Convert.ToInt32(xlWorksheet.Cells[i, 16].Value.ToString());
                        }
                        if (verticalID == 0)
                        {
                            suucess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Check the VID at row - " + (count + 1) + " or VID can not be left blank.";
                            break;
                        }                      
                        //3 Process
                        if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 3].Text))
                        {                            
                            if (ProcessRecords.Where(entry => entry.CustomerID==customerID && entry.Name.Trim().ToUpper() == xlWorksheet.Cells[i, 3].Text.ToString().Trim().ToUpper()).Select(entry => entry.Id).FirstOrDefault() != 0)
                                processid = Convert.ToInt32(ProcessRecords.Where(entry => entry.Name.Trim().ToUpper() == xlWorksheet.Cells[i, 3].Text.ToString().Trim().ToUpper()).Select(entry => entry.Id).FirstOrDefault());
                            else
                                processid = 0;
                        }
                        if (processid == 0)
                        {
                            suucess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Correct the Process Name at row - " + (count + 1) + " or Process not Defined in the System.";
                            break;
                        }

                        //4 Sub Process
                        if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 4].Text))
                        {                          
                            if (SubProcessRecords.Where(entry => entry.ProcessId == processid && entry.Name.Trim().ToUpper() == xlWorksheet.Cells[i, 4].Text.ToString().Trim().ToUpper()).Select(entry => entry.Id).FirstOrDefault() != 0)
                                subprocessid = Convert.ToInt32(SubProcessRecords.Where(entry => entry.ProcessId == processid && entry.Name.Trim().ToUpper() == xlWorksheet.Cells[i, 4].Text.ToString().Trim().ToUpper()).Select(entry => entry.Id).FirstOrDefault());
                            else
                                subprocessid = 0;
                        }
                        if (subprocessid == 0)
                        {
                            suucess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Correct the Sub Process Name at row - " + (count + 1) + " or Sub Process not Defined in the System.";
                            break;
                        }

                        //9 Audit Objective
                        if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 9].Text))
                            auditObj = xlWorksheet.Cells[i, 9].Text.Trim();
                        else
                            auditObj = "";

                        //10 Audit Step
                        if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 10].Text))
                        {
                            activitytobedone = xlWorksheet.Cells[i, 10].Text.Trim();
                        }
                        if (activitytobedone == null || activitytobedone == "")
                        {
                            suucess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Check the Audit Step at row number - " + (count + 1) + ", Audit Step can not be left blank.";
                            break;
                        }

                        //11 Analyis To Be Performed
                        if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 11].Text))
                            analyis_to_be_performed = xlWorksheet.Cells[i, 11].Text.Trim();
                        else
                            analyis_to_be_performed = "";

                        //COMMENT BY RAHUL ON 3 SEP 2019 TO HIDE STEP REATING
                        //13 Audit Step Rating
                        //if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 13].Text))
                        //{
                        //    rating = xlWorksheet.Cells[i, 13].Text.Trim();
                        //}
                        //if (rating.ToUpper() == "Major".ToUpper())
                        //{
                        //    ratingid = 1;
                        //}
                        //else if (rating.ToUpper() == "Moderate".ToUpper())
                        //{
                        //    ratingid = 2;
                        //}
                        //else if (rating.ToUpper() == "Minor".ToUpper())
                        //{
                        //    ratingid = 3;
                        //}
                        //else
                        //{
                        //    ratingid = 0;
                        //}
                        //if (ratingid == 0)
                        //{
                        //    suucess = false;
                        //    cvDuplicateEntry.IsValid = false;
                        //    cvDuplicateEntry.ErrorMessage = "Please Correct the Audit Step Rating at row - " + (count + 1) + " or Audit Step Rating can not left blank.";
                        //    break;
                        //}

                        suucess = true;

                        if (riskcreationId != 0)
                        {
                            if (RiskActivityID != 0)
                            {
                                if (processid != 0)
                                {
                                    if (subprocessid != 0)
                                    {
                                        if (custBranchID != 0)
                                        {
                                            if (verticalID != 0)
                                            {
                                                if (activitytobedone != "")
                                                {
                                                    //if (ratingid != 0)
                                                    //{                                                      
                                                        if (!(RiskCategoryManagement.RiskActivityToBeDoneMappingExists(riskcreationId, RiskActivityID, custBranchID, verticalID, processid, subprocessid, activitytobedone.Trim())))
                                                        {
                                                            auditStepMasterID = RiskCategoryManagement.GetAuditStepMasterID(activitytobedone);
                                                            if (auditStepMasterID == 0)
                                                            {
                                                                AuditStepMaster newASM = new AuditStepMaster()
                                                                {
                                                                    AuditStep = activitytobedone
                                                                };

                                                                if (RiskCategoryManagement.CreateAuditStepMaster(newASM))
                                                                    auditStepMasterID = newASM.ID;
                                                            }

                                                            if (auditStepMasterID != 0)
                                                            {
                                                                RiskActivityToBeDoneMapping RATBDM = new RiskActivityToBeDoneMapping()
                                                                {
                                                                    AuditStepMasterID = auditStepMasterID,
                                                                    RiskCategoryCreationId = riskcreationId,
                                                                    RiskActivityId = RiskActivityID,
                                                                    CustomerBranchID = custBranchID,
                                                                    VerticalID = verticalID,
                                                                    ProcessId = processid,
                                                                    SubProcessId = subprocessid,
                                                                    AuditObjective = auditObj,
                                                                    ActivityTobeDone = activitytobedone,
                                                                    Analyis_To_Be_Performed = analyis_to_be_performed,
                                                                    Rating = 1,
                                                                    IsActive = true
                                                                };
                                                                //RiskATBDM.Add(RATBDM); //Comment by Rahul on 3 July 2019

                                                                suucess = RiskCategoryManagement.CreateRiskActivityTBDMapping(RATBDM);
                                                                #region  12 PreRequisite
                                                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 12].Text.ToString().Trim()))
                                                                {
                                                                    string prerequisite = xlWorksheet.Cells[i, 12].Text.ToString().Trim();
                                                                    string[] split = prerequisite.Split('|');
                                                                    if (split.Length > 0)
                                                                    {
                                                                        string prerequisiteName = string.Empty;
                                                                        for (int rs = 0; rs < split.Length; rs++)
                                                                        {
                                                                            prerequisiteName = Convert.ToString(split[rs].ToString().Trim());
                                                                            if (!String.IsNullOrEmpty(prerequisiteName))
                                                                            {
                                                                                StepChecklistMapping SCM = new StepChecklistMapping();
                                                                                SCM.ATBDID = RATBDM.ID;
                                                                                SCM.ChecklistDocument = prerequisiteName;
                                                                                SCM.IsActive = true;
                                                                                SCMRecords.Add(SCM);
                                                                            }                                                                           
                                                                        }
                                                                    }
                                                                }
                                                                if (SCMRecords.Count>0)
                                                                {
                                                                    suucess = RiskCategoryManagement.CreateStepChecklistMapping(SCMRecords);
                                                                    SCMRecords.Clear();
                                                                }                                                                
                                                                #endregion
                                                            }
                                                        }
                                                        else
                                                        {
                                                            suucess= RiskCategoryManagement.RiskActivityToBeDoneMappingUpdate(riskcreationId, RiskActivityID, custBranchID, verticalID, processid, subprocessid, activitytobedone.Trim(), auditObj.Trim());                                                                                                                                       
                                                        }
                                                        //}//Check Customerbranch
                                                    //}
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (suucess)
                    {
                        //RiskATBDM1 = RiskATBDM.Where(entry => entry.RiskCategoryCreationId == 0 || entry.RiskActivityId == 0).ToList();

                        //if (RiskATBDM1.Count > 0)
                        //{
                        //    suucess = false;
                        //    cvDuplicateEntry.IsValid = false;
                        //    cvDuplicateEntry.ErrorMessage = "Something went wrong, Please check excel document carefully before uploading.";
                        //}
                        //else
                        //{                           
                            //suucess = RiskCategoryManagement.CreateRiskActivityTBDMapping(RiskATBDM);                          
                        //}
                    }
                }

                foreach (System.Collections.DictionaryEntry entry in HttpContext.Current.Cache)
                {
                    if ((string)entry.Key == "ProcessListCustomer" || (string)entry.Key == "SubProcessProcessListCustomer")
                        HttpContext.Current.Cache.Remove((string)entry.Key);
                }
            }
            catch (Exception ex)
            {
                suucess = false;
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";

                foreach (System.Collections.DictionaryEntry entry in HttpContext.Current.Cache)
                {
                    if ((string)entry.Key == "ProcessListCustomer" || (string)entry.Key == "SubProcessProcessListCustomer")
                        HttpContext.Current.Cache.Remove((string)entry.Key);
                }
            }
        }

        private void ProcessAuditStepUpdate(ExcelPackage xlWorkbook)
        {
            try
            {
                ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["AuditStepExportForUpdate"];

                if (xlWorksheet != null)
                {
                    int xlrow2 = xlWorksheet.Dimension.End.Row;
                    List<RiskActivityToBeDoneMapping> RiskATBDM = new List<RiskActivityToBeDoneMapping>();
                    List<RiskActivityToBeDoneMapping> RiskATBDM1 = new List<RiskActivityToBeDoneMapping>();
                    List<StepChecklistMapping> SCMRecords = new List<StepChecklistMapping>();

                    string auditObj = "";
                    string activitytobedone = "";
                    //string rating = "";
                    int ATBDID = 0;
                    //int ratingid = -1;
                    long auditStepMasterID = -1;

                    //Verify Data in Excel Document
                    for (int i = 2; i <= xlrow2; i++)
                    {
                        activitytobedone = "";
                        //rating = "";
                        ATBDID = 0;
                        //ratingid = -1;

                        //13 ATBDID
                        if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 13].Text))
                            ATBDID = Convert.ToInt32(xlWorksheet.Cells[i, 13].Value.ToString());

                        if (ATBDID == 0)
                        {
                            suucess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Check the ID at row - " + i + " or ID can not be empty.";
                            break;
                        }
                        else
                            suucess = true;

                        //10 AuditStep
                        if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 10].Text))
                            activitytobedone = Convert.ToString(xlWorksheet.Cells[i, 10].Text).Trim();

                        if (activitytobedone == "")
                        {
                            suucess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Check the Audit Step at row - " + i + " or Audit Step can not be empty.";
                            break;
                        }
                        else
                            suucess = true;

                        //COMMENT BY RAHUL ON 3 SEP 2019 TO HIDE STEP REATING
                        //13 Audit Step Rating
                        //if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 13].Text))
                        //{
                        //    rating = xlWorksheet.Cells[i, 13].Text.Trim();
                        //}
                        //if (rating.ToUpper() == "Major".ToUpper())
                        //{
                        //    ratingid = 1;
                        //}
                        //else if (rating.ToUpper() == "Moderate".ToUpper())
                        //{
                        //    ratingid = 2;
                        //}
                        //else if (rating.ToUpper() == "Minor".ToUpper())
                        //{
                        //    ratingid = 3;
                        //}
                        //else
                        //{
                        //    ratingid = 0;
                        //}
                        //if (ratingid == 0)
                        //{
                        //    suucess = false;
                        //    cvDuplicateEntry.IsValid = false;
                        //    cvDuplicateEntry.ErrorMessage = "Please Correct the Audit Step Rating at row - " + i + " or Audit Step Rating can not left blank.";
                        //    break;
                        //}
                    }

                    if (suucess)
                    {
                        for (int i = 2; i <= xlrow2; i++)
                        {
                            activitytobedone = "";
                            auditObj = "";
                            //rating = "";
                            ATBDID = 0;
                            //ratingid = -1;
                            auditStepMasterID = 0;

                            //14 ATBDID
                            if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 14].Text))
                                ATBDID = Convert.ToInt32(xlWorksheet.Cells[i, 14].Value.ToString());

                            //9 Audit Objective
                            if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 9].Text))
                                auditObj = Convert.ToString(xlWorksheet.Cells[i, 9].Text).Trim();

                            //10 AuditStep
                            if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 10].Text))
                                activitytobedone = Convert.ToString(xlWorksheet.Cells[i, 10].Text).Trim();

                            ////13 Audit Step Rating
                            //if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 13].Text))
                            //{
                            //    rating = xlWorksheet.Cells[i, 13].Text.Trim();
                            //}
                            //if (rating.ToUpper() == "Major".ToUpper())
                            //{
                            //    ratingid = 1;
                            //}
                            //else if (rating.ToUpper() == "Moderate".ToUpper())
                            //{
                            //    ratingid = 2;
                            //}
                            //else if (rating.ToUpper() == "Minor".ToUpper())
                            //{
                            //    ratingid = 3;
                            //}

                            if (ATBDID != 0)
                            {
                                if (RiskCategoryManagement.ExistsRATBDMByID(ATBDID))
                                {
                                    auditStepMasterID = RiskCategoryManagement.GetAuditStepMasterID(activitytobedone);
                                    if (auditStepMasterID == 0)
                                    {
                                        AuditStepMaster newASM = new AuditStepMaster()
                                        {
                                            AuditStep = activitytobedone
                                        };

                                        if (RiskCategoryManagement.CreateAuditStepMaster(newASM))
                                            auditStepMasterID = newASM.ID;
                                    }
                                    if (auditStepMasterID != 0)
                                    {
                                        RiskActivityToBeDoneMapping RATBDM = new RiskActivityToBeDoneMapping()
                                        {
                                            ID = ATBDID,
                                            AuditStepMasterID = auditStepMasterID,                                           
                                            AuditObjective = auditObj,
                                            ActivityTobeDone = activitytobedone,
                                            Rating = 1
                                        };
                                        RiskATBDM.Add(RATBDM);
                                    }

                                    #region  //12 PreRequisite
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 12].Text.ToString().Trim()))
                                    {
                                        string prerequisite = xlWorksheet.Cells[i, 12].Text.ToString().Trim();
                                        string[] split = prerequisite.Split('|');
                                        if (split.Length > 0)
                                        {
                                            List<int> prerequisiteID = new List<int>();
                                            {
                                                string prerequisites = xlWorksheet.Cells[i, 14].Text.ToString().Trim();
                                                string[] splitint = prerequisites.Split('|');
                                                if (splitint.Length > 0)
                                                {                                                   
                                                    for (int rs = 0; rs < splitint.Length; rs++)
                                                    {
                                                        if (!string.IsNullOrEmpty(splitint[rs].ToString().Trim()))
                                                        {
                                                            prerequisiteID.Add(Convert.ToInt32(splitint[rs].ToString().Trim()));
                                                        }                                                        
                                                    }
                                                }
                                            }

                                            string prerequisiteName = string.Empty;
                                            for (int rs = 0; rs < split.Length; rs++)
                                            {
                                                int SCMID = -1;
                                                if (rs + 1 <= prerequisiteID.Count )
                                                {
                                                    SCMID = prerequisiteID[rs];
                                                }                                                
                                                prerequisiteName = Convert.ToString(split[rs].ToString().Trim());
                                                if (!String.IsNullOrEmpty(prerequisiteName))
                                                {
                                                    StepChecklistMapping SCM = new StepChecklistMapping();
                                                    if (SCMID != -1)
                                                    {
                                                        SCM.ID = SCMID;
                                                    }
                                                    SCM.ATBDID = ATBDID;
                                                    SCM.ChecklistDocument = prerequisiteName;
                                                    SCM.IsActive = true;
                                                    SCMRecords.Add(SCM);
                                                }
                                            }
                                            prerequisiteID.Clear();
                                        }
                                    }
                                    if (SCMRecords.Count > 0)
                                    {                                        
                                        suucess = RiskCategoryManagement.CreateStepChecklistMapping(SCMRecords);
                                        SCMRecords.Clear();
                                    }
                                    #endregion
                                }
                            }
                        }

                        RiskATBDM1 = RiskATBDM.Where(entry => entry.ID == 0).ToList();

                        if (RiskATBDM1.Count == 0)
                            suucess = RiskCategoryManagement.BulkRATBDMUpdate(RiskATBDM);
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Something went wrong, Please check excel document before uploading.";
                            suucess = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                suucess = false;
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }


        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
               server control at run time. */
        }

        protected void btnUploadFile1_Click(object sender, EventArgs e)
        {
            try
            {
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("AuditStepExportCreate");
                    DataTable ExcelData = null;
                    var AuditStepList = GetDataExport("P");
                    DataView view = new System.Data.DataView(AuditStepList.ToDataTable());
                    //ExcelData = view.ToTable("Selected", false, "BranchName", "VerticalName", "ProcessName", "SubProcessName", "ControlNo", "ActivityDescription", "ControlDescription", "RiskRating", "AuditObjective", "ActivityTobeDone", "AnalyisToBePerformed", "PreRequisite", "AuditStepRating", "RiskCreationId", "RiskActivityID", "BranchId", "VerticalId");
                    ExcelData = view.ToTable("Selected", false, "BranchName", "VerticalName", "ProcessName", "SubProcessName", "ControlNo", "ActivityDescription", "ControlDescription", "RiskRating", "AuditObjective", "ActivityTobeDone", "AnalyisToBePerformed", "PreRequisite", "RiskCreationId", "RiskActivityID", "BranchId", "VerticalId");

                    exWorkSheet.Cells["A1"].LoadFromDataTable(ExcelData, true);
                    exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["A1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["A1"].Value = "Location";
                    exWorkSheet.Cells["A1"].AutoFitColumns(20);

                    exWorkSheet.Cells["B1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["B1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["B1"].Value = "Vertical";
                    exWorkSheet.Cells["B1"].AutoFitColumns(20);

                    exWorkSheet.Cells["C1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["C1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["C1"].Value = "Process";
                    exWorkSheet.Cells["C1"].AutoFitColumns(20);

                    exWorkSheet.Cells["D1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["D1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["D1"].Value = "Sub Process";
                    exWorkSheet.Cells["D1"].AutoFitColumns(20);

                    exWorkSheet.Cells["E1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["E1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["E1"].Value = "Control No";
                    exWorkSheet.Cells["E1"].AutoFitColumns(15);

                    exWorkSheet.Cells["F1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["F1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["F1"].Value = "Risk Description";
                    exWorkSheet.Cells["F1"].AutoFitColumns(50);

                    exWorkSheet.Cells["G1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["G1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["G1"].Value = "Control Description";
                    exWorkSheet.Cells["G1"].AutoFitColumns(50);

                    exWorkSheet.Cells["H1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["H1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["H1"].Value = "Risk Rating";
                    exWorkSheet.Cells["H1"].AutoFitColumns(15);

                    exWorkSheet.Cells["I1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["I1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["I1"].Value = "Audit Methodology";
                    exWorkSheet.Cells["I1"].AutoFitColumns(50);

                    exWorkSheet.Cells["J1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["J1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["J1"].Value = "Audit Steps";
                    exWorkSheet.Cells["J1"].AutoFitColumns(50);
                    
                    exWorkSheet.Cells["K1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["K1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["K1"].Value = "Analyis To Be Performed";
                    exWorkSheet.Cells["K1"].AutoFitColumns(50);

                    exWorkSheet.Cells["L1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["L1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["L1"].Value = "Pre-Requisite List";
                    exWorkSheet.Cells["L1"].AutoFitColumns(50);

                    //exWorkSheet.Cells["M1"].Style.Font.Bold = true;
                    //exWorkSheet.Cells["M1"].Style.Font.Size = 12;
                    //exWorkSheet.Cells["M1"].Value = "Audit Step Rating";
                    //exWorkSheet.Cells["M1"].AutoFitColumns(10);

                    exWorkSheet.Cells["M1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["M1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["M1"].Value = "RiskID";
                    exWorkSheet.Cells["M1"].AutoFitColumns(10);

                    exWorkSheet.Cells["N1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["N1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["N1"].Value = "RiskActivityID";
                    exWorkSheet.Cells["N1"].AutoFitColumns(10);

                    exWorkSheet.Cells["O1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["O1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["O1"].Value = "BID";
                    exWorkSheet.Cells["O1"].AutoFitColumns(10);

                    exWorkSheet.Cells["P1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["P1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["P1"].Value = "VID";
                    exWorkSheet.Cells["P1"].AutoFitColumns(10);
                                       
                    using (ExcelRange col = exWorkSheet.Cells[1, 1, 1 + ExcelData.Rows.Count, 16])
                    {
                        col.Style.WrapText = true;                        
                        col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                      
                        // Assign borders
                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    }                   
                    Byte[] fileBytes = exportPackge.GetAsByteArray();
                    Response.ClearContent();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=AuditStepExport.xlsx");
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.ms-excel";
                    StringWriter sw = new StringWriter();
                    Response.BinaryWrite(fileBytes);
                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);               
            }
        }

        public void BindVertical()
        {
            try
            {
                long customerID = -1;                
                customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;

                ddlVertical.DataTextField = "VerticalName";
                ddlVertical.DataValueField = "ID";
                ddlVertical.Items.Clear();
                ddlVertical.DataSource = UserManagementRisk.GetAllVertical(customerID);
                ddlVertical.DataBind();
                ddlVertical.Items.Insert(0, new ListItem("Select Vertical", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindProcess()
        {
            int CustomerBranchId = -1;
            if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlLegalEntity.SelectedValue);

                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
            {
                if (ddlFilterLocation.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                }
            }

            long customerID = -1;            
            customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;

            ddlProcess.DataTextField = "Name";
            ddlProcess.DataValueField = "ID";
            ddlProcess.Items.Clear();
            ddlProcess.DataSource = ProcessManagement.FillProcess("P", customerID);            
            ddlProcess.DataBind();
            ddlProcess.Items.Insert(0, new ListItem("Process", "-1"));
        }

        private void BindSubProcess(long Processid, string flag)
        {
            try
            {
                if (flag == "P")
                {
                    ddlSubProcess.Items.Clear();
                    ddlSubProcess.DataTextField = "Name";
                    ddlSubProcess.DataValueField = "Id";
                    ddlSubProcess.DataSource = ProcessManagement.FillSubProcess(Processid, flag);
                    ddlSubProcess.DataBind();                    
                }
                else
                {
                    ddlSubProcess.Items.Clear();
                    ddlSubProcess.DataTextField = "Name";
                    ddlSubProcess.DataValueField = "Id";
                    ddlSubProcess.DataSource = ProcessManagement.FillSubProcess(Processid, flag);
                    ddlSubProcess.DataBind();                   
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindDataExport(string Flag)
        {
            try
            {                
                int CustomerBranchId = -1;
                int verticalid = -1;
                int processid = -1;
                int customerID = -1;                
                customerID =Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                    }
                }

                if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                    }
                }

                if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                    }
                }

                if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                    }
                }

                if (!string.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                    }
                }

                if (ddlProcess.SelectedValue != "")
                {
                    if (ddlProcess.SelectedValue != "-1")
                    {
                        processid = Convert.ToInt32(ddlProcess.SelectedValue);
                    }
                }               
                if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 0)
                {
                    int vid = UserManagementRisk.VerticalgetBycustomerid(Portal.Common.AuthenticationHelper.CustomerID);
                    if (vid != -1)
                    {
                        verticalid = vid;
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(ddlVertical.SelectedValue))
                    {
                        if (ddlVertical.SelectedValue != "-1")
                        {
                            verticalid = Convert.ToInt32(ddlVertical.SelectedValue);
                        }
                    }
                }
                Branchlist.Clear();
                GetAllHierarchy(customerID, CustomerBranchId);              

                if (Flag == "L")
                {
                    var Riskcategorymanagementlist = AdditionalRiskCreation.GetAllRiskCategoryCreationExport(customerID, Branchlist, processid, verticalid, "P");                    
                    GridExportExcel.DataSource = Riskcategorymanagementlist;
                    Session["TotalRows"] = Riskcategorymanagementlist.Count;
                    GridExportExcel.DataBind();

                    Session["grdAuditStepCreate"] = (GridExportExcel.DataSource as List<RCMExportCheckListView>).ToDataTable();
                }

                else if (Flag == "P")
                {
                    var Riskcategorymanagementlist = AdditionalRiskCreation.GetAllRiskCategoryCreationExport(customerID, Branchlist, processid, verticalid, "P");                    
                    GridExportExcel.DataSource = Riskcategorymanagementlist;
                    Session["TotalRows"] = Riskcategorymanagementlist.Count;
                    GridExportExcel.DataBind();

                    Session["grdAuditStepCreate"] = (GridExportExcel.DataSource as List<RCMExportCheckListView>).ToDataTable();
                }
            }

            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private List<RCMExportCheckListView> GetDataExport(string Flag)
        {
            int CustomerBranchId = -1;
            int processid = -1;
            int verticalid = -1;

            int customerID = -1;            
            customerID =Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            List<RCMExportCheckListView> AuditStepList = new List<RCMExportCheckListView>();

            if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                }
            }

            if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                }
            }

            if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                }
            }

            if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                }
            }

            if (!string.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
            {
                if (ddlFilterLocation.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                }
            }

            if (ddlProcess.SelectedValue != "")
            {
                if (ddlProcess.SelectedValue != "-1")
                {
                    processid = Convert.ToInt32(ddlProcess.SelectedValue);
                }
            }

            if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 0)
            {
                int vid = UserManagementRisk.VerticalgetBycustomerid(Portal.Common.AuthenticationHelper.CustomerID);
                if (vid != -1)
                {
                    verticalid = vid;
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(ddlVertical.SelectedValue))
                {
                    if (ddlVertical.SelectedValue != "-1")
                    {
                        verticalid = Convert.ToInt32(ddlVertical.SelectedValue);
                    }
                }
            }

            Branchlist.Clear();
            GetAllHierarchy(customerID, CustomerBranchId);

            if (Flag == "L")
            {
                AuditStepList = AdditionalRiskCreation.GetAllRiskCategoryCreationExport(customerID, Branchlist, processid, verticalid, "P");                
            }
            else if (Flag == "P")
            {
                AuditStepList = AdditionalRiskCreation.GetAllRiskCategoryCreationExport(customerID, Branchlist, processid, verticalid, "P");                
            }

            return AuditStepList;
        }

        protected void Tab1_Click(object sender, EventArgs e)
        {
            try
            {
                liImport.Attributes.Add("class", "active");
                liExportCreate.Attributes.Add("class", "");
                liExportUpdate.Attributes.Add("class", "");

                performerdocuments.Visible = true;
                performerdocuments.Attributes.Remove("class");
                performerdocuments.Attributes.Add("class", "tab-pane active");

                reviewerdocuments.Visible = false;
                reviewerdocuments.Attributes.Remove("class");
                reviewerdocuments.Attributes.Add("class", "tab-pane");

                DivExportUpdate.Visible = false;
                DivExportUpdate.Attributes.Remove("class");
                DivExportUpdate.Attributes.Add("class", "tab-pane");
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void Tab2_Click(object sender, EventArgs e)
        {
            try
            {               
                liImport.Attributes.Add("class", "");
                liExportCreate.Attributes.Add("class", "active");
                liExportUpdate.Attributes.Add("class", "");

                performerdocuments.Visible = false;
                performerdocuments.Attributes.Remove("class");
                performerdocuments.Attributes.Add("class", "tab-pane");

                reviewerdocuments.Visible = true;
                reviewerdocuments.Attributes.Remove("class");
                reviewerdocuments.Attributes.Add("class", "tab-pane active");

                DivExportUpdate.Visible = false;
                DivExportUpdate.Attributes.Remove("class");
                DivExportUpdate.Attributes.Add("class", "tab-pane");

                BindProcess();
                BindVertical();
                BindLegalEntityData();                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void Tab3_Click(object sender, EventArgs e)
        {
            try
            {                
                liImport.Attributes.Add("class", "");
                liExportCreate.Attributes.Add("class", "");
                liExportUpdate.Attributes.Add("class", "active");

                performerdocuments.Visible = false;
                performerdocuments.Attributes.Remove("class");
                performerdocuments.Attributes.Add("class", "tab-pane");

                reviewerdocuments.Visible = false;
                reviewerdocuments.Attributes.Remove("class");
                reviewerdocuments.Attributes.Add("class", "tab-pane");

                DivExportUpdate.Visible = true;
                DivExportUpdate.Attributes.Remove("class");
                DivExportUpdate.Attributes.Add("class", "tab-pane active");

                BindProcessUpdate();
                BindVerticalUpdate();
                BindLegalEntityDataUpdate();
                //BindDataExportUpdate("L");
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void GridExportExcel_DataBound(object sender, EventArgs e)
        {
            int rowCount = GridExportExcel.Rows.Count;

            if (rowCount == 0)
                btnUploadFile1.Visible = false;
            else
                btnUploadFile1.Visible = true;
        }


        #region Export (Update)

        public void BindLegalEntityDataUpdate()
        {
            long customerID = -1;            
            customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;

            ddlLegalEntityUpdate.DataTextField = "Name";
            ddlLegalEntityUpdate.DataValueField = "ID";
            ddlLegalEntityUpdate.Items.Clear();
            ddlLegalEntityUpdate.DataSource = AuditKickOff_NewDetails.FillLegalEntityData(customerID);
            ddlLegalEntityUpdate.DataBind();
            ddlLegalEntityUpdate.Items.Insert(0, new ListItem("Unit", "-1"));
        }

        public void BindSubEntityDataUpdate(DropDownList DRP, int ParentId)
        {
            long customerID = -1;            
            customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;

            DRP.DataTextField = "Name";
            DRP.DataValueField = "ID";
            DRP.Items.Clear();
            DRP.DataSource = AuditKickOff_NewDetails.FillSubEntityData(ParentId, customerID);
            DRP.DataBind();
            DRP.Items.Insert(0, new ListItem("Sub Unit", "-1"));
        }

        public void BindVerticalUpdate()
        {
            try
            {
                long customerID = -1;                
                customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;

                ddlVerticalUpdate.DataTextField = "VerticalName";
                ddlVerticalUpdate.DataValueField = "ID";
                ddlVerticalUpdate.Items.Clear();
                ddlVerticalUpdate.DataSource = UserManagementRisk.GetAllVertical(customerID);
                ddlVerticalUpdate.DataBind();
                ddlVerticalUpdate.Items.Insert(0, new ListItem("Select Vertical", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindProcessUpdate()
        {
            int CustomerBranchId = -1;
            if (!string.IsNullOrEmpty(ddlLegalEntityUpdate.SelectedValue))
            {
                if (ddlLegalEntityUpdate.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlLegalEntityUpdate.SelectedValue);

                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity1Update.SelectedValue))
            {
                if (ddlSubEntity1Update.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlSubEntity1Update.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity2Update.SelectedValue))
            {
                if (ddlSubEntity2Update.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlSubEntity2Update.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity3Update.SelectedValue))
            {
                if (ddlSubEntity3Update.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlSubEntity3Update.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlLocationUpdate.SelectedValue))
            {
                if (ddlLocationUpdate.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlLocationUpdate.SelectedValue);
                }
            }

            long customerID = -1;            
            customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;

            ddlProcessUpdate.DataTextField = "Name";
            ddlProcessUpdate.DataValueField = "ID";
            ddlProcessUpdate.Items.Clear();
            ddlProcessUpdate.DataSource = ProcessManagement.FillProcess("P", customerID);            
            ddlProcessUpdate.DataBind();
            ddlProcessUpdate.Items.Insert(0, new ListItem("Process", "-1"));
        }


        private void BindSubProcessUpdate(long Processid, string flag)
        {
            try
            {
                if (flag == "P")
                {
                    ddlSubProcessUpdate.Items.Clear();
                    ddlSubProcessUpdate.DataTextField = "Name";
                    ddlSubProcessUpdate.DataValueField = "Id";
                    ddlSubProcessUpdate.DataSource = ProcessManagement.FillSubProcess(Processid, flag);
                    ddlSubProcessUpdate.DataBind();
                }
                else
                {
                    ddlSubProcessUpdate.Items.Clear();
                    ddlSubProcessUpdate.DataTextField = "Name";
                    ddlSubProcessUpdate.DataValueField = "Id";
                    ddlSubProcessUpdate.DataSource = ProcessManagement.FillSubProcess(Processid, flag);
                    ddlSubProcessUpdate.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlLegalEntityUpdate_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlLegalEntityUpdate.SelectedValue != "-1")
            {
                //BindProcess();
                BindSubEntityData(ddlSubEntity1Update, Convert.ToInt32(ddlLegalEntityUpdate.SelectedValue));               
                //BindDataExportUpdate("L");                
            }
            else
            {
                if (ddlSubEntity1Update.Items.Count > 0)
                    ddlSubEntity1Update.Items.Clear();

                if (ddlSubEntity2Update.Items.Count > 0)
                    ddlSubEntity2Update.Items.Clear();

                if (ddlSubEntity3Update.Items.Count > 0)
                    ddlSubEntity3Update.Items.Clear();

                if (ddlLocationUpdate.Items.Count > 0)
                    ddlLocationUpdate.Items.Clear();
            }
        }

        protected void ddlSubEntity1Update_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity1Update.SelectedValue != "-1")
            {
                //BindProcess();
                BindSubEntityData(ddlSubEntity2Update, Convert.ToInt32(ddlSubEntity1Update.SelectedValue));

                //BindDataExportUpdate("L");                
            }
            else
            {
                if (ddlSubEntity2Update.Items.Count > 0)
                    ddlSubEntity2Update.ClearSelection();

                if (ddlSubEntity3Update.Items.Count > 0)
                    ddlSubEntity3Update.ClearSelection();

                if (ddlLocationUpdate.Items.Count > 0)
                    ddlLocationUpdate.Items.Clear();
            }
        }

        protected void ddlSubEntity2Update_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity2.SelectedValue != "-1")
            {
                //BindProcess();
                BindSubEntityData(ddlSubEntity3Update, Convert.ToInt32(ddlSubEntity2Update.SelectedValue));
               
                //BindDataExportUpdate("L");                
            }
            else
            {
                if (ddlSubEntity3Update.Items.Count > 0)
                    ddlSubEntity3Update.ClearSelection();

                if (ddlLocationUpdate.Items.Count > 0)
                    ddlLocationUpdate.Items.Clear();
            }
        }

        protected void ddlSubEntity3Update_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity3Update.SelectedValue != "-1")
            {
                //BindProcess();
                BindSubEntityData(ddlLocationUpdate, Convert.ToInt32(ddlSubEntity3Update.SelectedValue));

                //BindDataExportUpdate("L");
            }
            else
            {
                if (ddlLocationUpdate.Items.Count > 0)
                    ddlLocationUpdate.Items.Clear();
            }

        }

        protected void ddlLocationUpdate_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlLocationUpdate.SelectedValue != null)
            {
                if (ddlLocationUpdate.SelectedValue != "-1")
                {
                    //BindProcess();                 

                    //BindDataExportUpdate("L");
                }
            }
        }

        protected void ddlProcessUpdate_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlProcessUpdate.SelectedValue))
            {
                if (ddlProcessUpdate.SelectedValue != "-1")
                {                   
                    BindSubProcessUpdate(Convert.ToInt32(ddlProcessUpdate.SelectedValue), "P");
                    //BindDataExportUpdate("P");                    
                }
                else
                {
                    ddlSubProcessUpdate.Items.Clear();
                }
            }
            else
            {
                ddlSubProcessUpdate.Items.Clear();
            }
        }

        protected void ddlSubProcessUpdate_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlProcessUpdate.SelectedValue != null)
            {
                if (ddlProcessUpdate.SelectedValue != "Select Process")
                {
                    //BindDataExportUpdate("P");                    
                }
            }
        }

        private void BindDataExportUpdate(string Flag)
        {
            try
            {
                long customerID = -1;                
                customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;

                int processid = -1;
                int subProcessid = -1;
                int verticalid = -1;
                int CustomerBranchId = -1;                
                
                if (!string.IsNullOrEmpty(ddlLegalEntityUpdate.SelectedValue))
                {
                    if (ddlLegalEntityUpdate.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlLegalEntityUpdate.SelectedValue);
                    }
                }

                if (!string.IsNullOrEmpty(ddlSubEntity1Update.SelectedValue))
                {
                    if (ddlSubEntity1Update.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity1Update.SelectedValue);
                    }
                }

                if (!string.IsNullOrEmpty(ddlSubEntity2Update.SelectedValue))
                {
                    if (ddlSubEntity2Update.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity2Update.SelectedValue);
                    }
                }

                if (!string.IsNullOrEmpty(ddlSubEntity3Update.SelectedValue))
                {
                    if (ddlSubEntity3Update.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity3Update.SelectedValue);
                    }
                }

                if (!string.IsNullOrEmpty(ddlLocationUpdate.SelectedValue))
                {
                    if (ddlLocationUpdate.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlLocationUpdate.SelectedValue);
                    }
                }

                if (ddlProcessUpdate.SelectedValue != "")
                {
                    if (ddlProcessUpdate.SelectedValue != "-1")
                    {
                        processid = Convert.ToInt32(ddlProcessUpdate.SelectedValue);
                    }
                }

                if (ddlSubProcessUpdate.SelectedValue != "")
                {
                    if (ddlSubProcessUpdate.SelectedValue != "-1")
                    {
                        subProcessid = Convert.ToInt32(ddlSubProcessUpdate.SelectedValue);
                    }
                }             
                if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 0)
                {
                    int vid = UserManagementRisk.VerticalgetBycustomerid(Portal.Common.AuthenticationHelper.CustomerID);
                    if (vid != -1)
                    {
                        verticalid = vid;
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(ddlVerticalUpdate.SelectedValue))
                    {
                        if (ddlVerticalUpdate.SelectedValue != "-1")
                        {
                            verticalid = Convert.ToInt32(ddlVerticalUpdate.SelectedValue);
                        }
                    }
                }
                Branchlist.Clear();
                GetAllHierarchy(customerID, CustomerBranchId);

                if (Flag == "L")
                {
                    var Riskcategorymanagementlist = AdditionalRiskCreation.GetAuditStepExportForUpdate(customerID, Branchlist, processid, subProcessid, verticalid, "", "P");
                    GridExportUpdate.DataSource = Riskcategorymanagementlist;
                    Session["TotalRows"] = Riskcategorymanagementlist.Count;                   
                    GridExportUpdate.DataBind();

                    Session["grdAuditStepUpdate"] = (GridExportUpdate.DataSource as List<AuditStepExportFillView>).ToDataTable();
                }
                else if (Flag == "P")
                {
                    var Riskcategorymanagementlist = AdditionalRiskCreation.GetAuditStepExportForUpdate(customerID, Branchlist, processid, subProcessid, verticalid, "", "P");
                    GridExportUpdate.DataSource = Riskcategorymanagementlist;
                    Session["TotalRows"] = Riskcategorymanagementlist.Count;
                    GridExportUpdate.DataBind();

                    Session["grdAuditStepUpdate"] = (GridExportUpdate.DataSource as List<AuditStepExportFillView>).ToDataTable();
                }
            }

            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private List<sp_AuditStepExport_Result> GetDataExportUpdate(string Flag)
        {
            int CustomerBranchId = -1;
            int processid = -1;
            int subProcessid = -1;            
            int verticalid = -1;

            long customerID = -1;            
            customerID =Common.AuthenticationHelper.CustomerID;
            List<sp_AuditStepExport_Result> AuditStepList = new List<sp_AuditStepExport_Result>();

            if (!string.IsNullOrEmpty(ddlLegalEntityUpdate.SelectedValue))
            {
                if (ddlLegalEntityUpdate.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlLegalEntityUpdate.SelectedValue);
                }
            }

            if (!string.IsNullOrEmpty(ddlSubEntity1Update.SelectedValue))
            {
                if (ddlSubEntity1Update.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlSubEntity1Update.SelectedValue);
                }
            }

            if (!string.IsNullOrEmpty(ddlSubEntity2Update.SelectedValue))
            {
                if (ddlSubEntity2Update.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlSubEntity2Update.SelectedValue);
                }
            }

            if (!string.IsNullOrEmpty(ddlSubEntity3Update.SelectedValue))
            {
                if (ddlSubEntity3Update.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlSubEntity3Update.SelectedValue);
                }
            }

            if (!string.IsNullOrEmpty(ddlLocationUpdate.SelectedValue))
            {
                if (ddlLocationUpdate.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlLocationUpdate.SelectedValue);
                }
            }

            if (ddlProcessUpdate.SelectedValue != "")
            {
                if (ddlProcessUpdate.SelectedValue != "-1")
                {
                    processid = Convert.ToInt32(ddlProcessUpdate.SelectedValue);
                }
            }

            if (ddlSubProcessUpdate.SelectedValue != "")
            {
                if (ddlSubProcessUpdate.SelectedValue != "-1")
                {
                    subProcessid = Convert.ToInt32(ddlSubProcessUpdate.SelectedValue);
                }
            }           
            if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 0)
            {
                int vid = UserManagementRisk.VerticalgetBycustomerid(Portal.Common.AuthenticationHelper.CustomerID);
                if (vid != -1)
                {
                    verticalid = vid;
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(ddlVertical.SelectedValue))
                {
                    if (ddlVertical.SelectedValue != "-1")
                    {
                        verticalid = Convert.ToInt32(ddlVertical.SelectedValue);
                    }
                }
            }
            Branchlist.Clear();
            GetAllHierarchy(customerID, CustomerBranchId);

            if (Flag == "L")
            {
                AuditStepList = AdditionalRiskCreation.GetAuditStepExportForUpdate(customerID, Branchlist, processid, subProcessid, verticalid,"", "P");                
            }
            else if (Flag == "P")
            {
                AuditStepList = AdditionalRiskCreation.GetAuditStepExportForUpdate(customerID, Branchlist, processid, subProcessid, verticalid,"", "P");                
            }

            return AuditStepList;
        }

        protected void btnExportUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("AuditStepExportForUpdate");
                    DataTable ExcelData = null;

                    var AuditStepList = GetDataExportUpdate("P");

                    DataView view = new System.Data.DataView(AuditStepList.ToDataTable());
                    //ExcelData = view.ToTable("Selected", false, "BranchName", "VerticalName", "ProcessName", "SubProcessName", "ControlNo", "ActivityDescription", "ControlDescription", "RiskRating", "AuditObjective", "ActivityTobeDone", "Analyis_To_Be_Performed", "Pre_Requisite_List", "AuditStepRating", "ATBDId", "checklistdocid");
                    ExcelData = view.ToTable("Selected", false, "BranchName", "VerticalName", "ProcessName", "SubProcessName", "ControlNo", "ActivityDescription", "ControlDescription", "RiskRating", "AuditObjective", "ActivityTobeDone", "Analyis_To_Be_Performed", "Pre_Requisite_List", "ATBDId", "checklistdocid");

                    exWorkSheet.Cells["A1"].LoadFromDataTable(ExcelData, true);

                    exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["A1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["A1"].Value = "Location";
                    exWorkSheet.Cells["A1"].AutoFitColumns(20);

                    exWorkSheet.Cells["B1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["B1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["B1"].Value = "Vertical";
                    exWorkSheet.Cells["B1"].AutoFitColumns(20);

                    exWorkSheet.Cells["C1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["C1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["C1"].Value = "Process";
                    exWorkSheet.Cells["C1"].AutoFitColumns(20);

                    exWorkSheet.Cells["D1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["D1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["D1"].Value = "Sub Process";
                    exWorkSheet.Cells["D1"].AutoFitColumns(20);

                    exWorkSheet.Cells["E1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["E1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["E1"].Value = "Control No";
                    exWorkSheet.Cells["E1"].AutoFitColumns(15);

                    exWorkSheet.Cells["F1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["F1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["F1"].Value = "Risk Description";
                    exWorkSheet.Cells["F1"].AutoFitColumns(50);

                    exWorkSheet.Cells["G1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["G1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["G1"].Value = "Control Description";
                    exWorkSheet.Cells["G1"].AutoFitColumns(50);

                    exWorkSheet.Cells["H1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["H1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["H1"].Value = "Risk Rating";
                    exWorkSheet.Cells["H1"].AutoFitColumns(15);

                    exWorkSheet.Cells["I1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["I1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["I1"].Value = "Audit Methodology";
                    exWorkSheet.Cells["I1"].AutoFitColumns(50);

                    exWorkSheet.Cells["J1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["J1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["J1"].Value = "Audit Steps";
                    exWorkSheet.Cells["J1"].AutoFitColumns(50);

                    exWorkSheet.Cells["K1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["K1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["K1"].Value = "Analyis To Be Performed";
                    exWorkSheet.Cells["K1"].AutoFitColumns(50);

                    exWorkSheet.Cells["L1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["L1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["L1"].Value = "Pre-Requisite List";
                    exWorkSheet.Cells["L1"].AutoFitColumns(50);

                    //exWorkSheet.Cells["M1"].Style.Font.Bold = true;
                    //exWorkSheet.Cells["M1"].Style.Font.Size = 12;
                    //exWorkSheet.Cells["M1"].Value = "Audit Step Rating";
                    //exWorkSheet.Cells["M1"].AutoFitColumns(10);

                    exWorkSheet.Cells["M1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["M1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["M1"].Value = "ID";
                    exWorkSheet.Cells["M1"].AutoFitColumns(10);

                    exWorkSheet.Cells["N1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["N1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["N1"].Value = "CDOID";
                    exWorkSheet.Cells["N1"].AutoFitColumns(10);
                    

                    using (ExcelRange col = exWorkSheet.Cells[1, 1, 1 + ExcelData.Rows.Count, 14])
                    {
                        col.Style.WrapText = true;                        
                        col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                                
                        // Assign borders
                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    }

                    
                    Byte[] fileBytes = exportPackge.GetAsByteArray();
                    Response.ClearContent();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=AuditStepExportForUpdate.xlsx");
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.ms-excel";
                    StringWriter sw = new StringWriter();
                    Response.BinaryWrite(fileBytes);
                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);                
            }
        }

        #endregion
    }
}