﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Account
{
    public partial class UploadRiskCreationWithExcelsheet : System.Web.UI.Page
    {
        public List<long> locationList = new List<long>();
        public List<long> Branchlist = new List<long>();
        bool sucess = false;
        protected static int CustomerId = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                locationList.Clear();
                BindUploadedHistoryGridView(CustomerId);
                bindPageNumber();
            }
        }

        protected void btnUploadRCM_Click(object sender, EventArgs e)
        {
            if (fileUploadRCM.HasFile)
            {
                try
                {
                    string strfileName = Path.GetFileName(fileUploadRCM.FileName);
                    string filename = Guid.NewGuid() + strfileName;
                    fileUploadRCM.SaveAs(Server.MapPath("~/HistoricalObsUploaded/") + filename.Trim());
                    FileInfo excelfile = new FileInfo(Server.MapPath("~/HistoricalObsUploaded/") + filename.Trim());
                    if (excelfile != null)
                    {
                        using (ExcelPackage xlWorkbook = new ExcelPackage(excelfile))
                        {
                            bool flag = RiskTransactionSheetsExitsts(xlWorkbook, "AuditObservations");
                            if (flag == true)
                            {
                                Session["historicalObservationFileName"] = strfileName;
                                string filepath = "~/HistoricalObsUploaded/" + filename + "";
                                Session["historicalObservationFilePath"] = filepath;
                                ProcessRiskCategoryDataNew(xlWorkbook);
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "No Data Found in Excel Document or Sheet Name must be 'AuditObservations'.";
                            }
                        }
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Error uploading file. Please try again.";
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                }
            }
            else
            {
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Please upload file.";
            }
        }

        private bool RiskTransactionSheetsExitsts(ExcelPackage xlWorkbook, string data)
        {
            try
            {
                bool flag = false;
                foreach (ExcelWorksheet sheet in xlWorkbook.Workbook.Worksheets)
                {
                    if (data.Equals("AuditObservations"))
                    {
                        if (sheet.Name.Trim().Equals("AuditObservations") || sheet.Name.Trim().Equals("AuditObservations") || sheet.Name.Trim().Equals("AuditObservations"))
                        {
                            flag = true;
                            break;//added by Manisha
                        }
                        else
                        {
                            flag = false;
                            break;
                        }
                    }
                }
                return flag;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        private void BindLocationFilter()
        {
            try
            {
                long customerID = -1;
                customerID = Common.AuthenticationHelper.CustomerID;
                tvFilterLocation.Nodes.Clear();
                var bracnhes = CustomerBranchManagement.GetAllHierarchySatutory(customerID);
                TreeNode node = new TreeNode();
                foreach (var item in bracnhes)
                {
                    node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    CustomerBranchManagementRisk.BindBranchesHierarchy(node, item);
                    tvFilterLocation.Nodes.Add(node);
                }
                tvFilterLocation.CollapseAll();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void RetrieveNodes(TreeNode node)
        {
            if (node.Checked && node.ChildNodes.Count == 0) // && node.ChildNodes.Count == 0 if (node.Checked)
            {
                if (!locationList.Contains(Convert.ToInt32(node.Value)))
                    locationList.Add(Convert.ToInt32(node.Value));
            }

            foreach (TreeNode tn in node.ChildNodes)
            {
                if (tn.Checked && tn.ChildNodes.Count == 0)//  && tn.ChildNodes.Count == 0if (tn.Checked)
                {
                    if (!locationList.Contains(Convert.ToInt32(tn.Value)))
                        locationList.Add(Convert.ToInt32(tn.Value));
                }

                if (tn.ChildNodes.Count != 0)
                {
                    for (int i = 0; i < tn.ChildNodes.Count; i++)
                    {
                        RetrieveNodes(tn.ChildNodes[i]);
                    }
                }
            }
        }

        private void ProcessRiskCategoryDataNew(ExcelPackage xlWorkbook)
        {
            List<long> LocationList = new List<long>();
            bool finalSuccess = false;
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    ExcelWorksheet xlWorksheetvalidation = xlWorkbook.Workbook.Worksheets["AuditObservations"];
                    List<string> errorMessage = new List<string>();
                    if (xlWorksheetvalidation != null)
                    {
                        #region xlWorksheetvalidation  != null
                        if (GetDimensionRows(xlWorksheetvalidation) != 0)
                        {
                            int xlrow2 = xlWorksheetvalidation.Dimension.End.Row;
                            List<int> Verticallist = new List<int>();
                            Verticallist.Clear();
                            int CustomerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);

                            #region Validations

                            string valobjective_ref = string.Empty;
                            string valProcessName = string.Empty;
                            string valSubProcessName = string.Empty;
                            string valActivity = string.Empty;
                            string valactivitydescription = string.Empty;

                            string valProcessOwner = string.Empty;
                            string valProcessOwnerEmail = string.Empty;
                            string valControlOwner = string.Empty;
                            string valControlOwnerEmail = string.Empty;
                            string valstringeffectivedate = string.Empty;
                            string valPreventiveControl = string.Empty;
                            string valAutomatedControl = string.Empty;
                            string valFrequency = string.Empty;
                            string valERPsystem = string.Empty;
                            string valFraudRiskControl = string.Empty;
                            string valRiskRating = string.Empty;
                            string valControlRating = string.Empty;
                            string valAuditStep = string.Empty;
                            string valAssertions = string.Empty;

                            string valBranchName = string.Empty;
                            int valprocessid = -1;
                            int valsubprocessid = -1;
                            int valactivityid = -1;
                            string valVerticalName = string.Empty;
                            string valFinancialYear = string.Empty;
                            int valBranchId = -1;
                            string valPeriod = string.Empty;
                            string valPerformerEmail = string.Empty;
                            int valPerformerUserId = -1;
                            string valReviewerEmail = string.Empty;
                            int valReviewerUserId = -1;
                            string valReviewer2Email = string.Empty;
                            int valReviewer2UserId = -1;
                            string valAuditScheduleStartDate = string.Empty;
                            string valAuditScheduleEndDate = string.Empty;
                            string valAuditStartDate = string.Empty;
                            string valAuditEndDate = string.Empty;
                            string valActivityToBeDone = string.Empty;
                            string valObjectiveRef = string.Empty;
                            string valRisk = string.Empty;
                            string valControlObjective = string.Empty;
                            string valControlDescription = string.Empty;
                            string valAuditMethodology = string.Empty;

                            string valProcessWalkthrough = string.Empty;
                            string valActualWorkDone = string.Empty;
                            string valPopulation = string.Empty;
                            string valSample = string.Empty;
                            string valRemark = string.Empty;
                            string valObservationTitle = string.Empty;
                            string valObservation = string.Empty;
                            string valBriefObservation = string.Empty;
                            string valObservationBackgroud = string.Empty;
                            string valBusinessImplication = string.Empty;
                            string valRootCause = string.Empty;
                            string valFinancialImpact = string.Empty;
                            string valRecomendation = string.Empty;
                            string valManagementResponse = string.Empty;
                            string valTimeLine = string.Empty;
                            string valPersonResponsible = string.Empty;
                            string valPersonResponsibleEmail = string.Empty;
                            string valOwnerEmail = string.Empty;
                            string valObservationRating = string.Empty;
                            string valObservationCategory = string.Empty;
                            int valObservationCategoryId = -1;
                            string valObservationSubCategory = string.Empty;
                            string valStatus = string.Empty;

                            bool compareSchedulingStartDate = false;
                            bool compareSchedulingEndDate = false;
                            bool compareAuditStartDates = false;
                            bool compareAuditEndDates = false;
                            for (int rowNum = 2; rowNum <= xlrow2; rowNum++)
                            {
                                valobjective_ref = string.Empty;
                                valProcessName = string.Empty;
                                valSubProcessName = string.Empty;
                                valActivity = string.Empty;
                                valactivitydescription = string.Empty;
                                valProcessOwner = string.Empty;
                                valProcessOwnerEmail = string.Empty;
                                valControlOwner = string.Empty;
                                valControlOwnerEmail = string.Empty;
                                valstringeffectivedate = string.Empty;
                                valPreventiveControl = string.Empty;
                                valAutomatedControl = string.Empty;
                                valFrequency = string.Empty;
                                valERPsystem = string.Empty;
                                valFraudRiskControl = string.Empty;
                                valRiskRating = string.Empty;
                                valControlRating = string.Empty;
                                valAuditStep = string.Empty;

                                valBranchName = string.Empty;
                                valVerticalName = string.Empty;
                                valFinancialYear = string.Empty;
                                valprocessid = -1;
                                valsubprocessid = -1;
                                valactivityid = -1;
                                valBranchId = -1;
                                valPeriod = string.Empty;
                                valPerformerEmail = string.Empty;
                                valPerformerUserId = -1;
                                valReviewerEmail = string.Empty;
                                valReviewerUserId = -1;
                                valReviewer2Email = string.Empty;
                                valReviewer2UserId = -1;
                                valAuditScheduleStartDate = string.Empty;
                                valAuditScheduleEndDate = string.Empty;
                                valAuditStartDate = string.Empty;
                                valAuditEndDate = string.Empty;
                                valActivityToBeDone = string.Empty;
                                valObjectiveRef = string.Empty;
                                valRisk = string.Empty;
                                valControlObjective = string.Empty;
                                valControlDescription = string.Empty;
                                valAuditMethodology = string.Empty;

                                valProcessWalkthrough = string.Empty;
                                valActualWorkDone = string.Empty;
                                valPopulation = string.Empty;
                                valSample = string.Empty;
                                valRemark = string.Empty;
                                valObservationTitle = string.Empty;
                                valObservation = string.Empty;
                                valBriefObservation = string.Empty;
                                valObservationBackgroud = string.Empty;
                                valBusinessImplication = string.Empty;
                                valRootCause = string.Empty;
                                valFinancialImpact = string.Empty;
                                valRecomendation = string.Empty;
                                valManagementResponse = string.Empty;
                                valTimeLine = string.Empty;
                                valPersonResponsible = string.Empty;
                                valPersonResponsibleEmail = string.Empty;
                                valOwnerEmail = string.Empty;
                                valObservationRating = string.Empty;
                                valObservationCategory = string.Empty;
                                valObservationSubCategory = string.Empty;
                                valStatus = string.Empty;

                                #region Branch
                                if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 1].Text.ToString().Trim()))
                                {
                                    valBranchName = xlWorksheetvalidation.Cells[rowNum, 1].Text.Trim();
                                }
                                if (string.IsNullOrEmpty(valBranchName))
                                {
                                    errorMessage.Add("Required Branch Name at row number-" + rowNum);
                                }
                                if (!String.IsNullOrEmpty(valBranchName))
                                {
                                    bool checkBranchNameExistance = CustomerBranchManagement.ExistsBranch(valBranchName, CustomerID);
                                    if (checkBranchNameExistance == false && !String.IsNullOrEmpty(valBranchName))
                                    {
                                        CustomerBranch customerBranch = new CustomerBranch()
                                        {
                                            CustomerID = CustomerID,
                                            Name = valBranchName,
                                            Type = 1,
                                            StateID = 20,
                                            CityID = 2,
                                            IsDeleted = false,
                                            CreatedOn = DateTime.Now,
                                            AuditPR = true,
                                            Status = 1,
                                        };
                                        CustomerBranchManagement.Create(customerBranch);
                                    }

                                    bool checkBranchNameExistanceInAuditDb = CustomerBranchManagement.ExistsBranchInAuditDb(valBranchName, CustomerID);
                                    if (checkBranchNameExistanceInAuditDb == false && !String.IsNullOrEmpty(valBranchName))
                                    {
                                        mst_CustomerBranch customerBranch = new mst_CustomerBranch()
                                        {
                                            CustomerID = CustomerID,
                                            Name = valBranchName,
                                            Type = 1,
                                            StateID = 20,
                                            CityID = 2,
                                            IsDeleted = false,
                                            CreatedOn = DateTime.Now,
                                            Status = 1,
                                            AuditPR = true
                                        };
                                        CustomerBranchManagement.Create1(customerBranch);
                                    }

                                    valBranchId = CustomerBranchManagement.GetByNameFromAuditDb(valBranchName, CustomerID).ID;

                                    if (valBranchId == 0 || valBranchId == -1)
                                    {
                                        errorMessage.Add("Branch Name not defined in the system at row number-" + rowNum);
                                    }
                                }
                                #endregion

                                #region vertical 
                                if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 2].Text.ToString().Trim()))
                                {
                                    valVerticalName = xlWorksheetvalidation.Cells[rowNum, 2].Text.Trim();
                                }
                                if (!string.IsNullOrEmpty(valVerticalName))
                                {
                                    mst_Vertical objVerti = new mst_Vertical()
                                    {
                                        VerticalName = valVerticalName,
                                        CustomerID = CustomerID
                                    };
                                    if (!UserManagementRisk.VerticalNameExist(objVerti))
                                    {
                                        UserManagementRisk.CreateVerticalName(objVerti);
                                    }
                                }
                                else
                                {
                                    mst_Vertical objVerti = new mst_Vertical()
                                    {
                                        VerticalName = "NA",
                                        CustomerID = CustomerID
                                    };
                                    if (!UserManagementRisk.VerticalNameExist(objVerti))
                                    {
                                        UserManagementRisk.CreateVerticalName(objVerti);
                                    }
                                }
                                #endregion

                                #region ProcessName
                                if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 3].Text.ToString().Trim()))
                                {
                                    valProcessName = xlWorksheetvalidation.Cells[rowNum, 3].Text.Trim();
                                }
                                if (string.IsNullOrEmpty(valProcessName))
                                {
                                    errorMessage.Add("Required ProcessName  at row number-" + rowNum);
                                }
                                else
                                {
                                    if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 3].Text.ToString().Trim()))
                                    {
                                        string processName = Regex.Replace(xlWorksheetvalidation.Cells[rowNum, 3].Text.ToString().Trim(), @"\t|\n|\r", "");
                                        var checkProcessNameExistance = ProcessManagement.Exists(CustomerID, processName);
                                        //if (checkProcessNameExistance == false && !String.IsNullOrEmpty(processName))
                                        //{
                                        //    Mst_Process mstProcess = new Mst_Process()
                                        //    {
                                        //        Name = processName,
                                        //        CreatedBy = Common.AuthenticationHelper.UserID,
                                        //        UpdatedBy = Common.AuthenticationHelper.UserID,
                                        //        CreatedOn = DateTime.Now,
                                        //        UpdatedOn = DateTime.Now,
                                        //        IsDeleted = false,
                                        //        IsProcessNonProcess = "P",
                                        //        CustomerID = CustomerID
                                        //    };
                                        //    ProcessManagement.Create(mstProcess);
                                        //}
                                        if (checkProcessNameExistance)
                                        {
                                            valprocessid = ProcessManagement.GetProcessIDByName(processName, CustomerID);
                                            if (valprocessid == 0 || valprocessid == -1)
                                            {
                                                errorMessage.Add("Process Name not defined in the system at row number-" + rowNum);
                                            }
                                        }
                                        else
                                        {
                                            errorMessage.Add("Process Name not exists in the system at row number-" + rowNum + ". Please create process in system");
                                        }
                                    }

                                }
                                #endregion

                                #region SubProcessName
                                if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 4].Text.ToString().Trim()))
                                {
                                    valSubProcessName = xlWorksheetvalidation.Cells[rowNum, 4].Text.Trim();
                                }
                                if (string.IsNullOrEmpty(valSubProcessName))
                                {
                                    errorMessage.Add("Required SubProcessName at row number-" + rowNum);
                                }
                                else
                                {
                                    if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 4].Text.ToString().Trim()) && valprocessid != -1 && valprocessid != 0)
                                    {
                                        string subProcessName = Regex.Replace(xlWorksheetvalidation.Cells[rowNum, 4].Text.ToString().Trim(), @"\t|\n|\r", "");
                                        bool checkSubProcessExistance = ProcessManagement.ExistsProcess(valprocessid, subProcessName);
                                        if (checkSubProcessExistance == false && !String.IsNullOrEmpty(subProcessName))
                                        {
                                            mst_Subprocess subProcess = new mst_Subprocess()
                                            {
                                                Name = subProcessName,
                                                ProcessId = valprocessid,
                                                CreatedBy = Common.AuthenticationHelper.UserID,
                                                UpdatedBy = Common.AuthenticationHelper.UserID,
                                                CreatedOn = DateTime.Now,
                                                UpdatedOn = DateTime.Now,
                                                IsDeleted = false,
                                                IsProcessNonProcess = "P",
                                            };
                                            ProcessManagement.SubprocessCreate(subProcess);
                                        }
                                        valsubprocessid = ProcessManagement.GetSubProcessIDByName(subProcessName, valprocessid);
                                        if (valsubprocessid == 0 || valsubprocessid == -1)
                                        {
                                            errorMessage.Add("SubProcessName not defined in the system at row number-" + rowNum);
                                        }
                                    }

                                }
                                #endregion

                                #region Activity
                                if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 5].Text.ToString().Trim()))
                                {
                                    valActivity = xlWorksheetvalidation.Cells[rowNum, 5].Text.Trim();
                                }
                                if (!String.IsNullOrEmpty(valActivity))
                                {
                                    bool isExists = IsExistsmst_Activity(valprocessid, valsubprocessid, valActivity);
                                    if (!isExists)
                                    {
                                        mst_Activity mst_Activity = new mst_Activity()
                                        {
                                            Name = valActivity,
                                            ProcessId = valprocessid,
                                            SubProcessId = valsubprocessid,
                                            CreatedBy = Portal.Common.AuthenticationHelper.UserID,
                                            CreatedOn = DateTime.Now,
                                        };
                                        Insertmst_Activity(mst_Activity);
                                    }
                                }
                                else
                                {
                                    bool isExists = IsExistsmst_Activity(valprocessid, valsubprocessid, "NA");
                                    if (!isExists)
                                    {
                                        mst_Activity mst_Activity = new mst_Activity()
                                        {
                                            Name = "NA",
                                            ProcessId = valprocessid,
                                            SubProcessId = valsubprocessid,
                                            CreatedBy = Portal.Common.AuthenticationHelper.UserID,
                                            CreatedOn = DateTime.Now,
                                        };
                                        Insertmst_Activity(mst_Activity);
                                    }
                                }
                                #endregion

                                #region FinancialYear
                                if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 6].Text.ToString().Trim()))
                                {
                                    valFinancialYear = xlWorksheetvalidation.Cells[rowNum, 6].Text.ToString().Trim();
                                }
                                if (string.IsNullOrEmpty(valFinancialYear))
                                {
                                    errorMessage.Add("Required Financial Year at row number-" + rowNum);
                                }
                                if (valFinancialYear.Length <= 8)
                                {
                                    errorMessage.Add("Required Financial Year format '2020-2021' at row number-" + rowNum);
                                }
                                else
                                {
                                    if (!String.IsNullOrEmpty(valFinancialYear))
                                    {
                                        string[] financialYearArray = valFinancialYear.Split('-');
                                        string financialYear = financialYearArray[0].Trim().ToString();
                                        string financialYear1 = financialYearArray[1].Trim().ToString();
                                        int count = 0;
                                        int count1 = 0;
                                        foreach (char c in financialYear)
                                        {
                                            count++;
                                        }
                                        foreach (char c1 in financialYear1)
                                        {
                                            count1++;
                                        }
                                        if (count < 4 || count1 < 4)
                                        {
                                            errorMessage.Add("Invalid FinancialYear at row number-" + rowNum);
                                        }
                                        else
                                        {
                                            int CheckFirstFY = Convert.ToInt32(financialYear) + 1;
                                            int CheckSecondFY = Convert.ToInt32(financialYear1);
                                            if (CheckFirstFY != CheckSecondFY)
                                            {
                                                errorMessage.Add("Required Financial Year format '2020-2021' at row number-" + rowNum);
                                            }
                                        }
                                    }
                                }
                                #endregion

                                #region Period
                                if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 7].Text.ToString().Trim()))
                                {
                                    valPeriod = xlWorksheetvalidation.Cells[rowNum, 7].Text.ToString().Trim();
                                }
                                if (string.IsNullOrEmpty(valPeriod))
                                {
                                    errorMessage.Add("Required Period at row number-" + rowNum);
                                }
                                if (!String.IsNullOrEmpty(valPeriod))
                                {
                                    string period = valPeriod.Trim().ToUpper();
                                    switch (period)
                                    {
                                        case "ANNUALLY":
                                            break;
                                        case "APR-SEP":
                                            break;
                                        case "OCT-MAR":
                                            break;
                                        case "APR-JUN":
                                            break;
                                        case "JUL-SEP":
                                            break;
                                        case "OCT-DEC":
                                            break;
                                        case "JAN-MAR":
                                            break;
                                        case "APR":
                                            break;
                                        case "MAY":
                                            break;
                                        case "JUN":
                                            break;
                                        case "JUL":
                                            break;
                                        case "AUG":
                                            break;
                                        case "SEP":
                                            break;
                                        case "OCT":
                                            break;
                                        case "NOV":
                                            break;
                                        case "DEC":
                                            break;
                                        case "JAN":
                                            break;
                                        case "FEB":
                                            break;
                                        case "MAR":
                                            break;
                                        case "APRIL":
                                            break;
                                        case "JUNE":
                                            break;
                                        case "JULY":
                                            break;
                                        case "AUGUST":
                                            break;
                                        case "SEPTEMBER":
                                            break;
                                        case "OCTOBER":
                                            break;
                                        case "NOVEMBER":
                                            break;
                                        case "DECEMBER":
                                            break;
                                        case "JANUARY":
                                            break;
                                        case "FEBRUARY":
                                            break;
                                        case "MARCH":
                                            break;
                                        default:
                                            errorMessage.Add("Invalid Period at row number-" + rowNum);
                                            break;
                                    }
                                }
                                #endregion

                                #region Performer
                                if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 8].Text.ToString().Trim()))
                                {
                                    valPerformerEmail = xlWorksheetvalidation.Cells[rowNum, 8].Text.ToString().Trim();
                                }
                                if (string.IsNullOrEmpty(valPerformerEmail))
                                {
                                    errorMessage.Add("Required Performer at row number-" + rowNum);
                                }
                                if (!String.IsNullOrEmpty(valPerformerEmail))
                                {
                                    string email = valPerformerEmail.Trim();
                                    Regex regex = new Regex(@"^([\w-_]+\.)*[\w-_]+@([\w-_]+\.)*[\w-_]+\.[\w-_]+$");
                                    Match match = regex.Match(email);
                                    if (match.Success)
                                    {
                                        bool checkUserExistance = UserManagement.UserExistsByEmail(valPerformerEmail, CustomerID);
                                        string[] performerName = valPerformerEmail.Split('@');
                                        if (checkUserExistance == false)
                                        {
                                            string userName = string.Empty;
                                            userName = Portal.Common.AuthenticationHelper.User;
                                            User user = new User()
                                            {
                                                FirstName = performerName[0].ToString(),
                                                LastName = "NA",
                                                Email = valPerformerEmail,
                                                CreatedOn = DateTime.Now,
                                                IsDeleted = false,
                                                IsActive = true,
                                                CreatedByText = userName,
                                                RoleID = 7,
                                                WrongAttempt = 0,
                                                CustomerID = CustomerID,
                                                CustomerBranchID = valBranchId,
                                                IsHead = false,
                                            };
                                            string passwordText = Util.CreateRandomPassword(10);
                                            user.Password = Util.CalculateMD5Hash(passwordText);
                                            UserManagement.CreateNewHistoryObservation(user);
                                        }

                                        bool checkPerformerExistance = UserManagement.UserExistsByEmailInAuditDb(valPerformerEmail, CustomerID);
                                        if (checkPerformerExistance == false)
                                        {
                                            string userName = string.Empty;
                                            userName = Portal.Common.AuthenticationHelper.User;
                                            mst_User user = new mst_User()
                                            {
                                                FirstName = performerName[0].ToString(),
                                                LastName = "NA",
                                                Email = valPerformerEmail,
                                                CreatedOn = DateTime.Now,
                                                IsDeleted = false,
                                                IsActive = true,
                                                CreatedByText = userName,
                                                RoleID = 7,
                                                WrongAttempt = 0,
                                                CustomerID = CustomerID,
                                                CustomerBranchID = valBranchId,
                                                IsHead = false,
                                            };
                                            string passwordText = Util.CreateRandomPassword(10);
                                            user.Password = Util.CalculateMD5Hash(passwordText);
                                            UserManagement.CreateNewHistoryObservationInAuditDb(user);
                                        }
                                        //valPerformerUserId = UserManagement.GetUserIdByEmail(valPerformerEmail, CustomerID, valBranchId);
                                        valPerformerUserId = UserManagement.GetUserIdByEmail(valPerformerEmail, CustomerID);
                                    }
                                    else
                                    {
                                        errorMessage.Add("Invalid Performer email at row number-" + rowNum);
                                    }
                                }
                                #endregion

                                #region Reviewer
                                if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 9].Text.ToString().Trim()))
                                {
                                    valReviewerEmail = xlWorksheetvalidation.Cells[rowNum, 9].Text.ToString().Trim();
                                }
                                if (string.IsNullOrEmpty(valReviewerEmail))
                                {
                                    errorMessage.Add("Required Reviewer at row number-" + rowNum);
                                }
                                if (!String.IsNullOrEmpty(valReviewerEmail))
                                {
                                    string email = valReviewerEmail.Trim();
                                    Regex regex = new Regex(@"^([\w-_]+\.)*[\w-_]+@([\w-_]+\.)*[\w-_]+\.[\w-_]+$");
                                    Match match = regex.Match(email);

                                    if (match.Success)
                                    {
                                        bool checkUserExistance = UserManagement.UserExistsByEmail(valReviewerEmail, CustomerID);
                                        string[] ReviewerName = valReviewerEmail.Split('@');
                                        if (checkUserExistance == false)
                                        {
                                            string userName = string.Empty;
                                            userName = Portal.Common.AuthenticationHelper.User;
                                            User user = new User()
                                            {
                                                FirstName = ReviewerName[0].ToString(),
                                                LastName = "NA",
                                                Email = valReviewerEmail,
                                                CreatedOn = DateTime.Now,
                                                IsDeleted = false,
                                                IsActive = true,
                                                CreatedByText = userName,
                                                RoleID = 7,
                                                WrongAttempt = 0,
                                                CustomerID = CustomerID,
                                                CustomerBranchID = valBranchId,
                                                IsHead = false,
                                            };
                                            string passwordText = Util.CreateRandomPassword(10);
                                            user.Password = Util.CalculateMD5Hash(passwordText);
                                            UserManagement.CreateNewHistoryObservation(user);
                                        }
                                        bool checkPerformerExistance = UserManagement.UserExistsByEmailInAuditDb(valReviewerEmail, CustomerID);
                                        if (checkPerformerExistance == false)
                                        {
                                            string userName = string.Empty;
                                            userName = Portal.Common.AuthenticationHelper.User;
                                            mst_User user = new mst_User()
                                            {
                                                FirstName = ReviewerName[0].ToString(),
                                                LastName = "NA",
                                                Email = valReviewerEmail,
                                                CreatedOn = DateTime.Now,
                                                IsDeleted = false,
                                                IsActive = true,
                                                CreatedByText = userName,
                                                RoleID = 7,
                                                WrongAttempt = 0,
                                                CustomerID = CustomerID,
                                                CustomerBranchID = valBranchId,
                                                IsHead = false,
                                            };
                                            string passwordText = Util.CreateRandomPassword(10);
                                            user.Password = Util.CalculateMD5Hash(passwordText);
                                            UserManagement.CreateNewHistoryObservationInAuditDb(user);
                                        }
                                        //valReviewerUserId = UserManagement.GetUserIdByEmail(valReviewerEmail, CustomerID, valBranchId);
                                        valReviewerUserId = UserManagement.GetUserIdByEmail(valReviewerEmail, CustomerID);
                                    }
                                    else
                                    {
                                        errorMessage.Add("Invalid Reviewer email at row number-" + rowNum);
                                    }
                                }

                                #endregion

                                #region Reviewer2
                                if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 10].Text.ToString().Trim()))
                                {
                                    valReviewer2Email = xlWorksheetvalidation.Cells[rowNum, 10].Text.ToString().Trim();
                                }
                                if (!String.IsNullOrEmpty(valReviewer2Email))
                                {
                                    string email = valReviewer2Email.Trim();
                                    Regex regex = new Regex(@"^([\w-_]+\.)*[\w-_]+@([\w-_]+\.)*[\w-_]+\.[\w-_]+$");
                                    Match match = regex.Match(email);

                                    if (match.Success)
                                    {
                                        bool checkUserExistance = UserManagement.UserExistsByEmail(valReviewer2Email, CustomerID);
                                        string[] ReviewerName = valReviewer2Email.Split('@');
                                        if (checkUserExistance == false)
                                        {
                                            string userName = string.Empty;
                                            userName = Portal.Common.AuthenticationHelper.User;
                                            User user = new User()
                                            {
                                                FirstName = ReviewerName[0].ToString(),
                                                LastName = "NA",
                                                Email = valReviewer2Email,
                                                CreatedOn = DateTime.Now,
                                                IsDeleted = false,
                                                IsActive = true,
                                                CreatedByText = userName,
                                                RoleID = 7,
                                                WrongAttempt = 0,
                                                CustomerID = CustomerID,
                                                CustomerBranchID = valBranchId,
                                                IsHead = false,
                                            };
                                            string passwordText = Util.CreateRandomPassword(10);
                                            user.Password = Util.CalculateMD5Hash(passwordText);
                                            UserManagement.CreateNewHistoryObservation(user);
                                        }
                                        bool checkPerformerExistance = UserManagement.UserExistsByEmailInAuditDb(valReviewer2Email, CustomerID);
                                        if (checkPerformerExistance == false)
                                        {
                                            string userName = string.Empty;
                                            userName = Portal.Common.AuthenticationHelper.User;
                                            mst_User user = new mst_User()
                                            {
                                                FirstName = ReviewerName[0].ToString(),
                                                LastName = "NA",
                                                Email = valReviewer2Email,
                                                CreatedOn = DateTime.Now,
                                                IsDeleted = false,
                                                IsActive = true,
                                                CreatedByText = userName,
                                                RoleID = 7,
                                                WrongAttempt = 0,
                                                CustomerID = CustomerID,
                                                CustomerBranchID = valBranchId,
                                                IsHead = false,
                                            };
                                            string passwordText = Util.CreateRandomPassword(10);
                                            user.Password = Util.CalculateMD5Hash(passwordText);
                                            UserManagement.CreateNewHistoryObservationInAuditDb(user);
                                        }
                                        valReviewer2UserId = UserManagement.GetUserIdByEmail(valReviewer2Email, CustomerID);
                                    }
                                    else
                                    {
                                        errorMessage.Add("Invalid Reviewer 2 email at row number-" + rowNum);
                                    }
                                }

                                #endregion

                                #region Audit Schedule Start Date
                                if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 11].Text.ToString().Trim()))
                                {
                                    valAuditScheduleStartDate = xlWorksheetvalidation.Cells[rowNum, 11].Text.Trim();
                                }
                                if (string.IsNullOrEmpty(valAuditScheduleStartDate))
                                {
                                    errorMessage.Add("Required Audit Schedule Start Date at row number-" + rowNum);
                                }
                                if (!String.IsNullOrEmpty(valAuditScheduleStartDate))
                                {
                                    try
                                    {

                                        //var val = Convert.ToDateTime(valAuditScheduleStartDate).ToString("dd/MM/yyyy");

                                        ////string inputString = "2000-02-02";

                                        // added on 06-04-2020
                                        //DateTime dDate;

                                        //if (DateTime.TryParse(valAuditScheduleStartDate, out dDate))
                                        //{
                                        //    String.Format("{0:d/MM/yyyy}", dDate);
                                        //}
                                        //else
                                        //{
                                        //    errorMessage.Add("Invalid Audit Schedule Start Date at row number-" + rowNum);
                                        //}

                                        //var dummy = DateTimeExtensions.GetDate(val);

                                        //System.Globalization.CultureInfo provider = new System.Globalization.CultureInfo("en-US");
                                        //string[] format = {"dd/MM/yyyy","d/M/yyyy","d/MM/yyyy"}; 
                                        //#endregion

                                        //object objvalue = DateTime.ParseExact(valAuditScheduleStartDate, format, provider).ToString("dd/MM/yyyy");
                                        //string tempDate = null;
                                        //tempDate = DateTime.ParseExact(valAuditScheduleStartDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                        //valAuditScheduleStartDate = string.Empty;
                                        //valAuditScheduleStartDate = tempDate;
                                        //DateTime dt = DateTime.Parse(valAuditScheduleStartDate);


                                        bool check = CheckDate(valAuditScheduleStartDate);
                                        if (check)
                                        {
                                            sucess = true;
                                        }
                                        else
                                        {
                                            errorMessage.Add("Please Check the Audit Schedule Start Date at row number-" + rowNum);
                                        }
                                        bool isDateFormatValid = CheckDateFormat(valAuditScheduleStartDate);
                                        if (!isDateFormatValid)
                                        {
                                            errorMessage.Add("Please Check the Audit Schedule Start Date Format at row number-" + rowNum);

                                        }
                                        compareSchedulingStartDate = isDateFormatValid;
                                    }
                                    catch
                                    {
                                        errorMessage.Add("Invalid Audit Schedule Start Date at row number-" + rowNum);
                                    }
                                }
                                #endregion

                                #region Audit Schedule End Date
                                if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 12].Text.ToString().Trim()))
                                {
                                    valAuditScheduleEndDate = xlWorksheetvalidation.Cells[rowNum, 12].Text.Trim();
                                }
                                if (string.IsNullOrEmpty(valAuditScheduleEndDate))
                                {
                                    errorMessage.Add("Required Audit Schedule End Date at row number-" + rowNum);
                                }
                                if (!String.IsNullOrEmpty(valAuditScheduleEndDate))
                                {
                                    try
                                    {
                                        bool check = CheckDate(valAuditScheduleEndDate);
                                        if (check)
                                        {
                                            sucess = true;
                                        }
                                        else
                                        {
                                            errorMessage.Add("Please Check the Audit Schedule End Date at row number-" + rowNum);
                                        }
                                        bool isDateFormatValid = CheckDateFormat(valAuditScheduleEndDate);
                                        if (!isDateFormatValid)
                                        {
                                            errorMessage.Add("Please Check the Audit Schedule End Date Format at row number-" + rowNum);
                                        }
                                        compareSchedulingEndDate = isDateFormatValid;
                                        //string tempDate = null;
                                        //tempDate = DateTime.ParseExact(valAuditScheduleEndDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                        //valAuditScheduleEndDate = string.Empty;
                                        //valAuditScheduleEndDate = tempDate;
                                        //DateTime dt = DateTime.Parse(valAuditScheduleEndDate);
                                    }
                                    catch
                                    {
                                        errorMessage.Add("Invalid Audit Schedule End Date at row number-" + rowNum);
                                    }
                                }
                                #endregion

                                #region compare Audit Schedule Start Date and Audit Schedule End Date
                                if (compareSchedulingStartDate && compareSchedulingEndDate)
                                {
                                    if (DateTime.Parse(valAuditScheduleEndDate) < DateTime.Parse(valAuditScheduleStartDate))
                                    {
                                        errorMessage.Add("Audit Schedule End Date should be greater than Audit Schedule Start Date at row number-" + rowNum);
                                    }
                                }
                                #endregion

                                #region Audit Start Date
                                if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 13].Text.ToString().Trim()))
                                {
                                    valAuditStartDate = xlWorksheetvalidation.Cells[rowNum, 13].Text.Trim();
                                }
                                if (string.IsNullOrEmpty(valAuditStartDate))
                                {
                                    errorMessage.Add("Required Audit Start Date at row number-" + rowNum);
                                }
                                if (!String.IsNullOrEmpty(valAuditStartDate))
                                {
                                    try
                                    {
                                        bool check = CheckDate(valAuditStartDate);
                                        if (check)
                                        {
                                            sucess = true;
                                        }
                                        else
                                        {
                                            errorMessage.Add("Please Check the Audit Start Date at row number-" + rowNum);
                                        }
                                        bool isDateFormatValid = CheckDateFormat(valAuditStartDate);
                                        if (!isDateFormatValid)
                                        {
                                            errorMessage.Add("Please Check the Audit Start Date Format at row number-" + rowNum);

                                        }
                                        compareAuditStartDates = isDateFormatValid;
                                        //string tempDate = null;
                                        //tempDate = DateTime.ParseExact(valAuditStartDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                        //valAuditStartDate = string.Empty;
                                        //valAuditStartDate = tempDate;
                                        //DateTime dt = DateTime.Parse(valAuditStartDate);
                                    }
                                    catch
                                    {
                                        errorMessage.Add("Invalid Audit Start Date at row number-" + rowNum);
                                    }
                                }
                                #endregion

                                #region Audit End Date
                                if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 14].Text.ToString().Trim()))
                                {
                                    valAuditEndDate = xlWorksheetvalidation.Cells[rowNum, 14].Text.Trim();
                                }
                                if (string.IsNullOrEmpty(valAuditEndDate))
                                {
                                    errorMessage.Add("Required Audit End Date at row number-" + rowNum);
                                }
                                if (!String.IsNullOrEmpty(valAuditEndDate))
                                {
                                    try
                                    {
                                        bool check = CheckDate(valAuditEndDate);
                                        if (check)
                                        {
                                            sucess = true;
                                        }
                                        else
                                        {
                                            errorMessage.Add("Please Check the Audit End Date at row number-" + rowNum);
                                        }
                                        bool isDateFormatValid = CheckDateFormat(valAuditEndDate);
                                        if (!isDateFormatValid)
                                        {
                                            errorMessage.Add("Please Check the Audit End Date Format at row number-" + rowNum);

                                        }
                                        compareAuditEndDates = isDateFormatValid;
                                        //string tempDate = null;
                                        //tempDate = DateTime.ParseExact(valAuditEndDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                        //valAuditEndDate = string.Empty;
                                        //valAuditEndDate = tempDate;
                                        //DateTime dt = DateTime.Parse(valAuditEndDate);
                                    }
                                    catch
                                    {
                                        errorMessage.Add("Invalid Audit End Date at row number-" + rowNum);
                                    }
                                }
                                #endregion

                                #region compare Audit Start Date and Audit End Date
                                if (compareAuditStartDates && compareAuditEndDates)
                                {
                                    if (DateTime.Parse(valAuditEndDate) < DateTime.Parse(valAuditStartDate))
                                    {
                                        errorMessage.Add("Audit End Date should be greater than Audit Start Date at row number-" + rowNum);
                                    }
                                }
                                #endregion

                                #region Activity To Be Done
                                if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 15].Text.ToString().Trim()))
                                {
                                    valActivityToBeDone = xlWorksheetvalidation.Cells[rowNum, 15].Text.Trim();
                                }
                                if (string.IsNullOrEmpty(valActivityToBeDone))
                                {
                                    errorMessage.Add("Required Activity To Be Done at row number-" + rowNum);
                                }
                                #endregion

                                #region Objective Ref. (Control No)
                                if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 16].Text.ToString().Trim()))
                                {
                                    valObjectiveRef = xlWorksheetvalidation.Cells[rowNum, 16].Text.Trim();
                                }
                                if (string.IsNullOrEmpty(valObjectiveRef))
                                {
                                    errorMessage.Add("Required Objective Ref. (Control No) at row number-" + rowNum);
                                }
                                #endregion

                                #region Risk
                                if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 17].Text.ToString().Trim()))
                                {
                                    valRisk = xlWorksheetvalidation.Cells[rowNum, 17].Text.Trim();
                                }
                                if (string.IsNullOrEmpty(valRisk))
                                {
                                    errorMessage.Add("Required Risk at row number-" + rowNum);
                                }
                                #endregion

                                #region Control Objective
                                if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 18].Text.ToString().Trim()))
                                {
                                    valControlObjective = xlWorksheetvalidation.Cells[rowNum, 18].Text.Trim();
                                }
                                if (string.IsNullOrEmpty(valControlObjective))
                                {
                                    errorMessage.Add("Required Control Objective at row number-" + rowNum);
                                }
                                #endregion

                                #region Control description
                                if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 19].Text.ToString().Trim()))
                                {
                                    valControlDescription = xlWorksheetvalidation.Cells[rowNum, 19].Text.Trim();
                                }
                                if (string.IsNullOrEmpty(valControlDescription))
                                {
                                    errorMessage.Add("Required Control Description at row number-" + rowNum);
                                }
                                #endregion

                                #region Audit Methodology 
                                if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 20].Text.ToString().Trim()))
                                {
                                    valAuditMethodology = xlWorksheetvalidation.Cells[rowNum, 20].Text.Trim();
                                }
                                if (string.IsNullOrEmpty(valAuditMethodology))
                                {
                                    errorMessage.Add("Required Audit Methodology  at row number-" + rowNum);
                                }
                                #endregion

                                #region Process Walkthrough
                                if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 21].Text.ToString().Trim()))
                                {
                                    valProcessWalkthrough = xlWorksheetvalidation.Cells[rowNum, 21].Text.Trim();
                                }
                                #endregion

                                #region Actual Work Done
                                if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 22].Text.ToString().Trim()))
                                {
                                    valActualWorkDone = xlWorksheetvalidation.Cells[rowNum, 22].Text.Trim();
                                }
                                #endregion

                                #region Population
                                if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 23].Text.ToString().Trim()))
                                {
                                    valPopulation = xlWorksheetvalidation.Cells[rowNum, 23].Text.Trim();
                                }
                                #endregion

                                #region Sample
                                if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 24].Text.ToString().Trim()))
                                {
                                    valSample = xlWorksheetvalidation.Cells[rowNum, 24].Text.Trim();
                                }
                                #endregion

                                #region Remark
                                if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 25].Text.ToString().Trim()))
                                {
                                    valRemark = xlWorksheetvalidation.Cells[rowNum, 25].Text.Trim();
                                }
                                #endregion

                                #region Observation Title
                                if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 26].Text.ToString().Trim()))
                                {
                                    valObservationTitle = xlWorksheetvalidation.Cells[rowNum, 26].Text.Trim();
                                }
                                if (string.IsNullOrEmpty(valObservationTitle))
                                {
                                    errorMessage.Add("Required Observation Title at row number-" + rowNum);
                                }
                                #endregion

                                #region Observation

                                if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 27].Text.ToString().Trim()))
                                {
                                    valObservation = xlWorksheetvalidation.Cells[rowNum, 27].Text.Trim();
                                }
                                if (string.IsNullOrEmpty(valObservation))
                                {
                                    errorMessage.Add("Required Observation at row number-" + rowNum);
                                }
                                #endregion

                                #region Brief Observation
                                if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 28].Text.ToString().Trim()))
                                {
                                    valBriefObservation = xlWorksheetvalidation.Cells[rowNum, 28].Text.Trim();
                                }
                                if (string.IsNullOrEmpty(valBriefObservation))
                                {
                                    errorMessage.Add("Required Brief Observation at row number-" + rowNum);
                                }
                                #endregion

                                #region Observation Background
                                if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 29].Text.ToString().Trim()))
                                {
                                    valObservationBackgroud = xlWorksheetvalidation.Cells[rowNum, 29].Text.Trim();
                                }
                                if (string.IsNullOrEmpty(valObservationBackgroud))
                                {
                                    errorMessage.Add("Required Observation Background at row number-" + rowNum);
                                }
                                #endregion

                                #region Business Implication

                                if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 30].Text.ToString().Trim()))
                                {
                                    valBusinessImplication = xlWorksheetvalidation.Cells[rowNum, 30].Text.Trim();
                                }
                                if (string.IsNullOrEmpty(valBusinessImplication))
                                {
                                    errorMessage.Add("Required Business Implication at row number-" + rowNum);
                                }
                                #endregion

                                #region Root Cause
                                if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 31].Text.ToString().Trim()))
                                {
                                    valRootCause = xlWorksheetvalidation.Cells[rowNum, 31].Text.Trim();
                                }
                                if (string.IsNullOrEmpty(valRootCause))
                                {
                                    errorMessage.Add("Required Root Cause at row number-" + rowNum);
                                }
                                #endregion

                                #region Financial Impact
                                if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 32].Text.ToString().Trim()))
                                {
                                    valFinancialImpact = xlWorksheetvalidation.Cells[rowNum, 32].Text.Trim();
                                }
                                if (string.IsNullOrEmpty(valFinancialImpact))
                                {
                                    errorMessage.Add("Required Financial Impact at row number-" + rowNum);
                                }
                                #endregion

                                #region Recomendation
                                if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 33].Text.ToString().Trim()))
                                {
                                    valRecomendation = xlWorksheetvalidation.Cells[rowNum, 33].Text.Trim();
                                }
                                if (string.IsNullOrEmpty(valRecomendation))
                                {
                                    errorMessage.Add("Required Recomendation at row number-" + rowNum);
                                }
                                #endregion

                                #region Management Response
                                if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 34].Text.ToString().Trim()))
                                {
                                    valManagementResponse = xlWorksheetvalidation.Cells[rowNum, 34].Text.Trim();
                                }
                                if (string.IsNullOrEmpty(valManagementResponse))
                                {
                                    errorMessage.Add("Required Management Response at row number-" + rowNum);
                                }
                                #endregion

                                #region TimeLine
                                if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 35].Text.ToString().Trim()))
                                {
                                    valTimeLine = xlWorksheetvalidation.Cells[rowNum, 35].Text.Trim();
                                }
                                if (string.IsNullOrEmpty(valTimeLine))
                                {
                                    errorMessage.Add("Required TimeLine at row number-" + rowNum);
                                }
                                if (!String.IsNullOrEmpty(valTimeLine))
                                {
                                    try
                                    {
                                        bool check = CheckDate(valTimeLine);
                                        if (check)
                                        {
                                            sucess = true;
                                        }
                                        else
                                        {
                                            errorMessage.Add("Please Check the TimeLine at row number-" + rowNum);
                                        }
                                        bool isDateFormatValid = CheckDateFormat(valTimeLine);
                                        if (!isDateFormatValid)
                                        {
                                            errorMessage.Add("Please Check the TimeLine Date Format at row number-" + rowNum);

                                        }
                                        //string tempDate = null;
                                        //tempDate = DateTime.ParseExact(valTimeLine, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                        //valTimeLine = string.Empty;
                                        //valTimeLine = tempDate;
                                        //DateTime dt = DateTime.Parse(valTimeLine);
                                    }
                                    catch
                                    {
                                        errorMessage.Add("Invalid TimeLine at row number-" + rowNum);
                                    }
                                }
                                #endregion

                                #region Person Responsible
                                if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 36].Text.ToString().Trim()))
                                {
                                    valPersonResponsible = xlWorksheetvalidation.Cells[rowNum, 36].Text.Trim();
                                }
                                if (string.IsNullOrEmpty(valPersonResponsible))
                                {
                                    errorMessage.Add("Required Person Responsible at row number-" + rowNum);
                                }
                                #endregion

                                #region Person Responsible Email
                                if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 37].Text.ToString().Trim()))
                                {
                                    valPersonResponsibleEmail = xlWorksheetvalidation.Cells[rowNum, 37].Text.Trim();
                                }
                                if (string.IsNullOrEmpty(valPersonResponsibleEmail))
                                {
                                    errorMessage.Add("Required Person Responsible Email at row number-" + rowNum);
                                }
                                if (!String.IsNullOrEmpty(valPersonResponsibleEmail))
                                {
                                    string email = valPersonResponsibleEmail.Trim();
                                    Regex regex = new Regex(@"^([\w-_]+\.)*[\w-_]+@([\w-_]+\.)*[\w-_]+\.[\w-_]+$");
                                    Match match = regex.Match(email);

                                    if (match.Success)
                                    {
                                        string[] PersonResponsibleName = valPersonResponsible.Split(' ');
                                        string personResponsibleFirstName = string.Empty;
                                        string PersonResponsibleLastName = string.Empty;
                                        if (PersonResponsibleName.Length > 0 && !string.IsNullOrEmpty(PersonResponsibleName[0].ToString()))
                                        {
                                            personResponsibleFirstName = PersonResponsibleName[0].ToString();
                                            PersonResponsibleLastName = PersonResponsibleName[1].ToString();
                                        }
                                        else
                                        {
                                            string[] PersonResponsibleName1 = valPersonResponsibleEmail.Split('@');
                                            personResponsibleFirstName = PersonResponsibleName1[0].ToString();
                                            PersonResponsibleLastName = "NA";
                                        }
                                        User userDetails = UserManagement.GetPersonalResponsibleDetailsByEmail(valPersonResponsibleEmail, CustomerID);

                                        if (userDetails == null || userDetails.FirstName == personResponsibleFirstName && userDetails.LastName == PersonResponsibleLastName)
                                        {
                                            bool checkUserExistance = UserManagement.UserExistsByEmail(valPersonResponsibleEmail, CustomerID);
                                            if (checkUserExistance == false)
                                            {
                                                string userName = string.Empty;
                                                userName = Portal.Common.AuthenticationHelper.User;
                                                User user = new User()
                                                {
                                                    FirstName = personResponsibleFirstName,
                                                    LastName = PersonResponsibleLastName,
                                                    Email = valPersonResponsibleEmail,
                                                    CreatedOn = DateTime.Now,
                                                    IsDeleted = false,
                                                    IsActive = true,
                                                    CreatedByText = userName,
                                                    RoleID = 7,
                                                    WrongAttempt = 0,
                                                    CustomerID = CustomerID,
                                                    CustomerBranchID = valBranchId,
                                                    IsHead = false,
                                                };
                                                string passwordText = Util.CreateRandomPassword(10);
                                                user.Password = Util.CalculateMD5Hash(passwordText);
                                                UserManagement.CreateNewHistoryObservation(user);
                                            }
                                        }
                                        else
                                        {
                                            errorMessage.Add("Person Responsible email is already exists for another user at row number-" + rowNum);
                                        }

                                        mst_User mstUser = UserManagement.GetPersonalResponsibleDetailsByEmailInAuditDb(valPersonResponsibleEmail, CustomerID);

                                        if (mstUser == null || mstUser.FirstName == personResponsibleFirstName && mstUser.LastName == PersonResponsibleLastName)
                                        {
                                            bool checkPerformerExistance = UserManagement.UserExistsByEmailInAuditDb(valPersonResponsibleEmail, CustomerID);
                                            if (checkPerformerExistance == false)
                                            {
                                                string userName = string.Empty;
                                                userName = Portal.Common.AuthenticationHelper.User;
                                                mst_User user = new mst_User()
                                                {
                                                    FirstName = personResponsibleFirstName,
                                                    LastName = PersonResponsibleLastName,
                                                    Email = valPersonResponsibleEmail,
                                                    CreatedOn = DateTime.Now,
                                                    IsDeleted = false,
                                                    IsActive = true,
                                                    CreatedByText = userName,
                                                    RoleID = 7,
                                                    WrongAttempt = 0,
                                                    CustomerID = CustomerID,
                                                    CustomerBranchID = valBranchId,
                                                    IsHead = false,
                                                };
                                                string passwordText = Util.CreateRandomPassword(10);
                                                user.Password = Util.CalculateMD5Hash(passwordText);
                                                UserManagement.CreateNewHistoryObservationInAuditDb(user);
                                            }
                                        }
                                        else
                                        {
                                            errorMessage.Add("Person Responsible email is already exists for another user at row number-" + rowNum);
                                        }
                                    }
                                    else
                                    {
                                        errorMessage.Add("Invalid Person Responsible email at row number-" + rowNum);
                                    }
                                    // valReviewerUserId = UserManagement.GetUserIdByEmail(valPersonResponsibleEmail, CustomerID, valBranchId);
                                }
                                #endregion

                                #region Owner Email
                                if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 38].Text.ToString().Trim()))
                                {
                                    valOwnerEmail = xlWorksheetvalidation.Cells[rowNum, 38].Text.Trim();
                                }
                                if (string.IsNullOrEmpty(valOwnerEmail))
                                {
                                    errorMessage.Add("Required Owner Email at row number-" + rowNum);
                                }
                                if (!String.IsNullOrEmpty(valOwnerEmail))
                                {
                                    string email = valOwnerEmail.Trim();
                                    Regex regex = new Regex(@"^([\w-_]+\.)*[\w-_]+@([\w-_]+\.)*[\w-_]+\.[\w-_]+$");
                                    Match match = regex.Match(email);

                                    if (match.Success)
                                    {
                                        bool checkUserExistance = UserManagement.UserExistsByEmail(valOwnerEmail, CustomerID);
                                        string[] OwnerName = valOwnerEmail.Split('@');
                                        if (checkUserExistance == false)
                                        {
                                            string userName = string.Empty;
                                            userName = Portal.Common.AuthenticationHelper.User;
                                            User user = new User()
                                            {
                                                FirstName = OwnerName[0].ToString(),
                                                LastName = "NA",
                                                Email = valOwnerEmail,
                                                CreatedOn = DateTime.Now,
                                                IsDeleted = false,
                                                IsActive = true,
                                                CreatedByText = userName,
                                                RoleID = 7,
                                                WrongAttempt = 0,
                                                CustomerID = CustomerID,
                                                CustomerBranchID = valBranchId,
                                                IsHead = false,
                                            };
                                            string passwordText = Util.CreateRandomPassword(10);
                                            user.Password = Util.CalculateMD5Hash(passwordText);
                                            UserManagement.CreateNewHistoryObservation(user);
                                        }
                                        bool checkPerformerExistance = UserManagement.UserExistsByEmailInAuditDb(valOwnerEmail, CustomerID);
                                        if (checkPerformerExistance == false)
                                        {
                                            string userName = string.Empty;
                                            userName = Portal.Common.AuthenticationHelper.User;
                                            mst_User user = new mst_User()
                                            {
                                                FirstName = OwnerName[0].ToString(),
                                                LastName = "NA",
                                                Email = valOwnerEmail,
                                                CreatedOn = DateTime.Now,
                                                IsDeleted = false,
                                                IsActive = true,
                                                CreatedByText = userName,
                                                RoleID = 7,
                                                WrongAttempt = 0,
                                                CustomerID = CustomerID,
                                                CustomerBranchID = valBranchId,
                                                IsHead = false,
                                            };
                                            string passwordText = Util.CreateRandomPassword(10);
                                            user.Password = Util.CalculateMD5Hash(passwordText);
                                            UserManagement.CreateNewHistoryObservationInAuditDb(user);
                                        }
                                    }
                                    else
                                    {
                                        errorMessage.Add("Invalid Owner email at row number-" + rowNum);
                                    }
                                }
                                #endregion

                                #region Observation Rating
                                if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 39].Text.ToString().Trim()))
                                {
                                    valObservationRating = xlWorksheetvalidation.Cells[rowNum, 39].Text.Trim();
                                }
                                if (string.IsNullOrEmpty(valObservationRating))
                                {
                                    errorMessage.Add("Required Observation Rating at row number-" + rowNum);
                                }
                                if (!string.IsNullOrEmpty(valObservationRating.ToUpper()))
                                {
                                    string ObserRating = valObservationRating.ToUpper();
                                    switch (ObserRating)
                                    {
                                        case "MAJOR":
                                            break;
                                        case "MODERATE":
                                            break;
                                        case "MINOR":
                                            break;
                                        default:
                                            errorMessage.Add("Invalid Observation Rating format at row number-" + rowNum);
                                            break;
                                    }
                                }
                                #endregion

                                #region Observation Category
                                if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 40].Text.ToString().Trim()))
                                {
                                    valObservationCategory = xlWorksheetvalidation.Cells[rowNum, 40].Text.Trim();
                                }
                                if (string.IsNullOrEmpty(valObservationCategory))
                                {
                                    errorMessage.Add("Required Observation Category at row number-" + rowNum);
                                }
                                if (!String.IsNullOrEmpty(valObservationCategory))
                                {
                                    //if (!ObservationSubcategory.IsExistObservationCategoryByName(valObservationCategory, CustomerID))
                                    //{
                                    //    Mst_ObservationCategory observationSubcategory = new Mst_ObservationCategory()
                                    //    {
                                    //        Name = valObservationCategory,
                                    //        IsActive = false,
                                    //        CustomerID = CustomerID
                                    //    };
                                    //    ObservationSubcategory.CreateObservationCategory(observationSubcategory);
                                    //}

                                    if (!ObservationSubcategory.IsExistObservationCategoryByName(valObservationCategory, CustomerID))
                                    {
                                        errorMessage.Add("Observation Category not defined in the system at row number-" + rowNum);
                                    }
                                    else
                                    {
                                        valObservationCategoryId = ObservationSubcategory.GetIdObservationCategoryByName(valObservationCategory, CustomerID);
                                    }
                                }
                                #endregion

                                #region Observation Sub Category
                                if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 41].Text.ToString().Trim()))
                                {
                                    valObservationSubCategory = xlWorksheetvalidation.Cells[rowNum, 41].Text.Trim();
                                }
                                if (!String.IsNullOrEmpty(valObservationSubCategory))
                                {
                                    if (!ObservationSubcategory.IsExistObservationSubCategoryByName(valObservationSubCategory, valObservationCategoryId))
                                    {
                                        int observationCategoryId = 0;
                                        observationCategoryId = ObservationSubcategory.GetObservationCategoryByName(valObservationCategory, CustomerID);
                                        if (observationCategoryId != 0 && observationCategoryId != -1)
                                        {
                                            Mst_ObservationSubCategory observationSubCategory = new Mst_ObservationSubCategory()
                                            {
                                                ObscatID = observationCategoryId,
                                                Name = valObservationSubCategory,
                                                ClientID = CustomerID,
                                                IsActive = false
                                            };
                                            ObservationSubcategory.CreateObservationSubCategory(observationSubCategory);
                                        }
                                    }
                                }
                                #endregion

                                #region Status (Open/Close)
                                if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 42].Text.ToString().Trim()))
                                {
                                    valStatus = xlWorksheetvalidation.Cells[rowNum, 42].Text.Trim();
                                }
                                if (String.IsNullOrEmpty(valStatus))
                                {
                                    errorMessage.Add("Required Status at row number-" + rowNum);
                                }

                                if (!String.IsNullOrEmpty(valStatus))
                                {
                                    if (valStatus == "OPEN" || valStatus == "CLOSE" || valStatus == "open" || valStatus == "close" || valStatus == "Open" || valStatus == "Close")
                                    {

                                    }
                                    else
                                    {
                                        errorMessage.Add("Invalid Status at row number-" + rowNum);
                                    }
                                }
                                #endregion

                                #region commented code for Validation - If Observation exists then subsequent fields are require.
                                //if (!String.IsNullOrEmpty(valObservation))
                                //{
                                //    if (String.IsNullOrEmpty(valObservationTitle))
                                //    {
                                //        errorMessage.Add("Observation Title can not be empty at row number-" + rowNum);
                                //    }
                                //    if (String.IsNullOrEmpty(valBusinessImplication))
                                //    {
                                //        errorMessage.Add("Business Implication can not be empty at row number-" + rowNum);
                                //    }
                                //    if (String.IsNullOrEmpty(valFinancialImpact))
                                //    {
                                //        errorMessage.Add("Financial Impact can not be Empty at row number-" + rowNum);
                                //    }
                                //    if (String.IsNullOrEmpty(valRecomendation))
                                //    {
                                //        errorMessage.Add("Recommendation can not be empty at row number-" + rowNum);
                                //    }
                                //    if (String.IsNullOrEmpty(valManagementResponse))
                                //    {
                                //        errorMessage.Add("Management Response can not be empty at row number-" + rowNum);
                                //    }
                                //    if (String.IsNullOrEmpty(valPersonResponsible))
                                //    {
                                //        errorMessage.Add("Person Responsible can not be empty at row number-" + rowNum);
                                //    }
                                //    if (String.IsNullOrEmpty(valPersonResponsibleEmail))
                                //    {
                                //        errorMessage.Add("Person Responsible Email can not be empty at row number-" + rowNum);
                                //    }
                                //    if (String.IsNullOrEmpty(valObservationRating))
                                //    {
                                //        errorMessage.Add("Observation Rating can not be empty at row number-" + rowNum);
                                //    }
                                //    if (String.IsNullOrEmpty(valObservationCategory))
                                //    {
                                //        errorMessage.Add("Observation Category can not be empty at row number-" + rowNum);
                                //    }
                                //}
                                #endregion
                            }


                            if (errorMessage.Count > 0)
                            {
                                ErrorMessages(errorMessage);
                                sucess = false;
                                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 0, function () { });", true);
                            }
                            else
                            {
                                #region New Table
                                ExcelWorksheet xlWorksheetMaster = xlWorkbook.Workbook.Worksheets["AuditObservations"];
                                int xlrowtemptable = xlWorksheetMaster.Dimension.End.Row;

                                List<mst_RCMMaster> MasterRCMList = new List<mst_RCMMaster>();
                                for (int temprow = 2; temprow <= xlrowtemptable; temprow++)
                                {
                                    string ControlNo = string.Empty;
                                    string ActivityDescription = string.Empty;
                                    string ControlObjective = string.Empty;
                                    string ControlDescription = string.Empty;
                                    string MControlDescription = string.Empty;
                                    DateTime? effectivedate = null;
                                    string GapDescription = string.Empty;
                                    string Recommendations = string.Empty;
                                    string ActionRemediationplan = string.Empty;
                                    string IPE = string.Empty;
                                    string ERPsystem = string.Empty;
                                    string FRC = string.Empty;
                                    string UniqueReferred = string.Empty;
                                    string TestStrategy = string.Empty;
                                    string DocumentsExamined = string.Empty;
                                    string finalriskcategoryid = string.Empty;
                                    string finalAssertionsmappigid = string.Empty;
                                    string AuditObjective = string.Empty;
                                    string AuditStep = string.Empty;
                                    string Analysistobeperformed = string.Empty;
                                    string PreRequisiteList = string.Empty;
                                    string Activity = string.Empty;

                                    int processid = -1;
                                    int subprocessid = -1;
                                    long activityid = -1;
                                    int personresponsibleID = 0;
                                    int ControlownerID = 0;
                                    int PrevationControlID = 0;
                                    int PrimarySecondaryID = 0;
                                    int AutomatedControlID = 0;
                                    int Frequency = 0;
                                    int KeyID = 0;
                                    int KCONEID = 0;
                                    long branchId = 0;
                                    int verticalId = -1;
                                    string branchName = string.Empty;
                                    string verticalName = string.Empty;
                                    #region Branch Name
                                    if (!String.IsNullOrEmpty(xlWorksheetMaster.Cells[temprow, 1].Text.ToString().Trim()))
                                    {
                                        branchName = Convert.ToString(xlWorksheetMaster.Cells[temprow, 1].Text).Trim();

                                        branchId = CustomerBranchManagement.GetByNameFromAuditDb(branchName, CustomerID).ID;

                                        LocationList.Clear();
                                        LocationList.Add(branchId);
                                    }
                                    #endregion

                                    #region Vertical
                                    if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[temprow, 2].Text.ToString().Trim()))
                                    {
                                        verticalName = xlWorksheetvalidation.Cells[temprow, 2].Text.Trim();
                                    }
                                    else
                                    {
                                        verticalName = "NA";
                                    }
                                    if (!String.IsNullOrEmpty(verticalName))
                                    {
                                        verticalId = UserManagementRisk.GetVerticalIDByName(verticalName, CustomerID);
                                    }
                                    #endregion

                                    #region Process Name
                                    if (!String.IsNullOrEmpty(xlWorksheetMaster.Cells[temprow, 3].Text.ToString().Trim()))
                                    {
                                        processid = ProcessManagement.GetProcessIDByName(Regex.Replace(xlWorksheetMaster.Cells[temprow, 3].Text.ToString().Trim(), @"\t|\n|\r", ""), CustomerID);
                                    }
                                    #endregion

                                    #region SubProcess
                                    if (!String.IsNullOrEmpty(xlWorksheetMaster.Cells[temprow, 4].Text.ToString().Trim()))
                                    {
                                        subprocessid = ProcessManagement.GetSubProcessIDByName(Regex.Replace(xlWorksheetMaster.Cells[temprow, 4].Text.ToString().Trim(), @"\t|\n|\r", ""), processid);
                                    }
                                    #endregion

                                    activityid = GetActivityId_mst_Activity(Convert.ToInt32(processid), Convert.ToInt32(subprocessid));

                                    #region Activity To Be Done
                                    if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[temprow, 15].Text.ToString().Trim()))
                                    {
                                        AuditStep = Convert.ToString(GetSafeTagName(xlWorksheetvalidation.Cells[temprow, 15].Text.ToString().Trim())).Trim();
                                    }

                                    #endregion

                                    #region Objective Ref.
                                    if (!String.IsNullOrEmpty(xlWorksheetMaster.Cells[temprow, 16].Text.ToString().Trim()))
                                    {
                                        ControlNo = Convert.ToString(xlWorksheetMaster.Cells[temprow, 16].Text).Trim();
                                    }
                                    #endregion

                                    #region Risk Description
                                    if (!String.IsNullOrEmpty(xlWorksheetMaster.Cells[temprow, 17].Text.ToString().Trim()))
                                    {
                                        ActivityDescription = Convert.ToString(GetSafeTagName(xlWorksheetMaster.Cells[temprow, 17].Text.ToString().Trim())).Trim();
                                    }
                                    #endregion

                                    #region Control Objective
                                    if (!String.IsNullOrEmpty(xlWorksheetMaster.Cells[temprow, 18].Text.ToString().Trim()))
                                    {
                                        ControlObjective = Convert.ToString(GetSafeTagName(xlWorksheetMaster.Cells[temprow, 18].Text.ToString().Trim())).Trim();
                                    }
                                    #endregion

                                    #region  Control Description
                                    if (!String.IsNullOrEmpty(xlWorksheetMaster.Cells[temprow, 19].Text.ToString().Trim()))
                                    {                                        
                                        ControlDescription = Convert.ToString(GetSafeTagName(xlWorksheetMaster.Cells[temprow, 19].Text.ToString().Trim())).Trim();
                                    }
                                    #endregion

                                    #region Audit Methodology
                                    if (!String.IsNullOrEmpty(xlWorksheetMaster.Cells[temprow, 20].Text.ToString().Trim()))
                                    {
                                        AuditObjective = Convert.ToString(GetSafeTagName(xlWorksheetMaster.Cells[temprow, 20].Text.ToString().Trim())).Trim();
                                    }
                                    #endregion

                                    #region Recommendations
                                    if (!String.IsNullOrEmpty(xlWorksheetMaster.Cells[temprow, 33].Text.ToString().Trim()))
                                    {                                        
                                        Recommendations = Convert.ToString(GetSafeTagName(xlWorksheetMaster.Cells[temprow, 33].Text.ToString().Trim())).Trim();
                                    }
                                    #endregion

                                    #region Process Owner
                                    if (!String.IsNullOrEmpty(xlWorksheetMaster.Cells[temprow, 36].Text.ToString().Trim()))
                                    {   //12 Process Owner Email
                                        if (!String.IsNullOrEmpty(xlWorksheetMaster.Cells[temprow, 37].Text.ToString().Trim()))
                                        {
                                            personresponsibleID = RiskCategoryManagement.GetUserIDByName(xlWorksheetMaster.Cells[temprow, 36].Text.ToString().Trim(), CustomerID, xlWorksheetMaster.Cells[temprow, 37].Text.ToString().Trim());
                                        }
                                    }
                                    #endregion

                                    #region Control Owner
                                    #region imp commented code
                                    //if (!String.IsNullOrEmpty(xlWorksheetMaster.Cells[temprow, 13].Text.ToString().Trim()))
                                    //{   //14 Control Ownerr Email
                                    //    if (!String.IsNullOrEmpty(xlWorksheetMaster.Cells[temprow, 14].Text.ToString().Trim()))
                                    //    {
                                    //        ControlownerID = RiskCategoryManagement.GetUserIDByName(xlWorksheetMaster.Cells[temprow, 13].Text.ToString().Trim(), CustomerID, xlWorksheetMaster.Cells[temprow, 14].Text.ToString().Trim());
                                    //    }
                                    //} 
                                    #endregion

                                    if (!String.IsNullOrEmpty(xlWorksheetMaster.Cells[temprow, 38].Text.ToString().Trim()))
                                    {
                                        //ControlownerID = UserManagement.GetUserIdByEmail(xlWorksheetMaster.Cells[temprow, 36].Text.ToString().Trim(), CustomerID, Convert.ToInt32(branchId));
                                        ControlownerID = UserManagement.GetUserIdByEmail(xlWorksheetMaster.Cells[temprow, 38].Text.ToString().Trim(), CustomerID);
                                    }

                                    #endregion

                                    #region RiskRating
                                    int RiskRating = -1;
                                    if (!String.IsNullOrEmpty(xlWorksheetMaster.Cells[temprow, 39].Text.ToString().Trim()))
                                    {
                                        RiskRating = GetRiskControlRatingID("R", Convert.ToString(xlWorksheetMaster.Cells[temprow, 39].Text).Trim());
                                    }
                                    #endregion

                                    mst_RCMMaster mstrcmmaster = new mst_RCMMaster();
                                    mstrcmmaster.ObjectiveRef = ControlNo;
                                    mstrcmmaster.Processid = processid;
                                    mstrcmmaster.SubProcessid = subprocessid;
                                    mstrcmmaster.ActivityId = activityid;
                                    mstrcmmaster.RiskDesc = ActivityDescription;
                                    mstrcmmaster.ControlObjective = ControlObjective;
                                    mstrcmmaster.Controldescription = ControlDescription;
                                    mstrcmmaster.RiskCategory = finalriskcategoryid.Trim(',');
                                    mstrcmmaster.FinancialAssertion = finalAssertionsmappigid.Trim(',');
                                    mstrcmmaster.MCDescription = MControlDescription;
                                    mstrcmmaster.ProcessOwnerID = personresponsibleID;
                                    mstrcmmaster.ControlOwnerID = ControlownerID;
                                    mstrcmmaster.EffectiveDate = effectivedate;
                                    mstrcmmaster.KC1 = KeyID;
                                    mstrcmmaster.KC2 = KCONEID;
                                    mstrcmmaster.PrimarySecondary = PrimarySecondaryID;
                                    mstrcmmaster.PreventiveDetective = PrevationControlID;
                                    mstrcmmaster.AutomatedManual = AutomatedControlID;
                                    mstrcmmaster.Frequency = Frequency;
                                    mstrcmmaster.GapDescription = GapDescription;
                                    mstrcmmaster.ProposedRecommendations = Recommendations;
                                    mstrcmmaster.ActionRemediationPlan = ActionRemediationplan;
                                    mstrcmmaster.IPE = IPE;
                                    mstrcmmaster.ERPSystem = ERPsystem;
                                    mstrcmmaster.FraudRiskIndecator = FRC;
                                    mstrcmmaster.UniqueReferred = UniqueReferred;
                                    mstrcmmaster.TestStrategy = TestStrategy;
                                    mstrcmmaster.DocumentsExamined = DocumentsExamined;
                                    mstrcmmaster.Riskrating = RiskRating;
                                    mstrcmmaster.ControlRating = null;
                                    mstrcmmaster.AuditObjective = AuditObjective;
                                    mstrcmmaster.AuditStep = AuditStep;
                                    mstrcmmaster.AnalysistobePerformed = Analysistobeperformed;
                                    mstrcmmaster.PreRequisiteList = PreRequisiteList;
                                    mstrcmmaster.IsProcessed = false;
                                    mstrcmmaster.IsDeleted = false;
                                    mstrcmmaster.Createdon = DateTime.Now;
                                    mstrcmmaster.CreatedBy = Portal.Common.AuthenticationHelper.UserID;
                                    mstrcmmaster.BranchList = Convert.ToString(branchId);
                                    mstrcmmaster.VerticalList = Convert.ToString(verticalId);

                                    if (!(ExistsNew(mstrcmmaster)))
                                    {
                                        MasterRCMList.Add(mstrcmmaster);
                                    }
                                }
                                sucess = RiskCategoryManagement.CreateBulkExcelRCMMaster(MasterRCMList);
                                #endregion

                                var transactionsQuery = (from row in entities.mst_RCMMaster
                                                         where row.IsProcessed == false
                                                         && row.IsDeleted == false
                                                         select row).ToList();

                                foreach (var item in transactionsQuery)
                                {
                                    #region Master Region  
                                    int BranchitemId = Convert.ToInt32(item.BranchList);
                                    //List<long> masterbranchlist = new List<long>();
                                    List<long> masterverticalist = new List<long>();
                                    List<long> masterriskcategory = new List<long>();
                                    List<long> masterasseration = new List<long>();
                                    //if (!string.IsNullOrEmpty(item.BranchList))
                                    //{
                                    //    masterbranchlist.Clear();
                                    //    masterbranchlist = item.BranchList.Trim(',').Split(',').Select(long.Parse).Distinct().ToList();
                                    //}
                                    if (!string.IsNullOrEmpty(item.VerticalList))
                                    {
                                        masterverticalist.Clear();
                                        masterverticalist = item.VerticalList.Trim(',').Split(',').Select(long.Parse).Distinct().ToList();
                                    }
                                    if (!string.IsNullOrEmpty(item.RiskCategory))
                                    {
                                        masterriskcategory.Clear();
                                        masterriskcategory = item.RiskCategory.Trim(',').Split(',').Select(long.Parse).Distinct().ToList();
                                    }
                                    if (!string.IsNullOrEmpty(item.FinancialAssertion))
                                    {
                                        masterasseration.Clear();
                                        masterasseration = item.FinancialAssertion.Trim(',').Split(',').Select(long.Parse).Distinct().ToList();
                                    }
                                    //foreach (var Branchitem in masterbranchlist)
                                    //{
                                    if (!(RiskCategoryManagement.ExistsNew(item.ObjectiveRef, Convert.ToInt32(item.Processid), (long)item.SubProcessid,
                                        CustomerID, BranchitemId, Convert.ToString(item.RiskDesc), Convert.ToString(item.ControlObjective))))
                                    {
                                        #region risk not Exists
                                        RiskCategoryCreation riskcategorycreation = new RiskCategoryCreation()
                                        {
                                            ControlNo = item.ObjectiveRef,
                                            ProcessId = (long)item.Processid,
                                            SubProcessId = item.SubProcessid,
                                            ActivityDescription = item.RiskDesc,
                                            ControlObjective = item.ControlObjective,
                                            IsDeleted = false,
                                            LocationType = -1,
                                            IsInternalAudit = "N",
                                            CustomerId = CustomerID,
                                            CustomerBranchId = BranchitemId,
                                        };
                                        sucess = CreateRiskCategoryCreation(riskcategorycreation);

                                        foreach (var riskcategoryitem in masterriskcategory)
                                        {
                                            RiskCategoryMapping riskcategorymapping = new RiskCategoryMapping()
                                            {
                                                RiskCategoryCreationId = riskcategorycreation.Id,
                                                RiskCategoryId = Convert.ToInt32(riskcategoryitem),
                                                ProcessId = item.Processid,
                                                SubProcessId = item.SubProcessid,
                                                IsActive = true,
                                            };
                                            RiskCategoryManagement.CreateRiskCategoryMapping(riskcategorymapping);
                                        }
                                        foreach (var asserationitem in masterasseration)
                                        {
                                            AssertionsMapping assertionsmapping = new AssertionsMapping()
                                            {
                                                RiskCategoryCreationId = riskcategorycreation.Id,
                                                AssertionId = Convert.ToInt32(asserationitem),
                                                ProcessId = item.Processid,
                                                SubProcessId = item.SubProcessid,
                                                IsActive = true,
                                            };
                                            RiskCategoryManagement.CreateAssertionsMapping(assertionsmapping);
                                        }

                                        foreach (var verticalitem in masterverticalist)
                                        {
                                            BranchVertical BranchVerticals = new BranchVertical();
                                            BranchVerticals.Branch = BranchitemId;
                                            BranchVerticals.CustomerID = CustomerID;
                                            BranchVerticals.VerticalID = (int)verticalitem;
                                            BranchVerticals.IsActive = true;
                                            if (!UserManagementRisk.VerticalBrachExist(CustomerID, BranchitemId, (int)verticalitem))
                                            {
                                                UserManagementRisk.CreateVerticalBranchlist(BranchVerticals);
                                            }

                                            long auditStepMasterID = -1;
                                            if (!(RiskCategoryManagement.Exists((long)item.Processid, (long)item.SubProcessid, riskcategorycreation.Id,
                                                Convert.ToInt32(item.Frequency), BranchitemId, verticalitem, (long)item.ActivityId, item.Controldescription)))
                                            {
                                                long ActivityId = GetActivityId_mst_Activity((long)item.Processid, (long)item.SubProcessid);

                                                #region Control not Exists                                           
                                                RiskActivityTransaction riskactivitytransaction = new RiskActivityTransaction()
                                                {
                                                    CustomerBranchId = BranchitemId,
                                                    VerticalsId = verticalitem,
                                                    ProcessId = (long)item.Processid,
                                                    SubProcessId = (long)item.SubProcessid,
                                                    RiskCreationId = riskcategorycreation.Id,
                                                    ControlDescription = item.Controldescription,
                                                    MControlDescription = item.MCDescription,
                                                    PersonResponsible = (long)item.ProcessOwnerID,
                                                    EffectiveDate = item.EffectiveDate,
                                                    Key_Value = (long)item.KC1,
                                                    PrevationControl = (long)item.PreventiveDetective,
                                                    AutomatedControl = (long)item.AutomatedManual,
                                                    Frequency = (long)item.Frequency,
                                                    IsDeleted = false,
                                                    GapDescription = item.GapDescription,
                                                    Recommendations = item.ProposedRecommendations,
                                                    ActionRemediationplan = item.ActionRemediationPlan,
                                                    IPE = item.IPE,
                                                    ERPsystem = item.ERPSystem,
                                                    FRC = item.FraudRiskIndecator,
                                                    UniqueReferred = item.UniqueReferred,
                                                    TestStrategy = item.TestStrategy,
                                                    RiskRating = (int)item.Riskrating,
                                                    ControlRating = 4,
                                                    ProcessScore = 0,
                                                    ActivityID = item.ActivityId,
                                                    KC2 = item.KC2,
                                                    Primary_Secondary = (long)item.PrimarySecondary,
                                                    ControlOwner = (long)item.ControlOwnerID,
                                                    ISICFR_Close = true,
                                                    RCMType = "ARS"
                                                };
                                                sucess = CreateRiskActivityTransaction(riskactivitytransaction);
                                                auditStepMasterID = RiskCategoryManagement.GetAuditStepMasterID(item.AuditStep);
                                                if (auditStepMasterID == 0)
                                                {
                                                    AuditStepMaster newASM = new AuditStepMaster()
                                                    {
                                                        AuditStep = item.AuditStep
                                                    };

                                                    if (RiskCategoryManagement.CreateAuditStepMaster(newASM))
                                                        auditStepMasterID = newASM.ID;
                                                }

                                                if (!(RiskCategoryManagement.RiskActivityToBeDoneMappingExists(riskcategorycreation.Id, riskactivitytransaction.Id,
                                                    BranchitemId, (int)verticalitem, (int)item.Processid, (int)item.SubProcessid, item.AuditStep)))
                                                {
                                                    if (auditStepMasterID != 0)
                                                    {
                                                        RiskActivityToBeDoneMapping RATBDM = new RiskActivityToBeDoneMapping()
                                                        {
                                                            AuditStepMasterID = auditStepMasterID,
                                                            RiskCategoryCreationId = riskcategorycreation.Id,
                                                            RiskActivityId = riskactivitytransaction.Id,
                                                            CustomerBranchID = BranchitemId,
                                                            VerticalID = verticalitem,
                                                            ProcessId = (long)item.Processid,
                                                            SubProcessId = item.SubProcessid,
                                                            AuditObjective = item.AuditObjective,
                                                            ActivityTobeDone = item.AuditStep,
                                                            Analyis_To_Be_Performed = item.AnalysistobePerformed,
                                                            Rating = 1,
                                                            IsActive = true,
                                                            RCMType = "ARS",
                                                        };
                                                        sucess = RiskCategoryManagement.CreateRiskActivityTBDMapping(RATBDM);

                                                        #region   PreRequisite
                                                        if (!String.IsNullOrEmpty(item.PreRequisiteList))
                                                        {
                                                            List<StepChecklistMapping> SCMRecords = new List<StepChecklistMapping>();
                                                            string prerequisite = item.PreRequisiteList;
                                                            string[] split = prerequisite.Split('|');
                                                            if (split.Length > 0)
                                                            {
                                                                string prerequisiteName = string.Empty;
                                                                for (int rs = 0; rs < split.Length; rs++)
                                                                {
                                                                    prerequisiteName = Convert.ToString(split[rs].ToString().Trim());
                                                                    if (!String.IsNullOrEmpty(prerequisiteName))
                                                                    {
                                                                        StepChecklistMapping SCM = new StepChecklistMapping();
                                                                        SCM.ATBDID = RATBDM.ID;
                                                                        SCM.ChecklistDocument = prerequisiteName;
                                                                        SCM.IsActive = true;
                                                                        SCMRecords.Add(SCM);
                                                                    }
                                                                }

                                                                if (SCMRecords.Count > 0)
                                                                {
                                                                    sucess = RiskCategoryManagement.CreateStepChecklistMapping(SCMRecords);
                                                                    SCMRecords.Clear();
                                                                }
                                                            }
                                                        }

                                                        #endregion
                                                    }
                                                }
                                                else
                                                {
                                                    sucess = RiskCategoryManagement.RiskActivityToBeDoneMappingUpdate(riskcategorycreation.Id, riskactivitytransaction.Id, BranchitemId, (int)verticalitem, (int)item.Processid, (int)item.SubProcessid, item.AuditStep, item.AuditObjective);
                                                }
                                                #endregion
                                            }
                                            else
                                            {
                                                #region Control Exists
                                                auditStepMasterID = RiskCategoryManagement.GetAuditStepMasterID(item.AuditStep);
                                                if (auditStepMasterID == 0)
                                                {
                                                    AuditStepMaster newASM = new AuditStepMaster()
                                                    {
                                                        AuditStep = item.AuditStep
                                                    };

                                                    if (RiskCategoryManagement.CreateAuditStepMaster(newASM))
                                                        auditStepMasterID = newASM.ID;
                                                }
                                                int controlactivityid = RiskCategoryManagement.GetControlCreationIdNew((long)item.Processid, (int)item.SubProcessid, riskcategorycreation.Id, item.Controldescription, (int)item.Frequency, BranchitemId, verticalitem, (long)item.ActivityId);
                                                if (!(RiskCategoryManagement.RiskActivityToBeDoneMappingExists(riskcategorycreation.Id, controlactivityid,
                                                    BranchitemId, (int)verticalitem, (int)item.Processid, (int)item.SubProcessid, item.AuditStep)))
                                                {
                                                    if (auditStepMasterID != 0)
                                                    {
                                                        RiskActivityToBeDoneMapping RATBDM = new RiskActivityToBeDoneMapping()
                                                        {
                                                            AuditStepMasterID = auditStepMasterID,
                                                            RiskCategoryCreationId = riskcategorycreation.Id,
                                                            RiskActivityId = controlactivityid,
                                                            CustomerBranchID = BranchitemId,
                                                            VerticalID = verticalitem,
                                                            ProcessId = (long)item.Processid,
                                                            SubProcessId = item.SubProcessid,
                                                            AuditObjective = item.AuditObjective,
                                                            ActivityTobeDone = item.AuditStep,
                                                            Analyis_To_Be_Performed = item.AnalysistobePerformed,
                                                            Rating = 1,
                                                            IsActive = true,
                                                            RCMType = "ARS",
                                                        };
                                                        sucess = RiskCategoryManagement.CreateRiskActivityTBDMapping(RATBDM);

                                                        #region   PreRequisite
                                                        if (!String.IsNullOrEmpty(item.PreRequisiteList))
                                                        {
                                                            List<StepChecklistMapping> SCMRecords = new List<StepChecklistMapping>();
                                                            string prerequisite = item.PreRequisiteList;
                                                            string[] split = prerequisite.Split('|');
                                                            if (split.Length > 0)
                                                            {
                                                                string prerequisiteName = string.Empty;
                                                                for (int rs = 0; rs < split.Length; rs++)
                                                                {
                                                                    prerequisiteName = Convert.ToString(split[rs].ToString().Trim());
                                                                    if (!String.IsNullOrEmpty(prerequisiteName))
                                                                    {
                                                                        StepChecklistMapping SCM = new StepChecklistMapping();
                                                                        SCM.ATBDID = RATBDM.ID;
                                                                        SCM.ChecklistDocument = prerequisiteName;
                                                                        SCM.IsActive = true;
                                                                        SCMRecords.Add(SCM);
                                                                    }
                                                                }

                                                                if (SCMRecords.Count > 0)
                                                                {
                                                                    sucess = RiskCategoryManagement.CreateStepChecklistMapping(SCMRecords);
                                                                    SCMRecords.Clear();
                                                                }
                                                            }
                                                        }
                                                        #endregion
                                                    }
                                                }
                                                else
                                                {
                                                    sucess = RiskCategoryManagement.RiskActivityToBeDoneMappingUpdate(riskcategorycreation.Id, controlactivityid, BranchitemId, (int)verticalitem, (int)item.Processid, (int)item.SubProcessid, item.AuditStep, item.AuditObjective);
                                                    int atbdid = RiskCategoryManagement.GetATBDID(riskcategorycreation.Id, controlactivityid, BranchitemId, (int)verticalitem, (int)item.Processid, (int)item.SubProcessid, item.AuditStep);
                                                    #region   PreRequisite
                                                    if (!String.IsNullOrEmpty(item.PreRequisiteList))
                                                    {
                                                        List<StepChecklistMapping> SCMRecords = new List<StepChecklistMapping>();
                                                        string prerequisite = item.PreRequisiteList;
                                                        string[] split = prerequisite.Split('|');
                                                        if (split.Length > 0)
                                                        {
                                                            string prerequisiteName = string.Empty;
                                                            for (int rs = 0; rs < split.Length; rs++)
                                                            {
                                                                prerequisiteName = Convert.ToString(split[rs].ToString().Trim());
                                                                if (!String.IsNullOrEmpty(prerequisiteName))
                                                                {
                                                                    StepChecklistMapping SCM = new StepChecklistMapping();
                                                                    SCM.ATBDID = atbdid;
                                                                    SCM.ChecklistDocument = prerequisiteName;
                                                                    SCM.IsActive = true;
                                                                    if (!(PrerequsiteExistsNew(SCM)))
                                                                    {
                                                                        SCMRecords.Add(SCM);
                                                                    }
                                                                }
                                                            }
                                                            if (SCMRecords.Count > 0)
                                                            {
                                                                sucess = RiskCategoryManagement.CreateStepChecklistMapping(SCMRecords);
                                                                SCMRecords.Clear();
                                                            }
                                                        }
                                                    }
                                                    #endregion
                                                }
                                                #endregion

                                                //update PersonResponsible and Owner ***
                                                #region Update PersonResponsible and Owner
                                                bool RATUpdate =RiskCategoryManagement.UpdateRecord((long)item.Processid, (long)item.SubProcessid, riskcategorycreation.Id,
                                                Convert.ToInt32(item.Frequency), BranchitemId, verticalitem, (long)item.ActivityId, item.Controldescription, item.ProcessOwnerID, item.ControlOwnerID);
                                                #endregion
                                            }

                                            #region AuditAssignment
                                            if (!(RiskCategoryManagement.Exists_Internal_AuditAssignment(BranchitemId, verticalitem, CustomerID)))
                                            {
                                                Internal_AuditAssignment InA = new Internal_AuditAssignment();
                                                InA.AssignedTo = "I";
                                                InA.VerticalID = (int)verticalitem;
                                                InA.CustomerBranchid = BranchitemId;
                                                InA.ExternalAuditorId = -1;
                                                InA.CreatedBy = Common.AuthenticationHelper.UserID;
                                                InA.CreatedOn = DateTime.Now.Date;
                                                InA.IsActive = false;
                                                InA.CustomerId = (int)Common.AuthenticationHelper.CustomerID;
                                                entities.Internal_AuditAssignment.Add(InA);
                                                entities.SaveChanges();
                                            }
                                            #endregion
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        #region risk Exists                            
                                        int riskcreationId = RiskCategoryManagement.GetRiskCreationIdNew(item.RiskDesc, item.ControlObjective, Convert.ToInt32(item.Processid), CustomerID, BranchitemId, (int)item.SubProcessid, item.ObjectiveRef);
                                        foreach (var riskcategoryitem in masterriskcategory)
                                        {
                                            RiskCategoryMapping riskcategorymapping = new RiskCategoryMapping()
                                            {
                                                RiskCategoryCreationId = riskcreationId,
                                                RiskCategoryId = Convert.ToInt32(riskcategoryitem),
                                                ProcessId = item.Processid,
                                                SubProcessId = item.SubProcessid,
                                                IsActive = true,
                                            };
                                            if (!RiskCategoryManagement.RiskCategoryMappingExists(Convert.ToInt64(riskcategoryitem), riskcreationId))
                                            {
                                                RiskCategoryManagement.CreateRiskCategoryMapping(riskcategorymapping);
                                            }
                                        }
                                        foreach (var asserationitem in masterasseration)
                                        {
                                            AssertionsMapping assertionsmapping = new AssertionsMapping()
                                            {
                                                RiskCategoryCreationId = riskcreationId,
                                                AssertionId = Convert.ToInt32(asserationitem),
                                                ProcessId = item.Processid,
                                                SubProcessId = item.SubProcessid,
                                                IsActive = true,
                                            };
                                            if (!AssertionsMappingExists(Convert.ToInt64(asserationitem), riskcreationId))
                                            {
                                                RiskCategoryManagement.CreateAssertionsMapping(assertionsmapping);
                                            }
                                        }
                                        foreach (var verticalitem in masterverticalist)
                                        {
                                            long auditStepMasterID = -1;
                                            if (!(RiskCategoryManagement.Exists((long)item.Processid, (long)item.SubProcessid, riskcreationId,
                                                Convert.ToInt32(item.Frequency), BranchitemId, verticalitem, (long)item.ActivityId, item.Controldescription)))
                                            {
                                                #region Control not Exists
                                                RiskActivityTransaction riskactivitytransaction = new RiskActivityTransaction()
                                                {
                                                    CustomerBranchId = BranchitemId,
                                                    VerticalsId = verticalitem,
                                                    ProcessId = (long)item.Processid,
                                                    SubProcessId = (long)item.SubProcessid,
                                                    RiskCreationId = riskcreationId,
                                                    ControlDescription = item.Controldescription,
                                                    MControlDescription = "NA",
                                                    PersonResponsible = (long)item.ProcessOwnerID,
                                                    EffectiveDate = DateTime.Now.Date,
                                                    Key_Value = 0,
                                                    PrevationControl = 0,
                                                    AutomatedControl = 0,
                                                    Frequency = 0,
                                                    IsDeleted = false,
                                                    GapDescription = "NA",
                                                    Recommendations = item.ProposedRecommendations,
                                                    ActionRemediationplan = "NA",
                                                    IPE = "NA",
                                                    ERPsystem = "NA",
                                                    FRC = "NA",
                                                    UniqueReferred = "NA",
                                                    TestStrategy = "NA",
                                                    RiskRating = 1,
                                                    ControlRating = -1,
                                                    ProcessScore = 0,
                                                    ActivityID = 0,
                                                    KC2 = 0,
                                                    Primary_Secondary = 0,
                                                    ControlOwner = (long)item.ControlOwnerID,
                                                    ISICFR_Close = true,
                                                    RCMType = "ARS",
                                                };
                                                sucess = CreateRiskActivityTransaction(riskactivitytransaction);
                                                auditStepMasterID = RiskCategoryManagement.GetAuditStepMasterID(item.AuditStep);
                                                if (auditStepMasterID == 0)
                                                {
                                                    AuditStepMaster newASM = new AuditStepMaster()
                                                    {
                                                        AuditStep = item.AuditStep
                                                    };

                                                    if (RiskCategoryManagement.CreateAuditStepMaster(newASM))
                                                        auditStepMasterID = newASM.ID;
                                                }
                                                if (!(RiskCategoryManagement.RiskActivityToBeDoneMappingExists(riskcreationId, riskactivitytransaction.Id,
                                                    BranchitemId, (int)verticalitem, (int)item.Processid, (int)item.SubProcessid, item.AuditStep)))
                                                {
                                                    if (auditStepMasterID != 0)
                                                    {
                                                        if (!(RiskCategoryManagement.RiskActivityToBeDoneMappingExists1(riskcreationId, BranchitemId, (int)verticalitem, (int)item.Processid, (int)item.SubProcessid, item.AuditStep)))
                                                        {
                                                            RiskActivityToBeDoneMapping RATBDM = new RiskActivityToBeDoneMapping()
                                                            {
                                                                AuditStepMasterID = auditStepMasterID,
                                                                RiskCategoryCreationId = riskcreationId,
                                                                RiskActivityId = riskactivitytransaction.Id,
                                                                CustomerBranchID = BranchitemId,
                                                                VerticalID = verticalitem,
                                                                ProcessId = (long)item.Processid,
                                                                SubProcessId = item.SubProcessid,
                                                                AuditObjective = item.AuditObjective,
                                                                ActivityTobeDone = item.AuditStep,
                                                                Analyis_To_Be_Performed = item.AnalysistobePerformed,
                                                                Rating = 1,
                                                                IsActive = true,
                                                                RCMType = "ARS",
                                                            };  
                                                            sucess = RiskCategoryManagement.CreateRiskActivityTBDMapping(RATBDM);
                                                            
                                                            #region   PreRequisite
                                                            if (!String.IsNullOrEmpty(item.PreRequisiteList))
                                                            {
                                                                List<StepChecklistMapping> SCMRecords = new List<StepChecklistMapping>();
                                                                string prerequisite = item.PreRequisiteList;
                                                                string[] split = prerequisite.Split('|');
                                                                if (split.Length > 0)
                                                                {
                                                                    string prerequisiteName = string.Empty;
                                                                    for (int rs = 0; rs < split.Length; rs++)
                                                                    {
                                                                        prerequisiteName = Convert.ToString(split[rs].ToString().Trim());
                                                                        if (!String.IsNullOrEmpty(prerequisiteName))
                                                                        {
                                                                            StepChecklistMapping SCM = new StepChecklistMapping();
                                                                            SCM.ATBDID = RATBDM.ID;
                                                                            SCM.ChecklistDocument = prerequisiteName;
                                                                            SCM.IsActive = true;
                                                                            SCMRecords.Add(SCM);
                                                                        }
                                                                    }

                                                                    if (SCMRecords.Count > 0)
                                                                    {
                                                                        sucess = RiskCategoryManagement.CreateStepChecklistMapping(SCMRecords);
                                                                        SCMRecords.Clear();
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        #endregion
                                                    }
                                                }
                                                else
                                                {
                                                    sucess = RiskCategoryManagement.RiskActivityToBeDoneMappingUpdate(riskcreationId, riskactivitytransaction.Id, BranchitemId, (int)verticalitem, (int)item.Processid, (int)item.SubProcessid, item.AuditStep, item.AuditObjective);
                                                }
                                                #endregion
                                            }
                                            else
                                            {
                                                #region Control Exists
                                                int controlactivityid = RiskCategoryManagement.GetControlCreationIdNew((long)item.Processid, (int)item.SubProcessid, riskcreationId, item.Controldescription, (int)item.Frequency, BranchitemId, verticalitem, (long)item.ActivityId);
                                                auditStepMasterID = RiskCategoryManagement.GetAuditStepMasterID(item.AuditStep);
                                                if (auditStepMasterID == 0)
                                                {
                                                    AuditStepMaster newASM = new AuditStepMaster()
                                                    {
                                                        AuditStep = item.AuditStep
                                                    };

                                                    if (RiskCategoryManagement.CreateAuditStepMaster(newASM))
                                                        auditStepMasterID = newASM.ID;
                                                }
                                                if (!(RiskCategoryManagement.RiskActivityToBeDoneMappingExists(riskcreationId, controlactivityid,
                                                    BranchitemId, (int)verticalitem, (int)item.Processid, (int)item.SubProcessid, item.AuditStep)))
                                                {
                                                    if (auditStepMasterID != 0)
                                                    {
                                                        RiskActivityToBeDoneMapping RATBDM = new RiskActivityToBeDoneMapping()
                                                        {
                                                            AuditStepMasterID = auditStepMasterID,
                                                            RiskCategoryCreationId = riskcreationId,
                                                            RiskActivityId = controlactivityid,
                                                            CustomerBranchID = BranchitemId,
                                                            VerticalID = verticalitem,
                                                            ProcessId = (long)item.Processid,
                                                            SubProcessId = item.SubProcessid,
                                                            AuditObjective = item.AuditObjective,
                                                            ActivityTobeDone = item.AuditStep,
                                                            Analyis_To_Be_Performed = item.AnalysistobePerformed,
                                                            Rating = 1,
                                                            IsActive = true,
                                                            RCMType = "ARS",
                                                        };
                                                        sucess = RiskCategoryManagement.CreateRiskActivityTBDMapping(RATBDM);

                                                        #region   PreRequisite
                                                        if (!String.IsNullOrEmpty(item.PreRequisiteList))
                                                        {
                                                            List<StepChecklistMapping> SCMRecords = new List<StepChecklistMapping>();
                                                            string prerequisite = item.PreRequisiteList;
                                                            string[] split = prerequisite.Split('|');
                                                            if (split.Length > 0)
                                                            {
                                                                string prerequisiteName = string.Empty;
                                                                for (int rs = 0; rs < split.Length; rs++)
                                                                {
                                                                    prerequisiteName = Convert.ToString(split[rs].ToString().Trim());
                                                                    if (!String.IsNullOrEmpty(prerequisiteName))
                                                                    {
                                                                        StepChecklistMapping SCM = new StepChecklistMapping();
                                                                        SCM.ATBDID = RATBDM.ID;
                                                                        SCM.ChecklistDocument = prerequisiteName;
                                                                        SCM.IsActive = true;
                                                                        SCMRecords.Add(SCM);
                                                                    }
                                                                }

                                                                if (SCMRecords.Count > 0)
                                                                {
                                                                    sucess = RiskCategoryManagement.CreateStepChecklistMapping(SCMRecords);
                                                                    SCMRecords.Clear();
                                                                }
                                                            }
                                                        }
                                                        #endregion
                                                    }
                                                }
                                                else
                                                {
                                                    sucess = RiskCategoryManagement.RiskActivityToBeDoneMappingUpdate(riskcreationId, controlactivityid, BranchitemId, (int)verticalitem, (int)item.Processid, (int)item.SubProcessid, item.AuditStep, item.AuditObjective);
                                                    int atbdid = RiskCategoryManagement.GetATBDID(riskcreationId, controlactivityid, BranchitemId, (int)verticalitem, (int)item.Processid, (int)item.SubProcessid, item.AuditStep);
                                                    #region   PreRequisite
                                                    if (!String.IsNullOrEmpty(item.PreRequisiteList))
                                                    {
                                                        List<StepChecklistMapping> SCMRecords = new List<StepChecklistMapping>();
                                                        string prerequisite = item.PreRequisiteList;
                                                        string[] split = prerequisite.Split('|');
                                                        if (split.Length > 0)
                                                        {
                                                            string prerequisiteName = string.Empty;
                                                            for (int rs = 0; rs < split.Length; rs++)
                                                            {
                                                                prerequisiteName = Convert.ToString(split[rs].ToString().Trim());
                                                                if (!String.IsNullOrEmpty(prerequisiteName))
                                                                {
                                                                    StepChecklistMapping SCM = new StepChecklistMapping();
                                                                    SCM.ATBDID = atbdid;
                                                                    SCM.ChecklistDocument = prerequisiteName;
                                                                    SCM.IsActive = true;
                                                                    if (!(PrerequsiteExistsNew(SCM)))
                                                                    {
                                                                        SCMRecords.Add(SCM);
                                                                    }
                                                                }
                                                            }
                                                            if (SCMRecords.Count > 0)
                                                            {
                                                                sucess = RiskCategoryManagement.CreateStepChecklistMapping(SCMRecords);
                                                                SCMRecords.Clear();
                                                            }
                                                        }
                                                    }
                                                    #endregion
                                                }
                                                #endregion
                                            }
                                        }
                                        #endregion
                                    }
                                    //}
                                    #endregion
                                    RCMMasterUpdate(item.ID);
                                } // for each loop  end

                                #region Aduit Scheduling

                                ExcelWorksheet xlWorksheetMaster1 = xlWorkbook.Workbook.Worksheets["AuditObservations"];
                                int xlrowtemptable1 = xlWorksheetMaster1.Dimension.End.Row;
                                List<long> AuditList = new List<long>();
                                for (int temprow = 2; temprow <= xlrowtemptable1; temprow++)
                                {
                                    Int32 customerBranchId = 0;
                                    long VerticalId = 0;
                                    string period1 = string.Empty;
                                    string financialYear = string.Empty;
                                    string branchName = string.Empty;
                                    string verticalName = string.Empty;
                                    int processId = 0;
                                    int subProcessId = 0;
                                    int OldProcessID = 0;
                                    string AuditStartDate = string.Empty;
                                    string AuditEndDate = string.Empty;
                                    int InternalAuditScheduleOnID = 0;

                                    string ActivityDescription = string.Empty;
                                    string ControlObjective = string.Empty;
                                    string ControlNo = string.Empty;
                                    string ControlDescription = string.Empty;
                                    string AuditStep = string.Empty;
                                    
                                    #region kick off variables
                                    string performerEmailId = string.Empty;
                                    int performerId = -1;
                                    string reviewerEmailId = string.Empty;
                                    int reviewerId = -1;
                                    string reviewer2EmailId = string.Empty;
                                    int reviewer2Id = -1;
                                    string personResponsibleEmailId = string.Empty;
                                    int personResponsibleId = -1;
                                    #endregion

                                    #region Quanterly Variable
                                    int OldProcessIDAprJun = -1;
                                    int InternalAuditScheduleOnIDAprJun = 0;
                                    int OldProcessIDJulSep = -1;
                                    int InternalAuditScheduleOnIDJulSep = 0;
                                    int OldProcessIDOctDec = -1;
                                    int InternalAuditScheduleOnIDOctDec = 0;
                                    int OldProcessIDJanMar = -1;
                                    int InternalAuditScheduleOnIDJanMar = 0;
                                    #endregion

                                    #region Monthly Variable
                                    int InternalAuditScheduleOnIDApr = 0;
                                    int InternalAuditScheduleOnIDMay = 0;
                                    int InternalAuditScheduleOnIDJun = 0;
                                    int InternalAuditScheduleOnIDJul = 0;
                                    int InternalAuditScheduleOnIDAug = 0;
                                    int InternalAuditScheduleOnIDSep = 0;
                                    int InternalAuditScheduleOnIDOct = 0;
                                    int InternalAuditScheduleOnIDNov = 0;
                                    int InternalAuditScheduleOnIDDec = 0;
                                    int InternalAuditScheduleOnIDJan = 0;
                                    int InternalAuditScheduleOnIDFeb = 0;
                                    int InternalAuditScheduleOnIDMar = 0;


                                    int OldProcessIDApr = 0;
                                    int OldProcessIDMay = 0;
                                    int OldProcessIDJun = 0;
                                    int OldProcessIDJul = 0;
                                    int OldProcessIDAug = 0;
                                    int OldProcessIDSep = 0;
                                    int OldProcessIDOct = 0;
                                    int OldProcessIDNov = 0;
                                    int OldProcessIDDec = 0;
                                    int OldProcessIDJan = 0;
                                    int OldProcessIDFeb = 0;
                                    int OldProcessIDMar = 0;
                                    #endregion

                                    #region Audit Perform Variable
                                    string ProcessWalkthrough = string.Empty;
                                    string ActualWorkDone = string.Empty;
                                    string Population = string.Empty;
                                    string Sample = string.Empty;
                                    string Remark = string.Empty;
                                    string ObservationTitle = string.Empty;
                                    string Observation = string.Empty;
                                    string BriefObservation = string.Empty;
                                    string ObservationBackgroud = string.Empty;
                                    string BusinessImplication = string.Empty;
                                    string RootCause = string.Empty;
                                    string FinancialImpact = string.Empty;
                                    string Recomendation = string.Empty;
                                    string ManagementResponse = string.Empty;
                                    string TimeLine = string.Empty;
                                    string Risk = string.Empty;
                                    int ownerID = -1;
                                    string ObservationRating = string.Empty;
                                    string ObservationCategory = string.Empty;
                                    string ObservationSubCategory = string.Empty;
                                    string ActivityToBeDone = string.Empty;
                                    int ObservationCategoryId = -1;
                                    int ObservationSubCategoryId = -1;
                                    string StatusOpenClose = string.Empty;
                                    bool successAuditCreate = false;
                                    bool successKickOff = false;
                                    bool successAuditPerform = false;
                                    bool successImplemenation = false;
                                    string AuditScheduleStartDate = string.Empty;
                                    string AuditScheduleEndDate = string.Empty;
                                    string AuditMethodology = string.Empty;
                                    #endregion

                                    #region AuditMethodology
                                    if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[temprow, 20].Text.ToString().Trim()))
                                    {
                                        AuditMethodology = xlWorksheetvalidation.Cells[temprow, 20].Text.Trim();
                                    }
                                    #endregion

                                    #region Branch Name
                                    if (!String.IsNullOrEmpty(xlWorksheetMaster.Cells[temprow, 1].Text.ToString().Trim()))
                                    {
                                        branchName = Convert.ToString(xlWorksheetMaster.Cells[temprow, 1].Text).Trim();
                                    }
                                    if (!string.IsNullOrEmpty(branchName))
                                    {
                                        customerBranchId = CustomerBranchManagement.GetByNameFromAuditDb(branchName, CustomerID).ID;
                                    }
                                    #endregion

                                    #region Vertical Name
                                    if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[temprow, 2].Text.ToString().Trim()))
                                    {
                                        verticalName = xlWorksheetvalidation.Cells[temprow, 2].Text.Trim();
                                    }
                                    else
                                    {
                                        verticalName = "NA";
                                    }

                                    VerticalId = UserManagementRisk.GetVerticalIDByName(verticalName, CustomerID);

                                    #endregion

                                    #region Process Name
                                    if (!String.IsNullOrEmpty(xlWorksheetMaster.Cells[temprow, 3].Text.ToString().Trim()))
                                    {
                                        processId = ProcessManagement.GetProcessIDByName(Regex.Replace(xlWorksheetMaster.Cells[temprow, 3].Text.ToString().Trim(), @"\t|\n|\r", ""), CustomerID);
                                    }
                                    #endregion

                                    #region SubProcess
                                    if (!String.IsNullOrEmpty(xlWorksheetMaster.Cells[temprow, 4].Text.ToString().Trim()))
                                    {
                                        subProcessId = ProcessManagement.GetSubProcessIDByName(Regex.Replace(xlWorksheetMaster.Cells[temprow, 4].Text.ToString().Trim(), @"\t|\n|\r", ""), processId);
                                    }
                                    #endregion
                                    #region FinancialYear
                                    if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[temprow, 6].Text.ToString().Trim()))
                                    {
                                        string fyStr = xlWorksheetvalidation.Cells[temprow, 6].Text.ToString().Trim();
                                        string[] financialYearArray = fyStr.Split('-');
                                        string fy = financialYearArray[0].Trim().ToString();
                                        string fy1 = financialYearArray[1].Trim().ToString();
                                        financialYear = fy + "-" + fy1;
                                    }
                                    #endregion

                                    #region Period
                                    if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[temprow, 7].Text.ToString().Trim()))
                                    {
                                        period1 = xlWorksheetvalidation.Cells[temprow, 7].Text.ToString().Trim();
                                    }
                                    #endregion

                                    #region Performer
                                    if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[temprow, 8].Text.ToString().Trim()))
                                    {
                                        performerEmailId = xlWorksheetvalidation.Cells[temprow, 8].Text.ToString().Trim();
                                    }
                                    if (!String.IsNullOrEmpty(performerEmailId))
                                    {
                                        //performerId = UserManagement.GetUserIdByEmail(performerEmailId, CustomerID, Convert.ToInt32(customerBranchId));
                                        performerId = UserManagement.GetUserIdByEmail(performerEmailId, CustomerID);
                                    }
                                    #endregion

                                    #region Reviewer
                                    if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[temprow, 9].Text.ToString().Trim()))
                                    {
                                        reviewerEmailId = xlWorksheetvalidation.Cells[temprow, 9].Text.ToString().Trim();
                                    }
                                    if (!String.IsNullOrEmpty(reviewerEmailId))
                                    {
                                        //reviewerId = UserManagement.GetUserIdByEmail(reviewerEmailId, CustomerID, Convert.ToInt32(customerBranchId));
                                        reviewerId = UserManagement.GetUserIdByEmail(reviewerEmailId, CustomerID);
                                    }
                                    #endregion

                                    #region Reviewer 2
                                    if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[temprow, 10].Text.ToString().Trim()))
                                    {
                                        reviewer2EmailId = xlWorksheetvalidation.Cells[temprow, 10].Text.ToString().Trim();
                                    }
                                    if (!String.IsNullOrEmpty(reviewer2EmailId))
                                    {
                                        reviewer2Id = UserManagement.GetUserIdByEmail(reviewer2EmailId, CustomerID);
                                    }
                                    #endregion

                                    #region Audit Start Date
                                    if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[temprow, 13].Text.ToString().Trim()))
                                    {
                                        DateTime? aaaa = CleanDateField(xlWorksheetvalidation.Cells[temprow, 13].Text.ToString().Trim());
                                        DateTime dt1 = Convert.ToDateTime(aaaa);
                                        DateTime dt2 = GetDate(dt1.ToString("dd/MM/yyyy"));
                                        AuditStartDate = Convert.ToString(dt2.ToShortDateString());
                                    }
                                    #endregion

                                    #region Audit End Date
                                    if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[temprow, 14].Text.ToString().Trim()))
                                    {
                                        DateTime? aaaa = CleanDateField(xlWorksheetvalidation.Cells[temprow, 14].Text.ToString().Trim());
                                        DateTime dt1 = Convert.ToDateTime(aaaa);
                                        DateTime dt2 = GetDate(dt1.ToString("dd/MM/yyyy"));
                                        AuditEndDate = Convert.ToString(dt2.ToShortDateString());

                                        //AuditEndDate = xlWorksheetvalidation.Cells[temprow, 14].Text.Trim();
                                        //string tempDate = null;
                                        //tempDate = DateTime.ParseExact(AuditEndDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                        //AuditEndDate = string.Empty;
                                        //AuditEndDate = tempDate;
                                    }
                                    #endregion

                                    #region Activity To Be Done
                                    if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[temprow, 15].Text.ToString().Trim()))
                                    {
                                        ActivityToBeDone = Convert.ToString(GetSafeTagName(xlWorksheetvalidation.Cells[temprow, 15].Text.ToString().Trim())).Trim();
                                    }
                                    #endregion

                                    #region Risk
                                    if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[temprow, 17].Text.ToString().Trim()))
                                    {
                                        Risk = Convert.ToString(GetSafeTagName(xlWorksheetvalidation.Cells[temprow, 17].Text.ToString().Trim())).Trim();                                       
                                    }
                                    #endregion

                                    #region Control Owner
                                    //if (!String.IsNullOrEmpty(xlWorksheetMaster.Cells[temprow, 14].Text.ToString().Trim()))
                                    //{
                                    //    ownerID = RiskCategoryManagement.GetUserIDByName(xlWorksheetMaster.Cells[temprow, 13].Text.ToString().Trim(), CustomerID, xlWorksheetMaster.Cells[temprow, 14].Text.ToString().Trim());
                                    //}

                                    if (!String.IsNullOrEmpty(xlWorksheetMaster.Cells[temprow, 38].Text.ToString().Trim()))
                                    {
                                        ownerID = UserManagement.GetUserIdByEmail(xlWorksheetMaster.Cells[temprow, 38].Text.ToString().Trim(), CustomerID);
                                    }
                                    #endregion

                                    #region Process Walkthrough
                                    if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[temprow, 21].Text.ToString().Trim()))
                                    {
                                        ProcessWalkthrough = Convert.ToString(GetSafeTagName(xlWorksheetvalidation.Cells[temprow, 21].Text.ToString().Trim())).Trim();
                                    }
                                    #endregion

                                    #region Actual Work Done
                                    if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[temprow, 22].Text.ToString().Trim()))
                                    {                                        
                                        ActualWorkDone = Convert.ToString(GetSafeTagName(xlWorksheetvalidation.Cells[temprow, 22].Text.ToString().Trim())).Trim();
                                    }
                                    #endregion


                                    #region Population
                                    if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[temprow, 23].Text.ToString().Trim()))
                                    {
                                        Population = Convert.ToString(GetSafeTagName(xlWorksheetvalidation.Cells[temprow, 23].Text.ToString().Trim())).Trim();
                                    }
                                    #endregion

                                    #region Sample
                                    if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[temprow, 24].Text.ToString().Trim()))
                                    {
                                        Sample = Convert.ToString(GetSafeTagName(xlWorksheetvalidation.Cells[temprow, 24].Text.ToString().Trim())).Trim();
                                    }
                                    #endregion

                                    #region Remark
                                    if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[temprow, 25].Text.ToString().Trim()))
                                    {
                                        Remark = Convert.ToString(GetSafeTagName(xlWorksheetvalidation.Cells[temprow, 25].Text.ToString().Trim())).Trim();                                        
                                    }
                                    #endregion

                                    #region Observation Title
                                    if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[temprow, 26].Text.ToString().Trim()))
                                    {
                                        ObservationTitle = Convert.ToString(GetSafeTagName(xlWorksheetvalidation.Cells[temprow, 26].Text.ToString().Trim())).Trim();                                        
                                    }
                                    #endregion

                                    #region Observation

                                    if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[temprow, 27].Text.ToString().Trim()))
                                    {
                                        Observation = Convert.ToString(GetSafeTagName(xlWorksheetvalidation.Cells[temprow, 27].Text.ToString().Trim())).Trim();                                        
                                    }
                                    #endregion

                                    #region Brief Observation
                                    if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[temprow, 28].Text.ToString().Trim()))
                                    {
                                        BriefObservation = Convert.ToString(GetSafeTagName(xlWorksheetvalidation.Cells[temprow, 28].Text.ToString().Trim())).Trim();
                                    }
                                    #endregion

                                    #region Observation Background
                                    if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[temprow, 29].Text.ToString().Trim()))
                                    {
                                        ObservationBackgroud = Convert.ToString(GetSafeTagName(xlWorksheetvalidation.Cells[temprow, 29].Text.ToString().Trim())).Trim();                                        
                                    }
                                    #endregion

                                    #region Business Implication

                                    if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[temprow, 30].Text.ToString().Trim()))
                                    {
                                        BusinessImplication = Convert.ToString(GetSafeTagName(xlWorksheetvalidation.Cells[temprow, 30].Text.ToString().Trim())).Trim();                                        
                                    }
                                    #endregion

                                    #region Root Cause
                                    if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[temprow, 31].Text.ToString().Trim()))
                                    {
                                        RootCause = Convert.ToString(GetSafeTagName(xlWorksheetvalidation.Cells[temprow, 31].Text.ToString().Trim())).Trim();
                                    }
                                    #endregion

                                    #region Financial Impact
                                    if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[temprow, 32].Text.ToString().Trim()))
                                    {
                                        FinancialImpact = xlWorksheetvalidation.Cells[temprow, 32].Text.Trim();
                                    }

                                    #endregion

                                    #region Recomendation
                                    if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[temprow, 33].Text.ToString().Trim()))
                                    {
                                        Recomendation = Convert.ToString(GetSafeTagName(xlWorksheetvalidation.Cells[temprow, 33].Text.ToString().Trim())).Trim();
                                    }
                                    #endregion

                                    #region Management Response
                                    if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[temprow, 34].Text.ToString().Trim()))
                                    {
                                        ManagementResponse = Convert.ToString(GetSafeTagName(xlWorksheetvalidation.Cells[temprow, 34].Text.ToString().Trim())).Trim();
                                    }
                                    #endregion

                                    #region TimeLine
                                    if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[temprow, 35].Text.ToString().Trim()))
                                    {
                                        //TimeLine = xlWorksheetvalidation.Cells[temprow, 35].Text.ToString().Trim();
                                        DateTime? aaaa = CleanDateField(xlWorksheetvalidation.Cells[temprow, 35].Text.ToString().Trim());
                                        DateTime dt1 = Convert.ToDateTime(aaaa);
                                        DateTime dt2 = GetDate(dt1.ToString("dd/MM/yyyy"));
                                        TimeLine = Convert.ToString(dt2.ToShortDateString());
                                    }
                                    #endregion

                                    #region Process Owner
                                    if (!String.IsNullOrEmpty(xlWorksheetMaster.Cells[temprow, 36].Text.ToString().Trim()))
                                    {
                                        if (!String.IsNullOrEmpty(xlWorksheetMaster.Cells[temprow, 37].Text.ToString().Trim()))
                                        {
                                            personResponsibleId = RiskCategoryManagement.GetUserIDByName(xlWorksheetMaster.Cells[temprow, 36].Text.ToString().Trim(), CustomerID, xlWorksheetMaster.Cells[temprow, 37].Text.ToString().Trim());
                                        }
                                    }
                                    #endregion

                                    #region Observation Rating
                                    if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[temprow, 39].Text.ToString().Trim()))
                                    {
                                        ObservationRating = xlWorksheetvalidation.Cells[temprow, 39].Text.Trim();
                                    }
                                    #endregion

                                    #region Observation Category
                                    if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[temprow, 40].Text.ToString().Trim()))
                                    {
                                        ObservationCategory = xlWorksheetvalidation.Cells[temprow, 40].Text.Trim();
                                    }
                                    #endregion

                                    #region Observation Sub Category
                                    if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[temprow, 41].Text.ToString().Trim()))
                                    {
                                        ObservationSubCategory = xlWorksheetvalidation.Cells[temprow, 41].Text.Trim();
                                    }
                                    #endregion

                                    #region Status (Open/Close)
                                    if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[temprow, 42].Text.ToString().Trim()))
                                    {
                                        StatusOpenClose = xlWorksheetvalidation.Cells[temprow, 42].Text.Trim();
                                    }


                                    #region Audit Schedule Start Date
                                    if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[temprow, 11].Text.ToString().Trim()))
                                    {
                                        AuditScheduleStartDate = xlWorksheetvalidation.Cells[temprow, 11].Text.Trim();
                                    }

                                    if (!String.IsNullOrEmpty(AuditScheduleStartDate))
                                    {
                                        //string tempDate = null;
                                        //tempDate = DateTime.ParseExact(AuditScheduleStartDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                        //AuditScheduleStartDate = string.Empty;
                                        //AuditScheduleStartDate = tempDate;

                                        //string c = Convert.ToString(AuditScheduleStartDate).Trim();
                                        DateTime? aaaa = CleanDateField(AuditScheduleStartDate);
                                        DateTime dt1 = Convert.ToDateTime(aaaa);
                                        DateTime dt2 = GetDate(dt1.ToString("dd/MM/yyyy"));
                                        AuditScheduleStartDate = Convert.ToString(dt2.ToShortDateString());
                                    }
                                    #endregion

                                    #region Audit Schedule End Date
                                    if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[temprow, 12].Text.ToString().Trim()))
                                    {
                                        AuditScheduleEndDate = xlWorksheetvalidation.Cells[temprow, 12].Text.Trim();
                                    }
                                    if (!String.IsNullOrEmpty(AuditScheduleEndDate))
                                    {

                                        DateTime? aaaa = CleanDateField(AuditScheduleEndDate);
                                        DateTime dt1 = Convert.ToDateTime(aaaa);
                                        DateTime dt2 = GetDate(dt1.ToString("dd/MM/yyyy"));
                                        AuditScheduleEndDate = Convert.ToString(dt2.ToShortDateString());

                                        //string tempDate = null;
                                        //tempDate = DateTime.ParseExact(AuditScheduleEndDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                        //AuditScheduleEndDate = string.Empty;
                                        //AuditScheduleEndDate = tempDate;
                                    }
                                    #endregion

                                    #endregion
                                    
                                    #region Risk Description
                                    if (!String.IsNullOrEmpty(xlWorksheetMaster.Cells[temprow, 17].Text.ToString().Trim()))
                                    {
                                        ActivityDescription = Convert.ToString(GetSafeTagName(xlWorksheetMaster.Cells[temprow, 17].Text.ToString().Trim())).Trim();
                                    }
                                    #endregion

                                    #region Control Objective
                                    if (!String.IsNullOrEmpty(xlWorksheetMaster.Cells[temprow, 18].Text.ToString().Trim()))
                                    {
                                        ControlObjective = Convert.ToString(GetSafeTagName(xlWorksheetMaster.Cells[temprow, 18].Text.ToString().Trim())).Trim();
                                    }
                                    #endregion

                                    #region Objective Ref.
                                    if (!String.IsNullOrEmpty(xlWorksheetMaster.Cells[temprow, 16].Text.ToString().Trim()))
                                    {
                                        ControlNo = Convert.ToString(xlWorksheetMaster.Cells[temprow, 16].Text).Trim();
                                    }
                                    #endregion

                                    #region  Control Description
                                    if (!String.IsNullOrEmpty(xlWorksheetMaster.Cells[temprow, 19].Text.ToString().Trim()))
                                    {
                                       // ControlDescription = Convert.ToString(xlWorksheetMaster.Cells[temprow, 19].Text).Trim();
                                        ControlDescription = Convert.ToString(GetSafeTagName(xlWorksheetMaster.Cells[temprow, 19].Text.ToString().Trim())).Trim();
                                    }
                                    #endregion

                                    #region Activity To Be Done
                                    if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[temprow, 15].Text.ToString().Trim()))
                                    {
                                        AuditStep = Convert.ToString(GetSafeTagName(xlWorksheetvalidation.Cells[temprow, 15].Text.ToString().Trim())).Trim();
                                    }
                                    #endregion

                                    long auditStepIDs = RiskCategoryManagement.GetAuditStepMasterID(AuditStep);
                                    long RiskCategoryCreationID = RiskCategoryManagement.GetRiskCategoryCreationID(ControlNo, processId, subProcessId, CustomerID, (int)customerBranchId, ActivityDescription, ControlObjective);
                                    long RiskActivityTransactionID= RiskCategoryManagement.GetRiskActivityTransactionID(processId, subProcessId, RiskCategoryCreationID, customerBranchId, VerticalId, ControlDescription);
                                    long ATBDIDS = RiskCategoryManagement.GetRiskActivityToBeDoneMappingID(RiskCategoryCreationID, RiskActivityTransactionID, customerBranchId, (int)VerticalId, processId, subProcessId, auditStepIDs);
                                    string period = string.Empty;
                                    period = period1;

                                    if (customerBranchId != 0 && !string.IsNullOrEmpty(financialYear) && !string.IsNullOrEmpty(period) && CustomerID != 0 && VerticalId != 0)
                                    {
                                        period = GetPeriod(period);
                                        long AuditId = 0;
                                        if (!(UserManagementRisk.IsExistAuditClosureDetails(CustomerID, customerBranchId, VerticalId, period, financialYear)))
                                        {
                                            AuditId = UserManagementRisk.CreateAuditID(customerBranchId, VerticalId, financialYear, period, CustomerID);
                                        }
                                        else
                                        {
                                            AuditId = UserManagementRisk.GetAuditIdFromAuditClosureDetails(CustomerID, customerBranchId, VerticalId, period, financialYear);
                                        }
                                        if (!AuditList.Contains(AuditId))
                                        {
                                            AuditList.Add(AuditId);
                                        }
                                        if (AuditId > 0)
                                        {
                                            #region Audit Scheduling
                                            int OldProcessIDHalfyearly1 = -1;
                                            int OldProcessIDHalfyearly2 = -1;
                                            int InternalAuditScheduleOnIDHalfyearly1 = 0;
                                            int InternalAuditScheduleOnIDHalfyearly2 = 0;
                                            int noofphases = -1;
                                            
                                            #region Annually
                                            if (period == "Annually" || period == "ANNUALLY")
                                            {
                                                if (UserManagementRisk.IsExistAuditScheduling_ScheduleFirst(Convert.ToInt64(customerBranchId), Convert.ToInt16(VerticalId), processId, financialYear, AuditId, subProcessId, period))
                                                {
                                                    #region Annually
                                                    if (processId != OldProcessID)
                                                    {
                                                        OldProcessID = processId;
                                                        DateTime? dateAuditStartDate = null;
                                                        DateTime? dateAuditEndDate = null;

                                                        InternalAuditScheduling internalAuditScheduling = UserManagementRisk.GeDatestAuditScheduling_ScheduleFirst(Convert.ToInt64(customerBranchId), Convert.ToInt64(VerticalId), processId, financialYear, AuditId, subProcessId, period);
                                                        if (internalAuditScheduling != null)
                                                        {
                                                            dateAuditStartDate = internalAuditScheduling.StartDate;
                                                            dateAuditEndDate = internalAuditScheduling.EndDate;
                                                        }
                                                        else
                                                        {
                                                            dateAuditStartDate = Convert.ToDateTime(AuditScheduleStartDate);
                                                            dateAuditEndDate = Convert.ToDateTime(AuditScheduleEndDate);
                                                        }

                                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                        Internalauditscheduling.CustomerBranchId = customerBranchId;
                                                        Internalauditscheduling.FinancialYear = financialYear;
                                                        Internalauditscheduling.TermName = period;
                                                        Internalauditscheduling.TermStatus = true;
                                                        Internalauditscheduling.Process = processId;
                                                        Internalauditscheduling.ISAHQMP = "A";
                                                        Internalauditscheduling.StartDate = dateAuditStartDate;
                                                        Internalauditscheduling.EndDate = dateAuditEndDate;
                                                        Internalauditscheduling.VerticalID = Convert.ToInt32(VerticalId);
                                                        Internalauditscheduling.AuditID = AuditId;
                                                        Internalauditscheduling.Createdby = Common.AuthenticationHelper.UserID;
                                                        Internalauditscheduling.CreatedOn = DateTime.Now;
                                                        if (processId != 0 && AuditId != 0)
                                                        {
                                                            if (!UserManagementRisk.IsExistsInInternalAuditSchedulings(Internalauditscheduling.Process, Internalauditscheduling.CustomerBranchId, Internalauditscheduling.FinancialYear, Internalauditscheduling.TermName, Convert.ToInt32(Internalauditscheduling.VerticalID), Convert.ToInt32(Internalauditscheduling.AuditID)))
                                                            {
                                                                InternalAuditScheduleOnID = UserManagementRisk.AddDetailsInternalAuditSchedulingTable_ScheduleFirst(Internalauditscheduling);
                                                            }
                                                            else
                                                            {
                                                                InternalAuditScheduling InternalAuditSchedulingObject = UserManagementRisk.GetDetailsInternalAuditSchedulingsId(Internalauditscheduling.Process, Internalauditscheduling.CustomerBranchId, Internalauditscheduling.FinancialYear, Internalauditscheduling.TermName, Convert.ToInt32(Internalauditscheduling.VerticalID), Convert.ToInt32(Internalauditscheduling.AuditID));
                                                                if (InternalAuditSchedulingObject != null)
                                                                {
                                                                    InternalAuditScheduleOnID = InternalAuditSchedulingObject.Id;
                                                                }
                                                            }
                                                        }
                                                        AuditScheduling_SubProcessMapping auditScheduling_SubProcessMapping = new AuditScheduling_SubProcessMapping();
                                                        auditScheduling_SubProcessMapping.Process = processId;
                                                        auditScheduling_SubProcessMapping.AuditID = AuditId;
                                                        auditScheduling_SubProcessMapping.SubProcessID = subProcessId;
                                                        auditScheduling_SubProcessMapping.CreatedBy = Common.AuthenticationHelper.UserID;
                                                        auditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                        auditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnID;
                                                        auditScheduling_SubProcessMapping.IsActive = true;
                                                        auditScheduling_SubProcessMapping.IsDeleted = false;
                                                        if (processId != 0 && AuditId != 0 && subProcessId != 0)
                                                        {
                                                            UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(auditScheduling_SubProcessMapping);
                                                        }
                                                        successAuditCreate = true;
                                                    }
                                                    else
                                                    {
                                                        AuditScheduling_SubProcessMapping auditScheduling_SubProcessMapping = new AuditScheduling_SubProcessMapping();
                                                        auditScheduling_SubProcessMapping.Process = processId;
                                                        auditScheduling_SubProcessMapping.AuditID = AuditId;
                                                        auditScheduling_SubProcessMapping.SubProcessID = subProcessId;
                                                        auditScheduling_SubProcessMapping.CreatedBy = Common.AuthenticationHelper.UserID;
                                                        auditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                        auditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnID;
                                                        auditScheduling_SubProcessMapping.IsActive = true;
                                                        auditScheduling_SubProcessMapping.IsDeleted = false;
                                                        if (processId != 0 && AuditId != 0 && subProcessId != 0 && InternalAuditScheduleOnID != 0)
                                                        {
                                                            UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(auditScheduling_SubProcessMapping);
                                                        }
                                                        successAuditCreate = true;
                                                    }
                                                    #endregion
                                                }
                                                //else
                                                //{
                                                //    cvDuplicateEntry.IsValid = false;
                                                //    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                                                //    break;
                                                //}
                                            }
                                            #endregion

                                            #region Half Yearly 
                                            if (period == "Apr-Sep" || period == "APR-SEP" || period == "APRIL-SEPTEMBER")
                                            {
                                                if (UserManagementRisk.IsExistAuditScheduling_ScheduleFirst(Convert.ToInt16(customerBranchId), Convert.ToInt16(VerticalId), processId, financialYear, AuditId, subProcessId, "Apr-Sep"))
                                                {
                                                    if (processId != OldProcessIDHalfyearly1)
                                                    {
                                                        OldProcessIDHalfyearly1 = processId;
                                                        DateTime? dateAuditStartDate = null;
                                                        DateTime? dateAuditEndDate = null;

                                                        InternalAuditScheduling internalAuditScheduling = UserManagementRisk.GeDatestAuditScheduling_ScheduleFirst(Convert.ToInt16(customerBranchId), Convert.ToInt16(VerticalId), processId, financialYear, AuditId, subProcessId, "Apr-Sep");
                                                        if (internalAuditScheduling != null)
                                                        {
                                                            dateAuditStartDate = internalAuditScheduling.StartDate;
                                                            dateAuditEndDate = internalAuditScheduling.EndDate;
                                                        }
                                                        else
                                                        {
                                                            dateAuditStartDate = Convert.ToDateTime(AuditScheduleStartDate);
                                                            dateAuditEndDate = Convert.ToDateTime(AuditScheduleEndDate);
                                                        }

                                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(customerBranchId);
                                                        Internalauditscheduling.FinancialYear = financialYear;
                                                        Internalauditscheduling.TermName = period;
                                                        Internalauditscheduling.TermStatus = true;
                                                        Internalauditscheduling.Process = processId;
                                                        Internalauditscheduling.ISAHQMP = "H";
                                                        Internalauditscheduling.StartDate = dateAuditStartDate;
                                                        Internalauditscheduling.EndDate = dateAuditEndDate;
                                                        Internalauditscheduling.VerticalID = Convert.ToInt32(VerticalId);
                                                        Internalauditscheduling.AuditID = AuditId;
                                                        Internalauditscheduling.Createdby = Common.AuthenticationHelper.UserID; ;
                                                        Internalauditscheduling.CreatedOn = DateTime.Now;
                                                        if (processId != 0 && AuditId != 0)
                                                        {
                                                            if (!UserManagementRisk.IsExistsInInternalAuditSchedulings(Internalauditscheduling.Process, Internalauditscheduling.CustomerBranchId, Internalauditscheduling.FinancialYear, Internalauditscheduling.TermName, Convert.ToInt32(Internalauditscheduling.VerticalID), Convert.ToInt32(Internalauditscheduling.AuditID)))
                                                            {
                                                                InternalAuditScheduleOnIDHalfyearly1 = UserManagementRisk.AddDetailsInternalAuditSchedulingTable_ScheduleFirst(Internalauditscheduling);
                                                            }
                                                            else
                                                            {
                                                                InternalAuditScheduling InternalAuditSchedulingObject = UserManagementRisk.GetDetailsInternalAuditSchedulingsId(Internalauditscheduling.Process, Internalauditscheduling.CustomerBranchId, Internalauditscheduling.FinancialYear, Internalauditscheduling.TermName, Convert.ToInt32(Internalauditscheduling.VerticalID), Convert.ToInt32(Internalauditscheduling.AuditID));
                                                                if (InternalAuditSchedulingObject != null)
                                                                {
                                                                    InternalAuditScheduleOnIDHalfyearly1 = InternalAuditSchedulingObject.Id;
                                                                }
                                                            }
                                                        }
                                                        AuditScheduling_SubProcessMapping auditScheduling_SubProcessMapping = new AuditScheduling_SubProcessMapping();
                                                        auditScheduling_SubProcessMapping.Process = processId;
                                                        auditScheduling_SubProcessMapping.AuditID = AuditId;
                                                        auditScheduling_SubProcessMapping.SubProcessID = subProcessId;
                                                        auditScheduling_SubProcessMapping.CreatedBy = Common.AuthenticationHelper.UserID;
                                                        auditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                        auditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDHalfyearly1;
                                                        auditScheduling_SubProcessMapping.IsActive = true;
                                                        auditScheduling_SubProcessMapping.IsDeleted = false;
                                                        if (processId != 0 && AuditId != 0 && subProcessId != 0)
                                                        {
                                                            UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(auditScheduling_SubProcessMapping);
                                                        }

                                                    }
                                                    else
                                                    {
                                                        AuditScheduling_SubProcessMapping auditScheduling_SubProcessMapping = new AuditScheduling_SubProcessMapping();
                                                        auditScheduling_SubProcessMapping.Process = processId;
                                                        auditScheduling_SubProcessMapping.AuditID = AuditId;
                                                        auditScheduling_SubProcessMapping.SubProcessID = subProcessId;
                                                        auditScheduling_SubProcessMapping.CreatedBy = Common.AuthenticationHelper.UserID;
                                                        auditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                        auditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDHalfyearly1;
                                                        auditScheduling_SubProcessMapping.IsActive = true;
                                                        auditScheduling_SubProcessMapping.IsDeleted = false;
                                                        if (processId != 0 && AuditId != 0 && subProcessId != 0 && InternalAuditScheduleOnIDHalfyearly1 != 0)
                                                        {
                                                            UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(auditScheduling_SubProcessMapping);
                                                        }
                                                    }
                                                }
                                                //else
                                                //{
                                                //    cvDuplicateEntry.IsValid = false;
                                                //    cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled";
                                                //    break;
                                                //}
                                            }

                                            if (period == "Oct-Mar" || period == "OCT-MAR" || period == "OCTOBER-MARCH")
                                            {
                                                if (UserManagementRisk.IsExistAuditScheduling_ScheduleFirst(Convert.ToInt16(customerBranchId), Convert.ToInt16(VerticalId), processId, financialYear, AuditId, subProcessId, "Oct-Mar"))
                                                {
                                                    if (processId != OldProcessIDHalfyearly2)
                                                    {
                                                        OldProcessIDHalfyearly2 = processId;
                                                        DateTime? dateAuditStartDate = null;
                                                        DateTime? dateAuditEndDate = null;

                                                        InternalAuditScheduling internalAuditScheduling = UserManagementRisk.GeDatestAuditScheduling_ScheduleFirst(Convert.ToInt16(customerBranchId), Convert.ToInt16(VerticalId), processId, financialYear, AuditId, subProcessId, "Oct-Mar");
                                                        if (internalAuditScheduling != null)
                                                        {
                                                            dateAuditStartDate = internalAuditScheduling.StartDate;
                                                            dateAuditEndDate = internalAuditScheduling.EndDate;
                                                        }
                                                        else
                                                        {
                                                            dateAuditStartDate = Convert.ToDateTime(AuditScheduleStartDate);
                                                            dateAuditEndDate = Convert.ToDateTime(AuditScheduleEndDate);
                                                        }

                                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(customerBranchId);
                                                        Internalauditscheduling.FinancialYear = financialYear;
                                                        Internalauditscheduling.TermName = period;
                                                        Internalauditscheduling.TermStatus = true;
                                                        Internalauditscheduling.Process = processId;
                                                        Internalauditscheduling.ISAHQMP = "H";
                                                        Internalauditscheduling.StartDate = dateAuditStartDate;
                                                        Internalauditscheduling.EndDate = dateAuditEndDate;
                                                        Internalauditscheduling.VerticalID = Convert.ToInt32(VerticalId);
                                                        Internalauditscheduling.AuditID = AuditId;
                                                        Internalauditscheduling.Createdby = Common.AuthenticationHelper.UserID;
                                                        Internalauditscheduling.CreatedOn = DateTime.Now;
                                                        if (processId != 0 && AuditId != 0)
                                                        {
                                                            if (!UserManagementRisk.IsExistsInInternalAuditSchedulings(Internalauditscheduling.Process, Internalauditscheduling.CustomerBranchId, Internalauditscheduling.FinancialYear, Internalauditscheduling.TermName, Convert.ToInt32(Internalauditscheduling.VerticalID), Convert.ToInt32(Internalauditscheduling.AuditID)))
                                                            {
                                                                InternalAuditScheduleOnIDHalfyearly2 = UserManagementRisk.AddDetailsInternalAuditSchedulingTable_ScheduleFirst(Internalauditscheduling);
                                                            }
                                                            else
                                                            {
                                                                InternalAuditScheduling InternalAuditSchedulingObject = UserManagementRisk.GetDetailsInternalAuditSchedulingsId(Internalauditscheduling.Process, Internalauditscheduling.CustomerBranchId, Internalauditscheduling.FinancialYear, Internalauditscheduling.TermName, Convert.ToInt32(Internalauditscheduling.VerticalID), Convert.ToInt32(Internalauditscheduling.AuditID));
                                                                if (InternalAuditSchedulingObject != null)
                                                                {
                                                                    InternalAuditScheduleOnIDHalfyearly2 = InternalAuditSchedulingObject.Id;
                                                                }
                                                            }
                                                        }
                                                        AuditScheduling_SubProcessMapping auditScheduling_SubProcessMapping = new AuditScheduling_SubProcessMapping();
                                                        auditScheduling_SubProcessMapping.Process = processId;
                                                        auditScheduling_SubProcessMapping.AuditID = AuditId;
                                                        auditScheduling_SubProcessMapping.SubProcessID = subProcessId;
                                                        auditScheduling_SubProcessMapping.CreatedBy = Common.AuthenticationHelper.UserID;
                                                        auditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                        auditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDHalfyearly2;
                                                        auditScheduling_SubProcessMapping.IsActive = true;
                                                        auditScheduling_SubProcessMapping.IsDeleted = false;
                                                        if (processId != 0 && AuditId != 0 && subProcessId != 0)
                                                        {
                                                            UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(auditScheduling_SubProcessMapping);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        AuditScheduling_SubProcessMapping auditScheduling_SubProcessMapping = new AuditScheduling_SubProcessMapping();
                                                        auditScheduling_SubProcessMapping.Process = processId;
                                                        auditScheduling_SubProcessMapping.AuditID = AuditId;
                                                        auditScheduling_SubProcessMapping.SubProcessID = subProcessId;
                                                        auditScheduling_SubProcessMapping.CreatedBy = Common.AuthenticationHelper.UserID;
                                                        auditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                        auditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDHalfyearly2;
                                                        auditScheduling_SubProcessMapping.IsActive = true;
                                                        auditScheduling_SubProcessMapping.IsDeleted = false;
                                                        if (processId != 0 && AuditId != 0 && subProcessId != 0 && InternalAuditScheduleOnIDHalfyearly2 != 0)
                                                        {
                                                            UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(auditScheduling_SubProcessMapping);
                                                        }
                                                    }
                                                }
                                                //else
                                                //{
                                                //    cvDuplicateEntry.IsValid = false;
                                                //    cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled";
                                                //    break;
                                                //}
                                            }
                                            #endregion

                                            #region Quarterly
                                            if (period == "Apr-Jun" || period == "APR-JUN" || period == "APRIL-JUNE")
                                            {
                                                if (UserManagementRisk.IsExistAuditScheduling_ScheduleFirst(Convert.ToInt16(customerBranchId), Convert.ToInt16(VerticalId), processId, financialYear, AuditId, subProcessId, "Apr-Jun"))
                                                {
                                                    if (processId != OldProcessIDAprJun)
                                                    {
                                                        OldProcessIDAprJun = processId;
                                                        DateTime? dateAuditStartDate = null;
                                                        DateTime? dateAuditEndDate = null;

                                                        InternalAuditScheduling internalAuditScheduling = UserManagementRisk.GeDatestAuditScheduling_ScheduleFirst(Convert.ToInt16(customerBranchId), Convert.ToInt16(VerticalId), processId, financialYear, AuditId, subProcessId, "Apr-Jun");
                                                        if (internalAuditScheduling != null)
                                                        {
                                                            dateAuditStartDate = internalAuditScheduling.StartDate;
                                                            dateAuditEndDate = internalAuditScheduling.EndDate;
                                                        }
                                                        else
                                                        {
                                                            dateAuditStartDate = Convert.ToDateTime(AuditScheduleStartDate);
                                                            dateAuditEndDate = Convert.ToDateTime(AuditScheduleEndDate);
                                                        }

                                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(customerBranchId);
                                                        Internalauditscheduling.FinancialYear = financialYear;
                                                        Internalauditscheduling.TermName = period;
                                                        Internalauditscheduling.TermStatus = true;
                                                        Internalauditscheduling.Process = processId;
                                                        Internalauditscheduling.ISAHQMP = "Q";
                                                        Internalauditscheduling.StartDate = dateAuditStartDate;
                                                        Internalauditscheduling.EndDate = dateAuditEndDate;
                                                        Internalauditscheduling.VerticalID = Convert.ToInt32(VerticalId);
                                                        Internalauditscheduling.AuditID = AuditId;
                                                        Internalauditscheduling.Createdby = Common.AuthenticationHelper.UserID;
                                                        Internalauditscheduling.CreatedOn = DateTime.Now;
                                                        if (processId != 0 && AuditId != 0)
                                                        {
                                                            if (!UserManagementRisk.IsExistsInInternalAuditSchedulings(Internalauditscheduling.Process, Internalauditscheduling.CustomerBranchId, Internalauditscheduling.FinancialYear, Internalauditscheduling.TermName, Convert.ToInt32(Internalauditscheduling.VerticalID), Convert.ToInt32(Internalauditscheduling.AuditID)))
                                                            {
                                                                InternalAuditScheduleOnIDAprJun = UserManagementRisk.AddDetailsInternalAuditSchedulingTable_ScheduleFirst(Internalauditscheduling);
                                                            }
                                                            else
                                                            {
                                                                InternalAuditScheduling InternalAuditSchedulingObject = UserManagementRisk.GetDetailsInternalAuditSchedulingsId(Internalauditscheduling.Process, Internalauditscheduling.CustomerBranchId, Internalauditscheduling.FinancialYear, Internalauditscheduling.TermName, Convert.ToInt32(Internalauditscheduling.VerticalID), Convert.ToInt32(Internalauditscheduling.AuditID));
                                                                if (InternalAuditSchedulingObject != null)
                                                                {
                                                                    InternalAuditScheduleOnIDAprJun = InternalAuditSchedulingObject.Id;
                                                                }
                                                            }
                                                        }
                                                        AuditScheduling_SubProcessMapping auditScheduling_SubProcessMapping = new AuditScheduling_SubProcessMapping();
                                                        auditScheduling_SubProcessMapping.Process = processId;
                                                        auditScheduling_SubProcessMapping.AuditID = AuditId;
                                                        auditScheduling_SubProcessMapping.SubProcessID = subProcessId;
                                                        auditScheduling_SubProcessMapping.CreatedBy = Common.AuthenticationHelper.UserID;
                                                        auditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                        auditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDAprJun;
                                                        auditScheduling_SubProcessMapping.IsActive = true;
                                                        auditScheduling_SubProcessMapping.IsDeleted = false;
                                                        if (processId != 0 && AuditId != 0 && subProcessId != 0)
                                                        {
                                                            UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(auditScheduling_SubProcessMapping);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        AuditScheduling_SubProcessMapping auditScheduling_SubProcessMapping = new AuditScheduling_SubProcessMapping();
                                                        auditScheduling_SubProcessMapping.Process = processId;
                                                        auditScheduling_SubProcessMapping.AuditID = AuditId;
                                                        auditScheduling_SubProcessMapping.SubProcessID = subProcessId;
                                                        auditScheduling_SubProcessMapping.CreatedBy = Common.AuthenticationHelper.UserID;
                                                        auditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                        auditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDAprJun;
                                                        auditScheduling_SubProcessMapping.IsActive = true;
                                                        auditScheduling_SubProcessMapping.IsDeleted = false;
                                                        if (processId != 0 && AuditId != 0 && subProcessId != 0)
                                                        {
                                                            UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(auditScheduling_SubProcessMapping);
                                                        }
                                                    }
                                                }
                                                //else
                                                //{
                                                //    cvDuplicateEntry.IsValid = false;
                                                //    cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled.";
                                                //    break;
                                                //}
                                            }
                                            if (period == "Jul-Sep" || period == "JUL-SEP" || period == "JULY-SEPTEMBER")
                                            {
                                                if (UserManagementRisk.IsExistAuditScheduling_ScheduleFirst(Convert.ToInt16(customerBranchId), Convert.ToInt16(VerticalId), processId, financialYear, AuditId, subProcessId, "Jul-Sep"))
                                                {
                                                    if (processId != OldProcessIDJulSep)
                                                    {
                                                        OldProcessIDJulSep = processId;
                                                        DateTime? dateAuditStartDate = null;
                                                        DateTime? dateAuditEndDate = null;

                                                        InternalAuditScheduling internalAuditScheduling = UserManagementRisk.GeDatestAuditScheduling_ScheduleFirst(Convert.ToInt16(customerBranchId), Convert.ToInt16(VerticalId), processId, financialYear, AuditId, subProcessId, "Jul-Sep");
                                                        if (internalAuditScheduling != null)
                                                        {
                                                            dateAuditStartDate = internalAuditScheduling.StartDate;
                                                            dateAuditEndDate = internalAuditScheduling.EndDate;
                                                        }
                                                        else
                                                        {
                                                            dateAuditStartDate = Convert.ToDateTime(AuditScheduleStartDate);
                                                            dateAuditEndDate = Convert.ToDateTime(AuditScheduleEndDate);
                                                        }

                                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(customerBranchId);
                                                        Internalauditscheduling.FinancialYear = financialYear;
                                                        Internalauditscheduling.TermName = period;
                                                        Internalauditscheduling.TermStatus = true;
                                                        Internalauditscheduling.Process = processId;
                                                        Internalauditscheduling.ISAHQMP = "Q";
                                                        Internalauditscheduling.StartDate = dateAuditStartDate;
                                                        Internalauditscheduling.EndDate = dateAuditEndDate;
                                                        Internalauditscheduling.VerticalID = Convert.ToInt32(VerticalId);
                                                        Internalauditscheduling.AuditID = AuditId;
                                                        Internalauditscheduling.Createdby = Common.AuthenticationHelper.UserID;
                                                        Internalauditscheduling.CreatedOn = DateTime.Now;
                                                        if (processId != 0 && AuditId != 0)
                                                        {
                                                            if (!UserManagementRisk.IsExistsInInternalAuditSchedulings(Internalauditscheduling.Process, Internalauditscheduling.CustomerBranchId, Internalauditscheduling.FinancialYear, Internalauditscheduling.TermName, Convert.ToInt32(Internalauditscheduling.VerticalID), Convert.ToInt32(Internalauditscheduling.AuditID)))
                                                            {
                                                                InternalAuditScheduleOnIDJulSep = UserManagementRisk.AddDetailsInternalAuditSchedulingTable_ScheduleFirst(Internalauditscheduling);
                                                            }
                                                            else
                                                            {
                                                                InternalAuditScheduling InternalAuditSchedulingObject = UserManagementRisk.GetDetailsInternalAuditSchedulingsId(Internalauditscheduling.Process, Internalauditscheduling.CustomerBranchId, Internalauditscheduling.FinancialYear, Internalauditscheduling.TermName, Convert.ToInt32(Internalauditscheduling.VerticalID), Convert.ToInt32(Internalauditscheduling.AuditID));
                                                                if (InternalAuditSchedulingObject != null)
                                                                {
                                                                    InternalAuditScheduleOnIDJulSep = InternalAuditSchedulingObject.Id;
                                                                }
                                                            }
                                                        }
                                                        AuditScheduling_SubProcessMapping auditScheduling_SubProcessMapping = new AuditScheduling_SubProcessMapping();
                                                        auditScheduling_SubProcessMapping.Process = processId;
                                                        auditScheduling_SubProcessMapping.AuditID = AuditId;
                                                        auditScheduling_SubProcessMapping.SubProcessID = subProcessId;
                                                        auditScheduling_SubProcessMapping.CreatedBy = Common.AuthenticationHelper.UserID;
                                                        auditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                        auditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDJulSep;
                                                        auditScheduling_SubProcessMapping.IsActive = true;
                                                        auditScheduling_SubProcessMapping.IsDeleted = false;
                                                        if (processId != 0 && AuditId != 0 && subProcessId != 0)
                                                        {
                                                            UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(auditScheduling_SubProcessMapping);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        AuditScheduling_SubProcessMapping auditScheduling_SubProcessMapping = new AuditScheduling_SubProcessMapping();
                                                        auditScheduling_SubProcessMapping.Process = processId;
                                                        auditScheduling_SubProcessMapping.AuditID = AuditId;
                                                        auditScheduling_SubProcessMapping.SubProcessID = subProcessId;
                                                        auditScheduling_SubProcessMapping.CreatedBy = Common.AuthenticationHelper.UserID;
                                                        auditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                        auditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDJulSep;
                                                        auditScheduling_SubProcessMapping.IsActive = true;
                                                        auditScheduling_SubProcessMapping.IsDeleted = false;
                                                        if (processId != 0 && AuditId != 0 && subProcessId != 0)
                                                        {
                                                            UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(auditScheduling_SubProcessMapping);
                                                        }
                                                    }
                                                }
                                                //else
                                                //{
                                                //    cvDuplicateEntry.IsValid = false;
                                                //    cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled.";
                                                //    break;
                                                //}
                                            }
                                            if (period == "Oct-Dec" || period == "OCT-DEC" || period == "OCTOBER-DECEMBER")
                                            {
                                                if (UserManagementRisk.IsExistAuditScheduling_ScheduleFirst(Convert.ToInt16(customerBranchId), Convert.ToInt16(VerticalId), processId, financialYear, AuditId, subProcessId, "Oct-Dec"))
                                                {
                                                    if (processId != OldProcessIDOctDec)
                                                    {
                                                        OldProcessIDOctDec = processId;
                                                        DateTime? dateAuditStartDate = null;
                                                        DateTime? dateAuditEndDate = null;

                                                        InternalAuditScheduling internalAuditScheduling = UserManagementRisk.GeDatestAuditScheduling_ScheduleFirst(Convert.ToInt16(customerBranchId), Convert.ToInt16(VerticalId), processId, financialYear, AuditId, subProcessId, "Oct-Dec");
                                                        if (internalAuditScheduling != null)
                                                        {
                                                            dateAuditStartDate = internalAuditScheduling.StartDate;
                                                            dateAuditEndDate = internalAuditScheduling.EndDate;
                                                        }
                                                        else
                                                        {
                                                            dateAuditStartDate = Convert.ToDateTime(AuditScheduleStartDate);
                                                            dateAuditEndDate = Convert.ToDateTime(AuditScheduleEndDate);
                                                        }

                                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(customerBranchId);
                                                        Internalauditscheduling.FinancialYear = financialYear;
                                                        Internalauditscheduling.TermName = period;
                                                        Internalauditscheduling.TermStatus = true;
                                                        Internalauditscheduling.Process = processId;
                                                        Internalauditscheduling.ISAHQMP = "Q";
                                                        Internalauditscheduling.StartDate = dateAuditStartDate;
                                                        Internalauditscheduling.EndDate = dateAuditEndDate;
                                                        Internalauditscheduling.VerticalID = Convert.ToInt32(VerticalId);
                                                        Internalauditscheduling.AuditID = AuditId;
                                                        Internalauditscheduling.Createdby = Common.AuthenticationHelper.UserID;
                                                        Internalauditscheduling.CreatedOn = DateTime.Now;
                                                        if (processId != 0 && AuditId != 0)
                                                        {
                                                            if (!UserManagementRisk.IsExistsInInternalAuditSchedulings(Internalauditscheduling.Process, Internalauditscheduling.CustomerBranchId, Internalauditscheduling.FinancialYear, Internalauditscheduling.TermName, Convert.ToInt32(Internalauditscheduling.VerticalID), Convert.ToInt32(Internalauditscheduling.AuditID)))
                                                            {
                                                                InternalAuditScheduleOnIDOctDec = UserManagementRisk.AddDetailsInternalAuditSchedulingTable_ScheduleFirst(Internalauditscheduling);
                                                            }
                                                            else
                                                            {
                                                                InternalAuditScheduling InternalAuditSchedulingObject = UserManagementRisk.GetDetailsInternalAuditSchedulingsId(Internalauditscheduling.Process, Internalauditscheduling.CustomerBranchId, Internalauditscheduling.FinancialYear, Internalauditscheduling.TermName, Convert.ToInt32(Internalauditscheduling.VerticalID), Convert.ToInt32(Internalauditscheduling.AuditID));
                                                                if (InternalAuditSchedulingObject != null)
                                                                {
                                                                    InternalAuditScheduleOnIDOctDec = InternalAuditSchedulingObject.Id;
                                                                }
                                                            }
                                                        }
                                                        AuditScheduling_SubProcessMapping auditScheduling_SubProcessMapping = new AuditScheduling_SubProcessMapping();
                                                        auditScheduling_SubProcessMapping.Process = processId;
                                                        auditScheduling_SubProcessMapping.AuditID = AuditId;
                                                        auditScheduling_SubProcessMapping.SubProcessID = subProcessId;
                                                        auditScheduling_SubProcessMapping.CreatedBy = Common.AuthenticationHelper.UserID;
                                                        auditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                        auditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDOctDec;
                                                        auditScheduling_SubProcessMapping.IsActive = true;
                                                        auditScheduling_SubProcessMapping.IsDeleted = false;
                                                        if (processId != 0 && AuditId != 0 && subProcessId != 0)
                                                        {
                                                            UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(auditScheduling_SubProcessMapping);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        AuditScheduling_SubProcessMapping auditScheduling_SubProcessMapping = new AuditScheduling_SubProcessMapping();
                                                        auditScheduling_SubProcessMapping.Process = processId;
                                                        auditScheduling_SubProcessMapping.AuditID = AuditId;
                                                        auditScheduling_SubProcessMapping.SubProcessID = subProcessId;
                                                        auditScheduling_SubProcessMapping.CreatedBy = Common.AuthenticationHelper.UserID;
                                                        auditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                        auditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDOctDec;
                                                        auditScheduling_SubProcessMapping.IsActive = true;
                                                        auditScheduling_SubProcessMapping.IsDeleted = false;
                                                        if (processId != 0 && AuditId != 0 && subProcessId != 0)
                                                        {
                                                            UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(auditScheduling_SubProcessMapping);
                                                        }
                                                    }
                                                }
                                                //else
                                                //{
                                                //    cvDuplicateEntry.IsValid = false;
                                                //    cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled.";
                                                //    break;
                                                //}
                                            }
                                            if (period == "Jan-Mar" || period == "JAN-MAR" || period == "JANUARY-MARCH")
                                            {
                                                if (UserManagementRisk.IsExistAuditScheduling_ScheduleFirst(Convert.ToInt16(customerBranchId), Convert.ToInt16(VerticalId), processId, financialYear, AuditId, subProcessId, "Jan-Mar"))
                                                {
                                                    if (processId != OldProcessIDJanMar)
                                                    {
                                                        OldProcessIDJanMar = processId;
                                                        DateTime? dateAuditStartDate = null;
                                                        DateTime? dateAuditEndDate = null;
                                                        InternalAuditScheduling internalAuditScheduling = UserManagementRisk.GeDatestAuditScheduling_ScheduleFirst(Convert.ToInt16(customerBranchId), Convert.ToInt16(VerticalId), processId, financialYear, AuditId, subProcessId, "Jan-Mar");
                                                        if (internalAuditScheduling != null)
                                                        {
                                                            dateAuditStartDate = internalAuditScheduling.StartDate;
                                                            dateAuditEndDate = internalAuditScheduling.EndDate;
                                                        }
                                                        else
                                                        {
                                                            dateAuditStartDate = Convert.ToDateTime(AuditScheduleStartDate);
                                                            dateAuditEndDate = Convert.ToDateTime(AuditScheduleEndDate);
                                                        }
                                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(customerBranchId);
                                                        Internalauditscheduling.FinancialYear = financialYear;
                                                        Internalauditscheduling.TermName = period;
                                                        Internalauditscheduling.TermStatus = true;
                                                        Internalauditscheduling.Process = processId;
                                                        Internalauditscheduling.ISAHQMP = "Q";
                                                        Internalauditscheduling.StartDate = dateAuditStartDate;
                                                        Internalauditscheduling.EndDate = dateAuditEndDate;
                                                        Internalauditscheduling.VerticalID = Convert.ToInt32(VerticalId);
                                                        Internalauditscheduling.AuditID = AuditId;
                                                        Internalauditscheduling.Createdby = Common.AuthenticationHelper.UserID;
                                                        Internalauditscheduling.CreatedOn = DateTime.Now;
                                                        if (processId != 0 && AuditId != 0)
                                                        {
                                                            if (!UserManagementRisk.IsExistsInInternalAuditSchedulings(Internalauditscheduling.Process, Internalauditscheduling.CustomerBranchId, Internalauditscheduling.FinancialYear, Internalauditscheduling.TermName, Convert.ToInt32(Internalauditscheduling.VerticalID), Convert.ToInt32(Internalauditscheduling.AuditID)))
                                                            {
                                                                InternalAuditScheduleOnIDJanMar = UserManagementRisk.AddDetailsInternalAuditSchedulingTable_ScheduleFirst(Internalauditscheduling);
                                                            }
                                                            else
                                                            {
                                                                InternalAuditScheduling InternalAuditSchedulingObject = UserManagementRisk.GetDetailsInternalAuditSchedulingsId(Internalauditscheduling.Process, Internalauditscheduling.CustomerBranchId, Internalauditscheduling.FinancialYear, Internalauditscheduling.TermName, Convert.ToInt32(Internalauditscheduling.VerticalID), Convert.ToInt32(Internalauditscheduling.AuditID));
                                                                if (InternalAuditSchedulingObject != null)
                                                                {
                                                                    InternalAuditScheduleOnIDJanMar = InternalAuditSchedulingObject.Id;
                                                                }
                                                            }
                                                        }
                                                        AuditScheduling_SubProcessMapping auditScheduling_SubProcessMapping = new AuditScheduling_SubProcessMapping();
                                                        auditScheduling_SubProcessMapping.Process = processId;
                                                        auditScheduling_SubProcessMapping.AuditID = AuditId;
                                                        auditScheduling_SubProcessMapping.SubProcessID = subProcessId;
                                                        auditScheduling_SubProcessMapping.CreatedBy = Common.AuthenticationHelper.UserID;
                                                        auditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                        auditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDJanMar;
                                                        auditScheduling_SubProcessMapping.IsActive = true;
                                                        auditScheduling_SubProcessMapping.IsDeleted = false;
                                                        if (processId != 0 && AuditId != 0 && subProcessId != 0)
                                                        {
                                                            UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(auditScheduling_SubProcessMapping);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        AuditScheduling_SubProcessMapping auditScheduling_SubProcessMapping = new AuditScheduling_SubProcessMapping();
                                                        auditScheduling_SubProcessMapping.Process = processId;
                                                        auditScheduling_SubProcessMapping.AuditID = AuditId;
                                                        auditScheduling_SubProcessMapping.SubProcessID = subProcessId;
                                                        auditScheduling_SubProcessMapping.CreatedBy = Common.AuthenticationHelper.UserID;
                                                        auditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                        auditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDJanMar;
                                                        auditScheduling_SubProcessMapping.IsActive = true;
                                                        auditScheduling_SubProcessMapping.IsDeleted = false;
                                                        if (processId != 0 && AuditId != 0 && subProcessId != 0)
                                                        {
                                                            UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(auditScheduling_SubProcessMapping);
                                                        }
                                                    }
                                                }
                                                //else
                                                //{
                                                //    cvDuplicateEntry.IsValid = false;
                                                //    cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled.";
                                                //    break;
                                                //}
                                            }
                                            #endregion

                                            #region Monthly

                                            #region Apr
                                            if (period == "Apr" || period == "APR" || period == "APRIL")
                                            {
                                                if (UserManagementRisk.IsExistAuditScheduling_ScheduleFirst(Convert.ToInt16(customerBranchId), Convert.ToInt16(VerticalId), processId, financialYear, AuditId, subProcessId, "Apr"))
                                                {
                                                    if (processId != OldProcessIDApr)
                                                    {
                                                        OldProcessIDApr = processId;
                                                        DateTime? dateAuditStartDate = null;
                                                        DateTime? dateAuditEndDate = null;

                                                        InternalAuditScheduling internalAuditScheduling = UserManagementRisk.GeDatestAuditScheduling_ScheduleFirst(Convert.ToInt16(customerBranchId), Convert.ToInt16(VerticalId), processId, financialYear, AuditId, subProcessId, "Apr");
                                                        if (internalAuditScheduling != null)
                                                        {
                                                            dateAuditStartDate = internalAuditScheduling.StartDate;
                                                            dateAuditEndDate = internalAuditScheduling.EndDate;
                                                        }
                                                        else
                                                        {
                                                            dateAuditStartDate = Convert.ToDateTime(AuditScheduleStartDate);
                                                            dateAuditEndDate = Convert.ToDateTime(AuditScheduleEndDate);
                                                        }

                                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(customerBranchId);
                                                        Internalauditscheduling.FinancialYear = financialYear;
                                                        Internalauditscheduling.TermName = period;
                                                        Internalauditscheduling.TermStatus = true;
                                                        Internalauditscheduling.Process = processId;
                                                        Internalauditscheduling.ISAHQMP = "M";
                                                        Internalauditscheduling.StartDate = dateAuditStartDate;
                                                        Internalauditscheduling.EndDate = dateAuditEndDate;
                                                        Internalauditscheduling.VerticalID = Convert.ToInt32(VerticalId);
                                                        Internalauditscheduling.AuditID = AuditId;
                                                        Internalauditscheduling.Createdby = Common.AuthenticationHelper.UserID;
                                                        Internalauditscheduling.CreatedOn = DateTime.Now;
                                                        if (processId != 0 && AuditId != 0)
                                                        {
                                                            if (!UserManagementRisk.IsExistsInInternalAuditSchedulings(Internalauditscheduling.Process, Internalauditscheduling.CustomerBranchId, Internalauditscheduling.FinancialYear, Internalauditscheduling.TermName, Convert.ToInt32(Internalauditscheduling.VerticalID), Convert.ToInt32(Internalauditscheduling.AuditID)))
                                                            {
                                                                InternalAuditScheduleOnIDApr = UserManagementRisk.AddDetailsInternalAuditSchedulingTable_ScheduleFirst(Internalauditscheduling);
                                                            }
                                                            else
                                                            {
                                                                InternalAuditScheduling InternalAuditSchedulingObject = UserManagementRisk.GetDetailsInternalAuditSchedulingsId(Internalauditscheduling.Process, Internalauditscheduling.CustomerBranchId, Internalauditscheduling.FinancialYear, Internalauditscheduling.TermName, Convert.ToInt32(Internalauditscheduling.VerticalID), Convert.ToInt32(Internalauditscheduling.AuditID));
                                                                if (InternalAuditSchedulingObject != null)
                                                                {
                                                                    InternalAuditScheduleOnIDApr = InternalAuditSchedulingObject.Id;
                                                                }
                                                            }
                                                        }
                                                        AuditScheduling_SubProcessMapping auditScheduling_SubProcessMapping = new AuditScheduling_SubProcessMapping();
                                                        auditScheduling_SubProcessMapping.Process = processId;
                                                        auditScheduling_SubProcessMapping.AuditID = AuditId;
                                                        auditScheduling_SubProcessMapping.SubProcessID = subProcessId;
                                                        auditScheduling_SubProcessMapping.CreatedBy = Common.AuthenticationHelper.UserID;
                                                        auditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                        auditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDApr;
                                                        auditScheduling_SubProcessMapping.IsActive = true;
                                                        auditScheduling_SubProcessMapping.IsDeleted = false;
                                                        if (processId != 0 && AuditId != 0 && subProcessId != 0)
                                                        {
                                                            UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(auditScheduling_SubProcessMapping);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        AuditScheduling_SubProcessMapping auditScheduling_SubProcessMapping = new AuditScheduling_SubProcessMapping();
                                                        auditScheduling_SubProcessMapping.Process = processId;
                                                        auditScheduling_SubProcessMapping.AuditID = AuditId;
                                                        auditScheduling_SubProcessMapping.SubProcessID = subProcessId;
                                                        auditScheduling_SubProcessMapping.CreatedBy = Common.AuthenticationHelper.UserID;
                                                        auditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                        auditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDApr;
                                                        auditScheduling_SubProcessMapping.IsActive = true;
                                                        auditScheduling_SubProcessMapping.IsDeleted = false;
                                                        if (processId != 0 && AuditId != 0 && subProcessId != 0)
                                                        {
                                                            UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(auditScheduling_SubProcessMapping);
                                                        }
                                                    }
                                                }
                                                //else
                                                //{
                                                //    cvDuplicateEntry.IsValid = false;
                                                //    cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled.";
                                                //    break;
                                                //}
                                            }
                                            #endregion

                                            #region May
                                            if (period == "May" || period == "MAY")
                                            {
                                                if (UserManagementRisk.IsExistAuditScheduling_ScheduleFirst(Convert.ToInt16(customerBranchId), Convert.ToInt16(VerticalId), processId, financialYear, AuditId, subProcessId, "May"))
                                                {
                                                    if (processId != OldProcessIDMay)
                                                    {
                                                        OldProcessIDMay = processId;
                                                        DateTime? dateAuditStartDate = null;
                                                        DateTime? dateAuditEndDate = null;

                                                        InternalAuditScheduling internalAuditScheduling = UserManagementRisk.GeDatestAuditScheduling_ScheduleFirst(Convert.ToInt16(customerBranchId), Convert.ToInt16(VerticalId), processId, financialYear, AuditId, subProcessId, "May");
                                                        if (internalAuditScheduling != null)
                                                        {
                                                            dateAuditStartDate = internalAuditScheduling.StartDate;
                                                            dateAuditEndDate = internalAuditScheduling.EndDate;
                                                        }
                                                        else
                                                        {
                                                            dateAuditStartDate = Convert.ToDateTime(AuditScheduleStartDate);
                                                            dateAuditEndDate = Convert.ToDateTime(AuditScheduleEndDate);
                                                        }

                                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(customerBranchId);
                                                        Internalauditscheduling.FinancialYear = financialYear;
                                                        Internalauditscheduling.TermName = period;
                                                        Internalauditscheduling.TermStatus = true;
                                                        Internalauditscheduling.Process = processId;
                                                        Internalauditscheduling.ISAHQMP = "M";
                                                        Internalauditscheduling.StartDate = dateAuditStartDate;
                                                        Internalauditscheduling.EndDate = dateAuditEndDate;
                                                        Internalauditscheduling.VerticalID = Convert.ToInt32(VerticalId);
                                                        Internalauditscheduling.AuditID = AuditId;
                                                        Internalauditscheduling.Createdby = Common.AuthenticationHelper.UserID;
                                                        Internalauditscheduling.CreatedOn = DateTime.Now;
                                                        if (processId != 0 && AuditId != 0)
                                                        {
                                                            if (!UserManagementRisk.IsExistsInInternalAuditSchedulings(Internalauditscheduling.Process, Internalauditscheduling.CustomerBranchId, Internalauditscheduling.FinancialYear, Internalauditscheduling.TermName, Convert.ToInt32(Internalauditscheduling.VerticalID), Convert.ToInt32(Internalauditscheduling.AuditID)))
                                                            {
                                                                InternalAuditScheduleOnIDMay = UserManagementRisk.AddDetailsInternalAuditSchedulingTable_ScheduleFirst(Internalauditscheduling);
                                                            }
                                                            else
                                                            {
                                                                InternalAuditScheduling InternalAuditSchedulingObject = UserManagementRisk.GetDetailsInternalAuditSchedulingsId(Internalauditscheduling.Process, Internalauditscheduling.CustomerBranchId, Internalauditscheduling.FinancialYear, Internalauditscheduling.TermName, Convert.ToInt32(Internalauditscheduling.VerticalID), Convert.ToInt32(Internalauditscheduling.AuditID));
                                                                if (InternalAuditSchedulingObject != null)
                                                                {
                                                                    InternalAuditScheduleOnIDMay = InternalAuditSchedulingObject.Id;
                                                                }
                                                            }
                                                        }
                                                        AuditScheduling_SubProcessMapping auditScheduling_SubProcessMapping = new AuditScheduling_SubProcessMapping();
                                                        auditScheduling_SubProcessMapping.Process = processId;
                                                        auditScheduling_SubProcessMapping.AuditID = AuditId;
                                                        auditScheduling_SubProcessMapping.SubProcessID = subProcessId;
                                                        auditScheduling_SubProcessMapping.CreatedBy = Common.AuthenticationHelper.UserID;
                                                        auditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                        auditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDMay;
                                                        auditScheduling_SubProcessMapping.IsActive = true;
                                                        auditScheduling_SubProcessMapping.IsDeleted = false;
                                                        if (processId != 0 && AuditId != 0 && subProcessId != 0)
                                                        {
                                                            UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(auditScheduling_SubProcessMapping);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        AuditScheduling_SubProcessMapping auditScheduling_SubProcessMapping = new AuditScheduling_SubProcessMapping();
                                                        auditScheduling_SubProcessMapping.Process = processId;
                                                        auditScheduling_SubProcessMapping.AuditID = AuditId;
                                                        auditScheduling_SubProcessMapping.SubProcessID = subProcessId;
                                                        auditScheduling_SubProcessMapping.CreatedBy = Common.AuthenticationHelper.UserID;
                                                        auditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                        auditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDMay;
                                                        auditScheduling_SubProcessMapping.IsActive = true;
                                                        auditScheduling_SubProcessMapping.IsDeleted = false;
                                                        if (processId != 0 && AuditId != 0 && subProcessId != 0)
                                                        {
                                                            UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(auditScheduling_SubProcessMapping);
                                                        }
                                                    }
                                                }
                                                //else
                                                //{
                                                //    cvDuplicateEntry.IsValid = false;
                                                //    cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled.";
                                                //    break;
                                                //}
                                            }
                                            #endregion

                                            #region June
                                            if (period == "Jun" || period == "JUN" || period == "JUNE")
                                            {
                                                if (UserManagementRisk.IsExistAuditScheduling_ScheduleFirst(Convert.ToInt16(customerBranchId), Convert.ToInt16(VerticalId), processId, financialYear, AuditId, subProcessId, "Jun"))
                                                {
                                                    if (processId != OldProcessIDJun)
                                                    {
                                                        OldProcessIDJun = processId;
                                                        DateTime? dateAuditStartDate = null;
                                                        DateTime? dateAuditEndDate = null;

                                                        InternalAuditScheduling internalAuditScheduling = UserManagementRisk.GeDatestAuditScheduling_ScheduleFirst(Convert.ToInt16(customerBranchId), Convert.ToInt16(VerticalId), processId, financialYear, AuditId, subProcessId, "Jun");
                                                        if (internalAuditScheduling != null)
                                                        {
                                                            dateAuditStartDate = internalAuditScheduling.StartDate;
                                                            dateAuditEndDate = internalAuditScheduling.EndDate;
                                                        }
                                                        else
                                                        {
                                                            dateAuditStartDate = Convert.ToDateTime(AuditScheduleStartDate);
                                                            dateAuditEndDate = Convert.ToDateTime(AuditScheduleEndDate);
                                                        }

                                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(customerBranchId);
                                                        Internalauditscheduling.FinancialYear = financialYear;
                                                        Internalauditscheduling.TermName = period;
                                                        Internalauditscheduling.TermStatus = true;
                                                        Internalauditscheduling.Process = processId;
                                                        Internalauditscheduling.ISAHQMP = "M";
                                                        Internalauditscheduling.StartDate = dateAuditStartDate;
                                                        Internalauditscheduling.EndDate = dateAuditEndDate;
                                                        Internalauditscheduling.VerticalID = Convert.ToInt32(VerticalId);
                                                        Internalauditscheduling.AuditID = AuditId;
                                                        Internalauditscheduling.Createdby = Common.AuthenticationHelper.UserID;
                                                        Internalauditscheduling.CreatedOn = DateTime.Now;
                                                        if (processId != 0 && AuditId != 0)
                                                        {
                                                            if (!UserManagementRisk.IsExistsInInternalAuditSchedulings(Internalauditscheduling.Process, Internalauditscheduling.CustomerBranchId, Internalauditscheduling.FinancialYear, Internalauditscheduling.TermName, Convert.ToInt32(Internalauditscheduling.VerticalID), Convert.ToInt32(Internalauditscheduling.AuditID)))
                                                            {
                                                                InternalAuditScheduleOnIDJun = UserManagementRisk.AddDetailsInternalAuditSchedulingTable_ScheduleFirst(Internalauditscheduling);
                                                            }
                                                            else
                                                            {
                                                                InternalAuditScheduling InternalAuditSchedulingObject = UserManagementRisk.GetDetailsInternalAuditSchedulingsId(Internalauditscheduling.Process, Internalauditscheduling.CustomerBranchId, Internalauditscheduling.FinancialYear, Internalauditscheduling.TermName, Convert.ToInt32(Internalauditscheduling.VerticalID), Convert.ToInt32(Internalauditscheduling.AuditID));
                                                                if (InternalAuditSchedulingObject != null)
                                                                {
                                                                    InternalAuditScheduleOnIDJun = InternalAuditSchedulingObject.Id;
                                                                }
                                                            }
                                                        }
                                                        AuditScheduling_SubProcessMapping auditScheduling_SubProcessMapping = new AuditScheduling_SubProcessMapping();
                                                        auditScheduling_SubProcessMapping.Process = processId;
                                                        auditScheduling_SubProcessMapping.AuditID = AuditId;
                                                        auditScheduling_SubProcessMapping.SubProcessID = subProcessId;
                                                        auditScheduling_SubProcessMapping.CreatedBy = Common.AuthenticationHelper.UserID;
                                                        auditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                        auditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDJun;
                                                        auditScheduling_SubProcessMapping.IsActive = true;
                                                        auditScheduling_SubProcessMapping.IsDeleted = false;
                                                        if (processId != 0 && AuditId != 0 && subProcessId != 0)
                                                        {
                                                            UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(auditScheduling_SubProcessMapping);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        AuditScheduling_SubProcessMapping auditScheduling_SubProcessMapping = new AuditScheduling_SubProcessMapping();
                                                        auditScheduling_SubProcessMapping.Process = processId;
                                                        auditScheduling_SubProcessMapping.AuditID = AuditId;
                                                        auditScheduling_SubProcessMapping.SubProcessID = subProcessId;
                                                        auditScheduling_SubProcessMapping.CreatedBy = Common.AuthenticationHelper.UserID;
                                                        auditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                        auditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDJun;
                                                        auditScheduling_SubProcessMapping.IsActive = true;
                                                        auditScheduling_SubProcessMapping.IsDeleted = false;
                                                        if (processId != 0 && AuditId != 0 && subProcessId != 0)
                                                        {
                                                            UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(auditScheduling_SubProcessMapping);
                                                        }
                                                    }
                                                }
                                                //else
                                                //{
                                                //    cvDuplicateEntry.IsValid = false;
                                                //    cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled.";
                                                //    break;
                                                //}
                                            }
                                            #endregion

                                            #region July
                                            if (period == "Jul" || period == "JUL" || period == "JULY")
                                            {
                                                if (UserManagementRisk.IsExistAuditScheduling_ScheduleFirst(Convert.ToInt16(customerBranchId), Convert.ToInt16(VerticalId), processId, financialYear, AuditId, subProcessId, "Jul"))
                                                {
                                                    if (processId != OldProcessIDJul)
                                                    {
                                                        OldProcessIDJul = processId;
                                                        DateTime? dateAuditStartDate = null;
                                                        DateTime? dateAuditEndDate = null;

                                                        InternalAuditScheduling internalAuditScheduling = UserManagementRisk.GeDatestAuditScheduling_ScheduleFirst(Convert.ToInt16(customerBranchId), Convert.ToInt16(VerticalId), processId, financialYear, AuditId, subProcessId, "Jul");
                                                        if (internalAuditScheduling != null)
                                                        {
                                                            dateAuditStartDate = internalAuditScheduling.StartDate;
                                                            dateAuditEndDate = internalAuditScheduling.EndDate;
                                                        }
                                                        else
                                                        {
                                                            dateAuditStartDate = Convert.ToDateTime(AuditScheduleStartDate);
                                                            dateAuditEndDate = Convert.ToDateTime(AuditScheduleEndDate);
                                                        }

                                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(customerBranchId);
                                                        Internalauditscheduling.FinancialYear = financialYear;
                                                        Internalauditscheduling.TermName = period;
                                                        Internalauditscheduling.TermStatus = true;
                                                        Internalauditscheduling.Process = processId;
                                                        Internalauditscheduling.ISAHQMP = "M";
                                                        Internalauditscheduling.StartDate = dateAuditStartDate;
                                                        Internalauditscheduling.EndDate = dateAuditEndDate;
                                                        Internalauditscheduling.VerticalID = Convert.ToInt32(VerticalId);
                                                        Internalauditscheduling.AuditID = AuditId;
                                                        Internalauditscheduling.Createdby = Common.AuthenticationHelper.UserID;
                                                        Internalauditscheduling.CreatedOn = DateTime.Now;
                                                        if (processId != 0 && AuditId != 0)
                                                        {
                                                            if (!UserManagementRisk.IsExistsInInternalAuditSchedulings(Internalauditscheduling.Process, Internalauditscheduling.CustomerBranchId, Internalauditscheduling.FinancialYear, Internalauditscheduling.TermName, Convert.ToInt32(Internalauditscheduling.VerticalID), Convert.ToInt32(Internalauditscheduling.AuditID)))
                                                            {
                                                                InternalAuditScheduleOnIDJul = UserManagementRisk.AddDetailsInternalAuditSchedulingTable_ScheduleFirst(Internalauditscheduling);
                                                            }
                                                            else
                                                            {
                                                                InternalAuditScheduling InternalAuditSchedulingObject = UserManagementRisk.GetDetailsInternalAuditSchedulingsId(Internalauditscheduling.Process, Internalauditscheduling.CustomerBranchId, Internalauditscheduling.FinancialYear, Internalauditscheduling.TermName, Convert.ToInt32(Internalauditscheduling.VerticalID), Convert.ToInt32(Internalauditscheduling.AuditID));
                                                                if (InternalAuditSchedulingObject != null)
                                                                {
                                                                    InternalAuditScheduleOnIDJul = InternalAuditSchedulingObject.Id;
                                                                }
                                                            }
                                                        }
                                                        AuditScheduling_SubProcessMapping auditScheduling_SubProcessMapping = new AuditScheduling_SubProcessMapping();
                                                        auditScheduling_SubProcessMapping.Process = processId;
                                                        auditScheduling_SubProcessMapping.AuditID = AuditId;
                                                        auditScheduling_SubProcessMapping.SubProcessID = subProcessId;
                                                        auditScheduling_SubProcessMapping.CreatedBy = Common.AuthenticationHelper.UserID;
                                                        auditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                        auditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDJul;
                                                        auditScheduling_SubProcessMapping.IsActive = true;
                                                        auditScheduling_SubProcessMapping.IsDeleted = false;
                                                        if (processId != 0 && AuditId != 0 && subProcessId != 0)
                                                        {
                                                            UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(auditScheduling_SubProcessMapping);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        AuditScheduling_SubProcessMapping auditScheduling_SubProcessMapping = new AuditScheduling_SubProcessMapping();
                                                        auditScheduling_SubProcessMapping.Process = processId;
                                                        auditScheduling_SubProcessMapping.AuditID = AuditId;
                                                        auditScheduling_SubProcessMapping.SubProcessID = subProcessId;
                                                        auditScheduling_SubProcessMapping.CreatedBy = Common.AuthenticationHelper.UserID;
                                                        auditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                        auditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDJul;
                                                        auditScheduling_SubProcessMapping.IsActive = true;
                                                        auditScheduling_SubProcessMapping.IsDeleted = false;
                                                        if (processId != 0 && AuditId != 0 && subProcessId != 0)
                                                        {
                                                            UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(auditScheduling_SubProcessMapping);
                                                        }
                                                    }
                                                }
                                                //else
                                                //{
                                                //    cvDuplicateEntry.IsValid = false;
                                                //    cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled.";
                                                //    break;
                                                //}
                                            }
                                            #endregion

                                            #region Aug
                                            if (period == "Aug" || period == "AUG" || period == "AUGUST")
                                            {
                                                if (UserManagementRisk.IsExistAuditScheduling_ScheduleFirst(Convert.ToInt16(customerBranchId), Convert.ToInt16(VerticalId), processId, financialYear, AuditId, subProcessId, "Aug"))
                                                {
                                                    if (processId != OldProcessIDAug)
                                                    {
                                                        OldProcessIDAug = processId;
                                                        DateTime? dateAuditStartDate = null;
                                                        DateTime? dateAuditEndDate = null;

                                                        InternalAuditScheduling internalAuditScheduling = UserManagementRisk.GeDatestAuditScheduling_ScheduleFirst(Convert.ToInt16(customerBranchId), Convert.ToInt16(VerticalId), processId, financialYear, AuditId, subProcessId, "Aug");
                                                        if (internalAuditScheduling != null)
                                                        {
                                                            dateAuditStartDate = internalAuditScheduling.StartDate;
                                                            dateAuditEndDate = internalAuditScheduling.EndDate;
                                                        }
                                                        else
                                                        {
                                                            dateAuditStartDate = Convert.ToDateTime(AuditScheduleStartDate);
                                                            dateAuditEndDate = Convert.ToDateTime(AuditScheduleEndDate);
                                                        }

                                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(customerBranchId);
                                                        Internalauditscheduling.FinancialYear = financialYear;
                                                        Internalauditscheduling.TermName = period;
                                                        Internalauditscheduling.TermStatus = true;
                                                        Internalauditscheduling.Process = processId;
                                                        Internalauditscheduling.ISAHQMP = "M";
                                                        Internalauditscheduling.StartDate = dateAuditStartDate;
                                                        Internalauditscheduling.EndDate = dateAuditEndDate;
                                                        Internalauditscheduling.VerticalID = Convert.ToInt32(VerticalId);
                                                        Internalauditscheduling.AuditID = AuditId;
                                                        Internalauditscheduling.Createdby = Common.AuthenticationHelper.UserID;
                                                        Internalauditscheduling.CreatedOn = DateTime.Now;
                                                        if (processId != 0 && AuditId != 0)
                                                        {
                                                            if (!UserManagementRisk.IsExistsInInternalAuditSchedulings(Internalauditscheduling.Process, Internalauditscheduling.CustomerBranchId, Internalauditscheduling.FinancialYear, Internalauditscheduling.TermName, Convert.ToInt32(Internalauditscheduling.VerticalID), Convert.ToInt32(Internalauditscheduling.AuditID)))
                                                            {
                                                                InternalAuditScheduleOnIDAug = UserManagementRisk.AddDetailsInternalAuditSchedulingTable_ScheduleFirst(Internalauditscheduling);
                                                            }
                                                            else
                                                            {
                                                                InternalAuditScheduling InternalAuditSchedulingObject = UserManagementRisk.GetDetailsInternalAuditSchedulingsId(Internalauditscheduling.Process, Internalauditscheduling.CustomerBranchId, Internalauditscheduling.FinancialYear, Internalauditscheduling.TermName, Convert.ToInt32(Internalauditscheduling.VerticalID), Convert.ToInt32(Internalauditscheduling.AuditID));
                                                                if (InternalAuditSchedulingObject != null)
                                                                {
                                                                    InternalAuditScheduleOnIDAug = InternalAuditSchedulingObject.Id;
                                                                }
                                                            }
                                                        }
                                                        AuditScheduling_SubProcessMapping auditScheduling_SubProcessMapping = new AuditScheduling_SubProcessMapping();
                                                        auditScheduling_SubProcessMapping.Process = processId;
                                                        auditScheduling_SubProcessMapping.AuditID = AuditId;
                                                        auditScheduling_SubProcessMapping.SubProcessID = subProcessId;
                                                        auditScheduling_SubProcessMapping.CreatedBy = Common.AuthenticationHelper.UserID;
                                                        auditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                        auditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDAug;
                                                        auditScheduling_SubProcessMapping.IsActive = true;
                                                        auditScheduling_SubProcessMapping.IsDeleted = false;
                                                        if (processId != 0 && AuditId != 0 && subProcessId != 0)
                                                        {
                                                            UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(auditScheduling_SubProcessMapping);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        AuditScheduling_SubProcessMapping auditScheduling_SubProcessMapping = new AuditScheduling_SubProcessMapping();
                                                        auditScheduling_SubProcessMapping.Process = processId;
                                                        auditScheduling_SubProcessMapping.AuditID = AuditId;
                                                        auditScheduling_SubProcessMapping.SubProcessID = subProcessId;
                                                        auditScheduling_SubProcessMapping.CreatedBy = Common.AuthenticationHelper.UserID;
                                                        auditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                        auditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDAug;
                                                        auditScheduling_SubProcessMapping.IsActive = true;
                                                        auditScheduling_SubProcessMapping.IsDeleted = false;
                                                        if (processId != 0 && AuditId != 0 && subProcessId != 0)
                                                        {
                                                            UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(auditScheduling_SubProcessMapping);
                                                        }
                                                    }
                                                }
                                                //else
                                                //{
                                                //    cvDuplicateEntry.IsValid = false;
                                                //    cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled.";
                                                //    break;
                                                //}
                                            }
                                            #endregion

                                            #region Sep
                                            if (period == "Sep" || period == "SEP" || period == "SEPTEMBER")
                                            {
                                                if (UserManagementRisk.IsExistAuditScheduling_ScheduleFirst(Convert.ToInt16(customerBranchId), Convert.ToInt16(VerticalId), processId, financialYear, AuditId, subProcessId, "Sep"))
                                                {
                                                    if (processId != OldProcessIDSep)
                                                    {
                                                        OldProcessIDSep = processId;
                                                        DateTime? dateAuditStartDate = null;
                                                        DateTime? dateAuditEndDate = null;

                                                        InternalAuditScheduling internalAuditScheduling = UserManagementRisk.GeDatestAuditScheduling_ScheduleFirst(Convert.ToInt16(customerBranchId), Convert.ToInt16(VerticalId), processId, financialYear, AuditId, subProcessId, "Sep");
                                                        if (internalAuditScheduling != null)
                                                        {
                                                            dateAuditStartDate = internalAuditScheduling.StartDate;
                                                            dateAuditEndDate = internalAuditScheduling.EndDate;
                                                        }
                                                        else
                                                        {
                                                            dateAuditStartDate = Convert.ToDateTime(AuditScheduleStartDate);
                                                            dateAuditEndDate = Convert.ToDateTime(AuditScheduleEndDate);
                                                        }
                                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(customerBranchId);
                                                        Internalauditscheduling.FinancialYear = financialYear;
                                                        Internalauditscheduling.TermName = period;
                                                        Internalauditscheduling.TermStatus = true;
                                                        Internalauditscheduling.Process = processId;
                                                        Internalauditscheduling.ISAHQMP = "M";
                                                        Internalauditscheduling.StartDate = dateAuditStartDate;
                                                        Internalauditscheduling.EndDate = dateAuditEndDate;
                                                        Internalauditscheduling.VerticalID = Convert.ToInt32(VerticalId);
                                                        Internalauditscheduling.AuditID = AuditId;
                                                        Internalauditscheduling.Createdby = Common.AuthenticationHelper.UserID;
                                                        Internalauditscheduling.CreatedOn = DateTime.Now;
                                                        if (processId != 0 && AuditId != 0)
                                                        {
                                                            if (!UserManagementRisk.IsExistsInInternalAuditSchedulings(Internalauditscheduling.Process, Internalauditscheduling.CustomerBranchId, Internalauditscheduling.FinancialYear, Internalauditscheduling.TermName, Convert.ToInt32(Internalauditscheduling.VerticalID), Convert.ToInt32(Internalauditscheduling.AuditID)))
                                                            {
                                                                InternalAuditScheduleOnIDSep = UserManagementRisk.AddDetailsInternalAuditSchedulingTable_ScheduleFirst(Internalauditscheduling);
                                                            }
                                                            else
                                                            {
                                                                InternalAuditScheduling InternalAuditSchedulingObject = UserManagementRisk.GetDetailsInternalAuditSchedulingsId(Internalauditscheduling.Process, Internalauditscheduling.CustomerBranchId, Internalauditscheduling.FinancialYear, Internalauditscheduling.TermName, Convert.ToInt32(Internalauditscheduling.VerticalID), Convert.ToInt32(Internalauditscheduling.AuditID));
                                                                if (InternalAuditSchedulingObject != null)
                                                                {
                                                                    InternalAuditScheduleOnIDSep = InternalAuditSchedulingObject.Id;
                                                                }
                                                            }
                                                        }
                                                        AuditScheduling_SubProcessMapping auditScheduling_SubProcessMapping = new AuditScheduling_SubProcessMapping();
                                                        auditScheduling_SubProcessMapping.Process = processId;
                                                        auditScheduling_SubProcessMapping.AuditID = AuditId;
                                                        auditScheduling_SubProcessMapping.SubProcessID = subProcessId;
                                                        auditScheduling_SubProcessMapping.CreatedBy = Common.AuthenticationHelper.UserID;
                                                        auditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                        auditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDSep;
                                                        auditScheduling_SubProcessMapping.IsActive = true;
                                                        auditScheduling_SubProcessMapping.IsDeleted = false;
                                                        if (processId != 0 && AuditId != 0 && subProcessId != 0)
                                                        {
                                                            UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(auditScheduling_SubProcessMapping);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        AuditScheduling_SubProcessMapping auditScheduling_SubProcessMapping = new AuditScheduling_SubProcessMapping();
                                                        auditScheduling_SubProcessMapping.Process = processId;
                                                        auditScheduling_SubProcessMapping.AuditID = AuditId;
                                                        auditScheduling_SubProcessMapping.SubProcessID = subProcessId;
                                                        auditScheduling_SubProcessMapping.CreatedBy = Common.AuthenticationHelper.UserID;
                                                        auditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                        auditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDSep;
                                                        auditScheduling_SubProcessMapping.IsActive = true;
                                                        auditScheduling_SubProcessMapping.IsDeleted = false;
                                                        if (processId != 0 && AuditId != 0 && subProcessId != 0)
                                                        {
                                                            UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(auditScheduling_SubProcessMapping);
                                                        }
                                                    }
                                                }
                                                //else
                                                //{
                                                //    cvDuplicateEntry.IsValid = false;
                                                //    cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled.";
                                                //    break;
                                                //}
                                            }

                                            #endregion

                                            #region Oct
                                            if (period == "Oct" || period == "OCT" || period == "OCTOBER")
                                            {
                                                if (UserManagementRisk.IsExistAuditScheduling_ScheduleFirst(Convert.ToInt16(customerBranchId), Convert.ToInt16(VerticalId), processId, financialYear, AuditId, subProcessId, "Oct"))
                                                {
                                                    if (processId != OldProcessIDOct)
                                                    {
                                                        OldProcessIDOct = processId;
                                                        DateTime? dateAuditStartDate = null;
                                                        DateTime? dateAuditEndDate = null;

                                                        InternalAuditScheduling internalAuditScheduling = UserManagementRisk.GeDatestAuditScheduling_ScheduleFirst(Convert.ToInt16(customerBranchId), Convert.ToInt16(VerticalId), processId, financialYear, AuditId, subProcessId, "Oct");
                                                        if (internalAuditScheduling != null)
                                                        {
                                                            dateAuditStartDate = internalAuditScheduling.StartDate;
                                                            dateAuditEndDate = internalAuditScheduling.EndDate;
                                                        }
                                                        else
                                                        {
                                                            dateAuditStartDate = Convert.ToDateTime(AuditScheduleStartDate);
                                                            dateAuditEndDate = Convert.ToDateTime(AuditScheduleEndDate);
                                                        }
                                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(customerBranchId);
                                                        Internalauditscheduling.FinancialYear = financialYear;
                                                        Internalauditscheduling.TermName = period;
                                                        Internalauditscheduling.TermStatus = true;
                                                        Internalauditscheduling.Process = processId;
                                                        Internalauditscheduling.ISAHQMP = "M";
                                                        Internalauditscheduling.StartDate = dateAuditStartDate;
                                                        Internalauditscheduling.EndDate = dateAuditEndDate;
                                                        Internalauditscheduling.VerticalID = Convert.ToInt32(VerticalId);
                                                        Internalauditscheduling.AuditID = AuditId;
                                                        Internalauditscheduling.Createdby = Common.AuthenticationHelper.UserID;
                                                        Internalauditscheduling.CreatedOn = DateTime.Now;
                                                        if (processId != 0 && AuditId != 0)
                                                        {
                                                            if (!UserManagementRisk.IsExistsInInternalAuditSchedulings(Internalauditscheduling.Process, Internalauditscheduling.CustomerBranchId, Internalauditscheduling.FinancialYear, Internalauditscheduling.TermName, Convert.ToInt32(Internalauditscheduling.VerticalID), Convert.ToInt32(Internalauditscheduling.AuditID)))
                                                            {
                                                                InternalAuditScheduleOnIDOct = UserManagementRisk.AddDetailsInternalAuditSchedulingTable_ScheduleFirst(Internalauditscheduling);
                                                            }
                                                            else
                                                            {
                                                                InternalAuditScheduling InternalAuditSchedulingObject = UserManagementRisk.GetDetailsInternalAuditSchedulingsId(Internalauditscheduling.Process, Internalauditscheduling.CustomerBranchId, Internalauditscheduling.FinancialYear, Internalauditscheduling.TermName, Convert.ToInt32(Internalauditscheduling.VerticalID), Convert.ToInt32(Internalauditscheduling.AuditID));
                                                                if (InternalAuditSchedulingObject != null)
                                                                {
                                                                    InternalAuditScheduleOnIDOct = InternalAuditSchedulingObject.Id;
                                                                }
                                                            }
                                                        }
                                                        AuditScheduling_SubProcessMapping auditScheduling_SubProcessMapping = new AuditScheduling_SubProcessMapping();
                                                        auditScheduling_SubProcessMapping.Process = processId;
                                                        auditScheduling_SubProcessMapping.AuditID = AuditId;
                                                        auditScheduling_SubProcessMapping.SubProcessID = subProcessId;
                                                        auditScheduling_SubProcessMapping.CreatedBy = Common.AuthenticationHelper.UserID;
                                                        auditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                        auditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDOct;
                                                        auditScheduling_SubProcessMapping.IsActive = true;
                                                        auditScheduling_SubProcessMapping.IsDeleted = false;
                                                        if (processId != 0 && AuditId != 0 && subProcessId != 0)
                                                        {
                                                            UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(auditScheduling_SubProcessMapping);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        AuditScheduling_SubProcessMapping auditScheduling_SubProcessMapping = new AuditScheduling_SubProcessMapping();
                                                        auditScheduling_SubProcessMapping.Process = processId;
                                                        auditScheduling_SubProcessMapping.AuditID = AuditId;
                                                        auditScheduling_SubProcessMapping.SubProcessID = subProcessId;
                                                        auditScheduling_SubProcessMapping.CreatedBy = Common.AuthenticationHelper.UserID;
                                                        auditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                        auditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDOct;
                                                        auditScheduling_SubProcessMapping.IsActive = true;
                                                        auditScheduling_SubProcessMapping.IsDeleted = false;
                                                        if (processId != 0 && AuditId != 0 && subProcessId != 0)
                                                        {
                                                            UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(auditScheduling_SubProcessMapping);
                                                        }
                                                    }
                                                }
                                                //else
                                                //{
                                                //    cvDuplicateEntry.IsValid = false;
                                                //    cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled.";
                                                //    break;
                                                //}
                                            }
                                            #endregion

                                            #region Nov
                                            if (period == "Nov" || period == "NOV" || period == "NOVEMBER")
                                            {
                                                if (UserManagementRisk.IsExistAuditScheduling_ScheduleFirst(Convert.ToInt16(customerBranchId), Convert.ToInt16(VerticalId), processId, financialYear, AuditId, subProcessId, "Nov"))
                                                {
                                                    if (processId != OldProcessIDNov)
                                                    {
                                                        OldProcessIDNov = processId;
                                                        DateTime? dateAuditStartDate = null;
                                                        DateTime? dateAuditEndDate = null;

                                                        InternalAuditScheduling internalAuditScheduling = UserManagementRisk.GeDatestAuditScheduling_ScheduleFirst(Convert.ToInt16(customerBranchId), Convert.ToInt16(VerticalId), processId, financialYear, AuditId, subProcessId, "Nov");
                                                        if (internalAuditScheduling != null)
                                                        {
                                                            dateAuditStartDate = internalAuditScheduling.StartDate;
                                                            dateAuditEndDate = internalAuditScheduling.EndDate;
                                                        }
                                                        else
                                                        {
                                                            dateAuditStartDate = Convert.ToDateTime(AuditScheduleStartDate);
                                                            dateAuditEndDate = Convert.ToDateTime(AuditScheduleEndDate);
                                                        }

                                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(customerBranchId);
                                                        Internalauditscheduling.FinancialYear = financialYear;
                                                        Internalauditscheduling.TermName = period;
                                                        Internalauditscheduling.TermStatus = true;
                                                        Internalauditscheduling.Process = processId;
                                                        Internalauditscheduling.ISAHQMP = "M";
                                                        Internalauditscheduling.StartDate = dateAuditStartDate;
                                                        Internalauditscheduling.EndDate = dateAuditEndDate;
                                                        Internalauditscheduling.VerticalID = Convert.ToInt32(VerticalId);
                                                        Internalauditscheduling.AuditID = AuditId;
                                                        Internalauditscheduling.Createdby = Common.AuthenticationHelper.UserID;
                                                        Internalauditscheduling.CreatedOn = DateTime.Now;
                                                        if (processId != 0 && AuditId != 0)
                                                        {
                                                            if (!UserManagementRisk.IsExistsInInternalAuditSchedulings(Internalauditscheduling.Process, Internalauditscheduling.CustomerBranchId, Internalauditscheduling.FinancialYear, Internalauditscheduling.TermName, Convert.ToInt32(Internalauditscheduling.VerticalID), Convert.ToInt32(Internalauditscheduling.AuditID)))
                                                            {
                                                                InternalAuditScheduleOnIDNov = UserManagementRisk.AddDetailsInternalAuditSchedulingTable_ScheduleFirst(Internalauditscheduling);
                                                            }
                                                            else
                                                            {
                                                                InternalAuditScheduling InternalAuditSchedulingObject = UserManagementRisk.GetDetailsInternalAuditSchedulingsId(Internalauditscheduling.Process, Internalauditscheduling.CustomerBranchId, Internalauditscheduling.FinancialYear, Internalauditscheduling.TermName, Convert.ToInt32(Internalauditscheduling.VerticalID), Convert.ToInt32(Internalauditscheduling.AuditID));
                                                                if (InternalAuditSchedulingObject != null)
                                                                {
                                                                    InternalAuditScheduleOnIDNov = InternalAuditSchedulingObject.Id;
                                                                }
                                                            }
                                                        }
                                                        AuditScheduling_SubProcessMapping auditScheduling_SubProcessMapping = new AuditScheduling_SubProcessMapping();
                                                        auditScheduling_SubProcessMapping.Process = processId;
                                                        auditScheduling_SubProcessMapping.AuditID = AuditId;
                                                        auditScheduling_SubProcessMapping.SubProcessID = subProcessId;
                                                        auditScheduling_SubProcessMapping.CreatedBy = Common.AuthenticationHelper.UserID;
                                                        auditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                        auditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDNov;
                                                        auditScheduling_SubProcessMapping.IsActive = true;
                                                        auditScheduling_SubProcessMapping.IsDeleted = false;
                                                        if (processId != 0 && AuditId != 0 && subProcessId != 0)
                                                        {
                                                            UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(auditScheduling_SubProcessMapping);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        AuditScheduling_SubProcessMapping auditScheduling_SubProcessMapping = new AuditScheduling_SubProcessMapping();
                                                        auditScheduling_SubProcessMapping.Process = processId;
                                                        auditScheduling_SubProcessMapping.AuditID = AuditId;
                                                        auditScheduling_SubProcessMapping.SubProcessID = subProcessId;
                                                        auditScheduling_SubProcessMapping.CreatedBy = Common.AuthenticationHelper.UserID;
                                                        auditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                        auditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDNov;
                                                        auditScheduling_SubProcessMapping.IsActive = true;
                                                        auditScheduling_SubProcessMapping.IsDeleted = false;
                                                        if (processId != 0 && AuditId != 0 && subProcessId != 0)
                                                        {
                                                            UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(auditScheduling_SubProcessMapping);
                                                        }
                                                    }
                                                }
                                                //else
                                                //{
                                                //    cvDuplicateEntry.IsValid = false;
                                                //    cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled.";
                                                //    break;
                                                //}
                                            }
                                            #endregion

                                            #region Dec
                                            if (period == "Dec" || period == "DEC" || period == "DECEMBER")
                                            {
                                                if (UserManagementRisk.IsExistAuditScheduling_ScheduleFirst(Convert.ToInt16(customerBranchId), Convert.ToInt16(VerticalId), processId, financialYear, AuditId, subProcessId, "Dec"))
                                                {
                                                    if (processId != OldProcessIDDec)
                                                    {
                                                        OldProcessIDDec = processId;
                                                        DateTime? dateAuditStartDate = null;
                                                        DateTime? dateAuditEndDate = null;

                                                        InternalAuditScheduling internalAuditScheduling = UserManagementRisk.GeDatestAuditScheduling_ScheduleFirst(Convert.ToInt16(customerBranchId), Convert.ToInt16(VerticalId), processId, financialYear, AuditId, subProcessId, "Dec");
                                                        if (internalAuditScheduling != null)
                                                        {
                                                            dateAuditStartDate = internalAuditScheduling.StartDate;
                                                            dateAuditEndDate = internalAuditScheduling.EndDate;
                                                        }
                                                        else
                                                        {
                                                            dateAuditStartDate = Convert.ToDateTime(AuditScheduleStartDate);
                                                            dateAuditEndDate = Convert.ToDateTime(AuditScheduleEndDate);
                                                        }

                                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(customerBranchId);
                                                        Internalauditscheduling.FinancialYear = financialYear;
                                                        Internalauditscheduling.TermName = period;
                                                        Internalauditscheduling.TermStatus = true;
                                                        Internalauditscheduling.Process = processId;
                                                        Internalauditscheduling.ISAHQMP = "M";
                                                        Internalauditscheduling.StartDate = dateAuditStartDate;
                                                        Internalauditscheduling.EndDate = dateAuditEndDate;
                                                        Internalauditscheduling.VerticalID = Convert.ToInt32(VerticalId);
                                                        Internalauditscheduling.AuditID = AuditId;
                                                        Internalauditscheduling.Createdby = Common.AuthenticationHelper.UserID;
                                                        Internalauditscheduling.CreatedOn = DateTime.Now;
                                                        if (processId != 0 && AuditId != 0)
                                                        {
                                                            if (!UserManagementRisk.IsExistsInInternalAuditSchedulings(Internalauditscheduling.Process, Internalauditscheduling.CustomerBranchId, Internalauditscheduling.FinancialYear, Internalauditscheduling.TermName, Convert.ToInt32(Internalauditscheduling.VerticalID), Convert.ToInt32(Internalauditscheduling.AuditID)))
                                                            {
                                                                InternalAuditScheduleOnIDDec = UserManagementRisk.AddDetailsInternalAuditSchedulingTable_ScheduleFirst(Internalauditscheduling);
                                                            }
                                                            else
                                                            {
                                                                InternalAuditScheduling InternalAuditSchedulingObject = UserManagementRisk.GetDetailsInternalAuditSchedulingsId(Internalauditscheduling.Process, Internalauditscheduling.CustomerBranchId, Internalauditscheduling.FinancialYear, Internalauditscheduling.TermName, Convert.ToInt32(Internalauditscheduling.VerticalID), Convert.ToInt32(Internalauditscheduling.AuditID));
                                                                if (InternalAuditSchedulingObject != null)
                                                                {
                                                                    InternalAuditScheduleOnIDDec = InternalAuditSchedulingObject.Id;
                                                                }
                                                            }
                                                        }
                                                        AuditScheduling_SubProcessMapping auditScheduling_SubProcessMapping = new AuditScheduling_SubProcessMapping();
                                                        auditScheduling_SubProcessMapping.Process = processId;
                                                        auditScheduling_SubProcessMapping.AuditID = AuditId;
                                                        auditScheduling_SubProcessMapping.SubProcessID = subProcessId;
                                                        auditScheduling_SubProcessMapping.CreatedBy = Common.AuthenticationHelper.UserID;
                                                        auditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                        auditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDDec;
                                                        auditScheduling_SubProcessMapping.IsActive = true;
                                                        auditScheduling_SubProcessMapping.IsDeleted = false;
                                                        if (processId != 0 && AuditId != 0 && subProcessId != 0)
                                                        {
                                                            UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(auditScheduling_SubProcessMapping);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        AuditScheduling_SubProcessMapping auditScheduling_SubProcessMapping = new AuditScheduling_SubProcessMapping();
                                                        auditScheduling_SubProcessMapping.Process = processId;
                                                        auditScheduling_SubProcessMapping.AuditID = AuditId;
                                                        auditScheduling_SubProcessMapping.SubProcessID = subProcessId;
                                                        auditScheduling_SubProcessMapping.CreatedBy = Common.AuthenticationHelper.UserID;
                                                        auditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                        auditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDDec;
                                                        auditScheduling_SubProcessMapping.IsActive = true;
                                                        auditScheduling_SubProcessMapping.IsDeleted = false;
                                                        if (processId != 0 && AuditId != 0 && subProcessId != 0)
                                                        {
                                                            UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(auditScheduling_SubProcessMapping);
                                                        }
                                                    }
                                                }
                                                //else
                                                //{
                                                //    cvDuplicateEntry.IsValid = false;
                                                //    cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled.";
                                                //    break;
                                                //}
                                            }
                                            #endregion

                                            #region Jan
                                            if (period == "Jan" || period == "JAN" || period == "JANUARY")
                                            {
                                                if (UserManagementRisk.IsExistAuditScheduling_ScheduleFirst(Convert.ToInt16(customerBranchId), Convert.ToInt16(VerticalId), processId, financialYear, AuditId, subProcessId, "Jan"))
                                                {
                                                    if (processId != OldProcessIDJan)
                                                    {
                                                        OldProcessIDJan = processId;
                                                        DateTime? dateAuditStartDate = null;
                                                        DateTime? dateAuditEndDate = null;

                                                        InternalAuditScheduling internalAuditScheduling = UserManagementRisk.GeDatestAuditScheduling_ScheduleFirst(Convert.ToInt16(customerBranchId), Convert.ToInt16(VerticalId), processId, financialYear, AuditId, subProcessId, "Jan");
                                                        if (internalAuditScheduling != null)
                                                        {
                                                            dateAuditStartDate = internalAuditScheduling.StartDate;
                                                            dateAuditEndDate = internalAuditScheduling.EndDate;
                                                        }
                                                        else
                                                        {
                                                            dateAuditStartDate = Convert.ToDateTime(AuditScheduleStartDate);
                                                            dateAuditEndDate = Convert.ToDateTime(AuditScheduleEndDate);
                                                        }

                                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(customerBranchId);
                                                        Internalauditscheduling.FinancialYear = financialYear;
                                                        Internalauditscheduling.TermName = period;
                                                        Internalauditscheduling.TermStatus = true;
                                                        Internalauditscheduling.Process = processId;
                                                        Internalauditscheduling.ISAHQMP = "M";
                                                        Internalauditscheduling.StartDate = dateAuditStartDate;
                                                        Internalauditscheduling.EndDate = dateAuditEndDate;
                                                        Internalauditscheduling.VerticalID = Convert.ToInt32(VerticalId);
                                                        Internalauditscheduling.AuditID = AuditId;
                                                        Internalauditscheduling.Createdby = Common.AuthenticationHelper.UserID;
                                                        Internalauditscheduling.CreatedOn = DateTime.Now;
                                                        if (processId != 0 && AuditId != 0)
                                                        {
                                                            if (!UserManagementRisk.IsExistsInInternalAuditSchedulings(Internalauditscheduling.Process, Internalauditscheduling.CustomerBranchId, Internalauditscheduling.FinancialYear, Internalauditscheduling.TermName, Convert.ToInt32(Internalauditscheduling.VerticalID), Convert.ToInt32(Internalauditscheduling.AuditID)))
                                                            {
                                                                InternalAuditScheduleOnIDJan = UserManagementRisk.AddDetailsInternalAuditSchedulingTable_ScheduleFirst(Internalauditscheduling);
                                                            }
                                                            else
                                                            {
                                                                InternalAuditScheduling InternalAuditSchedulingObject = UserManagementRisk.GetDetailsInternalAuditSchedulingsId(Internalauditscheduling.Process, Internalauditscheduling.CustomerBranchId, Internalauditscheduling.FinancialYear, Internalauditscheduling.TermName, Convert.ToInt32(Internalauditscheduling.VerticalID), Convert.ToInt32(Internalauditscheduling.AuditID));
                                                                if (InternalAuditSchedulingObject != null)
                                                                {
                                                                    InternalAuditScheduleOnIDJan = InternalAuditSchedulingObject.Id;
                                                                }
                                                            }
                                                        }
                                                        AuditScheduling_SubProcessMapping auditScheduling_SubProcessMapping = new AuditScheduling_SubProcessMapping();
                                                        auditScheduling_SubProcessMapping.Process = processId;
                                                        auditScheduling_SubProcessMapping.AuditID = AuditId;
                                                        auditScheduling_SubProcessMapping.SubProcessID = subProcessId;
                                                        auditScheduling_SubProcessMapping.CreatedBy = Common.AuthenticationHelper.UserID;
                                                        auditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                        auditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDJan;
                                                        auditScheduling_SubProcessMapping.IsActive = true;
                                                        auditScheduling_SubProcessMapping.IsDeleted = false;
                                                        if (processId != 0 && AuditId != 0 && subProcessId != 0)
                                                        {
                                                            UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(auditScheduling_SubProcessMapping);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        AuditScheduling_SubProcessMapping auditScheduling_SubProcessMapping = new AuditScheduling_SubProcessMapping();
                                                        auditScheduling_SubProcessMapping.Process = processId;
                                                        auditScheduling_SubProcessMapping.AuditID = AuditId;
                                                        auditScheduling_SubProcessMapping.SubProcessID = subProcessId;
                                                        auditScheduling_SubProcessMapping.CreatedBy = Common.AuthenticationHelper.UserID;
                                                        auditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                        auditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDJan;
                                                        auditScheduling_SubProcessMapping.IsActive = true;
                                                        auditScheduling_SubProcessMapping.IsDeleted = false;
                                                        if (processId != 0 && AuditId != 0 && subProcessId != 0)
                                                        {
                                                            UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(auditScheduling_SubProcessMapping);
                                                        }
                                                    }
                                                }
                                                //else
                                                //{
                                                //    cvDuplicateEntry.IsValid = false;
                                                //    cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled.";
                                                //    break;
                                                //}
                                            }
                                            #endregion

                                            #region Feb
                                            if (period == "Feb" || period == "FEB" || period == "FEBRUARY")
                                            {
                                                if (UserManagementRisk.IsExistAuditScheduling_ScheduleFirst(Convert.ToInt16(customerBranchId), Convert.ToInt16(VerticalId), processId, financialYear, AuditId, subProcessId, "Feb"))
                                                {
                                                    if (processId != OldProcessIDFeb)
                                                    {
                                                        OldProcessIDFeb = processId;
                                                        DateTime? dateAuditStartDate = null;
                                                        DateTime? dateAuditEndDate = null;

                                                        InternalAuditScheduling internalAuditScheduling = UserManagementRisk.GeDatestAuditScheduling_ScheduleFirst(Convert.ToInt16(customerBranchId), Convert.ToInt16(VerticalId), processId, financialYear, AuditId, subProcessId, "Feb");
                                                        if (internalAuditScheduling != null)
                                                        {
                                                            dateAuditStartDate = internalAuditScheduling.StartDate;
                                                            dateAuditEndDate = internalAuditScheduling.EndDate;
                                                        }
                                                        else
                                                        {
                                                            dateAuditStartDate = Convert.ToDateTime(AuditScheduleStartDate);
                                                            dateAuditEndDate = Convert.ToDateTime(AuditScheduleEndDate);
                                                        }

                                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(customerBranchId);
                                                        Internalauditscheduling.FinancialYear = financialYear;
                                                        Internalauditscheduling.TermName = period;
                                                        Internalauditscheduling.TermStatus = true;
                                                        Internalauditscheduling.Process = processId;
                                                        Internalauditscheduling.ISAHQMP = "M";
                                                        Internalauditscheduling.StartDate = dateAuditStartDate;
                                                        Internalauditscheduling.EndDate = dateAuditEndDate;
                                                        Internalauditscheduling.VerticalID = Convert.ToInt32(VerticalId);
                                                        Internalauditscheduling.AuditID = AuditId;
                                                        Internalauditscheduling.Createdby = Common.AuthenticationHelper.UserID;
                                                        Internalauditscheduling.CreatedOn = DateTime.Now;
                                                        if (processId != 0 && AuditId != 0)
                                                        {
                                                            if (!UserManagementRisk.IsExistsInInternalAuditSchedulings(Internalauditscheduling.Process, Internalauditscheduling.CustomerBranchId, Internalauditscheduling.FinancialYear, Internalauditscheduling.TermName, Convert.ToInt32(Internalauditscheduling.VerticalID), Convert.ToInt32(Internalauditscheduling.AuditID)))
                                                            {
                                                                InternalAuditScheduleOnIDFeb = UserManagementRisk.AddDetailsInternalAuditSchedulingTable_ScheduleFirst(Internalauditscheduling);
                                                            }
                                                            else
                                                            {
                                                                InternalAuditScheduling InternalAuditSchedulingObject = UserManagementRisk.GetDetailsInternalAuditSchedulingsId(Internalauditscheduling.Process, Internalauditscheduling.CustomerBranchId, Internalauditscheduling.FinancialYear, Internalauditscheduling.TermName, Convert.ToInt32(Internalauditscheduling.VerticalID), Convert.ToInt32(Internalauditscheduling.AuditID));
                                                                if (InternalAuditSchedulingObject != null)
                                                                {
                                                                    InternalAuditScheduleOnIDFeb = InternalAuditSchedulingObject.Id;
                                                                }
                                                            }
                                                        }
                                                        AuditScheduling_SubProcessMapping auditScheduling_SubProcessMapping = new AuditScheduling_SubProcessMapping();
                                                        auditScheduling_SubProcessMapping.Process = processId;
                                                        auditScheduling_SubProcessMapping.AuditID = AuditId;
                                                        auditScheduling_SubProcessMapping.SubProcessID = subProcessId;
                                                        auditScheduling_SubProcessMapping.CreatedBy = Common.AuthenticationHelper.UserID;
                                                        auditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                        auditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDFeb;
                                                        auditScheduling_SubProcessMapping.IsActive = true;
                                                        auditScheduling_SubProcessMapping.IsDeleted = false;
                                                        if (processId != 0 && AuditId != 0 && subProcessId != 0)
                                                        {
                                                            UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(auditScheduling_SubProcessMapping);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        AuditScheduling_SubProcessMapping auditScheduling_SubProcessMapping = new AuditScheduling_SubProcessMapping();
                                                        auditScheduling_SubProcessMapping.Process = processId;
                                                        auditScheduling_SubProcessMapping.AuditID = AuditId;
                                                        auditScheduling_SubProcessMapping.SubProcessID = subProcessId;
                                                        auditScheduling_SubProcessMapping.CreatedBy = Common.AuthenticationHelper.UserID;
                                                        auditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                        auditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDFeb;
                                                        auditScheduling_SubProcessMapping.IsActive = true;
                                                        auditScheduling_SubProcessMapping.IsDeleted = false;
                                                        if (processId != 0 && AuditId != 0 && subProcessId != 0)
                                                        {
                                                            UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(auditScheduling_SubProcessMapping);
                                                        }
                                                    }
                                                }
                                                //else
                                                //{
                                                //    cvDuplicateEntry.IsValid = false;
                                                //    cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled.";
                                                //    break;
                                                //}
                                            }
                                            #endregion

                                            #region Mar
                                            if (period == "Mar" || period == "MAR" || period == "MARCH")
                                            {
                                                if (UserManagementRisk.IsExistAuditScheduling_ScheduleFirst(Convert.ToInt16(customerBranchId), Convert.ToInt16(VerticalId), processId, financialYear, AuditId, subProcessId, "Mar"))
                                                {
                                                    if (processId != OldProcessIDMar)
                                                    {
                                                        OldProcessIDMar = processId;
                                                        DateTime? dateAuditStartDate = null;
                                                        DateTime? dateAuditEndDate = null;

                                                        InternalAuditScheduling internalAuditScheduling = UserManagementRisk.GeDatestAuditScheduling_ScheduleFirst(Convert.ToInt16(customerBranchId), Convert.ToInt16(VerticalId), processId, financialYear, AuditId, subProcessId, "Mar");
                                                        if (internalAuditScheduling != null)
                                                        {
                                                            dateAuditStartDate = internalAuditScheduling.StartDate;
                                                            dateAuditEndDate = internalAuditScheduling.EndDate;
                                                        }
                                                        else
                                                        {
                                                            dateAuditStartDate = Convert.ToDateTime(AuditScheduleStartDate);
                                                            dateAuditEndDate = Convert.ToDateTime(AuditScheduleEndDate);
                                                        }

                                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(customerBranchId);
                                                        Internalauditscheduling.FinancialYear = financialYear;
                                                        Internalauditscheduling.TermName = period;
                                                        Internalauditscheduling.TermStatus = true;
                                                        Internalauditscheduling.Process = processId;
                                                        Internalauditscheduling.ISAHQMP = "M";
                                                        Internalauditscheduling.StartDate = dateAuditStartDate;
                                                        Internalauditscheduling.EndDate = dateAuditEndDate;
                                                        Internalauditscheduling.VerticalID = Convert.ToInt32(VerticalId);
                                                        Internalauditscheduling.AuditID = AuditId;
                                                        Internalauditscheduling.Createdby = Common.AuthenticationHelper.UserID;
                                                        Internalauditscheduling.CreatedOn = DateTime.Now;
                                                        if (processId != 0 && AuditId != 0)
                                                        {
                                                            if (!UserManagementRisk.IsExistsInInternalAuditSchedulings(Internalauditscheduling.Process, Internalauditscheduling.CustomerBranchId, Internalauditscheduling.FinancialYear, Internalauditscheduling.TermName, Convert.ToInt32(Internalauditscheduling.VerticalID), Convert.ToInt32(Internalauditscheduling.AuditID)))
                                                            {
                                                                InternalAuditScheduleOnIDMar = UserManagementRisk.AddDetailsInternalAuditSchedulingTable_ScheduleFirst(Internalauditscheduling);
                                                            }
                                                            else
                                                            {
                                                                InternalAuditScheduling InternalAuditSchedulingObject = UserManagementRisk.GetDetailsInternalAuditSchedulingsId(Internalauditscheduling.Process, Internalauditscheduling.CustomerBranchId, Internalauditscheduling.FinancialYear, Internalauditscheduling.TermName, Convert.ToInt32(Internalauditscheduling.VerticalID), Convert.ToInt32(Internalauditscheduling.AuditID));
                                                                if (InternalAuditSchedulingObject != null)
                                                                {
                                                                    InternalAuditScheduleOnIDMar = InternalAuditSchedulingObject.Id;
                                                                }
                                                            }
                                                        }
                                                        AuditScheduling_SubProcessMapping auditScheduling_SubProcessMapping = new AuditScheduling_SubProcessMapping();
                                                        auditScheduling_SubProcessMapping.Process = processId;
                                                        auditScheduling_SubProcessMapping.AuditID = AuditId;
                                                        auditScheduling_SubProcessMapping.SubProcessID = subProcessId;
                                                        auditScheduling_SubProcessMapping.CreatedBy = Common.AuthenticationHelper.UserID;
                                                        auditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                        auditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDMar;
                                                        auditScheduling_SubProcessMapping.IsActive = true;
                                                        auditScheduling_SubProcessMapping.IsDeleted = false;
                                                        if (processId != 0 && AuditId != 0 && subProcessId != 0)
                                                        {
                                                            UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(auditScheduling_SubProcessMapping);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        AuditScheduling_SubProcessMapping auditScheduling_SubProcessMapping = new AuditScheduling_SubProcessMapping();
                                                        auditScheduling_SubProcessMapping.Process = processId;
                                                        auditScheduling_SubProcessMapping.AuditID = AuditId;
                                                        auditScheduling_SubProcessMapping.SubProcessID = subProcessId;
                                                        auditScheduling_SubProcessMapping.CreatedBy = Common.AuthenticationHelper.UserID;
                                                        auditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                        auditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDMar;
                                                        auditScheduling_SubProcessMapping.IsActive = true;
                                                        auditScheduling_SubProcessMapping.IsDeleted = false;
                                                        if (processId != 0 && AuditId != 0 && subProcessId != 0)
                                                        {
                                                            UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(auditScheduling_SubProcessMapping);
                                                        }
                                                    }
                                                }
                                                //else
                                                //{
                                                //    cvDuplicateEntry.IsValid = false;
                                                //    cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled.";
                                                //    break;
                                                //}
                                            }
                                            #endregion
                                            #endregion

                                            #region Phase

                                            //    #region Commented Code
                                            //    #region Phase 1
                                            //    if (period == "Phase1")
                                            //    {
                                            //        int InternalAuditScheduleOnIDPhase1 = 0;
                                            //        int OldProcessIDPhase1 = -1;
                                            //        if (UserManagementRisk.IsExistAuditScheduling_ScheduleFirst(Convert.ToInt16(customerBranchId), Convert.ToInt16(VerticalId), processId, financialYear, AuditId, subProcessId, "Phase1", noofphases))
                                            //        {
                                            //            if (processId != OldProcessIDPhase1)
                                            //            {
                                            //                OldProcessIDPhase1 = processId;

                                            //                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                            //                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(customerBranchId);
                                            //                Internalauditscheduling.FinancialYear = financialYear;
                                            //                Internalauditscheduling.TermName = "Phase1";
                                            //                Internalauditscheduling.TermStatus = true;
                                            //                Internalauditscheduling.Process = processId;
                                            //                Internalauditscheduling.ISAHQMP = "P";
                                            //                Internalauditscheduling.PhaseCount = noofphases;
                                            //                Internalauditscheduling.StartDate = Convert.ToDateTime(AuditStartDate, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                            //                Internalauditscheduling.EndDate = Convert.ToDateTime(AuditEndDate, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                            //                Internalauditscheduling.VerticalID = Convert.ToInt32(VerticalId);
                                            //                Internalauditscheduling.AuditID = AuditId;
                                            //                Internalauditscheduling.Createdby = Common.AuthenticationHelper.UserID;
                                            //                Internalauditscheduling.CreatedOn = DateTime.Now;
                                            //                if (processId != 0 && AuditId != 0)
                                            //                {
                                            //                    InternalAuditScheduleOnIDPhase1 = UserManagementRisk.AddDetailsInternalAuditSchedulingTable_ScheduleFirst(Internalauditscheduling);
                                            //                }

                                            //                AuditScheduling_SubProcessMapping auditScheduling_SubProcessMapping = new AuditScheduling_SubProcessMapping();
                                            //                auditScheduling_SubProcessMapping.Process = processId;
                                            //                auditScheduling_SubProcessMapping.AuditID = AuditId;
                                            //                auditScheduling_SubProcessMapping.SubProcessID = subProcessId;
                                            //                auditScheduling_SubProcessMapping.CreatedBy = Common.AuthenticationHelper.UserID;
                                            //                auditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                            //                auditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDPhase1;
                                            //                auditScheduling_SubProcessMapping.IsActive = true;
                                            //                auditScheduling_SubProcessMapping.IsDeleted = false;
                                            //                if (processId != 0 && AuditId != 0 && subProcessId != 0)
                                            //                {
                                            //                    UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(auditScheduling_SubProcessMapping);
                                            //                }
                                            //            }
                                            //            else
                                            //            {
                                            //                AuditScheduling_SubProcessMapping auditScheduling_SubProcessMapping = new AuditScheduling_SubProcessMapping();
                                            //                auditScheduling_SubProcessMapping.Process = processId;
                                            //                auditScheduling_SubProcessMapping.AuditID = AuditId;
                                            //                auditScheduling_SubProcessMapping.SubProcessID = subProcessId;
                                            //                auditScheduling_SubProcessMapping.CreatedBy = Common.AuthenticationHelper.UserID;
                                            //                auditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                            //                auditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDPhase1;
                                            //                auditScheduling_SubProcessMapping.IsActive = true;
                                            //                auditScheduling_SubProcessMapping.IsDeleted = false;
                                            //                if (processId != 0 && AuditId != 0 && subProcessId != 0)
                                            //                {
                                            //                    UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(auditScheduling_SubProcessMapping);
                                            //                }
                                            //            }
                                            //        }
                                            //        else
                                            //        {
                                            //            cvDuplicateEntry.IsValid = false;
                                            //            cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled.";
                                            //            break;
                                            //        }
                                            //    }
                                            //    #endregion

                                            //    #region Phase 2
                                            //    if (period == "Phase2")
                                            //    {
                                            //        if (UserManagementRisk.IsExistAuditScheduling_ScheduleFirst(Convert.ToInt16(customerBranchId), Convert.ToInt16(VerticalId), processId, financialYear, AuditId, subProcessId, "Phase2", noofphases))
                                            //        {
                                            //            if (processId != OldProcessIDPhase2)
                                            //            {
                                            //                OldProcessIDPhase2 = processId;

                                            //                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                            //                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                            //                Internalauditscheduling.FinancialYear = f1;
                                            //                Internalauditscheduling.TermName = "Phase2";
                                            //                Internalauditscheduling.TermStatus = true;
                                            //                Internalauditscheduling.Process = processId;
                                            //                Internalauditscheduling.ISAHQMP = "P";
                                            //                Internalauditscheduling.PhaseCount = noofphases;
                                            //                Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                            //                Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                            //                Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                            //                Internalauditscheduling.AuditID = AuditIDPhase2;
                                            //                Internalauditscheduling.Createdby = UserID;
                                            //                Internalauditscheduling.CreatedOn = DateTime.Now;
                                            //                if (processId != 0 && AuditIDPhase2 != 0)
                                            //                {
                                            //                    InternalAuditScheduleOnIDPhase2 = UserManagementRisk.AddDetailsInternalAuditSchedulingTable_ScheduleFirst(Internalauditscheduling);
                                            //                }
                                            //                AuditScheduling_SubProcessMapping.Process = processId;
                                            //                AuditScheduling_SubProcessMapping.AuditID = AuditIDPhase2;
                                            //                AuditScheduling_SubProcessMapping.SubProcessID = subprocessid;
                                            //                AuditScheduling_SubProcessMapping.CreatedBy = UserID;
                                            //                AuditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                            //                AuditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDPhase2;
                                            //                AuditScheduling_SubProcessMapping.IsActive = true;
                                            //                AuditScheduling_SubProcessMapping.IsDeleted = false;
                                            //                if (processId != 0 && AuditIDPhase2 != 0 && subprocessid != 0)
                                            //                {
                                            //                    UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(AuditScheduling_SubProcessMapping);
                                            //                }
                                            //            }
                                            //            else
                                            //            {
                                            //                AuditScheduling_SubProcessMapping.Process = processId;
                                            //                AuditScheduling_SubProcessMapping.AuditID = AuditIDPhase2;
                                            //                AuditScheduling_SubProcessMapping.SubProcessID = subprocessid;
                                            //                AuditScheduling_SubProcessMapping.CreatedBy = UserID;
                                            //                AuditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                            //                AuditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDPhase2;
                                            //                AuditScheduling_SubProcessMapping.IsActive = true;
                                            //                AuditScheduling_SubProcessMapping.IsDeleted = false;
                                            //                if (processId != 0 && AuditIDPhase2 != 0 && subprocessid != 0)
                                            //                {
                                            //                    UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(AuditScheduling_SubProcessMapping);
                                            //                }
                                            //            }
                                            //        }
                                            //        else
                                            //        {
                                            //            cvDuplicateEntry.IsValid = false;
                                            //            cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                            //            break;
                                            //        }
                                            //    }

                                            //    #endregion

                                            //    #region Phase 3
                                            //    if (period == "Phase3")
                                            //    {
                                            //        if (UserManagementRisk.IsExistAuditScheduling_ScheduleFirst(Convert.ToInt16(customerBranchId), Convert.ToInt16(VerticalId), processId, financialYear, AuditId, subProcessId, "Phase3", noofphases))
                                            //        {
                                            //            if (processid != OldProcessIDPhase1)
                                            //            {
                                            //                OldProcessIDPhase1 = Convert.ToInt32(lblProcessId.Text);

                                            //                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                            //                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                            //                Internalauditscheduling.FinancialYear = f1;
                                            //                Internalauditscheduling.TermName = "Phase3";
                                            //                Internalauditscheduling.TermStatus = true;
                                            //                Internalauditscheduling.Process = processid;
                                            //                Internalauditscheduling.ISAHQMP = "P";
                                            //                Internalauditscheduling.PhaseCount = noofphases;
                                            //                Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                            //                Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                            //                Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                            //                Internalauditscheduling.AuditID = AuditIDPhase1;
                                            //                Internalauditscheduling.Createdby = UserID;
                                            //                Internalauditscheduling.CreatedOn = DateTime.Now;
                                            //                if (processid != 0 && AuditIDPhase1 != 0)
                                            //                {
                                            //                    InternalAuditScheduleOnIDPhase1 = UserManagementRisk.AddDetailsInternalAuditSchedulingTable_ScheduleFirst(Internalauditscheduling);
                                            //                }
                                            //                AuditScheduling_SubProcessMapping.Process = processid;
                                            //                AuditScheduling_SubProcessMapping.AuditID = AuditIDPhase1;
                                            //                AuditScheduling_SubProcessMapping.SubProcessID = subprocessid;
                                            //                AuditScheduling_SubProcessMapping.CreatedBy = UserID;
                                            //                AuditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                            //                AuditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDPhase1;
                                            //                AuditScheduling_SubProcessMapping.IsActive = true;
                                            //                AuditScheduling_SubProcessMapping.IsDeleted = false;
                                            //                if (processid != 0 && AuditIDPhase1 != 0 && subprocessid != 0)
                                            //                {
                                            //                    UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(AuditScheduling_SubProcessMapping);
                                            //                }
                                            //                result = true;
                                            //            }
                                            //            else
                                            //            {
                                            //                AuditScheduling_SubProcessMapping.Process = processid;
                                            //                AuditScheduling_SubProcessMapping.AuditID = AuditIDPhase1;
                                            //                AuditScheduling_SubProcessMapping.SubProcessID = subprocessid;
                                            //                AuditScheduling_SubProcessMapping.CreatedBy = UserID;
                                            //                AuditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                            //                AuditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDPhase1;
                                            //                AuditScheduling_SubProcessMapping.IsActive = true;
                                            //                AuditScheduling_SubProcessMapping.IsDeleted = false;
                                            //                if (processid != 0 && AuditIDPhase1 != 0 && subprocessid != 0)
                                            //                {
                                            //                    UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(AuditScheduling_SubProcessMapping);
                                            //                }
                                            //                result = true;
                                            //            }
                                            //        }
                                            //        else
                                            //        {
                                            //            cvDuplicateEntry.IsValid = false;
                                            //            cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                            //            break;
                                            //        }
                                            //    }

                                            //    #endregion

                                            //    #region Phase 4
                                            //    if (period == "Phase4")
                                            //    {
                                            //        if (UserManagementRisk.IsExistAuditScheduling_ScheduleFirst(Convert.ToInt16(customerBranchId), Convert.ToInt16(VerticalId), processId, financialYear, AuditId, subProcessId, "Phase4", noofphases))
                                            //        {
                                            //            if (processid != OldProcessIDPhase2)
                                            //            {
                                            //                OldProcessIDPhase2 = Convert.ToInt32(lblProcessId.Text);

                                            //                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                            //                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                            //                Internalauditscheduling.FinancialYear = f1;
                                            //                Internalauditscheduling.TermName = "Phase4";
                                            //                Internalauditscheduling.TermStatus = true;
                                            //                Internalauditscheduling.Process = processid;
                                            //                Internalauditscheduling.ISAHQMP = "P";
                                            //                Internalauditscheduling.PhaseCount = noofphases;
                                            //                Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                            //                Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                            //                Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                            //                Internalauditscheduling.AuditID = AuditIDPhase2;
                                            //                Internalauditscheduling.Createdby = UserID;
                                            //                Internalauditscheduling.CreatedOn = DateTime.Now;
                                            //                if (processid != 0 && AuditIDPhase2 != 0)
                                            //                {
                                            //                    InternalAuditScheduleOnIDPhase2 = UserManagementRisk.AddDetailsInternalAuditSchedulingTable_ScheduleFirst(Internalauditscheduling);
                                            //                }
                                            //                AuditScheduling_SubProcessMapping.Process = processid;
                                            //                AuditScheduling_SubProcessMapping.AuditID = AuditIDPhase2;
                                            //                AuditScheduling_SubProcessMapping.SubProcessID = subprocessid;
                                            //                AuditScheduling_SubProcessMapping.CreatedBy = UserID;
                                            //                AuditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                            //                AuditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDPhase2;
                                            //                AuditScheduling_SubProcessMapping.IsActive = true;
                                            //                AuditScheduling_SubProcessMapping.IsDeleted = false;
                                            //                if (processid != 0 && AuditIDPhase2 != 0 && subprocessid != 0)
                                            //                {
                                            //                    UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(AuditScheduling_SubProcessMapping);
                                            //                }
                                            //                result = true;
                                            //            }
                                            //            else
                                            //            {
                                            //                AuditScheduling_SubProcessMapping.Process = processid;
                                            //                AuditScheduling_SubProcessMapping.AuditID = AuditIDPhase2;
                                            //                AuditScheduling_SubProcessMapping.SubProcessID = subprocessid;
                                            //                AuditScheduling_SubProcessMapping.CreatedBy = UserID;
                                            //                AuditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                            //                AuditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDPhase2;
                                            //                AuditScheduling_SubProcessMapping.IsActive = true;
                                            //                AuditScheduling_SubProcessMapping.IsDeleted = false;
                                            //                if (processid != 0 && AuditIDPhase2 != 0 && subprocessid != 0)
                                            //                {
                                            //                    UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(AuditScheduling_SubProcessMapping);
                                            //                }
                                            //                result = true;
                                            //            }
                                            //        }
                                            //    }
                                            //    else
                                            //    {
                                            //        cvDuplicateEntry.IsValid = false;
                                            //        cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                            //        break;
                                            //    }
                                            //}
                                            //#endregion

                                            //#region Phase 5
                                            //if (period == "Phase5")
                                            //{
                                            //    if (UserManagementRisk.IsExistAuditScheduling_ScheduleFirst(Convert.ToInt16(customerBranchId), Convert.ToInt16(VerticalId), processId, financialYear, AuditId, subProcessId, "Phase5", noofphases))
                                            //    {
                                            //        if (processid != OldProcessIDPhase3)
                                            //        {
                                            //            OldProcessIDPhase3 = Convert.ToInt32(lblProcessId.Text);

                                            //            InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                            //            Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                            //            Internalauditscheduling.FinancialYear = f1;
                                            //            Internalauditscheduling.TermName = "Phase5";
                                            //            Internalauditscheduling.TermStatus = true;
                                            //            Internalauditscheduling.Process = processid;
                                            //            Internalauditscheduling.ISAHQMP = "P";
                                            //            Internalauditscheduling.PhaseCount = noofphases;
                                            //            Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                            //            Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                            //            Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                            //            Internalauditscheduling.AuditID = AuditIDPhase3;
                                            //            Internalauditscheduling.Createdby = UserID;
                                            //            Internalauditscheduling.CreatedOn = DateTime.Now;
                                            //            if (processid != 0 && AuditIDPhase3 != 0)
                                            //            {
                                            //                InternalAuditScheduleOnIDPhase3 = UserManagementRisk.AddDetailsInternalAuditSchedulingTable_ScheduleFirst(Internalauditscheduling);
                                            //            }
                                            //            AuditScheduling_SubProcessMapping.Process = processid;
                                            //            AuditScheduling_SubProcessMapping.AuditID = AuditIDPhase3;
                                            //            AuditScheduling_SubProcessMapping.SubProcessID = subprocessid;
                                            //            AuditScheduling_SubProcessMapping.CreatedBy = UserID;
                                            //            AuditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                            //            AuditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDPhase3;
                                            //            AuditScheduling_SubProcessMapping.IsActive = true;
                                            //            AuditScheduling_SubProcessMapping.IsDeleted = false;
                                            //            if (processid != 0 && AuditIDPhase3 != 0 && subprocessid != 0)
                                            //            {
                                            //                UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(AuditScheduling_SubProcessMapping);
                                            //            }
                                            //            result = true;
                                            //        }
                                            //        else
                                            //        {
                                            //            AuditScheduling_SubProcessMapping.Process = processid;
                                            //            AuditScheduling_SubProcessMapping.AuditID = AuditIDPhase3;
                                            //            AuditScheduling_SubProcessMapping.SubProcessID = subprocessid;
                                            //            AuditScheduling_SubProcessMapping.CreatedBy = UserID;
                                            //            AuditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                            //            AuditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDPhase3;
                                            //            AuditScheduling_SubProcessMapping.IsActive = true;
                                            //            AuditScheduling_SubProcessMapping.IsDeleted = false;
                                            //            if (processid != 0 && AuditIDPhase3 != 0 && subprocessid != 0)
                                            //            {
                                            //                UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(AuditScheduling_SubProcessMapping);
                                            //            }
                                            //            result = true;
                                            //        }
                                            //    }
                                            //}
                                            //#endregion 
                                            //#endregion
                                            #endregion
                                            #endregion

                                            #region Unit Assignment
                                            //IsExistEntitiesAssignmentAuditManagerRisk() added by sagar more
                                            if (!AssignEntityAuditManager.IsExistEntitiesAssignmentAuditManagerRisk(processId, customerBranchId, Common.AuthenticationHelper.UserID, CustomerID))
                                            {
                                                List<EntitiesAssignmentAuditManagerRisk> EntitiesAssignmentAuditManagerRisklist = new List<EntitiesAssignmentAuditManagerRisk>();
                                                EntitiesAssignmentAuditManagerRisk objEntitiesAssignmentrisk = new EntitiesAssignmentAuditManagerRisk();
                                                objEntitiesAssignmentrisk.UserID = Common.AuthenticationHelper.UserID;
                                                objEntitiesAssignmentrisk.CustomerID = CustomerID;
                                                objEntitiesAssignmentrisk.BranchID = customerBranchId;
                                                objEntitiesAssignmentrisk.CreatedOn = DateTime.Today.Date;
                                                objEntitiesAssignmentrisk.ISACTIVE = true;
                                                objEntitiesAssignmentrisk.ProcessId = processId;
                                                objEntitiesAssignmentrisk.CretedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                EntitiesAssignmentAuditManagerRisklist.Add(objEntitiesAssignmentrisk);
                                                AssignEntityAuditManager.CreateEntitiesAssignmentAuditManagerRisklist(EntitiesAssignmentAuditManagerRisklist);
                                            }
                                            #endregion

                                            #region AuditAssignment
                                            if (!(RiskCategoryManagement.Exists_Internal_AuditAssignment(customerBranchId, VerticalId, CustomerID)))
                                            {
                                                Internal_AuditAssignment InA = new Internal_AuditAssignment();
                                                InA.AssignedTo = "I";
                                                InA.VerticalID = (int)VerticalId;
                                                InA.CustomerBranchid = customerBranchId;
                                                InA.ExternalAuditorId = -1;
                                                InA.CreatedBy = Common.AuthenticationHelper.UserID;
                                                InA.CreatedOn = DateTime.Now.Date;
                                                InA.IsActive = false;
                                                InA.CustomerId = (int)Common.AuthenticationHelper.CustomerID;
                                                entities.Internal_AuditAssignment.Add(InA);
                                                entities.SaveChanges();
                                            }
                                            #endregion

                                            #region Audit Kickoff
                                            List<InternalControlAuditAssignment> tempAssignmentList = new List<InternalControlAuditAssignment>();
                                            List<InternalAuditTransaction> tempTransactionList = new List<InternalAuditTransaction>();
                                            string listNotSaveList = string.Empty;
                                            List<int> distinctprocess = new List<int>();
                                            List<int> distinctprocessImp = new List<int>();

                                            string userName = string.Empty;
                                            userName = Portal.Common.AuthenticationHelper.User;

                                            tempAssignmentList.Clear();
                                            tempTransactionList.Clear();
                                            List<RiskActivityToBeDoneMapping> ActiveAuditStepIDList = new List<RiskActivityToBeDoneMapping>();
                                            ActiveAuditStepIDList.Clear();
                                            ActiveAuditStepIDList = UserManagementRisk.GetListOfActiveAuditStep(ActivityToBeDone, Convert.ToInt32(processId), Convert.ToInt32(subProcessId), Convert.ToInt32(customerBranchId));

                                            List<int> Auditstepdetails = new List<int>();                                            
                                            //Auditstepdetails = UserManagementRisk.GetAuditStepDetailsCountForHistoricalObservation(ActiveAuditStepIDList, Convert.ToInt32(customerBranchId), Convert.ToInt32(VerticalId), AuditId, Common.AuthenticationHelper.UserID, Convert.ToInt32(processId), Convert.ToInt32(subProcessId));
                                            
                                            if (ATBDIDS != 0)
                                            {
                                                Auditstepdetails.Add((int)ATBDIDS);
                                            }
                                            if (Auditstepdetails.Count > 0)
                                            {
                                                var ScheduledProcessList = UserManagementRisk.GetProcessListAuditScheduled(Convert.ToInt32(customerBranchId), Convert.ToInt32(VerticalId), financialYear, period, Convert.ToInt32(AuditId), Common.AuthenticationHelper.UserID);
                                                if (performerId != -1 && performerId != 0 && reviewerId != -1 && reviewerId != 0)
                                                {
                                                    string AssignedTo = "";
                                                    AssignedTo = "I";

                                                    if (ScheduledProcessList.Count > 0)
                                                    {
                                                        tempAssignmentList.Clear();
                                                        tempTransactionList.Clear();
                                                        InternalAuditInstance riskinstance = new InternalAuditInstance();
                                                        InternalAuditScheduleOn auditScheduleon = new InternalAuditScheduleOn();
                                                        InternalAuditTransaction transaction = new InternalAuditTransaction();

                                                        ScheduledProcessList.ForEach(EachProcess =>
                                                        {
                                                            #region Instance Save
                                                            riskinstance.CustomerBranchID = Convert.ToInt32(customerBranchId);
                                                            riskinstance.VerticalID = Convert.ToInt32(VerticalId);
                                                            riskinstance.ProcessId = EachProcess.Process;
                                                            riskinstance.SubProcessId = -1;
                                                            riskinstance.IsDeleted = false;
                                                            riskinstance.CreatedOn = DateTime.Now;
                                                            riskinstance.CreatedBy = Common.AuthenticationHelper.UserID;
                                                            riskinstance.AuditID = Convert.ToInt64(AuditId);

                                                            auditScheduleon.FinancialYear = financialYear;
                                                            auditScheduleon.ForMonth = period;
                                                            auditScheduleon.ProcessId = EachProcess.Process;
                                                            auditScheduleon.CreatedOn = DateTime.Now;
                                                            auditScheduleon.CreatedBy = Common.AuthenticationHelper.UserID;
                                                            auditScheduleon.AuditID = Convert.ToInt64(AuditId);

                                                            transaction.AuditScheduleOnID = auditScheduleon.ID;
                                                            transaction.CustomerBranchId = Convert.ToInt32(customerBranchId);
                                                            transaction.VerticalID = Convert.ToInt32(VerticalId);
                                                            transaction.ProcessId = EachProcess.Process;
                                                            transaction.SubProcessId = -1;
                                                            transaction.CreatedByText = userName;
                                                            transaction.StatusId = 1;
                                                            transaction.Remarks = "New Audit Steps Assigned.";
                                                            transaction.CreatedOn = DateTime.Now;
                                                            transaction.CreatedBy = Common.AuthenticationHelper.UserID;
                                                            transaction.AuditID = Convert.ToInt64(AuditId);

                                                            if (!distinctprocess.Contains((int)EachProcess.Process))
                                                            {
                                                                if (!UserManagementRisk.IsExistsInInternalAuditInstance(riskinstance.CustomerBranchID, riskinstance.ProcessId, Convert.ToInt32(riskinstance.SubProcessId), Convert.ToInt32(riskinstance.VerticalID), Convert.ToInt32(riskinstance.AuditID)))
                                                                {
                                                                    UserManagementRisk.AddDetailsInternalAuditInstances(riskinstance);
                                                                }
                                                                else
                                                                {
                                                                    InternalAuditInstance InternalAuditInstanceObject = UserManagementRisk.GetDetailsInternalAuditInstanceId(riskinstance.CustomerBranchID, riskinstance.ProcessId, Convert.ToInt32(riskinstance.SubProcessId), Convert.ToInt32(riskinstance.VerticalID), Convert.ToInt32(riskinstance.AuditID));
                                                                    if (InternalAuditInstanceObject != null)
                                                                    {
                                                                        riskinstance.ID = InternalAuditInstanceObject.ID;
                                                                        auditScheduleon.InternalAuditInstance = InternalAuditInstanceObject.ID;
                                                                        transaction.InternalAuditInstance = InternalAuditInstanceObject.ID;
                                                                    }
                                                                }
                                                                //UserManagementRisk.AddDetailsInternalAuditInstances(riskinstance);
                                                                auditScheduleon.InternalAuditInstance = riskinstance.ID;
                                                                transaction.InternalAuditInstance = riskinstance.ID;

                                                                if (!UserManagementRisk.IsExistsInInternalAuditScheduleOns(auditScheduleon.ProcessId, auditScheduleon.ForMonth, auditScheduleon.FinancialYear, Convert.ToInt32(auditScheduleon.AuditID)))
                                                                {
                                                                    UserManagementRisk.AddDetailsInternalAuditScheduleOn(auditScheduleon);
                                                                }
                                                                if (tempTransactionList.Count != 0)
                                                                {
                                                                    UserManagementRisk.CreateTransactionForHistroricalObservation(transaction);
                                                                }
                                                                distinctprocess.Add((int)EachProcess.Process);
                                                            }
                                                            #endregion
                                                            if (performerId != -1 && performerId != 0)
                                                            {
                                                                DateTime? dateAuditStartDate1 = null;
                                                                DateTime? dateAuditEndDate1 = null;

                                                                dateAuditStartDate1 = Convert.ToDateTime(AuditStartDate);
                                                                dateAuditEndDate1 = Convert.ToDateTime(AuditEndDate);

                                                                InternalControlAuditAssignment internalControlAuditAssignment = UserManagementRisk.GetAuditStartDateEndDateFromInternalControlAuditAssignment(3, Convert.ToInt32(performerId), EachProcess.Process, EachProcess.SubProcessID, Convert.ToInt32(customerBranchId), Convert.ToInt32(VerticalId), Convert.ToInt64(AuditId));
                                                                if (internalControlAuditAssignment != null)
                                                                {
                                                                    dateAuditStartDate1 = internalControlAuditAssignment.ExpectedStartDate;
                                                                    dateAuditEndDate1 = internalControlAuditAssignment.ExpectedEndDate;
                                                                }

                                                                InternalControlAuditAssignment TempAssP = new InternalControlAuditAssignment();
                                                                TempAssP.InternalAuditInstance = riskinstance.ID;
                                                                TempAssP.CustomerBranchID = Convert.ToInt32(customerBranchId);
                                                                TempAssP.VerticalID = Convert.ToInt32(VerticalId);
                                                                TempAssP.ProcessId = EachProcess.Process;
                                                                TempAssP.SubProcessId = EachProcess.SubProcessID;
                                                                TempAssP.RoleID = 3;
                                                                TempAssP.UserID = Convert.ToInt32(performerId);
                                                                TempAssP.IsActive = true;
                                                                TempAssP.ISInternalExternal = AssignedTo;
                                                                TempAssP.CreatedOn = DateTime.Now;
                                                                TempAssP.CreatedBy = Common.AuthenticationHelper.UserID;
                                                                TempAssP.AuditID = Convert.ToInt64(AuditId);
                                                                TempAssP.AuditPeriodStartdate = null;
                                                                TempAssP.AuditPeriodEnddate = null;
                                                                TempAssP.ExpectedStartDate = dateAuditStartDate1;
                                                                TempAssP.ExpectedEndDate = dateAuditEndDate1;
                                                                if (!UserManagementRisk.IsExistsInternalControlAuditAssignment(TempAssP))
                                                                {
                                                                    tempAssignmentList.Add(TempAssP);
                                                                }

                                                            }

                                                            if (reviewerId != -1 && reviewerId != 0)
                                                            {
                                                                DateTime? dateAuditStartDate2 = null;
                                                                DateTime? dateAuditEndDate2 = null;

                                                                dateAuditStartDate2 = Convert.ToDateTime(AuditStartDate);
                                                                dateAuditEndDate2 = Convert.ToDateTime(AuditEndDate);

                                                                InternalControlAuditAssignment internalControlAuditAssignment = UserManagementRisk.GetAuditStartDateEndDateFromInternalControlAuditAssignment(4, Convert.ToInt32(performerId), EachProcess.Process, EachProcess.SubProcessID, Convert.ToInt32(customerBranchId), Convert.ToInt32(VerticalId), Convert.ToInt64(AuditId));
                                                                if (internalControlAuditAssignment != null)
                                                                {
                                                                    dateAuditStartDate2 = internalControlAuditAssignment.ExpectedStartDate;
                                                                    dateAuditEndDate2 = internalControlAuditAssignment.ExpectedEndDate;
                                                                }

                                                                InternalControlAuditAssignment TempAssR = new InternalControlAuditAssignment();
                                                                TempAssR.InternalAuditInstance = riskinstance.ID;
                                                                TempAssR.CustomerBranchID = Convert.ToInt32(customerBranchId);
                                                                TempAssR.RoleID = 4;
                                                                TempAssR.UserID = Convert.ToInt32(reviewerId);
                                                                TempAssR.IsActive = true;
                                                                TempAssR.ProcessId = EachProcess.Process;
                                                                TempAssR.SubProcessId = EachProcess.SubProcessID;
                                                                TempAssR.ISInternalExternal = AssignedTo;
                                                                TempAssR.InternalAuditInstance = riskinstance.ID;
                                                                TempAssR.VerticalID = Convert.ToInt32(VerticalId);
                                                                TempAssR.CreatedOn = DateTime.Now;
                                                                TempAssR.CreatedBy = Common.AuthenticationHelper.UserID;
                                                                TempAssR.AuditID = Convert.ToInt64(AuditId);
                                                                TempAssR.AuditPeriodStartdate = null;
                                                                TempAssR.AuditPeriodEnddate = null;
                                                                TempAssR.ExpectedStartDate = dateAuditStartDate2;
                                                                TempAssR.ExpectedEndDate = dateAuditEndDate2;
                                                                if (!UserManagementRisk.IsExistsInternalControlAuditAssignment(TempAssR))
                                                                {
                                                                    tempAssignmentList.Add(TempAssR);
                                                                }
                                                            }
                                                            // Reviewer2 code
                                                            if (reviewer2Id != -1 && reviewer2Id != 0)
                                                            {
                                                                DateTime? dateAuditStartDate2 = null;
                                                                DateTime? dateAuditEndDate2 = null;

                                                                dateAuditStartDate2 = Convert.ToDateTime(AuditStartDate);
                                                                dateAuditEndDate2 = Convert.ToDateTime(AuditEndDate);

                                                                InternalControlAuditAssignment internalControlAuditAssignment = UserManagementRisk.GetAuditStartDateEndDateFromInternalControlAuditAssignment(5, Convert.ToInt32(performerId), EachProcess.Process, EachProcess.SubProcessID, Convert.ToInt32(customerBranchId), Convert.ToInt32(VerticalId), Convert.ToInt64(AuditId));
                                                                if (internalControlAuditAssignment != null)
                                                                {
                                                                    dateAuditStartDate2 = internalControlAuditAssignment.ExpectedStartDate;
                                                                    dateAuditEndDate2 = internalControlAuditAssignment.ExpectedEndDate;
                                                                }

                                                                InternalControlAuditAssignment TempAssR = new InternalControlAuditAssignment();
                                                                TempAssR.InternalAuditInstance = riskinstance.ID;
                                                                TempAssR.CustomerBranchID = Convert.ToInt32(customerBranchId);
                                                                TempAssR.RoleID = 5;
                                                                TempAssR.UserID = Convert.ToInt32(reviewer2Id);
                                                                TempAssR.IsActive = true;
                                                                TempAssR.ProcessId = EachProcess.Process;
                                                                TempAssR.SubProcessId = EachProcess.SubProcessID;
                                                                TempAssR.ISInternalExternal = AssignedTo;
                                                                TempAssR.InternalAuditInstance = riskinstance.ID;
                                                                TempAssR.VerticalID = Convert.ToInt32(VerticalId);
                                                                TempAssR.CreatedOn = DateTime.Now;
                                                                TempAssR.CreatedBy = Common.AuthenticationHelper.UserID;
                                                                TempAssR.AuditID = Convert.ToInt64(AuditId);
                                                                TempAssR.AuditPeriodStartdate = null;
                                                                TempAssR.AuditPeriodEnddate = null;
                                                                TempAssR.ExpectedStartDate = dateAuditStartDate2;
                                                                TempAssR.ExpectedEndDate = dateAuditEndDate2;
                                                                if (!UserManagementRisk.IsExistsInternalControlAuditAssignment(TempAssR))
                                                                {
                                                                    tempAssignmentList.Add(TempAssR);
                                                                }
                                                            }
                                                        });

                                                        if (tempAssignmentList.Count != 0)
                                                        {
                                                            UserManagementRisk.AddDetailsInternalControlAuditAssignment(tempAssignmentList);

                                                            //Save Total Active Ateps and Count 
                                                            //IF Kickoff new record then only step will add.
                                                            UserManagementRisk.SaveAuditStepsDetailsForHistoricalObservation(Auditstepdetails, CustomerID, Convert.ToInt32(customerBranchId), Convert.ToInt32(VerticalId), financialYear, period, Convert.ToInt64(AuditId));
                                                            // UserManagementRisk.SaveAuditStepsDetails(Auditstepdetails, CustomerID, Convert.ToInt32(customerBranchId), Convert.ToInt32(VerticalId), financialYear, period, Convert.ToInt64(AuditId));
                                                        }
                                                        successKickOff = true;
                                                    }
                                                }
                                                else
                                                {
                                                    cvDuplicateEntry.IsValid = false;
                                                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                                                    break;
                                                }

                                                #region Audit Implementation                                   
                                                if (performerId != 0 && performerId != -1)
                                                {
                                                    string AssignedToIMP = "";
                                                    AssignedToIMP = "I";
                                                    long UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                    if (ScheduledProcessList.Count > 0)
                                                    {
                                                        tempAssignmentList.Clear();
                                                        tempTransactionList.Clear();

                                                        ScheduledProcessList.ForEach(EachProcess =>
                                                        {
                                                            AuditImplementationInstance riskinstanceIMP = new AuditImplementationInstance();
                                                            riskinstanceIMP.CustomerBranchID = Convert.ToInt32(customerBranchId);
                                                            riskinstanceIMP.VerticalID = Convert.ToInt32(VerticalId);
                                                            riskinstanceIMP.ForPeriod = period;
                                                            riskinstanceIMP.ProcessId = EachProcess.Process;
                                                            riskinstanceIMP.SubProcessId = -1;
                                                            riskinstanceIMP.IsDeleted = false;
                                                            riskinstanceIMP.CreatedOn = DateTime.Now;
                                                            riskinstanceIMP.CreatedBy = Common.AuthenticationHelper.UserID;
                                                            riskinstanceIMP.AuditID = Convert.ToInt32(AuditId);

                                                            if (!distinctprocessImp.Contains((int)EachProcess.Process))
                                                            {
                                                                if (!UserManagementRisk.IsExistsAuditImplementationInstance(riskinstanceIMP))
                                                                {
                                                                    UserManagementRisk.AddDetailsAuditImplementationInstance(riskinstanceIMP);
                                                                    UserManagementRisk.CreateScheduleOnImplementation(riskinstanceIMP.Id, Portal.Common.AuthenticationHelper.UserID, Portal.Common.AuthenticationHelper.User,
                                                                    financialYear.Trim(), Convert.ToInt32(customerBranchId), period, Convert.ToInt32(VerticalId), Convert.ToInt32(EachProcess.Process), Convert.ToInt32(AuditId), -1, UserID);
                                                                    distinctprocessImp.Add(Convert.ToInt32(EachProcess.Process));
                                                                }
                                                            }

                                                            if (riskinstanceIMP.Id == 0)
                                                            {
                                                                AuditImplementationInstance AuditImplementationInstanceObject = UserManagementRisk.GetIdFromAuditImplementationInstance(riskinstanceIMP);
                                                                if (AuditImplementationInstanceObject != null)
                                                                {
                                                                    riskinstanceIMP.Id = AuditImplementationInstanceObject.Id;
                                                                }
                                                            }

                                                            List<AuditImplementationAssignment> Tempassignments = new List<AuditImplementationAssignment>();
                                                            {
                                                                if (performerId != 0 && performerId != -1)
                                                                {
                                                                    AuditImplementationAssignment TempAssP = new AuditImplementationAssignment();
                                                                    TempAssP.ImplementationInstance = riskinstanceIMP.Id;
                                                                    TempAssP.CustomerBranchID = Convert.ToInt32(customerBranchId);
                                                                    TempAssP.VerticalID = Convert.ToInt32(VerticalId);
                                                                    TempAssP.FinancialYear = financialYear;
                                                                    TempAssP.ForPeriod = period;
                                                                    TempAssP.ProcessId = EachProcess.Process;
                                                                    TempAssP.SubProcessId = EachProcess.SubProcessID;
                                                                    TempAssP.RoleID = RoleManagement.GetByCode("PERF").ID;
                                                                    TempAssP.UserID = performerId;
                                                                    TempAssP.IsActive = true;
                                                                    TempAssP.CreatedOn = DateTime.Now;
                                                                    TempAssP.CreatedBy = Common.AuthenticationHelper.UserID;
                                                                    TempAssP.AuditID = Convert.ToInt32(AuditId);
                                                                    if (!UserManagementRisk.IsExistAuditImplementationAssignment(TempAssP))
                                                                    {
                                                                        Tempassignments.Add(TempAssP);
                                                                    }
                                                                }

                                                                if (reviewerId != 0 && reviewerId != -1)
                                                                {
                                                                    AuditImplementationAssignment TempAssR = new AuditImplementationAssignment();
                                                                    TempAssR.CustomerBranchID = Convert.ToInt32(customerBranchId);
                                                                    TempAssR.RoleID = RoleManagement.GetByCode("RVW1").ID;
                                                                    TempAssR.UserID = reviewerId;
                                                                    TempAssR.IsActive = true;
                                                                    TempAssR.CreatedOn = DateTime.Now;
                                                                    TempAssR.ForPeriod = period;
                                                                    TempAssR.ImplementationInstance = riskinstanceIMP.Id;
                                                                    TempAssR.FinancialYear = financialYear;
                                                                    TempAssR.VerticalID = Convert.ToInt32(VerticalId);
                                                                    TempAssR.ProcessId = EachProcess.Process;
                                                                    TempAssR.SubProcessId = EachProcess.SubProcessID;
                                                                    TempAssR.AuditID = Convert.ToInt32(AuditId);
                                                                    TempAssR.CreatedBy = Common.AuthenticationHelper.UserID;
                                                                    if (!UserManagementRisk.IsExistAuditImplementationAssignment(TempAssR))
                                                                    {
                                                                        Tempassignments.Add(TempAssR);
                                                                    }
                                                                }
                                                                // Reviewer2 code
                                                                if (reviewer2Id != 0 && reviewer2Id != -1)
                                                                {
                                                                    AuditImplementationAssignment TempAssR = new AuditImplementationAssignment();
                                                                    TempAssR.CustomerBranchID = Convert.ToInt32(customerBranchId);
                                                                    TempAssR.RoleID = RoleManagement.GetByCode("RVW2").ID;
                                                                    TempAssR.UserID = Convert.ToInt32(reviewer2Id);
                                                                    TempAssR.IsActive = true;
                                                                    TempAssR.CreatedOn = DateTime.Now;
                                                                    TempAssR.ForPeriod = period;
                                                                    TempAssR.ImplementationInstance = riskinstanceIMP.Id;
                                                                    TempAssR.FinancialYear = financialYear;
                                                                    TempAssR.VerticalID = Convert.ToInt32(VerticalId);
                                                                    TempAssR.ProcessId = EachProcess.Process;
                                                                    TempAssR.SubProcessId = EachProcess.SubProcessID;
                                                                    TempAssR.AuditID = Convert.ToInt32(AuditId);
                                                                    TempAssR.CreatedBy = Common.AuthenticationHelper.UserID;
                                                                    if (!UserManagementRisk.IsExistAuditImplementationAssignment(TempAssR))
                                                                    {
                                                                        Tempassignments.Add(TempAssR);
                                                                    }
                                                                }

                                                                if (Tempassignments.Count != 0)
                                                                {
                                                                    UserManagementRisk.AddDetailsAuditImplementationAssignment(Tempassignments);
                                                                }
                                                            }
                                                        });                                                        
                                                    }
                                                }
                                                #endregion
                                            }
                                            #endregion

                                            #region Audit perform

                                            //var ATBDIDList = RiskCategoryManagement.GetRiskActivityToBeDoneMappingsList(ActivityToBeDone, Common.AuthenticationHelper.UserID, 3, financialYear, period, Convert.ToInt32(customerBranchId), Convert.ToInt32(VerticalId), Convert.ToInt32(AuditId), Convert.ToInt32(processId), Convert.ToInt32(subProcessId));

                                            //List<RiskActivityToBeDoneMapping> tempAtbIDs = RiskCategoryManagement.GetRiskActivityToBeDoneMappingsList(ActivityToBeDone, Common.AuthenticationHelper.UserID, 3, financialYear, period, Convert.ToInt32(customerBranchId), Convert.ToInt32(VerticalId), Convert.ToInt32(AuditId), Convert.ToInt32(processId), Convert.ToInt32(subProcessId));
                                            List<RiskActivityToBeDoneMapping> tempAtbIDs = RiskCategoryManagement.GetRiskActivityToBeDoneMappingsDetails(ATBDIDS);
                                            List<RiskActivityToBeDoneMapping> ATBDIDList = new List<RiskActivityToBeDoneMapping>();

                                            foreach (var item in tempAtbIDs)
                                            {
                                                if (item.CustomerBranchID == customerBranchId)
                                                {
                                                    ATBDIDList.Add(item);
                                                }
                                            }

                                            if (ATBDIDList.Count > 0)
                                            {
                                                int ObservationRatingId = -1;
                                                string ObserRating = ObservationRating.ToUpper();
                                                if (!String.IsNullOrEmpty(ObserRating))
                                                {
                                                    if (ObserRating == "MAJOR")
                                                    {
                                                        ObservationRatingId = 1;
                                                    }
                                                    if (ObserRating == "MODERATE")
                                                    {
                                                        ObservationRatingId = 2;
                                                    }
                                                    if (ObserRating == "MINOR")
                                                    {
                                                        ObservationRatingId = 3;
                                                    }
                                                }
                                                ObservationCategoryId = ObservationSubcategory.GetObservationCategoryByName(ObservationCategory, CustomerID);
                                                ObservationSubCategoryId = ObservationSubcategory.GetObservationSubCategoryByName(ObservationSubCategory, ObservationCategoryId, CustomerID);

                                                foreach (var ATBDID in ATBDIDList)
                                                {
                                                    //DateTime? dateTimeLine = DateTime.ParseExact(TimeLine, "MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                                    DateTime? dateTimeLine = null;

                                                    if (!string.IsNullOrEmpty(TimeLine))
                                                    {
                                                        DateTime? aaaa = CleanDateField(TimeLine);
                                                        DateTime dtt1 = Convert.ToDateTime(aaaa);
                                                        DateTime dtt2 = GetDate(dtt1.ToString("dd/MM/yyyy"));
                                                        TimeLine = Convert.ToString(dtt2.ToShortDateString());
                                                        dateTimeLine = Convert.ToDateTime(TimeLine);
                                                    }

                                                    //if (!string.IsNullOrEmpty(TimeLine))
                                                    //{
                                                    //    string tempDate = null;
                                                    //    string duplicateTime = TimeLine;
                                                    //    tempDate = DateTime.ParseExact(TimeLine, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                                    //    TimeLine = string.Empty;
                                                    //    TimeLine = tempDate;
                                                    //    dateTimeLine = Convert.ToDateTime(TimeLine);
                                                    //    TimeLine = duplicateTime;
                                                    //}

                                                    var getScheduleonDetails = RiskCategoryManagement.GetInternalAuditScheduleOnByDetails(Common.AuthenticationHelper.UserID, 3, financialYear, period, Convert.ToInt32(customerBranchId), Convert.ToInt32(ATBDID.ID), Convert.ToInt32(VerticalId), Convert.ToInt32(AuditId));
                                                    List<long> ProcessList = new List<long>();
                                                    var RoleListInAudit1 = CustomerManagementRisk.GetListOfRoleInAudit(Convert.ToInt32(AuditId), Portal.Common.AuthenticationHelper.UserID, ProcessList);
                                                    var RoleListInAudit = CustomerManagementRisk.GetAuditListRole(Convert.ToInt32(AuditId), ProcessList);

                                                    int StatusID = 3;
                                                    long roleid = 4;
                                                    //DateTime b = DateTime.ParseExact(DateTime.Now.ToString("dd-MM-yyyy"), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                                    DateTime b = DateTime.Now;
                                                    InternalControlAuditResult MstRiskResult = new InternalControlAuditResult()
                                                    {
                                                        AuditScheduleOnID = getScheduleonDetails.ID,
                                                        ProcessId = getScheduleonDetails.ProcessId,
                                                        FinancialYear = financialYear,
                                                        ForPerid = period,
                                                        CustomerBranchId = customerBranchId,
                                                        IsDeleted = false,
                                                        UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                                        ATBDId = Convert.ToInt32(ATBDID.ID),
                                                        RoleID = roleid,
                                                        InternalAuditInstance = getScheduleonDetails.InternalAuditInstance,
                                                        VerticalID = Convert.ToInt32(VerticalId),
                                                        AuditID = AuditId,
                                                        ISACPORMIS = 1,
                                                        AStatusId = 3,
                                                        AuditObjective = AuditMethodology,
                                                        AuditSteps = ActivityToBeDone,
                                                        AnalysisToBePerofrmed = "",
                                                        ProcessWalkthrough = ProcessWalkthrough,
                                                        ActivityToBeDone = ActualWorkDone,
                                                        Population = Population,
                                                        Sample = Sample,
                                                        ObservationNumber = "",
                                                        ObservationTitle = ObservationTitle,
                                                        Observation = Observation,
                                                        Risk = Risk,
                                                        RootCost = RootCause,
                                                        FinancialImpact = FinancialImpact,
                                                        Recomendation = Recomendation,
                                                        ManagementResponse = ManagementResponse,
                                                        TimeLine = dateTimeLine,
                                                        PersonResponsible = personResponsibleId,
                                                        ObservationRating = ObservationRatingId,
                                                        ObservationCategory = ObservationCategoryId,
                                                        FixRemark = Remark,
                                                        ObservationSubCategory = ObservationSubCategoryId,
                                                        Owner = ownerID,
                                                        Dated = DateTime.Now,
                                                        CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                                        CreatedOn = DateTime.Now,
                                                        UHPersonResponsible = null,
                                                        PRESIDENTPersonResponsible = null,
                                                        UHComment = "",
                                                        PRESIDENTComment = "",
                                                        BodyContent = "",
                                                        BodySubContent = "",
                                                        AuditeeResponse = "AC",
                                                        BriefObservation = BriefObservation,
                                                        DefeciencyType = null,
                                                        ObjBackground = ObservationBackgroud,
                                                        AnnexueTitle = "",

                                                    };
                                                    DateTime? statuschanged = GetDate(b.ToString("dd/MM/yyyy"));

                                                    InternalAuditTransaction transaction = new InternalAuditTransaction()
                                                    {
                                                        CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User,
                                                        StatusChangedOn = statuschanged,
                                                        AuditScheduleOnID = getScheduleonDetails.ID,
                                                        FinancialYear = financialYear,
                                                        CustomerBranchId = Convert.ToInt32(customerBranchId),
                                                        InternalAuditInstance = getScheduleonDetails.InternalAuditInstance,
                                                        ProcessId = getScheduleonDetails.ProcessId,
                                                        SubProcessId = -1,
                                                        ForPeriod = period,
                                                        ATBDId = Convert.ToInt32(ATBDID.ID),
                                                        RoleID = roleid,
                                                        UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                                        VerticalID = Convert.ToInt32(VerticalId),
                                                        AuditID = AuditId,
                                                        BodyContent = "",
                                                        UHComment = "",
                                                        BriefObservation = BriefObservation,
                                                        ObjBackground = ObservationBackgroud,
                                                        DefeciencyType = null,
                                                        PRESIDENTComment = "",
                                                        UHPersonResponsible = null,
                                                        PRESIDENTPersonResponsible = null,
                                                        StatusId = 3,
                                                        Remarks = "Audit Steps Closed.",
                                                        PersonResponsible = personResponsibleId,
                                                        Owner = ownerID,
                                                        ObservatioRating = ObservationRatingId,
                                                        ObservationCategory = ObservationCategoryId,
                                                        ObservationSubCategory = ObservationSubCategoryId,
                                                        ReviewerRiskRating = null,
                                                        CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                                        CreatedOn = DateTime.Now,
                                                    };

                                                    int ResultID = 0;
                                                    ResultID = RiskCategoryManagement.GetResultIdForHistroricalObservation(customerBranchId, period, financialYear, Convert.ToInt32(ATBDID.ID), Convert.ToInt32(VerticalId), AuditId);

                                                    ComplianceManagement.Business.DataRisk.AuditClosure AuditClosureResult = new ComplianceManagement.Business.DataRisk.AuditClosure()
                                                    {
                                                        ProcessId = getScheduleonDetails.ProcessId,
                                                        FinancialYear = financialYear,
                                                        ForPeriod = period,
                                                        CustomerbranchId = Convert.ToInt32(customerBranchId),
                                                        ACStatus = 1,
                                                        AuditCommiteRemark = "",
                                                        ATBDId = Convert.ToInt32(ATBDID.ID),
                                                        VerticalID = Convert.ToInt32(VerticalId),
                                                        AuditCommiteFlag = 0,
                                                        AuditID = AuditId,
                                                        ISACPORMIS = 1,
                                                        BodyContent = "",
                                                        AnnexueTitle = "",
                                                        UHComment = "",
                                                        BriefObservation = BriefObservation,
                                                        ObjBackground = ObservationBackgroud,
                                                        DefeciencyType = null,
                                                        PRESIDENTComment = "",
                                                        UHPersonResponsible = null,
                                                        PRESIDENTPersonResponsible = null,
                                                        ObservationNumber = "",
                                                        ObservationTitle = ObservationTitle,
                                                        Observation = Observation,
                                                        Risk = Risk,
                                                        RootCost = RootCause,
                                                        FinancialImpact = FinancialImpact,
                                                        Recomendation = Recomendation,
                                                        ManagementResponse = ManagementResponse,
                                                        TimeLine = dateTimeLine,
                                                        PersonResponsible = personResponsibleId,
                                                        Owner = ownerID,
                                                        ObservationRating = ObservationRatingId,
                                                        ObservationCategory = ObservationCategoryId,
                                                        ObservationSubCategory = ObservationSubCategoryId,
                                                        CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                                        CreatedOn = DateTime.Now
                                                    };
                                                    DateTime dt1 = new DateTime();
                                                    ObservationHistory objHistory = new ObservationHistory()
                                                    {
                                                        ATBTID = Convert.ToInt32(ATBDID.ID),
                                                        Observation = Observation,
                                                        UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                                        RoleID = roleid,
                                                        CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                                        CreatedOn = DateTime.Now,
                                                        AuditId = AuditId,
                                                        ISACPORMIS = 1,// need discuss with sushant
                                                        ProcessWalkthrough = ProcessWalkthrough,
                                                        ActualWorkDone = ActualWorkDone,
                                                        Population = Population,
                                                        Sample = Sample,
                                                        Remarks = Remark,
                                                        ObservationTitle = ObservationTitle,
                                                        Risk = Risk,
                                                        RootCause = RootCause,
                                                        FinancialImpact = FinancialImpact,
                                                        Recommendation = Recomendation,
                                                        ManagementResponse = ManagementResponse,
                                                        TimeLine = dateTimeLine,
                                                        BodyContent = "",
                                                        UHComment = "",
                                                        PRESIDENTComment = "",
                                                        UHPersonResponsible = null,
                                                        PRESIDENTPersonResponsible = null,
                                                        PersonResponsible = personResponsibleId,
                                                        Owner = ownerID,
                                                        ObservationRating = ObservationRatingId,
                                                        ObservationCategory = ObservationCategoryId,
                                                        ObservationSubCategory = ObservationSubCategoryId,
                                                    };


                                                    bool Success1 = RiskCategoryManagement.CreateInternalControlResult(MstRiskResult);
                                                    bool Success2 = false;
                                                    bool Flag = false;
                                                    if (Success1)
                                                    {
                                                        if (RiskCategoryManagement.InternalControlResultClosedCount(MstRiskResult) == 1)
                                                        {
                                                            #region  AuditClosure 
                                                            RiskCategoryManagement.AuditClosureDetailsExists(CustomerID, Convert.ToInt32(MstRiskResult.CustomerBranchId), (long)MstRiskResult.VerticalID, MstRiskResult.ForPerid, MstRiskResult.FinancialYear, Convert.ToInt32(AuditId));

                                                            using (AuditControlEntities entities1 = new AuditControlEntities())
                                                            {
                                                                var RecordtoUpdate = (from row in entities1.InternalControlAuditResults
                                                                                      where row.ProcessId == MstRiskResult.ProcessId
                                                                                      && row.FinancialYear == MstRiskResult.FinancialYear
                                                                                      && row.ForPerid == MstRiskResult.ForPerid
                                                                                      && row.CustomerBranchId == MstRiskResult.CustomerBranchId
                                                                                      && row.UserID == MstRiskResult.UserID
                                                                                      && row.RoleID == MstRiskResult.RoleID && row.ATBDId == MstRiskResult.ATBDId
                                                                                      && row.VerticalID == MstRiskResult.VerticalID
                                                                                      && row.AuditID == MstRiskResult.AuditID
                                                                                      select row.ID).OrderByDescending(x => x).FirstOrDefault();
                                                                if (RecordtoUpdate != null)
                                                                {
                                                                    AuditClosureResult.ResultID = RecordtoUpdate;
                                                                    RiskCategoryManagement.CreateAuditClosureResult(AuditClosureResult);
                                                                }
                                                            }
                                                            #endregion
                                                        }
                                                        else
                                                        {
                                                            //update PersonResponsible and Owner ***
                                                            
                                                            RiskCategoryManagement.UpdateAuditClosureResult(AuditClosureResult);
                                                        }
                                                    }

                                                    if (!string.IsNullOrEmpty(objHistory.Observation))
                                                    {
                                                        if (!RiskCategoryManagement.CheckExistObservationHistory(objHistory))
                                                        {
                                                            objHistory.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                            objHistory.CreatedOn = DateTime.Now;
                                                            RiskCategoryManagement.AddObservationHistory(objHistory);
                                                        }
                                                    }

                                                    if (!RiskCategoryManagement.InternalAuditTxnExists(transaction))
                                                    {
                                                        transaction.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                        transaction.CreatedOn = DateTime.Now;
                                                        Success2 = RiskCategoryManagement.CreateInternalAuditTxn(transaction);
                                                    }
                                                    else
                                                    {
                                                        //check Status Open or close update ***
                                                        RiskCategoryManagement.InternalAuditTxnUpdate(transaction);
                                                    }

                                                    InternalReviewHistory RH = new InternalReviewHistory()
                                                    {
                                                        ProcessId = Convert.ToInt32(getScheduleonDetails.ProcessId),
                                                        InternalAuditInstance = getScheduleonDetails.InternalAuditInstance,
                                                        CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                                        CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User,
                                                        Dated = DateTime.Now,
                                                        Remarks = Remark,
                                                        AuditScheduleOnID = getScheduleonDetails.ID,
                                                        FinancialYear = financialYear,
                                                        CustomerBranchId = Convert.ToInt32(customerBranchId),
                                                        ATBDId = Convert.ToInt32(ATBDID.ID),
                                                        FixRemark = Remark,
                                                        VerticalID = Convert.ToInt32(VerticalId),
                                                        AuditID = AuditId,
                                                        CreatedOn = DateTime.Now,
                                                    };
                                                    Flag = RiskCategoryManagement.CreateInternalReviewRemark(RH);

                                                    if (RiskCategoryManagement.CheckAllStepClosed(AuditId))
                                                    {
                                                        AuditClosureClose auditclosureclose = new AuditClosureClose();
                                                        auditclosureclose.CustomerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
                                                        auditclosureclose.CustomerBranchId = Convert.ToInt32(customerBranchId);
                                                        auditclosureclose.FinancialYear = financialYear;
                                                        auditclosureclose.ForPeriod = period;
                                                        auditclosureclose.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                        auditclosureclose.CreatedDate = DateTime.Today.Date;
                                                        auditclosureclose.ACCStatus = 1;
                                                        auditclosureclose.VerticalID = Convert.ToInt32(VerticalId);
                                                        auditclosureclose.AuditId = AuditId;
                                                        if (!RiskCategoryManagement.AuditClosureCloseExists(auditclosureclose.CustomerBranchId, (int)auditclosureclose.VerticalID, auditclosureclose.FinancialYear, auditclosureclose.ForPeriod, Convert.ToInt32(AuditId)))
                                                        {
                                                            RiskCategoryManagement.CreateAuditClosureClosed(auditclosureclose);
                                                        }
                                                    }
                                                    if (Success1 && Success2 && Flag)
                                                    {
                                                        successAuditPerform = true;
                                                    }
                                                }//ATBDIDList foreach loop
                                            }
                                            else
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                                                break;
                                            }
                                            #endregion

                                            #region Implementation
                                            bool implemenationSuccess = false;
                                            if (StatusOpenClose == "Closed" || StatusOpenClose == "closed" || StatusOpenClose == "Close" || StatusOpenClose == "close")
                                            {
                                                foreach (var ATBDID in ATBDIDList)
                                                {
                                                    int ScheduledOnID = 0;
                                                    ScheduledOnID = RiskCategoryManagement.GetScheduledOnIDForHistroricalObservation(customerBranchId, period, financialYear, Convert.ToInt32(ATBDID.ID), Convert.ToInt32(VerticalId), AuditId, Convert.ToInt32(ATBDID.ProcessId));

                                                    var getauditimplementationscheduledetails = RiskCategoryManagement.GetAuditImpementationScheduleOnDetailsNew(financialYear, period, Convert.ToInt32(customerBranchId), Convert.ToInt32(VerticalId), ScheduledOnID, Convert.ToInt32(AuditId));
                                                    bool implementationSuccess = false;
                                                    bool implementationSuccess2 = false;
                                                    bool implementationFlag = false;
                                                    int ResultID = 0;
                                                    ResultID = RiskCategoryManagement.GetResultIdForHistroricalObservation(customerBranchId, period, financialYear, Convert.ToInt32(ATBDID.ID), Convert.ToInt32(VerticalId), AuditId);
                                                    //DateTime b1 = DateTime.ParseExact(DateTime.Now.ToString("dd-MM-yyyy"), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                                    DateTime b1 = DateTime.Now;
                                                    ImplementationAuditResult MstRiskResult1 = new ImplementationAuditResult()
                                                    {
                                                        AuditScheduleOnID = getauditimplementationscheduledetails.Id,
                                                        FinancialYear = getauditimplementationscheduledetails.FinancialYear,
                                                        ForPeriod = getauditimplementationscheduledetails.ForMonth,
                                                        CustomerBranchId = Convert.ToInt32(customerBranchId),
                                                        IsDeleted = false,
                                                        UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                                        ResultID = ResultID,
                                                        RoleID = 4,
                                                        ImplementationInstance = getauditimplementationscheduledetails.ImplementationInstance,
                                                        VerticalID = Convert.ToInt32(VerticalId),
                                                        ProcessId = Convert.ToInt32(getauditimplementationscheduledetails.ProcessId),
                                                        AuditID = AuditId,
                                                        Status = 3,
                                                        ImplementationStatus = 5,
                                                    };
                                                    if (!RiskCategoryManagement.ImplementationAuditResultExists(MstRiskResult1))
                                                    {
                                                        implementationSuccess = RiskCategoryManagement.CreateImplementationAuditResult(MstRiskResult1);
                                                    }
                                                    AuditImplementationTransaction transaction1 = new AuditImplementationTransaction()
                                                    {
                                                        CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User,
                                                        StatusChangedOn = GetDate(b1.ToString("dd/MM/yyyy")),
                                                        ImplementationScheduleOnID = getauditimplementationscheduledetails.Id,
                                                        FinancialYear = financialYear,
                                                        CustomerBranchId = Convert.ToInt32(customerBranchId),
                                                        ImplementationInstance = getauditimplementationscheduledetails.ImplementationInstance,
                                                        ForPeriod = period,
                                                        ResultID = ResultID,
                                                        RoleID = 4,
                                                        UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                                        VerticalID = Convert.ToInt32(VerticalId),
                                                        ProcessId = Convert.ToInt32(getauditimplementationscheduledetails.ProcessId),
                                                        AuditID = AuditId,
                                                        AuditeeResponse = "AC",
                                                        StatusId = 3,
                                                        CreatedBy = Common.AuthenticationHelper.UserID,
                                                        ImplementationStatusId = 5,
                                                    };
                                                    if (!RiskCategoryManagement.AuditImplementationTransactionExists(transaction1))
                                                    {
                                                        implementationSuccess2 = RiskCategoryManagement.CreateAuditImplementationTransaction(transaction1);
                                                    }
                                                    int AuditteeResponse = CustomerManagementRisk.GetLatestAuditteeStatus(Convert.ToInt32(AuditId), ResultID);
                                                    var ResponstStatus = RiskCategoryManagement.GetLatestImplementationAuditteeResponse(ResultID, Convert.ToInt32(VerticalId), Convert.ToInt32(AuditId));
                                                    string remark = string.Empty;


                                                    ImplementationReviewHistory RH1 = new ImplementationReviewHistory()
                                                    {
                                                        ForMonth = Convert.ToString(getauditimplementationscheduledetails.ForMonth),
                                                        ImplementationInstance = getauditimplementationscheduledetails.ImplementationInstance,
                                                        CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                                        CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User,
                                                        Dated = DateTime.Now,
                                                        Remarks = "NA",
                                                        ImplementationScheduleOnID = getauditimplementationscheduledetails.Id,
                                                        FinancialYear = financialYear,
                                                        CustomerBranchId = customerBranchId,
                                                        ResultID = ResultID,
                                                        FixRemark = "NA",
                                                        VerticalID = Convert.ToInt32(VerticalId),
                                                        ProcessId = Convert.ToInt32(getauditimplementationscheduledetails.ProcessId),
                                                        AuditID = AuditId,
                                                    };
                                                    implementationFlag = RiskCategoryManagement.CreateImplementationReviewHistoryRemark(RH1);
                                                    if (implementationSuccess && implementationSuccess2 && implementationFlag)
                                                    {
                                                        successImplemenation = true;
                                                    }
                                                }
                                            }
                                            #endregion

                                            finalSuccess = true;
                                        }
                                        else
                                        {
                                            cvDuplicateEntry.IsValid = false;
                                            cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                                            break;
                                        }
                                    }                                    
                                }
                                #endregion
                                #region Code added by Sushant for Process Name/SubProcess
                                if (AuditList.Count > 0)
                                {
                                    var x = (from y in entities.ProcessCommaSaperates
                                             where AuditList.Contains(y.AuditID)
                                             select y).ToList();

                                    x.ForEach(eachRecord =>
                                    {
                                        entities.ProcessCommaSaperates.Remove(eachRecord);
                                        entities.SaveChanges();
                                    });

                                    var xy = (from y in entities.SubProcessCommaSeparates
                                             where AuditList.Contains(y.AuditID)
                                             select y).ToList();

                                    xy.ForEach(eachRecord =>
                                    {
                                        entities.SubProcessCommaSeparates.Remove(eachRecord);
                                        entities.SaveChanges();
                                    });
                                }
                                var DistinctAuditID = (from row in entities.ProcessCommaSaperates
                                                       select row.AuditID).Distinct().ToList();

                                var SubProcessAuditList = (from row in entities.SubProcessCommaSeparates
                                                           select row.AuditID).Distinct().ToList();

                                var CheckAuditID = (from row in entities.AuditClosureDetails
                                                    where !DistinctAuditID.Contains(row.ID)
                                                    select row.ID).Distinct().ToList();

                                var CheckAuditIDSubProcess = (from row in entities.AuditClosureDetails
                                                              where !SubProcessAuditList.Contains(row.ID)
                                                              select row.ID).Distinct().ToList();

                                foreach (var item in CheckAuditID)
                                {
                                    var ProcessList = (from row in entities.InternalControlAuditAssignments
                                                       join row1 in entities.Mst_Process
                                                       on row.ProcessId equals row1.Id
                                                       join RATDM in entities.RiskActivityToBeDoneMappings
                                                       on row.CustomerBranchID equals RATDM.CustomerBranchID
                                                       where row.AuditID == item
                                                       && row.ProcessId == RATDM.ProcessId
                                                       && row.IsActive == true
                                                       && RATDM.RCMType != "IFC"
                                                       select row1.Name).Distinct().ToList();

                                    var ProcessNameComaSaperate = string.Empty;
                                    foreach (var item1 in ProcessList)
                                    {
                                        ProcessNameComaSaperate += item1 + ",";
                                    }
                                    ProcessNameComaSaperate = ProcessNameComaSaperate.TrimEnd(',');
                                    ProcessNameComaSaperate = ProcessNameComaSaperate.TrimStart(',');

                                    if (!string.IsNullOrEmpty(ProcessNameComaSaperate))
                                    {
                                        ProcessCommaSaperate objProcess = new ProcessCommaSaperate();
                                        objProcess.AuditID = item;
                                        objProcess.ProcessName = ProcessNameComaSaperate;
                                        objProcess.CreatedOn = DateTime.Now;
                                        entities.ProcessCommaSaperates.Add(objProcess);
                                        entities.SaveChanges();
                                    }
                                }

                                foreach (var item in CheckAuditIDSubProcess)
                                {
                                    var listOfSP = (from row in entities.InternalControlAuditAssignments
                                                          join row1 in entities.mst_Subprocess
                                                          on row.SubProcessId equals row1.Id
                                                          join RATDM in entities.RiskActivityToBeDoneMappings
                                                          on row.CustomerBranchID equals RATDM.CustomerBranchID
                                                          where row.AuditID == item
                                                          && RATDM.ProcessId == row.ProcessId
                                                          && RATDM.SubProcessId == row.SubProcessId
                                                          && row.IsActive == true
                                                          && RATDM.RCMType != "IFC"
                                                          select row1).Distinct().ToList();

                                    var SubProcessList = listOfSP.Select(entry => entry.Name).ToList();
                                    
                                    var SubProcessNameComaSeparate = string.Empty;
                                    foreach (var item1 in SubProcessList)
                                    {
                                        SubProcessNameComaSeparate += item1 + ",";
                                    }

                                    SubProcessNameComaSeparate = SubProcessNameComaSeparate.TrimEnd(',');
                                    SubProcessNameComaSeparate = SubProcessNameComaSeparate.TrimStart(',');
                                    if (!string.IsNullOrEmpty(SubProcessNameComaSeparate))
                                    {
                                        SubProcessCommaSeparate objSubProcess = new SubProcessCommaSeparate();
                                        objSubProcess.AuditID = item;
                                        objSubProcess.SubProcessName = SubProcessNameComaSeparate;
                                        objSubProcess.CreatedOn = DateTime.Now;
                                        entities.SubProcessCommaSeparates.Add(objSubProcess);
                                        entities.SaveChanges();
                                    }
                                }
                                #endregion
                            }
                            if (finalSuccess)
                            {
                                DateTime? aaaa = CleanDateField(Convert.ToString(DateTime.Now.ToShortDateString()));
                                DateTime fileUploadedDate1 = Convert.ToDateTime(aaaa);
                                DateTime fileUploadedDate2 = GetDate(fileUploadedDate1.ToString("dd/MM/yyyy"));
                                string fileUploadedDate = Convert.ToString(fileUploadedDate2.ToShortDateString());
                                HistoricalObservationUploadedHistory objectHistoricalObservationUploadedHistory = new HistoricalObservationUploadedHistory()
                                {
                                    HistoricalObservationFileName = Session["historicalObservationFileName"].ToString(),
                                    HistoricalObservationUploadedDate = fileUploadedDate,
                                    HistoricalObservationFilePath = Session["historicalObservationFilePath"].ToString(),
                                    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID),
                                    IsActive = true,
                                    CreatedOn = DateTime.Now,
                                    CreatedBy = Portal.Common.AuthenticationHelper.UserID
                                };
                                bool SaveHistoricalObservationUploadedHistorySuccess = false;
                                if (objectHistoricalObservationUploadedHistory != null)
                                {
                                    SaveHistoricalObservationUploadedHistorySuccess = SaveHistoricalObservationUploadedHistory(objectHistoricalObservationUploadedHistory);
                                }

                                if (SaveHistoricalObservationUploadedHistorySuccess)
                                {
                                    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                                    BindUploadedHistoryGridView(CustomerId);
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Uploaded Successfully.";
                                }
                            }
                            //else
                            //{
                            //    cvDuplicateEntry.IsValid = false;
                            //    cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled.";
                            //    break;
                            //}
                        }
                        else
                        {
                            errorMessage.Add("Sheet cannot be empty");
                            ErrorMessages(errorMessage);
                            ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 0, function () { });", true);
                            sucess = false;
                        }

                    } //if loop end
                    #endregion

                    #endregion

                }
            }
            catch (Exception ex)
            {
                sucess = false;
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private int GetDimensionRows(ExcelWorksheet sheet)
        {
            var startRow = sheet.Dimension.Start.Row;
            var endRow = sheet.Dimension.End.Row;
            return endRow - startRow;
        }

        public static void Insertmst_Activity(mst_Activity mstactivity)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                entities.mst_Activity.Add(mstactivity);
                entities.SaveChanges();
            }
        }

        public static bool IsExistsmst_Activity(long processId, long subProcessId, string activityName)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from cs in entities.mst_Activity
                             where cs.Name == activityName && cs.ProcessId == processId && cs.SubProcessId == subProcessId
                             select cs).FirstOrDefault();
                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static bool AssertionExists(string CategoryName)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_Assertions
                             where row.Name.Equals(CategoryName)
                             select row);
                return query.Select(entry => true).SingleOrDefault();
            }
        }
        public static void AssertionCreate(mst_Assertions Data)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                entities.mst_Assertions.Add(Data);
                entities.SaveChanges();
            }
        }
        protected bool CheckDate(String date)
        {
            try
            {
                DateTime dt = DateTime.Parse(date);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public static int GetRiskControlRatingID(string Flag, string Name)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var transactionsQuery = (from row in entities.mst_Risk_ControlRating
                                         where row.Name.ToUpper() == Name.ToUpper() && row.IsActive == false
                                         && row.IsRiskControl == Flag
                                         select row.Value).FirstOrDefault();
                return transactionsQuery;
            }
        }
        public void ErrorMessages(List<string> emsg)
        {
            string finalErrMsg = string.Empty;

            finalErrMsg += "<ol type='1'>";

            if (emsg.Count > 0)
            {
                emsg.ForEach(eachErrMsg =>
                {
                    finalErrMsg += "<li>" + eachErrMsg + "</li>";
                });

                finalErrMsg += "</ol>";
            }

            cvDuplicateEntry.IsValid = false;
            cvDuplicateEntry.ErrorMessage = finalErrMsg;
        }
        public static string GetSafeTagName(string tag)
        {
            tag = tag.Replace("'", "`")
            .Replace('"', '`')
            .Replace('‐', '_');
            tag = Regex.Replace(tag, @"\s+", " "); //multiple spaces with single spaces
            return tag;
        }
        public static DateTime? CleanDateField(string DateField)
        {
            // Convert the text to DateTime and return the value or null
            DateTime? CleanDate = new DateTime();
            int intDate;
            bool DateIsInt = int.TryParse(DateField, out intDate);
            if (DateIsInt)
            {
                // If this is a serial date, convert it
                CleanDate = DateTime.FromOADate(intDate);
            }
            else if (DateField.Length != 0 && DateField != "1/1/0001 12:00:00 AM" &&
                DateField != "1/1/1753 12:00:00 AM")
            {
                // Convert from a General format
                CleanDate = (Convert.ToDateTime(DateField));
            }
            else
            {
                // Date is blank
                CleanDate = null;
            }
            return CleanDate;
        }

        public DateTime GetDate(string date)
        {
            string date1 = "";
            if (date.Contains("/"))
            {
                date1 = date.Trim().Substring(date.IndexOf("/") + 4, 4) + "-" + date.Substring(date.IndexOf("/") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains("-"))
            {
                date1 = date.Substring(date.IndexOf("-") + 4, 4) + "-" + date.Substring(date.IndexOf("-") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains(" "))
            {
                date1 = date.Substring(date.IndexOf(" ") + 4, 4) + "-" + date.Substring(date.IndexOf(" ") + 1, 2) + "-" + date.Substring(0, 2);
            }
            return Convert.ToDateTime(date1);
        }

        public static bool ExistsNew(mst_RCMMaster objmaster)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_RCMMaster
                             where
                                row.ObjectiveRef.ToUpper().Trim() == objmaster.ObjectiveRef.ToUpper().Trim()
                                && row.Processid == objmaster.Processid
                                && row.SubProcessid == objmaster.SubProcessid
                                && row.ActivityId == objmaster.ActivityId
                                && row.RiskDesc.ToUpper().Trim() == objmaster.RiskDesc.ToUpper().Trim()
                                && row.ControlObjective.ToUpper().Trim() == objmaster.ControlObjective.ToUpper().Trim()
                                && row.Controldescription.ToUpper().Trim() == objmaster.Controldescription.ToUpper().Trim()
                                && row.RiskCategory.ToUpper().Trim() == objmaster.RiskCategory.ToUpper().Trim()
                                && row.FinancialAssertion.ToUpper().Trim() == objmaster.FinancialAssertion.ToUpper().Trim()
                                && row.MCDescription.ToUpper().Trim() == objmaster.MCDescription.ToUpper().Trim()
                                && row.ProcessOwnerID == objmaster.ProcessOwnerID
                                && row.ControlOwnerID == objmaster.ControlOwnerID
                                && row.EffectiveDate == objmaster.EffectiveDate
                                && row.KC1 == objmaster.KC1
                                && row.KC2 == objmaster.KC2
                                && row.PrimarySecondary == objmaster.PrimarySecondary
                                && row.PreventiveDetective == objmaster.PreventiveDetective
                                && row.AutomatedManual == objmaster.AutomatedManual
                                && row.Frequency == objmaster.Frequency
                                && row.IPE.ToUpper().Trim() == objmaster.IPE.ToUpper().Trim()
                                && row.ERPSystem.ToUpper().Trim() == objmaster.ERPSystem.ToUpper().Trim()
                                && row.FraudRiskIndecator.ToUpper().Trim() == objmaster.FraudRiskIndecator.ToUpper().Trim()
                                && row.UniqueReferred.ToUpper().Trim() == objmaster.UniqueReferred.ToUpper().Trim()
                                && row.TestStrategy.ToUpper().Trim() == objmaster.TestStrategy.ToUpper().Trim()
                                && row.DocumentsExamined.ToUpper().Trim() == objmaster.DocumentsExamined.ToUpper().Trim()
                                && row.Riskrating == objmaster.Riskrating
                                && row.ControlRating == objmaster.ControlRating
                                && row.AuditObjective.ToUpper().Trim() == objmaster.AuditObjective.ToUpper().Trim()
                                && row.AuditStep.ToUpper().Trim() == objmaster.AuditStep.ToUpper().Trim()
                                && row.AnalysistobePerformed.ToUpper().Trim() == objmaster.AnalysistobePerformed.ToUpper().Trim()
                                && row.PreRequisiteList.ToUpper().Trim() == objmaster.PreRequisiteList.ToUpper().Trim()
                                && row.BranchList.ToUpper().Trim() == objmaster.BranchList.ToUpper().Trim()
                                && row.VerticalList.ToUpper().Trim() == objmaster.VerticalList.ToUpper().Trim()
                             select row).FirstOrDefault();

                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public static bool CreateRiskCategoryCreation(RiskCategoryCreation RCC)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    entities.Database.CommandTimeout = 300;
                    entities.RiskCategoryCreations.Add(RCC);
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool CreateRiskActivityTransaction(RiskActivityTransaction RAT)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    entities.Database.CommandTimeout = 300;
                    entities.RiskActivityTransactions.Add(RAT);
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool PrerequsiteExistsNew(StepChecklistMapping objmaster)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.StepChecklistMappings
                             where
                                row.ChecklistDocument.ToUpper().Trim() == objmaster.ChecklistDocument.ToUpper().Trim()
                                && row.ATBDID == objmaster.ATBDID
                                && row.IsActive == objmaster.IsActive
                             select row).FirstOrDefault();

                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public static bool RCMMasterUpdate(long rcmID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_RCMMaster
                             where row.ID == rcmID
                             select row).FirstOrDefault();


                if (query != null)
                {
                    query.IsProcessed = true;
                    entities.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public static bool AssertionsMappingExists(long Assid, long Riskid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.AssertionsMappings
                             where row.RiskCategoryCreationId == Riskid && row.AssertionId == Assid
                             select row).FirstOrDefault();
                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        protected void rdoCompliance_CheckedChanged(object sender, EventArgs e)
        {
            if (rdoRCMUpload.Checked == true)
                fileUploadRCM.Visible = true;
            BindLocationFilter();

            Divbranchlist.Style.Add("display", "block");
            BindddlVerticalBranch();
        }
        private void BindddlVerticalBranch()
        {
            if (ddlVerticalBranch.SelectedValue != null)
            {
                long customerID = -1;
                customerID = Common.AuthenticationHelper.CustomerID;
                ddlVerticalBranch.DataTextField = "Name";
                ddlVerticalBranch.DataValueField = "ID";
                ddlVerticalBranch.DataSource = UserManagementRisk.GetVerticalBranchList(customerID);
                ddlVerticalBranch.DataBind();
            }
        }

        protected void rdoSubProcess_CheckedChanged(object sender, EventArgs e)
        {
            if (rdoSubProcess.Checked == true)
                fileUploadRCM.Visible = true;
            Divbranchlist.Style.Add("display", "none");
        }


        public long GetActivityId_mst_Activity(long processId, long subProcessId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from cs in entities.mst_Activity
                             where cs.ProcessId == processId && cs.SubProcessId == subProcessId
                             select cs).FirstOrDefault();
                if (query != null)
                {
                    return query.Id;
                }
                else
                {
                    return 0;
                }
            }
        }

        public static bool SaveHistoricalObservationUploadedHistory(HistoricalObservationUploadedHistory historicalObservationUploadedHistory)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                int SavedRecord = 0;

                entities.HistoricalObservationUploadedHistories.Add(historicalObservationUploadedHistory);
                SavedRecord = entities.SaveChanges();
                if (SavedRecord != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
        }

        public void BindUploadedHistoryGridView(int CustomerId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from cs in entities.HistoricalObservationUploadedHistories
                             where cs.CustomerId == CustomerId && cs.IsActive == true
                             select cs).ToList();

                if (query.Count > 0)
                {
                    grdUploadedHistrory.DataSource = query;
                    Session["TotalRows"] = query.Count;
                    grdUploadedHistrory.DataBind();
                }
                else
                {
                    grdUploadedHistrory.DataSource = null;
                    Session["TotalRows"] = 0;
                    grdUploadedHistrory.DataBind();
                }
            }
        }


        protected void grdUploadedHistrory_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("ViewAuditStatusSummary"))
                {

                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    int UploadId = Convert.ToInt32(commandArgs[0]);
                   // string fileName = Convert.ToString(commandArgs[1]);
                    string filePath = Convert.ToString(commandArgs[3]);
                    string[] filepath = filePath.Split('/');
                    string foldername = filepath[1].ToString();
                    string fileName = filepath[2].ToString();
                    ViewState["UploadId"] = UploadId;
                    string hpath = "~/HistoricalObsUploaded/" + fileName;
                    //Response.Clear();
                    //Response.Buffer = true;
                    //Response.ContentType = "application/vnd.ms-excel";
                    //// to open file prompt Box open or Save file  
                    //Response.AddHeader("content-disposition", "attachment;filename=" + dr["Name"].ToString());
                    //Response.Charset = "";
                    //Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    //Response.BinaryWrite((byte[])dr["data"]);
                    //Response.End();


                    #region Word Download Code
                    Response.AppendHeader("Content-Type", "image/jpg");
                    Response.AppendHeader("Content-disposition", "attachment; filename=\"" + hpath + "\"");
                    Response.TransmitFile(Server.MapPath(hpath));
                    Response.End();
                    #endregion
                }
            }
            catch (Exception ee)
            {
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "There is no file on current location.";
            }
        }


        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdUploadedHistrory.PageIndex = chkSelectedPage - 1;

            grdUploadedHistrory.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);

            CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            BindUploadedHistoryGridView(CustomerId);
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdUploadedHistrory.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);

                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                BindUploadedHistoryGridView(CustomerId);
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdUploadedHistrory.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
            }
            catch (Exception ex)
            {
            }
        }

        private bool CheckDateFormat(string inputDate)
        {
            string str = string.Empty;
            string dateResult = string.Empty;
            string monthResult = string.Empty;
            string FYResult = string.Empty;
            int FYcount = 0;
            int datecount = 0;

            if (!inputDate.Contains("-"))
            {
                str = "Invalid";
                return false;
            }
            else
            {
                str = "Valid";
                string[] dateFormat = inputDate.Split('-');
                string date = dateFormat[0].ToString();
                string month = dateFormat[1].ToString();
                string year = dateFormat[2].ToString();
                string monthInUpperCase = month.Trim().ToUpper();

                foreach (char c1 in date)
                {
                    datecount++;
                }
                if (datecount < 1 || datecount > 2)
                {
                    dateResult = "Invalid";
                }
                else
                {
                    dateResult = "Valid";
                }

                #region check month here
                switch (monthInUpperCase)
                {
                    case "APR":
                        monthResult = "Valid";
                        break;
                    case "MAY":
                        monthResult = "Valid";
                        break;
                    case "JUN":
                        monthResult = "Valid";
                        break;
                    case "JUL":
                        monthResult = "Valid";
                        break;
                    case "AUG":
                        monthResult = "Valid";
                        break;
                    case "SEP":
                        monthResult = "Valid";
                        break;
                    case "OCT":
                        monthResult = "Valid";
                        break;
                    case "NOV":
                        monthResult = "Valid";
                        break;
                    case "DEC":
                        monthResult = "Valid";
                        break;
                    case "JAN":
                        monthResult = "Valid";
                        break;
                    case "FEB":
                        monthResult = "Valid";
                        break;
                    case "MAR":
                        monthResult = "Valid";
                        break;
                    default:
                        monthResult = "Invalid";
                        break;
                }
                #endregion

                #region check Financial Year here
                foreach (char c1 in year)
                {
                    FYcount++;
                }
                if (FYcount < 4 || FYcount > 4)
                {
                    FYResult = "Invalid";
                }
                else
                {
                    FYResult = "Valid";
                }
                if (FYcount == 2)
                {
                    FYResult = "Valid";
                }
                #endregion
            }


            if (monthResult == "Valid" && dateResult == "Valid" && FYResult == "Valid")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private string GetPeriod(string Period)
        {
            Period = Period.Trim().ToUpper();

            if (Period == "ANNUALLY")
            {
                Period = "Annually";
            }
            else if (Period == "APR-SEP")
            {
                Period = "Apr-Sep";
            }
            else if (Period == "OCT-MAR")
            {
                Period = "Oct-Mar";
            }
            else if (Period == "APR-JUN")
            {
                Period = "Apr-Jun";
            }
            else if (Period == "JUL-SEP")
            {
                Period = "Jul-Sep";
            }
            else if (Period == "OCT-DEC")
            {
                Period = "Oct-Dec";
            }
            else if (Period == "JAN-MAR")
            {
                Period = "Jan-Mar";
            }
            else if (Period == "APR")
            {
                Period = "Apr";
            }
            else if (Period == "MAY")
            {
                Period = "May";
            }
            else if (Period == "JUN")
            {
                Period = "Jun";
            }
            else if (Period == "JUL")
            {
                Period = "Jul";
            }
            else if (Period == "AUG")
            {
                Period = "Aug";
            }
            else if (Period == "SEP")
            {
                Period = "Sep";
            }
            else if (Period == "OCT")
            {
                Period = "Oct";
            }
            else if (Period == "NOV")
            {
                Period = "Nov";
            }
            else if (Period == "DEC")
            {
                Period = "Dec";
            }
            else if (Period == "JAN")
            {
                Period = "Jan";
            }
            else if (Period == "FEB")
            {
                Period = "Feb";
            }
            else if (Period == "MAR")
            {
                Period = "Mar";
            }
            else if (Period == "APRIL")
            {
                Period = "April";
            }
            else if (Period == "MAY")
            {
                Period = "May";
            }
            else if (Period == "JUNE")
            {
                Period = "June";
            }
            else if (Period == "JULY")
            {
                Period = "July";
            }
            else if (Period == "AUGUST")
            {
                Period = "August";
            }
            else if (Period == "SEPTEMBER")
            {
                Period = "September";
            }
            else if (Period == "OCTOBER")
            {
                Period = "October";
            }
            else if (Period == "NOVEMBER")
            {
                Period = "November";
            }
            else if (Period == "DECEMBER")
            {
                Period = "December";
            }
            else if (Period == "JANUARY")
            {
                Period = "January";
            }
            else if (Period == "FEBRUARY")
            {
                Period = "February";
            }
            else if (Period == "MARCH")
            {
                Period = "March";
            }
            return Period;
        }
    }
}