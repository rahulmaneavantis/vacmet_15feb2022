﻿
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Account
{
    public partial class EventAssignmentUpload : System.Web.UI.Page
    {
        bool suucess = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                lblMessage.Text = string.Empty;
                LblErormessage.Text = string.Empty;
                BindCustomers();
            }
        }
        protected void upComplianceDetails_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "Script", "initializeCombobox();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindCustomers()
        {
            try
            {
                int UserID = AuthenticationHelper.UserID;
                var customerdata = GetAllCustomer(UserID, "");

                ddlCustomerList.DataTextField = "Name";
                ddlCustomerList.DataValueField = "ID";

                ddlCustomerList.DataSource = customerdata;
                ddlCustomerList.DataBind();

                ddlCustomerList.Items.Insert(0, new ListItem("< Select Customer >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static object GetAllCustomer(int userID, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var users = (from row in entities.CustomerAssignmentDetails
                             join row1 in entities.Customers
                             on row.CustomerID equals row1.ID

                             where row.IsDeleted == false
                             && row.UserID == userID
                             && row1.IsDeleted == false
                             && row1.ComplianceProductType != 1
                             && row1.Status == 1
                             select row1);

                if (!string.IsNullOrEmpty(filter))
                {
                    users = users.Where(entry => entry.Name.Contains(filter) || entry.BuyerName.Contains(filter) || entry.BuyerEmail.Contains(filter) || entry.BuyerContactNumber.Contains(filter));
                }

                return users.OrderBy(entry => entry.Name).Distinct().ToList();
            }
        }
        protected void btnUploadFile_Click(object sender, EventArgs e)
        {
            if (MasterFileUpload.HasFile)
            {
                try
                {
                    if (!string.IsNullOrEmpty(ddlCustomerList.SelectedValue))
                    {
                        if (ddlCustomerList.SelectedValue != "-1")
                        {
                            string filename = Path.GetFileName(MasterFileUpload.FileName);
                            MasterFileUpload.SaveAs(Server.MapPath("~/Uploaded/") + filename.Trim());
                            FileInfo excelfile = new FileInfo(Server.MapPath("~/Uploaded/") + filename.Trim());
                            if (excelfile != null)
                            {
                                using (ExcelPackage xlWorkbook = new ExcelPackage(excelfile))
                                {
                                    if (rdoAssignCompliance.Checked)
                                    {
                                        bool flag = EventAssignmentSheetsExitsts(xlWorkbook, "UploadEventAssignment");
                                        if (flag == true)
                                        {
                                            int customerID = Convert.ToInt32(ddlCustomerList.SelectedValue);
                                            int complianceProductType = 0;
                                            complianceProductType = RLCS_Master_Management.GetComplianceProductType(customerID);

                                            if (HttpContext.Current.Cache.Get("EventListData") != null)
                                            {
                                                HttpContext.Current.Cache.Remove("EventListData");
                                            }
                                            if (HttpContext.Current.Cache.Get("UserListData") != null)
                                            {
                                                HttpContext.Current.Cache.Remove("UserListData");
                                            }
                                            if (HttpContext.Current.Cache.Get("CustomerBranchListData") != null)
                                            {
                                                HttpContext.Current.Cache.Remove("CustomerBranchListData");
                                            }
                                            if (HttpContext.Current.Cache.Get("EventInstanceAssignmentListData") != null)
                                            {
                                                HttpContext.Current.Cache.Remove("EventInstanceAssignmentListData");
                                            }

                                            if (HttpContext.Current.Cache.Get("HRComplianceListData") != null)
                                            {
                                                HttpContext.Current.Cache.Remove("HRComplianceListData");
                                            }

                                            GetEvent();
                                            GetUser(customerID);
                                            GetCustomerBranch(customerID);

                                            ComplianceAssignmentData(complianceProductType, xlWorkbook);
                                            if (suucess == true)
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Data uploaded successfully.";

                                                if (HttpContext.Current.Cache.Get("EventListData") != null)
                                                {
                                                    HttpContext.Current.Cache.Remove("EventListData");
                                                }
                                                if (HttpContext.Current.Cache.Get("UserListData") != null)
                                                {
                                                    HttpContext.Current.Cache.Remove("UserListData");
                                                }
                                                if (HttpContext.Current.Cache.Get("CustomerBranchListData") != null)
                                                {
                                                    HttpContext.Current.Cache.Remove("CustomerBranchListData");
                                                }
                                            }
                                        }
                                        else
                                        {
                                            cvDuplicateEntry.IsValid = false;
                                            cvDuplicateEntry.ErrorMessage = "Please check the sheet name, Sheet name must be 'UploadEventAssignment'";
                                        }
                                    }
                                }
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Error uploading file. Please try again.";
                            }
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Select Customer.";
                        }
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Please Select Customer.";
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                }
            }
        }
        private bool EventAssignmentSheetsExitsts(ExcelPackage xlWorkbook, string data)
        {
            try
            {
                bool flag = false;
                foreach (ExcelWorksheet sheet in xlWorkbook.Workbook.Worksheets)
                {
                    if (data.Equals("UploadEventAssignment"))
                    {
                        if (sheet.Name.Trim().Equals("UploadEventAssignment"))
                        {
                            flag = true;
                            break; 
                        }
                        else
                        {
                            flag = false;
                            break;
                        }
                    }
                }
                return flag;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }
        private bool ComplianceAssignmentCheckListSheetsExitsts(ExcelPackage xlWorkbook, string data)
        {
            try
            {
                bool flag = false;
                foreach (ExcelWorksheet sheet in xlWorkbook.Workbook.Worksheets)
                {
                    if (data.Equals("UploadComplianceAssignmentCheckList"))
                    {
                        if (sheet.Name.Trim().Equals("UploadComplianceAssignmentCheckList") || sheet.Name.Trim().Equals("UploadComplianceAssignmentCheckList") || sheet.Name.Trim().Equals("UploadComplianceAssignmentCheckList"))
                        {
                            flag = true;
                            break;//added by Manisha
                        }
                        else
                        {
                            flag = false;
                            break;
                        }
                    }
                }
                return flag;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }
        protected void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                lblMessage.Text = "";
                MasterFileUpload = null;
                //lblMessage1.Text = "";
                //MasterFileUpload1 = null;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public partial class TempTable
        {
            public long ComplianceId { get; set; }
            public int CustomerBranchID { get; set; }
            public long Performerid { get; set; }
            public string SequenceID { get; set; }
        }
        public void ErrorMessages(List<string> emsg)
        {
            string finalErrMsg = string.Empty;

            finalErrMsg += "<ol type='1'>";

            if (emsg.Count > 0)
            {
                emsg.ForEach(eachErrMsg =>
                {
                    finalErrMsg += "<li>" + eachErrMsg + "</li>";
                });

                finalErrMsg += "</ol>";
            }

            cvDuplicateEntry.IsValid = false;
            cvDuplicateEntry.ErrorMessage = finalErrMsg;
        }
        private bool CheckInt(string val)
        {
            try
            {
                int i = Convert.ToInt32(val);
                return true;
            }
            catch
            {
                return false;
            }
        }
        private void ComplianceAssignmentData(int complianceProductType, ExcelPackage xlWorkbook)
        {
            try
            {
                ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["UploadEventAssignment"];
                if (xlWorksheet != null)
                {
                    int count = 1;
                    int xlrow2 = xlWorksheet.Dimension.End.Row;
                    List<TempAssignmentTable> TempassignmentTableList = new List<TempAssignmentTable>();
                    List<TempAssignmentTableCheckList> TempassignmentTableCheckList = new List<TempAssignmentTableCheckList>();
                    List<string> errorMessage = new List<string>();
                    List<TempTable> lstTemptable = new List<TempTable>();

                 
                    int valEventID = -1;
                    int valEventOwnerID = -1;
                    int valCustomerBranchID = -1;
                    for (int rowNum = 2; rowNum <= xlrow2; rowNum++)
                    {
                        valEventID = -1;
                        valEventOwnerID = -1;
                        valCustomerBranchID = -1;
                        #region 1 EventID
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 1].Text.ToString().Trim()))
                        {
                            valEventID = Convert.ToInt32(xlWorksheet.Cells[rowNum, 1].Text.Trim());
                        }
                        if (valEventID == -1 || valEventID == 0)
                        {
                            errorMessage.Add("Required EventID at row number-" + rowNum);
                        }
                        else
                        {
                            if (EventExists(valEventID) == false)
                            {
                                errorMessage.Add("EventID not defined in the System or It is Sub event or Intermediate Event at row number-" + rowNum);
                            }
                        }
                        #endregion

                        #region 2 EventOwnerID
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 2].Text.ToString()))
                        {
                            valEventOwnerID = Convert.ToInt32(xlWorksheet.Cells[rowNum, 2].Text);
                        }
                        if (valEventOwnerID == -1 || valEventOwnerID == 0)
                        {
                            errorMessage.Add("Required EventOwnerID  at row number-" + rowNum);
                        }
                        else
                        {
                            if (UserExists(valEventOwnerID) == false)
                            {
                                errorMessage.Add("EventOwnerID not Defined in the System at row number-" + rowNum);
                            }
                        }
                        #endregion

                        #region 3 CustomerBranchID
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 3].Text.ToString()))
                        {
                            valCustomerBranchID = Convert.ToInt32(xlWorksheet.Cells[rowNum, 3].Text);
                        }
                        if (valCustomerBranchID == -1 || valCustomerBranchID == 0)
                        {
                            errorMessage.Add("Required CustomerBranchID  at row number-" + rowNum);
                        }
                        else
                        {
                            if (BranchIDExists(valCustomerBranchID) == false)
                            {
                                errorMessage.Add("CustomerBranchID not Defined in the System at row number-" + rowNum);
                            }
                        }
                        #endregion

                        #region 3 Check CustomerBranch Industry Mapping
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 3].Text.ToString()))
                        {
                            valCustomerBranchID = Convert.ToInt32(xlWorksheet.Cells[rowNum, 3].Text);
                        }
                        if (valCustomerBranchID == -1 || valCustomerBranchID == 0)
                        {
                            Boolean IsIndustryMapped = EventManagement.CheckIndustryMapping(valCustomerBranchID);

                            if (IsIndustryMapped == false)
                            {
                                errorMessage.Add("Industry not assigned for location at row number-" + rowNum);
                            }
                        }
                        #endregion
                    }
                    #region 4 Check Compliance Assignment

                    // ---------------------------------------------

                    List<Tuple<int, bool, bool>> EventList = new List<Tuple<int, bool, bool>>();

                    int index = -1;
                    for (int rowNum1 = 2; rowNum1 <= xlrow2; rowNum1++)
                    {
                        index = Convert.ToInt32(xlWorksheet.Cells[rowNum1, 1].Text.Trim());
                        var data = EventList.Where(entry => entry.Item1 == index).FirstOrDefault();
                        if (data != null)
                        {
                            EventList.Remove(data);
                            EventList.Add(new Tuple<int, bool, bool>(index, false, false));
                        }
                        else
                        {
                            EventList.Add(new Tuple<int, bool, bool>(index, false, false));
                        }
                        // ----------------------------------------------
                        List<long> EventList1 = new List<long>();

                        long EventID = EventList[0].Item1;
                        int branchID = Convert.ToInt32(xlWorksheet.Cells[rowNum1, 3].Text);
                        bool FlgCheck = false;
                        FlgCheck = EventManagement.CheckEventAssigned(EventID, branchID);

                        if (FlgCheck == true)
                        {
                            continue;
                        }
                        else
                        {
                            EventList1.Add(EventID);
                            long? EventClassification = EventManagement.GetEventClassification(EventID);
                            var exceptComplianceIDs = EventManagement.CheckAllEventComplianceNotAssigned((int)EventClassification, EventList1, branchID);
                            if (exceptComplianceIDs.Count > 0)
                            {
                                var cList = EventManagement.GetAllNotAssignedComplinceListForEvent((int)EventClassification, exceptComplianceIDs, EventList1, branchID, -1);
                                errorMessage.Add("Compliance not assigned for event at row number-" + rowNum1 + " - ");

                                foreach (var item in cList)
                                {
                                    string str = "Please assign Compliance :- Event ID="+ item.EventID + ", EvnetName=" + item.EvnetName + ",  ComplianceID="+ item.ID + ", ShortDescription "+ item.ComplianceName + " CustomerBranchName+ "+item.CustomerBranchName +"";
                                    errorMessage.Add(str);
                                }
                            }
                        }
                    }
                    #endregion

                    if (errorMessage.Count > 0)
                    {
                        ErrorMessages(errorMessage);
                    }
                    else
                    {
                        #region Save
                        for (int i = 2; i <= xlrow2; i++)
                        {
                            count = count + 1;
                            int EventID = -1;
                            int EventOwnerID = -1;
                            int CustomerBranchID = -1; ;
                            #region EventID                     
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 1].Text.ToString()))
                            {
                                EventID = Convert.ToInt32(xlWorksheet.Cells[i, 1].Text.Trim());
                            }
                            #endregion

                            #region EventOwnerID
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 2].Text.ToString()))
                            {
                                EventOwnerID = Convert.ToInt32(xlWorksheet.Cells[i, 2].Text);
                            }
                            #endregion

                            #region CustomerBranchID
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 3].Text.ToString()))
                            {
                                CustomerBranchID = Convert.ToInt32(xlWorksheet.Cells[i, 3].Text);
                            }
                            #endregion


                            if ((Business.EventManagement.EventAssignmentExists(EventID, CustomerBranchID)))
                            {
                                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                {

                                    List<EventInstance> eventInstanceOwner = new List<EventInstance>();
                                    List<EventAssignment> eventAssignmentOwner = new List<EventAssignment>();

                                    EventInstance inst = new EventInstance();
                                    inst.EventID = EventID;
                                    inst.CustomerBranchID = CustomerBranchID;
                                    inst.CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID);
                                    inst.IsDeleted = false;
                                    inst.StartDate = DateTime.Now;
                                    inst.CreatedOn = DateTime.Now;
                                    eventInstanceOwner.Add(inst);
                                    Business.EventManagement.AddEventInstance(eventInstanceOwner);

                                    EventAssignment assig = new EventAssignment();
                                    assig.EventInstanceID = inst.ID;
                                    assig.UserID = EventOwnerID;
                                    assig.Role = 10;
                                    assig.CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID);
                                    assig.UpdatedBy = Convert.ToInt32(AuthenticationHelper.UserID);
                                    assig.UpdatedOn = DateTime.Now;
                                    assig.IsDeleted = false;
                                    assig.CreatedOn = DateTime.Now;
                                    eventAssignmentOwner.Add(assig);

                                    Business.EventManagement.AddEventAssignment(eventAssignmentOwner, CustomerBranchID);

                                    suucess = true;
                                }
                            }
                        }//exists end

                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                suucess = false;
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public static void GetEvent()
        {
            List<ComplianceManagement.Business.Data.Event> Records = new List<ComplianceManagement.Business.Data.Event>();

            var EventList = HttpContext.Current.Cache["EventListData"];

            if (EventList == null)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Records = (from row in entities.Events
                               where row.IsDeleted == false
                               && row.Visible == "Y"
                               select row).ToList();

                    HttpContext.Current.Cache.Insert("EventListData", Records); // add it to cache
                }
            }
        }

        public void GetUser(int CustomerID)
        {
            List<ComplianceManagement.Business.Data.User> Records = new List<ComplianceManagement.Business.Data.User>();

            var ComplianceList = HttpContext.Current.Cache["UserListData"];

            if (ComplianceList == null)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Records = (from row in entities.Users
                               where row.CustomerID == CustomerID && row.RoleID != 19
                               && row.IsDeleted == false
                               select row).ToList();

                    HttpContext.Current.Cache.Insert("UserListData", Records); // add it to cache
                }
            }
        }
        public void GetCustomerBranch(int CustomerID)
        {
            List<ComplianceManagement.Business.Data.CustomerBranch> Records = new List<ComplianceManagement.Business.Data.CustomerBranch>();

            var ComplianceList = HttpContext.Current.Cache["CustomerBranchListData"];

            if (ComplianceList == null)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Records = (from row in entities.CustomerBranches
                               where row.CustomerID == CustomerID
                               && row.IsDeleted == false
                               select row).ToList();

                    HttpContext.Current.Cache.Insert("CustomerBranchListData", Records); // add it to cache
                }
            }
        }

        public void GetComplianceInstanceAssignment(int CustomerID)
        {
            List<ComplianceManagement.Business.Data.SP_CheckSequence_Result> Records = new List<ComplianceManagement.Business.Data.SP_CheckSequence_Result>();

            var ComplianceList = HttpContext.Current.Cache["ComplianceInstanceAssignmentListData"];

            if (ComplianceList == null)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Records = (from row in entities.SP_CheckSequence(CustomerID, "S")
                               select row).ToList();

                    HttpContext.Current.Cache.Insert("ComplianceInstanceAssignmentListData", Records); // add it to cache
                }
            }
        }

        public bool EventExists(int EventID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Records = (List<ComplianceManagement.Business.Data.Event>)Cache["EventListData"];
                if (Records.Count > 0)
                {
                    var query = (from row in Records
                                 where row.ID == EventID
                                 select row).FirstOrDefault();
                    if (query != null)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }
        public bool UserExists(int Userid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Records = (List<ComplianceManagement.Business.Data.User>)Cache["UserListData"];
                if (Records.Count > 0)
                {
                    var query = (from row in Records
                                 where row.ID == Userid
                                 select row).FirstOrDefault();
                    if (query != null)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }
        public bool BranchIDExists(int Branchid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Records = (List<ComplianceManagement.Business.Data.CustomerBranch>)Cache["CustomerBranchListData"];
                if (Records.Count > 0)
                {
                    var query = (from row in Records
                                 where row.ID == Branchid
                                 select row).FirstOrDefault();
                    if (query != null)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }
        public bool HRComplianceExists(int complianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Records = (List<SP_RLCS_RegisterReturnChallanCompliance_Result>)Cache["HRComplianceListData"];
                if (Records.Count > 0)
                {
                    var query = (from row in Records
                                 where row.ID == complianceID
                                 select row).FirstOrDefault();
                    if (query != null)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }

    }
}
