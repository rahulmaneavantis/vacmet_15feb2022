﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using System.Xml;
using System.Xml.Linq;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using System.Reflection;
using System.IO;
using System.Threading;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using System.Configuration;
using System.Web.Security;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Account
{
    public partial class SendNewsLetter : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (User.Identity.IsAuthenticated)
                {
                    //xmlnode[i].ChildNodes[0].ChildNodes.Item(1).InnerText
                    //str = xmlnode[i].ChildNodes.Item(0).InnerText.Trim() 
                    //    + " " + xmlnode[i].ChildNodes.Item(1).InnerText.Trim() 
                    //    + " " + xmlnode[i].ChildNodes.Item(2).InnerText.Trim();       

                    tbxTo.Text = tbxCC.Text = tbxBcc.Text = tbxSubject.Text = txtMessagebody.Text = "";
                    fuAttachments.Attributes.Clear();
                }
                else
                    FormsAuthentication.RedirectToLoginPage();
            }
        }

        protected void btnSend_Click(object sender, EventArgs e)
        {
            try
            {
                if (tbxSubject.Text != "" && txtFromName.Text != "")
                {
                    bool sendMailSuccess = false;
                    List<string> lstTO = new List<string>();
                    List<string> lstCc = new List<string>();
                    List<string> lstBcc = new List<string>();

                    string strReceiver = string.Empty;                 

                    strReceiver = tbxTo.Text;
                    string[] Multiple = strReceiver.Split(',');

                    foreach (string multiple_email in Multiple)
                    {
                        if (multiple_email != "")
                            lstTO.Add(multiple_email.Trim());
                    }

                    strReceiver = tbxCC.Text;
                    Multiple = strReceiver.Split(',');

                    foreach (string multiple_email in Multiple)
                    {
                        if (multiple_email != "")
                            lstCc.Add(multiple_email.Trim());
                    }

                    strReceiver = tbxBcc.Text;
                    Multiple = strReceiver.Split(',');

                    foreach (string multiple_email in Multiple)
                    {
                        if (multiple_email != "")
                            lstBcc.Add(multiple_email.Trim());
                    }

                    List<Tuple<string, string>> attachmentwithPath = new List<Tuple<string, string>>();

                    string folderPath = string.Empty;

                    if (fuAttachments.HasFiles)
                    {
                        folderPath = "~/Uploaded/" + DateTime.Now.ToString("ddMMyyyy");

                        if (!Directory.Exists(folderPath))
                            Directory.CreateDirectory(Server.MapPath(folderPath));

                        foreach (HttpPostedFile postedFile in fuAttachments.PostedFiles)
                        {
                            postedFile.SaveAs(Server.MapPath(folderPath + "/") + postedFile.FileName.Trim());
                            attachmentwithPath.Add(new Tuple<string, string>(Server.MapPath(folderPath + "/") + postedFile.FileName.Trim(), postedFile.FileName));
                        }
                    }

                    //List<Tuple<Stream, string>> attachment = new List<Tuple<Stream, string>>();
                    //HttpFileCollection fileCollection = Request.Files;
                    //    if (fileCollection.Count > 0)
                    //{
                    //    for (int i = 0; i < fileCollection.Count; i++)
                    //    {
                    //        HttpPostedFile uploadfile = fileCollection[i];
                    //        attachment.Add(new Tuple<Stream, string>(uploadfile.InputStream, uploadfile.FileName));
                    //    }
                    //}

                    try
                    {
                        //SendGridEmailManager.SendGridNewsLetterMail(ConfigurationManager.AppSettings["SenderEmailAddress"],txtFromName.Text, lstTO, lstCc, lstBcc, tbxSubject.Text, txtMessagebody.Text, attachmentwithPath);
                        sendMailSuccess = true;
                    }
                    catch (Exception ex)
                    {
                        sendMailSuccess = false;
                    }

                    if(sendMailSuccess)
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "E-Mail Sent Successfully.";
                    }
                }
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Required Subject Line and From Name.";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong. Please try again.";
            }
        }       

        protected void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                tbxTo.Text = tbxCC.Text = tbxBcc.Text = tbxSubject.Text =   txtMessagebody.Text = "";
                fuAttachments.Attributes.Clear();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong. Please try again.";
            }
        }

        protected void btnGetMailIds_Click(object sender, EventArgs e)
        {
            try
            {
                string EmailList = string.Empty;

                EmailList = ZohoCRMAPI.GetMailMasterLists(ddlType.SelectedValue);

                if (EmailList != "")
                {
                    tbxBcc.Text = EmailList;                    
                }
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "No Email Found";
                }

                txtFilter_TextChanged(sender, e);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong. Please try again.";
            }
        }

        protected void btnRefreshMail_Click(object sender, EventArgs e)
        {
            try
            {
                bool successGetRecords = false;

                if (ddlType.SelectedValue == "0")
                {
                    successGetRecords = ZohoCRMAPI.RefreshZohoRecords();

                    if (successGetRecords)
                        successGetRecords = ZohoCRMAPI.RefreshComplianceProductCustomerRecords();
                }
                else if (ddlType.SelectedValue == "1")
                {
                    successGetRecords = ZohoCRMAPI.RefreshComplianceProductCustomerRecords();
                }
                else if (ddlType.SelectedValue == "2")
                {
                    successGetRecords = ZohoCRMAPI.RefreshZohoRecords();
                }

                if (successGetRecords)
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Email List Refreshed Successfully. ";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong. Please try again.";
            }
        }

        protected void txtFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (tbxBcc.Text != "")
                {
                    string[] Multiple = tbxBcc.Text.Trim(',').Split(',');

                    if (Multiple.Length > 1)
                        lblEmailCount.Text = Multiple.Length + " Emails Selected";
                    else if (Multiple.Length == 1)
                        lblEmailCount.Text = Multiple.Length + " Email Selected";
                    else
                        lblEmailCount.Text = "";
                }
                else
                    lblEmailCount.Text = "";
            }

            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong. Please try again.";
            }
        }
    }
}
   