﻿<%@ Page Title="Upload Master Data" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="UploadInternalMasterData.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Account.UploadInternalMasterData" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">

    <table width="100%" align="left">
        <tr>
            <td>
              <asp:Button Text="Add Compliance" BorderStyle="None" ID="Tab1" CssClass="Initial" runat="server"
                    OnClick="Tab1_Click" />
                    <asp:Button Text="Add Checklist" BorderStyle="None" ID="Tab3" CssClass="Initial" runat="server"
                    OnClick="Tab3_Click" />
                    <asp:Button Text="Download Excel Format" BorderStyle="None" ID="btnExcelFormat" CssClass="Initial" runat="server"
                    OnClick="btnExcelFormat_Click" />
                <asp:MultiView ID="MainView" runat="server">
                    <asp:View ID="View1" runat="server">

                        <div style="width: 100%; float: left; margin-bottom: 15px">
                            <div style="margin-bottom: 4px; width: auto">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="vdsummary" ValidationGroup="oplValidationGroup" />
                                <div align="center" style="margin-top: 30px; font-family: Arial; font-size: 10pt">
                                    <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
                                    <asp:Label ID="LblErormessage" runat="server" Text="" ForeColor="red"></asp:Label>
                                    <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                        ValidationGroup="oplValidationGroup" Display="None" Enabled="true" ShowSummary="true" />
                                </div>
                            </div>
                         <div style="margin-bottom: 7px" id="customerdiv" runat="server">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;margin-top:-2px">
                                    Customer:</label>
                                <asp:DropDownList ID="ddlCustomer" runat="server" AutoPostBack="true" Style="padding: 0px; margin: 0px; height: 22px; width: 300px;margin-left: -87px; margin-top: -4px;"
                                    CssClass="txtbox">
                                </asp:DropDownList>
                                <asp:CompareValidator ErrorMessage="Please select Customer." ControlToValidate="ddlCustomer"
                                    runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="oplValidationGroup"
                                    Display="None" />
                            </div>
                            <br />
                            <br />
                            <table align="left" cellpadding="2" style="margin-left: 55px;">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblUploadFile" runat="server" Text="Upload File :"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:FileUpload ID="MasterFileUpload" runat="server" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Please Select File." ControlToValidate="MasterFileUpload"
                                            runat="server" Display="None" ValidationGroup="oplValidationGroup" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; padding-left: 8px;">
                                        <asp:Button ID="btnUploadFile" runat="server" Text="Upload" ValidationGroup="oplValidationGroup"
                                            Style="margin-left: 75px;"
                                            OnClick="btnUploadFile_Click" />
                                    </td>
                                     <td style="text-align: left; padding-left: 8px;">
                                        <asp:Button ID="btnSave" runat="server" Text="Save" Enabled="false"
                                            Style="margin-left: 75px;"
                                            OnClick="btnSave_Click" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <asp:UpdatePanel ID="upComplianceList" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div id="divComp" runat="server">
                                    <asp:GridView ID="grdCompliance" AutoGenerateColumns="false" CellPadding="5" runat="server">
                                         <Columns>
                                        <asp:BoundField HeaderText="Compliance Type Master" DataField="IComplianceTypeID" />
                                        <asp:BoundField HeaderText="Compliance Category Master" DataField="IComplianceCategoryID" />
                                        <asp:BoundField HeaderText="Compliance Type" DataField="IComplianceType" />
                                        <asp:BoundField HeaderText="Timely based type" DataField="ISubComplianceType" />
                                        <asp:BoundField HeaderText="Short Description" DataField="IShortDescription" />
                                        <asp:BoundField HeaderText="UploadDocument" DataField="IUploadDocument" />
                                        <asp:BoundField HeaderText="Required Forms" DataField="IRequiredFrom" />
                                        <asp:BoundField HeaderText="Compliance Occourrence" DataField="IComplianceOccurrence" />
                                        <asp:BoundField HeaderText="Frequency" DataField="IFrequency" />
                                        <asp:BoundField HeaderText="Due Date" DataField="IDueDate" />
                                     <%--   <asp:BoundField HeaderText="Special Due Date - For Month" DataField="specilMonth" />
                                        <asp:BoundField HeaderText="Special Due Date - Date" DataField="specialDay" />--%>
                                        <asp:BoundField HeaderText="Reminder type" DataField="IReminderType" />
                                        <asp:BoundField HeaderText="Before (in days)" DataField="IReminderBefore" />
                                        <asp:BoundField HeaderText="Gap (in days)" DataField="IReminderGap" />
                                        <asp:BoundField HeaderText="Risk Type" DataField="IRiskType" />
                                        <asp:BoundField HeaderText="OneTime Date" DataField="IOneTimeDate" />
                                        <asp:BoundField HeaderText="CalFlag" DataField="CalFlag" />
                                        <asp:BoundField HeaderText="IsDocumentRequired" DataField="IsDocumentRequired" />
                                        <asp:BoundField HeaderText="Detailed Description" DataField="IDetailedDescription" />
                                             <asp:BoundField HeaderText="Short Form" DataField="IShortForm" />
                                             <asp:BoundField HeaderText="Schedule Type" DataField="ScheduleType" />
                                             <asp:BoundField HeaderText="Is EventBased" DataField="IsEventBased" />
                                               <asp:BoundField HeaderText="DueWeekDay" DataField="DueWeekDay" />
                                    </Columns>
                                        <HeaderStyle BackColor="#df5015" Font-Bold="true" ForeColor="White" />
                                    </asp:GridView>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </asp:View>
                    <asp:View ID="View3" runat="server">
                        <div style="width: 100%; float: left; margin-bottom: 15px">
                            <div style="margin-bottom: 4px; width: auto">
                                <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="vdsummary" ValidationGroup="oplValidationGroup" />
                                <div align="center" style="margin-top: 30px; font-family: Arial; font-size: 10pt">
                                    <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
                                    <asp:Label ID="Label2" runat="server" Text="" ForeColor="red"></asp:Label>
                                    <asp:CustomValidator ID="cvDuplicateEntry1" runat="server" EnableClientScript="False"
                                        ValidationGroup="oplValidationGroup" Display="None" Enabled="true" ShowSummary="true" />
                                </div>
                            </div>
                            <div style="margin-bottom: 7px"  id="checklistcustomer" runat="server">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;margin-top:-2px">
                                    Customer</label>
                                <asp:DropDownList ID="ddlchecklistCustomer" runat="server" AutoPostBack="true" Style="padding: 0px; margin: 0px; height: 22px; width: 300px;margin-left: -87px; margin-top: -4px;"
                                    CssClass="txtbox">
                                </asp:DropDownList>
                                <asp:CompareValidator ErrorMessage="Please select Customer." ControlToValidate="ddlchecklistCustomer"
                                    runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="oplValidationGroup"
                                    Display="None" />
                            </div>
                            <br />
                            <br />
                            <table align="left" cellpadding="2" style="margin-left: 55px;">
                                <tr>
                                    <td>
                                        <asp:Label ID="Label3" runat="server" Text="Upload File :"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:FileUpload ID="MasterCheckListFileUpload" runat="server" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Please Select File." ControlToValidate="MasterCheckListFileUpload"
                                            runat="server" Display="None" ValidationGroup="oplValidationGroup" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; padding-left: 8px;">
                                        <asp:Button ID="btnChecklistUploadFile" runat="server" Text="Upload" ValidationGroup="oplValidationGroup"
                                            Style="margin-left: 75px;"
                                            OnClick="btnChecklistUploadFile_Click" />
                                    </td>
                                     <td style="text-align: left; padding-left: 8px;">
                                        <asp:Button ID="btnCheckListSave" runat="server" Text="Save" Enabled="false"
                                            Style="margin-left: 75px;"
                                            OnClick="btnCheckListSave_Click" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div id="div1" runat="server">
                                    <asp:GridView ID="grdCheckList" AutoGenerateColumns="false" CellPadding="5" runat="server">
                                         <Columns>
                                        <asp:BoundField HeaderText="Compliance Type Master" DataField="IComplianceTypeID" />
                                        <asp:BoundField HeaderText="Compliance Category Master" DataField="IComplianceCategoryID" />
                                        <asp:BoundField HeaderText="Compliance Type" DataField="IComplianceType" />
                                        <asp:BoundField HeaderText="Timely based type" DataField="ISubComplianceType" />
                                        <asp:BoundField HeaderText="Short Description" DataField="IShortDescription" />
                                        <asp:BoundField HeaderText="UploadDocument" DataField="IUploadDocument" />
                                        <asp:BoundField HeaderText="Required Forms" DataField="IRequiredFrom" />
                                        <asp:BoundField HeaderText="Compliance due (in days)" DataField="IDueDate" />
                                        <asp:BoundField HeaderText="Frequency" DataField="IFrequency" />
                                        <asp:BoundField HeaderText="Due Date" DataField="IDueDate" />
                                        <asp:BoundField HeaderText="Special Due Date - For Month" DataField="specilMonth" />
                                        <asp:BoundField HeaderText="Special Due Date - Date" DataField="specialDay" />
                                        <asp:BoundField HeaderText="Reminder type" DataField="IReminderType" />
                                        <asp:BoundField HeaderText="Before (in days)" DataField="IReminderBefore" />
                                        <asp:BoundField HeaderText="Gap (in days)" DataField="IReminderGap" />
                                        <asp:BoundField HeaderText="Risk Type" DataField="IRiskType" />
                                        <asp:BoundField HeaderText="OneTime Date" DataField="IOneTimeDate" />
                                        <asp:BoundField HeaderText="CheckListType" DataField="CheckListTypeID" />
                                        <asp:BoundField HeaderText="CalFlag" DataField="CalFlag" />
                                        <asp:BoundField HeaderText="Detailed Description" DataField="IDetailedDescription" />
                                              <asp:BoundField HeaderText="Short Form" DataField="IShortForm" />
                                             <asp:BoundField HeaderText="Schedule Type" DataField="ScheduleType" />
                                             <asp:BoundField HeaderText="Is EventBased" DataField="IsEventBased" />
                                               <asp:BoundField HeaderText="DueWeekDay" DataField="DueWeekDay" />
                                    </Columns>
                                        <HeaderStyle BackColor="#df5015" Font-Bold="true" ForeColor="White" />
                                    </asp:GridView>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </asp:View>
                </asp:MultiView>
            </td>
        </tr>
    </table>
</asp:Content>
