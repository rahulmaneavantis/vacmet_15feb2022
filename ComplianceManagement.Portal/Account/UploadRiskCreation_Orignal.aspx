﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true" CodeBehind="UploadRiskCreation_Orignal.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Account.UploadRiskCreation_Orignal" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
        .excelgriddisplay{
            display:none;
        }
               .chosen-container-single .chosen-single{background:none !important;border:1px solid #c7c7cc   !important;height: 32px  !important;padding: 3px 0px 0px 14px !important; font-family:'Roboto', sans-serif !important; }
  
   
    </style>

    <script type="text/javascript">
        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');
            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }
        function initializeRadioButtonsList(controlID) {
            $(controlID).buttonset();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:UpdatePanel ID="upComplianceDetails" runat="server" UpdateMode="Conditional"
        OnLoad="upComplianceDetails_Load">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <div style="margin-bottom: 4px; width: auto">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" class="alert alert-block alert-danger fade in" ValidationGroup="oplValidationGroup" />
                            <div align="center" style="margin-top: 30px; font-family: Arial; font-size: 10pt">
                                <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
                                <asp:Label ID="LblErormessage" runat="server" Text="" ForeColor="red"></asp:Label>
                                <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                    ValidationGroup="oplValidationGroup" Display="None" Enabled="true" ShowSummary="true" />
                            </div>
                        </div>

                        <div style="margin-bottom: 4px; width: auto">
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="vdsummary" ValidationGroup="oplValidationGroup1" />
                            <div align="center" style="margin-top: 30px; font-family: Arial; font-size: 10pt">
                                <asp:Label ID="lblMessage1" runat="server" Text=""></asp:Label>
                                <asp:Label ID="LblErormessage1" runat="server" Text="" ForeColor="red"></asp:Label>
                                <asp:CustomValidator ID="cvDuplicateEntry1" runat="server" EnableClientScript="False"
                                    ValidationGroup="oplValidationGroup1" Display="None" Enabled="true" ShowSummary="true" />
                            </div>
                        </div>

                        <section class="panel">                            
                            <header class="panel-heading tab-bg-primary ">
                                          <ul id="rblRole1" class="nav nav-tabs">                                              
                                                   <li class="dummyval">                                                     
                                                        <asp:LinkButton ID="Tab1" OnClick="Tab1_Click1" runat="server">Import Utility</asp:LinkButton>
                                                    </li>                                              
                                                    <li class="dummyval">                                                      
                                                           <asp:LinkButton ID="Tab2" OnClick="Tab2_Click1" runat="server">Export Utility</asp:LinkButton>
                                                    </li>                                                                                                                                                                                                      
                                        </ul>
                                </header>

                           <div class="clearfix"></div> 

                            <div class="tab-content ">                                 
                                   <div runat="server" id="performerdocuments"  class="tab-pane active">
                                    <div style="width: 100%; float: left; margin-bottom: 15px">
                                      <div class="col-md-2 colpadding0 entrycount" style="margin-top: 5px; color:#333;">
                                           <asp:RadioButton ID="rdoCompliance" runat="server" AutoPostBack="True" Text="Risk Control Matrix" OnCheckedChanged="rdoCompliance_CheckedChanged" GroupName="uploadContentGroup" />
                                         </div>
                                         <div class="col-md-2 colpadding0 entrycount" style="margin-top: 5px; color:#333;">
                                             <asp:RadioButton ID="rdoSubProcess" runat="server" AutoPostBack="True" Text="Sub Process" OnCheckedChanged="rdoSubProcess_CheckedChanged" GroupName="uploadContentGroup" />
                                          </div>
                                        <div class="col-md-6 colpadding0 entrycount" style="margin-top: 5px; color:#333;">
                                            <div class="col-md-3 colpadding0 entrycount">
                                                <asp:FileUpload ID="MasterFileUpload" runat="server"/>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Please Select File." ControlToValidate="MasterFileUpload"
                                                    runat="server" Display="None" ValidationGroup="oplValidationGroup" />
                                            </div>
                                             <div class="col-md-3 colpadding0 entrycount" style="float: right;">
                                                <asp:Button ID="btnUploadFile" runat="server" Text="Upload" ValidationGroup="oplValidationGroup"
                                                    Style="margin-left: 75px;" class="btn btn-search"
                                                    OnClick="btnUploadFile_Click" />
                                                 </div>
                                        </div>
                                    </div></div>
<%--//////////////////////////////////////////////////Tab 2////////////////////////////////////////////////////////////--%>
                                   <div runat="server" id="reviewerdocuments" class="tab-pane">
                                       <div class="clearfix"></div><div class="clearfix" style="height:15px"></div>
                                      <div style="width: 100%; float: left; margin-bottom: 15px">
                                          <div style="width:100%">
                                <div style="width:15%">
                                     <div class="col-md-2 colpadding0 entrycount" style="margin-top: 5px;width:100%">
                                    <div class="col-md-2 colpadding0" style="width:23%">
                                        <p style="color: #999; margin-top: 5px;">Show </p>
                                    </div>
                                    <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px; float: left"
                                        AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" >                        
                                        <asp:ListItem Text="5" Selected="True" />
                                        <asp:ListItem Text="10" />
                                        <asp:ListItem Text="20" />
                                        <asp:ListItem Text="50" />
                                    </asp:DropDownList>
                                 </div>    
                                </div>      
                                   <div style="width:87%">
                            <div class="col-md-12 colpadding0">
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                    <asp:DropDownListChosen runat="server" ID="ddlLegalEntity"  class="form-control m-bot15"  Width="80%" Height="32px"
                                    AutoPostBack="true" Style="background:none;" OnSelectedIndexChanged="ddlLegalEntity_SelectedIndexChanged" DataPlaceHolder="Unit">
                                    </asp:DropDownListChosen> 
                                </div>
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                    <asp:DropDownListChosen runat="server" ID="ddlSubEntity1" class="form-control m-bot15"  Width="80%" Height="32px"
                                    AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity1_SelectedIndexChanged" DataPlaceHolder="Sub Unit 1">
                                    </asp:DropDownListChosen>
                                </div>
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                    <asp:DropDownListChosen runat="server" ID="ddlSubEntity2"  class="form-control m-bot15" Width="80%" Height="32px"
                                    AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity2_SelectedIndexChanged" DataPlaceHolder="Sub Unit 2">
                                    </asp:DropDownListChosen>
                                </div>
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                    <asp:DropDownListChosen runat="server" ID="ddlSubEntity3" class="form-control m-bot15" Width="80%" Height="32px"
                                    AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity3_SelectedIndexChanged" DataPlaceHolder="Sub Unit 3">
                                    </asp:DropDownListChosen>
                                </div>
                                 <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                        <asp:DropDownListChosen runat="server" ID="ddlFilterLocation" AutoPostBack="true" Width="80%" DataPlaceHolder="Location" class="form-control m-bot15" Style="width: 160px;"
                                OnSelectedIndexChanged="ddlFilterLocation_SelectedIndexChanged">
                            </asp:DropDownListChosen> 
                                     </div>
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                     <asp:DropDownListChosen runat="server" ID="ddlProcess" AutoPostBack="true" Width="80%"   DataPlaceHolder="Process" class="form-control m-bot15" Style="width: 150px;"
                                OnSelectedIndexChanged="ddlProcess_SelectedIndexChanged">
                            </asp:DropDownListChosen> 
                               <asp:CompareValidator ErrorMessage="Please select Process." ControlToValidate="ddlProcess" 
                                runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceInstanceValidationGroup"
                                Display="None" />
                                  </div>
                             
                                       </div>
                                </div>  </div>   
                            <div style="width:100%;">
                                <div style="width:15%;">    <div class="col-md-2 colpadding0 entrycount" style="margin-top: 5px;width:100%">

                                                                       </div></div>
                                 <div style="width:85%;">
                                        <div class="col-md-12 colpadding0">
                                            
                                 <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                           <asp:DropDownListChosen runat="server" ID="ddlFilterSubProcess" AutoPostBack="true" Width="80%"  DataPlaceHolder="Sub Process"  class="form-control m-bot15" Style="width: 160px;"
                                OnSelectedIndexChanged="ddlFilterSubProcess_SelectedIndexChanged">
                            </asp:DropDownListChosen> 
                            <asp:CompareValidator ErrorMessage="Select Sub Process." ControlToValidate="ddlFilterSubProcess"
                                runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                                     </div>
                                  <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;margin-left: 0.5%;">
                                        <asp:DropDownListChosen runat="server" ID="ddlFilterFinancial" AutoPostBack="true" Width="80%" DataPlaceHolder="Finanacial Year"   class="form-control m-bot15" Style="width: 150px;"
                                OnSelectedIndexChanged="ddlFilterFinancial_SelectedIndexChanged">                                
                            </asp:DropDownListChosen>
                            <asp:CompareValidator ErrorMessage="Select Financial Year" ControlToValidate="ddlFilterFinancial"
                                runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />                            
                            <asp:ValidationSummary ID="ValidationSummary3" runat="server" CssClass="vdsummary" ForeColor="Red" ValidationGroup="ComplianceInstanceValidationGroup" />
                            <asp:CustomValidator ID="CustomValidator1" runat="server" EnableClientScript="False"
                                ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                            <asp:Label ID="lblErrorMessage" runat="server" Style="color: Red"></asp:Label>  
                            </div> 
                                            </div>
                                 </div>
                                </div>

                            <div class="clearfix"></div> 
                            <div class="clearfix" style="height:15px"></div>     
                                      
                                          <div style="float:right;">
                                                <asp:Button ID="btnUploadFile1" runat="server" Text="Download" OnClick="btnUploadFile1_Click" style="text-align:right;margin-right:10px;" class="btn btn-search" ValidationGroup="oplValidationGroup" />
                                          </div>   <div class="clearfix" style="height:15px"></div> 
                                                <div style="margin-bottom: 4px">
                                                <asp:Panel ID="panel" runat="server"  Width="100%">
                                                    <asp:UpdatePanel ID="upCompliance" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <asp:GridView ID="GridExportExcel" runat="server" OnDataBound="GridExportExcel_DataBound"
                                                                CssClass="table" GridLines="none" BorderWidth="0px" AutoGenerateColumns="false" AllowPaging="True" PageSize="5">
                                                                <Columns>
                                                                    <%--<asp:TemplateField HeaderText="Sr">
                                                                     <ItemTemplate>
                                                                      <%#Container.DataItemIndex+1 %>
                                                                    </ItemTemplate>
                                                                  </asp:TemplateField>
                                                                    <asp:BoundField DataField="ControlNo" HeaderText="Control No" ItemStyle-Width="150px" />
                                                                    <asp:BoundField DataField="RiskCreationId" HeaderText="Task Id" ItemStyle-Width="150px"/>
                                                                    <asp:TemplateField HeaderText="ProcessName">
                                                                        <ItemTemplate> <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                                            <asp:Label ID="ProcessName" runat="server" Text='<%# ShowProcessName((long)Eval("ProcessId")) %>' data-toggle="tooltip" data-placement="bottom"  ToolTip='<%# ShowProcessName((long)Eval("ProcessId")) %>'></asp:Label>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="SubProcessName">
                                                                        <ItemTemplate><div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                                            <asp:Label ID="SubProcessName" runat="server" Text='<%# ShowSubProcessName((long)Eval("ProcessId"),(long)Eval("SubProcessId")) %>' data-toggle="tooltip" data-placement="bottom"  ToolTip='<%# ShowSubProcessName((long)Eval("ProcessId"),(long)Eval("SubProcessId")) %>'></asp:Label>
                                                                       </div> </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Activity Description">
                                                                        <ItemTemplate><div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 250px;">
                                                                            <asp:Label ID="lblActivityDescription" runat="server" Text='<%# Eval("ActivityDescription") %>' data-toggle="tooltip" data-placement="bottom"  ToolTip='<%# Eval("ActivityDescription") %>'></asp:Label>
                                                                       </div> </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Control Objective">
                                                                        <ItemTemplate><div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 250px;">
                                                                            <asp:Label ID="lblControlObjective" runat="server" Text='<%# Eval("ControlObjective") %>' data-toggle="tooltip" data-placement="bottom"  ToolTip='<%# Eval("ControlObjective") %>'></asp:Label>
                                                                       </div> </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="RiskCategory">
                                                                        <ItemTemplate><div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                            <asp:Label ID="lblRiskCategory" runat="server" Text='<%# ShowRiskCategoryName((long)Eval("RiskCategory")) %>'></asp:Label>
                                                                       </div> </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Location">
                                                                        <ItemTemplate><div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                                            <asp:Label ID="lblClient" runat="server" Text='<%# ShowCustomerBranchName((long)Eval("RiskCategory"),(int)Eval("BranchId")) %>'></asp:Label>
                                                                        </div></ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Assertions">
                                                                        <ItemTemplate><div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 250px;">
                                                                            <asp:Label ID="lblAssertions" runat="server" Text='<%# ShowAssertionsName((long)Eval("RiskCategory")) %>' data-toggle="tooltip" data-placement="bottom"  ToolTip='<%# ShowAssertionsName((long)Eval("RiskCategory")) %>'></asp:Label>
                                                                       </div> </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="ControlDescription" HeaderText="Control Description" ItemStyle-Width="250px"  />
                                                                    <asp:BoundField DataField="MControlDescription" HeaderText="Mitigating Control Description" ItemStyle-Width="250px" />
                                                                    <asp:BoundField DataField="PersonResponsible" HeaderText="Person Responsible" ItemStyle-Width="250px" />
                                                                    <asp:BoundField DataField="Email" HeaderText="Email" ItemStyle-Width="250px"/>
                                                                    <asp:BoundField DataField="EffectiveDate" HeaderText="Effective Date" ItemStyle-Width="200px" />
                                                                    <asp:BoundField DataField="Key" HeaderText="Key" ItemStyle-Width="150px" />
                                                                    <asp:BoundField DataField="PreventiveControl" HeaderText="Preventive Control" ItemStyle-Width="250px" />
                                                                    <asp:BoundField DataField="AutomatedControl" HeaderText="Automated Control" ItemStyle-Width="250px"/>
                                                                    <asp:BoundField DataField="Frequency" HeaderText="Frequency" ItemStyle-Width="200px"/>
                                                                    <asp:BoundField DataField="GapDescription" HeaderText="Gap Description" ItemStyle-Width="250px"/>
                                                                    <asp:BoundField DataField="Recommendations" HeaderText="Recommendations" ItemStyle-Width="250px"/>
                                                                    <asp:BoundField DataField="ActionRemediationplan" HeaderText="Action Remediation Plan" ItemStyle-Width="250px" />
                                                                    <asp:BoundField DataField="IPE" HeaderText="Information Produced by Entity" ItemStyle-Width="250px" />
                                                                    <asp:BoundField DataField="ERPsystem" HeaderText="ERP System" ItemStyle-Width="200px" />
                                                                    <asp:BoundField DataField="FRC" HeaderText="Fraud Risk Control" ItemStyle-Width="250px" />
                                                                    <asp:BoundField DataField="UniqueReferred"  HeaderText="Unique Referred" ItemStyle-Width="250px"/>
                                                                    <asp:BoundField DataField="TestStrategy" HeaderText="Test Strategy" ItemStyle-Width="250px" />
                                                                    <asp:BoundField DataField="DocumentsExamined" HeaderText="Documents Examined" ItemStyle-Width="250px"/>
                                                                    <asp:BoundField DataField="RiskRating" HeaderText="Risk Rating" ItemStyle-Width="250px"/>
                                                                    <asp:BoundField DataField="ControlRating" HeaderText="Control Rating" ItemStyle-Width="250px"/>
                                                                --%>
                                                                    <asp:BoundField DataField="ControlNo" HeaderText="ControlNo" ItemStyle-Width="150px" />
                                                                    <asp:BoundField DataField="RiskCreationId" HeaderText="TaskId" ItemStyle-Width="150px" ItemStyle-CssClass="excelgriddisplay"  HeaderStyle-CssClass="excelgriddisplay"  />
                                                                    <asp:TemplateField HeaderText="Process">
                                                                        <ItemTemplate> <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                                            <asp:Label ID="ProcessName" runat="server" Text='<%# ShowProcessName((long)Eval("ProcessId")) %>' data-toggle="tooltip" data-placement="bottom"  ToolTip='<%# ShowProcessName((long)Eval("ProcessId")) %>'></asp:Label>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="SubProcess">
                                                                        <ItemTemplate><div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                            <asp:Label ID="SubProcessName" runat="server" Text='<%# ShowSubProcessName((long)Eval("ProcessId"),(long)Eval("SubProcessId")) %>' data-toggle="tooltip" data-placement="bottom"  ToolTip='<%# ShowSubProcessName((long)Eval("ProcessId"),(long)Eval("SubProcessId")) %>'></asp:Label>
                                                                       </div> </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Activity Description">
                                                                        <ItemTemplate><div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                                            <asp:Label ID="lblActivityDescription" runat="server" Text='<%# Eval("ActivityDescription") %>' data-toggle="tooltip" data-placement="bottom"  ToolTip='<%# Eval("ActivityDescription") %>'></asp:Label>
                                                                       </div> </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Control Objective">
                                                                        <ItemTemplate><div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                                            <asp:Label ID="lblControlObjective" runat="server" Text='<%# Eval("ControlObjective") %>' data-toggle="tooltip" data-placement="bottom"  ToolTip='<%# Eval("ControlObjective") %>'></asp:Label>
                                                                       </div> </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="RiskCategory">
                                                                        <ItemTemplate><div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                            <asp:Label ID="lblRiskCategory" runat="server" Text='<%# ShowRiskCategoryName((long)Eval("RiskCategory")) %>'></asp:Label>
                                                                       </div> </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Location"  ItemStyle-CssClass="excelgriddisplay"  HeaderStyle-CssClass="excelgriddisplay"    >
                                                                        <ItemTemplate><div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                                            <asp:Label ID="lblClient" runat="server" Text='<%# ShowCustomerBranchName((long)Eval("RiskCategory"),(int)Eval("BranchId")) %>'></asp:Label>
                                                                        </div></ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Assertions" ItemStyle-CssClass="excelgriddisplay"  HeaderStyle-CssClass="excelgriddisplay"    >
                                                                        <ItemTemplate><div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 250px;">
                                                                            <asp:Label ID="lblAssertions" runat="server" Text='<%# ShowAssertionsName((long)Eval("RiskCategory")) %>' data-toggle="tooltip" data-placement="bottom"  ToolTip='<%# ShowAssertionsName((long)Eval("RiskCategory")) %>'></asp:Label>
                                                                       </div> </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="ControlDescription" HeaderText="Control Description" ItemStyle-Width="250px" ItemStyle-CssClass="excelgriddisplay"  HeaderStyle-CssClass="excelgriddisplay" />
                                                                    <asp:BoundField DataField="MControlDescription" HeaderText="Mitigating Control Description" ItemStyle-Width="250px" ItemStyle-CssClass="excelgriddisplay" HeaderStyle-CssClass="excelgriddisplay" /> 
                                                                    <asp:BoundField DataField="PersonResponsible" HeaderText="Person Responsible" ItemStyle-Width="250px"  ItemStyle-CssClass="excelgriddisplay"  HeaderStyle-CssClass="excelgriddisplay" />
                                                                    <asp:BoundField DataField="Email" HeaderText="Email" ItemStyle-Width="250px"  ItemStyle-CssClass="excelgriddisplay"  HeaderStyle-CssClass="excelgriddisplay" />
                                                                    <asp:BoundField DataField="EffectiveDate" HeaderText="Effective Date" ItemStyle-Width="200px"  ItemStyle-CssClass="excelgriddisplay" HeaderStyle-CssClass="excelgriddisplay" />
                                                                    <asp:BoundField DataField="Key" HeaderText="Key" ItemStyle-Width="150px" ItemStyle-CssClass="excelgriddisplay" HeaderStyle-CssClass="excelgriddisplay" />
                                                                    <asp:BoundField DataField="PreventiveControl" HeaderText="Preventive Control" ItemStyle-Width="250px"  ItemStyle-CssClass="excelgriddisplay" HeaderStyle-CssClass="excelgriddisplay" />
                                                                    <asp:BoundField DataField="AutomatedControl" HeaderText="Automated Control" ItemStyle-Width="250px"  ItemStyle-CssClass="excelgriddisplay" HeaderStyle-CssClass="excelgriddisplay" /> 
                                                                    <asp:BoundField DataField="Frequency" HeaderText="Frequency" ItemStyle-Width="200px"  ItemStyle-CssClass="excelgriddisplay" HeaderStyle-CssClass="excelgriddisplay" />
                                                                    <asp:BoundField DataField="GapDescription" HeaderText="Gap Description" ItemStyle-Width="250px"  ItemStyle-CssClass="excelgriddisplay" HeaderStyle-CssClass="excelgriddisplay" /> 
                                                                    <asp:BoundField DataField="Recommendations" HeaderText="Recommendations" ItemStyle-Width="250px"  ItemStyle-CssClass="excelgriddisplay"  HeaderStyle-CssClass="excelgriddisplay" />
                                                                    <asp:BoundField DataField="ActionRemediationplan" HeaderText="Action Remediation Plan" ItemStyle-Width="250px"  ItemStyle-CssClass="excelgriddisplay"  HeaderStyle-CssClass="excelgriddisplay" />
                                                                    <asp:BoundField DataField="IPE" HeaderText="Information Produced by Entity" ItemStyle-Width="250px"  ItemStyle-CssClass="excelgriddisplay" HeaderStyle-CssClass="excelgriddisplay" />
                                                                    <asp:BoundField DataField="ERPsystem" HeaderText="ERP System" ItemStyle-Width="200px"  ItemStyle-CssClass="excelgriddisplay" HeaderStyle-CssClass="excelgriddisplay" />
                                                                    <asp:BoundField DataField="FRC" HeaderText="Fraud Risk Control" ItemStyle-Width="250px"  ItemStyle-CssClass="excelgriddisplay" HeaderStyle-CssClass="excelgriddisplay" />
                                                                    <asp:BoundField DataField="UniqueReferred"  HeaderText="Unique Referred" ItemStyle-Width="250px"  ItemStyle-CssClass="excelgriddisplay" HeaderStyle-CssClass="excelgriddisplay" />
                                                                    <asp:BoundField DataField="TestStrategy" HeaderText="Test Strategy" ItemStyle-Width="250px"  ItemStyle-CssClass="excelgriddisplay"  HeaderStyle-CssClass="excelgriddisplay" />
                                                                    <asp:BoundField DataField="DocumentsExamined" HeaderText="Documents Examined" ItemStyle-Width="250px"  ItemStyle-CssClass="excelgriddisplay"  HeaderStyle-CssClass="excelgriddisplay" />
                                                                    <asp:BoundField DataField="RiskRating" HeaderText="Risk Rating" ItemStyle-Width="250px"  ItemStyle-CssClass="excelgriddisplay"  HeaderStyle-CssClass="excelgriddisplay" />
                                                                    <asp:BoundField DataField="ControlRating" HeaderText="Control Rating" ItemStyle-Width="250px"  ItemStyle-CssClass="excelgriddisplay" HeaderStyle-CssClass="excelgriddisplay" />
                                                             
                                                                </Columns>
                                                                <RowStyle CssClass="clsROWgrid" />
                                                                <HeaderStyle CssClass="clsheadergrid" />
                                                        <PagerTemplate>
                                                            <table style="display: none">
                                                                <tr>
                                                                    <td>
                                                                    <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </PagerTemplate>                                                          
                                                            </asp:GridView>                                                                                                                       
                                                        </ContentTemplate>                                                         
                                                        <Triggers>
                                                            <asp:PostBackTrigger ControlID="btnUploadFile1" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </asp:Panel>
                                          <div class="col-md-5 colpadding0">
                                    <div class="table-Selecteddownload">
                                    <div class="table-Selecteddownload-text">
                                        <p><asp:Label runat="server" ID="lblTotalSelected" Text="" style="color: #999; margin-right: 10px;"></asp:Label></p>
                                    </div>                                   
                                </div>
                            </div>
                            <div class="col-md-6 colpadding0" style="float:right;">
                                <div class="table-paging" style="margin-bottom: 10px;">
                                    <asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick="lBPrevious_Click"/>                                  
                                    <div class="table-paging-text">
                                        <p>
                                            <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                            <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>
                                        </p>
                                    </div>
                                    <asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick="lBNext_Click"/>                                   
                                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                </div>
                            </div>                                           
                            </div>
                            </div>    
                                       </div>                        
                            </section>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnUploadFile" />
            <asp:PostBackTrigger ControlID="btnUploadFile1" />
        </Triggers>
    </asp:UpdatePanel>
    
    <script>
        $(document).ready(function () {
         
            setactivemenu('Import / Export Utility');
            fhead('Import / Export Utility');
        });
    </script>
</asp:Content>
